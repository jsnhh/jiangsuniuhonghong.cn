<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="black" name="apple-mobile-web-app-status-bar-style">
    <meta content="telephone=no" name="format-detection">
    <meta content="email=no" name="format-detection">
    <title>授权成功</title>
    <style type="text/css">
        .content{
            padding: 15px;
        }
        .authorize_success{

        }
        .authorize_success_img{
            display: block;
            width: 74px;
            margin: 0 auto;
        }
        .authorize_success_text{
            font-weight: 500;
            color: #333333;
            text-align: center;
            margin-top: 5px;
            margin: 0 auto;
            padding-top:24px;
        }
        .athorize{margin:0 auto;width: 85px;padding-top: 24px;}
    </style>
</head>
<body>
<section class="content">
    <div class="authorize_success">
        <div style="width:74px;margin:0 auto;padding-top:300px;">
            <img class="authorize_success_img" src="/school/images/cgsuccess.png">
        </div>
        <div class="authorize_success_text">追加授权成功</div>
        <div class="authorize_success_text" style="font-size: 14px;color: #666666;">恭喜你追加授权成功，还有一步即可完成创建小程序哦</div>
        <!-- <div class="athorize"> -->
           <button type="button" class="layui-btn next" style="margin: auto;border-radius: 2px;background: #1890FF;padding: 6px;color: #fff;outline:none;border:none;cursor:pointer;">进入下一步</button>
           <!--  <a class="layui-btn layui-btn-primary addcashier" lay-href="" style="background-color:#3475c3;border-radius: 5px;border:none;color:#fff;display: inline-block;width: 122px;">添加收银员</a> -->
        <!-- </div> -->
    </div>
</section>
</body>
</html>

<script type="text/javascript" src="<?php echo e(asset('/phone/js/jquery-2.1.4.js')); ?>"></script>
<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script src="<?php echo e(asset('/js/qrcode.min.js')); ?>"></script>
<script type="text/javascript">
 layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        steps: './extends/steps/steps'
    }).use(['index','form','table','laydate',"upload", 'steps'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate
            ,upload = layui.upload
            ,steps = layui.steps;

     $('.next').click(function(){
        steps.next(); //下一步
        location.href ="/mb/createApplet?"
        // $(this).attr('lay-href',"<?php echo e(url('/mb/createApplet?')); ?>"+store_id);
      });
});
   
</script>