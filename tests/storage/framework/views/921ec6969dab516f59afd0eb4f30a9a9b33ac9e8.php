<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>首页</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
<link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
</head>
<style>
.title{
  position :relative;
  top:35%;
};
.current{
background-color:red !importanrt;
}
</style>
<body style="padding:20px;">
<div id="app">
<div style="box-shadow:0px 0px 3px -2px #015;background-color:#ffffff">
<div class="header">
    <div class="Data-overview">
        <div style="padding: 20px; background-color: #ffffff;">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md6" style="padding:0px;padding-right:20px;">
              <div class="layui-card" style="box-shadow:0px 0px 10px #ebeef5">
                <div class="layui-card-header" style="height:60px">
                  <div class="title" style="border-left:5px solid blue;line-height:20px;font-size:18px;padding-left:10px;">
                  数据概览
                  </div>
                </div>
                <div class="layui-card-body">
                  <div class="content" style="display:flex;justify-content:space-between;padding:20px;cursor:pointer;">
                    <div class="item" style="width:150px;height:50px;text-align:center">
                        <div>
                          交易总额
                        </div>
                        <div style="padding-top:10px">
                        ￥
                        </div>
                    </div>
                    <div class="item" style="width:150px;height:50px;text-align:center">
                       <div>
                          佣金总额
                        </div>
                        <div style="padding-top:10px">
                        ￥
                       </div>
                    </div>
                    <div class="item" style="width:150px;height:50px;text-align:center">
                       <div>
                          二级代理商
                        </div>
                        <div style="padding-top:10px">
                        ￥
                       </div>
                    </div>
                    <div class="item" style="width:150px;height:50px;text-align:center">
                       <div>
                          三级代理商
                        </div>
                        <div style="padding-top:10px">
                        ￥
                       </div>
                    </div>
                    <div class="item" style="width:150px;height:50px;text-align:center">
                      <div>
                          总商户数
                        </div>
                        <div style="padding-top:10px">
                        ￥
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="layui-col-md6" style=";padding:0px;padding-left:20px">
              <div class="layui-card" style="box-shadow:0px 0px 10px #ebeef5">
                <div class="layui-card-header" style="height:60px">
                <div class="title" style="border-left:5px solid blue;line-height:20px;font-size:18px;padding-left:10px">
                最新公告
                  </div>
                </div>
                <div class="layui-card-body">
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>

<div class="transaction-data">
    <div class="Data-overview">
        <div style="padding: 20px; background-color: #ffffff;">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12" style="padding:0px">
              <div class="layui-card" style="box-shadow:0px 0px 10px #ebeef5">
                <div class="layui-card-header" style="height:60px">
                  <div class="title" style="border-left:5px solid blue;line-height:20px;font-size:18px;padding-left:10px">
                    交易数据
                  </div>
                  <div class="layui-form" style="float:right;position:relative">
                    <div style="height:38px;display:flex;justify-content:space-between;border:1px solid #ebeef5;margin-bottom:3px;position:relative;top:-9px;border-right:0px;">
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">今天</button>
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">昨天</button>
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">3天 </button>
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">7天 </button>
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">15天</button>
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">30天</button>
                    </div>
                    <div class="layui-form-item" style="position:absolute;top:-8px;right:400px">
                      <div class="layui-inline">
                        <div class="layui-input-inline">
                          <input type="text" class="layui-input" id="transaction-item" placeholder=" - ">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="layui-card-body">
                <div class="content" style="display:flex;justify-content:space-between;padding:20px;cursor:pointer;">
                    <div class="item" style="width:150px;height:50px;text-align:center">
                        <div>
                          交易金额
                        </div>
                        <div style="padding-top:10px">
                        ￥
                        </div>
                    </div>
                    <div class="item" style="width:150px;height:50px;text-align:center">
                       <div>
                          佣金笔数
                        </div>
                        <div style="padding-top:10px">
                        ￥
                       </div>
                    </div>
                    <div class="item" style="width:150px;height:50px;text-align:center">
                       <div>
                          退款金额
                        </div>
                        <div style="padding-top:10px">
                        ￥
                       </div>
                    </div>
                    <div class="item" style="width:150px;height:50px;text-align:center">
                       <div>
                          退款笔数
                        </div>
                        <div style="padding-top:10px">
                        ￥
                       </div>
                    </div>
                    <div class="item" style="width:150px;height:50px;text-align:center">
                      <div>
                          实际营收
                        </div>
                        <div style="padding-top:10px">
                        ￥
                      </div>
                    </div>
                    <div class="item" style="width:150px;height:50px;text-align:center">
                      <div>
                          我的佣金
                        </div>
                        <div style="padding-top:10px">
                        ￥
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
<div class="transaction-data">
    <div class="Data-overview">
        <div style="padding: 20px; background-color: #ffffff;">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12" style="padding:0px">
              <div class="layui-card" style="box-shadow:0px 0px 10px #ebeef5">
                <div class="layui-card-header" style="height:60px">
                  <div class="title" style="border-left:5px solid blue;line-height:20px;font-size:18px;padding-left:10px">
                  支付通道金额 / 笔数
                  </div>
                  <div class="layui-form" style="float:right;position:relative">
                    <div style="height:38px;display:flex;justify-content:space-between;border:1px solid #ebeef5;margin-bottom:3px;position:relative;top:-9px;border-right:0px;">
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">今天</button>
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">昨天</button>
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">3天 </button>
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">7天 </button>
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">15天</button>
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">30天</button>
                    </div>
                    <div class="layui-form-item" style="position:absolute;top:-8px;right:400px">
                      <div class="layui-inline">
                        <div class="layui-input-inline">
                          <input type="text" class="layui-input" id="transactions-item" placeholder=" - ">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="layui-card-body">
                <div class="content" style="display:flex;justify-content:space-between;padding:20px;cursor:pointer;">
                    <div class="item" style="width:150px;height:50px;text-align:center">
                        <div>
                        微信（金额 / 笔数）
                        </div>
                        <div style="padding-top:10px">
                        ￥
                        </div>
                    </div>
                    <div class="item" style="width:150px;height:50px;text-align:center">
                       <div>
                       支付宝（金额 / 笔数）
                        </div>
                        <div style="padding-top:10px">
                        ￥
                       </div>
                    </div>
                    <div class="item" style="width:150px;height:50px;text-align:center">
                       <div>
                       乐刷（金额 / 笔数）
                        </div>
                        <div style="padding-top:10px">
                        ￥
                       </div>
                    </div>
                    <div class="item" style="width:200pxx;height:50px;text-align:center">
                       <div>
                       天阙随行付（金额 / 笔数）
                        </div>
                        <div style="padding-top:10px">
                        ￥
                       </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>

<div class="transaction-data">
  <div class="Data-overview">
        <div style="padding: 20px; background-color: #ffffff;">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md6" style="padding:0px;padding-right:20px;">
              <div class="layui-card" style="box-shadow:0px 0px 10px #ebeef5">
                <div class="layui-card-header" style="height:60px">
                <div class="title" style="border-left:5px solid blue;line-height:20px;font-size:18px;padding-left:10px">
                数据统计
                  </div>
                  <div class="layui-form" style="float:right;position:relative">
                    <div class="layui-form-item" style="position:absolute;top:-8px;right:0px">
                      <div class="layui-inline">
                        <div class="layui-input-inline">
                          <input type="text" class="layui-input" id="statistics-item" placeholder=" - ">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="layui-card-body">
                  <div style="height:38px;width:396px;display:flex;justify-content:space-between;">
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">今天</button>
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">昨天</button>
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">3天 </button>
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">7天 </button>
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">15天</button>
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">30天</button>
                 </div>
                 <div id="main" style="width: 800px;height:400px;"></div>
                </div>
              </div>
            </div>
            <div class="layui-col-md6" style=";padding:0px;padding-left:20px">
              <div class="layui-card" style="box-shadow:0px 0px 10px #ebeef5">
                <div class="layui-card-header" style="height:60px">
                <div class="title" style="border-left:5px solid blue;line-height:20px;font-size:18px;padding-left:10px">
                支付方式占比
                  </div>
                </div>
                <div class="layui-card-body">
                <div style="height:38px;width:200px;display:flex;justify-content:space-between;">
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">交易金额</button>
                      <button type="button" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">交易笔数</button>
                 </div>
                  <div id="modeproportion" style="width: 800px;height:400px;margin-top:50px"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
   </div>
</div>
</div>
</div>
</body>
<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script src="<?php echo e(asset('/layuiadmin/echarts.min.js')); ?>"></script>
<script>

var token = localStorage.getItem("Usertoken");
layui.config({
    base: '../../layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index','form', 'upload','table','laydate'], function(){
    var $ = layui.$
        ,admin = layui.admin
        ,table = layui.table
        ,form = layui.form
        ,upload = layui.upload
        ,laydate = layui.laydate;
     $('.item').mouseover(function(){
     $(this).addClass('div1');
        if($('div').hasClass('div1')){
            $('.div1').css({
            'background-color':'#1E9FFF',
            'color':'#fff',
        });
        }
     });
     $('.item').mouseout(function(){
      $(this).removeClass('div1');
      $('.item').css({
            'background-color':'#ffffff',
            'color':'black',
        });
    });
      laydate.render({
        elem: '#transaction-item'
        ,type: 'datetime'
        ,trigger: 'click'
        ,range: true
        ,ready: function(){
          $('#layui-laydate1').css({
            'width':'548px',
        });
        }
      });
      laydate.render({
        elem: '#transactions-item'
        ,type: 'datetime'
        ,trigger: 'click'
        ,range: true
        ,ready: function(){
          $('#layui-laydate2').css({
            'width':'548px',
        });
        }
      });
      laydate.render({
        elem: '#statistics-item'
        ,type: 'datetime'
        ,trigger: 'click'
        ,range: true
        ,ready: function(){
          $('#layui-laydate3').css({
            'width':'548px',
        });
        }
      });

    var myChart = echarts.init(document.getElementById('main'));
    var option = {
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data: ['邮件营销', '联盟广告', '视频广告', '直接访问', '搜索引擎'],
        bottom:"0%"
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '15%',
        containLabel: true
    },
    toolbox: {
        feature: {
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
    },
    yAxis: {
        type: 'value'
    },
    series: [
        {
            name: '邮件营销',
            type: 'line',
            stack: '总量',
            data: [120, 132, 101, 134, 90, 230, 210]
        },
        {
            name: '联盟广告',
            type: 'line',
            stack: '总量',
            data: [220, 182, 191, 234, 290, 330, 310]
        },
        {
            name: '视频广告',
            type: 'line',
            stack: '总量',
            data: [150, 232, 201, 154, 190, 330, 410]
        },
        {
            name: '直接访问',
            type: 'line',
            stack: '总量',
            data: [320, 332, 301, 334, 390, 330, 320]
        },
        {
            name: '搜索引擎',
            type: 'line',
            stack: '总量',
            data: [820, 932, 901, 934, 1290, 1330, 1320]
        }
    ]
};
myChart.setOption(option);
var modeproportion = echarts.init(document.getElementById('modeproportion'));
option1 = {
    title: {
        text: '某站点用户访问来源',
        subtext: '纯属虚构',
        left: 'center'
    },
    tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
    },
    legend: {
        orient: 'vertical',
        right: 'right',
        data: ['直接访问', '邮件营销', '联盟广告', '视频广告', '搜索引擎']
    },
    series: [
        {
            name: '访问来源',
            type: 'pie',
            radius: '55%',
            center: ['50%', '60%'],
            data: [
                {value: 335, name: '直接访问'},
                {value: 310, name: '邮件营销'},
                {value: 234, name: '联盟广告'},
                {value: 135, name: '视频广告'},
                {value: 1548, name: '搜索引擎'}
            ],
            emphasis: {
                itemStyle: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};
modeproportion.setOption(option1);

$.ajax({
        url : "<?php echo e(url('api/user/order_data')); ?>",
        data : {token:token,l:100},
        type : 'post',
        dataType:'json',
        success : function(data) {
            // console.log(data);
            // var optionStr = "";
            //     for(var i=0;i<data.data.length;i++){
            //         optionStr += "<option value='" + data.data[i].company + "'>"
            //             + data.data[i].company_desc + "</option>";
            //     }
            //     $("#passway").append('<option value="">选择通道类型</option>'+optionStr);
            //     layui.form.render('select');
        },
    });



});
</script>
</html>
