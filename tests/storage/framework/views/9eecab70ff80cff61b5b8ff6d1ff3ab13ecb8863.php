<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>微信小程序授权管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style type="text/css">
        .xgrate{color: #fff;font-size: 15px;padding: 7px;height: 30px;line-height: 30px;background-color: #3475c3;}
        .up #uploadFile{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
        #demo5{width: 200px;}
    </style>
</head>
<body>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-fluid">
                    <div class="layui-row layui-col-space15">
                        <div class="layui-col-md12" style="margin-top:0px">
                            <div class="layui-card">
                                <div class="layui-card-header">微信小程序授权管理</div>
                                <div class="layui-card-body">
                                    <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                    <script type="text/html" id="table-content-list">

                                        {{#  if(d.authorizer_appid_status == 0){ }}
                                        <a class="layui-btn layui-btn-xs" lay-event="checkSuccess">审核成功，进行发布</a>
                                        {{#  } }}

                                        {{#  if(d.authorizer_appid_status == 1){ }}
                                        <a class="layui-btn layui-btn-xs" lay-event="checkRefuse">审核拒绝，重新提交审核</a>
                                        {{#  } }}

                                        {{#  if(d.authorizer_appid_status == 2){ }}
                                        <a class="layui-btn layui-btn-xs" lay-event="checkIng">审核中</a>
                                        {{#  } }}

                                        {{#  if(d.authorizer_appid_status == 3){ }}
                                        <a class="layui-btn layui-btn-xs" lay-event="checkCancel">已撤回审核</a>
                                        {{#  } }}

                                        {{#  if((d.authorizer_appid_status == null || d.authorizer_appid_status == '' || d.authorizer_appid_status == undefined) && d.authorizer_appid_status != 0){ }}
                                        <a class="layui-btn layui-btn-xs" lay-event="checkNotUpload">未上传代码模板，不可提交审核</a>
                                        {{#  } }}

                                        <a class="layui-btn layui-btn-xs" lay-event="setAppletName">修改小程序名称</a>
                                        <a class="layui-btn layui-btn-xs" lay-event="updateServerDomain">修改服务器域名</a>
                                        <a class="layui-btn layui-btn-xs" lay-event="selectMonthQuota">查询当月提审限额</a>
                                        <a class="layui-btn layui-btn-xs templateInfo" lay-event="templateInfo">模板消息</a>
                                        <a class="layui-btn layui-btn-xs categoryInfo" lay-event="categoryInfo">类目信息</a>
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="checkRefuseContent" class="hide layui-form" lay-filter="appletsInfo" style="display: none;background-color: #fff;">
        <div class="xgrate">小程序描述</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form">
                <div class="layui-form-item" pane="">
                    <label class="layui-form-label">描述内容</label>
                    <div class="layui-input-block">
                        <textarea name="desc" placeholder="请输入内容" class="layui-textarea textarea"></textarea>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <div class="layui-footer" style="left: 0;">
                        <button class="layui-btn submitAppletsInfo" lay-submit lay-filter="submitAppletsInfo" style="border-radius:5px">确定</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="submitAppletsNameInfo" class="hide layui-form" lay-filter="appletsName" style="display: none;background-color: #fff;">
        <div class="xgrate">小程序名称</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form">
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center">微信小程序名称</label>
                    <div class="layui-input-block">
                        <input type="text" placeholder="请输入微信小程序名称" name="wechat_name" class="layui-input title">
                    </div>
                </div>
                <div class="layui-card-body">
                    <label class="layui-form-label" style="text-align:center">门店经营执照（仅供微信审核小程序名称使用）</label>
                    <div class="layui-upload">
                        <button class="layui-btn up" style="border-radius:5px;position: relative;">
                            <i class="layui-icon">&#xe67c;</i>
                            <input type="file" name="file" id="uploadFile" style="position: absolute;top: 0;left: 0;" />上传营业执照
                        </button>
                        <div class="layui-upload-list">
                            <img class="layui-upload-img" id="demo5">
                            <p id="demoText"></p>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="license" value="" />
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <div class="layui-footer" style="left: 0;">
                        <button class="layui-btn submitAppletsNameInfo" lay-submit lay-filter="submitAppletsNameInfo" style="border-radius:5px">确定</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="updateServerDomain" class="hide layui-form" lay-filter="serverDomain" style="display: none;background-color: #fff;">
        <div class="xgrate">小程序服务器域名</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form">
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center">服务器域名</label>
                    <div class="layui-input-block">
                        <input type="text" placeholder="请输入服务器域名" name="domain" class="layui-input title">
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <div class="layui-footer" style="left: 0;">
                        <button class="layui-btn updateServerDomain" lay-submit lay-filter="updateServerDomain" style="border-radius:5px">确定</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <input name="applets" type="hidden" value="<?php echo e(url('/user/appletWeChatTemplate')); ?>"></input>
    </div>

</body>
<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script type="text/javascript">
    var token = localStorage.getItem("Usertoken");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form', 'upload','table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,table = layui.table
            ,form = layui.form
            ,upload = layui.upload
            ,laydate = layui.laydate;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="<?php echo e(url('/user/login')); ?>";
            }
        });

        /**
         * 进入该页面，初始化渲染该表格
         */
        layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
        table.render({
            elem: '#test-table-page',
            url: "<?php echo e(url('/api/customer/weixin/getAuthorizerList')); ?>",
            method: 'post',
            where:{
                token:token
            },
            request:{
                pageName: 'page',
                limitName: 'count'
            },
            page: true,
            cellMinWidth: 100,
            cols: [
                [
                    {width:200,field:'authorizer_appid', title: '小程序appid',templet: '#appletsId'},
                    {width:200,field:'authorizer_appid_name', title: '小程序名称'},
                    {width:100,field:'authorizer_appid_status',  title: '审核状态'},
                    {width:200,field:'authorizer_appid_status_reason',  title: '审核原因'},
                    {width:950,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
                ]
            ],
            response: {
                statusName: 'status', //数据状态的字段名称，默认：code
                statusCode: 200, //成功的状态码，默认：0
                msgName: 'message', //状态信息的字段名称，默认：msg
                countName: 't', //数据总数的字段名称，默认：count
                dataName: 'data', //数据列表的字段名称，默认：data
            },
            done: function(res, curr, count){
                layer.msg("已完成", {
                    offset: '50px'
                    ,icon: 1
                    ,time: 1000
                });
                //进行表头样式设置
                $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});
            }
        });

        /**
         * 上传营业执照
         **/
        var uploadInst = upload.render({
            elem: '#uploadFile', //绑定元素
            url: "<?php echo e(url('/api/customer/weixin/uploadFile')); ?>", //上传接口
            field:"file",
            method:"post",
            type : 'images',
            ext : 'jpg|png|gif',
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                //上传完毕回调
                if(res.status == 200){
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:1000});
                    layui.jquery('#demo5').attr("src", res.data.path);
                    $("input[name=license]").val(res.data.media_id);
                }else{
                    layer.msg(res.message, {icon:2, shade:0.5, time:1000});
                }
            },
            error: function(err){
                //请求异常回调
                console.log(err);
            }
        });

        /**
         * 表格的操作列中的每个操作项
         */
        table.on('tool(test-table-page)', function(obj){
            var lineData = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            if(layEvent === 'checkRefuse'){
                //审核拒绝，重新提交审核
                var checkRefuseContent = layer.open({
                    type: 1,
                    title: false,
                    closeBtn: 0,
                    area: '516px',
                    skin: 'layui-layer-nobg', //没有背景色
                    shadeClose: true,
                    content: $('#checkRefuseContent')
                });
                //点击submit进行提交
                form.on("submit(submitAppletsInfo)",function(data){
                    var requestData = data.field;
                    layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                    $.post("<?php echo e(url('/api/customer/weixin/submit_audit')); ?>",{
                        authorizer_appid:lineData.authorizer_appid,
                        refresh_token:lineData.refresh_token,
                        version_desc:requestData.desc
                    },function(data){
                        var status = data.status;
                        if(status == 200){
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 1000
                            },function(){
                                layer.close(checkRefuseContent);
                                window.location.reload();
                            });
                        }else{
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 1000
                            });
                        }
                    },"json");
                    return false;
                });
            }else if(layEvent === 'checkSuccess'){
                //审核成功，进行发布代码
                layer.confirm('确认进行发布代码?',{icon: 2}, function(index){
                    layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                    $.post("<?php echo e(url('/api/customer/weixin/releaseApplet')); ?>",{
                        authorizer_appid:lineData.authorizer_appid,
                        refresh_token:lineData.refresh_token
                    },function(data){
                        var status = data.status;
                        if(status == 200){
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 1000
                            },function(){
                                layer.close(index);
                            });
                        }else{
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 1000
                            });
                        }
                    },"json");
                });
            }else if(layEvent === 'setAppletName'){
                //设置小程序名称
                var submitAppletsNameInfo = layer.open({
                    type: 1,
                    title: false,
                    closeBtn: 0,
                    area: '516px',
                    skin: 'layui-layer-nobg', //没有背景色
                    shadeClose: true,
                    content: $('#submitAppletsNameInfo')
                });
                //点击submit进行提交
                form.on("submit(submitAppletsNameInfo)",function(data){
                    var requestData = data.field;
                    if(!requestData.wechat_name){
                        layer.msg("微信小程序名称不可为空", {
                            offset: '50px'
                            ,icon: 2
                            ,time: 2000
                        });
                        return false;
                    }

                    if(!requestData.license){
                        layer.msg("门店经营执照不可为空", {
                            offset: '50px'
                            ,icon: 2
                            ,time: 2000
                        });
                        return false;
                    }
                    layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                    $.post("<?php echo e(url('/api/customer/weixin/checkWxVerifyNickname')); ?>",{
                        authorizer_appid:lineData.authorizer_appid,
                        refresh_token:lineData.refresh_token,
                        nick_name:requestData.wechat_name,
                        license:requestData.license,
                    },function(data){
                        var status = data.status;
                        if(status == 200){
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 1000
                            },function(){
                                layer.close(submitAppletsNameInfo);
                                window.location.reload();
                            });
                        }else{
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 1000
                            });
                        }
                    },"json");
                    return false;
                });
            }else if(layEvent === 'updateServerDomain'){
                //设置小程序服务器域名
                var updateServerDomain = layer.open({
                    type: 1,
                    title: false,
                    closeBtn: 0,
                    area: '516px',
                    skin: 'layui-layer-nobg', //没有背景色
                    shadeClose: true,
                    content: $('#updateServerDomain')
                });
                //点击submit进行提交
                form.on("submit(updateServerDomain)",function(data){
                    var requestData = data.field;
                    if(!requestData.domain){
                        layer.msg("服务器域名不可为空", {
                            offset: '50px'
                            ,icon: 2
                            ,time: 2000
                        });
                        return false;
                    }

                    layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                    $.post("<?php echo e(url('/api/customer/weixin/updateServerDomain')); ?>",{
                        authorizer_appid:lineData.authorizer_appid,
                        refresh_token:lineData.refresh_token,
                        domain:requestData.domain
                    },function(data){
                        var status = data.status;
                        if(status == 200){
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 1000
                            },function(){
                                layer.close(updateServerDomain);
                            });
                        }else{
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 1000
                            });
                        }
                    },"json");
                    return false;
                });
            }else if(layEvent === 'selectMonthQuota'){
                //查看Quota值
                layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                $.post("<?php echo e(url('/api/customer/weixin/queryQuota')); ?>",{
                    authorizer_appid:lineData.authorizer_appid,
                    refresh_token:lineData.refresh_token,
                },function(data){
                    var status = data.status;
                    if(status == 200){
                        var rest = data.data.rest;
                        var limit = data.data.limit;
                        var message = "quota剩余值:  "+rest+"; 当月分配quota:  "+limit;
                        layer.msg(message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 3000
                        });
                    }else{
                        layer.msg(data.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 1000
                        });
                    }
                },"json");
                return false;
            }else if(layEvent === "templateInfo"){
                var authorizer_appid = lineData.authorizer_appid;
                var refresh_token = lineData.refresh_token;
                $('.templateInfo').attr('lay-href',"<?php echo e(url('/user/appletWeChatTemplate?')); ?>"+"authorizer_appid="+authorizer_appid+"&refresh_token="+refresh_token);
            }else if(layEvent === "categoryInfo"){
                var authorizer_appid = lineData.authorizer_appid;
                var refresh_token = lineData.refresh_token;
                $('.categoryInfo').attr('lay-href',"<?php echo e(url('/user/appletWeChatCateGory?')); ?>"+"authorizer_appid="+authorizer_appid+"&refresh_token="+refresh_token);
            }

        });
    });

</script>
</html>