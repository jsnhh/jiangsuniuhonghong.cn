<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>新蓝海列表页</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .tongbu {
            background-color: #4c9ef8;
            color: #fff;
        }

        .cur {
            color: #009688;
        }

        .userbox,
        .storebox {
            height: 200px;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 63px;
            width: 498px;
            background-color: #ffffff;
            border: 1px solid #ddd;
        }

        .userbox .list,
        .storebox .list {
            height: 38px;
            line-height: 38px;
            cursor: pointer;
            padding-left: 10px;
        }

        .userbox .list:hover,
        .storebox .list:hover {
            background-color: #eeeeee;
        }

        .s_id {
            line-height: 36px;
        }

        .layui-form-radio {
            margin-left: 10px;
            margin-bottom: 7px;
        }

    </style>
</head>

<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">

            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12" style="margin-top:0px">
                        <div class="layui-card">
                            <div class="layui-card-header">新蓝海列表页</div>

                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">
                                    <!-- 搜索 -->
                                    <div class="layui-form" lay-filter="component-form-group" style="width:800px;display: inline-block;">
                                        <div class="layui-form-item" style="margin-left:5px;">
                                            <!-- <div class="layui-inline" style='margin-right:0'>
                                                <div class="layui-input-inline" style="width:185px">
                                                    <text class="yname">门店ID</text>
                                                    <input type="text" style="border-radius:5px" name="schoolname" placeholder="请输入门店名称或者门店ID" autocomplete="off" class="layui-input">
                                                </div>
                                            </div> -->
                                            <div class="layui-inline" style='margin-right:0'>
                                                <div class="layui-input-inline" style="width:185px">
                                                    <text class="yname">门店名称</text>
                                                    <input type="text" style="border-radius:5px" name="tradeno" placeholder="请输入门店名称" autocomplete="off" class="layui-input">
                                                </div>
                                            </div>

                                            <div class="layui-inline">
                                                <button class="layui-btn layuiadmin-btn-list" lay-submit="" lay-filter="LAY-app-contlist-search" style="border-radius:5px;margin-top:20px;margin-bottom: 0;height:36px;line-height: 36px;">
                                                    <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>


                                <script type="text/html" id="table-content-list" class="layui-btn-small">
                                    <a class="layui-btn layui-btn-normal layui-btn-xs iotOperation" style="background-color:#52C41A;border-radius: 2px;margin-right:15px;" lay-event="iotOperation">IOT授权</a>
                                    <a class="layui-btn layui-btn-normal layui-btn-xs bluesea" style="background-color:##FA6400;border-radius: 2px;margin-right:15px;" lay-event="bluesea">新蓝海报名</a>
                                    <a class="layui-btn layui-btn-normal layui-btn-xs newBluelist" style="border-radius: 2px;margin-right:15px;" lay-event="newBluelist">新蓝海列表</a>
                                    <a class="layui-btn layui-btn-normal layui-btn-xs operateList" style="border-radius: 2px;" lay-event="operateList">代运营授权列表</a>
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<input type="hidden" class="source_type">
<input type="hidden" class="source_type_desc">
<input type="hidden" class="user_id">
<input type="hidden" class="daili_id">
<input type="hidden" class="daili_name">
<input type="hidden" class="store_id">
<input type="hidden" class="store_name">



<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script>
    var token = localStorage.getItem("Usertoken");
    var str = location.search;
    var user_id = str.split('?')[1];


    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'table', 'laydate'], function() {
        var $ = layui.$,
            admin = layui.admin,
            form = layui.form,
            table = layui.table,
            laydate = layui.laydate;
        // 未登录,跳转登录页面
        $(document).ready(function() {
            if (token == null) {
                window.location.href = "<?php echo e(url('/user/login')); ?>";
            }

        })




        // 渲染表格
        table.render({
            elem: '#test-table-page'
            ,url: "<?php echo e(url('/api/user/store_pc_lists')); ?>"
            ,method: 'post'
            ,where:{
                token:token,
                is_close: $('.open_id').val(),
                is_delete: $('.del_id').val()
            }
            ,request:{
                pageName: 'p',
                limitName: 'l'
            }
            ,page: true
            ,cellMinWidth: 150
            ,cols: [[
                {field:'store_id',  title: '门店id'}
                ,{field:'store_name', title: '门店名称'}
                ,{align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
            ]]
            ,response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                ,statusCode: 1 //成功的状态码，默认：0
                ,msgName: 'message' //状态信息的字段名称，默认：msg
                ,countName: 't' //数据总数的字段名称，默认：count
                ,dataName: 'data' //数据列表的字段名称，默认：data
            }
            ,done: function(res, curr, count){
                //console.log(res);
                $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'}); //进行表头样式设置
            }
        });
        //监听搜索
        form.on('submit(LAY-app-contlist-search)', function(data){
            var obj = data.field;
            console.log(obj);
            var store_name = data.field.tradeno;
            var store_id = data.field.schoolname;

            $('.store_id').val(store_id);
            $('.store_name').val(store_name);


            // console.log(data);
            //执行重载
            table.reload('test-table-page', {
                page: {
                    curr: 1
                },
                where: {
                    store_name:store_name,
                    store_id:store_id
                }
            });
        });


        table.on('tool(test-table-page)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var e = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            console.log(e);
            // localStorage.setItem('s_store_id', e.store_id);

            if(layEvent === 'iotOperation'){
                localStorage.setItem("iotstore_id",e.store_id);
                localStorage.setItem("iotid",e.id);
                $('.iotOperation').attr('lay-href',"<?php echo e(url('/user/addoperation?id=')); ?>"+e.id+"&store_id="+e.store_id);
                

            }else if(layEvent === 'bluesea'){
                $('.bluesea').attr('lay-href',"<?php echo e(url('/user/bluesea?id=')); ?>"+e.id+"&store_id="+e.store_id);
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
            }else if(layEvent === 'newBluelist'){
                $('.newBluelist').attr('lay-href',"<?php echo e(url('/user/bluesealist?id=')); ?>"+e.id+"&store_id="+e.store_id);
                

                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
            }else if(layEvent === 'operateList'){
                $('.operateList').attr('lay-href',"<?php echo e(url('/user/operationlist?id=')); ?>"+e.id+"&store_id="+e.store_id);
                
            }
        });


    });
</script>

</body>

</html>