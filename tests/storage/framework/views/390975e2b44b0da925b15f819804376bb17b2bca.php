<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>添加哆啦宝进件信息</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
<link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
<link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/formSelects-v4.css')); ?>" media="all">
<style>
    .img img{width:100%;height:100%;}
    .layui-card-header{width:80px;text-align: right;float:left;}
    .layui-card-body{margin-left:28px;}
    .layui-upload-img{width: 100px; height: 92px; /*margin: 0 10px 10px 0;*/}
    .up{position: relative;display: inline-block;cursor: pointer;border-color: #1ab394; color: #FFF;width: auto !important;font-size: 10px !important;text-align: center !important;}
    .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
    .layui-upload-list{width: 100px;height:96px;overflow: hidden;margin: 10px auto;}
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {-webkit-appearance: none !important;margin: 0;}
    .userbox,.branchbox{
        height:200px;
        overflow-y: auto;
        z-index: 999;
        position: absolute;
        left: 0px;
        top: 85px;
        width:400px;
        background-color:#ffffff;
        border: 1px solid #ddd;
    }
    .userbox .list,.branchbox .list{
        height:38px;line-height: 38px;cursor:pointer;
        padding-left:10px;
    }
    .userbox .list:hover,.branchbox .list:hover{
        background-color:#eeeeee;
    }
</style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card" style="margin-top: 0px">
        <div class="layui-card-header">补充信息</div>
        <div class="layui-card-body layui-row layui-col-space10">
            <div class="layui-row layui-form">
                <div class="layui-col-md8">
                <form class="layui-form">
                    <div class="layui-form-item" style="width:60%">
                        <label class="layui-form-label" style="text-align:center;">经营类型</label>
                        <div class="layui-input-block">
                            <select name="microBizType" id="microBizType" lay-filter="">
                                <option value="MICRO_TYPE_STORE">门店场所</option>
                                <option value="MICRO_TYPE_MOBILE">流动经营/便民服务</option>
                                <option value="MICRO_TYPE_ONLINE">线上商品/服务交易</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item primary_industry" style="width:60%">
                        <label class="layui-form-label" style="text-align:center;">店铺一级行业</label>
                        <div class="layui-input-block">
                            <select  name="industry" id="industry" lay-filter="industry" childrenSelectId="second_industry">
                            <!-- <option value="">请选择</option> -->
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item" style="width:60%">
                        <label class="layui-form-label" style="text-align:center;">店铺二级行业</label>
                        <div class="layui-input-block">
                            <select name="second_industry" id="second_industry" lay-filter="second_industry" childrenSelectId="province" >
                                <!-- <option value="">请选择</option> -->
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item" style="width:60%">
                        <label class="layui-form-label" style="text-align:center;">商户所属省份</label>
                        <div class="layui-input-block">
                            <select name="province" id="province" lay-filter="province"></select>
                        </div>
                    </div>
                    <div class="layui-form-item storeitem" style="width:60%">
                        <label class="layui-form-label" style="text-align:center;">商户所属城市</label>
                        <div class="layui-input-block">
                            <select name="city" id="city" lay-filter="city"></select>
                        </div>
                    </div>
                    <div class="layui-form-item" style="width:60%">
                        <label class="layui-form-label" style="text-align:center;">银行名称</label>
                        <div class="layui-input-block js_bank">
                            <select name="bank" id="bank" lay-filter="bank" lay-search></select>
                            <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="搜索所属银行" class="layui-input transfer" style='margin-top:10px;'>
                            <div class="userbox" style='display: none'></div>
                        </div>
                    </div>
                    <div class="layui-form-item" style="width:60%">
                        <label class="layui-form-label" style="text-align:center;">银行分行名称</label>
                        <div class="layui-input-block subbank">
                            <select name="subbank" id="subbank" lay-filter="subbank" lay-search></select>
                            <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="搜索所属支行" class="layui-input branchbank" style='margin-top:10px;'>
                            <div class="branchbox" style='display: none'></div>
                        </div>
                    </div>
                    <div class="layui-form-item storeitem" style="width:60%">
                        <label class="layui-form-label" style="text-align:center;">支付类型编号</label>
                        <div class="layui-input-block">
                            <select name="payment_type_no" id="payment_type_no" xm-select="payment_type_no">
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item storeitem" style="width:60%">
                        <label class="layui-form-label" style="text-align:center;">费率</label>
                        <div class="layui-input-block">
                            <select name="rate" id="rate" lay-filter="rate"></select>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="layui-col-md4" >
                    <div class="layui-card">
                        <div class="layui-card-body" style="margin-left:28px;padding:0 50px;float:left;">
                            <div class="layui-upload">
                                <button class="layui-btn up" style="border-radius:5px;left:6px">
                                    <input type="file" name="img_upload" class="image_file">结算人手持结算卡
                                </button>
                                <div class="layui-upload-list">
                                    <img class="layui-upload-img" id="demo">
                                    <p id="demoText"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block" style="margin-left:440px">
            <div class="layui-footer" style="left: 0;">
                <button class="layui-btn submit" style="border-radius:5px">保存</button>
            </div>
        </div>
    </div>
</div>

<!-- 商户性质 -->
<input type="hidden" class="store_name" value="">
<input type="hidden" class="store_type" value="">
<input type="hidden" class="category_name" value="">
<input type="hidden" class="category_id" value="">

<!-- 卡类型 -->
<input type="hidden" class="cardtype_id" value="">

<!-- 银行卡 -->
<input type="hidden" class="bankName" value="">
<input type="hidden" class="sub_bank_name" value="">
<input type="hidden" class="bank_no" value="">

<!-- 地区 -->
<input type="hidden" class="provincecode" value="">
<input type="hidden" class="provincename" value="">
<input type="hidden" class="citycode" value="">
<input type="hidden" class="cityname" value="">
<input type="hidden" class="areacode" value="">
<input type="hidden" class="areaname" value="">

<!-- 银行卡卡户地区 -->
<input type="hidden" class="provincecodebank" value="">
<input type="hidden" class="provincenamebank" value="">
<input type="hidden" class="citycodebank" value="">
<input type="hidden" class="citynamebank" value="">
<input type="hidden" class="areacodebank" value="">
<input type="hidden" class="areanamebank" value="">

<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script>
    var token = localStorage.getItem("Usertoken");
    var store_id = localStorage.getItem("store_store_id");

    var province_code = localStorage.getItem("store_province_code");
    var city_code = localStorage.getItem("store_city_code");
    var area_code = localStorage.getItem("store_area_code");

    var str = location.search;
    var store_id_add = str.split('?')[1];

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;
            formSelects.render('payment_type_no');
            formSelects.btns('payment_type_no', []);
            var arrr=[];

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token == null){
                window.location.href="<?php echo e(url('/user/login')); ?>";
            }
        });

        // 店铺一级行业
        $.ajax({
            url : "<?php echo e(url('/api/user/dlb_store_info')); ?>",
            data : {
                token:token
                ,select_type:1
                ,store_id:store_id
            },
            type : 'post',
            success : function(data) {
                const newData = JSON.parse(data).data;
                var optionStr = "";
                for(var i=0;i<newData.length;i++){
                    optionStr += "<option value='" + newData[i].industryNum + "'>"
                                      + newData[i].industryName + "</option>";
                }
                $("#industry").append('<option value=""></option>'+optionStr);
                layui.form.render('select');
            },
            error : function(data) {
                alert('查找板块报错');
            }
        });

        //  店铺2级行业
        form.on('select(industry)', function(data){
            let category = data.value;
            let categoryName = data.elem[data.elem.selectedIndex].text;
            // $('.store_name').val(category);
            // $('.store_type').val(categoryName);
            $("#second_industry").html('');
            $.ajax({
                url : "<?php echo e(url('/api/user/dlb_store_info')); ?>",
                data : {
                    token:token,
                    select_type:2,
                    store_id:store_id,
                    industry_num:category
                },
                type : 'post',
                success : function(data) {
                    const newData = JSON.parse(data).data;
                    var optionStr = "";
                    for(var i=0;i<newData.length;i++){
                        optionStr += "<option value='" + newData[i].industryNum + "'>"
                                          + newData[i].industryName + "</option>";
                    }
                    $("#second_industry").append('<option value=""></option>'+optionStr);
                    layui.form.render('select');
                },
                error : function(data) {
                    alert('查找板块报错');
                }
            });
        });

        // 商户所属省份
        $.ajax({
            url : "<?php echo e(url('/api/user/dlb_store_info')); ?>",
            data : {
                token:token
                ,select_type:3
                ,store_id:store_id
                // ,industry_num:category
            },
            type : 'post',
            success : function(data) {
                const newData = JSON.parse(data).data;
                // console.log(newData)
                var optionStr = "";
                    for(var i=0;i<newData.length;i++){
                        optionStr += "<option value='" + newData[i].code + "'>"
                                          + newData[i].name + "</option>";
                    }
                    $("#province").append('<option value=""></option>'+optionStr);
                    layui.form.render('select');
            },
            error : function(data) {
                alert('查找板块报错');
            }
        });

        // 商户所属城市
        form.on('select(province)', function(data){
            let category = data.value;
            let categoryName = data.elem[data.elem.selectedIndex].text;

            $("#city").html('');
            $.ajax({
                url : "<?php echo e(url('/api/user/dlb_store_info')); ?>",
                data : {
                    token:token,
                    select_type:4,
                    store_id:store_id,
                    province_code:category
                },
                type : 'post',
                success : function(data) {
                    //  console.log(data)
                    const newData = JSON.parse(data).data;

                    var optionStr = "";
                    for(var i=0;i<newData.length;i++){
                        optionStr += "<option value='" + newData[i].code + "'>"
                                        + newData[i].name + "</option>";
                    }
                    $("#city").append('<option value=""></option>'+optionStr);
                    layui.form.render('select');
                },
                error : function(data) {
                    alert('查找板块报错');
                }
            });
        });

        // 输入选择银行
        $(".transfer").bind("input propertychange",function(event){
//          console.log($(this).val(), 66666);
          $('.bankName').val($(this).val());

          $.post("<?php echo e(url('/api/user/dlb_store_info')); ?>",
          {
              token:token
              ,select_type:5
              ,store_id:store_id
              ,bank_keywords:$(this).val()
          },function(res){
//              console.log(res, 77777777);
              var html="";
              if (res.data) {
                for(var i=0;i<res.data.length;i++){
                  html+='<div class="list" data-name='+res.data[i].bankName+' data='+res.data[i].bankCode+'>'+res.data[i].bankName+'</div>';
                }
                  $(".userbox").show();
                  $('.userbox').html('');
                  $('.userbox').append(html);
              }
          },"json");
        });

        var bankCode = "";
        $(".userbox").on("click",".list",function(){
            $('.userbox').hide();

            $(".transfer").val('');
            var bankName=$(this).data('name');
            bankCode = $(this).attr("data");

            $('.bankName').val(bankName);
            $.post("<?php echo e(url('/api/user/dlb_store_info')); ?>",
            {
                token:token
                ,select_type:5
                ,store_id:store_id
                ,bank_keywords:bankName
            },function(data){
//                console.log(data);
                if(data.status==1){
                    var optionStr = "";
                    for(var i=0;i<data.data.length;i++){
                        optionStr += "<option value='"  + data.data[i].bankCode + "' "+((bankName==data.data[i].bankName)?"selected":"")+" >" + data.data[i].bankName + "</option>";
                    }
                    $("#bank").append('<option value="">请选择银行</option>'+optionStr);
                    layui.form.render('select');
                }
            },"json");
        });

        // 输入选择支行
        $(".branchbank").bind("input propertychange",function(event){
//            console.log($(this).val());
            // var bankCode = $(this).attr('data');
            if(!bankCode){
                alert("银行编码信息没获取到");
                return false;
            }

            $.post("<?php echo e(url('/api/user/dlb_store_info')); ?>",
            {
                token:token
                ,select_type:6
                ,store_id:store_id
                ,bank_code:bankCode
                ,sub_branch_keywords:$(this).val()
            },function(res){
                //console.log(res);
                var html="";
                if(res.status != -1){
                    for(var i=0;i<res.data.length;i++){
                        html+='<div class="list" data='+res.data[i]+'>'+res.data[i]+'</div>';
                    }
                    $(".branchbox").show();
                    $('.branchbox').html('');
                    $('.branchbox').append(html);
                }
            },"json");
        });

        /**
         * 显示支行名称
         */
        $(".branchbox").on("click",".list",function(){
            $('.branchbox').hide();
            var bankno=$(this).attr('data');
            var sub_bank_name=$(this).html();

            $('.bank_no').val(bankno);
            $('.sub_bank_name').val(sub_bank_name);
            $(".branchbank").val(sub_bank_name);
            $('.item12').val(bankno);

            $('.subbank .layui-form-select .layui-select-title input').val('');

            $('.subbank .layui-form-select .layui-anim-upbit').append(function(){
                var str = '<dd lay-value="" class="layui-this">'+sub_bank_name+'</dd>';
                return str;
            });
        });

        // 选择银行卡
        form.on('select(bank)', function(data){
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.bankName').val(category);
            $.post("<?php echo e(url('/api/user/dlb_store_info')); ?>",
            {
                token:token
                ,select_type:6
                ,store_id:store_id
                ,bank_code:bankCode
                ,sub_branch_keywords:$(this).val()
            },function(data){
//                console.log(data);
                if(data.status==1){
                    var optionStr = "";
                    for(var i=0;i<data.data.length;i++){
                        optionStr += "<option value='" + data.data[i] + "'>"+ data.data[i] + "</option>";
                    }
                    $("#subbank").html('');
                    $("#subbank").append('<option value="">选择所属支行</option>'+optionStr);
                    layui.form.render('select');
                }
            },"json");
        });

        //  选择支行
        form.on('select(subbank)', function(data){
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.item12').val(category);
            $('.sub_bank_name').val(categoryName);
            $('.bank_no').val(category);
            $('.branchbank').val('')
        });

        // 支付类型编号
        $('#payment_type_no').html('');
        formSelects.config('payment_type_no', {
            searchUrl: "<?php echo e(url('/api/user/dlb_store_info?token=')); ?>"+token+'&select_type='+7+"&store_id="+store_id,
            searchName: 'user_name',
            beforeSuccess: function(id, url, searchVal, result){
                //我要把数据外层的code, msg, data去掉
//                console.log(result);
                result = result.data;
                var arrr=[];
                for(var i=0;i<result.length;i++){
                    var data ={"value":result[i].num,"name":result[i].name};
                    arrr.push(data);
                }
//                console.log(arrr);
                //然后返回数据
                return arrr;
            }
        }).data('payment_type_no', 'server', {

        });


         // 费率

         $.ajax({
            url : "<?php echo e(url('/api/user/dlb_store_info')); ?>",
            data : {
                token:token
                ,select_type:7
                ,store_id:store_id
            },
            type : 'post',
            success : function(data) {
                const newData = JSON.parse(data).data;

                var optionStr = "";
                for(var i=0;i<newData.length;i++){
                    optionStr += "<option value='" + newData[i].num + "'>"
                                    + newData[i].rate + "</option>";
                }
                $("#rate").append('<option value=""></option>'+optionStr);
                layui.form.render('select');
            },
            error : function(data) {
                alert('查找板块报错');
            }
        });



        // 上传图片
        var uploadInst = upload.render({
            url : "<?php echo e(url('/api/basequery/webupload?act=images')); ?>"+'&token='+token,  //提交到的地址 可以自定义其他参数
            elem : '.image_file',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            ext : 'jpg|png|gif',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    layui.jquery('#demo').attr("src", res.data.img_url);
                }
            }
        });

        $('.submit').click(function(){
            $.post("<?php echo e(url('/api/user/up_store')); ?>",
            {
                token:token
                ,store_id:store_id
                ,DLB_micro_biz_type:$('#microBizType').val()
                ,DLB_industry:$('.store_type').val()
                ,DLB_second_industry:$('.category_name').val()
                ,DLB_province:$('.provincecode').val()
                ,DLB_city:$('.citycode').val()
                ,DLB_bank:$('.bankname').val()
                ,DLB_sub_bank:$('.sub_bank_name').val()
                ,DLB_pay_bank_list:$('.list').val()
                ,DLB_settler_hold_settlecard:$('demo').attr('src')
            },function(res){
                if(res.status==1){
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 3000
                    });
                }else{
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 3000
                    });
                }
            },"json");
        });
    });

</script>

</body>
</html>
