<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>商户管理系统 - 首页</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
<link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
<link rel="icon" href="<?php echo e(asset('/layuiadmin/layui/images/icon.png')); ?>">
<link rel="icon" href="<?php echo e(asset('/layuiadmin/layui/images/icon.png')); ?>" type="image/x-icon"/>
<link rel="shortcut icon" href="<?php echo e(asset('/layuiadmin/layui/images/icon.png')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/modules/layuiicon/iconfont.css')); ?>" media="all">
<style>
    .layadmin-side-shrink .layui-layout-admin .layui-logo{background-image: none !important;}
    @font-face {
    font-family: 'iconfont';  /* project id 1584981 */
    src: url('//at.alicdn.com/t/font_1584981_8thx3buwa8t.eot');
    src: url('//at.alicdn.com/t/font_1584981_8thx3buwa8t.eot?#iefix') format('embedded-opentype'),
    url('//at.alicdn.com/t/font_1584981_8thx3buwa8t.woff2') format('woff2'),
    url('//at.alicdn.com/t/font_1584981_8thx3buwa8t.woff') format('woff'),
    url('//at.alicdn.com/t/font_1584981_8thx3buwa8t.ttf') format('truetype'),
    url('//at.alicdn.com/t/font_1584981_8thx3buwa8t.svg#iconfont') format('svg');
    }
</style>
</head>
<body class="layui-layout-body">

  <div id="LAY_app">
    <div class="layui-layout layui-layout-admin">
      <div class="layui-header">
        <!-- 头部区域 -->
        <ul class="layui-nav layui-layout-left">
          <!-- <li class="layui-nav-item layadmin-flexible" lay-unselect>
            <a href="javascript:;" layadmin-event="flexible" title="侧边伸缩">
              <i class="layui-icon layui-icon-shrink-right" id="LAY_app_flexible"></i>
            </a>
          </li> -->

          <li class="layui-nav-item" lay-unselect>
            <a href="javascript:;" layadmin-event="refresh" title="刷新">
              <i class="layui-icon layui-icon-refresh-3"></i>
            </a>
          </li>
          <!-- <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <input type="text" placeholder="搜索..." autocomplete="off" class="layui-input layui-input-search" layadmin-event="serach" lay-action="template/search.html?keywords=">
          </li> -->
        </ul>
        <ul class="layui-nav layui-layout-right" lay-filter="layadmin-layout-right">

          <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <a href="javascript:;" layadmin-event="fullscreen" class="icondynamic">
              <i class="layui-icon layui-icon-screen-full"></i>
            </a>
          </li>
          <!-- <li class="layui-nav-item" lay-unselect>
            <a href="javascript:;">
              <cite>贤心</cite>
            </a>
            <dl class="layui-nav-child">
              <dd><a lay-href="set/user/info.html">基本资料</a></dd>
              <dd><a lay-href="set/user/password.html">修改密码</a></dd>
            </dl>
          </li> -->
          <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <a href="javascript:;" layadmin-event="" class="">帮助中心</a>
          </li>
          <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <a href="javascript:;" layadmin-event="logout" class="logout"><embed src="<?php echo e(asset('/layuiadmin/img/tuichudenglu.svg')); ?>" width="14px" height="14px" type="image/svg+xml"/>退出</a>
          </li>
          <li class="layui-nav-item layui-show-xs-inline-block layui-hide-sm" lay-unselect>
            <a href="javascript:;" layadmin-event="more"><i class="layui-icon layui-icon-more-vertical"></i></a>
          </li>
        </ul>
      </div>

      <!-- 侧边菜单 -->
      <div class="layui-side layui-side-menu" style="background-color: #3475c3 !important;width:200px">
        <div class="layui-side-scroll">
          <div class="layui-logo"  style="background-color: #3475c3 !important;width:200px">
            <span>商户管理系统</span>
          </div>

          <ul class="layui-nav layui-nav-tree box" lay-shrink="all" id="LAY-system-side-menu" lay-filter="layadmin-system-side-menu" style="width:200px">

            <li data-name="component" class="layui-nav-item layui-nav-itemed">
              <a lay-href="<?php echo e(url('/mb/home')); ?>" lay-tips="对账统计" lay-direction="2">
                <i class="iconfont layui-icon" style="font-size: 14px;">&#xe665;</i>
                <cite>对账统计</cite>
              </a>
            </li>

            <li data-name="home" class="layui-nav-item">
              <a href="javascript:;" lay-tips="门店管理" lay-direction="2">
                <i class="iconfont layui-icon" style="font-size: 14px;">&#xe668;</i>
                <cite>门店管理</cite>
              </a>
              <dl class="layui-nav-child">
                <dd data-name="console">
                  <a lay-href="<?php echo e(url('/mb/store')); ?>">门店列表</a>
                </dd>
                <dd data-name="console">
                  <a lay-href="<?php echo e(url('/mb/tableNumber')); ?>">桌号管理</a>
                </dd>
              </dl>
            </li>
            <li data-name="component" class="layui-nav-item">
              <a href="javascript:;" lay-tips="商品管理" lay-direction="2">
                <i class="iconfont layui-icon" style="font-size: 14px;">&#xe66b;</i>
                <cite>商品管理</cite>
              </a>
              <dl class="layui-nav-child">
                <dd data-name="grid">
                  <a lay-href="<?php echo e(url('/mb/commodityClass')); ?>">商品分类</a>
                </dd>
                <dd data-name="grid">
                  <a lay-href="<?php echo e(url('/mb/commodityList')); ?>">商品列表</a>
                </dd>
                <dd data-name="grid">
                  <a lay-href="<?php echo e(url('/mb/commodityOeder')); ?>">商品订单</a>
                </dd>
                <dd data-name="grid">
                  <a lay-href="<?php echo e(url('/mb/refundOrder')); ?>">退款订单</a>
                </dd>
              </dl>
            </li>
            <li data-name="component" class="layui-nav-item">
              <a href="javascript:;" lay-tips="流水查询" lay-direction="2">
                <i class="iconfont layui-icon" style="font-size: 14px;">&#xe666;</i>
                <cite>流水查询</cite>
              </a>
              <dl class="layui-nav-child">
                <dd data-name="grid"><a lay-href="<?php echo e(url('/mb/waterlist')); ?>">流水查询</a></dd>
                <dd data-name="grid"><a lay-href="<?php echo e(url('/mb/flowerlist')); ?>">花呗流水查询</a></dd>
                <dd data-name="grid"><a lay-href="<?php echo e(url('/mb/appletmoneyqrcodelist')); ?>">小程序收款码</a></dd>
                
              </dl>
            </li>
            <li data-name="component" class="layui-nav-item">
              <a href="javascript:;" lay-tips="微信发券" lay-direction="2">
                <i class="layui-icon iconfont" style="font-size: 14px;">&#xe66e;</i>
                <cite>微信发券</cite>
              </a>
              <dl class="layui-nav-child">
                <dd data-name="grid">
                  <a lay-href="<?php echo e(url('/mb/cashCoupon')); ?>">代金券</a>
                </dd>



                <dd data-name="grid"> 
                  <a lay-href="<?php echo e(url('/mb/cashCoupondetailed')); ?>">代金券核销明细</a>
                </dd>


                <dd data-name="grid">
                  <a lay-href="<?php echo e(url('/mb/cashCouponTwo')); ?>">商家券</a>
                </dd>
                <dd data-name="grid">
                  <a lay-href="<?php echo e(url('/mb/cashCoupondetailedTwo')); ?>">商家券核销明细</a>
                </dd>


              </dl>
            </li>
            <li data-name="component" class="layui-nav-item">
              <a href="javascript:;" lay-tips="支付宝发券" lay-direction="2">
                <i class="layui-icon iconfont" style="font-size: 12px;">&#xe67f;</i>
                <cite>支付宝发券</cite>
              </a>
              <dl class="layui-nav-child">
                <!-- <dd data-name="grid">
                  <a lay-href="<?php echo e(url('/mb/commodityClass')); ?>">商品分类</a>
                </dd>
                <dd data-name="grid">
                  <a lay-href="<?php echo e(url('/mb/commodityList')); ?>">商品列表</a>
                </dd> -->
                <dd data-name="grid">
                  <a lay-href="<?php echo e(url('/mb/alipayCoupon')); ?>">代金券</a>
                </dd>



                <dd data-name="grid">
                  <a lay-href="<?php echo e(url('/mb/alipayCoupondetailed')); ?>">代金券核销明细</a>
                </dd>
              </dl>
            </li>
            <li data-name="component" class="layui-nav-item">
              <a href="javascript:;" lay-tips="会员管理" lay-direction="2">
                <i class="layui-icon iconfont" style="font-size: 14px;">&#xe669;</i>
                <cite>会员管理</cite>
              </a>
              <dl class="layui-nav-child">
                <dd data-name="grid"><a lay-href="<?php echo e(url('/mb/memberlist')); ?>">会员列表</a></dd>
                <dd data-name="grid"><a lay-href="<?php echo e(url('/mb/setmember')); ?>">会员等级设置</a></dd>
                <dd data-name="grid"><a lay-href="<?php echo e(url('/mb/templatemember')); ?>">会员模板设置</a></dd>
                <dd data-name="grid"><a lay-href="<?php echo e(url('/mb/rechargerecord')); ?>">充值记录</a></dd>
                
                
              </dl>
            </li>
            <li data-name="component" class="layui-nav-item">
              <a href="javascript:;" lay-tips="营销管理" lay-direction="2">
                <i class="layui-icon iconfont" style="font-size: 14px;">&#xe66a;</i>
                <cite>营销管理</cite>
              </a>
              <dl class="layui-nav-child">
                <dd data-name="grid"><a lay-href="<?php echo e(url('/mb/marketlist')); ?>">营销活动列表</a></dd>
                <dd data-name="grid"><a lay-href="<?php echo e(url('/mb/addmarket')); ?>">新增充值活动</a></dd>
                <dd data-name="grid"><a lay-href="<?php echo e(url('/mb/setpoints')); ?>">积分设置</a></dd>
              </dl>
            </li>
            <li data-name="template" class="layui-nav-item item_shangjin" data='赏金管理'>
              <a href="javascript:;" lay-tips="赏金管理" lay-direction="2">
                <i class="layui-icon iconfont" style="font-size: 14px;">&#xe66d;</i>
                <cite>赏金管理</cite>
              </a>
              <dl class="layui-nav-child">
                <dd><a lay-href="<?php echo e(url('/mb/reward')); ?>" data='赏金列表'>赏金列表</a></dd>
                <dd><a lay-href="<?php echo e(url('/mb/putforward')); ?>" data='提现记录'>提现记录</a></dd>
              </dl>
            </li>
            <li data-name="component" class="layui-nav-item">
              <a href="javascript:;" lay-tips="系统设置" lay-direction="2">
                <i class="layui-icon iconfont" style="font-size: 14px;">&#xe66c;</i>
                <cite>系统设置</cite>
              </a>
              <dl class="layui-nav-child">
                <dd data-name="grid"><a lay-href="<?php echo e(url('/mb/setpaypassward')); ?>">设置支付密码</a></dd>
                <dd data-name="grid"><a lay-href="<?php echo e(url('/mb/editpaypassward')); ?>">忘记支付密码</a></dd>
                
              </dl>
            </li>
            <li data-name="component" class="layui-nav-item">
              <a href="javascript:;" lay-tips="创建小程序" lay-direction="2">
                <i class="layui-icon iconfont" style="font-size: 14px;">&#xe667;</i>
                <cite>创建小程序</cite>
              </a>
              <dl class="layui-nav-child">
                <dd data-name="grid"><a lay-href="<?php echo e(url('/mb/createApplet')); ?>">微信小程序</a></dd>
                <dd data-name="grid"><a lay-href="<?php echo e(url('/mb/alipycreatApplet')); ?>">支付宝小程序</a>
                
              </dl>
            </li>
          </ul>
        </div>
      </div>

      <!-- 页面标签 -->
      <div class="layadmin-pagetabs" id="LAY_app_tabs"style="position:fixed;top:50px">
        <div class="layui-icon layadmin-tabs-control layui-icon-prev" layadmin-event="leftPage"></div>
        <div class="layui-icon layadmin-tabs-control layui-icon-next" layadmin-event="rightPage"></div>
        <div class="layui-icon layadmin-tabs-control layui-icon-down">
          <ul class="layui-nav layadmin-tabs-select" lay-filter="layadmin-pagetabs-nav">
            <li class="layui-nav-item" lay-unselect>
              <a href="javascript:;"></a>
              <dl class="layui-nav-child layui-anim-fadein">
                <dd layadmin-event="closeThisTabs"><a href="javascript:;">关闭当前标签页</a></dd>
                <dd layadmin-event="closeOtherTabs"><a href="javascript:;">关闭其它标签页</a></dd>
                <dd layadmin-event="closeAllTabs"><a href="javascript:;">关闭全部标签页</a></dd>
              </dl>
            </li>
          </ul>
        </div>
        <div class="layui-tab" lay-unauto lay-allowClose="true" lay-filter="layadmin-layout-tabs">
          <ul class="layui-tab-title" id="LAY_app_tabsheader">
            <li lay-id="<?php echo e(url('/mb/home')); ?>" lay-attr="<?php echo e(url('/mb/home')); ?>" class="layui-this">对账统计</li>
          </ul>
        </div>
      </div>

      <!-- 主体内容 -->
      <div class="layui-body" id="LAY_app_body">
        <div class="layadmin-tabsbody-item layui-show">
          <iframe src="<?php echo e(url('/mb/home')); ?>" frameborder="0" class="layadmin-iframe"></iframe>
        </div>
      </div>

      <!-- 辅助元素，一般用于移动设备下遮罩 -->
      <div class="layadmin-body-shade" layadmin-event="shade"></div>
    </div>
  </div>

  <script type="text/javascript" src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
  <script type="text/javascript" src="<?php echo e(asset('/phone/js/jquery-2.1.4.js')); ?>"></script>
  <script>
    //   $(function(){
    //       $(".icondynamic").off().on("click",function(){
    //           $(this).find("i").html("&#xe677;")
    //       })
    //   })
  </script>
  <script>

  layui.config({
    base: '../../layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index','element'], function(){
    var $ = layui.$;

    var token = localStorage.getItem("Publictoken");
      // console.log(token);
      var level = localStorage.getItem("mb_level");

    $('.logout').click(function(){
      localStorage.removeItem("token");
      localStorage.clear();
      window.location.reload();
    });

    // 未登录,跳转登录页面
    $(document).ready(function(){
        var token = localStorage.getItem("Publictoken");
        var admin = localStorage.getItem("admin");
        // var level = localStorage.getItem("level");
        if(token==null){
          window.location.href="<?php echo e(url('/mb/login')); ?>";
        }
    });

      var permissions = localStorage.getItem("mb_permissions");
      var str = JSON.parse(permissions);
      var arr = [];
      for(var i=0;i<str.length;i++){
          var aa = str[i].name;
          arr.push(aa);
      }

//      if(level != 0){
//          // 权限管理+++++++++++++
//          $('ul.box li').each(function(index,item){
////             console.log($(this).attr('data'));
////             console.log(arr);
////             if($.inArray( $(this).attr('data'), arr ) == -1){
////                 $(this).hide();
////             }
//          });
//
//          $('ul.box li dl dd').each(function(index,item){
////             console.log($(this).find('a').attr('data'));
////             console.log($.inArray($(this).find('a').attr('data'),arr));
//              if($.inArray($(this).find('a').attr('data'),arr)==-1){
//                  $(this).find('a').hide();
//              }
//          })
//      }

  });

  </script>
</body>
</html>
