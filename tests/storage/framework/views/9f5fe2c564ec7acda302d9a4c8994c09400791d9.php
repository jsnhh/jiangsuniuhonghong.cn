<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>支付宝小程序</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/modules/layuiicon/iconfont.css')); ?>" media="all">
    <style type="text/css">
        .xgrate {
            color: #fff;
            font-size: 15px;
            padding: 7px;
            height: 30px;
            line-height: 30px;
            background-color: #3475c3;
        }

        .up {
            position: relative;
        }

        .up #uploadFile {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            opacity: .01;
            width: 100px;
            height: 30px;
        }

        .up input[type=file] {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            opacity: .01;
            width: 100px;
            height: 30px;
        }

        .demo5 {
            width: 100px;
        }

        .box1 {
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            padding: 15px;
            box-shadow: 0 1px 3px rgba(0, 0, 0, .02), 0 16px 32px -4px rgba(0, 0, 0, .17);
        }

        .box2 {
            margin-left: 20px;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            padding: 15px;
            box-shadow: 0 1px 3px rgba(0, 0, 0, .02), 0 16px 32px -4px rgba(0, 0, 0, .17);
        }

        .person {
            width: 100%;
            height: 100px;
            text-align: center;
            line-height: 20px;
            color: #000;
            font-size: 14px;
        }

        .athorize {
            margin-top: 20px;
            width: 50%;
            height: 40px;
            line-height: 40px;
            text-align: center;
            font-size: 14px;
            color: #fff;
            background-color: #00a3fe;
        }

        .athorize:hover {
            cursor: pointer;
        }

        .button {
            margin: 30px 0 30px 111px;
            width: 83%;
        }

        #steps {
            padding: 30px 0 0 260px;
        }

        .layui-form-label {
            display: block;
            margin-left: 111px;
            padding-bottom: 8px;
        }

        p {
            width: 50%;
            margin-left: 111px;
            height: 22px;
            font-size: 14px;
            font-weight: 400;
            color: #999999;
            line-height: 22px;
        }

        input {
            width: 50%;
        }
        .audit{

            position:relative;
            width: 100%;
            height: 100%;

        }
        .audit img{
            width: 400px;
            display: block;
            margin: 30px auto 0;
            border: 2px dashed #e2e2e2;
            box-sizing: border-box;
        }

        .audit h3{

            font-size: 32px;
            text-align: center;
            margin: 24px 0 8px;
            color: #333333;
        }
        .audit  p{
            font-size: 20px;
            text-align: center;
            width: 100%;
            margin-left: 0;
            color: #666666;
        }
        .auditnone{
            display: none;
        }

        .fail{
            padding: 0 50px;
            box-sizing: border-box;
            overflow: hidden;
        }
        .fail .button{
            text-align: center;
        }
        .fail img{
            width: 400px;
            display: block;
            margin: 30px auto 0;
            border: 2px dashed #e2e2e2;
            box-sizing: border-box;
        }
        .fail h3{
            font-size: 24px;
            text-align: center;
            margin: 24px 0 8px;
            color: #333333;
            text-align: center;
            font-family: PingFangSC-Medium, PingFang SC;
            font-weight: 500;
            line-height: 32px;
        }
        .fail .text{

            background: rgba(0, 0, 0, 0.02);
            padding: 28px 40px;
        }
        .fail .text .title{

            font-size: 16px;
            color: #333333;

        }
        .fail .text p{
            width: 100%;
            font-size: 14px;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 400;
            color: #333333;
            line-height: 22px;
            margin-left: 0px;
            height: auto;
        }
        .fail .text .txt{
            display: flex;
            align-items: center;
            margin-top: 16px;
        }
        .logs .title{

            height: 22px;
            font-size: 16px;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 400;
            color: #333333;
            line-height: 22px;
            padding: 24px 0 6px 0;
        }
        .failnone{
            display: none;
        }
        .success {
            overflow: hidden;
            padding: 0 50px 42px;
            box-sizing: border-box;
        }
        .success .top img{
            display: block;
            margin: 30px auto 24px;
            width: 296px;
            border: 2px dashed #e2e2e2;
        }
        .success .top h3{
            height: 32px;
            font-size: 24px;
            font-family: PingFangSC-Medium, PingFang SC;
            font-weight: 500;
            color: #333333;
            line-height: 32px;
            text-align: center;
            margin-bottom: 30px;
        }
        .success .top form{
            text-align: center;
        }
        .success .top .but{
            display: inline-block;
        }
        .centre{
            background: rgba(0, 0, 0, 0.02);
            padding: 24px 40px 26px;
            box-sizing: border-box;
            margin-top: 35px;
        }
        .centre .flex{
            display: flex;
            justify-content: space-between;
            max-width: 1300px;
            min-width: 1100px;
        }
        .centre .title{
            height: 24px;
            font-size: 16px;
            font-family: PingFangSC-Medium, PingFang SC;
            font-weight: 500;
            color: #333333;
            line-height: 24px;
            margin-bottom: 16px;
        }
        .centre .flex p{
            width: 100%;
            margin-left: 0px;
            font-size: 14px;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 400;
            color: #666666;
            line-height: 22px;
            margin-bottom: 8px;
        }
        .centre .flex span{
            font-size: 14px;
            font-family: PingFangSC-Regular, PingFang SC;
            font-weight: 400;
            color: #333333;
            line-height: 22px;
        }
        .successnone{
            display: none;
        }
        .stepsboxnone{
            display: none!important;
        }
        .uploadDataImg{
        	display: none;
        }
        .yyzzuploadDataImg{
        	display: none;
        }
        .oneboxBlock{
            display: none;
        }

        .groundingnone{
            display: none;
        }
        .grounding img{
            display: block;
            margin: 0 auto;
            padding:  100px 0 200px;
        }
        .layui-form-checked span{
            background-color: #1E9FFF!important;
        }

        .selectcolorplaceholder input::-webkit-input-placeholder {
            color: #000000;
        }
        .selectcolorplaceholder input:-moz-placeholder{
            color: #000000;
        }
        .selectcolorplaceholder input::-moz-placeholder {
            color: #000000;
        }
        .selectcolorplaceholder input:-ms-input-placeholder {
            color: #000000;
        }
        .zfbyyzzimg{
            width: 256px;
            margin-left: 120px;
            margin-top: 10px;
        }
        .zfbyyzzimg img{
            display: block;
            width: 100%;
        }
    </style>
</head>
<body>
    <div class="layui-fluid" style="margin-top:50px;">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <!-- 创建支付宝小程序 -->
                    <div class="layui-card-header">支付宝小程序</div>
                    <div class="oneboxnone oneboxBlock">
                        <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief" style="height:100%;padding-bottom:215px;display:block;">
                            <!-- 步骤条 -->
                            <div id="steps"></div>
                            <!-- 创建小程序步骤 -->
                            <div id="stepsbox" class="layui-tab-content">
                                <!-- 创建 -->
                                <!-- 授权 -->
                                <div class="layui-tab-item layui-show stepsboxnone" id="stepThree" style=" margin-top:50px;">
                                    <div style="text-align: center;">
                                        <div class="one" style="display: inline-block;">

                                            <div style="display: flex;">
                                                <i class="layui-icon iconfont" style="font-size: 14px;color:#FFB800;margin-right: 3px;margin-top: 3px;">&#xe670;</i>
                                                <div style="text-align: left;">创建支付宝小程序之前，需要法人用支付宝开启授权。 <br/>第一步：点击“开启授权”按钮，跳转到码页面，法人用支付宝扫描，并按照下图所示完成操作。 <br/>第二步：手机操作成功后，点击“下一步”，进入创建小程序页面。</div>
                                            </div>

                                        </div>
                                    </div>
                                    <form class="layui-form" action="">
                                        <div class="layui-form-item">
                                            <button class="layui-btn" lay-submit lay-filter="zjsq" style="background: #1E9FFF;width: 480px;margin: 30px auto 0;display: block;">开启授权</button>
                                        </div>
                                    </form>
                                    <button class="layui-btn" lay-submit lay-filter="sqxyb" style="border:1px solid #1E9FFF;background:none;width: 480px;margin: 15px auto 0;display: block;color: #1E9FFF;">下一步</button>
                                    <div class="zfbsqImg">
                                        <img  style="display: block;margin: 30px auto 0;" src="/mb/zfbimg/zfbsq.jpg"/>
                                    </div>
                                </div>
                                <!-- 追加授权失败 -->
                                <div class="stepsboxnone" style="text-align: center;width: 100%;margin-top: 40px;">

                                    <i class="layui-icon iconfont" style="font-size: 72px;color:red;">&#xe670;</i>
                                    <h3 style="font-size: 32px;margin-top: 8px;">追加授权失败</h3>
                                    <p style="font-size: 22px;width: 100%;margin: 8px 0 0 0;">请使用平台绑定的管理员个人微信号扫码</p>

                                    <form class="layui-form" action="">
                                        <div class="layui-form-item">
                                            <button class="layui-btn" lay-submit lay-filter="cxzjsq" style="background: #1E9FFF;margin-top: 15px;">重新追加授权</button>
                                        </div>
                                    </form>

                                </div>
                                <!-- 未查询到您的授权 -->
                                <div class="stepsboxnone" style="text-align: center;width: 100%;margin-top: 40px;">

                                    <i class="layui-icon iconfont" style="font-size: 72px;color:red;">&#xe670;</i>
                                    <h3 style="font-size: 32px;margin-top: 8px;">未查询到您的授权</h3>
                                    <p style="font-size: 22px;width: 100%;margin: 8px 0 0 0;">请使用平台绑定的管理员个人微信号扫码</p>

                                    <form class="layui-form" action="">
                                        <div class="layui-form-item">
                                            <button class="layui-btn" lay-submit lay-filter="cxzjsq" style="background: #1E9FFF;margin-top: 15px;">重新追加授权</button>
                                        </div>
                                    </form>

                                </div>
                                <!-- 创建小程序 -->
                                <div class="layui-tab-item layui-show stepsboxnone" id="stepTwo" style="margin-left: 24%; margin-top:25px;">

                                    <form class="layui-form" action="">

                                        <div class="layui-form-item">
                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                <span
                                                    style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                小程序名称
                                            </label>
                                            <div class="layui-input-block">
                                                <input type="text" name="app_name" lay-verify="title"
                                                    autocomplete="off" placeholder="小程序名称" class="layui-input"
                                                    style="width: 50%;">
                                            </div>
                                            <p style="width:82%;margin-left:111px;display:block;height: 32px;font-size: 14px;font-weight: 400;color: #999999;line-height: 32px;margin-bottom: 15px;">
                                            名称由中文、英文、数字及下划线组成，长度在3-20个字符之间，一个中文等于2个字符。
                                            </p>
                                        </div>

                                        <div class="layui-form-item">
                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                <span
                                                    style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                小程序英文名称
                                            </label>
                                            <div class="layui-input-block">
                                                <input type="text" name="app_english_name" lay-verify="title"
                                                    autocomplete="off" placeholder="请输入小程序英文名称，长度在3~20个字符" class="layui-input"
                                                    style="width: 50%;">
                                            </div>
                                        </div>


                                        <div class="layui-form-item">
                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                <span style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                小程序Logo
                                            </label>

                                            <div style="display: flex;align-items:flex-end;">
                                                <div class="layui-upload up" style="width:100px;height:100px;border:1px dashed #e2e2e2;margin-left: 111px;position: relative;">

                                                    <img style="position: absolute;left: 0;top: 0;width: 100px;height: 100px;border: 0px ;" id="logobgm" class="uploadDataImg" src=""/>
                                                    <input type="file" name="file" id="uploadFile" style="width:100px;height:100px;" />
                                                    <input type="hidden" name="license" value="" />

                                                </div>
                                                <p style="margin-left: 10px;">
                                                    支持png、jpeg、jpg格式，建议上传像素180*180，最大256KB
                                                </p>
                                            </div>
                                        </div>

                                        <div class="layui-form-item">
                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                <span style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                营业执照图片
                                            </label>

                                            <div style="display: flex;align-items:flex-end;">
                                                <div class="layui-upload up" style="width:100px;height:100px;border:1px dashed #e2e2e2;margin-left: 111px;position: relative;">

                                                    <img style="position: absolute;left: 0;top: 0;width: 100px;height: 100px;border: 0px ;" id="yyzzbgm" class="yyzzuploadDataImg" src=""/>
                                                    <input type="file" name="file" id="yyzzuploadFile" style="width:100px;height:100px;" />
                                                    <input type="hidden" name="licensePic" value="" />

                                                </div>
                                                <p style="margin-left: 10px;">
                                                    图片大小不能超过2M
                                                </p>
                                            </div>
                                        </div>
                                        <div class="layui-form-item">
                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                <span
                                                    style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                营业执照编号（注意不是社会信用代码）
                                            </label>
                                            <div class="layui-input-block">
                                                <input type="text" name="licenseNo" lay-verify="title"
                                                    autocomplete="off" placeholder="营业执照编号" class="layui-input"
                                                    style="width: 50%;">
                                            </div>
                                            <div class="zfbyyzzimg">
                                                <img src="/mb/zfbimg/zfbyyzz.png" alt="">
                                            </div>
                                        </div>

                                        <div class="layui-form-item">
                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                <span
                                                    style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                客服电话
                                            </label>
                                            <div class="layui-input-block">
                                                <input type="text" name="service_phone" lay-verify="title"
                                                    autocomplete="off" placeholder="请输入客服电话" class="layui-input"
                                                    style="width: 50%;">
                                            </div>
                                        </div>
                                        <div class="layui-form-item">
                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                邮箱
                                            </label>
                                            <div class="layui-input-block">
                                                <input type="text" name="service_email" lay-verify="title"
                                                    autocomplete="off" placeholder="请输入邮箱" class="layui-input"
                                                    style="width: 50%;">
                                            </div>
                                        </div>

                                        <div class="layui-form-item selectcolorplaceholder">
                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                <span
                                                    style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                小程序类目(最多支持3个类目)
                                            </label>
                                            <div class="layui-input-block">
                                                <div style="width: 50%;margin-bottom: 15px;" class="selectLeiMuYiclass">
                                                    <select name="mini_category_list0_1" id="selectLeiMuYi" lay-filter="selectLeiMuYi">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="layui-input-block">
                                                <div style="width: 50%;margin-bottom: 15px;">
                                                    <select name="mini_category_list0_2"  id="selectLeiMuEr" lay-filter="selectLeiMuEr">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="layui-input-block">
                                                <div style="width: 50%;">
                                                    <select name="mini_category_list0_3"  id="selectLeiMuSan" lay-filter="selectLeiMuSan">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="layui-form-item">
                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                <span
                                                    style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                小程序简介
                                            </label>
                                            <div class="layui-input-block">
                                                <input type="text" name="app_slogan" lay-verify="title"
                                                    autocomplete="off" placeholder="描述小程序提供的服务，10～32个字符，应用上架后一个自然月可修改5次。" class="layui-input"
                                                    style="width: 50%;">
                                            </div>
                                        </div>


                                        <div class="layui-form-item">
                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                <span
                                                    style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                    小程序描述信息
                                            </label>
                                            <div class="layui-input-block">
                                                <textarea name="app_desc" placeholder="小程序描述信息，简要描述小程序主要功能（20-200个字），例如：XX小程序提供了XX功能，主要解决了XX问题。" class="layui-textarea" style="width: 50%;"></textarea>
                                            </div>
                                        </div>

                                        <div class="layui-form-item">
                                            <div class="layui-input-block">
                                            <button class="layui-btn" lay-submit lay-filter="cjxcx" style="background: #1E9FFF;">创建</button>
                                            </div>
                                        </div>

                                    </form>

                                </div>
                                <!-- 提交审核 -->
                                <div class="layui-tab-item layui-show stepsboxnone" id="stepFour" style="margin-left: 24%; margin-top:50px;">
                                    <form class="layui-form" action="">
                                        <div class="grid-demo">
                                            <div class="layui-form-item">
                                                <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                    <span
                                                        style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                    使用的模板ID
                                                </label>
                                                <div class="layui-input-block">
                                                    <input type="text" name="legal_persona_wechat" lay-verify="title"
                                                        autocomplete="off" placeholder="使用的模板ID（自动抓取）" class="layui-input"
                                                        style="width: 50%;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="grid-demo">
                                            <div class="layui-form-item">
                                                <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                    <span style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                        支付宝小程序版本
                                                </label>
                                                <div class="layui-input-block">
                                                    <input type="text" name="legal_persona_wechatname" lay-verify="title"
                                                        autocomplete="off" placeholder="最新版本名称（自动抓取）" class="layui-input"
                                                        style="width: 50%;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="button">
                                            <button type="submit" class="layui-btn" lay-submit="" lay-filter="tjsh" id="addwechat" style="background: #1E9FFF;">第一步：提交</button>
                                        </div>
                                    </form>
                                    <form class="layui-form" action="">
                                        <div class="grid-demo">
                                            <div class="layui-form-item">
                                                <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                    <span style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                    营业执照名称
                                                </label>
                                                <div class="layui-input-block">
                                                    <input type="text" name="license_name" lay-verify="title"
                                                        autocomplete="off" placeholder="请输入营业执照名称" class="layui-input"
                                                        style="width: 50%;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="grid-demo">
                                            <div class="layui-form-item">
                                                <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                    <span style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                    营业执照有效截至日期
                                                </label>
                                                <div class="layui-input-block">

                                                    <input type="text" name="license_valid_date" class="layui-input" id="test1" placeholder="请输入营业执照截至日期(yyyy-MM-dd)" style="width: 50%;">


                                                    <input type="checkbox" lay-filter="number" title="长期" class="layui-btn layui-btn-sm">


                                                </div>
                                            </div>
                                        </div>
                                        <div class="grid-demo">
                                            <div class="layui-form-item">
                                                <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                    <span style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                    小程序版本描述
                                                </label>
                                                <div class="layui-input-block">
                                                <textarea name="desc" placeholder="请输入内容（最少15个文字）" class="layui-textarea textarea" style="width: 50%;"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="button">
                                            <button type="submit" class="layui-btn" lay-submit="" lay-filter="tjsh2" id="addwechat" style="background: #1E9FFF;">第二步：提交</button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 支付宝小程序申请审核中 -->
                    <div class="audit auditnone">
                        <div>
                            <img src="/mb/zfbimg/audit.png" alt="">
                        </div>
                        <h3>支付宝小程序申请审核中 </h3>
                        <p>注意查看审核结果，一般在3个工作日内，审核成功自动发布</p>
                        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                    </div>
                    <!-- 支付宝小程序创建失败 -->
                    <div class="fail failnone">
                        <div class="img">
                            <img src="/mb/zfbimg/fail.png" alt="">
                        </div>
                        <h3>支付宝小程序创建失败</h3>
                        <div class="button">
                            <button type="button" class="layui-btn cxcj" lay-filter="cxcj" style="background: #1E9FFF;">重新创建</button>
                        </div>
                        <div class="text">
                            <div class="title">
                                创建失败原因
                            </div>
                            <div class="txt">
                                <p></p>
                            </div>
                        </div>
                        <br/><br/><br/><br/>
                    </div>
                    <!-- 支付宝小程序创建成功 -->
                    <div class="success successnone">
                        <div class="top">
                            <img src="/mb/zfbimg/success.png" alt="">
                            <h3>支付宝小程序创建成功</h3>
                            <div class="layui-form-item">
                                <button class="layui-btn successGxXcx" style="background: #1E9FFF;margin-right: 16px;display: block;margin: 0 auto;">更新支付宝小程序</button>
                                <!-- <span><button type="submit" class="layui-btn" style="background: #FFFFFF;border-radius: 2px;border: 1px solid rgba(0, 0, 0, 0.15);color:#666666;">修改支付宝小程序</button></span> -->
                            </div>
                        </div>
                        <div class="centre">
                            <div class="title">
                                支付宝小程序
                            </div>
                            <div class="flex">
                                <div class="left">
                                    <p>小程序名称：<span>小程序名称：</span></p>
                                    <p>小程序英文名：<span>小程序英文名：</span></p>
                                    <!-- <p>应用状态：<span><b style="color: #52C41A;font-size: 16px;margin-right: 4px;">·</b>已上线</span></p> -->
                                    <!-- <p>当前支付宝小程序版本：<span>我是小程序应用版本</span></p> -->
                                    <!-- <p>最新应用版本：<span style="color: #1E9FFF;">最新程序应用版本</span></p> -->
                                    <p>客服电话：<span>客服电话：</span></p>
                                    <p>小程序类目：<span>类目1、类目2、类目3</span></p>
                                    <p>小程序LOGO：</p>
                                    <div class="centrelogoimg" style="width: 138px;">
                                        <img src="" style="display: block;width: 100%;">
                                    </div>
                                </div>
                                <div class="auto">
                                    <!-- <p>商户账户：<span>我是商户账户</span></p>
                                    <p>法人姓名：<span>我是法人姓名</span></p>
                                    <p>法人手机号码：<span>我是法人手机号</span></p>
                                    <p>法人邮箱：<span>我是法人邮箱</span></p> -->
                                    <p>小程序简介：<span>小程序简介：</span></p>
                                    <p>小程序描述信息：<span>小程序描述信息：</span></p>
                                </div>
                                <div class="right">
                                    <!-- <p>小程序二维码：</p> -->
                                    <div style="width: 300px;height: 300px;background-color: rgba(0, 0, 0, 0);">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 审核成功 立即上架 -->
                    <div class="grounding groundingnone groundingimg">
                        <img src="/mb/zfbimg/grounding.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" class="editiondescribe">
</body>

<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script src="<?php echo e(asset('/js/qrcode.min.js')); ?>"></script>

<script type="text/javascript">
	/*
	 * 用户 门店id
	 */
    var store_id = localStorage.getItem("store_id");
   	/*
   	 * 获取用户token
   	 */
    var token = localStorage.getItem("Publictoken");
    /*
     * 获取保存在本地的类目
     */
    var sessionLeiMu = window.sessionStorage.getItem("alipycreatAppletLeiMu");

    /*
        判断用户是否登录
    */
    if (token == null) {
        window.location.href = "<?php echo e(url('/user/login')); ?>";
    }

	layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        steps: './extends/steps/steps'
    }).use(['index', 'form', 'upload', 'table', 'laydate', 'steps'], function() {
        var $ = layui.$,
            admin = layui.admin,
            form = layui.form,
            table = layui.table,
            laydate = layui.laydate,
            upload = layui.upload,
            steps = layui.steps
        /*
         * 步骤条
         */
        layui.use('steps', function() {
            var steps = layui.steps;
            steps.render({
                ele: '#steps',
                data: [
                	{
                        'title': "第一步",
                        "desc": "支付宝授权"
                    },
                    {
                        'title': "第二步",
                        "desc": "创建小程序"
                    },
                    {
                        'title': "第三步",
                        "desc": "提交审核"
                    }
                ],
                //默认为第几步
                // current: 0 ,
            });
        })
        /*
            监听复选框
        */
        form.on('checkbox(number)', function (data) {
            let res = data.elem.checked;
            if(res){
                $("#test1").val("9999-99-99");
            }else{
                $("#test1").val("");
            }
        })
        /*
            时间
        */
        laydate.render({
            elem: '#test1'
        });
        /*
            步骤条函数,不等于1的时候才会下一步,让步骤条跳转到第几步 ， 数据库传下来的默认为1
        */
        oneFun();
        function oneFun(){
            $.post("/api/customer/aliPay/getAliAppletsStep",{
                storeId:store_id,
                token:token
            },function(data){
                let stepspage = data.data.created_step;
                console.log(stepspage);
                if(stepspage !== 1){
                    if(stepspage == 2){
                        $(".oneboxnone").removeClass("oneboxBlock");
                        dierbuFun();
                    }else if(stepspage == 3){
                        $(".oneboxnone").removeClass("oneboxBlock");
                        $("#stepsbox > div").addClass("stepsboxnone");
                        $("#stepFour").removeClass("stepsboxnone");
                        getTemplateFun();
                    }else if(stepspage >= 4){
                        judgeFun();
                    }
                    stepspage --;
                    for(let i=0;i<stepspage;i++){
                        //执行下一步
                        steps.next();
                    }
                }else{
                    $(".oneboxnone").removeClass("oneboxBlock");
                    $("#stepThree").removeClass("stepsboxnone");
                }
            },"json")
        }
        /*
            判断是在审核中、审核成功、审核失败
         */
        function judgeFun(){
            // 根据判断来显示 哪一个页面
            var alertobj = layer.msg('请稍后......', {icon:16, shade:0.5, time:0});
            $.post("/api/customer/aliPay/aliPayOpenMiniStatus",{
                storeId:store_id,
            },function(data){
                let res = data.data;
                let resCode = data.data.code;
                console.log(resCode)
                //审核失败
                if(resCode == "AUDIT_REJECT"){
                    layer.close(alertobj);
                    $(".fail .text p").html(res.reject_reason)
                    $(".fail").removeClass("failnone");
                //审核成功
                }else if(resCode == "WAIT_RELEASE"){
                    layer.close(alertobj);
                    $(".grounding").removeClass("groundingnone");
                //审核中
                }else if(resCode == "AUDITING"){
                    layer.close(alertobj);
                    $(".audit").removeClass("auditnone");
                //上架成功
                }else if(resCode == "RELEASE"){
                    $(".success").removeClass("successnone");
                    zfxcjcgData(alertobj);
                }
            })
        }
        /*
            支付宝小程序创建成功 获取数据回显
        */
        function zfxcjcgData(alertobj){
            //拿小程序mini_app_id
            $.post("/api/alipayopen/aliPayApplet",{
                token:token,
                storeId:store_id,
            },function(data){
                if(data.status == 1){
                    let miniAppId = data.data.AuthorizerAppid;
                    //查询数据
                    $.post("/api/customer/aliPay/getAliPayOpenMiniBaseInfoQuery",{
                        mini_app_id:miniAppId,
                        type:1,
                    },function(data){
                        layer.close(alertobj);
                        let res = data.data;
                        if(data.status == 200){
                            console.log(res);
                            $(".centre .left p").eq(0).find("span").text(res.app_name);
                            $(".centre .left p").eq(1).find("span").text(res.app_english_name);
                            $(".centre .left p").eq(2).find("span").text(res.service_phone);
                            $(".centre .left p").eq(3).find("span").text(res.category_names);
                            $(".centrelogoimg img").attr("src",res.app_logo)
                            $(".centre .auto p").eq(0).find("span").text(res.app_slogan);
                            $(".centre .auto p").eq(1).find("span").text(res.app_desc);
                        }
                    })
                }else{
                    layer.msg(data.message, {
                        offset: '50px',
                        icon: 2,
                        time: 2000
                    });
                }
            })
        }
        /*
            审核成功 立即上架
        */
        $(".groundingimg").click(function(){
            console.log("立即上架");
            var alertobj = layer.msg('请稍后......', {icon:16, shade:0.5, time:0});
            $.post("/api/customer/aliPay/getTemplate",{
                token:token,
            },function(data){
                if(data.status == 1){
                    let res = data.data;
                    $.post("/api/alipayopen/aliPayApplet",{
                        token:token,
                        storeId:store_id,
                    },function(data){
                        let resdata =  data.data;
                        if(data.status == 1){
                            console.log(resdata.AuthorizerAppid)
                            console.log(res.version)
                            $.post("/api/customer/aliPay/getAliPayOpenMiniVersionOnline",{
                                mini_app_id:resdata.AuthorizerAppid,
                                app_version:res.version,
                                type:1
                            },function(data){
                                console.log(data)
                                if(data.status == 200){
                                    layer.msg(data.message, {time:1000});
                                    $(".grounding").addClass("groundingnone");
                                    $(".success").removeClass("successnone");
                                }else{
                                    layer.msg(data.message, {time:1000});
                                }
                            })
                        }else{
                            layer.msg(data.message, {icon:2, shade:0.5, time:1000});
                        }
                    })
                }else{
                    layer.msg(data.message, {icon:2, shade:0.5, time:1000});
                }
            })
        })
        /*
            支付宝小程序创建失败--重新创建
        */
        $(".cxcj").click(function(){
            //获取小程序版本
            $.post("/api/customer/aliPay/getTemplate",{
                token:token,
            },function(data){
                console.log(data)
                //获取小程序版本
                let version = data.data.version
                if( data.status == 1 ){
                    //获取 mini_app_id
                    $.post("/api/alipayopen/aliPayApplet",{
                        token:token,
                        storeId:store_id,
                    },function(data){
                        console.log(data)
                        let miniAppId = data.data.AuthorizerAppid;
                        if(data.status == 1){
                            $.post("/api/customer/aliPay/getAliPayOpenMiniVersionAuditedCancel",{
                                type:1,
                                mini_app_id:miniAppId,
                                app_version:version
                            },function(data){
                                console.log(data)
                                if(data.status == 200){
                                    console.log("重新创建");
                                    $(".fail").addClass("failnone");
                                    $(".oneboxnone").removeClass("oneboxBlock");
                                    dierbuFun();
                                    setBuData(2);
                                }else{
                                    layer.msg(data.message, {icon:2, shade:0.5, time:1000});
                                }
                            })
                        }else{
                            layer.msg(data.message, {icon:2, shade:0.5, time:1000});
                        }
                    })
                }else{
                    layer.msg(data.message, {icon:2, shade:0.5, time:1000});
                }
            })
        })

        /*
         营业执照的上传 
         */
        upload.render({
            elem: '#yyzzuploadFile', //绑定元素
            url: "/api/basequery/upload", //上传接口
            data:{
                token:token,
                type:'img',
                attach_name: 'file'
            },
            field:"file",
            method:"post",
            type : 'images',
            ext : 'jpg|png|gif',
            size: 2048,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
            	console.log(res);
                //上传完毕回调
                if(res.status == 1){
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:1000});

                    layui.jquery('#yyzzbgm').attr("src", res.data.img_url);
                    $("#yyzzbgm").removeClass("yyzzuploadDataImg");
                    $("input[name=licensePic]").val(res.data.img_url);

                }else{
                    layer.msg(res.message, {icon:2, shade:0.5, time:1000});
                }
            },
            error: function(err){
                //请求异常回调
                console.log(err);
            }
        });

        /*
         * Logo上传
         */
        upload.render({
            elem: '#uploadFile', //绑定元素
            url: "<?php echo e(url('/api/customer/user/aliPayOfflineMaterialImageUpload')); ?>", //上传接口
            data:{
                app_id:window.localStorage.getItem("mini_app_id"),
            },
            field:"file",
            method:"post",
            type : 'images',
            ext : 'jpg|png|gif',
            size: 256,
            auto: false,
            choose :function(obj){ //上传前限制 图片的宽高
                obj.preview(function(index, file, result){
                    var img = new Image();
                    img.src = result;
                    img.onload = function () { //初始化夹在完成后获取上传图片宽高，判断限制上传图片的大小。
                        if(img.width == img.height){
                            obj.upload(index, file); //满足条件调用上传方法
                        }else{
                            layer.msg("Logo图片宽高必须 1:1 ", {icon:2, shade:0.5, time:3000});
                        }
                    }
                });
            },
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
            	console.log(res);
                //上传完毕回调
                if(res.status == 200){
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:1000});
                    layui.jquery('#logobgm').attr("src", res.data.image_url);
                    $("#logobgm").removeClass("uploadDataImg");
                    $("input[name=license]").val(res.data.image_path);
                }else{
                    layer.msg(res.message, {icon:2, shade:0.5, time:1000});
                }
            },
            error: function(err){
                //请求异常回调
                console.log(err);
            }
        });

        /*
            支付宝小程序创建成功 - 更新支付宝小程序
        */
        $(".successGxXcx").click(function(){
            $(".success").addClass("successnone");
            $(".oneboxnone").removeClass("oneboxBlock");
            $("#stepsbox > div").addClass("stepsboxnone");
            $("#stepFour").removeClass("stepsboxnone");
            getTemplateFun();
        })
        /**
         * 监听类目第一select的选择
         */
        form.on("select(selectLeiMuYi)",function(data){
            console.log("我是类目一的选择");
        	let obj = {...data.value.split("&")};
        	let sessionLeiMu2 = window.sessionStorage.getItem("alipycreatAppletLeiMu");
        	let objLeiMu = JSON.parse(sessionLeiMu2);
        	let src = "<option value=''></option>";
        	for(let i=0;i<objLeiMu.length;i++){
        		if(objLeiMu[i].parent_category_id == obj[0]){
        			src += "<option value='"+objLeiMu[i].category_id+"&"+objLeiMu[i].category_name+"'>"+objLeiMu[i].category_name+"</option>"
        		}
        	}
			//将数据放到类目二
        	$("#selectLeiMuEr").html(src);
            $("#selectLeiMuSan").html("<option value=''></option>");
    		form.render("select");
        })
        /**
         * 监听类目第二select的选择
         */
        form.on("select(selectLeiMuEr)",function(data){
        	let obj = {...data.value.split("&")};
        	let sessionLeiMu2 = window.sessionStorage.getItem("alipycreatAppletLeiMu");
        	let objLeiMu = JSON.parse(sessionLeiMu2);
        	let src = "<option value=''></option>";
        	for(let i=0;i<objLeiMu.length;i++){
        		if(objLeiMu[i].parent_category_id == obj[0]){
        			src += "<option value='"+objLeiMu[i].category_id+"&"+objLeiMu[i].category_name+"'>"+objLeiMu[i].category_name+"</option>"
        		}
        	}
			//将数据放到类目三
        	$("#selectLeiMuSan").html(src);
    		form.render("select");

        })

        //上传 营业执照

        function setbusiness(licensePicObj,licenseNoObj){
            return new Promise(resolve=>{
                console.log(licensePicObj)
                console.log(licenseNoObj)
                $.post("/api/customer/aliPay/aliPayOpenBusinessCertifyy",{

                    storeId:store_id,
                    licensePic:licensePicObj,
                    licenseNo:licenseNoObj,

                },function(data){
                    resolve(data)
                },"json")
            })
        }
        /**
         * 创建小程序 
         */
        form.on('submit(cjxcx)',function(data) {
            var requestData = data.field;
            setbusiness(requestData.licensePic,requestData.licenseNo).then(v=>{
               if(v.status == 200){
                    cjxcxfun(requestData)
               }else if(v.message.indexOf("企业账号不支持成为个体工商户") >= 0){
                    cjxcxfun(requestData)
               }else if(v.message.indexOf("营业执照有效期非法") >= 0){
                    cjxcxfun(requestData)
               }else{
                    layer.msg(v.message, {icon:2, shade:0.5, time:1000});
               }
            })
            return false;
        })
        function cjxcxfun(requestData){
            let select1 = requestData.mini_category_list0_1;
            let select2 = requestData.mini_category_list0_2;
            let select3 = requestData.mini_category_list0_3;

            let mini_category_ids;
            if(select1 && !select2 && !select3){

                mini_category_ids = requestData.mini_category_list0_1.split("&")[0];

            }else if(select1 && select2 && !select3){

                mini_category_ids = requestData.mini_category_list0_1.split("&")[0]+"_"+requestData.mini_category_list0_2.split("&")[0];

            }else if(select1 && select2 && select3){

                mini_category_ids = requestData.mini_category_list0_1.split("&")[0]+"_"+requestData.mini_category_list0_2.split("&")[0]+"_"+requestData.mini_category_list0_3.split("&")[0];

            }

            console.log(mini_category_ids);

            if(!requestData.app_name){
				layer.msg("小程序名称不可为空", {
                    offset: '50px',
                    icon: 2,
                    time: 2000
                });
                return false;
			}
			if(!requestData.app_english_name){
				layer.msg("小程序英文名称不可为空", {
                    offset: '50px',
                    icon: 2,
                    time: 2000
                });
                return false;
			}
            let logImg = $("#logobgm").attr("src");
			if(!logImg){
				layer.msg("小程序Logo不可为空", {
                    offset: '50px',
                    icon: 2,
                    time: 2000
                });
                return false;
			}
			if(!requestData.service_phone){
				layer.msg("小程序客服电话不可为空", {
                    offset: '50px',
                    icon: 2,
                    time: 2000
                });
                return false;
			}
			if(!requestData.app_slogan){
				layer.msg("小程序简介不可为空", {
                    offset: '50px',
                    icon: 2,
                    time: 2000
                });
                return false;
			}
			if(!requestData.app_desc){
				layer.msg("小程序描述信息不可为空", {
                    offset: '50px',
                    icon: 2,
                    time: 2000
                });
                return false;
			}
			layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
            $.post("<?php echo e(url('/api/customer/aliPay/getAliPayOpenMiniBaseInfoQuery')); ?>",{
                type:2,
                mini_app_id:window.localStorage.getItem("mini_app_id"),
                app_name:requestData.app_name,
                app_english_name:requestData.app_english_name,
                app_slogan:requestData.app_slogan,
                app_logo:$("input[name=license]").val(),
                app_desc:requestData.app_desc,
                service_phone:requestData.service_phone,
                service_email:requestData.service_email,
                mini_category_ids:mini_category_ids
            },function(data){
                console.log(data)
                if(data.status == 200){
                    layer.msg("小程序创建成功", {icon:1, shade:0.5, time:1000});
                    getTemplateFun();
                    $("#stepsbox > div").addClass("stepsboxnone");
                    $("#stepFour").removeClass("stepsboxnone");
                    steps.next();
                    setBuData(3);
                }else{
                    layer.msg(data.message, {icon:2, shade:0.5, time:1000});
                }
            })
        }

        /**
         * 开启授权
         */
        form.on('submit(zjsq)', function(data) {
            const door_id = store_id;
            let alertObj = layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
            $.post("/api/customer/aliPay/getTemplate",{
                token:token,
            },function(data){
                let res = data;
                if(res.status == 1){
                    layer.close(alertObj);
                    window.open(""+res.data.alipay_applet_auth_url+"&ticket="+door_id+"","_blank");
                }else{
                    layer.msg(res.message, {icon:2, shade:0.5, time:1000});
                }
            })
            return false;
        })
        /*
            授权后下一步
         */
        form.on('submit(sqxyb)', function(data) {
            let next = true;
            dierbuFun(next);
        })
        /**
         * 重新追加授权
         */
		form.on('submit(cxzjsq)', function(data) {
            console.log(data);
            return false;
        })
        /**
         * 提交( -审核- )
         */
        form.on('submit(tjsh)', function(data) {
            console.log("tjsh")
            let res = data.field;
            layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
            $.post("/api/customer/aliPay/aliPayOpenMiniVersionUpload",{
                store_id:store_id,
                template_version: res.legal_persona_wechatname ,
                template_id: res.legal_persona_wechat ,
            },function(data){
                console.log(data.status);
                if(data.status == 200){
                    $.post("/api/customer/aliPay/updateAliVersion",{
                        storeId:store_id,
                        version: res.legal_persona_wechatname
                    },function(data){
                        if(data.status == 200){
                            layer.msg("提交成功！！！", {time:1000});
                        }
                    })
                }else{
                    layer.msg(data.message, {icon:1, shade:0.5, time:1000});
                }
            })
            return false;
        });

        form.on('submit(tjsh2)', function(data) {
            console.log("tjsh2")
            let res = data.field;
            let legal_persona_wechatname = $("input[name='legal_persona_wechatname']").val();
            layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
            $.post("/api/alipayopen/aliPayApplet",{
                token:token,
                storeId:store_id,
            },function(data1){
                if(data1.status == 1){
                    $.post("<?php echo e(url('/api/customer/aliPay/getAliPayOpenMiniVersionAuditApply')); ?>",{
                        mini_app_id:data1.data.AuthorizerAppid,
                        app_version:legal_persona_wechatname,
                        license_name:res.license_name,
                        license_valid_date:res.license_valid_date,
                        store_applets_desc:res.desc
                    },function(data){
                        var status = data.status;
                        if(status == 200){
                            layer.msg("审核中！！！", {time:1000});
                            $(".oneboxnone").addClass("oneboxBlock");
                            $(".audit").removeClass("auditnone");
                            setBuData(4);
                        }else{
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 1000
                            });
                        }
                    },"json");
                }else{
                    layer.msg("appId获取失败", {time:1000});
                }
            })
            return false;
        })
        /**
         * 函数将数据放到第一类目
         */
        function selectLeiMuYiFun(arr){
            console.log("3")
        	let newArr = "";
    		for(let i=0;i<arr.length;i++){
    			if(arr[i].parent_category_id == 0){
    				newArr += "<option value='"+arr[i].category_id+"&"+arr[i].category_name+"'>"+arr[i].category_name+"</option>";
    			}
    		}
    		$("#selectLeiMuYi option").after(newArr);
    		form.render("select");
        }
        /*
         * 获取第二步创建小程序的用户类目
         */
        function getLeiMuData(){ 
            $.post("<?php echo e(url('/api/customer/aliPay/getAliPayOpenMiniBaseInfoQuery')); ?>",{
                mini_app_id:window.localStorage.getItem("mini_app_id"),
                type:1
            },function(data){
                if( data.status !== 200 ){
                    console.log("获取支付宝类目失败！！！");
                }else{
                    console.log("2")
                    var responseData = data.data;
                    var arr = responseData.mini_category_list;
                    window.sessionStorage.setItem("alipycreatAppletLeiMu",JSON.stringify(arr));
                    selectLeiMuYiFun(arr);
                }
            })
        }
        /*
            改变步骤条状态 ， 并存到数据库
        */
        function setBuData(page){
            let num = page;
            $.post("/api/customer/aliPay/updateAliAppletsStep",{
                storeId:store_id,
                token:token,
                createdStep: num ,
            },function(data){
                console.log(data);
            })
        }
        /*
            获取模板ID 和 小程序版本
        */
        function getTemplateFun(){
            $.post("/api/customer/aliPay/getTemplate",{
                token:token,
            },function(data){
                let res = data.data;
                console.log(res);
                console.log(res.aliPay_applet_template_id,"模板ID");
                console.log(res.version,"小程序版本");
                $("input[name='legal_persona_wechat']").val(res.aliPay_applet_template_id);
                $("input[name='legal_persona_wechatname']").val(res.version);
            })
        }
        /*
            判断是否授权，（如果成功）就将数据赋予给创建页面
        */
        function dierbuFun(next){
            $.post("/api/alipayopen/aliPayApplet",{
                token:token,
                storeId:store_id,
            },function(data){

                console.log(data)

                if(data.status == 1){
                    layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                    if(!next){
                        $("#stepsbox > div").addClass("stepsboxnone");
                        $("#stepTwo").removeClass("stepsboxnone");
                    }
                    // 说明授权成功，并将mini_app_id保存在内存中
                    let miniAppId = data.data.AuthorizerAppid;

                    window.localStorage.setItem("mini_app_id",miniAppId);
                    //查询数据
                    $.post("/api/customer/aliPay/getAliPayOpenMiniBaseInfoQuery",{
                        mini_app_id:miniAppId,
                        type:1,
                    },function(data){
                        let res = data.data;
                        if(data.status == 200){
                            //判断是否从“授权后下一步”过来
                            layer.msg("请求成功！！！", {time:1000});
                            if(next){
                                $("#stepsbox > div").addClass("stepsboxnone");
                                $("#stepTwo").removeClass("stepsboxnone");
                                steps.next();
                            }
                            setBuData(2);
                            cjxcxFunData(res);
                        }
                    })
                }else{
                    layer.msg("请给支付宝小程序授权", {
                        offset: '50px',
                        icon: 2,
                        time: 2000
                    });
                }



            })
        }
        /*
            创建小程序的数据回显
        */
        function cjxcxFunData(data){

            console.log(data);
            //姓名回显
            $("input[name='app_name']").val(data.app_name);
            // //Logo回显
            layui.jquery('#logobgm').attr("src",data.app_logo);
            $("#logobgm").removeClass("uploadDataImg");
            // //英文名称
            $("input[name='app_english_name']").val(data.app_english_name);
            // // 客服电话
            $("input[name='service_phone']").val(data.service_phone);
            // // 邮箱
            if(data.service_email){
                $("input[name='service_email']").val(data.service_email);
            }
            // //小程序简介
            $("input[name='app_slogan']").val(data.app_slogan);
            // //小程序描述信息
            $("textarea[name='app_desc']").val(data.app_desc);

            //回显小程序类目
            // $("#selectLeiMuYi option").eq(1).html("789");

            let leiMuList = data.category_names.split("_");

            console.log(leiMuList)
            if(leiMuList[0]){
                let src = "<option value=''>"+leiMuList[0]+"</option>";
                $("#selectLeiMuYi").html(src);
            }
            if(leiMuList[1]){
                let src = "<option value=''>"+leiMuList[1]+"</option>";
                $("#selectLeiMuEr").html(src);
            }
            if(leiMuList[2]){
                let src = "<option value=''>"+leiMuList[2]+"</option>";
                $("#selectLeiMuSan").html(src);
            }
            //判断本地是否有 类目数据
            if(sessionLeiMu){
                let arr = JSON.parse(sessionLeiMu);
                selectLeiMuYiFun(arr);
            //否则就需要请求一次
            }else{
                getLeiMuData();
            }
            form.render("select");
        }
        /**
         * 点击图片放大
         */
        $('.zfbsqImg img').click(function(){
        	layer.photos({
                photos: '.zfbsqImg',
                shadeClose: false,
                closeBtn: 2,
                anim: 0
            });
        })
    })





</script>

</html>