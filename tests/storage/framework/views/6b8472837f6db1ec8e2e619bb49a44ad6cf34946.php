<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>桌号管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style>
        .layui-table-cell{
            height: 60px;
            /*line-height: 60px;*/
        }
        .layui-table-header{
            height: 40px;
            line-height: 40px !important;
        }
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .tongbu {
            background-color: #4c9ef8;
            color: #fff;
            border-radius: 3.5px
        }

        .cur {
            color: #21c4f5;
        }

        .userbox{
            height: 190px;
            margin-right: 0;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left:75px;
            top: 50px;
            width: 298px;
            background-color: #ffffff;
            border: 1px solid #ddd;
        }

        .userbox .list,
        .storebox .list {
            height: 38px;
            line-height: 38px;
            cursor: pointer;
            padding-left: 10px;
        }

        .userbox .list:hover,
        .storebox .list:hover {
            background-color: #eeeeee;
        }

        .yname {
            font-size: 13px;
            color: #444;
            width: 60px;
            text-align: center;
            margin-top: 7px;
        }
        .xgrate{color: #fff;font-size: 15px;padding: 7px;height: 30px;line-height: 30px;background-color: #3475c3;}
        .layui-form-select{width: 300px;}
    </style>
</head>

<body>
    <div id="mask" class="mask"></div>
    <div class="layui-fluid" style="margin-top:50px;">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-fluid">
                    <div class="layui-row layui-col-space15">
                        <div class="layui-col-md12">
                            <div class="layui-card">
                                <div class="layui-card-header">桌号管理</div>
                                <div class="layui-card-body">
                                    <div class="layui-btn-container" style="font-size:14px;">
                                        <div id="search-form" style="font-size:14px;display:flex;">
                                            <div class="layui-form" lay-filter="component-form-group">
                                                <div class="layui-form-item">
                                                    <div style="display:flex;">
                                                        <label class="yname">门店</label>
                                                        <select name="agent" id="agent" lay-filter="agent" lay-search>

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="layui-form" lay-filter="component-form-group">
                                                <div class="layui-form-item">
                                                    <div style="display:flex;">
                                                        <label class="yname">桌名称</label>
                                                        <input type="text" style="border-radius:5px;width: 300px;" name="table_name" id="table_name" lay-verify="schoolname" autocomplete="off" placeholder="请输入桌名称" class="layui-input inputstore">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="layui-inline">
                                                <button class="layui-btn layuiadmin-btn-list" lay-submit="" lay-filter="LAY-app-contlist-search" style="border-radius:5px;margin-bottom: 0;height:36px;line-height: 36px;margin-left: 50px;">
                                                    <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                                </button>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="layui-inline">
                                        <button class="layui-btn layuiadmin-btn-list" id="addTable" style="border-radius:5px;margin-bottom: 30px;height:36px;line-height: 36px;margin-left: 50px;">
                                           <i class="layui-icon">&#xe608;</i>新增
                                        </button>
                                    </div>

                                    <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>

                                    <script type="text/html" id="storeTableToolbar">
                                        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="edit">编辑</a>
                                        {{# if (d.table_status == 2) { }}
                                        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="closeTable">闭台</a>
                                        {{# } }}
                                        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="alipay_qrcode_download">支付宝下载</a>
                                        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="wechat_qrcode_download">微信下载</a>
                                        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="common_qrcode_download">聚合码下载</a>
                                        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="del">删除</a>
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" class="store_id">
    <input type="hidden" class="sort">
    <input type="hidden" class="stu_class_no">

    <input type="hidden" class="stu_order_batch_no">
    <input type="hidden" class="user_id">

    <input type="hidden" class="pay_status">
    <input type="hidden" class="pay_type">

    <input type="hidden" class="paycode">
    <input type="hidden" class="company_id">

    <input type="hidden" class="sbid">
    <input type="hidden" class="danhao">
    <input type="hidden" class="tiaoma">
    <input type="hidden" class="js_role_id">

    
    <script type="text/html" id="common_qrcode_template">
        <img src="{{ d.common_qrcode }}" width="60px;">
    </script>

    <script type="text/html" id="wechat_qrcode_template">
        {{# if (d.wechat_applet_qrcode != '') { }}
        <img src="{{ d.wechat_applet_qrcode }}" width="60px;">
        {{# } else { }}
        暂无数据
        {{# } }}
    </script>

    <script type="text/html" id="alipay_qrcode_template">
        {{# if (d.alipay_applet_qrcode_full != '') { }}
        <img src="{{ d.alipay_applet_qrcode_full }}" width="60px;">
        {{# } else { }}
        暂无数据
        {{# } }}
    </script>

<script src="<?php echo e(asset('/user/js/jquery.min.js?v=2.1.4')); ?>"></script>
<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
    <script>
        var token = localStorage.getItem("Publictoken");
        var str = location.search;
        var store_id = str.split('?')[1];

        layui.config({
            base: '../../layuiadmin/' //静态资源所在路径
        }).extend({
            index: 'lib/index' //主入口模块
        }).use(['index', 'form', 'table'], function() {
            var $ = layui.$,
            form = layui.form,
            table = layui.table;

            $('.store_id').val(store_id);
            // 未登录,跳转登录页面
            $(document).ready(function() {
                if (token == null) {
                    window.location.href = "<?php echo e(url('/user/login')); ?>";
                }
            });
            var s_storename = localStorage.getItem('s_storename');
            if (store_id == undefined) {

            } else {
                $('.inputstore').val(s_storename);
            }

            // 重置按钮
            $("#reset").click(function () {
                $("#agent").val("");
                $("#table_name").val("");
                var store_id = $('#agent').val();
                var table_name = $('#table_name').val();
                table.reload('test-table-page', {
                    where: {
                        store_id: store_id,
                        table_name: table_name,
                    }
                });
            })

            // 打开新增窗口
            $('#addTable').on("click",function(){
                // 选择门店
                $.ajax({
                    url : "<?php echo e(url('/api/merchant/store_lists')); ?>",
                    data : {token:token,l:100},
                    type : 'post',
                    success : function(data) {
                        var optionStr = "";
                        for(var i=0;i<data.data.length;i++){
                            optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
                        }
                        $("#agent_modal").append('<option value="">选择门店</option>'+optionStr);
                        layui.form.render('select');
                    },
                    error : function(data) {
                        alert('查找板块报错');
                    }
                });

                layer.open({
                    type: 1,
                    title: false,
                    closeBtn: 0,
                    area: '516px',
                    skin: 'layui-layer-nobg', //没有背景色
                    shadeClose: true,
                    content: $('#submitAppletsNameInfo')
                });
            });
            //关闭新增弹窗
            $(".close").on("click",function(){
                $("#submitAppletsNameInfo").hide();
                document.location.reload();
            })

            // 选择门店
            $.ajax({
                url : "<?php echo e(url('/api/merchant/store_lists')); ?>",
                data : {token:token,l:100},
                type : 'post',
                success : function(data) {
                    var optionStr = "";
                    for(var i=0;i<data.data.length;i++){
                        optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
                    }
                    $("#agent").append('<option value="">选择门店</option>'+optionStr);
                    layui.form.render('select');
                },
                error : function(data) {
                    alert('查找板块报错');
                }
            });

            // 渲染表格
            table.render({
                elem: '#test-table-page',
                url: "<?php echo e(url('/api/merchant/table_lists')); ?>",
                method: 'post',
                where: {
                    token: token,
                    store_id: store_id
                },
                request: {
                    pageName: 'p',
                    limitName: 'l'
                },
                page: true,
                cellMinWidth: 100,
                cols: [[
                    {type:'checkbox', fixed: 'left'}
                    , {field: 'store_name',align: 'center',title: '门店'}
                    // , {field: 'table_id',align: 'center',title: '桌ID',width: 200}
                    , {field: 'table_name', align: 'center',title: '桌名称'}
                    , {field: 'table_status_name',align: 'center',title: '状态'}
                    , {field: '',align: 'center',title: '支付宝码',templet: '#alipay_qrcode_template'}
                    , {field: '',align: 'center',title: '微信码',templet: '#wechat_qrcode_template'}
                    , {field: 'common_qrcode',align: 'center',title: '聚合二维码',templet: '#common_qrcode_template'}
                    , {fixed: 'right', title:'操作', align: 'center', toolbar:'#storeTableToolbar',width:400}
                    ]
                ],
                response: {
                    statusName: 'status' //数据状态的字段名称，默认：code
                    ,statusCode: 1 //成功的状态码，默认：0
                    ,msgName: 'message' //状态信息的字段名称，默认：msg
                    ,countName: 't' //数据总数的字段名称，默认：count
                    ,dataName: 'data' //数据列表的字段名称，默认：data
                },
                done: function(res, curr, count) {
                    $('th').css({
                        'font-weight': 'bold',
                        'font-size': '15',
                        'color': 'black',
                        'background': 'linear-gradient(#f2f2f2,#cfcfcf)'
                    }); //进行表头样式设置
                }
            });

            form.on('submit(LAY-app-contlist-search)', function(data){
                var store_id = $('#agent').val();
                var table_name = $('#table_name').val();
                table.reload('test-table-page', {
                    where: {
                        store_id: store_id,
                        table_name: table_name,
                    }
                });
            });

            // 新增桌号
            form.on('submit(submitNewInfo)',function(data){
                var datas = data.field;
                if(!datas.agent_modal){
                    layer.msg("门店不可为空", {
                        offset: '50px'
                        ,icon: 2
                        ,time: 2000
                    });
                    return false;
                }

                if(!datas.table_name_modal){
                    layer.msg("桌名称不可为空", {
                        offset: '50px'
                        ,icon: 2
                        ,time: 2000
                    });
                    return false;
                }

                $.ajax({
                    type: 'POST',
                    url: "<?php echo e(url('/api/merchant/create_table')); ?>",
                    data: {
                        token:token,
                        store_id: datas.agent_modal,
                        table_name: datas.table_name_modal,
                    },
                    success: function (res) {
                        if(res.status == 1){
                            layer.msg('添加成功',{icon:1},function(){
                                window.location.reload();
                            });
                        }else{
                            layer.msg(res.msg, {icon:1},function(){
                                window.location.reload();
                            });
                        }
                    }
                });
                return false;
            });

            // 监听行工具事件
            table.on('tool(test-table-page)', function(obj){
                // 获取obj里的data
                var data = obj.data;
                console.log(obj.event)
                if(obj.event === 'del'){
                    console.log(data)

                    layer.confirm('确认删除吗？', function(){
                        $.ajax({
                            type: 'POST',
                            url: "<?php echo e(url('/api/merchant/delete_table')); ?>",
                            data: {
                                token:token,
                                store_id: data.store_id,
                                table_id: data.table_id,
                            },
                            success:function(res){
                                console.log(res)
                                if(res.status == 1){
                                    layer.msg('删除成功',{icon:1},function(){
                                        window.location.reload();
                                    });
                                }else{
                                    layer.msg('删除失败: ' + res.msg,{icon:1},function(){
                                        window.location.reload();
                                    });
                                }
                            }
                        })
                    });
                }else if(obj.event === 'edit'){
                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#editModal')
                    });

                    // 将值放入模态框
                    $('#table_name_modal_edit').val(data.table_name);

                    form.on('submit(submitEditTwo)',function(message){
                        var mes = message.field;
                        $.ajax({
                            url: "<?php echo e(url('/api/merchant/edit_table')); ?>",
                            type: "POST",
                            data:{
                                token:token,
                                id: data.id,
                                table_name: mes.table_name_modal_edit,
                            },
                            success: function(res){
                                if(res.status == 1){
                                    layer.msg('编辑成功',{icon:1},function(){
                                        window.location.reload();
                                    });
                                }else{
                                    layer.msg('编辑失败',{icon:1},function(){
                                        window.location.reload();
                                    });
                                }
                            }
                        })
                        // 防止页面直接刷新不提示
                        return false;
                    })
                }else if(obj.event === 'closeTable'){
                    $.ajax({
                        url: "<?php echo e(url('/api/merchant/close_table')); ?>",
                        type: "POST",
                        data:{
                            token:token,
                            id: data.id,
                        },
                        success: function(res){
                            if(res.status == 1){
                                layer.msg('编辑成功',{icon:1},function(){
                                    window.location.reload();
                                });
                            }else{
                                layer.msg('编辑失败',{icon:1},function(){
                                    window.location.reload();
                                });
                            }
                        }
                    })
                }else if(obj.event === 'common_qrcode_download'){
                    var img_url = data.common_qrcode_url_full;

                    const a = document.createElement('a'); // 创建a标签
                    a.setAttribute('download', '');// download属性
                    a.setAttribute('href', img_url);// href链接
                    a.click();// 自执行点击事件
                }else if(obj.event === 'wechat_qrcode_download'){
                    var img_url = data.wechat_applet_qrcode_url_full;

                    const a = document.createElement('a'); // 创建a标签
                    a.setAttribute('download', '');// download属性
                    a.setAttribute('href', img_url);// href链接
                    a.click();// 自执行点击事件
                }else if(obj.event === 'alipay_qrcode_download'){
                    var img_url = data.alipay_applet_qrcode;

                    const a = document.createElement('a'); // 创建a标签
                    a.setAttribute('download', '');// download属性
                    a.setAttribute('href', img_url);// href链接
                    a.click();// 自执行点击事件
                }
            });

        });
    </script>

    <!-- 新增 -->
    <div id="submitAppletsNameInfo" class="hide layui-form" lay-filter="appletsName" style="display: none;background-color: #fff;">
        <div class="xgrate">新增</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" style="padding-top: 20px;padding-bottom: 20px;">
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center">门店</label>
                    <div class="layui-input-block">
                        <select name="agent_modal" id="agent_modal" lay-filter="agent" lay-search></select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center">桌名称</label>
                    <div class="layui-input-block">
                        <input type="text" placeholder="请输入桌名称" name="table_name_modal"  class="layui-input title">
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <div class="layui-footer" style="float: right;">
                        <button class="layui-btn close"style="border-radius:5px;background-color: #FFB800;">关闭</button>
                        <button class="layui-btn submitNewInfo" lay-submit lay-filter="submitNewInfo" style="border-radius:5px">确定</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- 编辑 -->
    <div id="editModal" class="hide layui-form" lay-filter="appletsName" style="display: none;background-color: #fff;">
        <div class="xgrate">编辑</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" style="padding-top: 20px;padding-bottom: 20px;">






                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center">桌名称</label>
                    <div class="layui-input-block">
                        <input type="text" placeholder="请输入桌名称" name="table_name_modal_edit"  class="layui-input title">
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <div class="layui-footer" style="float: right;">
                        <button class="layui-btn close"style="border-radius:5px;background-color: #FFB800;">关闭</button>
                        <button class="layui-btn submitEdit" lay-submit lay-filter="submitEditTwo" style="border-radius:5px">确定</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>