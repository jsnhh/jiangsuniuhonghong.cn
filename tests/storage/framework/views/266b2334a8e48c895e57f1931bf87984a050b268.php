<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>新增充值活动</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/formSelects-v4.css')); ?>" media="all">
    <style>
        .img{width:130px;height:90px;overflow: hidden;}
        .img img{width:100%;height:100%;}
        .layui-layer-nobg{width: none !important;}
        /*.layui-layer-content{width:600px;height:550px;}*/
        .layui-card-header{width:200px;text-align: left;float:left;}
        .layui-card-body{margin-left:28px;}
        .layui-upload-img{width: 120px; height: 120px; /*margin: 0 10px 10px 0;*/}

        .up{position: relative;display: inline-block;cursor: pointer;border-color: #1ab394; color: #FFF;width: auto !important;font-size: 10px !important;text-align: center !important;}
        .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
        .layui-upload-list{width: 120px;height:120px;overflow: hidden;margin: 10px auto;}
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {-webkit-appearance: none !important;margin: 0;}
        .cz{
            width:200px;float:left;
        }
        .layui-input-block p{
            float:left;
            line-height: 36px;
            padding:0 10px;
        }
        .jiahao{
            background-color: #3475c3;
            color:#fff;
            width:26px;
            height:26px;
            line-height:26px;
            float:left;
            margin-left:10px;
            font-size:26px;
            text-align: center;
            border-radius: 50px;
            margin-top: 5px;
        }
        .jiahao.hover{
            cursor: pointer;
        }
        .jianhao{
            background-color: #3475c3;
            color:#fff;
            width:26px;
            height:26px;
            line-height:24px;
            float:left;
            margin-left:10px;
            font-size:26px;
            text-align: center;
            border-radius: 50px;
            margin-top: 5px;
        }
        .jianhao.hover{
            cursor: pointer;
        }
    </style>
</head>
<body>

<div class="layui-fluid" style="margin-top:68px;">
    <div class="layui-card">
      <div class="layui-card-header">新增充值活动</div>
      <div class="layui-card-body layui-row layui-col-space10">
        <div class="layui-form">

            <div class="czlist">

                <div class="layui-form-item">
                    <label class="layui-form-label">充值</label>
                    <div class="layui-input-block ">
                        <input class="layui-input cz c_value" type="number" value="">
                        <p>送</p>
                        <input class="layui-input cz s_value" type="number" value="">
                        <p>元</p>
                        <div class="jiahao">+</div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">活动开始时间</label>
                <div class="layui-inline">
                    <div class="layui-input-inline" style="width:500px;">
                        <input type="text" class="layui-input frs-item test-item" placeholder="活动开始时间" lay-key="21">
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">活动结束时间</label>
                <div class="layui-inline">
                    <div class="layui-input-inline" style="width:500px;">
                        <input type="text" class="layui-input fre-item test-item" placeholder="活动结束时间" lay-key="22">
                    </div>
                </div>
            </div>
            <div class="layui-form-item class">
                <label class="layui-form-label">适用门店</label>
                <div class="layui-input-block">
                    <select name="class" id="class" xm-select="class">

                    </select>
                </div>
            </div>


            <div class="layui-form-item layui-layout-admin">
                <div class="layui-input-block">
                    <div class="layui-footer" style="left: 0;">
                        <button class="layui-btn submit">确定提交</button>
                        <!--<button type="reset" class="layui-btn layui-btn-primary">重置</button>-->
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>

</div>

<input type="hidden" class="js_store" value="">
<input type="hidden" class="js_jifen" value="">
<input type="hidden" class="js_guize" value="">
<input type="hidden" class="js_newsjf" value="">

<input type="hidden" class="classid" value="">


<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script>
    var token = localStorage.getItem("Publictoken");


    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'table','form','upload','formSelects','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,table = layui.table
            ,element = layui.element
            ,upload = layui.upload
            ,form = layui.form
            ,formSelects = layui.formSelects
            ,laydate = layui.laydate;

            element.render();
        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="<?php echo e(url('/user/login')); ?>";
            }
        })

        formSelects.render('class');
        formSelects.btns('class', []);
        var arr=[];
        var storearr=[];
        formSelects.config('class', {
            beforeSuccess: function(id, url, searchVal, result){
                //我要把数据外层的code, msg, data去掉
                result = result.data;
                // console.log(result);
                for(var i=0;i<result.length;i++){

                    var data ={"value":result[i].store_id,"name":result[i].store_name}
                    arr.push(data);
                    // console.log(arr);
                }
                // console.log(arr);
                //然后返回数据
                return arr;
            }
        }).data('class', 'server', {
            url:"<?php echo e(url('/api/merchant/store_lists?token=')); ?>"+token
        });

        formSelects.on('class', function(id, vals, val, isAdd, isDisabled){
            //id:           点击select的id
            //vals:         当前select已选中的值
            //val:          当前select点击的值
            //isAdd:        当前操作选中or取消
            //isDisabled:   当前选项是否是disabled
             console.log(isAdd);
            //如果return false, 那么将取消本次操作
            // return false;
            // console.log(val.value);
            $('.classid').val(val.value);
            if(isAdd==true){

                storearr.push(val.value)
                console.log(storearr)

            }else{
                var a=val.value
                Array.prototype.indexOf = function (val) {
                     for(var i = 0; i < this.length; i++){
                      if(this[i] == val){return i;}
                     }
                     return -1;
                }
                Array.prototype.remove = function (val) {
                     var index = this.indexOf(val);
                     if(index > -1){this.splice(index,1);}
                }

                storearr.remove(a);//测试OK
                console.log(storearr)
                return storearr
            }
        });


        laydate.render({
          elem: '.frs-item'
          ,type:'datetime'
          ,done: function(value){

          }
        });
        laydate.render({
          elem: '.fre-item'
          ,type:'datetime'
          ,done: function(value){

          }
        });

        $('.jiahao').click(function(){
            var str=''
            str+='<div class="layui-form-item">';
                str+='<label class="layui-form-label">充值</label>';
                str+='<div class="layui-input-block">';
                    str+='<input class="layui-input cz c_value" type="number" value="">';
                    str+='<p>送</p>';
                    str+='<input class="layui-input cz s_value" type="number" value="">';
                    str+='<p>元</p>';
                    str+='<div class="jianhao">-</div>';
                str+='</div>';
            str+='</div> ';


            $('.czlist').append(str)
        });

        $('.czlist').on('click','.jianhao',function(){
            $(this).parents('.layui-form-item').remove()
        })









        $('.submit').on('click', function(){
            var oneline = []
            $('.czlist .layui-form-item').each(function(){
                $(this).find('.c_value').val()
                $(this).find('.s_value').val()

                var obj = { 'cz': $(this).find('.c_value').val(), 'cz_s': $(this).find('.s_value').val()}
                oneline.push(obj)
            })
            var cz=JSON.stringify(oneline)
            console.log(cz)

            console.log(storearr.join(','))




            $.post("<?php echo e(url('/api/member/cz_set')); ?>",
            {
                token:token,
                store_ids: storearr.join(','),
                yx_type :'1',
                cz_list: cz,
                b_time: $('.frs-item').val(),
                e_time: $('.fre-item').val(),

            },function(res){
                console.log(res);
                if(res.status==1){
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 3000
                    });
                }else{
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 3000
                    });
                }
            },"json");

        });


    });
</script>
</body>
</html>
