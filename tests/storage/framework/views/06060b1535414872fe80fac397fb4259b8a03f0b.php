<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>小程序</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/modules/layuiicon/iconfont.css')); ?>" media="all">
    <style type="text/css">
        .xgrate {
            color: #fff;
            font-size: 15px;
            padding: 7px;
            height: 30px;
            line-height: 30px;
            background-color: #3475c3;
        }

        .up {
            position: relative;
        }

        .up #uploadFile {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            opacity: .01;
            width: 100px;
            height: 30px;
        }

        .up input[type=file] {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            opacity: .01;
            width: 100px;
            height: 30px;
        }

        .demo5 {
            width: 100px;
        }

        .box1 {
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            padding: 15px;
            box-shadow: 0 1px 3px rgba(0, 0, 0, .02), 0 16px 32px -4px rgba(0, 0, 0, .17);
        }

        .box2 {
            margin-left: 20px;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            padding: 15px;
            box-shadow: 0 1px 3px rgba(0, 0, 0, .02), 0 16px 32px -4px rgba(0, 0, 0, .17);
        }

        .person {
            width: 100%;
            height: 100px;
            text-align: center;
            line-height: 20px;
            color: #000;
            font-size: 14px;
        }

        .athorize {
            margin-top: 20px;
            width: 50%;
            height: 40px;
            line-height: 40px;
            text-align: center;
            font-size: 14px;
            color: #fff;
            background-color: #00a3fe;
        }

        .athorize:hover {
            cursor: pointer;
        }

        .button {
            margin: 30px auto;
            width: 83%;
        }

        #steps {
            padding: 30px 0 0 260px;
        }

        .layui-form-label {
            display: block;
            margin-left: 111px;
            padding-bottom: 8px;
        }

        p {
            width: 50%;
            margin-left: 111px;
            height: 22px;
            font-size: 14px;
            font-weight: 400;
            color: #999999;
            line-height: 22px;
        }

        input {
            width: 50%;
        }
        .stepsboxnone{
            display: none!important;
        }
        .disabled{cursor: not-allowed; }
        .uploadDataImg{display: none;}
        .licenseDataImg{display: none;}

        .selectoptioncolor input::-webkit-input-placeholder {
            color: #000000; 
        }
        .selectoptioncolor input:-moz-placeholder{
            color: #000000;
        }
        .selectoptioncolor input::-moz-placeholder {
            color: #000000;
        }
        .selectoptioncolor input:-ms-input-placeholder {
            color: #000000;  
        }

    </style>
</head>

<body>
<div class="layui-fluid" style="margin-top:50px;">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">微信小程序设置</div>
                <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief"
                     style="height:100%;padding-bottom:215px;">
                    <div id="steps"></div>
                    <div class="layui-tab-content">
                        <!-- 创建 -->
                        <div class="layui-tab-item layui-show" id="addwechatapp" style="margin-left: 24%; margin-top:50px;display:none !important;">
                            <form class="layui-form" action="" lay-filter="example">
                                <div class="layui-row">
                                    <div class="grid-demo">
                                        <div class="layui-form-item">
                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                    <span
                                                            style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                企业名称
                                            </label>
                                            <div class="layui-input-block">
                                                <input type="text" name="name" placeholder="请输入企业名称"
                                                       class="layui-input">
                                            </div>
                                        </div>
                                        <div class="layui-form-item">
                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                    <span
                                                            style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                法人微信
                                            </label>
                                            <div class="layui-input-block">
                                                <input type="text" name="legal_persona_wechat" lay-verify="title"
                                                       autocomplete="off" placeholder="请输入法人微信" class="layui-input"
                                                       style="width: 50%;">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="grid-demo grid-demo-bg1">
                                        <div class="layui-form-item">
                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                    <span
                                                            style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                信用代码
                                            </label>
                                            <div class="layui-input-block">
                                                <input type="text" name="code" lay-verify="title" autocomplete="off"
                                                       placeholder="请输入信用代码" class="layui-input" style="width: 50%;">
                                            </div>
                                        </div>
                                        <div class="layui-form-item">
                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                    <span
                                                            style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                联系电话
                                            </label>
                                            <div class="layui-input-block">
                                                <input type="text" name="component_phone" lay-verify="title"
                                                       autocomplete="off" placeholder="请输入联系电话" class="layui-input"
                                                       style="width: 50%;">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="grid-demo">
                                        <div class="layui-form-item">
                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                    <span
                                                            style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                法人姓名
                                            </label>
                                            <div class="layui-input-block">
                                                <input type="text" name="legal_persona_name" lay-verify="title"
                                                       autocomplete="off" placeholder="请输入法人姓名" class="layui-input"
                                                       style="width: 50%;">
                                            </div>
                                            <!-- <div class="grid-demo grid-demo-bg1">
                                                <label style="display: block;margin-left: 111px; padding-bottom: 15px;">
                                                    <b style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</b>
                                                    经营执照
                                                </label>
                                                <div class="layui-upload up">
                                                    <div style="position: absolute;top:-20px;left:105px;border:1px dashed #e2e2e2;width:170px;height:100px">
                                                       <input type="file" name="file" id="uploadFile"style="position: absolute;top:-20px;left:106px;width:100px;height:100px;" />
                                                       <img class="layui-upload-img" id="demo5">
                                                       <p id="demoText"></p>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="license" value="" />
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="button">
                                    <button type="submit" class="layui-btn" lay-submit="" lay-filter="preservation"
                                            id="preservation"
                                            style="background: #FFFFFF;border-radius: 2px;border: 1px solid rgba(0, 0, 0, 0.15);color:#666666;">保存</button>
                                    <button type="submit" class="layui-btn" lay-submit="" lay-filter="addwechat"
                                            id="addwechat" style="background: #1E9FFF;">创建</button>
                                </div>
                            </form>
                        </div>
                        <!-- 认证 -->
                        <div class="layui-tab-item layui-show" id="stepTwo" style="margin-left: 24%; margin-top:25px;display:none !important;">
                            <p
                                    style="width:100%;margin-left:8px;display:block;height: 22px;font-size: 14px;font-weight: 400;color: #333333;line-height: 22px;margin-bottom: 15px;">
                                <i class="layui-icon iconfont" style="font-size: 14px;color:#FFB800;">&#xe670;</i>
                                该步骤需要法人在微信上操作授权：平台向法人微信下发模板消息。法人需在24小时内点击消息，进行身份证信息与人脸识别信息收集。
                            </p>
                            <div id="photo-list">
                                <img style="width:70%" src="<?php echo e(asset('/mb/renzheng.png')); ?>">
                            </div>
                            <div class="button">
                                <button type="button" class="layui-btn authentication"style="background: #1E9FFF;">已认证</button>
                            </div>

                        </div>
                        <!-- 授权 -->
                        <div class="layui-tab-item layui-show" id="stepThree" style="margin-left: 31%; margin-top:50px;display:none !important;">
                            <button type="button" class="layui-btn athorize"
                                    style="margin-right:12px;border-radius: 5px;">追加授权</button>
                        </div>
                        <!-- 上传 -->
                        <div class="layui-tab-item layui-form" id="stepFour" style="margin-left: 24%; margin-top:50px;display:none !important;">
                            <form class="layui-form" action="" lay-filter="example">
                                <div class="grid-demo">
                                    <div class="layui-form-item">
                                        <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                            <b style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</b>
                                            程序名称
                                        </label>
                                        <div class="layui-input-block">
                                            <input type="text" name="wechat_name" lay-verify="title"
                                                   autocomplete="off" placeholder="请输入程序名称" class="layui-input"
                                                   style="width: 50%;">
                                        </div>
                                        <p>名称由中文、英文、下划线、+、-组成，不超过15个汉字或30个字符。</p>
                                        <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                            <b style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</b>
                                            应用版本
                                        </label>
                                        <div class="layui-input-block">
                                            <input type="text" name="user_version" lay-verify="title" disabled autocomplete="off" class="layui-input disabled" id="user_version" style="width: 50%;">
                                        </div>
                                    </div>
                                </div>

                                <div class="grid-demo grid-demo-bg1">
                                    <div class="layui-form-item">
                                        <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                            <b style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</b>
                                            模板ID :
                                        </label>
                                        <div class="layui-input-block">
                                            <input type="text" name="template_id" lay-verify="title" disabled autocomplete="off" class="layui-input disabled" id="template_id" style="width: 50%;">
                                        </div>
                                    </div>
                                    <div style="display:flex;">
                                        <div class="grid-demo grid-demo-bg1" style="width: 200px;">
                                            <label style="display: block;margin-left: 111px; padding-bottom: 15px;">
                                                <b style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</b>
                                                小程序logo
                                            </label>
                                            <div class="layui-upload up" style="width:100px;height:100px;">
                                                <div
                                                        style="position: absolute;top:-20px;left:105px;border:1px dashed #e2e2e2;width:100px;height:100px">
                                                </div>
                                                <input type="file" name="file" id="uploadFile2" style="position: absolute;top:-20px;left:106px;width:100px;height:100px;" />
                                                <div class="layui-upload-list">
                                                    <img style="width:100%;height:100%;margin-left: 106px;margin-top: -19px;" id="demo6" class="uploadDataImg" src=""/>
                                                    <p id="demoText"></p>
                                                </div>
                                            </div>
                                            <input type="hidden" name="license2" value="" />
                                        </div>
                                        <div class="grid-demo grid-demo-bg1" style="width: 200px;">
                                            <label style="display: block;margin-left: 111px; padding-bottom: 15px;">
                                                <b style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</b>
                                                营业执照
                                            </label>
                                            <div class="layui-upload up" style="width:100px;height:100px;">
                                                <div
                                                        style="position: absolute;top:-20px;left:105px;border:1px dashed #e2e2e2;width:100px;height:100px">
                                                </div>
                                                <input type="file" name="file" id="uploadFile" style="position: absolute;top:-20px;left:106px;width:100px;height:100px;" />
                                                <div class="layui-upload-list">
                                                    <img class="licenseDataImg" id="demo5" style="width:100%;height:100%;margin-left: 106px;margin-top: -19px;" src=""/>
                                                    <p id="demoText"></p>
                                                </div>
                                            </div>
                                            <input type="hidden" name="license" value="" />
                                        </div>
                                    </div>
                                </div>
                                <p
                                        style="display:block;height: 22px;font-size: 14px;font-weight: 400;color: #999999;line-height: 22px;margin-top: -29px;margin-left: 420px;">
                                    小程序Logo和营业执照均支持png、jpeg、jpg格式</p>
                                <div class="layui-form-item">
                                    <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                        商家电话
                                    </label>
                                    <div class="layui-input-block">
                                        <input type="text" name="component_phone" lay-verify="title"
                                               autocomplete="off" placeholder="请输入联系电话" class="layui-input"
                                               style="width: 50%;">
                                    </div>
                                </div>
                                <!-- <div class="layui-form-item">
                                    <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                        <b style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</b>
                                        应用描述
                                    </label>
                                    <div class="layui-input-block">
                                        <input type="text" name="describe" lay-verify="title" autocomplete="off"
                                               placeholder="请输入应用描述" class="layui-input" style="width: 50%;">
                                    </div>
                                </div> -->
                                <div class="layui-form-item">
                                    <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                        <b style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</b>
                                        版本描述
                                    </label>
                                    <div class="layui-input-block">
                                        <input type="text" name="editiondescribe" lay-verify="title" autocomplete="off"
                                               placeholder="请输入版本描述" class="layui-input" style="width: 50%;">
                                    </div>
                                </div>
                                <div style="margin-left:-18px;" class="selectoptioncolor">
                                    <form class="layui-form" method="post" lay-filter="useThisTemplate">
                                        <div class="layui-form-item">
                                            <label class="layui-form-label" style="width:85px;"><span style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>请选择类目</label>
                                            <div class="layui-input-inline">
                                                <select id="first" name="first" lay-filter="first"></select>
                                            </div>
                                            <div class="layui-input-inline">
                                                <select id="second" name="second" lay-filter="second"></select>
                                            </div>
                                        </div>
                                        <div class="ziZhi_show layui-hide">
                                            <div>
                                                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
                                                    <legend>需要上传以下资质</legend>
                                                </fieldset>
                                            </div>
                                            <div class="ziZhi_content"></div>
                                        </div>
                                    </form>
                                </div>
                                <div class="layui-form-item">
                                    <div class="layui-input-block">
                                        <button type="submit" class="layui-btn" lay-submit=""lay-filter="submitAppletInfo" id= "submitAppletInfo"style="background: #1E9FFF;">确定</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
</div>
</div>
<input type="hidden" class="editiondescribe">
</body>

<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script src="<?php echo e(asset('/js/qrcode.min.js')); ?>"></script>

<script type="text/javascript">
    var authorizer_appid = ""
    var refresh_token = ""
    var token = localStorage.getItem("Publictoken");
    var store_id = localStorage.getItem("store_id");
    var cateGory = [];
    var exter_list = [];
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        steps: './extends/steps/steps'
    }).use(['index', 'form', 'upload', 'table', 'laydate', 'steps'], function() {
        var $ = layui.$,
            admin = layui.admin,
            form = layui.form,
            table = layui.table,
            laydate = layui.laydate,
            upload = layui.upload,
            steps = layui.steps

        //步骤条
        layui.use('steps', function() {
            var steps = layui.steps;
            steps.render({
                ele: '#steps',
                data: [
	                {
	                    'title': "第一步",
	                    "desc": "创建小程序"
	                },
                    {
                        'title': "第二步",
                        "desc": "微信认证"
                    },
                    {
                        'title': "第三步",
                        "desc": "追加授权"
                    },
                    {
                        'title': "第四步",
                        "desc": "上传代码"
                    }
                ], //desc 不是必须
                // current: 0 //默认为第几步
            });
            /*
             * 获取第几步的状态
             */
            console.log(store_id)
            $.ajax({
                url: "<?php echo e(url('/api/customer/weixin/getAppletsStatusByStore')); ?>",
                data: {
                    store_id: store_id,
                    type: 1
                },
            	type: 'post',
            	success: function(data) {
            		console.log(data);
            		let stepspage = data.data.created_step;
			        //不等于1的时候才会下一步
			        if(stepspage !== 1){
			            if(stepspage == 2){

			                $("#addwechatapp").css("cssText", "display:none !important;");
                            $("#stepTwo").show();

			            }else if(stepspage == 3){

			                $("#addwechatapp").css("cssText", "display:none !important;");
                            $("#stepTwo").css("cssText", "display:none !important;");
                            $("#stepThree").show();

			            }else if(stepspage == 4){
			            	$("#addwechatapp").css("cssText", "display:none !important;");
                            $("#stepTwo").css("cssText", "display:none !important;");
                            $("#stepThree").css("cssText", "display:none !important;");
                            $("#stepFour").show();
                            //回显数据
                            EchoInfo();

                            /*
                                获取appid
                            */
                            getAppId();
                            function getAppId(){
                                $.ajax({
                                    url: "<?php echo e(url('/api/customer/weixin/getAppletsStatusByStore')); ?>",
                                    data: {
                                        store_id: store_id,
                                        type: 1
                                    },
                                    type: 'post',
                                    success: function(data) {
                                        if(data.status == 200){
                                        window.localStorage.setItem('authorizer_appid',data.data.AuthorizerAppid);
                                        window.localStorage.setItem('refresh_token',data.data.authorizer_refresh_token);
                                        let appId = data.data.AuthorizerAppid;
                                        let token = data.data.authorizer_refresh_token;
                                        // let user_version = data.data.applet_version;
                                        // let template_id = data.data.applet_template_id;
                                        $("#user_version").val(data.data.applet_version);
                                        $("#template_id").val(data.data.applet_template_id);
                                        leimufun(appId,token);
                                        }
                                    }
                                });
                            }
                            /**
                            * 获取可以设置的所有类目
                            **/
                            function leimufun(appId,token){
                                var cateGory = [];
                                var exter_list = [];

                                var authorizer_appid = appId;
                                var refresh_token = token;

                                $.post("<?php echo e(url('/api/customer/weixin/getAllCategories')); ?>",{
                                    authorizer_appid:authorizer_appid,
                                    refresh_token:refresh_token,
                                },function(data){
                                    var status = data.status;
                                    if(status == 200){
                                        cateGory = data.data.categories;
                                        var cateGoryNameList = [];
                                        var cateGoryNameid = [];
                                        var cateGoryName = "<option value=''>请选择</option>";
                                        $.each(cateGory,function(index1,item1){
                                            if(item1['level'] == 1){
                                                cateGoryName+="<option value='"+cateGory[index1].id+"&"+cateGory[index1].name+"'>"+cateGory[index1].name+"</option>"
                                            }
                                            cateGoryNameList.push(cateGory[index1].name);
                                            cateGoryNameid.push(cateGory[index1].id);
                                        });

                                        $('#first').html(cateGoryName);
                                        form.render("select");

                                    }else{
                                        layer.msg(data.message, {
                                            offset: '50px'
                                            ,icon: 2
                                            ,time: 1000
                                        });
                                    }
                                },"json");

                                form.on('select(first)', function(data){
                                    var value = data.value;

                                    let nameList = value.split("&");

                                    let obj = {...nameList};

                                    window.localStorage.setItem('first_class',obj[1]);
                                    window.localStorage.setItem('first_id',obj[0]);


                                    var second = "<option selected value=''>请选择</option>";

                                    $.each(cateGory, function(index, item) {
                                        if(cateGory[index].father == obj[0] && cateGory[index].level == 2){
                                            second += "<option value='"+cateGory[index].id+"&"+cateGory[index].name+"'>"+cateGory[index].name+"</option>"
                                        }
                                    });

                                    $('#second').html(second);
                                    $(".ziZhi_show").addClass("layui-hide");
                                    $(".ziZhi_content").html("");
                                    form.render("select");

                                });

                                form.on('select(second)', function(data){
                                    var value = data.value;

                                    let secondnameList = value.split("&");

                                    let obj = {...secondnameList};

                                    window.localStorage.setItem('second_class',obj[1]);
                                    window.localStorage.setItem('second_id',obj[0]);

                                    if(value){
                                        $(".ziZhi_show").addClass("layui-hide");
                                        $.each(cateGory, function(index, item) {
                                            if(cateGory[index].id == obj[0] && cateGory[index].level == 2){
                                                var cateGory_first = cateGory[index];
                                                if(cateGory_first.sensitive_type == 1){
                                                    $(".ziZhi_show").removeClass("layui-hide");
                                                    exter_list = cateGory_first.qualify.exter_list;
                                                    var txt = "";
                                                    $.each(exter_list,function(index1,item1){
                                                        $.each(exter_list[index1].inner_list,function(index2,item2){
                                                            txt += "<div class=\"layui-form-item\">\n" +
                                                                "                            <label class=\"layui-form-label\">资质名称</label>\n" +
                                                                "                            <div class=\"layui-input-block\">\n" +
                                                                "                                <input type=\"text\" name=\"cerTiCate_key_"+index1+"_"+index2+"\" value=\""+item2.name+"\" placeholder=\"请输入资质名称\" class=\"layui-input\">\n" +
                                                                "                            </div>\n" +
                                                                "                        </div>\n" +
                                                                "                        <div class=\"layui-upload\">\n" +
                                                                "                            <label class=\"layui-form-label\">上传图片</label>\n" +
                                                                "                            <button type=\"button\" class=\"layui-btn up\" id=\"uploadFile1_"+index1+"_"+index2+"\"><input type=\"file\" name=\"file\" id=\"uploadFile_"+index1+"_"+index2+"\" style=\"position: absolute;top: 0;left: 0;\" />上传资质图片</button>\n" +
                                                                "                            <div class=\"layui-upload-list\">\n" +
                                                                "                                <img class=\"layui-upload-img demo5\" id=\"uploadImg_"+index1+"_"+index2+"\">\n" +
                                                                "                                <p id=\"demoText\"></p>\n" +
                                                                "                                <input type=\"hidden\" name=\"uploadImg_"+index1+"_"+index2+"\">\n" +
                                                                "                            </div>\n" +
                                                                "                        </div>";
                                                            uploadFile("#uploadFile_"+index1+"_"+index2,"uploadImg_"+index1+"_"+index2);
                                                        });
                                                    });
                                                    $(".ziZhi_content").html(txt);
                                                    //渲染上传文件
                                                    $.each(exter_list,function(index1,item1){
                                                        $.each(exter_list[index1].inner_list,function(index2,item2){
                                                            uploadFile("#uploadFile_"+index1+"_"+index2,"uploadImg_"+index1+"_"+index2);
                                                        });
                                                    });

                                                    form.render();
                                                }else{
                                                    $(".ziZhi_content").html("");
                                                    form.render();
                                                }
                                            }
                                        });
                                    }
                                });
                            }

			            }else if(stepspage == 5){
                            // $("#addwechatapp").css("cssText", "display:none !important;");
                            // $("#stepTwo").css("cssText", "display:none !important;");
                            // $("#stepThree").css("cssText", "display:none !important;");
                            // $("#stepFour").css("cssText", "display:none !important;");
			            	//查询小程序审核状态
                            $.ajax({
                                url: "<?php echo e(url('/api/customer/weixin/getLatestAuditStatus')); ?>",
                                data: {
                                    authorizerAppId: data.data.AuthorizerAppid,
                                    refreshToken:  data.data.authorizer_refresh_token,
                                },
                                type: 'post',
                                success: function(data) {
                                    if (data.status == 200) {
                                        console.log(data,"999999999999999999999999")
                                        if(data.data.authorizer_appid_status == 0){
                                            layer.open({
                                                type: 2,
                                                title: '详细',
                                                scrollbar: false,
                                                shade: false,
                                                maxmin: true,
                                                area: ['100%', '100%'],
                                                content: "<?php echo e(url('/mb/WeChatshenhesuccess?')); ?>"
                                            });
                                        }else if(data.data.authorizer_appid_status == 1){
                                            layer.open({
                                                type: 2,
                                                title: '详细',
                                                scrollbar: false,
                                                shade: false,
                                                maxmin: true,
                                                area: ['100%', '100%'],
                                                content: "<?php echo e(url('/mb/WeChatshenhefail?')); ?>"
                                            });
                                        }else if(data.data.authorizer_appid_status == 2){
                                            layer.open({
                                                type: 2,
                                                title: '详细',
                                                scrollbar: false,
                                                shade: false,
                                                maxmin: true,
                                                area: ['100%', '100%'],
                                                content: "<?php echo e(url('/mb/WeChatshenheing?')); ?>"
                                            });
                                        }

                                    }
                                }
                            })
                        }else if(stepspage == 6){
			            	//小程序上架成功
                                layer.open({
                                    type: 2,
                                    title: '详细',
                                    scrollbar: false,
                                    shade: false,
                                    maxmin: true,
                                    area: ['100%', '100%'],
                                    content: "<?php echo e(url('/mb/auditSuccess?')); ?>"
                                });
                        }
			            stepspage --;
			            for(let i=0;i<stepspage;i++){
			            	//执行下一步
			                steps.next();
			            }
			        }else{
			            $("#addwechatapp").show();
                        $("#stepTwo").css("cssText", "display:none !important;");
                        $("#stepThree").css("cssText", "display:none !important;");
                        $("#stepFour").css("cssText", "display:none !important;");
			            console.log("默认显示第一步");
			        }
            	}
             })
            /*
             * 第一步 创建小程序
             */
            form.on('submit(addwechat)', function(data) {
                var token = localStorage.getItem("Publictoken");
                var value = data.field;
                if (!value.name) {
                    layer.msg("企业名称不可为空", {
                        offset: '50px',
                        icon: 2,
                        time: 2000
                    });
                    return false;
                }

                if (!value.legal_persona_wechat) {
                    layer.msg("法人微信号不可为空", {
                        offset: '50px',
                        icon: 2,
                        time: 2000
                    });
                    return false;
                }
                if (!value.code) {
                    layer.msg("信用代码不可为空", {
                        offset: '50px',
                        icon: 2,
                        time: 2000
                    });
                    return false;
                }
                if (!value.component_phone) {
                    layer.msg("联系电话不可为空", {
                        offset: '50px',
                        icon: 2,
                        time: 2000
                    });
                    return false;
                }
                if (!value.legal_persona_name) {
                    layer.msg("法人姓名不可为空", {
                        offset: '50px',
                        icon: 2,
                        time: 2000
                    });
                    return false;
                }

                layer.msg('正在请求，请稍后......', {
                    icon: 16,
                    shade: 0.5,
                    time: 0
                });

                $.ajax({
                    url: '/api/customer/weixin/fastRegisterWeApp',
                    data: {
                        store_id: store_id,
                        name: value.name,
                        legal_persona_wechat: value.legal_persona_wechat,
                        code: value.code,
                        component_phone: value.component_phone,
                        legal_persona_name: value.legal_persona_name
                    },
                    type: "post",
                    success: function(data) {
                        if (data.status == 200) {
                            layer.msg("创建成功，请查收法人的微信通知", {
                                offset: '50px',
                                icon: 1,
                                time: 2000
                            });
                            /*
                             * 更新第几步的状态
                             * */
                            $.ajax({
                                url: "<?php echo e(url('/api/customer/weixin/updateAppletsStatus')); ?>",
                                data: {
                                    store_id: store_id,
                                    type: 1,
                                    created_step: 2
                                },
                                type: 'post',
                                success: function(data) {
                                    if (data.status == 200) {
                                        console.log("步骤状态更新成功")
                                    }else{
                                        console.log("步骤状态更新失败")
                                    }
                                }
                            })
                            $("#addwechatapp").css("cssText","display:none !important;");
                            $("#stepTwo").show();
                            steps.next(); //下一步
                            document.getElementById("addwechat").style.display = "none";
                        } else {
                            layer.open({
                                type: 2,
                                title: '详细',
                                scrollbar: false,
                                shade: false,
                                maxmin: true,
                                area: ['100%', '100%'],
                                content: "<?php echo e(url('/mb/auditFailed?')); ?>"
                            });
                        }
                    }
                })
                return false;
            })
            /*
                第二步 微信认证
            */
            $(".authentication").on("click", function() {
                console.log("微信认证")
                $.ajax({
                    url: "<?php echo e(url('/api/customer/weixin/updateAppletsStatus')); ?>",
                    data: {
                        store_id: store_id,
                        type: 1,
                        created_step: 3
                    },
                    type: 'post',
                    success: function(data) {
                        if (data.status == 200) {
                            $("#addwechatapp").css("cssText", "display:none !important;");
                            $("#stepTwo").css("cssText", "display:none !important;");
                            $("#stepThree").show();
                            steps.next(); //下一步

                        }
                    }
                })

            })

            //获取appid


            function getappid(){

                return new Promise((resolve)=>{

                    $.post("/api/customer/weixin/getAppletsStatusByStore",{

                        store_id: store_id,
                        type: 1

                    },function(data){

                        resolve(data)

                    })

                })

            }




            /*
                第三步 追加授权 / 授权
            */
            $(".athorize").on("click", function() {
                getappid().then(v=>{
                    console.log(v.data.component_appid)
                    if(v.status == 200){
                        var component_appid = v.data.component_appid;
                        var redirect_uri = "<?php echo e(env("APP_URL")); ?>" + "/authorize/refundAuthUrl/" +
                            store_id;
                        $.post("/api/customer/weixin/getPreAuthCode", {
                            component_appid: ""
                        }, function(data) {
                            var status = data.status;   
                            if (status == 200) {
                                /*
                                * 更新第几步的状态
                                * */
                                $.ajax({
                                    url: "<?php echo e(url('/api/customer/weixin/updateAppletsStatus')); ?>",
                                    data: {
                                        store_id: store_id,
                                        type: 1,
                                        created_step: 3
                                    },
                                    type: 'post',
                                    success: function(data) {
                                        if (data.status == 200) {
                                            console.log("步骤状态更新成功")
                                        }else{
                                            console.log("步骤状态更新失败")
                                        }
                                    }
                                })
                                var preAuthCode = data.data.pre_auth_code;
                                // 点击小程序授权按钮现在不方便测，等能测的时候在打开
                                // var component_appid = data.data.app_id;
                                layer.msg("请稍后", {
                                    offset: '50px',
                                    icon: 1,
                                    time: 2000
                                }, function() {
                                    //这里是直接跳转到了微信公众号第三方平台
                                    // window.location.href = "https://mp.weixin.qq.com/safe/bindcomponent?action=bindcomponent&auth_type=2&no_scan=1&component_appid="+component_appid+"&pre_auth_code="+preAuthCode+"&redirect_uri="+redirect_uri+"&auth_type=2#wechat_redirect";
                                    window.location.href =
                                        "https://mp.weixin.qq.com/cgi-bin/componentloginpage?component_appid=" +
                                        component_appid +
                                        "&pre_auth_code=" +
                                        preAuthCode +
                                        "&redirect_uri=" +
                                        redirect_uri +
                                        "&auth_type=2";
                                });
                            } else {
                                layer.msg(data.message, {
                                    offset: '50px',
                                    icon: 2,
                                    time: 2000
                                });
                            }
                        }, "json");
                    }
                })
            });
            /*
               第四步 上传代码
            */
            form.on("submit(submitAppletInfo)",function(data){
                var requestData = data.field;
                console.log(requestData,"上传信息")
                var first_class = window.localStorage.getItem('first_class')
                var first_id = window.localStorage.getItem('first_id')
                var second_class = window.localStorage.getItem('second_class')
                var second_id = window.localStorage.getItem('second_id')
                var authorizer_appid = window.localStorage.getItem('authorizer_appid')
                // console.log(first_class,first_id,second_class,second_id,"11111111111111111111111")
                console.log(first_class,first_id,second_class,second_id,authorizer_appid)
                if(!requestData.editiondescribe){
                    layer.msg("版本描述不可为空", {
                        offset: '50px'
                        ,icon: 2
                        ,time: 2000
                    });
                    return false;
                }

                // if(requestData.wechat_name){
                //     if(!requestData.wechat_name){
                //         layer.msg("程序名称不可为空", {
                //             offset: '50px'
                //             ,icon: 2
                //             ,time: 2000
                //         });
                //         return false;
                //     }
                // }
                if(requestData.user_version){
                    if(!requestData.user_version){
                        layer.msg("应用版本不可为空", {
                            offset: '50px'
                            ,icon: 2
                            ,time: 2000
                        });
                        return false;
                    }
                }

                if(!requestData.template_id){
                    layer.msg("模板id不可为空", {
                        offset: '50px'
                        ,icon: 2
                        ,time: 2000
                    });
                    return false;
                }
                layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                $.post("/api/customer/weixin/uploadCode",{
                    store_id:store_id,
                    store_applets_desc:requestData.editiondescribe,
                    nick_name:requestData.wechat_name,
                    template_id:requestData.template_id,
                    user_version:requestData.user_version,
                    license:requestData.license,
                    applet_logo:requestData.license2,
                    first_class:first_class,
                    first_id:first_id,
                    second_class:second_class,
                    second_id:second_id,
                    authorizer_appid:authorizer_appid,
                },function(data){
                    console.log(data)
                    var status = data.status;
                    if(status == 200){
                        layer.msg(data.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 2000
                        });
                        window.location.reload();
                    }else{
                        layer.msg(data.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 2000
                        });
                    }
                },"json");
                return false;
            });
            /*
                数据回显
            */
            $.ajax({
                url: "<?php echo e(url('/api/customer/weixin/getAppletsInfoListByStore')); ?>",
                data: {
                    storeId: store_id,
                    type: 1
                },
                type: 'post',
                success: function(data) {
                    var vul = data.data[0]
                    form.val('example', {
                        "name": vul.name,
                        "code": vul.code,
                        "legal_persona_wechat": vul.legal_persona_wechat,
                        "legal_persona_name": vul.legal_persona_name,
                        "component_phone": vul.component_phone,
                        "desc": vul.store_applets_desc,
                        "wechat_name": vul.nick_name,
                        "like[write]": true, //复选框选中状态
                        "close": true //开关状态
                    });
                    if (vul != "") {
                        // document.getElementById("addwechat").style.display="none";
                    }
                },

                error: function(data) {
                    alert('查找板块报错');
                }
            });
            function EchoInfo(){

                console.log("我是数据回显");

                $.ajax({
                    url: "<?php echo e(url('/api/customer/weixin/getAppletsStatusByStore')); ?>",
                    data: {
                        store_id: store_id,
                        type: 1
                    },
                    type: 'post',
                    success: function(data) {
                       var vul = data.data.applet_info
                       console.log(vul)
                       if(vul.logo && vul.license){
	                       	layui.jquery('#demo5').attr("src", vul.license);
		                    $("#demo5").removeClass("licenseDataImg");
		                    layui.jquery('#demo6').attr("src", vul.logo);
	                       	$("#demo6").removeClass("uploadDataImg");
                       }

                       let first_class = vul.first_class;
                       let second_class = vul.second_class;


                        let setIntervalFun = setInterval(() => {
                            console.log($("#first option").length)
                            if($("#first option").length > 0){
                                $("#first option:first").html(first_class)
                                $("#second").html("<option value=''>"+second_class+"</option>")
                                form.render("select");
                                clearInterval(setIntervalFun);
                            }
                        }, 500);

                       form.val('example', {
                        "wechat_name": vul.nick_name,
                        "component_phone": vul.component_phone,
                        // "describe": vul.store_applets_desc,
                        "store_applets_desc": vul.editiondescribe,
                        "editiondescribe":vul.store_applets_desc,
                        "first":vul.first_class,
                        "second":vul.second_class,
                        "like[write]": true, //复选框选中状态
                        "close": true ,//开关状态
                    	});
                    }
                });
            }
            /**
             * select选择门店事件，
             * 重新选择了门店，就要重新生成一个新的二维码
             **/
            form.on('select(store_id1)', function(data) {
                var value = data.value;
                $("#qrCode").html("");
                aliPayQrCode(value);
            });

            /**
             * 点击图片放大
             */
            $('#photo-list img').on('click', function() {
                layer.photos({
                    photos: '#photo-list',
                    shadeClose: false,
                    closeBtn: 2,
                    anim: 0
                });
            })
            /*
                图片上传
            */
            layui.use('upload', function() {
                var $ = layui.jquery,
                    upload = layui.upload;
                //营业执照上传
                upload.render({
                    elem: '#uploadFile', //绑定元素
                    url: "<?php echo e(url('/api/customer/weixin/uploadFile')); ?>", //上传接口
                    field: "file",
                    method: "post",
                    type: 'images',
                    ext: 'jpg|png|gif',
                    before: function(input) {
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {
                            icon: 16,
                            shade: 0.5,
                            time: 0
                        });
                    },
                    done: function(res) {
                        //上传完毕回调
                        if (res.status == 200) {
                            layer.msg("文件上传成功", {
                                icon: 1,
                                shade: 0.5,
                                time: 1000
                            });
                            layui.jquery('#demo5').attr("src", res.data.path);
                            $("input[name=license]").val(res.data.path);
                            $("#demo5").removeClass("licenseDataImg");
                        } else {
                            layer.msg(res.message, {
                                icon: 2,
                                shade: 0.5,
                                time: 1000
                            });
                        }
                    },
                    error: function(err) {
                        //请求异常回调
                        console.log(err);
                    }
                });
            });
            layui.use('upload', function() {
                var $ = layui.jquery,
                    upload = layui.upload;
                //logo上传
                upload.render({
                    elem: '#uploadFile2', //绑定元素
                    url: "<?php echo e(url('/api/customer/weixin/uploadFile')); ?>", //上传接口
                    field: "file",
                    method: "post",
                    type: 'images',
                    ext: 'jpg|png|gif',
                    before: function(input) {
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {
                            icon: 16,
                            shade: 0.5,
                            time: 0
                        });
                    },
                    done: function(res) {
                        //上传完毕回调
                        if (res.status == 200) {
                            layer.msg("文件上传成功", {
                                icon: 1,
                                shade: 0.5,
                                time: 1000
                            });
                            layui.jquery('#demo6').attr("src", res.data.path);
                            $("input[name=license2]").val(res.data.path);
                            $("#demo6").removeClass("uploadDataImg");
                        } else {
                            layer.msg(res.message, {
                                icon: 2,
                                shade: 0.5,
                                time: 1000
                            });
                        }
                    },
                    error: function(err) {
                        //请求异常回调
                        console.log(err);
                    }
                });
            });
            function uploadFile(element1, element2) {
                layui.use('upload', function() {
                    var $ = layui.jquery,
                        upload = layui.upload;
                    //营业执照上传
                    upload.render({
                        elem: element1, //绑定元素
                        url: "<?php echo e(url('/api/customer/weixin/uploadFile')); ?>", //上传接口
                        field: "file",
                        method: "post",
                        type: 'images',
                        ext: 'jpg|png|gif',
                        before: function(input) {
                            //执行上传前的回调  可以判断文件后缀等等
                            layer.msg('上传中，请稍后......', {
                                icon: 16,
                                shade: 0.5,
                                time: 0
                            });
                        },
                        done: function(res) {
                            //上传完毕回调
                            if (res.status == 200) {
                                layer.msg("文件上传成功", {
                                    icon: 1,
                                    shade: 0.5,
                                    time: 1000
                                });
                                $("#" + element2).attr("src", res.data.path);
                                $("input[name=" + element2 + "]").val(res.data.media_id);
                            } else {
                                layer.msg(res.message, {
                                    icon: 2,
                                    shade: 0.5,
                                    time: 1000
                                });
                            }
                        },
                        error: function(err) {
                            //请求异常回调
                            console.log(err);
                        }
                    });
                });
            }



        });
    });
</script>

</html>