<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>赏金日结算列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style>
        .edit{background-color: #ed9c3a;}
        .shenhe{background-color: #429488;}
        .see{background-color: #7cb717;}
        .tongbu{background-color: #4c9ef8;color:#fff;}
        .cur{color:#009688;}
        .inline{
            display: inline-block;
            margin-right:20px;
        }
        .userbox,.storebox{
            height:200px;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 63px;
            width:298px;
            background-color:#ffffff;
            border: 1px solid #ddd;
        }
        .userbox .list,.storebox .list{
            height:38px;line-height: 38px;cursor:pointer;
            padding-left:10px;
        }
        .userbox .list:hover,.storebox .list:hover{
            background-color:#eeeeee;
        }
        .yname{
            font-size: 13px;
            color: #444;
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12" style="margin-top:0px">

            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12">
                        <div class="layui-card">
                            <div class="layui-card-header">赏金日结算列表</div>

                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">
                                    <!-- 选择业务员 -->
                                    <!-- <div class="layui-form" lay-filter="component-form-group" style="width:190px;margin-left:10px;display: inline-block;">
                                      <div class="layui-form-item">
                                        <div class="layui-input-block" style="margin-left:0">
                                            <select name="agent" id="agent" lay-filter="agent" lay-search>

                                            </select>
                                        </div>
                                      </div>
                                    </div> -->




















                                    <div class="layui-form" lay-filter="component-form-group" style="width:190px;margin-left:10px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <div class="layui-input-block" style="margin-left:0">
                                                <text class="yname">日常任务类型</text>
                                                <select name="statusvalue" id="statusvalue" lay-filter="statusvalue" lay-search>
                                                    <option value="">全部</option>
                                                    <option value="1">开启</option>
                                                    <option value="2">关闭</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- 状态 -->














                                    <!-- 搜索 -->
                                    <div class="layui-form" lay-filter="component-form-group" style="width:100%;display: flex;">

                                        <div class="layui-form" lay-filter="component-form-group" style="width:190px;margin-left:10px;display: inline-block;">
                                            <div class="layui-form-item">
                                                <div class="layui-input-block" style="margin-left:0">
                                                    <text class="yname">服务商名称</text>
                                                    <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入服务商名称" class="layui-input transfer">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="layui-form-item">
                                            <div class="layui-inline">
                                                <button class="layui-btn layuiadmin-btn-list" lay-submit="" lay-filter="LAY-app-contlist-search" style="margin-top:22px;border-radius:5px;margin-bottom: 0;height:36px;line-height: 36px;">
                                                    <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="layui-row layui-col-space15">
                                    <div class="layui-col-md12">
                                        <div class="layui-card" style="background-color: transparent;">
                                            <a class="layui-btn layui-btn-primary" lay-href="<?php echo e(url('/user/addsettlementday')); ?>" style="background-color:#3475c3;border-radius: 5px;border:none;color:#fff;margin-top:25px;display: block;width: 122px;">添加日结算设置</a>
                                        </div>
                                    </div>
                                </div>

                                <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                <!-- 判断状态 -->
                                <script type="text/html" id="statusTap">
                                    {{#  if(d.status == 1){ }}
                                    开启
                                    {{#  } else { }}
                                    关闭
                                    {{#  } }}
                                </script>
                                <script type="text/html" id="company_type">
                                    {{#  if(d.source_type=='12000'){ }}
                                    TF传化
                                    {{#  } else if(d.source_type=='hb') { }}
                                    红包码
                                    {{#  } else if(d.source_type=='3000') { }}
                                    快钱支付
                                    {{#  } else if(d.source_type=='1000') { }}
                                    支付宝
                                    {{#  } else if(d.source_type=='2000') { }}
                                    微信支付
                                    {{#  } else if(d.source_type=='6000') { }}
                                    京东金融
                                    {{#  } else if(d.source_type=='8000') { }}
                                    新大陆
                                    {{#  } else if(d.source_type=='9000') { }}
                                    和融通
                                    {{#  } else if(d.source_type=='6000') { }}
                                    京东金融
                                    {{#  } else if(d.source_type=='15000') { }}
                                    哆啦宝
                                    {{#  } else if(d.source_type=='13000') { }}
                                    随行付
                                    {{#  } else if(d.source_type=='19000') { }}
                                    随行付A
                                    {{#  } else if(d.source_type=='16000') { }}
                                    支付宝-ZFT
                                    {{#  } else if(d.source_type=='16001') { }}
                                    花呗分期-ZFT
                                    {{#  } else if(d.source_type=='1001') { }}
                                    花呗分期
                                    {{#  } else if(d.source_type=='23000') { }}
                                    HL葫芦
                                    {{#  } else { }}
                                    未知
                                    {{#  } }}
                                </script>
                                <!-- 判断状态 -->





                                <script type="text/html" id="table-content-list">
                                    {{#  if(d.status == 2){ }}
                                    <a class="layui-btn layui-btn-danger layui-btn-xs del" lay-event="del">关闭</a>
                                    {{#  } else { }}
                                    <a class="layui-btn layui-btn-normal layui-btn-xs del" lay-event="del">开启</a>
                                    {{#  } }}
                                    <a class="layui-btn layui-btn-normal layui-btn-xs edit" lay-event="edit">修改</a>

                                    
                                </script>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<input type="hidden" class="store_id">
<input type="hidden" class="sort">
<input type="hidden" class="stu_class_no">

<input type="hidden" class="stu_order_batch_no">
<input type="hidden" class="user_id">

<input type="hidden" class="pay_status">
<input type="hidden" class="pay_type">

<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script>
    var token = localStorage.getItem("Usertoken");
    var str=location.search;
    var user_id=str.split('?')[1];

    console.log(user_id);

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate;
        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="<?php echo e(url('/user/login')); ?>";
            }
        })


        var user_name=localStorage.getItem('agentName');

        if(user_id == undefined){

        }else{
            $('.transfer').val(user_name)
        }

        // 选择门店
        $.ajax({
            url : "<?php echo e(url('/api/wallet/source_type')); ?>",
            data : {token:token,l:100},
            type : 'post',
            success : function(data) {
                // console.log(data);
                var optionStr = "";
                for(var i=0;i<data.data.length;i++){

                    optionStr += "<option value='" + data.data[i].source_type + "' > " + data.data[i].source_desc + "</option>";
                }
                $("#schooltype").append('<option value="">全部</option>'+optionStr);
                layui.form.render('select');
            },
            error : function(data) {
                alert('查找板块报错');
            }
        });




        $(".transfer").bind("input propertychange",function(event){
            //  console.log($(this).val())
            user_name = $(this).val()
            if (user_name.length == 0) {
                $('.userbox').html('')
                $('.userbox').hide()
            } else {
                $.post("<?php echo e(url('/api/user/get_sub_users')); ?>",
                    {
                        token:token,
                        user_name:$(this).val(),
                        self:'1'

                    },function(res){
                        // console.log(res);
                        var html="";
                        // console.log(res.t)
                        if(res.t==0){
                            $('.userbox').html('')
                        }else{
                            for(var i=0;i<res.data.length;i++){
                                html+='<div class="list" data='+res.data[i].id+'>'+res.data[i].name+'-'+res.data[i].level_name+'</div>'
                            }
                            $(".userbox").show()
                            $('.userbox').html('')
                            $('.userbox').append(html)
                        }

                    },"json");
            }
        });

        $(".userbox").on("click",".list",function(){

            $('.transfer').val($(this).html())
            $('.js_user_id').val($(this).attr('data'))
            $('.userbox').hide()

            table.reload('test-table-page', {
                where: {
                    user_id:$(this).attr('data')
                }
                ,page: {
                    curr: 1
                }
            });

        })


        // 门店
        $(".inputstore").bind("input propertychange",function(event){
            //   console.log($(this).val())
            store_name = $(this).val()
            if (store_name.length == 0) {
                $('.storebox').html('')
                $('.storebox').hide()
            } else {
                $.post("<?php echo e(url('/api/user/store_lists')); ?>",
                    {
                        token:token,
                        token:token,store_name:$(this).val(),l:100

                    },function(res){
                        console.log(res);
                        var html="";
                        console.log(res.t)
                        if(res.t==0){
                            $('.storebox').html('')
                        }else{
                            for(var i=0;i<res.data.length;i++){
                                html+='<div class="list" data='+res.data[i].store_id+'>'+res.data[i].store_name+'</div>'
                            }
                            $(".storebox").show()
                            $('.storebox').html('')
                            $('.storebox').append(html)
                        }

                    },"json");
            }
        });
        $(".storebox").on("click",".list",function(){

            $('.inputstore').val($(this).html())
            $('.store_id').val($(this).attr('data'))
            $('.storebox').hide()

            table.reload('test-table-page', {
                where: {
                    store_id:$(this).attr('data')
                }
                ,page: {
                    curr: 1
                }
            });

        })

//          $(".inputstore").blur(function(){
//           $('.storebox').hide()
//         });



        // 渲染表格
        table.render({
            elem: '#test-table-page'
            ,url: "<?php echo e(url('/api/wallet/settlementDayQuery')); ?>"
            ,method: 'post'
            ,where:{
                token:token,
                user_id:user_id,
            }
            ,request:{
                pageName: 'p',
                limitName: 'l'
            }
            ,page: true
            ,cellMinWidth: 150
            ,cols: [[
                {field:'name', title: '服务商名称'}
                ,{field:'source_type', title: '返佣来源',toolbar:'#company_type'}
                ,{field:'rate', title: '税点'}
                ,{field:'status', title: '日结开关',toolbar:'#statusTap'}
                ,{field:'created_at',  title: '创建时间'}
                ,{width:200,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
            ]]
            ,response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                ,statusCode: 1 //成功的状态码，默认：0
                ,msgName: 'message' //状态信息的字段名称，默认：msg
                ,countName: 't' //数据总数的字段名称，默认：count
                ,dataName: 'data' //数据列表的字段名称，默认：data
            }
            ,done: function(res, curr, count){
                console.log(res);
                $('.total').html(res.order_data.all_money)
                $('.yijiesuan').html(res.order_data.settlement_money)
                $('.weijiesuan').html(res.order_data.unsettlement_money)
                $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});	//进行表头样式设置
            }

        });



        table.on('tool(test-table-page)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var e = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            console.log(e);
            // localStorage.setItem('s_store_id', e.store_id);

            if(layEvent === 'see'){ //审核
                layer.open({
                    type: 2,
                    title: '查看',
                    shade: false,
                    maxmin: true,
                    area: ['70%', '80%'],
                    content: "<?php echo e(url('/user/seewater?')); ?>"+e.out_trade_no
                });
            }else if(layEvent === 'edit'){

                $('.edit').attr('lay-href',"<?php echo e(url('/user/editsetday?')); ?>"+e.id+"&status="+e.status);


            }else if(layEvent === 'del'){
                if(e.status==1){ //开启状态需关停参数传2
                    // $(".obj.tr").css('display', 'none')
                    layer.confirm('确认关闭',{icon: 7}, function(index){
                        $.ajax({
                            url : "<?php echo e(url('/api/wallet/changeDaily')); ?>",
                            data : {token:token,id:e.id,status:2},
                            type : 'post',
                            dataType:'json',
                            success : function(data) {
                                //                                console.log(data);
                                if(data.status==1){
                                    layer.close(index);
                                    layer.msg(data.message, {
                                        offset: '50px'
                                        ,icon: 1
                                        ,time: 1000
                                    });
                                }else{
                                    layer.msg(data.message, {
                                        offset: '50px'
                                        ,icon: 2
                                        ,time: 3000
                                    });
                                }
                            }
                        });
                    });
                }else{ //关停状态需开启参数传2
                    layer.confirm('确认开启',{icon: 7}, function(index){
                        $.ajax({
                            url : "<?php echo e(url('/api/wallet/changeDaily')); ?>",
                            data : {
                                token:token
                                ,id:e.id
                                ,status:1
                            },
                            type : 'post',
                            dataType:'json',
                            success : function(data) {
                                //                                console.log(data);
                                if(data.status==1){
                                    layer.close(index);
                                    layer.msg(data.message, {
                                        offset: '50px'
                                        ,icon: 1
                                        ,time: 1000
                                    });
                                }else{
                                    layer.msg(data.message, {
                                        offset: '50px'
                                        ,icon: 2
                                        ,time: 3000
                                    });
                                }
                            }
                        });
                    });
                }
            }

            var data = obj.data;
            if(obj.event === 'setSign'){
                layer.open({
                    type: 2,
                    title: '模板详细',
                    shade: false,
                    maxmin: true,
                    area: ['60%', '70%'],
                    content: "<?php echo e(url('/merchantpc/paydetail?')); ?>"+e.stu_order_type_no
                });
            }
        });


        // 选择赏金来源
        form.on('select(schooltype)', function(data){
            var source_type = data.value;
            $('.store_id').val(source_type);
            //执行重载
            table.reload('test-table-page', {
                where: {
                    source_type: source_type
                }
                ,page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });

        form.on('select(statusvalue)', function(data){
            var status = data.value;
            //执行重载
            table.reload('test-table-page', {
                where: {
                    status: status
                }
                ,page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });


        // 选择业务员
        form.on('select(agent)', function(data){
            var user_id = data.value;
            $('.user_id').val(user_id);
            //执行重载
            table.reload('test-table-page', {
                where: {
                    user_id: $(".user_id").val(),
                }
                ,page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });
        // 选择赏金状态
        form.on('select(sstatus)', function(data){
            var status = data.value;
            $('.pay_status').val(pay_status);
            //执行重载
            table.reload('test-table-page', {
                where: {
                    status:status,
                }
                ,page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });
        // 选择支付类型
        form.on('select(type)', function(data){
            var pay_type = data.value;
            $('.pay_type').val(pay_type);
            //执行重载
            table.reload('test-table-page', {
                where: {
                    ways_source:pay_type
                }
                ,page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });
        // 时间++++++++++++++++++++++++++++++++++++++++++++++++
        laydate.render({
            elem: '.start-item'
            ,type: 'datetime'
            ,done: function(value){
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start:value,
                        time_end:$('.end-item').val()
                    }
                    ,page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        laydate.render({
            elem: '.end-item'
            ,type: 'datetime'
            ,done: function(value){
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start:$('.start-item').val(),
                        time_end:value
                    }
                    ,page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        form.on('submit(LAY-app-contlist-search)', function(data){
            var obj = data.field
            console.log(obj)
            var name = data.field.schoolname;
            console.log(data);
            //执行重载
            table.reload('test-table-page', {
                page: {
                    curr: 1
                },
                where: {
                    name:name,
                }
            });
        });


    });

</script>

</body>
</html>





