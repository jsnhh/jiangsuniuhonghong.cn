<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>支付宝小程序模板消息</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
        <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
        <style type="text/css">
            .xgrate{color: #fff;font-size: 15px;padding: 7px;height: 30px;line-height: 30px;background-color: #3475c3;}
            .up #uploadFile{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
            #demo5{width: 200px;}
        </style>
    </head>
    <body>

        <div class="layui-fluid">
            <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
                <ul class="layui-tab-title">
                    <li class="layui-this">小程序模板消息</li>
                    <li>添加小程序模板</li>
                </ul>
                <div class="layui-tab-content" style="height: 100px;">
                    <div class="layui-tab-item layui-show">
                        <div class="layui-row layui-col-space15">
                            <div class="layui-col-md12">
                                <div class="layui-fluid">
                                    <div class="layui-row layui-col-space15">
                                        <div class="layui-col-md12" style="margin-top:0px">
                                            <div class="layui-card">
                                                <div class="layui-card-header">
                                                    <div class="layui-row">
                                                        <div class="layui-col-md6">第三方平台小程序模板消息</div>
                                                    </div>
                                                </div>
                                                <div class="layui-card-body">
                                                    <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                                    <script type="text/html" id="table-content-list">
                                                        <a class="layui-btn layui-btn-xs" lay-event="updateTemplateInfo">修改</a>
                                                    </script>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-tab-item">
                        <form class="layui-form" lay-filter="templateInfo">
                            <div class="layui-form-item">
                                <label class="layui-form-label">小程序模板id</label>
                                <div class="layui-input-block">
                                    <input type="text" name="applet_id" placeholder="小程序模板id" value="2021002108609082" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">模板标题</label>
                                <div class="layui-input-block">
                                    <input type="text" name="title" placeholder="请输入模板标题" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">模板id</label>
                                <div class="layui-input-block">
                                    <input type="text" name="template_id" placeholder="请输入模板id" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">模板关键词</label>
                                <div class="layui-input-block">
                                    <input type="text" name="template_keyword" placeholder="请输入模板关键词" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button type="button" class="layui-btn layui-btn-normal" lay-submit="" lay-filter="submitTemplateInfo">确定</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div id="useThisTemplateIndex" class="hide layui-form" lay-filter="useThisTemplateIndex" style="display: none;background-color: #fff;">
            <div class="xgrate">修改模板消息</div>
            <div class="layui-card-body" style="padding: 15px;">
                <form class="layui-form" lay-filter="updateTemplateInfo">
                    <div class="layui-form-item">
                        <label class="layui-form-label">小程序模板id</label>
                        <div class="layui-input-block">
                            <input type="text" name="applet_id" placeholder="小程序模板id" value="2021002108609082" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">模板标题</label>
                        <div class="layui-input-block">
                            <input type="text" name="title" placeholder="请输入模板标题" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">模板id</label>
                        <div class="layui-input-block">
                            <input type="text" name="template_id" placeholder="请输入模板id" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">模板关键词</label>
                        <div class="layui-input-block">
                            <input type="text" name="template_keyword" placeholder="请输入模板关键词" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-block">
                            <input type="checkbox" checked="" name="status" lay-skin="switch" lay-filter="switchTest" lay-text="使用|关闭">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button type="button" class="layui-btn layui-btn-normal" lay-submit="" lay-filter="submitUpdateTemplateInfo">确定</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </body>
    <script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
    <script type="text/javascript">
        var token = localStorage.getItem("Usertoken");
        layui.config({
            base: '../../layuiadmin/' //静态资源所在路径
        }).extend({
            index: 'lib/index' //主入口模块
        }).use(['index','form', 'upload','table','laydate'], function(){
            var $ = layui.$
                ,admin = layui.admin
                ,table = layui.table
                ,form = layui.form
                ,upload = layui.upload
                ,laydate = layui.laydate;

            // 未登录,跳转登录页面
            $(document).ready(function(){
                if(token==null){
                    window.location.href="<?php echo e(url('/user/login')); ?>";
                }
            });

            /**
             * 进入该页面，初始化渲染该表格
             */
            layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
            table.render({
                elem: '#test-table-page',
                url: "<?php echo e(url('/api/customer/aliPay/getTemplateListData')); ?>",
                method: 'post',
                where:{

                },
                request:{
                    pageName: 'page',
                    limitName: 'count'
                },
                page: true,
                cellMinWidth: 100,
                cols: [
                    [
                        {width:200,field:'applet_id', title: '小程序模板id',templet: '#appletsId'},
                        {width:200,field:'title',  title: '模板标题'},
                        {width:300,field:'template_id',  title: '模板id'},
                        {width:400,field:'template_keyword',  title: '模板关键词'},
                        {width:200,field:'status',  title: '状态', templet: function (d) {
                            return d.status == 1 ? "使用中":"停止使用";
                        }},
                        {width:350,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
                    ]
                ],
                response: {
                    statusName: 'status', //数据状态的字段名称，默认：code
                    statusCode: 200, //成功的状态码，默认：0
                    msgName: 'message', //状态信息的字段名称，默认：msg
                    countName: 'total_count', //数据总数的字段名称，默认：count
                    dataName: 'data', //数据列表的字段名称，默认：data
                },
                done: function(res, curr, count){
                    layer.msg("已完成", {
                        offset: '50px'
                        ,icon: 1
                        ,time: 1000
                    });
                    //进行表头样式设置
                    $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});
                }
            });

            /**
             * 表格的操作列中的每个操作项
             */
            table.on('tool(test-table-page)', function(obj){
                var lineData = obj.data; //获得当前行数据
                var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
                var tr = obj.tr; //获得当前行 tr 的DOM对象
                if(layEvent === 'updateTemplateInfo'){
                    //修改模板消息
                    var useThisTemplateIndex = layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#useThisTemplateIndex')
                    });
                    form.val('updateTemplateInfo', {
                        "applet_id": lineData.applet_id,
                        "title": lineData.title,
                        "template_id": lineData.template_id,
                        "template_keyword": lineData.template_keyword,
                        "status": lineData.status == 1 ? true : false,
                    });

                    /**
                     * 点击 确定 提交小程序模板信息
                     */
                    form.on("submit(submitUpdateTemplateInfo)",function(data){
                        var value = data.field;
                        if(value.status){
                            if(value.status == "on"){
                                value.status = 1;
                            }else{
                                value.status = 2;
                            }
                        }else{
                            value.status = 2;
                        }
                        value.id = lineData.id;
                        layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                        $.post("<?php echo e(url('/api/customer/aliPay/updateTemplateListData')); ?>",value,function(data){
                            var status = data.status;
                            if(status == 200){
                                layer.msg(data.message, {
                                    offset: '50px'
                                    ,icon: 1
                                    ,time: 1000
                                },function(){
                                    window.location.reload();
                                });
                            }else{
                                layer.msg(data.message, {
                                    offset: '50px'
                                    ,icon: 2
                                    ,time: 1000
                                });
                            }
                        });
                    });
                }
            });

            /**
             * 点击 确定 提交小程序模板信息
             */
            form.on("submit(submitTemplateInfo)",function(data){
                var value = data.field;
                layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                $.post("<?php echo e(url('/api/customer/aliPay/addTemplateListData')); ?>",value,function(data){
                    var status = data.status;
                    if(status == 200){
                        layer.msg(data.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 1000
                        },function(){
                            window.location.reload();
                        });
                    }else{
                        layer.msg(data.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 1000
                        });
                    }
                });
            });
        });
    </script>
</html>