<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="renderer" content="webkit">
    <meta name="imagemode" content="force">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>支付宝付款</title>
    <link rel="stylesheet" href="<?php echo e(asset('/payviews/css/style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/phone/css/swiper.min.css')); ?>">
    <script type="text/javascript" src="<?php echo e(asset('/phone/js/Screen.js')); ?>"></script>
    <style>
        .swiper-container {
            width: 100%;
            height: 1.85rem;
            margin-top: .2rem;
        }

        .swiper-container .swiper-wrapper .swiper-slide img {
            width: 100%;
        }

        /*遮罩*/
        .popup_bg {
            width: 100%;
            height: 100%;
            background: #000;
            opacity: .6;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 1200
        }

        .result_layer {
            width: 5.6rem;
            height: 3rem;
            z-index: 1300;
            position: fixed;
            border-radius: .05rem;
            background: #fff;
            top: 50%;
            left: 50%;
            margin: -1.5rem auto auto -2.8rem;
            text-align: center
        }

        .result_layer div {
            float: left;
            width: 100%;
            box-sizing: border-box;
            vertical-align: middle;
        }

        .result_layer div p {
            font-size: .3rem;
            font-weight: 500;
            word-wrap: break-word;
        }

        .result_layer p b {
            color: #f30;
            font-size: .42rem;
            font-family: serif;
            margin: 0.05rem
        }

        .result_layer a {
            float: left;
            width: 50%;
            font: .32rem/.8rem "微软雅黑";
            color: #333;
            border-top: 1px solid #ccc
        }

        .result_layer a:last-child {
            color: #f30;
            border-left: 1px solid #ccc;
            box-sizing: border-box
        }

        .result_layer a.confirm {
            border-left: none;
            width: 100%
        }

        .payment {
            padding-bottom: .3rem;
        }

        .payment .title {
            text-align: left;
            padding: .4rem 0 .3rem .3rem;
            width: 90%;
        }

        .payment .title span {
            display: inline-block;
            float: right;
            color: #108ee9;
        }

        .payment .title img {
            width: .6rem;
            height: .6rem;
        }

        .pay_money {
            height: 1.2rem;
            line-height: 1.2rem;
        }

        .pay_type {
            background: #fff;
            overflow: hidden;
            margin-top: .2rem;
        }

        .pay_type .line_x {
            border-bottom: .01rem solid #ddd;
            margin-left: .3rem;
            height: 0.9rem;
            line-height: 0.9rem;
        }

        .pay_type .line_x img:nth-child(1) {
            width: .50rem;
            height: .50rem;
            padding-top: .2rem;
            float: left;
            padding-right: .2rem;
        }

        .pay_type .line_x label {
            display: inline-block;
            height: 0.9rem;
            line-height: 0.9rem;

        }

        .pay_type .line_x img:nth-child(3) {
            width: .32rem;
            height: .32rem;
            float: right;
            padding: .29rem .3rem;
        }

        .pay_type .line_x span a {
            color: #108ee9;
            float: right;
            padding-right: .3rem;
        }

        .xuanze {
            width: .32rem;
            height: .32rem;
            float: right;
            padding: .29rem .3rem;
        }

        .tip label {
            color: #ccc;
        }

        .keyboard {
            height: 33%;
        }

        .keyboard ul li {
            height: 25%;
        }

        .keyboard ul li:after {
            bottom: 15%;
        }

        #shijipay {
            font-size: .26rem;
            color: #fff;
            position: absolute;
            bottom: 6.5%;
            left: 82%;
            z-index: 999;
        }

        i {
            font-style: normal;
        }

        .result_layer {
            background: #dedede;
            border-radius: .2rem;
        }

        .result_layer .remark {
            margin: 0;
        }

        .remark textarea {
            height: .6rem;
            padding-top: .2rem;
            text-indent: .2rem;
        }

        .danwei {
            display: none
        }
    </style>
</head>
<body>

<input type="hidden" value="<?php echo e($data['dk_jf']); ?>" id="dk_jf">
<input type="hidden" value="<?php echo e($data['dk_money']); ?>" id="dk_money">
<input type="hidden" value="<?php echo e($data['mb_jf']); ?>" id="mb_jf">
<input type="hidden" value="<?php echo e($data['mb_id']); ?>" id="mb_id">
<input type="hidden" value="<?php echo e($data['mb_money']); ?>" id="mb_money">
<input type="hidden" value="<?php echo e($data['store_id']); ?>" id="store_id">
<input type="hidden" value="<?php echo e($data['open_id']); ?>" id="open_id">
<input type="hidden" value="<?php echo e($data['merchant_id']); ?>" id="merchant_id">

<input type="hidden" value="<?php echo e($data['ways_type']); ?>" id="ways_type">
<input type="hidden" value="<?php echo e($data['ways_source']); ?>" id="ways_source">
<input type="hidden" value="<?php echo e($data['company']); ?>" id="company">

<input type="hidden" value="<?php echo e($data['is_info']); ?>" id="is_info">

<input type="hidden" id="token" value="<?php echo e(csrf_token()); ?>">


<div class="payment">
    <div class="title">
        <img src="<?php echo e(url('/payviews/img/touxiang.png')); ?>"><?php echo e($data['store_name']); ?>

        <span class="beizhu">添加备注</span>
    </div>


    <div class="pay_money">
        <span>支付金额</span>
        <i></i>
        <div class="ipt"> ￥ <span id="price"></span></div>
    </div>

</div>

<div class="pay_type" id='pay_type'>
    <div class="line_x">
        <img class="paysimg" src="<?php echo e(url('/zhifu/img/weixin-logo.png')); ?>">
        <label class="paysname">微信支付</label>
        <img class="pay_type_btn" data='2' src="<?php echo e(url('/zhifu/img/selected.png')); ?>">
    </div>
    <div class="line_x">
        <img src="<?php echo e(url('/zhifu/img/vip-logo.png')); ?>">
        <label>会员卡支付</label>
        <span class="to_cz" id='to_cz'><a class="weibuqi">去充值</a></span>
        <img class="pay_type_btn xuanze hykpay" data='1' src="<?php echo e(url('/zhifu/img/NO-selected.png')); ?>"
             style='display: none'>
    </div>
    <div class="line_x hykpay">
        <label style='color:#999'>会员卡余额:<i class="mb_money"></i>元</label>
        <span class="to_czs" id='to_cz'><a class="weibuqi">去充值</a></span>
    </div>
</div>
<!-- 没有输入金额的时候,显示 -->
<div class="pay_type tip" style='display: block'>
    <div class="line_x">
        <label>消费金额大于1元可使用积分抵扣</label>
        <img class="xuanze" src="<?php echo e(url('/zhifu/img/NO-selected.png')); ?>">
    </div>
</div>

<!-- 优惠活动选项 -->
<div class="pay_type yhhd" style='display: none' id='jf_hd'>
    <div class="line_x">
        <label>可用<i class="yong"></i>积分抵扣<i class="di"></i>元</label>
        <img class="xuanze" src="<?php echo e(url('/zhifu/img/NO-selected.png')); ?>">
    </div>

</div>


<div id="shijipay"></div>
<div class="keyboard">
    <ul>
        <li data="1"></li>
        <li data="2"></li>
        <li data="3"></li>
        <li class="del"><img src="<?php echo e(url('/payviews/img/tui.png')); ?>"></li>
        <!-- <li class="xyk_pay" data="信用卡分期"></li> -->
        <li class="confirm" id="payLogButton" data="支付"></li>
        <li data="4"></li>
        <li data="5"></li>
        <li data="6"></li>
        <li data="7"></li>
        <li data="8"></li>
        <li data="9"></li>
        <li data="0"></li>
        <li class="disable_li"></li>
        <li data="."></li>

    </ul>
</div>
<input type="hidden" value="<?php echo e($data['store_id']); ?>" id="store_id">
<input type="hidden" value="<?php echo e($data['open_id']); ?>" id="open_id">
<input type="hidden" value="<?php echo e($data['merchant_id']); ?>" id="merchant_id">
<input type="hidden" id="token" value="<?php echo e(csrf_token()); ?>">
<div class="load-hidden"></div>
<div class="load-hiddenbg" style="display: none"></div>


<!-- 弹框 -->
<div class="show" style="display: none">
    <div class="popup_bg"></div>
    <div class="result_layer" id="accountMsg">
        <div style="padding: .48rem 0;">
            <div class="remark">
                <textarea class="remark_con" id="remark" placeholder="最多可输入30个字"></textarea>
            </div>
        </div>
        <a class="qd" style="width:100%;border-left:1px solid #ccc;color:#219aff">我知道了</a>
    </div>
</div>


<input type="hidden" id="js_pay_money" value="">
<input type="hidden" id="js_is_member_pay" value="2">
<input type="hidden" id="js_is_jf_dk" value="2">

<script type="text/javascript" src="<?php echo e(asset('/phone/js/jquery-2.1.4.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('/phone/js/swiper.jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('/phone/js/fastclick.js')); ?>"></script>
<script>
    window.addEventListener("load", function () {
        FastClick.attach(document.body);
    }, false);
    document.documentElement.addEventListener('dblclick', function (e) {
        e.preventDefault();
    });

    document.addEventListener('touchmove', function (e) {
        e.preventDefault()
    }, false);


</script>
<script>
    $('.keyboard ul li').on("touchend", function () {
        var _this = $(this);
        var num = _this.attr('data');
        var $money = $("#price");
        var oldValue = $money.html();
        var newValue = "";


        if (_this.hasClass('del')) {
            newValue = oldValue.substring(0, oldValue.length - 1);
            if (newValue == "") {
                newValue = "";
                $('.confirm').removeClass('color');

                $('#shijipay').html(' ')
                $('#jf_hd').hide()
                $('.tip').hide()


                if ($('#dk_jf').val() == '' || $('#dk_jf').val() == 0) {
                    $('.yhhd').hide()
                    $('.tip').hide()
                } else {
                    console.log('000')
                    $('.tip').hide()
                    $('.yhhd').hide()
                    $('.yong').html($('#dk_jf').val())
                    $('.di').html($('#dk_money').val())
                }

            } else {
                if ($('#dk_money').val() == '') {
                    $('#shijipay').html(newValue)
                    $('#js_pay_money').val(newValue)

                } else {


                    var img = $('#jf_hd .xuanze').attr('src')
                    if (img == "<?php echo e(url('/zhifu/img/NO-selected.png')); ?>") {
                        $('#shijipay').html(newValue + '元')
                        $('#js_pay_money').val(newValue)
                    } else {
                        var dk_money = parseInt($('#dk_money').val())
                        var currentmoney = (parseFloat(newValue) - parseFloat($('#dk_money').val())).toFixed(2)

                        console.log(currentmoney)
                        if (newValue > dk_money) {
                            $('#shijipay').html(currentmoney + '元')
                            $('#js_pay_money').val(currentmoney)
                        } else {
                            $('#shijipay').html('0元')
                            $('#js_pay_money').val('0')
                        }
                    }


                }
            }

            $money.html(newValue);


        }
        else {
            console.log(oldValue)
            if (oldValue == "0.00") {
                if (num != ".") {
                    oldValue = "";
                }
                $('.confirm').removeClass('color');
                $('#shijipay').html(' ')
                $('.yhhd').hide()
                $('.tip').hide()
            }
            if (oldValue == "0") {
                if (num != ".") {
                    oldValue = "";
                }

                $('.confirm').removeClass('color');
                $('#shijipay').html('')
                $('.yhhd').hide()
                $('.tip').hide()
            }
            if (oldValue == "") {
                if (num == ".") {
                    oldValue = "0.";
                }

                $('.confirm').removeClass('color');
                $('#shijipay').html('')
                $('.yhhd').hide()
                $('.tip').hide()

            }
            newValue = oldValue + num;

            //控制输入的值为五位数和2位小数点
            reg = /^\d{0,5}(\.\d{0,2})?$/g;

            if (reg.test(newValue)) {
                $money.html(newValue);
                if (newValue == "" || newValue == "0" || newValue == "0." || newValue == "0.0" || newValue == "0.00") {
                    $('#payLogButton').removeClass('color');
                    $('#shijipay').html(' ')
                    $('#jf_hd').hide()
                } else {
                    // $('.tip').hide()
                    $('.confirm').addClass('color');
                    // $('#jf_hd').show()
                    if ($('#dk_jf').val() == '' || $('#dk_jf').val() == 0) {
                        $('.yhhd').hide()
                        $('.tip').hide()
                    } else {
                        $('.yhhd').show()
                        $('.yong').html($('#dk_jf').val())
                        $('.di').html($('#dk_money').val())
                    }


                    var img = $('#jf_hd .xuanze').attr('src')
                    if (img == "<?php echo e(url('/zhifu/img/NO-selected.png')); ?>") {
                        $('#shijipay').html(newValue + '元')
                        $('#js_pay_money').val(newValue)
                    } else {
                        var dk_money = parseInt($('#dk_money').val())
                        var currentmoney = (parseFloat(newValue) - parseFloat($('#dk_money').val())).toFixed(2)

                        if (newValue > dk_money) {
                            $('#shijipay').html(currentmoney + '元')
                            $('#js_pay_money').val(currentmoney)
                        } else {
                            $('#shijipay').html('0元')
                            $('#js_pay_money').val('0')
                        }
                    }
                }


            } else {
                $money.html(oldValue);
                if (oldValue == "" || oldValue == "0" || oldValue == "0." || oldValue == "0.0" || oldValue == "0.00") {
                    $('#payLogButton').removeClass('color');
                    $('#shijipay').html(' ')
                    $('.yhhd').hide()


                } else {
                    // $('.tip').hide()
                    $('.confirm').addClass('color');
                    // $('.yhhd').show()

                    if ($('#dk_jf').val() == '' || $('#dk_jf').val() == 0) {
                        $('.yhhd').hide()
                        $('.tip').hide()
                    } else {
                        $('.yhhd').show()
                        $('.yong').html($('#dk_jf').val())
                        $('.di').html($('#dk_money').val())
                    }


                    oldValue = oldValue.substring(0, 5)


                    var img = $('#jf_hd .xuanze').attr('src')
                    if (img == "<?php echo e(url('/zhifu/img/NO-selected.png')); ?>") {
                        $('#shijipay').html(oldValue + '元')
                        $('#js_pay_money').val(oldValue)
                    } else {
                        var dk_money = parseInt($('#dk_money').val())
                        var currentmoney = (parseFloat(oldValue) - parseFloat($('#dk_money').val())).toFixed(2)

                        if (oldValue > dk_money) {
                            $('#shijipay').html(currentmoney + '元')
                            $('#js_pay_money').val(currentmoney)
                        } else {
                            $('#shijipay').html('0元')
                            $('#js_pay_money').val('0')
                        }
                    }
                }


            }

        }
    });


    $(document).ready(function () {
        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            paginationClickable: true
        });
    });
    $(".remark_con").focus(function () {
        $('.keyboard').hide();
    });
    $('.pay_money').click(function () {
        $('.keyboard').show();
    });
    $(".remark_con").bind("input propertychange", function () {

        var len = $(this).val().length;
        var max = 30;

        if (len > max) {
            var value = $(this).val().substring(0, max);
            $(this).val(value);
        }

    });
    $('.quxiao').click(function () {
        $('.remark_con').val('');
    });
    $('.remark_con').blur(function () {
        $('.keyboard').show();
    });

    $('#pay_type .line_x').click(function(){
        $('#pay_type .line_x').find('.pay_type_btn').attr('src', "<?php echo e(url('/zhifu/img/NO-selected.png')); ?>")
        $(this).find('.pay_type_btn').attr('src', "<?php echo e(url('/zhifu/img/selected.png')); ?>")

        console.log($(this).find('.pay_type_btn').attr('data'))


        if ($(this).find('img.pay_type_btn').attr('data') == 1) {
            if ($('#mb_money').val() == 0) {
                $('#pay_type .line_x .pay_type_btn').attr('src', "<?php echo e(url('/zhifu/img/selected.png')); ?>")
                $(this).find('img.pay_type_btn').attr('src', "<?php echo e(url('/zhifu/img/NO-selected.png')); ?>")

            } else {
                
            }
        }

        $('#js_is_member_pay').val($(this).find('.pay_type_btn').attr('data'))

    });

    $(document).ready(function () {
        if ($('#mb_id').val() == '') {
            $('.hykpay').hide()
            $('.to_cz').show()
        } else {
            $('.hykpay').show()
            $('.to_cz').hide()
            $('.mb_money').html($('#mb_money').val())
        }
        // console.log($('#dk_jf').val())
        if ($('#dk_jf').val() == '' || $('#dk_jf').val() == 0) {
            $('.yhhd').hide()
            $('.tip').hide()
        } else {
            $('.yhhd').hide()
            $('.yong').html($('#dk_jf').val())
            $('.di').html($('#dk_money').val())
        }

        if ($('#ways_source').val() == 'alipay') {
            $('.paysimg').attr('src', "<?php echo e(url('/zhifu/img/zhifubao-logo.png')); ?>")
            $('.paysname').html('支付宝支付')
        } else if ($('#ways_source').val() == 'weixin') {
            $('.paysimg').attr('src', "<?php echo e(url('/zhifu/img/weixin-logo.png')); ?>")
            $('.paysname').html('微信支付')
        }


    });


    $('.beizhu').click(function () {
        $('.show').show()
    });

    $('#jf_hd').click(function () {
        var a = $(this).find('.xuanze').attr('src')
        if (a == "<?php echo e(url('/zhifu/img/NO-selected.png')); ?>") {
            $(this).find('.xuanze').attr('src', "<?php echo e(url('/zhifu/img/selected.png')); ?>")
            $('#js_is_jf_dk').val('1')
            if ($('#dk_money').val() == '') {

            } else {
                var dk_money = parseInt($('#dk_money').val())
                var jinetotal = $('#price').html()
                var currentmoney = (parseFloat(jinetotal) - parseFloat($('#dk_money').val())).toFixed(2)
                if (jinetotal > dk_money) {
                    $('#shijipay').html(currentmoney + '元')
                    $('#js_pay_money').val(currentmoney)
                } else {
                    $('#shijipay').html('0元')
                    $('#js_pay_money').val('0')
                }

            }
        } else {
            $(this).find('.xuanze').attr('src', "<?php echo e(url('/zhifu/img/NO-selected.png')); ?>")
            $('#shijipay').html($('#price').html() + '元')
            $('#js_pay_money').val($('#price').html())
            $('#js_is_jf_dk').val('2')
        }

    })


</script>
<script>

    var times = 1;
    $("#payLogButton").click(function () {

        // console.log($('#price').html())
        if ($('#price').html() === '' || $('#price').html() === '0' || $('#price').html() === '0.0' || $('#price').html() === '0.00' || $('#price').html() === '0.') {
            $(this).removeClass('color')
            $('#shijipay').html('')
            $('.yhhd').hide()
        } else {
            $('.load-hidden').addClass('loading');
            $('.load-hiddenbg').show();
            $.post("<?php echo e(url('/api/member/qr_pay_submit')); ?>", {
                store_id: $("#store_id").val(),
                merchant_id: $("#merchant_id").val(),
                total_amount: $("#price").html(),
                pay_amount: $("#js_pay_money").val(),
                is_jf_dk: $('#js_is_jf_dk').val(),
                dk_jf: $("#dk_jf").val(),
                dk_money: $("#dk_money").val(),
                is_member_pay: $('#js_is_member_pay').val(),
                remark: $("#remark").val(),
                open_id: $("#open_id").val(),
                ways_type: $("#ways_type").val(),
                ways_source: $('#ways_source').val(),
                mb_id: $('#mb_id').val()
            }, function (data) {
                $('.load-hidden').removeClass('loading');
                $('.load-hiddenbg').hide();

                var data_url = "&total_amount=" + $("#price").html() + "&store_id=" + $("#store_id").val();
                //官方支付宝
                if (data.status == 1) {
                    var jf = "";
                    if (data.end_dk_jf > 0) {
                        jf = "(" + data.end_dk_jf + "积分抵扣" + data.end_dk_money + ")";
                    }
                    //正常支付
                    if (data.is_member_pay == "2") {

                        //支付或者支付+积分
                        if (data.pay_amount > 0) {
                            AlipayJSBridge.call("tradePay", {
                                tradeNO: data.data.trade_no
                            }, function (result) {
                                //付款成功
                                if (result.resultCode == "9000") {
                                    data_url = data_url + "&ad_p_id=1";

                                    window.location.href = "<?php echo e(url('page/pay_success?message=支付成功')); ?>" + jf + data_url;
                                }
                                if (result.resultCode == "6001") {
                                    data_url = data_url + "&ad_p_id=3";
                                    window.location.href = "<?php echo e(url('page/pay_errors?message=取消支付')); ?>" + data_url;
                                }
                            });
                        } else {

                            //积分支付
                            data_url = data_url + "&ad_p_id=1&no_ad=1";
                            window.location.href = "<?php echo e(url('page/pay_success?message=支付成功')); ?>" + jf + data_url;


                        }

                    } else {
                        //会员卡支付
                        data_url = data_url + "&ad_p_id=1&no_ad=1";
                        window.location.href = "<?php echo e(url('page/pay_success?message=会员卡扣费成功')); ?>" + jf + data_url;
                    }

                } else {
                    data_url = data_url + "&ad_p_id=3";
                    window.location.href = "<?php echo e(url('page/pay_errors?message=')); ?>" + data.message + data_url;
                }


            }, "json");
        }

    });


    $('.qd').click(function () {
        $('.show').hide();
        localStorage.setItem("show", '1');
    });
    var show = localStorage.getItem("show");
    if (show == 1) {
        $('.beizhu').html('备注')
    } else {

    }


    // 补齐信息
    var a=<?php echo e($data['is_info']); ?> 
    $('.weibuqi').click(function(){
        if(a==1){
            window.location.href="<?php echo e(url('/web/member/cz_view?store_id=')); ?><?php echo e($data['store_id']); ?>&merchant_id=<?php echo e($data['merchant_id']); ?>&open_id=<?php echo e($data['open_id']); ?>&mb_id=<?php echo e($data['mb_id']); ?>&ways_type=<?php echo e($data['ways_type']); ?>"
        }else{
            window.location.href="<?php echo e(url('/web/member/member_info?store_id=')); ?><?php echo e($data['store_id']); ?>&merchant_id=<?php echo e($data['merchant_id']); ?>&open_id=<?php echo e($data['open_id']); ?>&mb_id=<?php echo e($data['mb_id']); ?>&ways_source=<?php echo e($data['ways_source']); ?>&ways_type=<?php echo e($data['ways_type']); ?>"
        }
        
    })

</script>
</body>
</html>