<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="black" name="apple-mobile-web-app-status-bar-style">
    <meta content="telephone=no" name="format-detection">
    <meta content="email=no" name="format-detection">
    <title>添加学生</title>
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <script type="text/javascript" src="{{asset('/school/js/Screen.js')}}"></script>

</head>
<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header">班级信息</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item school">

                    <label class="layui-form-label"> 学校名称</label>
                    <div class="layui-input-block" style="padding-top:7px">
                        <span class="school_name"></span>
                    </div>
                </div>
                <div class="layui-form-item grade">
                    <label class="layui-form-label"><span style="color:red">*</span> 选择年级</label>
                    <div class="layui-input-block">
                        <select name="grade" id="grade" lay-filter="grade">

                        </select>
                    </div>
                </div>
                <div class="layui-form-item grade">
                    <label class="layui-form-label"><span style="color:red">*</span> 选择班级</label>
                    <div class="layui-input-block">
                        <select name="class" id="class" lay-filter="class">

                        </select>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="layui-card">
        <div class="layui-card-header">学生信息</div>
        <div class="layui-card-body layui-row layui-col-space10">
            <div class="layui-form">
                <div class="layui-form-item">
                    <label class="layui-form-label"><span style="color:red">*</span> 学生学号</label>
                    <div class="layui-input-block">
                        <input type="text" name="studentno" lay-verify="studentno" autocomplete="off" placeholder="请输入学生学号" class="layui-input studentno">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label"><span style="color:red">*</span> 学生姓名</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入学生姓名" class="layui-input studentname">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">学生证件号</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入学生身份证号" class="layui-input studentid" maxlength="18">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-card">
        <div class="layui-card-header">家长信息</div>
        <div class="layui-card-body layui-row layui-col-space10">
            <div class="layui-form">
                <div class="layui-form-item">
                    <label class="layui-form-label">家长姓名</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入家长姓名" class="layui-input parentname">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label"><span style="color:red">*</span>家长电话</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入家长电话" class="layui-input parentphone" maxlength="11">
                    </div>
                </div>
                <div class="layui-form-item grade">
                    <label class="layui-form-label">所属关系</label>
                    <div class="layui-input-block">
                        <select name="relationship" id="relationship" lay-filter="relationship">
                            <option value="">选择关系</option>
                            <option value="1">爸爸</option>
                            <option value="2">妈妈</option>
                            <option value="3">爷爷</option>
                            <option value="4">奶奶</option>
                            <option value="5">外公</option>
                            <option value="6">外婆</option>
                            <option value="7">家长</option>
                        </select>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="layui-form-item" style="text-align: center;margin-top:50px">
        <div style="min-height: 36px;">
            <div class="layui-footer">
                <button class="layui-btn submit layui-btn-disabled" disabled="disabled">确定提交</button>
                <button class="layui-btn goBack" style="background-color: #0bb20c">返回</button>
            </div>
        </div>
    </div>
    <div style="padding-top:15px"></div>

</div>

<input type="hidden" class="school_no" value="">
<input type="hidden" class="gradeid" value="">
<input type="hidden" class="classid" value="">
<input type="hidden" class="statusid" value="">
<input type="hidden" class="relationshipid" value="">
<input type="hidden" class="student_name" value="">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    // var token = sessionStorage.getItem("token");
    // var store_id = sessionStorage.getItem("store_id");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;

        var store_id = "{{$_GET['store_id']}}";
        var open_id = "{{$_GET['open_id']}}";
        var school_name = "{{$_GET['school_name']}}";
        var school_no = "{{$_GET['school_no']}}";
        var merchant_id = "{{$_GET['merchant_id']}}";
        $(document).ready(function () {
            $('.school_no').val(school_no);
            $('.school_name').text(school_name);
            getGradeList()
        });
        $('.goBack').click(function () {
            var data_url = "?store_id=" + store_id + "&open_id=" + open_id + "&school_name=" + school_name + "&school_no="
                + school_no + "&merchant_id=" + merchant_id;
            window.location.href = "{{url('api/easypay/alipay/studentManage')}}" + data_url;
        });
        function getGradeList(){
            $.post("{{url('/api/school/teacher/grade/lst')}}",
                {
                    store_id:store_id,school_no:school_no,merchant_id:merchant_id
                }, function (res) {
                    if (res.status == 1) {
                        var optionStr = "";
                        for(var i=0;i<res.data.length;i++){
                            optionStr += "<option value='" + res.data[i].stu_grades_no + "'>"
                                + res.data[i].stu_grades_name + "</option>";
                        }
                        $("#grade").html('') ;
                        $("#grade").append('<option value="">选择年级</option>'+optionStr);
                        layui.form.render('select');
                    } else {
                        alert('查找板块报错');
                    }

                }, "json");
        }
        form.on('select(grade)', function(data){
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.gradeid').val(category);
            // 选择班级
            $.ajax({
                url : "{{url('/api/school/teacher/class/lst')}}",
                data : {store_id:store_id,school_no:school_no,stu_grades_no:$('.gradeid').val(),merchant_id:merchant_id},
                type : 'post',
                success : function(data) {
                    var optionStr = "";
                    for(var i=0;i<data.data.length;i++){
                        optionStr += "<option value='" + data.data[i].stu_class_no + "'>"
                            + data.data[i].stu_class_name + "</option>";
                    }
                    $("#class").html('');
                    $("#class").append('<option value="">选择班级</option>'+optionStr);
                    layui.form.render('select');
                },
                error : function(data) {
                    alert('查找板块报错');
                }
            });
        });
        form.on('select(class)', function(data){
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.classid').val(category);
        });
        form.on('select(status)', function(data){
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.statusid').val(category);
        });
        form.on('select(relationship)', function(data){
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.relationshipid').val(category);
        });

        $('.studentno').blur(function (){
            $.post("{{url('/api/school/stu/getStudentNoAndStuClassNo')}}",
                {

                    stu_class_no:$('.classid').val(),
                    student_no:$('.studentno').val()

                },function(res){
                    if(res.status==1){
                        $('.submit').removeClass("layui-btn-disabled").attr("disabled",false);
                        $('.studentname').val(res.data.student_name)
                        $('.student_name').val(res.data.student_name)
                    }else{
                        layer.msg(res.message, {
                            offset: '15px'
                            ,icon: 2
                            ,time: 1000
                        });
                        $('.submit').addClass("layui-btn-disabled").attr("disabled",true);
                    }
                },"json");
        })
        function save() {
            $.post("{{url('/api/school/teacher/stu/add')}}",
                {
                    store_id: store_id,
                    school_no: $('.school_no').val(),
                    stu_grades_no: $('.gradeid').val(),
                    stu_class_no: $('.classid').val(),

                    student_name: $('.studentname').val(),
                    student_no: $('.studentno').val(),
                    student_identify: $('.studentid').val(),
                    //status:1,

                    student_user_name: $('.parentname').val(),
                    student_user_mobile: $('.parentphone').val(),
                    student_user_relation: $('.relationshipid').val(),
                    merchant_id: merchant_id,
                    open_id: open_id,

                }, function (res) {
                    if (res.status == 1) {
                        layer.msg(res.message, {
                            offset: '15px'
                            , icon: 1
                            , time: 1000
                        });

                        layer.confirm(res.message, {
                            btn: ['确定', '取消']//按钮
                        }, function (index) {
                            var data_url = "?store_id=" + store_id + "&open_id=" + open_id + "&school_name=" + school_name + "&school_no="
                                + school_no + "&merchant_id=" + merchant_id;
                            window.location.href = "{{url('api/easypay/weixin/studentManage')}}" + data_url;
                        });

                    } else {
                        layer.msg(res.message, {
                            offset: '15px'
                            , icon: 2
                            , time: 1000
                        });
                    }
                }, "json");
        }

        $('.submit').on('click', function () {
            if ($('.parentphone').val() == '') {
                alert("家长手机号不能为空");
                return;
            }
            if ($('.student_name').val() != $('.studentname').val()) {
                layer.confirm('您输入的学生姓名与学校录入的学生姓名不一致，是否确认修改？', function (index) {
                    save()
                })
            } else {
                save()
            }
        });

    });
</script>
</body>
</html>
