<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="black" name="apple-mobile-web-app-status-bar-style">
    <meta content="telephone=no" name="format-detection">
    <meta content="email=no" name="format-detection">
    <title>学生管理</title>
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <script type="text/javascript" src="{{asset('/school/js/Screen.js')}}"></script>

    <style>
        .cur {
            background-color: #eee !important;
        }
    </style>
</head>
<body style="background-color: #f2f4f5;">
<div style="width:100%"><img src="../../images/banner.png" style="max-width:100%;"></div>
<div style="position: relative;margin: 0 auto;">
    <div class="layui-card">
        <div class="layui-card-header" style="width:auto !important;font-size:18px">学生信息&nbsp;&nbsp;&nbsp;<span
                    class="zong_school_name"></span></div>

        <div class="layui-form" lay-filter="component-form-group">
            <div class="layui-form-item layui-layout-admin">
                <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                <div class="layui-input-block">
                    <div class="layui-footer" style="left: 0;">
                        <button class="layui-btn submit" data-type="tabChange">添加学生</button>
                        <button class="layui-btn goBack" data-type="tabChange" style="background-color: #0bb20c">返回首页
                        </button>
                        <!--<button type="reset" class="layui-btn layui-btn-primary">重置</button>-->
                    </div>
                </div>
            </div>
        </div>

        <script type="text/html" id="table-content-list" class="layui-btn-small">
            <a class="layui-btn layui-btn-normal layui-btn-sm edit" lay-event="edit">修改</a>
        </script>

    </div>
</div>


<script type="text/javascript" src="{{asset('/school/js/jquery-2.1.4.js')}}"></script>
<!-- <script type="text/javascript" src="{{asset('/school/js/mobileSelect.js')}}"></script> -->
<script type="text/javascript" src="{{asset('/school/js/fastclick.js')}}"></script>
<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>$(function () {
        FastClick.attach(document.body);
    });</script>
<script>
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form', 'upload', 'formSelects', 'element', 'table'], function () {
        var $ = layui.$
            , admin = layui.admin
            , element = layui.element
            , layer = layui.layer
            , laydate = layui.laydate
            , form = layui.form
            , upload = layui.upload
            , table = layui.table
            , formSelects = layui.formSelects;
        var store_id = "{{$_GET['store_id']}}";
        var open_id = "{{$_GET['open_id']}}";
        var school_name = "{{$_GET['school_name']}}";
        var school_no = "{{$_GET['school_no']}}";
        var merchant_id = "{{$_GET['merchant_id']}}";
        $(document).ready(function () {
        });
        table.render({
            elem: '#test-table-page'
            , url: "{{url('/api/school/stu/getStudentList')}}"
            , method: 'post'
            , where: {
                open_id: open_id,
                school_no: school_no
            }
            , request: {
                pageName: 'p',
                limitName: 'l'
            }
            , cellMinWidth: 160
            , cols: [[
                {width: '26%', field: 'grade_name', title: '年级名称'}
                , {width: '26%', field: 'class_name', title: '班级名称'}
                , {width: '28%', field: 'student_name', title: '学生姓名'}
                //,{field:'student_no', title: '学生学号'}
                //,{field:'status', width:80, title: '状态',templet:'#statusTap'}
                , {align: 'center', width: '22%', fixed: 'right', toolbar: '#table-content-list', title: '操作'}
            ]]
            , response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                , statusCode: 1 //成功的状态码，默认：0
                , msgName: 'message' //状态信息的字段名称，默认：msg
                , countName: 't' //数据总数的字段名称，默认：count
                , dataName: 'data' //数据列表的字段名称，默认：data
            }
            , done: function (res, curr, count) {
            }

        });
        $('.submit').click(function () {
            var data_url = "?store_id=" + store_id + "&open_id=" + open_id + "&school_name=" + school_name + "&school_no="
                + school_no + "&merchant_id=" + merchant_id;
            window.location.href = "{{url('api/easypay/alipay/addStudent')}}" + data_url;
        });
        $('.goBack').click(function () {
            var data_url = "?store_id=" + store_id + "&open_id=" + open_id + "&school_name=" + school_name + "&school_no="
                + school_no + "&merchant_id=" + merchant_id;
            window.location.href = "{{url('api/easypay/alipay/index')}}" + data_url;
        });
        table.on('tool(test-table-page)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var e = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            if (layEvent === 'edit') {
                var id = e.id;
                var stu_grades_no = e.stu_grades_no;
                var stu_class_no = e.stu_class_no
                var data_url = "?store_id=" + store_id + "&open_id=" + open_id + "&school_name=" + school_name + "&school_no="
                    + school_no + "&merchant_id=" + merchant_id + "&id=" + id + "&stu_grades_no=" + stu_grades_no + "&stu_class_no=" + stu_class_no;
                window.location.href = "{{url('api/easypay/alipay/editStudent')}}" + data_url;
            }
        });
    });


</script>
</body>
</html>