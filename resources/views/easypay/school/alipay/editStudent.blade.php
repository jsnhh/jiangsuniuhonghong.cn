<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="black" name="apple-mobile-web-app-status-bar-style">
    <meta content="telephone=no" name="format-detection">
    <meta content="email=no" name="format-detection">
    <title>学生修改</title>
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <script type="text/javascript" src="{{asset('/school/js/Screen.js')}}"></script>

</head>
<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header">班级信息</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item school">

                    <label class="layui-form-label"> 学校名称</label>
                    <div class="layui-input-block" style="margin-top:10px;padding-top:7px">
                        <span class="school_name"></span>
                    </div>
                </div>
                <div class="layui-form-item grade">
                    <label class="layui-form-label"><span style="color:red">*</span> 选择年级</label>
                    <div class="layui-input-block">
                        <select name="grade" id="grade" lay-filter="grade">

                        </select>
                    </div>
                </div>
                <div class="layui-form-item grade">
                    <label class="layui-form-label"><span style="color:red">*</span> 选择班级</label>
                    <div class="layui-input-block">
                        <select name="class" id="class" lay-filter="class">

                        </select>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="layui-card">
        <div class="layui-card-header">学生信息</div>
        <div class="layui-card-body layui-row layui-col-space10">
            <div class="layui-form">
                <div class="layui-form-item">
                    <label class="layui-form-label"><span style="color:red">*</span> 学生姓名</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off"
                               placeholder="请输入学生姓名" class="layui-input studentname">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">学生证件号</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off"
                               placeholder="请输入学生身份证号" class="layui-input studentid" maxlength="18">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-card">
        <div class="layui-card-header">家长信息</div>
        <div class="layui-card-body layui-row layui-col-space10">
            <div class="layui-form">
                <div class="layui-form-item">
                    <label class="layui-form-label">家长姓名</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off"
                               placeholder="请输入家长姓名" class="layui-input parentname">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">家长电话</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off"
                               placeholder="请输入家长电话" class="layui-input parentphone" maxlength="11">
                    </div>
                </div>
                <div class="layui-form-item grade">
                    <label class="layui-form-label">所属关系</label>
                    <div class="layui-input-block">
                        <select name="relationship" id="relationship" lay-filter="relationship">
                            <option value="">选择关系</option>
                            <option value="1">爸爸</option>
                            <option value="2">妈妈</option>
                            <option value="3">爷爷</option>
                            <option value="4">奶奶</option>
                            <option value="5">外公</option>
                            <option value="6">外婆</option>
                            <option value="7">家长</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item" style="text-align: center;margin-top:30px">
                    <div style="min-height: 36px;">
                        <div class="layui-footer">
                            <button class="layui-btn submit">确定提交</button>
                            <button class="layui-btn goBack" style="background-color: #0bb20c">返回</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<input type="hidden" class="school_no" value="">
<input type="hidden" class="gradeid" value="">
<input type="hidden" class="classid" value="">
<input type="hidden" class="statusid" value="">
<input type="hidden" class="relationshipid" value="">
<input type="hidden" class="student_id" value="">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form', 'upload', 'formSelects'], function () {
        var $ = layui.$
            , admin = layui.admin
            , element = layui.element
            , layer = layui.layer
            , laydate = layui.laydate
            , form = layui.form
            , upload = layui.upload
            , formSelects = layui.formSelects;


        var store_id = "{{$_GET['store_id']}}";
        var open_id = "{{$_GET['open_id']}}";
        var school_name = "{{$_GET['school_name']}}";
        var school_no = "{{$_GET['school_no']}}";
        var merchant_id = "{{$_GET['merchant_id']}}";
        var s_stu_grades_no="{{$_GET['stu_grades_no']}}";
        var s_stu_class_no="{{$_GET['stu_class_no']}}";
        var s_stu_id="{{$_GET['id']}}"
        $('.school_no').val(school_no);
        $('.school_name').text(school_name);
        $('.gradeid').val(s_stu_grades_no);
        $('.classid').val(s_stu_class_no);
        getBoards();

        function getBoards() {
            // 每条信息
            $.ajax({
                url: "{{url('/api/school/teacher/stu/show')}}",
                data: {stu_id: s_stu_id},
                type: 'post',
                success: function (res) {
                    var optionStr = "";
                    var optionStrs = "";

                    $('.studentname').val(res.data.student_name);
                    $('.studentid').val(res.data.student_identify);
                    $('.studentnum').val(res.data.student_no);
                    $('.parentname').val(res.data.student_user_name);
                    $('.parentphone').val(res.data.student_user_mobile);
                    $('.student_id').val(res.data.id);

                    $('#status option').each(function () {
                        if (res.data.status == $(this).val()) {
                            $(this).attr('selected', 'selected');
                        }
                    });
                    $('#relationship option').each(function () {
                        if (res.data.student_user_relation == $(this).val()) {
                            $(this).attr('selected', 'selected');
                        }
                    })

                },
                error: function (data) {
                    alert('查找板块报错');
                }
            });
            // 选择年级
            $.ajax({
                url: "{{url('/api/school/teacher/grade/lst')}}",
                data: {store_id: store_id, school_no: $('.school_no').val(), merchant_id: merchant_id},
                type: 'post',
                success: function (data) {
                    var optionStr = "";
                    for (var i = 0; i < data.data.length; i++) {

                        optionStr += "<option value='" + data.data[i].stu_grades_no + "' " + ((s_stu_grades_no == data.data[i].stu_grades_no) ? "selected" : "") + ">"
                            + data.data[i].stu_grades_name + "</option>";
                    }
                    $("#grade").append('<option value="">选择年级</option>' + optionStr);
                    layui.form.render('select');
                },
                error: function (data) {
                    alert('查找板块报错');
                }
            });
            // 选择班级
            $.ajax({
                url: "{{url('/api/school/teacher/class/lst')}}",
                data: {
                    store_id:store_id,school_no:school_no,stu_grades_no:$('.gradeid').val(),merchant_id:merchant_id
                },
                type: 'post',
                success: function (data) {
                    var optionStr = "";
                    for (var i = 0; i < data.data.length; i++) {

                        optionStr += "<option value='" + data.data[i].stu_class_no + "' " + ((s_stu_class_no == data.data[i].stu_class_no) ? "selected" : "") + ">"
                            + data.data[i].stu_class_name + "</option>";
                    }
                    $("#class").append('<option value="">选择班级</option>' + optionStr);
                    layui.form.render('select');
                },
                error: function (data) {
                    alert('查找板块报错');
                }
            });

        }
        $('.goBack').click(function () {
            var data_url = "?store_id=" + store_id + "&open_id=" + open_id + "&school_name=" + school_name + "&school_no="
                + school_no + "&merchant_id=" + merchant_id;
            window.location.href = "{{url('api/easypay/alipay/studentManage')}}" + data_url;
        });
        form.on('select(schooltype)', function (data) {
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.schooltypeid').val(category);
            // 选择年级
            $.ajax({
                url: "{{url('/api/school/teacher/grade/lst')}}",
                data: {token: token, school_no: $('.schooltypeid').val()},
                type: 'post',
                success: function (data) {
                    var optionStr = "";
                    for (var i = 0; i < data.data.length; i++) {

                        optionStr += "<option value='" + data.data[i].stu_grades_no + "' " + ((s_stu_grades_no == data.data[i].stu_grades_no) ? "selected" : "") + ">"
                            + data.data[i].stu_grades_name + "</option>";
                    }
                    $("#grade").html('');
                    $("#grade").append('<option value="">选择年级</option>' + optionStr);
                    layui.form.render('select');
                },
                error: function (data) {
                    alert('查找板块报错');
                }
            });
        });
        form.on('select(grade)', function (data) {
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.gradeid').val(category);
            // 选择班级
            $.ajax({
                url: "{{url('/api/school/teacher/class/lst')}}",
                data: {token: token, school_no: $('.schooltypeid').val(), stu_grades_no: $('.gradeid').val()},
                type: 'post',
                success: function (data) {
                    var optionStr = "";
                    for (var i = 0; i < data.data.length; i++) {

                        optionStr += "<option value='" + data.data[i].stu_class_no + "' " + ((s_stu_class_no == data.data[i].stu_class_no) ? "selected" : "") + ">"
                            + data.data[i].stu_class_name + "</option>";
                    }
                    $("#class").html('');
                    $("#class").append('<option value="">选择班级</option>' + optionStr);
                    layui.form.render('select');
                },
                error: function (data) {
                    alert('查找板块报错');
                }
            });

        });
        form.on('select(class)', function (data) {
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.classid').val(category);

        });
        //---------------------------------------------------------------------------
        form.on('select(status)', function (data) {
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.statusid').val(category);
        });
        form.on('select(relationship)', function (data) {
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.relationshipid').val(category);
        });

        $('.submit').on('click', function () {
            $.post("{{url('/api/school/teacher/stu/save')}}",
                {

                    school_no: $('.school_no').val(),
                    stu_grades_no: $('.gradeid').val(),
                    stu_class_no: $('.classid').val(),

                    student_name: $('.studentname').val(),
                    student_no: $('.studentnum').val(),
                    student_identify: $('.studentid').val(),
                    status: $('.statusid').val(),
                    student_id: $('.student_id').val(),

                    student_user_name: $('.parentname').val(),
                    student_user_mobile: $('.parentphone').val(),
                    student_user_relation: $('.relationshipid').val(),

                }, function (res) {
                    if (res.status == 1) {
                        layer.msg(res.message, {
                            offset: '15px'
                            , icon: 1
                            , time: 1000
                        }/*,function(){
                        var index=parent.layer.getFrameIndex(window.name);
                        parent.layer.close(index);
                        window.parent.location.reload();
                    }*/);
                    } else {
                        layer.msg(res.message, {
                            offset: '15px'
                            , icon: 2
                            , time: 1000
                        });
                    }
                }, "json");

        });

    });
</script>
</body>
</html>
