<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>修改缴费模板</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">

</head>
<body>

<div class="layui-fluid" style="margin-top:45px !important;">

    <div class="layui-card">
      <div class="layui-card-header">缴费模板修改</div>
      <div class="layui-card-body layui-row layui-col-space10">
        <div class="layui-form">
            <div class="layui-form-item school">
                <label class="layui-form-label">选择学校</label>
                <div class="layui-input-block">
                    <select name="schooltype" id="schooltype" lay-filter="schooltype">

                    </select>
                </div>
            </div>
            <div class="layui-form-item grade">
                <label class="layui-form-label">选择年级</label>
                <div class="layui-input-block">
                    <select name="grade" id="grade" lay-filter="grade">

                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">模版名称</label>
                <div class="layui-input-block">
                    <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入模版名称" class="layui-input templatename">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">模版描述</label>
                <div class="layui-input-block">
                    <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入模版描述" class="layui-input templatedesc">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">缴费名称</label>
                <div class="layui-input-block">
                    <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入缴费名称" class="layui-input item_name">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">缴费金额</label>
                <div class="layui-input-block">
                    <input type="number" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入缴费金额" class="layui-input item_price">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">缴费数量</label>
                <div class="layui-input-block">
                    <input type="number" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入数量" class="layui-input item_number">
                </div>
            </div>

{{--            <div class="layui-form-item status">--}}
{{--                <label class="layui-form-label">是否必交(Y或N)</label>--}}
{{--                <div class="layui-input-block">--}}
{{--                    <select name="item_mandatory" id="item_mandatory" lay-filter="item_mandatory">--}}
{{--                        <option value="">选择状态</option>--}}
{{--                        <option value="Y">Y</option>--}}
{{--                        <option value="N">N</option>--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--            </div>--}}


            <div class="layui-form-item layui-layout-admin">
                <div class="layui-input-block">
                    <div class="layui-footer" style="left: 0;">
                        <button class="layui-btn submit">确定提交</button>

                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>

</div>
<input type="hidden" class="item_mandatory_id" value="">
<input type="hidden" class="schooltypeid" value="">
<input type="hidden" class="gradeid" value="">
<input type="hidden" class="stu_class_no" value="">
<input type="hidden" class="school_no" value="">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("token");
    var store_id = sessionStorage.getItem("edit_store_id");
    var stu_order_type_no = sessionStorage.getItem("stu_order_type_no");
    var school_no = sessionStorage.getItem("school_no");
    var stu_grades_no = sessionStorage.getItem("stu_grades_no");
    var stu_item_mandatory = sessionStorage.getItem("stu_item_mandatory");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'table','form'], function(){
        var $ = layui.$
            table = layui.table
            ,form = layui.form;

        $('.schooltypeid').val(school_no);
        $('.gradeid').val(stu_grades_no);
        $('.item_mandatory_id').val(stu_item_mandatory);

        $.post("{{url('/api/school/teacher/template/show')}}",
        {
          token:token,
          stu_order_type_no:stu_order_type_no
        },
        function(res){
            $('.school .layui-input-block').eq(0).find('input').val(res.data.school_name);
            $('.grade .layui-input-block').eq(0).find('input').val(res.data.grades_name);

          $('.layui-form .layui-form-item').eq(2).find('input').val(res.data.charge_name);
          $('.layui-form .layui-form-item').eq(3).find('input').val(res.data.charge_desc);
          $('.layui-form .layui-form-item').eq(4).find('input').val(res.data.item_name);
          $('.layui-form .layui-form-item').eq(5).find('input').val(res.data.amount);
          $('.layui-form .layui-form-item').eq(6).find('input').val(res.data.item_number);

            $('#item_mandatory option').each(function(){
                if(res.data.item_mandatory==$(this).val()){
                    $(this).attr('selected','selected');
                }
            });

        });


        getBoards();
        // 选择学校-----开始
        function getBoards(){
            // 选择学校
            $.ajax({
                url : "{{url('/api/school/teacher/lst')}}",
                data : {token:token,store_id:store_id},
                type : 'post',
                success : function(data) {

                    var optionStr = "";
                        for(var i=0;i<data.data.length;i++){

                            optionStr += "<option value='" + data.data[i].school_no + "' "+((school_no==data.data[i].school_no)?"selected":"")+">"
                                + data.data[i].school_name + "</option>";
                        }
                        $("#schooltype").append('<option value="">选择学校</option>'+optionStr);
                        layui.form.render('select');
                },
                error : function(data) {
                    alert('查找板块报错');
                }
            });

            // 选择年级
            $.ajax({
                url : "{{url('/api/school/teacher/grade/lst')}}",
                data : {token:token},
                type : 'post',
                success : function(data) {

                    var optionStr = "";
                    for(var i=0;i<data.data.length;i++){

                        optionStr += "<option value='" + data.data[i].stu_grades_no + "' "+((stu_grades_no==data.data[i].stu_grades_no)?"selected":"")+">"
                            + data.data[i].stu_grades_name + "</option>";
                    }
                    $("#grade").append('<option value="">选择学校</option>'+optionStr);
                    layui.form.render('select');
                },
                error : function(data) {
                    alert('查找板块报错');
                }
            });
        }

        form.on('select(schooltype)', function(data){
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.schooltypeid').val(category);

            // 选择年级
            $.ajax({
                url : "{{url('/api/school/teacher/grade/lst')}}",
                data : {token:token,school_no:category},
                type : 'post',
                success : function(data) {
                    var optionStr = "";
                    for(var i=0;i<data.data.length;i++){
                        optionStr += "<option value='" + data.data[i].stu_grades_no + "'>"
                            + data.data[i].stu_grades_name + "</option>";
                    }
                    $("#grade").html('');
                    $("#grade").append('<option value="">选择年级</option>'+optionStr);
                    layui.form.render('select');
                },
                error : function(data) {
                    alert('查找板块报错');
                }
            });

        });
        form.on('select(grade)', function(data){
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.gradeid').val(category);
            $('.gradename').val(categoryName);
        });

        // 选择学校----结束

        form.on('select(item_mandatory)', function(data){
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.item_mandatory_id').val(category);
        });

        // 提交表单----------------------
        $('.submit').on('click', function(){

            $.post("{{url('/api/school/teacher/template/save')}}",
                {
                    token:token,
                    stu_order_type_no:stu_order_type_no,
                    store_id:store_id,
                    school_no:$('.schooltypeid').val(),
                    stu_grades_no:$('.gradeid').val(),
                    charge_name:$('.templatename').val(),
                    charge_desc:$('.templatedesc').val(),
                    item_name:$('.item_name').val(),
                    item_number:$('.item_number').val(),
                    //item_mandatory:$('.item_mandatory_id').val(),
                    amount:$('.item_price').val(),

                },function(res){
                    if(res.status==1){
                        layer.msg(res.message, {
                            offset: '15px'
                            ,icon: 1
                            ,time: 1000
                        });
                    }else{
                        layer.msg(res.message, {
                            offset: '15px'
                            ,icon: 2
                            ,time: 1000
                        });
                    }
                },"json");

        });

    });
</script>
</body>
</html>
