<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>教师列表</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
  <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
</head>
<body>

  <div class="layui-fluid" style="margin-top:45px !important;">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">

        <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
              <div class="layui-card">
                <div class="layui-card-header">教师列表</div>

                <div class="layui-card-body">
                  <div class="layui-btn-container" style="font-size:14px;">
                    <a class="layui-btn layui-btn-primary" lay-href="{{url('/merchantpc/addteacher')}}" style="display: block;width: 100px;">添加教师</a>
                    <div class="layui-form" lay-filter="component-form-group" style="width:300px;display: inline-block;">
                      <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:0">
                            <select name="schooltype" id="schooltype" lay-filter="schooltype">

                            </select>
                        </div>
                      </div>
                    </div>

                    <!-- <div class="layui-form" lay-filter="component-form-group" style="width:300px;display: inline-block;">
                      <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:0">
                            <select name="class" id="class" lay-filter="class">

                            </select>
                        </div>
                      </div>
                    </div> -->

                    <div class="layui-form" lay-filter="component-form-group" style="width:300px;display: inline-block;">
                      <div class="layui-form-item">
                          <div class="layui-inline">
                            <div class="layui-input-inline">
                              <input type="text" name="id" placeholder="请输入教师姓名" autocomplete="off" class="layui-input">
                            </div>
                          </div>

                          <div class="layui-inline">
                            <button class="layui-btn layuiadmin-btn-list" lay-submit="" lay-filter="LAY-app-contlist-search" style="margin-bottom: 0;height:36px;line-height: 36px;">
                              <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                            </button>
                          </div>
                        </div>
                    </div>

                  </div>

                  <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>

                  <script type="text/html" id="statusTap">
                    @{{#  if(d.status == 1){ }}
                    <span class="cur">班主任</span>
                    @{{#  } else { }}
                    老师
{{--                     @{{ d.status_desc }}--}}
                    @{{#  } }}
                  </script>

                  <script type="text/html" id="table-content-list">

                    {{-- <a class="layui-btn layui-btn-normal layui-btn-xs edit" lay-event="edit" lay-href="">修改</a>  --}}
                    <a class="layui-btn  layui-btn-xs del" lay-event="del">删除</a>

                  </script>


                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
    <script>
    var token = sessionStorage.getItem("token");
    var store_id = sessionStorage.getItem("store_id");

    function GetQueryString(name)
    {
         var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
         var r = window.location.search.substr(1).match(reg);
         if(r!=null)return  unescape(r[2]); return null;
    }
    var stu_class_no=GetQueryString("stu_class_no");


    layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form', 'table'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,table = layui.table
            ,form = layui.form;

        // 选择学校
        $.ajax({
            url : "{{url('/api/school/teacher/lst')}}",
            data : {token:token},
            type : 'post',
            success : function(data) {
                var optionStr = "";
                    for(var i=0;i<data.data.length;i++){
                        optionStr += "<option value='" + data.data[i].school_no  + "'>"
                            + data.data[i].school_name + "</option>";
                    }
                    $("#schooltype").append('<option value="">选择学校</option>'+optionStr);
                    layui.form.render('select');
            },
            error : function(data) {
                alert('查找板块报错');
            }
        });


        table.render({
            elem: '#test-table-page'
            ,url: "{{url('/api/school/teacher/ter/lst')}}"
            ,method: 'post'
            ,where:{
              token:token,
              store_id:store_id
            }
            ,request:{
              pageName: 'p',
              limitName: 'l'
            }
            ,page: true
            ,cellMinWidth: 150
            ,cols: [[
                {field:'school_name', title: '学校名称'}
                ,{field:'name', title: '教师姓名'}
                ,{field:'phone', title: '手机号'}
                ,{field:'status', title: '类型',templet: '#statusTap'}
                ,{align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
            ]]
            ,response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                ,statusCode: 1 //成功的状态码，默认：0
                ,msgName: 'message' //状态信息的字段名称，默认：msg
                ,countName: 't' //数据总数的字段名称，默认：count
                ,dataName: 'data' //数据列表的字段名称，默认：data
              }
            ,done: function(res, curr, count){
            }

        });


        table.on('tool(test-table-page)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
          var e = obj.data; //获得当前行数据
          var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
          var tr = obj.tr; //获得当前行 tr 的DOM对象

          sessionStorage.setItem('stu_grades_no', e.stu_grades_no);
          sessionStorage.setItem('school_name', e.school_name);

          if(layEvent === 'del'){ //删除
            layer.confirm('真的删除行么', function(index){
              obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
              layer.close(index);
              //向服务端发送删除指令
              $.ajax({
                url : "{{url('/api/school/teacher/ter/del')}}",
                data : {token:token,id:e.id},
                type : 'post',
                success : function(data) {
                  layer.msg(data.message, {
                    offset: '15px'
                    ,icon: 1
                    ,time: 1000
                  });
                },
                error : function(data) {
                  layer.msg(data.message, {
                    offset: '15px'
                    ,icon: 2
                    ,time: 3000
                  });
                }
              });
            });
          }else if(layEvent === 'edit'){
            //$('.edit').attr('lay-href',"{{url('/merchantpc/editschool?store_id=')}}"+e.store_id)


          }
        });


        // 选择学校
        form.on('select(schooltype)', function(data){
          var school_no = data.value;

          // 选择班级
          //   $.ajax({
          //     url : "{{url('/api/school/teacher/class/lst')}}",
          //     data : {token:token,store_id:store_id,school_no:school_no},
          //     type : 'post',
          //     success : function(data) {
          //       var optionStr = "";
          //       for(var i=0;i<data.data.length;i++){
          //         optionStr += "<option value='" + data.data[i].stu_class_no + "' "+((stu_class_no==data.data[i].stu_class_no)?"selected":"")+">"
          //                 + data.data[i].stu_class_name + "</option>";
          //       }
          //       $("#class").html('');
          //       $("#class").append('<option value="">选择班级</option>'+optionStr);
          //       layui.form.render('select');
          //     },
          //     error : function(data) {
          //       alert('查找板块报错');
          //     }
          //   });

          //执行重载
          table.reload('test-table-page', {
            where: {
              store_id: store_id,
              school_no:school_no
            }
          });
        });

        // 选择班级
        form.on('select(class)', function(data){
          var stu_class_no = data.value;
          //执行重载
          table.reload('test-table-page', {
            where: {
              store_id: store_id,
              stu_class_no: stu_class_no
            }
          });
        });

        //监听搜索
        form.on('submit(LAY-app-contlist-search)', function(data){
          var value = data.field.id;
          //执行重载
          table.reload('test-table-page', {
            where: {
              name: value
            }
          });
        });

    });

  </script>

</body>
</html>