<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>表单元素</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
  <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
</head>
<body>

  <div class="layui-fluid" style="background-color: #fff;margin-top:45px !important;">
    <div class="layui-row">
      <div class="layui-form">

        <div class="layui-form-item">
          <div class="layui-inline">
            <label class="layui-form-label">学校名称</label>
            <div class="layui-form-mid layui-word-aux"></div>
          </div>

          <div class="layui-inline">
            <label class="layui-form-label">年级名称</label>
            <div class="layui-form-mid layui-word-aux"></div>
          </div>
        </div>

        <div class="layui-form-item">
          <div class="layui-inline">
            <label class="layui-form-label">模板名称</label>
            <div class="layui-form-mid layui-word-aux"></div>
          </div>

          <div class="layui-inline">
            <label class="layui-form-label">模板描述</label>
            <div class="layui-form-mid layui-word-aux"></div>
          </div>
        </div>
        <div class="layui-form-item">
          <div class="layui-inline">
            <label class="layui-form-label">缴费名称</label>
            <div class="layui-form-mid layui-word-aux"></div>
          </div>

          <div class="layui-inline">
            <label class="layui-form-label">缴费金额</label>
            <div class="layui-form-mid layui-word-aux"></div>
          </div>
        </div>
        <div class="layui-form-item">
          <div class="layui-inline">
            <label class="layui-form-label">缴费数量</label>
            <div class="layui-form-mid layui-word-aux"></div>
          </div>

          <div class="layui-inline">
            <label class="layui-form-label">是否必交</label>
            <div class="layui-form-mid layui-word-aux"></div>
          </div>
        </div>

      </div>
    </div>
  </div>


  <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
  var token = sessionStorage.getItem("token");

  var str=location.search;
  var stu_order_type_no=str.split('?')[1];

  layui.config({
    base: '../../../layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index', 'form','table'], function(){
    var $ = layui.$
    ,admin = layui.admin
    ,element = layui.element
    ,table = layui.table
    ,form = layui.form;

    $.post("{{url('/api/school/teacher/template/show')}}",
    {
      token:token,
      stu_order_type_no:stu_order_type_no
    },
    function(res){

      $('.layui-form .layui-inline').eq(0).find('.layui-word-aux').html(res.data.school_name);
      $('.layui-form .layui-inline').eq(1).find('.layui-word-aux').html(res.data.grades_name);
      $('.layui-form .layui-inline').eq(2).find('.layui-word-aux').html(res.data.charge_name);
      $('.layui-form .layui-inline').eq(3).find('.layui-word-aux').html(res.data.charge_desc);
      $('.layui-form .layui-inline').eq(4).find('.layui-word-aux').html(res.data.item_name);
      $('.layui-form .layui-inline').eq(5).find('.layui-word-aux').html(res.data.amount);
      $('.layui-form .layui-inline').eq(6).find('.layui-word-aux').html(res.data.item_number);
      $('.layui-form .layui-inline').eq(7).find('.layui-word-aux').html(res.data.item_mandatory);


    });


  });
  </script>
</body>
</html>