<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="black" name="apple-mobile-web-app-status-bar-style">
    <meta content="telephone=no" name="format-detection">
    <meta content="email=no" name="format-detection">
    <title>管理授权的微信小程序</title>
    <style type="text/css">
        body, html {
            margin: 0;
            padding: 0;
            height: 100%;
            font-family: Helvetica Neue,Helvetica,PingFang SC,Hiragino Sans GB,Microsoft YaHei,SimSun,sans-serif;
            font-weight: 400;
            -webkit-font-smoothing: antialiased;
        }

        .content{
            padding: 20px 10px;
        }
        .spinner {
            width: 60px;
            height: 60px;
            position: relative;
            margin: 100px auto;
        }

        .double-bounce1, .double-bounce2 {
            width: 100%;
            height: 100%;
            border-radius: 50%;
            background-color: #67CF22;
            opacity: 0.6;
            position: absolute;
            top: 0;
            left: 0;

            -webkit-animation: bounce 2.0s infinite ease-in-out;
            animation: bounce 2.0s infinite ease-in-out;
        }

        .double-bounce2 {
            -webkit-animation-delay: -1.0s;
            animation-delay: -1.0s;
        }

        @-webkit-keyframes bounce {
            0%, 100% { -webkit-transform: scale(0.0) }
            50% { -webkit-transform: scale(1.0) }
        }

        @keyframes bounce {
            0%, 100% {
                transform: scale(0.0);
                -webkit-transform: scale(0.0);
            } 50% {
                  transform: scale(1.0);
                  -webkit-transform: scale(1.0);
              }
        }
        .el-table__body, .el-table__footer, .el-table__header {
            table-layout: fixed;
        }
        table {
            display: table;
            border-collapse: separate;
            box-sizing: border-box;
            border-spacing: 2px;
            border-color: grey;
        }
        .el-table {
            position: relative;
            overflow: hidden;
            box-sizing: border-box;
            flex: 1;
            width: 100%;
            max-width: 100%;
            background-color: #fff;
            font-size: 14px;
            color: #606266;
        }
        tbody {
            display: table-row-group;
            vertical-align: middle;
            border-color: inherit;
        }
        .el-table tr {
            background-color: #fff;
        }
        tr {
            display: table-row;
            vertical-align: inherit;
            border-color: inherit;
        }
        .el-table--enable-row-transition .el-table__body td {
            transition: background-color .25s ease;
        }
        .el-table--border td, .el-table--border th {
            border-right: 1px solid #ebeef5;
        }
        .el-table td, .el-table th.is-leaf {
            border-bottom: 1px solid #ebeef5;
        }
        .el-table td, .el-table th {
            padding: 12px 0;
            min-width: 0;
            box-sizing: border-box;
            text-overflow: ellipsis;
            vertical-align: middle;
            position: relative;
        }
        .el-table .cell {
            box-sizing: border-box;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: normal;
            word-break: break-all;
            line-height: 23px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .el-button {
            margin-left: 10px;
            display: inline-block;
            line-height: 1;
            white-space: nowrap;
            cursor: pointer;
            background: #fff;
            border: 1px solid #dcdfe6;
            border-color: #dcdfe6;
            color: #606266;
            -webkit-appearance: none;
            text-align: center;
            box-sizing: border-box;
            outline: none;
            margin: 0;
            transition: .1s;
            font-weight: 500;
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
            padding: 12px 20px;
            font-size: 14px;
            border-radius: 4px;
        }
        .el-button--primary {
            color: #fff;
            background-color: #409eff;
            border-color: #409eff;
        }
        .el-button--success {
            color: #fff;
            background-color: #67c23a;
            border-color: #67c23a;
        }
    </style>
</head>
<body>
    <section class="content" id="content">
        <div v-if="load" class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>

        <table cellspacing="0" cellpadding="0" border="0" class="el-table__body el-table" style="width: 100%;">
            <tbody>
                <tr v-for="site in appletList" class="el-table__row">
                    <td class="">
                        <div class="cell">@{{site.authorizer_appid}}</div>
                    </td>
                    <td class="">
                        <div class="cell">@{{site.authorizer_appid_name}}</div>
                    </td>
                    <td class="">
                        <div v-if="site.authorizer_appid_status">
                            <div v-if="site.authorizer_appid_status == 2" class="el-button el-button--success">正在审核</div>
                        </div>
                        <div v-else>
                            <div class="el-button el-button--primary">发布审核</div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </section>
</body>
</html>
<script type="text/javascript" src="{{asset('/phone/js/jquery-2.1.4.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/vue.js')}}"></script>
<script type="text/javascript">
    $(function(){
        new Vue({
            el:"#content",
            data:{
               load:false,
               appletList:[]
            },
            create(){

            },
            mounted() {
                var that = this;
                that.load = true;
                //获取管理的小程序列表
                $.post("/api/customer/weixin/getAuthorizerList",{
                    page:1,
                    count:10,
                },function(data){
                    console.log(data);
                    var status = data.status;
                    that.load = false;
                    if(status == 200){
                        that.appletList = data.data.list;
                    }else{

                    }
                },"json");
            }
        });
    });
</script>