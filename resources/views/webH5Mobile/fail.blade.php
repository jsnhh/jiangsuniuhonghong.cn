<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="black" name="apple-mobile-web-app-status-bar-style">
    <meta content="telephone=no" name="format-detection">
    <meta content="email=no" name="format-detection">
    <title>领券失败</title>
    <style type="text/css">
        .content{
            padding: 15px;
        }
        .authorize_success{

        }
        .authorize_success_img{
            display: block;
            width: 74px;
            margin: 0 auto;
        }
        .authorize_success_text{
            font-weight: 500;
            color: #333333;
            text-align: center;
            margin-top: 5px;
            margin: 0 auto;
            padding-top:24px;
        }
        .athorize{margin:0 auto;width: 85px;padding-top: 24px;}
    </style>
</head>
<body>
<section class="content">
    <div class="authorize_success">
        <div style="width:74px;margin:0 auto;padding-top:300px;">
            <img class="authorize_success_img" src="/school/images/fail.png">
        </div>
        <div class="authorize_success_text" style="font-size: 14px;color: #666666;">{{$message}}</div>
    </div>
</section>
</body>
</html>
