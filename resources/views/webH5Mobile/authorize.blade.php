<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="black" name="apple-mobile-web-app-status-bar-style">
    <meta content="telephone=no" name="format-detection">
    <meta content="email=no" name="format-detection">
    <title>授权微信小程序</title>
    <style type="text/css">
        .content{
            padding: 15px;
        }
        .person{
            width: 100%;
            height: 100px;
            text-align: center;
            line-height: 20px;
            color: #000;
            font-size: 14px;
        }
        .athorize{
            width: 100%;
            height: 40px;
            line-height: 40px;
            text-align: center;
            font-size: 14px;
            color: #fff;
            background-color: #00a3fe;
        }
    </style>
</head>
<body>
    <section class="content">
        <div class="person"></div>
        <div class="athorize">请去管理后台进行授权小程序</div>
    </section>
</body>
</html>
<script type="text/javascript" src="{{asset('/phone/js/jquery-2.1.4.js')}}"></script>
<script type="text/javascript">
    $(function(){
        {{--var component_appid = "wx569476fd97973ad2";--}}
        {{--var redirect_uri    = "{{env("APP_URL")}}"+"/authorize/refundAuthUrl";--}}
        {{--$(".athorize").on("click",function(){--}}
           {{--$(".person").html("");--}}
           {{--$.post("/api/customer/weixin/getPreAuthCode",{--}}
               {{--component_appid:""--}}
           {{--},function(data){--}}
               {{--var status = data.status;--}}
               {{--if(status == 200){--}}
                   {{--var preAuthCode = data.data.pre_auth_code;--}}
                   {{--window.location.href = "https://mp.weixin.qq.com/safe/bindcomponent?action=bindcomponent&auth_type=2&no_scan=1&component_appid="+component_appid+"&pre_auth_code="+preAuthCode+"&redirect_uri="+redirect_uri+"&auth_type=2#wechat_redirect";--}}
               {{--}else{--}}
                   {{--$(".person").html(data.message);--}}
               {{--}--}}
           {{--},"json");--}}
        {{--});--}}
    });
</script>