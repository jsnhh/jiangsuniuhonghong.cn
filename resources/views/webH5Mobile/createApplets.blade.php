<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="black" name="apple-mobile-web-app-status-bar-style">
    <meta content="telephone=no" name="format-detection">
    <meta content="email=no" name="format-detection">
    <title>创建微信小程序</title>
    <style type="text/css">
        .content{
            padding: 15px;
        }
        form{
            width: 100%;
        }
        .form-group{
            width: 100%;
            margin-bottom: 15px;
        }
        .form-control{
            display: block;
            width: 90%;
            height: 20px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }
        .label{
            font-size: 14px;
        }
        .input{
            width: 100%;
            margin-top: 10px;
        }
        .createApplet{
            width: 100%;
            height: 40px;
            line-height: 40px;
            text-align: center;
            font-size: 14px;
            color: #fff;
            background-color: #00a3fe;
        }
        .person{
            width: 100%;
            text-align: left;
            line-height: 20px;
            color: #E60012;
            font-size: 14px;
            margin-top: 5px;
        }
    </style>
</head>
<body>
<section class="content">
    <form action="" method="post">
        {{--<div class="company_name form-group">--}}
            {{--<div class="label">企业名</div>--}}
            {{--<div class="input">--}}
                {{--<input class="form-control" type="text" name="name" value="" placeholder="企业名（需与工商部门登记信息一致）" />--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="company_name form-group">--}}
            {{--<div class="label">企业代码</div>--}}
            {{--<div class="input">--}}
                {{--<input class="form-control" type="text" name="code" value="" placeholder="统一社会信用代码（18 位）" />--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="company_name form-group">--}}
            {{--<div class="label">法人微信号</div>--}}
            {{--<div class="input">--}}
                {{--<input class="form-control" type="text" name="legal_persona_wechat" value="" placeholder="法人微信号" />--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="company_name form-group">--}}
            {{--<div class="label">法人姓名</div>--}}
            {{--<div class="input">--}}
                {{--<input class="form-control" type="text" name="legal_persona_name" value="" placeholder="法人姓名" />--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="company_name form-group">--}}
            {{--<div class="label">联系电话</div>--}}
            {{--<div class="input">--}}
                {{--<input class="form-control" type="text" name="component_phone" value="" placeholder="联系电话" />--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<input type="hidden" name="code_type" value="1" />--}}
        {{--<div class="person"></div>--}}
        <div class="createApplet">请去管理后台创建小程序</div>
    </form>
</section>
</body>
</html>

<script type="text/javascript" src="{{asset('/phone/js/jquery-2.1.4.js')}}"></script>
<script type="text/javascript">
    $(function(){
        // $(".createApplet").on("click",function(){
        //     var request_data         = $("form").serialize();
        //     var name                 = $("input[name=name]").val();
        //     var code                 = $("input[name=code]").val();
        //     var legal_persona_wechat = $("input[name=legal_persona_wechat]").val();
        //     var legal_persona_name   = $("input[name=legal_persona_name]").val();
        //     var component_phone      = $("input[name=component_phone]").val();
        //     $(".person").html("");
        //     if(!name){
        //         $(".person").html("企业名不可为空");
        //         return false;
        //     }
        //     if(!code){
        //         $(".person").html("企业代码不可为空");
        //         return false;
        //     }
        //     if(!legal_persona_wechat){
        //         $(".person").html("法人微信号不可为空");
        //         return false;
        //     }
        //     if(!legal_persona_name){
        //         $(".person").html("法人姓名不可为空");
        //         return false;
        //     }
        //     if(!component_phone){
        //         $(".person").html("联系电话不可为空");
        //         return false;
        //     }
        //     $.post("/api/customer/weixin/fastRegisterWeApp",request_data,function(data){
        //         var status = data.status;
        //         if(status == 200){
        //             $(".person").html("创建成功，请查收微信通知");
        //         }else{
        //             $(".person").html(data.message);
        //         }
        //     },"json");
        // });
    });
</script>