<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>刷脸优选</title>
    <link href="/sweetalert2/sweetalert2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
</head>
<style type="text/css">
    body:before{
        content: ' ';
        position: fixed;
        z-index: -1;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background: url(/payviews/img/beijing1.jpg) top center no-repeat;
        background-size: cover;
    }
    h2 {text-align: center;color: #fff;font-size: 40px;}
    .block{float: left;display: inline-block;font-size: 24px;line-height: 38px;color: #fff;}
    #search, input[type=text], input[type=password] {border: 1px solid #DCDEE0;font-size: 16px;text-align: center;width: 100px;height: 34px;box-sizing: border-box;display: inline-block;vertical-align: middle;}
    .td1 {border-right: 1px solid #fff;border-bottom: 1px solid #fff;color: #fff;font-size: 20px;text-align: center;line-height: 40px;}
    .td2 {border-bottom: 1px solid #fff;color: #fff;font-size: 20px;text-align: center;line-height: 40px;}
</style>
<body>
<h1 style="height: 80px;margin: 50px"></h1>
<div style="width:880px; margin:auto">
    <h2>刷脸支付全国在线地区剩余名额查询</h2>
    <form method="post" class="layui-form" style="text-align:center;display:  inline-block;" autocomplete="off" >
        <div class="block"><span>城市/区、县</span>
            <input name="dr_key" type="text" class="layui-input" id="dr_key" placeholder="请输入区域名称" datatype="*" nullmsg="请输入区域名称" style="width:350px;" autocomplete="new-password">
        </div>
        <div class="block">
            <span>　密码</span>
            <input name="pass" type="password" id="pass" style="width:160px;" placeholder="查询密码" maxlength="6" datatype="*" nullmsg="请填写查询密码" autocomplete="new-password">
        </div>
        <div class="block">
            <span>　</span>
            <input type="button"  class="submit" id="search" value="查询">
        </div>
        <div class="clear"></div>
    </form>
</div>
<div style="height:20px"></div>
<div style="width:880px; margin:auto; border:1px solid #fff; border-bottom:none; color:#fff">
    <table width="880" cellspacing="0" cellpadding="0">
        <tbody id="list">
            <tr>
                <td class="td1">区域名称</td>
                <td class="td1">剩余名额</td>
                <td class="td2">查询次数</td>
            </tr>
            <tr>
               <td colspan="3" class="td2">暂无数据</td>
            </tr>
        </tbody>
    </table>
</div>
<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script> 
<script type="text/javascript">
		layui.use(['form','jquery'],function(){
		  var form = layui.form;
		  var $ = jquery = layui.jquery;//如果不写$ = jquery,默认是不会把jquery中的$对象引进来
          
            $("#search").on('click', function () {
                var pass = $("#pass").val();
                var drKey = $("#dr_key").val()


                if(null == drKey || drKey.trim().length === 0){
                    layer.msg("请填写区域名称", {icon:1, shade:0.5, time:1000});
                    return false;
                }
                if(null == pass || pass.trim().length === 0){
                    layer.msg("请填写查询密码", {icon:1, shade:0.5, time:1000});
                    return false;
                }


                var num = pass.substr(0, 3);
                var n = parseInt(num);
                if (isNaN(n)) {
                    //不是数字
                    layer.msg("密码必须是纯数字", {icon:1, shade:0.5, time:1000});
                    return false;
                }

                var info = '<tr><td class="td1">区域名称</td><td class="td1">剩余名额</td><td class="td2">查询次数</td></tr>' +
                '<tr>' +
                '<td class="td1">' + drKey + '</td>' +
                ' <td class="td1">' + num.substr(0, 1) + '</td>'+
                ' <td class="td2">'+ num.substr(1,2) +'</td>'+
                '</tr>';

                document.getElementById("list").innerHTML = info;
            
            });
		});
	</script>
</body>
</html>