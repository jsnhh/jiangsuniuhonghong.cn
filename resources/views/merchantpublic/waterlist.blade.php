<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>流水查询</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .tongbu {
            background-color: #FFB800;
            color: #fff;
            margin-right: 5px;
        }

        .tui {
            background-color: #FF5722;
            margin-right: 5px;
        }

        .cur {
            color: #3475c3;
        }

        .yname {
            font-size: 13px;
            color: #444;
        }
    </style>
</head>
<body>

<div class="layui-fluid" style="margin-top:50px;">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">

            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12">
                        <div class="layui-card">
                            <div class="layui-card-header">交易流水列表</div>

                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">
                                    <!-- 选择业务员 -->
                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:190px;margin-right:10px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <div class="layui-input-block" style="margin-left:0">
                                                <text class="yname">选择门店</text>
                                                <select name="agent" id="agent" lay-filter="agent" lay-search>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 学校 -->
                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:190px;margin-right:10px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <div class="layui-input-block" style="margin-left:0">
                                                <text class="yname">选择收银员</text>
                                                <select name="schooltype" id="schooltype" lay-filter="schooltype"
                                                        lay-search>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 支付状态 -->
                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:190px;margin-right:10px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <div class="layui-input-block" style="margin-left:0">
                                                <text class="yname">选择支付状态</text>
                                                <select name="status" id="status" lay-filter="status">
                                                    <option value="">全部</option>
                                                    <option value="1">成功</option>
                                                    <option value="2">等待支付</option>
                                                    <option value="3">失败</option>
                                                    <option value="4">关闭</option>
                                                    <option value="5">退款中</option>
                                                    <option value="6">已退款</option>
                                                    <option value="7">有退款</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 支付类型 -->
                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:190px;margin-right:10px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <div class="layui-input-block" style="margin-left:0">
                                                <text class="yname">选择支付类型</text>
                                                <select name="type" id="type" lay-filter="type">
                                                    <option value="">全部</option>
                                                    <option value="alipay">支付宝</option>
                                                    <option value="weixin">微信</option>
                                                    <option value="jd">京东</option>
                                                    <option value="unionpay">银联刷卡</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 排序 -->
                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:190px;margin-right:10px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <div class="layui-input-block" style="margin-left:0">
                                                <text class="yname">金额排序</text>
                                                <select name="sort" id="sort" lay-filter="sort">
                                                    <option value="">选择排序</option>
                                                    <option value="desc">金额从大到小</option>
                                                    <option value="asc">金额从小到大</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- 缴费时间 -->
                                    <div class="layui-form" style="display: inline-block;">
                                        <div class="layui-form-item">
                                            <div class="layui-inline" style="margin-top:-25px">
                                                <div class="layui-input-inline">
                                                    <text class="yname">订单开始时间</text>
                                                    <input type="text" class="layui-input start-item test-item"
                                                           placeholder="订单开始时间" lay-key="23">
                                                </div>
                                            </div>
                                            <div class="layui-inline" style="margin-top:-25px">
                                                <div class="layui-input-inline">
                                                    <text class="yname">订单结束时间</text>
                                                    <input type="text" class="layui-input end-item test-item"
                                                           placeholder="订单结束时间" lay-key="24">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 搜索 -->
                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:600px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <div class="layui-inline" style="margin-right:0">
                                                <div class="layui-input-inline">
                                                    <text class="yname">订单号</text>
                                                    <input type="text" name="tradeno" placeholder="请输入订单号"
                                                           autocomplete="off" class="layui-input dingdan">
                                                </div>
                                            </div>
                                            <div class="layui-inline">
                                                <div class="layui-input-inline">
                                                    <text class="yname">支付条码单号</text>
                                                    <input type="text" name="paytradeno" placeholder="支付条码单号"
                                                           autocomplete="off" class="layui-input tiaoma">
                                                </div>
                                            </div>

                                            <div class="layui-inline">
                                                <button class="layui-btn layuiadmin-btn-list" lay-submit=""
                                                        lay-filter="LAY-app-contlist-search"
                                                        style="border-radius:5px;margin-bottom: -1.2rem;height:36px;line-height: 36px;">
                                                    <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                                </button>
                                            </div>
                                            <button class="layui-btn export"
                                                    style="border-radius:5px;margin-bottom: -0.8rem;height:36px;line-height: 36px;">
                                                导出
                                            </button>   <!-- onclick="exportdata()" -->
                                        </div>
                                    </div>
                                </div>

                                <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                <!-- 判断状态 -->
                                <script type="text/html" id="statusTap">
                                    @{{#  if(d.pay_status == 1){ }}
                                    <span class="cur">@{{ d.pay_status_desc }}</span>
                                    @{{#  } else { }}
                                    @{{ d.pay_status_desc }}
                                    @{{#  } }}
                                </script>
                                <!-- 判断状态 -->
                                <script type="text/html" id="paymoney">
                                    @{{ d.rate }}%
                                </script>
                                <!-- 积分抵扣 -->
                                <script type="text/html" id="jfdkTap">
                                    @{{ d.dk_jf }}/@{{ d.dk_money }}
                                </script>

                                <!-- 通道类型 -->
                                <script type="text/html" id="company_type">
                                    @{{#  if(d.company=='member'){ }}
                                    会员卡
                                    @{{#  } else if(d.company=='vbilla') { }}
                                    随行付A
                                    @{{#  } else if(d.company=='vbill') { }}
                                    随行付
                                    @{{#  } else if(d.company=='alipay') { }}
                                    支付宝
                                    @{{#  } else if(d.company=='weixin') { }}
                                    微信
                                    @{{#  } else if(d.company=='mybank') { }}
                                    快钱支付
                                    @{{#  } else if(d.company=='herongtong') { }}
                                    和融通
                                    @{{#  } else if(d.company=='newland') { }}
                                    新大陆
                                    @{{#  } else if(d.company=='fuiou') { }}
                                    富友
                                    @{{#  } else if(d.company=='jdjr') { }}
                                    京东聚合
                                    @{{#  } else if(d.company=='dlb') { }}
                                    哆啦宝
                                    @{{#  } else if(d.company=='zft') { }}
                                    花呗分期
                                    @{{#  } else if(d.company=='tfpay') { }}
                                    TF通道
                                    @{{#  } else if(d.company=='hkrt') { }}
                                    海科融通
                                    @{{#  } else if(d.company=='easypay') { }}
                                    易生
                                    @{{#  } else if(d.company=='hltx') { }}
                                    葫芦天下
                                    @{{#  } else if(d.company=='linkage') { }}
                                    联动优势
                                    @{{#  } else if(d.company=='lianfu') { }}
                                    工行
                                    @{{#  } else if(d.company=='changsha') { }}
                                    长沙银行
                                    @{{#  } else if(d.company=='lianfuyouzheng') { }}
                                    邮政
                                    @{{#  } else if(d.company=='wftpay') { }}
                                    威富通
                                    @{{#  } else if(d.company=='hwcpay') { }}
                                    汇旺财
                                    @{{#  } else if(d.company=='weixina') { }}
                                    微信a
                                    @{{#  } else if(d.company=='ysepay') { }}
                                    银盛
                                    @{{#  } else if(d.company=='qfpay') { }}
                                    钱方
                                    @{{#  } else { }}
                                    @{{ d.company }}
                                    @{{#  } }}
                                </script>
                                <!-- 判断状态 -->
                                <script type="text/html" id="moneyTap">
                                    @{{# if(d.receipt_amount== ""){}}
                                    @{{d.total_amount - d.mdiscount_amount}}
                                    @{{# } else{ }}
                                    @{{d.receipt_amount}}
                                    @{{# } }}
                                </script>
                                <script type="text/html" id="table-content-list" class="layui-btn-small">
                                    <a class="layui-btn layui-btn-normal layui-btn-xs tongbu"
                                       lay-event="tongbu">同步状态</a>
                                    <a class="layui-btn layui-btn-normal layui-btn-xs tui" lay-event="tui">退款</a>
                                    <a class="layui-btn layui-btn-normal layui-btn-xs print" lay-event="print">补打小票</a>
                                </script>
                                <div id="refundOpen" class="hide"
                                     style="display: none;background-color:#fff;border-radius:10px;">
                                    <div class="layui-card-body tankuang">
                                        <div class="layui-form">
                                            <div class="layui-form-item">
                                                <label class="layui-form-label">可退金额</label>
                                                <div class="layui-input-block">
                                                    <input type="text" name="" autocomplete="off"
                                                           class="layui-input total_amount" disabled>
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <label class="layui-form-label">退款金额</label>
                                                <div class="layui-input-block">
                                                    <input type="text" name="" placeholder="请输入退款金额" autocomplete="off"
                                                           class="layui-input refund_amount" lay-verify="refund_amount">
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <label class="layui-form-label">支付密码</label>
                                                <div class="layui-input-block">
                                                    <input type="text" name="" placeholder="请输入支付密码" autocomplete="off"
                                                           class="layui-input password" lay-verify="password">
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <div class="layui-input-block">
                                                    <button class="layui-btn submit_refund" lay-submit
                                                            lay-filter="submit_refund">
                                                        立即提交
                                                    </button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<input type="hidden" class="store_id">
<input type="hidden" class="sort">
<input type="hidden" class="stu_class_no">

<input type="hidden" class="stu_order_batch_no">
<input type="hidden" class="user_id">

<input type="hidden" class="pay_status">
<input type="hidden" class="pay_type">
<input type="hidden" class="refund_order_code">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script>
<script type="text/javascript" src="{{asset('/school/js/jsencrypt.min.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Publictoken");
    var str = location.search;
    var store_id = sessionStorage.getItem("store_store_id");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'table', 'laydate'], function () {
        var $ = layui.$
            , admin = layui.admin
            , form = layui.form
            , table = layui.table
            , laydate = layui.laydate;
        // 未登录,跳转登录页面
        $(document).ready(function () {
            if (token == null) {
                window.location.href = "{{url('/mb/login')}}";
            }
        });

        // 选择门店
        $.ajax({
            url: "{{url('/api/merchant/store_lists')}}",
            data: {token: token, l: 100},
            type: 'post',
            success: function (data) {
//                console.log(data);
                var optionStr = "";
                for (var i = 0; i < data.data.length; i++) {
                    optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
                    // optionStr += "<option value='" + data.data[i].store_id + "' "+((store_id==data.data[i].store_id)?"selected":"")+">" + data.data[i].store_name + "</option>";
                }
                $("#agent").append('<option value="">选择门店</option>' + optionStr);
                layui.form.render('select');
            },
            error: function (data) {
                alert('查找板块报错');
            }
        });

        // 渲染表格
        table.render({

            elem: '#test-table-page'
            , url: "{{url('/api/merchant/order')}}"
            , method: 'post'
            , where: {
                token: token
            }
            , request: {
                pageName: 'p',
                limitName: 'l'
            }
            , page: true
            , cellMinWidth: 130
            , cols: [[
                {field: 'pay_time', align: 'center', title: '交易时间'}
                , {field: 'device_id', align: 'center', title: '设备ID'}
                , {field: 'store_name', align: 'center', title: '门店'}
                , {field: 'company', align: 'center', title: '通道类型', toolbar: '#company_type'}
                , {field: 'ways_source_desc', align: 'center', title: '支付方式'}
                // ,{field:'shop_price',align:'center', title: '订单金额'}
                , {field: 'total_amount', align: 'center', title: '支付金额'}
                , {field: 'receipt_amount', align: 'center', title: '实收金额', templet: '#moneyTap'}
                , {field: 'mdiscount_amount', align: 'center', title: '优惠金额'}
                , {field: 'refund_amount', align: 'center', title: '退款金额'}
                , {field: 'rate', align: 'center', title: '费率', templet: '#paymoney'}
                , {field: 'pay_status_desc', align: 'center', title: '状态', templet: '#statusTap'}
                , {field: 'dk_jf', align: 'center', title: '积分抵扣', templet: '#jfdkTap'}
                , {field: 'out_trade_no', align: 'center', title: '订单号'}
                , {field: 'trade_no', align: 'center', title: '支付条码单号'}
                , {field: 'remark', align: 'center', title: '备注'}
                , {width: 220, align: 'center', fixed: 'right', toolbar: '#table-content-list', title: '操作'}
            ]]
            , response: {
                statusName: 'status' //数据状态的字段名称，默认：code

                , statusCode: 1 //成功的状态码，默认：0
                , msgName: 'message' //状态信息的字段名称，默认：msg
                , countName: 't' //数据总数的字段名称，默认：count
                , dataName: 'data' //数据列表的字段名称，默认：data
            }
            , done: function (res, curr, count) {
                $('th').css({
                    'font-weight': 'bold',
                    'font-size': '15',
                    'color': 'black',
                    'background': 'linear-gradient(#f2f2f2,#cfcfcf)'
                });	//进行表头样式设置
            }
            // , text: {
            //   none: '时间跨度不能超过31天'
            // }
        });

        table.on('tool(test-table-page)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var e = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            // console.log(token,e.out_trade_no)
            // sessionStorage.setItem('s_store_id', e.store_id);

            if (layEvent === 'tongbu') { //审核
                $.post("{{url('/api/basequery/update_order')}}",
                    {
                        token: token,
                        store_id: e.store_id,
                        out_trade_no: e.out_trade_no
                    }, function (res) {
                        console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 1
                                , time: 2000
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 2
                                , time: 2000
                            });
                        }
                    }, "json");
            } else if (layEvent === 'tui') {
                layer.confirm('确认是否退款?', {icon: 2}, function (index) {
                    layer.close(index);
                    var amount = e.total_amount - e.refund_amount
                    $('.total_amount').val(amount)
                    $('.refund_amount ').val('')
                    $('.password').val('')
                    $('.refund_order_code').val(e.out_trade_no)

                    layer.open({
                        type: 1,
                        title: '退款信息',
                        closeBtn: 1,
                        area: ['500px', '300px'], //设置宽高
                        //skin: 'layui-layer-rim', //加上边框
                        shadeClose: true,
                        content: $('#refundOpen')
                    });
                });
            } else if (layEvent === 'print') {
                $.post("{{url('api/merchant/yly_print')}}",
                    {
                        token: token,
                        outTradeNo: e.out_trade_no
                    }, function (res) {
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 1
                                , time: 2000
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 2
                                , time: 2000
                            });
                        }
                    }, "json");
            }


            var data = obj.data;
            if (obj.event === 'setSign') {
                layer.open({
                    type: 2,
                    title: '模板详细',
                    shade: false,
                    maxmin: true,
                    area: ['60%', '70%'],
                    content: "{{url('/merchantpc/paydetail?')}}" + e.stu_order_type_no
                });
            }
        });
        $('.submit_refund').on('click', function () {
            if ($('.refund_amount').val() == '') {
                alert("请输入退款金额");
                return;
            }
            if ($('.password').val() == '') {
                alert("请输入支付密码");
                return;
            }
            var password=$('.password').val();
            var refund_amount=$('.refund_amount').val();
            var encrypt = new JSEncrypt();
            var out_trade_no=$('.refund_order_code').val();
            encrypt.setPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4COVutRbOUfQNjvVOzwK49NzHIPRwwksnJ6QtdHwGmdUZiT2HZxVwfotcOjA5aY16D/2Ahq3gLH4yu2y42dS0lfeBMqUcm+bY7aZ54wClm75RI90uc54F8IgMkNz8J/VS9LYI/B4uHVsc+4KK4Ycr8S8O004ExtvQqu2QCl7Aai/WC4URIdCyNm8La2axoA1jjj3SzpytLvP6Z/iHSlx37Y9AMR0V94R13v4BFlMQDG+2REVJsk6LCyzHQfUvJlnsyKey0n/v8DLC070lQzLPYV0jsiit2AUkyURRLxEaZm2C0YYhfrGjl+x8n/kDteZbDVcyn7UsEdSicijv9DXkQIDAQAB");
            var data = encrypt.encrypt('pay_password=' + password);
            $.post("{{url('/api/merchant/check_pay_password')}}",
                {
                    token: token,
                    sign: data
                }, function (data) {
                    if (data.status == 1) {
                        $.post("{{url('/api/merchant/refund')}}",
                            {
                                token: token, out_trade_no: out_trade_no, refund_amount: refund_amount
                            }, function (data) {
                                if (data.status == 1) {
                                    obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                                    layer.closeAll();
                                    layer.msg(data.message, {
                                        offset: '50px'
                                        , icon: 1
                                        , time: 1000
                                    });
                                } else {
                                    layer.closeAll();
                                    layer.msg(data.message, {
                                        offset: '50px'
                                        , icon: 2
                                        , time: 3000
                                    });
                                }
                            }, "json");
                    } else {
                        layer.msg(data.message, {
                            offset: '50px'
                            , icon: 2
                            , time: 3000
                        });
                    }
                }, "json");

        });
        // 选择门店
        form.on('select(agent)', function (data) {
            var store_id = data.value;
            $('.store_id').val(store_id);
            //执行重载
            table.reload('test-table-page', {
                where: {
                    store_id: $(".store_id").val()
                }
            });
            // 选择收银员
            $.ajax({
                url: "{{url('/api/merchant/merchant_lists')}}",
                data: {token: token, store_id: store_id, l: 100},
                type: 'post',
                success: function (data) {
                    console.log(data);
                    var optionStr = "";
                    for (var i = 0; i < data.data.length; i++) {
                        optionStr += "<option value='" + data.data[i].merchant_id + "'>"
                            + data.data[i].name + "</option>";
                    }
                    $("#schooltype").html('');
                    $("#schooltype").append('<option value="">选择收银员</option>' + optionStr);
                    layui.form.render('select');
                },
                error: function (data) {
                    alert('查找板块报错');
                }
            });

        });

        // 选择收银员
        form.on('select(schooltype)', function (data) {
            var user_id = data.value;
            $('.user_id').val(user_id);
            //执行重载
            table.reload('test-table-page', {
                where: {
                    store_id: $('.store_id').val(),
                    merchant_id: $('.user_id').val()
                }
            });
        });
        // 选择排序
        form.on('select(sort)', function (data) {
            var sort = data.value;

            $('.sort').val(sort);
            //执行重载
            table.reload('test-table-page', {
                where: {
                    sort: sort
                }
            });
        });

        // 选择状态
        form.on('select(status)', function (data) {
            var pay_status = data.value;
            $('.pay_status').val(pay_status);
            //执行重载
            table.reload('test-table-page', {
                where: {
                    pay_status: pay_status,
                }
            });
        });
        // 选择支付类型
        form.on('select(type)', function (data) {
            var pay_type = data.value;
            $('.pay_type').val(pay_type);
            //执行重载
            table.reload('test-table-page', {
                where: {
                    ways_source: pay_type
                }
            });
        });

        laydate.render({
            elem: '.start-item'
            , type: 'datetime'
            , done: function (value) {
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: value,
                        time_end: $('.end-item').val()
                    }
                });
            }
        });

        laydate.render({
            elem: '.end-item'
            , type: 'datetime'
            , done: function (value) {
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: $('.start-item').val(),
                        time_end: value
                    }
                });
            }
        });

        form.on('submit(LAY-app-contlist-search)', function (data) {
            var paytradeno = data.field.paytradeno;
            var out_trade_no = data.field.tradeno;
            console.log(data);
            //执行重载
            table.reload('test-table-page', {
                where: {
                    out_trade_no: out_trade_no,
                    trade_no: paytradeno
                }
            });
        });

        // 导出
        function exportdata() {
            var store_id = $('.store_id').val();
            var merchant_id = $('.user_id').val();
            var sort = $('.sort').val();
            var pay_status = $('.pay_status').val();
            var ways_source = $('.pay_type').val();
            var time_start = $('.start-item').val();
            var time_end = $('.end-item').val();
            var out_trade_no = $('.danhao').val();
            var trade_no = $('.tiaoma').val();

            window.location.href = "{{url('/api/export/MerchantOrderExcelDown')}}" + "?token=" + token + "&store_id=" + store_id + "&merchant_id=" + merchant_id + "&sort=" + sort + "&pay_status=" + pay_status + "&ways_source=" + ways_source + "&time_start=" + time_start + "&time_end=" + time_end + "&out_trade_no=" + out_trade_no + "&trade_no=" + trade_no;
        }

        $('.export').click(function () {
            var store_id = $('.store_id').val();
            var merchant_id = $('.user_id').val();
            var sort = $('.sort').val();
            var pay_status = $('.pay_status').val();
            var ways_source = $('.pay_type').val();
            var time_start = $('.start-item').val();
            var time_end = $('.end-item').val();
            var out_trade_no = $('.danhao').val();
            var trade_no = $('.tiaoma').val();

            window.location.href = "{{url('/api/export/MerchantOrderExcelDown')}}" + "?token=" + token + "&store_id=" + store_id + "&merchant_id=" + merchant_id + "&sort=" + sort + "&pay_status=" + pay_status + "&ways_source=" + ways_source + "&time_start=" + time_start + "&time_end=" + time_end + "&out_trade_no=" + out_trade_no + "&trade_no=" + trade_no;
        })

    });

</script>

</body>
</html>
