<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>微信跳转链接</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/inputTags/inputTags.css')}}">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/formSelects-v4.css')}}" media="all">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .tongbu {
            background-color: #4c9ef8;
            color: #fff;
        }

        .cur {
            color: #009688;
        }

        .yname {
            font-size: 13px;
            color: #444;
        }

        .xgrate {
            color: #fff;
            font-size: 15px;
            padding: 7px;
            height: 30px;
            line-height: 30px;
            background-color: #3475c3;
        }

        .input_block {
            display: block;
        }
    </style>
</head>

<body>

    <div class="layui-fluid" style="margin-top:50px;">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">

                <div class="layui-fluid">
                    <div class="layui-row layui-col-space15">
                        <div class="layui-col-md12">
                            <div class="layui-card">
                                <div class="layui-card-header">获取微信小程序跳转链接</div>

                                <div class="layui-card-body">
                                    <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/html" id="table-content-list">
        <a class="layui-btn layui-btn-normal layui-btn-xs storecode" lay-event="storecode">获取地址</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs setdatacode" lay-event="setdatacode">输入门店id</a>
    </script>

    <input type="hidden" class="classname">

    <script src="{{asset('/userweb/layuinew/layui.js')}}"></script>
    <script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script>
    <script src="{{asset('/layuiadmin/layui/inputTags/inputTags.js')}}"></script>
    <script src="{{asset('/layuiadmin/layui/jquery.qrcode.min.js')}}"></script>
    <script>
        var token = sessionStorage.getItem("Publictoken");
        var str = location.search;
        var store_id = sessionStorage.getItem("store_id");

        layui.config({
            base: '../../layuiadmin/', //静态资源所在路径
        }).extend({
            index: 'lib/index',//主入口模块
            formSelects: 'formSelects'
        }).use(['index', 'form', 'table', 'laydate', "upload",'formSelects'], function() {
            var $ = layui.$,
                admin = layui.admin,
                form = layui.form,
                table = layui.table,
                laydate = layui.laydate,
                upload = layui.upload,
                formSelects = layui.formSelects;
                formSelects.render('range');
                formSelects.btns('range', []);

            // 未登录,跳转登录页面
            $(document).ready(function() {
                if (token == null) {
                    window.location.href = "{{url('/mb/login')}}";
                }
            });

            // 渲染表格
            table.render({
                elem: '#test-table-page',
                url: "{{url('/api/merchant/getStoreWechatAppletInfo')}}", //接口需要替换
                method: 'post',
                where: {
                    token: token,
                    store_id: store_id,
                },
                request: {
                    pageName: 'p',
                    limitName: 'l'
                },
                page: true,
                cellMinWidth: 150,
                cols: [
                    [
                        {align: 'center',field: 'applet_name',title: '小程序名称'},
                        {align: 'center',field: 'applet_appid',title: '小程序APPID'},
                        {align: 'center',field: 'pegesrc',title: '跳转路径'},    //pages/payment/payment
                        {align: 'center',field: 'pegeurl',title: '跳转地址'},
                        {align: 'center',toolbar: '#table-content-list',title: '操作'}
                    ]
                ],
                response: {
                     statusName: 'status' //数据状态的字段名称，默认：code
                    ,statusCode: 1 //成功的状态码，默认：0
                    ,msgName: 'message' //状态信息的字段名称，默认：msg
                    ,countName: 't' //数据总数的字段名称，默认：count
                    ,dataName: 'data' //数据列表的字段名称，默认：data
                },
                parseData(data){

                    let listres = data.data;
                    if(listres.length > 0){  
                        for(let i=0;i<listres.length;i++){
                            listres[i]["pegesrc"] = "pages/payment/payment";
                            listres[i]["pegeurl"] = "请获取";
                        }
                        return {
                            "status": 1,
                            "data":listres,
                            "t":10
                        }
                    }else{
                        return { 
                            "status": 1,
                            "data":listres,
                            "t":10
                        }
                    }

                },
                done: function(res, curr, count) {
                    $('th').css({'font-weight': 'bold','font-size': '15','color': 'black','background':' #FAFAFA'}); //进行表头样式设置
                }
            });


            table.on('tool(test-table-page)', function(obj) {

                if(obj.event == "storecode"){
                    layer.prompt({title: '请输入密码', formType: 1}, function(pass, index){
                        if(pass == "123123" ){
                            layer.close(index);
                            $.post("/api/customer/weixin/getWechatAppletScheme",{
                                token: token,
                                store_id:window.sessionStorage.getItem("getwechatpagestoreid") ? window.sessionStorage.getItem("getwechatpagestoreid") : store_id,
                                path:"pages/payment/payment",
                            },res => {
                                if(res.status == 200){
                                    if(res.data.errcode == 61007){
                                        layer.msg("小程序未授权", {icon:2, shade:0.5, time:2000});
                                    }else if(res.data.errcode == 40165){
                                        layer.msg("无效的页面路径", {icon:2, shade:0.5, time:2000});
                                    }else if(res.data.errcode == 0){ 
                                        obj.update({
                                            pegeurl: res.data.openlink
                                        })
                                    }
                                }else{
                                    layer.msg(res.message, {icon:2, shade:0.5, time:2000});
                                }
                            })
                        }else{
                            layer.msg("密码错误", {icon:2, shade:0.5, time:2000});
                        }
                    })
                }else if(obj.event == "setdatacode"){

                    let store_data = "";
                    if(window.sessionStorage.getItem("getwechatpagestoreid")){
                        store_data = window.sessionStorage.getItem("getwechatpagestoreid");
                    }else{
                        store_data = store_id;
                    }

                    layer.prompt({title: '请输入门店id', value:store_data,formType: 3}, function(pass, index){
                        window.sessionStorage.setItem("getwechatpagestoreid",pass);
                        layer.close(index);
                    })

                }









            })
        });
    </script>
</body>

</html>