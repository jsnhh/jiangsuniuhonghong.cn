<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>修改商品</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/formSelects-v4.css')}}" media="all">
    <style>
        #demo1 img{width: 100%;height: 100%;}
        .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
        .img_box{position: relative;width:13%;height:10%;display: inline-block; margin-right: 10px;}
        .img_box span{position: absolute;right:0;top:0;font-size: 30px;background: #fff;cursor: pointer;}
        .img_box2 span{position: absolute;right:0;top:0;font-size: 30px;background: #fff;cursor: pointer;}
        video{
            width:200px;
        }
        .lianjiecon div{
            width:100%;
            overflow: hidden;
            margin-bottom:20px;
        }
        .lianjiecon label{
            display: inline-block;
            float: left;
            width:10%;
            line-height: 36px;
        }
        .lianjiecon input{
            display: inline-block;
            float: left;
            width:90%;
        }
    </style>
</head>
<body>

<div class="layui-fluid" style="margin-top:50px;">
    <div class="layui-card">
        <div class="layui-card-header">修改商品</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item">
                    <label class="layui-form-label">商品名称</label>
                    <div class="layui-input-block">
                        <input type="text" placeholder="请输入商品名称:" class="layui-input title">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">副标题</label>
                    <div class="layui-input-block">
                        <input type="text" name="sub_title" placeholder="请输入商品副标题:" class="layui-input sub_title">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">所属门店</label>
                    <div class="layui-input-block">
                        <select name="store" id="store" lay-filter="store">
                        </select>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">商品价格</label>
                    <div class="layui-input-block">
                        <input type="text" name="goods_price" placeholder="请输入商品价格:" class="layui-input goods_price">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">图片</label>
                    <div class="layui-input-block">
                        <div class="layui-upload">
                            <button class="layui-btn up"><input type="file" name="img_upload" class="test1">上传图片</button>
                            <blockquote class="layui-elem-quote layui-quote-nm" style="margin-top: 10px;">
                                预览图：
                                <div class="layui-upload-list" id="demo1">
                                    <div class="img_box" data="">
                                        <img src="" alt="" class="layui-upload-img pre_img">
                                        <span>×</span>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">状态</label>
                    <div class="layui-input-block" id="goods_status">
                        <input type="radio" name="status" value="0" title="下架">
                        <input type="radio" name="status" value="1" title="上架" checked>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">排序</label>
                    <div class="layui-input-block">
                        <input type="text" title="sort" name="sort" placeholder="排序（1--255，数字越小越靠前）" class="layui-input sort">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">商品描述</label>
                    <div class="layui-input-block">
                        <textarea name="desc" placeholder="请输入内容商品描述" class="layui-textarea desc"></textarea>
                    </div>
                </div>

                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit">确定提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


</div>

<input type="hidden" class="store-id" value="">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<!-- <script src="{{asset('/layuiadmin/modules/formSelects.js')}}"></script> -->
<script>
    var token = sessionStorage.getItem("Publictoken");

    // var ad_p_id = sessionStorage.getItem("ad_p_id");
    // var user_ids = sessionStorage.getItem("user_ids");
    var store_key_ids = sessionStorage.getItem("store_key_ids");
    var str = location.search;
    var id = str.split('?')[1];

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form', 'upload', 'formSelects', 'laydate'], function(){
        var $ = layui.$
            // ,admin = layui.admin
            // ,element = layui.element
            ,layer = layui.layer
            // ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;

            formSelects.render('store');
            formSelects.btns('store', []);
            // var arrp=[];
            var arrs=[];

//单条信息------------------------------------------------------------------------
        // 选择门店
        $.ajax({
            url : "{{url('/api/merchant/store_lists')}}",
            data : {
                token:token
                ,l:100
            },
            type : 'post',
            datatype:'json',
            success : function(data) {
                console.log(data);
                // $('.store_id').val(data.data[0].store_id);

                var optionStr = "";
                for(var i=0; i<data.data.length; i++){
                    optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
                }
                $("#store").append('<option value="">请选择门店</option>'+optionStr);
                layui.form.render('select');
            },
            error : function(data) {
                alert('选择门店报错');
            }
        });


        $.post("{{url('/api/merchant/goods_info')}}",
        {
            token:token
            ,id:id
        },function(res){
            // console.log(res);
            //     alert(res.data.store_id)
            //     var select = '';
            $('.title').val(res.data.title);
            $('.sub_title').val(res.data.sub_title);
            $('.goods_price').val(res.data.price);
            $('.sort').val(res.data.goods_sort);
            $('.desc').val(res.data.goods_desc);
            // $('.position_name').val(res.data.ad_p_desc);
            // $('.store-id').val(res.data.store_key_ids);
            $(".pre_img").attr('src',res.data.images);

            var select = 'dd[lay-value=' + res.data.store_id + ']';
            $('#store').siblings("div.layui-form-select").find('dl').find(select).click();
            // $('#store').val(res.data.store_id);

            //商品状态单选状态
            $("input[name='status'][value='0']").attr("checked", res.data.status == 0 ? true : false);
            $("input[name='status'][value='1']").attr("checked", res.data.status == 1 ? true : false);
            form.render('radio'); //更新全部

            var select_store = res.data.store_id;
            var str = res.data.imgs;
            var data = JSON.parse(str);
            var html = '';
            // console.log(data);

            // for(var i=0; i<data.length; i++){
            //     html+='<div class="img_box" data=""><img src="'+ data[i].img_url +'" alt="'+ data[i].click_url +'" class="layui-upload-img"><span>×</span><input type="text" class="layui-input url" placeholder="点击图片跳转链接" value="'+data[i].click_url+'"></div>';
            // }
            html = '<div class="img_box"><img src="'+ data.img_url +'" class="layui-upload-img"><span>×</span></div>';
            $('#demo1').append(html);
        });
//-----------------------------------------------------------------------
        // 门店
        // {{--formSelects.config('store', {--}}
        //     {{--beforeSuccess: function(id, url, searchVal, result){--}}
        //         {{--//我要把数据外层的code, msg, data去掉--}}

        //         {{--result = result.data;--}}
        //         {{--// console.log(result);--}}
        //         {{--if(result != ''){--}}
        //             {{--alert(22)--}}
        //             {{--for(var i=0; i<result.length; i++){--}}
        //                 {{--var data = {--}}
        //                     {{--"value":result[i].id--}}
        //                     {{--,"name":result[i].store_name--}}
        //                 {{--};--}}
        //                 {{--arrs.push(data);--}}
        //             {{--}--}}
        //             {{--//然后返回数据--}}
        //             {{--return arrs;--}}
        //         {{--}else{--}}
        //             {{--$('#store').html('');--}}
        //         {{--}--}}
        //         {{--// console.log(arr);--}}
        //     {{--},--}}
        //     {{--success: function(id, url, searchVal, result){    //使用远程方式的success回调--}}
        //         {{--var stuclass = store_key_ids.split(',');//转换数组--}}
        //         {{--formSelects.value('store', stuclass);--}}
        //         {{--console.log(stuclass);--}}
        //     {{--}--}}
        //     {{--// clearInput: true--}}
        // {{--}).data('store', 'server', {--}}
        //     {{--url:"{{url('/api/merchant/store_lists?token=')}}"+token--}}
        // {{--});--}}

        //多图片上传
        upload.render({
            url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token
            ,elem: '.test1'
            ,method : 'POST'
            ,type : 'images'
            ,ext : 'jpg|png|gif'
            ,multiple: true
            ,before: function(obj){
                //预读本地文件示例，不支持ie8
                obj.preview(function(index, file, result){
                    $('#demo1').append('<div class="img_box" data=""><img src="'+ result +'" alt="'+ file.name +'" class="layui-upload-img"><span>×</span></div>');

                    $("#demo1 .img_box").each(function(){
                        var index = $(this).index()+1;
                        $(this).attr('data', index);
                    })
                });
            }
            ,done: function(res){
                // console.log(res);
                $("#demo1 .img_box:last-child").find('img').attr('src', res.data.img_url);
            }
        });

        $('.submit').on('click', function(){
            var adarr=[];
             // console.log(layui.formSelects.value('store', 'valStr'));
             // $('.position_id').val(layui.formSelects.value('position', 'valStr'));
             // $('.position_name').val(layui.formSelects.value('position', 'nameStr'));

             $('.store-id').val(layui.formSelects.value('store', 'valStr'));

             // var daTa = {
             //     "img_url":""
             //     ,"click_url":""
             // };
             $('#demo1 .img_box').each(function(index,item){
                var img_url = $(item).find('img').attr('src');
                var click_url = $(item).find('input').val();

                var data = {
                    "img_url":img_url
                    ,"click_url":click_url
                }; //构造数组
                adarr.push(data)
            });
            var adarrJson = JSON.stringify(adarr);//转化成json格式
            // console.log(adarrJson);

            $.post("{{url('api/merchant/edit_goods')}}",
            {
                token:token,
                id:id,
                title:$('.title').val(),
                sub_title:$('.sub_title').val(),
                // store_id:$('.store_id').val(),
                store_id:$('#store').val(),
                goods_price:$('.goods_price').val(),
                imgs:adarrJson,
                goods_status:$('#goods_status input[name="status"]:checked ').val(), //获取商品状态
                goods_sort:$('.sort').val(),
                goods_desc:$('.desc').val()
            },function(res){
                // console.log(res);
                if(res.status==1){
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 3000
                    });
                }else{
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 3000
                    });
                }
            },"json");
        });

        // 删除上传的图片
        $('#demo1').on('click','.img_box span',function(){
            $(this).parent().remove();
        });

    });
</script>
</body>
</html>
