<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>查看商品信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
        .img{width:130px;height:90px;overflow: hidden;}
        .img img{width:100%;height:100%;}
    </style>
</head>
<body>

<div class="layui-fluid" style="margin-top:50px;">
    <div class="layui-card">
        <div class="layui-card-header">商品信息</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-row layui-form" lay-filter="component-form-group">
            <div class="layui-col-md6">
                <div class="layui-form-item">
                    <label class="layui-form-label">商品名称</label>
                    <div class="layui-input-block">
                        <div class="layui-form-mid"></div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">商品副标题</label>
                    <div class="layui-input-block">
                        <div class="layui-form-mid"></div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">门店</label>
                    <div class="layui-input-block">
                        <div class="layui-form-mid"></div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">图片</label>
                        <div class="img_box_con">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">价格</label>
                    <div class="layui-input-block">
                        <div class="layui-form-mid"></div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">状态</label>
                    <div class="layui-input-block">
                        <div class="layui-form-mid"></div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">添加时间</label>
                    <div class="layui-input-block">
                        <div class="layui-form-mid"></div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">更新时间</label>
                    <div class="layui-input-block">
                        <div class="layui-form-mid"></div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">商品描述</label>
                    <div class="layui-input-block">
                        <div class="layui-form-mid"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="img_box"></div>

<input type="hidden" class="schooltypeid" value="">
<input type="hidden" class="gradeid" value="">
<input type="hidden" class="classid" value="">
<input type="hidden" class="statusid" value="">
<input type="hidden" class="relationshipid" value="">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Publictoken");

    var str = location.search;
    var id = str.split('?')[1];

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token == null){
                window.location.href="{{url('/mb/login')}}";
            }
        });

        $.post("{{url('/api/merchant/goods_info')}}",
        {
            token:token
            ,id:id
        },function(res){
            // console.log(res);
            if(res.status==1){
                $('.layui-row .layui-form-item').eq(0).find('.layui-form-mid').html(res.data.title); //商品名称
                $('.layui-row .layui-form-item').eq(1).find('.layui-form-mid').html(res.data.sub_title); //商品副标题
                $('.layui-row .layui-form-item').eq(2).find('.layui-form-mid').html(res.data.store_name); //门店名
                // $('.layui-row .layui-form-item').eq(3).find('.layui-form-mid').append('<img src="'+res.data.images+'">'); //图片
                $('.layui-row .layui-form-item').eq(4).find('.layui-form-mid').html(res.data.price);  //价格
                $('.layui-row .layui-form-item').eq(5).find('.layui-form-mid').html(res.data.status); //状态
                $('.layui-row .layui-form-item').eq(6).find('.layui-form-mid').html(res.data.created_at); //添加时间
                $('.layui-row .layui-form-item').eq(7).find('.layui-form-mid').html(res.data.updated_at); //更新时间
                $('.layui-row .layui-form-item').eq(8).find('.layui-form-mid').html(res.data.goods_desc); //描述
                $('.img_box_con').html('<img src="'+res.data.images+'">');

                // var str = res.data.images;
                // var data = JSON.parse(str);
                // console.log(data);
                // var html='';
                // var str='';
                // var strs='';
                // for(var i=0; i<data.length; i++){
                //     html+='<div id="sfz_z" class="hide" style="display: none"><img style="width:100%;height:100%" src="'+data[i].img_url+'"></div>';
                // }
                // $('.img_box_con').append(html);

                // for(var j=0; j<data.length; j++){
                //     str+='<div class="layui-input-block" style="width:130px;display: inline-block;">';
                //     str+='<div class="img sfz_z"><img data-type="test" src="'+data[j].img_url+'"></div>';
                //     str+='<label class="layui-form-label" style="padding:9px 0">'+data[j].click_url+'</label>';
                //     str+='</div>';
                // }

                // $('.img_box_con').append(str);
            }
        },"json");

        $('.sfz_z').on("click",function(){
            layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area: '516px',
                skin: 'layui-layer-nobg', //没有背景色
                shadeClose: true,
                content: $('#sfz_z')
            });
        });

    });
</script>
</body>
</html>
