<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>积分设置</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
        .img{width:130px;height:90px;overflow: hidden;}
        .img img{width:100%;height:100%;}
        .layui-layer-nobg{width: none !important;}
        /*.layui-layer-content{width:600px;height:550px;}*/
        .layui-card-header{width:200px;text-align: left;float:left;}
        .layui-card-body{margin-left:28px;}
        .layui-upload-img{width: 120px; height: 120px; /*margin: 0 10px 10px 0;*/}

        .up{position: relative;display: inline-block;cursor: pointer;border-color: #1ab394; color: #FFF;width: auto !important;font-size: 10px !important;text-align: center !important;}
        .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
        .layui-upload-list{width: 120px;height:120px;overflow: hidden;margin: 10px auto;}
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {-webkit-appearance: none !important;margin: 0;}

        .edit{display: none}
        .forget_mima{display: none}
    </style>
</head>
<body>

<div class="layui-fluid" style="margin-top:68px;">

    <div class="layui-card">
      <div class="layui-card-header">设置支付密码</div>
      <div class="layui-card-body layui-row layui-col-space10">
        <div class="layui-form">

            <!-- 设置密码 -->
            <div class="layui-form-item set">
                <label class="layui-form-label">输入密码</label>
                <div class="layui-input-block">
                    <input type="password" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入支付输入密码" class="layui-input zhifucode1">
                </div>
            </div>
            <div class="layui-form-item set">
                <label class="layui-form-label">确认密码</label>
                <div class="layui-input-block">
                    <input type="password" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请再次确认支付密码" class="layui-input zhifucode2">
                </div>
            </div>
            <!-- 修改密码 -->
            <div class="layui-form-item edit">
                <label class="layui-form-label">输入旧密码</label>
                <div class="layui-input-block">
                    <input type="password" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入旧密码" class="layui-input zhifucode11">
                </div>
            </div>
            <div class="layui-form-item edit">
                <label class="layui-form-label">确认新密码</label>
                <div class="layui-input-block">
                    <input type="password" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入新密码" class="layui-input zhifucode22">
                </div>
            </div>

        </div>



        <div class="layui-form-item layui-layout-admin">
            <div class="layui-input-block">
                <div class="layui-footer" style="left: 0;">
                    <button class="layui-btn submit">保存</button>
                    <!--<button type="reset" class="layui-btn layui-btn-primary">重置</button>-->
                </div>
            </div>
        </div>
        </div>
      </div>
    </div>

</div>




<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script type="text/javascript" src="{{asset('/school/js/jsencrypt.min.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Publictoken");


    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'table','form','upload'], function(){
        var $ = layui.$
            admin = layui.admin
            ,table = layui.table
            ,element = layui.element
            ,upload = layui.upload
            ,form = layui.form;

            element.render();
        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }

            $.post("{{url('/api/merchant/is_pay_password')}}",
            {
                token:token

            },function(res){
                console.log(res);
                if(res.data.is_pay_password == 1){//修改
                    $('.edit').show()
                    $('.set').hide()

                    $('.submit').on('click', function(){
                        var encrypt = new JSEncrypt();
                        encrypt.setPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4COVutRbOUfQNjvVOzwK49NzHIPRwwksnJ6QtdHwGmdUZiT2HZxVwfotcOjA5aY16D/2Ahq3gLH4yu2y42dS0lfeBMqUcm+bY7aZ54wClm75RI90uc54F8IgMkNz8J/VS9LYI/B4uHVsc+4KK4Ycr8S8O004ExtvQqu2QCl7Aai/WC4URIdCyNm8La2axoA1jjj3SzpytLvP6Z/iHSlx37Y9AMR0V94R13v4BFlMQDG+2REVJsk6LCyzHQfUvJlnsyKey0n/v8DLC070lQzLPYV0jsiit2AUkyURRLxEaZm2C0YYhfrGjl+x8n/kDteZbDVcyn7UsEdSicijv9DXkQIDAQAB");
                        var data = encrypt.encrypt('old_pay_password='+$('.zhifucode11').val());
                        var data1 = encrypt.encrypt('old_pay_password='+$('.zhifucode11').val()+'&new_pay_password='+$('.zhifucode22').val());


                        $.post("{{url('/api/merchant/edit_pay_password')}}",
                        {
                            token:token,
                            sign:data

                        },function(res){
                            console.log(res);
                            if(res.status==1){
                                $.post("{{url('/api/merchant/edit_pay_password')}}",
                                {
                                    token:token,
                                    sign:data1

                                },function(res){
                                    console.log(res);
                                    if(res.status==1){
                                        layer.msg(res.message, {
                                            offset: '50px'
                                            ,icon: 1
                                            ,time: 3000
                                        });
                                    }else{
                                        layer.msg(res.message, {
                                            offset: '50px'
                                            ,icon: 2
                                            ,time: 3000
                                        });
                                    }
                                },"json");
                            }else{
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 2
                                    ,time: 3000
                                });
                            }
                        },"json");
                    });


                }else{
                    $('.edit').hide()
                    $('.set').show()
                    $('.submit').on('click', function(){
                        var encrypt = new JSEncrypt();
                        encrypt.setPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4COVutRbOUfQNjvVOzwK49NzHIPRwwksnJ6QtdHwGmdUZiT2HZxVwfotcOjA5aY16D/2Ahq3gLH4yu2y42dS0lfeBMqUcm+bY7aZ54wClm75RI90uc54F8IgMkNz8J/VS9LYI/B4uHVsc+4KK4Ycr8S8O004ExtvQqu2QCl7Aai/WC4URIdCyNm8La2axoA1jjj3SzpytLvP6Z/iHSlx37Y9AMR0V94R13v4BFlMQDG+2REVJsk6LCyzHQfUvJlnsyKey0n/v8DLC070lQzLPYV0jsiit2AUkyURRLxEaZm2C0YYhfrGjl+x8n/kDteZbDVcyn7UsEdSicijv9DXkQIDAQAB");
                        var data = encrypt.encrypt('pay_password='+$('.zhifucode1').val()+'&pay_password_confirmed='+$('.zhifucode2').val());


                        $.post("{{url('/api/merchant/add_pay_password')}}",
                        {
                            token:token,
                            sign:data

                        },function(res){
                            console.log(res);
                            if(res.status==1){
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 1
                                    ,time: 3000
                                });
                            }else{
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 2
                                    ,time: 3000
                                });
                            }
                        },"json");

                    });
                }
            },"json");
        });


        //发送验证码
        var InterValObj; //timer变量，控制时间
        var count = 60; //间隔函数，1秒执行
        var curCount;//当前剩余秒数

        $('#btnSendCode').click(function(){
          var encrypt = new JSEncrypt();
          var phone=$('.js-tel').val();
          encrypt.setPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4COVutRbOUfQNjvVOzwK49NzHIPRwwksnJ6QtdHwGmdUZiT2HZxVwfotcOjA5aY16D/2Ahq3gLH4yu2y42dS0lfeBMqUcm+bY7aZ54wClm75RI90uc54F8IgMkNz8J/VS9LYI/B4uHVsc+4KK4Ycr8S8O004ExtvQqu2QCl7Aai/WC4URIdCyNm8La2axoA1jjj3SzpytLvP6Z/iHSlx37Y9AMR0V94R13v4BFlMQDG+2REVJsk6LCyzHQfUvJlnsyKey0n/v8DLC070lQzLPYV0jsiit2AUkyURRLxEaZm2C0YYhfrGjl+x8n/kDteZbDVcyn7UsEdSicijv9DXkQIDAQAB");
          var data = encrypt.encrypt('type=editpassword&info=2&phone='+phone);


          curCount = count;
          //设置button效果，开始计时
          $("#btnSendCode").attr("disabled", "true");
          $("#btnSendCode").val(curCount+'(s)');
          InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次
          //向后台发送处理数据
          $.post("{{url('/api/Sms/send')}}",
              {
                  sign:data
              },
              function (res){
                  console.log(res);
                  if(res.status==1){
                      layer.msg(res.message);
                      $("#btnSendCode").attr("disabled", "true");
                      $("#btnSendCode").html(curCount+'(s)');
                      window.clearInterval(InterValObj);//停止计时器
                      InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次
                  }else{
                      $("#btnSendCode").html("获取验证码");
                      window.clearInterval(InterValObj);//停止计时器
                      $("#btnSendCode").removeAttr("disabled");//启用按钮
                      alert(res.message);
                  }
              },'json');
        })

        //timer处理函数
        function SetRemainTime() {
            if (curCount == 0) {
                window.clearInterval(InterValObj);//停止计时器
                $("#btnSendCode").removeAttr("disabled");//启用按钮
                $("#btnSendCode").html("获取验证码");
            }
            else {
                curCount--;
                $("#btnSendCode").html(curCount+'(s)');
            }
        }







    });
</script>
</body>
</html>
