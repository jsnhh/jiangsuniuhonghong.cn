<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>小程序</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/modules/layuiicon/iconfont.css')}}" media="all">
    <style type="text/css">
        .xgrate {
            color: #fff;
            font-size: 15px;
            padding: 7px;
            height: 30px;
            line-height: 30px;
            background-color: #3475c3;
        }

        .up {
            position: relative;
        }

        .up #uploadFile {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            opacity: .01;
            width: 100px;
            height: 30px;
        }

        .up input[type=file] {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            opacity: .01;
            width: 100px;
            height: 30px;
        }

        .demo5 {
            width: 100px;
        }

        .box1 {
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            padding: 15px;
            box-shadow: 0 1px 3px rgba(0, 0, 0, .02), 0 16px 32px -4px rgba(0, 0, 0, .17);
        }

        .box2 {
            margin-left: 20px;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            padding: 15px;
            box-shadow: 0 1px 3px rgba(0, 0, 0, .02), 0 16px 32px -4px rgba(0, 0, 0, .17);
        }

        .person {
            width: 100%;
            height: 100px;
            text-align: center;
            line-height: 20px;
            color: #000;
            font-size: 14px;
        }

        .athorize {
            margin-top: 20px;
            width: 552px;
            height: 40px;
            line-height: 40px;
            text-align: center;
            font-size: 14px;
            color: #fff;
            background-color: #00a3fe;
        }

        .athorize:hover {
            cursor: pointer;
        }

        .button {
            margin: 20px auto;
            text-align: center;
        }
        #addwechatapp img{
            display: block;
            margin: 0 auto;
        }
        .shenhe{height: 32px;font-size: 24px;font-weight: 500;color: #333333;line-height: 32px; padding-top:24px;text-align: center;}
        .texttip{margin:0 auto;width: 373px;height: 20px;font-size: 14px;font-weight: 400;color: #666666;line-height: 20px;padding-top:8px;}
        .faliedinfo{margin:0 auto;width: 85%;background: rgba(0, 0, 0, 0.02);padding:24px;}
        .yuanyin{font-size: 16px;font-weight: 500;color: #333333;line-height: 24px;padding-bottom:20px;}
        .wetchinfo{display:flex;justify-content:space-around;margin-left: -8%;margin-top: 16px;}
        p{padding-bottom:14px;height: 22px;font-size: 14px;font-weight: 400;color: #666666;line-height: 22px;}
        .logo{width:138px;height:138px;background-color:#EBB6B6;}
        .qcode{width:300px;height:300px;background-color:#EBB6B6;}
        .wetchtitle{font-size: 16px;font-weight: 500;color: #333333;line-height: 24px;margin-left:20px;}
    </style>
</head>

<body>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-header">微信小程序</div>
                    <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief" style="height:100%;padding-bottom:30px;">
                        <div class="layui-tab-content">
                            <!-- 创建 -->
                            <div class="layui-tab-item layui-show" id="addwechatapp">
                                <img src="{{asset('/mb/success.png')}}">
                            </div>
                            <div class="shenhe">小程序上架成功</div>
                            <div class="button">
                                <button type="submit" class="layui-btn" lay-submit="" lay-filter="updatexcx" id="updatexcx" style="background-color:#1890FF;color:#fff;border-radius: 2px;">更新微信小程序</button>
                            </div>
                            </div>
                            <div class="faliedinfo">
                               <div class="wetchtitle">微信小程序</div>
                              <div class="wetchinfo">
                                  <div class="wetchdetails">
                                    <p>小程序名称：<text class="name"></text></p>
                                    <p>当前应用版本：<text class="banben"></text></p>
                                    <p>商家电话：<text class="storephone"></text></p>
                                    <p>模版ID：<text class="muban"></text></p>
                                    <p>版本描述：<text class="miaoshu"></text></p>
                                    <p>小程序LOGO：</p>
                                    <div class="logo">
                                       <img style="width:138px;height:138px;" src="" alt="" id="demo5">
                                    </div>
                                  </div>
                                  <div class="wetchdetails">
                                    <p>企业名称：<text class="enterprisename"></text></p>
                                    <p>信用代码：<text class="credit"></text></p>
                                    <p>法人微信号：<text class="wacthnum"></text></p>
                                    <p>一级类目：<text class="frist"></text></p>
                                    <p>二级类目：<text class="second"></text></p>
                                    <p>经营执照：</p>
                                    <div class="logo">
                                       <img style="width:138px;height:138px;" src="" alt="" id="demo6">
                                    </div>
                                  </div>
                                  <div class="wetchdetails">
                                    <div class="yuanyin">小程序二维码:</div>
                                    <div class="qcode">
                                       <img style="width:300px;height:300px;" src="" alt="" id="demo7">
                                    </div>
                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
</body>

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script src="{{asset('/js/qrcode.min.js')}}"></script>

<script type="text/javascript">
    var authorizer_appid = ""
    var refresh_token = ""
    var token = sessionStorage.getItem("Publictoken");
    var store_id = sessionStorage.getItem("store_id");
    var cateGory = [];
    var exter_list = [];
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
    }).use(['index', 'form', 'upload', 'table', 'laydate'], function() {
        var $ = layui.$,
            admin = layui.admin,
            form = layui.form,
            table = layui.table,
            laydate = layui.laydate,
            upload = layui.upload
       /*
         *点击重新新创建
         */
        form.on('submit(updatexcx)', function(data) {
            //更新步骤
            $.ajax({
                url: "{{url('/api/customer/weixin/updateAppletsStatus')}}",
                data: {
                    store_id: store_id,
                    type: 1,
                    created_step: 4
                },
                type: 'post',
                success: function(data) {
                    if (data.status == 200) {
                        console.log("步骤状态更新成功")
                        window.parent.location.reload();
                    }else{
                        console.log("步骤状态更新失败")
                    }
                }
            })
        })
        EchoInfo();
        //信息展示
        function EchoInfo(){
            $.ajax({
                url: "{{url('/api/customer/weixin/getAppletsStatusByStore')}}",
                data: {
                    store_id: store_id,
                    type: 1
                },
                type: 'post',
                success: function(data) {
                    var vul = data.data.applet_info
                    layui.jquery('#demo5').attr("src", vul.logo);
                    $("#demo5").html();
                    layui.jquery('#demo6').attr("src", vul.license);
                    $("#demo6").html();
                    $('.name').html(vul.nick_name);
                    $('.banben').html(vul.user_version);
                    $('.storephone').html(vul.component_phone);
                    $('.muban').html(vul.template_id);
                    $('.miaoshu').html(vul.store_applets_desc);
                    $('.enterprisename').html(vul.name);
                    $('.credit').html(vul.code);
                    $('.wacthnum').html(vul.legal_persona_wechat);
                    $('.frist').html(vul.first_class);
                    $('.second').html(vul.second_class);
                }
            });
        }

        $.ajax({
            url: "{{url('/api/customer/weixin/createAppletQrcode')}}",
            data: {
                token:token,
                store_id: store_id,
                path: "pages/index/index",
            },
            type: 'post',
            success: function(data) {
                var url = data.data.qrcode_url;
                layui.jquery('#demo7').attr("src", url);
                $("#demo7").html();
            }
        });


    });
</script>

</html>