<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>积分设置</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
        .img{width:130px;height:90px;overflow: hidden;}
        .img img{width:100%;height:100%;}
        .layui-layer-nobg{width: none !important;}
        /*.layui-layer-content{width:600px;height:550px;}*/
        .layui-card-header{width:200px;text-align: left;float:left;}
        .layui-card-body{margin-left:28px;}
        .layui-upload-img{width: 120px; height: 120px; /*margin: 0 10px 10px 0;*/}

        .up{position: relative;display: inline-block;cursor: pointer;border-color: #1ab394; color: #FFF;width: auto !important;font-size: 10px !important;text-align: center !important;}
        .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
        .layui-upload-list{width: 120px;height:120px;overflow: hidden;margin: 10px auto;}
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {-webkit-appearance: none !important;margin: 0;}
        .boss{color:red;line-height: 39px;}
    </style>
</head>
<body>

<div class="layui-fluid" style="margin-top:68px;">

    <div class="layui-card">
      <div class="layui-card-header">积分设置</div>
      <div class="layui-card-body layui-row layui-col-space10">
        <div class="layui-form">
            <div class="layui-form-item">
                <label class="layui-form-label">所属门店</label>
                <div class="layui-input-block">
                    <select name="store" id="store" lay-filter="store">

                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">积分条件</label>
                <div class="layui-input-block">
                    <select name="jifen" id="jifen" lay-filter="jifen">

                    </select>
                </div>
            </div>
            <!-- <div class="layui-form-item">
                <label class="layui-form-label">使用规则</label>
                <div class="layui-input-block">
                    <select name="guize" id="guize" lay-filter="guize">

                    </select>
                </div>
            </div>  -->
            <div class="layui-form-item">
              <label class="layui-form-label">使用规则</label>
              <div class="layui-input-block boss">
                待开发
              </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">新会员送积分</label>
                <div class="layui-input-block">
                    <select name="newsjf" id="newsjf" lay-filter="newsjf">

                    </select>
                </div>
            </div>


            <div class="layui-form-item layui-layout-admin">
                <div class="layui-input-block">
                    <div class="layui-footer" style="left: 0;">
                        <button class="layui-btn submit">确定提交</button>
                        <!--<button type="reset" class="layui-btn layui-btn-primary">重置</button>-->
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>

</div>

<input type="hidden" class="js_store" value="">
<input type="hidden" class="js_jifen" value="">
<input type="hidden" class="js_guize" value="">
<input type="hidden" class="js_newsjf" value="">


<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Publictoken");


    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'table','form','upload'], function(){
        var $ = layui.$
            admin = layui.admin
            ,table = layui.table
            ,element = layui.element
            ,upload = layui.upload
            ,form = layui.form;

            element.render();
        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        })

        // 选择门店
        $.ajax({
            url : "{{url('/api/merchant/store_lists')}}",
            data : {token:token,l:100},
            type : 'post',
            datatype:'json',
            success : function(data) {
                console.log(data);
                $('.store_id').val(data.data[0].store_id)

                var optionStr = "";
                for(var i=0;i<data.data.length;i++){
                    optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
                }
                $("#store").append('<option value="">请选择门店</option>'+optionStr);
                layui.form.render('select');


                // 积分条件
                $.post("{{url('/api/member/newmbsjf')}}",
                {
                    token:token,
                    store_id:data.data[0].store_id
                },function(res1){
                    // console.log(res1);
                    var str1 = "";
                    for(var i=0;i<res1.data.length;i++){
                        str1 += "<option value='" + res1.data[i].new_mb_s_jf + "'>" + res1.data[i].new_mb_desc + "</option>";
                    }
                    $("#jifen").append('<option value="">请选择积分条件</option>'+str1);
                    layui.form.render('select');
                },"json");

                // 使用规则
                $.post("{{url('/api/member/jfsytj')}}",
                {
                    token:token,
                    store_id:data.data[0].store_id
                },function(res2){
                    // console.log(res2);
                    var str1 = "";
                    for(var i=0;i<res2.data.length;i++){
                        str1 += "<option value='" + res2.data[i].dk_jf_m +','+res2.data[i].dk_rmb+ "'>" + res2.data[i].dk_desc + "</option>";
                    }
                    $("#guize").append('<option value="">请选择积分条件</option>'+str1);
                    layui.form.render('select');
                },"json");

                // 新会员送积分
                $.post("{{url('/api/member/jflqtj')}}",
                {
                    token:token,
                    store_id:data.data[0].store_id
                },function(res3){
                    // console.log(res3);
                    var str1 = "";
                    for(var i=0;i<res3.data.length;i++){
                        str1 += "<option value='" + res3.data[i].xf + ','+ res3.data[i].xf_s_jf +"'>" + res3.data[i].xf_desc + "</option>";
                    }
                    $("#newsjf").append('<option value="">请选择积分条件</option>'+str1);
                    layui.form.render('select');
                },"json");
            },
            error : function(data) {
                alert('查找板块报错');
            }
        });







        form.on('select(store)', function(data){
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.js_store').val(category);

            $.post("{{url('/api/member/jf')}}",
            {
                token:token,
                store_id:category,
            },function(data){
                console.log(data);
                if(data.status==1){
                $("#jifen").html('')
                $("#guize").html('')
                $("#newsjf").html('')
                    // 积分条件
                $.post("{{url('/api/member/newmbsjf')}}",
                {
                    token:token,
                    store_id:category
                },function(res1){
                    // console.log(res1);

                    var str1 = "";
                        for(var i=0;i<res1.data.length;i++){
                            str1 += "<option value='" + res1.data[i].new_mb_s_jf + "' "+((res1.data[i].new_mb_s_jf==data.data.new_mb_s_jf)?"selected":"")+">" + res1.data[i].new_mb_desc + "</option>";

                            $('.js_jifen').val(res1.data[i].new_mb_s_jf);
                        }
                        $("#jifen").append('<option value="">请选择积分条件</option>'+str1);
                        layui.form.render('select');
                    },"json");

                    // 使用规则
                    $.post("{{url('/api/member/jfsytj')}}",
                    {
                        token:token,
                        store_id:category
                    },function(res2){
                        // console.log(res2);

                        var str1 = "";
                        for(var i=0;i<res2.data.length;i++){
                            str1 += "<option value='" + res2.data[i].dk_jf_m +','+res2.data[i].dk_rmb+ "' "+((res2.data[i].dk_jf_m==data.data.dk_jf_m)?"selected":"")+">" + res2.data[i].dk_desc + "</option>";

                            $('.js_guize').val(res2.data[i].dk_jf_m +','+res2.data[i].dk_rmb);
                        }
                        $("#guize").append('<option value="">请选择积分条件</option>'+str1);
                        layui.form.render('select');
                    },"json");

                    // 新会员送积分
                    $.post("{{url('/api/member/jflqtj')}}",
                    {
                        token:token,
                        store_id:category
                    },function(res3){
                        // console.log(res3);
                        var str1 = "";
                        for(var i=0;i<res3.data.length;i++){
                            str1 += "<option value='" + res3.data[i].xf + ','+ res3.data[i].xf_s_jf +"' "+((res3.data[i].xf+'.00'==data.data.xf)?"selected":"")+">" + res3.data[i].xf_desc + "</option>";

                            $('.js_newsjf').val(res3.data[i].xf + ','+ res3.data[i].xf_s_jf);
                        }
                        $("#newsjf").append('<option value="">请选择积分条件</option>'+str1);
                        layui.form.render('select');
                    },"json");
                }else{
                    layer.msg(data.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 3000
                    });
                }
            },"json");
        });
        form.on('select(jifen)', function(data){

            category = data.value;
            // categoryName = data.elem[data.elem.selectedIndex].text;
            $('.js_jifen').val(category);
            console.log(category)

        });
        form.on('select(guize)', function(data){

            category = data.value;
            // categoryName = data.elem[data.elem.selectedIndex].text;
            $('.js_guize').val(category);

        });
        form.on('select(newsjf)', function(data){

            category = data.value;
            // categoryName = data.elem[data.elem.selectedIndex].text;
            $('.js_newsjf').val(category);

        });








        $('.submit').on('click', function(){

            var js_guize=$('.js_guize').val()
            var dikou=js_guize.split(',')
            console.log(dikou)
            var js_newsjf=$('.js_newsjf').val()
            var sjf=js_newsjf.split(',')


            $.post("{{url('/api/member/set_jf')}}",
            {
                token:token,
                store_id:$('.js_store').val(),
                // 消费1元送1积分
                xf:sjf[0],
                xf_s_jf:sjf[1],
                // 10积分抵扣1元
                dk_jf_m:dikou[0],
                dk_rmb:dikou[1],
                // 新会员送积分
                new_mb_s_jf:$('.js_jifen').val()

            },function(res){
                console.log(res);
                if(res.status==1){
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 3000
                    });
                }else{
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 3000
                    });
                }
            },"json");

        });


    });
</script>
</body>
</html>
