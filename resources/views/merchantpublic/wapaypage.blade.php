<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>支付方式</title>
    <style>
        *{
            margin: 0;
            padding: 0;
            list-style: none;
            text-decoration: none;
        }
        body{
            background-color: #ffffff; 
        }
        .middle .title{
            font-size: 24px;
            padding: 18px 0px 16px 0px;
            font-weight: 500;
            color: #333333;
            width: 345px;
            margin: 0 auto;
        }
        .middle .p{

            width: 345px;
            margin: 0 auto;
            font-size: 14px;
            color: #666666;

        }
        .paylist li label{
            display: flex;
            justify-content: space-between;
            align-items: center;
        }
        .paylist li {
            padding-top: 10px;
            margin-top: 10px;
            border-top: 1px solid #eeeeee;
        }
        .paylist li:first-child{
            border-top:none;
        }
        .paylist li img{
            display: block;
            margin: 0 auto;
        }
        .btn{
            width: 90%;
            margin: 62px auto 0;
            background: rgba(0,117,255,0.8);
            color: #ffffff;
            height: 50px;
            text-align: center;
            line-height: 50px;
            font-size: 18px;
            border-radius: 50px;
            
        }

    </style>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script type="text/javascript" src="{{asset('/userweb/layuinew/layui.js')}}"></script>
</head>
<body>
    <div class="middle" id="app">
        <div class="title">
            请选择支付方式
        </div>
        <div class="p">
            竭诚为您的支付保驾护航
        </div>
        <div class="paylist">
            <ul>
                <li>
                    <label for="wechat">
                        <img @click="gopay(1)" src="/wapaypageimg/wechetpay.png">
                        <input type="hidden" id="wechat" value="1" v-model="radiobtn">
                    </label>
                </li>
                <li>
                    <label for="alipay">
                        <img @click="gopay(2)" src="/wapaypageimg/alipay.png">
                        <input type="hidden" id="alipay" value="2" v-model="radiobtn">
                    </label>
                </li>
            </ul>
        </div>

        <div style="position: fixed;
                    left: 50%;
                    bottom: 26px;
                    width: 100%;
                    transform: translateX(-50%);">
            <div style="width: 100px;margin: 0 auto;">
                <!-- 自己的 -->
                <!-- <img  style="display: block;width: 100%;" src="/wapaypageimg/logo.png"> -->
                <!-- 趣高的 -->
                <img  style="display: block;width: 100%;" src="/wapaypageimg/logo2.png">
            </div>
            <!-- 自己的 -->
            <!-- <p style="font-size: 14px;text-align: center;color: #666666;">由苏州韶风文化传媒提供技术支持</p> -->
            <!-- 趣高的 -->
            <p style="font-size: 14px;text-align: center;color: #666666;">江苏蜻蜓文化科技提供技术支持</p>
        </div>

    </div>

    <script>
    
    layui.use(function() {
        var $ = layui.$
        var vm = new Vue({
            el:"#app",
            data:{
                radiobtn:"1", //1微信，2支付宝
                storeid:"",   //门店id
            },
            created() {
                // 获取 url 后缀
                let url = window.location.search.substring(1);
                let urldata = this.geturlobj(url);
                this.storeid = urldata.storeid;
            },
            methods: {
                // 去支付
                async gopay(radiotype){

                    let that = this;
                   
                    let radiobtn = radiotype;
                    // 获取 storeid
                    if(this.storeid == "" || this.storeid == null || this.storeid == undefined){
                        alert("门店id不存在");
                        return false;
                    }
                    // 获取平台
                    if(radiobtn == "" || radiobtn == null || radiobtn == undefined){
                        alert("请选择支付方式");
                        return false;
                    }
                    let data = await this.ajax("/api/customer/applet/getAppletScheme",{
                        applet_type:radiobtn,
                        store_id:that.storeid,
                    });
                    console.log(data)
                    if(data.code == 200){
                        console.log(data.data.wechat_scheme)
                        console.log(data.data.alipay_appid)
                        if(radiobtn == 1){
                            window.location.href = data.data.wechat_scheme;
                        }else if(radiobtn == 2){ 
                            
                            // window.location.href = "alipays://platformapi/startapp?appId="+data.data.alipay_appid+"&page=pages/payment/payment&query={store_id:'"+that.storeid+"',page:'payment'}";

                            window.location.href = "alipays://platformapi/startapp?appId="+data.data.alipay_appid+"&page=pages/payment/payment&query=store_id%3D"+that.storeid+"%26page%3Dpayment";

                            // window.location.href = "alipays://platformapi/startapp?appId=2021001187618312&page=pages/payment/payment&query=store_id%3D20211517402863632%26page%3Dpayment"

                        }
                    }else{
                        layer.msg(data.msg, {icon:2, shade:0.5, time:2000});
                    }
                },
                // 切割url
                geturlobj(src){
                    let arr = src.split("&")
                    let obj = {}
                    for(let i=0;i<arr.length;i++){
                        let newarr = arr[i].split("=");
                        obj[newarr[0]] = newarr[1];
                    }
                    return obj;
                },
                // ajax
                ajax(url,data){
                    return new Promise(resolve=>{
                        $.post(url,data,res=>{
                            resolve(res)
                        })
                    })
                }
            },
        })
    });
    </script>





</body>
</html>