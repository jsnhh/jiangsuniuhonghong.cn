<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>核销详情</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/inputTags/inputTags.css')}}">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .tongbu {
            background-color: #4c9ef8;
            color: #fff;
        }

        .cur {
            color: #009688;
        }

        .yname {
            font-size: 13px;
            color: #444;
        }

        .xgrate {
            color: #fff;
            font-size: 15px;
            padding: 7px;
            height: 30px;
            line-height: 30px;
            background-color: #3475c3;
        }

        .input_block {
            display: block;
        }
        .specificstatistics{display:flex;justify-content:space-around;}
    </style>
</head>

<body>
    <div class="layui-fluid" style="margin-top:50px;">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">

                <div class="layui-fluid">
                    <div class="layui-row layui-col-space15">
                        <div class="layui-col-md12">
                            <div class="layui-card" style="padding:0px 15px 0px 15px;">
                                <div class="layui-card-header">核销详情</div>
                                <div style="padding-left:12px;color:#333333;font-size:20px;margin-top:30px">该代金券名称</div>
                                <div class="specificstatistics" style="width:100%">
                                    <div style="padding-left:14px;">
                                        <div style="color:#333333;font-size:14px;padding-top:20px">
                                            发放总数量
                                          <p style="color:#52C41A;font-size:30px;text-align: center;padding-top: 10px;" class="total"> </p>
                                        </div>
                                    </div>
                                    <div style="padding-left:24px;">
                                        <div style="color:#333333;font-size:14px;padding-top:20px">
                                            已领取张数
                                            <p style="color:#1E9FFF;font-size:30px;text-align: center;padding-top: 10px;" class="number"> </p>
                                        </div>

                                    </div>
                                    <!-- <div style="padding-left:24px;">
                                        <div style="color:#333333;font-size:14px;padding-top:20px">
                                            已领取占比
                                            <p style="color:#1E9FFF;font-size:30px;text-align: center;padding-top: 10px;" class="proportion"> </p>
                                        </div>
                                    </div> -->
                                    <div style="padding-left:24px;">
                                        <div style="color:#333333;font-size:14px;padding-top:20px">
                                            已核销张数
                                            <p style="color:#FF5500;font-size:30px;text-align: center;padding-top: 10px;" class="writeNumber"> </p>
                                        </div>
                                    </div>
                                    <!-- <div style="padding-left:24px;">
                                        <div style="color:#333333;font-size:14px;padding-top:20px">
                                            已领取占比
                                            <p style="color:#FF5500;font-size:30px;text-align: center;padding-top: 10px;" class="receiveProportion"> </p>
                                        </div>
                                    </div> -->
                                </div>
                                <div class="layui-card-body">
                                    <div class="layui-btn-container" style="font-size:14px;">
                                        <!-- 缴费时间 -->
                                        <div class="layui-form" style="width:100%;display:flex;">
                                            <!-- 选择业务员 -->
                                            <div class="layui-form" lay-filter="component-form-group"
                                                style="margin-right:10px;display: inline-block;display:flex;width:1375px;flex-wrap:wrap;margin-top:10px">
                                                <div class="layui-form-item">
                                                    <div class="layui-input-block" style="margin-left:0px;width:300px">
                                                        <text class="yname">使用状态</text>
                                                        <input type="text" name="state" placeholder="请输代金券名称" autocomplete="off" class="layui-input state">
                                                    </div>
                                                </div>
                                                <div class="layui-form-item">
                                                    <div class="layui-input-block" style="margin-left:20px;width:300px">
                                                        <text class="yname">会员名称</text>
                                                        <input type="text" name="vipname" placeholder="请输代金券名称" autocomplete="off" class="layui-input vipname">
                                                    </div>
                                                </div>
                                                <div class="layui-form-item">
                                                    <div class="layui-input-block" style="margin-left:20px;width:300px">
                                                        <text class="yname">会员ID</text>
                                                        <input type="text" name="vipId" placeholder="请输代金券名称" autocomplete="off" class="layui-input vipId">
                                                    </div>
                                                </div>
                                                <div class="layui-inline" style="margin-left: 10px;">
                                                    <button class="layui-btn layuiadmin-btn-list" lay-submit="" lay-filter="LAY-app-contlist-search" style="border-radius:5px;margin-bottom: -3.8rem;height:36px;line-height: 36px;">
                                                        <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                </div>

                                <!-- 更新状态 -->
                                <script type="text/html" id="table-content-list">
                                    <a class="layui-btn layui-btn-normal layui-btn-xs tongbu" lay-event="tongbu">更新状态</a>
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script type="text/html" id="company_type">
       @{{#  if(d.status =='1'){ }}
        待使用
        @{{#  } else if(d.status =='2'){ }}
        可用
        @{{#  } else if(d.status =='3'){ }}
        不可用
        @{{#  } else if(d.status =='4'){ }}
        已使用
        @{{#  } else if(d.status =='5'){ }}
        已过期
        @{{#  } else if(d.status =='6'){ }}
        未激活
        @{{#  } }}
    </script>
    <input type="hidden" class="classname">
    <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
    <script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script>
    <script src="{{asset('/layuiadmin/layui/inputTags/inputTags.js')}}"></script>
    <script>
        var token = sessionStorage.getItem("Publictoken");
        var str = location.search;
        var store_id = sessionStorage.getItem("store_store_id");
        var template_id = sessionStorage.getItem("store_template_id");
        // console.log(store_id)

        layui.config({
            base: '../../layuiadmin/' //静态资源所在路径
        }).extend({
            index: 'lib/index' //主入口模块
        }).use(['index', 'form', 'table', 'laydate', "upload"], function() {
            var $ = layui.$,
                admin = layui.admin,
                form = layui.form,
                table = layui.table,
                laydate = layui.laydate,
                upload = layui.upload;

            // 未登录,跳转登录页面
            $(document).ready(function() {
                if (token == null) {
                    window.location.href = "{{url('/mb/login')}}";
                }
            });
            // 数量展示
            $.post("{{url('/api/alipayopen/couponDetailQuery')}}",
            {
                token: token,
                storeId: store_id,
                templateId: template_id,
            },
            function(res){
                console.log(res)
                    $('.total').html(res.data.voucher_quantity);
                    $('.number').html(res.data.publish_count);
                    $('.writeNumber').html(res.data.used_count);

            },"json");

            // 渲染表格
            table.render({
                elem: '#test-table-page',
                url: "{{url('/api/alipayopen/voucherTemList')}}",
                method: 'post',
                where: {
                    token: token,
                    storeId: store_id,
                    templateId: template_id,
                },
                request: {
                    pageName: 'p',
                    limitName: 'l'
                },
                cellMinWidth: 150,
                cols: [
                    [
                    {field: 'alipay_user_id',title: '会员ID',align: 'center'}
                    ,{field: 'voucher_id',title: '券ID',align: 'center'}
                    ,{field: 'status',title: '状态',align: 'center',toolbar:'#company_type'}
                    ,{align: 'center',toolbar: '#table-content-list',title: '操作'}]
                ],
                page: true,
                response: {
                     statusName: 'status' //数据状态的字段名称，默认：code
                    ,statusCode: 1 //成功的状态码，默认：0
                    , msgName: 'message' //状态信息的字段名称，默认：msg
                    ,countName: 't' //数据总数的字段名称，默认：count
                    ,dataName: 'data' //数据列表的字段名称，默认：data
                },
                done: function(res, curr, count) {
                    console.log(res)
                    $('th').css({'font-weight': 'bold','font-size': '15','color': 'black','background': 'linear-gradient(#f2f2f2,#cfcfcf)' }); //进行表头样式设置
                    form.render();
                },
            });

            table.on('tool(test-table-page)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
                var e = obj.data; //获得当前行数据
                var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
                var tr = obj.tr; //获得当前行 tr 的DOM对象
                        //  console.log(e);
                // sessionStorage.setItem('s_store_id', e.store_id);

                if(layEvent === 'tongbu'){ //同步
                    $.post("{{url('/api/alipayopen/voucherQuery')}}",
                    {
                        token:token
                        ,storeId:e.store_id
                        ,voucher_id:e.voucher_id
                    },function(res){
                        if(res.status==1){
                            layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 2000
                            });
                        }else{
                            layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 2000
                            });
                        }
                    },"json");
                }
            });

            /*
             * 搜索
             */
            form.on('submit(LAY-app-contlist-search)', function(data){
                var state = data.field.state;
                var vipname = data.field.vipname;
                var vipId = data.field.vipId;
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        status:state,
                        brand_name:vipname,
                        alipay_user_id:vipId
                    },page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            });


        });
    </script>
</body>

</html>