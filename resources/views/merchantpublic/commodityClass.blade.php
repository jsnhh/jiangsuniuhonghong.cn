<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>商品分类管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/modules/layuiicon/iconfont.css')}}" media="all">
    <style>
        .layui-table-header {
            height: 40px;
            line-height: 40px !important;
        }

        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .tongbu {
            background-color: #4c9ef8;
            color: #fff;
            border-radius: 3.5px
        }

        .cur {
            color: #21c4f5;
        }

        .userbox {
            height: 190px;
            margin-right: 0;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 75px;
            top: 50px;
            width: 298px;
            background-color: #ffffff;
            border: 1px solid #ddd;
        }

        .userbox .list,
        .storebox .list {
            height: 38px;
            line-height: 38px;
            cursor: pointer;
            padding-left: 10px;
        }

        .userbox .list:hover,
        .storebox .list:hover {
            background-color: #eeeeee;
        }

        .yname {
            font-size: 13px;
            color: #444;
            width: 60px;
            text-align: center;
            margin-top: 7px;
        }

        .xgrate {
            color: #fff;
            font-size: 15px;
            padding: 7px;
            height: 30px;
            line-height: 30px;
            background-color: #3475c3;
        }

        .layui-form-select {
            width: 300px;
        }

        .layui-form-label span {
            color: red;
            padding-right: 5px;
        }
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
        }

        input[type="number"] {
            -moz-appearance: textfield;
        }
    </style>
</head>

<body>
    <div id="mask" class="mask"></div>
    <div class="layui-fluid" style="margin-top:50px;">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-fluid">
                    <div class="layui-row layui-col-space15">
                        <div class="layui-col-md12">
                            <div class="layui-card">
                                <div class="layui-card-header">商品分类管理</div>
                                <div class="layui-card-body">
                                    <div class="layui-btn-container" style="font-size:14px;">
                                        <div id="search-form" style="font-size:14px;display:flex;">
                                            <div class="layui-form" lay-filter="component-form-group">
                                                <div class="layui-form-item">
                                                    <div style="display:flex;">
                                                        <label class="yname">门店</label>
                                                        <select name="agent" id="agent" lay-filter="agent" lay-search>

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="layui-inline">
                                                <button class="layui-btn layuiadmin-btn-list" id="addTable" style="border-radius:5px;margin-bottom: 30px;height:36px;line-height: 36px;margin-left: 50px;">
                                                    <i class="layui-icon">&#xe608;</i>新增
                                                </button>
                                            </div>
                                        </div>
                                    </div>


                                    <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>

                                    <script type="text/html" id="storeTableToolbar">
                                        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="edit">编辑</a>
                                        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="del">删除</a>
                                    </script>

                                    <script type="text/html" id="statusTemp">
                                        @{{# if(d.status==1){ }}
                                            <input type="checkbox" name="status" lay-skin="switch" checked lay-filter="status">
                                            @{{# } else { }}
                                                <input type="checkbox" name="status" lay-skin="switch" lay-filter="status">
                                                @{{# } }}
                                    </script>
                                    <script type="text/html" id="purchaseTotal">
                                        <input type="number" class="layui-input" name="event" value="" lay-event="purchaseTotal" lay-filter="purchaseTotal" style="width:100px;">
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" class="store_id">
        <input type="hidden" class="sort">
        <input type="hidden" class="stu_class_no">

        <input type="hidden" class="stu_order_batch_no">
        <input type="hidden" class="user_id">

        <input type="hidden" class="pay_status">
        <input type="hidden" class="pay_type">

        <input type="hidden" class="paycode">
        <input type="hidden" class="company_id">

        <input type="hidden" class="sbid">
        <input type="hidden" class="danhao">
        <input type="hidden" class="tiaoma">
        <input type="hidden" class="js_role_id">


        <script src="{{asset('/user/js/jquery.min.js?v=2.1.4')}}"></script>
        <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
        <script>
            var token = sessionStorage.getItem("Publictoken");
            var str = location.search;
            var store_id = str.split('?')[1];

            layui.config({
                base: '../../layuiadmin/' //静态资源所在路径
            }).extend({
                index: 'lib/index' //主入口模块
            }).use(['index', 'form', 'table'], function() {
                var $ = layui.$,
                    form = layui.form,
                    table = layui.table;

                $('.store_id').val(store_id);
                // 未登录,跳转登录页面
                $(document).ready(function() {
                    if (token == null) {
                        window.location.href = "{{url('/user/login')}}";
                    }
                });
                var s_storename = sessionStorage.getItem('s_storename');
                if (store_id == undefined) {

                } else {
                    $('.inputstore').val(s_storename);
                }

                // 打开新增窗口
                $('#addTable').on("click", function() {
                    // 选择门店
                    $.ajax({
                        url: "{{url('/api/merchant/store_lists')}}",
                        data: {
                            token: token,
                            l: 100
                        },
                        type: 'post',
                        success: function(data) {
                            var optionStr = "";
                            for (var i = 0; i < data.data.length; i++) {
                                optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
                            }
                            $("#agent_modal").append('<option value="">选择门店</option>' + optionStr);
                            layui.form.render('select');
                        },
                        error: function(data) {
                            alert('查找板块报错');
                        }
                    });

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#submitAppletsNameInfo')
                    });
                });
                //关闭新增弹窗
                $(".close").on("click", function() {
                    $("#submitAppletsNameInfo").hide();
                    document.location.reload();
                })

                // 选择门店
                $.ajax({
                    url: "{{url('/api/merchant/store_lists')}}",
                    data: {
                        token: token,
                        l: 100
                    },
                    type: 'post',
                    success: function(data) {
                        var optionStr = "";
                        for (var i = 0; i < data.data.length; i++) {
                            optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
                        }
                        $("#agent").append('<option value="">选择门店</option>' + optionStr);
                        layui.form.render('select');
                    },
                    error: function(data) {
                        alert('查找板块报错');
                    }
                });

                var store_id = sessionStorage.getItem('store_id')
                //表格渲染
                table.render({
                    elem: '#test-table-page'
                    ,url: "{{url('/api/merchant/goodsCategoryList')}}"
                    ,method: 'get'
                    ,where:{
                        token: token,
                        storeId: store_id,
                        sortMethod: "asc"
                    }
                    ,request:{
                    pageName: 'p',
                    limitName: 'l'
                    }
                    ,page: true
                    ,cellMinWidth: 150
                    ,cols: [[
                    // {type:'checkbox', fixed: 'left'}
                       {field: 'name',align: 'center',title: '商品分类名称'},
                       {field: 'status',title: '显示',align: 'center',templet: '#statusTemp',sort: true},
                       {field: 'sort',title: '排序',align: 'center',edit: 'text',sort: true},
                       {fixed: 'right',title: '操作',align: 'center',toolbar: '#storeTableToolbar',width: 400}
                    ]]
                    ,response: {
                    statusName: 'status' //数据状态的字段名称，默认：code
                    ,statusCode: 1 //成功的状态码，默认：0
                    ,msgName: 'message' //状态信息的字段名称，默认：msg
                    ,countName: 't' //数据总数的字段名称，默认：count
                    ,dataName: 'data' //数据列表的字段名称，默认：data
                    }
                    ,done: function(res, curr, count){
                    console.log(res);
                    $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});	//进行表头样式设置
                    }

                });

                form.on('switch(status)', function(obj) {
                    $.ajax({
                        url: "{{url('/api/merchant/goodsCategoryList')}}",
                        data: {
                            token: token,
                            storeId: store_id,
                            sortMethod: "asc"
                        },
                        type: 'get',
                        success: function(data) {
                            console.log(data)
                            //根据业务判断是开启还是关闭
                            var status = obj.elem.checked ? 1 : 2;
                            //方法一取数据（根据相对位置取）
                            var index = obj.othis.parents('tr').attr("data-index");
                            var id = data.data[index].id;
                            var name = data.data[index].name;
                            $.ajax({
                                type: 'POST',
                                url: "{{url('/api/merchant/updateGoodsCategory')}}",
                                data: {
                                    token: token,
                                    status: status,
                                    storeId: store_id,
                                    cid: id,
                                    name: name
                                },
                                success: function(res) {
                                    if (res.status == 1) {
                                        layer.msg('更新成功', {
                                            icon: 1
                                        }, function() {
                                            window.location.reload();
                                        });
                                    } else {
                                        layer.msg(res.msg, {
                                            icon: 1
                                        }, function() {
                                            window.location.reload();
                                        });
                                    }
                                }
                            });
                        },
                        error: function(data) {
                            alert('查找板块报错');
                        }
                    });

                });


                // 新增桌号
                form.on('submit(submitNewInfo)', function(data) {
                    var datas = data.field;
                    if (!datas.agent_modal) {
                        layer.msg("门店不可为空", {
                            offset: '50px',
                            icon: 2,
                            time: 2000
                        });
                        return false;
                    }

                    if (!datas.table_name_modal) {
                        layer.msg("分类称不可为空", {
                            offset: '50px',
                            icon: 2,
                            time: 2000
                        });
                        return false;
                    }

                    $.ajax({
                        type: 'POST',
                        url: "{{url('/api/merchant/updateGoodsCategory')}}",
                        data: {
                            token: token,
                            storeId: datas.agent_modal,
                            name: datas.table_name_modal,
                        },
                        success: function(res) {
                            $('#submitAppletsNameInfo').hide()
                            if (res.status == 1) {
                                layer.msg('添加成功', {
                                    icon: 1
                                }, function() {
                                    window.location.reload();
                                });
                            } else {
                                layer.msg(res.msg, {
                                    icon: 1
                                }, function() {
                                    window.location.reload();
                                });
                            }
                        }
                    });
                    return false;
                });

                table.on('edit(test-table-page)', function(obj){
                    var value = obj.value //得到修改后的值
                    ,data = obj.data //得到所在行所有键值
                    ,field = obj.field; //得到字段
                    layui.use('jquery',function(){
                    var $=layui.$;
                    $.ajax({
                                type: 'POST',
                                url: "{{url('/api/merchant/updateGoodsCategory')}}",
                                data: {
                                    token: token,
                                    status: status,
                                    storeId: store_id,
                                    cid: data.id,
                                    name: data.name,
                                    sort:value
                                },
                                success: function(res) {
                                    if (res.status == 1) {
                                        layer.msg('更新成功', {
                                            icon: 1
                                        }, function() {
                                            window.location.reload();

                                        });
                                    } else {
                                        layer.msg(res.msg, {
                                            icon: 1
                                        }, function() {
                                            window.location.reload();
                                        });
                                    }
                                }
                            });
                    });
                });
                // 监听行工具事件
                table.on('tool(test-table-page)', function(obj) {
                    // 获取obj里的data
                    var data = obj.data;
                     if (obj.event === 'del') {
                        layer.confirm('确认删除吗？', function() {
                            $.ajax({
                                type: 'POST',
                                url: "{{url('/api/merchant/delGoodsCategory')}}",
                                data: {
                                    cid: data.id,
                                },
                                success: function(res) {
                                    console.log(res)
                                    if (res.status == 1) {
                                        layer.msg('删除成功', {
                                            icon: 1
                                        }, function() {
                                            window.location.reload();
                                        });
                                    } else {
                                        layer.msg('删除失败: ' + res.msg, {
                                            icon: 1
                                        }, function() {
                                            window.location.reload();
                                        });
                                    }
                                }
                            })
                        });
                    } else if (obj.event === 'edit') {
                        layer.open({
                            type: 1,
                            title: false,
                            closeBtn: 0,
                            area: '516px',
                            skin: 'layui-layer-nobg', //没有背景色
                            shadeClose: true,
                            content: $('#editModal')
                        });

                        // 将值放入模态框
                        $('#table_name_modal_edit').val(data.table_name);

                        form.on('submit(submitEditTwo)', function(message) {
                            var mes = message.field;
                            if (!mes.table_name_modal_edit) {
                                layer.msg("分类称不可为空", {
                                    offset: '50px',
                                    icon: 2,
                                    time: 2000
                                });
                                return false;
                            }
                            $.ajax({
                                url: "{{url('/api/merchant/updateGoodsCategory')}}",
                                type: "POST",
                                data: {
                                    token: token,
                                    storeId: store_id,
                                    cid: data.id,
                                    name: mes.table_name_modal_edit,

                                },
                                success: function(res) {
                                    if (res.status == 1) {
                                        layer.msg('编辑成功', {
                                            icon: 1
                                        }, function() {
                                            window.location.reload();
                                        });
                                    } else {
                                        layer.msg('编辑失败', {
                                            icon: 1
                                        }, function() {
                                            window.location.reload();
                                        });
                                    }
                                }
                            })
                            // 防止页面直接刷新不提示
                            return false;
                        })
                    }
                });

            });
        </script>

        <!-- 新增 -->
        <div id="submitAppletsNameInfo" class="hide layui-form" lay-filter="appletsName" style="display: none;background-color: #fff;">
            <div class="xgrate">新增</div>
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form" style="padding-top: 20px;padding-bottom: 20px;">
                    <div class="layui-form-item">
                        <label class="layui-form-label" style="text-align:center"><span>*</span>门店</label>
                        <div class="layui-input-block">
                            <select name="agent_modal" id="agent_modal" lay-filter="agent" lay-search></select>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label" style="text-align:center"><span>*</span>商品分类</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入桌名称" name="table_name_modal" class="layui-input title">
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="float: right;">
                            <button class="layui-btn close" style="border-radius:5px;background-color: #FFB800;">关闭</button>
                            <button class="layui-btn submitNewInfo" lay-submit lay-filter="submitNewInfo" style="border-radius:5px">确定</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- 编辑 -->
        <div id="editModal" class="hide layui-form" lay-filter="appletsName" style="display: none;background-color: #fff;">
            <div class="xgrate">编辑</div>
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form" style="padding-top: 20px;padding-bottom: 20px;">
                    {{-- <div class="layui-form-item">--}}
                    {{-- <label class="layui-form-label" style="text-align:center"><span>*</span>门店</label>--}}
                    {{-- <div class="layui-input-block">--}}
                    {{-- <select name="agent_modal" id="agent_modal_edit" lay-filter="agent" lay-search></select>--}}
                    {{-- </div>--}}
                    {{-- </div>--}}
                    <div class="layui-form-item">
                        <label class="layui-form-label" style="text-align:center"><span>*</span>商品分类</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入桌名称" name="table_name_modal_edit" class="layui-input title">
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="float: right;">
                            <button class="layui-btn close" style="border-radius:5px;background-color: #FFB800;">关闭</button>
                            <button class="layui-btn submitEdit" lay-submit lay-filter="submitEditTwo" style="border-radius:5px">确定</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>

</html>