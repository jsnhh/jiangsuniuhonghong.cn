<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>支付宝小程序授权管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style type="text/css">
        .xgrate{color: #fff;font-size: 15px;padding: 7px;height: 30px;line-height: 30px;background-color: #3475c3;}
        .up #uploadFile{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
        #demo5,#app_logo{width: 80px;}
        .layui-form-item{margin-bottom: 0px !important;}
        .layui-form-item .layui-input-inline{width: 115px !important;}
    </style>
</head>
<body>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-fluid">
                    <div class="layui-row layui-col-space15">
                        <div class="layui-col-md12" style="margin-top:0px">
                            <div class="layui-card">
                                <div class="layui-card-header">
                                    <div class="layui-col-md6">
                                        支付宝小程序授权管理
                                    </div>
                                    <div class="layui-col-md6">
                                        <div class="layui-form-item">
                                            <div class="layui-footer">
                                                <a lay-href="{{url('/user/appletAliPayTemplate')}}">
                                                    <button class="layui-btn" style="border-radius:5px">支付宝模板消息</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-card-body">
                                    <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                    <script type="text/html" id="table-content-list">

                                        <a class="layui-btn layui-btn-xs" lay-event="getAppletsInfo">修改小程序信息</a>

                                        @{{#  if(d.status == "INIT"){ }}
                                        <a class="layui-btn layui-btn-xs" lay-event="checkRefuse">提交审核</a>
                                        <a class="layui-btn layui-btn-xs" lay-event="cancelKaiFa">退回开发</a>
                                        <a class="layui-btn layui-btn-xs" lay-event="versionDelete">删除此版本</a>
                                        @{{#  } }}

                                        @{{#  if(d.status == "AUDITING"){ }}
                                        {{--<a class="layui-btn layui-btn-xs" lay-event="">审核中</a>--}}
                                        <a class="layui-btn layui-btn-xs" lay-event="auditCancel">撤销审核</a>
                                        @{{#  } }}

                                        @{{#  if(d.status == "WAIT_RELEASE"){ }}
                                        <a class="layui-btn layui-btn-xs" lay-event="online">发布上架</a>
                                        <a class="layui-btn layui-btn-xs" lay-event="cancelKaiFa">退回开发</a>
                                        @{{#  } }}

                                        @{{#  if(d.status == "AUDIT_REJECT"){ }}
                                        <a class="layui-btn layui-btn-xs" lay-event="checkRefuse">提交审核</a>
                                        <a class="layui-btn layui-btn-xs" lay-event="cancelKaiFa">退回开发</a>
                                        @{{#  } }}

                                        @{{#  if(d.status == "RELEASE"){ }}
                                        <a class="layui-btn layui-btn-xs" lay-event="">已上架</a>
                                        {{--<a class="layui-btn layui-btn-xs" lay-event="offline">下架</a>--}}
                                        @{{#  } }}

                                        @{{#  if(d.status == "GRAY"){ }}
                                        {{--<a class="layui-btn layui-btn-xs" lay-event="">灰度中</a>--}}
                                        <a class="layui-btn layui-btn-xs" lay-event="online">发布上架</a>
                                        <a class="layui-btn layui-btn-xs" lay-event="cancelKaiFa">退回开发</a>
                                        @{{#  } }}

                                        @{{#  if(d.status == "OFFLINE"){ }}
                                        <a class="layui-btn layui-btn-xs" lay-event="">已下架</a>
                                        @{{#  } }}

                                        <a class="layui-btn layui-btn-xs" lay-event="getAliPayOpenAuthAppAesGet">获取aesKey密钥</a>
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="checkRefuseContent" class="hide layui-form" lay-filter="appletsInfo" style="display: none;background-color: #fff;">
        <div class="xgrate">提交审核</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">* 营业执照名称</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="请输入营业执照名称" value="" name="license_name" class="layui-input title">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">* 营业执照有效期</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="请输入营业执照有效期" value="9999-12-31" id="license_valid_date" name="license_valid_date" class="layui-input title">
                </div>
            </div>
            <div class="layui-form">
                <div class="layui-form-item" pane="">
                    <label class="layui-form-label">* 小程序版本描述</label>
                    <div class="layui-input-block">
                        <textarea name="desc" placeholder="请输入内容" class="layui-textarea textarea"></textarea>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <div class="layui-footer" style="left: 0;">
                        <button class="layui-btn submitAppletsInfo" lay-submit lay-filter="submitAppletsInfo" style="border-radius:5px">确定</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="getAppletsInfo" class="hide layui-form" lay-filter="getAppletsInfo" style="display: none;background-color: #fff;">
        <div class="xgrate">小程序基础信息</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">小程序应用名称</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="请输入小程序应用名称" value="" name="app_name" class="layui-input title">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">小程序应用英文名称</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="请输入小程序应用英文名称" value="" name="app_english_name" class="layui-input title">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">小程序应用简介</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="一句话描述小程序功能" value="" name="app_slogan" class="layui-input title">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">小程序类目</label>
                <div class="layui-input-inline">
                    <select id="mini_category_list0_1" name="mini_category_list0_1" lay-filter="mini_category_list0_1">
                        <option value="">请选择</option>
                    </select>
                </div>
                <div class="layui-input-inline">
                    <select id="mini_category_list0_2" name="mini_category_list0_2" lay-filter="mini_category_list0_2">
                        <option value="">请选择</option>
                    </select>
                </div>
                <div class="layui-input-inline">
                    <select id="mini_category_list0_3" name="mini_category_list0_3" lay-filter="mini_category_list0_3">
                        <option value="">请选择</option>
                    </select>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label"></label>
                <div class="layui-input-inline">
                    <select id="mini_category_list1_1" name="mini_category_list1_1" lay-filter="mini_category_list1_1">
                        <option value="">请选择</option>
                    </select>
                </div>
                <div class="layui-input-inline">
                    <select id="mini_category_list1_2" name="mini_category_list1_2" lay-filter="mini_category_list1_2">
                        <option value="">请选择</option>
                    </select>
                </div>
                <div class="layui-input-inline">
                    <select id="mini_category_list1_3" name="mini_category_list1_3" lay-filter="mini_category_list1_3">
                        <option value="">请选择</option>
                    </select>
                </div>
            </div>

            <div class="layui-card-body">
                <label class="layui-form-label" style="text-align:center">小程序logo</label>
                <div class="layui-upload">
                    <button class="layui-btn up" style="border-radius:5px;position: relative;">
                        <i class="layui-icon">&#xe67c;</i>
                        <input type="file" name="file" id="uploadFile" style="position: absolute;top: 0;left: 0;" />上传logo
                    </button>
                    <div class="layui-upload-list">
                        <img class="layui-upload-img" id="app_logo">
                        <p id="demoText"></p>
                    </div>
                </div>
            </div>
            <input type="hidden" name="license" value="" />
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">小程序应用描述</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="请输入小程序应用描述" value="" id="app_desc" name="app_desc" class="layui-input title">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">小程序客服电话</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="请输入小程序客服电话" value="" id="service_phone" name="service_phone" class="layui-input title">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">小程序客服邮箱</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="请输入小程序客服邮箱" value="" id="service_email" name="service_email" class="layui-input title">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">小程序域白名单</label>
                <div class="layui-input-block">
                    <input type="text" readonly value="" id="safe_domains" name="safe_domains" class="layui-input title">
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <div class="layui-footer" style="left: 0;">
                        <button class="layui-btn submitGetAppletsInfo" lay-submit lay-filter="submitGetAppletsInfo" style="border-radius:5px">确定</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script type="text/javascript">
    var token = sessionStorage.getItem("Usertoken");
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form', 'upload','table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,table = layui.table
            ,form = layui.form
            ,upload = layui.upload
            ,laydate = layui.laydate;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });

        //日期选择
        laydate.render({
            elem: '#license_valid_date'
        });

        /**
         * 进入该页面，初始化渲染该表格
         */
        table.render({
            elem: '#test-table-page',
            url: "{{url('/api/customer/aliPay/getAliPayOpenMiniTemplateUsageQuery')}}",
            method: 'post',
            where:{
                token:token
            },
            request:{
                pageName: 'page',
                limitName: 'count'
            },
            page: true,
            cellMinWidth: 100,
            cols: [
                [
                    {width:200,field:'mini_app_id', title: '小程序appid',templet: '#appletsId'},
                    {width:200,field:'app_name', title: '小程序名称'},
                    {width:200,field:'app_version', title: '小程序版本'},
                    {width:100,field:'status',  title: '审核状态'},
                    {width:100,field:'status_name',  title: '状态描述'},
                    {width:300,field:'reject_reason',  title: '审核原因'},
                    {width:500,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
                ]
            ],
            response: {
                statusName: 'status', //数据状态的字段名称，默认：code
                statusCode: 200, //成功的状态码，默认：0
                msgName: 'message', //状态信息的字段名称，默认：msg
                countName: 't', //数据总数的字段名称，默认：count
                dataName: 'data', //数据列表的字段名称，默认：data
            },
            done: function(res, curr, count){
                //进行表头样式设置
                $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});
            }
        });

        /**
         * 表格的操作列中的每个操作项
         */
        table.on('tool(test-table-page)', function(obj){
            var lineData = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            if(layEvent === 'checkRefuse'){
                //重新提交审核
                var checkRefuseContent = layer.open({
                    type: 1,
                    title: false,
                    closeBtn: 0,
                    area: '516px',
                    skin: 'layui-layer-nobg', //没有背景色
                    shadeClose: true,
                    content: $('#checkRefuseContent')
                });
                //点击submit进行提交
                form.on("submit(submitAppletsInfo)",function(data){
                    var requestData = data.field;
                    layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});

                    $.post("{{url('/api/customer/aliPay/getAliPayOpenMiniVersionAuditApply')}}",{
                        mini_app_id:lineData.mini_app_id,
                        app_version:lineData.app_version,
                        license_name:requestData.license_name,
                        license_valid_date:requestData.license_valid_date,
                        store_applets_desc:requestData.desc
                    },function(data){
                        var status = data.status;
                        if(status == 200){
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 1000
                            },function(){
                                layer.close(checkRefuseContent);
                                window.location.reload();
                            });
                        }else{
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 1000
                            });
                        }
                    },"json");
                    return false;
                });
            }else if(layEvent === 'online'){
                //审核成功，进行发布代码，上架
                layer.confirm('确认进行发布代码?',{icon: 2}, function(index){
                    layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                    $.post("{{url('/api/customer/aliPay/getAliPayOpenMiniVersionOnline')}}",{
                        mini_app_id:lineData.mini_app_id,
                        app_version:lineData.app_version,
                        type:1
                    },function(data){
                        var status = data.status;
                        if(status == 200){
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 1000
                            },function(){
                                layer.close(index);
                                window.location.reload();
                            });
                        }else{
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 1000
                            });
                        }
                    },"json");
                });
            }else if(layEvent === 'getAppletsInfo'){
                //点击查看小程序基础信息，可以进行修改小程序基础信息
                var getAppletsInfo = "";
                layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                $.post("{{url('/api/customer/aliPay/getAliPayOpenMiniBaseInfoQuery')}}",{
                    mini_app_id:lineData.mini_app_id,
                    type:1
                },function(data){
                    var status = data.status;
                    var responseData = data.data;
                    var mini_category_list = responseData.mini_category_list;
                    if(status == 200){
                        layer.msg("正在打开", {
                            offset: '50px'
                            ,icon: 1
                            ,time: 1000
                        },function(){
                            getAppletsInfo = layer.open({
                                type: 1,
                                title: false,
                                closeBtn: 0,
                                area: '516px',
                                skin: 'layui-layer-nobg', //没有背景色
                                shadeClose: true,
                                content: $('#getAppletsInfo')
                            });

                            //设置值 {name值:val}
                            form.val("getAppletsInfo",{
                                "app_name": responseData.app_name ? responseData.app_name:"",
                                "app_english_name": responseData.app_english_name ? responseData.app_english_name:"",
                                "app_slogan": responseData.app_slogan ? responseData.app_slogan:"",
                                "app_desc": responseData.app_desc ? responseData.app_desc:"",
                                "service_phone": responseData.service_phone ? responseData.service_phone:"",
                                "service_email": responseData.service_email ? responseData.service_email:"",
                                "safe_domains": responseData.app_english_name ? responseData.safe_domains:""
                            });
                            //设置小程序logo
                            if(responseData.app_logo){
                                $("#app_logo").attr("src",responseData.app_logo);
                            }
                            //设置小程序类目
                            var category_names = responseData.category_names;
                            category_names = category_names.split(";");
                            category_names.pop();
                            $.each(category_names, function(index1, item) {
                                var category_names_list = category_names[index1].split("_");

                                $.each(category_names_list,function(index2){

                                    $.each(mini_category_list, function(index3, item) {
                                        if(mini_category_list[index3].parent_category_id == 0){
                                            if(mini_category_list[index3].category_name == category_names_list[0]){
                                                $('#mini_category_list'+index1+'_1').append("<option selected value='"+mini_category_list[index3].category_id+"'>"+mini_category_list[index3].category_name+"</option>");

                                                if(category_names_list[1]){
                                                    $.each(mini_category_list, function(index4, item) {
                                                        if(mini_category_list[index4].parent_category_id == mini_category_list[index3].category_id){
                                                            if(mini_category_list[index4].category_name == category_names_list[1]){
                                                                $('#mini_category_list'+index1+'_2').append("<option selected value='"+mini_category_list[index4].category_id+"'>"+mini_category_list[index4].category_name+"</option>");

                                                                if(category_names_list[2]){
                                                                    $.each(mini_category_list, function(index5, item) {
                                                                        if(mini_category_list[index5].parent_category_id == mini_category_list[index4].category_id){
                                                                            if(mini_category_list[index5].category_name == category_names_list[2]){
                                                                                $('#mini_category_list'+index1+'_3').append("<option selected value='"+mini_category_list[index5].category_id+"'>"+mini_category_list[index5].category_name+"</option>");
                                                                            }else{
                                                                                $('#mini_category_list'+index1+'_3').append("<option value='"+mini_category_list[index5].category_id+"'>"+mini_category_list[index5].category_name+"</option>");
                                                                            }
                                                                        }

                                                                    });
                                                                }
                                                            }else{
                                                                $('#mini_category_list'+index1+'_2').append("<option value='"+mini_category_list[index4].category_id+"'>"+mini_category_list[index4].category_name+"</option>");
                                                            }
                                                        }
                                                    });
                                                }

                                            }else{
                                                $('#mini_category_list'+index1+'_1').append("<option value='"+mini_category_list[index3].category_id+"'>"+mini_category_list[index3].category_name+"</option>");
                                            }

                                        }
                                    });
                                });
                            });

                            form.render("select");
                            //监听select事件
                            form.on('select(mini_category_list0_1)', function(data){
                                var value = data.value;
                                var mini_category_list2_content = "<option selected value=''>请选择</option>";
                                $('#mini_category_list0_3').html("");
                                if(value){
                                    $.each(mini_category_list, function(index, item) {
                                        if(mini_category_list[index].parent_category_id == value){
                                            mini_category_list2_content += "<option value='"+mini_category_list[index].category_id+"'>"+mini_category_list[index].category_name+"</option>";
                                        }
                                    });
                                }
                                $('#mini_category_list0_2').html(mini_category_list2_content);
                                form.render("select");
                            });
                            form.on('select(mini_category_list0_2)', function(data){
                                var value = data.value;
                                var mini_category_list3_content = "<option selected value=''>请选择</option>";
                                if(value){
                                    $.each(mini_category_list, function(index, item) {
                                        if(mini_category_list[index].parent_category_id == value){
                                            mini_category_list3_content+="<option value='"+mini_category_list[index].category_id+"'>"+mini_category_list[index].category_name+"</option>"
                                        }
                                    });
                                }
                                $('#mini_category_list0_3').html(mini_category_list3_content);
                                form.render("select");
                            });
                            form.on('select(mini_category_list1_1)', function(data){
                                var value = data.value;
                                var mini_category_list2_content = "<option selected value=''>请选择</option>";
                                $('#mini_category_list1_3').html("");
                                if(value){
                                    $.each(mini_category_list, function(index, item) {
                                        if(mini_category_list[index].parent_category_id == value){
                                            mini_category_list2_content += "<option value='"+mini_category_list[index].category_id+"'>"+mini_category_list[index].category_name+"</option>";
                                        }
                                    });
                                }
                                $('#mini_category_list1_2').html(mini_category_list2_content);
                                form.render("select");
                            });
                            form.on('select(mini_category_list1_2)', function(data){
                                var value = data.value;
                                var mini_category_list3_content = "<option selected value=''>请选择</option>";
                                if(value){
                                    $.each(mini_category_list, function(index, item) {
                                        if(mini_category_list[index].parent_category_id == value){
                                            mini_category_list3_content+="<option value='"+mini_category_list[index].category_id+"'>"+mini_category_list[index].category_name+"</option>"
                                        }
                                    });
                                }
                                $('#mini_category_list1_3').html(mini_category_list3_content);
                                form.render("select");
                            });
                        });
                    }else{
                        layer.msg(data.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 1000
                        });
                    }
                },"json");

                /**
                 * 上传小程序logo
                 **/
                upload.render({
                    elem: '#uploadFile', //绑定元素
                    url: "{{url('/api/customer/user/aliPayOfflineMaterialImageUpload')}}", //上传接口
                    data:{
                        app_id:lineData.mini_app_id
                    },
                    field:"file",
                    method:"post",
                    type : 'images',
                    ext : 'jpg|png|gif',
                    before : function(input){
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                    },
                    done: function(res){
                        //上传完毕回调
                        if(res.status == 200){
                            layer.msg("文件上传成功", {icon:1, shade:0.5, time:1000});
                            layui.jquery('#app_logo').attr("src", res.data.image_url);
                            $("input[name=license]").val(res.data.image_path);
                        }else{
                            layer.msg(res.message, {icon:2, shade:0.5, time:1000});
                        }
                    },
                    error: function(err){
                        //请求异常回调
                        console.log(err);
                    }
                });

                //点击submit进行提交
                form.on("submit(submitGetAppletsInfo)",function(data){
                    var requestData = data.field;
                    layer.confirm('确认进行修改吗?',{icon: 2}, function(index){
                        //设置小程序服务类目
                        var mini_category_ids  = "";
                        var mini_category_ids1 = "";
                        var mini_category_ids2 = "";
                        if(requestData.mini_category_list0_1){
                            mini_category_ids1 = mini_category_ids1 + requestData.mini_category_list0_1;
                        }
                        if(requestData.mini_category_list0_2){
                            mini_category_ids1 = mini_category_ids1 + '_' +requestData.mini_category_list0_2;
                        }
                        if(requestData.mini_category_list0_3){
                            mini_category_ids1 = mini_category_ids1 + '_' +requestData.mini_category_list0_3;
                        }
                        if(requestData.mini_category_list1_1){
                            mini_category_ids2 = mini_category_ids2 + requestData.mini_category_list1_1;
                        }
                        if(requestData.mini_category_list1_2){
                            mini_category_ids2 = mini_category_ids2 + '_' +requestData.mini_category_list1_2;
                        }
                        if(requestData.mini_category_list1_3){
                            mini_category_ids2 = mini_category_ids2 + '_' +requestData.mini_category_list1_3;
                        }

                        if(mini_category_ids1){
                            mini_category_ids = mini_category_ids + mini_category_ids1;
                        }
                        if(mini_category_ids2){
                            mini_category_ids = mini_category_ids + ';' + mini_category_ids2;
                        }

                        layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                        $.post("{{url('/api/customer/aliPay/getAliPayOpenMiniBaseInfoQuery')}}",{
                            type:2,
                            mini_app_id:lineData.mini_app_id,
                            app_version:lineData.app_version,
                            app_name:requestData.app_name,
                            app_english_name:requestData.app_english_name,
                            app_slogan:requestData.app_slogan,
                            app_logo:$("input[name=license]").val(),
                            app_desc:requestData.app_desc,
                            service_phone:requestData.service_phone,
                            service_email:requestData.service_email,
                            mini_category_ids:mini_category_ids
                        },function(data){
                            var status = data.status;
                            if(status == 200){
                                layer.msg(data.message, {
                                    offset: '50px'
                                    ,icon: 1
                                    ,time: 1000
                                },function(){
                                    layer.close(getAppletsInfo);
                                });
                            }else{
                                layer.msg(data.message, {
                                    offset: '50px'
                                    ,icon: 2
                                    ,time: 1000
                                });
                            }
                        },"json");
                    });
                });
            }else if(layEvent === 'offline'){
                //下架
                layer.confirm('确认下架该小程序版本吗?',{icon: 2}, function(index){
                    layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                    $.post("{{url('/api/customer/aliPay/getAliPayOpenMiniVersionOnline')}}",{
                        mini_app_id:lineData.mini_app_id,
                        app_version:lineData.app_version,
                        type:2
                    },function(data){
                        var status = data.status;
                        if(status == 200){
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 2000
                            },function(){
                                layer.close(index);
                            });
                        }else{
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 2000
                            });
                        }
                    },"json");
                });
            }else if(layEvent === 'cancelKaiFa'){
                //退回开发
                layer.confirm('确认退回开发状态吗?',{icon: 2}, function(index){
                    layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                    $.post("{{url('/api/customer/aliPay/getAliPayOpenMiniVersionAuditedCancel')}}",{
                        type:1,
                        mini_app_id:lineData.mini_app_id,
                        app_version:lineData.app_version
                    },function(data){
                        var status = data.status;
                        if(status == 200){
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 2000
                            },function(){
                                window.location.reload();
                            });
                        }else{
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 2000
                            });
                        }
                    },"json");
                });
            }else if(layEvent === 'auditCancel'){
                //小程序撤销审核
                layer.confirm('确认撤销审核吗?',{icon: 2}, function(index){
                    layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                    $.post("{{url('/api/customer/aliPay/getAliPayOpenMiniVersionAuditedCancel')}}",{
                        type:4,
                        mini_app_id:lineData.mini_app_id,
                        app_version:lineData.app_version
                    },function(data){
                        var status = data.status;
                        if(status == 200){
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 2000
                            },function(){
                                layer.close(index);
                                window.location.reload();
                            });
                        }else{
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 2000
                            });
                        }
                    },"json");
                });
            }else if(layEvent === 'versionDelete'){
                //删除此版本
                layer.confirm('确认删除此版本吗?',{icon: 2}, function(index){
                    layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                    $.post("{{url('/api/customer/aliPay/getAliPayOpenMiniVersionAuditedCancel')}}",{
                        type:3,
                        mini_app_id:lineData.mini_app_id,
                        app_version:lineData.app_version
                    },function(data){
                        var status = data.status;
                        if(status == 200){
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 2000
                            },function(){
                                layer.close(index);
                                window.location.reload();
                            });
                        }else{
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 2000
                            });
                        }
                    },"json");
                });
            }else if(layEvent === "getAliPayOpenAuthAppAesGet"){
                layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                $.post("{{url('/api/customer/aliPay/getAliPayOpenAuthAppAesGet')}}",{
                    // mini_app_id:lineData.mini_app_id,
                    mini_app_id:"2021002108609082",
                },function(data){
                    var status = data.status;
                    if(status == 200){
                        layer.msg(data.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 2000
                        });
                    }else{
                        layer.msg(data.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 2000
                        });
                    }
                },"json");
            }
        });
    });

</script>
</html>