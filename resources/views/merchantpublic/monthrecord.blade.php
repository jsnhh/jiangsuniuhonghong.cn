<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>流水查询</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
  <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
  <style>
    .edit{background-color: #ed9c3a;}
    .shenhe{background-color: #429488;}
    .see{background-color: #7cb717;}
    .tongbu{background-color: #4c9ef8;color:#fff;}
    .cur{color:#009688;}
  </style>
</head>
<body>

  <div class="layui-fluid" style="margin-top: 50px;">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">

        <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
              <div class="layui-card">
                <div class="layui-card-header">月对账中心</div>

                <div class="layui-card-body">
                  <div class="layui-btn-container" style="font-size:14px;">
                    <!-- 选择业务员 -->
                    <div class="layui-form" lay-filter="component-form-group" style="width:300px;display: inline-block;">
                      <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:0">
                            <select name="agent" id="agent" lay-filter="agent" lay-search>

                            </select>
                        </div>
                      </div>
                    </div>
                    <!-- 学校 -->
                    <!-- <div class="layui-form" lay-filter="component-form-group" style="width:300px;display: inline-block;">
                      <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:0">
                            <select name="schooltype" id="schooltype" lay-filter="schooltype" lay-search>

                            </select>
                        </div>
                      </div>
                    </div> -->
                    <!-- 通道类型 -->
                    <div class="layui-form" lay-filter="component-form-group" style="width:300px;display: inline-block;">
                      <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:0">
                            <select name="passway" id="passway" lay-filter="passway">

                            </select>
                        </div>
                      </div>
                    </div>
                    <!-- 支付类型 -->
                    <div class="layui-form" lay-filter="component-form-group" style="width:300px;display: inline-block;">
                      <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:0">
                            <select name="type" id="type" lay-filter="type">
                              <option value="">选择支付类型</option>
                              <option value="alipay">支付宝</option>
                              <option value="weixin">微信</option>
                              <option value="alipay_face">支付宝刷脸</option>
                              <option value="weixin_face">微信刷脸</option>
                              <option value="member">会员支付</option>
                              <option value="jd">京东</option>
                              <option value="unionpay">银联刷卡</option>
                              <option value="unionpayqr">银联扫码</option>
                              <option value="unionpaysf">银联闪付</option>
                            </select>
                        </div>
                      </div>
                    </div>


                    <!-- 缴费时间 -->
                    <div class="layui-form" style="display: inline-block;">
                      <div class="layui-form-item">
                        <div class="layui-inline">

                          <div class="layui-input-inline">
                            <input type="text" class="layui-input start-item test-item" placeholder="请选择月份
                            " lay-key="23">
                          </div>
                        </div>

                      </div>
                    </div>
                    <!-- 搜索 -->
                    <div class="layui-form" lay-filter="component-form-group" style="width:600px;display: inline-block;">
                      <div class="layui-form-item">
                          <div class="layui-inline">
                            <button class="layui-btn layuiadmin-btn-list" lay-submit="" lay-filter="LAY-app-contlist-search" style="margin-bottom: 0;height:36px;line-height: 36px;">
                              <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                            </button>
                          </div>
                          {{--<button class="layui-btn export"  style="margin-bottom: 4px;height:36px;line-height: 36px;">导出</button>   <!-- onclick="exportdata()" -->--}}
                        </div>
                    </div>

                  </div>

                  <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                  <!-- 判断状态 -->
                  <script type="text/html" id="statusTap">
                    @{{#  if(d.pay_status == 1){ }}
                      <span class="cur">@{{ d.pay_status_desc }}</span>
                    @{{#  } else { }}
                      @{{ d.pay_status_desc }}
                    @{{#  } }}
                  </script>
                  <!-- 判断状态 -->
                  <script type="text/html" id="paymoney">
                    @{{ d.rate }}%
                  </script>
                  <!-- 积分抵扣 -->
                  <script type="text/html" id="jfdkTap">
                    @{{ d.dk_jf }}/@{{ d.dk_money }}
                  </script>

                  <script type="text/html" id="table-content-list" class="layui-btn-small">
                    <a class="layui-btn layui-btn-normal layui-btn-xs tongbu" lay-event="tongbu">同步状态</a>
                    <a class="layui-btn layui-btn-normal layui-btn-xs tui" lay-event="tui">退款</a>
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  <input type="hidden" class="store_id">
  <input type="hidden" class="source_type">
  <input type="hidden" class="type">
  <input type="hidden" class="starttime">

  <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
  <script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script>
<script type="text/javascript" src="{{asset('/school/js/jsencrypt.min.js')}}"></script>
    <script>
    var token = sessionStorage.getItem("Publictoken");
    var str=location.search;
    var store_id = sessionStorage.getItem("store_store_id");


    layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate;
      // 未登录,跳转登录页面
      $(document).ready(function(){
          if(token==null){
              window.location.href="{{url('/mb/login')}}";
          }
      })
        // 选择门店
        $.ajax({
            url : "{{url('/api/merchant/get_merchant')}}",
            data : {token:token,l:100},
            type : 'post',
            success : function(data) {
              console.log(data);
              var optionStr = "";
              for(var i=0;i<data.data.length;i++){
                optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
              }
              $("#agent").append('<option value="">选择门店</option>'+optionStr);
              layui.form.render('select');
            },
            error : function(data) {
                alert('查找板块报错');
            }
        });
        // 选择通道
        $.ajax({
          url : "{{url('/api/merchant/store_all_pay_way_lists')}}",
          data : {token:token,l:100},
          type : 'post',
          dataType:'json',
          success : function(data) {
              console.log(data);
              var optionStr = "";
                  for(var i=0;i<data.data.length;i++){
                      optionStr += "<option value='" + data.data[i].company + "'>"
                          + data.data[i].company_desc + "</option>";
                  }
                  $("#passway").append('<option value="">选择通道类型</option>'+optionStr);
                  layui.form.render('select');
          },
          error : function(data) {
              alert('查找板块报错');
          }
        });



        // 渲染表格
        table.render({
            elem: '#test-table-page'
            ,url: "{{url('/api/merchant/Order_mouth_count')}}"
            ,method: 'post'
            ,where:{
              token:token
            }
            ,request:{
              pageName: 'p',
              limitName: 'l'
            }
            ,page: true
            ,cellMinWidth: 150
            ,cols: [[
              {field:'store_id',align:'center', title: '门店ID'}
              ,{field:'store_name',align:'center', title: '门店名称'}
              ,{field:'month',align:'center', title: '月份'}
              // ,{field:'company', title: '通道'}
              ,{field:'source_type',align:'center', title: '支付方式'}
              ,{field:'total_amount',align:'center', title: '交易金额'}
              ,{field:'order_sum',align:'center', title: '交易笔数'}
              ,{field:'refund_amount',align:'center',  title: '退款金额'}
              ,{field:'refund_count',align:'center', title: '退款笔数'}
              ,{field:'fee_amount', align:'center', title: '手续费'}
              // ,{width:150,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
            ]]
            ,response: {
              statusName: 'status' //数据状态的字段名称，默认：code
              ,statusCode: 1 //成功的状态码，默认：0
              ,msgName: 'message' //状态信息的字段名称，默认：msg
              ,countName: 't' //数据总数的字段名称，默认：count
              ,dataName: 'data' //数据列表的字段名称，默认：data
            }
            ,done: function(res, curr, count){
              console.log(res);
              $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});	//进行表头样式设置
            }

        });




        // 选择门店
        form.on('select(agent)', function(data){
          var store_id = data.value;
          $('.store_id').val(store_id);
          // //执行重载
          // table.reload('test-table-page', {
          //   where: {
          //     store_id: $(".store_id").val(),

          //   }
          // });
          // 选择收银员


        });



        // 选择通道
        form.on('select(passway)', function(data){
          var type = data.value;
          $('.type').val(type);

        });
        // 选择支付类型
        form.on('select(type)', function(data){
          var pay_type = data.value;
          $('.source_type').val(pay_type);

        });



        laydate.render({
          elem: '.start-item'
          ,type: 'month'
          ,done: function(value){
            var reg = new RegExp("-","");
            var a = value.replace(reg,"");
            console.log(a);
            $('.starttime').val(a)
            //执行重载
            // acount();
          }
        });


        //监听搜索
        form.on('submit(LAY-app-contlist-search)', function(data){
          // var obj = data.field
          // console.log(obj)
          // var store_name = data.field.schoolname;
          // console.log(data);
          console.log($('.starttime').val());
          console.log($('.store_id').val());
          console.log($('.source_type').val());
          console.log($('.type').val());


          //执行重载
          table.reload('test-table-page', {
            page: {
             curr: 1
            },
            where: {
              store_id:$('.store_id').val(),
              month:$('.starttime').val(),
              source_type:$('.source_type').val(),
              company:$('.type').val(),
            }
          });
        });

        $('.export').click(function(){
          var store_id=$('.store_id').val();
          var month=$('.starttime').val();
          var source_type=$('.source_type').val();
          var company=$('.type').val();

          window.location.href="{{url('/api/export/OrdermouthcountdataDown')}}"+"?token="+token+"&store_id="+store_id+"&month="+month+"&source_type="+source_type+"&company="+company;
        })

    });

  </script>

</body>
</html>





