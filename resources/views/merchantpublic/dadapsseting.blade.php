<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>达达配送设置</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
<link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
<style>
    .img{width:130px;height:90px;overflow: hidden;}
    .img img{width:100%;height:100%;}
    .layui-layer-nobg{width: none !important;}
    /*.layui-layer-content{width:600px;height:550px;}*/
    .layui-card-header{width:200px;text-align: left;float:left;}
    .layui-card-body{margin-left:28px;}
    .layui-upload-img{width: 120px; height: 120px; /*margin: 0 10px 10px 0;*/}
    .up{position: relative;display: inline-block;cursor: pointer;border-color: #1ab394; color: #FFF;width: auto !important;font-size: 10px !important;text-align: center !important;}
    .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
    .layui-upload-list{width: 120px;height:120px;overflow: hidden;margin: 10px auto;}
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {-webkit-appearance: none !important;margin: 0;}
    .edit{display: none}
    .forget_mima{display: none}
</style>
</head>
<body>

<div class="layui-fluid" style="margin-top:50px;">
    <div class="layui-card">
        <div class="layui-card-header">达达配送设置</div>
        <div class="layui-card-body layui-row layui-col-space10">
            <div class="layui-form">

                <div class="layui-form-item">
                    <label class="layui-form-label">应用Key(app_key)</label>
                    <div class="layui-input-block">
                        <input type="password" name="appKey" autocomplete="off" placeholder="请输入应用Key" class="layui-input appKey">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">应用secret(app_secret)</label>
                    <div class="layui-input-block">
                        <input type="password" name="appSecret" autocomplete="off" placeholder="请输入应用secret" class="layui-input appSecret">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">商户编号(创建商户账号分配的编号)</label>
                    <div class="layui-input-block">
                        <input type="password" name="sourceId" autocomplete="off" placeholder="请输入商户编号" class="layui-input sourceId">
                    </div>
                </div>

            </div>
            <div class="layui-form-item layui-layout-admin">
                <div class="layui-input-block">
                    <div class="layui-footer" style="left: 0;">
                        <button class="layui-btn submit">保存</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script type="text/javascript" src="{{asset('/school/js/jsencrypt.min.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Publictoken");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'table','form','upload'], function(){
        var $ = layui.$;
            admin = layui.admin
            ,table = layui.table
            ,element = layui.element
            ,upload = layui.upload
            ,form = layui.form;

            element.render();
        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }

            $.post("{{url('/api/merchant/dada_delivery_setting')}}",
            {
                token:token
                ,type:1
            },function(res){

            },"json");

        });


    });
</script>
</body>
</html>
