<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>会员列表</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
  <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
  <style>
    .edit{background-color: #ed9c3a;}
    .shenhe{background-color: #429488;}
    .see{background-color: #7cb717;}
    .tongbu{background-color: #4c9ef8;color:#fff;}
    .cur{color:#009688;}
    .yname{font-size: 13px;color: #444;}
    .xgrate{color: #fff;font-size: 15px;padding: 7px;height: 30px;line-height: 30px;background-color: #3475c3;}
    .input_block{
      display: block;
    }
  </style>
</head>
<body>
  <div class="layui-fluid" style="margin-top:50px;">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">

        <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
              <div class="layui-card">
                <div class="layui-card-header">会员列表</div>

                <div class="layui-card-body">
                  <div class="layui-btn-container" style="font-size:14px;">

                    <!-- 缴费时间 -->
                    <div class="layui-form" style="width:100%;display:flex;">
                          <!-- 选择业务员 -->
                      <div class="layui-form" lay-filter="component-form-group" style="width:190px;margin-right:10px;display: inline-block;">
                      <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:0">
                            <text class="yname">请选择门店</text>
                            <select name="agent" id="agent" lay-filter="agent" lay-search></select>
                        </div>
                      </div>

                    </div>

                      <div class="layui-form-item">
                        <div class="layui-inline"style="margin-right:0">
                          <div class="layui-input-inline">
                          <text class="yname">订单开始时间</text>
                            <input type="text" class="layui-input start-item test-item" placeholder="开始时间" lay-key="23">
                          </div>
                        </div>
                        <div class="layui-inline"style="margin-right:0">
                          <div class="layui-input-inline">
                          <text class="yname">订单结束时间</text>
                            <input type="text" class="layui-input end-item test-item" placeholder="结束时间" lay-key="24">
                          </div>
                        </div>
                        <div class="layui-inline">
                          <div class="layui-input-inline">
                          <text class="yname">请输入会员</text>
                            <input type="text" name="tradeno" placeholder="请输入会员卡号/姓名/手机号" autocomplete="off" class="layui-input dingdan">
                          </div>
                        </div>
                        <div class="layui-inline">
                          <button class="layui-btn layuiadmin-btn-list" lay-submit="" lay-filter="LAY-app-contlist-search" style="border-radius:5px;margin-bottom: -1.2rem;height:36px;line-height: 36px;">
                            <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                          </button>
                        </div>

                        <div class="layui-inline" id="templateShow">
                          <button class="layui-btn layuiadmin-btn-list" lay-submit="" lay-filter="LAY-app-contlist-template" style="border-radius:5px;margin-bottom: -1.2rem;height:36px;line-height: 36px;">
                            发送模板消息
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>

                  <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                  <!-- 判断状态 -->
                  <script type="text/html" id="statusTap">
                    @{{#  if(d.mb_status == 1){ }}
                      激活
                    @{{#  } else if(d.mb_status == 2){ }}
                      未激活
                    @{{#  } else { }}
                      关闭
                    @{{#  } }}
                  </script>
                  <!-- 判断状态 -->
                  <script type="text/html" id="paymoney">
                    @{{ d.rate }}%
                  </script>
                  <script type="text/html" id="table-content-list" class="layui-btn-small">
                    <a class="layui-btn layui-btn-normal layui-btn-xs tongbu" lay-event="edit">编辑</a>
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
  <input type="hidden" class="store_id">
  <input type="hidden" class="sort">
  <input type="hidden" class="stu_class_no">

  <input type="hidden" class="stu_order_batch_no">
  <input type="hidden" class="user_id">

  <input type="hidden" class="pay_status">
  <input type="hidden" class="pay_type">


  <div id="useThisTemplateIndex" class="hide layui-form" lay-filter="useThisTemplateIndex" style="display: none;background-color: #fff;">
    <div class="xgrate">选择模板消息</div>
    <div class="layui-card-body" style="padding: 15px;">
      <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief" style="margin: 0px;">
        <ul class="layui-tab-title">
          <li class="layui-this tab_template" data-type="1">微信模板列表</li>
          <!-- <li class="tab_template" data-type="2">支付宝模板列表</li> -->
        </ul>
        <div class="layui-tab-content" style="height: 100px;">
          <div class="layui-tab-item layui-show">
            <div class="layui-form-item">
              <div class="layui-input-block weChatTemplate" style="margin-left: 0px;"></div>
            </div>
          </div>
          <div class="layui-tab-item">
            <div class="layui-form-item">
              <div class="layui-input-block aliPayTemplate" style="margin-left: 0px;"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    
  <div id="templateInfo" class="hide layui-form" lay-filter="templateInfo" style="display:none;background-color: #fff;">
    <div class="xgrate">输入模板消息内容111</div>
    <div class="layui-card-body form_info_template" lay-filter="templateFormInfo" style="padding: 15px;">

    </div>
  </div>

  <script type="text/html" id="mb_money_template">
    @{{ d.mb_used_money }} / @{{  d.mb_used_points }}
  </script>

  <script src="https://www.layuicdn.com/layui/layui.js"></script>
  <script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script>
  <script>
    var token = sessionStorage.getItem("Publictoken");
    var str=location.search;
    var store_id = sessionStorage.getItem("store_store_id");

    layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate;
        // 未登录,跳转登录页面
        $(document).ready(function(){
          if(token==null){
              window.location.href="{{url('/mb/login')}}";
          }

          // 选择门店
          $.ajax({
              url : "{{url('/api/merchant/store_lists')}}",
              data : {
                token:token,
                l:100
              },
              type : 'post',
              success : function(data) {
                console.log(data.data[0].is_send_tpl_mess)
                if(data.data[0].is_send_tpl_mess==2){
                  document.getElementById("templateShow").style.display="none";
                }
                  $('.store_id').val(data.data[0].store_id);
                  var optionStr = "";
                  for(var i=0;i<data.data.length;i++){
                      optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
                      // optionStr += "<option value='" + data.data[i].store_id + "' "+((store_id==data.data[i].store_id)?"selected":"")+">" + data.data[i].store_name + "</option>";
                  }
                  // 渲染表格
                  table.render({
                      elem: '#test-table-page'
                      ,url: "{{url('/api/member/wechatMemberLists')}}"
                      ,method: 'post'
                      ,where:{
                          token:token,
                          store_id:data.data[0].store_id
                      }
                      ,request:{
                          pageName: 'p',
                          limitName: 'l'
                      }
                      ,page: true
                      ,cellMinWidth: 150
                      ,cols: [[
                          {type:'checkbox',},
                          // {align:'center',field:'mb_nick_name', title: '会员昵称'}
                          {align:'center',field:'mb_nickname', title: '会员名称'}
                          ,{align:'center',field:'mb_ver_desc',  title: '会员等级',width:90}
                          // ,{align:'center',field:'mb_name', title: '会员名称'}
                          ,{align:'center',field:'mb_phone',  title: '手机号'}
                          ,{align:'center',field:'mb_id', title: '会员卡号',width:230}
                          ,{align:'center',field:'mb_money',  title: '累计消费/积分消费', templet: '#mb_money_template'}
                          ,{align:'center',field:'mb_pay_counts',  title: '总消费次数',width:100}
                          ,{align:'center',field:'mb_money',  title: '会员余额'}
                          ,{align:'center',field:'mb_points',  title: '积分',width:100}
                          // ,{align:'center',field:'mb_status', title: '会员状态',templet:'#statusTap',width:100}
                          // ,{align:'center',field:'mb_time',  title: '激活时间'}
                          ,{align:'center',field:'updated_at',  title: '最后支付日期'}
                          ,{width:100,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
                      ]]
                      ,response: {
                          statusName: 'status' //数据状态的字段名称，默认：code
                          ,statusCode: 1 //成功的状态码，默认：0
                          ,msgName: 'message' //状态信息的字段名称，默认：msg
                          ,countName: 't' //数据总数的字段名称，默认：count
                          ,dataName: 'data' //数据列表的字段名称，默认：data
                      }
                      ,
                      parseData:function(res){
                        var newarrlist = res.data;
                        let yuming = window.location.host;
                        if(yuming.indexOf("test.yunsoyi.cn") >= 0){
                          for(let u=0;u<newarrlist.length;u++){
                            newarrlist[u].mb_phone = "";
                          }
                        }
                        return {
                          data: newarrlist,
                          message: res.message,
                          order_data: res.order_data,
                          p: res.p,
                          status: res.status,
                          t: res.t,
                        }
                      }
                      ,done: function(res, curr, count){
                          $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});	//进行表头样式设置
                      }
                  });
                  $("#agent").append(optionStr);
                  layui.form.render('select');
              },
              error : function(data) {
                  alert('查找板块报错');
              }
          });
        });

        //默认当前是 微信模板列表
        var type = 1;
        //点击切换tab栏
        $(".tab_template").on("click",function(){
          var data_type = $(this).data("type");
          if(data_type){
            type = data_type;
          }
        });

        /**
         * 点击 发送模板消息 按钮
         */
        //模板弹框
        var useThisTemplateIndex;
        //微信模板数据
        var weChatTemplateData;
        //支付宝模板数据
        var aliPayTemplateData;
        form.on('submit(LAY-app-contlist-template)', function(data){
          var value = data.field;
          //先判断发送给哪些人员
          var checkStatus   = table.checkStatus('test-table-page');
          var tableLineData = checkStatus.data;
          if(tableLineData.length <= 0){
            layer.msg("请选择接收消息的人员", {
              offset: '50px'
              ,icon: 2
              ,time: 2000
            });
            return false;
          }
          //判断当前是哪个门店
          var store_id = $("select[name=agent]").val();
          if(!store_id){
            layer.msg("请选择门店", {
              offset: '50px'
              ,icon: 2
              ,time: 2000
            });
            return false;
          }

          //根据门店获取该门店下面的微信小程序，支付宝小程序对应的模板消息列表
          $.post("{{url('/api/customer/store/getStoreAppletTemplate')}}",{

            store_id:store_id,

          },function(data){
            var status = data.status;
            if(status == 200){
              var weChatTemplate = data.data.weChatTemplate;
              var aliPayTemplate = data.data.aliPayTemplate;
              weChatTemplateData = data.data.weChatTemplate;
              aliPayTemplateData = data.data.aliPayTemplate;

              //弹出选择框（微信模板，还是支付宝模板）
              useThisTemplateIndex = layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area: '516px',
                skin: 'layui-layer-nobg', //没有背景色
                shadeClose: true,
                content: $('#useThisTemplateIndex')
              });

              var weChatTemplateHtml = "";
              if(weChatTemplate.length > 0){
                $.each(weChatTemplate,function(index,item){
                  weChatTemplateHtml += "<div class=\"input_block\">\n" +
                          "                  <input type=\"radio\" name=\"template_id\" lay-filter=\"template_radio\" value=\""+item.template_id+"\" title=\""+item.title+"\">\n" +
                          "                </div>";
                });
              }
              $(".weChatTemplate").html(weChatTemplateHtml);

              var aliPayTemplateHtml = "";
              if(aliPayTemplate.length > 0){
                $.each(aliPayTemplate,function(index,item){
                  aliPayTemplateHtml += "<div class=\"input_block\">\n" +
                          "                  <input type=\"radio\" name=\"template_id\" lay-filter=\"template_radio\" value=\""+item.template_id+"\" title=\""+item.title+"\">\n" +
                          "                </div>";
                });
              }
              $(".aliPayTemplate").html(aliPayTemplateHtml);
              form.render();
            }else{
              layer.msg(data.message, {
                offset: '50px'
                ,icon: 2
                ,time: 1000
              });
            }
          },"json");
        });

        /**
         * 选择消息模板 监听事件
         */
        var template_id;
        form.on('radio(template_radio)', function(data){
          template_id = data.value;
          var templateData;
          if(type == 1){
            $.each(weChatTemplateData,function(index,item){
              if(item.template_id == data.value){
                templateData = weChatTemplateData[index];
              }
            });
          }else{
            $.each(aliPayTemplateData,function(index,item){
              if(item.template_id == data.value){
                templateData = aliPayTemplateData[index];
              }
            });
          }

          var template_input = "";
          if(templateData.template_input.length > 0){
            $.each(templateData.template_input,function(index,item){
              template_input += "<div class=\"layui-form-item\">\n" +
                      "                  <label class=\"layui-form-label\">"+item+"</label>\n" +
                      "                  <div class=\"layui-input-block\">\n" +
                      "                   <input type=\"text\" name=\""+templateData.keywords[index]+"\" placeholder=\""+templateData.keywords[index]+"，必填项"+"\" class=\"layui-input\">\n" +
                      "                  </div>\n" +
                      "                  </div>";
            });
            template_input += "<div class=\"layui-form-item\">\n" +
                    "                        <div class=\"layui-input-block\">\n" +
                    "                            <button type=\"submit\" class=\"layui-btn\" lay-submit=\"\" lay-filter=\"templateSubmitFormInfo\">发送模板消息</button>\n" +
                    "                        </div>\n" +
                    "                    </div>";
          }
          $(".form_info_template").html(template_input);
          var templateInfo = layer.open({
            type: 1,
            title: false,
            closeBtn: 0,
            area: '516px',
            skin: 'layui-layer-nobg', //没有背景色
            shadeClose: true,
            content: $('#templateInfo')
          });
          form.render();
        });

        /**
         * 点击 发送模板消息按钮
         */
        form.on('submit(templateSubmitFormInfo)', function(data){
          var value         = data.field;
          var key;
          var sendTemplateData  = [];
          for (key in value){
            var keyPre = key.split(".");
            if(!value[key]){
              layer.msg("模板消息内容不可为空", {
                offset: '50px'
                ,icon: 2
                ,time: 2000
              });
              return false;
            }else{
              sendTemplateData.push([keyPre[0],value[key]]);
            }
          }

          if(!template_id){
            layer.msg("模板id为空，请选择模板", {
              offset: '50px'
              ,icon: 2
              ,time: 2000
            });
            return false;
          }

          var checkStatus   = table.checkStatus('test-table-page');
          
          var tableLineData = checkStatus.data;
          console.log(tableLineData)
          if(tableLineData.length <= 0){
            layer.msg("请选择接收消息的人员", {
              offset: '50px'
              ,icon: 2
              ,time: 2000
            });
            return false;
          }

          var sendUser = [];


          tableLineData.forEach(element => {

            sendUser.push(element.applet_openid);

          });

          console.log(sendUser)
          if(sendUser.length <= 0){
            layer.msg("请保证接收消息的人员信息不为空", {
              offset: '50px'
              ,icon: 2
              ,time: 2000
            });
            return false;
          }

          //判断当前是哪个门店
          var store_id = $("select[name=agent]").val();
          if(!store_id){
            layer.msg("请选择门店", {
              offset: '50px'
              ,icon: 2
              ,time: 2000
            });
            return false;
          }

          layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
          $.post("{{url('/api/customer/weixin/sendTemplateInfo')}}",{
            store_id:store_id,
            type:type,
            sendTemplateData:sendTemplateData,
            sendUser:sendUser,
            template_id:template_id,
          },function(data){
            var status = data.status;
            if(status == 200){
              layer.msg("模板消息发送成功，具体根据用户收到消息为准", {
                offset: '50px'
                ,icon: 1
                ,time: 2000
              });
            }else{
              layer.msg(data.message, {
                offset: '50px'
                ,icon: 2
                ,time: 2000
              });
            }
          },"json");
          return false;
        });


        table.on('tool(test-table-page)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
          var e = obj.data; //获得当前行数据
          var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
          var tr = obj.tr; //获得当前行 tr 的DOM对象
//          console.log(e);
          // sessionStorage.setItem('s_store_id', e.store_id);

        //   {{--if(layEvent === 'tongbu'){ //审核--}}
        //     {{--$.post("{{url('/api/basequery/update_order')}}",--}}
        //     {{--{--}}
        //         {{--token:token,--}}
        //         {{--store_id:e.store_id,--}}
        //         {{--out_trade_no:e.out_trade_no--}}
        //     {{--},function(res){--}}
        //         {{--console.log(res);--}}
        //         {{--if(res.status==1){--}}
        //             {{--layer.msg(res.message, {--}}
        //                 {{--offset: '50px'--}}
        //                 {{--,icon: 1--}}
        //                 {{--,time: 2000--}}
        //             {{--});--}}
        //         {{--}else{--}}
        //             {{--layer.msg(res.message, {--}}
        //                 {{--offset: '50px'--}}
        //                 {{--,icon: 2--}}
        //                 {{--,time: 2000--}}
        //             {{--});--}}
        //         {{--}--}}
        //     {{--},"json");--}}
        //   {{--}--}}

        //   var data = obj.data;
        //   if(obj.event === 'setSign'){
        //     layer.open({
        //       type: 2,
        //       title: '模板详细',
        //       shade: false,
        //       maxmin: true,
        //       area: ['60%', '70%'],
        //       content: "{{url('/merchantpc/paydetail?')}}"+e.stu_order_type_no
        //     });
        //   }
        // });

      // 关闭弹窗
      $(".close").on("click",function(){
        document.location.reload();
      })

        // 选择门店
        form.on('select(agent)', function(data){
          var store_id = data.value;
          $('.store_id').val(store_id);
          //执行重载
          table.reload('test-table-page', {
            where: {
              store_id: $(".store_id").val()
            }
          });
        });

        laydate.render({
          elem: '.start-item'
          ,type: 'datetime'
          ,done: function(value){
            //执行重载
            table.reload('test-table-page', {
              where: {
                time_start:value,
                time_end:$('.end-item').val()
              }
            });
          }
        });

        laydate.render({
          elem: '.end-item'
          ,type: 'datetime'
          ,done: function(value){
            //执行重载
            table.reload('test-table-page', {
              where: {
                time_start:$('.start-item').val(),
                time_end:value
              }
            });
          }
        });

        form.on('submit(LAY-app-contlist-search)', function(data){
          var mb_id = data.field.tradeno;
//          console.log(data);
          //执行重载
          table.reload('test-table-page', {
            where: {
              mb_id:mb_id,
            }
          });
        });

        // 导出
        // {{--function exportdata(){--}}
        //   {{--var store_id=$('.store_id').val();--}}
        //   {{--var merchant_id=$('.user_id').val();--}}
        //   {{--var sort=$('.sort').val();          --}}
        //   {{--var pay_status=$('.pay_status').val();--}}
        //   {{--var ways_source=$('.pay_type').val();--}}

        //   {{--var time_start=$('.start-item').val();--}}
        //   {{--var time_end=$('.end-item').val();--}}
        //   {{----}}
        //   {{--var out_trade_no=$('.danhao').val();--}}
        //   {{--var trade_no=$('.tiaoma').val();--}}

        //   {{--window.location.href="{{url('/api/export/MerchantOrderExcelDown')}}"+"?token="+token+"&store_id="+store_id+"&merchant_id="+merchant_id+"&sort="+sort+"&pay_status="+pay_status+"&ways_source="+ways_source+"&time_start="+time_start+"&time_end="+time_end+"&out_trade_no="+out_trade_no+"&trade_no="+trade_no;     --}}

        // {{--}--}}

        // {{--$('.export').click(function(){--}}
        //   {{--var store_id=$('.store_id').val();--}}
        //   {{--var merchant_id=$('.user_id').val();--}}
        //   {{--var sort=$('.sort').val();          --}}
        //   {{--var pay_status=$('.pay_status').val();--}}
        //   {{--var ways_source=$('.pay_type').val();--}}

        //   {{--var time_start=$('.start-item').val();--}}
        //   {{--var time_end=$('.end-item').val();--}}
        //   {{----}}
        //   {{--var out_trade_no=$('.danhao').val();--}}
        //   {{--var trade_no=$('.tiaoma').val();--}}

        //   {{--window.location.href="{{url('/api/export/MerchantOrderExcelDown')}}"+"?token="+token+"&store_id="+store_id+"&merchant_id="+merchant_id+"&sort="+sort+"&pay_status="+pay_status+"&ways_source="+ways_source+"&time_start="+time_start+"&time_end="+time_end+"&out_trade_no="+out_trade_no+"&trade_no="+trade_no;     --}}
        // {{--})--}}

    });
});

  </script>

  <!-- 编辑 -->
  <div id="editModal" class="hide layui-form" lay-filter="appletsName" style="display: none;background-color: #fff;">
    <div class="xgrate">编辑</div>
    <div class="layui-card-body" style="padding: 15px;">
      <div class="layui-form" style="padding-top: 20px;padding-bottom: 20px;">
        <div class="layui-form-item">
            <label class="layui-form-label" style="text-align:center">会员等级</label>
            <div class="layui-input-block">
                <select name="vip_level_edit" id="vip_level_edit" lay-search>
                  <option id="vip_level_edit_option"></option>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
          <label class="layui-form-label" style="text-align:center">虚拟消费金额</label>
          <div class="layui-input-block">
            <input type="text" placeholder="请输入虚拟消费金额" name="virtual_money_edit" id="virtual_money_edit"  class="layui-input title">
          </div>
        </div>
        <div class="layui-form-item">
          <label class="layui-form-label" style="text-align:center">手机号码</label>
          <div class="layui-input-block">
            <input type="text" placeholder="请输入手机号码" name="phone_edit" id="phone_edit"  class="layui-input title">
          </div>
        </div>
        <div class="layui-form-item">
          <label class="layui-form-label" style="text-align:center">余额</label>
          <div class="layui-input-block">
            <input type="text" placeholder="请输入余额" name="mb_money_edit" id="mb_money_edit"  class="layui-input title">
          </div>
        </div>
        <div class="layui-form-item">
          <label class="layui-form-label" style="text-align:center">积分</label>
          <div class="layui-input-block">
            <input type="text" placeholder="请输入积分" name="mb_jf_edit" id="mb_jf_edit"  class="layui-input title">
          </div>
        </div>
      </div>
      <div class="layui-form-item">
        <div class="layui-input-block">
          <div class="layui-footer" style="float: right;">
            <button class="layui-btn close"style="border-radius:5px;background-color: #FFB800;">关闭</button>
            <button class="layui-btn submitEdit" lay-submit lay-filter="submitEditTwo" style="border-radius:5px">确定</button>
          </div>
        </div>
      </div>
    </div>
  </div>

</body>
</html>







