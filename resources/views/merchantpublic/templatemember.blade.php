<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>模板设置</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
  <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
  <style>
    .edit{background-color: #ed9c3a;}
    .shenhe{background-color: #429488;}
    .see{background-color: #7cb717;}
    .tongbu{background-color: #4c9ef8;color:#fff;}
    .cur{color:#009688;}
    .bj{
      background: url('{{asset('/mb/background.png')}}') no-repeat; width:100%;
      height:800px;background-size:contain;position: relative;
    }
    .bj .img{
      width:86%;
      height:160px;
      top:53px;
      left: 7%;
      position: absolute;
    }
    .bj .logo{
      width:35px;
      height:35px;
      margin-top: 60px;
      margin-left: 10%;
      position: absolute;
    }
    .bj .name{
      position: absolute;
      margin-top: 100px;
      margin-left: 11%;
      color:#fff;
      font-size:17px;
    }
    .bj .name p:last-child{
      font-size:14px;
      margin-top:2px;
    }
    .bj .code{
      width:20px;
      height:20px;
      position: absolute;
      margin-top: 115px;
      right: 11%;
    }
    .bj .num{
      position: absolute;
      top: 160px;
      left: 10%;
      color:#fff;
      font-size:22px;
    }
    .min_img {
      width: 60px;
      height: 70px;
      margin-right: 10px;
      float:left;
      overflow: hidden;
    }
    .min_img img{
      width: 60px;
      height: 38px;
      margin-right: 10px;
    }
    .layui-form-radio{
      margin-left: 20px;
    }
    .layui-form-label span{
      color:red;
      padding-right:5px;
    }
  </style>
</head>
<body>

  <div class="layui-fluid" style="margin-top:50px;">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">

        <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
              <div class="layui-card">
                <div class="layui-card-header">模板设置</div>

                <div class="layui-card-body">

                    <div class="layui-row">
                      <div class="layui-col-md4">
                        <div class="bj">
                          <image class='img' src="{{asset('/mb/vip-bg-1.png')}}"></image>
                          <image class='logo' src="{{asset('/mb/vip.png')}}"></image>
                          <div class="name">
                            <p class="store_name"></p>
                            <p class="store_desc"></p>
                          </div>
                          <image class='code' src="{{asset('/mb/shuzima.png')}}"></image>
                          <div class="num">123456789</div>
                        </div>
                      </div>
                      <div class="layui-col-md6">
                        <!-- 选择业务员 -->
                        <div class="layui-form">
                          <div class="layui-form-item" style="width:200px;display: inline-block;float:left;font-size:26px;">
                            <label class="layui-form-label"  style="width:200px;text-align: left;">会员卡功能</label>
                          </div>
                          <div class="layui-form-item" style="display: inline-block;float:right;">
                            <label class="layui-form-label">启用</label>
                            <div class="layui-input-block">
                              <input class="kaiguan" type="checkbox" checked lay-skin="switch" lay-filter="kaiguan" value='1'>
                            </div>
                          </div>
                          <div class="layui-form-item">
                            <div class="layui-input-block" style="margin-left:20px;">
                                <select name="agent" id="agent" lay-filter="agent" lay-search>

                                </select>
                            </div>
                          </div>

                        </div>
                        <div class="layui-form" lay-filter="component-form-group">

                          <div class="layui-form-item">
                            <label style="border-left:3px solid #ff5e5f;padding-left:10px;margin-left:20px;">卡片信息</label>
                          </div>
                          <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>会员卡名称:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item1">
                              </div>
                          </div>
                          <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>宣传语:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >
                                  <div class="layui-form-mid layui-word-aux">最多设置14个字</div>
                                  <button class="layui-btn up">使用默认</button>
                              </div>
                          </div>
                          <div class="layui-form-item">
                            <label class="layui-form-label"><span>*</span>会员卡模板:</label>
                            <div class="layui-input-block" id='temple' >
                              <div class="min_img">
                                <img src="{{asset('/mb/vip-bg-1.png')}}">
                                <input class="test1" type="radio" name="sex" value="1" title="a" checked lay-filter="test1">
                              </div>
                              <div class="min_img">
                                <img src="{{asset('/mb/vip-bg-2.png')}}">
                                <input class="test2" type="radio" name="sex" value="2" title="b" lay-filter="test2">
                              </div>
                              <div class="min_img">
                                <img src="{{asset('/mb/vip-bg-3.png')}}">
                                <input class="test3" type="radio" name="sex" value="3" title="c" lay-filter="test3">
                              </div>
                              <div class="min_img">
                                <img src="{{asset('/mb/vip-bg-4.png')}}">
                                <input class="test4" type="radio" name="sex" value="4" title="d" lay-filter="test4">
                              </div>
                            </div>
                          </div>

                          <div class="layui-form-item">
                            <label style="border-left:3px solid #ff5e5f;padding-left:10px;margin-left:20px;">会员卡详情</label>
                          </div>
                          <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>卡片库存:</label>
                              <div class="layui-input-block">
                                  <input type="number" name="select"  autocomplete="off" placeholder="" class="layui-input item3" id="checkbox2" style="width:64%;float:left;margin-right:10px;">张

                                  <!-- <input type="radio" name="numberselect" value="1" title="不限库存" lay-filter="test5" id="checkbox1"> -->
                              </div>
                          </div>

                          <div class="layui-form-item layui-form-text">
                            <label class="layui-form-label"><span>*</span>使用说明:</label>
                            <div class="layui-input-block">
                              <textarea name="desc" placeholder="" class="layui-textarea item4"></textarea>
                              <div class="layui-form-mid layui-word-aux">字数上限为1024个汉字</div>
                            </div>
                          </div>
                          <div class="layui-form-item">
                            <label class="layui-form-label"><span>*</span>客服电话:</label>
                            <div class="layui-input-block">
                                <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item5">
                            </div>
                          </div>
                          <div class="layui-form-item">
                            <div class="layui-input-block">
                              <button class="layui-btn" lay-submit="" lay-filter="formDemo" id='submit'>提交</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>



                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  <input type="hidden" class="store_id">
  <input type="hidden" class="js_tpl_sku">
  <input type="hidden" class="js_tpl_bck" value="1">

  <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
  <script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script>
    <script>
    var token = sessionStorage.getItem("Publictoken");
    var str=location.search;
    var store_id = sessionStorage.getItem("store_store_id");


    layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate'], function(){
      var $ = layui.$
        ,admin = layui.admin
        ,form = layui.form
        ,table = layui.table
        ,laydate = layui.laydate;
        // 未登录,跳转登录页面
        $(document).ready(function(){
          if(token==null){
              window.location.href="{{url('/mb/login')}}";
          }
        });

        $('#temple .min_img').click(function(){

        });
        // form.on('checkbox(switchTest)', function(data){
        //   console.log(data.elem.checked);
        //   console.log(data.value);
        // });
        form.on('switch(kaiguan)', function(data){
          // console.log(data.elem); //得到checkbox原始DOM对象
          // console.log(data.elem.checked); //是否被选中，true或者false
          // console.log(data.value); //复选框value值，也可以通过data.elem.value得到
          // console.log(data.othis); //得到美化后的DOM对象
          if(data.value==1){
            $('.kaiguan').val('2')
          }else{
            $('.kaiguan').val('1')
          }
        });

        form.on('radio(test1)', function(data){
          // console.log(data.elem); //得到radio原始DOM对象
          // console.log(data.value); //被点击的radio的value值
          $('.img').attr('src',"{{asset('/mb/vip-bg-1.png')}}");
          $('.js_tpl_bck').val('1')
        });

        form.on('radio(test2)', function(data){
          // console.log(data.elem); //得到radio原始DOM对象
          // console.log(data.value); //被点击的radio的value值
          $('.img').attr('src',"{{asset('/mb/vip-bg-2.png')}}");
          $('.js_tpl_bck').val('2')
        });
        form.on('radio(test3)', function(data){
          // console.log(data.elem); //得到radio原始DOM对象
          // console.log(data.value); //被点击的radio的value值
          $('.img').attr('src',"{{asset('/mb/vip-bg-3.png')}}");
          $('.js_tpl_bck').val('3')
        });

        form.on('radio(test4)', function(data){
          // console.log(data.elem); //得到radio原始DOM对象
          // console.log(data.value); //被点击的radio的value值
          $('.img').attr('src',"{{asset('/mb/vip-bg-4.png')}}");
          $('.js_tpl_bck').val('4')
        });

        form.on('radio(test5)', function(data){
          $("#checkbox2").val('');
          $('.js_tpl_sku').val('')
        });

        $('#checkbox2').focus(function(event){
          $('.js_tpl_sku').val($(this).val());
          $("#checkbox1").attr("checked",false);
          form.render('radio');
        });

        $('#checkbox2').bind("input propertychange",function(event){
          $('.js_tpl_sku').val($(this).val());
          $("#checkbox1").attr("checked",false);
          form.render('radio');
        });

        $('.item1').bind("input propertychange",function(event){
          $(".store_name").html($('.item1').val());
        });

        $('.item2').bind("input propertychange",function(event){
          $(".store_desc").html($('.item2').val());
        });

        $('.up').click(function(){
          $('.item2').val('欢迎使用本店会员卡');
          $(".store_desc").html('欢迎使用本店会员卡');
        });

        // 门店***********************
        $.ajax({
          url : "{{url('/api/merchant/store_lists')}}",
          data : {token:token,l:100},
          type : 'post',
          success : function(data) {
//              console.log(data);
              $('.store_id').val(data.data[0].store_id);

              // 查看
              $.post("{{url('/api/member/query_tpl')}}",
              {
                  token:token
                  ,store_id:data.data[0].store_id
              },function(res){
//                  console.log(res);
                  if(res.status==1){
                    $('.store_name').html(res.data.tpl_name);
                    $('.store_desc').html(res.data.tpl_desc);
                    $('.item1').val(res.data.tpl_name);
                    $('.item2').val(res.data.tpl_desc);
                    $('.js_tpl_bck').val(res.data.tpl_bck);
                    if(res.data.tpl_bck == 1){
                      $('.img').attr('src',"{{asset('/mb/vip-bg-1.png')}}");
                      $('.test1').attr('checked',true)
                    }else if(res.data.tpl_bck == 2){
                      $('.img').attr('src',"{{asset('/mb/vip-bg-2.png')}}");
                      $('.test2').attr('checked',true)
                    }else if(res.data.tpl_bck == 3){
                      $('.img').attr('src',"{{asset('/mb/vip-bg-3.png')}}");
                      $('.test3').attr('checked',true)
                    }else if(res.data.tpl_bck == 4){
                      $('.img').attr('src',"{{asset('/mb/vip-bg-4.png')}}");
                      $('.test4').attr('checked',true);
                    }
                    form.render('radio');

                    $('.js_tpl_sku').val(res.data.tpl_sku);
                    if(res.data.tpl_sku!=''){
                      $('.item3').val(res.data.tpl_sku);
                      $("#checkbox1").attr("checked",true);
                    }else{
                      $("#checkbox1").attr("checked",false);
                    }

                    $('.item4').val(res.data.tpl_content);
                    $('.kaiguan').val(res.data.tpl_status);
                    $('.item5').val(res.data.tpl_phone);

                    if(res.data.tpl_status==1){
                      $('.kaiguan').val('1');
                      $('.kaiguan').attr("checked",true);
                      form.render('checkbox');
                    }else{
                      $('.kaiguan').val('2');
                      $('.kaiguan').attr("checked",false);
                      form.render('checkbox');
                    }
                  }else{
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 3000
                    });
                  }
              },"json");

              var optionStr = "";
              for(var i=0;i<data.data.length;i++){
                  optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
              }
              $("#agent").append(optionStr);
              layui.form.render('select');
          },
          error : function(data) {
              alert('查找板块报错');
          }
        });

        // 提交
        $('#submit').click(function(){
          $.post("{{url('/api/member/set_tpl')}}",
          {
              token:token
              ,store_id:$('.store_id').val()
              ,tpl_name:$('.item1').val()
              ,tpl_desc:$('.item2').val()
              ,tpl_bck:$('.js_tpl_bck').val()
              ,tpl_sku:$('.js_tpl_sku').val()
              ,tpl_content:$('.item4').val(),
              tpl_status:$('.kaiguan').val()
              ,tpl_phone:$('.item5').val()
          },function(res){
//              console.log(res);
              if(res.status==1){
                  layer.msg(res.message, {
                      offset: '50px'
                      ,icon: 1
                      ,time: 3000
                  });
              }else{
                  layer.msg(res.message, {
                      offset: '50px'
                      ,icon: 2
                      ,time: 3000
                  });
              }
          },"json");
        })

    });

  </script>

</body>
</html>
