<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>营销活动列表</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
  <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
  <style>
    .edit{background-color: #ed9c3a;}
    .shenhe{background-color: #429488;}
    .see{background-color: #7cb717;}
    .tongbu{background-color: #4c9ef8;color:#fff;}
    .cur{color:#009688;}
    .yname{
      font-size: 13px;
      color: #444;
    }
    .xgrate{
        color: #fff;
        font-size: 15px;
        padding: 7px;
        height: 30px;
        line-height: 30px;
        /* border: 1px solid #666; */
        background-color: #3475c3;
    }
  </style>
</head>
<body>

  <div class="layui-fluid" style="margin-top:50px;">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">

        <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
              <div class="layui-card">
                <div class="layui-card-header">营销活动列表</div>

                <div class="layui-card-body">
                  <div class="layui-btn-container" style="font-size:14px;">
                    <!-- 选择业务员 -->
                    <div class="layui-form" lay-filter="component-form-group" style="width:300px;display: inline-block;">
                      <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:0">
                        <text class="yname">代理商名称</text>
                            <select name="agent" id="agent" lay-filter="agent" lay-search>

                            </select>
                        </div>
                      </div>
                    </div>



                  </div>

                  <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                  <!-- 判断状态 -->
                  <script type="text/html" id="statusTap">

                    消费@{{ d.pay_amount }}元
                  </script>

                  <script type="text/html" id="table-content-list" class="layui-btn-small">
                    <a class="layui-btn layui-btn-normal layui-btn-xs see" lay-event="see">查看</a>
                    <a class="layui-btn layui-btn-normal layui-btn-xs del" lay-event="del">删除</a>
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

<div id="open_js" class="hide" style="display: none;background-color: #fff;height: 400px;color:#111;">
  <div class="xgrate">查看详情</div>
  <div class="layui-card-body" style="padding: 15px;">
    <div class="layui-form">
      <label class="layui-form-label">充值详情:</label>

      <div class="layui-form-item xfs">


      </div>


      <div class="layui-form-item">
        <label class="layui-form-label">活动开始:</label>
        <div class="layui-input-block start" style='line-height: 36px;'>

        </div>
      </div>
      <div class="layui-form-item">
        <label class="layui-form-label">活动结束:</label>
        <div class="layui-input-block end" style='line-height: 36px;'>

        </div>
      </div>
      <div class="layui-form-item">
        <label class="layui-form-label">适用门店:</label>
        <div class="layui-input-block store" style='line-height: 36px;'>

        </div>
      </div>

      <!-- <div class="layui-form-item">
        <div class="layui-input-block">
            <div class="layui-footer" style="left: 0;">
                <button class="layui-btn open_jiesuan">确定</button>
            </div>
        </div>
      </div> -->
    </div>
  </div>
</div>

  <input type="hidden" class="store_id">
  <input type="hidden" class="js_type">
  <input type="hidden" class="js_type_desc">


  <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
  <script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script>
    <script>
    var token = sessionStorage.getItem("Publictoken");
    var str=location.search;
    var store_id = sessionStorage.getItem("store_store_id");


    layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate;
        // 未登录,跳转登录页面
        $(document).ready(function(){
          if(token==null){
              window.location.href="{{url('/mb/login')}}";
          }
        })
        // 选择门店
        $.ajax({
            url : "{{url('/api/merchant/store_lists')}}",
            data : {token:token,l:100},
            type : 'post',
            success : function(data) {
                console.log(data);
                $('.store_id').val(data.data[0].store_id)

                // 渲染表格
                table.render({
                  elem: '#test-table-page'
                  ,url: "{{url('/api/member/yx_lists')}}"
                  ,method: 'post'
                  ,where:{
                    token:token,
                    store_id:data.data[0].store_id
                  }
                  ,request:{
                    pageName: 'p',
                    limitName: 'l'
                  }
                  ,page: true
                  ,cellMinWidth: 150
                  ,cols: [[
                    {field:'yx_name', title: '活动名称'}
                    ,{field:'yx_desc', title: '活动说明'}
                    ,{width:140,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
                  ]]
                  ,response: {
                    statusName: 'status' //数据状态的字段名称，默认：code
                    ,statusCode: 1 //成功的状态码，默认：0
                    ,msgName: 'message' //状态信息的字段名称，默认：msg
                    ,countName: 't' //数据总数的字段名称，默认：count
                    ,dataName: 'data' //数据列表的字段名称，默认：data
                  }
                  ,done: function(res, curr, count){
                    console.log(res);
                    $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});	//进行表头样式设置
                  }

                });


                var optionStr = "";
                for(var i=0;i<data.data.length;i++){
                    optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";


                }
                $("#agent").append(optionStr);
                layui.form.render('select');
            },
            error : function(data) {
                alert('查找板块报错');
            }
        });





        table.on('tool(test-table-page)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
          var e = obj.data; //获得当前行数据
          var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
          var tr = obj.tr; //获得当前行 tr 的DOM对象
          console.log(e);
          // sessionStorage.setItem('s_store_id', e.store_id);

          if(layEvent === 'see'){ //修改
            layer.open({
              type: 1,
              title: false,
              closeBtn: 0,
              area: '516px',
              skin: 'layui-layer-nobg', //没有背景色
              shadeClose: true,
              content: $('#open_js')
            });

            // 结算类型查询
            $.post("{{url('/api/member/cz_info')}}",
            {
              token:token,
              yx_type: e.yx_type,
              yx_id: e.yx_id
            },function(data){
              console.log(data);
              var xf=JSON.parse(data.MemberCzSet.cz_list)
              console.log(xf)
              var str1=''
              for(var i=0;i<xf.length;i++){
                str1+='<div class="layui-input-block" style="line-height: 36px;margin-left:100px;">充值'+xf[i].cz+'元送'+xf[i].cz_s+'元</div>';

              }
              $('.xfs').html('')
              $('.xfs').append(str1)

              // *************
              $('.start').html(data.MemberCzSet.b_time)
              $('.end').html(data.MemberCzSet.e_time)

              var str2=''
              var nameA=data.MemberCzSet.store_info

              for(var j=0;j<nameA.length;j++){

                str2 += nameA[j].store_name+ ",";
              }
              if (str2.length > 0) {
                str2 = str2.substr(0, str2.length - 1);
              }

              $('.store').html(str2)


            },"json");
          }else if(layEvent === 'del'){
            layer.confirm('确认删除此消息?',{icon: 2}, function(index){
              $.post("{{url('/api/member/yx_del')}}",
              {
                token:token,
                yx_type: e.yx_type,
                yx_id: e.yx_id,

              },function(res){
                  console.log(res);
                  if(res.status==1){
                    obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                    layer.close(index);
                    layer.msg(res.message, {
                      offset: '50px'
                      ,icon: 1
                      ,time: 2000
                    });
                  }else{
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 3000
                    });
                  }
              },"json");

            });
          }

          var data = obj.data;

        });

        // 选择门店
        form.on('select(agent)', function(data){
          var store_id = data.value;
          $('.store_id').val(store_id);
          //执行重载
          table.reload('test-table-page', {
            where: {
              store_id: $(".store_id").val(),

            }
          });
        });

        form.on('select(open)', function(data){
        console.log(data.value)
          category = data.value;
          categoryName = data.elem[data.elem.selectedIndex].text;

          $('.js_type').val(category);
          $('.js_type_desc').val(categoryName);
        });

    });

  </script>

</body>
</html>





