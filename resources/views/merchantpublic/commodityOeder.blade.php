<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>商品订单</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
  <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">

</head>
<body>
  <div class="layui-fluid" style="margin-top:50px;">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">

        <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
              <div class="layui-card">
                <div class="layui-card-header">商品订单</div>

                <div class="layui-card-body">
                  <div class="layui-btn-container" style="font-size:14px;">
                    <!-- 缴费时间 -->
                    <div class="layui-form" style="width:100%;display:flex;">
                        
                    <form class="layui-form" action="">
                        <div class="layui-form-item">

                            <div class="layui-inline" style="margin-bottom: 20px;">
                                <label class="layui-form-label">选择门店</label>
                                <div class="layui-input-inline">
                                    <select name="row_one">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>

                            <div class="layui-inline" style="margin-bottom: 20px;">
                                <label class="layui-form-label">订单状态</label>
                                <div class="layui-input-inline">
                                    <select name="row_two">
                                        <option value=""></option>
                                        <option value="1">已支付</option>
                                        <option value="2">待支付</option>
                                        <option value="3">已取消</option>
                                        <option value="4">订单开始配送</option>
                                        <option value="5">订单配送结束</option>
                                        <option value="6">退款订单</option>
                                        <option value="7">骑手正在取货</option>
                                        <option value="8">订单已指定配送员</option>
                                        <option value="9">妥投异常之物品返回中</option>
                                        <option value="10">妥投异常之物品返回完成</option>
                                        <option value="11">订单已完成</option>
                                        <option value="100">骑士到店</option>
                                        <option value="1000">创建达达运单失败</option>
                                    </select>
                                </div>
                            </div>

                            <div class="layui-inline" style="margin-bottom: 20px;">
                                <label class="layui-form-label">订单类型</label>
                                <div class="layui-input-inline">
                                    <select name="row_three">
                                        <option value=""></option>
                                        <option value="1">外卖订单</option>
                                        <option value="2">自提订单</option>
                                        <option value="3">扫码点餐</option>
                                    </select>
                                </div>
                            </div>

                            <!-- <div class="layui-inline" style="margin-bottom: 20px;">
                                <label class="layui-form-label">配送情况</label>
                                <div class="layui-input-inline">
                                    <select name="row_four">
                                        <option value=""></option>
                                        <option value="1">自提</option>
                                        <option value="2">外卖</option>
                                    </select>
                                </div>
                            </div> -->

                            <!-- <div class="layui-inline" style="margin-bottom: 20px;">
                                <label class="layui-form-label">开始时间</label>
                                <div class="layui-input-inline">
                                    <input type="text" id="test1" name="row_five" class="layui-input" placeholder="请输入">
                                </div>
                            </div>

                            <div class="layui-inline" style="margin-bottom: 20px;">
                                <label class="layui-form-label">结束时间</label>
                                <div class="layui-input-inline">
                                    <input type="text" id="test2" name="row_sex" class="layui-input" placeholder="请输入">
                                </div>
                            </div> -->

                            <!-- <div class="layui-inline" style="margin-bottom: 20px;">
                                <label class="layui-form-label">商品名称</label>
                                <div class="layui-input-inline">
                                    <input type="text" name="row_seven" class="layui-input" placeholder="请输入">
                                </div>
                            </div> -->

                            <div class="layui-inline" style="margin-bottom: 20px;">
                                <label class="layui-form-label">订单号</label>
                                <div class="layui-input-inline">
                                    <input type="text" name="row_eight" class="layui-input" placeholder="请输入">
                                </div>
                            </div>

                            <div class="layui-inline" style="margin-bottom: 20px;">
                                <label class="layui-form-label"></label>
                                <div class="layui-input-inline">
                                    <button type="submit" class="layui-btn" lay-submit="" lay-filter="demo1">查询</button>
                                    <!-- <button type="reset" class="layui-btn layui-btn-primary">重置</button> -->
                                </div>
                            </div>

                        </div>
                    </form>

                    </div>
                  </div>

                    <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>

                    <!--  商品名称 -->
                    <script type="text/html" id="shopnameandimg">
                        @{{# if(d.main_image){ }}
                            <img style="width: 25px;margin-right: 6px;border: 1px solid #eee;padding: 1px;" src="@{{ d.main_image }}" alt="">
                        @{{# } }}
                        @{{ d.name }}
                    </script>

                    <!-- 订单状态 -->
                    <script type="text/html" id="orderpaystatus">
                        @{{# if(d.order_pay_status == "1"){ }}
                        <span style="font-size: 12px;color: #2b4490;">已支付</span>
                        @{{# }else if(d.order_pay_status == "2"){ }}
                        <span style="font-size: 12px;color: #f47920;">待支付</span>
                        @{{# }else if(d.order_pay_status == "3"){ }}
                        <span style="font-size: 12px;color: #8a8c8e;">已取消</span>
                        @{{# }else if(d.order_pay_status == "4"){ }}
                        <span style="font-size: 12px;color: #f15a22;">订单开始配送</span>
                        @{{# }else if(d.order_pay_status == "5"){ }}
                        <span style="font-size: 12px;color: #de773f;">订单配送结束</span>
                        @{{# }else if(d.order_pay_status == "6"){ }}
                        <span style="font-size: 12px;color: #b22c46;">退款订单</span>
                        @{{# }else if(d.order_pay_status == "7"){ }}
                        <span style="font-size: 12px;color: #f58220;">骑手正在取货</span>
                        @{{# }else if(d.order_pay_status == "8"){ }}
                        <span style="font-size: 12px;color: #5c7a29;">订单已指定配送员</span>
                        @{{# }else if(d.order_pay_status == "9"){ }}
                        <span style="font-size: 12px;color: #7f7522;">妥投异常之物品返回中</span>
                        @{{# }else if(d.order_pay_status == "10"){ }}
                        <span style="font-size: 12px;color: #80752c;">妥投异常之物品返回完成</span>
                        @{{# }else if(d.order_pay_status == "11"){ }}
                        <span style="font-size: 12px;color: #121a2a;">订单已完成</span>
                        @{{# }else if(d.order_pay_status == "100"){ }}
                        <span style="font-size: 12px;color: #ffe600;">骑士到店</span>
                        @{{# }else if(d.order_pay_status == "1000"){ }}
                        <span style="font-size: 12px;color: #840228;">创建达达运单失败</span>
                        @{{# } }}
                    </script>

                    <!-- 订单类型 -->
                    <script type="text/html" id="orderTypeId">
                        @{{# if(d.order_type == "3"){ }}
                            <span style="font-size: 12px;background-color: #598CFF;color: #fff;border-radius: 6px;padding: 6px 12px;">扫码点餐</span>
                        @{{# }else if(d.order_type == "1"){ }}
                            <span style="font-size: 12px;background-color: #5FB878;color: #fff;border-radius: 6px;padding: 6px 12px;">外卖订单</span>
                        @{{# }else if(d.order_type == "2"){ }}
                            <span style="font-size: 12px;background-color: #009688;color: #fff;border-radius: 6px;padding: 6px 12px;">自提订单</span>
                        @{{# } }}
                    </script>

                    <!-- 配送详情 orderdetails -->
                    <script type="text/html" id="orderdetails">
                        @{{# if(d.order_type == "3"){ }}
                            <span style="font-size: 12px;color: #393D49;">@{{ d.table_id }}号桌</span>
                        @{{# } }}
                    </script>

                    <!-- 操作 -->
                    <script type="text/html" id="table-content-list">
                        <a class="layui-btn layui-btn-normal layui-btn-xs " lay-event="lookdata">查看</a>
                        <a class="layui-btn layui-btn-danger layui-btn-xs " lay-event="deledata">删除</a>
                    </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      
  </div>



  <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>


  <script>
    var token = sessionStorage.getItem("Publictoken");
    var str=location.search;
    var store_id=sessionStorage.getItem("store_id");
    console.log(store_id)

    layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate',"upload"], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate
            ,upload = layui.upload;

        // 时间范围选择
		laydate.render({
			elem: '#test1',
			format: 'yyyy-MM-dd'
		});
		laydate.render({
			elem: '#test2',
			format: 'yyyy-MM-dd'
		});

        // post请求
        let getData = (url,data) => {
            return new Promise(resolve=>{
                $.post(url,data,res=>{
                    resolve(res);
                })
            })
        }

        // 数据初始化
        (async ()=>{
            var alertObj = layer.msg('数据初始化,请稍后......', {icon:16, shade:0.5, time:0});
            let get_row_one = await getData("/api/merchant/store_lists",{
                token:token,
                l:100
            })
            Promise.all([get_row_one]).then(res=>{
                let get_row_one_data = res[0];
                if(get_row_one_data.data.length > 0){
                    for(let i=0 ; i < get_row_one_data.data.length ; i ++ ){
                        $('select[name="row_one"]').append(`<option value="`+get_row_one_data.data[i].store_id+`">`+get_row_one_data.data[i].store_short_name+`</option>`)
                    }
                }
                layui.form.render('select');
                layer.close(alertObj);
            });
        })();

        // 渲染表格
        table.render({
			elem: '#test-table-page',
			url: '/api/merchant/merchandiseOrder',
            method: 'post',
			page: true,
            request:{
                pageName: 'p',
                limitName: 'l'
            },
            where:{
                token:token,
                storeId:store_id
            },
			cols: [
				[ 
                    {
                        align: 'center',
						field: 'out_trade_no',
						title: '订单号',
					},{
						title: '商品名称',
                        templet: '#shopnameandimg'
					},{
                        align: 'center',
						field: 'good_money',
						title: '交易金额',
					},{
                        align: 'center',
						field: 'good_price',
						title: '实收金额',
					},{
                        align: 'center',
						title: '顾客备注',
					},{
                        align: 'center',
						title: '订单状态',
                        templet: '#orderpaystatus'
					},{
                        align: 'center',
						title: '订单类型',
                        templet: '#orderTypeId'
					},{
                        align: 'center',
						title: '配送详情',
                        templet: '#orderdetails'
					}
				]
			],
			response: {
                statusName: 'status', //数据状态的字段名称，默认：code
                statusCode: 1 , //成功的状态码，默认：0
                msgName: 'message', //状态信息的字段名称，默认：msg
                countName: 't', //数据总数的字段名称，默认：count
                dataName: 'data' //数据列表的字段名称，默认：data
			},
		});

        // 操作
        table.on('tool(test-table-page)', function(e) {

            if(e.event == "lookdata"){

                console.log("查看");

            }else if(e.event == "deledata"){

                console.log("删除");
                
            }

        })

        // 数据查询
        form.on('submit(demo1)', function(data) {

            let datalist = data.field;

            table.reload('test-table-page',{
				where:{
					storeId: datalist.row_one ? datalist.row_one : store_id,
                    outTradeNo: datalist.row_eight ,
                    orderPayStatus: datalist.row_two ,
                    orderType: datalist.row_three ,
				}
			});
            return false;
        })
    });

  </script>
</body>
</html>







