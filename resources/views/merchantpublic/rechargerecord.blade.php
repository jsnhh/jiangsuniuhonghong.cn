<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>充值记录</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
  <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
  <style>
    .edit{background-color: #ed9c3a;}
    .shenhe{background-color: #429488;}
    .see{background-color: #7cb717;}
    .tongbu{background-color: #3475c3;color:#fff;}
    .cur{color:#3475c3;}

    .box{
      border-collapse:collapse;
    }
    .con{
      height:120px;
      text-align: center;
      margin:0px -1px 0px 0px ;
      padding-top:20px;
      margin-right: 5%;
    }
    .con span{
      display: inline-block;
      padding-top:10px;
      font-size:20px;
      font-weight: 400;
    }
    .conx span{
      padding-top:0px !important;
      font-size:16px;
    }
    .way {
      height: 38px;
      line-height: 38px;
    }
    .yname{
      font-size: 13px;
      color: #444;
    }
    .xgrate{
        color: #fff;
        font-size: 15px;
        padding: 7px;
        height: 30px;
        line-height: 30px;
        /* border: 1px solid #666; */
        background-color: #3475c3;
    }
  </style>
</head>
<body>

  <div class="layui-fluid" style="margin-top:50px;">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">

        <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
              <div class="layui-card">
                <div class="layui-card-header">充值记录</div>

                <div class="layui-card-body">
                  <div class="layui-btn-container" style="font-size:14px;margin-bottom:20px;">

                    <div class="layui-row box">
                      <div class="layui-col-md4 con" style="background: url({{asset('/school/images/yesterday_back.png')}}) center center / cover no-repeat;border-radius: 5px;">
                        <div style='padding-bottom:10px; margin-top: -10px;color:#fff'>充值总额：<span class="item1" style='font-size:20px;font-weight: 400;color:#fff'></span></div>
                        <div class="layui-col-xs6 conx"style="color:#fff">
                          线上充值<br>
                          <span class="item2"style="color:#fff"></span>
                        </div>
                        <div class="layui-col-xs6 conx"style="color:#fff">
                          赠送/返现<br>
                          <span class="item3"style="color:#fff"></span>
                        </div>
                      </div>
                      <div class="layui-col-md4 con"style="background: url({{asset('/school/images/today_back.png')}}) center center / cover no-repeat;border-radius: 5px;color:#fff">
                        手续费<br><span class="item4"style="color:#fff"></span>
                      </div>
                      <div class="layui-col-md4 con"style="background: url({{asset('/school/images/before_week_back.png')}}) center center / cover no-repeat;border-radius: 5px;color:#fff">
                        商家净收入<br><span class="item5"style="color:#fff"></span>
                      </div>
                    </div>
                  </div>
                  <div class="layui-btn-container" style="font-size:14px;">
                    <!-- 选择业务员 -->
                    <div class="layui-form" lay-filter="component-form-group" style="width:300px;display: inline-block;">
                      <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:0">
                        <text class="yname">代理商名称</text>
                            <select name="agent" id="agent" lay-filter="agent" lay-search>

                            </select>
                        </div>
                      </div>
                    </div>

                    <!-- 状态 -->
                    <div class="layui-form" lay-filter="component-form-group" style="width:240px;display: inline-block;">
                      <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:10px">
                            <text class="yname">支付状态</text>
                            <select name="status" id="status" lay-filter="status">
                              <option value="">请选择</option>
                              <option value="1">成功</option>
                              <option value="2">失败</option>
                            </select>
                        </div>
                      </div>
                    </div>
                    <!-- 充值满送 -->
                    <div class="layui-form" lay-filter="component-form-group" style="width:240px;display: inline-block;">
                      <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:10px">
                            <text class="yname">充值类型</text>
                            <select name="type" id="type" lay-filter="type">

                            </select>
                        </div>
                      </div>
                    </div>



                    <!-- 缴费时间 -->
                    <div class="layui-form" style="display: inline-block;">
                      <div class="layui-form-item">
                        <div class="layui-inline"style="margin-top:-24px;margin-left: 10px;">

                          <div class="layui-input-inline">
                          <text class="yname">订单开始时间</text>
                            <input type="text" class="layui-input start-item test-item" placeholder="开始时间" lay-key="23">
                          </div>
                        </div>
                        <div class="layui-inline"style="margin-top:-24px">
                          <div class="layui-input-inline">
                          <text class="yname">订单结束时间</text>
                            <input type="text" class="layui-input end-item test-item" placeholder="结束时间" lay-key="24">
                          </div>
                        </div>

                      </div>
                    </div>
                    <!-- 搜索 -->
                    <div class="layui-form" lay-filter="component-form-group" style="width:600px;display: inline-block;">
                      <div class="layui-form-item">

                          <div class="layui-inline">
                            <div class="layui-input-inline">
                            <text class="yname">会员卡号/订单号</text>
                              <input type="text" name="tradeno" placeholder="请输入会员卡号/订单号" autocomplete="off" class="layui-input dingdan">
                            </div>
                          </div>

                          <div class="layui-inline">
                            <button class="layui-btn layuiadmin-btn-list" lay-submit="" lay-filter="LAY-app-contlist-search" style="margin-bottom: 0;height:36px;line-height: 36px;margin-top: 33%;border-radius:5px">
                              <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                            </button>
                          </div>
                          <button class="layui-btn export"  style="margin-bottom: 4px;height:36px;line-height: 36px;margin-top: 3%;border-radius:5px">导出</button>   <!-- onclick="exportdata()" -->
                        </div>
                    </div>

                  </div>

                  <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                  <!-- 判断状态 -->
                  <script type="text/html" id="statusTap">
                    @{{#  if(d.pay_status == 1){ }}
                      <span class="cur">支付成功</span>
                    @{{#  } else { }}
                      支付失败
                    @{{#  } }}
                  </script>
                  <!-- 判断状态 -->
                  <script type="text/html" id="paymoney">

                    @{{ d.total_amount }}<br>


                    @{{ d.total_amount }}<br>
                    @{{ d.total_amount }}<br>


                    @{{ d.total_amount }}



                  </script>
                  <script type="text/html" id="table-content-list" class="layui-btn-small">
                    <a class="layui-btn layui-btn-normal layui-btn-xs tongbu" lay-event="tongbu">详情</a>
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>



<div id="open_js" class="hide" style="display: none;background-color: #fff;color: #111;">
  <div class="xgrate">详情</div>
  <div class="layui-card-body" style="padding: 15px;">
    <div class="layui-form">

      <div class="layui-form-item xingzhi" style="margin:20px 0 10px 13px">
        <label class="layui-form-label">会员卡号：</label>
        <div class="layui-input-block">
            <div class="way no1"></div>
        </div>
      </div>
      <div class="layui-form-item">
        <label class="layui-form-label">订单号：</label>
        <div class="layui-input-block">
            <div class="way no2"></div>
        </div>
        </div>
      </div>
      <!-- <div class="layui-form-item">
        <div class="layui-input-block">
            <div class="layui-footer" style="left: 0;">
                <button class="layui-btn open_jiesuan">确定</button>
            </div>
        </div>
      </div> -->
    </div>
  </div>
</div>



  <input type="hidden" class="store_id">


  <input type="hidden" class="pay_status">
  <input type="hidden" class="cz_type">

  <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
  <script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script>
    <script>
    var token = sessionStorage.getItem("Publictoken");
    var str=location.search;
    var store_id = sessionStorage.getItem("store_store_id");


    layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate;
      // 未登录,跳转登录页面
      $(document).ready(function(){
          if(token==null){
              window.location.href="{{url('/mb/login')}}";
          }
      })
      // 选择门店
      $.ajax({
        url : "{{url('/api/merchant/store_lists')}}",
        data : {token:token,l:100},
        type : 'post',
        success : function(data) {
          console.log(data);
          var optionStr = "";
            for(var i=0;i<data.data.length;i++){
              optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
            }
            $("#agent").append(optionStr);
            layui.form.render('select');


          // 充值类型
          $.post("{{url('/api/member/cz_type')}}",
          {
              token:token,
              store_id:data.data[0].store_id,
          },function(res2){
              console.log(res2);
              var str1 = "";
              for(var i=0;i<res2.data.length;i++){
                  str1 += "<option value='" + res2.data[i].cz_type + "'>" + res2.data[i].cz_type_desc + "</option>";
              }
              $("#type").append('<option value="">请选择充值类型</option>'+str1);
              layui.form.render('select');
          },"json");

          // 渲染表格
          table.render({
            elem: '#test-table-page'
            ,url: "{{url('/api/member/cz_lists')}}"
            ,method: 'post'
            ,where:{
              token:token,
              store_id:data.data[0].store_id,
              return_type:'2'
            }
            ,request:{
              pageName: 'p',
              limitName: 'l'
            }
            ,page: true
            ,cellMinWidth: 150
            ,cols: [[
              {align:'center',field:'store_name', title: '所属门店'}
              ,{align:'center',field:'cz_money', title: '订单金额'}
              ,{align:'center',field:'cz_s_money', title: '商家满送'}
              ,{align:'center',field:'cz_money',  title: '商家实收'}
              ,{align:'center',field:'total_amount',  title: '实际充值'}
              ,{align:'center',field:'cz_type_desc', title: '充值类型'}
              ,{align:'center',field:'fee_amount',  title: '手续费(元)'}
              ,{align:'center',field:'receipt_amount',  title: '净收入(元)'}
              ,{align:'center',field:'pay_status',  title: '支付状态',templet:'#statusTap'}
              ,{align:'center',field:'created_at',  title: '下单时间'}
              ,{width:100,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
            ]]
            ,response: {
              statusName: 'status' //数据状态的字段名称，默认：code
              ,statusCode: 1 //成功的状态码，默认：0
              ,msgName: 'message' //状态信息的字段名称，默认：msg
              ,countName: 't' //数据总数的字段名称，默认：count
              ,dataName: 'data' //数据列表的字段名称，默认：data
            }
            ,done: function(res, curr, count){
              console.log(res);
              $.post("{{url('/api/member/cz_lists')}}",
              {
                  token:token,
                  store_id:data.data[0].store_id,
                  return_type:'1'
              },function(ress){
                console.log(ress);

                $('.item1').html(ress.data.all_total_amount)
                $('.item2').html(ress.data.all_cz_money)
                $('.item3').html(ress.data.all_cz_s_money)
                $('.item4').html(ress.data.all_fee_amount)
                $('.item5').html(ress.data.all_receipt_amount)

              },"json");
              $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});	//进行表头样式设置
            }

          });

        },
        error : function(data) {
            alert('查找板块报错');
        }
      });




      table.on('tool(test-table-page)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
        var e = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        var tr = obj.tr; //获得当前行 tr 的DOM对象
        console.log(e);
        // sessionStorage.setItem('s_store_id', e.store_id);

        if(layEvent === 'tongbu'){ //审核
          $('.no1').html(e.mb_id)
          $('.no2').html(e.out_trade_no)
          layer.open({
            type: 1,
            title: false,
            closeBtn: 0,
            area: '516px',
            skin: 'layui-layer-nobg', //没有背景色
            shadeClose: true,
            content: $('#open_js')
          });
        }
      });

      // 选择门店
      form.on('select(agent)', function(data){
        var store_id = data.value;
        $('.store_id').val(store_id);
        //执行重载
        table.reload('test-table-page', {
          where: {
            store_id: $(".store_id").val(),
          }
        });

      });


      // 选择状态
      form.on('select(status)', function(data){
        var pay_status = data.value;
        $('.pay_status').val(pay_status);
        //执行重载
        table.reload('test-table-page', {
          where: {
            pay_status:pay_status,
          }
        });
      });
      // 选择充值类型
      form.on('select(type)', function(data){
        var cz_type = data.value;
        $('.cz_type').val(cz_type);
        //执行重载
        table.reload('test-table-page', {
          where: {
            cz_type:cz_type
          }
        });
      });



      laydate.render({
        elem: '.start-item'
        ,type: 'datetime'
        ,done: function(value){
          //执行重载
          table.reload('test-table-page', {
            where: {
              time_start:value,
              time_end:$('.end-item').val()
            }
          });
        }
      });

      laydate.render({
        elem: '.end-item'
        ,type: 'datetime'
        ,done: function(value){
          //执行重载
          table.reload('test-table-page', {
            where: {
              time_start:$('.start-item').val(),
              time_end:value
            }
          });
        }
      });

      form.on('submit(LAY-app-contlist-search)', function(data){
        var out_trade_no = data.field.tradeno;
        console.log(data);
        //执行重载
        table.reload('test-table-page', {
          where: {
            out_trade_no:out_trade_no,
          }
        });
      });

      // 导出
      function exportdata(){
        var store_id=$('.store_id').val();
        var merchant_id=$('.user_id').val();
        var sort=$('.sort').val();
        var pay_status=$('.pay_status').val();
        var ways_source=$('.pay_type').val();

        var time_start=$('.start-item').val();
        var time_end=$('.end-item').val();

        var out_trade_no=$('.danhao').val();
        var trade_no=$('.tiaoma').val();

        window.location.href="{{url('/api/export/MerchantOrderExcelDown')}}"+"?token="+token+"&store_id="+store_id+"&merchant_id="+merchant_id+"&sort="+sort+"&pay_status="+pay_status+"&ways_source="+ways_source+"&time_start="+time_start+"&time_end="+time_end+"&out_trade_no="+out_trade_no+"&trade_no="+trade_no;

      }

      $('.export').click(function(){
        var store_id=$('.store_id').val();
        var merchant_id=$('.user_id').val();
        var sort=$('.sort').val();
        var pay_status=$('.pay_status').val();
        var ways_source=$('.pay_type').val();

        var time_start=$('.start-item').val();
        var time_end=$('.end-item').val();

        var out_trade_no=$('.danhao').val();
        var trade_no=$('.tiaoma').val();

        window.location.href="{{url('/api/export/MerchantOrderExcelDown')}}"+"?token="+token+"&store_id="+store_id+"&merchant_id="+merchant_id+"&sort="+sort+"&pay_status="+pay_status+"&ways_source="+ways_source+"&time_start="+time_start+"&time_end="+time_end+"&out_trade_no="+out_trade_no+"&trade_no="+trade_no;
      })

    });

  </script>

</body>
</html>





