<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>添加商品</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/formSelects-v4.css')}}" media="all">
    <style>
        #demo1 img{width: 100%;height: 100%;}
        .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
        .img_box{position: relative;width:13%;height:10%;display: inline-block; margin-right: 10px;}
        .img_box span{position: absolute;right:0;top:0;font-size: 30px;background: #fff;cursor: pointer;}
        video{
            width:200px;
        }
        .img_box2 span{position: absolute;right:0;top:0;font-size: 30px;background: #fff;cursor: pointer;}
        .lianjiecon div{
            width:100%;
            overflow: hidden;
            margin-bottom:20px;
        }
        .lianjiecon label{
            display: inline-block;
            float: left;
            width:10%;
            line-height: 36px;
        }
        .lianjiecon input{
            display: inline-block;
            float: left;
            width:90%;
        }
    </style>
</head>
<body>

<div class="layui-fluid" style="margin-top:50px;">
    <div class="layui-card">
        <div class="layui-card-header">添加商品</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item">
                    <label class="layui-form-label">商品名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="title" placeholder="请输入商品名称:" class="layui-input title">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">副标题</label>
                    <div class="layui-input-block">
                        <input type="text" name="sub_title" placeholder="请输入商品副标题:" class="layui-input sub_title">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">所属门店</label>
                    <div class="layui-input-block">
                        <select name="store" id="store" lay-filter="store">
                        </select>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">商品价格</label>
                    <div class="layui-input-block">
                        <input type="text" name="goods_price" placeholder="请输入商品价格:" class="layui-input goods_price">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">图片</label>
                    <div class="layui-input-block">
                        <div class="layui-upload">
                          <button class="layui-btn up"><input type="file" name="img_upload" class="test1">上传图片</button>
                          <blockquote class="layui-elem-quote layui-quote-nm" style="margin-top: 10px;">
                            预览图：
                            <div class="layui-upload-list" id="demo1"></div>
                         </blockquote>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">状态</label>
                    <div class="layui-input-block" id="goods_status">
                        <input type="radio" name="status" value="0" title="下架">
                        <input type="radio" name="status" value="1" title="上架" checked>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">排序</label>
                    <div class="layui-input-block">
                        <input type="text" title="sort" name="sort" placeholder="排序（1--255，数字越小越靠前）" class="layui-input sort">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">商品描述</label>
                    <div class="layui-input-block">
                        <textarea name="desc" placeholder="请输入内容商品描述" class="layui-textarea desc"></textarea>
                    </div>
                </div>

                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit">确定提交</button>
                            <!--<button type="reset" class="layui-btn layui-btn-primary">重置</button>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{--<input type="hidden" class="store_id" value="">--}}
{{--<input type="hidden" class="store-id" value="">--}}

{{--<input type="hidden" class="js_store" value="">--}}
{{--<input type="hidden" class="type" value="1">--}}
{{--<input type="hidden" class="user_id" value="">--}}
{{--<input type="hidden" class="classname" value="">--}}


<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<!-- <script src="{{asset('/layuiadmin/modules/formSelects.js')}}"></script> -->
<script>
    var token = sessionStorage.getItem("Publictoken");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','laydate'], function(){
        var $ = layui.$
            // ,admin = layui.admin
            // ,table = layui.table
            // ,element = layui.element
            ,upload = layui.upload
            // ,form = layui.form;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token == null){
                window.location.href="{{url('/mb/login')}}";
            }
        });

        //多图片上传
        upload.render({
            url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token
            ,elem: '.test1'
            ,method : 'POST'
            ,type : 'images'
            ,ext : 'jpg|png|gif'
            ,multiple: false
            ,before: function(obj){
              //预读本地文件示例，不支持ie8
              obj.preview(function(index, file, result){
                $('#demo1').append('<div class="img_box" data=""><img src="'+ result +'" alt="'+ file.name +'" class="layui-upload-img"><span>×</span></div>');

                $("#demo1 .img_box").each(function(){
                    var index = $(this).index()+1;
                    $(this).attr('data',index);
                });

              });
            }
            ,done: function(res){
                // console.log(res);
                $("#demo1 .img_box:last-child").find('img').attr('src',res.data.img_url);
            }
        });

        // 选择门店
        $.ajax({
            url : "{{url('/api/merchant/store_lists')}}",
            data : {
                token:token
                ,l:100
            },
            type : 'post',
            datatype:'json',
            success : function(data) {
                // console.log(data);
                // $('.store_id').val(data.data[0].store_id);

                var optionStr = "";
                for(var i=0; i<data.data.length; i++){
                    optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
                }
                $("#store").append('<option value="">请选择门店</option>'+optionStr);
                layui.form.render('select');
            },
            error : function(data) {
                alert('选择门店报错');
            }
        });

        // form.on('select(store)', function(data){
        //     category = data.value;
        //     categoryName = data.elem[data.elem.selectedIndex].text;
        //     $('.js_store').val(category);
        // });

        $('.submit').on('click', function(){
            var adarr=[];

            $('#demo1 .img_box').each(function(index,item){
                var img_url = $(item).find('img').attr('src');
                var click_url = $(item).find('input').val();

                var data = {
                    "img_url":img_url
                    // ,"click_url":click_url
                }; //构造数组
                adarr.push(data);
            });
            var adarrJson = JSON.stringify(adarr); //转化成json格式

            $.post("{{url('/api/merchant/add_goods')}}",
            {
                token:token,
                title:$('.title').val(),
                sub_title:$('.sub_title').val(),
                store_id:$('#store').val(),
                goods_price:$('.goods_price').val(),
                imgs:adarrJson,
                goods_status:$('#goods_status input[name="status"]:checked ').val(), //获取商品状态
                goods_sort:$('.sort').val(),
                goods_desc:$('.desc').val()
            },function(res){
                // console.log(res);
                if(res.status == 1){
                    window.location.href="{{url('/mb/goodslist')}}";
                    // layer.msg(res.message, {
                    //     offset: '15px'
                    //     ,icon: 1
                    //     ,time: 3000
                    // });
                }
                // else{
                //     layer.msg(res.message, {
                //         offset: '15px'
                //         ,icon: 2
                //         ,time: 3000
                //     });
                // }
            },"json");
        });

        $('#demo1').on('click', '.img_box span', function(){
            $(this).parent().remove();
        });

    });
</script>
</body>
</html>
