<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>商品列表</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
  <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
  <link rel="stylesheet" href="{{asset('/layuiadmin/layui/inputTags/inputTags.css')}}">
  <link rel="stylesheet" href="{{asset('/layuiadmin/modules/layuiicon/iconfont.css')}}" media="all">
  <style>
    .layui-table-cell{
      height: 85px;
      line-height: 85px;
    }
    .layui-table-header .layui-table-cell {
            height: 30px;
            line-height: 30px;
        }
        layui-layer
    .edit{background-color: #ed9c3a;}
    .shenhe{background-color: #429488;}
    .see{background-color: #7cb717;}
    .tongbu{background-color: #4c9ef8;color:#fff;}
    .cur{color:#009688;}
    .yname{font-size: 13px;color: #444;}
    .xgrate{color: #fff;font-size: 15px;padding: 7px;height: 30px;line-height: 30px;background-color: #3475c3;}
    .input_block{
      display: block;
    }
  </style>
</head>
<body>
  <div class="layui-fluid" style="margin-top:50px;">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">

        <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
              <div class="layui-card">
                <div class="layui-card-header">会员列表</div>

                <div class="layui-card-body">
                  <div class="layui-btn-container" style="font-size:14px;">

                    <!-- 缴费时间 -->
                    <div class="layui-form" style="width:100%;display:flex;">
                          <!-- 选择业务员 -->
                      <div class="layui-form" lay-filter="component-form-group" style="margin-right:10px;display: inline-block;display:flex;">
                      <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:10px">
                            <text class="yname">请选择门店</text>
                            <select name="agent" id="agent" lay-filter="agent" lay-search></select>
                        </div>
                      </div>
                      <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:10px">
                            <text class="yname">商品分类</text>
                            <select name="classification" id="classification" lay-filter="classification" lay-search></select>
                        </div>
                      </div>
                      <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:10px">
                            <text class="yname">是否推荐</text>
                            <select name="agent" id="agent" lay-filter="agent" lay-search></select>
                        </div>
                      </div>

                    </div>

                      <div class="layui-form-item">
                        <div class="layui-inline"style="margin-right:0">
                          <div class="layui-input-inline">
                          <text class="yname">商品名称</text>
                            <input type="text" class="layui-input start-item test-item" placeholder="请输入商品名称" name="tradename" lay-key="23" lay-filter="serchname">
                          </div>
                        </div>
                        <!-- <div class="layui-inline"style="margin-right:0">
                          <div class="layui-input-inline">
                          <text class="yname">请输入商品</text>
                            <input type="text" class="layui-input end-item test-item" placeholder="请输入商品名称" lay-key="24">
                          </div>
                        </div> -->
                        <div class="layui-inline">
                          <button class="layui-btn layuiadmin-btn-list" lay-submit="" lay-filter="LAY-app-contlist-search" style="border-radius:5px;margin-bottom: -1.2rem;height:36px;line-height: 36px;">
                            <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                          </button>
                        </div>
                      </div>
                    </div>
                    
                    <div class="layui-inline">
                      <button class="layui-btn layuiadmin-btn-list" id="addTable" style="border-radius:5px;margin-bottom: 30px;height:36px;line-height: 36px;">
                         <i class="iconfont">&#xe678;</i>新增
                      </button>
                      <button class="layui-btn layuiadmin-btn-list" id="batchlaunch" style="border-radius:5px;margin-bottom: 30px;height:36px;line-height: 36px;">
                         <i class="iconfont">&#xe671;</i>一键上架商品
                      </button>
                      <button class="layui-btn layuiadmin-btn-list" id="batchoffshelves" style="border-radius:5px;margin-bottom: 30px;height:36px;line-height: 36px;">
                         <i class="iconfont">&#xe67a;</i>一键下架商品
                      </button>
                      <button class="layui-btn layuiadmin-btn-list" id="batchdelete" style="border-radius:5px;margin-bottom: 30px;height:36px;line-height: 36px;">
                         <i class="iconfont">&#xe66f;</i>批量删除
                      </button>
                    </div>

                  </div>

                  {{--  layui模板  --}}
                    <script type="text/html" id="common_qrcode_template">
                      @{{# if (d.main_image != '') { }}
                      <img src="@{{ d.main_image }}"  width="60px;">
                      @{{# } else { }}
                      暂无数据
                      @{{# } }}
                    </script>
                  
                  <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                  <!-- 是否推荐 -->
                  <script type="text/html" id="recommendstatus">
                    @{{#  if(d.is_recommend == 1){ }}
                    <input type="checkbox" name="recommendstatus" lay-skin="switch" checked lay-filter="recommendstatus">
                    @{{#  } else { }}
                    <input type="checkbox" name="recommendstatus" lay-skin="switch" lay-filter="recommendstatus">
                    @{{#  } }}
                  </script>

                  <!-- 是否上架 -->
                  <script type="text/html" id="shelfstatus">
                    @{{#  if(d.status == 1){ }}
                    <input type="checkbox" name="shelfstatus" lay-skin="switch" checked lay-filter="shelfstatus">
                    @{{#  } else { }}
                    <input type="checkbox" name="shelfstatus" lay-skin="switch" lay-filter="shelfstatus">
                    @{{#  } }}
                  </script>


                  <!-- 判断状态 -->
                  <script type="text/html" id="paymoney">
                    @{{ d.rate }}%
                  </script>
                  <script type="text/html" id="table-content-list">
                    <a class="layui-btn layui-btn-normal layui-btn-xs tongbu" lay-event="edit" >编辑</a>
                    <a class="layui-btn layui-btn-normal layui-btn-xs tongbu" lay-event="del" >删除</a>
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
        <!-- 新增 -->
    <div id="submitAppletsNameInfo" class="hide layui-form" style="display: none;background-color: #fff;" lay-filter="addcommodety">
        <div class="xgrate">新增</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" style="padding-top: 20px;padding-bottom: 20px;" lay-filter="example">
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center"><i style="color:red;margin-right:5px;font-weight:bold;font-size:17px">*</i>商品名称</label>
                    <div class="layui-input-block">
                        <input type="text" placeholder="请输商品名称" name="tradenametext"  class="layui-input title" lay-verify="tradenametext" id="tradenametext">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center"><i style="color:red;margin-right:5px;font-weight:bold;font-size:17px">*</i>商品价格</label>
                    <div class="layui-input-block">
                        <input type="text" placeholder="请输入商品价格" name="commodityprice"  class="layui-input title" lay-verify="commodityprice">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center"><i style="color:red;margin-right:5px;font-weight:bold;font-size:17px">*</i>所属分类</label>
                    <div class="layui-input-block">
                        <select name="belonging" id="belonging" lay-filter="belonging" lay-search></select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center"><i style="color:red;margin-right:5px;font-weight:bold;font-size:17px">*</i>商品图片</label>
                    <div class="layui-upload-drag" id="productpicture" style="width:100px;height:100px;padding:0px">
                    <!-- <p style="font-size:45px;position: absolute;top: 50%;left: 50%;margin-top: -12px;margin-left: -13px;">+</p> -->
                    <div class="layui-upload" id="uploadDemoView">
                      <img src="" style="max-width:100px;max-height:100px;background-size: cover;">
                    </div>
                  </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center"><i style="color:red;margin-right:5px;font-weight:bold;font-size:17px">*</i>商品规格</label>
                    <div class="layui-input-block">
                        <input type="text" placeholder="请输入商品规格" name="specifications"  class="layui-input title" lay-verify="specifications">
                    </div>
                </div>
                <div class="layui-form-item ">
                    <label class="layui-form-label" style="text-align:center">商品属性</label>
                    <div class="layui-input-block attribute" id="attribute" style="width:auto">
  
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center">商品标签</label>
                    <div class="layui-input-block productlabel">
                        <!-- <input type="text" placeholder="请输入商品标签" name="productlabel"  class="layui-input title" id="productlabel" lay-verify="productlabel"> -->
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center"><i style="color:red;margin-right:5px;font-weight:bold;font-size:17px">*</i>是否推荐</label>
                    <div class="layui-input-block">
                      <!-- <input type="radio" name="isrecommended" value="1" title="是" lay-skin="sharing" lay-filter="isrecommended">
                      <input type="radio" name="isrecommended" value="2" title="否" lay-skin="sharing" lay-filter="isrecommended" > -->
                      <input type="radio" name="isrecommended" value="1" title="是" checked="">
                      <input type="radio" name="isrecommended" value="0" title="否">
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <div class="layui-footer" style="float: right;">
                        <button class="layui-btn close"style="border-radius:5px;background-color: #FFB800;">关闭</button>
                        <button class="layui-btn submitNewInfo" lay-submit lay-filter="submitNewInfo" style="border-radius:5px">确定</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
  <input type="hidden" class="classname">
  <!-- <input type="hidden" class="sort">
  <input type="hidden" class="stu_class_no">
<input type="hidden" class="stu_class_no">
  <input type="hidden" class="stu_order_batch_no">
  <input type="hidden" class="user_id">

  <input type="hidden" class="pay_status">
  <input type="hidden" class="pay_type"> -->

  <script type="text/html" id="mb_money_template">
    @{{ d.mb_money }} / @{{  d.mb_virtual_money }}
  </script>

  <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
  <script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script>
  <script src="{{asset('/layuiadmin/layui/inputTags/inputTags.js')}}"></script>
  <!-- <script src="{{asset('/layuiadmin/layui/jq.js')}}"></script> -->
  <script>
    var token = sessionStorage.getItem("Publictoken");
    var str=location.search;
    var store_id=sessionStorage.getItem("store_id");;
    console.log(store_id)

    layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate',"upload"], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate
            ,upload = layui.upload;
            var attribute=[] //商品属性
            var productlabel=[]//商品标签
            var attributeids=[]//商品属性id集
            var attrproductlabel=[]//商品标签id集
          // 打开新增窗口
          $('#addTable').on("click",function(){
                layer.open({
                    type: 1,
                    title: false,
                    closeBtn: 0,
                    area: '516px',
                    skin: 'layui-layer-nobg', //没有背景色
                    shadeClose: true,
                    content: $('#submitAppletsNameInfo')
                });
            });
            //关闭新增弹窗
            $(".close").on("click",function(){
                $("#submitAppletsNameInfo").hide();
                document.location.reload();
            })

        // 未登录,跳转登录页面
        $(document).ready(function(){
          if(token==null){
              window.location.href="{{url('/mb/login')}}";
          }

          // 选择门店
          $.ajax({
              url : "{{url('/api/merchant/store_lists')}}",
              data : {
                token:token,
                l:100
              },
              type : 'post',
              success : function(data) {
                console.log(data.data[0].is_send_tpl_mess)
                if(data.data[0].is_send_tpl_mess==2){
                }
                  $('.store_id').val(data.data[0].store_id);
                  var optionStr = "";
                  for(var i=0;i<data.data.length;i++){
                      optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
                      // optionStr += "<option value='" + data.data[i].store_id + "' "+((store_id==data.data[i].store_id)?"selected":"")+">" + data.data[i].store_name + "</option>";
                  }
                  // 渲染表格
                  table.render({
                      elem: '#test-table-page'
                      ,url: "{{url('/api/merchant/goodsListNew')}}"
                      ,method: 'post'
                      ,where:{
                          token:token,
                          storeId:data.data[0].store_id
                      }
                      ,page: true
                      ,cols: [[
                          {type:'checkbox',},
                          {align:'center',field:'name', title: '商品名称'}
                          ,{align:'center',field:'main_image',  title: '商品图片',templet: '#common_qrcode_template',event:'showimage'}
                          ,{align:'center',field:'c_name', title: '所属分类'}
                          // ,{align:'center',field:'sort',  title: '排序', edit: 'text',width:100,}
                          ,{align:'center',field:'price',  title: '商品价格'}
                          ,{align:'center',field:'unit',  title: '商品规格'}
                          ,{align:'center',field:'attribute_ids',  title: '商品属性'}
                          ,{align:'center',field:'tag_ids',  title: '商品标签'}
                          ,{align:'center',field:'is_recommend',  title: '推荐',templet:"#recommendstatus"}
                          ,{align:'center',field:'status',  title: '上架',templet:"#shelfstatus"}
                          ,{width:130,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
                      ]]
                      ,response: {
                          statusName: 'status' //数据状态的字段名称，默认：code
                          ,statusCode: 1 //成功的状态码，默认：0
                          ,msgName: 'message' //状态信息的字段名称，默认：msg
                          ,countName: 't' //数据总数的字段名称，默认：count
                          ,dataName: 'data' //数据列表的字段名称，默认：data
                      }
                      ,done: function(res, curr, count) {
                         $('th').css({
                             'font-weight': 'bold',
                             'font-size': '15',
                             'color': 'black',
                             'background': 'linear-gradient(#f2f2f2,#cfcfcf)'
                         }); //进行表头样式设置
                      }
                  });
                  $("#agent").append(optionStr);
                  layui.form.render('select');
              },
              error : function(data) {
                  alert('查找板块报错');
              }
          });
        });
//点击图片放大

//验证表单
form.verify({
          tradename: function(value){
            if(value==""){
              return '商品名称格式错误（商品名称不可为空）';
            }else if(value.length > 30){
              return '商品名称格式错误（商品名称超过三十个字）';
            }
          }
          ,classification: function(value) {
            if(value==""){
              return '暂无所属分类，请先在“商品管理/商品分类';
            }
          }
          ,content: function(value){
            layedit.sync(editIndex);
          }
        });


        /**
         * 属性数据回车切换成标签数据显示
         * 
         */
        var tags_attribute = new inputTags({
          elem: '.attribute',
          content: [],
          aldaBtn: true,
          inputType: 'text',
          theme: '#3475c3',
          placeholder: '按回车添加',
          regular: {
            rule: 'mobile',
          },
          beforeEnter: function() {
            // 可以使用 return false 阻止生成标签
          },
          afterEnter: function(res) {
            attribute=res;
          }
        })

        /**
         * 点击标签的删除图标的处理逻辑
         * 
         */
        tags_attribute.on('delTag', function(res) {});
        /**
         * 属性输入框按下回车键显示成标签数据展示
         * 
         */
        $(".attribute").keypress(function(event){
          if(event.which === 13) { 
            strattribute = ',' + attribute.join(",") + ",";
            var endattribute = attribute.pop()
            if(strattribute.indexOf("," + endattribute + ",") != -1){
              $.ajax({
                  url : "{{url('/api/merchant/updateGoodsAttribute')}}",
                  data : {
                    name:endattribute,
                    storeId: store_id,
                    token:token
                  },
                  type : 'POST',
                  success : function(data) {
                    attributeids.push(data.data.id)
                  },
                  error : function(data) {
                    alert('查找板块报错');
                  }
              });
            };
          }
        })

        /**
         * 商品标签数据
         * 
         */
        var tags_productlabel = new inputTags({
          elem: '.productlabel',
          content: [],
          aldaBtn: true,
          inputType: 'text',
          theme: '#3475c3',
          placeholder: '按回车添加',
          regular: {
            rule: 'mobile',
          },
          beforeEnter: function() {
            // 可以使用 return false 阻止生成标签
          },
          afterEnter: function(res) {
            productlabel=res;
          }
        });

        /**
         * 商品标签输入框按下回车键形成标签数据展示
         * 
         */
        $(".productlabel").keypress(function(event){
          if(event.which === 13) { 
            strproductlabel = ',' + productlabel.join(",") + ",";
            var endproductlabel = productlabel.pop()
            if(strproductlabel.indexOf("," + endproductlabel + ",") != -1){
              $.ajax({
                  url: "{{url('/api/merchant/updateGoodsTag')}}",
                  data: {
                    name:endproductlabel,
                    storeId: store_id,
                    token:token
                  },
                  type: 'POST',
                  success: function(data) {
                    attrproductlabel.push(data.data.id)
                  },
                  error: function(data) {
                    alert('查找板块报错');
                  }
              });
            };
          }
        });


  //单行删除
  table.on('tool(test-table-page)', function(obj){
    var value = obj.value //得到修改后的值
    ,e = obj.data //得到所在行所有键值
    ,field = obj.field; //得到字段
    console.log(e)
    // layer.msg('[ID: '+obj.event +'] ' + field + ' 字段更改为：'+ value);
    if(obj.event === 'del'){
      console.log(123)
      layer.confirm('确认删除吗？', function(){
          $.ajax({
              type: 'POST',
              url: "{{url('/api/merchant/delGoods')}}",
              data: {
                  token:token,
                  id:e.id,
                  delTag:1
              },
              success:function(res){
                  console.log(res)
                  if(res.status == 1){
                      layer.msg('删除成功',{icon:1},function(){
                          window.location.reload();
                      });
                  }else{
                      layer.msg('删除失败: ' + res.message,{icon:1},function(){
                          window.location.reload();
                      });
                  }
              }
          })
      });
    } else if (obj.event === 'edit') {
      layer.open({
        type: 1,
        title: false,
        closeBtn: 0,
        area: '516px',
        skin: 'layui-layer-nobg', //没有背景色
        shadeClose: true,
        content: $('#submitAppletsNameInfo')
    });

    // 将值放入模态框
    $('#tradenametext').val(e.name);
    setInterval(() => {
      $("#belonging").find("option[value="+e.cid+"]").prop("selected",true);
      form.render();  
    }, 500);

    $('#uploadDemoView>img').attr('src',e.main_image);
    $('#commodityprice').val(e.price);
    $('#specifications').val(e.unit);
    $('#isrecommended').val(e.is_recommend); 

    /**
     * 提交信息按钮
     * 
     */
    form.on("submit(submitNewInfo)",function(data){
        var requestData = data.field;
        console.log(requestData,'requestData')
        layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
        $.post("/api/merchant/updateGoods",{
            storeId:store_id,
            token: token,
            name:requestData.tradenametext,
            id:e.id,
            mainImage:$('#uploadDemoView >img').attr('src'),
            cid:requestData.belonging,
            price:requestData.commodityprice,
            attributeIds:attributeids.join(),
            tagIds:attrproductlabel.join(),
            unit:requestData.specifications,
            isRecommend:requestData.isrecommended,
        },function(data){
            var status = data.status;
            if(status == 1){
                layer.msg(data.message, {
                    offset: '50px'
                    ,icon: 1
                    ,time: 2000
                });
            }else{
                layer.msg(data.message, {
                    offset: '50px'
                    ,icon: 2
                    ,time: 2000
                });
            }
        },"json");
    });

    if(e.attribute_ids != ""){
      attribute_ids = e.attribute_ids.join();
      form.val('example', {
        "attribute":123,
      });
      $("#productlabel").val(e.tag_ids)
    }else if(e.tag_ids != ""){
      tag_ids = e.tag_ids.join();
      form.val('example', {
       "productlabel":321,
      });
    }

    
    form.val('example', {
       "tradenametext": e.name ,// "name": "value"
       "belonging":e.c_name,
       "commodityprice":e.price,
       "specifications":e.unit,
       "isrecommended":e.isRecommend,
    });
    tags_attribute.render(e.attribute_ids);
    tags_productlabel.render(e.tag_ids);


  }else if(obj.event === 'showimage'){
    img = e.main_image;
    layer.photos({
        photos: { "data": [{"src": e.main_image}] }
        ,anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机
  });

  }
});
   
        //商品分类
        $.ajax({
            url : "{{url('/api/merchant/goodsCategoryList')}}",
            data : {storeId:store_id,status: 1,sortMethod:"asc"},
            type : 'get',
            success : function(data) {
              console.log(data)
                var optionStr = "";
                for(var i=0;i<data.data.length;i++){
                    optionStr += "<option value='" + data.data[i].id + "'>" + data.data[i].name + "</option>";
                    // optionStr += "<option value='" + data.data[i].store_id + "' "+((store_id==data.data[i].store_id)?"selected":"")+">" + data.data[i].store_name + "</option>";
                }
                $("#classification").append('<option value="">选择分类</option>'+optionStr);
                layui.form.render(' ');
            },
            error : function(data) {
                alert('查找板块报错');
            }
        });
 //商品分类查询
        form.on('select(classification)', function(data){
          cid= data.value;
        //执行重载
        table.reload('test-table-page', {
            where: {
            cid:cid
            }
            ,page: {
                curr: 1
            }
        });
    });
//商品名称查询
    form.on('submit(LAY-app-contlist-search)', function(data){
      var requestData = data.field;
      var tradename=requestData.tradename
              //执行重载
      table.reload('test-table-page', {
            where: {
            name:tradename
            }
            ,page: {
                curr: 1
            }
      });
      });
  //商品排序
  table.on('edit(test-table-page)', function(obj){
     var value = obj.value //得到修改后的值
     ,data = obj.data //得到所在行所有键值
     ,field = obj.field; //得到字段
     layui.use('jquery',function(){
     var $=layui.$;
     $.ajax({
             type: 'POST',
             url: "{{url('/api/merchant/batchUpdateGoods')}}",
             data: {
                 token: token,
                 id: data.id,
                 sort:value
             },
             success: function(res) {
                 if (res.status == 1) {
                     layer.msg('更新成功', {
                         icon: 1
                     }, function() {
                      window.location.reload();
                     });
                 } else {
                     layer.msg(res.msg, {
                         icon: 1
                     }, function() {
                         window.location.reload();
                     });
                 }
             }
        });
     });
    });    
//是否推荐
form.on('switch(recommendstatus)', function(obj) {
    console.log(123)
          $.ajax({
              url: "{{url('/api/merchant/goodsListNew')}}",
              data: {
                token:token,
                storeId:store_id
              },
              type: 'POST',
              success: function(data) {
                  console.log(data)
                  //根据业务判断是开启还是关闭
                  var status = obj.elem.checked ? 1 : 0;
                  console.log(status)
                  //方法一取数据（根据相对位置取）
                  var index = obj.othis.parents('tr').attr("data-index");
                  var id = data.data[index].id;
                  var name = data.data[index].name;
                  $.ajax({
                      type: 'POST',
                      url: "{{url('/api/merchant/updateGoods')}}",
                      data: {
                          token: token,
                          isRecommend: status,
                          storeId: store_id,
                          id:id,
                          name:name
                      },
                      success: function(res) {
                          if (res.status == 1) {
                              layer.msg('更新成功', {
                                  icon: 1
                              }, function() {
                                  window.location.reload();
                              });
                          } else {
                              layer.msg(res.msg, {
                                  icon: 1
                              }, function() {
                                  window.location.reload();
                              });
                          }
                      }
                  });
              },
              error: function(data) {
                  alert('查找板块报错');
              }
          });

  });

  //商品上架状态
  form.on('switch(shelfstatus)', function(obj) {
    console.log(123)
          $.ajax({
              url: "{{url('/api/merchant/goodsListNew')}}",
              data: {
                token:token,
                storeId:store_id
              },
              type: 'POST',
              success: function(data) {
                  console.log(data)
                  //根据业务判断是开启还是关闭
                  var status = obj.elem.checked ? 1 : 2;
                  console.log(status)
                  //方法一取数据（根据相对位置取）
                  var index = obj.othis.parents('tr').attr("data-index");
                  var id = data.data[index].id;
                  var name = data.data[index].name;
                  $.ajax({
                      type: 'POST',
                      url: "{{url('/api/merchant/updateGoodsStatus')}}",
                      data: {
                          token: token,
                          status: status,
                          id:id,
                      },
                      success: function(res) {
                          if (res.status == 1) {
                              layer.msg('更新成功', {
                                  icon: 1
                              }, function() {
                                  window.location.reload();
                              });
                          } else {
                              layer.msg(res.msg, {
                                  icon: 1
                              }, function() {
                                  window.location.reload();
                              });
                          }
                      }
                  });
              },
              error: function(data) {
                  alert('查找板块报错');
              }
          });

  });
  //一键上架
  let btn = document.getElementById("batchlaunch");
  btn.onclick = function(){
    var checkStatus=table.checkStatus('test-table-page')
    console.log(checkStatus)
    var data=checkStatus.data
    console.log(data)
    var deList=[];
    for(let i=0;i<data.length;i++){
      deList.push(data[i].id)
    }
    console.log(deList)
    if(deList!=""){
      $.ajax({
              type: 'POST',
              url: "{{url('/api/merchant/batchUpdateGoods')}}",
              data: {
                  token: token,
                  status: 1,
                  id:deList.join(),
              },
              success: function(res) {
                  if (res.status == 1) {
                      layer.msg('更新成功', {
                          icon: 1
                      }, function() {
                          window.location.reload();
                      });
                  } else {
                      layer.msg(res.msg, {
                          icon: 1
                      }, function() {
                          window.location.reload();
                      });
                  }
              }
        });
    }
  }
    //一键下架
  let batchoffshelvesbtn = document.getElementById("batchoffshelves");
  batchoffshelvesbtn.onclick = function(){
    var checkStatus=table.checkStatus('test-table-page')
    console.log(checkStatus)
    var data=checkStatus.data
    console.log(data)
    var deList=[];
    for(let i=0;i<data.length;i++){
      deList.push(data[i].id)
    }
    console.log(deList)
    if(deList!=""){
      $.ajax({
              type: 'POST',
              url: "{{url('/api/merchant/batchUpdateGoods')}}",
              data: {
                  token: token,
                  status: 2,
                  id:deList.join(),
              },
              success: function(res) {
                  if (res.status == 1) {
                      layer.msg('更新成功', {
                          icon: 1
                      }, function() {
                          window.location.reload();
                      });
                  } else {
                      layer.msg(res.msg, {
                          icon: 1
                      }, function() {
                          window.location.reload();
                      });
                  }
              }
        });
    }
  }
  //批量删除
  let batchdeletebtn = document.getElementById("batchdelete");
  batchdeletebtn.onclick = function(){
    var checkStatus=table.checkStatus('test-table-page')
    console.log(checkStatus)
    var data=checkStatus.data
    console.log(data)
    var deList=[];
    for(let i=0;i<data.length;i++){
      deList.push(data[i].id)
    }
    console.log(deList)
    if(deList!=""){
      layer.confirm('确认删除吗？', function(){
        $.ajax({
              type: 'POST',
              url: "{{url('/api/merchant/batchUpdateGoods')}}",
              data: {
                  token: token,
                  status: 3,
                  delTag:1,
                  id:deList.join(),
              },
              success: function(res) {
                  if (res.status == 1) {
                      layer.msg('更新成功', {
                          icon: 1
                      }, function() {
                          window.location.reload();
                      });
                  } else {
                      layer.msg(res.msg, {
                          icon: 1
                      }, function() {
                          window.location.reload();
                      });
                  }
              }
        });
      });
    }
  }

        $.ajax({
            url : "{{url('/api/merchant/goodsCategoryList')}}",
            data : {storeId:store_id,status: 1,sortMethod:"asc"},
            type : 'get',
            success : function(data) {
              console.log(data)
//                console.log(data);
                var optionStr = "";
                for(var i=0;i<data.data.length;i++){
                    optionStr += "<option value='" + data.data[i].id + "'>" + data.data[i].name + "</option>";
                    // optionStr += "<option value='" + data.data[i].store_id + "' "+((store_id==data.data[i].store_id)?"selected":"")+">" + data.data[i].store_name + "</option>";
                }
                $("#belonging").append('<option value="">选择分类</option>'+optionStr);
                layui.form.render('select');
            },
            error : function(data) {
                alert('查找板块报错');
            }
        });
        
        // 监听删除标签
        //新增商品信息
        form.on("submit(submitNewInfo)",function(data){
            var requestData = data.field;
            console.log(requestData)
            layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
            $.post("/api/merchant/updateGoods",{
                storeId:store_id,
                token: token,
                name:requestData.tradenametext,
                // id
                mainImage:$('#uploadDemoView >img').attr('src'),
                cid:requestData.belonging,
                price:requestData.commodityprice,
                attributeIds:attributeids.join(),
                tagIds:attrproductlabel.join(),
                unit:requestData.specifications,
                isRecommend:requestData.isrecommended,
            },function(data){
                var status = data.status;
                if(status == 1){
                    layer.msg(data.message, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 2000
                    });
                }else{
                    layer.msg(data.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 2000
                    });
                }
            },"json");
            // layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
            // $.post("/api/merchant/updateGoodsStandard",{
            //     storeId:store_id,
            //     token: token,
            //     name:requestData.tradenametext,
            //     cid:requestData.belonging,
            //     price:requestData.commodityprice,
            //     attributeIds:attribute.join(),
            //     tagIds:productlabel.join(),
            //     unit:requestData.specifications,
            //     isRecommend:requestData.isrecommended,
            // },function(data){
            //     var status = data.status;
            //     if(status == 1){
            //         layer.msg(data.message, {
            //             offset: '50px'
            //             ,icon: 1
            //             ,time: 2000
            //         });
            //     }else{
            //         layer.msg(data.message, {
            //             offset: '50px'
            //             ,icon: 2
            //             ,time: 2000
            //         });
            //     }
            // },"json");
            // return false;
        });
        //上传商品图片
        upload.render({
          elem: '#productpicture'
          ,url: '/api/basequery/upload'//上传接口' //改成您自己的上传接口
          ,data:{token:token,type:'img',attach_name: 'file',}
          ,done: function(res){
            layer.msg('上传成功');
            layui.$('#uploadDemoView').removeClass('layui-hide').find('img').attr('src', res.data.img_url);
            console.log(res,999999999999999)
           }
        });

});

  </script>
</body>
</html>







