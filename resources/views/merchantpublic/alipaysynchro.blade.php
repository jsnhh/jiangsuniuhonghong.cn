<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>支付宝模板设置</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
  <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
  <style>
    .edit{background-color: #ed9c3a;}
    .shenhe{background-color: #429488;}
    .see{background-color: #7cb717;}
    .tongbu{background-color: #4c9ef8;color:#fff;}
    .cur{color:#009688;}
    .bj{
      background: url('{{asset('/mb/background.png')}}') no-repeat; width:100%;
      height:800px;background-size:contain;position: relative;
    }
    .bj .img{
      width:86%;
      height:160px;
      top: 40px;
      left: 7%;
      position: absolute;
    }
    .bj .logo{
      width:35px;
      height:35px;
      margin-top: 50px;
      margin-left: 10%;
      position: absolute;
    }
    .bj .name{
      position: absolute;
      margin-top: 100px;
      margin-left: 11%;
      color:#fff;
      font-size:17px;
    }
    .bj .name p:last-child{     
      font-size:14px;
      margin-top:2px;
    }
    .bj .code{
      width:20px;
      height:20px;
      position: absolute;
      margin-top: 115px;
      right: 11%;
    }
    .bj .num{
      position: absolute;
      top: 160px;
      left: 10%;
      color:#fff;
      font-size:22px;
    }
    .min_img {
      width: 60px;
      height: 70px;
      margin-right: 10px;
      float:left;
      overflow: hidden;
    }
    .min_img img{
      width: 60px;
      height: 38px;
      margin-right: 10px;
    }
    .layui-form-radio{
      margin-left: 20px;
    }
    .layui-form-label span{
      color:red;
      padding-right:5px;
    }
  </style>
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">

        <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12" style="margin-top:30px">
              <div class="layui-card"> 
                <div class="layui-card-header">支付宝卡券模板设置</div>

                <div class="layui-card-body">

                    <div class="layui-row">
                      <div class="layui-col-md4">
                        <div class="bj">
                          <image class='img' src="{{asset('/mb/vip-bg-1.png')}}"></image>
                          <image class='logo' src="{{asset('/mb/vip.png')}}"></image>
                          <div class="name">
                            <p class="store_name"></p>
                            <p class="store_desc"></p>
                          </div>
                          <image class='code' src="{{asset('/mb/shuzima.png')}}"></image>
                          <div class="num">123456789</div>
                        </div>
                      </div>
                      <div class="layui-col-md6">
                        <!-- 选择业务员 -->
                        <div class="layui-form">
                          <!-- <div class="layui-form-item" style="width:200px;display: inline-block;float:left;font-size:26px;">
                            <label class="layui-form-label"  style="width:200px;text-align: left;">会员卡功能</label>
                          </div> -->
                          
                          <div class="layui-form-item">                          
                            <div class="layui-input-block" style="margin-left:20px;">
                                <select name="agent" id="agent" lay-filter="agent" lay-search>
                                </select>
                            </div>
                          </div>

                        </div> 
                        <div class="layui-form" lay-filter="component-form-group">
                          
                          <div class="layui-form-item">
                            <label style="border-left:3px solid #2db7f5;padding-left:10px;margin-left:20px;">自定义入口一</label>
                          </div>

                          <div class="layui-form-item">
                            <div class="layui-input-block" style='margin-left:0;'>
                              <input type="radio" name="abox" value="1" title="未设置" lay-filter="test1">
                              <input type="radio" name="abox" value="2" title="自定义链接"  checked="" lay-filter="test2">
                              <input type="radio" name="abox" value="3" title="自定义小程序" lay-filter="test3">
                            </div>
                          </div>

                          <div class="one_a">                            
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>入口名称:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item1">
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>自定义链接:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label">引导语:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                          </div>
                          <div class="one_b" style='display: none'>                            
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>入口名称:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item1">
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>小程序ID:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>小程序页面:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label">引导语:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                          </div>


                          <!-- 二************************* -->
                          <div class="layui-form-item">
                            <label style="border-left:3px solid #2db7f5;padding-left:10px;margin-left:20px;">自定义入口二</label>
                          </div>
                          
                          <div class="layui-form-item">
                            <div class="layui-input-block" style='margin-left:0;'>
                              <input type="radio" name="bbox" value="4" title="未设置" lay-filter="test4">
                              <input type="radio" name="bbox" value="5" title="自定义链接"  checked="" lay-filter="test5">
                              <input type="radio" name="bbox" value="6" title="自定义小程序" lay-filter="test6">
                            </div>
                          </div>

                          <div class="two_a">                            
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>入口名称:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item1">
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>自定义链接:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label">引导语:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                          </div>
                          <div class="two_b" style='display: none'>                            
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>入口名称:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item1">
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>小程序ID:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>小程序页面:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label">引导语:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                          </div>

                          <!-- 三 *********************************-->
                          <div class="layui-form-item">
                            <label style="border-left:3px solid #2db7f5;padding-left:10px;margin-left:20px;">自定义入口三</label>
                          </div>
                          
                          <div class="layui-form-item">
                            <div class="layui-input-block" style='margin-left:0;'>
                              <input type="radio" name="cbox" value="4" title="未设置" lay-filter="test7">
                              <input type="radio" name="cbox" value="5" title="自定义链接"  checked="" lay-filter="test8">
                              <input type="radio" name="cbox" value="6" title="自定义小程序" lay-filter="test9">
                            </div>
                          </div>

                          <div class="three_a">                            
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>入口名称:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item1">
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>自定义链接:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label">引导语:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                          </div>
                          <div class="three_b" style='display: none'>                            
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>入口名称:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item1">
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>小程序ID:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>小程序页面:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label">引导语:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                          </div>


                          <!-- 四 *********************************-->
                          <div class="layui-form-item">
                            <label style="border-left:3px solid #2db7f5;padding-left:10px;margin-left:20px;">自定义入口四</label>
                          </div>
                          
                          <div class="layui-form-item">
                            <div class="layui-input-block" style='margin-left:0;'>
                              <input type="radio" name="dbox" value="4" title="未设置" lay-filter="test10">
                              <input type="radio" name="dbox" value="5" title="自定义链接"  checked="" lay-filter="test11">
                              <input type="radio" name="dbox" value="6" title="自定义小程序" lay-filter="test12">
                            </div>
                          </div>

                          <div class="four_a">                            
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>入口名称:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item1">
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>自定义链接:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label">引导语:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                          </div>
                          <div class="four_b" style='display: none'>                            
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>入口名称:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item1">
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>小程序ID:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label"><span>*</span>小程序页面:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                            <div class="layui-form-item">
                              <label class="layui-form-label">引导语:</label>
                              <div class="layui-input-block">
                                  <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input item2" style="width:75%;float:left;margin-right:10px;" >  
                              </div>
                            </div>
                          </div>

                          <div class="layui-form-item">
                            <div class="layui-input-block">
                              <button class="layui-btn" lay-submit="" lay-filter="formDemo" id='submit'>提交</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  <input type="hidden" class="store_id" value="">

  <input type="hidden" class="js_one_id" value="1">
  <input type="hidden" class="js_two_id" value="1">
  <input type="hidden" class="js_three_id" value="1">
  <input type="hidden" class="js_four_id" value="1">

  <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script> 
  <script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script>
    <script>
    var token = sessionStorage.getItem("Publictoken");
    var str=location.search;
    var store_id=str.split('?')[1];
    
    layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate'], function(){
      var $ = layui.$
        ,admin = layui.admin
        ,form = layui.form
        ,table = layui.table
        ,laydate = layui.laydate;
        // 未登录,跳转登录页面
        $(document).ready(function(){        
          if(token==null){
              window.location.href="{{url('/mb/login')}}"; 
          } 

                  
        });
        
        // 第一板块************
        form.on('radio(test1)', function(data){  
          $('.one_a').hide();
          $('.one_b').hide();
          $('.js_one_id').val('0');
        });
        form.on('radio(test2)', function(data){
          // console.log(data.elem); //得到radio原始DOM对象
          // console.log(data.value); //被点击的radio的value值
          $('.one_a').show();
          $('.one_b').hide();
          $('.js_one_id').val('1');
        });
        form.on('radio(test3)', function(data){
          // console.log(data.elem); //得到radio原始DOM对象
          // console.log(data.value); //被点击的radio的value值
          $('.one_a').hide();
          $('.one_b').show();
          $('.js_one_id').val('2');
        });
        // 第二板块************
        form.on('radio(test4)', function(data){  
          $('.two_a').hide();
          $('.two_b').hide();
          $('.js_two_id').val('0');
        });
        form.on('radio(test5)', function(data){
          // console.log(data.elem); //得到radio原始DOM对象
          // console.log(data.value); //被点击的radio的value值
          $('.two_a').show();
          $('.two_b').hide();
          $('.js_two_id').val('1');
        });
        form.on('radio(test6)', function(data){
          // console.log(data.elem); //得到radio原始DOM对象
          // console.log(data.value); //被点击的radio的value值
          $('.two_a').hide();
          $('.two_b').show();
          $('.js_two_id').val('2');
        });
        // 第三板块************************        
        form.on('radio(test7)', function(data){  
          $('.three_a').hide();
          $('.three_b').hide();
          $('.js_three_id').val('0');
        });
        form.on('radio(test8)', function(data){
          // console.log(data.elem); //得到radio原始DOM对象
          // console.log(data.value); //被点击的radio的value值
          $('.three_a').show();
          $('.three_b').hide();
          $('.js_three_id').val('1');
        });
        form.on('radio(test9)', function(data){
          // console.log(data.elem); //得到radio原始DOM对象
          // console.log(data.value); //被点击的radio的value值
          $('.three_a').hide();
          $('.three_b').show();
          $('.js_three_id').val('2');
        });
        // 第四板块************************        
        form.on('radio(test10)', function(data){  
          $('.four_a').hide();
          $('.four_b').hide();
          $('.js_four_id').val('0');
        });
        form.on('radio(test11)', function(data){
          // console.log(data.elem); //得到radio原始DOM对象
          // console.log(data.value); //被点击的radio的value值
          $('.four_a').show();
          $('.four_b').hide();
          $('.js_four_id').val('1');
        });
        form.on('radio(test12)', function(data){
          // console.log(data.elem); //得到radio原始DOM对象
          // console.log(data.value); //被点击的radio的value值
          $('.four_a').hide();
          $('.four_b').show();
          $('.js_four_id').val('2');
        });

        // 门店***********************
        $.ajax({
          url : "{{url('/api/merchant/store_lists')}}",
          data : {token:token,l:100},
          type : 'post',
          success : function(data) {
              console.log(data);
              $('.store_id').val(data.data[0].store_id);
              // 查看
              $.post("{{url('/api/member/query_tpl')}}",
              {
                  token:token
                  ,store_id:data.data[0].store_id
              },function(res){
                  console.log(res);
                  if(res.status==1){
                    $('.store_name').html(res.data.tpl_name)
                    $('.store_desc').html(res.data.tpl_desc)
                  
                    $('.js_tpl_bck').val(res.data.tpl_bck)                    
                  }else{
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 3000
                    });
                  }
              },"json");




              var optionStr = "";
              for(var i=0;i<data.data.length;i++){
                  optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
                  
              }    
              $("#agent").append(optionStr);
              layui.form.render('select');
          },
          error : function(data) {
              alert('查找板块报错');
          }
        });
        


        // 提交
        $('#submit').click(function(){
          var a_box=[]
          var b_box=[]
          var c_box=[]
          var d_box=[]
          // ***********************************第一部分
          if($('.js_one_id').val()=='0'){
            a_box=[]
            console.log(JSON.stringify(a_box))
          }else if($('.js_one_id').val()=='1'){
            var one_a1=$('.one_a .layui-form-item').eq(0).find('input').val();
            var one_a2=$('.one_a .layui-form-item').eq(1).find('input').val();
            var one_a3=$('.one_a .layui-form-item').eq(2).find('input').val();
            var obja1={"custom_name":one_a1,"custom_url":one_a2,"custom_desc":one_a3}

            a_box=JSON.stringify(obja1)
            console.log(a_box)
          }else{
            var one_b1=$('.one_b .layui-form-item').eq(0).find('input').val();
            var one_b2=$('.one_b .layui-form-item').eq(1).find('input').val();
            var one_b3=$('.one_b .layui-form-item').eq(2).find('input').val();
            var one_b4=$('.one_b .layui-form-item').eq(3).find('input').val();
            var obja1={"custom_name":one_b1,"custom_wx_appid":one_b2,"custom_wx_path":one_b3,"custom_desc":one_b4}
            
            a_box=JSON.stringify(obja1)
            console.log(a_box)
          }

          // ***********************************第二部分
          if($('.js_two_id').val()=='0'){
            b_box=[]
            console.log(JSON.stringify(b_box))
          }else if($('.js_two_id').val()=='1'){
            var one_a1=$('.two_a .layui-form-item').eq(0).find('input').val();
            var one_a2=$('.two_a .layui-form-item').eq(1).find('input').val();
            var one_a3=$('.two_a .layui-form-item').eq(2).find('input').val();
            var obja1={"custom_name":one_a1,"custom_url":one_a2,"custom_desc":one_a3}

            b_box=JSON.stringify(obja1)
            console.log(b_box)
          }else{
            var one_b1=$('.two_b .layui-form-item').eq(0).find('input').val();
            var one_b2=$('.two_b .layui-form-item').eq(1).find('input').val();
            var one_b3=$('.two_b .layui-form-item').eq(2).find('input').val();
            var one_b4=$('.two_b .layui-form-item').eq(3).find('input').val();
            var obja1={"custom_name":one_b1,"custom_wx_appid":one_b2,"custom_wx_path":one_b3,"custom_desc":one_b4}
            
            b_box=JSON.stringify(obja1)
            console.log(b_box)
          }

          // ***********************************第三部分
          if($('.js_three_id').val()=='0'){
            c_box=[]
            console.log(JSON.stringify(c_box))
          }else if($('.js_three_id').val()=='1'){
            var one_a1=$('.three_a .layui-form-item').eq(0).find('input').val();
            var one_a2=$('.three_a .layui-form-item').eq(1).find('input').val();
            var one_a3=$('.three_a .layui-form-item').eq(2).find('input').val();
            var obja1={"custom_name":one_a1,"custom_url":one_a2,"custom_desc":one_a3}

            c_box=JSON.stringify(obja1)
            console.log(c_box)
          }else{
            var one_b1=$('.three_b .layui-form-item').eq(0).find('input').val();
            var one_b2=$('.three_b .layui-form-item').eq(1).find('input').val();
            var one_b3=$('.three_b .layui-form-item').eq(2).find('input').val();
            var one_b4=$('.three_b .layui-form-item').eq(3).find('input').val();
            var obja1={"custom_name":one_b1,"custom_wx_appid":one_b2,"custom_wx_path":one_b3,"custom_desc":one_b4}
            
            c_box=JSON.stringify(obja1)
            console.log(c_box)
          }

          // ***********************************第四部分
          if($('.js_four_id').val()=='0'){
            d_box=[]
            console.log(JSON.stringify(d_box))
          }else if($('.js_four_id').val()=='1'){
            var one_a1=$('.four_a .layui-form-item').eq(0).find('input').val();
            var one_a2=$('.four_a .layui-form-item').eq(1).find('input').val();
            var one_a3=$('.four_a .layui-form-item').eq(2).find('input').val();
            var obja1={"custom_name":one_a1,"custom_url":one_a2,"custom_desc":one_a3}

            d_box=JSON.stringify(obja1)
            console.log(d_box)
          }else{
            var one_b1=$('.four_b .layui-form-item').eq(0).find('input').val();
            var one_b2=$('.four_b .layui-form-item').eq(1).find('input').val();
            var one_b3=$('.four_b .layui-form-item').eq(2).find('input').val();
            var one_b4=$('.four_b .layui-form-item').eq(3).find('input').val();
            var obja1={"custom_name":one_b1,"custom_wx_appid":one_b2,"custom_wx_path":one_b3,"custom_desc":one_b4}
            
            d_box=JSON.stringify(obja1)
            console.log(d_box)
          }





          $.post("{{url('api/member/wx_member_card')}}",
          {
            token:token,
            store_id:$('.store_id').val(),

            custom_a:a_box,
            custom_b:b_box,
            custom_c:c_box,
            custom_d:d_box,

          },function(res){
              console.log(res);
              if(res.status==1){
                  layer.msg(res.message, {
                      offset: '50px'
                      ,icon: 1
                      ,time: 3000
                  });
              }else{
                  layer.msg(res.message, {
                      offset: '50px'
                      ,icon: 2
                      ,time: 3000
                  });
              }
          },"json");
        })
               

    });

  </script>

</body>
</html>





