<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>微信商家券</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/inputTags/inputTags.css')}}">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .tongbu {
            background-color: #4c9ef8;
            color: #fff;
        }

        .cur {
            color: #009688;
        }

        .yname {
            font-size: 13px;
            color: #444;
        }

        .xgrate {
            color: #fff;
            font-size: 15px;
            padding: 7px;
            height: 30px;
            line-height: 30px;
            background-color: #3475c3;
        }

        .input_block {
            display: block;
        }
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
        }
        input[type="number"]{
            -moz-appearance: textfield;
        }
    </style>
</head>

<body>
    <div class="layui-fluid" style="margin-top:50px;">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">

                <div class="layui-fluid">
                    <div class="layui-row layui-col-space15">
                        <div class="layui-col-md12">
                            <div class="layui-card">
                                <div class="layui-card-header">微信商家券</div>

                                <div class="layui-card-body">
                                    <div class="layui-btn-container" style="font-size:14px;">

                                        <!-- 缴费时间 -->
                                        <div class="layui-form" style="width:100%;display:flex;">
                                            <!-- 选择业务员 -->
                                            <div class="layui-form" lay-filter="component-form-group" style="margin-right:10px;display: inline-block;display:flex;width:1375px;flex-wrap:wrap;">
                                                <!-- <div class="layui-form-item">
                                                    <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">通道类型</text>
                                                        <select name="channeltype" id="channeltype" lay-filter="channeltype" lay-search></select>
                                                    </div>
                                                </div>
                                                <div class="layui-form-item">
                                                    <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">商家券类型</text>
                                                        <select name="vouchertype" id="vouchertype" lay-filter="vouchertype" lay-search></select>
                                                    </div>
                                                </div>
                                                <div class="layui-form-item">
                                                    <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">发放状态</text>
                                                        <select name="distributionstatus" id="distributionstatus" lay-filter="distributionstatus" lay-search></select>
                                                    </div>
                                                </div> -->

                                                <!-- <div class="layui-form-item">
                                                    <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">商家券名称</text>
                                                        <input type="text" name="vouchername" class="layui-input vouchername" placeholder="输入商家券名称" >
                                                    </div>
                                                </div> -->

                                                <div class="layui-form-item">
                                                    <div class="layui-input-inline" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">券有效期开始时间</text>
                                                        <input type="text" name="effectivetimeone" class="layui-input validitystart-item test-item" placeholder="券有效期开始时间" lay-key="23">
                                                    </div>
                                                </div>
                                                <div class="layui-form-item">
                                                    <div class="layui-input-inline" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">券有效期结束时间</text>
                                                        <input type="text" name="effectivetimetwo" class="layui-input validityend-item test-item" placeholder="券有效期结束时间" lay-key="24">
                                                    </div>
                                                </div> 
                                                <!-- <div class="layui-form-item">
                                                    <div class="layui-input-inline" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">券创建开始时间</text>
                                                        <input type="text" class="layui-input establishstart-item test-item"  placeholder="券创建开始时间" lay-key="25">
                                                    </div>
                                                </div>
                                                <div class="layui-form-item">
                                                    <div class="layui-input-inline" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">券创建结束时间</text>
                                                        <input type="text" class="layui-input establishend-item test-item" placeholder="券创建结束时间" lay-key="26">
                                                    </div>
                                                </div> -->
                                                <div style="display:flex;align-items: center;margin-left:10px;margin-top: 16px;margin-bottom: 0px;">
                                                    <div class="layui-inline">
                                                        <button class="layui-btn layuiadmin-btn-list" lay-submit=""  lay-filter="LAY-app-contlist-search"  style="border-radius:5px;height:36px;line-height: 36px;">
                                                            <i  class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                                        </button>
                                                    </div>
                                                    <div class="layui-inline" style="width:300px;margin-right:0px;margin-top:10px;margin-bottom:10px">
                                                        <button class="layui-btn layuiadmin-btn-list" id="addTable" style="border-radius:5px;height:36px;line-height: 36px;">
                                                            <i class="layui-icon">&#xe608;</i>新增
                                                        </button>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                    <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 判断状态 -->
    <script type="text/html" id="paymoney">
        @{{ d.rate }}%
    </script>
    <script type="text/html" id="table-content-list">
        <a class="layui-btn layui-btn-normal layui-btn-xs writeoffdetails" lay-event="writeoffdetails">核销详情</a>
        <a class="layui-btn layui-btn-danger  layui-btn-xs delectdata" lay-event="delectdata">删除</a>
        <!-- <a class="layui-btn  layui-btn-xs storecode" lay-event="storecode">二维码</a> -->
    </script>
    <!-- 新增 -->
    
        <div id="submitAppletsNameInfo" class="hide layui-form" style="display: none;background-color: rgba(0,0,0,0.4);position: fixed;width: 100%;height: 100%;left: 0;top: 0;" lay-filter="addcommodety">
            <div style="width: 500px;margin: 0 auto ;transform: translateY(42px);">
                <div class="xgrate">新增</div>
                <div class="layui-card-body" style="padding: 15px;background-color: #ffffff;">
                <div class="layui-form" style="padding-top: 20px;padding-bottom: 20px;" lay-filter="example">
                    <div class="layui-form-item">
                        <label class="layui-form-label" style="text-align:center">商家券名称</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入商家券名称(15字以内)" name="addvouchername_add"
                                class="layui-input title" lay-verify="addvouchername" id="addvouchername">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label" style="text-align:center">券类型</label>
                        <div class="layui-input-block">
                            <input type="radio" name="addvouchertype" value="1" title="满减券" checked="">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label" style="text-align:center">总预算</label>
                        <div class="layui-input-block">
                            <input type="number" placeholder="请输入总预算金额(元)" name="max_amount" class="layui-input title">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label" style="text-align:center">顾客消费满</label>
                        <div class="layui-input-block">
                            <input type="number" placeholder="金额（元）" name="transaction_minimum" class="layui-input title"
                                lay-verify="addfullreduction" id="addfullreduction">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label" style="text-align:center">可享优惠</label>
                        <div class="layui-input-block">
                            <input type="number" placeholder="金额（元）" name="discount_amount" class="layui-input title"
                                lay-verify="addbenefits" id="addbenefits">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label" style="text-align:center">使用有效期</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input addvaliditystart-item test-item"
                                name="available_begin_time" placeholder="有效期开始时间" lay-key="27">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label" style="text-align:center"></label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input addvalidityend-item test-item" name="available_end_time"
                                placeholder="有效期结束时间" lay-key="28">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label" style="text-align:center">发券张数</label>
                        <div class="layui-input-block">
                            <input type="number" placeholder="请输入发券张数" name="max_coupons" class="layui-input title"
                                lay-verify="addcouponsissued" id="addcouponsissued">
                        </div>
                    </div>
                    <div class="layui-form-item" style="display: none;">
                        <label class="layui-form-label" style="text-align:center">单人可领</label>
                        <div class="layui-input-block">
                            <input type="number" placeholder="单个用户可领取数量" name="max_coupons_per_user" class="layui-input title"
                                lay-verify="addsingleperson" id="addsingleperson">
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="float: right;">
                            <button class="layui-btn close" style="border-radius:5px;background-color: #FFB800;">关闭</button>
                            <button type="submit" class="layui-btn submitNewInfo" lay-submit="" lay-filter="submitNewInfo"
                                style="border-radius:5px">确定</button>
                        </div>
                    </div>
                </div>
             </div>
            </div>
        </div>
        
    </div>
     <!-- 是否推荐 -->
     <script type="text/html" id="recommendstatus">
        <div class="switchboxlist">
            @{{#  if(d.is_send == 2){ }}
            <input type="checkbox" name="recommendstatus" lay-event="switchobj" lay-skin="switch" lay-filter="recommendstatus" checked >
            @{{#  } else { }}
            <input type="checkbox" name="recommendstatus" lay-event="switchobj" lay-skin="switch" lay-filter="recommendstatus">
            @{{#  } }}
        </div>
    </script>
    <div id="edit_rate" class="hide" style="display: none;background-color: #eee;">
        <div class="xgrate">二维码（微信扫码）</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form">
                <div class="layui-form-item">
                    <div id="code">

                    </div>
                    <div style="display:flex;margin-left: 20%;"> 
                        <div style="text-align: center;" class="storename"></div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- </div>--}}
    <input type="hidden" class="classname">

    <script type="text/html" id="mb_money_template">
        @{{ d.mb_money }} / @{{ d.mb_virtual_money }}
    </script>
    <script type="text/javascript" src="{{asset('/userweb/layuinew/layui.js')}}"></script>
    <script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script>
    <script src="{{asset('/layuiadmin/layui/inputTags/inputTags.js')}}"></script>
    <!-- <script src="{{asset('/layuiadmin/layui/jq.js')}}"></script> -->
    <script src="{{asset('/layuiadmin/layui/jquery.qrcode.min.js')}}"></script>
    <script>
        var token = sessionStorage.getItem("Publictoken");
        var str = location.search;
        var store_id = sessionStorage.getItem("store_id");
        console.log(store_id)

        layui.config({
            base: '../../layuiadmin/' //静态资源所在路径
        }).extend({
            index: 'lib/index' //主入口模块
        }).use(['index', 'form', 'table', 'laydate', "upload"], function() {
            var $ = layui.$,
                admin = layui.admin,
                form = layui.form,
                table = layui.table,
                laydate = layui.laydate,
                upload = layui.upload;
            form.on('switch(recommendstatus)',function(data){
                //获取到当前的状态
                var checked = data.elem.checked;
                let number = 0;
                $(".switchboxlist").each((k,v)=>{
                    if($(v).find("input").is(":checked")){
                        number ++ ;
                    }
                })
                //判断是否是一个打开的按钮
                if(number > 1){
                    
                    layer.msg("请关闭其他发券状态！！！", {icon:2, shade:0.5, time:2000});
                    $(data.elem).prop("checked",false)
                    layui.form.render('checkbox');
                    return false;

                }else{

                    //判断点击了第几个
                    let index ;
                    //获取点击的父级
                    let parentobj = $(data.elem).parent()[0];
                    $(".switchboxlist").each((k,v)=>{
                        if(v == parentobj ) {
                            index = k;
                        }
                    })
                    let cashCouponitem = window.sessionStorage.getItem("cashCouponitemsjq");
                    let dataobjlistname = JSON.parse(cashCouponitem);
                    let datafromlist = dataobjlistname[index];
                    if($(data.elem).is(':checked')){ //说明强制发券 开启

                        console.log("打开",index)
                        $.post("/api/wechat/updateCouponSendStatus",{
                            id:datafromlist.id,
                            store_id:store_id,
                            is_send:2,
                            coupon_type:1,
                            out_request_no:datafromlist.out_request_no,
                            token:token,
                        },res=>{
                            console.log(res)
                            if(res.code == 200){
                                layer.msg("开启成功", {icon:1, shade:0.5, time:1000},function(){
                                    location.reload();
                                });
                                data.elem.checked = true;
                                form.render();
                            }else{
                                data.elem.checked = false;
                                form.render();
                                layer.msg(res.msg, {icon:2, shade:0.5, time:1000});
                            }
                        })
                        
                    }else{                          //说明强制发券 关闭
                        console.log("关闭",index)
                        $.post("/api/wechat/updateCouponSendStatus",{
                            id:datafromlist.id,
                            store_id:store_id,
                            is_send:1,
                            coupon_type:1,
                            out_request_no:datafromlist.out_request_no,
                            token:token,
                        },res=>{
                            console.log(res)
                            if(res.code == 200){
                                layer.msg("关闭成功", {icon:1, shade:0.5, time:1000},function(){
                                    location.reload();
                                });
                                data.elem.checked = false;
                                form.render();
                            }else{
                                data.elem.checked = true;
                                form.render();
                                layer.msg(res.msg, {icon:2, shade:0.5, time:1000});
                            }
                        })
                    }
                }
            })
            // 打开新增窗口
            $('#addTable').on("click", function() {
                $("#submitAppletsNameInfo").css({"display":"block"})
            });
            //关闭新增弹窗
            $(".close").on("click", function() {
                $("#submitAppletsNameInfo").hide();
                document.location.reload();
            })
            //关闭新增窗口
            $("#submitAppletsNameInfo").click(function(e){
                if(e.target == this){
                    $("#submitAppletsNameInfo").hide();
                }
            })



            // 未登录,跳转登录页面
            $(document).ready(function() {
                if (token == null) {
                    window.location.href = "{{url('/mb/login')}}";
                }
            });
            // 渲染表格
            table.render({
                elem: '#test-table-page',
                url: "{{url('/api/merchant/getWechatMerchantCouponList')}}",
                method: 'post',
                where: {
                    token: token,
                    store_id: store_id,
                    terminal_type: "weChat"
                },
                request: {
                    pageName: 'page',
                    limitName: 'limit'
                },
                page: true,
                cellMinWidth: 150,
                cols: [
                    [{
                        align: 'center',
                        field: 'coupon_stock_name',
                        title: '商家券名称'
                    }, {
                        align: 'center',
                        field: 'coupon_type',
                        title: '商家券类型'
                    }, 
                    {
                        align: 'center',
                        field: 'channel',
                        title: '通道'
                    }, 
                    {
                        align: 'center',
                        field: 'coupon_status',
                        title: '发放状态',
                        edit: 'text'
                    },{
                        align:'center',
                        field:'is_send',
                        title: '领券立减',
                        width:140,
                        templet:"#recommendstatus"
                    } ,
                    {
                        align: 'center',
                        field: 'manduohsao',
                        title: '活动规则'
                    }, 
                    {
                        align: 'center',
                        field: 'max_coupons',
                        title: '可发放数量'
                    }, {
                        align: 'center',
                        field: 'sent_num',
                        title: '发放数量'
                    }, {
                        align: 'center',
                        field: 'sent_percent',
                        title: '发放比例'
                    }, 
                    {
                        align: 'center',
                        field: 'created_at',
                        title: '创建时间',
                        width: 180,
                    }, 
                     {
                        align: 'center',
                        field: 'available_begin_time',
                        title: '有效期开始时间',
                        width: 180,
                    }, {
                        align: 'center',
                        field: 'available_end_time',
                        title: '有效期结束时间',
                        width: 180,
                    }, 
                    {
                        align: 'center',
                        fixed: 'right',
                        toolbar: '#table-content-list',
                        title: '操作'
                    }]
                ],
                response: {
                    statusName: 'status' //数据状态的字段名称，默认：code
                        ,
                    statusCode: 200 //成功的状态码，默认：0
                        ,
                    msgName: 'message' //状态信息的字段名称，默认：msg
                        ,
                    countName: 't' //数据总数的字段名称，默认：count
                        ,
                    dataName: 'data' //数据列表的字段名称，默认：data
                },
                parseData:function(data){
                    console.log(data)
                    let listres = data.data;
                    if(listres.length > 0){  
                        for(let i=0;i<listres.length;i++){
                            listres[i].coupon_type = "满减券"
                            listres[i].manduohsao = "满"+listres[i].transaction_minimum+"减"+listres[i].discount_amount+"";
                        }
                        return {
                            "status": 200,
                            "data":listres,
                            "t":data.t  
                        }
                    }else{
                        return {
                            "status": 200,
                            "data":listres,
                            "t":data.t  
                        }
                    }
                },
                done: function(res, curr, count) {
                    console.log(res)
                    window.sessionStorage.setItem("cashCouponitemsjq",JSON.stringify(res.data))
                    $('th').css({
                        'font-weight': 'bold',
                        'font-size': '15',
                        'color': 'black',
                        'background': 'linear-gradient(#f2f2f2,#cfcfcf)'
                    }); //进行表头样式设置
                }
            });
            //时间
            laydate.render({
                elem: '.addvaliditystart-item' //指定元素
                ,done: function(data){
                    console.log(data)
                }
            });
            //时间
            laydate.render({
                elem: '.addvalidityend-item' //指定元素
                ,done: function(data){
                    console.log(data)
                }
            });
            //时间选择查询
            laydate.render({
                elem: '.validitystart-item' //指定元素
                ,done: function(data){
                    console.log(data)
                }
            });
            //时间选择查询
            laydate.render({
                elem: '.validityend-item' //指定元素
                ,done:function(data){
                    console.log(data)
                }
            });

            //操作 
            table.on('tool(test-table-page)', function(obj) {
                var value = obj.value //得到修改后的值
                    ,e = obj.data //得到所在行所有键值
                    ,field = obj.field; //得到字段
                    console.log(e)
                if (obj.event === 'writeoffdetails') {   //核销明细

                    sessionStorage.setItem('store_stock_id', e.stock_id);
                    sessionStorage.setItem('store_store_id', e.store_id);
                    $('.writeoffdetails').attr('lay-href', "{{url('/mb/writeoffdetailstwo?store_id=')}}" +e.store_id + "&stock_id=" + e.stock_id);


                } else if (obj.event === 'storecode'){
                    // $('.storename').html(e.store_name);
                    $.post("{{url('/api/customer/user/coupon_qr')}}",
                    {
                        token:token
                        ,store_id:e.store_id
                        ,stock_id:e.stock_id
                        ,out_request_no:e.out_request_no
                        ,coupon_belong_merchant:e.coupon_belong_merchant
                    },
                    function(res){
                        console.log(res);
                        if(res.code==1){
                            $('#code').html('');
                            $('#code').qrcode(res.data.store_pay_qr);
                            layer.open({
                                type: 1,
                                title: false,
                                closeBtn: 0,
                                skin: 'layui-layer-nobg', //没有背景色
                                shadeClose: true,
                                content: $('#edit_rate')
                            });
                        }else if(res.status==2){
                            layer.msg(res.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 3000
                            });
                        }
                    },"json");
                }else if(obj.event === 'delectdata'){

                    layer.confirm("删除后不可恢复，请确认后删除", { title: "删除确认",icon:2 }, function (index) {
                        $.post("/api/merchant/delUserStoreCoupon",{  
                            token:token,
                            store_id:store_id,
                            id:e.id
                        },function(data){  
                            
                            if(data.code == 200){
                                table.reload('test-table-page');
                                layer.msg("删除成功！！！", {icon:1, shade:0.5, time:1000});
                            }else{
                                layer.msg(data.msg, {icon:2, shade:0.5, time:1000});
                            }
                            
                        })
                        layer.close(index);
                    }); 

                }
            });
            //新增商品信息
            form.on("submit(submitNewInfo)", function(data) {

                var requestData = data.field;
                // if(Number(requestData.max_coupons) < Number(requestData.max_coupons_per_user)){
                //     layer.msg("可领数量必须小于发券张数", {offset: '50px',icon: 2,time: 2000});
                //     return false;
                // }else 
                if(Number(requestData.max_amount) < Number(requestData.discount_amount)){
                    layer.msg("总预算必须大于优惠金额", {offset: '50px',icon: 2,time: 2000});
                    return false;
                }else{
                    var alertObj = layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                    $.post("/api/customer/user/createWeChatPayStoreCoupon", {
                        available_begin_time: requestData.available_begin_time, //开始时间
                        available_end_time: requestData.available_end_time,     //结束时间
                        coupon_stock_name: requestData.addvouchername_add,      //商户简称
                        discount_amount: requestData.discount_amount,           //可享优惠
                        max_amount: requestData.max_amount,                      //总预算
                        max_coupons: requestData.max_coupons,                   //发券张数
                        // store_id: "20211517402863632",                       //门店id
                        store_id:store_id,
                        transaction_minimum: requestData.transaction_minimum,   //顾客消费满
                        coupon_source: 1,     
                        coupon_type: 1,
                        use_method: 1
                    }, function(data) {
                        console.log(data)
                        layer.close(alertObj);
                        $("#submitAppletsNameInfo").hide();
                        var status = data.status;  
                        if (status == 200) {
                            layer.msg("添加成功", {
                                offset: '50px',
                                icon: 1,
                                time: 2000
                            }, function() {
                                window.location.reload();   
                            });
                        } else {
                            layer.msg(data.message, {icon:2, time:1000});
                        }
                    }, "json");
                }
                return false;
            });
            /*
             *查询
             */
            form.on('submit(LAY-app-contlist-search)', function(data) {
                let objfield = data.field;
                var objset = {};
                if(objfield.effectivetimeone != "" && objfield.effectivetimetwo == ""){
                    objset.available_begin_time = objfield.effectivetimeone;
                }else if(objfield.effectivetimetwo != "" && objfield.effectivetimeone == ""){
                    objset.available_end_time = objfield.effectivetimetwo;
                }else if(objfield.effectivetimeone != "" && objfield.effectivetimetwo != ""){
                    objset.available_begin_time = objfield.effectivetimeone;
                    objset.available_end_time = objfield.effectivetimetwo;
                }
                objset.token = token;
                objset.store_id = store_id;
                if(objfield.effectivetimeone != "" || objfield.effectivetimetwo != ""){
                    //执行重载
                    table.reload('test-table-page', {    
                        where: objset
                    });
                }else{
                    layer.msg("请选择时间", {icon:2, shade:0.5, time:1000});
                }
            });

        });
    </script>
</body>

</html>