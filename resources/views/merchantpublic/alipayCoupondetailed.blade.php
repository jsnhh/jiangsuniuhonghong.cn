<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>微信代金券核销明细</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
  <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
  <link rel="stylesheet" href="{{asset('/layuiadmin/layui/inputTags/inputTags.css')}}">
  <style>
  layui-table-cell
    .layui-table-cell{
      height: 85px;
      line-height: 85px;
    }
    .layui-table-header .layui-table-cell {
            height: 30px;
            line-height: 30px;
    }
    .layui-table-view{
        margin-top: 30px;
    }
    .edit{background-color: #ed9c3a;}
    .shenhe{background-color: #429488;}
    .see{background-color: #7cb717;}
    .tongbu{background-color: #4c9ef8;color:#fff;}
    .cur{color:#009688;}
    .yname{font-size: 13px;color: #444;}
    .xgrate{color: #fff;font-size: 15px;padding: 7px;height: 30px;line-height: 30px;background-color: #3475c3;}
    .input_block{
      display: block;
    }
  </style>
</head>
<body>
  <div class="layui-fluid" style="margin-top:50px;">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">

        <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
              <div class="layui-card">
                <div class="layui-card-header">微信代金券核销明细</div>

                <div class="layui-card-body">
                  <div class="layui-btn-container" style="font-size:14px;">

                    <!-- 缴费时间 -->
                    <div class="layui-form">
                          <!-- 选择业务员 -->
                          <div class="layui-form" lay-filter="component-form-group" style="margin-right:10px;display: inline-block;display:flex;flex-wrap:wrap;">
                            <!-- <div class="layui-form-item">
                                <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                    <text class="yname">使用状态</text>
                                    <select name="channeltype" id="channeltype" lay-filter="channeltype" lay-search></select>
                                </div>
                            </div> -->
                            <div class="layui-form-item">
                                <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                    <text class="yname">代金券名称</text>
                                    <input type="text" name="vouchertype" placeholder="请输代金券名称" autocomplete="off" class="layui-input vouchertype">
                                </div>
                            </div>
                                <!-- <div class="layui-form-item">
                                    <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                        <text class="yname">会员名称</text>
                                        <select name="distributionstatus" id="distributionstatus" lay-filter="distributionstatus" lay-search></select>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                        <text class="yname">会员卡号</text>
                                        <select name="vouchername" id="vouchername" lay-filter="vouchername" lay-search></select>
                                    </div>
                                </div> -->

                                    <!-- <div class="layui-form-item"> -->
                                    <!-- <div class="layui-input-inline" style="margin-left:10px;width:300px;margin-right:0px;">
                                    <text class="yname">领取开始时间</text>
                                        <input type="text" class="layui-input validitystart-item test-item" placeholder="券有效期开始时间" lay-key="23">
                                    </div>
                                    </div>
                                    <div class="layui-form-item">
                                    <div class="layui-input-inline" style="margin-left:10px;width:300px;margin-right:0px;">
                                        <text class="yname">领取结束时间</text>
                                        <input type="text" class="layui-input validityend-item test-item" placeholder="券有效期结束时间" lay-key="24">
                                    </div>
                                    </div> -->
                                    <div class="layui-form-item">
                                        <div class="layui-input-inline" style="margin-left:10px;width:300px;margin-right:0px;">
                                            <text class="yname">核销开始时间</text>
                                            <input type="text" class="layui-input start-item test-item" placeholder="券创建开始时间" lay-key="23">
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <div class="layui-input-inline" style="margin-left:10px;width:300px;margin-right:0px;">
                                            <text class="yname">核销结束时间</text>
                                            <input type="text" class="layui-input end-item test-item" placeholder="券创建结束时间" lay-key="24">
                                        </div>
                                    </div>
                                    <div style="margin-left:10px;">
                                        <div class="layui-inline">
                                            <button class="layui-btn layuiadmin-btn-list" lay-submit="" lay-filter="LAY-app-contlist-search" style="border-radius:5px;margin-top:23px;">
                                                <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                            </button>
                                        </div>
                                    </div>
                        </div>

                        </div>
                    </div>
                    <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                        <!-- <div class="layui-inline"style="margin-right:0">
                          <div class="layui-input-inline">
                          <text class="yname">请输入商品</text>
                            <input type="text" class="layui-input end-item test-item" placeholder="请输入商品名称" lay-key="24">
                          </div>
                        </div> -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/html" id="company_type">
       @{{#  if(d.status =='1'){ }}
        待使用
        @{{#  } else if(d.status =='2'){ }}
        可用
        @{{#  } else if(d.status =='3'){ }}
        不可用
        @{{#  } else if(d.status =='4'){ }}
        已使用
        @{{#  } else if(d.status =='5'){ }}
        已过期
        @{{#  } else if(d.status =='6'){ }}
        未激活
        @{{#  } }}
    </script>
  <input type="hidden" class="classname">
  <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
  <script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script>
  <script src="{{asset('/layuiadmin/layui/inputTags/inputTags.js')}}"></script>
  <script>
    var token = sessionStorage.getItem("Publictoken");
    var str=location.search;
    var store_id=sessionStorage.getItem("store_id");;

    layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate',"upload"], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate
            ,upload = layui.upload;

        // 未登录,跳转登录页面
        $(document).ready(function(){
          if(token==null){
              window.location.href="{{url('/mb/login')}}";
          }
        });

           // 渲染表格
           table.render({
                elem: '#test-table-page'
                ,url: "{{url('/api/alipayopen/voucherList')}}"
                ,method: 'post'
                ,where:{
                    token:token,
                    storeId:store_id
                }
                ,request:{
                    pageName: 'p',
                    limitName: 'l'
                }
                ,page: true
                ,cellMinWidth: 150
                ,cols: [[
                    {align:'center',field:'brand_name', title: '代金券名称'}
                    ,{align:'center',field:'status',  title: '代金券状态',toolbar:'#company_type'}
                    ,{align:'center',field:'created_at', title: '领券时间'}
                    ,{align:'center',field:'use_time',  title: '最近核销时间'}
                    ,{align:'center',field:'amount',  title: '优惠金额'}
                    ,{align:'center',field:'out_biz_no',  title: '批次ID'}
                    ,{align:'center',field:'floor_amount',  title: '门槛'}

                ]]
                ,response: {
                    statusName: 'status' //数据状态的字段名称，默认：code
                    ,statusCode: 1 //成功的状态码，默认：0
                    ,msgName: 'message' //状态信息的字段名称，默认：msg
                    ,countName: 't' //数据总数的字段名称，默认：count
                    ,dataName: 'data' //数据列表的字段名称，默认：data
                }
                ,done: function(res, curr, count) {
                    // console.log(res)
                    $('th').css({'font-weight': 'bold','font-size': '15','color': 'black','background': 'linear-gradient(#f2f2f2,#cfcfcf)'}); //进行表头样式设置
                }
            });
            /*
             *时间控件
             */
            laydate.render({
                elem: '.start-item',
                type: 'datetime',
                trigger: 'click',
                done: function(value) {
                    //执行重载
                    table.reload('test-table-page', {
                        where: {
                            time_start: value,
                            time_end: $('.end-item').val()
                        },
                        page: {
                            curr: 1 //重新从第 1 页开始
                        }
                    });
                }
            });
            /*
             *时间控件
             */
            laydate.render({
                elem: '.end-item',
                type: 'datetime',
                trigger: 'click',
                done: function(value) {
                    //执行重载
                    table.reload('test-table-page', {
                        where: {
                            time_start: $('.start-item').val(),
                            time_end: value
                        },
                        page: {
                            curr: 1 //重新从第 1 页开始
                        }
                    });
                }
            });

            /*
             * 搜索
             */
            form.on('submit(LAY-app-contlist-search)', function(data){
                var vouchertype = data.field.vouchertype;
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        brand_name:vouchertype,
                    }
                });
            });

});

  </script>
</body>
</html>







