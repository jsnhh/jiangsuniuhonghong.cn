<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>微信代金券</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/inputTags/inputTags.css')}}">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .tongbu {
            background-color: #4c9ef8;
            color: #fff;
        }

        .cur {
            color: #009688;
        }

        .yname {
            font-size: 13px;
            color: #444;
        }

        .xgrate {
            color: #fff;
            font-size: 15px;
            padding: 7px;
            height: 30px;
            line-height: 30px;
            background-color: #3475c3;
        }

        .input_block {
            display: block;
        }
    </style>
</head>

<body>
    <div class="layui-fluid" style="margin-top:50px;">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">

                <div class="layui-fluid">
                    <div class="layui-row layui-col-space15">
                        <div class="layui-col-md12">
                            <div class="layui-card">
                                <div class="layui-card-header">微信代金券</div>

                                <div class="layui-card-body">
                                    <div class="layui-btn-container" style="font-size:14px;">

                                        <!-- 缴费时间 -->
                                        <div class="layui-form" style="width:100%;display:flex;">
                                            <!-- 选择业务员 -->
                                            <div class="layui-form" lay-filter="component-form-group" style="margin-right:10px;display: inline-block;display:flex;width:1375px;flex-wrap:wrap;">
                                                <div class="layui-form-item">
                                                    <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">通道类型</text>
                                                        <select name="channeltype" id="channeltype" lay-filter="channeltype" lay-search></select>
                                                    </div>
                                                </div>
                                                <div class="layui-form-item">
                                                    <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">代金券类型</text>
                                                        <select name="vouchertype" id="vouchertype" lay-filter="vouchertype" lay-search></select>
                                                    </div>
                                                </div>
                                                <div class="layui-form-item">
                                                    <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">发放状态</text>
                                                        <select name="distributionstatus" id="distributionstatus" lay-filter="distributionstatus" lay-search></select>
                                                    </div>
                                                </div>
                                                <div class="layui-form-item">
                                                    <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">代金券名称</text>
                                                        <select name="vouchername" id="vouchername" lay-filter="vouchername" lay-search></select>
                                                    </div>
                                                </div>

                                                <div class="layui-form-item">
                                                    <div class="layui-input-inline" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">券有效期开始时间</text>
                                                        <input type="text" class="layui-input validitystart-item test-item" placeholder="券有效期开始时间" lay-key="23">
                                                    </div>
                                                </div>
                                                <div class="layui-form-item">
                                                    <div class="layui-input-inline" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">券有效期结束时间</text>
                                                        <input type="text" class="layui-input validityend-item test-item" placeholder="券有效期结束时间" lay-key="24">
                                                    </div>
                                                </div>
                                                <div class="layui-form-item">
                                                    <div class="layui-input-inline" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">券创建开始时间</text>
                                                        <input type="text" class="layui-input establishstart-item test-item"  placeholder="券创建开始时间" lay-key="25">
                                                    </div>
                                                </div>
                                                <div class="layui-form-item">
                                                    <div class="layui-input-inline" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">券创建结束时间</text>
                                                        <input type="text" class="layui-input establishend-item test-item" placeholder="券创建结束时间" lay-key="26">
                                                    </div>
                                                </div>
                                                <div style="display:flex;align-items: center;margin-left:10px;">
                                                    <div class="layui-inline">
                                                        <button class="layui-btn layuiadmin-btn-list" lay-submit=""  lay-filter="LAY-app-contlist-search"  style="border-radius:5px;height:36px;line-height: 36px;">
                                                            <i  class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                                        </button>
                                                    </div>
{{--                                                    <div class="layui-inline" style="width:300px;margin-right:0px;margin-top:10px;margin-bottom:10px">--}}
{{--                                                        <button class="layui-btn layuiadmin-btn-list" id="addTable" style="border-radius:5px;height:36px;line-height: 36px;">--}}
{{--                                                            <i class="layui-icon">&#xe608;</i>新增--}}
{{--                                                        </button>--}}
{{--                                                    </div>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 判断状态 -->
    <script type="text/html" id="paymoney">
        @{{ d.rate }}%
    </script>
    <script type="text/html" id="table-content-list">
        <a class="layui-btn  layui-btn-xs delete" lay-event="delete" style="margin-right:3.5px;border-radius:3.5px">删除</a>
        <a class="layui-btn  layui-btn-xs shenhe" lay-event="shenhe" style="margin-right:3.5px;border-radius:3.5px">审核操作</a>
    </script>

    <div id="edit_rate" class="hide" style="display: none;background-color: #eee;">
        <div class="xgrate">二维码（微信扫码）</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form">
                <div class="layui-form-item">
                    <div id="code">

                    </div>
                    <div style="display:flex;margin-left: 20%;">
                        <div style="text-align: center;" class="storename"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- </div>--}}
    <input type="hidden" class="classname">

    <script type="text/html" id="mb_money_template">
        @{{ d.mb_money }} / @{{ d.mb_virtual_money }}
    </script>
    <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
    <script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script>
    <script src="{{asset('/layuiadmin/layui/inputTags/inputTags.js')}}"></script>
    <!-- <script src="{{asset('/layuiadmin/layui/jq.js')}}"></script> -->
    <script src="{{asset('/layuiadmin/layui/jquery.qrcode.min.js')}}"></script>
    <script>
        var token = sessionStorage.getItem("Usertoken");
        var str = location.search;
        var store_id = sessionStorage.getItem("store_id");

        layui.config({
            base: '../../layuiadmin/' //静态资源所在路径
        }).extend({
            index: 'lib/index' //主入口模块
        }).use(['index', 'form', 'table', 'laydate', "upload"], function() {
            var $ = layui.$,
                admin = layui.admin,
                form = layui.form,
                table = layui.table,
                laydate = layui.laydate,
                upload = layui.upload;
            // 打开新增窗口
            $('#addTable').on("click", function() {
                layer.open({
                    type: 1,
                    title: false,
                    closeBtn: 0,
                    area: '516px',
                    skin: 'layui-layer-nobg', //没有背景色
                    shadeClose: true,
                    content: $('#submitAppletsNameInfo')
                });
            });
            //关闭新增弹窗
            $(".close").on("click", function() {
                $("#submitAppletsNameInfo").hide();
                document.location.reload();
            })

            // 未登录,跳转登录页面
            $(document).ready(function() {
                if (token == null) {
                    window.location.href = "{{url('/user/login')}}";
                }
            });

            // 选择通道
            // $.ajax({
            //     url : "{{url('/api/user/store_open_pay_way_lists')}}",
            //     data : {token:token,l:100},
            //     type : 'post',
            //     dataType:'json',
            //     success : function(data) {
            //         console.log(data);
            //         var optionStr = "";
            //             for(var i=0;i<data.data.length;i++){
            //                 optionStr += "<option value='" + data.data[i].company + "'>"
            //                     + data.data[i].company_desc + "</option>";
            //             }
            //             $("#channeltype").append('<option value="">选择通道类型</option>'+optionStr);
            //             layui.form.render('select');
            //     },
            //     error : function(data) {
            //         alert('查找板块报错');
            //     }
            // });

            // 选择门店
            {{--$.ajax({--}}
            {{--    url: "{{url('/api/merchant/store_lists')}}",--}}
            {{--    data: {--}}
            {{--        token: token,--}}
            {{--        l: 100--}}
            {{--    },--}}
            {{--    type: 'post',--}}
            {{--    success: function(data) {--}}
            {{--        console.log(data.data[0].is_send_tpl_mess)--}}
            {{--        if (data.data[0].is_send_tpl_mess == 2) {}--}}
            {{--        $('.store_id').val(data.data[0].store_id);--}}
            {{--        var optionStr = "";--}}
            {{--        for (var i = 0; i < data.data.length; i++) {--}}
            {{--            optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i]--}}
            {{--                .store_name + "</option>";--}}
            {{--            // optionStr += "<option value='" + data.data[i].store_id + "' "+((store_id==data.data[i].store_id)?"selected":"")+">" + data.data[i].store_name + "</option>";--}}
            {{--        }--}}
            {{--        $("#agent").append(optionStr);--}}
            {{--        layui.form.render('select');--}}
            {{--    },--}}
            {{--    error: function(data) {--}}
            {{--        alert('查找板块报错');--}}
            {{--    }--}}
            {{--});--}}
            // 渲染表格
            table.render({
                elem: '#test-table-page',
                url: "{{url('/api/user/wx_cash_coupon_list')}}",
                method: 'post',
                where: {
                    token: token,
                    store_id: store_id
                },
                request: {
                    pageName: 'p',
                    limitName: 'l'
                },
                page: true,
                cellMinWidth: 150,
                cols: [
                    [{
                        align: 'center',
                        field: 'store_name',
                        title: '门店名称'
                    }, {
                        align: 'center',
                        field: 'consume_mchid',
                        title: '商户号'
                    }, {
                        align: 'center',
                        field: 'coupon_stock_name',
                        title: '代金券名称'
                    }, {
                        align: 'center',
                        field: 'coupon_stock_type_name',
                        title: '代金券类型'
                    }, {
                        align: 'center',
                        field: 'channel',
                        title: '通道'
                    }, {
                        align: 'center',
                        field: 'coupon_status',
                        title: '发放状态',
                        edit: 'text'
                    }, {
                        align: 'center',
                        field: 'transaction_minimum',
                        title: '门槛',
                        edit: 'text'
                    }, {
                        align: 'center',
                        field: 'discount_amount',
                        title: '优惠金额',
                        edit: 'text'
                    }, {
                        align: 'center',
                        field: 'available_begin_time',
                        title: '有效期开始时间'
                    }, {
                        align: 'center',
                        field: 'available_end_time',
                        title: '有效期结束时间'
                    }, {
                        align: 'center',
                        field: 'created_at',
                        title: '创建时间'
                    }, {
                        align: 'center',
                        field: 'max_coupons',
                        title: '可发放数量'
                    }, {
                        align: 'center',
                        field: 'used_num',
                        title: '核销数量'
                    }, {
                        align: 'center',
                        field: 'used_percent',
                        title: '已核销比例'
                    }, {
                        align: 'center',
                        fixed: 'right',
                        toolbar: '#table-content-list',
                        title: '操作'
                    }]
                ],
                response: {
                    statusName: 'status' //数据状态的字段名称，默认：code
                        ,
                    statusCode: 1 //成功的状态码，默认：0
                        ,
                    msgName: 'message' //状态信息的字段名称，默认：msg
                        ,
                    countName: 't' //数据总数的字段名称，默认：count
                        ,
                    dataName: 'data' //数据列表的字段名称，默认：data
                },
                done: function(res, curr, count) {
                    $('th').css({
                        'font-weight': 'bold',
                        'font-size': '15',
                        'color': 'black',
                        'background': 'linear-gradient(#f2f2f2,#cfcfcf)'
                    }); //进行表头样式设置
                }
            });

            // 选择通道类型
            form.on('select(passway)', function(data) {
                var store_id = data.value;
                $('.company_id').val(store_id);
                //执行重载
                acount()
            });

            laydate.render({
                elem: '.validitystart-item',
                type: 'datetime',
                trigger: 'click',
                done: function(value) {
                    //执行重载
                    table.reload('test-table-page', {
                        where: {
                            time_start: value,
                            time_end: $('.validityend-item').val()
                        },
                        page: {
                            curr: 1 //重新从第 1 页开始
                        }
                    });
                }
            });

            laydate.render({
                elem: '.validityend-item',
                type: 'datetime',
                trigger: 'click',
                done: function(value) {
                    //执行重载
                    table.reload('test-table-page', {
                        where: {
                            time_start: $('.validitystart-item').val(),
                            time_end: value
                        },
                        page: {
                            curr: 1 //重新从第 1 页开始
                        }
                    });
                }
            });

            laydate.render({
                elem: '.establishstart-item',
                type: 'datetime',
                trigger: 'click',
                done: function(value) {
                    //执行重载
                    table.reload('test-table-page', {
                        where: {
                            time_start: value,
                            time_end: $('.establishend-item').val()
                        },
                        page: {
                            curr: 1 //重新从第 1 页开始
                        }
                    });
                }
            });

            laydate.render({
                elem: '.establishend-item',
                type: 'datetime',
                trigger: 'click',
                done: function(value) {
                    //执行重载
                    table.reload('test-table-page', {
                        where: {
                            time_start: $('.establishstart-item').val(),
                            time_end: value
                        },
                        page: {
                            curr: 1 //重新从第 1 页开始
                        }
                    });
                }
            });

            laydate.render({
                elem: '.addvaliditystart-item',
                type: 'datetime',
                trigger: 'click',
                done: function(value) {
                    //执行重载
                    table.reload('test-table-page', {
                        where: {
                            time_start: value,
                            time_end: $('.addvalidityend-item').val()
                        },
                        page: {
                            curr: 1 //重新从第 1 页开始
                        }
                    });
                }
            });

            laydate.render({
                elem: '.addvalidityend-item',
                type: 'datetime',
                trigger: 'click',
                done: function(value) {
                    //执行重载
                    table.reload('test-table-page', {
                        where: {
                            time_start: $('.addvaliditystart-item').val(),
                            time_end: value
                        },
                        page: {
                            curr: 1 //重新从第 1 页开始
                        }
                    });
                }
            });


            table.on('tool(test-table-page)', function(obj) {
                var value = obj.value //得到修改后的值
                    ,e = obj.data //得到所在行所有键值
                    ,field = obj.field; //得到字段
                var data = obj.data;
                if (obj.event === 'writeoffdetails') {
                    sessionStorage.setItem('store_stock_id', e.stock_id);
                    sessionStorage.setItem('store_store_id', e.store_id);
                    $('.writeoffdetails').attr('lay-href', "{{url('/mb/writeoffdetails?store_id=')}}" +e.store_id + "&stock_id=" + e.stock_id);
                } else if (obj.event === 'shenhe') {
                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#edit_shenhe')
                    });

                    form.on('submit(submitEditTwo)',function(message){
                        $.ajax({
                            url: "{{url('/api/user/check_wechat_coupon')}}",
                            type: "POST",
                            data:{
                                token:token,
                                id: data.id,
                                status: $('input[name="isPass"]:checked').val(),
                            },
                            success: function(res){
                                if(res.status == 1){
                                    layer.msg('审核成功',{icon:1},function(){
                                        window.location.reload();
                                    });
                                }else{
                                    layer.msg('审核失败',{icon:1},function(){
                                        window.location.reload();
                                    });
                                }
                            }
                        })
                        // 防止页面直接刷新不提示
                        return false;
                    })
                } else if (obj.event === 'delete') {
                    layer.confirm('确认删除吗？', function(){
                        $.ajax({
                            type: 'POST',
                            url: "{{url('/api/user/delete_wechat_coupon')}}",
                            data: {
                                token:token,
                                id: data.id,
                            },
                            success:function(res){
                                if(res.status == 1){
                                    layer.msg('删除成功',{icon:1},function(){
                                        window.location.reload();
                                    });
                                }else{
                                    layer.msg('删除失败: ' + res.msg,{icon:1},function(){
                                        window.location.reload();
                                    });
                                }
                            }
                        })
                    });
                }
            });

            //新增商品信息
            form.on("submit(submitNewInfo)", function(data) {
                var requestData = data.field;
                layer.msg('正在请求，请稍后......', {
                    icon: 16,
                    shade: 0.5,
                    time: 0
                });
                $.post("/api/wechat/create_merchant_coupon", {
                    store_id: store_id,
                    token: token,
                    coupon_stock_name: requestData.addvouchername_add,
                    transaction_minimum: requestData.transaction_minimum,
                    discount_amount: requestData.discount_amount,
                    available_begin_time: requestData.available_begin_time,
                    available_end_time: requestData.available_end_time,
                    max_coupons: requestData.max_coupons,
                    max_coupons_per_user: requestData.max_coupons_per_user,
                }, function(data) {
                    $("#submitAppletsNameInfo").hide();
                    var status = data.status;
                    if (status == 1) {
                        layer.msg(data.message, {
                            offset: '50px',
                            icon: 1,
                            time: 2000
                        }, function() {
                            window.location.reload();
                        });
                    } else {
                        layer.msg(data.message, {
                            offset: '50px',
                            icon: 2,
                            time: 2000
                        });
                    }
                }, "json");
                return false;
            });

            /*
             *查询
             */
            form.on('submit(LAY-app-contlist-search)', function(data) {
                var paytradeno = data.field.paytradeno;
                var out_trade_no = data.field.tradeno;
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        out_trade_no: out_trade_no,
                        trade_no: paytradeno
                    }
                });
            });

        });
    </script>

    <div id="edit_shenhe" class="hide" style="display: none;background-color: #fff;">
        <div class="xgrate">审核操作</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form">
                <div class="layui-form-item" pane="">
                    <label class="layui-form-label">审核状态</label>
                    <div class="layui-input-block pass">
                        <input type="radio" id="pass" name="isPass" value="2" title="通过" checked="">
                        <input type="radio" id="noPass" name="isPass" value="3" title="不通过">
                    </div>
                </div>
            </div>
{{--            <div class="layui-form">--}}
{{--                <div class="layui-form-item" pane="">--}}
{{--                    <label class="layui-form-label">审核说明</label>--}}
{{--                    <div class="layui-input-block">--}}
{{--                        <textarea name="desc" placeholder="请输入内容" class="layui-textarea textarea"></textarea>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <div class="layui-footer" style="left: 0;">
{{--                        <button class="layui-btn closestore"style="border-radius:5px">确定</button>--}}
                        <button class="layui-btn submitEdit" lay-submit lay-filter="submitEditTwo" style="border-radius:5px">确定</button>
                    </div>
                </div>
            </div>
            <input type="hidden" class="shenhe_store">
        </div>
    </div>

</body>

</html>