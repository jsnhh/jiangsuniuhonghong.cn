<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>支付宝无金额代金券</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/inputTags/inputTags.css')}}">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/formSelects-v4.css')}}" media="all">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #4e7af5;
        }

        .see {
            background-color: #7cb717;
        }

        .tongbu {
            background-color: #4c9ef8;
            color: #fff;
        }

        .cur {
            color: #009688;
        }

        .yname {
            font-size: 13px;
            color: #444;
        }

        .xgrate {
            color: #fff;
            font-size: 15px;
            padding: 7px;
            height: 30px;
            line-height: 30px;
            background-color: #3475c3;
        }

        .input_block {
            display: block;
        }
    </style>
</head>

<body>
<div class="layui-fluid" style="margin-top:50px;">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">

            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12">
                        <div class="layui-card">
                            <div class="layui-card-header">支付宝无金额代金券</div>

                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">

                                    <!-- 缴费时间 -->
                                    <div class="layui-form" style="width:100%;display:flex;">
                                        <!-- 选择业务员 -->
                                        <div class="layui-form" lay-filter="component-form-group" style="margin-right:10px;display: inline-block;display:flex;width:1375px;flex-wrap:wrap;">
                                            <div class="layui-form-item">
                                                <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                                    <text class="yname">通道类型</text>
                                                    <select name="channeltype" id="channeltype" lay-filter="channeltype" lay-search></select>
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                                    <text class="yname">代金券类型</text>
                                                    <select name="vouchertype" id="vouchertype" lay-filter="vouchertype" lay-search></select>
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                                    <text class="yname">发放状态</text>
                                                    <select name="distributionstatus" id="distributionstatus" lay-filter="distributionstatus" lay-search></select>
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                                    <text class="yname">代金券名称</text>
                                                    <select name="vouchername" id="vouchername" lay-filter="vouchername" lay-search></select>
                                                </div>
                                            </div>

                                            <div class="layui-form-item">
                                                <div class="layui-input-inline" style="margin-left:10px;width:300px;margin-right:0px;">
                                                    <text class="yname">券有效期开始时间</text>
                                                    <input type="text" class="layui-input validitystart-item test-item" placeholder="券有效期开始时间" lay-key="23">
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <div class="layui-input-inline" style="margin-left:10px;width:300px;margin-right:0px;">
                                                    <text class="yname">券有效期结束时间</text>
                                                    <input type="text" class="layui-input validityend-item test-item" placeholder="券有效期结束时间" lay-key="24">
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <div class="layui-input-inline" style="margin-left:10px;width:300px;margin-right:0px;">
                                                    <text class="yname">券创建开始时间</text>
                                                    <input type="text" class="layui-input establishstart-item test-item"  placeholder="券创建开始时间" lay-key="25">
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <div class="layui-input-inline" style="margin-left:10px;width:300px;margin-right:0px;">
                                                    <text class="yname">券创建结束时间</text>
                                                    <input type="text" class="layui-input establishend-item test-item" placeholder="券创建结束时间" lay-key="26">
                                                </div>
                                            </div>
{{--                                            <div style="display:flex;align-items: center;margin-left:10px;">--}}
{{--                                                <div class="layui-inline">--}}
{{--                                                    <button class="layui-btn layuiadmin-btn-list" lay-submit=""  lay-filter="LAY-app-contlist-search"  style="border-radius:5px;height:36px;line-height: 36px;">--}}
{{--                                                        <i  class="layui-icon layui-icon-search layuiadmin-button-btn"></i>--}}
{{--                                                    </button>--}}
{{--                                                </div>--}}
{{--                                                <div class="layui-inline" style="width:300px;margin-right:0px;margin-top:10px;margin-bottom:10px">--}}
{{--                                                    <button class="layui-btn layuiadmin-btn-list" id="addTable" style="border-radius:5px;height:36px;line-height: 36px;">--}}
{{--                                                        <i class="layui-icon">&#xe608;</i>新增--}}
{{--                                                    </button>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
                                        </div>
                                    </div>
                                </div>
                                <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/html" id="company_type">
    @{{#  if(d.voucher_type =='1'){ }}
    满减券
    @{{#  } else { }}
    未知类型
    @{{#  } }}
</script>
<script type="text/html" id="company_status">
    @{{#  if(d.status =='1'){ }}
    审核通过
    @{{#  } else if(d.status =='2'){ }}
    审核中
    @{{#  } else if(d.status =='3'){ }}
    不通过
    @{{#  } else { }}
    未知类型
    @{{#  } }}
</script>
{{--<script type="text/html" id="company_del_status">--}}
{{--    @{{#  if(d.del_status =='1'){ }}--}}
{{--    使用中--}}
{{--    @{{#  } else if(d.del_status =='2'){ }}--}}
{{--    已删除--}}
{{--    @{{#  } else { }}--}}
{{--    未知类型--}}
{{--    @{{#  } }}--}}
{{--</script>--}}
<script type="text/html" id="table-content-list">
    <a class="layui-btn  layui-btn-xs shenhe" lay-event="shenhe" style="margin-right:3.5px;border-radius:3.5px">审核操作</a>
    <a class="layui-btn layui-btn-normal layui-btn-xs see " lay-event="see">查看</a>
    <a class="layui-btn layui-btn-normal layui-btn-xs del " lay-event="del">删除</a>
{{--    <a class="layui-btn layui-btn-normal layui-btn-xs alipaydetails" lay-event="alipaydetails">核销详情</a>--}}
</script>
<!-- 新增 -->
<div id="submitAppletsNameInfo" class="hide layui-form" style="display: none;background-color: #fff;" lay-filter="addcommodety">
    <div class="xgrate">新增</div>
    <div class="layui-card-body" style="padding: 15px;">
        <div class="layui-form" style="padding-top: 20px;padding-bottom: 20px;" lay-filter="example">
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">代金券名称</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="请输入代金券名称(15字以内)" name="addvouchername_add" class="layui-input title" lay-verify="addvouchername" id="addvouchername">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">品牌名</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="请输入品牌名" name="brandName" class="layui-input title" lay-verify="brandName" id="brandName">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">券类型</label>
                <div class="layui-input-block">
                    <input type="radio" name="addvouchertype" value="1" title="满减券" checked="">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">顾客消费满</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="金额（元）" name="transaction_minimum" class="layui-input title" lay-verify="addfullreduction" id="addfullreduction">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">可享优惠</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="金额（元）" name="discount_amount" class="layui-input title" lay-verify="addbenefits" id="addbenefits">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">发放开始时间</label>
                <div class="layui-input-block">
                    <input type="text" class="layui-input addvaliditystart-item test-item" name="available_begin_time" placeholder="发放开始时间" lay-key="27">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">发放结束时间</label>
                <div class="layui-input-block">
                    <input type="text" class="layui-input addvalidityend-item test-item" name="available_end_time" placeholder="发放结束时间" lay-key="28">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">券有效期生效时间</label>
                <div class="layui-input-block">
                    <input type="text" class="layui-input shengstart-item test-item" name="sheng_begin_time" placeholder="有效期开始时间" lay-key="29">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">券有效期失效时间</label>
                <div class="layui-input-block">
                    <input type="text" class="layui-input shengend-item test-item" name="sheng_end_time" placeholder="有效期结束时间" lay-key="30">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">券可用时间段（开始时间）</label>
                <div class="layui-input-block">
                    <input type="text" class="layui-input shengxiaostart-item test-item" name="shengxiao_begin_time" placeholder="券可用时间段（开始时间）" lay-key="31">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">券可用时间段（结束时间）</label>
                <div class="layui-input-block">
                    <input type="text" class="layui-input shengxiaoend-item test-item" name="shengxiao_end_time" placeholder="券可用时间段（结束时间）" lay-key="32">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">券可用时间段（周几可用）</label>
                <div class="layui-input-block">
                    <select name="range" id="range" xm-select="range" xm-select-search="">
                        <option value=""></option>
                        <option value="1">星期一</option>
                        <option value="2">星期二</option>
                        <option value="3">星期三</option>
                        <option value="4">星期四</option>
                        <option value="5">星期五</option>
                        <option value="6">星期六</option>
                        <option value="7">星期日</option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">使用说明</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="请输入使用说明" name="explain" class="layui-input title" lay-verify="explain" id="explain">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">发券张数</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="请输入发券张数" name="max_coupons" class="layui-input title" lay-verify="addcouponsissued" id="addcouponsissued">
                </div>
            </div>
            <!-- <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">面额</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="请输入面额" name="denomination" class="layui-input title" lay-verify="denomination" id="denomination">
                </div>
            </div> -->
            <!-- <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">门槛</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="请输入最低金额" name="max_coupons_per_user" class="layui-input title" lay-verify="addsingleperson" id="addsingleperson">
                </div>
            </div> -->
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <div class="layui-footer" style="float: right;">
                    <button class="layui-btn close" style="border-radius:5px;background-color: #FFB800;">关闭</button>
                    <button type="submit" class="layui-btn submitNewInfo" lay-submit="" lay-filter="submitNewInfo" style="border-radius:5px">确定</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="edit_rate" class="hide" style="display: none;background-color: #eee;">
    <div class="xgrate">二维码（支付宝扫码）</div>
    <div class="layui-card-body" style="padding: 15px;">
        <div class="layui-form">
            <div class="layui-form-item">
                <div id="code">

                </div>
                <div style="display:flex;margin-left: 20%;">
                    <div style="text-align: center;" class="storename"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="edit_shenhe" class="hide" style="display: none;background-color: #fff;">
    <div class="xgrate">审核操作</div>
    <div class="layui-card-body" style="padding: 15px;">
        <div class="layui-form">
            <div class="layui-form-item" pane="">
                <label class="layui-form-label">审核状态</label>
                <div class="layui-input-block pass">
                    <input type="radio" name="sex" value="1" title="通过" checked="">
                    <input type="radio" name="sex" value="3" title="不通过">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <div class="layui-footer" style="left: 0;">
                    <button class="layui-btn closestore"style="border-radius:5px">确定</button>
                </div>
            </div>
        </div>
        <input type="hidden" class="shenhe_store">
        <input type="hidden" class="shenhe_id">
    </div>
</div>
{{-- </div>--}}
<input type="hidden" class="classname">


<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script>
<script src="{{asset('/layuiadmin/layui/inputTags/inputTags.js')}}"></script>
<script src="{{asset('/layuiadmin/layui/jquery.qrcode.min.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var str = location.search;
    var store_id = sessionStorage.getItem("store_id");
    console.log(store_id)

    layui.config({
        base: '../../layuiadmin/', //静态资源所在路径
    }).extend({
        index: 'lib/index',//主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form', 'table', 'laydate', "upload",'formSelects'], function() {
        var $ = layui.$,
            admin = layui.admin,
            form = layui.form,
            table = layui.table,
            laydate = layui.laydate,
            upload = layui.upload,
            formSelects = layui.formSelects;
        formSelects.render('range');
        formSelects.btns('range', []);
        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });
        // 打开新增窗口
        $('#addTable').on("click", function() {
            layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area:["650px","600px"],
                skin: 'layui-layer-nobg', //没有背景色
                shadeClose: true,
                content: $('#submitAppletsNameInfo')
            });
        });
        //关闭新增弹窗
        $(".close").on("click", function() {
            $("#submitAppletsNameInfo").hide();
            document.location.reload();
        })

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        })

        // 渲染表格
        table.render({
            elem: '#test-table-page',
            url: "{{url('/api/alipayopen/CashlessUserList')}}",
            method: 'post',
            where: {
                token: token,
            },
            request: {
                pageName: 'p',
                limitName: 'l'
            },
            page: true,
            cellMinWidth: 150,
            cols: [
                [
                    {align: 'center',field: 'activity_name',title: '代金券活动名称'},
                    {align: 'center',field: 'brand_name',title: '代金券品牌'},
                    {align: 'center',field: 'voucher_type',title: '代金券类型',toolbar:'#company_type'},
                    {align: 'center',field: 'status',title: '券审核状态',toolbar:'#company_status'},
                    // {align: 'center',field: 'del_status',title: '券状态',toolbar:'#company_del_status'},
                    {align: 'center',field: 'voucher_start',title: '有效期开始时间'},
                    {align: 'center',field: 'voucher_end',title: '有效期结束时间'},
                    {align: 'center',field: 'floor_amount',title: '最低限额'},
                    {align: 'center',field: 'amount',title: '优惠金额'},
                    {align: 'center',field: 'day_rule',title: '时间段'},
                    {align: 'center',field: 'voucher_time_start',title: '开始时段'},
                    {align: 'center',field: 'voucher_time_end',title: '结束时段'},
                    {align: 'center',field: 'created_at',title: '创建时间'},
                    {align: 'center',field: 'voucher_quantity',title: '可发放数量'},
                    // {align: 'center',toolbar: '#table-content-list',title: '操作'}
                    {width:220,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
                ]
            ],
            response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                ,statusCode: 1 //成功的状态码，默认：0
                ,msgName: 'message' //状态信息的字段名称，默认：msg
                ,countName: 't' //数据总数的字段名称，默认：count
                ,dataName: 'data' //数据列表的字段名称，默认：data
            },
            done: function(res, curr, count) {
                $('th').css({'font-weight': 'bold','font-size': '15','color': 'black','background': 'linear-gradient(#f2f2f2,#cfcfcf)'}); //进行表头样式设置
            }
        });

        // 选择通道类型
        form.on('select(passway)', function(data) {
            var store_id = data.value;
            $('.company_id').val(store_id);
            //执行重载
            acount()
        });

        laydate.render({
            elem: '.validitystart-item',
            type: 'datetime',
            trigger: 'click',
            done: function(value) {
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: value,
                        time_end: $('.validityend-item').val()
                    },
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        laydate.render({
            elem: '.validityend-item',
            type: 'datetime',
            trigger: 'click',
            done: function(value) {
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: $('.validitystart-item').val(),
                        time_end: value
                    },
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        laydate.render({
            elem: '.establishstart-item',
            type: 'datetime',
            trigger: 'click',
            done: function(value) {
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: value,
                        time_end: $('.establishend-item').val()
                    },
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        laydate.render({
            elem: '.establishend-item',
            type: 'datetime',
            trigger: 'click',
            done: function(value) {
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: $('.establishstart-item').val(),
                        time_end: value
                    },
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        laydate.render({
            elem: '.addvaliditystart-item',
            type: 'datetime',
            trigger: 'click',
            done: function(value) {
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: value,
                        time_end: $('.addvalidityend-item').val()
                    },
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        laydate.render({
            elem: '.addvalidityend-item',
            type: 'datetime',
            trigger: 'click',
            done: function(value) {
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: $('.addvaliditystart-item').val(),
                        time_end: value
                    },
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        laydate.render({
            elem: '.shengstart-item',
            type: 'datetime',
            trigger: 'click',
            done: function(value) {
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: value,
                        time_end: $('.shengend-item').val()
                    },
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        laydate.render({
            elem: '.shengend-item',
            type: 'datetime',
            trigger: 'click',
            done: function(value) {
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: $('.shengstart-item').val(),
                        time_end: value
                    },
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        laydate.render({
            elem: '.shengxiaostart-item',
            type: 'time',
            trigger: 'click',
            done: function(value) {
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: value,
                        time_end: $('.shengxiaoend-item').val()
                    },
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        laydate.render({
            elem: '.shengxiaoend-item',
            type: 'time',
            trigger: 'click',
            done: function(value) {
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: $('.shengxiaostart-item').val(),
                        time_end: value
                    },
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });


        table.on('tool(test-table-page)', function(obj) {
            var value = obj.value //得到修改后的值
                ,e = obj.data //得到所在行所有键值
                ,field = obj.field; //得到字段
            console.log(e)
            if (obj.event === 'alipaydetails') {
                sessionStorage.setItem('store_template_id', e.template_id);
                sessionStorage.setItem('store_store_id', e.store_id);
                $('.alipaydetails').attr('lay-href', "{{url('/mb/alipaydetails?store_id=')}}" +e.store_id + "&template_id=" + e.template_id);
            }else if(obj.event === 'storecode'){
                // $('.storename').html(e.store_name);
                $.post("{{url('/api/user/couponQr')}}",
                    {
                        token:token
                        ,store_id:e.store_id
                        ,template_id:e.template_id
                    },
                    function(res){
                        console.log(res);
                        if(res.status==1){
                            $('#code').html('');
                            $('#code').qrcode(res.data.store_pay_qr);
                            layer.open({
                                type: 1,
                                title: false,
                                closeBtn: 0,
                                skin: 'layui-layer-nobg', //没有背景色
                                shadeClose: true,
                                content: $('#edit_rate')
                            });
                        }else if(res.status==2){
                            layer.msg(res.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 3000
                            });
                        }
                    },"json");
            }else if(obj.event === 'shenhe'){
                $('.shenhe_store').val(e.store_id);
                $('.shenhe_id').val(e.id);
                layer.open({
                    type: 1,
                    title: false,
                    closeBtn: 0,
                    area: '516px',
                    skin: 'layui-layer-nobg', //没有背景色
                    shadeClose: true,
                    content: $('#edit_shenhe')
                });
            }else if(obj.event === 'see'){
                layer.open({
                    type: 2,
                    title: '详细',
                    shade: false,
                    maxmin: true,
                    area: ['90%', '80%'],
                    content: "{{url('/user/cashlesssee?')}}"+e.id
                });
            }else if(obj.event === 'del'){
                layer.confirm('确认删除券?',{icon: 2}, function(index){
                    $.post("{{url('/api/alipayopen/CashlessDel')}}",
                        {
                            token:token
                            ,id:e.id
                        },function(res){
                            if(res.status==1){
                                obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                                layer.close(index);
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 1
                                    ,time: 2000
                                });
                            }else{
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 2
                                    ,time: 3000
                                });
                            }
                        },"json");

                });
            }
        });

        //新增商品信息
        form.on("submit(submitNewInfo)", function(data) {
            var requestData = data.field;
            console.log(requestData)
            layer.msg('正在请求，请稍后......', {
                icon: 16,
                shade: 0.5,
                time: 0
            });
            $.post("/api/alipayopen/createCashlessCoupon", {
                store_id: store_id,
                token: token,
                // voucher_type:requestData.addvouchertype,
                voucher_type:"CASHLESS_FIX_VOUCHER",
                activity_name: requestData.addvouchername_add,
                brand_name: requestData.brandName,
                publish_start_time:requestData.available_begin_time,
                publish_end_time:requestData.available_end_time,
                start:requestData.sheng_begin_time,
                end:requestData.sheng_end_time,
                time_begin:requestData.shengxiao_begin_time,
                time_end:requestData.shengxiao_end_time,
                description:requestData.explain,
                quantity:requestData.max_coupons,
                amount:requestData.discount_amount,
                floor_amount:requestData.transaction_minimum,
                day_rule:requestData.range,
            }, function(data) {
                var status = data.status;
                if (status == 1) {
                    layer.msg(data.message, {
                        offset: '50px',
                        icon: 1,
                        time: 2000
                    }, function() {
                        $("#submitAppletsNameInfo").hide();
                        document.location.reload();
                    });
                } else {
                    layer.msg(data.message, {
                        offset: '50px',
                        icon: 2,
                        time: 2000
                    });
                }
            }, "json");
            return false;
        });

        /*
         *查询
         */
        form.on('submit(LAY-app-contlist-search)', function(data) {
            var paytradeno = data.field.paytradeno;
            var out_trade_no = data.field.tradeno;
            console.log(data);
            //执行重载
            table.reload('test-table-page', {
                where: {
                    out_trade_no: out_trade_no,
                    trade_no: paytradeno
                }
            });
        });

    });
    $('.closestore').click(function(){

        $("input:radio[name='sex']:checked").each(function() { // 遍历name=standard选中的多选框的值

            console.log($(this).val());
            $.post("{{url('/api/alipayopen/checkStatus')}}",
                {
                    token:token,
                    store_id:$('.shenhe_store').val(),
                    status:$(this).val(),
                    id: $('.shenhe_id').val(),
                },function(res){
                    console.log(res);
                    if(res.status==1){
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 3000
                        },function(){
                            window.location.reload();
                        });

                    }else{
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 3000
                        });
                    }
                },"json");
        });
        $('#edit_shenhe').css('display', 'none');
    });
</script>
</body>

</html>