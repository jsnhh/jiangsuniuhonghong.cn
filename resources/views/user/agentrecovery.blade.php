<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>代理回收站</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
<link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
<style>
  .edit{background-color: #ed9c3a;}
  .shenhe{background-color: #429488;}
  .see{background-color: #7cb717;}
  .tongbu{background-color: #4c9ef8;color:#fff;}
  .cur{color:#009688;}
  .del {
    background-color: #e85052;
  }
  .userbox{
    height:200px;
    overflow-y: auto;
    z-index: 999;
    position: absolute;
    left: 0px;
    top: 63px;
    width:298px;
    background-color:#ffffff;
    border: 1px solid #ddd;
  }
  .userbox .list{
    height:38px;line-height: 38px;cursor:pointer;
    padding-left:10px;
  }
  .userbox .list:hover{
    background-color:#eeeeee;
  }
</style>
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">
        <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
              <div class="layui-card" style="margin-top:0px;">
                <div class="layui-card-header">代理回收站</div>
                <div class="layui-card-body">
                  <div class="layui-btn-container" style="font-size:14px;">
                    <!-- 搜索 -->
                    <div class="layui-form" lay-filter="component-form-group" style="width:600px;display: inline-block;">
                      <div class="layui-form-item">                          
                          <button class="layui-btn layuiadmin-btn-forum-list huifu" style="margin-bottom: 4px;height:36px;line-height: 36px; border-radius:5px;" data-type="batchdel">恢复</button>
                        </div>
                    </div>
                  </div>
                  
                  <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                  <!-- 判断状态 -->
                  <script type="text/html" id="statusTap">
                    @{{#  if(d.pay_status == 1){ }}
                      <span class="cur">@{{ d.pay_status_desc }}</span>
                    @{{#  } else { }}
                      @{{ d.pay_status_desc }}
                    @{{#  } }}
                  </script>
                  <!-- 判断状态 -->
                  <script type="text/html" id="table-content-list" class="layui-btn-small">
                    <a class="layui-btn layui-btn-normal layui-btn-xs del" lay-event="del">彻底删除</a>
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<input type="hidden" class="js_user_id">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
var token = sessionStorage.getItem("Usertoken");
var str=location.search;
var user_id=str.split('?')[1];

layui.config({
  base: '../../layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index','form','table','laydate'], function(){
    var $ = layui.$
        ,admin = layui.admin
        ,form = layui.form
        ,table = layui.table
        ,laydate = layui.laydate;

    // 未登录,跳转登录页面
    $(document).ready(function(){
      if(token==null){
          window.location.href="{{url('/user/login')}}";
      }
    });

    var user_name=sessionStorage.getItem('agentName');

    if(user_id == undefined){

    }else{
    $('.transfer').val(user_name)
    }

    // 渲染表格
    table.render({
        elem: '#test-table-page'
        ,url: "{{url('/api/user/get_del_sub_users')}}"
        ,method: 'post'
        ,where:{
          token:token
        }
        ,request:{
          pageName: 'p',
          limitName: 'l'
        }
        ,page: true
        ,cellMinWidth: 150
        ,cols: [[
         {type:'checkbox', fixed: 'left'}
          ,{field:'name', title: '姓名'}
          ,{field:'phone', title: '登录手机号'}
          ,{field:'level', title: '等级'}
          ,{field:'level_name',  title: '等级名称'}
          ,{width:100,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
        ]]
        ,response: {
          statusName: 'status' //数据状态的字段名称，默认：code
          ,statusCode: 1 //成功的状态码，默认：0
          ,msgName: 'message' //状态信息的字段名称，默认：msg
          ,countName: 't' //数据总数的字段名称，默认：count
          ,dataName: 'data' //数据列表的字段名称，默认：data
        }
        ,done: function(res, curr, count){
          console.log(res);
          $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});	//进行表头样式设置
        }
    });

    table.on('tool(test-table-page)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
      var e = obj.data; //获得当前行数据
      var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
      var tr = obj.tr; //获得当前行 tr 的DOM对象
//      console.log(e);
      // sessionStorage.setItem('s_store_id', e.store_id);

      if(layEvent === 'del'){ //审核
        layer.confirm('确认删除此代理吗?',{icon: 2}, function(index){
          $.post("{{url('/api/user/cdel_sub_user')}}",
          {
             token:token
              ,user_id:e.id
          },function(res){
//              console.log(res);
              if(res.status==1){
                obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                layer.close(index);
                layer.msg(res.message, {
                  offset: '50px'
                  ,icon: 1
                  ,time: 2000
                });
              }else{
                layer.msg(res.message, {
                    offset: '50px'
                    ,icon: 2
                    ,time: 3000
                });
              }
          },"json");
        });
      }
    });

    // 删除
    var active = {
      batchdel: function(){
        var checkStatus = table.checkStatus('test-table-page')
        ,checkData = checkStatus.data; //得到选中的数据
//        console.log(checkData);
        var arrs=[];

        for(var i=0;i<checkData.length;i++){
          arrs.push(checkData[i].id);
        }

        var user_id=arrs.join();
//        console.log(arrs.join());

        if(checkData.length === 0){
          return layer.msg('请选择代理');
        }

        layer.confirm('确定恢复吗？', function(index) {
          $.post("{{url('/api/user/fog_sub_user')}}",
          {
             token:token
              ,user_id:user_id
          },function(res){
//              console.log(res);
              if(res.status==1){
                layer.msg(res.message, {
                  offset: '50px'
                  ,icon: 1
                  ,time: 2000
                },function(){
                    window.location.reload();
                  });
              }else{
                layer.msg(res.message, {
                    offset: '50px'
                    ,icon: 2
                    ,time: 3000
                });
              }
          },"json");
        });
      }
    };

    $('.layui-btn.layuiadmin-btn-forum-list').on('click', function(){
        var type = $(this).data('type');

        active[type] ? active[type].call(this) : '';
    });

});

</script>

</body>
</html>
