<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>钱方配置</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
        .up{position: relative;display: inline-block;cursor: pointer;background-color: #1ab394;border-color: #1ab394; color: #FFF;width: 100px !important;text-align: center !important; padding: 0px 8px !important;}
        .up input {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            opacity: .01;
            width: 100px;
            height: 32px;
        }
        .modal{top:25% !important;}
        .gohome{display: none;}
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card" style="margin-top:0px">
        <div class="layui-card-header">钱方配置</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item">
                    <label class="layui-form-label">微信appid</label>
                    <div class="layui-input-block">
                        <input type="text" lay-verify="schoolshortname" autocomplete="off" placeholder="请输入微信appid" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">微信密钥secret</label>
                    <div class="layui-input-block">
                        <input type="text" lay-verify="schoolshortname" autocomplete="off" placeholder="请输入微信密钥secret" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">加签key</label>
                    <div class="layui-input-block">
                        <input type="text" lay-verify="schoolshortname" autocomplete="off" placeholder="请输入加签key" class="layui-input">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">钱方分配唯一标示code</label>
                    <div class="layui-input-block" style="width: 70%;float: left;display: inline-block;margin-left: 0px;">
                        <input type="text" lay-verify="schoolshortname" autocomplete="off" placeholder="请输入钱方分配唯一标示code" class="layui-input">
                    </div>
                </div>

                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit site-demo-active" data-type="tabChange">保存</button>
                        </div>
                    </div>
                </div>
                <div id="edit_rate" class="hide" style="display: none;background-color:#fff;border-radius:10px;">
                    <div class="layui-card-body tankuang">
                        <div class="layui-form">
                            <div class="layui-form-item">
                            <label class="layui-form-label">是否保存</label>
                            </div>
                            <div class="layui-form-item" style="display: flex;">
                                <div class="layui-input-block">
                                    <div class="layui-footer">
                                        <button class="layui-btn submits yes" style="background-color: #1E9FFF;border-radius:5px;margin-left:-55px;">是</button>
                                    </div>
                                </div>
                                <div class="layui-input-block">
                                    <div class="layui-footer">
                                        <button class="layui-btn submits no" style="background-color: #FF5722;border-radius:5px;">否</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="qd" value="1">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script> 
<script>
    var token = sessionStorage.getItem("Usertoken");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','element'], function(){
        var $ = layui.$ 
            ,admin = layui.admin
            ,element = layui.element
            ,form = layui.form
            ,upload = layui.upload;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });

        $.post("{{url('/api/user/qfpay_config')}}",
        {
            token:token
            ,type:2
        },function(res){
    //        console.log(res);
                if(res.status == 1){
                    $('.layui-form .layui-form-item').eq(0).find('input').val(res.data.wx_appid);
                    $('.layui-form .layui-form-item').eq(1).find('input').val(res.data.wx_secret);
                    $('.layui-form .layui-form-item').eq(2).find('input').val(res.data.key);
                    $('.layui-form .layui-form-item').eq(3).find('input').val(res.data.code);
//                    layer.msg(res.message, {
//                        offset: '50px'
//                        ,icon: 1
//                        ,time: 2000
//                    },function(){
//                        window.location.reload();
//                    });
                }else{
                    layer.alert(res.message, {icon: 2});
                }

        },"json");

        $('.submit').on('click', function(){
            layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area: '350px',
                skin: 'layui-layer-nobg', //没有背景色
                shadeClose: true,
                content: $('#edit_rate')
            });

            $('.yes').on('click',function(){
                $.post("{{url('/api/user/qfpay_config')}}",
                {
                    token:token
                    ,type:1
                    ,wx_appid:$('.layui-form .layui-form-item').eq(0).find('input').val()
                    ,wx_secret:$('.layui-form .layui-form-item').eq(1).find('input').val()
                    ,key:$('.layui-form .layui-form-item').eq(2).find('input').val()
                    ,code:$('.layui-form .layui-form-item').eq(3).find('input').val()
                },function(res){
//                    console.log(res);
                    if(res.status == 200103){
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 2000
                        },function(){
                            window.location.reload();
                        });
                    }else{
                        layer.alert(res.message, {icon: 2});
                    }
                },'json')
            });

            $('.no').on('click',function(){
                $('#layui-layer-shade1').css('opacity','0');
                $('#edit_rate').css('display', 'none');
            });

        });

        $('.submits').click(function(){
            $('#edit_rate').css('display','none')
        });

    });

</script>

</body>
</html>
