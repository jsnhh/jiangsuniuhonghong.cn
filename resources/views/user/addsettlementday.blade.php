<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>赏金日结算设置</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/formSelects-v4.css')}}" media="all">
    <style>
        .icon-close{display: none;}
        #demo1 img{width: 100%;height: 100%;}
        .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
        .img_box{position: relative;width:13%;height:10%;display: inline-block; margin-right: 10px;}
        .img_box span{position: absolute;right:0;top:0;font-size: 30px;background: #fff;cursor: pointer;}
        video{
            width:200px;
        }
        .img_box2{
            position: relative;width:200px;height:10%;display: inline-block; margin-right: 10px;
        }
        .img_box2 span{position: absolute;right:0;top:0;font-size: 30px;background: #fff;cursor: pointer;}
        .line{
            line-height: 36px;
        }
        .lianjiecon div{
            width:100%;
            overflow: hidden;
            margin-bottom:20px;
        }
        .lianjiecon label{
            display: inline-block;
            float: left;
            width:10%;
            line-height: 36px;
        }
        .lianjiecon input{
            display: inline-block;
            float: left;
            width:90%;
        }
        .type2{
            display: none
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header">赏金日结算设置</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">


                <!-- 返佣来源  -->
                <div class="layui-form-item class isShow">
                    <label class="layui-form-label" style="text-align:center">返佣来源</label>
                    <div class="layui-input-block" style="width:27%;display: inline-block;margin-left:0">
                        <select name="range1" id="range1" xm-select="range1" xm-select-search="">
                            <option value="">请选择赏金来源</option>
                            <option value="1000" class=""> 支付宝</option>
                            <option value="2000" class=""> 微信支付</option>
                            <option value="13000" class=""> 随行付</option>
                            <option value="19000" class=""> 随行付A</option>
                            <option value="31000" class=""> 建行</option>
                            <option value="29000" class=""> 邮驿付</option>
                            <option value="8000" class=""> 新大陆</option>
                            <option value="15000" class=""> 哆啦宝</option>
                            <option value="21000" class=""> 易生</option>
                            <option value="16000" class=""> 支付宝-ZFT</option>
                            <option value="16001" class=""> 花呗分期-ZFT</option>
                            <option value="1001" class=""> 花呗分期</option>
{{--                            <option value="hb" class=""> 红包码</option>--}}
{{--                            <option value="5000" class=""> 联动优势</option>--}}
{{--                            <option value="6000" class=""> 京东金融</option>--}}
{{--                            <option value="9000" class=""> 和融通</option>--}}
{{--                            <option value="14000" class=""> 银盛</option>--}}
{{--                            <option value="20000" class=""> 工行</option>--}}
{{--                            <option value="22000" class=""> 海科融通</option>--}}
{{--                            <option value="24000" class=""> 钱方</option>--}}
{{--                            <option value="27000" class=""> 威富通</option>--}}
{{--                            <option value="28000" class=""> 汇旺财</option>--}}
{{--                            <option value="12000" class=""> TF传化</option>--}}
{{--                            <option value="3000" class=""> 快钱支付</option>--}}
{{--                            <option value="23000" class=""> HL葫芦</option>--}}
                        </select>
                    </div>
                </div>
                <div class="layui-form-item class isShow_xi_an">
                    <label class="layui-form-label" style="text-align:center">返佣来源</label>
                    <div class="layui-input-block" style="width:27%;display: inline-block;margin-left:0">
                        <select name="range2" id="range2" xm-select="range2" xm-select-search="">
                            <option value="">请选择赏金来源</option>
                            <option value="21000" class=""> 易生</option>
                            <option value="13000" class=""> 随行付</option>
                            <option value="19000" class=""> 随行付A</option>
                            <option value="31000" class=""> 建行</option>
                            <option value="29000" class=""> 邮驿付</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item class isShow_hn">
                    <label class="layui-form-label" style="text-align:center">返佣来源</label>
                    <div class="layui-input-block" style="width:27%;display: inline-block;margin-left:0">
                        <select name="range3" id="range3" xm-select="range3" xm-select-search="">
                            <option value="">请选择赏金来源</option>

                            <option value="21000" class=""> 易生</option>
                            <option value="32000" class=""> 易生数科</option>
                            <option value="31000" class=""> 建行</option>

                        </select>
                    </div>
                </div>

                <div class="layui-form-item class">
                    <label class="layui-form-label" style="text-align:center">服务商</label>
                    <div class="layui-input-block" style="width:50%;display: inline-block;margin-left:0">
                        <select name="range" id="range" xm-select="range" xm-select-search="" xm-select-max="6">
                            <option value="">请选择服务商</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">税&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;点</label>
                    <div class="layui-input-block"  style="margin-left:0;width:400px;float: left;line-height: 38px;">
                        <input type="text" placeholder="请输入税点" autocomplete="off" class="layui-input rate" style="width: 94%;float: left;margin-right: 10px;">%
                    </div>
                </div>
                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit">确定提交</button>
                            <!--<button type="reset" class="layui-btn layui-btn-primary">重置</button>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="user_id" value="">
<input type="hidden" class="store-id" value="">

<input type="hidden" class="classname" value="">
<input type="hidden" class="templateid" value="">
<input type="hidden" class="student_code" value="">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<!-- <script src="{{asset('/layuiadmin/modules/formSelects.js')}}"></script> -->
<script>
    var token = sessionStorage.getItem("Usertoken");
    var source = sessionStorage.getItem("source");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects-v4'
    }).use(['index', 'form','formSelects','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,formSelects = layui.formSelects;
        // formSelects.render('position');
        // formSelects.btns('position', []);
        formSelects.render('range');
        formSelects.btns('range', []);
        formSelects.render('store');
        formSelects.btns('store', []);
        formSelects.render('son');
        formSelects.btns('son', []);
        var arrp=[];
        var arrr=[];
        var arrs=[];
        var arrn=[];

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });
        if(source == '01'){
            $(".isShow").show();
            $(".isShow_hn").hide();
            $(".isShow_xi_an").hide();
        }
        if(source == '02'){
            $(".isShow").hide();
            $(".isShow_hn").show();
            $(".isShow_xi_an").hide();
        }
        if( source == '03'){
            $(".isShow").hide();
            $(".isShow_hn").hide();
            $(".isShow_xi_an").show();
        }

        $('#range').html('');
        formSelects.config('range', {
            searchUrl: "{{url('/api/user/get_sub_users?token=')}}"+token+'&self='+1+'&level='+1+'&ty=admin',
            searchName: 'user_name',
            beforeSuccess: function(id, url, searchVal, result){
                result = result.data;
                var arrr=[];
                for(var i=0;i<result.length;i++){
                    var children=[];
                    if(result[i].children){
                        for(var c=0;c<result[i].children.length;c++){
                            var children2=[];
                            for(var c2=0;c2<result[i].children[c].children.length;c2++){

                                var children3=[];
                                for(var c3=0;c3<result[i].children[c].children[c2].children.length;c3++){

                                    var children4=[];
                                    for(var c4=0;c4<result[i].children[c].children[c2].children[c3].children.length;c4++){
                                        children4.push({
                                            "value":result[i].children[c].children[c2].children[c3].children[c4].id,"name":result[i].children[c].children[c2].children[c3].children[c4].name+'-'+result[i].children[c].children[c2].children[c3].children[c4].level_name
                                        });
                                    }
                                    children3.push({
                                        "value":result[i].children[c].children[c2].children[c3].id,"name":result[i].children[c].children[c2].children[c3].name+'-'+result[i].children[c].children[c2].children[c3].level_name,'children':children4
                                    });
                                }
                                children2.push({
                                    "value":result[i].children[c].children[c2].id,"name":result[i].children[c].children[c2].name+'-'+result[i].children[c].children[c2].level_name,'children':children3
                                });
                            }
                            children.push({
                                "value":result[i].children[c].id,"name":result[i].children[c].name+'-'+result[i].children[c].level_name,'children':children2
                            })
                        }
                    }

                    var data ={"value":result[i].id,"name":result[i].name+'-'+result[i].level_name,'children':children};
                    arrr.push(data);
                }
                // console.log(arrr);
                //然后返回数据
                return arrr;
            }
        }).data('range', 'server', {

        });

        var arr=[];
        formSelects.on('range', function(id, vals, val, isAdd, isDisabled){
            arrs=[];
            if(isAdd==true){
                if($('.user_id').val()==''){
                    $('.user_id').val(val.value);
                }else{
                    if($('.user_id').val()!=val.value){
                        $('.user_id').val($('.user_id').val()+','+val.value);
                    }
                }
            }else{
                var arrbox=[];
                for(var i=0;i<vals.length;i++){
                    if(val.value!=vals[i].value){
                        var box = vals[i].value;
                        arrbox.push(box);
                        $('.user_id').val(arrbox.join());
                    }
                }
            }

            var a=$('.user_id').val();
            var aa=a.split(',');
            var c=parseInt(a);
            var b=val.value;//number类型

            if(aa.length>1){
                formSelects.data('store', 'local', {arr: []});
            }else{
//                console.log('2222');
                if(b==a && isAdd==false){
                    formSelects.data('store', 'local', {arr: []});
                    $('.user_id').val('');
                }else{
                    //第二个input获取门店列表
                    formSelects.config('store', {
                        beforeSuccess: function(id, url, searchVal, result){
                            //我要把数据外层的code, msg, data去掉
                            arrs=[];
//                            console.log(result);
                            result = result.data;
                            for(var i=0;i<result.length;i++){
                                var data ={"value":result[i].id,"name":result[i].store_name};
                                arrs.push(data);
                                // console.log(data)
                            }
                            //然后返回数据
                            return arrs;
                            //console.log(arr);
                        },
                        clearInput: true
                    }).data('store', 'server', {
                        searchUrl:"{{url('/api/user/store_lists?token=')}}"+token+"&user_id="+$('.user_id').val()+"l=100",
                        searchName: 'store_name'
                    });
                }
            }
        });

        formSelects.on('range1', function(id, vals, val, isAdd, isDisabled){
            arrn=[];
            if(isAdd==true){
                if($('.store-id').val()==''){
                    $('.store-id').val(val.value);

                }else{
                    if($('.store-id').val()!=val.value){
                        $('.store-id').val($('.store-id').val()+','+val.value);
                    }
                }
            }else{
                var arrbox=[];
                for(var i=0;i<vals.length;i++){
                    if(val.value!=vals[i].value){//当为false时去掉的userid 在数组中去掉重复的userid,此方法有小bug
                        var box = vals[i].value;
                        arrbox.push(box);
                        //  console.log(arrbox);//选中的userid
                        $('.store-id').val(arrbox.join());
                    }
                }
            }
        });
        formSelects.on('range2', function(id, vals, val, isAdd, isDisabled){
            arrn=[];
            if(isAdd==true){
                if($('.store-id').val()==''){
                    $('.store-id').val(val.value);

                }else{
                    if($('.store-id').val()!=val.value){
                        $('.store-id').val($('.store-id').val()+','+val.value);
                    }
                }
            }else{
                var arrbox=[];
                for(var i=0;i<vals.length;i++){
                    if(val.value!=vals[i].value){//当为false时去掉的userid 在数组中去掉重复的userid,此方法有小bug
                        var box = vals[i].value;
                        arrbox.push(box);
                        //  console.log(arrbox);//选中的userid
                        $('.store-id').val(arrbox.join());
                    }
                }
            }
        });
        formSelects.on('range3', function(id, vals, val, isAdd, isDisabled){
            arrn=[];
            if(isAdd==true){
                if($('.store-id').val()==''){
                    $('.store-id').val(val.value);

                }else{
                    if($('.store-id').val()!=val.value){
                        $('.store-id').val($('.store-id').val()+','+val.value);
                    }
                }
            }else{
                var arrbox=[];
                for(var i=0;i<vals.length;i++){
                    if(val.value!=vals[i].value){//当为false时去掉的userid 在数组中去掉重复的userid,此方法有小bug
                        var box = vals[i].value;
                        arrbox.push(box);
                        //  console.log(arrbox);//选中的userid
                        $('.store-id').val(arrbox.join());
                    }
                }
            }
        });

        $('.submit').click(function(){
            $.post("{{url('/api/wallet/addSettlement')}}",
                {
                    token:token,
                    user_ids:$('.user_id').val(),  //用户合集-‘1,2,3,4’
                    store_key_ids:$('.store-id').val(),  //门店合-'1,2,3,4'
                    source_type:$('.source_type').val(),
                    source_type_desc:$('.source_type_desc').val(),
                    rate:$('.rate').val(),
                },function(res){
                    console.log(res);
                    if(res.status==1){
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 2000
                        },function(){
                            window.location.reload();
                        });
                    }else{
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 2000
                        });
                    }
                },"json");
        })








    });
</script>
</body>
</html>
