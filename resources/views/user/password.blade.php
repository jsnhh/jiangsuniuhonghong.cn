<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>修改密码</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
  <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
</head>
<body>

    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md6">
                <form class="layui-form layui-row changePwd">
                    <div class="layui-col-xs12 layui-col-sm6 layui-col-md6" style="padding-left:50%;margin-top:30%">
                        <div class="layui-form-item">
                            <label class="layui-form-label">旧密码</label>
                            <div class="layui-input-block">
                                <input type="password" value="" name="old_pwd" placeholder="请输入旧密码" lay-verify="required|oldPwd" id="oldpass" class="layui-input pwd"style="width:200px">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">新密码</label>
                            <div class="layui-input-block">
                                <input type="password" value="" name="new_pwd" placeholder="请输入新密码" lay-verify="required|newPwd" id="oldPwd" class="layui-input pwd"style="width:200px">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">确认新密码</label>
                            <div class="layui-input-block">
                                <input type="password" value="" name="re_new_pwd" placeholder="请确认密码" lay-verify="required|confirmPwd" id="repass" class="layui-input pwd"style="width:200px">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <div class="layui-input-block" style="width:200px;margin-left:120px">
                                <button class="layui-btn" lay-submit id="changePwd" lay-filter="changePwd" style="border-radius:5px;margin-right:15px">立即修改</button>
                                <button type="reset" class="layui-btn layui-btn-primary"style="border-radius:5px">清空</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
  
<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
var token = sessionStorage.getItem("Usertoken");
// var str=location.search;
// var customer_id=str.split('?')[1];
// console.log(customer_id)
layui.config({
    base: '../../../layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['form','layer','laydate','table','laytpl'], function(){
    // 修改密码---------------------------------------------------------------------------
    var form = layui.form,
    layer = parent.layer === undefined ? layui.layer : top.layer,
    $ = layui.jquery,
        laydate = layui.laydate,
        laytpl = layui.laytpl,
        table = layui.table;

    form.render();
    let param = {
        token:token,
        old_pwd:$('#oldpass').val(),
        new_pwd:$('#oldPwd').val(),
        re_new_pwd:$('#repass').val()
    };
// 未登录,跳转登录页面
    $(document).ready(function(){
        if(token == null){
            window.location.href="{{url('/user/login')}}";
        }
    });
    form.verify({
        oldPwd : function(value, item){
        //                    console.log('value',value);
          // if(value != "123456"){
          //     return "密码错误，请重新输入！";
          // }
        },
        newPwd : function(value, item){
//        console.log(value,'new');
            if(value.length < 6){
                return "密码长度不能小于6位";
            }
        },
        confirmPwd : function(value, item){
//        console.log(value,'confirm');
            if(!new RegExp($("#oldPwd").val()).test(value)){
                return "两次输入密码不一致，请重新输入！";
            }
        }

    });

    form.on('submit(changePwd)',function(data){
      $.ajax({
        url:"{{url('/api/user/edit_pwd')}}",
        method:'post',
        data:{...data.field,token},
        success: function (item) {
          const arr = item.split(',')
           const num = arr[0].indexOf('1')

           if(num !== -1){
            layer.alert("修改成功",{icon: 1,time:2000});
           }else{
            layer.alert('旧密码输入错误');
           }
        }

      });
      return false
    })

})

</script>

</body>
</html>
