<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>终端奖励详情</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
        .layui-card-header {
            width: 80px;
            text-align: right;
            float: left;
        }

        .layui-card-body {
            margin-left: 28px;
        }

        .layui-upload-img {
            width: 92px;
            height: 92px;
            margin: 0 10px 10px 0;
        }

        .up {
            position: relative;
            display: inline-block;
            cursor: pointer;
            border-color: #1ab394;
            color: #FFF;
            width: 92px !important;
            font-size: 10px !important;
            text-align: center !important;
        }

        .up input {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            opacity: .01;
            width: 100px;
            height: 30px;
        }

        .layui-upload-list {
            width: 100px;
            height: 96px;
            overflow: hidden;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none !important;
            margin: 0;
        }

        .width {
            width: 60%;
            float: left;
        }

        p {
            float: left;
            line-height: 36px;
            margin-left: 10px;
        }

        .userbox, .storebox {
            height: 200px;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 38px;
            width: 298px;
            background-color: #ffffff;
            border: 1px solid #ddd;
        }

        .userbox .list, .storebox .list {
            height: 38px;
            line-height: 38px;
            cursor: pointer;
            padding-left: 10px;
        }

        .userbox .list:hover, .storebox .list:hover {
            background-color: #eeeeee;
        }

        .s_id {
            line-height: 36px;
        }

        /*.layui-form-label{*/
        /*    width: 140px;*/
        /*}*/

    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card" style="margin-top:0px">
        <div class="layui-card-header" style="width:auto !important">终端奖励详情<span class="zong_school_name"></span></div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item" style="width:500px;">
                    <label class="layui-form-label">门店名称</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input store_name">
                    </div>
                </div>
                <div class="layui-form-item" style="width:500px;">
                    <label class="layui-form-label">门&nbsp;&nbsp;店&nbsp;&nbsp;ID</label>
                    <div class="layui-input-block">
                        <input type="text" autocomplete="off" class="layui-input store_id">
                    </div>
                </div>
                <div class="layui-form-item school" style="width:500px;">
                    <label class="layui-form-label">设备sn</label>
                    <div class="layui-input-block">
                        <input type="text" autocomplete="off" class="layui-input terminal_sn">
                    </div>
                </div>
                <div class="layui-form-item school" style="width:500px;">
                    <label class="layui-form-label">设备名称</label>
                    <div class="layui-input-block">
                        <input type="text" autocomplete="off" class="layui-input terminal_name">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">活动时间</label>
                    <div class="layui-input-block">
                        <div class="layui-form" style="display: inline-block;">
                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <div class="layui-input-inline">
                                        <input type="text" class="layui-input start_time">
                                    </div>
                                </div>
                                -
                                <div class="layui-inline" style='margin-left:10px;'>
                                    <div class="layui-input-inline">
                                        <input type="text" class="layui-input end_time">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-card-header" style="width:auto !important;margin-top: 20px;text-align: left">激活标准规则
            <hr style="margin-top: -5px;width: 1650px;">
        </div>
        <div class="layui-form" lay-filter="component-form-group" style="margin-top: 100px;margin-left: 100px">

            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">激活标准单笔交易金额</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input standard_single_amt" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">激活标准交易笔数</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input standard_total" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">激活标准奖励金额</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input standard_amt" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">激活标准期限范围</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input standard_term" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">是否达标</label>
                <label class="layui-form-label standard_status_y" style="color: #0bb20c;text-align: left;display: none">是</label>
                <label class="layui-form-label standard_status_n" style="color: #942a25;text-align: left;display: none">否</label>
            </div>
            <div class="layui-form-item standard_amt_get" style="display: none">
                <label class="layui-form-label" style="width: 140px">已获得激活奖励金额</label>
                <label class="layui-form-label standard_amt_y" style="color: #0bb20c;text-align: left"></label>
            </div>
        </div>
        <div class="layui-card-header" style="width:auto !important;margin-top: 20px;text-align: left">首次奖励标准
            <hr style="margin-top: -5px;width: 1650px;">
        </div>
        <div class="layui-form" lay-filter="component-form-group" style="margin-top: 100px;margin-left: 100px">
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">首次单笔交易金额</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input first_single_amt" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">首次交易笔数</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input first_total" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">首次奖励金额</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input first_amt" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item" >
                <label class="layui-form-label" style="width: 140px">首次奖励期限范围</label>
                <label class="layui-form-label" style="margin-left: -70px">T+1</label>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">是否达标</label>
                <label class="layui-form-label first_status_y" style="color: #0bb20c;text-align: left;display: none">是</label>
                <label class="layui-form-label first_status_n" style="color: #942a25;text-align: left;display: none">否</label>
            </div>
            <div class="layui-form-item first_amt_get" style="display: none">
                <label class="layui-form-label" style="width: 140px">已获得首次奖励金额</label>
                <label class="layui-form-label first_amt_y" style="color: #0bb20c;text-align: left"></label>
            </div>
        </div>
        <div class="layui-card-header" style="width:auto !important;margin-top: 20px;text-align: left">首次奖励标准
            <hr style="margin-top: -5px;width: 1650px;">
        </div>
        <div class="layui-form" lay-filter="component-form-group" style="margin-top: 100px;margin-left: 100px">
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">二次单笔交易金额</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input again_single_amt" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">二次交易笔数</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input again_total" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">二次奖励金额</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input again_amt" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item" >
                <label class="layui-form-label" style="width: 140px">二次奖励期限范围</label>
                <label class="layui-form-label" style="margin-left: -70px">T+2</label>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">是否达标</label>
                <label class="layui-form-label again_status_y" style="color: #0bb20c;text-align: left;display: none">是</label>
                <label class="layui-form-label again_status_n" style="color: #942a25;text-align: left;display: none">否</label>
            </div>
            <div class="layui-form-item again_amt_get" style="display: none">
                <label class="layui-form-label" style="width: 140px">已获得二次奖励金额</label>
                <label class="layui-form-label again_amt_y" style="color: #0bb20c;text-align: left"></label>
            </div>
        </div>

    </div>
</div>


<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var terminalRewardId = sessionStorage.getItem("terminalRewardId");
    // var str=location.search;
    // var school_name=str.split('?')[1];

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form', 'upload', 'formSelects', 'element', 'laydate'], function () {
        var $ = layui.$
            , admin = layui.admin
            , element = layui.element
            , layer = layui.layer
            , laydate = layui.laydate
            , form = layui.form
            , upload = layui.upload
            , formSelects = layui.formSelects;

        var src = $('#demo1').attr('src');
        element.render();
        // 未登录,跳转登录页面
        $(document).ready(function () {
            if (token == null) {
                window.location.href = "{{url('/user/login')}}";

            } else {
                $.ajax({
                    url: "{{url('/api/user/getTerminalRewardId')}}",
                    data: {token: token, id: terminalRewardId},
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        $('.store_name').val(data.data.store_name)
                        $('.store_id').val(data.data.store_id)
                        $('.terminal_sn').val(data.data.terminal_sn)
                        $('.terminal_name').val(data.data.terminal_name)
                        $('.start_time').val(data.data.start_time)
                        $('.end_time').val(data.data.end_time)
                        $('.standard_amt').val(data.data.standard_amt)
                        $('.standard_single_amt').val(data.data.standard_single_amt)
                        $('.standard_total').val(data.data.standard_total)
                        $('.first_amt').val(data.data.first_amt)
                        $('.first_single_amt').val(data.data.first_single_amt)
                        $('.first_total').val(data.data.first_total)
                        $('.again_amt').val(data.data.again_amt)
                        $('.again_single_amt').val(data.data.again_single_amt)
                        $('.again_total').val(data.data.again_total)
                        $('.standard_term').val(data.data.standard_term)
                        if (data.data.standard_status == '1') {
                            $('.standard_status_y').show();
                            $('.standard_amt_get').show();
                            $('.standard_amt_y').text(data.data.standard_amt);
                        }
                        if (data.data.standard_status == '0') {
                            $('.standard_status_n').show();
                        }
                        if (data.data.first_status == '1') {
                            $('.first_status_y').show();
                            $('.first_amt_get').show();
                            $('.first_amt_y').text(data.data.first_amt);
                        }
                        if (data.data.first_status == '0') {
                            $('.first_status_n').show();
                        }
                        if (data.data.again_status == '1') {
                            $('.again_status_y').show();
                            $('.again_amt_get').show();
                            $('.again_amt_y').text(data.data.again_amt);
                        }
                        if (data.data.again_status == '0') {
                            $('.again_status_n').show();
                        }

                    },
                    error: function (data) {
                        alert('查找板块报错');
                    }
                });
            }

        });


    });

</script>

</body>
</html>
