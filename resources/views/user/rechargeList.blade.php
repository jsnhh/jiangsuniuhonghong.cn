<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>商户充值记录</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .tongbu {
            background-color: #4c9ef8;
            color: #fff;
            border-radius: 3.5px
        }

        .cur {
            color: #21c4f5;
        }

        .userbox, .storebox {
            height: 190px;
            margin-right: 0;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 63px;
            width: 298px;
            background-color: #ffffff;
            border: 1px solid #ddd;
        }

        .userbox .list, .storebox .list {
            height: 38px;
            line-height: 38px;
            cursor: pointer;
            padding-left: 10px;
        }

        .userbox .list:hover, .storebox .list:hover {
            background-color: #eeeeee;
        }

        .yname {
            font-size: 13px;
            color: #444;
            margin-bottom: 8px;
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12" style="margin-top:0px">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12">
                        <div class="layui-card">
                            <div class="layui-card-header">商户充值记录</div>
                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">
                                    <div style="font-size:14px">
                                        <div class="layui-form" lay-filter="component-form-group"
                                             style="width:190px;margin-right:0;display: inline-block;">
                                            <div class="layui-form-item">
                                                <div class="layui-input-block"
                                                     style="margin-left:5px;border-radius:5px">
                                                    <text class="yname">代理商名称</text>
                                                    <input type="text" style="border-radius:5px"
                                                           tyle="border-radius:5px" name="schoolname"
                                                           lay-verify="schoolname" autocomplete="off"
                                                           placeholder="请输入代理商名称" class="layui-input transfer">
                                                    <div class="userbox" style='display: none'></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-form" lay-filter="component-form-group"
                                             style="width:190px;margin-right:0;display: inline-block;">
                                            <div class="layui-form-item">
                                                <div class="layui-input-block"
                                                     style="margin-left:5px;border-radius:5px">
                                                    <text class="yname">门店名称</text>
                                                    <input type="text" style="border-radius:5px" name="schoolname"
                                                           lay-verify="schoolname" autocomplete="off"
                                                           placeholder="请输入门店名称" class="layui-input inputstore">
                                                    <div class="storebox" style='display: none'></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 支付状态 -->
                                        <div class="layui-form" lay-filter="component-form-group"
                                             style="width:190px;margin-right:0;display: inline-block;">
                                            <div class="layui-form-item">
                                                <div class="layui-input-block"
                                                     style="margin-left:5px;border-radius:5px">
                                                    <text class="yname">支付状态</text>
                                                    <select name="status" id="status" lay-filter="status">
                                                        <option value="">全部</option>
                                                        <option value="1">成功</option>
                                                        <option value="2">等待支付</option>
                                                        <!--<option value="3">失败</option>-->
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-form" lay-filter="component-form-group"
                                             style="width:190px;margin-right:0;display: inline-block;">
                                            <div class="layui-form-item">
                                                <div class="layui-input-block"
                                                     style="margin-left:5px;border-radius:5px">
                                                    <text class="yname">充值方式</text>
                                                    <select name="pay_type" id="pay_type" lay-filter="pay_type">
                                                        <option value="">全部</option>
                                                        <option value="1">流量</option>
                                                        <option value="2">月包</option>
                                                        <option value="3">季包</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:100%;display: flex;">
                                        <div class="layui-form-item" style="margin-left:10px;">
                                            <div class="layui-inline">
                                                <div class="layui-input-inline" style="width:175px;margin-right:0;">
                                                    <text class="yname">订单开始时间</text>
                                                    <input type="text" style="border-radius:5px"
                                                           class="layui-input start-item test-item"
                                                           placeholder="订单开始时间" lay-key="23">
                                                </div>
                                            </div>
                                            <div class="layui-inline">
                                                <div class="layui-input-inline" style="width:175px;margin-right:0;">
                                                    <text class="yname">订单结束时间</text>
                                                    <input type="text" style="border-radius:5px"
                                                           class="layui-input end-item test-item"
                                                           placeholder="订单结束时间" lay-key="24">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="layui-form-item" style="margin-left:5px;">

                                            <div class="layui-inline" style='margin-right:0'>
                                                <div class="layui-input-inline" style="width:185px">
                                                    <text class="yname">订单号</text>
                                                    <input type="text" style="border-radius:5px" name="tradeno"
                                                           placeholder="请输入订单号" autocomplete="off" class="layui-input">
                                                </div>
                                            </div>

                                            <div class="layui-inline">
                                                <button class="layui-btn layuiadmin-btn-list" lay-submit=""
                                                        lay-filter="LAY-app-contlist-search"
                                                        style="border-radius:5px;margin-top:20px;margin-bottom: 0;height:36px;line-height: 36px;">
                                                    <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                                </button>
                                            </div>
                                            <button class="layui-btn export"
                                                    style="border-radius:5px;margin-top:20px;margin-bottom: 4px;height:36px;line-height: 36px;">
                                                导出
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-row layui-col-space15">
                                    <div class="layui-col-md12">
                                        <div class="layui-card" style="background-color: transparent;">
                                            <div class="layui-card-header"><div class="inline">充值总额:<span class="total"></span></div></div>
                                        </div>
                                    </div>
                                </div>
                                <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>

                                <!-- 判断状态 -->
                                <script type="text/html" id="statusTap">
                                    @{{#  if(d.status=='1'){ }}
                                    <span style="color:#00963a">支付成功</span>
                                    @{{#  } else if(d.status=='2') { }}
                                    <span style="color:#0066FF">等待支付</span>
                                    @{{#  } else if(d.status=='0') { }}
                                    <span style="color:#e85052">支付失败</span>
                                    @{{#  } }}
                                </script>
                                <script type="text/html" id="payTypeTap">
                                    @{{#  if(d.pay_type=='1'){ }}
                                    流量
                                    @{{#  } else if(d.pay_type=='2') { }}
                                    月包
                                    @{{#  } else if(d.pay_type=='3') { }}
                                    季包
                                    @{{#  } }}
                                </script>

                                <script type="text/html" id="timeTap">
                                    @{{#  if(d.pay_type=='2' || d.pay_type=='3'){ }}
                                    <span style="color:#0066FF">@{{ d.end_time.substring(0,10)}}</span>
                                    @{{#  } }}
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="store_id">
<input type="hidden" class="user_id">
<input type="hidden" class="pay_status">
<input type="hidden" class="pay_type">
<input type="hidden" class="danhao">
<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var str = location.search;
    var store_id = str.split('?')[1];


    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'table', 'laydate'], function () {
        var $ = layui.$
            , admin = layui.admin
            , form = layui.form
            , table = layui.table
            , laydate = layui.laydate;

        $('.store_id').val(store_id);

        // 未登录,跳转登录页面
        $(document).ready(function () {
            if (token == null) {
                window.location.href = "{{url('/user/login')}}";
            }
        });

        // 获取时间
        var nowdate = new Date();
        // 本月
        var year=nowdate.getFullYear();
        var mounth=nowdate.getMonth()+1;
        var day=nowdate.getDate();
        var hour = nowdate.getHours();
        var min = nowdate.getMinutes();
        var sec = nowdate.getSeconds();
        if(mounth.toString().length<2 && day.toString().length<2){
            var nwedata = year+'-0'+mounth+'-0'+day+' '+hour+':'+min+':'+sec;
        }
        else if(mounth.toString().length<2){
            var nwedata = year+'-0'+mounth+'-'+day+' '+hour+':'+min+':'+sec;
        }
        else if(day.toString().length<2){
            var nwedata = year+'-'+mounth+'-0'+day+' '+hour+':'+min+':'+sec;
        }
        else{
            var nwedata = year+'-'+mounth+'-'+day+' '+hour+':'+min+':'+sec;
        }
        $('.end-item').val(nwedata);//今天的时间
        // =========================================
        // 上个月
        var y = nowdate.getFullYear();
        var mon = nowdate.getMonth();
        var d = nowdate.getDate();
        var h = '00';
        var m = '00';
        var s = '00';
        if(mon.toString().length<2 && d.toString().length<2){
            var formatwdate = y+'-0'+mon+'-0'+d+' '+h+':'+m+':'+s;
        }
        else if(mon.toString().length<2){
            var formatwdate = y+'-0'+mon+'-'+d+' '+h+':'+m+':'+s;
        }
        else if(d.toString().length<2){
            var formatwdate = y+'-'+mon+'-0'+d+' '+h+':'+m+':'+s;
        }
        else{
            var formatwdate = y+'-'+mon+'-'+d+' '+h+':'+m+':'+s;
        }
        $('.start-item').val(formatwdate);


        var s_storename = sessionStorage.getItem('s_storename');

        if (store_id == undefined) {

        } else {
            $('.inputstore').val(s_storename);
        }

        // 选择门店
        $.ajax({
            url: "{{url('/api/user/store_lists')}}",
            data: {token: token, l: 100},
            type: 'post',
            success: function (data) {
                //console.log(data);
                var optionStr = "";
                for (var i = 0; i < data.data.length; i++) {
                    optionStr += "<option value='" + data.data[i].store_id + "' " + ((store_id == data.data[i].store_id) ? "selected" : "") + ">" + data.data[i].store_name + "</option>";
                }
                $("#schooltype").append('<option value="">选择门店</option>' + optionStr);
                layui.form.render('select');
            },
            error: function (data) {
                alert('查找板块报错');
            }
        });


        $(".transfer").bind("input propertychange", function (event) {
            //         console.log($(this).val());
            user_name = $(this).val();
            if (user_name.length == 0) {
                $('.userbox').html('');
                $('.userbox').hide();
            } else {
                $.post("{{url('/api/user/get_sub_users')}}",
                    {
                        token: token
                        , user_name: $(this).val()
                        , self: '1'
                    }, function (res) {
                        var html = "";

                        if (res.t == 0) {
                            $('.userbox').html('');
                        } else {
                            for (var i = 0; i < res.data.length; i++) {
                                html += '<div class="list" data=' + res.data[i].id + '>' + res.data[i].name + '-' + res.data[i].level_name + '</div>'
                            }
                            $(".userbox").show();
                            $('.userbox').html('');
                            $('.userbox').append(html);
                        }
                    }, "json");
            }
        });

        $(".userbox").on("click", ".list", function () {
            $('.transfer').val($(this).html());
            $('.user_id').val($(this).attr('data'));
            $('.userbox').hide();

            table.reload('test-table-page', {
                where: {
                    user_id: $(this).attr('data'),
                    store_id: $('store_id').val()
                }
                , page: {
                    curr: 1
                }
            });
        });

        $(".inputstore").bind("input propertychange", function (event) {
            store_name = $(this).val();
            if (store_name.length == 0) {
                $('.storebox').html('');
                $('.storebox').hide();
            } else {
                $.post("{{url('/api/user/store_lists')}}",
                    {
                        token: token
                        , store_name: $(this).val()
                        , l: 100
                    }, function (res) {
                        var html = "";
                        if (res.t == 0) {
                            $('.storebox').html('');
                        } else {
                            for (var i = 0; i < res.data.length; i++) {
                                html += '<div class="list" data=' + res.data[i].store_id + '>' + res.data[i].store_name + '</div>'
                            }
                            $(".storebox").show();
                            $('.storebox').html('');
                            $('.storebox').append(html);
                        }
                    }, "json");
            }
        });

        $(".storebox").on("click", ".list", function () {
            $('.inputstore').val($(this).html());
            $('.store_id').val($(this).attr('data'));
            $('.storebox').hide();

            table.reload('test-table-page', {
                where: {
                    user_id: $('user_id').val(),
                    store_id: $(this).attr('data')
                }
                , page: {
                    curr: 1
                }
            });

            $("#passway").html('');
        });

        // 渲染表格
        table.render({
            elem: '#test-table-page'
            , url: "{{url('/api/user/getRechargeList')}}"
            , method: 'post'
            , where: {
                token: token,
                store_id: store_id,
                time_start: $('.start-item').val(),
                time_end: $('.end-item').val()
            }
            , request: {
                pageName: 'p'
                , limitName: 'l'
            }
            , page: true
            , cellMinWidth: 100
            , cols: [[
                {field: 'created_at', align: 'center', title: '充值时间', width: 160}
                , {field: 'store_name', align: 'center', title: '门店', width: 200}
                , {field: 'name', align: 'center', title: '所属代理', width: 200}
                , {field: 'amount', align: 'center', title: '充值金额'}
                , {field: 'avail_amount', align: 'center', title: '可用金额'}
                , {field: 'order_id', align: 'center', title: '订单号', width: 260}
                , {field: 'status', align: 'center', title: '状态', templet: '#statusTap'}
                , {field: 'pay_type', align: 'center', title: '充值方式', templet: '#payTypeTap'}
                , {field: 'end_time', align: 'center', title: '月包/季包到期日',templet:'#timeTap'}
            ]]
            , response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                , statusCode: 1 //成功的状态码，默认：0
                , msgName: 'message' //状态信息的字段名称，默认：msg
                , countName: 't' //数据总数的字段名称，默认：count
                , dataName: 'data' //数据列表的字段名称，默认：data
            }
            , done: function (res, curr, count) {
                $('.total').html(res.order_data.total)
                $('th').css({
                    'font-weight': 'bold',
                    'font-size': '15',
                    'color': 'black',
                    'background': 'linear-gradient(#f2f2f2,#cfcfcf)'
                });	//进行表头样式设置
            }
        });


        // 选择门店
        form.on('select(schooltype)', function (data) {
            var store_id = data.value;
            $('.store_id').val(store_id);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    store_id: store_id
                    , user_id: $('.user_id').val()
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });


        // 选择业务员
        form.on('select(agent)', function (data) {
            var user_id = data.value;
            $('.user_id').val(user_id);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    user_id: $(".user_id").val()
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });

            // 选择门店
            $.ajax({
                url: "{{url('/api/user/store_lists')}}",
                data: {
                    token: token
                    , user_id: user_id
                    , l: 100
                },
                type: 'post',
                success: function (data) {
                    var optionStr = "";
                    for (var i = 0; i < data.data.length; i++) {
                        optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
                    }
                    $("#schooltype").html('');
                    $("#schooltype").append('<option value="">选择门店</option>' + optionStr);
                    layui.form.render('select');
                },
                error: function (data) {
                    alert('查找板块报错');
                }
            });
        });

        // 选择状态
        form.on('select(status)', function (data) {
            var pay_status = data.value;
            $('.pay_status').val(pay_status);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    pay_status: pay_status
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });
        // 选择支付类型
        form.on('select(pay_type)', function (data) {
            var pay_type = data.value;
            $('.pay_type').val(pay_type);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    pay_type: pay_type
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });
        laydate.render({
            elem: '.start-item'
            , type: 'datetime'
            , trigger: 'click'
            , done: function (value) {
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: value
                        , time_end: $('.end-item').val()
                    }
                    , page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        laydate.render({
            elem: '.end-item'
            , type: 'datetime'
            , trigger: 'click'
            , done: function (value) {
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: $('.start-item').val()
                        , time_end: value
                    }
                    , page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        form.on('submit(LAY-app-contlist-search)', function (data) {

            var out_trade_no = data.field.tradeno; //订单号
            $('.danhao').val(out_trade_no);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    out_trade_no: out_trade_no
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });

        $('.export').click(function () {
            var store_id = $('.store_id').val();
            var user_id = $('.user_id').val();
            var pay_status = $('.pay_status').val();
            var pay_type = $('.pay_type').val();
            var time_start = $('.start-item').val();
            var time_end = $('.end-item').val();
            var out_trade_no = $('.danhao').val();
            window.location.href = "{{url('/api/export/rechargeRecordExportDown')}}" + "?token=" + token + "&store_id=" + store_id + "&user_id=" + user_id +"&pay_status=" + pay_status + "&pay_type=" + pay_type + "&time_start=" + time_start + "&time_end=" + time_end + "&out_trade_no=" + out_trade_no ;
        })

    });

</script>

</body>
</html>
