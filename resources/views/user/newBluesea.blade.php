<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>IOT授权</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/modules/layuiicon/iconfont.css')}}" media="all">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .tongbu {
            background-color: #4c9ef8;
            color: #fff;
        }

        .cur {
            color: #009688;
        }

        .userbox,
        .storebox {
            height: 200px;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 63px;
            width: 498px;
            background-color: #ffffff;
            border: 1px solid #ddd;
        }

        .userbox .list,
        .storebox .list {
            height: 38px;
            line-height: 38px;
            cursor: pointer;
            padding-left: 10px;
        }

        .userbox .list:hover,
        .storebox .list:hover {
            background-color: #eeeeee;
        }

        .s_id {
            line-height: 36px;
        }

        .layui-form-radio {
            margin-left: 10px;
            margin-bottom: 7px;
        }

        #steps {
            padding: 30px 0 0 260px;
            font-weight: 400;
        }
    </style>
</head>

<body>

    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">

                <div class="layui-fluid">
                    <div class="layui-row layui-col-space15">
                        <div class="layui-col-md12" style="margin-top:0px">
                            <div class="layui-card">
                                <div class="layui-card-header">IOT授权</div>
                                <div class="layui-card-body" style="height:750px;padding-bottom:30px;">
                                    <div id="steps"></div>
                                    <div class="layui-btn-container" style="font-size:14px;">
                                        <!-- 代运营授权 -->
                                        <div class="layui-tab-item layui-show" id="addwechatapp" style="margin-left: 24%; margin-top:50px;">
                                            <form class="layui-form" action="" lay-filter="example">
                                                <div class="layui-row">
                                                    <div class="grid-demo">
                                                        <div class="layui-form-item">
                                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                                <span style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                                商户类型
                                                            </label>
                                                            <div class="layui-input-block">
                                                                <input type="radio" name="radio" value="" title="" checked><text>直连商户</text>
                                                            </div>
                                                        </div>
                                                        <div class="layui-form-item">
                                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                                <span style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                                支付宝账号
                                                            </label>
                                                            <div class="layui-input-block">
                                                                <input type="text" name="legal_persona_wechat" lay-verify="title" autocomplete="off" placeholder="请输入支付宝账号" class="layui-input" style="width: 552px;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="layui-form-item">
                                                        <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                            <span style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                            绑定类型
                                                        </label>
                                                        <div class="layui-input-block">
                                                            <input type="radio" name="radio" value="" title="" checked><text>支付宝推送</text>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="button" style="margin-left: 9%;">
                                                    <button type="submit" class="layui-btn" lay-submit="" lay-filter="addwechat" id="addwechat">确认提交</button>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- 代运营邀请 -->
                                        <div class="layui-tab-item layui-show" id="stepTwo" style="margin-left: 24%; margin-top:25px;display:none !important;">
                                            <p style="width:865px;margin-left:8px;display:block;height: 22px;font-size: 14px;font-weight: 400;color: #333333;line-height: 22px;margin-bottom: 15px;">
                                                <i class="layui-icon iconfont" style="font-size: 14px;color:#FFB800;">&#xe670;</i>
                                                该步骤需要在收款的支付宝上操作授权：平台向该支付宝下发服务提醒消息，用户参考以下步骤接受支付宝商家服务邀请。
                                            </p>
                                            <form class="layui-form" action="" lay-filter="example">
                                                <div id="photo-list">
                                                    <img src="{{asset('/user/img/shouquan.png')}}">
                                                </div>
                                                <div class="button" style="margin-top: 30px;">
                                                    <button type="submit" class="layui-btn" lay-submit="" lay-filter="authentication" id="authentication">已接受</button>
                                                </div>
                                            </form>
                                        </div>
                                         <!-- 绑定门店 没有门店的情况 -->
                                         <div class="layui-tab-item layui-show" id="stepThree" style="margin-left: 24%; margin-top:25px;display:none !important;">
                                            <p style="width: 955px;margin-left:8px;display:block;height: 22px;font-size: 14px;font-weight: 400;color: #333333;line-height: 22px;margin-bottom: 15px;">
                                                <i class="layui-icon iconfont" style="font-size: 14px;color:#FFB800;">&#xe670;</i>
                                                该步骤需要在支付宝服务商平台（https://p.alipay.com/）创建支付宝蚂蚁门店，再查询并绑定门店。若蚂蚁后台无蚂蚁门店，可参考下图，去创建门店。
                                            </p>
                                            <form class="layui-form" action="" lay-filter="example">
                                                <div id="photo-store">
                                                    <img src="{{asset('/user/img/bindstore.png')}}">
                                                </div>
                                                <div class="button" style="margin-top: 30px;">
                                                    <button type="submit" class="layui-btn" lay-submit="" lay-filter="jueryStore" id="jueryStore">查询门店</button>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- 绑定门店 有门店的情况显示表格 -->
                                        <div class="layui-tab-item layui-show" id="stepThree" style="margin-left: 24%; margin-top:25px;display:none !important;">
                                            <p style="margin-left:8px;display:block;height: 22px;font-size: 14px;font-weight: 400;color: #333333;line-height: 22px;margin-bottom: 15px;">
                                                支付宝门店
                                            </p>
                                             <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                             <div class="button" style="margin-top: 30px;">
                                                    <button type="submit" class="layui-btn" lay-submit="" lay-filter="bindStore" id="bindStore">绑定门店</button>
                                                </div>
                                        </div>
                                         <!-- IOT授权 -->
                                         <div class="layui-tab-item layui-show" id="stepFour" style="margin-left: 24%; margin-top:50px;display:none !important;">
                                            <form class="layui-form" action="" lay-filter="example">
                                                <div class="layui-row">
                                                    <div class="grid-demo">
                                                        <div class="layui-form-item">
                                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                                <span style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                                蚂蚁门店ID
                                                            </label>
                                                            <div class="layui-input-block">
                                                                <input type="text" name="legal_persona_wechat" lay-verify="title" autocomplete="off" placeholder="请输入" class="layui-input" style="width: 552px;">
                                                            </div>
                                                        </div>
                                                        <div class="layui-form-item">
                                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                                <span style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                                门店名称
                                                            </label>
                                                            <div class="layui-input-block">
                                                                <input type="text" name="legal_persona_wechat" lay-verify="title" autocomplete="off" placeholder="请输入" class="layui-input" style="width: 552px;">
                                                            </div>
                                                        </div>
                                                        <div class="layui-form-item">
                                                            <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                                <span style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                                设备ID
                                                            </label>
                                                            <div class="layui-input-block">
                                                                <input type="text" name="legal_persona_wechat" lay-verify="title" autocomplete="off" placeholder="请输入" class="layui-input" style="width: 552px;">
                                                            </div>
                                                            <a class="deviceId" style="margin-left:113px;display:block;height: 22px;font-size: 14px;font-weight: 400;color: #FFB800;line-height: 22px;margin-bottom: 15px;margin-top: 5px;">
                                                                如何获取设备ID
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="layui-form-item">
                                                        <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                            <span style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                            商户类型
                                                        </label>
                                                        <div class="layui-input-block">
                                                            <input type="radio" name="radio" value="" title="" checked><text>直连商户</text>
                                                        </div>
                                                    </div>
                                                    <div class="layui-form-item">
                                                        <label style="display: block;margin-left: 111px; padding-bottom: 8px;">
                                                            <span style="color:#f5222d;margin-right: 4px;font-size: 18px;">*</span>
                                                            支付宝2088/pid
                                                        </label>
                                                        <div class="layui-input-block">
                                                            <input type="text" name="legal_persona_wechat" lay-verify="title" autocomplete="off" placeholder="请输入" class="layui-input" style="width: 552px;">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="button" style="margin-left: 9%;">
                                                    <button type="submit" class="layui-btn" lay-submit="" lay-filter="submitBind" id="submitBind">提交绑定</button>
                                                </div>
                                            </form>
                                        </div>
                                        <!--如何获取设备id提示框-->
                                        <div id="open_button" class="hide" style="display: none;background-color: #fff;">
                                            <div class="layui-card-body" style="padding: 15px;">
                                                <div class="layui-form">
                                                    <div class="layui-form-item box">
                                                        <p>获得设备id</p>
                                                        <text style="color:#108ee9;">方法一:电脑端</text>
                                                        <p>登录支付宝服务商平台，进入顶部“运营中心”>左侧导航栏“经营工具”>“商家职能设备”>“马上去智能设备工作台开展作业”>“设备管理，查看设备管理列表，点击对应的设备编号里的操作“查看”，在“设备基础信息”里的“设备biztid”后的编号为设备id（注意：一般是01010开头），复制到本平台粘贴即可。</p>
                                                        <p>获得设备id</p>
                                                        <text style="color:#108ee9;">方法二：设备上查看</text>
                                                        <p>登录支付宝服务商平台，进入顶部“运营中心”>左侧导航栏“经营工具”>“商家职能设备”>“马上去智能设备工作台开展作业”>“设备管理，查看设备管理列表，点击对应的设备编号里的操作“查看”，在“设备基础信息”里的“设备biztid”后的编号为设备id（注意：一般是01010开头），复制到本平台粘贴即可。</p></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <input type="hidden" class="source_type">
    <input type="hidden" class="source_type_desc">
    <input type="hidden" class="user_id">
    <input type="hidden" class="daili_id">
    <input type="hidden" class="daili_name">
    <input type="hidden" class="store_id">
    <input type="hidden" class="store_name">



    <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
    <script>
        var token = sessionStorage.getItem("Usertoken");
        var str = location.search;
        var user_id = str.split('?')[1];


        layui.config({
            base: '../../layuiadmin/' //静态资源所在路径
        }).extend({
            index: 'lib/index', //主入口模块
            steps: './extends/steps/steps'
        }).use(['index', 'form', 'table', 'laydate', 'steps'], function() {
            var $ = layui.$,
                admin = layui.admin,
                form = layui.form,
                table = layui.table,
                laydate = layui.laydate;
            // 未登录,跳转登录页面
            $(document).ready(function() {
                if (token == null) {
                    window.location.href = "{{url('/user/login')}}";
                }

            })

            //步骤条
            layui.use('steps', function() {
                var steps = layui.steps;
                steps.render({
                    ele: '#steps',
                    data: [{
                            'title': "第一步",
                            "desc": "代运营授权"
                        },
                        {
                            'title': "第二步",
                            "desc": "代运营邀请"
                        },
                        {
                            'title': "第三步",
                            "desc": "绑定门店"
                        },
                        {
                            'title': "第四步",
                            "desc": "IOT授权"
                        }
                    ], //desc 不是必须
                    // current: 0 //默认为第几步
                });
                /**
                 * 代运营授权
                 */
                form.on('submit(addwechat)', function(data) {
                    steps.next(); //下一步
                    $("#addwechatapp").css("cssText", "display:none !important;");
                    $("#stepTwo").show();
                })
                /**
                 * 代运营邀请
                 */
                form.on('submit(authentication)', function(data) {
                    steps.next(); //下一步
                    $("#addwechatapp").css("cssText", "display:none !important;");
                    $("#stepTwo").css("cssText", "display:none !important;");
                    $("#stepThree").show();
                })
                 /**
                 * 绑定门店
                 */
                form.on('submit(jueryStore)', function(data) {
                    steps.next(); //下一步
                    $("#addwechatapp").css("cssText", "display:none !important;");
                    $("#stepTwo").css("cssText", "display:none !important;");
                    $("#stepThree").css("cssText", "display:none !important;");
                    $("#stepFour").show();
                })
            })


                /**
                 * 点击代运营邀请图片放大
                 */
                $('#photo-list img').on('click', function () {
                    layer.photos({
                        photos: '#photo-list',
                        shadeClose: false,
                        closeBtn: 2,
                        anim: 0
                    });
                })
                /**
                 * 点击绑定门店图片放大
                 */
                $('#photo-store img').on('click', function () {
                    layer.photos({
                        photos: '#photo-store',
                        shadeClose: false,
                        closeBtn: 2,
                        anim: 0
                    });
                })

                /**
                 * 点击如何获取设备id提示
                 */
                $('.deviceId').on('click', function () {
                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_button')
                    });
                })
                /**
                 * 绑定门店有门店的时候显示门店列表
                 */
                // 渲染表格
                table.render({
                    elem: '#test-table-page',
                    url: "{{url('/api/merchant/goodsCategoryList')}}",
                    method: 'get',
                    where: {
                        token: token,
                        storeId: store_id,
                        sortMethod: "asc"
                    }
                    ,request:{
                    pageName: 'p',
                    limitName: 'l'
                    }
                    ,cellMinWidth: 150
                    ,cols: [
                        [
                        {field: 'store_id', align: 'center',title: '门店ID'},
                        {field: 'name',title: '支付蚂蚁门店',align: 'center',templet: '#statusTemp',sort: true},
                        {field: 'store_name',title: '门店名称',align: 'center',edit: 'text',sort: true},
                        ]
                    ],
                    page: true,
                    response: {
                        statusName: 'status' //数据状态的字段名称，默认：code
                       ,statusCode: 1 //成功的状态码，默认：0
                       ,msgName: 'message' //状态信息的字段名称，默认：msg
                       ,countName: 't' //数据总数的字段名称，默认：count
                       ,dataName: 'data' //数据列表的字段名称，默认：data
                    },
                    done: function(res, curr, count) {
                        $('th').css({'font-weight': 'bold','font-size': '15','color': 'black','background': 'linear-gradient(#f2f2f2,#cfcfcf)'}); //进行表头样式设置
                        form.render();
                    },


                });


        });
    </script>

</body>

</html>