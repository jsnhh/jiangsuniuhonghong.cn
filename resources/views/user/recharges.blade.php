<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>商户充值设置</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card" style="margin-top:0px">
        <div class="layui-card-header">门店名称:<span></span></div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">

                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center">月包</label>
                    <div class="layui-input-block" style="width:597px">
                        <input type="number" lay-verify="title" autocomplete="off" placeholder="请输入月包金额" class="layui-input amount">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center">季包</label>
                    <div class="layui-input-block" style="width:597px">
                        <input type="number" lay-verify="title" autocomplete="off" placeholder="请输入季包金额" class="layui-input quarterly_amount">
                    </div>
                </div>
                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit" style="border-radius:5px">确定提交</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="user_id">
<input type="hidden" class="status">
<input type="hidden" class="js_store_id">
<input type="hidden" class="js_id">
<input type="hidden" class="js_store_name">
<input type="hidden" class="open_id" value="0">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");

    var merchant_id="{{$_GET['merchant_id']}}";
    var store_name="{{$_GET['store_name']}}";
    var user_id="{{$_GET['user_id']}}";


    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;

        var src=$('#demo1').attr('src');

        $('.layui-card-header span').html(store_name);

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });

        getBoards();

        function getBoards(){
            $.post("{{url('/api/user/merchant_payments_list')}}",
                {
                    token:token,
                    user_id: user_id,
                    merchant_id: merchant_id,

                },function(res){

                    if(res.status==1){
                        $('.amount').val(res.data.amount);
                        $('.quarterly_amount').val(res.data.quarterly_amount);
                    }
                },"json");

        }

        $('.submit').on('click', function(){
            $.post("{{url('/api/user/merchant_payments_list')}}",
                {
                    token:token,
                    amount:$('.amount').val(),
                    quarterly_amount:$('.quarterly_amount').val(),
                    user_id: user_id,
                    merchant_id: merchant_id,
                    type_value: '03'

                },function(res){

                    if(res.status==1){
                        getBoards();
                    }
                },"json");
        });

    });

</script>
</body>
</html>
