<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>新蓝海报名更新</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .tongbu {
            background-color: #4c9ef8;
            color: #fff;
        }

        .cur {
            color: #009688;
        }

        .userbox,
        .storebox {
            height: 200px;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 63px;
            width: 498px;
            background-color: #ffffff;
            border: 1px solid #ddd;
        }

        .userbox .list,
        .storebox .list {
            height: 38px;
            line-height: 38px;
            cursor: pointer;
            padding-left: 10px;
        }

        .userbox .list:hover,
        .storebox .list:hover {
            background-color: #eeeeee;
        }

        .s_id {
            line-height: 36px;
        }

        .layui-form-radio {
            margin-left: 10px;
            margin-bottom: 7px;
        }
        .img{width:130px;height:90px;overflow: hidden;}
        .img img{width:100%;height:100%;}
        .layui-layer-nobg{width: none !important;}
        /*.layui-layer-content{width:600px;height:550px;}*/
        .layui-card-header{width:80px;text-align: right;float:left;}
        .layui-card-body{margin-left:28px;}
        .layui-upload-img{width: 100px; height: 92px; /*margin: 0 10px 10px 0;*/}
        .up{position: relative;display: inline-block;cursor: pointer;border-color: #1ab394; color: #FFF;width: auto !important;font-size: 10px !important;text-align: center !important;}
        .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
        .layui-upload-list{width: 100px;height:96px;overflow: hidden;margin: 10px auto;}
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {-webkit-appearance: none !important;margin: 0;}
        .userbox,.branchbox{
            height:200px;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 85px;
            width:400px;
            background-color:#ffffff;
            border: 1px solid #ddd;
        }
        .userbox .list,.branchbox .list{
            height:38px;line-height: 38px;cursor:pointer;
            padding-left:10px;
        }
        .userbox .list:hover,.branchbox .list:hover{
            background-color:#eeeeee;
        }
    </style>
</head>

<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">

            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12" style="margin-top:0px">
                        <div class="layui-card">
                            <div class="layui-card-header">新蓝海报名</div>

                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">

                                    <!-- 新蓝海报名 -->
                                    <div class="layui-form" style="line-height: 30px;">
                                        <div class="layui-form-item">
                                            <label style="margin-left: 10px;">新蓝海活动的场景</label>
                                            <input type="radio" name="radio" checked style="margin-left: 10px;margin-bottom:7px;">
                                            <text style="margin-left: -20px;">直连商户</text>
                                        </div>
                                    </div>

                                    <!-- 新蓝海活动类别 -->
                                    <div class="layui-form-item" style="display: flex;">
                                        <div class="layui-form" lay-filter="component-form-group">
                                            <label style="margin-left: 10px;">新蓝海活动类别</label>
                                            <div class="layui-form-item">
                                                <div class="layui-input-block" style="width:200px;margin-left: 130px;margin-top: -28px;">
                                                    <select name="first" lay-filter="first" id="first">
                                                        <option value="">请选择类型</option>
                                                        <option value="BLUE_SEA_FOOD_APPLY">直连餐饮</option>
                                                        <option value="BLUE_SEA_FMCG_APPLY">直连快消</option>
                                                        {{--                                                        <option value="BLUE_SEA_FOOD_INDIRECT_APPLY">间连餐饮</option>--}}
                                                        {{--                                                        <option value="BLUE_SEA_FMCG_INDIRECT_APPLY">间连快消</option>--}}
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="layui-form" lay-filter="component-form-group">
                                            <div class="layui-form-item">
                                                <div class="layui-input-block" style="width:300px;margin-top: -6px;margin-left: 20px;">
                                                    <select name="second" lay-filter="second" id="second">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- 支付宝账号 -->
                                    <div class="layui-form-item" style="margin-left: 14px;margin-bottom:25px;">
                                        <label class="layui-form-label">支付宝账号</label>
                                        <div class="layui-input-block">
                                            <input type="text" placeholder="请输入" autocomplete="off" class="layui-input merchant_no" style="width: 530px;float: left;margin-right: 10px;">
                                        </div>
                                    </div>
                                    <!-- 平台id -->

                                    <div class="layui-form-item" style="margin-left: 14px;">
                                        <label class="layui-form-label">平台id</label>
                                        <div class="layui-input-block">
                                            <input type="text" placeholder="请输入" autocomplete="off" class="layui-input store_id" style="width: 530px;float: left;margin-right: 10px;">
                                        </div>
                                    </div>

                                    <div class="layui-col-md6">
                                        <div class="layui-form-item" style="width:100%">
                                            <div class="layui-card">
                                                <div class="layui-card-body public" id="imgone" style="margin-left:28px;padding:0 50px;float:left;">
                                                    <div class="layui-upload">
                                                        <button class="layui-btn up" style="border-radius:5px;padding-left:7px;padding-right:7px;left:4px"><input type="file" name="img_upload" class="test20">餐饮服务许可证</button>
                                                        <div class="layui-upload-list">
                                                            <img class="layui-upload-img" id="demo20">
                                                            <p id="demoText"></p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="layui-card-body public" id="imgtow" style="margin-left:28px;padding:0 50px;float:left;">
                                                    <div class="layui-upload">
                                                        <button class="layui-btn up" style="border-radius:5px;padding-left:7px;padding-right:7px;left:4px"><input type="file" name="img_upload" class="test21">食品卫生许可证</button>
                                                        <div class="layui-upload-list">
                                                            <img class="layui-upload-img" id="demo21">
                                                            <p id="demoText"></p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="layui-card-body public" id="imgthree" style="margin-left:28px;padding:0 50px;float:left;">
                                                    <div class="layui-upload">
                                                        <button class="layui-btn up" style="border-radius:5px;padding-left:7px;padding-right:7px;left:4px"><input type="file" name="img_upload" class="test22">食品经营许可证</button>
                                                        <div class="layui-upload-list">
                                                            <img class="layui-upload-img" id="demo22">
                                                            <p id="demoText"></p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="layui-card-body public" id="imgfor" style="margin-left:28px;padding:0 50px;float:left;">
                                                    <div class="layui-upload">
                                                        <button class="layui-btn up" style="border-radius:5px;padding-left:7px;padding-right:7px;left:4px"><input type="file" name="img_upload" class="test23">食品流通许可证</button>
                                                        <div class="layui-upload-list">
                                                            <img class="layui-upload-img" id="demo23">
                                                            <p id="demoText"></p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="layui-card-body public" id="imgfive" style="margin-left:28px;padding:0 50px;float:left;">
                                                    <div class="layui-upload">
                                                        <button class="layui-btn up" style="border-radius:5px;padding-left:7px;padding-right:7px;left:4px"><input type="file" name="img_upload" class="test24">食品生产许可证</button>
                                                        <div class="layui-upload-list">
                                                            <img class="layui-upload-img" id="demo24">
                                                            <p id="demoText"></p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="layui-card-body public" id="yancao" style="margin-left:28px;padding:0 50px;float:left;">
                                                    <div class="layui-upload">
                                                        <button class="layui-btn up" style="border-radius:5px;padding-left:7px;padding-right:7px;left:4px"><input type="file" name="img_upload" class="test25">烟草许可证</button>
                                                        <div class="layui-upload-list">
                                                            <img class="layui-upload-img" id="demo25">
                                                            <p id="demoText"></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-form-item" id="subbutton">
                                            <div class="layui-input-block" style="margin-left:440px">
                                                <div class="layui-footer" style="left: 0;">
                                                    <button class="layui-btn submit5" lay-submit lay-filter="submit5" style="border-radius:5px">保存</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="layui-form-item layui-layout-admin">
                                        <div class="layui-input-block">
                                            <div class="layui-footer" style="left: 0;">
                                                <button class="layui-btn submit site-demo-active" style="border-radius:5px" data-type="tabChange">确认报名</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="biz_scene">
<input type="hidden" class="biz_scene_name">
<input type="hidden" class="source_type">
<input type="hidden" class="source_type_desc">
<input type="hidden" class="user_id">
<input type="hidden" class="daili_id">
<input type="hidden" class="daili_name">
<input type="hidden" class="store_name">
<input type="hidden" class="order_id">
<input type="hidden" class="app_id">




<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var str = location.search;
    var id="{{$_GET['id']}}";
    {{--var store_id="{{$_GET['store_id']}}";--}}
    // console.log(store_id);
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index',//主入口模块
        formSelects: 'formSelects'

    }).use(['index', 'form', 'table', 'laydate','formSelects','upload'], function() {
        var $ = layui.$,
            admin = layui.admin,
            form = layui.form,
            table = layui.table,
            upload = layui.upload,
            formSelects = layui.formSelects,
            laydate = layui.laydate;
        // 未登录,跳转登录页面
        $(document).ready(function() {
            if (token == null) {
                window.location.href = "{{url('/user/login')}}";
            }
        })
        $("#yancao").hide();
        $("#imgone").hide();
        $("#imgtow").hide();
        $("#imgthree").hide();
        $("#imgfor").hide();
        $("#imgfive").hide();
        $("#yancao").hide();
        $("#subbutton").hide();
        // $('.store_id').val(store_id);
        $.post("{{url('/api/alipayopen/blueseaInfo')}}",
            {
                token:token,
                id:id
            },function(res){
           console.log(res);
                $('.order_id').val(res.data.order_id);
                $('.app_id').val(res.data.app_id);
                $('.merchant_no').val(res.data.merchant_no);
                $('.store_id').val(res.data.store_id);
//                 $('.version').val(res.data.version);
//                 $('.msg').val(res.data.msg);
//                 $('#demo1').val(res.data.UpdateUrl);
            },"json");
        form.on('select(first)', function(data){
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.biz_scene').val(category);
            $('.biz_scene_name').val(categoryName);
            $("#second").html('');

            // if(category == "BLUE_SEA_FOOD_APPLY"){
            //       $("#yancao").hide();
            //     }else{
            //     $("#imgone").hide();
            //     // $("#addwechatapp").css("cssText", "display:none !important;");
            //     $("#imgtow").hide();
            //     $("#imgthree").hide();
            //     $("#imgfor").hide();
            //     $("#imgfive").hide();
            //     $("#yancao").hide();
            //     $("#subbutton").hide();
            // }
            $.ajax({
                url : "{{url('/api/alipayopen/shopType')}}",
                data : {biz_scene:category,token:token},
                type : 'get',
                success : function(data) {
                    console.log(data);
                    var optionStr = "";
                    for(var i=0;i<data.data.length;i++){
                        optionStr += "<option value='" + data.data[i].shop_value + "'>"
                            + data.data[i].shop_type + "</option>";
                    }
                    $("#second").append('<option value="">请选择店铺</option>'+optionStr);
                    layui.form.render('select');
                },
                error : function(data) {
                    alert('查找板块报错');
                }
            });
        });
        {{--$.post("{{url('/api/alipayopen/zfbAccount')}}",--}}
        {{--    {--}}
        {{--        token:token,--}}
        {{--        store_id:store_id--}}
        {{--    },function(res){--}}
        {{--        // console.log(res);--}}
        {{--        $('.merchant_no').val(res.data.alipay_account);--}}
        {{--    },"json");--}}
        form.on('select(second)', function(data){
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.source_type').val(category);
            $('.source_type_desc').val(categoryName);
            console.log(category,categoryName)
            if(category == "5499" && categoryName == "其他食品零售"){

                $("#imgone").show();
                $("#imgtow").show();
                $("#imgthree").show();
                $("#imgfor").show();
                $("#imgfive").show();
                $("#subbutton").show();
            }else{
                $("#imgone").hide();
                $("#imgtow").hide();
                $("#imgthree").hide();
                $("#imgfor").hide();
                $("#imgfive").hide();
                $("#yancao").hide();
                $("#subbutton").hide();

            }
            if(category == "5993"){
                $("#yancao").show();
                $("#subbutton").show();

            }
        });

        $('.submit').on('click', function(){
            $.post("{{url('/api/alipayopen/iotBlueseaUp')}}",
                {
                    token:token,
                    store_id:$('.store_id').val(),
                    merchant_no:$('.merchant_no').val(),
                    order_id:$('.order_id').val(),
                    biz_scene:$('.biz_scene').val(),
                    type:$('.source_type').val(),
                },function(res){
                    console.log(res);
                    if(res.status==1){
                        layer.msg(res.message, {
                            offset: '15px'
                            ,icon: 1
                            ,time: 2000
                        });
                    }else{
                        layer.msg(res.message, {
                            offset: '15px'
                            ,icon: 2
                            ,time: 2000
                        });
                    }
                },"json");
        });

        //餐饮服务许可证
        var uploadInst = upload.render({
            url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token,  //提交到的地址 可以自定义其他参数
            elem : '.test20',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            ext : 'jpg|png|gif',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                console.log(res);
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    layui.jquery('#demo20').attr("src", res.data.img_url);
                }
                //console.log(res); //上传成功返回值，必须为json格式
            }
        });

        //食品卫生许可证
        var uploadInst = upload.render({
            url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token,  //提交到的地址 可以自定义其他参数
            elem : '.test21',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            ext : 'jpg|png|gif',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                console.log(res);
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    layui.jquery('#demo21').attr("src", res.data.img_url);
                }
                //console.log(res); //上传成功返回值，必须为json格式
            }
        });
        //食品经营许可证
        var uploadInst = upload.render({
            url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token,  //提交到的地址 可以自定义其他参数
            elem : '.test22',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            ext : 'jpg|png|gif',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                console.log(res);
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    layui.jquery('#demo22').attr("src", res.data.img_url);
                }
                //console.log(res); //上传成功返回值，必须为json格式
            }
        });

        // 食品流通许可证
        var uploadInst = upload.render({
            url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token,  //提交到的地址 可以自定义其他参数
            elem : '.test23',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            ext : 'jpg|png|gif',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                console.log(res);
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    layui.jquery('#demo23').attr("src", res.data.img_url);
                }
                //console.log(res); //上传成功返回值，必须为json格式
            }
        });

        // 食品生产许可证
        var uploadInst = upload.render({
            url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token,  //提交到的地址 可以自定义其他参数
            elem : '.test24',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            ext : 'jpg|png|gif',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                console.log(res);
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    layui.jquery('#demo24').attr("src", res.data.img_url);
                }
                //console.log(res); //上传成功返回值，必须为json格式
            }
        });

        // 烟草许可证
        var uploadInst = upload.render({
            url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token,  //提交到的地址 可以自定义其他参数
            elem : '.test25',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            ext : 'jpg|png|gif',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                console.log(res);
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    layui.jquery('#demo25').attr("src", res.data.img_url);
                }
                //console.log(res); //上传成功返回值，必须为json格式
            }
        });

        $('.submit5').click(function(){
            $.post("{{url('/api/user/up_store')}}",
                {
                    token:token,
                    store_id:store_id,
                    food_service_lic:$('#demo20').attr('src'),
                    food_health_lic:$('#demo21').attr('src'),
                    food_business_lic:$('#demo22').attr('src'),
                    food_circulate_lic:$('#demo23').attr('src'),
                    food_production_lic:$('#demo24').attr('src'),
                    tobacco_img:$('#demo25').attr('src'),
                },function(res){
                    console.log(res);
                    if(res.status==1){
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 3000
                        });
                    }else{
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 3000
                        });
                    }
                },"json");
        });

    });






</script>

</body>

</html>