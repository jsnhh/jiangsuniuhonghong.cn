<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>修改广告链接</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
        .layui-card-header{width:80px;text-align: right;float:left;}
        .layui-card-body{margin-left:28px;}
        .layui-upload-img{width: 92px; height: 92px; margin: 0 10px 10px 0;}
        .up{position: relative;display: inline-block;cursor: pointer;border-color: #1ab394; color: #FFF;width: 92px !important;font-size: 10px !important;text-align: center !important;}
        .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
        .layui-upload-list{width: 100px;height:96px;overflow: hidden;}
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {-webkit-appearance: none !important;margin: 0;}
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card" style="margin-top:0px">
        <div class="layui-card-header" style="width:auto !important">修改广告链接&nbsp;&nbsp;&nbsp;<span class="zong_school_name"></span></div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item school">
                    <label class="layui-form-label">使用门店</label>
                    <div class="layui-input-block">
                        <select name="store" id="store" lay-filter="store">
                        </select>
                    </div>
                </div>
                <div class="layui-form-item type2">
                    <label class="layui-form-label" style="text-align:center">链接</label>
                    <div class="layui-input-block line">
                        <div class="layui-btn" style='display: inline-block;cursor: pointer;' id='addlj'>添加</div>
                    </div>
                </div>
                <div class="layui-form-item type2" style="width:500px">
                    <label class="layui-form-label"></label>
                    <div class="layui-input-block lianjiebox">

                    </div>
                </div>


                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit site-demo-active" style="border-radius:5px" data-type="tabChange">确定提交</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="storeid">
<input type="hidden" class="storename">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var str=location.search;
    var id=str.split('?')[1];

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','element'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;

        element.render();
        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });
        // 选择门店
        $.ajax({
            url : "{{url('/api/user/store_lists')}}",
            data : {token:token,l:100},
            type : 'post',
            success : function(data) {
//                console.log(data);
                var optionStr = "";
                for(var i=0;i<data.data.length;i++){
                    optionStr += "<option value='" + data.data[i].store_id + "'>"
                        + data.data[i].store_name + "</option>";
                }
                $("#store").html('');
                $("#store").append('<option value="">选择门店</option>'+optionStr);
                layui.form.render('select');
            },
            error : function(data) {
                alert('查找板块报错');
            }
        });

        form.on('select(store)', function(data){
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.storeid').val(category);
            $('.storename').val(categoryName);
        });
        $('#addlj').click(function(){
            var str='';
            str+='<div class="lianjiecon">';
            str+='<div>';
            str+='<label>键</label>';
            str+='<input type="text" class="layui-input ljitem1">';
            str+='</div>';
            str+='<div>';
            str+='<label>值</label>';
            str+='<input type="text" class="layui-input ljitem2">';
            str+='</div>';
            str+='</div>';

            $('.lianjiebox').append(str)
        });

        //加载页面参数
        $.post("{{url('/api/adlinks/adlinksInfo')}}",
            {
                token:token,
                id:id
            },function(res){
                // $('.taobaoname').val(res.data.taobaoUrl);
                // $('.pinduoduoname').val(res.data.pinduoduoUrl);
                // $('.douyinname').val(res.data.douyinUrl);
                // $('.provincecode').val(res.data.store_id);
                $('.storeid').val(res.data.store_id);
                $('.storename').val(res.data.store_name);
                $.ajax({
                    url : "{{url('/api/user/store_lists')}}",
                    data : {token:token,l:100},
                    type : 'post',
                    success : function(data) {
                        console.log(data);
                        console.log(data);
                        var optionStr = "";
                        for(var i=0;i<data.data.length;i++){
                            optionStr += "<option value='" + data.data[i].store_id + "' "+((res.data.store_id==data.data[i].store_id)?"selected":"")+">"
                                + data.data[i].store_name + "</option>";
                        }
                        $("#store").html('');
                        $("#store").append('<option value="">选择门店</option>'+optionStr);
                        layui.form.render('select');

                    },
                    error : function(data) {
                        alert('查找板块报错');
                    }
                });
                var str1 = res.data.links;
                var data1 = JSON.parse(str1);
                console.log(data1[0]['url_details']);

                var str='';
                for(var m=0;m<data1.length;m++){

                    str+='<div class="lianjiecon">';
                    str+='<div>';
                    str+='<label>键</label>';
                    str+='<input type="text" class="layui-input ljitem1" value="'+data1[m].url_name+'"'+'>';
                    str+='</div>';
                    str+='<div>';
                    str+='<label>值</label>';
                    str+='<input type="text" class="layui-input ljitem2" value="'+data1[m].url_details+'"'+'>';
                    str+='</div>';
                    str+='</div>';

                }
                console.log(str);

                $('.lianjiebox').append(str)


            },"json");

        $('.submit').on('click', function(){
            var adarrl=[];
            var daTal={"url_name":"","url_details":""};
            $('.lianjiebox .lianjiecon').each(function(index,item){
                var url_name=$(item).find('input.ljitem1').val();
                var url_details=$(item).find('input.ljitem2').val();
                if(url_name!=''){
                    var data = {"url_name":url_name,"url_details":url_details}; //构造数组
                    adarrl.push(data);
                }
            });

            var adarrJsonl=JSON.stringify(adarrl); //转化成json格式
            $.post("{{url('/api/adlinks/adlinksUp')}}",
                {
                    token:token,
                    id:id,
                    store_id:$('.storeid').val(),
                    store_name:$('.storename').val(),
                    links:adarrJsonl,
                },function(res){
//                console.log(res);
                    if(res.status==170003){
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 3000
                        });
                    }else{
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 3000
                        });
                    }
                },"json");
        });
    });
</script>

</body>
</html>
