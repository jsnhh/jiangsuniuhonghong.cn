<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>HL支付配置</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header">HL支付配置</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item xingzhi">
                    <label class="layui-form-label">渠道</label>
                    <div class="layui-input-block">
                        <select name="xingzhi" id="xingzhi" lay-filter="xingzhi">
                            <option value="1">渠道1</option>
                            <option value="2">渠道2</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">mch_id</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">md_key</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                
                <div class="layui-form-item">
                    <label class="layui-form-label">pri_key</label>
                    <div class="layui-input-block">
                        <textarea name="desc" placeholder="" class="layui-textarea private_key"></textarea>
                    </div>
                </div>                
                <div class="layui-form-item">
                    <label class="layui-form-label">pub_key</label>
                    <div class="layui-input-block">
                        <textarea name="desc" placeholder="" class="layui-textarea public_key"></textarea>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">微信wx_appid</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolshortname" lay-verify="schoolshortname" autocomplete="off" placeholder="" class="layui-input">                        
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">微信密钥wx_secret</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolshortname" lay-verify="schoolshortname" autocomplete="off" placeholder="" class="layui-input">                        
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">微信渠道号</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                
                <div class="layui-form-item">
                    <label class="layui-form-label">支付宝pid</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolshortname" lay-verify="schoolshortname" autocomplete="off" placeholder="" class="layui-input">                        
                    </div>
                </div>
                

                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit site-demo-active" data-type="tabChange">保存</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="qd" value="1">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script> 
<script>
    var token = sessionStorage.getItem("Usertoken");
    

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','element'], function(){
        var $ = layui.$ 
            ,admin = layui.admin
            ,element = layui.element
            ,form = layui.form
    // 未登录,跳转登录页面
    $(document).ready(function(){        
        if(token==null){
            window.location.href="{{url('/user/login')}}"; 
        }
    })
    $.post("{{url('/api/user/tf_config')}}",
    {
        token:token,
        getdata:"1",
        qd:"1"

    },function(res){
        console.log(res);
        $('.layui-form .layui-form-item').eq(1).find('input').val(res.data.mch_id);
        $('.layui-form .layui-form-item').eq(2).find('input').val(res.data.md_key);
        $('.layui-form .layui-form-item').eq(3).find('textarea').val(res.data.pri_key);
        $('.layui-form .layui-form-item').eq(4).find('textarea').val(res.data.pub_key);
        $('.layui-form .layui-form-item').eq(5).find('input').val(res.data.wx_appid);
        $('.layui-form .layui-form-item').eq(6).find('input').val(res.data.wx_secret);
        $('.layui-form .layui-form-item').eq(7).find('input').val(res.data.wechat_channel_no);
        $('.layui-form .layui-form-item').eq(8).find('input').val(res.data.alipay_pid);

        $('.qd').val(res.data.qd)


    },"json");
        
    // 性质
    form.on('select(xingzhi)', function(data){            
        category = data.value;  
        categoryName = data.elem[data.elem.selectedIndex].text; 
        $('.qd').val(category);  
        $.post("{{url('/api/user/tf_config')}}",
        {
            token:token,
            getdata:"1",
            qd:category

        },function(res){
            console.log(res);
            $('.layui-form .layui-form-item').eq(1).find('input').val(res.data.mch_id);
            $('.layui-form .layui-form-item').eq(2).find('input').val(res.data.md_key);
            $('.layui-form .layui-form-item').eq(3).find('textarea').val(res.data.pri_key);
            $('.layui-form .layui-form-item').eq(4).find('textarea').val(res.data.pub_key);
            $('.layui-form .layui-form-item').eq(5).find('input').val(res.data.wx_appid);
            $('.layui-form .layui-form-item').eq(6).find('input').val(res.data.wx_secret);
            $('.layui-form .layui-form-item').eq(7).find('input').val(res.data.wechat_channel_no);
            $('.layui-form .layui-form-item').eq(8).find('input').val(res.data.alipay_pid);

        },"json");     
    });

    $('.submit').on('click', function(){
        console.log($('.private_key').val());
        $.post("{{url('/api/user/tf_config')}}",
        {
            token:token,
            getdata:"2",
            qd:$('.qd').val(), 
            mch_id:$('.layui-form .layui-form-item').eq(1).find('input').val(),
            md_key:$('.layui-form .layui-form-item').eq(2).find('input').val(),
            pri_key:$('.layui-form .layui-form-item').eq(3).find('textarea').val(),
            pub_key:$('.layui-form .layui-form-item').eq(4).find('textarea').val(),
            wx_appid:$('.layui-form .layui-form-item').eq(5).find('input').val(),
            wx_secret:$('.layui-form .layui-form-item').eq(6).find('input').val(),
            wechat_channel_no:$('.layui-form .layui-form-item').eq(7).find('input').val(),
            alipay_pid:$('.layui-form .layui-form-item').eq(8).find('input').val(),
        },function(res){
            console.log(res);

            if(res.status==1){
                layer.msg(res.message, {
                    offset: '15px'
                    ,icon: 1
                    ,time: 3000
                });
            }else{
                layer.alert(res.message, {icon: 2});
            }

        },"json");

    });

    

    });
</script>

</body>
</html>
