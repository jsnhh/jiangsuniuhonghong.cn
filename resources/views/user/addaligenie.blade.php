<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>绑定天猫精灵</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
<link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
<style>
    .layui-card-header{width:80px;text-align: right;float:left;}
    .layui-card-body{margin-left:28px;}
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {-webkit-appearance: none !important;margin: 0;}
</style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card" style="margin-top:0px">
        <div class="layui-card-header" style="width:auto !important">绑定天猫精灵&nbsp;&nbsp;&nbsp;<span class="zong_school_name"></span></div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item">
                    <label class="layui-form-label">门店名称</label>
                    <div class="layui-input-block">
                        <div class="store_name" style="line-height: 36px;"></div>
                    </div>
                </div>
                <div class="layui-form-item school">
                    <label class="layui-form-label">激活码</label>
                    <div class="layui-input-block">
                        <input type="text" name="store" id="storeid" style="line-height: 25px;">
                    </div>
                </div>
                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit site-demo-active" data-type="tabChange">确定提交</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="storeid">
<input type="hidden" class="storename">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script> 
<script>
    var token = sessionStorage.getItem("Usertoken");
    // var str=location.search;
    // var school_name=str.split('?')[1];
    var store_id="{{$_GET['store_id']}}";
    var store_name="{{$_GET['store_name']}}";

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','element'], function(){
        var $ = layui.$ 
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });

        var src=$('#demo1').attr('src');
        element.render();
        $('.store_name').html(store_name);
        
        $('.submit').on('click', function(){
            $.post("{{url('/api/aligenie/addAliGenie')}}",
            {
                token:token
                ,storeId:store_id
                ,activationCode:$('#storeid').val()
            },function(res){
//                console.log(res);
                if(res.status==150002){
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 3000
                    });                    
                }else{
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 3000
                    });
                }
            },"json");
        });

    });

</script>

</body>
</html>
