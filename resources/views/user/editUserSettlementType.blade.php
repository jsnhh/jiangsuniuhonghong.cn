<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>设置零费率结算方式</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card" style="height: 250px">
                <div class="layui-card-body">
                    <div class="layui-form" lay-filter="component-form-group"
                         style="width:500px;display: inline-block;">
                        <div class="layui-form-item">
                            <label class="layui-form-label">代理商名称</label>
                            <div class="layui-input-block">
                                <input type="text" autocomplete="off" class="layui-input agent_name"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">结算方式</label>
                            <div class="layui-input-block" id="settlement_type">
                                <input type="radio" name="identity" value="1" title="月结" checked="">
                                <input type="radio" name="identity" value="2" title="日结">
                            </div>
                        </div>

                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:0;text-align: center;margin-top:0px;">
                            <button class="layui-btn open" lay-submit="open">确定</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" class="agent_id">
    <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
    <script>
        var token = sessionStorage.getItem("Usertoken");
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        layui.config({
            base: '../../../layuiadmin/' //静态资源所在路径
        }).extend({
            index: 'lib/index' //主入口模块
        }).use(['index', 'form'], function () {
            var $ = layui.$
                , admin = layui.admin
                , element = layui.element
                , form = layui.form;
            var permission_idArr = [];
            form.render();
            // 未登录,跳转登录页面
            $(document).ready(function () {
                if (token == null) {
                    window.location.href = "{{url('/user/login')}}";
                }
            });
            var id = getUrlParam('customer_id')
            var customer_name = decodeURI(escape(getUrlParam('customer_name')))
            $('.agent_name').val(customer_name)
            $('.agent_id').val(id)
            getSettlementType();

            function getSettlementType() {
                var user_id = $('.agent_id').val()
                $.ajax({
                    url: "{{url('/api/user/getSettlementType')}}",
                    type: 'get',
                    data: {token: token, user_id: user_id},
                    success: function (data) {
                        if (data.data != null && data.status == "1") {
                            var value = data.data.settlement_type;
                            if(value==1){
                                $("input[name='identity'][value='1']").prop('checked', 'checked');
                            }else {
                                $("input[name='identity'][value='2']").prop('checked', 'checked');
                            }
                            form.render('radio');
                        }
                    },
                    error: function (data) {

                    }
                });
            }

            $('.open').on('click', function () {
                $.post("{{url('/api/user/editSettlementType')}}",
                    {
                        token: token,
                        user_id: $('.agent_id').val(),
                        settlement_type: $('#settlement_type input[name="identity"]:checked').val()

                    }, function (res) {
                        if (res.status == 1) {
                            parent.layer.msg(res.message, {
                                icon: 1
                                , time: 3000
                            });
                            parent.layer.close(index); //再执行关闭
                        } else {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 2
                                , time: 3000
                            });
                        }
                    }, "json");
            });


            //获取url中的参数
            function getUrlParam(name) {
                var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
                var r = window.location.search.substr(1).match(reg);  //匹配目标参数
                if (r != null) return unescape(r[2]);
                return null; //返回参数值
            }
        });
    </script>
</body>
</html>