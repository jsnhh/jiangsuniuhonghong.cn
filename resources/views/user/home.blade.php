<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>首页</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
<link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
</head>
<style>
.title{
  position :relative;
  top:35%;
};
.current{
background-color:red !importanrt;
}
</style>
<body style="padding:20px;">
<div id="app">
<div style="box-shadow:0px 0px 3px -2px #015;background-color:#ffffff">
<div class="header">
    <div class="Data-overview">
        <div style="padding: 20px; background-color: #ffffff;">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md6" style="padding:0px;padding-right:20px;">
              <div class="layui-card" style="box-shadow:0px 0px 10px #ebeef5">
                <div class="layui-card-header" style="height:60px">
                  <div class="title" style="border-left:5px solid blue;line-height:20px;font-size:18px;padding-left:10px;">
                  数据概览
                  </div>
                </div>
                <div class="layui-card-body">
                  <div class="content" style="display:flex;justify-content:space-between;padding:20px;cursor:pointer;">
                    <div class="item" style="width:150px;height:60px;text-align:center;">
                        <div>
                          交易总额
                        </div>
                        <div class="thesum" style="padding-top:10px;font-size:18px;font-weight: 400;">
                        <span id="acounts1"></span>
                        </div>
                    </div>
                    <div class="item" style="width:150px;height:60px;;text-align:center">
                       <div>
                          佣金总额
                        </div>
                        <div class="thesum" style="padding-top:10px;font-size:18px;font-weight: 400;">
                        <span id="acounts2"></span>
                       </div>
                    </div>
                    <div class="item" style="width:150px;height:60px;text-align:center">
                       <div>
                          二级代理商
                        </div>
                        <div class="thesum" style="padding-top:10px;font-size:18px;font-weight: 400;">
                        <span id="acounts3"></span>
                       </div>
                    </div>
                    <div class="item" style="width:150px;height:60px;text-align:center">
                       <div>
                          三级代理商
                        </div>
                        <div class="thesum" style="padding-top:10px;font-size:18px;font-weight: 400;">
                        <span id="acounts4"></span>
                       </div>
                    </div>
                    <div class="item" style="width:150px;height:60px;text-align:center">
                      <div>
                          总商户数
                        </div>
                        <div class="thesum" style="padding-top:10px;font-size:18px;font-weight: 400;">
                        <span id="acounts5"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="layui-col-md6" style=";padding:0px;padding-left:20px">
              <div class="layui-card" style="box-shadow:0px 0px 10px #ebeef5">
                <div class="layui-card-header" style="height:60px">
                <div class="title" style="border-left:5px solid blue;line-height:20px;font-size:18px;padding-left:10px">
                最新公告
                  </div>
                </div>
                <div class="layui-card-body">
                <div class="permission_lists">
                    <!-- <ul class="lists">

                    </ul> -->
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>

<div class="transaction-data">
    <div class="Data-overview">
        <div style="padding: 20px; background-color: #ffffff;">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12" style="padding:0px">
              <div class="layui-card" style="box-shadow:0px 0px 10px #ebeef5">
                <div class="layui-card-header" style="height:60px">
                  <div class="title" style="border-left:5px solid blue;line-height:20px;font-size:18px;padding-left:10px">
                    交易数据
                  </div>
                  <div class="layui-form" style="float:right;position:relative">
                    <div style="height:38px;display:flex;justify-content:space-between;border:1px solid #ebeef5;margin-bottom:3px;position:relative;top:-9px;border-right:0px;">
                      <button type="button" class="layui-btn layui-btn-normal" id="today" style="border-right:1px solid #ebeef5;">今天</button>
                      <button type="button" class="layui-btn layui-btn-normal" id="yesterday" style="border-right:1px solid #ebeef5;">昨天</button>
                      <button type="button" class="layui-btn layui-btn-normal" id="threeday" style="border-right:1px solid #ebeef5;">3天 </button>
                      <button type="button" class="layui-btn layui-btn-normal" id="sevenday" style="border-right:1px solid #ebeef5;">7天 </button>
                      <button type="button" class="layui-btn layui-btn-normal" id="fifteen" style="border-right:1px solid #ebeef5;">15天</button>
                      <button type="button" class="layui-btn layui-btn-normal" id="thirty" style="border-right:1px solid #ebeef5;">30天</button>
                    </div>
                    <div class="layui-form-item" style="position:absolute;top:-8px;right:485px">
                      <div class="layui-inline">
                        <div class="layui-input-inline" style="display:flex;width:320px;">
                          <!-- <input type="text" style="width: 291px;" class="layui-input" id="transaction-item" placeholder=" - "> -->
                          <input type="text" style="border-radius:5px;width: 200px;margin-right: 50px;" class="layui-input start-item test-item" placeholder="开始时间" lay-key="1">
                          <input type="text" style="border-radius:5px;width: 200px;margin-right: 50px;" class="layui-input end-item test-item" placeholder="结束时间" lay-key="2">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="layui-card-body">
                <div class="content" style="display:flex;justify-content:space-between;padding:20px;cursor:pointer;">
                    <div class="item" style="width:150px;height:60px;text-align:center">
                        <div>
                          交易金额
                        </div>
                        <div style="padding-top:10px;font-size:18px;font-weight: 400;">
                        <span id="transaction1"></span>
                        ￥
                        </div>
                    </div>
                    <div class="item" style="width:150px;height:60px;text-align:center">
                      <div>
                          实际营收
                        </div>
                        <div style="padding-top:10px;font-size:18px;font-weight: 400;">
                        <span id="transaction5"></span>
                        ￥
                      </div>
                    </div>      
                    <div class="item" style="width:150px;height:60px;text-align:center">
                      <div>
                          我的佣金
                        </div>
                        <div style="padding-top:10px;font-size:18px;font-weight: 400;">
                        <span id="transaction6"></span>
                        ￥
                      </div>
                    </div>
                    <div class="item" style="width:150px;height:60px;text-align:center">
                       <div>
                          佣金笔数
                        </div>
                        <div style="padding-top:10px;font-size:18px;font-weight: 400;">
                        <span id="transaction2"></span>
                        ￥
                       </div>
                    </div>
                    <div class="item" style="width:150px;height:60px;text-align:center">
                       <div>
                          退款金额
                        </div>
                        <div style="padding-top:10px;font-size:18px;font-weight: 400;">
                        <span id="transaction3"></span>
                        ￥
                       </div>
                    </div>
                    <div class="item" style="width:150px;height:60px;text-align:center">
                       <div>
                          退款笔数
                        </div>
                        <div style="padding-top:10px;font-size:18px;font-weight: 400;">
                        <span id="transaction4"></span>
                        ￥
                       </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
<div class="transaction-data">
    <div class="Data-overview">
        <div style="padding: 20px; background-color: #ffffff;">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12" style="padding:0px">
              <div class="layui-card" style="box-shadow:0px 0px 10px #ebeef5">
                <div class="layui-card-header" style="height:60px">
                  <div class="title" style="border-left:5px solid blue;line-height:20px;font-size:18px;padding-left:10px">
                  支付通道金额 / 笔数
                  </div>
                  <div class="layui-form" style="float:right;position:relative">
                    <div style="height:38px;display:flex;justify-content:space-between;border:1px solid #ebeef5;margin-bottom:3px;position:relative;top:-9px;border-right:0px;">
                      <button type="button" class="layui-btn layui-btn-normal" id="paytoday" style="border-right:1px solid #ebeef5;">今天</button>
                      <button type="button" class="layui-btn layui-btn-normal" id="payyesterday" style="border-right:1px solid #ebeef5;">昨天</button>
                      <button type="button" class="layui-btn layui-btn-normal" id="paythreeday" style="border-right:1px solid #ebeef5;">3天 </button>
                      <button type="button" class="layui-btn layui-btn-normal" id="paysevenday" style="border-right:1px solid #ebeef5;">7天 </button>
                      <button type="button" class="layui-btn layui-btn-normal" id="payfifteen" style="border-right:1px solid #ebeef5;">15天</button>
                      <button type="button" class="layui-btn layui-btn-normal" id="paythirty" style="border-right:1px solid #ebeef5;">30天</button>
                    </div>
                    <div class="layui-form-item" style="position:absolute;top:-8px;right:485px">
                      <div class="layui-inline">
                        <div class="layui-input-inline" style="display:flex;width:320px;">
                          <input type="text" style="border-radius:5px;width: 200px;margin-right: 50px;" class="layui-input start-item test-item" id="start-item" placeholder="开始时间" lay-key="3">
                          <input type="text" style="border-radius:5px;width: 200px;margin-right: 50px;" class="layui-input end-item test-item" id="end-item" placeholder="结束时间" lay-key="4">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="layui-card-body">
                <div class="content" style="display:flex;justify-content:space-between;padding:20px;cursor:pointer;">
                    <div class="item" style="width:150px;height:50px;text-align:center" id="weixinMoney">
                        <div>
                        微信（金额 / 笔数）
                        </div>
                        <div style="padding-top:10px;font-size:18px;font-weight: 400;">
                        <span id="weixinmoney"></span> /
                        <span id="weixincount"></span>
                        </div>
                    </div>
                    <div class="item" style="width:150px;height:60px;text-align:center" id="alipayMoney">
                       <div>
                       支付宝（金额 / 笔数）
                        </div>
                        <div style="padding-top:10px;font-size:18px;font-weight: 400;">
                        <span id="alipaymoney"></span> /
                        <span id="alipaycount"></span>

                        </div>
                    </div>
                    <div class="item" style="width:150px;height:60px;text-align:center" id="newlandMoney">
                       <div>
                       新大陆（金额 / 笔数）
                        </div>
                        <div style="padding-top:10px;font-size:18px;font-weight: 400;">
                        <span id="newlandmoney"></span> /
                        <span id="newlandcount"></span>

                        </div>
                    </div>
                    <div class="item" style="width:200pxx;height:60px;text-align:center" id="vbillaMoney">
                       <div>
                       随行付A（金额 / 笔数）
                        </div>
                        <div style="padding-top:10px;font-size:18px;font-weight: 400;">
                        <span id="vbillamoney"></span> /
                        <span id="vbillacount"></span>

                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>

<div class="transaction-data">
  <div class="Data-overview">
        <div style="padding: 20px; background-color: #ffffff;">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md6" style="padding:0px;padding-right:20px;">

              <div class="layui-card" style="box-shadow:0px 0px 10px #ebeef5">
                <div class="layui-card-header" style="height:60px">
                <div class="title" style="border-left:5px solid blue;line-height:20px;font-size:18px;padding-left:10px">
                数据统计
                  </div>
                  <div class="layui-form" style="float:right;position:relative">
                    <div class="layui-form-item" style="position:absolute;top:-8px;right:70px">
                      <div class="layui-inline">
                      <div class="layui-input-inline" style="display:flex;width:320px;">
                          <input type="text" style="border-radius:5px;width: 200px;margin-right: 50px;" class="layui-input start-item test-item" id="chartstart-item" placeholder="开始时间" lay-key="5">
                          <input type="text" style="border-radius:5px;width: 200px;margin-right: 50px;" class="layui-input end-item test-item" id="chartend-item" placeholder="结束时间" lay-key="6">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="layui-card-body">
                  <!-- <div style="height:38px;width:396px;display:flex;justify-content:space-between;">
                      <button type="button" class="layui-btn layui-btn-normal" id="charttoday" style="border-right:1px solid #ebeef5;">今天</button>
                      <button type="button" class="layui-btn layui-btn-normal" id="chartyesterday" style="border-right:1px solid #ebeef5;">昨天</button>
                      <button type="button" class="layui-btn layui-btn-normal" id="chartthreeday" style="border-right:1px solid #ebeef5;">3天 </button>
                      <button type="button" class="layui-btn layui-btn-normal" id="chartsevenday" style="border-right:1px solid #ebeef5;">7天 </button>
                      <button type="button" class="layui-btn layui-btn-normal" id="chartfifteen" style="border-right:1px solid #ebeef5;">15天</button>
                      <button type="button" class="layui-btn layui-btn-normal" id="chartthirty" style="border-right:1px solid #ebeef5;">30天</button>
                 </div> -->
                 <div id="main" style="width: 700px;height:400px;"></div>
                </div>
              </div>
            </div>
            <div class="layui-col-md6" style=";padding:0px;padding-left:20px">
              <div class="layui-card" style="box-shadow:0px 0px 10px #ebeef5">
                <div class="layui-card-header" style="height:60px">
                <div class="title" style="border-left:5px solid blue;line-height:20px;font-size:18px;padding-left:10px">
                   支付方式占比
                  </div>
                </div>
                <div class="layui-card-body">
                <div style="height:38px;width:200px;display:flex;justify-content:space-between;">
                      <button type="button" id="moneybutton" value="0" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">交易金额</button>
                      <button type="button" id="countbutton" value="1" class="layui-btn layui-btn-normal" style="border-right:1px solid #ebeef5;">交易笔数</button>
                 </div>
                  <div id="modeproportion" style="width: 800px;height:400px;margin-left:-85px;"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
   </div>
</div>
<!--*******交易数据*******-->
  <input type="hidden" class="starttime"><!-- 今天的开始时间 -->
  <input type="hidden" class="endtime"><!-- 今天的开始时间 -->
  <input type="hidden" class="starttimeY"><!-- 昨天的开始时间 -->
  <input type="hidden" class="endtimeY"><!-- 昨天的结束时间 -->
  <input type="hidden" class="starttimeThree"><!-- 3天的开始时间 -->
  <input type="hidden" class="endtimeThree"><!-- 3天的结束时间 -->
  <input type="hidden" class="starttimeseven"><!-- 7天的开始时间 -->
  <input type="hidden" class="endtimeseven"><!-- 7天的结束时间 -->
  <input type="hidden" class="starttimefifteen"><!-- 15天的开始时间 -->
  <input type="hidden" class="endtimefifteen"><!-- 15天的结束时间 -->
  <input type="hidden" class="starttimethirty"><!-- 30天的开始时间 -->
  <input type="hidden" class="endtimethirty"><!-- 30天的结束时间 -->
  <!--*******支付通道*******-->
  <input type="hidden" class="paystarttime"><!-- 今天的开始时间 -->
  <input type="hidden" class="payendtime"><!-- 今天的开始时间 -->
  <input type="hidden" class="paystarttimeY"><!-- 昨天的开始时间 -->
  <input type="hidden" class="payendtimeY"><!-- 昨天的结束时间 -->
  <input type="hidden" class="paystarttimeThree"><!-- 3天的开始时间 -->
  <input type="hidden" class="payendtimeThree"><!-- 3天的结束时间 -->
  <input type="hidden" class="paystarttimeseven"><!-- 7天的开始时间 -->
  <input type="hidden" class="payendtimeseven"><!-- 7天的结束时间 -->
  <input type="hidden" class="paystarttimefifteen"><!-- 15天的开始时间 -->
  <input type="hidden" class="payendtimefifteen"><!-- 15天的结束时间 -->
  <input type="hidden" class="paystarttimethirty"><!-- 30天的开始时间 -->
  <input type="hidden" class="payendtimethirty"><!-- 30天的结束时间 -->
  <!--*******折状图时间*******-->
  <input type="hidden" class="chartstarttime"><!-- 今天的开始时间 -->
  <input type="hidden" class="chartendtime"><!-- 今天的开始时间 -->
  <input type="hidden" class="chartstarttimeY"><!-- 昨天的开始时间 -->
  <input type="hidden" class="chartendtimeY"><!-- 昨天的结束时间 -->
  <input type="hidden" class="chartstarttimeThree"><!-- 3天的开始时间 -->
  <input type="hidden" class="chartendtimeThree"><!-- 3天的结束时间 -->
  <input type="hidden" class="chartstarttimeseven"><!-- 7天的开始时间 -->
  <input type="hidden" class="chartendtimeseven"><!-- 7天的结束时间 -->
  <input type="hidden" class="chartstarttimefifteen"><!-- 15天的开始时间 -->
  <input type="hidden" class="chartendtimefifteen"><!-- 15天的结束时间 -->
  <input type="hidden" class="chartstarttimethirty"><!-- 30天的开始时间 -->
  <input type="hidden" class="chartendtimethirty"><!-- 30天的结束时间 -->
</div>
</div>
</body>
<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script src="{{asset('/layuiadmin/echarts.min.js')}}"></script>
<script>
var token = sessionStorage.getItem("Usertoken");
layui.config({
    base: '../../layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index','form', 'upload','table','laydate'], function(){
    var $ = layui.$
        ,admin = layui.admin
        ,table = layui.table
        ,form = layui.form
        ,upload = layui.upload
        ,laydate = layui.laydate;
     $('.item').mouseover(function(){
     $(this).addClass('div1');
        if($('div').hasClass('div1')){
            $('.div1').css({
            'background-color':'#1E9FFF',
            'color':'#fff',
        });
        }
     });
     $('.item').mouseout(function(){
      $(this).removeClass('div1');
      $('.item').css({
            'background-color':'#ffffff',
            'color':'black',
        });
    });
    /*
     *交易数据时间戳
     */
    // 获取时间
    var nowdate = new Date();
    // 本月
    var year=nowdate.getFullYear();
    var mounth=nowdate.getMonth()+1;
    var day=nowdate.getDate();
    var hour = nowdate.getHours();
    var min = nowdate.getMinutes();
    var sec = nowdate.getSeconds();
    var time = nowdate.getTime();
    if(mounth.toString().length<2 && day.toString().length<2){
        var nwedata = year+'-0'+mounth+'-0'+day+' '+hour+':'+min+':'+sec;
    }
    else if(mounth.toString().length<2){
        var nwedata = year+'-0'+mounth+'-'+day+' '+hour+':'+min+':'+sec;
    }
    else if(day.toString().length<2){
        var nwedata = year+'-'+mounth+'-0'+day+' '+hour+':'+min+':'+sec;
    }
    else{
        var nwedata = year+'-'+mounth+'-'+day+' '+hour+':'+min+':'+sec;
    }
    $('.end-item').val(nwedata);//今天的时间
    $('.endtime').val(nwedata)
    //今天的开始时间
    if(mounth.toString().length<2 && day.toString().length<2){
        var nwedatastart = year+'-0'+mounth+'-0'+day+' '+'00'+':'+'00'+':'+'00';
    }
    else if(mounth.toString().length<2){
        var nwedatastart = year+'-0'+mounth+'-'+day+' '+'00'+':'+'00'+':'+'00';
    }
    else if(day.toString().length<2){
        var nwedatastart = year+'-'+mounth+'-0'+day+' '+'00'+':'+'00'+':'+'00';
    }
    else{
        var nwedatastart = year+'-'+mounth+'-'+day+' '+'00'+':'+'00'+':'+'00';
    }
    $('.starttime').val(nwedatastart);
    // *******************************************************************************
    var years=nowdate.getFullYear();
    var mounths=nowdate.getMonth()+1;
    var days=nowdate.getDate()-1;
    //昨天的开始时间
    if(mounth.toString().length<2 && day.toString().length<2){
        var yesterdaystart = years+'-0'+mounths+'-0'+days+' '+'00'+':'+'00'+':'+'00';
    }
    else if(mounth.toString().length<2){
        var yesterdaystart = year+'-0'+mounths+'-'+days+' '+'00'+':'+'00'+':'+'00';
    }
    else if(day.toString().length<2){
        var yesterdaystart = years+'-'+mounths+'-0'+days+' '+'00'+':'+'00'+':'+'00';
    }
    else{
        var yesterdaystart = years+'-'+mounths+'-'+days+' '+'00'+':'+'00'+':'+'00';
    }

    if(mounth.toString().length<2 && day.toString().length<2){
        var yesterdayend = years+'-0'+mounths+'-0'+days+' '+'23'+':'+'59'+':'+'59';
    }
    else if(mounth.toString().length<2){
        var yesterdayend = years+'-0'+mounths+'-'+days+' '+'23'+':'+'59'+':'+'59';
    }
    else if(day.toString().length<2){
        var yesterdayend = years+'-'+mounths+'-0'+days+' '+'23'+':'+'59'+':'+'59';
    }
    else{
        var yesterdayend = years+'-'+mounths+'-'+days+' '+'23'+':'+'59'+':'+'59';
    }
    $('.starttimeY').val(yesterdaystart);
    $('.endtimeY').val(yesterdayend);
    // 3天开始时间
    var yearthress=nowdate.getFullYear();
    var mounthsthress=nowdate.getMonth()+1;
    var daysthress=nowdate.getDate()-3;
    var daysthressend=nowdate.getDate()-1;
    //3天的开始时间
    if(mounth.toString().length<2 && day.toString().length<2){
        var yesterdaystart = yearthress+'-0'+mounthsthress+'-0'+daysthress+' '+'00'+':'+'00'+':'+'00';
    }
    else if(mounth.toString().length<2){
        var yesterdaystart = year+'-0'+mounthsthress+'-'+daysthress+' '+'00'+':'+'00'+':'+'00';
    }
    else if(day.toString().length<2){
        var yesterdaystart = yearthress+'-'+mounthsthress+'-0'+daysthress+' '+'00'+':'+'00'+':'+'00';
    }
    else{
        var yesterdaystart = yearthress+'-'+mounthsthress+'-'+daysthress+' '+'00'+':'+'00'+':'+'00';
    }

    if(mounth.toString().length<2 && day.toString().length<2){
        var yesterdayend = yearthress+'-0'+mounthsthress+'-0'+daysthressend+' '+'23'+':'+'59'+':'+'59';
    }
    else if(mounth.toString().length<2){
        var yesterdayend = yearthress+'-0'+mounthsthress+'-'+daysthressend+' '+'23'+':'+'59'+':'+'59';
    }
    else if(day.toString().length<2){
        var yesterdayend = yearthress+'-'+mounthsthress+'-0'+daysthressend+' '+'23'+':'+'59'+':'+'59';
    }
    else{
        var yesterdayend = yearthress+'-'+mounthsthress+'-'+daysthressend+' '+'23'+':'+'59'+':'+'59';
    }
    $('.starttimeThree').val(yesterdaystart);
    $('.endtimeThree').val(yesterdayend);
       //获取最近7天前时间
        let today0 = new Date(new Date().toLocaleDateString()).getTime();   //今天0凌晨0点0分的时间戳
        let today1 = new Date(new Date().toLocaleDateString()).getTime() + 24 * 60 * 60 * 1000 - 1; //今天最晚的时间 23:59:59的时间戳

        let sevenDay0 = today0 - 24 * 60 * 60 * 1000 * 6     //7天前0凌晨的时间戳
        // let sevenDay1 = today1 - 24 * 60 * 60 * 1000 * 6     //7天前最晚的时间  23:59:59的时间戳
        let sevenDay1 = today1 - 1
        function formatDate(value) {
            if (typeof (value) == 'undefined') {
                return ''
            } else {
                let date = new Date(parseInt(value))
                let y = date.getFullYear()
                let MM = date.getMonth() + 1
                MM = MM < 10 ? ('0' + MM) : MM
                let d = date.getDate()
                d = d < 10 ? ('0' + d) : d
                let h = date.getHours()
                h = h < 10 ? ('0' + h) : h
                let m = date.getMinutes()
                m = m < 10 ? ('0' + m) : m
                let s = date.getSeconds()
                s = s < 10 ? ('0' + s) : s
                return y + '-' + MM + '-' + d + ' ' + h + ':' + m + ':' + s
            }
        }

        // console.log(formatDate(sevenDay0));
        // console.log(formatDate(sevenDay1));
        $('.starttimeseven').val(formatDate(sevenDay0));
        $('.endtimeseven').val(formatDate(sevenDay1));
        //获取最近15天前时间
        let todayfifteenstart = new Date(new Date().toLocaleDateString()).getTime();   //今天0凌晨0点0分的时间戳
        let todayfifteenend = new Date(new Date().toLocaleDateString()).getTime() + 24 * 60 * 60 * 1000 - 1; //今天最晚的时间 23:59:59的时间戳

        let sevenDaystart = todayfifteenstart - 24 * 60 * 60 * 1000 * 14     //15天前0凌晨的时间戳
        // let sevenDayend = todayfifteenend - 24 * 60 * 60 * 1000 * 14     //15天前最晚的时间  23:59:59的时间戳
        let sevenDayend = todayfifteenend - 1
        function formatDate(value) {
            if (typeof (value) == 'undefined') {
                return ''
            } else {
                let date = new Date(parseInt(value))
                let y = date.getFullYear()
                let MM = date.getMonth() + 1
                MM = MM < 10 ? ('0' + MM) : MM
                let d = date.getDate()
                d = d < 10 ? ('0' + d) : d
                let h = date.getHours()
                h = h < 10 ? ('0' + h) : h
                let m = date.getMinutes()
                m = m < 10 ? ('0' + m) : m
                let s = date.getSeconds()
                s = s < 10 ? ('0' + s) : s
                return y + '-' + MM + '-' + d + ' ' + h + ':' + m + ':' + s
            }
        }

        $('.starttimefifteen').val(formatDate(sevenDaystart));
        $('.endtimefifteen').val(formatDate(sevenDayend));

        //获取最近30天前时间
        let todaythirtystart = new Date(new Date().toLocaleDateString()).getTime();   //今天0凌晨0点0分的时间戳
        let todaythirtyend = new Date(new Date().toLocaleDateString()).getTime() + 24 * 60 * 60 * 1000 - 1; //今天最晚的时间 23:59:59的时间戳

        let sevenDaythirtystart = todaythirtystart - 24 * 60 * 60 * 1000 * 28    //30天前0凌晨的时间戳
        // let sevenDaythirtyend = todaythirtyend - 24 * 60 * 60 * 1000 * 29   //30天前最晚的时间  23:59:59的时间戳
        let sevenDaythirtyend = todaythirtyend - 1
        function formatDate(value) {
            if (typeof (value) == 'undefined') {
                return ''
            } else {
                let date = new Date(parseInt(value))
                let y = date.getFullYear()
                let MM = date.getMonth() + 1
                MM = MM < 10 ? ('0' + MM) : MM
                let d = date.getDate()
                d = d < 10 ? ('0' + d) : d
                let h = date.getHours()
                h = h < 10 ? ('0' + h) : h
                let m = date.getMinutes()
                m = m < 10 ? ('0' + m) : m
                let s = date.getSeconds()
                s = s < 10 ? ('0' + s) : s
                return y + '-' + MM + '-' + d + ' ' + h + ':' + m + ':' + s
            }
        }

        $('.starttimethirty').val(formatDate(sevenDaythirtystart));
        $('.endtimethirty').val(formatDate(sevenDaythirtyend));

    // 华丽的分割线----------------------------------------------------
    // nowdate.setMonth(nowdate.getMonth()-1);
    // 上个月
    var y = nowdate.getFullYear();
    var mon = nowdate.getMonth()+1;
    var d = nowdate.getDate();
    var h = '00';
    var m = '00';
    var s = '00';
    if(mon.toString().length<2 && d.toString().length<2){
        var formatwdate = y+'-0'+mon+'-0'+d+' '+h+':'+m+':'+s;
    }
    else if(mon.toString().length<2){
        var formatwdate = y+'-0'+mon+'-'+d+' '+h+':'+m+':'+s;
    }
    else if(d.toString().length<2){
        var formatwdate = y+'-'+mon+'-0'+d+' '+h+':'+m+':'+s;
    }
    else{
        var formatwdate = y+'-'+mon+'-'+d+' '+h+':'+m+':'+s;
    }
    $('.start-item').val(formatwdate);
    jiaoyiacton();
    payFuntion();
    chartFunction();
    circularFunction();
    // circularFunctioncount();
        //交易数据
        function jiaoyiacton(){
            $.post("{{url('/api/user/indexTransactionData')}}",
                {
                    token:token,
                    startTime:$('.start-item').val(),
                    endTime:$('.end-item').val(),
                },function(res){
                    if(res.status==1){
                    $('#transaction1').html(res.data.transaction_amount);
                    $('#transaction2').html(res.data.number_of_commission);
                    $('#transaction3').html(res.data.refund_amount);
                    $('#transaction4').html(res.data.number_of_refunds);
                    $('#transaction5').html(res.data.actual_revenue);
                    $('#transaction6').html(res.data.my_commission);
                    }else{
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 3000
                    });
                    }
        },"json");
       }

       //时间控件，交易数据
       laydate.render({
        elem: '.start-item'
        ,type: 'datetime'
        ,trigger: 'click'
        ,done: function(value){
            $('.start-item').val(value)
            //执行重载
            jiaoyiacton();
        }
        });
        laydate.render({
        elem: '.end-item'
        ,trigger: 'click'
        ,type: 'datetime'
        ,done: function(value){
            $('.end-item').val(value)
            //执行重载
            jiaoyiacton();
        }
        });
        //今天交易数据
        $('#today').click(function(){
            $('.start-item').val($('.starttime').val())
            $('.end-item').val($('.endtime').val());
            jiaoyiacton();
        })
        //昨天交易数据
        $('#yesterday').click(function(){
            $('.start-item').val($('.starttimeY').val())
            $('.end-item').val($('.endtimeY').val())
            jiaoyiacton();
        })
        //3天前交易数据
        $('#threeday').click(function(){
            $('.start-item').val($('.starttimeThree').val())
            $('.end-item').val($('.endtimeThree').val())
            jiaoyiacton();
        })
        //7天前交易数据
        $('#sevenday').click(function(){
            $('.start-item').val($('.starttimeseven').val())
            $('.end-item').val($('.endtimeseven').val())
            jiaoyiacton();
        })
        //15天前交易数据
        $('#fifteen').click(function(){
            $('.start-item').val($('.starttimefifteen').val())
            $('.end-item').val($('.endtimefifteen').val())
            jiaoyiacton();
        })
        //30天前交易数据
        $('#thirty').click(function(){
            $('.start-item').val($('.starttimethirty').val())
            $('.end-item').val($('.endtimethirty').val())
            jiaoyiacton();
        })
    // 华丽的分割线----------------------------------------------------
    /*
     *支付通道时间戳
     */
    // 获取时间
    var nowdate = new Date();
    // 本月
    var year=nowdate.getFullYear();
    var mounth=nowdate.getMonth()+1;
    var day=nowdate.getDate();
    var hour = nowdate.getHours();
    var min = nowdate.getMinutes();
    var sec = nowdate.getSeconds();
    var time = nowdate.getTime();
    if(mounth.toString().length<2 && day.toString().length<2){
        var nwedata = year+'-0'+mounth+'-0'+day+' '+hour+':'+min+':'+sec;
    }
    else if(mounth.toString().length<2){
        var nwedata = year+'-0'+mounth+'-'+day+' '+hour+':'+min+':'+sec;
    }
    else if(day.toString().length<2){
        var nwedata = year+'-'+mounth+'-0'+day+' '+hour+':'+min+':'+sec;
    }
    else{
        var nwedata = year+'-'+mounth+'-'+day+' '+hour+':'+min+':'+sec;
    }
    $('.endpay-item').val(nwedata);//今天的时间
    $('.payendtime').val(nwedata)
    //今天的开始时间
    if(mounth.toString().length<2 && day.toString().length<2){
        var paynwedatastart = year+'-0'+mounth+'-0'+day+' '+'00'+':'+'00'+':'+'00';
    }
    else if(mounth.toString().length<2){
        var paynwedatastart = year+'-0'+mounth+'-'+day+' '+'00'+':'+'00'+':'+'00';
    }
    else if(day.toString().length<2){
        var paynwedatastart = year+'-'+mounth+'-0'+day+' '+'00'+':'+'00'+':'+'00';
    }
    else{
        var paynwedatastart = year+'-'+mounth+'-'+day+' '+'00'+':'+'00'+':'+'00';
    }
    $('.paystarttime').val(paynwedatastart);
    // *******************************************************************************
    var years=nowdate.getFullYear();
    var mounths=nowdate.getMonth()+1;
    var days=nowdate.getDate()-1;
    //昨天的开始时间
    if(mounth.toString().length<2 && day.toString().length<2){
        var payyesterdaystart = years+'-0'+mounths+'-0'+days+' '+'00'+':'+'00'+':'+'00';
    }
    else if(mounth.toString().length<2){
        var payyesterdaystart = year+'-0'+mounths+'-'+days+' '+'00'+':'+'00'+':'+'00';
    }
    else if(day.toString().length<2){
        var payyesterdaystart = years+'-'+mounths+'-0'+days+' '+'00'+':'+'00'+':'+'00';
    }
    else{
        var payyesterdaystart = years+'-'+mounths+'-'+days+' '+'00'+':'+'00'+':'+'00';
    }

    if(mounth.toString().length<2 && day.toString().length<2){
        var payyesterdayend = years+'-0'+mounths+'-0'+days+' '+'23'+':'+'59'+':'+'59';
    }
    else if(mounth.toString().length<2){
        var payyesterdayend = years+'-0'+mounths+'-'+days+' '+'23'+':'+'59'+':'+'59';
    }
    else if(day.toString().length<2){
        var payyesterdayend = years+'-'+mounths+'-0'+days+' '+'23'+':'+'59'+':'+'59';
    }
    else{
        var payyesterdayend = years+'-'+mounths+'-'+days+' '+'23'+':'+'59'+':'+'59';
    }
    $('.paystarttimeY').val(payyesterdaystart);
    $('.payendtimeY').val(payyesterdayend);
    // 3天开始时间
    var yearthress=nowdate.getFullYear();
    var mounthsthress=nowdate.getMonth()+1;
    var daysthress=nowdate.getDate()-3;
    var daysthressend=nowdate.getDate()-1;
    //3天的开始时间
    if(mounth.toString().length<2 && day.toString().length<2){
        var yesterdaystart = yearthress+'-0'+mounthsthress+'-0'+daysthress+' '+'00'+':'+'00'+':'+'00';
    }
    else if(mounth.toString().length<2){
        var yesterdaystart = year+'-0'+mounthsthress+'-'+daysthress+' '+'00'+':'+'00'+':'+'00';
    }
    else if(day.toString().length<2){
        var yesterdaystart = yearthress+'-'+mounthsthress+'-0'+daysthress+' '+'00'+':'+'00'+':'+'00';
    }
    else{
        var yesterdaystart = yearthress+'-'+mounthsthress+'-'+daysthress+' '+'00'+':'+'00'+':'+'00';
    }

    if(mounth.toString().length<2 && day.toString().length<2){
        var yesterdayend = yearthress+'-0'+mounthsthress+'-0'+daysthressend+' '+'23'+':'+'59'+':'+'59';
    }
    else if(mounth.toString().length<2){
        var yesterdayend = yearthress+'-0'+mounthsthress+'-'+daysthressend+' '+'23'+':'+'59'+':'+'59';
    }
    else if(day.toString().length<2){
        var yesterdayend = yearthress+'-'+mounthsthress+'-0'+daysthressend+' '+'23'+':'+'59'+':'+'59';
    }
    else{
        var yesterdayend = yearthress+'-'+mounthsthress+'-'+daysthressend+' '+'23'+':'+'59'+':'+'59';
    }
    $('.paystarttimeThree').val(yesterdaystart);
    $('.payendtimeThree').val(yesterdayend);
    //获取最近7天前时间
        let paytoday0 = new Date(new Date().toLocaleDateString()).getTime();   //今天0凌晨0点0分的时间戳
        let paytoday1 = new Date(new Date().toLocaleDateString()).getTime() + 24 * 60 * 60 * 1000 - 1; //今天最晚的时间 23:59:59的时间戳

        let paysevenDay0 = paytoday0 - 24 * 60 * 60 * 1000 * 6     //7天前0凌晨的时间戳
        // let sevenDay1 = today1 - 24 * 60 * 60 * 1000 * 6     //7天前最晚的时间  23:59:59的时间戳
        let paysevenDay1 = paytoday1 - 1
        function formatDate(value) {
            if (typeof (value) == 'undefined') {
                return ''
            } else {
                let date = new Date(parseInt(value))
                let y = date.getFullYear()
                let MM = date.getMonth() + 1
                MM = MM < 10 ? ('0' + MM) : MM
                let d = date.getDate()
                d = d < 10 ? ('0' + d) : d
                let h = date.getHours()
                h = h < 10 ? ('0' + h) : h
                let m = date.getMinutes()
                m = m < 10 ? ('0' + m) : m
                let s = date.getSeconds()
                s = s < 10 ? ('0' + s) : s
                return y + '-' + MM + '-' + d + ' ' + h + ':' + m + ':' + s
            }
        }

        $('.paystarttimeseven').val(formatDate(paysevenDay0));
        $('.payendtimeseven').val(formatDate(paysevenDay1));
        //获取最近15天前时间
        let paytodayfifteenstart = new Date(new Date().toLocaleDateString()).getTime();   //今天0凌晨0点0分的时间戳
        let paytodayfifteenend = new Date(new Date().toLocaleDateString()).getTime() + 24 * 60 * 60 * 1000 - 1; //今天最晚的时间 23:59:59的时间戳

        let paysevenDaystart = paytodayfifteenstart - 24 * 60 * 60 * 1000 * 14     //15天前0凌晨的时间戳
        // let sevenDayend = todayfifteenend - 24 * 60 * 60 * 1000 * 14     //15天前最晚的时间  23:59:59的时间戳
        let paysevenDayend = paytodayfifteenend - 1
        function formatDate(value) {
            if (typeof (value) == 'undefined') {
                return ''
            } else {
                let date = new Date(parseInt(value))
                let y = date.getFullYear()
                let MM = date.getMonth() + 1
                MM = MM < 10 ? ('0' + MM) : MM
                let d = date.getDate()
                d = d < 10 ? ('0' + d) : d
                let h = date.getHours()
                h = h < 10 ? ('0' + h) : h
                let m = date.getMinutes()
                m = m < 10 ? ('0' + m) : m
                let s = date.getSeconds()
                s = s < 10 ? ('0' + s) : s
                return y + '-' + MM + '-' + d + ' ' + h + ':' + m + ':' + s
            }
        }

        $('.paystarttimefifteen').val(formatDate(paysevenDaystart));
        $('.payendtimefifteen').val(formatDate(paysevenDayend));

        //获取最近30天前时间
        let paytodaythirtystart = new Date(new Date().toLocaleDateString()).getTime();   //今天0凌晨0点0分的时间戳
        let paytodaythirtyend = new Date(new Date().toLocaleDateString()).getTime() + 24 * 60 * 60 * 1000 - 1; //今天最晚的时间 23:59:59的时间戳

        let paysevenDaythirtystart = paytodaythirtystart - 24 * 60 * 60 * 1000 * 28    //30天前0凌晨的时间戳
        // let sevenDaythirtyend = todaythirtyend - 24 * 60 * 60 * 1000 * 29   //30天前最晚的时间  23:59:59的时间戳
        let paysevenDaythirtyend = paytodaythirtyend - 1
        function formatDate(value) {
            if (typeof (value) == 'undefined') {
                return ''
            } else {
                let date = new Date(parseInt(value))
                let y = date.getFullYear()
                let MM = date.getMonth() + 1
                MM = MM < 10 ? ('0' + MM) : MM
                let d = date.getDate()
                d = d < 10 ? ('0' + d) : d
                let h = date.getHours()
                h = h < 10 ? ('0' + h) : h
                let m = date.getMinutes()
                m = m < 10 ? ('0' + m) : m
                let s = date.getSeconds()
                s = s < 10 ? ('0' + s) : s
                return y + '-' + MM + '-' + d + ' ' + h + ':' + m + ':' + s
            }
        }

        $('.paystarttimethirty').val(formatDate(paysevenDaythirtystart));
        $('.payendtimethirty').val(formatDate(paysevenDaythirtyend));
    // 上个月
    var y = nowdate.getFullYear();
    var mon = nowdate.getMonth()+1;
    var d = nowdate.getDate();
    var h = '00';
    var m = '00';
    var s = '00';
    if(mon.toString().length<2 && d.toString().length<2){
        var formatwdate = y+'-0'+mon+'-0'+d+' '+h+':'+m+':'+s;
    }
    else if(mon.toString().length<2){
        var formatwdate = y+'-0'+mon+'-'+d+' '+h+':'+m+':'+s;
    }
    else if(d.toString().length<2){
        var formatwdate = y+'-'+mon+'-0'+d+' '+h+':'+m+':'+s;
    }
    else{
        var formatwdate = y+'-'+mon+'-'+d+' '+h+':'+m+':'+s;
    }
    $('.startpay-item').val(formatwdate);
    //数据展示
      $.get("{{url('/api/user/indexDataOverview')}}",
            {
                token:token
            },function(res){
                if(res.status==1){
                $('#acounts1').html(res.data.order_total_sum);
                $('#acounts2').html(res.data.total_commission);
                $('#acounts3').html(res.data.order_total_2_sum);
                $('#acounts4').html(res.data.order_total_3_sum);
                $('#acounts5').html(res.data.total_mer_num);
                }else{
                layer.msg(res.message, {
                    offset: '50px'
                    ,icon: 2
                    ,time: 3000
                });
                }
       },"json");

        //支付通道金额 / 笔数
        function payFuntion(){
            $.post("{{url('/api/user/passagewayStatistics')}}",
                {
                    token:token,
                    startTime:$('#start-item').val(),
                    endTime:$('#end-item').val(),
                },function(res){
                    console.log(res.data);
                    if(res.status==1){
                            if(res.data.length == 0){
                                    $('#weixinmoney').html(0);
                                    $('#weixincount').html(0);
                                    $('#alipaymoney').html(0);
                                    $('#alipaycount').html(0);
                                    $('#newlandmoney').html(0);
                                    $('#newlandcount').html(0);
                                    $('#vbillamoney').html(0);
                                    $('#vbillacount').html(0);
                            }else{
                                for(var i=0;i<res.data.length;i++){
                                    if(res.data[i].company=="weixina"){
                                        $('#weixinmoney').html(res.data[i].company_sum);
                                        $('#weixincount').html(res.data[i].company_count);
                                    }
                                    if(res.data[i].company=="alipay"){
                                        $('#alipaymoney').html(res.data[i].company_sum);
                                        $('#alipaycount').html(res.data[i].company_count);
                                    }
                                    if(res.data[i].company=="newland"){
                                        $('#newlandmoney').html(res.data[i].company_sum);
                                        $('#newlandcount').html(res.data[i].company_count);
                                    }else{
                                        $('#newlandmoney').html(0);
                                        $('#newlandcount').html(0);
                                    }
                                    if(res.data[i].company=="vbilla"){
                                        $('#vbillamoney').html(res.data[i].company_sum);
                                        $('#vbillacount').html(res.data[i].company_count);
                                    }
                                }
                            }

                        }

                },"json");
       }
       //时间控件，交易数据
       laydate.render({
        elem: '#start-item'
        ,type: 'datetime'
        ,trigger: 'click'
        ,done: function(value){
            $('#start-item').val(value)
            //执行重载
            payFuntion();
        }
        });
        laydate.render({
        elem: '#end-item'
        ,trigger: 'click'
        ,type: 'datetime'
        ,done: function(value){
            $('#end-item').val(value)
            //执行重载
            payFuntion();
        }
        });
        //今天交易数据
        $('#paytoday').click(function(){
            $('#start-item').val($('.paystarttime').val())
            $('#end-item').val($('.payendtime').val());
            payFuntion();
        })
        //昨天交易数据
        $('#payyesterday').click(function(){
            $('#start-item').val($('.paystarttimeY').val())
            $('#end-item').val($('.payendtimeY').val())
            payFuntion();
        })
        //3天前交易数据
        $('#paythreeday').click(function(){
            $('#start-item').val($('.paystarttimeThree').val())
            $('#end-item').val($('.payendtimeThree').val())
            payFuntion();
        })
        //7天前交易数据
        $('#paysevenday').click(function(){
            $('#start-item').val($('.paystarttimeseven').val())
            $('#end-item').val($('.payendtimeseven').val())
            payFuntion();
        })
        //15天前交易数据
        $('#payfifteen').click(function(){
            $('#start-item').val($('.paystarttimefifteen').val())
            $('#end-item').val($('.payendtimefifteen').val())
            payFuntion();
        })
        //30天前交易数据
        $('#paythirty').click(function(){
            $('#start-item').val($('.paystarttimethirty').val())
            $('#end-item').val($('.payendtimethirty').val())
            payFuntion();
        })
      //最新公告
      $.post("{{url('/api/huodong/get_list')}}",
                {
                    token:token,
                },function(res){
                    // console.log(res,'公告');
                    jsonData = res;
                    var html = '';
                    html +='<ul class="lists">';
                    $.each(jsonData, function (i, v) {
                        // console.log(v);
                        for(var i=0;i<v.length;i++){
                            if(v[i].id==undefined){
                    
                            }else{
                                html +='<li class="item" data-id="'+v[i].id+'">'+v[i].content+'</li>';
                            }

                        }
                        html += '</li>';
                    })
                    $('.permission_lists').html(html);
        },"json");
    //折状图时间事件
    // 华丽的分割线----------------------------------------------------
    // 获取时间
    var nowdate = new Date();
    var year=nowdate.getFullYear();
    var mounth=nowdate.getMonth()+1;
    var day=nowdate.getDate();
    var hour = nowdate.getHours();
    var min = nowdate.getMinutes();
    var sec = nowdate.getSeconds();
    var time = nowdate.getTime();
	//今天的开始时间
	if(mounth.toString().length<2 && day.toString().length<2){
	    var chartnwedatastart = year+'-0'+mounth+'-0'+day+' '+'00'+':'+'00'+':'+'00';
	}
	else if(mounth.toString().length<2){
	    var chartnwedatastart = year+'-0'+mounth+'-'+day+' '+'00'+':'+'00'+':'+'00';
	}
	else if(day.toString().length<2){
	    var chartnwedatastart = year+'-'+mounth+'-0'+day+' '+'00'+':'+'00'+':'+'00';
	}
	else{
	    var chartnwedatastart = year+'-'+mounth+'-'+day+' '+'00'+':'+'00'+':'+'00';
	}
	//结束时间
    if(mounth.toString().length<2 && day.toString().length<2){
        var nwedata = year+'-0'+mounth+'-0'+day+' '+'23'+':'+'59'+':'+'59';
    }
    else if(mounth.toString().length<2){
        var nwedata = year+'-0'+mounth+'-'+day+' '+'23'+':'+'59'+':'+'59';
    }
    else if(day.toString().length<2){
        var nwedata = year+'-'+mounth+'-0'+day+' '+'23'+':'+'59'+':'+'59';
    }
    else{
        var nwedata = year+'-'+mounth+'-'+day+' '+'23'+':'+'59'+':'+'59';
    }


    $('#chartstart-item').val(chartnwedatastart);//今天的时间
    $('#chartend-item').val(nwedata)
    //时间控件，交易数据
    laydate.render({
        elem: '#chartstart-item'
        ,type: 'datetime'
        ,trigger: 'click'
        ,done: function(value){
            $('#chartstart-item').val(value)
            //执行重载
            chartFunction();
        }
    });
    laydate.render({
        elem: '#chartend-item'
        ,trigger: 'click'
        ,type: 'datetime'
        ,done: function(value){
            $('#chartend-item').val(value)
            //执行重载
            chartFunction();
        }
    });
    //折状图接口请求
    function chartFunction(){
            //折状图展示
        var myChart = echarts.init(document.getElementById('main'));
        $.post("{{url('/api/user/dataStatistics')}}",
        {
            token:token,
            startTime:$('#chartstart-item').val(),
            endTime:$('#chartend-item').val(),
        },function(res){
            if(res.status==1){
                let datalist = res.data;
                // 品台数据
                let alipay_list = [];
                let weixin_list = [];
                for(let i=0;i<datalist.length;i++){
                    if(datalist[i].ways_source == "alipay"){
                        alipay_list.push(datalist[i])
                    }else if(datalist[i].ways_source == "weixin"){
                        weixin_list.push(datalist[i])
                    }
                }

                // 时间数组
                let time_arr = [];
                for(let i=0;i<datalist.length;i++){
                    if(time_arr.length == 0){
                    time_arr.push(datalist[i].new_created_at)
                    }else{
                    let key = false;
                    for(let m=0;m<time_arr.length;m++){
                        if(datalist[i].new_created_at == time_arr[m]){
                        key = true;
                        }
                        }
                        if(key == false){
                        time_arr.push(datalist[i].new_created_at)
                        }
                    }
                }
                let time_new_arr = time_arr.sort(function(a,b){
                    return Date.parse(a) - Date.parse(b) ;//时间正序
                });
                console.log(time_new_arr)

                // 获取平台数据
                let alipay = [];
                let weixin = [];
                for(let y=0;y<time_new_arr.length;y++){
                    let key = false;
                    let obj = "";
                for(let o=0;o<alipay_list.length;o++){
                    if(time_new_arr[y] == alipay_list[o].new_created_at){
                        key = true;
                        obj = alipay_list[o].company_sum;
                    } 
                }
                    if(key == true){
                        alipay.push(obj)
                    }else{
                        alipay.push(0)
                    }
                }
                for(let y=0;y<time_new_arr.length;y++){
                    let key = false;
                    let obj = "";
                    for(let o=0;o<weixin_list.length;o++){
                        if(time_new_arr[y] == weixin_list[o].new_created_at){
                            key = true;
                            obj = weixin_list[o].company_sum;
                        } 
                    }
                    if(key == true){
                        weixin.push(obj)
                    }else{
                        weixin.push(0)
                    }
                }

                // 时间格式 处理 time_new_arr
                let time_new_arr_two = [];
                var  reg = /^(\d{4})-(\d{1,2})-(\d{1,2})$/;
                for(let k=0 ; k < time_new_arr.length ; k++){
                    time_new_arr[k].match(reg);
                    time_new_arr[k] = RegExp.$2 +"-"+ RegExp.$3;
                }

                var option = {
                    tooltip: {
                            trigger: 'axis'
                            },
                    legend: {
                            data: ['微信', '支付宝'],
                            bottom:"0%"
                            },
                    grid: {
                            left: '3%',
                            right: '4%',
                            bottom: '15%',
                            containLabel: true
                        },
                    xAxis: {
                            type: 'category',
                            boundaryGap: false,
                            data:time_new_arr
                            },
                    yAxis: {
                            type: 'value'
                        },
                    series: [
                        {
                            name: '微信',
                            type: 'line',
                            stack: '总量',
                            color: '#07c160',
                            data: weixin
                        },
                        {
                            name: '支付宝',
                            type: 'line',
                            stack: '总量',
                            color:"#027AFF",
                            data: alipay
                        }
                    ]
                };
                myChart.setOption(option);
            }
        },"json");
    }

//饼状图
function newbtFunction(wechatmoney,alipaymoney){
    var modeproportion = echarts.init(document.getElementById('modeproportion'));
    option1 = {
        tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b} : {c} ({d}%)'
        },
        legend: {
            orient: 'vertical',
            right: 'right',
            data: ['微信', '支付宝']
        },
        series: [
            {
                name: '访问来源',
                type: 'pie',
                radius: '55%',
                center: ['50%', '60%'],
                data: [
                    {value: wechatmoney, name: '微信',itemStyle: {color:"#07c160"}},
                    {value: alipaymoney, name: '支付宝',itemStyle: {color:"#027AFF"}},
                ],
                emphasis: {
                    itemStyle: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    };
    modeproportion.setOption(option1);
}
    //饼状图接口请求、、金额
    function circularFunction(){
        $.post("{{url('/api/user/proportionOfPaymentMethods')}}",
            {
                token:token,
            },function(res){
                if(res.status==1){
                    if(res.data.length == 0){
                        newbtFunction(0,0);
                    }else{

                        newbtFunction(res.data[1].company_sum,res.data[0].company_sum);
                        
                    }
                }
        },"json");
    }
     //交易金额按钮
    $('#moneybutton').click(function(){
        circularFunction();
    })
    //交易笔数按钮
    $('#countbutton').click(function(){
        $.post("{{url('/api/user/proportionOfPaymentMethods')}}",
                {
                    token:token,
                },function(res){
                    if(res.status==1){
                        if(res.data.length == 0){
                            newbtFunction(0,0);
                        }else{
                            newbtFunction(res.data[1].company_count,res.data[0].company_count);
                        }
                    }
        },"json");
    })

});
</script>
</html>
