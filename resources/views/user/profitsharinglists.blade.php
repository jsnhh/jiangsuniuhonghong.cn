<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>微信分账流水</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
<link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
<style>
    .edit{background-color: #ed9c3a;}
    .shenhe{background-color: #429488;}
    .see{background-color: #7cb717;}
    .tongbu{background-color: #4c9ef8;color:#fff;}
    .cur{color:#009688;}
    .err{
        color: #ff0000;
    }
</style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12" style="margin-top:0px">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12">
                        <div class="layui-card">
                            <div class="layui-card-header">微信分账流水列表</div>
                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">
                                    <div class="layui-form" style="width:820px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <!-- 交易时间 -->
                                            <div class="layui-inline">
                                                <div class="layui-input-inline">
                                                    <text class="yname">交易开始时间</text>
                                                    <input type="text" class="layui-input start-item test-item" name="startitem" placeholder="交易开始时间" lay-key="23">
                                                </div>
                                            </div>

                                            <!-- 交易时间end -->
                                            <div class="layui-inline">
                                                <div class="layui-input-inline">
                                                    <text class="yname">交易结束时间</text>
                                                    <input type="text" class="layui-input end-item test-item" name="enditem" placeholder="交易结束时间" lay-key="24">
                                                </div>
                                            </div>

                                             <!-- 搜索 -->
                                             {{--<div class="layui-inline" style='margin-right:0'>--}}
                                                {{--<div class="layui-input-inline" style="width:185px">--}}
                                                    {{--<text class="yname">设备ID</text>--}}
                                                    {{--<input type="text" style="border-radius:5px" name="deviceid" placeholder="请输入设备ID号" autocomplete="off" class="layui-input">--}}
                                                {{--</div>--}}
                                            {{--</div>--}}

                                            <div class="layui-inline">
                                                <button class="layui-btn layuiadmin-btn-list" lay-submit="" lay-filter="LAY-app-contlist-search" style="border-radius:5px;margin-top:20px;margin-bottom: 0;height:36px;line-height: 36px;">
                                                    <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                                </button>
                                              </div>

                                        </div>
                                    </div>

                                    <script type="text/html" id="fzRate">
                                        @{{ d.fz_rate }}%
                                    </script>

                                    <!-- 判断状态 -->
                                    <script type="text/html" id="status">
                                        @{{#  if(d.status == 1){ }}
                                        <span class="cur">成功</span>
                                        @{{#  } else { }}
                                        <span class="err">失败</span>
                                        @{{#  } }}
                                    </script>
                                    <!-- 判断状态 -->

                                    <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="starttime"><!-- 今天的开始时间 -->
{{--<input type="hidden" class="endtime"><!-- 今天的开始时间 -->--}}

<input type="hidden" class="starttimeY"><!-- 昨天的开始时间 -->
<input type="hidden" class="endtimeY"><!-- 昨天的结束时间 -->
<input type="hidden" class="sbid"><!-- 设备id -->


<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
var token = sessionStorage.getItem("Usertoken");
var str=location.search;
var store_id = sessionStorage.getItem("store_store_id");

layui.config({
    base: '../../layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index','form','table','laydate'], function(){
    var $ = layui.$
    ,admin = layui.admin
    ,form = layui.form
    ,table = layui.table
    ,laydate = layui.laydate;

    $('.user_id').val(store_id);

    // 未登录,跳转登录页面
    $(document).ready(function(){
        if(token==null){
            window.location.href="{{url('/user/login')}}";
        }
    });

    laydate.render({
        elem: '.start-item'
        ,type: 'datetime'
        ,done: function(value){
            //执行重载
            table.reload('test-table-page', {
                where: {
                    time_start:value
                    ,time_end:$('.end-item').val()
                }
                ,page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        }
    });

    laydate.render({
        elem: '.end-item'
        ,type: 'datetime'
        ,done: function(value){
            //执行重载
            table.reload('test-table-page', {
                where: {
                    time_start:$('.start-item').val()
                    ,time_end:value
                }
                ,page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        }
    });

    form.on('submit(LAY-app-contlist-search)', function(data){
        var device_id = data.field.deviceid;//设备id
        $('.sbid').val(device_id);

        //执行重载
        table.reload('test-table-page', {
            where: {
                // start_item:start_item
                // ,end_item:end_item
                device_id:device_id
            }
            ,page: {
                curr: 1 //重新从第 1 页开始
            }
        });
    });

    // 渲染表格
    table.render({
        elem: '#test-table-page'
        ,url: "{{url('/api/user/profit_sharing_lists')}}"
        ,method: 'post'
        ,where:{
            token:token
            ,time_start:$('.start-item').val()
            ,time_end:$('.end-item').val()
        }
        ,request:{
            pageName: 'p',
            limitName: 'l'
        }
        ,page: true
        ,cellMinWidth: 100
        ,cols: [[
            {align:'center',field:'store_id', title: '门店id'}
            ,{align:'center',field:'out_trade_no', title: '系统订单号'}
            ,{align:'center',field:'transaction_id',  title: '微信支付单号'}
            ,{align:'center',field:'fz_out_trade_no',  title: '分账单号'}
            ,{align:'center',field:'total_amount',  title: '订单金额'}
            ,{align:'center',field:'fz_amount',  title: '分账金额'}
            ,{align:'center',field:'fz_rate',  title: '分账比例', templet:'#fzRate'}
            ,{align:'center',field:'status',  title: '分账状态',  templet:'#status'}
            ,{align:'center',field:'status_desc',  title: '分账描述'}
        ]]
        ,response: {
            statusName: 'status' //数据状态的字段名称，默认：code
            ,statusCode: 1 //成功的状态码，默认：0
            ,msgName: 'message' //状态信息的字段名称，默认：msg
            ,countName: 't' //数据总数的字段名称，默认：count
            ,dataName: 'data' //数据列表的字段名称，默认：data
        }
        ,done: function(res, curr, count){
            $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});	//进行表头样式设置
        }
    });

    table.on('tool(test-table-page)', function(obj){
        var e = obj.data;
        var layEvent = obj.event;
        var tr = obj.tr;

        var data = obj.data;
        if(obj.event === 'setSign'){
            layer.open({
                type: 2,
                title: '模板详细',
                shade: false,
                maxmin: true,
                area: ['60%', '70%'],
                content: "{{url('/merchantpc/paydetail?')}}"+e.stu_order_type_no
            });
        }
    });

    // {{--// 导出--}}
    // {{--$('.export').click(function(){--}}
    //     {{--var time_start = $('.start-item').val();--}}
    //     {{--var time_end = $('.end-item').val();--}}
    //     {{--var device_id = $('.sbid').val(); --}}
    //     {{--window.location.href = "{{url('/api/export/facePaymentExcelDown')}}"+"?token="+token+"&start_time="+time_start+"&end_time="+time_end+"&device_id="+device_id;--}}
    // {{--})--}}

});

</script>

</body>
</html>





