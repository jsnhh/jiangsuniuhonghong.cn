<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>京东汇正配置</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card" style="margin-top:0px">
        <div class="layui-card-header">哆啦宝配置</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center;">accessKey</label>
                    <div class="layui-input-block" style="width:500px;">
                        <input type="text" name="access_key" lay-verify="access_key" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center;">secretKey </label>
                    <div class="layui-input-block" style="width:500px;">
                        <input type="text" name="secret_key " lay-verify="secret_key" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center;">代理商编号</label>
                    <div class="layui-input-block" style="width:500px;">
                        <input type="text" name="agent_num" lay-verify="agent_num" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center;margin-top:-10px">设备等待查询时间(秒)</label>
                    <div class="layui-input-block" style="width:500px;">
                        <input type="number" name="device_timewait" lay-verify="device_timewait"  autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center;margin-top:-10px">插件等待查询时间(秒)</label>
                    <div class="layui-input-block" style="width:500px;">
                        <input type="number" name="qwx_timewait" lay-verify="qwx_timewait" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>

                

                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit site-demo-active" style="border-radius:5px" data-type="tabChange">保存</button>
                        </div>
                    </div>
                </div>

                <div id="edit_rate" class="hide" style="display: none;background-color:#fff;border-radius:10px;">
                    <div class="layui-card-body tankuang">
                        <div class="layui-form">
                            <div class="layui-form-item">
                            <label class="layui-form-label">是否保存</label>
                            </div>
                            <div class="layui-form-item" style="display: flex;">
                                <div class="layui-input-block">
                                    <div class="layui-footer">
                                        <button class="layui-btn submits yes" style="background-color: #1E9FFF;border-radius:5px;margin-left:-55px;">是</button>
                                    </div>
                                </div>
                                <div class="layui-input-block">
                                    <div class="layui-footer">
                                        <button class="layui-btn submits no" style="background-color: #FF5722;border-radius:5px;">否</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
</div>



<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script> 
<script>
    var token = sessionStorage.getItem("Usertoken");
    

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','element'], function(){
        var $ = layui.$ 
            ,admin = layui.admin
            ,element = layui.element
            ,form = layui.form;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });

        $.post("{{url('/api/user/dlb_config')}}",
        {
            token:token
            ,getdata:1
        },function(res){
        //console.log(res);
            if(null != res && "" != res){
                if(res.status==1){
                    $('.layui-form .layui-form-item').eq(0).find('input').val(res.data.access_key);
                    $('.layui-form .layui-form-item').eq(1).find('input').val(res.data.secret_key);
                    $('.layui-form .layui-form-item').eq(2).find('input').val(res.data.agent_num);
                    $('.layui-form .layui-form-item').eq(3).find('input').val(res.data.device_timewait);
                    $('.layui-form .layui-form-item').eq(4).find('input').val(res.data.qwx_timewait );
                }else if(res.status!=3){
                    layer.alert(res.message, {icon: 2});
                }
            }else{
                layer.alert('获取信息失败', {icon: 2});
            }
        },"json");

        $('.submit').on('click', function(){
            layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area: '350px',
                skin: 'layui-layer-nobg', //没有背景色
                shadeClose: true,
                content: $('#edit_rate')
            });

            // console.log($('.private_key').val());       
            $('.yes').on('click',function(){
                $.post("{{url('/api/user/dlb_config')}}",
                {
                    token:token,
                    access_key:$('.layui-form .layui-form-item').eq(0).find('input').val(),
                    secret_key:$('.layui-form .layui-form-item').eq(1).find('input').val(),
                    agent_num:$('.layui-form .layui-form-item').eq(2).find('input').val(),
                    device_timewait:$('.layui-form .layui-form-item').eq(3).find('input').val(),
                    qwx_timewait :$('.layui-form .layui-form-item').eq(4).find('input').val()
                },function(res){
//                    console.log(res);
                    if(res.status==1){
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 2000
                        },function(){
                            window.location.reload();
                        });
                    }else{
                        layer.alert(res.message, {icon: 2});
                    }
                },'json')
            });

            $('.no').on('click',function(){
                $('#layui-layer-shade1').css('opacity','0');
                $('#edit_rate').css('display', 'none');
            });
        });

        $('.submits').click(function(){
            $('#edit_rate').css('display','none')
        });

    });

</script>

</body>
</html>
