<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>未结算佣金</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }


        .colour1 {
            background-color: #00BFFF;
        }

        .colour2 {
            background-color: #7cb717;
        }

        .colour3 {
            background-color: #985f0d;
        }

        .tongbu {
            background-color: #4c9ef8;
            color: #fff;
        }

        .cur {
            color: #009688;
        }

        .userbox {
            height: 200px;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 63px;
            width: 298px;
            background-color: #ffffff;
            border: 1px solid #ddd;
        }

        .userbox .list {
            height: 38px;
            line-height: 38px;
            cursor: pointer;
            padding-left: 10px;
        }

        .userbox .list:hover {
            background-color: #eeeeee;
        }

        .yname {
            font-size: 13px;
            color: #444;
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12" style="margin-top:0px">
                        <div class="layui-card">
                            <div class="layui-card-header">未结算佣金</div>
                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">


                                    <div class="layui-form-item" style="display: inline-block;">
                                        <div class="layui-inline">
                                            <div class="layui-input-inline">
                                                <text class="yname">代理商名称</text>
                                                <input type="text" name="name" id="name" placeholder="请输入代理商名称"
                                                       autocomplete="off" class="layui-input">
                                            </div>
                                        </div>

                                        <div class="layui-inline">
                                            <button class="layui-btn layuiadmin-btn-list"
                                                    lay-submit="usersListsAllSearch" lay-filter="usersListsAllSearch"
                                                    style="margin-top: 1.3rem;border-radius:5px;margin-bottom: 0;height:36px;line-height: 36px;">
                                                <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                            </button>

                                        </div>
                                        <div class="layui-inline">
                                            <button class="layui-btn layuiadmin-btn-list export" style="border-radius:5px;margin-top:20px;margin-bottom: 4px;height:36px;line-height: 36px;margin-top: 1.5rem">导出</button>
                                        </div>

                                    </div>


                                    <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                    <script type="text/html" id="table-content-list" class="layui-btn-small">

                                        <a class="layui-btn layui-btn-normal layui-btn-xs colour1 agent"
                                           lay-event="openbtn"
                                           style="margin-right:3.5px;border-radius:3.5px">功能管理</a>


                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div id="open_button" class="hide" style="display: none;background-color: #fff">
    <div class="layui-card-body" style="padding: 15px;">
        <div class="layui-form">
            <div class="layui-form-item box">
                <div style="padding-bottom:14px;">
                    <label>代理商名称:</label><span class="agent_name"></span>
                </div>
                <a class="layui-btn layui-btn-normal layui-btn-xs colour1 agent"
                   onclick="agent()"
                   style="margin-right:3.5px;border-radius:3.5px">添加代理商</a>
                <a class="layui-btn layui-btn-normal layui-btn-xs colour2 rate"
                   onclick="rate()"
                   style="margin-right:3.5px;border-radius:3.5px">支付成本费率管理</a>
                <a class="layui-btn layui-btn-normal layui-btn-xs colour3 reward"
                   onclick="reward()"
                   style="margin-right:3.5px;border-radius:3.5px">赏金列表</a>
                <a class="layui-btn  layui-btn-xs layui-btn-normal putforward"
                   onclick="putforward()"
                   style="margin-right:3.5px;border-radius:3.5px">提现记录</a>
                <a class="layui-btn layui-btn-normal layui-btn-xs colour1 storerate"
                   onclick="storerate()"
                   style="margin-right:3.5px;border-radius:3.5px">商户默认费率</a>
                <a class="layui-btn layui-btn-normal layui-btn-xs colour2 storeconfig"
                   onclick="storeconfig()"
                   style="margin-right:3.5px;border-radius:3.5px">门店配置</a>
            </div>
            <div class="layui-form-item box">
                <a class="layui-btn layui-btn-normal layui-btn-xs colour3 chaxun"
                   onclick="chaxun()"
                   style="margin-right:3.5px;border-radius:3.5px">对账查询</a>
                <a class="layui-btn layui-btn-normal layui-btn-xs settle"
                   onclick="settle()"
                   style="margin-right:3.5px;border-radius:3.5px">赏金结算明细</a>
                <a class="layui-btn layui-btn-normal layui-btn-xs colour1 flowerfq"
                   onclick="flowerfq()"
                   style="margin-right:3.5px;border-radius:3.5px">花呗分期成本费率</a>
                <a class="layui-btn layui-btn-normal layui-btn-xs colour2 editmsg"
                   onclick="editmsg()"
                   style="margin-right:3.5px;border-radius:3.5px">修改信息</a>
                <a class="layui-btn layui-btn-normal layui-btn-xs colour3 transfer"
                   onclick="transfer()"
                   style="margin-right:3.5px;border-radius:3.5px">转移代理商</a>
                <a class="layui-btn layui-btn-normal layui-btn-xs szbl"
                   onclick="szbl()"
                   style="margin-right:3.5px;border-radius:3.5px">设置比例</a>
            </div>

        </div>
    </div>
</div>
<input type="hidden" class="store_id">
<input type="hidden" class="user_id">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var str = location.search;
    var user_id = str.split('?')[1];
    var level = sessionStorage.getItem("level");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'table', 'laydate'], function () {
        var $ = layui.$
            , admin = layui.admin
            , form = layui.form
            , table = layui.table
            , laydate = layui.laydate;
        // 未登录,跳转登录页面
        $(document).ready(function () {
            if (token == null) {
                window.location.href = "{{url('/user/login')}}";
            }
        })
        var permissions = sessionStorage.getItem("permissions");
        var str = JSON.parse(permissions);
        var arr = [];
        for (var i = 0; i < str.length; i++) {
            var aa = str[i].name;
            arr.push(aa);
        }
        if (level != 0) {
            $('.box a').each(function (index, item) {
                if ($.inArray($(this).html(), arr) == -1) {
                    $(this).hide();
                }
            });
        }


        //监听搜索
        form.on('submit(usersListsAllSearch)', function () {
            var user_name = document.getElementById('name').value
            //执行重载
            table.reload('test-table-page', {
                page: {
                    curr: 1
                },
                where: {
                    user_name: user_name
                }
            });
        });

        // 渲染表格
        table.render({
            elem: '#test-table-page'
            , url: "{{url('/api/user/users_lists_all')}}"
            , method: 'post'
            , where: {
                token: token,
                user_id: user_id
            }
            , request: {
                pageName: 'p',
                limitName: 'l'
            }
            , page: true
            , cellMinWidth: 150
            , cols: [[
                {field: 'name', title: '代理商名称'}
                , {field: 'phone', title: '手机号'}
                , {field: 'sum', title: '累计收益'}
                , {field: 'tx_amount', title: '已提现金额'}
                , {field: 'money', title: '可提现金额'}
                , {field: 'pid_name', title: '所属代理商'}
                , {field: 'profit_ratio', title: '利润比例'}
                , {
                    field: 'is_withdraw', title: '提现状态', templet: function (d) {
                        var state = "";
                        if (d.is_withdraw == 1) {
                            state = "<input type='checkbox' value='" + d.id + "' id='status' lay-filter='stat' checked='checked' name='status'  lay-skin='switch' lay-text='开启|关闭' >";
                        } else {
                            state = "<input type='checkbox' value='" + d.id + "' id='status' lay-filter='stat'  name='status'  lay-skin='switch' lay-text='开启|关闭' >";

                        }

                        return state;
                    }
                }
                , {width: 120, fixed: 'right', toolbar: '#table-content-list', title: '操作'}
            ]]
            , response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                , statusCode: 1 //成功的状态码，默认：0
                , msgName: 'message' //状态信息的字段名称，默认：msg
                , countName: 't' //数据总数的字段名称，默认：count
                , dataName: 'data' //数据列表的字段名称，默认：data
            }
            , done: function (res, curr, count) {
                $('th').css({
                    'font-weight': 'bold',
                    'font-size': '15',
                    'color': 'black',
                    'background': 'linear-gradient(#f2f2f2,#cfcfcf)'
                });	//进行表头样式设置
            }
            //监听开关事件


        });
        //监听开关事件
        form.on('switch(stat)', function (data) {
            var x = data.elem.checked;//判断开关状态

            if(level!=0){
                layer.alert('该账号没有权限更改提现状态', {
                    icon: 5,
                    title: "提示"
                });
                data.elem.checked = !x;
                form.render();
                return
            }
            var contexts;

            if (x == true) {
                contexts = "打开";
            } else {
                contexts = "关闭";
            }
            layer.open({
                content: '确认' + contexts + '提现设置'
                , btn: ['确定', '取消']
                , yes: function (index, layero) {
                    data.elem.checked = x;
                    $.post("{{url('/api/user/set_withdraw')}}",
                        {
                            token: token,
                            user_id: data.value,
                        }, function (res) {
                            if (res.status == 1) {
                                layer.msg(res.message, {
                                    offset: '50px'
                                    , icon: 1
                                    , time: 1000
                                }, function () {
                                    window.location.reload();
                                });
                            } else {
                                layer.msg(res.message, {
                                    offset: '50px'
                                    , icon: 2
                                    , time: 2000
                                });
                            }
                        }, "json");
                    form.render();
                    layer.close(index);
                    //按钮【按钮一】的回调
                }
                , btn2: function (index, layero) {
                    //按钮【按钮二】的回调
                    data.elem.checked = !x;
                    form.render();
                    layer.close(index);
                    //return false 开启该代码可禁止点击该按钮关闭
                }
                , cancel: function () {
                    //右上角关闭回调
                    data.elem.checked = !x;
                    form.render();
                    //return false 开启该代码可禁止点击该按钮关闭
                }
            });
            return false;

        });
        table.on('tool(test-table-page)', function (obj) {
            var data = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            json = JSON.stringify(data);
            layui.use('layer', function () {
                if (layEvent === 'openbtn') { //添加代理商
                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '600px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_button')
                    });
                    $('.agent_name').html(data.name)
                }
                window.agent = function () {
                    sessionStorage.setItem('agentname', data.name);
                    sessionStorage.setItem('dataid', data.id);
                    $('.agent').attr('lay-href', "{{url('/user/addagent')}}");

                }
                window.rate = function () { //支付成本费率管理
                    sessionStorage.setItem('agentname', data.name);
                    var user_id = data.id;
                    $('.rate').attr('lay-href', "{{url('/user/ratelist?')}}" + user_id);
                }
                window.reward = function () { //赏金列表
                    sessionStorage.setItem('agentname', data.name);
                    var user_id = data.id;
                    $('.reward').attr('lay-href', "{{url('/user/reward?')}}" + user_id);
                }
                window.putforward = function () { //提现记录
                    sessionStorage.setItem('agentname', data.name);
                    var user_id = data.id;
                    $('.putforward').attr('lay-href', "{{url('/user/putforward?')}}" + user_id);
                }
                window.storerate = function () { //商户默认费率
                    sessionStorage.setItem('agentname', data.name);
                    var user_id = data.id;
                    $('.storerate').attr('lay-href', "{{url('/user/storeratelist?')}}" + user_id);
                }
                window.storeconfig = function () { //门店配置
                    sessionStorage.setItem('agentname', data.name);
                    var user_id = data.id;
                    $('.storeconfig').attr('lay-href', "{{url('/user/storeconfig?')}}" + user_id);
                }
                window.chaxun = function () { //对账查询
                    sessionStorage.setItem('agentname', data.name);
                    var user_id = data.id;
                    var user_name = data.name;
                    $('.chaxun').attr('lay-href', "{{url('/user/reconciliation?user_id=')}}" + user_id + "&user_name=" + user_name);
                }
                window.settle = function () { //赏金结算明细
                    sessionStorage.setItem('agentname', data.name);
                    var user_id = data.id;
                    $('.settle').attr('lay-href', "{{url('/user/settledetail?user_id=')}}" + user_id);
                }
                window.flowerfq = function () { //花呗分期
                    sessionStorage.setItem('agentname', data.name);
                    var user_id = data.id;
                    $('.flowerfq').attr('lay-href', "{{url('/user/flowerfq?user_id=')}}" + user_id);
                }
                window.editmsg = function () { //修改信息
                    sessionStorage.setItem('agentname', data.name);
                    var user_id = data.id;
                    $('.editmsg').attr('lay-href', "{{url('/user/editmsg?user_id=')}}" + user_id + "&user_name=" + data.name + "&phone=" + data.phone + "&logo=" + data.logo);
                }
                window.transfer = function () { //转移代理商
                    sessionStorage.setItem('agentname', data.name);
                    var user_id = data.id;
                    $('.transfer').attr('lay-href', "{{url('/user/transferstore?user_id=')}}" + user_id + "&user_name=" + data.name);
                }
                window.szbl = function () { //设置比例
                    layer.open({
                        type: 2,
                        title: '设置比例',
                        shade: false,
                        maxmin: true,
                        area: ['50%', '30%'],
                        content: "{{url('/user/setProfitRatio')}}"
                    });
                }
            });
        });
        $('.export').click(function(){
            var user_name = $('#name').val();


            window.location.href="{{url('/api/export/SettlementCommissionExcelDown')}}"+"?token="+token+"&user_name="+user_name;
        })
    });

</script>

</body>
</html>





