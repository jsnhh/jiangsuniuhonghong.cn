<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>支付宝红包</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
  <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
  <style>
    .edit{background-color: #ed9c3a;}
    .shenhe{background-color: #429488;}
    .see{background-color: #7cb717;}
    .tongbu{background-color: #4c9ef8;color:#fff;}
    .cur{color:#009688;}
    .del{background-color: #e85052;}
    .userbox{
      height:200px;
      overflow-y: auto;
      z-index: 999;
      position: absolute;
      left: 0px;
      top: 42px;
      width:298px;
      background-color:#ffffff;
      border: 1px solid #ddd;
    }
    .userbox .list{
      height:38px;line-height: 38px;cursor:pointer;
      padding-left:10px;
    }
    .userbox .list:hover{
      background-color:#eeeeee;
    }
  </style>
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">

        <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
              <div class="layui-card">
                <div class="layui-card-header">返还设置</div>

                <div class="layui-card-body">
                  <div class="layui-btn-container" style="font-size:14px;">
                    <a class="layui-btn layui-btn-primary addSettingreturn" lay-href="" style="display: block;width: 122px;">设置返还</a>


                  </div>

                  <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                  <script type="text/html" id="rate">
                    @{{ d.rate }}%
                  </script>
                  <!-- 判断状态 -->
                  <script type="text/html" id="acount">
                    @{{#  if(d.r_type == 1){ }}
                      <span>门店</span>
                    @{{#  } else { }}
                      <span>收银员</span>
                    @{{#  } }}
                  </script>
                  <!-- 判断状态 -->
                  <script type="text/html" id="undertaker">
                    @{{#  if(d.zc_type == 0){ }}
                      <span>平台</span>
                    @{{#  } else if(d.zc_type == 1) { }}
                      <span>服务商</span>
                    @{{#  } else { }}
                      <span>代理商</span>
                    @{{#  } }}
                  </script>
                  <script type="text/html" id="table-content-list" class="layui-btn-small">

                    <a class="layui-btn layui-btn-normal layui-btn-xs del" lay-event="del">删除</a>
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
    <script>
    var token = sessionStorage.getItem("Usertoken");
    var str=location.search;
    var store_id = sessionStorage.getItem("store_store_id");


    layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate;
      // 未登录,跳转登录页面
      $(document).ready(function(){
          if(token==null){
              window.location.href="{{url('/user/login')}}";
          }
      })



        $(".transfer").bind("input propertychange",function(event){
         console.log($(this).val())
          $.post("{{url('/api/user/get_sub_users')}}",
          {
              token:token,
              user_name:$(this).val(),
              self:'1'

          },function(res){
              console.log(res);
              var html="";
              console.log(res.t)
              if(res.t==0){
                  $('.userbox').html('')
              }else{
                  for(var i=0;i<res.data.length;i++){
                      html+='<div class="list" data='+res.data[i].id+'>'+res.data[i].name+'-'+res.data[i].level_name+'</div>'
                  }
                  $(".userbox").show()
                  $('.userbox').html('')
                  $('.userbox').append(html)
              }

          },"json");
        });

        $(".userbox").on("click",".list",function(){

          $('.transfer').val($(this).html())
          $('.js_user_id').val($(this).attr('data'))
          $('.userbox').hide()

          table.reload('test-table-page', {
            where: {
              user_id:$(this).attr('data')
            }
            ,page: {
              curr: 1
            }
          });
          // 选择门店
          $.ajax({
            url : "{{url('/api/user/store_lists')}}",
            data : {token:token,user_id:$(this).attr('data'),l:100},
            type : 'post',
            success : function(data) {
              console.log(data);
              var optionStr = "";
                  for(var i=0;i<data.data.length;i++){
                      optionStr += "<option value='" + data.data[i].store_id + "'>"
                        + data.data[i].store_name + "</option>";
                  }
                  $("#store").html('');
                  $("#store").append('<option value="">选择门店</option>'+optionStr);
                  layui.form.render('select');
            },
            error : function(data) {
                alert('查找板块报错');
            }
          });
        })


        // 渲染表格
        table.render({
            elem: '#test-table-page'
            ,url: "{{url('/api/user/fq/set_fq_get_list')}}"
            ,method: 'post'
            ,where:{
              token:token,
              store_id:store_id
            }
            ,request:{
              pageName: 'p',
              limitName: 'l'
            }
            ,page: true
            ,cellMinWidth: 150
            ,cols: [[
              {field:'num', title: '分期数'}
              ,{field:'rate', title: '返还比例',templet:'#rate'}
              ,{field:'alipay', title: '返还账号',templet:'#acount'}
              ,{field:'weixin', title: '承担方',templet:'#undertaker'}
              ,{width:100,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
            ]]
            ,response: {
              statusName: 'status' //数据状态的字段名称，默认：code
              ,statusCode: 1 //成功的状态码，默认：0
              ,msgName: 'message' //状态信息的字段名称，默认：msg
              ,countName: 't' //数据总数的字段名称，默认：count
              ,dataName: 'data' //数据列表的字段名称，默认：data
            }
            ,done: function(res, curr, count){
              console.log(res);

            }

        });



        table.on('tool(test-table-page)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
          var e = obj.data; //获得当前行数据
          var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
          var tr = obj.tr; //获得当前行 tr 的DOM对象
          console.log(e);
          // sessionStorage.setItem('s_store_id', e.store_id);

          if(layEvent === 'del'){
            layer.confirm('确认删除此消息?',{icon: 2}, function(index){
              $.post("{{url('/api/user/fq/del_fq_get_rate')}}",
              {
                 token:token,
                 store_id:store_id,
                 num:e.num

              },function(res){
                  console.log(res);
                  if(res.status==1){
                    obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                    layer.close(index);
                    layer.msg(res.message, {
                      offset: '15px'
                      ,icon: 1
                      ,time: 2000
                    });
                  }else{
                    layer.msg(res.message, {
                        offset: '15px'
                        ,icon: 2
                        ,time: 3000
                    });
                  }
              },"json");

            });
          }


        });



        $('.addSettingreturn').click(function(){
          $(this).attr("lay-href","{{url('/user/Settingreturn?')}}"+store_id)
        })


    });

  </script>

</body>
</html>





