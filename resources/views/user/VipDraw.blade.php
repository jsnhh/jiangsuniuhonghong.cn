<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>地图数据更新</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
<style>
  html{
    background-color: #ffffff;
  }
  .formbox{
    width: 500px;
    margin: 50px auto 0;
  }
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
      -webkit-appearance: none;
  }
  input[type="number"]{
      -moz-appearance: textfield;
  }
</style>
</head>
<body>

    <div class="formbox">
      <form class="layui-form" action="">
        <div class="layui-form-item">
          <label class="layui-form-label">高淳区</label>
          <div class="layui-input-block">
            <input type="number" name="gcq" placeholder="请输入" value="0" autocomplete="off" class="layui-input">
          </div>
        </div>
        <div class="layui-form-item">
          <label class="layui-form-label">鼓楼区</label>
          <div class="layui-input-block">
            <input type="number" name="glq" placeholder="请输入" value="0" autocomplete="off" class="layui-input">
          </div>
        </div>
        <div class="layui-form-item">
          <label class="layui-form-label">建邺区</label>
          <div class="layui-input-block">
            <input type="number" name="jyq" placeholder="请输入" value="0" autocomplete="off" class="layui-input">
          </div>
        </div>
        <div class="layui-form-item">
          <label class="layui-form-label">江宁区</label>
          <div class="layui-input-block">
            <input type="number" name="jnq" placeholder="请输入" value="0" autocomplete="off" class="layui-input">
          </div>
        </div>
        <div class="layui-form-item">
          <label class="layui-form-label">溧水区</label>
          <div class="layui-input-block">
            <input type="number" name="lsq" placeholder="请输入" value="0" autocomplete="off" class="layui-input">
          </div>
        </div>
        <div class="layui-form-item">
          <label class="layui-form-label">六合区</label>
          <div class="layui-input-block">
            <input type="number" name="lhq" placeholder="请输入" value="0" autocomplete="off" class="layui-input">
          </div>
        </div>
        <div class="layui-form-item">
          <label class="layui-form-label">浦口区</label>
          <div class="layui-input-block">
            <input type="number" name="pkq" placeholder="请输入" value="0" autocomplete="off" class="layui-input">
          </div>
        </div>
        <div class="layui-form-item">
          <label class="layui-form-label">栖霞区</label>
          <div class="layui-input-block">
            <input type="number" name="xxq" placeholder="请输入" value="0" autocomplete="off" class="layui-input">
          </div>
        </div>
        <div class="layui-form-item">
          <label class="layui-form-label">秦淮区</label>
          <div class="layui-input-block">
            <input type="number" name="qhq" placeholder="请输入" value="0" autocomplete="off" class="layui-input">
          </div>
        </div>
        <div class="layui-form-item">
          <label class="layui-form-label">玄武区</label>
          <div class="layui-input-block">
            <input type="number" name="xwq" placeholder="请输入" value="0" autocomplete="off" class="layui-input">
          </div>
        </div>
        <div class="layui-form-item">
          <label class="layui-form-label">雨花台区</label>
          <div class="layui-input-block">
            <input type="number" name="yhtq" placeholder="请输入" value="0" autocomplete="off" class="layui-input">
          </div>
        </div>
        <div class="layui-form-item">
          <label class="layui-form-label">剩余名额</label>
          <div class="layui-input-block">
            <input type="number" name="njssyme" placeholder="请输入" value="0" autocomplete="off" class="layui-input">
          </div>
        </div>
        <div class="layui-form-item">
          <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
          </div>
        </div>
      </form>
    </div>
    
<script src="/layuiadmin/layui/layui.js"></script>
<script>
  
   //获取 token ，用户登录是就会有
   var token = window.sessionStorage.getItem("Usertoken");
   layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
      index: 'lib/index', //主入口模块
      formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','laydate','jquery'], function(){
      var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;
            var $ = jquery = layui.jquery;
      // 地图数据回显
     // 未登录,跳转登录页面
     $(document).ready(function(){
       if(token == null){
         window.location.href="{{url('/user/login')}}";
       }
     });
      const getdata = () => {
        return new Promise(resolve=>{
          $.post("/api/user/mapEditName",{
            token:token,
            type:2,
          },function(data){
            resolve(data)
          })
        })
      }
      //修改地图数据
      const setdata = (datalist) => {
        return new Promise(resolve=>{
          $.post("/api/user/mapEditName",{
            token:token,
            type:3,
            id:3,
            nameList:datalist
          },function(data){
            resolve(data)
          })
        })
      }
      //进来就执行的函数
      (async () => {
        const setDataOne =await getdata();
        if(setDataOne.status == 1){ //查询成功
          let  listdatanew = JSON.parse(setDataOne.data[2].name);
          $(".formbox input[name='gcq']").val(listdatanew.gcq);
          $(".formbox input[name='glq']").val(listdatanew.glq);
          $(".formbox input[name='jnq']").val(listdatanew.jnq);
          $(".formbox input[name='jyq']").val(listdatanew.jyq);
          $(".formbox input[name='lhq']").val(listdatanew.lhq);
          $(".formbox input[name='lsq']").val(listdatanew.lsq);
          $(".formbox input[name='pkq']").val(listdatanew.pkq);
          $(".formbox input[name='qhq']").val(listdatanew.qhq);
          $(".formbox input[name='xwq']").val(listdatanew.xwq);
          $(".formbox input[name='xxq']").val(listdatanew.xxq);
          $(".formbox input[name='yhtq']").val(listdatanew.yhtq);
          $(".formbox input[name='njssyme']").val(listdatanew.njssyme);
        }
      })();
      //监听提交
      form.on('submit(formDemo)', data => {
        let datalist = JSON.stringify(data.field);
        setdata(datalist).then(v=>{
          if( v.status == 1 ){
            layer.msg("更新成功", {icon:1, shade:0.5, time:1000});
          }else{
            layer.msg(v.message, {icon:1, shade:0.5, time:1000});
          }
        })
        return false;
      });
    })

</script>
</body>
</html>
