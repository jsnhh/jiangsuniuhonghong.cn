<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>收银员绑定</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
    .layui-card-header{width:80px;text-align: right;float:left;}
    .layui-card-body{margin-left:28px;}
    .layui-upload-img{width: 92px; height: 92px; margin: 0 10px 10px 0;}

    .up{position: relative;display: inline-block;cursor: pointer;border-color: #1ab394; color: #FFF;width: 92px !important;font-size: 10px !important;text-align: center !important;}
    .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
    .layui-upload-list{width: 100px;height:96px;overflow: hidden;}
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {-webkit-appearance: none !important;margin: 0;}

    .line{
        line-height: 36px;
    }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header" style="width:auto !important">绑定收银员&nbsp;&nbsp;&nbsp;<span class="zong_school_name"></span></div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item">
                    <label class="layui-form-label">门店名称</label>
                    <div class="layui-input-block">
                        <div class="line store_name"></div>
                    </div>
                </div>
                <div class="layui-form-item school">
                    <label class="layui-form-label">收银员</label>
                    <div class="layui-input-block">
                        <select name="merchant" id="merchant" lay-filter="merchant">

                        </select>
                    </div>
                </div>
                <div class="layui-form-item school">
                    <label class="layui-form-label">员工角色</label>
                    <div class="layui-input-block">
                        <select name="agent" id="agent" lay-filter="agent">
                            <option value="">选择员工角色</option>
                            <option value="1">店长</option>
                            <option value="2">收银员</option>
                        </select>
                    </div>
                </div>




                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit site-demo-active" data-type="tabChange">确定提交</button>
                            <!--<button type="reset" class="layui-btn layui-btn-primary">重置</button>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<input type="hidden" class="merchant_id">
<input type="hidden" class="type">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var store_name = sessionStorage.getItem("store_store_name");
    var str=location.search;
    var store_id = sessionStorage.getItem("store_store_id");


    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','element'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;
        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
            $('.store_name').html(store_name)
        })
        // 门店***********************
        $.ajax({
          url : "{{url('/api/user/merchant_lists')}}",
          data : {token:token,l:200},
          type : 'post',
          success : function(data) {
              console.log(data);
              var optionStr = "";
              for(var i=0;i<data.data.length;i++){
                  optionStr += "<option value='" + data.data[i].merchant_id + "'>" + data.data[i].name + "</option>";

              }
              $("#merchant").html('');
              $("#merchant").append('<option value="">选择收银员</option>'+optionStr);
              layui.form.render('select');
          },
          error : function(data) {
              alert('查找板块报错');
          }
        });


        form.on('select(merchant)', function(data){
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.merchant_id').val(category);
        });
        form.on('select(agent)', function(data){
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.type').val(category);
        });

        $('.submit').on('click', function(){
            $.post("{{url('/api/user/bind_merchant')}}",
            {
                token:token,
                store_id:store_id,
                merchant_id:$('.merchant_id').val(),
                type:$('.type').val(),

            },function(res){
                console.log(res);

                if(res.status==1){
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 3000
                    });
                }else{
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 3000
                    });

                }

            },"json");
        });

    });
</script>

</body>
</html>
