<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>商户功能设置</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
<link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
<style>
    .layui-card-header{width:80px;text-align: right;float:left;}
    .layui-card-body{margin-left:28px;}
    .layui-upload-img{width: 92px; height: 92px; margin: 0 10px 10px 0;}
    .up{position: relative;display: inline-block;cursor: pointer;border-color: #1ab394; color: #FFF;width: 92px !important;font-size: 10px !important;text-align: center !important;}
    .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
    .layui-upload-list{width: 100px;height:96px;overflow: hidden;}
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {-webkit-appearance: none !important;margin: 0;}
    .seeting{width: 270px;border: 1px solid #EBEEF3;display: flex;justify-content: space-around;margin-top: 10px;padding: 15px;align-items: center;}
    .iconbox{width: 56px;height: 56px;background: #3475C3;border-radius: 50%;}
    .seeting2{
        width: 270px;
        border: 1px solid #EBEEF3;
        display: flex;
        justify-content: space-around;
        margin-top: 10px;
        padding: 15px;
        align-items: center;
        
    }
</style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card" style="margin-top:0px">
        <div class="layui-card-header" style="width:auto !important">商户功能设置&nbsp;&nbsp;&nbsp;<span class="zong_school_name"></span></div>
        <div class="layui-card-body" style="padding: 15px;display: flex;width: 90%;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="seeting">
                    <div class="iconbox">
                    </div>
                    <div>
                        <p>推送模版消息</p>
                    </div>
                    <div>
                    <input type="checkbox" name="noticeNormal" lay-skin="switch" lay-filter="switchTest" lay-text="是|否">
                    </div>
                </div>
            </div>
            <div class="layui-form" lay-filter="component-form-group" style="margin-left: 28px;">
                <div class="seeting2">
                    <div class="iconbox">
                    </div>
                    <div>
                        <p>微官网小程序授权</p>
                    </div>
                    <div>
                    <input type="checkbox" name="noticeNormal" lay-skin="switch" lay-filter="switchTest2" lay-text="是|否">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="agentid">
<input type="hidden" class="agentname">
<input type="hidden" class="pass" value="000000">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var str=location.search;
    var store_id = sessionStorage.getItem("store_store_id");
    var is_send_tpl_mess="{{$_GET['is_send_tpl_mess']}}";
    console.log(is_send_tpl_mess)
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','element'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });

        if(is_send_tpl_mess==1){
           $("input[name=noticeNormal]").prop("checked","checked");
           form.render();
        }else if(is_send_tpl_mess==2){
           $("input[name=noticeNormal]").prop("checked");
           form.render();
        }
        form.on('switch(switchTest)', function(data){
            var status = data.elem.checked ? 1 : 2;
            // console.log(status)
            $.ajax({
                url : "{{url('/api/user/up_store')}}",
                data:{
                    isSendTplMess:status,
                    store_id:store_id,
                    token:token,
                },
                type : 'post',
                success : function(data) {
                    layer.msg('更新成功', {
                        icon: 1
                    }, function() {
                        form.render();
                    });
                },
                error : function(data) {alert('查找板块报错');}
            });
        });

        form.on('switch(switchTest2)', function(data){
            var status = data.elem.checked ? 1 : 2;
            // console.log(status)
            $.ajax({
                url : "{{url('/api/user/up_store')}}",
                data:{
                    is_get_phone:status,
                    store_id:store_id,
                    token:token,
                },
                type : 'post',
                success : function(data) {
                    layer.msg('更新成功', {
                        icon: 1
                    }, function() {
                        form.render();
                    });
                },
                error : function(data) {alert('查找板块报错');}
            });
        });

    });

</script>

</body>
</html>
