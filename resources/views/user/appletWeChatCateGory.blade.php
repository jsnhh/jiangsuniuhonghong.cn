<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>微信小程序类目</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style type="text/css">
        .xgrate{color: #fff;font-size: 15px;padding: 7px;height: 30px;line-height: 30px;background-color: #3475c3;}
        .up{position: relative;}
        .up #uploadFile{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
        .up input[type=file]{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
        .demo5{width: 100px;}
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
        <ul class="layui-tab-title">
            <li class="layui-this">小程序已设置的类目</li>
            <li>小程序添加类目</li>
        </ul>
        <div class="layui-tab-content" style="height: 100px;">
            <div class="layui-tab-item layui-show">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12">
                        <div class="layui-fluid">
                            <div class="layui-row layui-col-space15">
                                <div class="layui-col-md12" style="margin-top:0px">
                                    <div class="layui-card">
                                        <div class="layui-card-header">
                                            <div class="layui-row">
                                                <div class="layui-col-md6">小程序已设置的类目信息</div>
                                                <div class="layui-col-md6"></div>
                                            </div>
                                        </div>
                                        <div class="layui-card-body">
                                            <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                            <script type="text/html" id="table-content-list">
                                                @{{#  if(d.audit_status == 3){ }}
                                                <a class="layui-btn layui-btn-xs" lay-event="deleteThisCate">删除</a>
                                                @{{#  } }}
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-tab-item">
                <form class="layui-form" method="post" lay-filter="useThisTemplate">
                    <div class="layui-form-item">
                        <label class="layui-form-label">请选择类目</label>
                        <div class="layui-input-inline">
                            <select id="first" name="first" lay-filter="first"></select>
                        </div>
                        <div class="layui-input-inline">
                            <select id="second" name="second" lay-filter="second"></select>
                        </div>
                    </div>
                    <div class="ziZhi_show layui-hide">
                        <div>
                            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
                                <legend>需要上传以下资质</legend>
                            </fieldset>
                        </div>
                        <div class="ziZhi_content"></div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button type="submit" class="layui-btn" lay-submit="" lay-filter="useThisTemplateBtn">确定</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</body>
<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script type="text/javascript">
    var token = sessionStorage.getItem("Usertoken");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form', 'upload','table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,table = layui.table
            ,form = layui.form
            ,upload = layui.upload
            ,laydate = layui.laydate;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });

        var url = window.location.search;
        url = url.substr(1);
        url = url.split("&");
        url1 = url[0].split("=");
        url2 = url[1].split("=");
        console.log(url)

        if(!url1[1] || !url2[1]){
            layer.msg("小程序appid不能为空", {
                offset: '50px'
                ,icon: 2
                ,time: 1000
            });
            return false;
        }

        var authorizer_appid = url1[1];
        var refresh_token    = url2[1];

        /**
         * 获取可以设置的所有类目
         **/
        var cateGory = [];
        var exter_list = [];
        $.post("{{url('/api/customer/weixin/getAllCategories')}}",{
            authorizer_appid:authorizer_appid,
            refresh_token:refresh_token,
        },function(data){
            var status = data.status;
            if(status == 200){
                cateGory         = data.data.categories;
                var cateGoryName = "<option value=''>请选择</option>";
                $.each(cateGory,function(index1,item1){
                    if(item1['level'] == 1){
                        cateGoryName+="<option value='"+cateGory[index1].id+"'>"+cateGory[index1].name+"</option>"
                    }
                });
                $('#first').html(cateGoryName);
                form.render("select");
            }else{
                layer.msg(data.message, {
                    offset: '50px'
                    ,icon: 2
                    ,time: 1000
                });
            }
        },"json");

        form.on('select(first)', function(data){
            var value = data.value;
            var second = "<option selected value=''>请选择</option>";
            if(value){
                $.each(cateGory, function(index, item) {
                    if(cateGory[index].father == value && cateGory[index].level == 2){
                        second += "<option value='"+cateGory[index].id+"'>"+cateGory[index].name+"</option>"
                    }
                });
            }
            $('#second').html(second);
            $(".ziZhi_show").addClass("layui-hide");
            $(".ziZhi_content").html("");
            form.render("select");
        });

        form.on('select(second)', function(data){
            var value = data.value;
            if(value){
                $(".ziZhi_show").addClass("layui-hide");
                $.each(cateGory, function(index, item) {
                    if(cateGory[index].id == value && cateGory[index].level == 2){
                        var cateGory_first = cateGory[index];
                        if(cateGory_first.sensitive_type == 1){
                            $(".ziZhi_show").removeClass("layui-hide");
                            exter_list = cateGory_first.qualify.exter_list;
                            var txt = "";
                            $.each(exter_list,function(index1,item1){
                                $.each(exter_list[index1].inner_list,function(index2,item2){
                                    txt += "<div class=\"layui-form-item\">\n" +
                                        "                            <label class=\"layui-form-label\">资质名称</label>\n" +
                                        "                            <div class=\"layui-input-block\">\n" +
                                        "                                <input type=\"text\" name=\"cerTiCate_key_"+index1+"_"+index2+"\" value=\""+item2.name+"\" placeholder=\"请输入资质名称\" class=\"layui-input\">\n" +
                                        "                            </div>\n" +
                                        "                        </div>\n" +
                                        "                        <div class=\"layui-upload\">\n" +
                                        "                            <label class=\"layui-form-label\">上传图片</label>\n" +
                                        "                            <button type=\"button\" class=\"layui-btn up\" id=\"uploadFile1_"+index1+"_"+index2+"\"><input type=\"file\" name=\"file\" id=\"uploadFile_"+index1+"_"+index2+"\" style=\"position: absolute;top: 0;left: 0;\" />上传资质图片</button>\n" +
                                        "                            <div class=\"layui-upload-list\">\n" +
                                        "                                <img class=\"layui-upload-img demo5\" id=\"uploadImg_"+index1+"_"+index2+"\">\n" +
                                        "                                <p id=\"demoText\"></p>\n" +
                                        "                                <input type=\"hidden\" name=\"uploadImg_"+index1+"_"+index2+"\">\n" +
                                        "                            </div>\n" +
                                        "                        </div>";
                                    uploadFile("#uploadFile_"+index1+"_"+index2,"uploadImg_"+index1+"_"+index2);
                                });
                            });
                            $(".ziZhi_content").html(txt);
                            //渲染上传文件
                            $.each(exter_list,function(index1,item1){
                                $.each(exter_list[index1].inner_list,function(index2,item2){
                                    uploadFile("#uploadFile_"+index1+"_"+index2,"uploadImg_"+index1+"_"+index2);
                                });
                            });

                            form.render();
                        }else{
                            $(".ziZhi_content").html("");
                            form.render();
                        }
                    }
                });
            }
        });

        /**
         * 上传资质照片
         **/
        function uploadFile(element1,element2){
            upload.render({
                elem: element1, //绑定元素
                url: "{{url('/api/customer/weixin/uploadFile')}}", //上传接口
                field:"file",
                method:"post",
                type : 'images',
                ext : 'jpg|png|gif',
                before : function(input){
                    //执行上传前的回调  可以判断文件后缀等等
                    layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                },
                done: function(res){
                    //上传完毕回调
                    if(res.status == 200){
                        layer.msg("文件上传成功", {icon:1, shade:0.5, time:1000});
                        $("#"+element2).attr("src", res.data.path);
                        $("input[name="+element2+"]").val(res.data.media_id);
                    }else{
                        layer.msg(res.message, {icon:2, shade:0.5, time:1000});
                    }
                },
                error: function(err){
                    //请求异常回调
                    console.log(err);
                }
            });
        }

        /**
         * 添加类目
         **/
        form.on('submit(useThisTemplateBtn)', function(data){
            var requestData = data.field;
            if(!requestData.first){
                layer.msg("一级类目不可为空", {
                    offset: '50px'
                    ,icon: 2
                    ,time: 1000
                });
                return false;
            }
            if(!requestData.second){
                layer.msg("二级类目不可为空", {
                    offset: '50px'
                    ,icon: 2
                    ,time: 1000
                });
                return false;
            }
            var imgData = [];
            var key = true;
            if(exter_list.length > 0){
                $.each(exter_list,function(index1,item1){
                    if(exter_list[index1].length > 0){
                        $.each(exter_list[index1].inner_list,function(index2,item2){
                            var imgDataFirst = [];
                            if(!requestData['cerTiCate_key_'+index1+'_'+index2]){
                                layer.msg("资质名称不可为空", {
                                    offset: '50px'
                                    ,icon: 2
                                    ,time: 2000
                                });
                                key = false;
                                return false;
                            }else{
                                imgDataFirst.push(requestData['cerTiCate_key_'+index1+'_'+index2]);
                            }
                            if(!requestData['uploadImg_'+index1+'_'+index2]){
                                layer.msg("资质图片不可为空", {
                                    offset: '50px'
                                    ,icon: 2
                                    ,time: 2000
                                });
                                key = false;
                                return false;
                            }else{
                                imgDataFirst.push(requestData['uploadImg_'+index1+'_'+index2]);
                            }
                            imgData.push(imgDataFirst);
                        });
                    }
                });
            }

            if(key){
                layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                $.post("{{url('/api/customer/weixin/addCateGory')}}",{
                    authorizer_appid:authorizer_appid,
                    refresh_token:refresh_token,
                    first:requestData.first,
                    second:requestData.second,
                    certicates:imgData
                },function(data){
                    var status = data.status;
                    if(status == 200){
                        layer.msg(data.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 2000
                        },function(){
                            window.location.reload();
                        });
                    }else{
                        layer.msg(data.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 2000
                        });
                    }
                },"json");
            }
            return false;
        });

        getTable();
        /**
         * 进入该页面，首先 获取小程序已设置的类目信息
         **/
        function getTable(){
            layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
            table.render({
                elem: '#test-table-page',
                url: "{{url('/api/customer/weixin/getSetCateGory')}}",
                method: 'post',
                where:{
                    authorizer_appid:authorizer_appid,
                    refresh_token:refresh_token
                },
                request:{
                    pageName: 'page',
                    limitName: 'count'
                },
                page: true,
                cellMinWidth: 100,
                cols: [
                    [
                        {width:150,field:'first', title: '一级类目 ID',templet: '#appletsId'},
                        {width:150,field:'first_name', title: '一级类目名称'},
                        {width:150,field:'second',  title: '二级类目 ID'},
                        {width:150,field:'second_name',  title: '二级类目名称'},
                        {width:100,field:'audit_status',  title: '审核状态（1-审核中 2-审核不通过 3-审核通过）'},
                        {width:300,field:'audit_reason',  title: '审核不通过原因'},
                        {width:600,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
                    ]
                ],
                response: {
                    statusName: 'status', //数据状态的字段名称，默认：code
                    statusCode: 200, //成功的状态码，默认：0
                    msgName: 'message', //状态信息的字段名称，默认：msg
                    countName: 'total_count', //数据总数的字段名称，默认：count
                    dataName: 'data', //数据列表的字段名称，默认：data
                },
                done: function(res, curr, count){
                    layer.msg("已完成", {
                        offset: '50px'
                        ,icon: 1
                        ,time: 1000
                    });
                    //进行表头样式设置
                    $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});
                }
            });
        }

        /**
         * 表格的操作列中的每个操作项
         */
        table.on('tool(test-table-page)', function(obj){
            var lineData = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            if(layEvent === 'deleteThisCate'){
                //使用该模板
                layer.confirm('确认删除该类目吗?',{icon: 2}, function(index){
                    layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                    $.post("{{url('/api/customer/weixin/deleteCateGory')}}",{
                        authorizer_appid:authorizer_appid,
                        refresh_token:refresh_token,
                        first:lineData.first,
                        second:lineData.second,
                    },function(data){
                        var status = data.status;
                        if(status == 200){
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 1000
                            },function(){
                                window.location.reload();
                            });
                        }else{
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 1000
                            });
                        }
                    },"json");
                });
            }
        });
    });

</script>
</html>