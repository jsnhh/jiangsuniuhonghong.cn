<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>对账统计</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
<link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
<style>
  .userbox,.storebox{
    height:200px;
    overflow-y: auto;
    z-index: 999;
    position: absolute;
    left: 0px;
    top: 63px;
    width:298px;
    background-color:#ffffff;
    border: 1px solid #ddd;
  }
  .userbox .list,.storebox .list{
    height:38px;line-height: 38px;cursor:pointer;
    padding-left:10px;
  }
  .userbox .list:hover,.storebox .list:hover{
    background-color:#eeeeee;
  }
</style>
</head>
<body>

  <div class="layui-fluid" style="margin-top:0px;">
    <!-- 筛选------------------------------------------------------------ -->
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">
        <div class="layui-card"> 
          <div class="layui-card-header a_name">代理月对账查询</div>
          <div class="layui-card-body">
            <div class="layui-btn-container" style="font-size:14px;">
              <div class="layui-form" lay-filter="component-form-group" style="width:300px;display: inline-block;">
                <div class="layui-form-item">                          
                  <div class="layui-input-block" style="margin-left:0">
                    <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入代理商名称" class="layui-input transfer">
                    <div class="userbox" style='display: none'></div>
                  </div>
                </div>
              </div>
              
              <!-- 通道类型 -->
              <div class="layui-form" lay-filter="component-form-group" style="width:300px;display: inline-block;">
                <div class="layui-form-item">                          
                  <div class="layui-input-block" style="margin-left:0">
                      <select name="usercon" id="usercon" lay-filter="usercon">
                        <option value="">请选择</option>
                        <option value="0">仅自己</option>
                        <option value="1">含下级</option>
                      </select>
                  </div>
                </div>
              </div>

              <!-- 支付状态 -->
              <div class="layui-form" lay-filter="component-form-group" style="width:300px;display: inline-block;">
                <div class="layui-form-item">                          
                  <div class="layui-input-block" style="margin-left:0">
                      <select name="status" id="status" lay-filter="status">
                        <option value="">选择支付类型</option>
                        <option value="alipay">支付宝</option>
                        <option value="weixin">微信</option>
                        <option value="alipay_face">支付宝刷脸</option>
                        <option value="weixin_face">微信刷脸</option>
                        <option value="member">会员支付</option>
                        <option value="jd">京东</option>
                        <option value="unionpay">银联刷卡</option>
                        <option value="unionpayqr">银联扫码</option>
                        <option value="unionpaysf">银联闪付</option>
                      </select>
                  </div>
                </div>
              </div>

              <!-- 通道类型 -->
              <div class="layui-form" lay-filter="component-form-group" style="width:300px;display: inline-block;">
                <div class="layui-form-item">                          
                  <div class="layui-input-block" style="margin-left:0">
                      <select name="passway" id="passway" lay-filter="passway">
                      </select>
                  </div>
                </div>
              </div>
                                  
              <!-- 缴费时间 -->
              <div class="layui-form" style="display: inline-block;">                      
                <div class="layui-form-item">                          
                  <div class="layui-inline">
                    <div class="layui-input-inline">
                      <input type="text" class="layui-input start-item test-item" placeholder="请选择月份" lay-key="23">
                      </div>
                    </div>
                </div>
              </div> 

              <!-- 搜索 -->
              <div class="layui-form" lay-filter="component-form-group" style="width:600px;display: inline-block;">
                <div class="layui-form-item">                    
                    <div class="layui-inline">
                      <button class="layui-btn layuiadmin-btn-list" lay-submit="" lay-filter="LAY-app-contlist-search" style="margin-bottom: 0;height:36px;line-height: 36px;">
                        <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                      </button>
                    </div>
                    {{--<button class="layui-btn export" style="margin-bottom: 4px;height:36px;line-height: 36px;">导出</button>--}}
                  </div>
              </div>

            </div> 

            <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table> 

          </div>
        </div>
      </div>
    </div>

  </div>

  </div>
  <div id="main" style="width: 600px;height:400px;"></div>

  <input type="hidden" class="user_id">
  <input type="hidden" class="source_type">
  <input type="hidden" class="type">
  <input type="hidden" class="starttime">
  <input type="hidden" class="own">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>   
<script>
    var token = sessionStorage.getItem("Usertoken");
    var agentName = sessionStorage.getItem("duizhang_agentName");

    layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate;
      // 未登录,跳转登录页面
      $(document).ready(function(){
        if(token==null){
          window.location.href="{{url('/user/login')}}";
        }
      });
        // 渲染表格
        table.render({
            elem: '#test-table-page'
            ,url: "{{url('/api/user/Order_mouth_count')}}"
            ,method: 'post'
            ,where:{
              token:token
            }
            ,request:{
              pageName: 'p',
              limitName: 'l'
            }
            ,page: true
            ,cellMinWidth: 150
            ,cols: [[
              {field:'phone', title: '代理手机号'}
              ,{field:'name', title: '代理名称'}
              ,{field:'month', title: '月份'}
              ,{field:'company', title: '通道'}
              ,{field:'source_type', title: '支付方式'}
              ,{field:'total_amount', title: '交易金额'}
              ,{field:'order_sum', title: '交易笔数'}
              ,{field:'refund_amount',  title: '退款金额'}
              ,{field:'refund_count', title: '退款笔数'}
              ,{field:'fee_amount',  title: '手续费'}
              // ,{width:150,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
            ]]
            ,response: {
              statusName: 'status' //数据状态的字段名称，默认：code
              ,statusCode: 1 //成功的状态码，默认：0
              ,msgName: 'message' //状态信息的字段名称，默认：msg
              ,countName: 't' //数据总数的字段名称，默认：count
              ,dataName: 'data' //数据列表的字段名称，默认：data
            }
            ,done: function(res, curr, count){
              console.log(res);
              $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});	//进行表头样式设置
            }
        });

        // 选择门店
        $.ajax({
          url : "{{url('/api/user/store_lists')}}",
          data : {token:token,l:100},
          type : 'post',
          success : function(data) {
              console.log(data);
              var optionStr = "";
                  for(var i=0;i<data.data.length;i++){
                      optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
                  }
                  $("#schooltype").append('<option value="">选择门店</option>'+optionStr);
                  layui.form.render('select');
          },
          error : function(data) {
              alert('查找板块报错');
          }
        });

        // 选择通道
        $.ajax({
            url : "{{url('/api/user/store_open_pay_way_lists')}}",
            data : {token:token,l:100},
            type : 'post',
            dataType:'json',
            success : function(data) {
                console.log(data);
                var optionStr = "";
                    for(var i=0;i<data.data.length;i++){
                        optionStr += "<option value='" + data.data[i].company + "'>"
                            + data.data[i].company_desc + "</option>";
                    }
                    $("#passway").append('<option value="">选择通道类型</option>'+optionStr);
                    layui.form.render('select');
            },
            error : function(data) {
                alert('查找板块报错');
            }
        });

        // 业务员
        $(".transfer").bind("input propertychange",function(event){
    //       console.log($(this).val());
            $.post("{{url('/api/user/get_sub_users')}}",
            {
                token:token,
                user_name:$(this).val(),
                self:'1'

            },function(res){
                console.log(res);
                var html="";
                console.log(res.t)
                if(res.t==0){
                    $('.userbox').html('')
                }else{
                    for(var i=0;i<res.data.length;i++){
                        html+='<div class="list" data='+res.data[i].id+'>'+res.data[i].name+'-'+res.data[i].level_name+'</div>'
                    }
                    $(".userbox").show()
                    $('.userbox').html('')
                    $('.userbox').append(html)
                }

            },"json");
        });

        $(".userbox").on("click",".list",function(){

          $('.transfer').val($(this).html());
          $('.user_id').val($(this).attr('data'));
          $('.userbox').hide();
          // acount();
        });

    //     $(".transfer").blur(function(){
    //     $('.userbox').hide()
    //     });

        // 选择门店
        form.on('select(usercon)', function(data){
          var usercon = data.value;
          $('.own').val(usercon);
        });

        // 选择支付类型
        form.on('select(status)', function(data){
          var source_type = data.value;
          $('.source_type').val(source_type);
        });

        // 选择通道类型
        form.on('select(passway)', function(data){
          var type = data.value;
          $('.type').val(type);
        });

        laydate.render({
          elem: '.start-item'
          ,type: 'month'
          ,done: function(value){
            var reg = new RegExp("-","");
            var a = value.replace(reg,"");
            console.log(a);
            $('.starttime').val(a)
          }
        });

        //监听搜索
        form.on('submit(LAY-app-contlist-search)', function(data){
          // var obj = data.field
          // console.log(obj)
          // var store_name = data.field.schoolname;
          // console.log(data);
    //      console.log($('.starttime').val());
    //      console.log($('.user_id').val());
    //      console.log($('.source_type').val());
    //      console.log($('.type').val());
    //      console.log($('.own').val());

          //执行重载
          table.reload('test-table-page', {
            page: {
             curr: 1
            },
            where: {
              user_id:$('.user_id').val()
              ,month:$('.starttime').val()
              ,source_type:$('.source_type').val()
              ,company:$('.type').val()
              ,own:$('.own').val()
            }
          });
        });

        $('.export').click(function(){
          var store_id=$('.store_id').val();
          var month=$('.starttime').val();
          var source_type=$('.source_type').val();
          var company=$('.type').val();

          window.location.href="{{url('/api/export/OrderMounthCountExportDown')}}"+"?token="+token+"&store_id="+store_id+"&month="+month+"&source_type="+source_type+"&company="+company;
        })
        
    });

  </script>

</body>
</html>