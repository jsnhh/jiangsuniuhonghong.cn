<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>添加返手续费</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
    .layui-card-header{width:80px;text-align: right;float:left;}
    .layui-card-body{margin-left:28px;}
    .layui-upload-img{width: 92px; height: 92px; margin: 0 10px 10px 0;}

    .up{position: relative;display: inline-block;cursor: pointer;border-color: #1ab394; color: #FFF;width: 92px !important;font-size: 10px !important;text-align: center !important;}
    .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
    .layui-upload-list{width: 100px;height:96px;overflow: hidden;}
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {-webkit-appearance: none !important;margin: 0;}

    .width{
      width:94%;float:left;
    }
    p{
      float:left;line-height: 36px;margin-left:10px;
    }

    .userbox,.storebox{
      height:200px;
      overflow-y: auto;
      z-index: 999;
      position: absolute;
      left: 0px;
      top: 42px;
      width:298px;
      background-color:#ffffff;
      border: 1px solid #ddd;
    }
    .userbox .list,.storebox .list{
      height:38px;line-height: 38px;cursor:pointer;
      padding-left:10px;
    }
    .userbox .list:hover,.storebox .list:hover{
      background-color:#eeeeee;
    }
    .s_id{
      line-height: 36px;
    }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card">
      <div class="layui-card-header" style="width:auto !important">设置返还&nbsp;&nbsp;&nbsp;<span class="zong_school_name"></span></div>
      <div class="layui-card-body" style="padding: 15px;">
        <div class="layui-form" lay-filter="component-form-group">
          <div class="layui-form-item school">
            <label class="layui-form-label">返还承担方</label>
            <div class="layui-input-block">
                <select name="undertaker" id="undertaker" lay-filter="undertaker">
                    <option value="">请选择返还承担方</option>
                    <option value="0">平台</option>
                    <option value="1">服务商</option>
                    <option value="2">代理商</option>
                </select>
            </div>
          </div>
          <div class="layui-form-item school">
            <label class="layui-form-label">选择期数</label>
            <div class="layui-input-block">
                <select name="qishu" id="qishu" lay-filter="qishu">
                    <option value="">清选择分期数</option>
                    <option value="3">3期</option>
                    <option value="6">6期</option>
                    <option value="12">12期</option>
                </select>
            </div>
          </div>
          <div class="layui-form-item school">
            <label class="layui-form-label">商户账户类型</label>
            <div class="layui-input-block">
                <select name="acount" id="acount" lay-filter="acount">
                    <option value="">请选择返还账户</option>
                    <option value="1">门店</option>
                    <option value="2">收银员</option>

                </select>
            </div>
          </div>
          <div class="layui-form-item">
            <label class="layui-form-label">设置比例</label>
            <div class="layui-input-block">
                <input type="number" placeholder="其输入返还比例" class="layui-input width alipayf"><p>%<p>
            </div>
          </div>

          <div class="layui-form-item layui-layout-admin">
              <div class="layui-input-block">
                  <div class="layui-footer" style="left: 0;">
                      <button class="layui-btn submit site-demo-active" data-type="tabChange">确定提交</button>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
</div>


<input type="hidden" class="undertaker_val">
<input type="hidden" class="qishu_val">
<input type="hidden" class="acount_val">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var str=location.search;
    var store_id = sessionStorage.getItem("store_store_id");


    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','element','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;

            var src=$('#demo1').attr('src');
        element.render();

// 未登录,跳转登录页面
        $(document).ready(function(){
            if(token == null){
                window.location.href="{{url('/user/login')}}";
            }
        });
        form.on('select(undertaker)', function(data){
          category = data.value;
          categoryName = data.elem[data.elem.selectedIndex].text;
          $('.undertaker_val').val(category);
        });
        form.on('select(qishu)', function(data){
          category = data.value;
          categoryName = data.elem[data.elem.selectedIndex].text;
          $('.qishu_val').val(category);
        });
        form.on('select(acount)', function(data){
          category = data.value;
          categoryName = data.elem[data.elem.selectedIndex].text;
          $('.acount_val').val(category);
        });



        $(".alipayf").bind("input propertychange",function(event){
          console.log($(this).val())
          if($(".alipayf").val()>10){
            layer.msg("返还比例不得超过10%", {
              offset: '15px'
              ,icon: 8
              ,time: 3000
            });
          }
        });




        $('.submit').on('click', function(){

            $.post("{{url('/api/user/fq/set_fq_get_rate')}}",
            {
                token:token,
                store_id:store_id,

                zc_type:$('.undertaker_val').val(),
                r_type:$('.acount_val').val(),
                num:$('.qishu_val').val(),
                rate:$('.alipayf').val(),

            },function(res){
                console.log(res);

                if(res.status==1){
                    layer.msg(res.message, {
                        offset: '15px'
                        ,icon: 1
                        ,time: 3000
                    });
                }else{
                    layer.msg(res.message, {
                        offset: '15px'
                        ,icon: 2
                        ,time: 3000
                    });

                }

            },"json");

        });



    });
</script>

</body>
</html>
