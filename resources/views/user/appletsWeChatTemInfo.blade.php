<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>微信小程序模板消息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style type="text/css">
        .xgrate{color: #fff;font-size: 15px;padding: 7px;height: 30px;line-height: 30px;background-color: #3475c3;}
        .up #uploadFile{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
        #demo5{width: 200px;}
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
        <ul class="layui-tab-title">
            <li class="layui-this">小程序模板消息</li>
            <li>小程序模板列表</li>
        </ul>
        <div class="layui-tab-content" style="height: 100px;">
            <div class="layui-tab-item layui-show">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12">
                        <div class="layui-fluid">
                            <div class="layui-row layui-col-space15">
                                <div class="layui-col-md12" style="margin-top:0px">
                                    <div class="layui-card">
                                        <div class="layui-card-header">
                                            <div class="layui-row">
                                                <div class="layui-col-md6">
                                                    第三方平台小程序模板消息
                                                </div>
                                                <div class="layui-col-md6">
                                                    <form class="layui-form" action="" lay-filter="selectType">
                                                        <div class="layui-form-item">
                                                            <label class="layui-form-label">选择类目</label>
                                                            <div class="layui-input-inline">
                                                                <select id="Leimu" name="selectLei" lay-filter="selectLei"></select>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-card-body">
                                            <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                            <script type="text/html" id="table-content-list">
                                                @{{#  if(d.status == 1){ }}
                                                <a class="layui-btn layui-btn-xs" lay-event="deleteThisTemplate">停止使用该模板</a>
                                                <a class="layui-btn layui-btn-xs" lay-event="updateUseIndex">修改使用位置</a>
                                                @{{#  } }}

                                                @{{#  if(d.status == 2){ }}
                                                <a class="layui-btn layui-btn-xs" lay-event="useThisTemplate">使用该模板</a>
                                                @{{#  } }}

                                                {{--<a class="layui-btn layui-btn-xs" lay-event="lookKeyword">查看模板关键词</a>--}}
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-tab-item">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12">
                        <div class="layui-fluid">
                            <div class="layui-row layui-col-space15">
                                <div class="layui-col-md12" style="margin-top:0px">
                                    <div class="layui-card">
                                        <div class="layui-card-header">第三方平台小程序模板列表</div>
                                        <div class="layui-card-body">
                                            <div class="layui-row">
                                                <div class="layui-col-xs3">
                                                    <div class="grid-demo grid-demo-bg1">模板id</div>
                                                </div>
                                                <div class="layui-col-xs1">
                                                    <div class="grid-demo grid-demo-bg1">模板标题</div>
                                                </div>
                                                <div class="layui-col-xs4">
                                                    <div class="grid-demo grid-demo-bg1">模板内容</div>
                                                </div>
                                                <div class="layui-col-xs4">
                                                    <div class="grid-demo grid-demo-bg1">模板示例</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-card-body templateList" style="margin-top: 20px;">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>

<div id="useThisTemplate" class="hide layui-form" lay-filter="useThisTemplate" style="display: none;background-color: #fff;">
    <div class="xgrate">请选择该模板关键词</div>
    <div class="layui-card-body" style="padding: 15px;">
        <div class="layui-form-item" pane="">
            <label class="layui-form-label">选择使用的关键词</label>
            <div class="layui-input-block keywordTemplate"></div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <div class="layui-footer" style="left: 0;">
                    <button class="layui-btn useThisTemplateBtn" lay-submit lay-filter="useThisTemplateBtn" style="border-radius:5px">确定</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="lookKeyword" class="hide layui-form" lay-filter="lookKeyword" style="display: none;background-color: #fff;">
    <div class="xgrate">关键词</div>
    <div class="layui-card-body keywordTP" style="padding: 15px;"></div>
</div>

<div id="useThisTemplateIndex" class="hide layui-form" lay-filter="useThisTemplateIndex" style="display: none;background-color: #fff;">
    <div class="xgrate">
        <div class="layui-row">
            <div class="layui-col-md6">
                模板放置位置
            </div>
            <div class="layui-col-md6">
                <a class="btn-info" lay-href="{{url('/user/appletWeChatTemplateIndex')}}">位置管理</a>
            </div>
        </div>
    </div>
    <div class="layui-card-body" style="padding: 15px;">
        <div class="layui-form-item" pane="">
            <label class="layui-form-label">请选择</label>
            <div class="layui-input-block keywordTemplateIndex"></div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <div class="layui-footer" style="left: 0;">
                    <button class="layui-btn useThisTemplateBtnIndex" lay-submit lay-filter="useThisTemplateBtnIndex" style="border-radius:5px">确定</button>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script type="text/javascript">
    var token = sessionStorage.getItem("Usertoken");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form', 'upload','table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,table = layui.table
            ,form = layui.form
            ,upload = layui.upload
            ,laydate = layui.laydate;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });

        var url = window.location.search;
        url = url.substr(1);
        url = url.split("&");
        url1 = url[0].split("=");
        url2 = url[1].split("=");

        if(!url1[1] || !url2[1]){
            layer.msg("小程序appid不能为空", {
                offset: '50px'
                ,icon: 2
                ,time: 1000
            });
            return false;
        }

        var authorizer_appid = url1[1];
        var refresh_token    = url2[1];

        console.log(authorizer_appid,refresh_token);

        /**
         * 进入该页面，首先 获取小程序所设置的类目信息
         **/
        $.post("{{url('/api/customer/weixin/getWeChatCateGory')}}",{
            authorizer_appid:authorizer_appid,
            refresh_token:refresh_token,
            type:1
        },function(data){
            var status = data.status;
            if(status == 200){
                //类目id
                var cateGory     = data.data;
                var cateGoryName = "<option selected value=''>请选择</option>";
                for (var i=0;i<cateGory.length;i++){
                    if(i == 0){
                        cateGoryName+="<option selected value='"+cateGory[i].id+"'>"+cateGory[i].name+"</option>"
                    }else{
                        cateGoryName+="<option value='"+cateGory[i].id+"'>"+cateGory[i].name+"</option>"
                    }
                }
                $('#Leimu').html(cateGoryName);
                form.render("select");

                //请求表格数据
                if(cateGory.length > 0){
                    getTable(cateGory[0].id);
                }
            }else{
                layer.msg(data.message, {
                    offset: '50px'
                    ,icon: 2
                    ,time: 1000
                });
            }
        },"json");

        form.on("select(selectLei)",function(data){
            var value = data.value;
            //请求表格数据
            if(value){
                getTable(value);
            }
        });

        /**
         * 进入该页面，初始化渲染该表格
         */
        function getTable(ids){
            layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
            table.render({
                elem: '#test-table-page',
                url: "{{url('/api/customer/weixin/getWeChatCateGory')}}",
                method: 'post',
                where:{
                    authorizer_appid:authorizer_appid,
                    refresh_token:refresh_token,
                    type:2,
                    ids:ids
                },
                request:{
                    pageName: 'page',
                    limitName: 'count'
                },
                page: true,
                cellMinWidth: 100,
                cols: [
                    [
                        {width:200,field:'categoryId', title: 'categoryId',templet: '#appletsId'},
                        {width:200,field:'tid', title: 'tid'},
                        {width:200,field:'title',  title: '模板标题'},
                        {width:300,field:'type_name',  title: '模板类型'},
                        {width:750,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
                    ]
                ],
                response: {
                    statusName: 'status', //数据状态的字段名称，默认：code
                    statusCode: 200, //成功的状态码，默认：0
                    msgName: 'message', //状态信息的字段名称，默认：msg
                    countName: 'total_count', //数据总数的字段名称，默认：count
                    dataName: 'data', //数据列表的字段名称，默认：data
                },
                done: function(res, curr, count){
                    layer.msg("已完成", {
                        offset: '50px'
                        ,icon: 1
                        ,time: 1000
                    });
                    //进行表头样式设置
                    $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});
                }
            });
        }

        /**
         * 表格的操作列中的每个操作项
         */
        table.on('tool(test-table-page)', function(obj){
            var lineData = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            if(layEvent === 'useThisTemplate'){
                //使用该模板
                layer.confirm('确认使用该模板吗?',{icon: 2}, function(index){
                    layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                    $.post("{{url('/api/customer/weixin/getWeChatCateGory')}}",{
                        authorizer_appid:authorizer_appid,
                        refresh_token:refresh_token,
                        type:6,
                        tid:lineData.tid,
                    },function(data){
                        var status = data.status;
                        var data   = data.data;
                        if(status == 200){
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 1000
                            },function(){
                                //选择关键词
                                var lookKeyword = layer.open({
                                    type: 1,
                                    title: false,
                                    closeBtn: 0,
                                    area: '516px',
                                    skin: 'layui-layer-nobg', //没有背景色
                                    shadeClose: true,
                                    content: $('#useThisTemplate')
                                });
                                var keywordTemplate = "";
                                if(data.length > 0){
                                    $.each(data,function(index,item){
                                        keywordTemplate = keywordTemplate + "<input type=\"checkbox\" name=\"keyword["+item.kid+"]\" value=\""+item.kid+"\" lay-skin=\"primary\" title=\""+item.name+"\">";
                                    });
                                }
                                $(".keywordTemplate").html(keywordTemplate);
                                form.render();
                            });
                        }else{
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 1000
                            });
                        }
                    },"json");
                });

                //点击确定，提交进行请求，使用该模板
                form.on('submit(useThisTemplateBtn)', function(data){
                    var value = data.field;
                    var kidList = [];
                    for(item in value){
                        kidList.push(value[item]);
                    }

                    if(kidList.length < 2 || kidList.length > 5){
                        layer.msg("最多支持5个，最少2个关键词组合", {
                            offset: '50px'
                            ,icon: 2
                            ,time: 1000
                        });
                        return false;
                    }

                    kidList = kidList.join(",");
                    layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                    $.post("{{url('/api/customer/weixin/getWeChatCateGory')}}",{
                        authorizer_appid:authorizer_appid,
                        refresh_token:refresh_token,
                        type:3,
                        tid:lineData.tid,
                        kidList:kidList,
                        title:lineData.title,
                        categoryId:lineData.categoryId,
                        sceneDesc:"使用该模板进行发送消息",
                    },function(data){
                        var status = data.status;
                        if(status == 200){
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 1000
                            },function(){
                                window.location.reload();
                            });
                        }else{
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 1000
                            });
                        }
                    },"json");
                });

            }else if(layEvent === 'lookKeyword'){
                //查看模板关键词
                layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                $.post("{{url('/api/customer/weixin/getWeChatCateGory')}}",{
                    authorizer_appid:authorizer_appid,
                    refresh_token:refresh_token,
                    type:6,
                    tid:lineData.tid,
                },function(data){
                    var status = data.status;
                    var data   = data.data;
                    if(status == 200){
                        layer.msg(data.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 1000
                        },function(){

                        });
                        //打开目录
                        var lookKeyword = layer.open({
                            type: 1,
                            title: false,
                            closeBtn: 0,
                            area: '516px',
                            skin: 'layui-layer-nobg', //没有背景色
                            shadeClose: true,
                            content: $('#lookKeyword')
                        });
                        var keywordTemplate = "<div class=\"layui-row\">\n" +
                            "            <div class=\"layui-col-xs4\">\n" +
                            "                <div class=\"grid-demo grid-demo-bg1\">kid</div>\n" +
                            "            </div>\n" +
                            "            <div class=\"layui-col-xs4\">\n" +
                            "                <div class=\"grid-demo\">名称</div>\n" +
                            "            </div>\n" +
                            "            <div class=\"layui-col-xs4\">\n" +
                            "                <div class=\"grid-demo grid-demo-bg1\">示例</div>\n" +
                            "            </div>\n" +
                            "        </div>";
                        if(data.length > 0){
                            $.each(data,function(index,item){
                                keywordTemplate = keywordTemplate + "<div class=\"layui-row\">\n" +
                                    "            <div class=\"layui-col-xs4\">\n" +
                                    "                <div class=\"grid-demo grid-demo-bg1\">"+item.kid+"</div>\n" +
                                    "            </div>\n" +
                                    "            <div class=\"layui-col-xs4\">\n" +
                                    "                <div class=\"grid-demo\">"+item.name+"</div>\n" +
                                    "            </div>\n" +
                                    "            <div class=\"layui-col-xs4\">\n" +
                                    "                <div class=\"grid-demo grid-demo-bg1\">"+item.example+"</div>\n" +
                                    "            </div>\n" +
                                    "        </div>";
                            });
                        }
                        $(".keywordTP").html(keywordTemplate);
                        form.render();
                    }else{
                        layer.msg(data.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 1000
                        });
                    }
                },"json");
            }else if(layEvent === 'deleteThisTemplate'){
                //停止使用该模板
                layer.confirm('确认停止使用该模板吗?',{icon: 2}, function(index){
                    layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                    $.post("{{url('/api/customer/weixin/getWeChatCateGory')}}",{
                        authorizer_appid:authorizer_appid,
                        refresh_token:refresh_token,
                        type:5,
                        tid:lineData.tid,
                        title:lineData.title,
                        categoryId:lineData.categoryId,
                        template_id:lineData.template_id
                    },function(data){
                        var status = data.status;
                        if(status == 200){
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 1000
                            },function(){
                                window.location.reload();
                            });
                        }else{
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 1000
                            });
                        }
                    },"json");
                });
            }else if(layEvent === 'updateUseIndex'){
                var lookKeyword;
                layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                $.post("{{url('/api/customer/weixin/getAllUseIndex')}}",{
                    authorizer_appid:authorizer_appid,
                    refresh_token:refresh_token,
                    is_select:1
                },function(data){
                    var status = data.status;
                    if(status == 200){
                        layer.msg(data.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 1000
                        });
                        var data = data.data;
                        var keywordTemplate = "";
                        //打开目录
                        lookKeyword = layer.open({
                            type: 1,
                            title: false,
                            closeBtn: 0,
                            area: '516px',
                            skin: 'layui-layer-nobg', //没有背景色
                            shadeClose: true,
                            content: $('#useThisTemplateIndex')
                        });
                        if(data.length > 0){
                            $.each(data,function(index,item){
                                if(item.is_use == true){
                                    keywordTemplate = keywordTemplate + "<input type=\"checkbox\" name=\"like_"+item.id+"\" checked title=\""+item.title+"\">\n";
                                }else{
                                    keywordTemplate = keywordTemplate + "<input type=\"checkbox\" name=\"like_"+item.id+"\" title=\""+item.title+"\">\n";
                                }
                            });
                        }
                        $(".keywordTemplateIndex").html(keywordTemplate);
                        form.render();
                    }else{
                        layer.msg(data.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 1000
                        });
                    }
                },"json");

                //点击确定，提交位置信息
                form.on('submit(useThisTemplateBtnIndex)', function(data) {
                    var value = data.field;
                    var key;
                    var requestData = [];
                    for (key in value){
                        var key_val = key.substr(5);
                        requestData.push(key_val);
                    }
                    layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
                    $.post("{{url('/api/customer/weixin/updateTemplateUseIndex')}}",{
                        authorizer_appid:authorizer_appid,
                        refresh_token:refresh_token,
                        template_id:lineData.template_id,
                        use_index:requestData
                    },function(data){
                        var status = data.status;
                        if(status == 200){
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 1000
                            },function(){
                                layer.close(lookKeyword);
                            });
                        }else{
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 1000
                            });
                        }
                    });
                });
            }
        });

        /**
         * 获取小程序模板列表
         */
        $.post("{{url('/api/customer/weixin/getWeChatCateGory')}}",{
            authorizer_appid:authorizer_appid,
            refresh_token:refresh_token,
            type:4
        },function(data){
            var status = data.status;
            var data   = data.data;
            if(status == 200){
                var keywordTemplate = "";
                if(data.length > 0){
                    $.each(data,function(index,item){
                        keywordTemplate = keywordTemplate + "<div class=\"layui-row\"><div class=\"layui-col-xs3\">\n" +
                            "                                                    <div class=\"grid-demo grid-demo-bg1\">"+item.priTmplId+"</div>\n" +
                            "                                                </div>\n" +
                            "                                                <div class=\"layui-col-xs1\">\n" +
                            "                                                    <div class=\"grid-demo grid-demo-bg1\">"+item.title+"</div>\n" +
                            "                                                </div>\n" +
                            "                                                <div class=\"layui-col-xs4\">\n" +
                            "                                                    <div class=\"grid-demo grid-demo-bg1\">"+item.content+"</div>\n" +
                            "                                                </div>\n" +
                            "                                                <div class=\"layui-col-xs4\">\n" +
                            "                                                    <div class=\"grid-demo grid-demo-bg1\">"+item.example+"</div>\n" +
                            "                                                </div></div>";
                    });
                }
                $(".templateList").html(keywordTemplate);
            }else{
                layer.msg(data.message, {
                    offset: '50px'
                    ,icon: 2
                    ,time: 1000
                });
            }
        },"json");

    });

</script>
</html>