<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>分润账单</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
<link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
<style>
    .userbox,.storebox{
        height:200px;
        overflow-y: auto;
        z-index: 999;
        position: absolute;
        left: 0px;
        top: 63px;
        width:298px;
        background-color:#ffffff;
        border: 1px solid #ddd;
    }
    .userbox .list,.storebox .list{
        height:38px;line-height: 38px;cursor:pointer;
        padding-left:10px;
    }
    .userbox .list:hover,.storebox .list:hover{
        background-color:#eeeeee;
    }
    .yname{
      font-size: 13px;
      color: #444;
    }
    .cur{
        color:#21c4f5;
    }
</style>
</head>
<body>

    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-fluid">
                    <div class="layui-row layui-col-space15">
                        <div class="layui-col-md12">
                            <div class="layui-card">
                                <div class="layui-card-header">分润账单列表</div>
                                <div class="layui-card-body">
                                    <div class="layui-btn-container" style="font-size:14px;">
                                        <!-- 时间 -->
                                        <div class="layui-form" style="display: inline-block;">
                                            <div class="layui-form-item">
                                                <div class="layui-inline">
                                                    <div class="layui-input-inline">
                                                    <text class="yname">交易时间</text>
                                                        <input type="text" id="start-item" class="layui-input test-item" placeholder="交易时间" lay-key="23"  autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-form" style="display: inline-block;">
                                            <div class="layui-form-item">
                                                <div class="layui-inline">
                                                    <div class="layui-input-inline">
                                                    <text class="yname">代理商名称</text>
                                                        <input type="text" id="userbox" autocomplete="off"  placeholder="请输入代理商名称" class="layui-input transfer">
                                                        <div class="userbox" style='display: none'></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-inline" style='margin-right:0'>
                                            <div class="layui-input-inline">
                                            <text class="yname">订单金额</text>
                                                <input type="text" class="layui-input amount_start" placeholder="订单金额">
                                            </div>
                                        </div>
                                        -
                                        <div class="layui-inline"  style='margin-left:10px;'>
                                            <div class="layui-input-inline">
                                            <text class="yname">订单金额</text>
                                                <input type="text" class="layui-input amount_end" placeholder="订单金额">
                                            </div>
                                        </div>
                                        <!-- 搜索 -->
                                        <div class="layui-form" lay-filter="component-form-group" style="margin-left:10px;display: inline-block;">
                                            <div class="layui-form-item">
                                                <button class="layui-btn export" style="border-radius:5px;margin-bottom:-23px;height:36px;line-height: 36px;">导出</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="layui-col-md12">
                                        <div class="layui-card" style="background-color: transparent;">
                                            <div>
                                                <div>
                                                    <span>（*不含官方、会员卡）</span>
                                                    <span>分润 = （费率 - 成本费率） X （支付成功 交易金额 - 商家优惠金额）</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>

                                    <!-- 门店所属 -->
                                    <script type="text/html" id="pid_name">
                                    @{{#  if(d.level == 1){ }}
                                        <span class="cur"> 收吖旗下 </span>
                                    @{{#  } else { }}
                                        @{{ d.pid_name }}
                                    @{{#  } }}
                                    </script>

                                    <!-- 费率 -->
                                    <script type="text/html" id="rate">
                                        @{{ d.rate }}%
                                    </script>

                                    <!-- 成本费率 -->
                                    <script type="text/html" id="cost_rate">
                                        @{{ d.cost_rate }}%
                                    </script>

                                    <!-- 分润 -->
                                    <script type="text/html" id="total_amount">
                                        @{{#  if((d.pay_status==1) && (d.rate > d.cost_rate)){ }}
                                            @{{ ((d.total_success_amount-d.total_return_amount-d.mdiscount_amount_sum)*((d.rate-d.cost_rate)/100)).toFixed(3) }}
                                        @{{#  } else { }}
                                            0.00
                                        @{{#  } }}
                                    </script>

                                    <!-- 判断订单状态 -->
                                    <script type="text/html" id="pay_status">
                                        @{{#  if(d.pay_status == 1){ }}
                                        <span class="cur">@{{ d.pay_status_desc }}</span>
                                        @{{#  } else { }}
                                        @{{ d.pay_status_desc }}
                                        @{{#  } }}
                                    </script>
                                    <!-- 判断状态 -->

                                    <!-- 通道类型 -->
                                    <script type="text/html" id="company_type">
                                        @{{#  if(d.company=='member'){ }}
                                            会员卡
                                        @{{#  } else if(d.company=='vbilla') { }}
                                            随行付A
                                        @{{#  } else if(d.company=='vbill') { }}
                                            随行付
                                        @{{#  } else if(d.company=='alipay') { }}
                                            支付宝
                                        @{{#  } else if(d.company=='weixin') { }}
                                            微信
                                        @{{#  } else if(d.company=='mybank') { }}
                                            快钱支付
                                        @{{#  } else if(d.company=='herongtong') { }}
                                            和融通
                                        @{{#  } else if(d.company=='newland') { }}
                                            新大陆
                                        @{{#  } else if(d.company=='fuiou') { }}
                                            富友
                                        @{{#  } else if(d.company=='jdjr') { }}
                                            京东聚合
                                        @{{#  } else if(d.company=='dlb') { }}
                                            哆啦宝
                                        @{{#  } else if(d.company=='zft') { }}
                                            花呗分期
                                        @{{#  } else if(d.company=='tfpay') { }}
                                            TF通道
                                        @{{#  } else { }}
                                            @{{ d.company }}
                                        @{{#  } }}
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--<input type="hidden" class="time_start">--}}

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    // var str = location.search;
    // var store_id = str.split('?')[1];

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'table', 'laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate;


        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token == null){
                window.location.href="{{url('/user/login')}}";
            }
        });

        // 获取时间
        var nowdate = new Date();
        // 本月
        var year=nowdate.getFullYear();
        var month=nowdate.getMonth()+1;
        var day=nowdate.getDate();
        if(month.toString().length<2 && day.toString().length<2){
            var nowdata = year+'-0'+month;
        }
        else if(month.toString().length<2){
            var nowdata = year+'-0'+month;
        }
        else if(day.toString().length<2){
            var nowdata = year+'-'+month;
        }
        else{
            var nowdata = year+'-'+month;
        }

        $('#start-item').val(nowdata);
        

        //订单开始时间
        laydate.render({
            elem: '#start-item'
            ,type: 'month'
            ,trigger: 'click'
            ,done: function(value){
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start:value
                        ,store_name:$('#store').val()
                    }
                    ,page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        

        // 代理商列表
        $(".transfer").bind("input propertychange",function(event){
            user_name = $(this).val()
            if (user_name.length == 0) {
                $('.userbox').html('')
                $('.userbox').hide()
            } else {
                $.post("{{url('/api/user/get_sub_users')}}",
                {
                    token:token
                    ,user_name:$(this).val()
                    ,self:'1'
                },function(res){
                    var html="";
                    if(res.t==0){
                        $('.userbox').html('');
                    }else{
                        for(var i=0; i<res.data.length; i++){
                            html+='<div class="list" data='+res.data[i].id+'>'+res.data[i].name+'-'+res.data[i].level_name+'</div>'
                        }
                        $(".userbox").show();
                        $('.userbox').html('');
                        $('.userbox').append(html)
                    }
                },"json");
            }
        });
        
        // $('#userbox').val();
        var fr_user_id = '';
        $(".userbox").on("click",".list",function(){
            $('.transfer').val($(this).html());
            $('.user_id').val($(this).attr('data'));
            $('.userbox').hide();
            fr_user_id = $(this).attr('data');

            table.reload('test-table-page', {
                where: {
                    user_id:$(this).attr('data')
                }
                ,page: {
                    curr: 1
                }
            });
        });

        // 渲染表格
        table.render({
            elem: '#test-table-page'
            ,url: "{{url('/api/wallet/commission_lists')}}"
            ,method: 'post'
            ,where:{
                token:token
                ,time_start:$('#start-item').val()
            }
            ,request:{
                pageName: 'p',
                limitName: 'l'
            }
            ,page: true
            ,cellMinWidth:105
            ,cols: [[
                {align:'center', field:'modify_pay_time', title: '交易时间'}
                ,{align:'center', field:'store_name',  title: '商户名称'}
                ,{align:'center', field:'company',  title: '交易渠道', toolbar:'#company_type'}
                ,{align:'center', field:'rate',  title: '费率', toolbar:'#rate'}
                ,{align:'center', field:'cost_rate',  title: '成本费率', toolbar:'#cost_rate'}
                ,{align:'center', field:'total_amount',  title: '交易金额'}
                ,{align:'center', field:'profit_amount',  title: '分润'}
                ,{align:'center', field:'ways_source_desc',  title: '支付类型'}
                ,{align:'center', field:'count_order',  title: '交易笔数'}
                ,{align:'center', field:'mdiscount_amount_sum',  title: '商家优惠金额', width:115}
                ,{align:'center', field:'pay_status_desc',  title: '订单状态', toolbar:'#pay_status'}
                //,{align:'center', field:'total_return_amount',  title: '退款金额'}
                // ,{align:'center', field:'name', title: '代理商名称'}
                // ,{align:'center', field:'pid_name', title: '代理商所属', toolbar: '#pid_name'}
                // ,{align:'center', field:'',  title: '分润金额', toolbar:'#total_amount'}
            ]]
            ,response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                ,statusCode: 1 //成功的状态码，默认：0
                ,msgName: 'message' //状态信息的字段名称，默认：msg
                ,countName: 't' //数据总数的字段名称，默认：count
                ,dataName: 'data' //数据列表的字段名称，默认：data
            }
            ,done: function(res, curr, count){
           // console.log(res);
                var login_level = res.order_data.login_level;
                $('.total_money').html(res.order_data.total_money);
                $('.total_number').html(res.order_data.total_number);
                $('.total_profit').html(res.order_data.total_profit);
                $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});	//进行表头样式设置
            }
        });

        // 开始金额
        $('.amount_start').bind("input propertychange",function(event){
            // console.log($(this).val())
            var amount_start=parseInt($(this).val());
            var amount_end=parseInt($('.amount_end').val());
            // console.log()
            if(amount_start<amount_end){
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        amount_start: $(this).val()
                        ,amount_end:$('.amount_end').val()
                    }
                    ,page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }else if($('.amount_end').val() == ''){

            }else{
                // layer.msg('开始金额不能大于结束金额')
            }
        });

        //结束金额
        $('.amount_end').bind("input propertychange",function(event){
            // console.log($(this).val())
            var amount_start=parseInt($('.amount_start').val());
            var amount_end=parseInt($(this).val());
            if(amount_start<amount_end){
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        amount_start: $('.amount_start').val()
                        ,amount_end:$(this).val()
                    }
                    ,page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }else if($('.amount_start').val() == ''){

            }else{
                // layer.msg('开始金额不能大于结束金额')
            }
        });

        // 导出分润账单
        $('.export').click(function(){
            var time_start = $('#start-item').val();
//            var fr_user_id = $('.list').attr('data');
            var amount_start = $('.amount_start').val();
            var amount_end = $('.amount_end').val();
            console.log(fr_user_id);

            window.location.href = "{{url('/api/export/CommissionExcelDown')}}"+"?token="+token+"&time_start="+time_start+"&user_id="+fr_user_id+"&amount_start="+amount_start+"&amount_end="+amount_end;

            {{--$.post("{{url('/api/export/CommissionExcelDown')}}",--}}
            {{--{--}}
                {{--token:token--}}
                {{--,time_start:time_start--}}
                {{--,user_id:fr_user_id--}}
                {{--,amount_start:amount_start--}}
                {{--,amount_end:amount_end--}}
            {{--},function(res){--}}
                {{--console.log(res);--}}
{{--//                if (res.status==1)  history.back();--}}
            {{--}, "json");--}}
        })

    });




  </script>

</body>
</html>





