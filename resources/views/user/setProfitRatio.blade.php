<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>设置比例</title>
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/formSelects-v4.css')}}" media="all">
</head>
<body>
<form class="layui-form" style="padding-top: 30px">
    <div class="layui-form-item" style="padding-right: 15px">
        <label class="layui-form-label">公司名称</label>
        <div class="layui-input-block">
            <input type="text" name="name" autocomplete="off" class="layui-input" id="name">

        </div>
    </div>
    <div class="layui-form-item" style="padding-right: 15px">
        <label class="layui-form-label">比例</label>
        <div class="layui-input-block">
            <input type="text" name="profitRatio" autocomplete="off" class="layui-input" id="profitRatio">
        </div>
    </div>
    <input type="hidden" name="user_id" autocomplete="off" class="layui-input" id="user_id">

    <div class="layui-form-item">
        <div class="layui-input-block" style="text-align: center;">
            <input type="button"class="layui-btn" lay-submit lay-filter="addProfitRatio" value="提交">
        </div>
    </div>

</form>
<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var str = location.search;
    var user_id = "";
    var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'table', 'laydate'], function () {
        var $ = layui.$
            , admin = layui.admin
            , form = layui.form
            , table = layui.table
            , laydate = layui.laydate;
        $(function () {
            //从父层获取值，json是父层的全局js变量。eval是将该string类型的json串变为标准的json串
            var parent_json = eval('(' + parent.json + ')');
            $('#name').val(parent_json.name)
            $('#profitRatio').val(parent_json.profit_ratio)
            $('#user_id').val(parent_json.id)

        });
        //表单提交
        form.on('submit(addProfitRatio)', function (data) {
            if (parseInt(data.field.profitRatio) > 100 || parseInt(data.field.profitRatio) < 1) {
                layer.msg("比例要大于1，不超过100", {icon: 2, shade: 0.5, time: 2000});
                return false;
            } else {

                $.post("{{url('/api/user/addPercentage')}}",
                    {
                        token: token,
                        user_id:data.field.user_id,
                        percentage: parseInt(data.field.profitRatio)

                    }, function (res) {

                        if (res.status == 1) {

                            parent.layui.table.reload('test-table-page'); //重载表格
                            parent.layer.msg(res.message, {
                                icon: 1
                                , time: 3000
                            });
                            parent.layer.close(index); //再执行关闭
                        } else {
                            layer.alert(res.message, {icon: 2});
                        }
                    }, "json");
            }

        });


    });
</script>
</body>
</html>