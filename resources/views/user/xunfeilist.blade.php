<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>讯飞语音合成</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
<link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
<style>
    .userbox,.storebox{
        height:200px;
        overflow-y: auto;
        z-index: 999;
        position: absolute;
        left: 0px;
        top: 63px;
        width:298px;
        background-color:#ffffff;
        border: 1px solid #ddd;
    }
    .userbox .list,.storebox .list{
        height:38px;line-height: 38px;cursor:pointer;
        padding-left:10px;
    }
    .userbox .list:hover,.storebox .list:hover{
        background-color:#eeeeee;
    }
    .yname{
      font-size: 13px;
      color: #444;
    }
    .cur{
        color:#21c4f5;
    }
</style>
</head>
<body>

    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-fluid">
                    <div class="layui-row layui-col-space15">
                        <div class="layui-col-md12">
                            <div class="layui-card">
                                <div class="layui-card-header">讯飞语音合成列表</div>
                                <div class="layui-card-body">

                                    <a class="layui-btn  layui-btn-xs storecode" lay-event="storecode" style="">充值入口</a>

                                    <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>

                                    <!-- 门店所属 -->
                                    <script type="text/html" id="pid_name">
                                    @{{#  if(d.level == 1){ }}
                                        <span class="cur"> 收吖旗下 </span>
                                    @{{#  } else { }}
                                        @{{ d.pid_name }}
                                    @{{#  } }}
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="edit_rate" class="hide" style="display: none;background-color: #fff;">
        <div class="xgrate">门店收款码</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form">
                <div class="layui-form-item">
                    <div style="display:flex;color: black;font-size: 16px;">
                        <div>门店名称：</div>
                        <div style="text-align: center;" class="storename"></div>
                    </div>
                    <div id="code"> </div>
                </div>
            </div>
        </div>
    </div>

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'table', 'laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token == null){
                window.location.href="{{url('/user/login')}}";
            }
        });

        // 渲染表格
        table.render({
            elem: '#test-table-page'
            ,url: "{{url('/api/user/putVoice')}}"
            ,method: 'post'
            ,where:{
                token:token
            }
            ,request:{
                pageName: 'p',
                limitName: 'l'
            }
            ,page: true
            ,cellMinWidth:105
            ,cols: [[
                {align:'center', field:'updated_at', title: '交易时间'}
                ,{align:'center', field:'created_at',  title: '创建时间'}
                ,{align:'center', field:'price',  title: '单价'}
                ,{align:'center', field:'total_num',  title: '充值次数'}
                ,{align:'center', field:'avali_num',  title: '剩余次数'}
            ]]
            ,response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                ,statusCode: 1 //成功的状态码，默认：0
                ,msgName: 'message' //状态信息的字段名称，默认：msg
                ,countName: 't' //数据总数的字段名称，默认：count
                ,dataName: 'data' //数据列表的字段名称，默认：data
            }
            ,done: function(res, curr, count){
                $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});	//进行表头样式设置
            }
        });


            $.post("{{url('/api/user/voicePayQr')}}",
            {
                token:token
            },
            function(res){
                if (res.status==1) {
                    $('#code').html('');
                    $('#code').qrcode(res.data.store_pay_qr);
                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#edit_rate')
                    });
                } else if(res.status == 2) {
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 3000
                    });
                }
            }, "json");


    });

  </script>

</body>
</html>
