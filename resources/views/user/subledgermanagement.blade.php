<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>分账管理</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
  <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
  <style>
    .edit{background-color: #ed9c3a;}
    .shenhe{background-color: #429488;}
    .see{background-color: #7cb717;}
    .tongbu{background-color: #4c9ef8;color:#fff;}
    .cur{color:#009688;}

    .userbox,.storebox{
      height:200px;
      overflow-y: auto;
      z-index: 999;
      position: absolute;
      left: 0px;
      top: 42px;
      width:298px;
      background-color:#ffffff;
      border: 1px solid #ddd;
    }
    .userbox .list,.storebox .list{
      height:38px;line-height: 38px;cursor:pointer;
      padding-left:10px;
    }
    .userbox .list:hover,.storebox .list:hover{
      background-color:#eeeeee;
    }
  </style>
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">

        <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
              <div class="layui-card">
                <div class="layui-card-header">分账管理列表</div>

                <div class="layui-card-body">
                  <div class="layui-btn-container" style="font-size:14px;">

                    <div class="layui-form" lay-filter="component-form-group" style="width:300px;display: inline-block;">
                      <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:0">
                          <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入业务员名称" class="layui-input transfer">

                          <div class="userbox" style='display: none'></div>
                        </div>
                      </div>
                    </div>

                    <div class="layui-form" lay-filter="component-form-group" style="width:300px;display: inline-block;">
                      <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:0">
                          <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入门店名称" class="layui-input inputstore">

                          <div class="storebox" style='display: none'></div>
                        </div>
                      </div>
                    </div>

                    <!-- 排序 -->
                    <div class="layui-form" lay-filter="component-form-group" style="width:300px;display: inline-block;">
                      <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:0">
                            <select name="grade" id="grade" lay-filter="grade">
                                <option value="">选择分账状态</option>
                                <option value="0">等待支付</option>
                                <option value="1">成功</option>
                                <option value="2">分成失败</option>
                            </select>
                        </div>
                      </div>
                    </div>




                    <!-- 缴费时间 -->
                    <div class="layui-form" style="display: inline-block;">
                      <div class="layui-form-item">
                        <div class="layui-inline">
                          <div class="layui-input-inline">
                            <input type="text" class="layui-input start-item test-item" placeholder="订单开始时间" lay-key="23">
                          </div>
                        </div>
                        <div class="layui-inline">
                          <div class="layui-input-inline">
                            <input type="text" class="layui-input end-item test-item" placeholder="订单结束时间" lay-key="24">
                          </div>
                        </div>

                      </div>
                    </div>
                    <!-- 搜索 -->
                    <div class="layui-form" lay-filter="component-form-group" style="width:600px;display: inline-block;">
                      <div class="layui-form-item">

                          <div class="layui-inline">
                            <div class="layui-input-inline">
                              <input type="text" name="tradeno" placeholder="请输入订单号" autocomplete="off" class="layui-input">
                            </div>
                          </div>


                          <div class="layui-inline">
                            <button class="layui-btn layuiadmin-btn-list" lay-submit="" lay-filter="LAY-app-contlist-search" style="margin-bottom: 0;height:36px;line-height: 36px;">
                              <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                            </button>
                          </div>
                          <button class="layui-btn export" style="margin-bottom: 4px;height:36px;line-height: 36px;">导出</button>
                        </div>
                    </div>

                  </div>

                  <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                  <!-- 判断状态 -->
                  <script type="text/html" id="statusTap">
                    @{{#  if(d.status == 1){ }}
                      <span class="cur">@{{ d.status_desc }}</span>
                    @{{#  } else { }}
                      @{{ d.status_desc }}
                    @{{#  } }}
                  </script>
                  <!-- 判断状态 -->

                  <script type="text/html" id="paymoney">
                    @{{ d.rate }}%
                  </script>
                  <!-- 积分抵扣 -->
                  <script type="text/html" id="jfdkTap">
                    @{{ d.dk_jf }}/@{{ d.dk_money }}
                  </script>
                  <script type="text/html" id="table-content-list" class="layui-btn-small">
                    <a class="layui-btn layui-btn-normal layui-btn-xs tongbu" lay-event="tongbu">同步状态</a>
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  <input type="hidden" class="user_id">
  <input type="hidden" class="store_id">
  <input type="hidden" class="submanagement">

  <input type="hidden" class="pay_status">
  <input type="hidden" class="pay_type">
  <input type="hidden" class="company_id">

  <input type="hidden" class="danhao">
  <input type="hidden" class="tiaoma">

  <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
    <script>
    var token = sessionStorage.getItem("Usertoken");
    var str=location.search;
    var store_id = sessionStorage.getItem("store_store_id");


    layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate;

      $('.store_id').val(store_id)
      // 未登录,跳转登录页面
      $(document).ready(function(){
          if(token==null){
              window.location.href="{{url('/user/login')}}";
          }
      })
      var s_storename=sessionStorage.getItem('s_storename');

      if(store_id == undefined){

      }else{
        $('.inputstore').val(s_storename)
      }
        // 选择门店
        $.ajax({
            url : "{{url('/api/user/store_lists')}}",
            data : {token:token,l:100},
            type : 'post',
            success : function(data) {
                console.log(data);
                var optionStr = "";
                for(var i=0;i<data.data.length;i++){
                    optionStr += "<option value='" + data.data[i].store_id + "' "+((store_id==data.data[i].store_id)?"selected":"")+">" + data.data[i].store_name + "</option>";
                }
                $("#schooltype").append('<option value="">选择门店</option>'+optionStr);
                layui.form.render('select');
            },
            error : function(data) {
                alert('查找板块报错');
            }
        });
        // 选择通道
        $.ajax({
          url : "{{url('/api/user/store_open_pay_way_lists')}}",
          data : {token:token,l:100},
          type : 'post',
          dataType:'json',
          success : function(data) {
              console.log(data);
              var optionStr = "";
              for(var i=0;i<data.data.length;i++){
                  optionStr += "<option value='" + data.data[i].company + "'>"
                      + data.data[i].company_desc + "</option>";
              }
              $("#passway").append('<option value="">选择通道类型</option>'+optionStr);
              layui.form.render('select');
          },
          error : function(data) {
              alert('查找板块报错');
          }
        });

        $(".transfer").bind("input propertychange",function(event){
         console.log($(this).val())
          $.post("{{url('/api/user/get_sub_users')}}",
          {
              token:token,
              user_name:$(this).val(),
              self:'1'

          },function(res){
              console.log(res);
              var html="";
              console.log(res.t)
              if(res.t==0){
                  $('.userbox').html('')
              }else{
                  for(var i=0;i<res.data.length;i++){
                    html+='<div class="list" data='+res.data[i].id+'>'+res.data[i].name+'-'+res.data[i].level_name+'</div>'
                  }
                  $(".userbox").show()
                  $('.userbox').html('')
                  $('.userbox').append(html)
              }

          },"json");
        });

        $(".userbox").on("click",".list",function(){

          $('.transfer').val($(this).html())
          $('.user_id').val($(this).attr('data'))
          $('.userbox').hide()

          // table.reload('test-table-page', {
          //   where: {
          //     user_id:$(this).attr('data'),
          //     store_id:$('store_id').val()
          //   }
          //   ,page: {
          //     curr: 1
          //   }
          // });


        })

        $(".inputstore").bind("input propertychange",function(event){
         console.log($(this).val())
          $.post("{{url('/api/user/store_lists')}}",
          {
              token:token,
              token:token,store_name:$(this).val(),l:100

          },function(res){
              console.log(res);
              var html="";
              console.log(res.t)
              if(res.t==0){
                  $('.storebox').html('')
              }else{
                  for(var i=0;i<res.data.length;i++){
                      html+='<div class="list" data='+res.data[i].store_id+'>'+res.data[i].store_name+'</div>'
                  }
                  $(".storebox").show()
                  $('.storebox').html('')
                  $('.storebox').append(html)
              }

          },"json");
        });

        $(".storebox").on("click",".list",function(){

          $('.inputstore').val($(this).html())
          $('.store_id').val($(this).attr('data'))
          $('.storebox').hide()

          // table.reload('test-table-page', {
          //   where: {
          //     user_id:$('user_id').val(),
          //     store_id:$(this).attr('data')
          //   }
          //   ,page: {
          //     curr: 1
          //   }
          // });
          $("#passway").html('')

          // 选择通道
          $.ajax({
              url : "{{url('/api/user/store_open_pay_way_lists')}}",
              data : {token:token,l:100,store_id:$('.store_id').val()},
              type : 'post',
              dataType:'json',
              success : function(data) {
                  console.log(data);
                  var optionStr = "";
                      for(var i=0;i<data.data.length;i++){
                          optionStr += "<option value='" + data.data[i].company + "'>"
                              + data.data[i].company_desc + "</option>";
                      }
                      $("#passway").append('<option value="">选择通道类型</option>'+optionStr);
                      layui.form.render('select');
              },
              error : function(data) {
                  alert('查找板块报错');
              }
          });


        })




        // 渲染表格
        table.render({
            elem: '#test-table-page'
            ,url: "{{url('/api/user/settle_order')}}"
            ,method: 'post'
            ,where:{
              token:token,
              store_id:store_id
            }
            ,request:{
              pageName: 'p',
              limitName: 'l'
            }
            ,page: true
            ,cellMinWidth: 150
            ,cols: [[
              {field:'store_id', title: '门店ID'}
              ,{field:'out_trade_no', title: '外部订单号'}
              ,{field:'trade_no', title: '交易订单号'}
              ,{field:'total_amount',  title: '订单金额'}
              ,{field:'order_settle_amount',  title: '分成金额'}
              ,{field:'status_desc', title: '状态',templet:'#statusTap'}
              ,{field:'created_at',  title: '时间'}
              // ,{width:100,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
            ]]
            ,response: {
              statusName: 'status' //数据状态的字段名称，默认：code
              ,statusCode: 1 //成功的状态码，默认：0
              ,msgName: 'message' //状态信息的字段名称，默认：msg
              ,countName: 't' //数据总数的字段名称，默认：count
              ,dataName: 'data' //数据列表的字段名称，默认：data
            }
            ,done: function(res, curr, count){
              console.log(res);

            }

        });



        table.on('tool(test-table-page)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
          var e = obj.data; //获得当前行数据
          var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
          var tr = obj.tr; //获得当前行 tr 的DOM对象
          console.log(e);
          // sessionStorage.setItem('s_store_id', e.store_id);

          if(layEvent === 'tongbu'){ //审核

            $.post("{{url('/api/basequery/update_order')}}",
            {
                token:token,
                store_id:e.store_id,
                out_trade_no:e.out_trade_no
            },function(res){
                console.log(res);
                if(res.status==1){
                    layer.msg(res.message, {
                        offset: '15px'
                        ,icon: 1
                        ,time: 2000
                    });
                }else{
                    layer.msg(res.message, {
                        offset: '15px'
                        ,icon: 2
                        ,time: 2000
                    });
                }
            },"json");
          }

          var data = obj.data;
          if(obj.event === 'setSign'){
            layer.open({
              type: 2,
              title: '模板详细',
              shade: false,
              maxmin: true,
              area: ['60%', '70%'],
              content: "{{url('/merchantpc/paydetail?')}}"+e.stu_order_type_no
            });
          }
        });




        // 选择排序
        form.on('select(grade)', function(data){
          var submanagement = data.value;
          $('.submanagement').val(submanagement);
          //执行重载
          // table.reload('test-table-page', {
          //   where: {
          //     sort:sort
          //   }
          //   ,page: {
          //     curr: 1//重新从第 1 页开始
          //   }
          // });
        });




        laydate.render({
          elem: '.start-item'
          ,type: 'datetime'
          ,done: function(value){
            //执行重载
            // table.reload('test-table-page', {
            //   where: {
            //     time_start:value,
            //     time_end:$('.end-item').val()
            //   }
            //   ,page: {
            //     curr: 1 //重新从第 1 页开始
            //   }
            // });
          }
        });

        laydate.render({
          elem: '.end-item'
          ,type: 'datetime'
          ,done: function(value){
            //执行重载
            // table.reload('test-table-page', {
            //   where: {
            //     time_start:$('.start-item').val(),
            //     time_end:value
            //   }
            //   ,page: {
            //     curr: 1 //重新从第 1 页开始
            //   }
            // });
          }
        });





        form.on('submit(LAY-app-contlist-search)', function(data){
          var paytradeno = data.field.paytradeno;  //条码单号
          var out_trade_no = data.field.tradeno; //订单号
          console.log(data);
          $('.danhao').val(out_trade_no);
          $('.tiaoma').val(paytradeno);
          //执行重载
          table.reload('test-table-page', {
            where: {
              out_trade_no:out_trade_no,
              user_id:$('.user_id').val(),
              store_id:$('.store_id').val(),
              status:$('.submanagement').val(),
              time_start:$('.start-item').val(),
              time_end:$('.end-item').val(),

            }
            ,page: {
              curr: 1 //重新从第 1 页开始
            }
          });
        });



        $('.export').click(function(){
          var store_id=$('.store_id').val();
          var user_id=$('.user_id').val();
          var sort=$('.sort').val();
          var pay_status=$('.pay_status').val();
          var ways_source=$('.pay_type').val();
          var company=$('.company_id').val();

          var time_start=$('.start-item').val();
          var time_end=$('.end-item').val();

          var out_trade_no=$('.danhao').val();
          var trade_no=$('.tiaoma').val();

          window.location.href="{{url('/api/export/UserOrderExcelDown')}}"+"?token="+token+"&store_id="+store_id+"&user_id="+user_id+"&sort="+sort+"&pay_status="+pay_status+"&ways_source="+ways_source+"&company="+company+"&time_start="+time_start+"&time_end="+time_end+"&out_trade_no="+out_trade_no+"&trade_no="+trade_no;
        })

    });

  </script>

</body>
</html>





