<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>通道使用记录</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .tongbu {
            background-color: #4c9ef8;
            color: #fff;
            border-radius: 3.5px
        }

        .cur {
            color: #21c4f5;
        }

        .userbox, .storebox {
            height: 190px;
            margin-right: 0;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 63px;
            width: 298px;
            background-color: #ffffff;
            border: 1px solid #ddd;
        }

        .userbox .list, .storebox .list {
            height: 38px;
            line-height: 38px;
            cursor: pointer;
            padding-left: 10px;
        }

        .userbox .list:hover, .storebox .list:hover {
            background-color: #eeeeee;
        }

        .yname {
            font-size: 13px;
            color: #444;
            margin-bottom: 8px;
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12" style="margin-top:0px">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12">
                        <div class="layui-card">
                            <div class="layui-card-header">通道使用记录</div>
                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">
                                    <div style="font-size:14px">
                                        <div class="layui-form" lay-filter="component-form-group"
                                             style="width:190px;margin-right:0;display: inline-block;">
                                            <div class="layui-form-item">
                                                <div class="layui-input-block"
                                                     style="margin-left:5px;border-radius:5px">
                                                    <text class="yname">代理商名称</text>
                                                    <input type="text" style="border-radius:5px"
                                                           tyle="border-radius:5px" name="schoolname"
                                                           lay-verify="schoolname" autocomplete="off"
                                                           placeholder="请输入代理商名称" class="layui-input transfer">
                                                    <div class="userbox" style='display: none'></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-form" lay-filter="component-form-group"
                                             style="width:190px;margin-right:0;display: inline-block;">
                                            <div class="layui-form-item">
                                                <div class="layui-input-block"
                                                     style="margin-left:5px;border-radius:5px">
                                                    <text class="yname">门店名称</text>
                                                    <input type="text" style="border-radius:5px" name="schoolname"
                                                           lay-verify="schoolname" autocomplete="off"
                                                           placeholder="请输入门店名称" class="layui-input inputstore">
                                                    <div class="storebox" style='display: none'></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 支付状态 -->
                                        <div class="layui-form" lay-filter="component-form-group"
                                             style="width:190px;margin-right:0;display: inline-block;">
                                            <div class="layui-form-item">
                                                <div class="layui-input-block"
                                                     style="margin-left:5px;border-radius:5px">
                                                    <text class="yname">使用类型</text>
                                                    <select name="type" id="type" lay-filter="type">
                                                        <option value="">全部</option>
                                                        <option value="1">商户充值</option>
                                                        <option value="2">交易扣款</option>
                                                        <option value="3">代理分润</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-form" lay-filter="component-form-group"
                                             style="width:190px;margin-right:0;display: inline-block;">
                                            <div class="layui-form-item">
                                                <div class="layui-input-block"
                                                     style="margin-left:5px;border-radius:5px">
                                                    <text class="yname">是否结算</text>
                                                    <select name="settlement_type" id="settlement_type"
                                                            lay-filter="settlement_type">
                                                        <option value="">全部</option>
                                                        <option value="01">未结算</option>
                                                        <option value="02">已结算</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:100%;display: flex;">
                                        <div class="layui-form-item" style="margin-left:10px;">
                                            <div class="layui-inline">
                                                <div class="layui-input-inline" style="width:175px;margin-right:0;">
                                                    <text class="yname">订单开始时间</text>
                                                    <input type="text" style="border-radius:5px"
                                                           class="layui-input start-item test-item"
                                                           placeholder="订单开始时间" lay-key="23">
                                                </div>
                                            </div>
                                            <div class="layui-inline">
                                                <div class="layui-input-inline" style="width:175px;margin-right:0;">
                                                    <text class="yname">订单结束时间</text>
                                                    <input type="text" style="border-radius:5px"
                                                           class="layui-input end-item test-item"
                                                           placeholder="订单结束时间" lay-key="24">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="layui-form-item" style="margin-left:5px;">

                                            <div class="layui-inline" style='margin-right:0'>
                                                <div class="layui-input-inline" style="width:185px">
                                                    <text class="yname">订单号</text>
                                                    <input type="text" style="border-radius:5px" name="tradeno"
                                                           placeholder="请输入订单号" autocomplete="off" class="layui-input">
                                                </div>
                                            </div>

                                            <div class="layui-inline">
                                                <button class="layui-btn layuiadmin-btn-list" lay-submit=""
                                                        lay-filter="LAY-app-contlist-search"
                                                        style="border-radius:5px;margin-top:20px;margin-bottom: 0;height:36px;line-height: 36px;">
                                                    <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                                </button>
                                            </div>
                                            <button class="layui-btn export"
                                                    style="border-radius:5px;margin-top:20px;margin-bottom: 4px;height:36px;line-height: 36px;">
                                                导出
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>

                                <script type="text/html" id="availAmountTap">
                                    @{{#  if(d.avail_amount > 0){ }}
                                    <span style="color:#00963a">@{{ d.avail_amount }}</span>
                                    @{{#  } else if(d.avail_amount < 0) { }}
                                    <span style="color:#e85052">@{{ d.avail_amount }}</span>
                                    @{{#  } }}
                                </script>

                                <!-- 判断状态 -->
                                <script type="text/html" id="settlementTypeTap">
                                    @{{#  if(d.settlement_type=='01'){ }}
                                    <span style="color:#e85052">未结算</span>

                                    @{{#  } else if(d.settlement_type=='02') { }}
                                    <span style="color:#00963a">已结算</span>

                                    @{{#  } }}
                                </script>
                                <script type="text/html" id="typeTap">
                                    @{{#  if(d.type=='1'){ }}
                                    <span style="color:#0066FF">商户充值</span>
                                    @{{#  } else if(d.type=='2') { }}
                                    <span style="color:#e85052">交易扣款</span>
                                    @{{#  } else if(d.type=='3') { }}
                                    <span style="color:#00963a">代理分润</span>
                                    @{{#  } }}
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="store_id">
<input type="hidden" class="user_id">
<input type="hidden" class="type">
<input type="hidden" class="settlement_type">
<input type="hidden" class="danhao">
<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var str = location.search;
    var store_id = str.split('?')[1];


    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'table', 'laydate'], function () {
        var $ = layui.$
            , admin = layui.admin
            , form = layui.form
            , table = layui.table
            , laydate = layui.laydate;

        $('.store_id').val(store_id);

        // 未登录,跳转登录页面
        $(document).ready(function () {
            if (token == null) {
                window.location.href = "{{url('/user/login')}}";
            }
        });

        // 获取时间
        var nowdate = new Date();
        // 本月
        var year=nowdate.getFullYear();
        var mounth=nowdate.getMonth()+1;
        var day=nowdate.getDate();
        var hour = nowdate.getHours();
        var min = nowdate.getMinutes();
        var sec = nowdate.getSeconds();
        if(mounth.toString().length<2 && day.toString().length<2){
            var nwedata = year+'-0'+mounth+'-0'+day+' '+hour+':'+min+':'+sec;
        }
        else if(mounth.toString().length<2){
            var nwedata = year+'-0'+mounth+'-'+day+' '+hour+':'+min+':'+sec;
        }
        else if(day.toString().length<2){
            var nwedata = year+'-'+mounth+'-0'+day+' '+hour+':'+min+':'+sec;
        }
        else{
            var nwedata = year+'-'+mounth+'-'+day+' '+hour+':'+min+':'+sec;
        }
        $('.end-item').val(nwedata);//今天的时间
        // =========================================
        var y = nowdate.getFullYear();
        var mon = nowdate.getMonth()+1;
        var d = nowdate.getDate();
        var h = '00';
        var m = '00';
        var s = '00';
        if(mon.toString().length<2 && d.toString().length<2){
            var formatwdate = y+'-0'+mon+'-0'+d+' '+h+':'+m+':'+s;
        }
        else if(mon.toString().length<2){
            var formatwdate = y+'-0'+mon+'-'+d+' '+h+':'+m+':'+s;
        }
        else if(d.toString().length<2){
            var formatwdate = y+'-'+mon+'-0'+d+' '+h+':'+m+':'+s;
        }
        else{
            var formatwdate = y+'-'+mon+'-'+d+' '+h+':'+m+':'+s;
        }
        $('.start-item').val(formatwdate);

        var s_storename = sessionStorage.getItem('s_storename');

        if (store_id == undefined) {

        } else {
            $('.inputstore').val(s_storename);
        }

        // 选择门店
        $.ajax({
            url: "{{url('/api/user/store_lists')}}",
            data: {token: token, l: 100},
            type: 'post',
            success: function (data) {
                //console.log(data);
                var optionStr = "";
                for (var i = 0; i < data.data.length; i++) {
                    optionStr += "<option value='" + data.data[i].store_id + "' " + ((store_id == data.data[i].store_id) ? "selected" : "") + ">" + data.data[i].store_name + "</option>";
                }
                $("#schooltype").append('<option value="">选择门店</option>' + optionStr);
                layui.form.render('select');
            },
            error: function (data) {
                alert('查找板块报错');
            }
        });


        $(".transfer").bind("input propertychange", function (event) {
            //         console.log($(this).val());
            user_name = $(this).val();
            if (user_name.length == 0) {
                $('.userbox').html('');
                $('.userbox').hide();
            } else {
                $.post("{{url('/api/user/get_sub_users')}}",
                    {
                        token: token
                        , user_name: $(this).val()
                        , self: '1'
                    }, function (res) {
                        var html = "";

                        if (res.t == 0) {
                            $('.userbox').html('');
                        } else {
                            for (var i = 0; i < res.data.length; i++) {
                                html += '<div class="list" data=' + res.data[i].id + '>' + res.data[i].name + '-' + res.data[i].level_name + '</div>'
                            }
                            $(".userbox").show();
                            $('.userbox').html('');
                            $('.userbox').append(html);
                        }
                    }, "json");
            }
        });

        $(".userbox").on("click", ".list", function () {
            $('.transfer').val($(this).html());
            $('.user_id').val($(this).attr('data'));
            $('.userbox').hide();

            table.reload('test-table-page', {
                where: {
                    user_id: $(this).attr('data'),
                    store_id: $('store_id').val()
                }
                , page: {
                    curr: 1
                }
            });
        });

        $(".inputstore").bind("input propertychange", function (event) {
            store_name = $(this).val();
            if (store_name.length == 0) {
                $('.storebox').html('');
                $('.storebox').hide();
            } else {
                $.post("{{url('/api/user/store_lists')}}",
                    {
                        token: token
                        , store_name: $(this).val()
                        , l: 100
                    }, function (res) {
                        var html = "";
                        if (res.t == 0) {
                            $('.storebox').html('');
                        } else {
                            for (var i = 0; i < res.data.length; i++) {
                                html += '<div class="list" data=' + res.data[i].store_id + '>' + res.data[i].store_name + '</div>'
                            }
                            $(".storebox").show();
                            $('.storebox').html('');
                            $('.storebox').append(html);
                        }
                    }, "json");
            }
        });

        $(".storebox").on("click", ".list", function () {
            $('.inputstore').val($(this).html());
            $('.store_id').val($(this).attr('data'));
            $('.storebox').hide();

            table.reload('test-table-page', {
                where: {
                    user_id: $('user_id').val(),
                    store_id: $(this).attr('data')
                }
                , page: {
                    curr: 1
                }
            });

            $("#passway").html('');
        });

        // 渲染表格
        table.render({
            elem: '#test-table-page'
            , url: "{{url('/api/user/getPassagewayList')}}"
            , method: 'post'
            , where: {
                token: token,
                store_id: store_id,
                time_start: $('.start-item').val(),
                time_end: $('.end-item').val()
            }
            , request: {
                pageName: 'p'
                , limitName: 'l'
            }
            , page: true
            , cellMinWidth: 100
            , cols: [[
                {field: 'created_at', align: 'center', title: '交易时间', width: 160}
                , {field: 'store_name', align: 'center', title: '门店', width: 200}
                , {field: 'name', align: 'center', title: '所属代理', width: 200}
                , {field: 'avail_amount', align: 'center', width: 120, title: '商户余额', templet: '#availAmountTap', sort: true}
                , {field: 'amount', align: 'center', title: '通道使用费'}
                // , {field: 'profit', align: 'center', title: '分润比例%'}
                , {field: 'rate', align: 'center', title: '分润比例%'}
                , {field: 'profit_amount', align: 'center', title: '代理分润'}
                , {field: 'total_profit_amount', align: 'center', width: 150,title: '分润总金额', sort: true}
                , {field: 'pay_amount', align: 'center', title: '交易金额'}
                , {field: 'order_id', align: 'center', title: '订单号', width: 220}
                , {field: 'type', align: 'center', title: '使用类型', templet: '#typeTap'}
                , {field: 'settlement_type', align: 'center', title: '是否结算', templet: '#settlementTypeTap'}
            ]]
            , response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                , statusCode: 1 //成功的状态码，默认：0
                , msgName: 'message' //状态信息的字段名称，默认：msg
                , countName: 't' //数据总数的字段名称，默认：count
                , dataName: 'data' //数据列表的字段名称，默认：data
            }
            , done: function (res, curr, count) {
                $('.total').html(res.order_data.total)
                $('th').css({
                    'font-weight': 'bold',
                    'font-size': '15',
                    'color': 'black',
                    'background': 'linear-gradient(#f2f2f2,#cfcfcf)'
                });	//进行表头样式设置
            }
        });

        table.on('sort(test-table-page)', function(obj) { //注：sort 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
            // console.log(obj.field); //当前排序的字段名
            // console.log(obj.type); //当前排序类型：desc（降序）、asc（升序）、null（空对象，默认排序）
            // console.log(this); //当前排序的 th 对象

            let sort_field = obj.field; //字段名
            let sort_order = obj.type; //排序顺序，可能的值：asc desc null

            let sortWhere = {sort_field, sort_order};

            // console.log(sortWhere); //Object { sort_field: "id", sort_order: "asc" }

            table.reload('test-table-page',{
                initSort: obj //记录初始排序，如果不设的话，将无法标记表头的排序状态。
                ,where: sortWhere
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });


        // 选择门店
        form.on('select(schooltype)', function (data) {
            var store_id = data.value;
            $('.store_id').val(store_id);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    store_id: store_id
                    , user_id: $('.user_id').val()
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });


        // 选择业务员
        form.on('select(agent)', function (data) {
            var user_id = data.value;
            $('.user_id').val(user_id);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    user_id: $(".user_id").val()
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });

            // 选择门店
            $.ajax({
                url: "{{url('/api/user/store_lists')}}",
                data: {
                    token: token
                    , user_id: user_id
                    , l: 100
                },
                type: 'post',
                success: function (data) {
                    var optionStr = "";
                    for (var i = 0; i < data.data.length; i++) {
                        optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
                    }
                    $("#schooltype").html('');
                    $("#schooltype").append('<option value="">选择门店</option>' + optionStr);
                    layui.form.render('select');
                },
                error: function (data) {
                    alert('查找板块报错');
                }
            });
        });

        // 选择状态
        form.on('select(type)', function (data) {
            var type = data.value;
            $('.type').val(type);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    type: type
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });
        // 选择支付类型
        form.on('select(settlement_type)', function (data) {
            var settlement_type = data.value;
            $('.settlement_type').val(settlement_type);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    settlement_type: settlement_type
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });
        laydate.render({
            elem: '.start-item'
            , type: 'datetime'
            , trigger: 'click'
            , done: function (value) {
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: value
                        , time_end: $('.end-item').val()
                    }
                    , page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        laydate.render({
            elem: '.end-item'
            , type: 'datetime'
            , trigger: 'click'
            , done: function (value) {
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: $('.start-item').val()
                        , time_end: value
                    }
                    , page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        form.on('submit(LAY-app-contlist-search)', function (data) {

            var out_trade_no = data.field.tradeno; //订单号
            $('.danhao').val(out_trade_no);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    out_trade_no: out_trade_no
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });

        $('.export').click(function () {
            var store_id = $('.store_id').val();
            var user_id = $('.user_id').val();
            var type = $('.type').val();
            var time_start = $('.start-item').val();
            var time_end = $('.end-item').val();
            var out_trade_no = $('.danhao').val();
            var settlement_type = $('.settlement_type').val();

            window.location.href = "{{url('/api/export/passagewayRecordExportDown')}}" + "?token=" + token + "&store_id=" + store_id + "&user_id=" + user_id + "&settlement_type=" + settlement_type + "&type=" + type   + "&time_start=" + time_start + "&time_end=" + time_end + "&out_trade_no=" + out_trade_no;
        })

    });

</script>

</body>
</html>
