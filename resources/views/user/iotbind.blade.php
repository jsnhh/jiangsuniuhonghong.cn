<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>代运营授权</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">

</head>
<body>

{{--<div class="layui-fluid">--}}
{{--    <div class="layui-card" style="margin-top:0px;">--}}
{{--        <div class="layui-card-header">代运营授权</div>--}}
{{--        <div class="layui-card-body" style="padding: 15px;">--}}
{{--            <div class="layui-form" lay-filter="component-form-group">--}}
{{--                <div class="layui-form-item">--}}
{{--                    <label class="layui-form-label" style="text-align:center;">支付宝账号</label>--}}
{{--                    <div class="layui-input-block" style="width:500px;">--}}
{{--                        <input type="text" style="margin-left:5px" name="alipay_account" lay-verify="alipay_account" autocomplete="off" placeholder="" class="layui-input">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                    <div class="layui-form-item">--}}
{{--                        <label class="layui-form-label w-150">蚂蚁shop_id/名称</label>--}}
{{--                        <div class="layui-input-block ">--}}
{{--                            <!-- disabled="disabled" -->--}}
{{--                            <input type="text" placeholder="请输入蚂蚁shop_id/名称" class="layui-input shopid_name w-80">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                <div class="layui-form-item layui-layout-admin">--}}
{{--                    <div class="layui-input-block">--}}
{{--                        <div class="layui-footer" style="left: 0;">--}}
{{--                            <button class="layui-btn submit site-demo-active"style="border-radius:5px" data-type="tabChange">保存</button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header">设备绑定</div>
        <div class="layui-card-body layui-row layui-col-space10">
            <!-- <form class="layui-form" action=""> -->
            <div class="layui-row layui-form">
                <div class="layui-col-md9">
                    <div class="layui-form-item">
                        <label class="layui-form-label w-150">蚂蚁shop_id</label>
                        <div class="layui-input-block ">
                            <!-- disabled="disabled" -->
                            <input type="text" placeholder="请输入蚂蚁shop_id" class="layui-input shop_id w-80">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label w-150">蚂蚁shop名称</label>
                        <div class="layui-input-block ">
                            <!-- disabled="disabled" -->
                            <input type="text" placeholder="请输入蚂蚁shop名称" class="layui-input shop_name w-80">
                        </div>
                    </div>


                    <div class="layui-form-item relative" style="margin-bottom:1px">
                        <label class="layui-form-label w-150">设备id</label>
                        <div class="layui-input-block  ">
                            <input type="text" placeholder="请输入设备id" class="layui-input face_sn w-80">

                        </div>


                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label w-150">商户类型</label>
                        <div class="layui-input-inline dtradio " style="padding-left: 10px">

                            <input type="radio" name="dt" lay-filter="dt" value="1" title="直连商户" checked="">
                            <div class="layui-unselect layui-form-radio layui-form-radioed">
                                <i class="layui-anim layui-icon"></i>
                                <div>直连商户</div>
                            </div>
{{--                            <input type="radio" name="dt" lay-filter="dt" value="2" title="间连商户">--}}
{{--                            <div class="layui-unselect layui-form-radio">--}}
{{--                                <i class="layui-anim layui-icon"></i>--}}
{{--                                <div>间连商户</div>--}}
{{--                            </div>--}}
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label w-150">支付宝2088/pid/smid</label>
                        <div class="layui-input-block">
                            <!-- disabled="disabled" -->
                            <input type="text" placeholder="请输入支付宝2088/pid/smid" class="layui-input smid_mer w-80">
                        </div>
                    </div>
                    <!-- <div class="layui-form-item">
                        <label class="layui-form-label">smid</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入smid" disabled="disabled" class="layui-input ">
                        </div>
                    </div>
 -->


                </div>

                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit lay-submit" lay-filter="formDemo">确定绑定</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- </form>        -->
        </div>
    </div>
</div>

<input type="hidden" class="dtval" value="1">
<input type="hidden" class="wdval" value="1">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var str=location.search;
    var id=str.split('?')[1];
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','element'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });

        //加载页面参数
        $.post("{{url('/api/alipayopen/iotAntStorelist')}}",
            {
                token:token,
                id:id
            },function(res){
           console.log(res);
                $('.shop_id').val(res.data.shop_id);
                $('.shop_name').val(res.data.shop_name);
                $('.smid_mer').val(res.data.smid_mer);
            },"json");

        {{--$.post("{{url('/api/basequery/j_push_info')}}",--}}
        {{--    {--}}
        {{--        token:token--}}
        {{--    },function(res){--}}
        {{--        console.log(res);--}}
        {{--        $('.layui-form .layui-form-item').eq(0).find('input').val(res.data.DevKey);--}}
        {{--        $('.layui-form .layui-form-item').eq(1).find('input').val(res.data.API_DevSecret);--}}
        {{--    },"json");--}}





        form.on('radio(dt)', function(data){
            console.log(data.value);
            $('.dtval').val(data.value)
            if(data.value==1){
                $('.alipayacount').show()
                $('.smid').hide()
            }else{
                $('.alipayacount').hide()
                $('.smid').show()
            }
        });
        form.on('radio(type)', function(data){
            console.log(data.value);
            $('.wdval').val(data.value)
        });




        $('.submit').on('click', function(){
            if( !$(".shop_id").val().trim()){
                layer.msg('请输入蚂蚁shop_id/名称！', {
                    offset: '15px'
                    ,icon: 2
                    ,time: 2000
                });
                return
            }
            if( !$(".face_sn").val().trim()){
                layer.msg('请输入设备id！', {
                    offset: '15px'
                    ,icon: 2
                    ,time: 2000
                });
                return
            }
            if( !$(".smid_mer").val().trim()){
                layer.msg('请输入支付宝2088/pid/smid！', {
                    offset: '15px'
                    ,icon: 2
                    ,time: 2000
                });
                return
            }
            // console.log($('.private_key').val());
            // console.log(shop_id,$(".face_sn").val(),$(".smid_mer").val())
            console.log($('.dtval').val())
            $.post("{{url('/api/alipayopen/iotBindDevice')}}",
                {
                    token:token,
                    smid:$(".smid_mer").val(),
                    store_type:$('.dtval').val(),
                    shop_id:$('.shop_id').val(),
                    biz_tid:$('.face_sn').val(),
                },function(res){
                    console.log(res);
                    if(res.status==1){
                        layer.msg(res.message, {
                            offset: '15px'
                            ,icon: 1
                            ,time: 2000
                        });
                    }else{
                        layer.msg(res.message, {
                            offset: '15px'
                            ,icon: 2
                            ,time: 2000
                        });
                    }
                },"json");
        });

    });

</script>

</body>
</html>
