<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>京东金融配置</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card" style="margin-top:0px">
        <div class="layui-card-header">京东金融配置</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center;width:110px">服务商商户号</label>
                    <div class="layui-input-block" style="width:500px;margin-left:140px">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center;width:110px">md5加签密钥</label>
                    <div class="layui-input-block" style="width:500px;margin-left:140px">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center;width:110px">3des加密密钥</label>
                    <div class="layui-input-block" style="width:500px;margin-left:140px">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center;width:110px">系&nbsp;统&nbsp;名&nbsp;称ID</label>
                    <div class="layui-input-block" style="width:500px;margin-left:140px">
                        <input type="text" name="schoolshortname" lay-verify="schoolshortname" autocomplete="off" placeholder="" class="layui-input">                        
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center;width:110px">支付宝app ID</label>
                    <div class="layui-input-block" style="width:500px;margin-left:140px">
                        <input type="text" name="schoolshortname" lay-verify="schoolshortname" autocomplete="off" placeholder="" class="layui-input"> 
                    </div>
                </div>                
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center;width:110px">微&nbsp;&nbsp;信&nbsp;&nbsp;app ID</label>
                    <div class="layui-input-block" style="width:500px;margin-left:140px">
                        <input type="text" name="schoolshortname" lay-verify="schoolshortname" autocomplete="off" placeholder="" class="layui-input"> 
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="text-align:center;width:110px">微&nbsp;&nbsp;信&nbsp;&nbsp;&nbsp;密&nbsp;&nbsp;钥</label>
                    <div class="layui-input-block" style="width:500px;margin-left:140px">
                        <input type="text" name="schoolshortname" lay-verify="schoolshortname" autocomplete="off" placeholder="" class="layui-input">                        
                    </div>
                </div>
                <div class="layui-form-item layui-layout-admi1">
                    <div class="layui-input-block" style="wid4h:500px;margin-left:150px">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit site-demo-active"style="border-radius:5px;margin-left:20%" data-type="tabChange">保存</button>
                        </div>
                    </div>
                </div>
                <div id="edit_rate" class="hide" style="display: none;background-color:#fff;border-radius:10px;">
                    <div class="layui-card-body tankuang">
                        <div class="layui-form">
                            <div class="layui-form-item">
                            <label class="layui-form-label">是否保存</label>
                            </div>
                            <div class="layui-form-item" style="display: flex;">
                                <div class="layui-input-block">
                                    <div class="layui-footer">
                                        <button class="layui-btn submits yes" style="background-color: #1E9FFF;border-radius:5px;margin-left:-55px;">是</button>
                                    </div>
                                </div>
                                <div class="layui-input-block">
                                    <div class="layui-footer">
                                        <button class="layui-btn submits no" style="background-color: #FF5722;border-radius:5px;">否</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script> 
<script>
    var token = sessionStorage.getItem("Usertoken");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','element'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,form = layui.form;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });

        $.post("{{url('/api/user/jd_config')}}",
        {
            token:token
            ,type:'2'
        },function(res){
    //        console.log(res);
            $('.layui-form .layui-form-item').eq(0).find('input').val(res.data.agentNo);
            $('.layui-form .layui-form-item').eq(1).find('input').val(res.data.store_md_key);
            $('.layui-form .layui-form-item').eq(2).find('input').val(res.data.store_des_key);
            $('.layui-form .layui-form-item').eq(3).find('input').val(res.data.systemId);
            $('.layui-form .layui-form-item').eq(4).find('input').val(res.data.ali_appid);
            $('.layui-form .layui-form-item').eq(5).find('input').val(res.data.wx_appid);
            $('.layui-form .layui-form-item').eq(6).find('input').val(res.data.wx_secret);
        },"json");

        // $('.submit').on('click', function(){
        //     console.log($('.private_key').val());
        //     $.post("{{url('/api/user/jd_config')}}",
        //     {
        //         token:token,
        //         type:"1",
        //         agentNo:$('.layui-form .layui-form-item').eq(0).find('input').val(),
        //         store_md_key:$('.layui-form .layui-form-item').eq(1).find('input').val(),
        //         store_des_key:$('.layui-form .layui-form-item').eq(2).find('input').val(),
        //         systemId:$('.layui-form .layui-form-item').eq(3).find('input').val(),
        //         ali_appid:$('.layui-form .layui-form-item').eq(4).find('input').val(),
        //         wx_appid:$('.layui-form .layui-form-item').eq(5).find('input').val(),
        //         wx_secret:$('.layui-form .layui-form-item').eq(6).find('input').val(),
        //     },function(res){
        //         console.log(res);
        //         if(res.status==1){
        //             layer.msg(res.message, {
        //                 offset: '50px'
        //                 ,icon: 1
        //                 ,time: 3000
        //             });
        //         }else{
        //             layer.alert(res.message, {icon: 2});
        //         }
        //     },"json");
        // });

        $('.submit').on('click', function(){
            layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area: '350px',
                skin: 'layui-layer-nobg', //没有背景色
                shadeClose: true,
                content: $('#edit_rate')
            });
        // console.log($('.private_key').val());
            $('.yes').on('click',function(){
                $.post("{{url('/api/user/jd_config')}}",
                {
                    token:token,
                    type:"1",
                    agentNo:$('.layui-form .layui-form-item').eq(0).find('input').val(),
                    store_md_key:$('.layui-form .layui-form-item').eq(1).find('input').val(),
                    store_des_key:$('.layui-form .layui-form-item').eq(2).find('input').val(),
                    systemId:$('.layui-form .layui-form-item').eq(3).find('input').val(),
                    ali_appid:$('.layui-form .layui-form-item').eq(4).find('input').val(),
                    wx_appid:$('.layui-form .layui-form-item').eq(5).find('input').val(),
                    wx_secret:$('.layui-form .layui-form-item').eq(6).find('input').val()
                },function(res){
    //                console.log(res);
                    if(res.status==1){
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 2000
                        },function(){
                            window.location.reload();
                        });
                    }else{
                        layer.alert(res.message, {icon: 2});
                    }
                },'json')
            });

            $('.no').on('click',function(){
                $('#layui-layer-shade1').css('opacity','0');
                $('#edit_rate').css('display', 'none');
            });

        });

        $('.submits').click(function(){
            $('#edit_rate').css('display','none')
        });

    });
</script>

</body>
</html>
