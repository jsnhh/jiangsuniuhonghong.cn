<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>服务商管理后台</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    {{--<link rel="icon" href="{{asset('/layuiadmin/layui/images/icon.png')}}">--}}
    {{--<link rel="icon" href="{{asset('/layuiadmin/layui/images/icon.png')}}" type="image/x-icon"/>--}}
    {{--<link rel="shortcut icon" href="{{asset('/layuiadmin/layui/images/icon.png')}}">--}}
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/usericon/iconfont.css')}}" media="all">
    <link id="demo5" rel="icon" href="">
    <link id="demo5" rel="icon" href="" type="image/x-icon"/>
    <link id="demo5" rel="shortcut icon" href="">
    <link id="demo5" rel="stylesheet" href="" media="all">
</head>
<body class="layui-layout-body">

<div id="LAY_app">
    <div class="layui-layout layui-layout-admin">
        <div class="layui-header" style="height:60px;line-height:60px">
            <!-- 头部区域 -->
            <ul class="layui-nav layui-layout-left">
                <!-- <li class="layui-nav-item layadmin-flexible" lay-unselect>
                <a href="javascript:;" layadmin-event="flexible" title="侧边伸缩">
                <i class="layui-icon layui-icon-shrink-right" id="LAY_app_flexible"></i>
                </a>
                </li> -->

                <li class="layui-nav-item" lay-unselect>
                    <a href="javascript:;" layadmin-event="refresh" title="刷新">
                        <i class="layui-icon layui-icon-refresh-3"></i>
                    </a>
                </li>
                <!-- <li class="layui-nav-item layui-hide-xs" lay-unselect>
                <input type="text" placeholder="搜索..." autocomplete="off" class="layui-input layui-input-search" layadmin-event="serach" lay-action="template/search.html?keywords=">
                </li> -->
            </ul>
            <ul class="layui-nav layui-layout-right" lay-filter="layadmin-layout-right">
                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    {{--<a href="javascript:;" layadmin-event="fullscreen">--}}
                    {{--<i class="layui-icon iconfont"></i>--}}
                    {{--</a>--}}
                    <a layadmin-event="gg" class="gg" id="gg"><img style="margin-top:-3px" title="公告" src="{{asset('/school/images/gg.png')}}" width="25px" height="25x" /></a>
                </li>
                <li class="layui-nav-item" lay-unselect>
                    <a class="seetixian" data=''>
                        <cite class="store_name"></cite>
                    </a>
                    <!-- <dl class="layui-nav-child">
                    <dd><a lay-href="set/user/info.html">基本资料</a></dd>
                    <dd><a lay-href="set/user/password.html">修改密码</a></dd>
                    </dl> -->
                </li>
                <!-- <li class="layui-nav-item layui-hide-xs" lay-unselect>
                <a target="_blank" href="https://fchelp.cloud.alipay.com/help.htm?tntInstId=EBBYLCCN&helpCode=SCE_00000888" layadmin-event="" class="">帮助中心</a>
                </li> -->
                <li class="layui-nav-item layui-hide-xs" lay-unselect >
                    <a layadmin-event="chpass" class="chpass" id="chpass"><img style="margin-right:5px;margin-top:-3px" src="{{asset('/school/images/mi.png')}}" width="14px" height="14px" />修改密码</a>
                </li>
                <li class="layui-nav-item layui-hide-xs" lay-unselect style="margin-right:6px">
                    <a href="javascript:;" layadmin-event="logout" class="logout"><embed src="{{asset('/layuiadmin/img/tuichudenglu.svg')}}" width="14px" height="14px" type="image/svg+xml"/>退出</a>
                </li>
                <li class="layui-nav-item layui-show-xs-inline-block layui-hide-sm" lay-unselect>
                    <a href="javascript:;" layadmin-event="more"><i class="layui-icon layui-icon-more-vertical"></i></a>
                </li>
            </ul>
        </div>

        <!-- 侧边菜单 -->
        <div class="layui-side layui-side-menu" style="background-color: #3475c3 !important;width:200px">
            <div class="layui-side-scroll" style="width:220px">
                <div class="layui-logo" style="background-color:#3475c3 !important;width:200px;height:60px">
                    <!-- <span>服务商管理后台</span> -->
                    <img id="demo4" src="" style="max-height:60px;">
                    {{--<img style="height: 40px; text-align: center;margin: 10px 1px 20px 0;" src="{{asset('/layuiadmin/layui/images/user_index.png')}}">--}}
                </div>

                <ul class="layui-nav layui-nav-tree box" style="width:200px" lay-shrink="all" id="LAY-system-side-menu" lay-filter="layadmin-system-side-menu">
                    <li data-name="home" class="layui-nav-item layui-nav-itemed" data="首页">
                        <a lay-href="{{url('/user/home')}}" lay-tips="首页" lay-direction="2">
                            <i class="layui-icon iconfont" style="font-size: 14px;">&#xe677;</i>
                            <cite>首页</cite>
                        </a>
                    </li>
                    <li data-name="template" class="layui-nav-item" data="代理商管理">
                        <a href="javascript:;" lay-tips="代理商管理" lay-direction="2">
                            <i class="layui-icon iconfont" style="font-size: 14px;">&#xe674;</i>
                            <cite>代理商管理</cite>
                        </a>
                        <dl class="layui-nav-child">
                            <dd data-name="console"><a lay-href="{{url('/user/agentlist')}}" data="代理商列表">代理商列表</a></dd>
                            <dd data-name="console"><a lay-href="{{url('/user/agentrecovery')}}" data="代理商列表">代理回收站</a></dd>
                            {{--<dd data-name="grid"><a lay-href="{{url('/user/qrcode')}}" data="我的激活码">我的激活码</a></dd>--}}
                        </dl>
                    </li>
                    <li data-name="template" class="layui-nav-item item_mendian" data='门店管理'>
                        <a href="javascript:;" lay-tips="门店管理" lay-direction="2">
                            <i class="layui-icon iconfont" style="font-size: 14px;">&#xe67e;</i>
                            <cite>门店管理</cite>
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a lay-href="{{url('/user/storelist')}}" data="门店列表">门店列表</a></dd>
                            {{--                            <dd><a lay-href="{{url('/user/cashlessvoucher')}}" data="支付宝代金券管理">支付宝代金券管理</a></dd>--}}
                            {{--                            <dd><a lay-href="{{url('/user/wechat_coupon_list')}}" data="微信代金券管理">微信代金券管理</a></dd>--}}
                            {{--<dd><a lay-href="{{url('/user/makemoney')}}" data="商户打款记录">商户打款记录</a></dd>--}}
                            {{--<dd><a lay-href="{{url('/user/editpaywaysort')}}" data="扣款顺序管理">批量收款顺序置顶</a></dd>--}}
                        </dl>
                    </li>
                    <li data-name="template" class="isShow layui-nav-item item_mendian" data='支付宝IOT代运营'>
                        <a href="javascript:;" lay-tips="支付宝IOT代运营" lay-direction="2">
                            <i class="layui-icon iconfont">&#xe67b;</i>
                            <cite>支付宝IOT代运营</cite>
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a lay-href="{{url('/user/storesealist')}}" data="门店列表">新蓝海活动</a></dd>
                            {{--                            <dd><a lay-href="{{url('/user/addoperation')}}" data="代运营授权">代运营授权</a></dd>--}}
                            <dd><a lay-href="{{url('/user/bluesealist?id=&store_id=')}}" data="新蓝海列表">新蓝海列表</a></dd>
                            <dd><a lay-href="{{url('/user/operationlist?id=&store_id=')}}" data="代运营授权列表">代运营授权列表</a></dd>



                            {{--                            <dd><a lay-href="{{url('/user/makemoney')}}" data="商户打款记录">商户打款记录</a></dd>--}}
                            {{--                            <dd><a lay-href="{{url('/user/editpaywaysort')}}" data="扣款顺序管理">批量收款顺序置顶</a></dd>--}}
                        </dl>
                    </li>
                    <li data-name="template" class="isShow layui-nav-item item_mendian" data='授权小程序'>

                        <a href="javascript:;" lay-tips="授权小程序" lay-direction="2">
                            <i class="layui-icon iconfont" style="font-size: 14px;">&#xe670;</i>
                            <cite>授权小程序</cite>
                        </a>

                        <dl class="layui-nav-child">
                            <dd><a lay-href="{{url('/user/applets')}}" data="授权小程序管理">微信小程序管理</a></dd>
                            <dd><a lay-href="{{url('/user/appletsAliPay')}}" data="授权小程序管理">支付宝小程序管理</a></dd>
                        </dl>

                    </li>
                    <li data-name="template" class="layui-nav-item item_zhifu" data='交易数据管理'>
                        <a href="javascript:;" lay-tips="交易统计" lay-direction="2">
                            <i class="layui-icon iconfont" style="font-size: 14px;">&#xe671;</i>
                            <cite>交易统计</cite>
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a lay-href="{{url('/user/commissionlist')}}" data='分润账单'>分润账单</a></dd>
                            <dd><a lay-href="{{url('/user/tradelist')}}" data="商户订单管理">商户订单管理</a></dd>
                            <dd><a lay-href="{{url('/user/reconciliation?user_id=&user_name=')}}" data="商户交易统计">商户交易统计</a></dd>
                            <dd><a lay-href="{{url('/user/flowerlist')}}" data="花呗分期流水">花呗分期流水</a></dd>
                            <dd><a lay-href="{{url('/user/transactionlist')}}" data='门店交易统计'>门店交易统计</a></dd>
                            {{--<dd><a lay-href="{{url('/user/transactionvolume')}}" data='门店交易量'>门店交易量</a></dd>--}}
                            <dd><a lay-href="{{url('/user/facepaymentstatistics')}}" data='刷脸日去重统计'>刷脸日去重统计</a></dd>
                            <dd><a lay-href="{{url('/user/facepaymentdis')}}" data='刷脸去重统计'>刷脸去重统计</a></dd>
                            {{--<dd data-name="console"><a lay-href="{{url('/user/agentrecord')}}" data="代理月对账查询">代理月对账查询</a></dd>--}}
                            {{--<dd><a lay-href="{{url('/user/monthrecord?user_id=&user_name=')}}" data="门店月对账查询">门店月对账查询</a></dd>--}}
                            {{--<dd><a lay-href="{{url('/user/profitsharinglists')}}" data="微信分账流水">微信分账流水</a></dd>--}}
                            <dd><a lay-href="{{url('/user/rechargeList')}}" data='商户充值记录'>商户充值记录</a></dd>
                            <dd><a lay-href="{{url('/user/passagewayList')}}" data='通道使用记录'>通道使用记录</a></dd>
                        </dl>
                    </li>
                    <li data-name="template" class="layui-nav-item item_huodong" data='行业解决方案'>
                        <a href="javascript:;" lay-tips="行业解决方案" lay-direction="2">
                            <i class="layui-icon iconfont" style="font-size: 14px;">&#xe67a;</i>
                            <cite>行业解决方案</cite>
                        </a>
                        <dl class="layui-nav-child">
                            <dd data-name="classmanage">
                                <a href="javascript:;" data='教育缴费'>教育缴费</a>
                                <dl class="layui-nav-child">
                                    <dd><a lay-href="{{url('/user/waterlist')}}" data='交易流水'>交易流水</a></dd>
                                    <dd><a lay-href="{{url('/user/schoollist')}}" data='学校列表'>学校列表</a></dd>
                                </dl>
                            </dd>
                            <dd data-name="classmanage" class="isShow">
                                <a href="javascript:;" data='押金支付'>押金支付</a>
                                <dl class="layui-nav-child">
                                    <dd><a lay-href="{{url('/user/depositwater')}}" data='押金流水'>押金流水</a></dd>
                                    <dd><a lay-href="{{url('/user/depositacount')}}" data='对账账单'>对账账单</a></dd>
                                </dl>
                            </dd>
                        </dl>
                    </li>
                    <li data-name="template" class="isShow layui-nav-item item_huodong" data='活动管理'>
                        <a href="javascript:;" lay-tips="活动管理" lay-direction="2">
                            <i class="layui-icon iconfont" style="font-size: 14px;">&#xe67c;</i>
                            <cite>活动管理</cite>
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a lay-href="{{url('/user/alipayred')}}" data='支付宝红包'>支付宝红包</a></dd>
                            {{--<dd data-name="classmanage" class="layui-nav-itemed">--}}
                            {{--<a href="javascript:;" data='京东白条'>京东白条</a>--}}
                            {{--<dl class="layui-nav-child">--}}
                            {{--<dd><a lay-href="{{url('/user/jdwhitebar')}}" data='白条数据录入'>白条数据录入</a></dd>--}}
                            {{--</dl>--}}
                            {{--</dd>--}}
                            <dd><a lay-href="{{url('/user/fee')}}" data='商户返手续费'>商户返手续费</a></dd>
                            <dd><a lay-href="{{url('/user/cashBackRule')}}" data='交易返现规则'>交易返现规则</a></dd>
                            <dd><a lay-href="{{url('/user/userRules')}}" data='代理返现列表'>代理返现列表</a></dd>
                            <dd><a lay-href="{{url('/user/storeTransactionRewardList')}}" data='商户交易达标返现'>商户交易达标返现</a></dd>

                        </dl>
                    </li>
                    <li data-name="template" class="layui-nav-item item_shangjin" data='赏金管理'>
                        <a href="javascript:;" lay-tips="赏金管理" lay-direction="2">
                            <i class="layui-icon iconfont" style="font-size: 14px;">&#xe67f;</i>
                            <cite>分润管理</cite>
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a lay-href="{{url('/user/reward')}}" data='赏金列表'>赏金列表</a></dd>
                            <dd><a lay-href="{{url('/user/putforward')}}" data='提现记录'>提现记录</a></dd>
                            <dd><a lay-href="{{url('/user/getUserMoney')}}" data='提现记录'>未提分润</a></dd>
                        <!--<dd><a lay-href="{{url('/user/settlement')}}" data='赏金结算'>赏金结算</a></dd>-->
                            <dd><a lay-href="{{url('/user/settlementday')}}" data='赏金结算'>日结设置</a></dd>
                            <dd><a lay-href="{{url('/user/cashset')}}" data='提现设置'>提现设置</a></dd>
                            <dd><a lay-href="{{url('/user/settlerecord')}}" data='结算记录'>结算记录</a></dd>
                            <dd><a lay-href="{{url('/user/returnexport')}}" data='返佣导出'>返佣导出</a></dd>
                            <dd><a lay-href="{{url('/user/unbalanced')}}" data='未结算佣金'>未结算佣金</a></dd>
                            <dd><a lay-href="{{url('/user/zeroRateSettlementDay')}}" data='零费率日结算记录'>零费率日结算记录</a></dd>
                            <dd><a lay-href="{{url('/user/zeroRateSettlementMonth')}}" data='零费率月结算记录'>零费率月结算记录</a></dd>
                        </dl>
                    </li>
                    {{--<li data-name="template" class="layui-nav-item item_shangjin" data='赏金管理'>--}}
                    {{--<a href="javascript:;" lay-tips="代付管理" lay-direction="2">--}}
                    {{--<i class="layui-icon layui-icon-dollar"></i>--}}
                    {{--<cite>代付管理</cite>--}}
                    {{--</a>--}}
                    {{--<dl class="layui-nav-child">--}}
                    {{--<dd><a lay-href="{{url('/user/yytong')}}" data='银盈通'>银盈通</a></dd>--}}
                    {{--</dl>--}}
                    {{--</li>--}}
                    <li data-name="template" class="layui-nav-item item_ad" data='广告管理'>
                        <a href="javascript:;" lay-tips="广告管理" lay-direction="2">
                            <i class="layui-icon iconfont" style="font-size: 14px;">&#xe676;</i>
                            <cite>广告管理</cite>
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a lay-href="{{url('/user/ad')}}" data='广告列表'>广告列表</a></dd>
                            <dd><a lay-href="{{url('/user/adlinks')}}" data='广告链接'>广告链接</a></dd>
                        </dl>
                    </li>
                    <li data-name="template" class="layui-nav-item item_code" data='二维码统一管理'>
                        <a href="javascript:;" lay-tips="二维码统一管理" lay-direction="2">
                            <i class="layui-icon iconfont" style="font-size: 13px;">&#xe673;</i>
                            <cite>二维码统一管理</cite>
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a lay-href="{{url('/user/qrcodemanage')}}" data='商户收款空码'>商户收款空码</a></dd>
                            {{--<dd><a lay-href="{{url('/user/percode')}}" data='个人码合并'>个人码合并</a></dd>--}}
                        </dl>
                    </li>
                    <li data-name="template" class="isShow layui-nav-item item_xinxi" data='信息管理'>
                        <a href="javascript:;" lay-tips="信息管理" lay-direction="2">
                            <i class="layui-icon iconfont" style="font-size: 14px;">&#xe675;</i>
                            <cite>信息管理</cite>
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a lay-href="{{url('/user/appmsg')}}" data='app消息'>app消息</a></dd>
                            <dd><a lay-href="{{url('/user/bannerlist')}}" data='banner列表'>banner列表</a></dd>
                            <dd data-name="classmanage" class="layui-nav-itemed">
                                <a href="javascript:;" data='app功能管理'>app功能管理</a>
                                <dl class="layui-nav-child">
                                    <dd><a lay-href="{{url('/user/appindexfunction')}}" data='APP首页功能区'>APP首页功能区</a></dd>
                                    <dd><a lay-href="{{url('/user/appmyfunction')}}" data='APP我的功能'>APP我的功能</a></dd>
                                    <dd><a lay-href="{{url('/user/apphead')}}" data='APP头条'>APP头条</a></dd>
                                </dl>
                            </dd>
                        </dl>
                    </li>
                    <li data-name="template" class="layui-nav-item item_juese" data='角色权限管理' >
                        <a href="javascript:;" lay-tips="角色权限管理" lay-direction="2">
                            <i class="layui-icon iconfont" style="font-size: 14px;">&#xe679;</i>
                            <cite>角色权限管理</cite>
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a lay-href="{{url('/user/role')}}" data='角色管理'>角色管理</a></dd>
                            <dd><a class="isShow" lay-href="{{url('/user/power')}}" data='权限管理'>权限管理</a></dd>
                        </dl>
                    </li>
                    <li data-name="template" class="isShow layui-nav-item item_zhifu" data='支付配置'>
                        <a href="javascript:;" lay-tips="支付配置" lay-direction="2">
                            <i class="layui-icon iconfont" style="font-size: 14px;">&#xe678;</i>
                            <cite>支付配置</cite>
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a lay-href="{{url('/user/alipayconfirm')}}" data='支付宝应用配置'>支付宝应用配置</a></dd>
                            <dd><a lay-href="{{url('/user/wechatconfirm')}}" data='微信应用配置'>微信应用配置</a></dd>
                            {{--<dd><a lay-href="{{url('/user/jdconfigure')}}" data='京东金融配置'>京东金融配置</a></dd>--}}
                            <dd><a lay-href="{{url('/user/newworld')}}" data='新大陆配置'>新大陆配置</a></dd>
                            {{--<dd><a lay-href="{{url('/user/hrtconfig')}}" data='和融通配置'>和融通配置</a></dd>--}}
                            <dd><a lay-href="{{url('/user/dlbconfig')}}" data='哆啦宝配置'>哆啦宝配置</a></dd>
                            {{--<dd><a lay-href="{{url('/user/zfthbfq')}}" data='直付通花呗分期'>直付通花呗分期</a></dd>--}}
                            {{--<dd><a lay-href="{{url('/user/tfconfig')}}" data='TF支付配置'>TF支付配置</a></dd>--}}
                            <dd><a lay-href="{{url('/user/sxfconfig')}}" data='随行付配置'>随行付配置</a></dd>
                            <dd><a lay-href="{{url('/user/sxfaconfig')}}" data='随行付A配置'>随行付A配置</a></dd>
                            <dd><a lay-href="{{url('/user/easypay')}}" data='易生支付配置'>易生支付配置</a></dd>
                            {{--<dd><a lay-href="{{url('/user/hkrtconfig')}}" data='海科融通配置'>海科融通配置</a></dd>--}}
                            {{--<dd><a lay-href="{{url('/user/huipayconfig')}}" data='随行付A配置'>汇付支付配置</a></dd>--}}
                            {{--<dd><a lay-href="{{url('/user/hltxconfig')}}" data='葫芦天下配置'>葫芦天下配置</a></dd>--}}
                            {{--<dd><a lay-href="{{url('/user/linkageconfig')}}" data='联动优势配置'>联动优势配置</a></dd>--}}
                            {{--<dd><a lay-href="{{url('/user/wftpayconfig')}}" data='威富通配置'>威富通配置</a></dd>--}}
                            {{--<dd><a lay-href="{{url('/user/hwcpayconfig')}}" data='汇旺财配置'>汇旺财配置</a></dd>--}}
                            <dd><a lay-href="{{url('/user/dadaconfigset')}}" data='达达配置'>达达配置</a></dd>
                            <dd><a lay-href="{{url('/user/wechataconfirm')}}" data='官方微信a配置'>官方微信a配置</a></dd>
                            <dd><a lay-href="{{url('/user/yinshengconfig')}}" data='银盛配置'>银盛配置</a></dd>
                            <dd><a lay-href="{{url('/user/qfpayconfig')}}" data='银盛配置'>钱方配置</a></dd>
                        </dl>
                    </li>
                    <li data-name="template" class="isShow layui-nav-item item_zhifu" data='微信卡券配置' >
                        <a href="javascript:;" lay-tips="微信卡券配置" lay-direction="2">
                            <i class="layui-icon iconfont" style="font-size: 14px;">&#xe67d;</i>
                            <cite>微信卡券配置</cite>
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a lay-href="{{url('/user/wechatcashcouponconfirm')}}" data='微信应用配置'>微信代金券配置</a></dd>
                        </dl>
                        <dl class="layui-nav-child">
                            <dd><a lay-href="{{url('/user/wechatmerchantcashcouponconfirm')}}" data='微信商家券配置'>微信商家券配置</a></dd>
                        </dl>
                    </li>
                    <li data-name="template" class="isShow layui-nav-item item_xitong" data='第三方平台配置' >
                        <a href="javascript:;" lay-tips="第三方平台配置" lay-direction="2">
                            <i class="layui-icon iconfont" style="font-size: 14px;">&#xe67d;</i>
                            <cite>开放平台配置</cite>
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a lay-href="{{url('/user/wechatthirdconfig')}}" data='微信第三方'>微信开放平台</a></dd>
                        </dl>
                        <dl class="layui-nav-child">
                            <dd><a lay-href="{{url('/user/alithirdconfig')}}" data='支付宝第三方'>支付宝开放平台</a></dd>
                        </dl>
                    </li>
                    <li data-name="template" class="isShow layui-nav-item item_xitong" data='系统配置'>
                        <a href="javascript:;" lay-tips="系统配置" lay-direction="2">
                            <i class="layui-icon iconfont" style="font-size: 14px;">&#xe67d;</i>
                            <cite>系统配置</cite>
                        </a>
                        <dl class="layui-nav-child">
                        <!--<dd><a lay-href="{{url('/user/updata')}}" data='系统更新'>系统更新</a></dd>-->
                        <!--<dd><a lay-href="{{url('/user/ysyupdata')}}" data='系统更新'>系统更新</a></dd>-->
                            <dd><a lay-href="{{url('/user/appconfig')}}" data='APP配置'>APP配置</a></dd>
                            <dd><a lay-href="{{url('/user/pushconfig')}}" data='推送配置'>推送配置</a></dd>
                            <dd><a lay-href="{{url('/user/msgconfig')}}" data='短信配置'>短信配置</a></dd>
                        <!-- <dd><a lay-href="{{url('/user/storeconfig')}}" data='门店配置'>门店配置</a></dd> -->
                            <dd><a lay-href="{{url('/user/devicemanage')}}" data='设备列表'>设备列表</a></dd>
                            <dd><a lay-href="{{url('/user/deviceconfig')}}" data='设备配置'>设备配置</a></dd>
                            <dd><a lay-href="{{url('/user/mqtt')}}" data='MQTT推送'>MQTT推送</a></dd>
                            <dd><a lay-href="{{url('/user/logoconfig')}}" data='logo配置'>logo配置</a></dd>
                            <dd><a lay-href="{{url('/user/apkconfig')}}" data='apk配置'>apk配置</a></dd>
                            <dd><a lay-href="{{url('/user/alispiconfig')}}" data='支付宝如意配置'>支付宝如意配置</a></dd>
                        </dl>
                    </li>
                </ul>
            </div>
        </div>

        <!-- 页面标签 -->
        <div class="layadmin-pagetabs" id="LAY_app_tabs" style="top: 58px;">
            <div class="layui-icon layadmin-tabs-control layui-icon-prev" layadmin-event="leftPage"></div>
            <div class="layui-icon layadmin-tabs-control layui-icon-next" layadmin-event="rightPage"></div>
            <div class="layui-icon layadmin-tabs-control layui-icon-down">
                <ul class="layui-nav layadmin-tabs-select" lay-filter="layadmin-pagetabs-nav">
                    <li class="layui-nav-item" lay-unselect>
                        <a href="javascript:;"></a>
                        <dl class="layui-nav-child layui-anim-fadein">
                            <dd layadmin-event="closeThisTabs"><a href="javascript:;">关闭当前标签页</a></dd>
                            <dd layadmin-event="closeOtherTabs"><a href="javascript:;">关闭其它标签页</a></dd>
                            <dd layadmin-event="closeAllTabs"><a href="javascript:;">关闭全部标签页</a></dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <div class="layui-tab" lay-unauto lay-allowClose="true" lay-filter="layadmin-layout-tabs">
                <ul class="layui-tab-title" id="LAY_app_tabsheader">
                    <li lay-id="{{url('/user/home')}}" lay-attr="{{url('/user/home')}}" class="layui-this">首页</li>
                </ul>
            </div>
        </div>

        <!-- 主体内容 -->
        <div class="layui-body" id="LAY_app_body" style="margin-top: 60px;">
            <div class="layadmin-tabsbody-item layui-show">
                <iframe src="{{url('/user/home')}}" frameborder="0" class="layadmin-iframe"></iframe>
            </div>
        </div>

        <!-- 辅助元素，一般用于移动设备下遮罩 -->
        <div class="layadmin-body-shade" layadmin-event="shade"></div>
    </div>
</div>

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','element'], function(){
        var $ = layui.$;
        var token = sessionStorage.getItem("Usertoken");
        var s_agentname = sessionStorage.getItem("s_agentname");
        var level = sessionStorage.getItem("level");
        var l_user_id = sessionStorage.getItem("l_user_id");
        var source = sessionStorage.getItem('source');

        $('.store_name').html(s_agentname);
        $('.seetixian').attr('data',l_user_id);

        $('.logout').click(function(){
            sessionStorage.removeItem("Usertoken");
            sessionStorage.clear();
            window.location.reload();
        });

        // 跳转修改密码页面
        $('#chpass').click(function(){
            $('.chpass').attr('lay-href',"{{url('/user/password')}}");
        });

        // 跳转公告页面
        $('#gg').click(function(){
            $('.gg').attr('lay-href',"{{url('/user/notice')}}");
        });

        // 未登录,跳转登录页面
        $(document).ready(function(){
            var token = sessionStorage.getItem("Usertoken");
            var admin = sessionStorage.getItem("admin");
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });

        if(source == '02' || source == '03'){
            $(".isShow ").hide();
        }

        var permissions = sessionStorage.getItem("permissions");
        var str = JSON.parse(permissions);
        var arr = [];
        for(var i=0;i<str.length;i++){
            var aa = str[i].name;
            arr.push(aa);
        }

        if(level != 0){
            // 权限管理+++++++++++++
            $('ul.box li').each(function(index,item){
//             console.log($(this).attr('data'));
//             console.log(arr);
//             if($.inArray( $(this).attr('data'), arr ) == -1){
//                 $(this).hide();
//             }
            });

            $('ul.box li dl dd').each(function(index,item){
//             console.log($(this).find('a').attr('data'));
//             console.log($.inArray($(this).find('a').attr('data'),arr));
                if($.inArray($(this).find('a').attr('data'),arr)==-1){
                    $(this).find('a').hide();
                }
            })
        }

        $.post("{{url('/api/basequery/app_logos_info')}}",
            {
                token:token
                ,type:'2'
            },function(res){
                if(res.status == 1){
                    if(res.data.ht_img==''){

                    }else{
                        $('#demo4').attr('src',res.data.ht_img);
                    }
                    if(res.data.favicon_url==''){

                    }else{
                        $('#demo5').attr('href',res.data.favicon_url);
                    }
                }
            }, "json")

    });

</script>
</body>
</html>
