<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>上传小程序代码</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/formSelects-v4.css')}}" media="all">
    <style type="text/css">
        .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
        .img_box span{position: absolute;right:0;top:0;font-size: 30px;background: #fff;cursor: pointer;}
        .img_box2 span{position: absolute;right:0;top:0;font-size: 30px;background: #fff;cursor: pointer;}
        .lianjiecon div{width:100%;overflow: hidden;margin-bottom:20px;}
        .lianjiecon label{display: inline-block;float: left;width:10%;line-height: 36px;}
        .lianjiecon input{display: inline-block;float: left;width:90%;}
        #demo5{width: 200px;}

    </style>
</head>
<body>
<div class="layui-fluid" id="appletsInfo">
    <div class="layui-card">
        <div class="layui-card-header">上传小程序代码</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
                <ul class="layui-tab-title">
                    <li class="layui-this">微信小程序代码</li>
                    <li>支付宝小程序代码</li>
                </ul>
                <div class="layui-tab-content">
                    <div class="layui-tab-item layui-show">
                        <form class="layui-form" action="" lay-filter="storeAppletsInfo">
                            <div class="layui-form-item">
                                <label class="layui-form-label" style="text-align:center">微信小程序名称</label>
                                <div class="layui-input-block">
                                    <input type="text" placeholder="请输入微信小程序名称" name="wechat_name" class="layui-input title">
                                </div>
                            </div>
                            <div class="layui-card-body">
                                <label class="layui-form-label" style="text-align:center">门店经营执照（仅供微信审核小程序名称使用）</label>
                                <div class="layui-upload">
                                    <button class="layui-btn up" style="border-radius:5px;position: relative;">
                                        <i class="layui-icon">&#xe67c;</i>
                                        <input type="file" name="file" id="uploadFile" style="position: absolute;top: 0;left: 0;" />上传营业执照
                                    </button>
                                    <div class="layui-upload-list">
                                        <img class="layui-upload-img" id="demo5">
                                        <p id="demoText"></p>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="license" value="" />

                            <div class="layui-form-item">
                                <label class="layui-form-label" style="text-align:center">* 使用的模板id</label>
                                <div class="layui-input-block">
                                    <input type="text" placeholder="请输入使用的模板id" name="template_id" class="layui-input title">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label" style="text-align:center">* 微信小程序版本</label>
                                <div class="layui-input-block">
                                    <input type="text" placeholder="请输入微信小程序版本" name="user_version" class="layui-input title">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label" style="text-align:center">* 小程序描述内容(仅供审核时使用)</label>
                                <div class="layui-input-block">
                                    <textarea name="desc" placeholder="请输入内容" class="layui-textarea con"></textarea>
                                </div>
                            </div>
                            <div class="layui-form-item layui-layout-admin">
                                <div class="layui-input-block">
                                    <div class="layui-footer" style="left: 0;">
                                        <button type="button" class="layui-btn submit" lay-submit lay-filter="appletsInfo">上传代码</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="layui-tab-item">
                        <form class="layui-form" action="" lay-filter="storeAliPayAppletsInfo">
                            {{--<div class="layui-form-item">--}}
                                {{--<label class="layui-form-label" style="text-align:center">营业执照名称</label>--}}
                                {{--<div class="layui-input-block">--}}
                                    {{--<input type="text" placeholder="请输入营业执照名称" value="" name="license_name" class="layui-input title">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="layui-form-item">--}}
                                {{--<label class="layui-form-label" style="text-align:center">营业执照有效期</label>--}}
                                {{--<div class="layui-input-block">--}}
                                    {{--<input type="text" placeholder="请输入营业执照有效期" value="9999-12-31" id="license_valid_date" name="license_valid_date" class="layui-input title">--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <div class="layui-form-item">
                                <label class="layui-form-label" style="text-align:center">* 使用的模板id</label>
                                <div class="layui-input-block">
                                    <input type="text" placeholder="请输入使用的模板id" value="2021002108609082" name="template_id" class="layui-input title">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label" style="text-align:center">* 支付宝小程序版本</label>
                                <div class="layui-input-block">
                                    <input type="text" placeholder="请输入支付宝小程序版本" value="" name="template_version" class="layui-input title">
                                </div>
                            </div>
                            {{--<div class="layui-form-item">--}}
                                {{--<label class="layui-form-label" style="text-align:center">* 小程序描述内容(仅供审核时使用)</label>--}}
                                {{--<div class="layui-input-block">--}}
                                    {{--<textarea name="desc" placeholder="请输入内容" class="layui-textarea con"></textarea>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="layui-form-item layui-layout-admin">
                                <div class="layui-input-block">
                                    <div class="layui-footer" style="left: 0;">
                                        <button type="button" class="layui-btn submit" lay-submit lay-filter="aliPayAppletsInfo">上传代码</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script type="text/javascript">

    var token = sessionStorage.getItem("Usertoken");
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });
        //获取商家小程序信息
        var store_id   = sessionStorage.getItem('store_store_id');
        var store_name = sessionStorage.getItem('store_store_name');

        //日期选择
        laydate.render({
            elem: '#license_valid_date'
        });

        /**
         * 上传营业执照
         **/
        var uploadInst = upload.render({
            elem: '#uploadFile', //绑定元素
            url: "{{url('/api/customer/weixin/uploadFile')}}", //上传接口
            field:"file",
            method:"post",
            type : 'images',
            ext : 'jpg|png|gif',
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                //上传完毕回调
                if(res.status == 200){
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:1000});
                    layui.jquery('#demo5').attr("src", res.data.path);
                    $("input[name=license]").val(res.data.media_id);
                }else{
                    layer.msg(res.message, {icon:2, shade:0.5, time:1000});
                }
            },
            error: function(err){
                //请求异常回调
                console.log(err);
            }
        });


        /**
         * 微信小程序上传代码
         */
        form.on("submit(appletsInfo)",function(data){
            var requestData = data.field;
            if(!requestData.desc){
                layer.msg("小程序描述内容不可为空", {
                    offset: '50px'
                    ,icon: 2
                    ,time: 2000
                });
                return false;
            }

            if(requestData.wechat_name){
                if(!requestData.license){
                    layer.msg("门店经营执照不可为空", {
                        offset: '50px'
                        ,icon: 2
                        ,time: 2000
                    });
                    return false;
                }
            }

            if(!requestData.template_id){
                layer.msg("小程序模板id不可为空", {
                    offset: '50px'
                    ,icon: 2
                    ,time: 2000
                });
                return false;
            }

            if(!requestData.user_version){
                layer.msg("微信小程序版本不可为空", {
                    offset: '50px'
                    ,icon: 2
                    ,time: 2000
                });
                return false;
            }

            layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
            $.post("/api/customer/weixin/uploadCode",{
                store_id:store_id,
                store_applets_desc:requestData.desc,
                nick_name:requestData.wechat_name,
                template_id:requestData.template_id,
                user_version:requestData.user_version,
                license:requestData.license,
            },function(data){
                var status = data.status;
                if(status == 200){
                    layer.msg(data.message, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 2000
                    });
                }else{
                    layer.msg(data.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 2000
                    });
                }
            },"json");
            return false;
        });

        /**
         * 支付宝小程序上传代码
         */
        form.on("submit(aliPayAppletsInfo)",function(data){
            var requestData = data.field;
            if(!requestData.template_id){
                layer.msg("小程序模板id不可为空", {
                    offset: '50px'
                    ,icon: 2
                    ,time: 2000
                });
                return false;
            }

            if(!requestData.template_version){
                layer.msg("小程序版本不可为空", {
                    offset: '50px'
                    ,icon: 2
                    ,time: 2000
                });
                return false;
            }

            layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
            $.post("api/customer/weixin/getAuthorizerAccessToken",{
                store_id:store_id,
                // license_name:requestData.license_valid_date,
                // license_valid_date:requestData.license_valid_date,
                // store_applets_desc:requestData.desc,
                template_id:requestData.template_id,
                template_version:requestData.template_version
            },function(data){
                var status = data.status;
                if(status == 200){
                    layer.msg(data.message, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 2000
                    });
                }else{
                    layer.msg(data.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 2000
                    });
                }
            },"json");
            return false;
        });

    });

</script>
</html>
