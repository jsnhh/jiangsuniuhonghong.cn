<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>瑞银信支付配置</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
<link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header">瑞银信支付配置</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item xingzhi">
                    <label class="layui-form-label">渠道</label>
                    <div class="layui-input-block">
                        <select name="xingzhi" id="xingzhi" lay-filter="xingzhi">
                            <option value="1">渠道1</option>
                            <option value="2">渠道2</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">isvid</label>
                    <div class="layui-input-block">
                        <input type="text" name="isvid" lay-verify="isvid" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">商户私钥</label>
                    <div class="layui-input-block">
                        <textarea name="private_key" placeholder="" class="layui-textarea"></textarea>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">平台公钥</label>
                    <div class="layui-input-block">
                        <textarea name="public_key" placeholder="" class="layui-textarea"></textarea>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">支付appid</label>
                    <div class="layui-input-block">
                        <input type="text" name="appid" lay-verify="pay_appid" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">微信appid</label>
                    <div class="layui-input-block">
                        <input type="text" name="appid" lay-verify="appid" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">微信secret</label>
                    <div class="layui-input-block">
                        <input type="text" name="wx_secret" lay-verify="wx_secret" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">微信推荐关注appid</label>
                    <div class="layui-input-block">
                        <input type="text" name="subscribe_appid" lay-verify="subscribe_appid" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">微信渠道号channelid</label>
                    <div class="layui-input-block">
                        <input type="text" name="wx_channelid" lay-verify="wx_channelid" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">支付宝pid</label>
                    <div class="layui-input-block">
                        <input type="text" name="ali_pid" lay-verify="ali_pid" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit site-demo-active" data-type="tabChange">保存</button>
                        </div>
                    </div>
                </div>
                <div id="edit_rate" class="hide" style="display: none;background-color:#fff;border-radius:10px;">
                    <div class="layui-card-body tankuang">
                        <div class="layui-form">
                            <div class="layui-form-item">
                                <label class="layui-form-label">是否保存</label>
                            </div>
                            <div class="layui-form-item" style="display: flex;">
                                <div class="layui-input-block">
                                    <div class="layui-footer">
                                        <button class="layui-btn submits yes" style="background-color: #1E9FFF;border-radius:5px;margin-left:-55px;">是</button>
                                    </div>
                                </div>
                                <div class="layui-input-block">
                                    <div class="layui-footer">
                                        <button class="layui-btn submits no" style="background-color: #FF5722;border-radius:5px;">否</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="qd" value="1">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");

    layui.config({
       base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','element'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,form = layui.form;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });

        $.post("{{url('/api/user/hltx_config')}}",
        {
            token:token
            ,getdata:"1"
        },function(res){
            if (res.status == 1) {
                $('.layui-form .layui-form-item').eq(1).find('input').val(res.data.isvid);
                $('.layui-form .layui-form-item').eq(2).find('textarea').val(res.data.private_key);
                $('.layui-form .layui-form-item').eq(3).find('textarea').val(res.data.public_key);
                $('.layui-form .layui-form-item').eq(4).find('input').val(res.data.pay_appid);
                $('.layui-form .layui-form-item').eq(5).find('input').val(res.data.wx_appid);
                $('.layui-form .layui-form-item').eq(6).find('input').val(res.data.wx_secret);
                $('.layui-form .layui-form-item').eq(7).find('input').val(res.data.subscribe_appid);
                $('.layui-form .layui-form-item').eq(8).find('input').val(res.data.wx_channelid);
                $('.layui-form .layui-form-item').eq(9).find('input').val(res.data.ali_pid);
            }
        },"json");

        {{--$('.submit').on('click', function(){--}}
            {{--$.post("{{url('/api/user/hltx_config')}}",--}}
            {{--{--}}
                {{--token:token--}}
                {{--,getdata:"2"--}}
                {{--,qd:$('.qd').val()--}}
                {{--,isvid:$('.layui-form .layui-form-item').eq(1).find('input').val()--}}
                {{--,private_key:$('.layui-form .layui-form-item').eq(2).find('textarea').val(),--}}
                {{--public_key:$('.layui-form .layui-form-item').eq(3).find('textarea').val()--}}
                {{--,pay_appid:$('.layui-form .layui-form-item').eq(4).find('input').val()--}}
                {{--,wx_appid:$('.layui-form .layui-form-item').eq(5).find('input').val()--}}
                {{--,wx_secret:$('.layui-form .layui-form-item').eq(6).find('input').val()--}}
                {{--,subscribe_appid:$('.layui-form .layui-form-item').eq(7).find('input').val()--}}
                {{--,wx_channelid:$('.layui-form .layui-form-item').eq(8).find('input').val()--}}
                {{--,ali_pid:$('.layui-form .layui-form-item').eq(9).find('input').val()--}}
            {{--},function(res){--}}
                {{--if(res.status==1){--}}
                    {{--layer.msg(res.message, {--}}
                        {{--offset: '15px'--}}
                        {{--,icon: 1--}}
                        {{--,time: 3000--}}
                    {{--});--}}
                {{--}else{--}}
                    {{--layer.alert(res.message, {icon: 2});--}}
                {{--}--}}
            {{--},"json");--}}
        {{--});--}}

        // 性质
        form.on('select(xingzhi)', function(data){
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.qd').val(category);

            $.post("{{url('/api/user/hltx_config')}}",
            {
                token:token
                ,getdata:"1"
                ,qd:category
            },function(res){
                if (res.status == 1) {
                    $('.layui-form .layui-form-item').eq(1).find('input').val(res.data.isvid);
                    $('.layui-form .layui-form-item').eq(2).find('textarea').val(res.data.private_key);
                    $('.layui-form .layui-form-item').eq(3).find('textarea').val(res.data.public_key);
                    $('.layui-form .layui-form-item').eq(4).find('input').val(res.data.pay_appid);
                    $('.layui-form .layui-form-item').eq(5).find('input').val(res.data.wx_appid);
                    $('.layui-form .layui-form-item').eq(6).find('input').val(res.data.wx_secret);
                    $('.layui-form .layui-form-item').eq(7).find('input').val(res.data.subscribe_appid);
                    $('.layui-form .layui-form-item').eq(8).find('input').val(res.data.wx_channelid);
                    $('.layui-form .layui-form-item').eq(9).find('input').val(res.data.ali_pid);
                }
            },"json");     
        });

        $('.submit').on('click', function(){
            layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area: '350px',
                skin: 'layui-layer-nobg', //没有背景色
                shadeClose: true,
                content: $('#edit_rate')
            });

            $('.yes').on('click',function(){
                $.post("{{url('/api/user/hltx_config')}}",
                    {
                        token:token
                        ,getdata:"2"
                        ,qd:$('.qd').val()
                        ,isvid:$('.layui-form .layui-form-item').eq(1).find('input').val()
                        ,private_key:$('.layui-form .layui-form-item').eq(2).find('textarea').val(),
                        public_key:$('.layui-form .layui-form-item').eq(3).find('textarea').val()
                        ,pay_appid:$('.layui-form .layui-form-item').eq(4).find('input').val()
                        ,wx_appid:$('.layui-form .layui-form-item').eq(5).find('input').val()
                        ,wx_secret:$('.layui-form .layui-form-item').eq(6).find('input').val()
                        ,subscribe_appid:$('.layui-form .layui-form-item').eq(7).find('input').val()
                        ,wx_channelid:$('.layui-form .layui-form-item').eq(8).find('input').val()
                        ,ali_pid:$('.layui-form .layui-form-item').eq(9).find('input').val()
                    },function(res){
                        if(res.status == 1){
                            layer.msg(res.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 2000
                            },function(){
                                window.location.reload();
                            });
                        }else{
                            layer.alert(res.message, {icon: 2});
                        }
                    },'json')
            });

            $('.no').on('click',function(){
                $('#layui-layer-shade1').css('opacity','0');
                $('#edit_rate').css('display', 'none');
            });

        });

        $('.submits').click(function(){
            $('#edit_rate').css('display','none')
        });

    });

</script>

</body>
</html>
