<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>商户交易返现详情</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
        .layui-card-header {
            width: 80px;
            text-align: right;
            float: left;
        }

        .layui-card-body {
            margin-left: 28px;
        }

        .layui-upload-img {
            width: 92px;
            height: 92px;
            margin: 0 10px 10px 0;
        }

        .up {
            position: relative;
            display: inline-block;
            cursor: pointer;
            border-color: #1ab394;
            color: #FFF;
            width: 92px !important;
            font-size: 10px !important;
            text-align: center !important;
        }

        .up input {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            opacity: .01;
            width: 100px;
            height: 30px;
        }

        .layui-upload-list {
            width: 100px;
            height: 96px;
            overflow: hidden;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none !important;
            margin: 0;
        }

        .width {
            width: 60%;
            float: left;
        }

        p {
            float: left;
            line-height: 36px;
            margin-left: 10px;
        }

        .userbox, .storebox {
            height: 200px;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 38px;
            width: 298px;
            background-color: #ffffff;
            border: 1px solid #ddd;
        }

        .userbox .list, .storebox .list {
            height: 38px;
            line-height: 38px;
            cursor: pointer;
            padding-left: 10px;
        }

        .userbox .list:hover, .storebox .list:hover {
            background-color: #eeeeee;
        }

        .s_id {
            line-height: 36px;
        }

        /*.layui-form-label{*/
        /*    width: 140px;*/
        /*}*/
        .layui-form-label-value {
            float: left;
            display: block;
            padding: 9px;
            font-weight: 400;
            line-height: 20px;
            text-align: left;
            width: 165px;
        }

        .layui-form-label-time {
            float: left;
            display: block;
            padding: 9px;
            font-weight: 400;
            line-height: 20px;
            text-align: center;
            width: 20px;
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card" style="margin-top:0px">
        <div class="layui-card-header" style="width:auto !important">交易返现规则详情<span class="zong_school_name"></span>
        </div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">

                <div class="layui-form-item">
                    <label class="layui-form-label">规则名称:</label>
                    <label class="layui-form-label-value rule_name"></label>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">活动时间:</label>
                    <label class="layui-form-label-value start_time" style="width: 140px"></label>
                    <label class="layui-form-label-time">—</label>
                    <label class="layui-form-label-value end_time"></label>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">商户名称:</label>
                    <label class="layui-form-label-value store_name" style="width: 500px"></label>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">所属代理商:</label>
                    <label class="layui-form-label-value agent_name" style="width: 500px"></label>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="width: 90px;margin-left: -10px">商户入网时间:</label>
                    <label class="layui-form-label-value store_bind_time"></label>
                </div>
            </div>
        </div>

        <div class="layui-card-header" style="width:auto !important;margin-top: 20px;text-align: left">激活标准规则
            <hr style="margin-top: -5px;width: 1650px;">
        </div>
        <div class="layui-form" lay-filter="component-form-group" style="margin-top: 100px;margin-left: 50px">
            <div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">激活检测时间区间:</label>
                    <label class="layui-form-label-value store_bind_time" style="width: 140px"></label>
                    <label class="layui-form-label-time">—</label>
                    <label class="layui-form-label-value standard_end_time"></label>
                </div>

            </div>
            <div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">激活状态:</label>
                    <label class="layui-form-label-value standard_status"></label>
                </div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">激活时间:</label>
                    <label class="layui-form-label-value standard_time"></label>
                </div>
            </div>
            <div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">激活需交易笔数:</label>
                    <label class="layui-form-label-value standard_total"></label>
                </div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">已交易笔数:</label>
                    <label class="layui-form-label-value standard_done_total"></label>
                </div>
            </div>
            <div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">激活奖励金额:</label>
                    <label class="layui-form-label-value standard_amt"></label>
                </div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">已获得激活奖励金额:</label>
                    <label class="layui-form-label-value standard_done_amt"></label>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">激活单笔交易金额:</label>
                <label class="layui-form-label-value standard_single_amt"></label>
            </div>
        </div>
        <div class="layui-card-header" style="width:auto !important;margin-top: 20px;text-align: left">T+1月奖励标准
            <hr style="margin-top: -5px;width: 1650px;">
        </div>
        <div class="layui-form" lay-filter="component-form-group" style="margin-top: 100px;margin-left: 100px">
            <div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">T+1月检测时间区间:</label>
                    <label class="layui-form-label-value first_start_time" style="width: 140px"></label>
                    <label class="layui-form-label-time">—</label>
                    <label class="layui-form-label-value first_end_time"></label>
                </div>
            </div>
            <div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">T+1月奖励状态:</label>
                    <label class="layui-form-label-value first_status"></label>
                </div>
            </div>
            <div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">T+1月需交易笔数:</label>
                    <label class="layui-form-label-value first_total"></label>
                </div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">已交易笔数:</label>
                    <label class="layui-form-label-value first_done_total"></label>
                </div>
            </div>
            <div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">T+1月需交易总金额:</label>
                    <label class="layui-form-label-value first_single_amt"></label>
                </div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">已交易总金额:</label>
                    <label class="layui-form-label-value first_total_amt"></label>
                </div>
            </div>
            <div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">T+1月奖励金额:</label>
                    <label class="layui-form-label-value first_amt"></label>
                </div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 150px">已获得T+1月奖励金额:</label>
                    <label class="layui-form-label-value first_done_amt"></label>
                </div>
            </div>


        </div>
        <div class="layui-card-header" style="width:auto !important;margin-top: 20px;text-align: left">T+2月奖励标准
            <hr style="margin-top: -5px;width: 1650px;">
        </div>
        <div class="layui-form" lay-filter="component-form-group" style="margin-top: 100px;margin-left: 100px">
            <div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">T+2月检测时间区间:</label>
                    <label class="layui-form-label-value again_start_time" style="width: 140px"></label>
                    <label class="layui-form-label-time">—</label>
                    <label class="layui-form-label-value again_end_time"></label>
                </div>

            </div>
            <div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">T+2月奖励状态:</label>
                    <label class="layui-form-label-value again_status"></label>
                </div>
            </div>
            <div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">T+2月需交易笔数:</label>
                    <label class="layui-form-label-value again_total"></label>
                </div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">已交易笔数:</label>
                    <label class="layui-form-label-value again_done_total"></label>
                </div>
            </div>
            <div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">T+2月需交易总金额:</label>
                    <label class="layui-form-label-value again_single_amt"></label>
                </div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">已交易总金额:</label>
                    <label class="layui-form-label-value again_total_amt"></label>
                </div>
            </div>
            <div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 140px">T+2月奖励金额:</label>
                    <label class="layui-form-label-value again_amt"></label>
                </div>
                <div class="layui-form-item" style="display: inline-block">
                    <label class="layui-form-label" style="width: 150px">已获得T+2月奖励金额:</label>
                    <label class="layui-form-label-value again_done_amt"></label>
                </div>
            </div>

        </div>

    </div>
</div>


<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var store_id = sessionStorage.getItem("store_id");
    // var str=location.search;
    // var school_name=str.split('?')[1];

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form', 'upload', 'formSelects', 'element', 'laydate'], function () {
        var $ = layui.$
            , admin = layui.admin
            , element = layui.element
            , layer = layui.layer
            , laydate = layui.laydate
            , form = layui.form
            , upload = layui.upload
            , formSelects = layui.formSelects;

        var src = $('#demo1').attr('src');
        element.render();
        // 未登录,跳转登录页面
        $(document).ready(function () {
            if (token == null) {
                window.location.href = "{{url('/user/login')}}";

            } else {
                $.ajax({
                    url: "{{url('/api/user/get_store_transaction_reward')}}",
                    data: {token: token, store_id: store_id},
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        $('.rule_name').text(data.data.rule_name)
                        $('.start_time').text(data.data.start_time)
                        $('.end_time').text(data.data.end_time)
                        $('.store_name').text(data.data.store_name)
                        $('.agent_name').text(data.data.agent_name)
                        $('.store_bind_time').text(data.data.store_bind_time)
                        $('.standard_end_time').text(data.data.standard_end_time)
                        $('.standard_amt').text(data.data.standard_amt)
                        $('.standard_done_amt').text(data.data.standard_done_amt)
                        $('.standard_single_amt').text(data.data.standard_single_amt)
                        $('.standard_total').text(data.data.standard_total)
                        $('.standard_done_total').text(data.data.standard_done_total)
                        if (data.data.standard_time != null) {
                            $('.standard_time').text(data.data.standard_time)
                        }

                        if (data.data.standard_status == "1") {
                            $('.standard_status').text("达标")
                            $(".standard_status").css("color", "#0bb20c");
                            $(".standard_done_amt").css("color", "#0bb20c");
                            $(".standard_done_total").css("color", "#0bb20c");
                        } else {
                            $('.standard_status').text("未达标")
                            $(".standard_status").css("color", "#bd2130");
                            $(".standard_done_amt").css("color", "#bd2130");
                            $(".standard_done_total").css("color", "#bd2130");
                        }
                        $('.first_start_time').text(data.data.first_start_time)
                        $('.first_end_time').text(data.data.first_end_time)
                        $('.first_single_amt').text(data.data.first_single_amt)
                        $('.first_total').text(data.data.first_total)
                        $('.first_total_amt').text(data.data.first_total_amt)
                        $('.first_done_amt').text(data.data.first_done_amt)
                        $('.first_done_total').text(data.data.first_done_total)
                        $('.first_amt').text(data.data.first_amt)
                        if (data.data.first_status == "1") {
                            $('.first_status').text("达标")
                            $(".first_status").css("color", "#0bb20c");
                            $(".first_done_amt").css("color", "#0bb20c");
                            $(".first_done_total").css("color", "#0bb20c");
                            $(".first_total_amt").css("color", "#0bb20c");
                        } else {
                            $('.first_status').text("未达标")
                            $(".first_status").css("color", "#bd2130");
                            $(".first_done_amt").css("color", "#bd2130");
                            $(".first_done_total").css("color", "#bd2130");
                            $(".first_total_amt").css("color", "#bd2130");
                        }
                        $('.again_single_amt').text(data.data.again_single_amt)
                        $('.again_total_amt').text(data.data.again_total_amt)
                        $('.again_total').text(data.data.again_total)
                        $('.again_start_time').text(data.data.again_start_time)
                        $('.again_end_time').text(data.data.again_end_time)
                        $('.again_done_amt').text(data.data.again_done_amt)
                        $('.again_done_total').text(data.data.again_done_total)
                        $('.again_amt').text(data.data.again_amt)
                        if (data.data.again_status == "1") {
                            $('.again_status').text("达标")
                            $(".again_status").css("color", "#0bb20c");
                            $(".again_done_amt").css("color", "#0bb20c");
                            $(".again_done_total").css("color", "#0bb20c");
                            $(".again_total_amt").css("color", "#0bb20c");
                        } else {
                            $('.again_status').text("未达标")
                            $(".again_status").css("color", "#bd2130");
                            $(".again_done_amt").css("color", "#bd2130");
                            $(".again_done_total").css("color", "#bd2130");
                            $(".again_total_amt").css("color", "#bd2130");
                        }
                    },
                    error: function (data) {
                        alert('查找板块报错');
                    }
                });
            }

        });


    });

</script>

</body>
</html>
