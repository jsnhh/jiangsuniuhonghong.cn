<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>赏金日结算</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
        .edit{background-color: #ed9c3a;}
        .shenhe{background-color: #429488;}
        .see{background-color: #7cb717;}
        .tongbu{background-color: #4c9ef8;color:#fff;}
        .cur{color:#009688;}

        .userbox,.storebox{
            height:200px;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 63px;
            width:498px;
            background-color:#ffffff;
            border: 1px solid #ddd;
        }
        .userbox .list,.storebox .list{
            height:38px;line-height: 38px;cursor:pointer;
            padding-left:10px;
        }
        .userbox .list:hover,.storebox .list:hover{
            background-color:#eeeeee;
        }
        .s_id{
            line-height: 36px;
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">

            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12" style="margin-top:0px">
                        <div class="layui-card">
                            <div class="layui-card-header">赏金结算</div>

                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">



                                    <div class="layui-form" lay-filter="component-form-group" style="">
                                        <div class="layui-form-item ">
                                            <label class="layui-form-label" style="float:left;">开启日结算功能</label>
                                            <div class="layui-input-block" style="margin-left:0;width:400px;float:left;">
                                                <select name="agent" id="agent" lay-filter="agent" lay-search>
                                                    <option value="">选择结算对象</option>
                                                    <option value="1">开启</option>
                                                    <option value="2">关闭</option>
                                                </select>

                                            </div>
                                        </div>
                                    </div>



                                    <div class="layui-form-item">
                                        <label class="layui-form-label">税&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;点</label>
                                        <div class="layui-input-block"  style="margin-left:0;width:400px;float: left;line-height: 38px;">
                                            <input type="text" placeholder="请输入税点" autocomplete="off" class="layui-input rate" style="width: 94%;float: left;margin-right: 10px;">%
                                        </div>
                                    </div>
                                    <!-- 选择商户时显示 -->


                                    <div class="layui-form-item layui-layout-admin">
                                        <div class="layui-input-block">
                                            <div class="layui-footer" style="left: 0;">
                                                <button class="layui-btn submit site-demo-active" style="border-radius:5px" data-type="tabChange">确认结算</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<input type="hidden" class="source_type">
<input type="hidden" class="source_type_desc">
<input type="hidden" class="user_id">
<input type="hidden" class="store_id">
<input type="hidden" class="store_name">



<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var source = sessionStorage.getItem("source");
    var str=location.search;
    var id=str.split('?')[1];
    // var user_id=str.split('?')[1];
    var status="{{$_GET['status']}}";


    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate;
        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
            $('.hidden').hide()
            $('.hidden_store').hide()
        })
        // 选择门店
        $.ajax({
            url : "{{url('/api/wallet/source_type')}}",
            data : {token:token,source:source,l:100},
            type : 'post',
            success : function(data) {
                console.log(data);
                var optionStr = "";
                for(var i=0;i<data.data.length;i++){

                    optionStr += "<option value='" + data.data[i].source_type + "' > " + data.data[i].source_desc + "</option>";
                }
                $("#schooltype").append('<option value="">选择赏金来源</option>'+optionStr);
                layui.form.render('select');
            },
            error : function(data) {
                alert('查找板块报错');
            }
        });

        //加载页面参数
        $.post("{{url('/api/wallet/dayInfo')}}",
            {
                token:token,
                id:id
            },function(res){
//            console.log(res);
                $('.device').val(res.data.type);
                $('.user_id').val(res.data.id);
                $('.version').val(res.data.version);
                $('.rate').val(res.data.rate);
                var opts = document.getElementById("agent");
                var value = res.data.status;

                if(value !== ""){
                    for(var i = 0; i < opts.options.length; i++){
                        if(value == opts.options[i].value){
                            opts.options[i].selected = true;
                            // alert(opts.options[i].value);
                            layui.form.render();		//很重要，必须重新渲染，才能回显
                            break;
                        }
                    }
                }
            },"json");

        $('.submit').on('click', function(){
            $.post("{{url('/api/wallet/upSettlement')}}",
                {
                    token:token,
                    id:id,
                    status:$('#agent').val(),
                    rate:$('.rate').val(),
                },function(res){
//                console.log(res);
                    if(res.status==1){
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 3000
                        });
                    }else{
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 3000
                        });
                    }
                },"json");
        });


    });

</script>

</body>
</html>





