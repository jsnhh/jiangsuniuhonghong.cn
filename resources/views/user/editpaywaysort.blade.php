<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>收款顺序批量置顶</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/formSelects-v4.css')}}" media="all">
    <style>
        #demo1 img{width: 100%;height: 100%;}
        .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
        .img_box span{position: absolute;right:0;top:0;font-size: 30px;background: #fff;cursor: pointer;}
        .img_box2 span{position: absolute;right:0;top:0;font-size: 30px;background: #fff;cursor: pointer;}
        video{
            width:200px;
        }
        .lianjiecon div{
            width:100%;
            overflow: hidden;
            margin-bottom:20px;
        }
        .lianjiecon label{
            display: inline-block;
            float: left;
            width:10%;
            line-height: 36px;
        }
        .lianjiecon input{
            display: inline-block;
            float: left;
            width:90%;
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card" style="margin-top:0px;">
        <div class="layui-card-header">收款顺序批量置顶</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group"> 
                <div class="layui-col-xs12 layui-col-md12">
                    <div class="layui-col-xs5 layui-col-md5">
                        <label class="layui-label">门店列表</label>
                        <div class="layui-input-block" style="margin-left:0; width:343px;">
                            <select name="store" id="store" xm-select="store" xm-select-search="" lay-tools="">
                            </select>
                        </div>
                    </div>

                    <div class="layui-col-xs5 layui-col-md5">
                        <div class="layui-form-item">
                            <label class="layui-label" style="margin-left: 110px">收款方式列表</label>
                            <div class="layui-input-block">
                                <select name="pay_ways" id="pay_ways" lay-filter="pay_ways">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit" style="border-radius:5px;">确定提交</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var store_key_ids = sessionStorage.getItem("store_key_ids");
    var str=location.search;

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','formSelects'], function(){
        var $ = layui.$
        // ,admin = layui.admin
        // ,element = layui.element
        // ,layer = layui.layer
        // ,form = layui.form
        ,formSelects = layui.formSelects;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token == null){
                window.location.href="{{url('/user/login')}}";
            }
        });

        formSelects.render('store');
        formSelects.btns('store', []);
        var arrs=[];

        // 门店                
        formSelects.config('store', {
            beforeSuccess: function(id, url, searchVal, result){
                //我要把数据外层的code, msg, data去掉
                
                result = result.data;
                // console.log(result);
                if(result!=''){
                    for(var i=0; i<result.length; i++){
                        var data ={"value":result[i].store_id,"name":result[i].store_name};
                        arrs.push(data);
                    } 
                    //然后返回数据
                    return arrs;
                }else{
                    $('#store').html('');
                }
                // console.log(arr);
            },
            success: function(id, url, searchVal, result){  //使用远程方式的success回调
                var stuclass=store_key_ids.split(','); //转换数组
                formSelects.value('store', stuclass);
                // console.log(stuclass);
            }
            // clearInput: true
        }).data('store', 'server', {
            url:"{{url('/api/user/store_lists?token=')}}"+token
        });

        //收款方式列表
        $.ajax({
            url : "{{url('/api/user/pay_ways_all_new')}}",
            data : {
                token:token
                // ,l:100
            },
            type : 'post',
            datatype:'json',
            success : function(data) {
                var optionStr = "";
                for(var i=0; i<data.data.length; i++){
                    optionStr += "<option value='" + data.data[i].ways_type + "'>" + data.data[i].ways_desc + "</option>";
                }
                $("#pay_ways").append('<option value="">请选择收款方式</option>'+optionStr);
                layui.form.render('select');
            },
            error : function(data) {
                alert('选择收款方式报错');
            }
        });

        $('.submit').on('click', function(){
            //拼接store_id数组
            var storeArr = [];
            $($("span[fsw$='xm-select']")).each(function(index, item){
                // console.log(item)
                var stores_id = $(item).attr('value');
                var data = {
                    "store_id":stores_id
                }; //构造数组
                // console.log(data);
                    storeArr.push(data);
            });
            // console.log(storeArr);
            var storeArrJson = JSON.stringify(storeArr); //转化成json格式

            //修改收款排序
            $.post("{{url('api/user/pay_ways_sort_top')}}",
            {
                token:token
                ,store_ids:storeArrJson
                ,ways_type:$("#pay_ways").val()
            },function(res){
                // console.log(res);
            });
        });

    });
</script>
</body>
</html>
