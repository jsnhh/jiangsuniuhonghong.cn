<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>电子协议</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
    <style>

        .personDis {
            display: none;
        }

        .companyDis {
            display: none;
        }

        .filebox input[type="file"] {
            position: absolute;
            left: 0;
            top: 0;
            display: block;
            opacity: .01;
            width: 100px;
            height: 30px;
        }

        .banksclass ul li {
            font-size: 14px;
            padding: 10px 6px;
            border-bottom: 1px solid #eeeeee;
            background-color: #ffffff;
            cursor: pointer
        }
        .banksclass2 ul li {
            font-size: 14px;
            padding: 10px 6px;
            border-bottom: 1px solid #eeeeee;
            background-color: #ffffff;
            cursor: pointer
        }


        .up input[type=file] {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            opacity: .01;
            width: 100px;
            height: 30px;
        }

        .athorize {
            margin-top: 20px;
            width: 50%;
            height: 40px;
            line-height: 40px;
            text-align: center;
            font-size: 14px;
            color: #fff;
            background-color: #00a3fe;
        }

        .athorize:hover {
            cursor: pointer;
        }

        .button {
            margin: 30px auto;
            width: 83%;
        }

        #steps {
            padding: 30px 0 0 260px;
        }

        .layui-form-label {
            display: block;
            padding-bottom: 8px;
        }

        p {
            width: 50%;
            margin-left: 111px;
            height: 22px;
            font-size: 14px;
            font-weight: 400;
            color: #999999;
            line-height: 22px;
        }

        input {
            width: 50%;
        }

        .disabled{cursor: not-allowed; }
        .uploadDataImg{display: none;}
        .licenseDataImg{display: none;}

        .selectoptioncolor input::-webkit-input-placeholder {
            color: #000000;
        }
        .selectoptioncolor input:-moz-placeholder{
            color: #000000;
        }
        .selectoptioncolor input::-moz-placeholder {
            color: #000000;
        }
        .selectoptioncolor input:-ms-input-placeholder {
            color: #000000;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
        }

        input[type="number"] {
            -moz-appearance: textfield;
        }

        .termModelbox ul li {
            margin: 10px 10px 10px;
            border-bottom: 1px solid #eeeeee;
            padding-bottom: 10px;
            font-size: 12px;
            cursor: pointer;
        }

    </style>
</head>
<body>
<div class="layui-fluid" style="background-color: #ffffff;">

    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card" style="margin-top: 0px;overflow:hidden;">
                <div class="layui-card-header">电子协议</div>

                <div id="steps"></div>
                <div class="layui-tab-content">
                    <!-- 开户 -->
                    <div class="layui-tab-item layui-show" id="openContract" style="margin-left: 24%; margin-top:50px;display:none !important;">
                        <form class="layui-form" action="" style="margin: 0px auto;width:auto;">
                            <div class="layui-row">
                                <div class="grid-demo">

                                    <div class="layui-form-item" style="width:500px">
                                        <label class="layui-form-label" style="text-align:center;"><span
                                                    style="color: red;margin-right: 4px;">*</span>开户类型</label>
                                        <div class="layui-input-block">
                                            <select name="userType" id="userType" lay-filter="userType">
                                                <option value="">请选择</option>
                                                <option value="3001">个人开户</option>
                                                <option value="3002">企业开户</option>
                                                {{--                            <option value="3003">商户开户</option>--}}
                                            </select>
                                        </div>
                                    </div>

                                    <div class="layui-form-item personNoneName personDis" style="width:500px">
                                        <label class="layui-form-label" style="text-align:center;"><span
                                                    style="color: red;margin-right: 4px;">*</span>法人姓名</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="personName" placeholder="法人姓名" class="layui-input item3">
                                        </div>
                                    </div>

                                    <div class="layui-form-item termModeNoneName companyDis" style="width:500px">
                                        <label class="layui-form-label" style="text-align:center;"><span
                                                    style="color: red;margin-right: 4px;">*</span>企业名称</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="enterpriseName" placeholder="企业名称" class="layui-input item3">
                                        </div>
                                    </div>

                                    <div class="layui-form-item" style="width:500px">
                                        <label class="layui-form-label" style="text-align:center;"><span
                                                    style="color: red;margin-right: 4px;">*</span>证件类型</label>
                                        <div class="layui-input-block">
                                            <select name="identTypeCode" id="identTypeCode" lay-filter="identTypeCode">
                                                <option value="">请选择</option>
                                                <option value="0">身份证</option>
                                                <option value="1">护照</option>
                                                <option value="8">企业营业执照</option>
                                                <option value="B">港澳居民往来内地通行证</option>
                                                <option value="C">台湾居民来往大陆通行证</option>
                                                <option value="H">事业单位法人证书</option>
                                                <option value="J">社会团体登记证书</option>
                                                <option value="N">企业统一社会信用代码</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="layui-form-item " style="width:500px">
                                        <label class="layui-form-label" style="text-align:center;"><span
                                                    style="color: red;margin-right: 4px;">*</span>证件号码</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="identNo" placeholder="证件号码" class="layui-input item3">
                                        </div>
                                    </div>


                                    <div class="layui-form-item" style="width:500px">
                                        <label class="layui-form-label" style="text-align:center;"><span
                                                    style="color: red;margin-right: 4px;">*</span>手机号</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="mobilePhone" placeholder="手机号" maxlength="11" minlength="11" class="layui-input item3">
                                        </div>
                                    </div>

                                    <!--   --->
                                    <div class="layui-form-item termModeNoneName companyDis" style="width:500px">
                                        <label class="layui-form-label" style="text-align:center;"><span
                                                    style="color: red;margin-right: 4px;">*</span>企业联系电话</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="landlinePhone" placeholder="企业联系电话" class="layui-input item3">
                                        </div>
                                    </div>

                                    <!--   -->
                                    <div class="layui-form-item termModeNoneName companyDis" style="width:500px">
                                        <label class="layui-form-label" style="text-align:center;"><span
                                                    style="color: red;margin-right: 4px;">*</span>经办人名称</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="transactorName" placeholder="经办人名称" class="layui-input item3">
                                        </div>
                                    </div>
                                    <div class="layui-form-item termModeNoneName companyDis" style="width:500px">
                                        <label class="layui-form-label" style="text-align:center;"><span
                                                    style="color: red;margin-right: 4px;">*</span>证件类型</label>
                                        <div class="layui-input-block">
                                            <select name="transactorIdentTypeCode" id="transactorIdentTypeCode" lay-filter="transactorIdentTypeCode">
                                                <option value="">请选择</option>
                                                <option value="0">身份证</option>
                                                <option value="1">护照</option>
                                                <option value="8">企业营业执照</option>
                                                <option value="B">港澳居民往来内地通行证</option>
                                                <option value="C">台湾居民来往大陆通行证</option>
                                                <option value="H">事业单位法人证书</option>
                                                <option value="J">社会团体登记证书</option>
                                                <option value="N">企业统一社会信用代码</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-form-item termModeNoneName companyDis" style="width:500px">
                                        <label class="layui-form-label" style="text-align:center;"><span
                                                    style="color: red;margin-right: 4px;">*</span>证件号码</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="transactorIdentNo" placeholder="证件号码" class="layui-input item3">
                                        </div>
                                    </div>
                                    <div class="layui-form-item termModeNoneName companyDis" style="width:500px">
                                        <label class="layui-form-label" style="text-align:center;">地址</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="address" placeholder="地址" class="layui-input item3">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div style="margin-left: 24%;">
                                <button type="submit" class="layui-btn" lay-submit lay-filter="saveContract"
                                        style="background: #1E9FFF;margin-top: 15px;float: left;">确定开户
                                </button>
                            </div>

                        </form>
                    </div>

                    <!-- 填写合同信息 + 发送验证码 -->
                    <div id="stepTwobox" style="display:none !important;">
                        <div class="layui-tab-item layui-show" id="stepTwo" style="margin-left: 24%; margin-right: 20%;margin-top:25px;">

                            <form class="layui-form" action="" lay-filter="example" style="margin: 0px auto;width:auto;">
                                <div class="layui-row">
                                    <div class="grid-demo">

                                        <div class="layui-form-item " style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;"><span
                                                        style="color: red;margin-right: 4px;">*</span>填表日期</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="contractDate" class="layui-input contractDate item3" placeholder="填表日期">
                                            </div>
                                        </div>

                                        <div class="layui-form-item " style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center; width: 120px"><span
                                                        style="color: red;margin-right: 4px;">*</span>工商注册名称</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="merName" placeholder="工商注册名称" class="layui-input item3">
                                            </div>
                                        </div>
                                        <div class="layui-form-item " style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;">实际经营名称</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="bizName" placeholder="实际经营名称" class="layui-input item3">
                                            </div>
                                        </div>
                                        <div class="layui-form-item " style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;"><span
                                                        style="color: red;margin-right: 4px;">*</span>统一社会信用代码</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="businLic" placeholder="统一社会信用代码" class="layui-input item3">
                                            </div>
                                        </div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;"><span
                                                        style="color: red;margin-right: 4px;">*</span>注册地址</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 420px;">
                                                <input type="text" name="merAddr" placeholder="注册地址"  class="layui-input item3">
                                            </div>
                                        </div>

                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;">实际经营地址</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 420px;">
                                                <input type="text" name="bizAddr" placeholder="实际经营地址"  class="layui-input item3">
                                            </div>
                                        </div>

                                        <div class="layui-card-header">法定代表人</div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;"><span
                                                        style="color: red;margin-right: 4px;">*</span>姓名</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="merLegal" placeholder="姓名"  class="layui-input item3">
                                            </div>
                                        </div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;"><span
                                                        style="color: red;margin-right: 4px;">*</span>身份证号</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="legalCode" placeholder="身份证号"  class="layui-input item3">
                                            </div>
                                        </div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;"><span
                                                        style="color: red;margin-right: 4px;">*</span>联系电话</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="legalPhone" placeholder="联系电话" maxlength="11" minlength="11" class="layui-input item3">
                                            </div>
                                        </div>

                                        <div class="layui-card-header">联系人</div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;"><span
                                                        style="color: red;margin-right: 4px;">*</span>姓名</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="linkMan" placeholder="联系人姓名"  class="layui-input item3">
                                            </div>
                                        </div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;"><span
                                                        style="color: red;margin-right: 4px;">*</span>电话</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="linkmanPhone" placeholder="联系人电话" maxlength="11" minlength="11" class="layui-input item3">
                                            </div>
                                        </div>
                                        <div class="layui-card-header">清算账户</div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;"><span
                                                        style="color: red;margin-right: 4px;">*</span>账户名称</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="accName" placeholder="清算账户名称"  class="layui-input item3">
                                            </div>
                                        </div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;"><span
                                                        style="color: red;margin-right: 4px;">*</span>开户行</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="bankName" placeholder="清算账户开户行"  class="layui-input item3">
                                            </div>
                                        </div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;"><span
                                                        style="color: red;margin-right: 4px;">*</span>账号</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="account" placeholder="清算账户账号"  class="layui-input item3">
                                            </div>
                                        </div>

                                        <div class="layui-card-header">银联卡支付-卡支付</div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;">借记卡</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="chargeTypeDebit" placeholder="%"  class="layui-input item3">
                                            </div>
                                        </div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">

                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="chargeTypeDebitMax" placeholder="封顶 ： 元"  class="layui-input item3">
                                            </div>
                                        </div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;">贷记卡</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="chargeTypeCredit" placeholder="%"  class="layui-input item3">
                                            </div>
                                        </div>


                                        <div class="layui-card-header">银联卡支付-银联二维码（单笔交易金额1000元以上）</div>
                                        <div class="layui-form-item" style="margin-right:10px;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;">同卡支付</label>
                                            <div class="layui-input-block">
                                                <input type="checkbox" name="checkSameCard" lay-skin="switch" lay-filter="checkSameCard" class="layui-input item3">
                                            </div>
                                        </div>

                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;">借记卡</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="chargeTypeUnionpayDebit" placeholder="%"  class="layui-input item3">
                                            </div>
                                        </div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">

                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="chargeTypeUnionpayDebitMax" placeholder="封顶 ： 元"  class="layui-input item3">
                                            </div>
                                        </div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;">贷记卡</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="chargeTypeUnionpayCredit" placeholder="%"  class="layui-input item3">
                                            </div>
                                        </div>

                                        <div class="layui-card-header">银联卡支付-银联二维码（单笔交易金额1000元（含）以下）</div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;">借记卡</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="chargeTypeUnionpayDisDebit" placeholder="%" class="layui-input item3">
                                            </div>
                                        </div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">

                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="chargeTypeUnionpayDisDebitMax" placeholder="封顶 ： 元"  class="layui-input item3">
                                            </div>
                                        </div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;">贷记卡</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="chargeTypeUnionpayDisCredit" placeholder="%" class="layui-input item3">
                                            </div>
                                        </div>

                                        <div class="layui-card-header">条码支付</div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;">微信支付</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="chargeTypeWxpay" placeholder="%" class="layui-input item3">
                                            </div>
                                        </div>

                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;">支付宝支付</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="chargeTypeAlipay" placeholder="%" class="layui-input item3">
                                            </div>
                                        </div>

                                        <div class="layui-card-header">清算时间</div>

                                        <div class="layui-form-item" style="margin-right:10px;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;">T1到账</label>
                                            <div class="layui-input-block">
                                                <input type="checkbox" name="checkSettleT1" lay-skin="switch" lay-filter="checkSettleT1" class="layui-input item3">
                                            </div>
                                        </div>
                                        <div class="layui-form-item" style="margin-right:10px;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;">D1到账</label>
                                            <div class="layui-input-block">
                                                <input type="checkbox" name="checkSettleD1" lay-skin="switch" lay-filter="checkSettleD1" class="layui-input item3">
                                            </div>
                                        </div>
                                        <div class="layui-form-item" style="margin-right:10px;margin-left: 120px;">

                                            <div class="layui-input-block" style="margin-right:10px;display: inline-block;">
                                                <input type="checkbox" name="checkSettleD1Ratio" lay-skin="switch" lay-filter="checkSettleD1Ratio" class="layui-input item3">
                                            </div>
                                            <div class="layui-input-block" style="width: 220px;margin-left: 30px;margin-right:10px;display: inline-block;">
                                                <input type="text" name="d1SettlementRatio" placeholder="%" class="layui-input item3">
                                            </div>
                                        </div>


                                        <div class="layui-form-item" style="margin-right:10px;margin-left:120px;display: inline-block;">

                                            <div class="layui-input-block" style="margin-right:10px;display: inline-block;">
                                                <input type="checkbox" name="checkSettleD1Fee" lay-skin="switch" lay-filter="checkSettleD1Fee" class="layui-input item3">
                                            </div>
                                            <div class="layui-input-block"  style="width: 220px;margin-left: 30px;margin-right:10px;display: inline-block;">
                                                <input type="text" name="d1SettlementFee" placeholder="元/笔" class="layui-input item3">
                                            </div>
                                        </div>


                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;">即时到账</label>
                                            <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                                <input type="checkbox" name="checkSettleD0" lay-skin="switch" lay-filter="checkSettleD0" class="layui-input item3">
                                            </div>
                                            <div class="layui-form-item" style="width: 220px;margin-right:10px;display: inline-block;">
                                                <input type="text" name="d0SettlementRatio" placeholder="%" class="layui-input item3">
                                            </div>

                                        </div>

                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;"></label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">

                                            </div>
                                        </div>


                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;">客户经理</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="manager" placeholder="客户经理" class="layui-input item3">
                                            </div>
                                        </div>
                                        <div class="layui-form-item" style="margin-right:10px;display: inline-block;">
                                            <label class="layui-form-label" style="text-align:center;width: 120px;">联系方式</label>
                                            <div class="layui-input-block" style="margin-left: 150px;width: 220px;">
                                                <input type="text" name="managerPhone" placeholder="联系方式" class="layui-input item3">
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div style="margin-left: 24%;margin-top:20px;" id="showContract">
                                    <button type="submit" class="layui-btn" lay-submit lay-filter="addContract"
                                            style="background: #1E9FFF;margin-top: 15px;float: left;">添加资料
                                    </button>
                                </div>

                            </form>

                            <div class="button" id="showSendSms" style="margin-left: 24%;margin-top:20px;display:none !important;">
                                <button type="button" class="layui-btn"  lay-submit="" lay-filter="sendSms" style="background: #1E9FFF;">发送短信验证码</button>
                            </div>



                        </div>
                    </div>

                    <!-- 创建合同 -->
                    <div class="layui-tab-item layui-show" id="stepThree" style="margin-left: 30%; margin-right: 20%; margin-top:50px;display:none !important;">


                        <form class="layui-form" action="" lay-filter="exampleSms" style="margin: 0px auto;width:auto;">
                            <div class="grid-demo">
                                <div class="layui-form-item " style="width:500px;">
                                    <label class="layui-form-label" style="text-align:center;"><span
                                                style="color: red;margin-right: 4px;">*</span>短信验证码</label>
                                    <div class="layui-input-block">
                                        <input type="text" id="smsCode" name="smsCode" placeholder="请输入短信验证码" class="layui-input item3">
                                    </div>
                                </div>
                            </div>

                        </form>

                        <div style="margin-left: 24%;">
                            <button type="button" class="layui-btn" lay-submit="" lay-filter="createContract"
                                    style="background: #1E9FFF;margin-right:12px;border-radius: 5px;">创建合同</button>
                        </div>


                    </div>

                    <!-- 签署合同 -->
                    <div class="layui-tab-item layui-form" id="stepFour" style="margin-left: 24%; margin-top:50px;display:none !important;">

                        <div class="layui-tab-item layui-show" id="stepFourContext" style="display:none !important;">

                            <div class="layui-form-item" style="width:500px">
                                <label class="layui-form-label" style="width: 120px"><span
                                            style="color: red;margin-right: 4px;">*</span>电子协议编号</label>
                                <div class="layui-input-block" style="margin-left: 160px;">
                                    <input type="text" name="contractNo"  id="contractNo" class="layui-input" disabled>
                                </div>
                            </div>

                            <div class="layui-form-item" style="width:500px">
                                <label class="layui-form-label" style="width: 120px"><span
                                            style="color: red;margin-right: 4px;">*</span>签署方名称</label>
                                <div class="layui-input-block" style="margin-left: 160px;">
                                    <input type="text" name="signatoryName"  id="signatoryName"  class="layui-input" disabled>
                                </div>
                            </div>


                            <div class="layui-form-item" style=" margin-left: 20%;margin-top: 50px;">

                                <button type="button" class="layui-btn signContractDownload"
                                        style="background: #1E9FFF;margin-right:12px;border-radius: 5px;">下载电子协议</button>
                            </div>


                        </div>


                        <div class="layui-tab-item layui-show" id="stepFourButton" style="margin-left: 30%; margin-top:50px;display:none !important;">

                            <button type="button" class="layui-btn signContract"
                                    style="background: #1E9FFF;margin-right:12px;border-radius: 5px;">签署合同</button>
                        </div>

                    </div>


                </div>


            </div>
        </div>
    </div>

</div>

<input type="hidden" class="identTypeCode" value="">
<input type="hidden" class="transactorIdentTypeCode" value="">

<input type="hidden" class="checkSameCard" value="">
<input type="hidden" class="checkSettleT1" value="">
<input type="hidden" class="checkSettleD1" value="">
<input type="hidden" class="checkSettleD1Ratio" value="">
<input type="hidden" class="checkSettleD1Fee" value="">
<input type="hidden" class="checkSettleD0" value="">

<script src="/layuiadmin/layui/layui.js"></script>
<script>
    //获取 token ，用户登录是就会有
    var token = window.sessionStorage.getItem("Usertoken");
    var storeid = window.location.search;
    var store_id = storeid.substring(1, storeid.length)

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        steps: './extends/steps/steps',
        formSelects: 'formSelects'
    }).use(['index', 'form', 'upload', 'formSelects', 'laydate', 'jquery', 'steps'], function () {
        var $ = layui.$
            , admin = layui.admin
            , element = layui.element
            , layer = layui.layer
            , laydate = layui.laydate
            , form = layui.form
            , upload = layui.upload
            , steps = layui.steps
            , formSelects = layui.formSelects;
        var $ = jquery = layui.jquery;
        // 未登录,跳转登录页面
        $(document).ready(function () {
            if (token == null) {
                window.location.href = "{{url('/user/login')}}";
            }
        });

        //步骤条
        layui.use('steps', function() {
            var steps = layui.steps;
            steps.render({
                ele: '#steps',
                data: [
                    {
                        'title': "第一步",
                        "desc": "开户"
                    },
                    {
                        'title': "第二步",
                        "desc": "填写合同信息"
                    },
                    {
                        'title': "第三步",
                        "desc": "创建合同"
                    },
                    {
                        'title': "第四步",
                        "desc": "签署合同"
                    }
                ],
                // current: 0 //默认为第几步
            });

            /*
             * 获取第几步的状态
             */

            $.ajax({
                url: "{{url('/api/user/queryContract')}}",
                data: {
                    token:token,
                    storeId: store_id
                },
                type: 'post',
                success: function(data) {
                    let stepspage = data.data.created_step;
                    let contractInfo = data.data.contractInfo;
                    //不等于1的时候才会下一步
                    if(stepspage !== 1){

                        if(stepspage == 2){
                            $("#openContract").css("cssText", "display:none !important;");
                            $("#stepTwobox").show();

                            //判断是否填写合同信息
                            if(contractInfo == null){
                                $("#showContract").show();
                                $("#showSendSms").css("cssText", "display:none !important;");
                            }else{
                                //数据回显
                                EchoContractInfo();
                                $("#showContract").css("cssText", "display:none !important;");
                                $("#showSendSms").show();
                            }

                        }else if(stepspage == 3){
                            $("#openContract").css("cssText", "display:none !important;");
                            $("#stepTwobox").css("cssText", "display:none !important;");
                            $("#stepThree").show();


                        }else if(stepspage == 4){
                            $("#openContract").css("cssText", "display:none !important;");
                            $("#stepTwobox").css("cssText", "display:none !important;");
                            $("#stepThree").css("cssText", "display:none !important;");
                            $("#stepFour").show();
                            //回显数据
                            EchoInfo();

                        }

                        stepspage --;
                        for(let i=0;i<stepspage;i++){
                            //执行下一步
                            steps.next();
                        }

                    }else{
                        $("#openContract").show();
                        $("#stepTwobox").css("cssText", "display:none !important;");
                        $("#stepThree").css("cssText", "display:none !important;");
                        $("#stepFour").css("cssText", "display:none !important;");

                    }

                }
            })

            /*
            * 第一步 开户
            */

            form.on('submit(saveContract)', function(data) {

                var value = data.field;

                if (!value.userType) {
                    layer.msg("开户类型不可为空", {
                        offset: '50px',
                        icon: 2,
                        time: 2000
                    });
                    return false;
                }

                if(value.userType == 3001){

                    if (!value.personName) {
                        layer.msg("法人姓名不可为空", {
                            offset: '50px',
                            icon: 2,
                            time: 2000
                        });
                        return false;
                    }

                }else if(value.txCode == 3002){

                    if (!value.enterpriseName) {
                        layer.msg("企业名称不可为空", {
                            offset: '50px',
                            icon: 2,
                            time: 2000
                        });
                        return false;
                    }
                    if (!value.landlinePhone) {
                        layer.msg("企业联系电话不可为空", {
                            offset: '50px',
                            icon: 2,
                            time: 2000
                        });
                        return false;
                    }
                    if (!value.transactorName) {
                        layer.msg("经办人名称不可为空", {
                            offset: '50px',
                            icon: 2,
                            time: 2000
                        });
                        return false;
                    }
                    if (!value.transactorIdentTypeCode) {
                        layer.msg("经办人证件类型不可为空", {
                            offset: '50px',
                            icon: 2,
                            time: 2000
                        });
                        return false;
                    }
                    if (!value.transactorIdentNo) {
                        layer.msg("经办人证件号码不可为空", {
                            offset: '50px',
                            icon: 2,
                            time: 2000
                        });
                        return false;
                    }

                }


                if (!value.identTypeCode) {
                    layer.msg("证件类型不可为空", {
                        offset: '50px',
                        icon: 2,
                        time: 2000
                    });
                    return false;
                }
                if (!value.identNo) {
                    layer.msg("证件号码不可为空", {
                        offset: '50px',
                        icon: 2,
                        time: 2000
                    });
                    return false;
                }
                if (!value.mobilePhone) {
                    layer.msg("手机号不可为空", {
                        offset: '50px',
                        icon: 2,
                        time: 2000
                    });
                    return false;
                }

                $.ajax({
                    url: "{{url('/api/user/easypayElectronic')}}",
                    data: {
                        token: token,
                        storeId: store_id,
                        txCode: value.userType,
                        personName: value.personName,
                        enterpriseName:value.enterpriseName,
                        identTypeCode: value.identTypeCode,
                        identNo: value.identNo,
                        mobilePhone: value.mobilePhone,
                        landlinePhone: value.landlinePhone,
                        transactorName: value.transactorName,
                        transactorIdentTypeCode: value.transactorIdentTypeCode,
                        transactorIdentNo: value.transactorIdentNo,
                        address: value.address,
                    },
                    type: "post",
                    success: function (data) {
                        if (data.status == 1) {
                            layer.msg(data.message, {
                                offset: '50px',
                                icon: 1,
                                time: 2000
                            });

                            $("#openContract").css("cssText", "display:none !important;");
                            $("#stepTwobox").show();
                            steps.next(); //下一步
                            document.getElementById("saveContract").style.display = "none";
                        }else{
                            layer.msg(data.message, {
                                offset: '50px',
                                icon: 1,
                                time: 2000
                            });
                        }
                    }

                })
                return false;
            })

            /*
            * 第二步 填写合同信息+短信验证
            */
            form.on('submit(addContract)', function(data) {

                var value = data.field;

                if (!value.contractDate) {
                    layer.msg("填表日期不可为空", {offset: '50px',icon: 2,time: 2000});
                    return false;
                }

                var dataArr = value.contractDate.split('-')
                var contractDateYear = dataArr[0];
                var contractDateMonth = dataArr[1];
                var contractDateDay = dataArr[2];

                if (!value.merName) {
                    layer.msg("工商注册名称不可为空", {offset: '50px',icon: 2,time: 2000});
                    return false;
                }
                if (!value.businLic) {
                    layer.msg("统一社会信用代码不可为空", {offset: '50px',icon: 2,time: 2000});
                    return false;
                }
                if (!value.merAddr) {
                    layer.msg("注册地址不可为空", {offset: '50px',icon: 2,time: 2000});
                    return false;
                }
                if (!value.merLegal) {
                    layer.msg("法定代表人姓名不可为空", {offset: '50px',icon: 2,time: 2000});
                    return false;
                }
                if (!value.legalCode) {
                    layer.msg("法定代表人身份证号不可为空", {offset: '50px',icon: 2,time: 2000});
                    return false;
                }
                if (!value.legalPhone) {
                    layer.msg("法定代表人联系电话不可为空", {offset: '50px',icon: 2,time: 2000});
                    return false;
                }
                if (!value.linkMan) {
                    layer.msg("联系人姓名不可为空", {offset: '50px',icon: 2,time: 2000});
                    return false;
                }
                if (!value.linkmanPhone) {
                    layer.msg("联系人电话不可为空", {offset: '50px',icon: 2,time: 2000});
                    return false;
                }
                if (!value.accName) {
                    layer.msg("账户名称不可为空", {offset: '50px',icon: 2,time: 2000});
                    return false;
                }
                if (!value.bankName) {
                    layer.msg("开户行不可为空", {offset: '50px',icon: 2,time: 2000});
                    return false;
                }
                if (!value.account) {
                    layer.msg("账号不可为空", {offset: '50px',icon: 2,time: 2000});
                    return false;
                }

                // let contractDateYear = value.contractDateYear.
                $.ajax({
                    url: "{{url('/api/user/addContractInfo')}}",
                    data: {
                        token: token,
                        storeId: store_id,
                        merName: value.merName,  // 商户名/注册名
                        merLegal: value.merLegal, //法人
                        merAddr: value.merAddr,  //商户地址/注册地址
                        contractDateYear: contractDateYear,    //合同日期 YYYY
                        contractDateMonth: contractDateMonth,
                        contractDateDay: contractDateDay,
                        bizName: value.bizName,  //经营名称
                        businLic: value.businLic,  //统一 社会信用代码
                        bizAddr: value.bizAddr,  //经营地址
                        legalCode: value.legalCode, //法人身份证号
                        legalPhone: value.legalPhone,  //法人电话
                        linkMan: value.linkMan,   //联系人
                        linkmanPhone: value.linkmanPhone, //联系人电话
                        accName: value.accName, //账号名称
                        bankName: value.bankName, //开户行
                        account: value.account,  //账号
                        chargeTypeDebit: value.chargeTypeDebit, //卡支付借记卡
                        chargeTypeDebitMax: value.chargeTypeDebitMax, //卡支付借记卡封顶值
                        chargeTypeCredit: value.chargeTypeCredit, //卡支付贷记卡
                        chargeTypeUnionpayDebit: value.chargeTypeUnionpayDebit, //银联二维码借记卡
                        chargeTypeUnionpayDebitMax: value.chargeTypeUnionpayDebitMax, //银联二维码借记卡封顶值
                        chargeTypeUnionpayCredit: value.chargeTypeUnionpayCredit, //银联二维码贷记卡
                        chargeTypeUnionpayDisDebit: value.chargeTypeUnionpayDisDebit, //银联营销借记卡
                        chargeTypeUnionpayDisDebitMax: value.chargeTypeUnionpayDisDebitMax, //银联营销借记卡封顶值
                        chargeTypeUnionpayDisCredit: value.chargeTypeUnionpayDisCredit, //银联营销贷记卡
                        chargeTypeWxpay: value.chargeTypeWxpay, //微信
                        chargeTypeAlipay: value.chargeTypeAlipay, //支付宝
                        d1SettlementRatio: value.d1SettlementRatio, //D1结算按比例
                        d1SettlementFee: value.d1SettlementFee, //D1结算按笔
                        d0SettlementRatio: value.d0SettlementRatio, //D0结算按比例
                        manager: value.manager,  //客户经理
                        managerPhone: value.managerPhone, //客户经理电话
                        contractDate: value.contractDate, //合同日期 YYYY年MM月DD日
                        checkSameCard:  $('.checkSameCard').val(), //同卡支付
                        checkSettleT1: $('.checkSettleT1').val(), //T1结算
                        checkSettleD1: $('.checkSettleD1').val(), //D1结算
                        checkSettleD1Ratio: $('.checkSettleD1Ratio').val(), //D1结算按比例
                        checkSettleD1Fee: $('.checkSettleD1Fee').val(), //D1结算按笔
                        checkSettleD0: $('.checkSettleD0').val(), //D0结算
                    },
                    type: "post",
                    success: function (data) {
                        if (data.status == 1) {
                            layer.msg(data.message, {
                                offset: '50px',
                                icon: 1,
                                time: 2000
                            });

                            $("#showContract").hide();
                            $("#showSendSms").show();

                            //回显数据
                            EchoContractInfo();

                        }else{
                            layer.msg(data.message, {
                                offset: '50px',
                                icon: 1,
                                time: 2000
                            });
                        }
                    }

                })

                return false;

            })

            form.on('submit(sendSms)', function() {
                $.ajax({
                    url: "{{url('/api/user/sendSms')}}",
                    data: {
                        token: token,
                        storeId: store_id,
                        txCode: 3101
                    },
                    type: 'post',
                    success: function(data) {

                        if (data.status == 1) {
                            $("#openContract").css("cssText", "display:none !important;");
                            $("#stepTwobox").css("cssText", "display:none !important;");
                            $("#stepThree").show();
                            steps.next(); //下一步

                            layer.msg("发送验证码成功", {
                                offset: '50px'
                                ,icon: 1
                                ,time: 2000
                            });

                        }else {
                            layer.msg(data.message, { offset: '140px', icon:2, shade:0.5, time:2000});
                        }
                    }
                })

            })

            /*
            * 第三步 创建合同
            */

            form.on('submit(createContract)', function(data) {
                $.ajax({
                    url: "{{url('/api/user/createContract')}}",
                    data: {
                        token: token,
                        storeId: store_id,
                        checkCode: $('#smsCode').val(),
                        txCode:3201
                    },
                    type: 'post',
                    success: function(data) {
                        if (data.status == 1 && data.data.contractState == 1) { //签署成功
                            $("#openContract").css("cssText", "display:none !important;");
                            $("#stepTwobox").css("cssText", "display:none !important;");
                            $("#stepThree").css("cssText", "display:none !important;");
                            $("#stepFourButton").css("cssText", "display:none !important;");

                            $("#stepFourContext").show();
                            $("#stepFour").show();
                            steps.next(); //下一步

                            layer.msg("创建合同成功", {
                                offset: '50px'
                                ,icon: 1
                                ,time: 2000
                            });

                        } else if (data.status == 1 && data.data.contractState == 0) {//未签署
                            $("#openContract").css("cssText", "display:none !important;");
                            $("#stepTwobox").css("cssText", "display:none !important;");
                            $("#stepThree").css("cssText", "display:none !important;");
                            $("#stepFourContext").css("cssText", "display:none !important;");

                            $("#stepFour").show();
                            $("#stepFourButton").show();
                            steps.next(); //下一步

                            layer.msg("创建合同成功", {
                                offset: '50px'
                                ,icon: 1
                                ,time: 2000
                            });
                        } else {
                            layer.msg(data.message, { offset: '140px', icon:2, shade:0.5, time:2000});
                        }
                    }
                })
            })


            /*
            * 第四步 签署合同
            */

            $(".signContract").on("click", function() {

                $.ajax({
                    url: "{{url('/api/user/signContract')}}",
                    data: {
                        token: token,
                        storeId: store_id,
                        txCode:3206
                    },
                    type: 'post',
                    success: function(data) {
                        if (data.status == 1) {
                            // $("#openContract").css("cssText", "display:none !important;");
                            // $("#stepTwobox").css("cssText", "display:none !important;");
                            // $("#stepThree").css("cssText", "display:none !important;");
                            // $("#stepFour").show();


                            layer.msg("签署成功", {
                                offset: '50px'
                                ,icon: 1
                                ,time: 2000
                            });

                            window.location.reload();

                        }else {
                            layer.msg(data.message, { offset: '140px', icon:2, shade:0.5, time:2000});
                        }
                    }
                })
            })


        });

        //下载电子协议
        $(".signContractDownload").on("click", function() {

            $.ajax({
                url: "{{url('/api/user/downloadContract')}}",
                data: {
                    token: token,
                    storeId: store_id
                },
                type: 'post',
                success: function(data) {
                    if (data.status == 1) {

                        //上面是把后台请求到的文件流进行转化为符合的流
                        const blob = convertBase64ToBlob(data.data, 'application/pdf', 1024)
                        var fileName = data.message + '.pdf';
                        if ('download' in document.createElement('a')) {
                            //非IE下载
                            const urlFromBlob = window.URL.createObjectURL(blob);
                            const a = document.createElement('a');
                            a.style.display = 'none';
                            a.href = urlFromBlob;
                            a.download = fileName;
                            document.body.appendChild(a);
                            a.click();
                            window.URL.revokeObjectURL(urlFromBlob);
                            document.body.removeChild(a)
                        } else {
                            navigator.msSaveBlob(blob, fileName);
                        }

                    }else {
                        layer.msg(data.message, { offset: '140px', icon:2, shade:0.5, time:2000});
                    }
                }
            })
        })


        // 监听 类型
        form.on("select(userType)", function (data) {
            if (data.value == 3001) {
                $(".personNoneName").removeClass("personDis");
                $(".termModeNoneName").addClass("companyDis");
            } else if (data.value == 3002) {
                $(".personNoneName").addClass("personDis");
                $(".termModeNoneName").removeClass("companyDis");
            } else {
                console('商户');
            }
        })

        // 监听 证件类型
        form.on("select(identTypeCode)", function (data) {
            $('.identTypeCode').val(data.value);
        })
        form.on("select(transactorIdentTypeCode)", function (data) {
            $('.transactorIdentTypeCode').val(data.value);
        })


        form.on("switch(checkSameCard)",(data)=>{
            let type = data.elem.checked;
            if(type){
                $('.checkSameCard').val("1");
            }else{
                $('.checkSameCard').val("0");
            }
        })
        form.on("switch(checkSettleT1)",(data)=>{
            let type = data.elem.checked;
            if(type){
                $('.checkSettleT1').val("1");
            }else{
                $('.checkSettleT1').val("0");
            }
        })
        form.on("switch(checkSettleD1)",(data)=>{
            let type = data.elem.checked;
            if(type){
                $('.checkSettleD1').val("1");
            }else{
                $('.checkSettleD1').val("0");
            }
        })
        form.on("switch(checkSettleD1Ratio)",(data)=>{
            let type = data.elem.checked;
            if(type){
                $('.checkSettleD1Ratio').val("1");
            }else{
                $('.checkSettleD1Ratio').val("0");
            }
        })
        form.on("switch(checkSettleD1Fee)",(data)=>{
            let type = data.elem.checked;
            if(type){
                $('.checkSettleD1Fee').val("1");
            }else{
                $('.checkSettleD1Fee').val("0");
            }
        })
        form.on("switch(checkSettleD0)",(data)=>{
            let type = data.elem.checked;
            if(type){
                $('.checkSettleD0').val("1");
            }else{
                $('.checkSettleD0').val("0");
            }
        })


        function EchoContractInfo(){
            //数据回显
            $.ajax({
                url: "{{url('/api/user/queryContract')}}",
                data: {
                    token: token,
                    storeId: store_id,
                },
                type: 'post',
                success: function(data) {

                    if(data.status == 1){
                        let contractInfo = data.data.contractInfo;

                        // $('.contractDate').val(contractInfo.contractDate);

                        form.val('example', {
                            "merName" :  contractInfo.merName,  // 商户名/注册名
                            "merLegal"  :  contractInfo.merLegal, //法人
                            "merAddr"  :  contractInfo.merAddr,  //商户地址/注册地址
                            "bizName"  :  contractInfo.bizName,  //经营名称
                            "businLic"  :  contractInfo.businLic,  //统一 社会信用代码
                            "bizAddr"  :  contractInfo.bizAddr,  //经营地址
                            "legalCode"  :  contractInfo.legalCode, //法人身份证号
                            "legalPhone"  :  contractInfo.legalPhone,  //法人电话
                            "linkMan"  :  contractInfo.linkMan,   //联系人
                            "linkmanPhone"  :  contractInfo.linkmanPhone, //联系人电话
                            "accName"  :  contractInfo.accName, //账号名称
                            "bankName"  :  contractInfo.bankName, //开户行
                            "account" :  contractInfo.account,  //账号
                            "chargeTypeDebit"  :  contractInfo.chargeTypeDebit, //卡支付借记卡
                            "chargeTypeDebitMax"  :  contractInfo.chargeTypeDebitMax, //卡支付借记卡封顶值
                            "chargeTypeCredit"  :  contractInfo.chargeTypeCredit, //卡支付贷记卡
                            "chargeTypeUnionpayDebit"  :  contractInfo.chargeTypeUnionpayDebit, //银联二维码借记卡
                            "chargeTypeUnionpayDebitMax"  :  contractInfo.chargeTypeUnionpayDebitMax, //银联二维码借记卡封顶值
                            "chargeTypeUnionpayCredit"  :  contractInfo.chargeTypeUnionpayCredit, //银联二维码贷记卡
                            "chargeTypeUnionpayDisDebit"  :  contractInfo.chargeTypeUnionpayDisDebit, //银联营销借记卡
                            "chargeTypeUnionpayDisDebitMax"  :  contractInfo.chargeTypeUnionpayDisDebitMax, //银联营销借记卡封顶值
                            "chargeTypeUnionpayDisCredit"  :  contractInfo.chargeTypeUnionpayDisCredit, //银联营销贷记卡
                            "chargeTypeWxpay"  :  contractInfo.chargeTypeWxpay, //微信
                            "chargeTypeAlipay"  :  contractInfo.chargeTypeAlipay, //支付宝
                            "d1SettlementRatio"  :  contractInfo.d1SettlementRatio, //D1结算按比例
                            "d1SettlementFee"  :  contractInfo.d1SettlementFee, //D1结算按笔
                            "d0SettlementRatio"  :  contractInfo.d0SettlementRatio, //D0结算按比例
                            "manager"  :  contractInfo.manager,  //客户经理
                            "managerPhone"  :  contractInfo.managerPhone, //客户经理电话
                            "contractDate" :  contractInfo.contractDate, //合同日期 YYYY年MM月DD日
                            "checkSameCard"  :  contractInfo.checkSameCard == '1' ? true : false, //同卡支付
                            "checkSettleT1"  :  contractInfo.checkSettleT1 == '1' ? true : false, //T1结算
                            "checkSettleD1"  :  contractInfo.checkSettleD1 == '1' ? true : false, //D1结算
                            "checkSettleD1Ratio"  :  contractInfo.checkSettleD1Ratio == '1' ? true : false, //D1结算按比例
                            "checkSettleD1Fee"  :  contractInfo.checkSettleD1Fee == '1' ? true : false, //D1结算按笔
                            "checkSettleD0"  :  contractInfo.checkSettleD0 == '1' ? true : false, //D0结算
                        });


                    }
                },

                error: function(data) {
                    alert('查找板块报错');
                }
            });
        }

        function EchoInfo(){
            //数据回显
            $.ajax({
                url: "{{url('/api/user/getContract')}}",
                data: {
                    token: token,
                    storeId: store_id,
                    txCode: '3210'
                },
                type: 'post',
                success: function(data) {
                    if(data.status == 1){
                        if(data.data.contractState == 1){ //签署完成

                            $("#openContract").css("cssText", "display:none !important;");
                            $("#stepTwobox").css("cssText", "display:none !important;");
                            $("#stepThree").css("cssText", "display:none !important;");
                            $("#stepFourButton").css("cssText", "display:none !important;");

                            $("#stepFour").show();
                            $("#stepFourContext").show();

                            $("#signatoryName").val(data.data.signatoryName);
                            $("#contractNo").val(data.data.contractNo);

                        }else{

                            $("#openContract").css("cssText", "display:none !important;");
                            $("#stepTwobox").css("cssText", "display:none !important;");
                            $("#stepThree").css("cssText", "display:none !important;");
                            $("#stepFourContext").css("cssText", "display:none !important;");

                            $("#stepFour").show();
                            $("#stepFourButton").show();

                        }
                    }
                },

                error: function(data) {
                    alert('查找板块报错');
                }
            });
        }

        //时间选择
        laydate.render({
            elem: '.contractDate',
            format: 'yyyy-MM-dd'
            ,done: function(value){

            }
        });

        function convertBase64ToBlob(base64, fileType, slice) {
            return new Blob(atob(base64)
                .match(new RegExp(`([\\s\\S]{${slice}})|([\\s\\S]{1,${slice}})`, 'g'))
                .map(function(item){
                    return new Uint8Array(item.split('').map(function(s, i) {
                        return item.charCodeAt(i)
                    }))
                }), {type: fileType})
        }


    })


</script>
</body>
</html>
