<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>添加交易规则</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card" style="height: 490px">
                <div class="layui-card-body">
                    <div class="layui-form" lay-filter="component-form-group"
                         style="width:500px;display: inline-block;">
                        <div class="layui-form-item">
                            <label class="layui-form-label">代理商名称</label>
                            <div class="layui-input-block">
                                <input type="text" autocomplete="off" class="layui-input agent_name"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">激活奖励金额</label>
                            <div class="layui-input-block">
                                <input type="text" autocomplete="off" class="layui-input standard_sum"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">首月奖励金额</label>
                            <div class="layui-input-block">
                                <input type="text" autocomplete="off" class="layui-input first_sum"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">次月奖励金额</label>
                            <div class="layui-input-block">
                                <input type="text" autocomplete="off" class="layui-input again_sum"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">下级激活奖励金额</label>
                            <div class="layui-input-block">
                                <input type="text" autocomplete="off" class="layui-input lower_standard_sum"
                                >
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">下级首月奖励金额</label>
                            <div class="layui-input-block">
                                <input type="text" autocomplete="off" class="layui-input lower_first_sum"
                                >
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">下级次月奖励金额</label>
                            <div class="layui-input-block">
                                <input type="text" autocomplete="off" class="layui-input lower_again_sum"
                                >
                            </div>
                        </div>

                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:0;text-align: center;margin-top:0px;">
                            <button class="layui-btn open" lay-submit="" style="display: none">开启</button>
                            <button class="layui-btn close" lay-submit=""
                                    style="background-color: #942a25;display: none">
                                停用
                            </button>
                            <button class="layui-btn edit" lay-submit="">修改下级返现</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" class="agent_id">
    <input type="hidden" class="rule_id">
    <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
    <script>
        var token = sessionStorage.getItem("Usertoken");
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        layui.config({
            base: '../../../layuiadmin/' //静态资源所在路径
        }).extend({
            index: 'lib/index' //主入口模块
        }).use(['index', 'form'], function () {
            var $ = layui.$
                , admin = layui.admin
                , element = layui.element
                , form = layui.form;
            var permission_idArr = [];
            form.render();
            // 未登录,跳转登录页面
            $(document).ready(function () {
                if (token == null) {
                    window.location.href = "{{url('/user/login')}}";
                }
            });

            var id = getUrlParam('customer_id')
            var customer_name = decodeURI(escape(getUrlParam('customer_name')))
            $('.agent_name').val(customer_name)
            $('.agent_id').val(id)
            getCashBackRuleId()

            function getCashBackRuleId() {
                var user_id = $('.agent_id').val()
                console.log(user_id)
                $.ajax({
                    url: "{{url('/api/user/getCashBackRuleId')}}",
                    type: 'get',
                    data: {token: token, user_id: user_id},
                    success: function (data) {
                        if (data.status == "1") {
                            $('.standard_sum').val(data.data.standard_sum)
                            $('.first_sum').val(data.data.first_sum)
                            $('.again_sum').val(data.data.again_sum)
                            $('.lower_standard_sum').val(data.data.lower_standard_sum)
                            $('.lower_first_sum').val(data.data.lower_first_sum)
                            $('.lower_again_sum').val(data.data.lower_again_sum)
                            $('.rule_id').val(data.data.rule_id)
                            if (data.data.status == "1") {
                                $('.close').show()
                                $('.open').hide()
                            } else {
                                $('.close').hide()
                                $('.open').show()
                            }
                        } else {
                            layer.alert("没有添加返现规则，请在交易返现中添加交易返现规则");
                        }
                    },
                    error: function (data) {

                    }
                });
            }

            //开启规则
            $('.open').click(function () {
                var rule_id = $('.rule_id').val()
                if (rule_id == "") {
                    layer.alert("请选择返现规则");
                } else {
                    $.post("{{url('/api/user/openUserRule')}}",
                        {
                            token: token,
                            agent_id: $('.agent_id').val(),
                        }, function (res) {
                            if (res.status == 1) {
                                parent.layer.msg(res.message, {
                                    icon: 1
                                    , time: 3000
                                });
                                parent.layer.close(index); //再执行关闭
                            } else {
                                layer.msg(res.message, {
                                    offset: '50px'
                                    , icon: 2
                                    , time: 3000
                                });
                            }
                        }, "json");
                }

            })
            //修改下级返现金额
            $('.edit').click(function () {
                var rule_id = $('.rule_id').val()
                if (rule_id == "") {
                    layer.alert("请选择返现规则");
                } else {
                    $.post("{{url('/api/user/editUserRule')}}",
                        {
                            token: token,
                            rule_id: rule_id,
                            user_id: $('.agent_id').val(),
                            lower_standard_sum: $('.lower_standard_sum').val(),
                            lower_first_sum: $('.lower_first_sum').val(),
                            lower_again_sum: $('.lower_again_sum').val(),
                            standard_sum: $('.standard_sum').val(),
                            first_sum: $('.first_sum').val(),
                            again_sum: $('.again_sum').val(),
                        }, function (res) {
                            if (res.status == 1) {

                                parent.layer.msg(res.message, {
                                    icon: 1
                                    , time: 3000
                                });
                                parent.layer.close(index); //再执行关闭
                            } else {
                                layer.msg(res.message, {
                                    offset: '50px'
                                    , icon: 2
                                    , time: 3000
                                });
                            }
                        }, "json");
                }

            })
            //关闭规则
            $('.close').click(function () {
                $.post("{{url('/api/user/get_store_transaction')}}",
                    {
                        token: token,
                        agent_id: $('.agent_id').val()
                    }, function (res) {
                        if (res.status == 1) {
                            closeRule()
                        } else if (res.status == 3) {
                            layer.confirm('该规则已产生商户检测，是否确认停用?', {icon: 2}, function (index) {
                                closeRule()
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 2
                                , time: 3000
                            });
                        }
                    }, "json");
            })

            function closeRule() {
                $.post("{{url('/api/user/close_user_rule')}}",
                    {
                        token: token,
                        agent_id: $('.agent_id').val(),
                    }, function (res) {
                        if (res.status == 1) {
                            parent.layer.msg(res.message, {
                                icon: 1
                                , time: 3000
                            });
                            parent.layer.close(index); //再执行关闭
                        } else {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 2
                                , time: 3000
                            });
                        }
                    }, "json");
            }

            //获取url中的参数
            function getUrlParam(name) {
                var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
                var r = window.location.search.substr(1).match(reg);  //匹配目标参数
                if (r != null) return unescape(r[2]);
                return null; //返回参数值
            }
        });
    </script>
</body>
</html>