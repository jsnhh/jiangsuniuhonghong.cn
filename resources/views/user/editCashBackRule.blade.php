<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>修改终端交易达标奖励</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
        .layui-card-header {
            width: 80px;
            text-align: right;
            float: left;
        }

        .layui-card-body {
            margin-left: 28px;
        }

        .layui-upload-img {
            width: 92px;
            height: 92px;
            margin: 0 10px 10px 0;
        }

        .up {
            position: relative;
            display: inline-block;
            cursor: pointer;
            border-color: #1ab394;
            color: #FFF;
            width: 92px !important;
            font-size: 10px !important;
            text-align: center !important;
        }

        .up input {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            opacity: .01;
            width: 100px;
            height: 30px;
        }

        .layui-upload-list {
            width: 100px;
            height: 96px;
            overflow: hidden;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none !important;
            margin: 0;
        }

        .width {
            width: 60%;
            float: left;
        }

        p {
            float: left;
            line-height: 36px;
            margin-left: 10px;
        }

        .userbox, .storebox {
            height: 200px;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 38px;
            width: 298px;
            background-color: #ffffff;
            border: 1px solid #ddd;
        }

        .userbox .list, .storebox .list {
            height: 38px;
            line-height: 38px;
            cursor: pointer;
            padding-left: 10px;
        }

        .userbox .list:hover, .storebox .list:hover {
            background-color: #eeeeee;
        }

        .s_id {
            line-height: 36px;
        }

        /*.layui-form-label{*/
        /*    width: 140px;*/
        /*}*/

    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card" style="margin-top:0px">
        <div class="layui-card-header" style="width:auto !important">添加激活标准规则&nbsp;&nbsp;&nbsp;<span
                    class="zong_school_name"></span></div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item school" style="width:500px;">
                    <label class="layui-form-label">规则名称</label>
                    <div class="layui-input-block">
                        <input type="text"  class="layui-input rule_name" disabled="disabled">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">活动时间</label>
                    <div class="layui-input-block">
                        <div class="layui-form" style="display: inline-block;">
                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <div class="layui-input-inline">
                                        <input type="text" class="layui-input start-item test-item" placeholder="活动开始时间"
                                               lay-key="23">
                                    </div>
                                </div>
                                -
                                <div class="layui-inline" style='margin-left:10px;'>
                                    <div class="layui-input-inline">
                                        <input type="text" class="layui-input end-item test-item" placeholder="活动结束时间"
                                               lay-key="24">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-card-header" style="width:auto !important;margin-top: 20px;text-align: left">激活标准规则
            <hr style="margin-top: -5px;width: 1650px;">
        </div>
        <div class="layui-form" lay-filter="component-form-group" style="margin-top: 100px;margin-left: 100px">

            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">激活标准单笔交易金额</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input standard_single_amt" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">激活标准交易笔数</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input standard_total" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">激活标准奖励金额</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input standard_amt" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">激活标准期限范围</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline" style="width: 150px">
                                <input type="number" class="layui-input standard_term" placeholder=""
                                       style="width: 150px">

                            </div>
                            <div class="layui-input-inline " style="padding-top: 10px">天内</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-card-header" style="width:auto !important;margin-top: 20px;text-align: left">首月奖励规则
            <span style="padding-left: 10px;font-size: 13px;color: red">注释：首月--激活日开始第一个月</span>
            <hr style="margin-top: -5px;width: 1650px;">
        </div>
        <div class="layui-form" lay-filter="component-form-group" style="margin-top: 100px;margin-left: 100px">
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">首月交易总金额</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input first_single_amt" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">首月交易笔数</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input first_total" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">首月奖励金额</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input first_amt" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">首月奖励时间范围</label>
                <label class="layui-form-label" style="margin-left: -35px;width: 150px">激活日起第一个月内</label>
            </div>
        </div>
        <div class="layui-card-header" style="width:auto !important;margin-top: 20px;text-align: left">次月奖励规则
            <span style="padding-left: 10px;font-size: 13px;color: red">注释：首月--激活日开始第二个月</span>
            <hr style="margin-top: -5px;width: 1650px;">
        </div>
        <div class="layui-form" lay-filter="component-form-group" style="margin-top: 100px;margin-left: 100px">
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">次月交易总金额</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input again_single_amt" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">次月交易笔数</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input again_total" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">次月奖励金额</label>
                <div class="layui-input-block" style="float:left;margin-left:0px;">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <div class="layui-input-inline">
                                <input type="text" class="layui-input again_amt" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 140px">次月月奖励时间范围</label>
                <label class="layui-form-label" style="margin-left: -35px;width: 150px">激活日起第二个月内</label>
            </div>
        </div>
        <div class="layui-form-item layui-layout-admin">
            <div class="layui-input-block">
                <div class="layui-footer" style="left: 0;">
                    <button class="layui-btn submit site-demo-active" style="border-radius:5px;" data-type="tabChange">
                        确定提交
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="id">
<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var terminalRewardId = sessionStorage.getItem("terminalRewardId");
    // var str=location.search;
    // var school_name=str.split('?')[1];

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form', 'upload', 'formSelects', 'element', 'laydate'], function () {
        var $ = layui.$
            , admin = layui.admin
            , element = layui.element
            , layer = layui.layer
            , laydate = layui.laydate
            , form = layui.form
            , upload = layui.upload
            , formSelects = layui.formSelects;

        var src = $('#demo1').attr('src');
        element.render();
        // 未登录,跳转登录页面
        $(document).ready(function () {
            if (token == null) {
                window.location.href = "{{url('/user/login')}}";
            }else {
                $.ajax({
                    url: "{{url('/api/user/getCashBackRuleId')}}",
                    data: {token: token, id: terminalRewardId},
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        $('.id').val(data.data.id)
                        $('.rule_name').val(data.data.rule_name)
                        $('.start-item').val(data.data.start_time)
                        $('.end-item').val(data.data.end_time)
                        $('.standard_amt').val(data.data.standard_amt)
                        $('.standard_single_amt').val(data.data.standard_single_amt)
                        $('.standard_total').val(data.data.standard_total)
                        $('.first_amt').val(data.data.first_amt)
                        $('.first_single_amt').val(data.data.first_single_amt)
                        $('.first_total').val(data.data.first_total)
                        $('.again_amt').val(data.data.again_amt)
                        $('.again_single_amt').val(data.data.again_single_amt)
                        $('.again_total').val(data.data.again_total)
                        $('.standard_term').val(data.data.standard_term)
                    },
                    error: function (data) {
                        alert('查找板块报错');
                    }
                });
            }
        });

        // 时间++++++++++++++++++++++++++++++++++++++++++++++++
        laydate.render({
            elem: '.start-item'
            , type: 'datetime'
            , trigger: 'click'
            , done: function (value) {
            }
        });

        laydate.render({
            elem: '.end-item'
            , type: 'datetime'
            , trigger: 'click'
            , done: function (value) {
            }
        });
        $('.submit').on('click', function () {
            $.post("{{url('/api/user/up_cash_back_rule')}}",
                {
                    token: token,
                    time_start: $('.start-item').val(),
                    time_end: $('.end-item').val(),
                    standard_amt: $('.standard_amt').val(),
                    standard_single_amt: $('.standard_single_amt').val(),
                    standard_total: $('.standard_total').val(),
                    first_amt: $('.first_amt').val(),
                    first_single_amt: $('.first_single_amt').val(),
                    first_total: $('.first_total').val(),
                    again_amt: $('.again_amt').val(),
                    again_single_amt: $('.again_single_amt').val(),
                    again_total: $('.again_total').val(),
                    standard_term: $('.standard_term').val(),
                    id:$('.id').val(),

                }, function (res) {
//                console.log(res);
                    if (res.status == 1) {
                        layer.msg(res.message, {
                            offset: '50px'
                            , icon: 1
                            , time: 3000
                        });
                    } else {
                        layer.msg(res.message, {
                            offset: '50px'
                            , icon: 2
                            , time: 3000
                        });
                    }
                }, "json");
        });

    });

</script>

</body>
</html>
