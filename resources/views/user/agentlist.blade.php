<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>代理商列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
        .box a{
            margin-bottom:10px;
        }
        .name{
            cursor: pointer;
        }
        .cengji{
            line-height: 36px;
        }
        #demo li{
            width:300px;
        }
        .eyes{
            margin-left: 25px;
            width: 20px;
            height: 7px;
            margin-top: 8px
        }
    </style>
</head>
<body>
<div class="layui-fluid" id="LAY-component-grid-mobile-pc">
    <div class="layui-row layui-col-space10" style="margin-top:0px">

        <div class="layui-row layui-col-space10">

            <div class="layui-col-xs4 layui-col-md4">
                <div class="layui-card">
                    <div class="layui-card-header">代理商列表</div>
                    <div class="layui-card-body" style="height:500px;overflow-y: auto;">
                        <div class="li_list" style="display: inline-block; width: 300px; padding: 10px;  overflow: auto;">
                            <ul id="demo"></ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-col-xs6 layui-col-md6">
                <div class="layui-card">
                    <div class="layui-card-header">代理商详情</div>
                    <div class="layui-card-body" style="margin-top: 30px;">
                        <div class="cmdlist-container">
                            <img style='margin-top:-60px;' src="{{asset('/school/images/touxiang.png')}}">
                            <div class="xinxi" style="display: inline-block;">
                                <p  data-id=""><span class="name"></span>(<span class="userId">000</span>)</p>
                                <div style="display:flex;">
                                    <p class="phone"> </p>
                                    <div class='eyes'>
                                        <img id="eyes" style="height:8px;margin-top: -15px;" src="{{asset('/school/images/see-none.png')}}">
                                    </div>
                                </div>
                                <p class="jhm"><a lay-href="{{url('/user/qrcode')}}">激活码</a>:<span></span></p>
                                <p class="jine">赏金余额:<span></span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-col-xs8 layui-col-md8">
                <div class="layui-card">
                    <div class="layui-card-header">功能管理</div>
                    <div class="layui-card-body">
                        <div class="cmdlist-container box">
                            <a class="layui-btn layui-btn-primary" style="border-radius: 5px;" lay-href="{{url('/user/addagent')}}" id="agent" data-id="">添加代理商</a>
                            <a class="layui-btn layui-btn-primary" style="border-radius: 5px;" id="role_fen" data-id="">角色分配</a>
                            <a class="layui-btn layui-btn-primary rate" style="border-radius: 5px;" id="rate" data-id="">支付成本费率管理</a>
                            <a class="layui-btn layui-btn-primary reward" style="border-radius: 5px;" id="reward" data-id="">赏金列表</a>
                            <a class="layui-btn layui-btn-primary putforward" style="border-radius: 5px;" id="putforward" data-id="">提现记录</a>
                            <a class="layui-btn layui-btn-primary storerate" style="border-radius: 5px; margin-left:0px;" id="storerate" data-id="">商户默认费率</a>
                            <a class="layui-btn layui-btn-primary storeconfig" style="border-radius: 5px;" id="storeconfig" data-id="">门店配置</a>
                            <a class="layui-btn layui-btn-primary chaxun" style="border-radius: 5px;" id="chaxun" data-id="">对账查询</a>
                            <a class="layui-btn layui-btn-primary settle" style="border-radius: 5px;" id="settle" data-id="">赏金结算明细</a>
                            <a class="layui-btn layui-btn-primary flowerfq" style="border-radius: 5px;" id="flowerfq" data-id="">花呗分期成本费率</a>
                            <a class="layui-btn layui-btn-primary editmsg" style="border-radius: 5px;" id="editmsg" data-id="" data-username="" data-phone="" data-name="">修改信息</a>
                            <a class="layui-btn layui-btn-primary transfer" style="border-radius: 5px;" id="transfer"  data-id="" data-username="" >转移代理商</a>
                            <a class="layui-btn layui-btn-primary szbl" style="border-radius: 5px;" id="szbl"  data-id="" data-username="" data-profit-ratio="" >设置比例</a>
                            <a class="layui-btn layui-btn-primary txsz" style="border-radius: 5px;" id="txsz"  data-id="">提现设置</a>
                            <a class="layui-btn layui-btn-primary jyfx" style="border-radius: 5px;" id="jyfx"  data-id="">活动配置</a>
                            <a class="layui-btn layui-btn-primary xjjyfx" style="border-radius: 5px;" id="xjjyfx"  data-id="">下级代理活动配置</a>
                            <a class="layui-btn layui-btn-primary zeroRate" style="border-radius: 5px;" id="zeroRate"  data-id="">零费率设置</a>
                            <a class="layui-btn layui-btn-primary recharge" style="border-radius: 5px;" lay-href="{{url('/user/setProfit')}}" id="recharge"  data-id="">设置充值比例</a>
                            <a class="layui-btn layui-btn-primary recharge" style="border-radius: 5px;" id="settlementType"  data-id="">零费率结算方式</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-col-xs8 layui-col-md8 ceng">
                <div class="layui-card">
                    <div class="layui-card-header">代理层级搜索</div>
                    <div class="layui-card-body">
                        <div class="cmdlist-container">
                            <div class="layui-form" lay-filter="component-form-group" style="width:600px;display: inline-block;">
                                <div class="layui-form-item">
                                    <div class="layui-inline">
                                        <div class="layui-input-inline">
                                            <input type="text" style="border-radius:5px;" name="schoolname" placeholder="请输入代理名称或手机号" autocomplete="off" class="layui-input">
                                        </div>
                                    </div>

                                    <div class="layui-inline">
                                        <button class="layui-btn layuiadmin-btn-list" style="border-radius:5px;" lay-submit="" lay-filter="LAY-app-contlist-search" style="margin-bottom: 0;height:36px;line-height: 36px;">
                                            <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <div class="layui-input-block cengji" style='margin-left: 0px;'>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <input class="search" ><button class="layui-btn">搜索</button> -->

    <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
    <script type="text/javascript" src="{{asset('/school/js/jsencrypt.min.js')}}"></script>
    <script>
        var token = sessionStorage.getItem("Usertoken");
        var level = sessionStorage.getItem("level");

        layui.config({
            base: '../../layuiadmin/' //静态资源所在路径
        }).extend({
            index: 'lib/index', //主入口模块
            formSelects: 'formSelects'
        }).use(['index', 'form','tree'], function(){
            var $ = layui.$
                ,admin = layui.admin
                ,element = layui.tree
                ,form = layui.form;

            // 未登录,跳转登录页面
            $(document).ready(function(){
                if(token==null || token==''){
                    window.location.href="{{url('/user/login')}}";
                }
            });

            var permissions = sessionStorage.getItem("permissions");
            var str=JSON.parse(permissions);
            var arr=[];
            for(var i=0;i<str.length;i++){
                var aa=str[i].name;
                arr.push(aa);
            }


            if(level != 0){
                // 权限管理+++++++++++++
                $('.box a').each(function(index,item){
                    // console.log($(this).attr('data'))
                    // if($.inArray($(this).attr('data'),arr)==-1){
                    //   $(this).hide()
                    // }
                });
                $('.box a').each(function(index,item){
                    // console.log($(this).find('a').html())
                    // console.log($.inArray($(this).find('a').html(),arr))

                    if($.inArray($(this).html(),arr)==-1){
                        console.log(arr)
                        $(this).hide();
                    }
                });
            }

            $.post("{{url('/api/user/get_sub_users')}}",
                {
                    token:token
                    ,return_type:'layui'
                },function(res){
                    //    console.log(res);
                    $('.name').html(res.data[0].name);
                    $('.phone').html(res.data[0].phone);
                    $('.jhm span').html(res.data[0].s_code);
                    $('.jine span').html(res.data[0].money);
                    $('#agent').attr('data-id',res.data[0].id);
                    $('#role_fen').attr('data-id',res.data[0].id);
                    $('#rate').attr('data-id',res.data[0].id);
                    $('#reward').attr('data-id',res.data[0].id);
                    $('#putforward').attr('data-id',res.data[0].id);
                    $('#storerate').attr('data-id',res.data[0].id);
                    $('#storeconfig').attr('data-id',res.data[0].id);
                    $('#chaxun').attr('data-id',res.data[0].id);
                    $('#settle').attr('data-id',res.data[0].id);
                    $('#flowerfq').attr('data-id',res.data[0].id);
                    $('.name').attr('data-id',res.data[0].id);
                    $('.userId').html(res.data[0].id);

                    $('.editmsg').attr('data-id',res.data[0].id);
                    $('.editmsg').attr('data-username',res.data[0].name);
                    $('.editmsg').attr('data-phone',res.data[0].phone);
                    $('.editmsg').attr('data-name',res.data[0].logo);

                    $(".szbl").attr('data-id',res.data[0].id);
                    $(".szbl").attr('data-username',res.data[0].name);
                    $("#szbl").attr('data-profit-ratio',res.data[0].profit_ratio);

                    $('#transfer').attr('data-id',res.data[0].id);
                    $('#transfer').attr('data-username',res.data[0].name);
                    $('#jyfx').attr('data-id',res.data[0].id);
                    $('#jyfx').attr('data-username',res.data[0].name);
                    $('#xjjyfx').attr('data-id',res.data[0].id);
                    $('#xjjyfx').attr('data-username',res.data[0].name);
                    if (res.data[0].is_withdraw==1){
                        $('#txsz').text('提现正常');
                    }else{
                        $('#txsz').text('提现禁止');
                    }
                    $("#txsz").attr('data-id',res.data[0].id);

                    if (res.data[0].is_zero_rate==1){
                        $('#zeroRate').text('零费率设置开启');
                    }else{
                        $('#zeroRate').text('零费率设置禁止');
                    }
                    $("#zeroRate").attr('data-id',res.data[0].id);
                    $('#settlementType').attr('data-id',res.data[0].id);
                    $('#settlementType').attr('data-username',res.data[0].name)


                    layui.tree({
                        elem: '#demo'
                        ,nodes: res.data
                        ,click: function(node){
                            //console.log(node); //node即为当前点击的节点数据
                            $('.name').html(node.name);
                            $('.phone').html(node.phone);
                            $('.jhm span').html(node.s_code);
                            $('.jine span').html(node.money);
                            $('#agent').attr('data-id',node.id);
                            $('#rate').attr('data-id',node.id);
                            $('#reward').attr('data-id',node.id);
                            $('#putforward').attr('data-id',node.id);
                            $('#role_fen').attr('data-id',node.id);
                            $('#storerate').attr('data-id',node.id);
                            $('#storeconfig').attr('data-id',node.id);
                            $('#chaxun').attr('data-id',node.id);
                            $('#settle').attr('data-id',node.id);
                            $('#flowerfq').attr('data-id',node.id);
                            $('.name').attr('data-id',node.id);
                            $('.userId').html(node.id);
                            $('.editmsg').attr('data-id',node.id);
                            $('.editmsg').attr('data-username',node.name);
//                    $('.editmsg').attr('data-levelname',node.level_name);
                            $('.editmsg').attr('data-phone',node.phone);
                            $('.editmsg').attr('data-name',node.logo);

                            $(".szbl").attr('data-id',node.id);
                            $(".szbl").attr('data-username',node.name);
                            $(".szbl").attr('data-profit-ratio',node.profit_ratio)

                            $('#transfer').attr('data-id',node.id);
                            $('#transfer').attr('data-username',node.name);
                            $('#jyfx').attr('data-id',node.id);
                            $('#jyfx').attr('data-username',node.name);

                            $('#xjjyfx').attr('data-id',node.id);
                            $('#xjjyfx').attr('data-username',node.name);
                            if (node.is_withdraw==1){
                                $('#txsz').text('提现正常');
                            }else{
                                $('#txsz').text('提现禁止');
                            }
                            $("#txsz").attr('data-id',node.id);

                            if (node.is_zero_rate==1){
                                $('#zeroRate').text('零费率设置开启');
                            }else{
                                $('#zeroRate').text('零费率设置禁止');
                            }
                            $("#zeroRate").attr('data-id',node.id);

                            $('#recharge').attr('data-id',node.id);
                            $('#recharge').attr('data-pid',node.pid);
                            $('#settlementType').attr('data-id',node.id);
                            $('#settlementType').attr('data-username',node.name)

                        }
                    });

                    var pid = parseInt(res.data[0].pid);

                    if(pid < 2){
                        $('.ceng').show()
                    }else{
                        $('.ceng').hide()
                    }

                },"json");

            $(".eyes").click(function (){
                var _this = $(this);
                var proxyCompanyId = _this.parents(".xinxi").find(".userId").text();
                layer.prompt({title: '请输入密码', formType: 1}, function(pass, index){
                    layer.close(index);
                    var encrypt = new JSEncrypt();
                    encrypt.setPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4COVutRbOUfQNjvVOzwK49NzHIPRwwksnJ6QtdHwGmdUZiT2HZxVwfotcOjA5aY16D/2Ahq3gLH4yu2y42dS0lfeBMqUcm+bY7aZ54wClm75RI90uc54F8IgMkNz8J/VS9LYI/B4uHVsc+4KK4Ycr8S8O004ExtvQqu2QCl7Aai/WC4URIdCyNm8La2axoA1jjj3SzpytLvP6Z/iHSlx37Y9AMR0V94R13v4BFlMQDG+2REVJsk6LCyzHQfUvJlnsyKey0n/v8DLC070lQzLPYV0jsiit2AUkyURRLxEaZm2C0YYhfrGjl+x8n/kDteZbDVcyn7UsEdSicijv9DXkQIDAQAB");
                    var data = encrypt.encrypt('pay_password='+pass);

                    $.post("{{url('/api/user/check_platform_pay_password')}}",
                        {
                            token:token,
                            sign:data
                        },function(data){
                            if(data.status==1){
                                layer.close(index);
                                $.post("{{url('/api/user/user_info')}}",
                                    {
                                        token:token
                                        ,user_id:proxyCompanyId
                                    },function(data){
//                          console.log(data)
                                        $('.phone').html(data.data.phone)
                                    },"json");
                            }else{
                                layer.msg(data.message, {
                                    offset: '50px'
                                    ,icon: 2
                                    ,time: 3000
                                });
                            }
                        },"json");
                });
            });

            // 获取激活码
            $('.jhm').click(function(){
                sessionStorage.setItem('s_code', $(this).find('span').html());
                sessionStorage.setItem('s_agentname', $('.name').html());
            });
            $('#agent').click(function(){
                sessionStorage.setItem('agentname', $('.name').html());
                sessionStorage.setItem('dataid', $(this).attr('data-id'));
            });

            $("#role_fen").click(function(){
                var customer_id=$(this).attr('data-id');
                layer.open({
                    type: 2,
                    title: '角色分配',
                    shade: false,
                    maxmin: true,
                    area: ['70%', '70%'],
                    content: "{{url('/user/rolelist?')}}"+customer_id
                });
            });
            $("#jyfx").click(function(){
                var customer_id=$(this).attr('data-id');
                var customer_name=$(this).attr('data-username');
                layer.open({
                    type: 2,
                    title: '交易返现',
                    shade: false,
                    maxmin: true,
                    area: ['50%', '40%'],
                    content: "{{url('/user/editUserRule?')}}customer_id="+customer_id+"&customer_name="+customer_name
                });
            });
            $("#xjjyfx").click(function(){
                var customer_id=$(this).attr('data-id');
                var customer_name=$(this).attr('data-username');
                layer.open({
                    type: 2,
                    title: '交易返现',
                    shade: false,
                    maxmin: true,
                    area: ['50%', '76%'],
                    content: "{{url('/user/editUserRuleReward?')}}customer_id="+customer_id+"&customer_name="+customer_name
                });
            });
            $("#rate").click(function(){
                sessionStorage.setItem('agentName', $('.name').html());
                var user_id=$(this).attr('data-id');
                $('.rate').attr('lay-href',"{{url('/user/ratelist?')}}"+user_id);
            });
            $("#storerate").click(function(){
                sessionStorage.setItem('agentName', $('.name').html());
                var user_id=$(this).attr('data-id');
                $('.storerate').attr('lay-href',"{{url('/user/storeratelist?')}}"+user_id);
            });
            $("#reward").click(function(){
                sessionStorage.setItem('agentName', $('.name').html());
                var user_id=$(this).attr('data-id');
                $('.reward').attr('lay-href',"{{url('/user/reward?')}}"+user_id);
            });
            $("#putforward").click(function(){
                sessionStorage.setItem('agentName', $('.name').html());
                var user_id=$(this).attr('data-id');
                $('.putforward').attr('lay-href',"{{url('/user/putforward?')}}"+user_id);
            });
            $("#storeconfig").click(function(){
                sessionStorage.setItem('agentName', $('.name').html());
                var user_id=$(this).attr('data-id');
                $('.storeconfig').attr('lay-href',"{{url('/user/storeconfig?')}}"+user_id);
            });
            $("#chaxun").click(function(){
                sessionStorage.setItem('agentName', $('.name').html());
                var user_id=$(this).attr('data-id');
                var user_name=$('.name').html();
                $('.chaxun').attr('lay-href',"{{url('/user/reconciliation?user_id=')}}"+user_id+"&user_name="+user_name);
            });
            $("#settle").click(function(){
                sessionStorage.setItem('agentName', $('.name').html());
                var user_id=$(this).attr('data-id');
                $('.settle').attr('lay-href',"{{url('/user/settledetail?user_id=')}}"+user_id);
            });
            $("#flowerfq").click(function(){
                sessionStorage.setItem('agentName', $('.name').html());
                var user_id=$(this).attr('data-id');
                $('.flowerfq').attr('lay-href',"{{url('/user/flowerfq?user_id=')}}"+user_id);
            });
            $("#editmsg").click(function(){
                sessionStorage.setItem('agentName', $('.name').html());
                var user_id=$(this).attr('data-id');
                $('.editmsg').attr('lay-href',"{{url('/user/editmsg?user_id=')}}"+user_id+"&user_name="+$(this).attr('data-username')+"&phone="+$(this).attr('data-phone')+"&logo="+$(this).attr('data-name'));
            });
            $('#transfer').click(function(){
                sessionStorage.setItem('agentName', $('.name').html());
                var user_id=$(this).attr('data-id');
                $('.transfer').attr('lay-href',"{{url('/user/transferstore?user_id=')}}"+user_id+"&user_name="+$(this).attr('data-username'));
            });

            $("#szbl").click(function(){
                sessionStorage.setItem('agentName', $('.name').html());
                var user_id=$(this).attr('data-id');
                $('.szbl').attr('lay-href',"{{url('/user/szbl?user_id=')}}"+user_id+"&profit_ratio="+$(this).attr('data-profit-ratio')+"&user_name="+$(this).attr('data-username'));
            });

            $(".name").dblclick(function(){
                var user_id=$(this).attr('data-id');
                layer.confirm('确认删除此消息?',{icon: 2}, function(index){
                    $.post("{{url('/api/user/del_sub_user')}}",
                        {
                            token:token
                            ,user_id:user_id
                        },function(res){
                            //console.log(res);
                            if(res.status==1){
                                // obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                                layer.close(index);
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 1
                                    ,time: 2000
                                });
                            }else{
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 2
                                    ,time: 3000
                                });
                            }
                        },"json");
                });
            });


            $("#txsz").click(function(){
                var user_id=$(this).attr('data-id');
                layer.confirm('确认要设置吗?',{icon: 2}, function(index){
                    $.post("{{url('/api/user/set_withdraw')}}",
                        {
                            token:token
                            ,user_id:user_id
                        },function(res){
                            if(res.status==1){
                                // obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                                var is_withdraw=res.is_withdraw;
                                if (is_withdraw==1){
                                    $('#txsz').text('提现正常');
                                }else{
                                    $('#txsz').text('提现禁止');
                                }
                                layer.close(index);
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 1
                                    ,time: 2000
                                });
                            }else{
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 2
                                    ,time: 3000
                                });
                            }
                        },"json");
                });
            });

            $("#zeroRate").click(function(){
                var user_id=$(this).attr('data-id');
                layer.confirm('确认要设置吗?',{icon: 2}, function(index){
                    $.post("{{url('/api/user/set_zeroRate')}}",
                        {
                            token:token
                            ,user_id:user_id
                        },function(res){
                            if(res.status==1){
                                var is_zero_rate=res.is_zero_rate;
                                if (is_zero_rate==1){
                                    $('#zeroRate').text('零费率设置开启');
                                }else{
                                    $('#zeroRate').text('零费率设置禁止');
                                }
                                layer.close(index);
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 1
                                    ,time: 2000
                                });
                            }else{
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 2
                                    ,time: 3000
                                });
                            }
                        },"json");
                });
            });

            $('#recharge').click(function(){
                sessionStorage.setItem('dataid', $(this).attr('data-id'));
                sessionStorage.setItem('datapid', $(this).attr('data-pid'));
            });
            $("#settlementType").click(function(){
                var customer_id=$(this).attr('data-id');
                var customer_name=$(this).attr('data-username');
                layer.open({
                    type: 2,
                    title: '设置零费率结算方式',
                    shade: false,
                    maxmin: true,
                    area: ['50%', '40%'],
                    content: "{{url('/user/editUserSettlementType?')}}customer_id="+customer_id+"&customer_name="+customer_name
                });
            });
            //监听搜索
            form.on('submit(LAY-app-contlist-search)', function(data){
                var obj = data.field;
                var user_name = data.field.schoolname;
                // console.log(user_name);
                $.post("{{url('/api/user/select_pid_user')}}",
                    {
                        token:token
                        ,user_name:user_name
                    },function(res){
//              console.log(res);
                        if(res.status==1){
                            $('.cengji').html(res.data.info)
                            $(".cengji").click(function (){
                                // console.log('11111')
                                $(".eyes").hide();
                                $('.userId').html(res.data.user_info.id);
                                $('.name').html(res.data.user_info.name);
                                $('.phone').html(res.data.user_info.phone);
                                $('.jhm span').html(res.data.user_info.s_code);
                                $('.jine span').html(res.data.user_info.money);

                                $('#agent').attr('data-id',res.data.user_info.id);
                                $('#role_fen').attr('data-id',res.data.user_info.id);
                                $('#rate').attr('data-id',res.data.user_info.id);
                                $('#reward').attr('data-id',res.data.user_info.id);
                                $('#putforward').attr('data-id',res.data.user_info.id);
                                $('#storerate').attr('data-id',res.data.user_info.id);
                                $('#storeconfig').attr('data-id',res.data.user_info.id);
                                $('#chaxun').attr('data-id',res.data.user_info.id);
                                $('#settle').attr('data-id',res.data.user_info.id);
                                $('#flowerfq').attr('data-id',res.data.user_info.id);
                                $('.name').attr('data-id',res.data.user_info.id);
                                $('.userId').html(res.data.user_info.id);


                                $('.editmsg').attr('data-id',res.data.user_info.id);
                                $('.editmsg').attr('data-username',res.data.user_info.name);
                                $('.editmsg').attr('data-phone',res.data.user_info.phone);
                                $('.editmsg').attr('data-name',res.data.user_info.logo);

                                $(".szbl").attr('data-id',res.data.user_info.id);
                                $(".szbl").attr('data-username',res.data.user_info.name);
                                $("#szbl").attr('data-profit-ratio',res.data.user_info.profit_ratio);

                                $('#transfer').attr('data-id',res.data.user_info.id);
                                $('#transfer').attr('data-username',res.data.user_info.name);
                                $('#jyfx').attr('data-id',res.data.user_info.id);
                                $('#jyfx').attr('data-username',res.data.user_info.name);
                                $('#xjjyfx').attr('data-id',res.data.user_info.id);
                                $('#xjjyfx').attr('data-username',res.data.user_info.name);
                                if (res.data.user_info.is_withdraw==1){
                                    $('#txsz').text('提现正常');
                                }else{
                                    $('#txsz').text('提现禁止');
                                }
                                $("#txsz").attr('data-id',res.data.user_info.id);

                                if (res.data.user_info.is_zero_rate==1){
                                    $('#zeroRate').text('零费率设置开启');
                                }else{
                                    $('#zeroRate').text('零费率设置禁止');
                                }
                                $("#zeroRate").attr('data-id',res.data.user_info.id);
                                $('#settlementType').attr('data-id',res.data.user_info.id);
                                $('#settlementType').attr('data-username',res.data.user_info.name)
                            });
                        }else{
                            $('.cengji').html('');
                            layer.msg(res.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 3000
                            });
                        }
                    },"json");
            });

            // function myFunction() {
            //     // 声明变量
            //     var input, filter, table, tr, td, i;
            //     input = document.getElementById("myInput");
            //     filter = input.value.toUpperCase();
            //     table = document.getElementById("demo");
            //     li = table.getElementsByTagName("li");
            //     // 循环表格每一行，查找匹配项
            //     for (i = 0; i < li.length; i++) {
            //         span = li[i].getElementsByTagName("span")[0];
            //         if (span) {
            //             if (span.innerHTML.toUpperCase().indexOf(filter) > -1) {
            //                 $('.con1').addClass('current');
            //                 li[i].style.display = "";
            //             } else {
            //                 li[i].style.display = "none";
            //             }
            //         }
            //     }
            // }

        });
    </script>

</body>
</html>
