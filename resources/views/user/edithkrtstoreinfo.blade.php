<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>海科融通补充进件信息</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
<link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
<link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/formSelects-v4.css')}}" media="all">
<style>
    .img img{width:100%;height:100%;}
    .layui-card-header{width:80px;text-align: right;float:left;}
    .layui-card-body{margin-left:28px;}
    .layui-upload-img{width: 100px; height: 92px; /*margin: 0 10px 10px 0;*/}
    .up{position: relative;display: inline-block;cursor: pointer;border-color: #1ab394; color: #FFF;width: auto !important;font-size: 10px !important;text-align: center !important;}
    .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
    .layui-upload-list{width: 100px;height:96px;overflow: hidden;margin: 10px auto;}
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {-webkit-appearance: none !important;margin: 0;}
    .userbox,.branchbox{
        height:200px;
        overflow-y: auto;
        z-index: 999;
        position: absolute;
        left: 0px;
        top: 85px;
        width:400px;
        background-color:#ffffff;
        border: 1px solid #ddd;
    }
    .userbox .list,.branchbox .list{
        height:38px;line-height: 38px;cursor:pointer;
        padding-left:10px;
    }
    .userbox .list:hover,.branchbox .list:hover{
        background-color:#eeeeee;
    }
</style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card" style="margin-top: 0px">
        <div class="layui-card-header">补充信息</div>
        <div class="layui-card-body layui-row layui-col-space10">
            <div class="layui-row layui-form">
                <div class="layui-col-md8">
                <form class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label" style="text-align:center;">经营类型</label>
                        <div class="layui-input-block">
                            <select  name="industry" id="industry" lay-filter="industry" lay-search>
                            <!-- <option value="">请选择</option> -->
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item js_phone">
                    <label class="layui-form-label" style="text-align:center;">手&nbsp;&nbsp;机&nbsp;&nbsp;号</label>
                    <div class="layui-input-block">
                        <input type="text" placeholder="请输入联系人手机号" class="layui-input phone">
                    </div>
                </div>
                </form>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block" style="margin-left:440px">
            <div class="layui-footer" style="left: 0;">
                <button class="layui-btn submit" style="border-radius:5px">保存</button>
            </div>
        </div>
    </div>
</div>

<!-- 商户性质 -->
<input type="hidden" class="store_name" value="">
<input type="hidden" class="store_type" value="">
<input type="hidden" class="category_name" value="">
<input type="hidden" class="category_id" value="">

<!-- 卡类型 -->
<input type="hidden" class="cardtype_id" value="">

<!-- 银行卡 -->
<input type="hidden" class="bankName" value="">
<input type="hidden" class="sub_bank_name" value="">
<input type="hidden" class="bank_no" value="">

<!-- 地区 -->
<input type="hidden" class="provincecode" value="">
<input type="hidden" class="provincename" value="">
<input type="hidden" class="citycode" value="">
<input type="hidden" class="cityname" value="">
<input type="hidden" class="areacode" value="">
<input type="hidden" class="areaname" value="">

<!-- 银行卡卡户地区 -->
<input type="hidden" class="provincecodebank" value="">
<input type="hidden" class="provincenamebank" value="">
<input type="hidden" class="citycodebank" value="">
<input type="hidden" class="citynamebank" value="">
<input type="hidden" class="areacodebank" value="">
<input type="hidden" class="areanamebank" value="">

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var store_id = sessionStorage.getItem("store_store_id");

    var province_code = sessionStorage.getItem("store_province_code");
    var city_code = sessionStorage.getItem("store_city_code");
    var area_code = sessionStorage.getItem("store_area_code");

    var str = location.search;
    var store_id_add = str.split('?')[1];

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;
            formSelects.render('payment_type_no');
            formSelects.btns('payment_type_no', []);
            var arrr=[];

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token == null){
                window.location.href="{{url('/user/login')}}";
            }
        });

        // 经营范围
        $.ajax({
            url : "{{url('/api/user/business_scope')}}",
            data : {
                token:token
                ,store_id:store_id
            },
            type : 'post',
            success : function(data) {
                console.log(data)
                // const newData = JSON.parse(data).data;
                var optionStr = "";
                for(var i=0;i<data.data.length;i++){
                    optionStr += "<option value='" + data.data[i].code + "'>"
                                      + data.data[i].name + "</option>";
                }
                $("#industry").append('<option value=""></option>'+optionStr);
                layui.form.render('select');
            },
            error : function(data) {
                alert('查找板块报错');
            }
        });

        $('.submit').click(function(){
            $.post("{{url('/api/user/up_store')}}",
            {
                token:token
                ,store_id:store_id
                ,hkrt_bus_scope_code:$('#industry').val()
                ,reserved_mobile:$('.phone').val(),
            },function(res){
                if(res.status==1){
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 3000
                    });
                }else{
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 3000
                    });
                }
            },"json");
        });
    });

</script>

</body>
</html>
