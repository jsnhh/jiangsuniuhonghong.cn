<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>APP头条</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
<link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card" style="margin-top:0px">
        <div class="layui-card-header">APP头条</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item">
                    <label class="layui-form-label">头条链接</label>
                    <div class="layui-input-block" style="width:600px">
                        <input type="text" name="access_key" lay-verify="access_key" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>

                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit site-demo-active" style="border-radius:5px" data-type="tabChange">保存</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script> 
<script>
    var token = sessionStorage.getItem("Usertoken");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','element'], function(){
        var $ = layui.$ 
            ,admin = layui.admin
            ,element = layui.element
            ,form = layui.form;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });

        $.post("{{url('/api/user/user_tt_set')}}",
        {
            token:token
        },function(res){
    //        console.log(res);
            if(null != res && "" != res){
                if(res.status==1){
                    $('.layui-form .layui-form-item').eq(0).find('input').val(res.data.url);
                }else if(res.status!=3){
                    layer.alert(res.message, {icon: 2});
                }
            }else{
                layer.alert('获取信息失败', {icon: 2});
            }

        }, "json");

        $('.submit').on('click', function(){
            $.post("{{url('/api/user/user_tt_set')}}",
            {
                token:token
                ,url:$('.layui-form .layui-form-item').eq(0).find('input').val()
            },function(res){
    //            console.log(res);
                if(res.status==1){
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 3000
                    });
                }else{
                    layer.alert(res.message, {icon: 2});
                }
            }, "json");
        });

    });

</script>

</body>
</html>
