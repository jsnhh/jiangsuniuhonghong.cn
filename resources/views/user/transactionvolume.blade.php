<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>门店交易量统计</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
        .edit{background-color: #ed9c3a;}
        .shenhe{background-color: #429488;}
        .see{background-color: #7cb717;}
        .tongbu{background-color: #4c9ef8;color:#fff;}
        .cur{color:#009688;}
        .yname{
            font-size: 13px;
            color: #444;
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12" style="margin-top:0px">

            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12">
                        <div class="layui-card">
                            <div class="layui-card-header">门店交易量统计</div>
                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">
                                    <div class="layui-form" style="width:820px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <!-- 缴费时间 -->
                                            <div class="layui-inline">
                                                <div class="layui-input-inline">
                                                    <text class="yname">交易时间</text>
                                                    <input type="text" class="layui-input start-item test-item" placeholder="交易开始时间" lay-key="23">
                                                </div>
                                            </div>
                                            <div class="layui-inline">
                                                <text class="yname">选择排序</text>
                                                <select name="agent" id="agent" lay-filter="agent" lay-search>
                                                    <option value="">默认</option>
                                                    <option value="1" selected="selected">交易量由大到小</option>
                                                    <option value="2">交易量由小到大</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                    <!-- 判断状态 -->
                                    <script type="text/html" id="statusTap">
                                        @{{#  if(d.pay_status == 1){ }}
                                        <span class="cur">@{{ d.pay_status_desc }}</span>
                                        @{{#  } else { }}
                                        @{{ d.pay_status_desc }}
                                        @{{#  } }}
                                    </script>
                                    <!-- 判断状态 -->

                                    <script type="text/html" id="paymoney">
                                        @{{ d.rate }}%
                                    </script>
                                    <script type="text/html" id="table-content-list" class="layui-btn-small">
                                        <!-- <a class="layui-btn layui-btn-normal layui-btn-xs tongbu" lay-event="tongbu">查看</a> -->
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <input type="hidden" class="starttime"><!-- 今天的开始时间 -->
    {{--<input type="hidden" class="endtime"><!-- 今天的开始时间 -->--}}

    <input type="hidden" class="starttimeY"><!-- 昨天的开始时间 -->
    <input type="hidden" class="endtimeY"><!-- 昨天的结束时间 -->

    <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
    <script>
        var token = sessionStorage.getItem("Usertoken");
        var str=location.search;
        var store_id = sessionStorage.getItem("store_store_id");

        layui.config({
            base: '../../layuiadmin/' //静态资源所在路径
        }).extend({
            index: 'lib/index' //主入口模块
        }).use(['index','form','table','laydate'], function(){
            var $ = layui.$
                ,admin = layui.admin
                ,form = layui.form
                ,table = layui.table
                ,laydate = layui.laydate;

            $('.user_id').val(store_id);
            // 未登录,跳转登录页面
            $(document).ready(function(){
                if(token==null){
                    window.location.href="{{url('/user/login')}}";
                }
            });

            laydate.render({
                elem: '.start-item'
                ,range: true
                ,type:'date'
                ,max:'date'//限制结束日期为今天
                ,done: function(value, date, endDate){
                    var StartDay=value.split(" - ")[0];
                    var EndDay=value.split(" - ")[1];
                    if(new Date(StartDay).getMonth() != new Date(EndDay).getMonth()) {
                    alert("该查询不支持跨月查询");
                    return false;
                    }else{
                        table.reload('test-table-page', {
                        where: {
                            time_start:value,
                        }
                        ,page: {
                            curr: 1 //重新从第 1 页开始
                        }
                    });
                    }
                }

            });

            // 渲染表格*********
            table.render({
                elem: '#test-table-page'
                ,url: "{{url('/api/user/volume')}}"
                ,method: 'post'
                ,where:{
                    token:token,
                    // month:months,
                    type:'1',
                    time_start:$('.start-item').val(),
                }
                ,request:{
                    pageName: 'p',
                    limitName: 'l'
                }
                ,page: true
                ,cellMinWidth: 100
                ,cols: [[
                    {align:'center',field:'store_name', title: '门店'}
                    // ,{align:'center',field:'total_amount', title: '总交易金额'}
                    ,{align:'center',field:'count_order', title: '总交易笔数'}

                ]]
                ,response: {
                    statusName: 'status' //数据状态的字段名称，默认：code
                    ,statusCode: 1 //成功的状态码，默认：0
                    ,msgName: 'message' //状态信息的字段名称，默认：msg
                    ,countName: 't' //数据总数的字段名称，默认：count
                    ,dataName: 'data' //数据列表的字段名称，默认：data
                }
                ,done: function(res, curr, count){
                    console.log(res);
                    $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});	//进行表头样式设置
                }
            });


            table.on('tool(test-table-page)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
                var e = obj.data; //获得当前行数据
                var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
                var tr = obj.tr; //获得当前行 tr 的DOM对象
                // console.log(e);
                // sessionStorage.setItem('s_store_id', e.store_id);

                if(layEvent === 'tongbu'){ //审核
                    $.post("{{url('/api/basequery/update_order')}}",
                        {
                            token:token,
                            store_id:e.store_id,
                            out_trade_no:e.out_trade_no
                        },function(res){
                            // console.log(res);
                            if(res.status==1){
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 1
                                    ,time: 2000
                                });
                            }else{
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 2
                                    ,time: 2000
                                });
                            }
                        },"json");
                }

                var data = obj.data;
                if(obj.event === 'setSign'){
                    layer.open({
                        type: 2,
                        title: '模板详细',
                        shade: false,
                        maxmin: true,
                        area: ['60%', '70%'],
                        content: "{{url('/merchantpc/paydetail?')}}"+e.stu_order_type_no
                    });
                }
            });

            // 选择排序
            form.on('select(agent)', function(data){
                var type = data.value;

                //执行重载
                table.reload('test-table-page', {
                    where: {
                        type: type
                    }
                    ,page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            });

        });

    </script>

</body>
</html>





