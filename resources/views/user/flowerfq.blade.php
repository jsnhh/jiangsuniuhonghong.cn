<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>花呗分期成本费率</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
  <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
  <style>
    .edit{background-color: #ed9c3a;}
    .shenhe{background-color: #429488;}    
    .see{background-color: #7cb717;} 
    .cur{color:#009688;}
    .del {background-color: #e85052;}    
    .way{
      height:36px;
      line-height: 36px;
    }
    .xgrate{
        color: #fff;
        font-size: 15px;
        padding: 7px;
        height: 30px;
        line-height: 30px;
        /* border: 1px solid #666; */
        background-color: #3475c3;
    }
  </style>
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">

        <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12" style="margin-top:0px">
              <div class="layui-card"> 
                <div class="layui-card-header">花呗分期成本费率</div>

                <div class="layui-card-body">
                                    
                  <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                  

                  <script type="text/html" id="table-content-list">                    
                    <a class="layui-btn layui-btn-normal layui-btn-xs edit" lay-event="edit">修改</a>
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>


<div id="edit_rate" class="hide" style="display: none;background-color: #fff;">
<div class="xgrate">修改成本费率</div>
  <div class="layui-card-body" style="padding: 15px;">
    <div class="layui-form">
      <div class="layui-form-item">
        <label class="layui-form-label">期数:</label>
        <div class="layui-input-block">
            <div class="way num"></div>
        </div>
      </div>
      <div class="layui-form-item">
        <label class="layui-form-label">成本费率:</label>
        <div class="layui-input-block">
            <input type="number" placeholder="请输入修改费率" class="layui-input rate">
        </div>
      </div>
      <div class="layui-form-item">
        <div class="layui-input-block">
            <div class="layui-footer" style="left: 0;">
                <button class="layui-btn submit">确定</button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script> 
    <script>
    var token = sessionStorage.getItem("Usertoken");
    var agentName = sessionStorage.getItem("agentName");
    // var str=location.search;
    // var id=str.split('?')[1];
    function GetQueryString(name)
    {
      var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
      var r = window.location.search.substr(1).match(reg);
      if(r!=null)return  unescape(r[2]); return null;
    }
    var s_id=GetQueryString("id");
    var user_id=GetQueryString("user_id");
    console.log(s_id)
    console.log(user_id)

    layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form', 'table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,table = layui.table
            ,form = layui.form
            ,laydate = layui.laydate;

        
        // 未登录,跳转登录页面
        $(document).ready(function(){        
            if(token==null){
                window.location.href="{{url('/user/login')}}"; 
            }
        })
        
        
        table.render({
          elem: '#test-table-page'
          ,url: "{{url('/api/alipayopen/user_rate')}}"
          ,method: 'post'
          ,where:{
            token:token,user_id:user_id            
          }
          ,request:{
            pageName: 'p', 
            limitName: 'l'
          }
          ,page: true
          ,cellMinWidth: 150
          ,cols: [[                
              {field:'num', title: '期数'}
              ,{field:'rate',  title: '成本费率'}       
              ,{width:120,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
          ]]
          ,response: {
              statusName: 'status' //数据状态的字段名称，默认：code
              ,statusCode: 1 //成功的状态码，默认：0
              ,msgName: 'message' //状态信息的字段名称，默认：msg
              ,countName: 't' //数据总数的字段名称，默认：count
              ,dataName: 'data' //数据列表的字段名称，默认：data
            } 
          ,done: function(res, curr, count){              
            console.log(res); 
            $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});	//进行表头样式设置
          }

        });


        table.on('tool(test-table-page)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
          var e = obj.data; //获得当前行数据
          var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
          var tr = obj.tr; //获得当前行 tr 的DOM对象
          console.log(e);
          // sessionStorage.setItem('s_store_id', e.store_id);

          if(layEvent === 'edit'){
            $('.num').html(e.num)
            $('.rate').val(e.rate)
            layer.open({
              type: 1,
              title: false,
              closeBtn: 0,
              area: '516px',
              skin: 'layui-layer-nobg', //没有背景色
              shadeClose: true,
              content: $('#edit_rate')
            });            
          }
          
        });

        $('.submit').click(function(){
          $.post("{{url('/api/alipayopen/set_user_rate')}}",
          {
              token:token,
              user_id:user_id,
              num:$('.num').html(),
              rate:$('.rate').val(),
          },function(data){
            console.log(data);
            if(data.status==1){              
              layer.msg(data.message, {
                offset: '50px'
                ,icon: 1
                ,time: 1000
              },function(){
                window.location.reload();
              });
            }else{
              layer.msg(data.message, {
                offset: '50px'
                ,icon: 2
                ,time: 3000
              });
            }
          },"json");
        })

    });


  </script>

</body>
</html>