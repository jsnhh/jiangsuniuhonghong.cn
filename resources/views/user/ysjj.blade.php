<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>补充资料</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
    <style>

        .termModeNone {
            display: none;
        }

        .filebox {
            width: 100px;
            height: 100px;
            /* background-color: #eeeeee; */
            border: 1.5px solid #9a9a9a;
            position: relative;
        }

        .filebox input[type="file"] {
            position: absolute;
            left: 0;
            top: 0;
            display: block;
            opacity: .01;
            width: 100px;
            height: 30px;
        }

        .imgImgNone {
            display: none;
        }

        .dCalcTypenone {
            display: none;
        }

        .banksclassfass {
            position: relative;

        }

        .banksclass {
            position: absolute;
            width: 100%;
            left: 0;
            top: 38px;

            z-index: 9999999999;
            height: 240px;
            overflow-y: auto;

        }

        .banksclass ul li {
            font-size: 14px;
            padding: 10px 6px;
            border-bottom: 1px solid #eeeeee;
            background-color: #ffffff;
            cursor: pointer
        }

        .banksclassfass2 {
            position: relative;

        }

        .banksclass2 {
            position: absolute;
            width: 100%;
            left: 0;
            top: 38px;

            z-index: 9999999999;
            height: 240px;
            overflow-y: auto;

        }

        .banksclass2 ul li {
            font-size: 14px;
            padding: 10px 6px;
            border-bottom: 1px solid #eeeeee;
            background-color: #ffffff;
            cursor: pointer
        }

        .banksclassnone {
            display: none;
        }

        .dCalcTypenone2 {
            display: none;
        }

        .banksclassfass2 {
            margin-bottom: 10px;
        }

        .banksclassnone2 {
            display: none;


        }

        .dStlmTyperadioclick {
            display: none;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
        }

        input[type="number"] {
            -moz-appearance: textfield;
        }

        .termModelbox ul li {
            margin: 10px 10px 10px;
            border-bottom: 1px solid #eeeeee;
            padding-bottom: 10px;
            font-size: 12px;
            cursor: pointer;
        }

        .termModelboxdivnone {
            display: none;
        }

        .twoBarCodeNone {
            display: none;
        }

        .twoBarCodeBtn {
            width: 64px;
            height: 30px;
            line-height: 30px;
            border-radius: 4px;
            background-color: #3475c3;
            font-size: 12px;
            color: #ffffff;
            cursor: pointer;
            text-align: center;
        }

        .switchtimemoneycirtypenone {
            display: none;
        }
    </style>
</head>
<body>
<div class="layui-fluid" style="background-color: #ffffff;">
    <div class="layui-card" style="margin-top: 0px;overflow:hidden;">
        <div class="layui-card-header">门店信息</div>

        <form class="layui-form" action="" style="margin: 0px auto;width:870px;">
            <div style="width: 600px;float: left;">

                <div class="layui-form-item" style="width:500px;margin-top: 20px;">
                    <label class="layui-form-label" style="text-align:center;">项目归属</label>
                    <div class="layui-input-block">
                        <select name="projectId" id="projectId" lay-filter="projectId" lay-search>
                            <option value="">请选择</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item" style="width:500px;margin-top: 20px;">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>分类名称</label>
                    <div class="layui-input-block">
                        <select name="mccId" id="mccId" lay-filter="mccId" lay-search>
                            <option value="">请选择</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>行业大类</label>
                    <div class="layui-input-block">
                        <select name="standardFlag" id="standardFlag" lay-filter="standardFlag">
                            <option value="">请选择</option>
                            <option value="0">标准</option>
                            <option value="1">优惠</option>
                            <option value="2">减免</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>公司规模</label>
                    <div class="layui-input-block">
                        <select name="employeeNum" id="employeeNum" lay-filter="employeeNum">
                            <option value="">请选择</option>
                            <option value="1">0-50人</option>
                            <option value="2">50-100人</option>
                            <option value="3">100以上</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>经营形态</label>
                    <div class="layui-input-block">
                        <select name="businForm" id="businForm" lay-filter="businForm">
                            <option value="">请选择</option>
                            <option value="02">普通店</option>
                            <option value="01">连锁店</option>
                        </select>
                    </div>
                </div>


                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>营业时间</label>
                    <div class="layui-input-block">
                        <input type="text" name="businBegTime" class="layui-input" id="time1" placeholder="开始时分">
                        <!-- <input type="text" name="businBegTime" placeholder="开始时间" id="time1" class="layui-input item3"> -->
                    </div>
                </div>
                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>营业时间</label>
                    <div class="layui-input-block">
                        <input type="text" name="businEndTime" class="layui-input" id="time2" placeholder="结束时分">
                        <!-- <input type="text" name="businEndTime" placeholder="结束时间" id="time2" class="layui-input item3"> -->
                    </div>
                </div>

                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>终端种类</label>
                    <div class="layui-input-block">
                        <select name="termMode" id="termMode" lay-filter="termMode">
                            <!-- <option value="0">POS终端</option> -->
                            <option value="2">虚拟终端</option>
                            <option value="4">银联POS</option>
                            <option value="5">其他POS</option>
                            <option value="6">二维码终端</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item termModeNoneName termModeNone" style="width:500px">
                    <div class="layui-input-block">
                        <input type="text" name="bangDingCommerCode" placeholder="渠道商户号" class="layui-input item3">
                    </div>
                </div>
                <div class="layui-form-item termModeNoneName termModeNone" style="width:500px">
                    <div class="layui-input-block">
                        <input type="text" name="bangDingTerMno" placeholder="渠道终端号" class="layui-input item3">
                    </div>
                </div>


                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>内部终端号</label>
                    <div class="layui-input-block">
                        <input type="text" name="termCode" placeholder="内部终端号" class="layui-input item3">
                    </div>
                </div>
                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>终端名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="terUserName" placeholder="终端名称" class="layui-input item3">
                    </div>
                </div>


                <!--<div class="layui-form-item" style="width:500px">
                <label class="layui-form-label" style="text-align:center;"><span style="color: red;margin-right: 4px;">*</span>区号</label>
                <div class="layui-input-block">
                    <select name="areaNo"  id="areaNo" lay-filter="areaNo">
                      <option value="">请选择区号</option>
                    </select>
                     <input type="text" name="areaNo" placeholder="区号11111111111111" class="layui-input item3">
                </div>
            </div>-->


                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>终端地区</label>
                    <div class="layui-input-block">
                        <select name="termArea1" id="termArea1" lay-filter="termArea1">
                            <option value="">请选择地区</option>
                        </select>
                    </div>
                    <div class="layui-input-block" style="margin-top: 10px;">
                        <select name="termArea2" id="termArea2" lay-filter="termArea2">
                            <option value="">请选择地区</option>
                        </select>
                    </div>
                    <div class="layui-input-block" style="margin-top: 10px;">
                        <select name="termArea" id="termArea" lay-filter="termArea">
                            <option value="">请选择地区</option>
                        </select>
                        <input type="hidden" id="areaNo" name="areaNo">
                    </div>
                </div>


                <!-- ************************************************************************************* -->
                <div class="layui-form-item termModelbox" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>设备型号</label>
                    <div class="layui-input-block" style="position: relative;">
                        <input type="text" id="termModel" name="termModel" placeholder="设备型号" class="layui-input item3">
                        <div class="termModelboxdiv termModelboxdivnone"
                             style="position: absolute;left:0;top:40px;z-index:9999999;width:100%;background:#ffffff;height:200px;overflow-y: auto;box-shadow: 0 2px 4px rgb(0 0 0 / 12%);">
                            <ul>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>机具号</label>
                    <div class="layui-input-block">
                        <input type="text" name="termModelLic" placeholder="机具号" class="layui-input item3">
                    </div>
                </div>

                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>主营业务</label>
                    <div class="layui-input-block">
                        <!-- <input type="text" name="businScope" placeholder="主营业务111111111111111111" class="layui-input item3"> -->
                        <div style="margin-bottom: 10px;">
                            <select name="businScope1" id="businScope1" lay-filter="businScope1">
                                <option value="">请选择</option>
                            </select>
                        </div>
                        <div style="margin-bottom: 10px;">
                            <select name="businScope2" id="businScope2" lay-filter="businScope2">
                                <option value="">请选择</option>
                            </select>
                        </div>
                        <div>
                            <select name="businScope" id="businScope3" lay-filter="businScope3">
                                <option value="">请选择</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>注册资本</label>
                    <div class="layui-input-block">
                        <input type="number" name="capital" placeholder="注册资本" class="layui-input item3">
                    </div>
                </div>

                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>开户行名称</label>
                    <div class="layui-input-block" class="banksclassfass">
                        <input type="text" id="bankName" name="bankName" placeholder="开户行支行" class="layui-input item3">
                        <div class="banksclass banksclassnone">
                            <ul>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- <div class="layui-form-item" style="width:500px">
                <label class="layui-form-label" style="text-align:center;"><span style="color: red;margin-right: 4px;">*</span>开户行行号</label>
                <div class="layui-input-block">
                    <input type="text" id="bankZongName" name="bankZongName" placeholder="总行" class="layui-input item3">
                </div>
            </div> -->
                <div class="layui-form-item" style="width:500px">
                    <!-- <label class="layui-form-label" style="text-align:center;"><span style="color: red;margin-right: 4px;">*</span>开户行行号</label> -->
                    <div class="layui-input-block">
                        <input type="text" id="bankCode" name="bankCode" placeholder="行号" class="layui-input item3">
                    </div>
                </div>


                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;">企业对私结算</label>
                    <div class="layui-input-block">
                        <input type="checkbox" name="realTimeEntry2" lay-skin="switch" lay-filter="switchtimemoney2">
                    </div>
                    <div class="layui-input-block dCalcTypeClass2 dCalcTypenone2">
                        <div class="banksclassfass2">
                            <input type="text" id="pubBankName" name="pubBankName" placeholder="第二开户行支行"
                                   class="layui-input item3">
                            <div class="banksclass2 banksclassnone2">
                                <ul>
                                </ul>
                            </div>
                        </div>
                        <!-- <div style="margin-bottom: 10px;">
                      <input type="text" id="pubBankZongName" name="pubBankZongName" placeholder="第二开户行总行" class="layui-input item3">
                  </div> -->
                        <div style="margin-bottom: 10px;">
                            <input type="text" id="pubBankCode" name="pubBankCode" placeholder="第二开户行行号"
                                   class="layui-input item3">
                        </div>
                        <div style="margin-bottom: 10px;">
                            <input type="text" id="pubAccount" name="pubAccount" placeholder="第二开户行账号"
                                   class="layui-input item3">
                        </div>
                        <div style="margin-bottom: 10px;">
                            <input type="text" id="pubAccName" name="pubAccName" placeholder="第二开户行账号名称"
                                   class="layui-input item3">
                        </div>
                    </div>
                </div>


                <div class="layui-form-item  dCalcTypeClass2 dCalcTypenone2" style="width:500px">

                    <div class="layui-input-block">
                        <span>请上传开户许可证</span>
                        <div class="filebox">
                            <img src="" class="imgImg6 imgImgNone" style="width:100px;height:100px;">

                            <input type="file" name="file" class="uploadFile" id="uploadFile6"
                                   style="width:100px;height:100px;">
                            <input type="hidden" name="store_industrylicense_img" value="">
                            <input type="hidden" name="store_imgs" value="">
                        </div>

                    </div>
                </div>


                <!-- ------------------------------------------------------------------------------------------------------------------------------------ -->
                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;">立招功能</label>
                    <div class="layui-input-block">
                        <input type="checkbox" name="twoBarsOpen" lay-skin="switch" lay-filter="twoBarsOpen">
                    </div>
                    <div class="layui-input-block twoBarCode twoBarCodeNone">
                        <div style="margin-top: 10px;" class="inputbox">
                            <div>
                                <input type="text" maxlength="30" style="margin-bottom: 10px;" name="twoBarCode1"
                                       placeholder="请输入二维码编号" class="layui-input item3">
                                <input type="text" maxlength="60" style="margin-bottom: 10px;" name="termName1"
                                       placeholder="请输入二维码名称" class="layui-input item3">
                            </div>
                        </div>
                        <div class="twoBarCodeBtn">
                            新增
                        </div>
                    </div>
                </div>

                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>商户区域</label>
                    <div class="layui-input-block" style="margin-bottom: 10px;">
                        <select name="merArea1" id="merArea1" lay-filter="merArea1">
                            <option value="">请选择</option>
                        </select>
                    </div>
                    <div class="layui-input-block" style="margin-bottom: 10px;">
                        <select name="merArea2" id="merArea2" lay-filter="merArea2">
                            <option value="">请选择</option>
                        </select>
                    </div>
                    <div class="layui-input-block">
                        <select name="merArea" id="merArea" lay-filter="merArea">
                            <option value="">请选择</option>
                        </select>
                    </div>
                </div>


                <div class="layui-form-item" style="width:500px;display:flex;">
                    <div>
                        <label class="layui-form-label" style="text-align:center;">实时入账</label>
                        <div class="layui-input-block">
                            <input type="checkbox" name="realTimeEntry" lay-skin="switch" lay-filter="switchtimemoney">
                        </div>
                    </div>
                    <div>
                        <label class="layui-form-label" style="text-align:center;">次日到账</label>
                        <div class="layui-input-block">
                            <input type="checkbox" name="d1IsOpen" lay-skin="switch" lay-filter="switchtimemoneycir">
                        </div>
                    </div>
                    <!-- <div class="layui-input-block dCalcTypeClass dCalcTypenone">
                  <input type="radio" name="dCalcType" value="0" title="否" checked>
                  <input type="radio" name="dCalcType" value="1" title="是">
                </div> -->
                </div>

                <div class="layui-form-item dCalcTypeClass dCalcTypenone" style="width:500px;">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>借记卡计费类型</label>
                    <div class="layui-input-block">
                        <input type="radio" name="dCalcType" value="0" title="按笔数(单位:元)" checked>
                        <input type="radio" name="dCalcType" value="1" title="按比例(单位:%)">
                    </div>
                </div>
                <div class="layui-form-item dCalcTypeClass dCalcTypenone" style="width:500px;">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>信用卡计费类型</label>
                    <div class="layui-input-block">
                        <input type="radio" name="cCalcType" value="0" title="按笔数(单位:元)" checked>
                        <input type="radio" name="cCalcType" value="1" title="按比例(单位:%)">
                    </div>
                </div>
                <!-- 次日到账 -->
                <div class="layui-form-item switchtimemoneycirtype switchtimemoneycirtypenone" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>手续费</label>
                    <div class="layui-input-block">
                        <input type="number" name="calcVal" placeholder="手续费" class="layui-input item3">
                    </div>
                </div>
                <div class="layui-form-item switchtimemoneycirtype switchtimemoneycirtypenone" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>费用处理模式</label>
                    <div class="layui-input-block">
                        <select name="payDateType" id="payDateType" lay-filter="payDateType">
                            <option value="0">每天计费</option>
                            <option value="1">节假日</option>
                            <option value="2">节假日按天</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item  switchtimemoneycirtype switchtimemoneycirtypenone" style="width:500px;">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>计算方式</label>
                    <div class="layui-input-block">
                        <input type="radio" name="calcType" value="0" title="按笔数(单位:元)">
                        <input type="radio" name="calcType" value="1" title="按比例(单位:%)" checked>
                    </div>
                </div>
                <!-- 次日到账 -->
                <div class="layui-form-item" style="width:500px;">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>借记卡扣率方式</label>
                    <div class="layui-input-block">
                        <input type="radio" name="dStlmType" lay-filter="dStlmTyperadio" lay value="1" title="封顶"
                               checked>
                        <input type="radio" name="dStlmType" lay-filter="dStlmTyperadio" value="0" title="不封顶">
                    </div>
                </div>
                <div class="layui-form-item dStlmTyperadioclickclass" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>借记卡封顶金额</label>
                    <div class="layui-input-block">
                        <input type="number" name="dStlmMaxAmt" placeholder="借记卡封顶金额" class="layui-input item3">
                    </div>
                </div>


                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>借记卡扣率</label>
                    <div class="layui-input-block">
                        <input type="number" onkeyup="value=value.replace(/^|[^\d.]+/g,'')" name="dCalcVal"
                               placeholder="借记卡扣率" class="layui-input item3">
                    </div>
                </div>

                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>借记卡手续费最低值</label>
                    <div class="layui-input-block">
                        <input type="number" name="dFeeLowLimit" placeholder="借记卡手续费最低值" class="layui-input item3">
                    </div>
                </div>


                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>信用卡扣率</label>
                    <div class="layui-input-block">
                        <input type="number" onkeyup="value=value.replace(/^|[^\d.]+/g,'')" name="cCalcVal"
                               placeholder="信用卡扣率" class="layui-input item3">
                    </div>
                </div>

                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>信用卡手续费最低值</label>
                    <div class="layui-input-block">
                        <input type="number" name="cFeeLowLimit" placeholder="信用卡手续费最低值" class="layui-input item3">
                    </div>
                </div>

                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>电子协议编号</label>
                    <div class="layui-input-block">
                        <input type="text" name="contractNo" id="contractNo" placeholder="电子协议编号" class="layui-input item3">
                    </div>
                </div>

                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>签署方名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="signatoryName" id="signatoryName" placeholder="签署方名称" class="layui-input item3">
                    </div>
                </div>

                <div class="layui-form-item" style="width:500px">
                    <button class="layui-btn" lay-submit lay-filter="save"
                            style="background: #1E9FFF;margin-top: 15px;float: right;">保存
                    </button>
                </div>


            </div>

            <div style="width: 260px;float: left;margin-top: 20px;">
                <div class="layui-form-item" style="width:500px;">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>门牌号图片</label>
                    <div class="layui-input-block">
                        <div class="filebox">
                            <img src="" class="imgImg1 imgImgNone" style="width:100px;height:100px;">
                            <input type="file" name="file" class="uploadFile" id="uploadFile1"
                                   style="width:100px;height:100px;"/>
                            <input type="hidden" name="houseNumber" value=""/>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item" style="width:250px;float:left;">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>协议图片</label>
                    <div class="layui-input-block">
                        <div class="filebox">
                            <img src="" class="imgImg2 imgImgNone" style="width:100px;height:100px;">
                            <input type="file" name="file" class="uploadFile" id="uploadFile2"
                                   style="width:100px;height:100px;"/>
                            <input type="hidden" name="agreement" value=""/>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>商户登记表正面图片</label>
                    <div class="layui-input-block">
                        <div class="filebox">
                            <img src="" class="imgImg3 imgImgNone" style="width:100px;height:100px;">
                            <input type="file" name="file" class="uploadFile" id="uploadFile3"
                                   style="width:100px;height:100px;"/>
                            <input type="hidden" name="mercRegistFormA" value=""/>
                        </div>
                    </div>
                </div>


                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>商户登记表反面图片</label>
                    <div class="layui-input-block">
                        <div class="filebox">
                            <img src="" class="imgImg4 imgImgNone" style="width:100px;height:100px;">
                            <input type="file" name="file" class="uploadFile" id="uploadFile4"
                                   style="width:100px;height:100px;"/>
                            <input type="hidden" name="mercRegistFormB" value=""/>
                        </div>
                    </div>
                </div>


                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center;"><span
                                style="color: red;margin-right: 4px;">*</span>清算授权书图片</label>
                    <div class="layui-input-block">
                        <div class="filebox">
                            <img src="" class="imgImg5 imgImgNone" style="width:100px;height:100px;">
                            <input type="file" name="file" class="uploadFile" id="uploadFile5"
                                   style="width:100px;height:100px;"/>
                            <input type="hidden" name="certOfAuth" value=""/>
                        </div>
                    </div>
                </div>
            </div>

        </form>

    </div>
</div>

<script src="/layuiadmin/layui/layui.js"></script>
<script>
    //获取 token ，用户登录是就会有
    var token = window.sessionStorage.getItem("Usertoken");
    var storeid = window.location.search;
    var store_id = storeid.substring(1, storeid.length)
    console.log(store_id)
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form', 'upload', 'formSelects', 'laydate', 'jquery'], function () {
        var $ = layui.$
            , admin = layui.admin
            , element = layui.element
            , layer = layui.layer
            , laydate = layui.laydate
            , form = layui.form
            , upload = layui.upload
            , formSelects = layui.formSelects;
        var $ = jquery = layui.jquery;
        // 未登录,跳转登录页面
        $(document).ready(function () {
            if (token == null) {
                window.location.href = "{{url('/user/login')}}";
            }
        });
        if (!store_id) {
            layer.msg("获取门店id失败", {icon: 2, shade: 0.5, time: 1000});
        }

        //获取项目ID 接口
        function getProjectId() {
            return new Promise((resolve) => {
                $.post("/api/user/getProjectId", {
                    p: 1,
                    l: 400,
                }, function (data) {
                    resolve(data);
                })
            })
        }

        //获取区号 3.1 接口
        function getAreaNo(code) {
            return new Promise((resolve) => {
                $.post("/api/user/getAreaNo", {
                    area_code: code,
                    p: 1,
                    l: 1000000
                }, function (data) {
                    resolve(data)
                })
            })
        }

        //获取mcc 3.3 接口
        function getMcc() {
            return new Promise((resolve) => {
                $.post("/api/user/getMcc", {
                    p: 1,
                    l: 400,
                }, function (data) {
                    resolve(data);
                })
            })
        }

        //获取银行信息 1-总行；2-支行 3.2 接口
        function getBanks(banktype, bankname) {
            return new Promise((resolve) => {
                $.post("/api/user/getBanks", {
                    type: banktype,
                    name: bankname
                }, function (data) {
                    resolve(data);
                })
            })
        }

        //数据回显
        function datahuixian() {
            return new Promise(resolve => {
                //数据回显
                $.post("/api/user/easyPayMerchantAccess", {
                    token: token,
                    storeId: store_id,
                    type: 2
                }, function (data) {
                    resolve(data);
                })
            })
        }

        // 门店分类
        function vbillstorecategory() {
            return new Promise((resolve) => {
                $.post("/api/user/getMcc", {
                    p: 1,
                    l: 400,
                }, function (data) {
                    resolve(data);
                })
            })
        }

        //回显电子协议信息
        function EchoInfo(){
            //数据回显
            $.ajax({
                url: "{{url('/api/user/getContract')}}",
                data: {
                    token: token,
                    storeId: store_id,
                    txCode: '3210'
                },
                type: 'post',
                success: function(data) {
                    if(data.status == 1){
                        if(data.data.contractState == 1){ //签署完成
                            $('input[name="contractNo"]').val(data.data.contractNo);
                            $('input[name="signatoryName"]').val(data.data.signatoryName);
                        }
                    }
                },
                error: function(data) {

                }
            });
        }


        //一进来就会 加载的数据
        (function () {
            datahuixian().then(v => {
                if (v.status == 1) {
                    let reslist = v.data;
                    if (reslist == null) {
                       //回显电子协议信息
                        EchoInfo();

                        allDataList();
                    } else {
                        //************************************ */
                        let busin_beg_timeobj = reslist.busin_beg_time;
                        let busin_beg_timeobj1 = busin_beg_timeobj.substring(0, 2)
                        let busin_beg_timeobj2 = busin_beg_timeobj.substring(2, 4)
                        let busin_beg_timeobjnew = busin_beg_timeobj1 + ":" + busin_beg_timeobj2;

                        let busin_end_timeobj = reslist.busin_end_time;
                        let busin_end_timeobj1 = busin_end_timeobj.substring(0, 2)
                        let busin_end_timeobj2 = busin_end_timeobj.substring(2, 4)
                        let busin_end_timeobjnew = busin_end_timeobj1 + ":" + busin_end_timeobj2;
                        $("#time1").val(busin_beg_timeobjnew);
                        $("#time2").val(busin_end_timeobjnew);

                        $('input[name="termCode"]').val(reslist.term_code);
                        $('input[name="terUserName"]').val(reslist.ter_user_name);
                        $("#termModel").val(reslist.term_model);
                        $('input[name="termModelLic"]').val(reslist.term_model_lic);
                        $('input[name="capital"]').val(reslist.capital);
                        $("#bankName").val(reslist.bank_name);
                        $("#bankCode").val(reslist.bank_code);
                        $("#pubBankName").val(reslist.pub_bank_name);
                        $("#pubBankCode").val(reslist.pub_bank_code);
                        $("#pubAccount").val(reslist.pub_account);
                        $("#pubAccName").val(reslist.pub_acc_name);
                        $('input[name="dStlmMaxAmt"]').val(reslist.d_stlm_max_amt);
                        $('input[name="dCalcVal"]').val(reslist.d_calc_val);
                        $('input[name="dFeeLowLimit"]').val(reslist.d_fee_low_limit);
                        $('input[name="cCalcVal"]').val(reslist.c_calc_val);
                        $('input[name="cFeeLowLimit"]').val(reslist.c_fee_low_limit);
                        $('input[name="contractNo"]').val(reslist.contract_no);
                        $('input[name="signatoryName"]').val(reslist.signatory_name);

                        // 立招 回显
                        if (reslist.two_bars != "") {

                            let twoBarsOpenList = JSON.parse(reslist.two_bars);
                            console.log(twoBarsOpenList)
                            if (twoBarsOpenList.length > 0) {
                                layui.jquery('input[name="twoBarsOpen"]').attr('checked', 'checked');
                                $(".twoBarCode").removeClass("twoBarCodeNone");
                                var twoBarsOpenStr = "";
                                for (let t = 0; t < twoBarsOpenList.length; t++) {
                                    let tdata = t + 1;
                                    twoBarsOpenStr += `<div>
                                        <input type="text" maxlength="30" style="margin-bottom: 10px;" name="twoBarCode` + tdata + `" value="` + twoBarsOpenList[t].twoBarCode + `" class="layui-input item3">
                                        <input type="text" maxlength="60" style="margin-bottom: 10px;" name="termName` + tdata + `" value="` + twoBarsOpenList[t].termName + `" class="layui-input item3">
                                      </div>`;
                                }
                                $(".inputbox").html(twoBarsOpenStr);
                            }

                        }

                        // function isArray(a) {
                        //   let str1 = '二维'
                        //   let str2 = '一维'
                        //   for (let i of a) {
                        //     if (Array.isArray(i))
                        //       return str1
                        //   }
                        //   return str2
                        // }
                        // console.log(isArray(twoBarsOpenList))

                        // if(isArray(twoBarsOpenList) == "一维"){
                        //   layui.jquery('input[name="twoBarsOpen"]').attr('checked', 'checked');
                        //   $(".twoBarCode").removeClass("twoBarCodeNone");
                        //   var twoBarsOpenStr = `<div>
                        //                         <input type="text" maxlength="30" style="margin-bottom: 10px;" name="twoBarCode1" value="`+twoBarsOpenList[0].twoBarCode+`" class="layui-input item3">
                        //                         <input type="text" maxlength="60" style="margin-bottom: 10px;" name="termName1" value="`+twoBarsOpenList[1].termName+`" class="layui-input item3">
                        //                       </div>`;
                        //   $(".inputbox").html(twoBarsOpenStr);
                        // }else if(isArray(twoBarsOpenList) == "二维"){
                        //   layui.jquery('input[name="twoBarsOpen"]').attr('checked', 'checked');
                        //   $(".twoBarCode").removeClass("twoBarCodeNone");
                        //   var twoBarsOpenStr = "";
                        //   for(let t=0;t<twoBarsOpenList.length;t++){
                        //     let tdata = t + 1;
                        //     twoBarsOpenStr += `<div>
                        //                         <input type="text" maxlength="30" style="margin-bottom: 10px;" name="twoBarCode`+tdata+`" value="`+twoBarsOpenList[t][0].twoBarCode+`" class="layui-input item3">
                        //                         <input type="text" maxlength="60" style="margin-bottom: 10px;" name="termName`+tdata+`" value="`+twoBarsOpenList[t][1].termName+`" class="layui-input item3">
                        //                       </div>`;
                        //   }
                        //   $(".inputbox").html(twoBarsOpenStr);
                        // }
                        // 企业对私结算
                        if (reslist.pub_bank_name) {
                            layui.jquery('input[name="realTimeEntry2"]').attr('checked', 'checked');
                            $(".dCalcTypeClass2").removeClass("dCalcTypenone2");
                        }
                        // 实时入账
                        if (reslist.real_time_entry == 1) {
                            layui.jquery('input[name="realTimeEntry"]').attr('checked', 'checked');
                            $(".dCalcTypeClass").removeClass("dCalcTypenone");
                            if (reslist.d_calc_type == 1) {
                                $('input[name="dCalcType"][value="1"]').attr('checked', 'checked')
                            } else if (reslist.d_calc_type == 0) {
                                $('input[name="dCalcType"][value="0"]').attr('checked', 'checked')
                            }
                            if (reslist.c_calc_type == 1) {
                                $('input[name="cCalcType"][value="1"]').attr('checked', 'checked')
                            } else if (reslist.c_calc_type == 0) {
                                $('input[name="cCalcType"][value="0"]').attr('checked', 'checked')
                            }
                        }

                        // 开启 D1 回显
                        if (reslist.d1_is_open == 1) {
                            // 开启 次日到账
                            layui.jquery('input[name="d1IsOpen"]').prop("checked", true);
                            $(".switchtimemoneycirtype").removeClass("switchtimemoneycirtypenone");
                            $('input[name="calcVal"]').val(reslist.calc_val);

                            $("#payDateType").find("option[value=" + reslist.pay_date_type + "]").prop("selected", true);

                            if (reslist.calc_type == 1) {
                                $('input[name="calcType"][value="1"]').attr('checked', 'checked')
                            } else if (reslist.calc_type == 0) {
                                $('input[name="calcType"][value="0"]').attr('checked', 'checked')
                            }

                        }

                        // 借记卡扣率方式
                        if (reslist.d_stlm_type == 1) {
                            $('input[name="dStlmType"][value="1"]').attr('checked', 'checked')
                        } else if (reslist.d_stlm_type == 0) {
                            $('input[name="dStlmType"][value="0"]').attr('checked', 'checked')
                        }
                        //图片回显
                        // 门牌号图片
                        $(".imgImg1").attr("src", reslist.house_number_url)
                        $(".imgImg1").removeClass("imgImgNone")
                        $('input[name="houseNumber"]').val(reslist.house_number_url)
                        // 协议图片
                        $(".imgImg2").attr("src", reslist.agreement_url)
                        $(".imgImg2").removeClass("imgImgNone")
                        $('input[name="agreement"]').val(reslist.agreement_url)
                        // 商户登记表正面图片
                        $(".imgImg3").attr("src", reslist.merc_regist_form_a_url)
                        $(".imgImg3").removeClass("imgImgNone")
                        $('input[name="mercRegistFormA"]').val(reslist.merc_regist_form_a_url)
                        // 商户登记表反面图片
                        $(".imgImg4").attr("src", reslist.merc_regist_form_a_url)
                        $(".imgImg4").removeClass("imgImgNone")
                        $('input[name="mercRegistFormB"]').val(reslist.merc_regist_form_b_url)
                        // 清算授权书图片
                        $(".imgImg5").attr("src", reslist.cert_of_auth_url)
                        $(".imgImg5").removeClass("imgImgNone")
                        $('input[name="certOfAuth"]').val(reslist.cert_of_auth_url)

                        //selet 回显
                        $("#standardFlag").find("option[value=" + reslist.standard_flag + "]").prop("selected", true);
                        $("#employeeNum").find("option[value=" + reslist.employee_num + "]").prop("selected", true);
                        $("#businForm").find("option[value=" + reslist.busin_form + "]").prop("selected", true);
                        $("#termMode").find("option[value=" + reslist.term_mode + "]").prop("selected", true);

                        // $("#termArea").html('<option value="'+reslist.term_area+'">'+reslist.term_area+'</option>');
                        // $("#businScope2").html('<option value="'+reslist.busin_scope+'">'+reslist.busin_scope+'</option>');
                        // $("#merArea").html('<option value="'+reslist.mer_area+'">'+reslist.mer_area+'</option>');

                        //重新渲染 checkbox
                        // layui.form.render('checkbox');
                        form.render();
                        allDataList(reslist);
                    }
                } else {
                    allDataList();
                }

            })
        }())

        //异步函数
        async function allDataList(reslist) {
            //易生项目ID
            let dataProject = await getProjectId();
            if (dataProject.status == 1) {
                //将默认数据放到第一个 select 中
                let dataProjectlist = dataProject.data;
                let projectsrc = "";
                for (let a = 0; a < dataProjectlist.length; a++) {
                    projectsrc += '<option value="' + dataProjectlist[a].project_id + '">' + dataProjectlist[a].project_name + '</option>';
                }
                $("#projectId").append(projectsrc);
                if (reslist) {
                    if (reslist.project_id) {
                        $("#projectId").find("option[value=" + reslist.project_id + "]").prop("selected", true);
                    }
                }
                form.render("select");
            } else {
                layer.msg(dataProject.message, {icon: 2, shade: 0.5, time: 1000});
            }

            //分类名称
            let datavbillstorecategory = await vbillstorecategory();
            if (datavbillstorecategory.status == 1) {
                let listdata = datavbillstorecategory.data;
                let strdatavbillstorecategory = "";
                for (let i = 0; i < listdata.length; i++) {
                    if( Number(listdata[i].mcc_id) ){
                        strdatavbillstorecategory += '<option value="' + listdata[i].mcc_id + '">' + listdata[i].mcc_name + '</option>';
                    }
                }
                $("#mccId").append(strdatavbillstorecategory);
                if (reslist) {
                    $("#mccId").find("option[value=" + reslist.mcc_id + "]").prop("selected", true);
                }
                form.render("select");
            } else {
                layer.msg(datavbillstorecategory.message, {icon: 2, shade: 0.5, time: 1000});
            }

            //3.3 接口
            let dataMcc = await getMcc();
            if (dataMcc.status == 1) {
                //将默认数据放到第一个 select 中
                let dataMcclist = dataMcc.data;
                let Mccsrc = "";
                for (let a = 0; a < dataMcclist.length; a++) {
                    if (dataMcclist[a].mcc_parent == "0000") {
                        Mccsrc += '<option value="' + dataMcclist[a].mcc_id + '">' + dataMcclist[a].mcc_name + '</option>';
                    }
                }
                $("#businScope1").append(Mccsrc);
                form.render("select");
            } else {
                layer.msg(dataMcc.message, {icon: 2, shade: 0.5, time: 1000});
            }


            //3.1 接口将区号放到 select 中----一开始就会获取
            let data = await getAreaNo(0);
            if (data.status == 1) {
                let datalist = data.data;
                let src = "";
                let src2 = "";
                let src3 = "";
                for (let i = 0; i < datalist.length; i++) {
                    src += '<option value="' + datalist[i].code + '">' + datalist[i].code + '</option>';
                    src2 += '<option value="' + datalist[i].id + '">' + datalist[i].yl_pr_name + '</option>';
                    src3 += '<option value="' + datalist[i].id + '">' + datalist[i].yl_pr_name + '</option>';
                }
                $("#areaNo").append(src);
                $("#merArea1").append(src2);
                $("#termArea1").append(src3);
                form.render("select");
            } else {
                layer.msg(data.message, {icon: 2, shade: 0.5, time: 1000});
            }
        }

        //点击新增
        $(".twoBarCodeBtn").click(() => {
            let num = $(".twoBarCode .inputbox div").length + 1;
            let str = '<div><input type="text" style="margin-bottom: 10px;" name="twoBarCode' + num + '" placeholder="请输入二维码编号" class="layui-input item3"><input type="text" style="margin-bottom: 10px;" name="termName' + num + '" placeholder="请输入二维码名称" class="layui-input item3"></div>';
            $(".twoBarCode .inputbox").append(str)
        })
        //var 要在外面
        //银行搜索
        var timer;
        var alertObjalertObj;
        $("#bankName").bind('input propertychange', function () {
            let val = $(this).val();
            clearTimeout(timer);
            timer = setTimeout(function () {
                if (val !== "") {
                    let banktype = 2;
                    $(".banksclass ul").html("")
                    getBanks(banktype, val).then(res => {

                        if (res.status == 1) {
                            if (res.data.length == 0) {
                                $(".banksclass").addClass("banksclassnone");
                            } else {
                                let strli = "";
                                let datalist = res.data;
                                for (let i = 0; i < datalist.length; i++) {
                                    strli += '<li data-bank="' + datalist[i].bank_no + '" data-name="' + datalist[i].bank_head_name + '">' + datalist[i].bank_name + '</li>';
                                }
                                $(".banksclass ul").html(strli);
                                $(".banksclass").removeClass("banksclassnone")
                                //将选择的 银行放到 input 框
                                $(".banksclass ul li").click(function () {
                                    $("#bankCode").val($(this).attr("data-bank"));
                                    $("#bankZongName").val($(this).attr("data-name"));
                                    $("#bankName").val($(this).text());
                                    $(".banksclass").addClass("banksclassnone");
                                })
                            }
                        } else {
                            layer.msg(res.message, {icon: 2, shade: 0.5, time: 1000});
                        }
                    })
                } else {
                    return false;
                }
            }, 1000);

        });
        //银行二搜索
        var timer2;
        var alertObjalertObj2;
        $("#pubBankName").bind('input propertychange', function () {

            let val = $(this).val();
            clearTimeout(timer2);
            timer2 = setTimeout(function () {
                if (val !== "") {
                    let banktype = 2;
                    $(".banksclass2 ul").html("")
                    getBanks(banktype, val).then(res => {
                        console.log(res)
                        if (res.status == 1) {
                            if (res.data.length == 0) {
                                $(".banksclass2").addClass("banksclassnone2");
                            } else {
                                let strli = "";
                                let datalist = res.data;
                                for (let i = 0; i < datalist.length; i++) {
                                    strli += '<li data-bank="' + datalist[i].bank_no + '" data-name="' + datalist[i].bank_head_name + '">' + datalist[i].bank_name + '</li>';
                                }
                                $(".banksclass2 ul").html(strli);
                                $(".banksclass2").removeClass("banksclassnone2")
                                //将选择的 银行放到 input 框
                                $(".banksclass2 ul li").click(function () {
                                    $("#pubBankCode").val($(this).attr("data-bank"));
                                    $("#pubBankZongName").val($(this).attr("data-name"));
                                    $("#pubBankName").val($(this).text());
                                    $(".banksclass2").addClass("banksclassnone2");
                                })
                            }
                        } else {
                            layer.msg(res.message, {icon: 2, shade: 0.5, time: 1000});
                        }
                    })
                } else {
                    return false;
                }
            }, 1000);

        });
        //搜索的银行列表
        $("#bankName").click(function () {
            if ($(".banksclass ul li").length == 0) {
                $(".banksclass").addClass("banksclassnone")
                return false;
            } else {
                let obj = $(".banksclass").hasClass("banksclassnone")
                console.log(obj)
                if (obj) {
                    $(".banksclass").removeClass("banksclassnone")
                } else {
                    $(".banksclass").addClass("banksclassnone")
                }
            }
        })
        //搜索的银行二列表
        $("#pubBankName").click(function () {
            if ($(".banksclass2 ul li").length == 0) {
                $(".banksclass2").addClass("banksclassnone2")
                return false;
            } else {
                let obj = $(".banksclass2").hasClass("banksclassnone2")
                console.log(obj)
                if (obj) {
                    $(".banksclass2").removeClass("banksclassnone2")
                } else {
                    $(".banksclass2").addClass("banksclassnone2")
                }
            }
        })
        //查询设备型型号
        var termModelVal
        $("#termModel").bind('input propertychange', function () {
            let thisval = $(this).val();
            clearTimeout(termModelVal);
            termModelVal = setTimeout(() => {
                if (thisval == "") {
                    $(".termModelboxdiv").addClass("termModelboxdivnone")
                    return false;
                } else {
                    $(".termModelbox ul").html("")
                    console.log(thisval)
                    $.post("/api/user/getTermModel", {
                        storeId: store_id,
                        paraType: 1,
                        paraName: thisval
                    }, function (data) {
                        if (data.status == 1) {
                            let datalist = JSON.parse(data.data);
                            if (datalist.length > 0) {
                                let str = "";
                                for (let i = 0; i < datalist.length; i++) {
                                    str += "<li data-bank='" + datalist[i].paramName + "'>" + datalist[i].paramName + "</li>";
                                }
                                $(".termModelbox ul").html(str);
                                $(".termModelboxdiv").removeClass("termModelboxdivnone")
                                //将选择的 银行放到 input 框
                                $(".termModelbox ul li").click(function () {
                                    $("#termModel").val($(this).attr("data-bank"));
                                    $(".termModelboxdiv").addClass("termModelboxdivnone");
                                })
                            } else {
                                $(".termModelboxdiv").addClass("termModelboxdivnone")
                            }
                        } else {
                            layer.msg(data.message, {icon: 2, shade: 0.5, time: 1000});
                        }
                    }, "json")
                }
            }, 1000)
        });
        $("#termModel").click(function () {
            if ($(".termModelbox ul li").length == 0) {
                $(".termModelboxdiv").addClass("termModelboxdivnone")
                return false;
            } else {
                let obj = $(".termModelboxdiv").hasClass("termModelboxdivnone")
                console.log(obj)
                if (obj) {
                    $(".termModelboxdiv").removeClass("termModelboxdivnone")
                } else {
                    $(".termModelboxdiv").addClass("termModelboxdivnone")
                }
            }
        })
        //监听 立招功能
        form.on("switch(twoBarsOpen)", function (data) {
            let timeMoney = data.elem.checked;
            if (timeMoney) {
                $(".twoBarCode").removeClass("twoBarCodeNone");
            } else {
                $(".twoBarCode").addClass("twoBarCodeNone");
            }
        })
        //监听 dStlmTyperadio 借记卡扣率方式
        form.on("radio(dStlmTyperadio)", function (data) {
            console.log(data.value)
            if (data.value == 1) {
                $(".dStlmTyperadioclickclass").removeClass("dStlmTyperadioclick")
            } else {
                $(".dStlmTyperadioclickclass").addClass("dStlmTyperadioclick")
            }
        })
        // 监听 终端地区 select1 的选择
        form.on("select(termArea1)", function (data) {
            getAreaNo(data.value).then(res => {
                $("#termArea2").html('<option value="">请选择</option>');
                var dataList = res.data;
                let str = "";
                for (let i = 0; i < dataList.length; i++) {
                    str += '<option value="' + dataList[i].id + '">' + dataList[i].name + '</option>';
                }
                $("#termArea2").append(str);
                form.render("select");
            })
        })
        //监听  终端地区 select2 的选择
        form.on("select(termArea2)", function (data) {
            console.log(data.value)
            getAreaNo(data.value).then(res => {
                $("#termArea").html('<option value="">请选择</option>');
                var dataList = res.data;
                let str = "";
                for (let i = 0; i < dataList.length; i++) {
                    str += '<option value="' + dataList[i].code + '">' + dataList[i].name + '</option>';
                }
                $("#termArea").append(str);
                form.render("select");
            })
        })
        //监听  终端地区 select3 的选择
        form.on("select(termArea)", function (data) {
            $("#areaNo").val(data.elem[data.elem.selectedIndex].text)
        })
        //监听  商户区域 select1 的选择
        form.on("select(merArea1)", function (data) {
            console.log(data.value)
            getAreaNo(data.value).then(res => {
                $("#merArea2").html('<option value="">请选择</option>');
                var dataList = res.data;
                let str = "";
                for (let i = 0; i < dataList.length; i++) {
                    str += '<option value="' + dataList[i].id + '">' + dataList[i].name + '</option>';
                }
                $("#merArea2").append(str);
                form.render("select");
            })
        })
        //监听  商户区域 select2 的选择
        form.on("select(merArea2)", function (data) {
            getAreaNo(data.value).then(res => {
                $("#merArea").html('<option value="">请选择</option>');
                var dataList = res.data;
                let str = "";
                for (let i = 0; i < dataList.length; i++) {
                    str += '<option value="' + dataList[i].code + '">' + dataList[i].name + '</option>';
                }
                $("#merArea").append(str);
                form.render("select");
            })
        })
        // 监听 项目ID 的选择
        form.on("select(projectId)", function (selectdata) {

            getProjectId().then(data => {
                if (data.status == 1) {
                    let dataProjectlist = data.data;
                    let Projectsrc = '<option value="">请选择</option>';
                    for (let a = 0; a < dataProjectlist.length; a++) {
                        Projectsrc += '<option value="' + dataProjectlist[a].project_id + '">' + dataProjectlist[a].project_name + '</option>';
                    }
                    $("#projectId").html(Projectsrc);
                    form.render("select");
                } else {
                    layer.msg(Projectsrc.message, {icon: 2, shade: 0.5, time: 1000});
                }
            })
        })

        // 监听 主营业务 select1 的选择
        form.on("select(businScope1)", function (selectdata) {
            $("#businScope2").html("");
            $("#businScope3").html("");
            getMcc().then(data => {
                if (data.status == 1) {
                    let dataMcclist = data.data;
                    let Mccsrc = '<option value="">请选择</option>';
                    for (let a = 0; a < dataMcclist.length; a++) {
                        if (dataMcclist[a].mcc_parent == selectdata.value) {
                            Mccsrc += '<option value="' + dataMcclist[a].mcc_id + '">' + dataMcclist[a].mcc_name + '</option>';
                        }
                    }
                    $("#businScope2").html(Mccsrc);
                    form.render("select");
                } else {
                    layer.msg(dataMcc.message, {icon: 2, shade: 0.5, time: 1000});
                }
            })
        })
        //监听 主营业务 select2 的选择
        form.on("select(businScope2)", function (selectdata) {
            $("#businScope3").html("");
            getMcc().then(data => {
                if (data.status == 1) {
                    let dataMcclist = data.data;
                    let Mccsrc = '<option value="">请选择</option>';
                    for (let a = 0; a < dataMcclist.length; a++) {
                        if (dataMcclist[a].mcc_parent == selectdata.value) {
                            Mccsrc += '<option value="' + dataMcclist[a].mcc_name + '">' + dataMcclist[a].mcc_name + '</option>';
                        }
                    }
                    $("#businScope3").html(Mccsrc);
                    form.render("select");
                } else {
                    layer.msg(dataMcc.message, {icon: 2, shade: 0.5, time: 1000});
                }
            })
        })
        //监听 主营业务 select3 的选择
        // form.on("select(businScope3)",function(selectdata){
        //   console.log(selectdata.value)
        // })
        // switchtimemoney2 第二开户行
        form.on('switch(switchtimemoney2)', function (data) {
            let timeMoney = data.elem.checked;
            if (timeMoney) {
                $(".dCalcTypeClass2").removeClass("dCalcTypenone2");
            } else {
                $(".dCalcTypeClass2").addClass("dCalcTypenone2");
            }
        })
        //监听实时入账
        form.on('switch(switchtimemoney)', function (data) {
            let timeMoney = data.elem.checked;
            if (timeMoney) {
                $(".dCalcTypeClass").removeClass("dCalcTypenone");
                layui.jquery('input[name="d1IsOpen"]').prop("checked", false);
                layui.form.render('checkbox');
                $(".switchtimemoneycirtype").addClass("switchtimemoneycirtypenone");

            } else {
                $(".dCalcTypeClass").addClass("dCalcTypenone");
            }
        });

        // 监听 次日到账
        form.on('switch(switchtimemoneycir)', function (data) {
            let timeMoney = data.elem.checked;
            if (timeMoney) {
                layui.jquery('input[name="realTimeEntry"]').prop("checked", false);
                $(".dCalcTypeClass").addClass("dCalcTypenone");
                layui.form.render('checkbox');
                $(".switchtimemoneycirtype").removeClass("switchtimemoneycirtypenone");
            } else {

                $(".switchtimemoneycirtype").addClass("switchtimemoneycirtypenone");

            }
        });


        // 门牌号图片图片上传
        upload.render({
            elem: '#uploadFile1',
            url: "/api/basequery/upload",
            data: {
                token: token,
                type: 'img',
                attach_name: 'file',
            },
            method: 'POST',
            type: 'images',
            ext: 'jpg|png|gif',
            unwrap: true,
            size: 5120,
            before: function (input) {
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon: 16, shade: 0.5, time: 0});
            },
            done: function (res) {
                console.log(res);
                if (res.status == 1) {
                    layer.msg("文件上传成功", {icon: 1, shade: 0.5, time: 1000});

                    layui.jquery('.imgImg1').attr("src", res.data.img_url);
                    $(".imgImg1").removeClass("imgImgNone");
                    $('input[name="houseNumber"]').val(res.data.img_url);
                    $("#uploadFile1").parent(".filebox").css({"background-color": "#ffffff"})
                } else {
                    layer.msg(res.message, {icon: 2, shade: 0.5, time: 1000});
                }
            }
        });
        // 协议图片上传
        upload.render({
            elem: '#uploadFile2',
            url: "/api/basequery/upload",
            data: {
                token: token,
                type: 'img',
                attach_name: 'file',
            },
            method: 'POST',
            type: 'images',
            ext: 'jpg|png|gif',
            unwrap: true,
            size: 5120,
            before: function (input) {
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon: 16, shade: 0.5, time: 0});
            },
            done: function (res) {
                console.log(res);
                if (res.status == 1) {
                    layer.msg("文件上传成功", {icon: 1, shade: 0.5, time: 1000});

                    layui.jquery('.imgImg2').attr("src", res.data.img_url);
                    $(".imgImg2").removeClass("imgImgNone");
                    $('input[name="agreement"]').val(res.data.img_url);
                    $("#uploadFile2").parent(".filebox").css({"background-color": "#ffffff"})
                } else {
                    layer.msg(res.message, {icon: 2, shade: 0.5, time: 1000});
                }
            }
        });
        //商户登记表正面图片
        upload.render({
            elem: '#uploadFile3',
            url: "/api/basequery/upload",
            data: {
                token: token,
                type: 'img',
                attach_name: 'file',
            },
            method: 'POST',
            type: 'images',
            ext: 'jpg|png|gif',
            unwrap: true,
            size: 5120,
            before: function (input) {
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon: 16, shade: 0.5, time: 0});
            },
            done: function (res) {
                console.log(res);
                if (res.status == 1) {
                    layer.msg("文件上传成功", {icon: 1, shade: 0.5, time: 1000});

                    layui.jquery('.imgImg3').attr("src", res.data.img_url);
                    $(".imgImg3").removeClass("imgImgNone");
                    $('input[name="mercRegistFormA"]').val(res.data.img_url);
                    $("#uploadFile3").parent(".filebox").css({"background-color": "#ffffff"})
                } else {
                    layer.msg(res.message, {icon: 2, shade: 0.5, time: 1000});
                }
            }
        });
        // 商户登记表反面图片
        upload.render({
            elem: '#uploadFile4',
            url: "/api/basequery/upload",
            data: {
                token: token,
                type: 'img',
                attach_name: 'file',
            },
            method: 'POST',
            type: 'images',
            ext: 'jpg|png|gif',
            unwrap: true,
            size: 5120,
            before: function (input) {
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon: 16, shade: 0.5, time: 0});
            },
            done: function (res) {
                console.log(res);
                if (res.status == 1) {
                    layer.msg("文件上传成功", {icon: 1, shade: 0.5, time: 1000});

                    layui.jquery('.imgImg4').attr("src", res.data.img_url);
                    $(".imgImg4").removeClass("imgImgNone");
                    $('input[name="mercRegistFormB"]').val(res.data.img_url);
                    $("#uploadFile4").parent(".filebox").css({"background-color": "#ffffff"});
                } else {
                    layer.msg(res.message, {icon: 2, shade: 0.5, time: 1000});
                }
            }
        });
        // 清算授权书图片
        upload.render({
            elem: '#uploadFile5',
            url: "/api/basequery/upload",
            data: {
                token: token,
                type: 'img',
                attach_name: 'file',
            },
            method: 'POST',
            type: 'images',
            ext: 'jpg|png|gif',
            unwrap: true,
            size: 5120,
            before: function (input) {
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon: 16, shade: 0.5, time: 0});
            },
            done: function (res) {
                console.log(res);
                if (res.status == 1) {
                    layer.msg("文件上传成功", {icon: 1, shade: 0.5, time: 1000});

                    layui.jquery('.imgImg5').attr("src", res.data.img_url);
                    $(".imgImg5").removeClass("imgImgNone");
                    $('input[name="certOfAuth"]').val(res.data.img_url);
                    $("#uploadFile5").parent(".filebox").css({"background-color": "#ffffff"});
                } else {
                    layer.msg(res.message, {icon: 2, shade: 0.5, time: 1000});
                }
            }
        });
        // 企业对私结算
        upload.render({
            elem: '#uploadFile6',
            url: "/api/basequery/upload",
            data: {
                token: token,
                type: 'img',
                attach_name: 'file',
            },
            method: 'POST',
            type: 'images',
            ext: 'jpg|png|gif',
            unwrap: true,
            size: 5120,
            before: function (input) {
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon: 16, shade: 0.5, time: 0});
            },
            done: function (res) {
                console.log(res);
                if (res.status == 1) {
                    layer.msg("文件上传成功", {icon: 1, shade: 0.5, time: 1000});

                    layui.jquery('.imgImg6').attr("src", res.data.img_url);
                    $(".imgImg6").removeClass("imgImgNone");
                    $('input[name="store_industrylicense_img"]').val(res.data.img_url);
                    $('input[name="store_imgs"]').val(res.data.img_url);
                    $("#uploadFile6").parent(".filebox").css({"background-color": "#ffffff"});
                } else {
                    layer.msg(res.message, {icon: 2, shade: 0.5, time: 1000});
                }
            }
        });
        //时间选择
        laydate.render({
            elem: '#time1',
            type: 'time',
            format: 'HH:mm'
        });
        laydate.render({
            elem: '#time2',
            type: 'time',
            format: 'HH:mm'
        });
        //监听终端种类选择
        form.on("select(termMode)", function (data) {
            if (data.value == 4 || data.value == 5) {
                $(".termModeNoneName").removeClass("termModeNone");
            } else {
                $(".termModeNoneName").addClass("termModeNone");
            }
        })
        // 保存提交
        form.on("submit(save)", function (data) {

            let listData = data.field;
            console.log(listData)

            var twoBarsOpen;
            var listarr = [];
            if (listData.twoBarsOpen) {
                if ($('.twoBarCode input[name="twoBarCode1"]').val() == "" && $('.twoBarCode input[name="termName1"]').val() != "") {
                    layer.msg("请输入二维码编号", {icon: 2, shade: 0.5, time: 1000});
                    return false;
                } else if ($('.twoBarCode input[name="termName1"]').val() == "" && $('.twoBarCode input[name="twoBarCode1"]').val() != "") {
                    layer.msg("请输入二维码名称", {icon: 2, shade: 0.5, time: 1000});
                    return false;
                } else {
                    twoBarsOpen = 1;
                    let twoBarCodeLength = $(".twoBarCode .inputbox div").length;
                    for (let a = 0; a < twoBarCodeLength; a++) {
                        // let newarr = [];
                        let num = a + 1;
                        if ($('.twoBarCode input[name="twoBarCode' + num + '"]').val() != "" && $('.twoBarCode input[name="termName' + num + '"]').val() != "") {
                            let newobj = {
                                'termName': $('.twoBarCode input[name="termName' + num + '"]').val(),
                                'twoBarCode': $('.twoBarCode input[name="twoBarCode' + num + '"]').val()
                            }
                            listarr.push(newobj)
                        } else if ($('.twoBarCode input[name="twoBarCode' + num + '"]').val() == "" && $('.twoBarCode input[name="termName' + num + '"]').val() != "") {
                            layer.msg("请输入二维码编号", {icon: 2, shade: 0.5, time: 1000});
                            return false;
                        } else if ($('.twoBarCode input[name="termName' + num + '"]').val() == "" && $('.twoBarCode input[name="twoBarCode' + num + '"]').val() != "") {
                            layer.msg("请输入二维码名称", {icon: 2, shade: 0.5, time: 1000});
                            return false;
                        }
                    }
                }
            } else {
                twoBarsOpen = 2;
            }

            var listarrnew = JSON.stringify(listarr);
            console.log(listarrnew)

            if (!token) {
                layer.msg("用户没有登录！非法操作！！！", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }

            if (!listData.mccId) {
                layer.msg("请选择，门店分类", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.standardFlag) {
                layer.msg("请选择，行业大类", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.employeeNum) {
                layer.msg("请选择，公司规模", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.businForm) {
                layer.msg("请选择，经营形态", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.businBegTime) {
                layer.msg("请选择，营业开始时间", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.businEndTime) {
                layer.msg("请选择，营业结束时间", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.termMode) {
                layer.msg("请选择，终端种类", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.houseNumber) {
                layer.msg("请选择，门牌号图片", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.agreement) {
                layer.msg("请选择，协议图片", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.mercRegistFormA) {
                layer.msg("请选择，商户登记表正面图片", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.mercRegistFormB) {
                layer.msg("请选择，商户登记表反面图片", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.certOfAuth) {
                layer.msg("请选择，清算授权书图片", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.termCode) {
                layer.msg("请输入，内部终端号", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.terUserName) {
                layer.msg("请输入，终端名称", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.areaNo) {
                layer.msg("请选择，区号", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.termArea) {
                layer.msg("请选择，终端地区", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.termModel) {
                layer.msg("请输入，设备型号", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.termModelLic) {
                layer.msg("请输入，机具号", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.businScope1) {
                layer.msg("请选择，主营业务", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.capital) {
                layer.msg("请输入，注册资本", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (listData.capital <= 0) {
                layer.msg("注册资本输入不规范", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.bankName) {
                layer.msg("请输入，开户行名称", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.bankCode) {
                layer.msg("请输入，开户行行号", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }

            if (!listData.merArea) {
                layer.msg("请选择，商户区域", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.dCalcVal) {
                layer.msg("请输入，借记卡扣率", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.dStlmMaxAmt) {
                layer.msg("请输入，借记卡封顶金额", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.dFeeLowLimit) {
                layer.msg("请输入，借记卡手续费最低值", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.cCalcVal) {
                layer.msg("请输入，信用卡扣率", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }
            if (!listData.cFeeLowLimit) {
                layer.msg("请输入，信用卡手续费最低值", {icon: 2, shade: 0.5, time: 2000});
                return false;
            }

            if (listData.termMode == 4 || listData.termMode == 5) {

                if (!listData.bangDingCommerCode) {
                    layer.msg("请输入，渠道商户号", {icon: 2, shade: 0.5, time: 2000});
                    return false;
                }
                if (!listData.bangDingTerMno) {
                    layer.msg("请输入，渠道终端号", {icon: 2, shade: 0.5, time: 2000});
                    return false;
                }

            }

            if (listData.realTimeEntry2) {
                if (!listData.pubBankName) {
                    layer.msg("开启企业对私结算后，必须输入，第二开户行名称", {icon: 2, shade: 0.5, time: 2000});
                    return false;
                }
                if (!listData.pubBankCode) {
                    layer.msg("开启企业对私结算后，必须输入，第二开户行行号", {icon: 2, shade: 0.5, time: 2000});
                    return false;
                }
                if (!listData.pubAccount) {
                    layer.msg("开启企业对私结算后，必须输入，第二开户行账号", {icon: 2, shade: 0.5, time: 2000});
                    return false;
                }
                if (!listData.pubAccName) {
                    layer.msg("开启企业对私结算后，必须输入，第二开户行账户名称", {icon: 2, shade: 0.5, time: 2000});
                    return false;
                }
                if (!listData.store_industrylicense_img) {
                    layer.msg("开启企业对私结算后，必须上传开户许可证", {icon: 2, shade: 0.5, time: 2000});
                    return false;
                }
                if (!listData.store_imgs) {
                    layer.msg("开启企业对私结算后，必须上传开户许可证", {icon: 2, shade: 0.5, time: 2000});
                    return false;
                }
            }
            var realTimeEntry;
            var dCalcType;
            var cCalcType;
            if (listData.realTimeEntry) {
                realTimeEntry = 1;
                dCalcType = listData.dCalcType;
                cCalcType = listData.cCalcType;
            } else {
                realTimeEntry = 0;
            }

            if (listData.d1IsOpen == "on") {
                var d1IsOpen = 1; //是否开通D1(0-否;1-是)
                var calcType = listData.calcType;
                var calcVal = listData.calcVal;
                var payDateType = listData.payDateType;
                if (calcVal == "") {
                    layer.msg("请输入手续费", {icon: 2, shade: 0.5, time: 1000});
                    return false;
                }
            } else {
                var d1IsOpen = 0; //是否开通D1(0-否;1-是)
            }

            console.log(d1IsOpen)
            console.log(calcType)
            console.log(calcVal)
            console.log(payDateType)

            console.log(realTimeEntry)
            var alertObj = layer.msg('请稍后......', {icon: 16, shade: 0.5, time: 0});
            $.post("/api/user/easyPayMerchantAccess", {
                token: token,
                storeId: store_id,

                d1IsOpen: d1IsOpen,
                calcType: calcType,
                calcVal: calcVal,
                payDateType: payDateType,
                signatoryName:listData.signatoryName,
                contractNo:listData.contractNo,

                store_industrylicense_img: listData.store_industrylicense_img,

                store_imgs: listData.store_imgs,
                projectId: listData.projectId,
                mccId: listData.mccId,
                twoBarsOpen: twoBarsOpen,
                twoBars: listarrnew,
                standardFlag: listData.standardFlag,
                employeeNum: listData.employeeNum,
                businForm: listData.businForm,
                businBegTime: listData.businBegTime,
                businEndTime: listData.businEndTime,
                termMode: listData.termMode,
                houseNumber: listData.houseNumber,
                agreement: listData.agreement,
                mercRegistFormA: listData.mercRegistFormA,
                mercRegistFormB: listData.mercRegistFormB,
                certOfAuth: listData.certOfAuth,
                termCode: listData.termCode,
                terUserName: listData.terUserName,
                areaNo: listData.termArea,
                termArea: listData.areaNo,
                termModel: listData.termModel,
                termModelLic: listData.termModelLic,

                businScope: listData.businScope,

                capital: listData.capital,
                bankName: listData.bankName,
                bankCode: listData.bankCode,
                realTimeEntry: realTimeEntry,
                merArea: listData.merArea,
                dStlmType: listData.dStlmType,
                dCalcVal: listData.dCalcVal,
                dStlmMaxAmt: listData.dStlmMaxAmt,
                dFeeLowLimit: listData.dFeeLowLimit,
                cCalcVal: listData.cCalcVal,
                cFeeLowLimit: listData.cFeeLowLimit,
                bangDingCommerCode: listData.bangDingCommerCode,
                bangDingTerMno: listData.bangDingTerMno,
                dCalcType: dCalcType,
                cCalcType: cCalcType,
                pubBankName: listData.pubBankName,
                pubBankCode: listData.pubBankCode,
                pubAccount: listData.pubAccount,
                pubAccName: listData.pubAccName
            }, function (data) {
                console.log(data)
                layer.close(alertObj);
                if (data.status == 1) {
                    layer.msg("保存成功！！！", {icon: 1, shade: 0.5, time: 1000});
                } else {
                    layer.msg(data.message, {icon: 2, shade: 0.5, time: 1000});
                }
            })

            return false;

        })
    })


</script>
</body>
</html>
