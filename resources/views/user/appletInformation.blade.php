<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>完善商铺小程序信息</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
<link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
<link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/formSelects-v4.css')}}" media="all">
<style type="text/css">
    .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
    .img_box span{position: absolute;right:0;top:0;font-size: 30px;background: #fff;cursor: pointer;}
    .img_box2 span{position: absolute;right:0;top:0;font-size: 30px;background: #fff;cursor: pointer;}
    .lianjiecon div{width:100%;overflow: hidden;margin-bottom:20px;}
    .lianjiecon label{display: inline-block;float: left;width:10%;line-height: 36px;}
    .lianjiecon input{display: inline-block;float: left;width:90%;}
</style>
</head>
<body>
    <div class="layui-fluid" id="appletsInfo">
        <div class="layui-card">
            <div class="layui-card-header">小程序信息</div>
            <div class="layui-card-body" style="padding: 15px;">
                <form class="layui-form" action="" lay-filter="storeAppletsInfo">
                    <div class="layui-form-item">
                        <label class="layui-form-label" style="text-align:center">* 微信小程序appid</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入微信小程序appid" name="wechat_appid" class="layui-input title">
                        </div>
                    </div>

                    <input type="hidden" placeholder="请输入微信小程序secret，不是必填" name="wechat_secret" class="layui-input title">

                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">* 支付宝小程序appid</label>
                            <div class="layui-input-inline">
                                <input type="text" placeholder="请输入支付宝小程序appid" name="alipay_appid" class="layui-input title">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <label class="layui-form-label">搜支付宝小程序appid</label>
                            <div class="layui-input-inline">
                                <input type="text" name="applet_name" placeholder="支付宝小程序完整名称" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <label class="layui-form-label"></label>
                            <div class="layui-input-inline">
                                <button type="button" class="layui-btn layui-btn-normal search_applet_name" lay-submit lay-filter="search_applet_name">搜索</button>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" placeholder="请输入支付宝小程序PrivateKey" name="alipay_rsaPrivateKey" class="layui-input title">
                    <input type="hidden" placeholder="请输入支付宝小程序PublicKey" name="alipay_alipayrsaPublicKey" class="layui-input title">

                    <div class="layui-form-item layui-layout-admin">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button type="button" class="layui-btn submit" lay-submit lay-filter="appletsInfo">确定提交</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script type="text/javascript">

    var token = sessionStorage.getItem("Usertoken");
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });
        //获取商家小程序信息
        var store_id   = sessionStorage.getItem('store_store_id');
        var store_name = sessionStorage.getItem('store_store_name');

        /**
         * 进入该页面，就先获取门店下的小程序配置信息
         */
        $.post("/api/customer/store/getStoreAppletsInfo",{
            store_id:store_id,
            type:"select"
        },function(data){
            var status = data.status;
            var responseData = data.data;
            if(status == 200){
                //设置值 {name值:val}
                form.val("storeAppletsInfo",{
                    "wechat_appid": responseData.wechat_appid ? responseData.wechat_appid:"",
                    "wechat_secret": responseData.wechat_secret ? responseData.wechat_secret:"",
                    "alipay_appid" : responseData.alipay_appid ? responseData.alipay_appid:"",
                    "alipay_rsaPrivateKey" : responseData.alipay_rsaPrivateKey ? responseData.alipay_rsaPrivateKey:"",
                    "alipay_alipayrsaPublicKey" : responseData.alipay_alipayrsaPublicKey ? responseData.alipay_alipayrsaPublicKey:"",
                });
            }else{
                layer.msg(data.message, {
                    offset: '50px'
                    ,icon: 2
                    ,time: 1000
                });
            }
        },"json");

        /**
         * 提交门店下的小程序信息
         */
        form.on("submit(appletsInfo)",function(data){
            var requestData = data.field;
            layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
            $.post("/api/customer/store/getStoreAppletsInfo",{
                store_id:store_id,
                type:"update",
                wechat_appid:requestData.wechat_appid,
                wechat_secret:requestData.wechat_secret,
                alipay_appid:requestData.alipay_appid,
                alipay_rsaPrivateKey:requestData.alipay_rsaPrivateKey,
                alipay_alipayrsaPublicKey:requestData.alipay_alipayrsaPublicKey,
            },function(data){
                var status = data.status;
                if(status == 200){
                    layer.msg(data.message, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 1000
                    },function(){
                        layui.view('appletsInfo').refresh();
                    });
                }else{
                    layer.msg(data.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 1000
                    });
                }
            },"json");
            return false;
        });

        /**
         * 根据支付宝小程序名称搜索小程序的appid
         */
        form.on("submit(search_applet_name)",function(data){
            var requestData = data.field;
            if(!requestData.applet_name){
                layer.msg("请输入小程序名称", {
                    offset: '50px'
                    ,icon: 2
                    ,time: 1000
                });
                return false;
            }
            layer.msg('正在请求，请稍后......', {icon:16, shade:0.5, time:0});
            $.post("/api/customer/aliPay/getAppletAppId",{
                applet_name:requestData.applet_name
            },function(data){
                var status = data.status;
                if(status == 200){
                    layer.msg(data.data.AuthorizerAppid, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 3000
                    });
                }else{
                    layer.msg(data.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 2000
                    });
                }
            },"json");
            return false;
        });
    });

</script>
</html>
