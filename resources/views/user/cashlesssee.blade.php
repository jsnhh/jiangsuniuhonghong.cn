<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>查看券信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header">代金券信息</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-row layui-form" lay-filter="component-form-group">
                <div class="layui-col-md6">
                    <div class="layui-form-item" style="width:500px;">
                        <label class="layui-form-label">门店名称</label>
                        <div class="layui-input-block">
                            <div class="layui-form-mid"></div>
                        </div>
                    </div>
                    <div class="layui-form-item" style="width:500px;">
                        <label class="layui-form-label">代金券活动名称</label>
                        <div class="layui-input-block">
                            <div class="layui-form-mid"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">代金券品牌</label>
                        <div class="layui-input-block">
                            <div class="layui-form-mid"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">有效期开始时间</label>
                        <div class="layui-input-block">
                            <div class="layui-form-mid"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">有效期结束时间</label>
                        <div class="layui-input-block">
                            <div class="layui-form-mid"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">券使用开始时间</label>
                        <div class="layui-input-block">
                            <div class="layui-form-mid"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">券使用结束时间</label>
                        <div class="layui-input-block">
                            <div class="layui-form-mid"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">最低限额</label>
                        <div class="layui-input-block">
                            <div class="layui-form-mid"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">优惠金额</label>
                        <div class="layui-input-block">
                            <div class="layui-form-mid"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">优惠时间(星期)</label>
                        <div class="layui-input-block">
                            <div class="layui-form-mid"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">优惠时间(开始时段)</label>
                        <div class="layui-input-block">
                            <div class="layui-form-mid"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">优惠时间(结束时段)</label>
                        <div class="layui-input-block">
                            <div class="layui-form-mid"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">可发放数量</label>
                        <div class="layui-input-block">
                            <div class="layui-form-mid"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">描述</label>
                        <div class="layui-input-block">
                            <div class="layui-form-mid"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var str=location.search;
    var id=str.split('?')[1];

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="{{url('/user/login')}}";
            }
        });

        $.post("{{url('/api/alipayopen/CashlessInfo')}}",
            {
                token:token
                ,id:id
            },function(res){
//            console.log(res);
                if(res.status==1){
                    $('.layui-row .layui-form-item').eq(0).find('.layui-form-mid').html(res.data.store_names);
                    $('.layui-row .layui-form-item').eq(1).find('.layui-form-mid').html(res.data.brand_name);
                    $('.layui-row .layui-form-item').eq(2).find('.layui-form-mid').html(res.data.activity_name);
                    $('.layui-row .layui-form-item').eq(3).find('.layui-form-mid').html(res.data.publish_start_time);
                    $('.layui-row .layui-form-item').eq(4).find('.layui-form-mid').html(res.data.publish_end_time);
                    $('.layui-row .layui-form-item').eq(5).find('.layui-form-mid').html(res.data.voucher_start);
                    $('.layui-row .layui-form-item').eq(6).find('.layui-form-mid').html(res.data.voucher_end);
                    $('.layui-row .layui-form-item').eq(7).find('.layui-form-mid').html(res.data.floor_amount);
                    $('.layui-row .layui-form-item').eq(8).find('.layui-form-mid').html(res.data.amount);
                    $('.layui-row .layui-form-item').eq(9).find('.layui-form-mid').html(res.data.day_rule);
                    $('.layui-row .layui-form-item').eq(10).find('.layui-form-mid').html(res.data.voucher_time_start);
                    $('.layui-row .layui-form-item').eq(11).find('.layui-form-mid').html(res.data.voucher_time_end);
                    $('.layui-row .layui-form-item').eq(12).find('.layui-form-mid').html(res.data.voucher_quantity);
                    $('.layui-row .layui-form-item').eq(13).find('.layui-form-mid').html(res.data.voucher_description);
                }
            },"json");

        $('.sfz_z').on("click",function(){
            layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area: '516px',
                skin: 'layui-layer-nobg', //没有背景色
                shadeClose: true,
                content: $('#sfz_z')
            });
        });

        $('.sfz_f').on("click",function(){
            layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area: '516px',
                skin: 'layui-layer-nobg', //没有背景色
                shadeClose: true,
                content: $('#sfz_f')
            });
        });

    });

</script>
</body>
</html>
