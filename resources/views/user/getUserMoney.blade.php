<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>未提分润</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
  <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
  <style>
    .edit{background-color: #ed9c3a;}
    .shenhe{background-color: #429488;}    
    .see{background-color: #7cb717;} 
    .tongbu{background-color: #4c9ef8;color:#fff;}
    .cur{color:#009688;}
    .userbox{
      height:200px;
      overflow-y: auto;
      z-index: 999;
      position: absolute;
      left: 0px;
      top: 63px;
      width:298px;
      background-color:#ffffff;
      border: 1px solid #ddd;
    }
    .userbox .list{
      height:38px;line-height: 38px;cursor:pointer;
      padding-left:10px;
    }
    .userbox .list:hover{
      background-color:#eeeeee;
    }
    .yname{
          font-size: 13px;
          color: #444;
        }
  </style>
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">
        <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12" style="margin-top:0px">
              <div class="layui-card"> 
                <div class="layui-card-header">未提分润</div>
                <div class="layui-card-body">
                  <div class="layui-btn-container" style="font-size:14px;">


                    <div class="layui-form" style="display: flex;width:100%">                      
                      
                    <div class="layui-form" lay-filter="component-form-group" style="width:190px;margin-right:10px;display: inline-block;">
                      <div class="layui-form-item">                          
                        <div class="layui-input-block" style="margin-left:0">
                        <text class="yname">代理商名称</text>
                          <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入代理商名称" class="layui-input transfer">
                          <div class="userbox" style='display: none'></div>
                        </div>
                      </div>
                    </div>

                      <div class="layui-form" lay-filter="component-form-group" style="width:190px;margin-right:10px;display: inline-block;">
                        <div class="layui-form-item">
                          <div class="layui-input-block" style="margin-left:0;border-radius:5px">
                            <text class="yname">提现状态</text>
                            <select name="withdrawType" id="withdrawType" lay-filter="withdrawType">
                              <option value="">全部</option>
                              <option value="1">正常提现</option>
                              <option value="0">禁止提现</option>
                            </select>
                          </div>
                        </div>
                      </div>

                    </div>

                  </div>

                  <div >
                      <div style="width:190px;margin-right:10px;display: inline-block;">分润总额:<span class="total"></span></div>
                      <div style="width:190px;margin-right:10px;display: inline-block;">可提现总额:<span class="totalMoney" style="color: #0e77ca"></span></div>
                      <div style="width:190px;margin-right:10px;display: inline-block;">禁止提现总额:<span class="notMoney"></span></div>
                  </div>
                  
                  <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>

                  <!-- 判断状态 -->


                  <script type="text/html" id="statusWithdraw">
                    @{{#  if(d.is_withdraw == 1){ }}
                    <span class="cur">@{{ "提现正常" }}</span>
                    @{{#  } else if(d.is_withdraw == 0){ }}
                    @{{ "禁止提现" }}
                    @{{#  } }}
                  </script>
                  <!-- 判断状态 -->
                  <script type="text/html" id="statusType">
                    @{{#  if(d.settlement_type == 1){ }}
                    <span class="">@{{ "月结" }}</span>
                    @{{#  } else if(d.settlement_type == 2){ }}
                    @{{ "日结" }}
                    @{{#  } }}
                  </script>


                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <input type="hidden" class="store_id">
  <input type="hidden" class="withdrawType">

  <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script> 
    <script>
    var token = sessionStorage.getItem("Usertoken");
    var source = sessionStorage.getItem("source");
    var str=location.search;
    var user_id=str.split('?')[1];

    layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate;
        // 未登录,跳转登录页面
        $(document).ready(function(){        
            if(token==null){
                window.location.href="{{url('/user/login')}}"; 
            }
        })

        var user_name=sessionStorage.getItem('agentName');

        if(user_id == undefined){

        }else{
          $('.transfer').val(user_name)
        }

        // 渲染表格
        table.render({
            elem: '#test-table-page'
            ,url: "{{url('/api/wallet/get_user_money')}}"
            ,method: 'post'
            ,where:{
              token:token,
              user_id:user_id
            }
            ,request:{
              pageName: 'p', 
              limitName: 'l'
            }
            ,page: true
            ,cellMinWidth: 150
            ,cols: [[
              {field:'name', title: '代理商名称'}
              ,{field:'money', title: '账户余额', sort: true}
              ,{field:'is_withdraw',  title: '提现状态',templet:'#statusWithdraw'}   //提现设置 1=正常 0=禁止，字段默认为1
              ,{field:'settlement_type',  title: '结算方式(建行)',templet:'#statusType'}
            ]]
            ,response: {
              statusName: 'status' //数据状态的字段名称，默认：code
              ,statusCode: 1 //成功的状态码，默认：0
              ,msgName: 'message' //状态信息的字段名称，默认：msg
              ,countName: 't' //数据总数的字段名称，默认：count
              ,dataName: 'data' //数据列表的字段名称，默认：data
            } 
            ,done: function(res, curr, count){
              $('.total').html(res.order_data.total)
              $('.totalMoney').html(res.order_data.totalMoney)
              $('.notMoney').html(res.order_data.notMoney)
              $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});	//进行表头样式设置
            }

        });

      $(".transfer").bind("input propertychange",function(event){
        //  console.log($(this).val())
        user_name = $(this).val()
        if (user_name.length == 0) {
          $('.userbox').html('')
          $('.userbox').hide()
        } else {
          $.post("{{url('/api/user/get_sub_users')}}",
                  {
                    token:token,
                    user_name:$(this).val(),
                    self:'1'

                  },function(res){
                    console.log(res);
                    var html="";
                    console.log(res.t)
                    if(res.t==0){
                      $('.userbox').html('')
                    }else{
                      for(var i=0;i<res.data.length;i++){
                        html+='<div class="list" data='+res.data[i].id+'>'+res.data[i].name+'-'+res.data[i].level_name+'</div>'
                      }
                      $(".userbox").show()
                      $('.userbox').html('')
                      $('.userbox').append(html)
                    }

                  },"json");
        }
      });

      $(".userbox").on("click",".list",function(){

        $('.transfer').val($(this).html())
        $('.js_user_id').val($(this).attr('data'))
        $('.userbox').hide()

        table.reload('test-table-page', {
          where: {
            user_id:$(this).attr('data')
          }
          ,page: {
            curr: 1
          }
        });
      })


      // 选择审核状态
      form.on('select(withdrawType)', function(data){
        var status = data.value;
        $('.withdrawType').val(status);
        //执行重载
        table.reload('test-table-page', {
          where: {
            is_withdraw:$('.withdrawType').val()
          }
          ,page: {
            curr: 1
          }
        });
      });

    });

  </script>

</body>
</html>





