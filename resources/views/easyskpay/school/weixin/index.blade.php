<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="black" name="apple-mobile-web-app-status-bar-style">
    <meta content="telephone=no" name="format-detection">
    <meta content="email=no" name="format-detection">
    <title>缴费教育</title>
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <script type="text/javascript" src="{{asset('/school/js/Screen.js')}}"></script>

    <style>
        .cur {
            background-color: #eee !important;
        }
    </style>
</head>
<body style="background-color: #f2f4f5;">
<div style="width:100%"><img src="../images/banner.png" style="max-width:100%;"></div>
<div class="layui-row" style="margin-top: 20px;text-align: center">
    <div style="float: left;width:33%;" class="student_manage">
        <div style="margin-bottom: 10px"><img src="../../images/studentManage.png" style="width:60px;height:60px"></div>
        <text>学生管理</text>
    </div>
    <div style="float: left;width:33%" class="pay">
        <div style="margin-bottom: 10px"><img src="../../images/pay.png" style="width:60px;height:60px"></div>
        <text>在线缴费</text>
    </div>
    <div style="float: left;width:33%" class="pay_info">
        <div style="margin-bottom: 10px"><img src="../../images/payInfo.png" style="width:60px;height:60px"></div>
        <text>缴费记录</text>
    </div>
</div>

<script type="text/javascript" src="{{asset('/school/js/jquery-2.1.4.js')}}"></script>
<!-- <script type="text/javascript" src="{{asset('/school/js/mobileSelect.js')}}"></script> -->
<script type="text/javascript" src="{{asset('/school/js/fastclick.js')}}"></script>
<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>$(function () {
        FastClick.attach(document.body);
    });</script>
<script>
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form', 'upload', 'formSelects', 'element'], function () {
        var $ = layui.$
            , admin = layui.admin
            , element = layui.element
            , layer = layui.layer
            , laydate = layui.laydate
            , form = layui.form
            , upload = layui.upload
            , formSelects = layui.formSelects;
        var store_id = "{{$_GET['store_id']}}";
        var open_id = "{{$_GET['open_id']}}";
        var school_name = "{{$_GET['school_name']}}";
        var school_no = "{{$_GET['school_no']}}";
        var merchant_id = "{{$_GET['merchant_id']}}";
        $(document).ready(function () {

        });

        $('.student_manage').click(function () {
            var data_url = "?store_id=" + store_id + "&open_id=" + open_id + "&school_name=" + school_name + "&school_no="
                + school_no + "&merchant_id=" + merchant_id;
            window.location.href = "{{url('api/easyskpay/weixin/studentManage')}}" + data_url;
        });
        $('.pay_info').click(function () {
            var data_url = "?store_id=" + store_id + "&open_id=" + open_id + "&school_name=" + school_name + "&school_no="
                + school_no + "&merchant_id=" + merchant_id;
            window.location.href = "{{url('api/easyskpay/weixin/payrecord')}}" + data_url;
        });
        $('.pay').click(function () {

            $.post("{{url('/api/school/stu/getStudentList')}}",
                {
                    open_id: open_id,
                    school_no:school_no
                }, function (res) {
                    if (res.status == 1) {

                        if (res.data.length <= 0) {
                            layer.confirm('还没有添加学生请添加学生', {
                                btn: ['确定', '取消']//按钮
                            }, function (index) {
                                var data_url = "?store_id=" + store_id + "&open_id=" + open_id + "&school_name=" + school_name + "&school_no="
                                    + school_no + "&merchant_id=" + merchant_id;
                                window.location.href = "{{url('api/easyskpay/weixin/addStudent')}}" + data_url;
                            });
                        } else {
                            var data_url = "?store_id=" + store_id + "&open_id=" + open_id + "&school_name=" + school_name + "&school_no="
                                + school_no + "&merchant_id=" + merchant_id;
                            window.location.href = "{{url('api/easyskpay/weixin/payeducation')}}" + data_url;
                        }
                    } else {
                        alert('查找板块报错');
                    }

                }, "json");

        });

    });


</script>
</body>
</html>