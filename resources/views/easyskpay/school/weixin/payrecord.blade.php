<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="black" name="apple-mobile-web-app-status-bar-style">
    <meta content="telephone=no" name="format-detection">
    <meta content="email=no" name="format-detection">
    <title>缴费记录</title>
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <script type="text/javascript" src="{{asset('/school/js/Screen.js')}}"></script>

    <style>
        .cur {
            background-color: #eee !important;
        }
    </style>
</head>
<body style="background-color: #f2f4f5;">
<div style="width:100%"><img src="../../images/banner.png" style="max-width:100%;"></div>
<div class="layui-fluid">
    <div style="font-size: 18px;text-align: center;margin-top: 100px;padding-bottom:100px;display:none"
         class="contant1">暂无缴费记录
    </div>
    <div class="contant">

    </div>
    <div class="layui-form-item layui-layout-admin">
        <div class="layui-input-block">
            <div class="layui-footer">
                <button class="layui-btn goBack" data-type="tabChange" style="background-color: #0bb840">返回首页</button>
                <!--<button type="reset" class="layui-btn layui-btn-primary">重置</button>-->
            </div>
        </div>
    </div>
</div>
<div class="load-hidden"></div>
<div class="load-hiddenbg" style="display: none"></div>
<script type="text/javascript" src="{{asset('/school/js/jquery-2.1.4.js')}}"></script>
<!-- <script type="text/javascript" src="{{asset('/school/js/mobileSelect.js')}}"></script> -->
<script type="text/javascript" src="{{asset('/school/js/fastclick.js')}}"></script>
<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>$(function () {
        FastClick.attach(document.body);
    });</script>
<script>
    var store_id = "{{$_GET['store_id']}}";
    var open_id = "{{$_GET['open_id']}}";
    var school_name = "{{$_GET['school_name']}}";
    var school_no = "{{$_GET['school_no']}}";
    var merchant_id = "{{$_GET['merchant_id']}}";
    $('.goBack').click(function () {
        var data_url = "?store_id=" + store_id + "&open_id=" + open_id + "&school_name=" + school_name + "&school_no="
            + school_no + "&merchant_id=" + merchant_id;
        window.location.href = "{{url('api/easyskpay/weixin/index')}}" + data_url;
    });
    $(document).ready(function () {
        $.post("{{url('/api/school/stu/getPayList')}}",
            {
                open_id: open_id
            }, function (res) {
                if (res.status == 1) {
                    var array = res.data;
                    if (array.length == 0) {
                        $('.contant1').show();
                    } else {
                        $('.contant1').hide();
                        var str = "";
                        for (var i = 0; i < array.length; i++) {
                            str += '<div style="margin-top:20px; font-size:15px;background-color:#fff;padding-left:10px;padding-top:10px"><div style="height: 30px">';
                            str += '<div style="font-size:18px">缴费项目：' + array[i].batch_name + '</div></div>';
                            str += '<div style="height: 30px">';
                            str += '<div style="float:left;width:180px;">学生姓名：' + array[i].student_name + '</div>';
                            str += '<div style="float:left; width:180px;">缴费方式：' + array[i].pay_type_desc + '</div></div>';
                            str += '<div style="height: 30px">';
                            str += '<div style="float:left;width:180px;">缴费金额：' + array[i].pay_amount + '</div>';
                            str += '<div style="float:left; width:180px;">缴费状态：<text style="color: #0bb840">' + array[i].pay_status_desc + '</text></div></div>';
                            str += '<div style="height: 30px">';
                            str += '<div style="float:left;width:300px;">缴费时间：' + array[i].pay_time + '</div></div></div>';

                            $('.contant').html('');
                            $('.contant').append(str);
                        }
                    }

                }
            }, "json");
    });
</script>
</body>
</html>