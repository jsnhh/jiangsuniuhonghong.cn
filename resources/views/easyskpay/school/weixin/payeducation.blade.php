<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="black" name="apple-mobile-web-app-status-bar-style">
    <meta content="telephone=no" name="format-detection">
    <meta content="email=no" name="format-detection">
    <title>在线缴费</title>
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <script type="text/javascript" src="{{asset('/school/js/Screen.js')}}"></script>

    <style>
        .cur {
            background-color: #eee !important;
        }
    </style>
</head>
<body style="background-color: #f2f4f5;">
<div style="width:100%"><img src="../../images/banner.png" style="max-width:100%;"></div>
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header" style="width:auto !important;font-size:23px">填写学生信息&nbsp;&nbsp;&nbsp;<span
                    class="zong_school_name"></span></div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item school" style="">
                    <label class="layui-form-label">学校名称</label>
                    <div class="layui-input-block" style="padding-top:7px">
                        <span class="school_name">{{$_GET['school_name']}}</span>
                    </div>
                </div>
                <div class="layui-form-item grade">
                    <label class="layui-form-label"><span style="color:red">*</span> 选择年级</label>
                    <div class="layui-input-block">
                        <select name="grade" id="grade" lay-filter="grade">

                        </select>
                    </div>
                </div>
                <div class="layui-form-item grade">
                    <label class="layui-form-label"><span style="color:red">*</span> 选择班级</label>
                    <div class="layui-input-block">
                        <select name="class" id="class" lay-filter="class">

                        </select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label"><span style="color:red">*</span>选择学生</label>
                    <div class="layui-input-block">
                        <select name="student" id="student" lay-filter="student">

                        </select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label"><span style="color:red">*</span>家长姓名</label>
                    <div class="layui-input-block">
                        <input type="text" name="parent_name" lay-verify="parent_name" autocomplete="off"
                               placeholder="请输入家长姓名" class="layui-input parent_name">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label"><span style="color:red">*</span>家长电话</label>
                    <div class="layui-input-block">
                        <input type="text" name="parent_phone" lay-verify="parent_phone" autocomplete="off"
                               placeholder="请输入家长电话" class="layui-input parent_phone">
                    </div>
                </div>

                <div class="layui-form-item school pay">
                    <label class="layui-form-label"> <span style="color:red">*</span>缴费项目</label>
                    <div class="layui-input-block">
                        <select name="paytype" class="paytype" lay-filter="paytype">

                        </select>
                    </div>
                </div>
                <div class="layui-form-item school batch_amount_d">
                    <label class="layui-form-label">缴费金额</label>
                    <div class="layui-input-block" style="padding-top:7px">
                        <span class="batch_amount"></span>

                    </div>
                </div>
                <div class="layui-form-item school remarks_d">
                    <label class="layui-form-label">备注</label>
                    <div class="layui-input-block" style="padding-top:7px">
                        <textarea name="remark" placeholder="请输入内容" class="layui-textarea remark"></textarea>
                    </div>
                </div>
                <div class="layui-form-item" style="text-align: center">
                    <div style="min-height: 36px;">
                        <div class="layui-footer">
                            <button class="layui-btn jf layui-btn-disabled" data-type="tabChange" disabled="disabled">确认缴费</button>
                            <button class="layui-btn goBack" data-type="tabChange"
                                    style="background-color: #0bb840">返回首页
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" class="school_no" value="">
<input type="hidden" class="gradeid" value="">
<input type="hidden" class="student_id" value="">
<input type="hidden" class="classid" value="">
<input type="hidden" class="statusid" value="">
<input type="hidden" class="relationshipid" value="">
<input type="hidden" class="stu_order_batch_no">
<script type="text/javascript" src="{{asset('/school/js/jquery-2.1.4.js')}}"></script>
<!-- <script type="text/javascript" src="{{asset('/school/js/mobileSelect.js')}}"></script> -->
<script type="text/javascript" src="{{asset('/school/js/fastclick.js')}}"></script>
<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script>$(function () {
        FastClick.attach(document.body);
    });</script>
<script>
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form', 'upload', 'formSelects', 'element'], function () {
        var $ = layui.$
            , admin = layui.admin
            , element = layui.element
            , layer = layui.layer
            , laydate = layui.laydate
            , form = layui.form
            , upload = layui.upload
            , formSelects = layui.formSelects;
        var store_id = "{{$_GET['store_id']}}";
        var open_id = "{{$_GET['open_id']}}";
        var school_name = "{{$_GET['school_name']}}";
        var school_no = "{{$_GET['school_no']}}";
        var merchant_id = "{{$_GET['merchant_id']}}";
        $(document).ready(function () {
            getGradeList()
        });

        function getGradeList() {
            $.post("{{url('/api/school/teacher/grade/lst')}}",
                {
                    store_id: store_id, school_no: school_no, merchant_id: merchant_id
                }, function (res) {
                    if (res.status == 1) {
                        var optionStr = "";
                        for (var i = 0; i < res.data.length; i++) {
                            optionStr += "<option value='" + res.data[i].stu_grades_no + "'>"
                                + res.data[i].stu_grades_name + "</option>";
                        }
                        $("#grade").html('');
                        $("#grade").append('<option value="">选择年级</option>' + optionStr);
                        layui.form.render('select');
                    } else {
                        alert('查找板块报错');
                    }

                }, "json");
        }

        $('.goBack').click(function () {
            var data_url = "?store_id=" + store_id + "&open_id=" + open_id + "&school_name=" + school_name + "&school_no="
                + school_no + "&merchant_id=" + merchant_id;
            window.location.href = "{{url('api/easyskpay/weixin/index')}}" + data_url;
        });

        function getStudentList() {
            $.post("{{url('/api/school/stu/getStudentList')}}",
                {
                    open_id: open_id
                    , stu_class_no: $('.classid').val(),
                    school_no: school_no
                }, function (res) {
                    if (res.status == 1) {
                        var optionStr = "";
                        for (var i = 0; i < res.data.length; i++) {
                            optionStr += "<option value='" + res.data[i].id + "'>"
                                + res.data[i].student_name + "</option>";
                        }
                        $("#student").html('');
                        $("#student").append('<option value="">选择学生</option>' + optionStr);
                        layui.form.render('select');
                    } else {
                        alert('查找板块报错');
                    }

                }, "json");
        }

        form.on('select(student)', function (data) {
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.student_id').val(category);
        });
        form.on('select(grade)', function (data) {
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.gradeid').val(category);
            // 选择班级
            $.ajax({
                url: "{{url('/api/school/teacher/class/lst')}}",
                data: {
                    store_id: store_id,
                    school_no: school_no,
                    stu_grades_no: $('.gradeid').val(),
                    merchant_id: merchant_id
                },
                type: 'post',
                success: function (data) {
                    var optionStr = "";
                    for (var i = 0; i < data.data.length; i++) {
                        optionStr += "<option value='" + data.data[i].stu_class_no + "'>"
                            + data.data[i].stu_class_name + "</option>";
                    }
                    $("#class").html('');
                    $("#class").append('<option value="">选择班级</option>' + optionStr);
                    layui.form.render('select');
                },
                error: function (data) {
                    alert('查找板块报错');
                }
            });
        });
        form.on('select(class)', function (data) {
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.classid').val(category);
            $.post("{{url('/api/school/stu/getOrderBatchList')}}",
                {
                    stu_class_no: category
                }, function (res) {
                    if (res.status == 1) {
                        var optionStr = "";
                        for (var i = 0; i < res.data.length; i++) {
                            optionStr += "<option value='" + res.data[i].stu_order_batch_no + "'>"
                                + res.data[i].batch_name + "</option>";
                        }
                        $(".paytype").html('');
                        $(".paytype").append('<option value="">选择缴费项目</option>' + optionStr);
                        layui.form.render('select');
                    } else {
                        alert('查找板块报错');
                    }

                }, "json");
            getStudentList()
        });
        $('.mx').click(function () {
            var data_url = "?store_id=" + store_id + "&open_id=" + open_id + "&school_name=" + school_name + "&parent_phone="
                + $('.parent_phone').val() + "&parent_name=" + $('.parent_name').val() + "&student_name=" + $('.student_name').val();
            window.location.href = "{{url('api/easyskpay/weixin/payrecord')}}" + data_url;
        });

        form.on('select(paytype)', function (data) {
            category = data.value;
            $('.stu_order_batch_no').val(category)
            $.post("{{url('/api/school/stu/getOrderBatchInfo')}}",
                {
                    stu_order_batch_no: category,
                }, function (res) {
                    if (res.status == 1) {
                        $('.batch_amount').text(res.data)
                        $('.jf').removeClass("layui-btn-disabled").attr("disabled",false);
                    } else {
                        $('.batch_amount').text(0.00)
                        $('.jf').addClass("layui-btn-disabled").attr("disabled",true);
                    }

                }, "json");

        });
        $(function () {
            function onBridgeReady() {
                var url = "{{url('/api/school/school_pay')}}";
                var data =
                    {
                        "ways_type": 32002,
                        "total_amount": $('.batch_amount').text(),
                        "store_id": store_id,
                        "open_id": open_id,
                        "school_name": school_name,
                        "id": $('.student_id').val(),
                        "parent_name": $('.parent_name').val(),
                        "parent_phone": $('.parent_phone').val(),
                        "remark": $('.remark').val(),
                        'stu_order_batch_no': $('.stu_order_batch_no').val(),
                    };
                $.post(url, data,
                    function (back) {
                        if (back.status == 1) {
                            var json_data = back.wx_data;
                            WeixinJSBridge.invoke(
                                'getBrandWCPayRequest', {
                                    "appId": json_data.appId, //公众号名称，由商户传入
                                    "timeStamp": json_data.timeStamp, //时间戳，自1970年以来的秒数
                                    "nonceStr": json_data.nonceStr, //随机串
                                    "package": json_data.package,
                                    "signType": json_data.signType, //微信签名方式
                                    "paySign": json_data.paySign //微信签名
                                },
                                function (res) {
                                    layui.layer.closeAll();
                                    if (res.err_msg == "get_brand_wcpay_request:ok") {
                                        var data_url = "?store_id=" + store_id + "&open_id=" + open_id + "&school_name=" + school_name + "&school_no="
                                            + school_no + "&merchant_id=" + merchant_id;
                                        window.location.href = "{{url('api/easyskpay/weixin/payrecord')}}" + data_url;
                                    } else {
                                        var data_url = "&ad_p_id=4";

                                        window.location.href = "{{url('page/pay_errors?message=取消支付')}}" + data_url;

                                    }
                                }
                            );
                        } else {
                            layui.layer.closeAll();
                            var data_url = "&ad_p_id=4";
                            window.location.href = "{{url('page/pay_errors?message=')}}" + back.message + data_url;
                        }

                    },
                    "json");

            }

            // 触发支付事件
            $('.jf').click(function () {
                if ($('.stu_order_batch_no').val() == '') {
                    alert("请选择缴费项目");
                    return;
                }
                if ($('.parent_phone').val() == '') {
                    alert("家长手机号不能为空");
                    return;
                }
                layui.layer.load();
                if (typeof WeixinJSBridge == "undefined") {
                    if (document.addEventListener) {
                        document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
                    } else if (document.attachEvent) {
                        document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
                        document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
                    }
                } else {
                    onBridgeReady();
                }
            });

        });
    });


</script>
</body>
</html>