<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="renderer" content="webkit">
    <meta name="imagemode" content="force">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>向商户付款</title>
    <link rel="stylesheet" href="{{asset('/payviews/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('/phone/css/swiper.min.css')}}">
    <script type="text/javascript" src="{{asset('/phone/js/Screen.js')}}"></script>
    <style>
        .swiper-container {
            width: 100%;
            height: 1.85rem;
            margin-top: .2rem;
        }

        .swiper-container .swiper-wrapper .swiper-slide img {
            width: 100%;
        }

        .swindle {
            width: 100% !important;
            height: .6rem !important;
            font-size: .24rem !important;
        }

        .keyboard ul li.swindle:after {
            font-size: .24rem !important;
        }

        .payment {
            padding-bottom:20px;
        }
        .bj_color{
            background-color: #ffffff;
        }
       
        .select_ali{
            background-color: #ffffff;
            margin-top:0.1rem;
            height: auto;
            overflow: scroll;
        }
        .list{
            display: flex;
            align-items: center;
            justify-content: space-between;
            width: 90%;
            height: 0.9rem;
            line-height: 0.9rem;
            margin:0 auto;
            border-bottom: 1px solid #eeeeee;
        }
        .list img:nth-child(1){
            width:0.5rem;
            height: 0.5rem;
            margin-right:0.2rem;
        }
        .list img:nth-child(2){
            width:0.32rem;
            height: 0.32rem;
        }
        .list_item{
            display: flex;
            align-items: center;
        }
        .flowerlist{
            display: flex;
            align-items: center;
            justify-content: space-between;
            width: 90%;
            height: 0.9rem;
            line-height: 0.9rem;
            margin:0 auto;
            border-bottom: 1px solid #eeeeee;
        }
        .flowerlist img{
            width:0.32rem;
            height: 0.32rem;
        }
        .f_show{
            display: none;
        }
        .payment .title {
            text-align: left;
            padding: .4rem 0 .3rem .3rem;
            width: 90%;
        }

        .payment .title span {
            display: inline-block;
            float: right;
            color: #108ee9;
        }

        .payment .title img {
            width: .6rem;
            height: .6rem;
        }

        /*遮罩*/
        .popup_bg {
            width: 100%;
            height: 100%;
            background: #000;
            opacity: .6;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 1200
        }

        .result_layer {
            width: 5.6rem;
            height: 3rem;
            z-index: 1300;
            position: fixed;
            border-radius: .05rem;
            background: #fff;
            top: 50%;
            left: 50%;
            margin: -1.5rem auto auto -2.8rem;
            text-align: center
        }

        .result_layer div {
            float: left;
            width: 100%;
            box-sizing: border-box;
            vertical-align: middle;
        }

        .result_layer div p {
            font-size: .3rem;
            font-weight: 500;
            word-wrap: break-word;
        }

        .result_layer p b {
            color: #f30;
            font-size: .42rem;
            font-family: serif;
            margin: 0.05rem
        }

        .result_layer a {
            float: left;
            width: 50%;
            font: .32rem/.8rem "微软雅黑";
            color: #333;
            border-top: 1px solid #ccc
        }

        .result_layer a:last-child {
            color: #f30;
            border-left: 1px solid #ccc;
            box-sizing: border-box
        }

        .result_layer a.confirm {
            border-left: none;
            width: 100%
        }

        .payment {
            padding-bottom: .3rem;
        }

        .payment .title {
            text-align: left;
            padding: .4rem 0 .3rem .3rem;
            width: 90%;
        }

        .payment .title span {
            display: inline-block;
            float: right;
            color: #108ee9;
        }

        .payment .title img {
            width: .6rem;
            height: .6rem;
        }

        .pay_money {
            height: 1.2rem;
            line-height: 1.2rem;
        }

        .pay_type {
            background: #fff;
            overflow: hidden;
            margin-top: .2rem;
        }

        .pay_type .line_x {
            border-bottom: .01rem solid #ddd;
            margin-left: .3rem;
            height: 0.9rem;
            line-height: 0.9rem;
        }

        .pay_type .line_x img:nth-child(1) {
            width: .50rem;
            height: .50rem;
            padding-top: .2rem;
            float: left;
            padding-right: .2rem;
        }

        .pay_type .line_x label {
            display: inline-block;
            height: 0.9rem;
            line-height: 0.9rem;

        }

        .pay_type .line_x img:nth-child(3) {
            width: .32rem;
            height: .32rem;
            float: right;
            padding: .29rem .3rem;
        }

        .pay_type .line_x span a {
            color: #108ee9;
            float: right;
            padding-right: .3rem;
        }

        .xuanze {
            width: .32rem;
            height: .32rem;
            float: right;
            padding: .29rem .3rem;
        }

        .result_layer {
            background: #dedede;
            border-radius: .2rem;
        }

        .result_layer .remark {
            margin: 0;
        }

        .remark textarea {
            height: .6rem;
            padding-top: .2rem;
            text-indent: .2rem;
        }

        .danwei {
            display: none
        }
        .show{
            display: none;
        }

    </style>
</head>
<body data="支付宝发红包啦！人人可领，天天可领！长按复制此消息，打开支付宝领红包！bmjnxn667B">
<div class="payment">

    <div class="title">
        <img src="{{url('/payviews/img/touxiang.png')}}">{{$data['store_name']}}
        <span class="beizhu">添加备注</span>
    </div>
    
    <div class="pay_money">
        <span>支付金额</span>
        <i></i>
        <div class="ipt"> ￥ <span id="price"></span></div>
    </div>
</div>

<div class="select_ali">
    <div class="list">
        <div class="list_item">
            <img src="{{url('/payviews/img/zhifubao-logo.png')}}">
            支付宝支付
        </div>
        <img class="img" src="{{url('/payviews/img/selected.png')}}">
    </div>
    <div class="list f_show">
        <div class="list_item">
            <img src="{{url('/payviews/img/huabei_icon.png')}}">
            花呗分期
        </div>        
    </div>
    <div class="flowercon f_show">
        
    </div>
    

</div>

<!-- <div class="bj_color">
    <div class="remark">
        <textarea class="remark_con" id="remark" placeholder="添加备注(30字以内)"></textarea>
        <img src="{{url('/payviews/img/qingkong.png')}}" class="quxiao">
    </div>
</div> -->

<div class="neirong" style="opacity: 0">支付宝发红包啦！人人可领，天天可领！长按复制此消息，打开支付宝领红包！bmjnxn667B</div>


<!--广告轮播图-->
<!-- <div class="swiper-container">
    <div class="swiper-wrapper">
        <div class="swiper-slide"><img src="img/ad.jpg"></div>
        <div class="swiper-slide"><img src="img/ad.jpg"></div>
    </div>
    Add Pagination
    <div class="swiper-pagination"></div>
</div> -->
<!-- 键盘 -->
<div class="keyboard">
    <ul>
        <li data="1"></li>
        <li data="2"></li>
        <li data="3"></li>
        <!-- <li class="xyk_pay" data="信用卡分期"></li> -->
        <li class="confirm btn" id="payLogButton" data="确认"></li>
        <li data="4"></li>
        <li data="5"></li>
        <li data="6"></li>
        <li data="7"></li>
        <li data="8"></li>
        <li data="9"></li>
        <li data="0"></li>
        <!-- <li class="disable_li"></li> -->
        <li data="."></li>
        <li class="del"><img src="{{url('/payviews/img/tui.png')}}"></li>
    </ul>
</div>
<input type="hidden" value="{{$data['store_id']}}" id="store_id">
<input type="hidden" value="{{$data['open_id']}}" id="open_id">
<input type="hidden" value="{{$data['merchant_id']}}" id="m_id">
<input type="hidden" id="token" value="{{csrf_token()}}">

<input type="hidden" class="total_amount" value="">
<input type="hidden" id="hb_fq_num" value="">

<div class="load-hidden"></div>
<div class="load-hiddenbg" style="display: none"></div>

<!-- 弹框 -->
<div class="show" style="display: none">
    <div class="popup_bg"></div>
    <div class="result_layer" id="accountMsg">
        <div style="padding: .65rem 0;">
            <div class="remark">
                <textarea class="remark_con" id="remark" placeholder="最多可输入30个字"></textarea>
            </div>
        </div>
        <a class="qd" style="width:49%;color:#219aff;float:left;">取消</a>
        <a class="qd" style="width:50%;color:#219aff">确定</a>
    </div>
</div>

<script type="text/javascript" src="{{asset('/phone/js/jquery-2.1.4.js')}}"></script>
<script type="text/javascript" src="{{asset('/phone/js/swiper.jquery.min.js')}}"></script>
<script src="{{asset('/phone/js/fastclick.js')}}"></script>
<script src="{{asset('/school/js/clipboard.min.js')}}"></script>
<script ytpe="text/javascript">
    var clipboard = new ClipboardJS('.btn', {
        target: function () {
            return document.querySelector('.neirong');
        }
    });

    clipboard.on('success', function (e) {
        // console.log(e);
    });

    clipboard.on('error', function (e) {
        // console.log(e);
    });
</script>

<script>
    $(function () {
        FastClick.attach(document.body);
    });
    document.documentElement.addEventListener('dblclick', function (e) {
        e.preventDefault();
    });
    document.addEventListener('touchmove', function (event) {
        event.preventDefault();
    }, false);
    document.addEventListener('touchmove', function (e) {
        e.preventDefault()
    }, false);
</script>
<script>
    $('.beizhu').click(function () {
        $('.show').show()
    });
    $('.keyboard ul li').on("touchend", function () {
        var _this = $(this);
        var num = _this.attr('data');
        var $money = $("#price");
        var oldValue = $money.html();
        var newValue = "";


        if (_this.hasClass('del')) {
            newValue = oldValue.substring(0, oldValue.length - 1);
            if (newValue == "") {
                newValue = "";
                $('.confirm').removeClass('color');
            }

            $money.html(newValue);
            console.log($money.html())
            if($money.html()!=''){
                $('.select_ali').height(3.6+'rem')
                $('.f_show').show()
                $.post("{{url('/api/merchant/fq/hb_query_rate')}}", {            
                    store_id: $("#store_id").val(),
                    shop_price: $money.html(),
                }, function (data) {
                    console.log(data)
                    
                    $(".flowercon").html('')            
                    var str=""
                    for(var i=0;i<data.data.length;i++){
                        str+='<div class="flowerlist" data="'+data.data[i].total_amount+'" num="'+data.data[i].hb_fq_num+'">￥'+data.data[i].hb_mq_h+'*'+data.data[i].hb_fq_num+'期 (手续费￥'+data.data[i].hb_fq_sxf+')';
                            str+='<img class="img" src="{{url('/payviews/img/NO-selected.png')}}">';
                        str+="</div>";

                    }
                    
                    $(".flowercon").append(str)

                }, "json");
            }else{
                $('.f_show').hide()
                $('.select_ali').height(0.9+'rem')
            }
        }else if(_this.hasClass('confirm')){

        }else {
            if (oldValue == "0.00") {
                if (num != ".") {
                    oldValue = "";
                }
            }
            if (oldValue == "0") {
                if (num != ".") {
                    oldValue = "";
                }
            }
            if (oldValue == "") {
                if (num == ".") {
                    oldValue = "0.";
                }
            }
            newValue = oldValue + num;

            //        控制输入的值为五位数和2位小数点
            reg = /^\d{0,8}(\.\d{0,2})?$/g;

            if (reg.test(newValue)) {
                $money.html(newValue);
                $('.confirm').addClass('color');
            } else {
                $money.html(oldValue);
                $('.confirm').addClass('color');
            }

            // 信用卡分期
            if (newValue >= 600 && newValue <= 50000) {
                $('.xyk_pay').addClass('fenqi');
            }

            console.log($money.html())

            if($money.html()!=''){
                $('.select_ali').height(3.6+'rem')
                $('.f_show').show()
                $.post("{{url('/api/merchant/fq/hb_query_rate')}}", {            
                    store_id: $("#store_id").val(),
                    shop_price: $money.html(),
                }, function (data) {
                    console.log(data)
                    
                    $(".flowercon").html('')            
                    var str=""
                    for(var i=0;i<data.data.length;i++){
                        str+='<div class="flowerlist" data="'+data.data[i].total_amount+'" num="'+data.data[i].hb_fq_num+'">￥'+data.data[i].hb_mq_h+'*'+data.data[i].hb_fq_num+'期 (手续费￥'+data.data[i].hb_fq_sxf+'/期)';
                            str+='<img class="img" src="{{url('/payviews/img/NO-selected.png')}}">';
                        str+="</div>";

                    }
                    
                    $(".flowercon").append(str)

                }, "json");
            }else{
                $('.f_show').hide()
                $('.select_ali').height(0.9+'rem')
            }
        }
 
    });


    $('.list').click(function(){
        $('.flowercon').find('.img').attr('src',"{{url('/payviews/img/NO-selected.png')}}")
        if($(this).find('.img').attr('src') == "{{url('/payviews/img/NO-selected.png')}}"){
            $(this).find('.img').attr('src',"{{url('/payviews/img/selected.png')}}")
        }else{
            $(this).find('.img').attr('src',"{{url('/payviews/img/NO-selected.png')}}")
        }
        $('.total_amount').val('')
        $('#hb_fq_num').val('')        

    })
    $('.flowercon').on("click",".flowerlist", function () {
        $('.list').find('.img').attr('src',"{{url('/payviews/img/NO-selected.png')}}")
        $('.flowercon').find('.img').attr('src',"{{url('/payviews/img/NO-selected.png')}}")

        $(this).find('.img').attr('src',"{{url('/payviews/img/selected.png')}}")
        $('.total_amount').val($(this).attr('data'))
        $('#hb_fq_num').val($(this).attr('num'))
    })

    // 信用卡分期star-----------------------------
    $('.keyboard').on("click", "ul li.fenqi", function () {
        var undertake = 1;
        var price = $('#price').html();
        var store_id = $('#store_id').val();
        var m_id = $('#m_id').val();
        window.location.href = "{{url('/phone/stages?undertake=')}}" + undertake + "&price=" + price + "&store_id=" + store_id + "&m_id=" + m_id;

    })
    // 信用卡分期end-----------------------------

    $(document).ready(function () {
        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            paginationClickable: true
        });
    });
    $(".remark_con").focus(function () {
        $('.keyboard').hide();
    });
    $('.pay_money').click(function () {
        $('.keyboard').show();
    });
    $(".remark_con").bind("input propertychange", function () {

        var len = $(this).val().length;
        var max = 30;

        if (len > max) {
            var value = $(this).val().substring(0, max);
            $(this).val(value);
        }

    });
    $('.quxiao').click(function () {
        $('.remark_con').val('');
    });
    $('.remark_con').blur(function () {
        $('.keyboard').show();
    })
</script>
<script>
    $("#payLogButton").click(function () {
        $('.load-hidden').addClass('loading');
        $('.load-hiddenbg').show();
        console.log($('.total_amount').val())
 

        
        if($('#hb_fq_num').val()==''){
            pay()
        }else{
            pay(1,$('#hb_fq_num').val())
        }

        

    });
    
    function pay(is_fq,hb_fq_num){
        $.post("{{url('/api/merchant/qr_auth_pay')}}", {
            total_amount: $("#price").html(),
            store_id: $("#store_id").val(),
            remark: $("#remark").val(),
            _token: $("#token").val(),
            merchant_id: $("#m_id").val(),
            open_id: $("#open_id").val(),
            ways_type: '16002',
            is_fq,
            hb_fq_num
        }, function (data) {

            $('.load-hidden').removeClass('loading');
            $('.load-hiddenbg').hide();
            var data_url = "&total_amount=" + $("#price").html() + "&store_id=" + $("#store_id").val();

            if (data.status == 1) {

                $("#payLogButton").attr('disabled', 'disabled');
                $('.confirm').removeClass('color');
                AlipayJSBridge.call("tradePay", {
                    tradeNO: data.data.trade_no
                }, function (result) {
                    //付款成功
                    if (result.resultCode == "9000") {
                        data_url = data_url + "&ad_p_id=1";

                        window.location.href = "{{url('page/pay_success?message=支付成功')}}" + data_url;
                    }
                    if (result.resultCode == "6001") {
                        data_url = data_url + "&ad_p_id=3";
                        window.location.href = "{{url('page/pay_errors?message=取消支付')}}" + data_url;
                    }
                });
            } else {
                data_url = data_url + "&ad_p_id=3";
                window.location.href = "{{url('page/pay_errors?message=')}}" + data.message + data_url;
            }
        }, "json");
    }

    $('.qd').click(function () {
        $('.show').hide();
        sessionStorage.setItem("show", '1');
    });
    var show = sessionStorage.getItem("show");
    if (show == 1) {
        $('.show').hide();
    } else {
        $('.show').show();
    }
</script>
</body>
</html>