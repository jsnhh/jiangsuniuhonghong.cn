<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>开户许可证</title>
    <style>
        .photo {
            background-color: #fff;
            width: 100%;
        }

        .tip {
            background-color: #FFFADA;
            height: 40px;
            line-height: 40px;
            font-size: 14px;
            color: #666666;
            text-align: center;
        }

        .photoImg {
            width: 160px;
            height: 160px;
            padding: 50px 0 0 30px;
        }

        .desc {
            text-align: right;
            font-size: 16px;
            color: #999999;
            padding-top: 30px;
        }

        .btn {
            width:300px;
            height: 40px;
            line-height: 40px;
            margin: 100px auto;
            text-align: center;
            color: #fff;
            background-color: #29A1F7;
            border-radius: 12px;
        }
    </style>
</head>

<body>
    <div class='photo'>
        <div class="tip">开户许可证是由中国人民银行核发的开设基本帐户的凭证</div>
        <div>
            <img class='photoImg'></img>
            <div class='desc'>需上传1张图片</div>
        </div>
    </div>

    <div class='btn bgcolor'>完成</div>

</body>

</html>