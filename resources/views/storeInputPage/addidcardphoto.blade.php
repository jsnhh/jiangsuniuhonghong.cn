<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>法人认证</title>
    <style>
        .photobox {
            display: flex;
        }

        .tip {
            font-size: 13px;
            color: #666666;
            text-align: center;
            padding: 20px 0 20px;
        }

        .img {
            width: 160px;
            height: 110px;
            margin: 0 auto;
            position: relative;
            border: 1px solid #666;
        }

        .texts {
            position: absolute;
            bottom: -25px;
            font-size: 15px;
            /* margin-left:-65px; */
        }

        .btn {
            width: 239px;
            position: fixed;
            color: #fff;
            text-align: center;
            border-radius: 45px;
            font-size: 20px;
            margin-left: 14%;
            background: #108ee9;
            padding: 5px;
            margin-top: 20px;
        }

        .paizhao {
            position: absolute;
            bottom: 26px;
            width: 55px;
            height: 55px;
            left: 31%;
        }

        .sectionBox {
            margin: 40px 10px auto;
        }

        .sectionBox .section_item {
            width: 100%;
            line-height: 45px;
            border-bottom: 1px solid #ddd;
            font-size: 17px;
            overflow: hidden;
        }

        .sectionBox .section_item label {
            float: left;
        }

        .sectionBox .section_item input {
            float: right;
            text-align: right;
            padding-right: 30px;
            line-height: 45px;
            border: none;
        }

        .sectionBox .section_item text.sfzNum {
            float: right;
            padding-right: 30px;
            color: #B2B2B2;
        }

        .sectionBox .section_item .img {
            display: inline-block;
            width: 500px;
            float: right;
            text-align: right;
            display: flex;

        }

        .sectionBox .section_item .img input.cshanghuAddress {
            width: 440px;
            text-align: right;
            padding-right: 5px;
            font-size: 28px;
            color: #333;
            padding-right: 30px;
        }

        .shanghuPh {
            color: #B2B2B2;
            font-size: 28px;
        }

        .time_input {
            padding-right: 20px !important;
            z-index: 9;
            float: left !important;
            width: 430px !important;
        }


        .sectionBox .section_item .img text {
            display: block;
            width: 440px;
            float: left;
            color: #B2B2B2;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }

        .sectionBox .section_item .img image {
            padding: 38px 30px 30px 0px;
        }

        .plac_color {
            color: #B2B2B2;
        }

        .pickers {
            margin-left: 262px;
        }
    </style>
</head>

<body>
    <div class='tip'>上传法人的二代身份证原件，请确保图片清晰、四角齐全</div>
    <div class="photobox">
        <div class='img' >
            <img class="imgbos"></img>
            <img class='paizhao'></img>
            <text class="texts">法人身份证正面</text>
        </div>
        <div class='img' >
            <img class="imgbos"></img>
            <img class='paizhao'></img>
            <text class="texts">法人身份证反面</text>
        </div>
    </div>

    <div class='sectionBox'>
        <div class='section_item'>
            <label>姓名</label>
            <input type='text' placeholder='请输入法人姓名' placeholder-class='plac_color' />
        </div>

        <div class='section_item'>
            <label>证件号</label>
            <input type='text' placeholder='请输入证件号码' placeholder-class='plac_color' />
        </div>
        <div class='section_item'>
            <label>有效期自</label>
            <picker class='youxiao' mode="date" start="1960-09-01" end="2999-12-31">
                <div class="pickers">

                </div>
                <input class='time_input' placeholder-class='plac_color' placeholder='请选择' />
            </picker>
        </div>
        <div class='section_item'>
            <label>有效期至</label>
            <picker class='youxiao' mode="date" start="1960-09-01" end="2999-12-31">
                <div class="pickers">

                </div>
                <input  class='time_input' placeholder-class='plac_color' placeholder='请选择' />
            </picker>
        </div>

        <div class='section_item'>
            <label>微信号</label>
            <input type='text' placeholder-class='plac_color' placeholder='请输入需要微信号'></input>
        </div>
        <div class="section_item">
            <text class="text1">支付宝账号</text>
            <input placeholder-class='plac_color' placeholder="请输入支付宝账号"></input>
        </div>
    </div>


    <div class='btn bgcolor'>下一步</div>

</body>

</html>