<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>结算信息</title>
    <style>
        .box {
            background-color: #fff;
            margin-top: 20px;
        }

        .box .section {
            width: 100%;
            display: flex;
            justify-content: space-between;
            border-bottom: 1px solid #ddd;
            line-height: 40px;
        }

        .box .section label {
            display: inline-block;
            padding-left: 10px;
            float: left;
        }

        .box .section input {
            display: inline-block;
            width: 200px;
            line-height: 40px;
            float: left;
            border: none;
            text-align: right;
        }


        .box .section input.code {
            width: 200px;
            float: left;
        }

        .account {
            margin-left: 50px;
            display: flex;
            justify-content: space-between;
        }

        .account div {
            display: inline-block;
        }

        .account div:nth-child(2) {
            padding-left: 50px;
        }

        .choicexuan {
            width: 30px !important;
            height: 30px !important;
            padding-right: 20px;
        }

        .jiantou {
            width: 15px;
            height: 17px;
            padding: 12px 0;
        }

        .btn_code {
            display: inline-block;
            height: 90px;
            line-height: 90px;
            float: right;
            padding-right: 30px;
            /* color: #108EE9; */
        }

        .btn_code text {
            padding-right: 20px;
        }

        .btn {
            width: 300px;
            height: 40px;
            line-height: 40px;
            margin: 25px;
            text-align: center;
            color: #fff;
            background: #29A1F7;
            border-radius: 12px;
            border: none;
        }

        .section .choice image {
            width: 12px;
            height: 26px;
            /* float:right; */
            padding-top: 30px;
        }

        .picker {
            width: 440px;
            float: left;
        }

        .none {
            display: none;
        }

        .section image {
            width: 16px;
            height: 20px;
            padding-right: 10px;
        }

        .pickers {
            width: 420px;
            float: left;
            padding-right: 17px;
        }



        /* 省市区选择 */
        .mask {
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.4);
            position: fixed;
            top: 0;
            z-index: 999;
        }

        .address {
            background-color: #fff;
            position: absolute;
            z-index: 9999;
            height: 100%;
        }

        .address .mask_title {
            height:40px;
            line-height: 40px;
            font-size: 17px;
            color: #333333;
            text-align: center;
            border-bottom: 1px solid #ddd;

        }

        .left {
            float: left;
        }

        .right {
            float: right;
            padding-right: 30px;
        }

        .address .mask_con {
            font-size: 17px;
            color: #333333;
        }

        .address .mask_con .addressname {
            height: 40px;
            line-height: 40px;
            padding-left: 10px;
            border-bottom: 1px solid #ddd;
        }

        .address .mask_con .addressname text {
            display: block;
            float: left;
            margin-right: 20px;
            border-bottom: 4px solid #29A1F7;
        }

        .address .mask_con .areaname {
            height: 540px;
            overflow-y: auto;
        }

        .address .mask_con .areaname .areaname_list {
            height: 90px;
            line-height: 90px;
            padding-left: 30px;
            border-bottom: 1px solid #ddd;
        }

        .clearfix {
            height: 40px;
        }

        .times {
            width: 100%;
            line-height: 40px;
            border-bottom: 1px solid #ccc;
            padding-left: 10px;
        }

        .time_input {
            line-height: 40px;
            border: none;
            margin-left: 85px;
        }

        .time_jiantou {
            align-items: center;
            width: 15px;
            height: 17px;
        }
    </style>
</head>

<body>
    <form>
        <div class='box'>
            <div class='section'>
                <label>账户类型</label>
                <div class='account'>
                    <div id="01">
                        <text>私人</text>
                    </div>
                    <div id="02">
                        <text>对公</text>
                    </div>
                </div>
            </div>
            <!-- 对公 -->
            <div class='section clearfix'>
                <label>企业名称</label>
                <input type='text' placeholder='请输入企业名称' />
            </div>
            <!-- 开户许可证 -->
            <div class='section clearfix'>
                <label>开户许可证</label>
                <input type='text' placeholder='(必填) 请上传开户许可证' />
                <img class='jiantou' src="{{url('/payviews/img/zhishijiantou-zuo@2x.png')}}"></img>
            </div>
            <div class='section clearfix'>
                <label>开户许可证</label>
                <input type='text' placeholder='请上传开户许可证' />
                <img class='jiantou' src="{{url('/payviews/img/zhishijiantou-zuo@2x.png')}}"></img>
            </div>

            <!-- 对私 -->
            <div class='section clearfix'>
                <label>持卡人</label>
                <input type='text' placeholder='请输入持卡人名称' />
            </div>
            <!-- 个人姓名不同时 -->
            <div class='section clearfix'>
                <label>身份证照片</label>
                <input type='text' placeholder='请上传持卡人身份证照片' />
                <img class='jiantou' src="{{url('/payviews/img/zhishijiantou-zuo@2x.png')}}"></img>
            </div>

            <div class='section clearfix'>
                <label>身份证号码</label>
                <input type='text' placeholder='请输入持卡人身份证号码' />
            </div>

            <div class="times">
                <text style="display: inline-block;">有效期自</text>
                <picker mode="date" start="1980-09-01" end="2080-01-01">
                    <div class="pickers">

                    </div>
                    <input class='time_input' placeholder-class='plac_color' placeholder='持卡人身份证开始时间' />
                    <img class="time_jiantou" src='../img/zhishijiantou-zuo@2x.png'></img>
                </picker>
            </div>
            <div class='times'>
                <label>有效期至</label>
                <picker mode="date" start="1980-09-01" end="2080-01-01">
                    <div class="pickers">

                    </div>
                    <input class='time_input' placeholder-class='plac_color' placeholder='持卡人身份证过期时间' />
                    <img class="time_jiantou" src='../img/zhishijiantou-zuo@2x.png'></img>
                </picker>
            </div>

            <!-- 手持身份证照 -->
            <div class='section clearfix'>
                <label>手持身份证照</label>
                <input type='text' placeholder='持卡人手持身份证正面照' />
                <img class='jiantou' src="{{url('/payviews/img/zhishijiantou-zuo@2x.png')}}"></img>
            </div>
            <div class='section clearfix'>
                <label>授权结算书</label>
                <input type='text' placeholder='请上传授权结算书照片' />
                <img class='jiantou' src="{{url('/payviews/img/zhishijiantou-zuo@2x.png')}}"></img>
            </div>

            <!-- *******************公共********************** -->
            <div class='section clearfix'>
                <label>银行卡照片</label>
                <input type='text' placeholder='请上传' />
                <img class='jiantou' src="{{url('/payviews/img/zhishijiantou-zuo@2x.png')}}"></img>
            </div>

            <div class='section clearfix'>
                <label>银行类型</label>
                <input type='text' placeholder='请输入银行类型' />
                <img class='jiantou' src="{{url('/payviews/img/zhishijiantou-zuo@2x.png')}}"></img>
            </div>
            <div class='section clearfix'>
                <label>银行卡号</label>
                <input type='number' placeholder='请输入银行卡号' />
            </div>
        </div>

        <div class='box'>
            <div class='section clearfix'>
                <label>所属地区</label>
                <input placeholder='请选择所属地区' />
                <img class='jiantou' src="{{url('/payviews/img/zhishijiantou-zuo@2x.png')}}"></img>
            </div>

            <div class='section clearfix'>
                <label>开户支行</label>
                <input placeholder='请选择开户支行' />
                <img class='jiantou' src="{{url('/payviews/img/zhishijiantou-zuo@2x.png')}}"></img>
            </div>
            <div class='section clearfix' style="display:none">
                <label>手机号码</label>
                <input type='number' placeholder='请输入银行预留手机号'  />
            </div>
            <div class='section clearfix' style="display:none">
                <label>验证码</label>
                <input class='code' type='number' placeholder='请输入验证码'  />
                <div class='btn_code' >
                    <text>|</text>获取验证码</div>
                <div class='btn_code miao'>
                    <text>|</text>在获取</div>
            </div>
        </div>

        <!-- <div class='btn' bindtap='bindcard'>立即绑定</div> -->
        <button class='btn bgcolor' formType="submit">下一步</button>
    </form>

    <!-- 省市区选择 -->
    <div class='mask' style="display: none;">
        <div class='address'>
            <div class='mask_title'>
                <text class='left' style="padding-left:30px;">取消</text>
                <text>选择省</text>
                <text class='right mask_color'>确定</text>
            </div>
            <div class='mask_con'>
                <!-- <div class='mask_box'> -->
                <div class='addressname'>
                    <text>江苏</text>
                    <text>苏州</text>
                    <text>园区</text>
                </div>
                <div class='areaname'>
                    <div class='areaname_list'>江苏</div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>