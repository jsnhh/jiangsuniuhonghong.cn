<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>开户支行</title>
    <style>
        .bankList {
            line-height: 40px;
            font-size: 17px;
            border-bottom: 1px solid #ccc;
            margin-left: 10px;
        }

        .bankList image {
            width: 44px;
            height: 44px;
            vertical-align: middle;
            margin-top: -10px;
            padding: 0 30px;
        }

        .search {
            height: 60px;
            line-height: 40px;
            background-color: #108EE9;
        }

        .search .search_box {
            background-color: #fff;
            height: 40px;
            margin: 0 auto;
            position: absolute;
            top: 20px;
            left: 25px;

        }

        .search .search_box input {
            width: 250px;
            position: absolute;
            left: 20px;
            top: 4px;
            color: #333;
            font-size: 17px;
            padding: 5px;
            border: none;
        }

        .search .search_box image {
            width: 28px;
            height: 28px;
            position: absolute;
            left: 10px;
            z-index: 999;
            top: 16px;

        }
    </style>
</head>

<body>
    <div class='search'>
        <div class='search_box'>
            <img src="{{url('/payviews/img/sousuotubiao.png')}}"></img>
            <input type='text' placeholder="搜索" placeholder-class='searchs' placeholder-style="color:#999999" />
        </div>
    </div>

    <div class='box'>
        <div class='bankList'>
            <div>上海支行</div>
        </div>
    </div>
</body>

</html>