<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>门店地址</title>
    <style>
        .section {
            width: 100%;
            height: 40px;
            line-height: 40px;
            background-color: #fff;
            border-bottom: 1px solid #e5e5e5;
        }

        .section .choice {
            float: right;
            text-align: left;
            padding-right: 30px;
        }

        .section .choice image {
            width: 12px;
            height: 26px;
            float: right;
            padding-top: 30px;
        }

        .section label {
            padding-left: 10px;
            float: left;
        }

        .section input {
            line-height: 34px;
            text-align: right;
            border: none;
            width: 257px;
        }

        .picker {
            width: 430px;
            float: left;
        }

        .btn {
            width: 300px;
            margin: 50px auto;
            background: #29A1F7;
            border-radius: 6px;
            color: #ffffff;
            text-align: center;
            line-height: 45px;
        }



        /* 省市区选择 */
        .mask {
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.4);
            position: fixed;
            top: 0;
            z-index: 999;
        }

        .address {
            background-color: #fff;
            position: absolute;
            bottom: 0;
            z-index: 9999;
        }

        .address .mask_title {
            height: 40px;
            line-height: 40px;
            font-size: 17px;
            color: #333333;
            text-align: center;
            border-bottom: 1px solid #ddd;
        }

        .left {
            float: left;
        }

        .right {
            float: right;
        }

        .address .mask_con {
            font-size: 17px;
            color: #333333;
        }

        .address .mask_con .addressname {
            height: 90px;
            line-height: 90px;
            padding-left: 30px;
            border-bottom: 1px solid #ddd;
        }

        .address .mask_con .addressname text {
            display: block;
            float: left;
            height: 86px;
            line-height: 86px;
            margin-right: 20px;
            border-bottom: 4px solid #29A1F7;
        }

        .address .mask_con .areaname {
            height: 540px;
            overflow-y: auto;
        }

        .address .mask_con .areaname .areaname_list {
            height: 90px;
            line-height: 90px;
            padding-left: 30px;
            border-bottom: 1px solid #ddd;
        }
        .deatle{margin-left: 20px;}
    </style>
</head>

<body>
    <div>
        <div class='section address_input'>
            <label>省／市／区</label>
            <input type="text" placeholder='请选择地区'></input>
        </div>
        <div class='section'>
            <label>详细地址</label>
            <input class="deatle" type="text" placeholder='请填写详细地址'></input>
        </div>
    </div>

    <div class='btn bgcolor'>完成</div>



    <!-- 省市区选择 -->
    <div class='mask' style="display: none;">
        <div class='address'>
            <div class='mask_title'>
                <text class='left' style="padding-left:30px;">取消</text>
                <text>选择省</text>
                <text class='right mask_color'>确定</text>
            </div>
            <div class='mask_con'>
                <!-- <div class='mask_box'> -->
                <div class='addressname'>
                    <text>江苏</text>
                    <text>苏州</text>
                    <text>园区</text>
                </div>
                <div class='areaname'>
                    <div class='areaname_list'>江苏</div>
                </div>

                <!-- </div> -->
            </div>
        </div>
    </div>
</body>
</html>
<script type="text/javascript" src="{{asset('/phone/js/jquery-2.1.4.js')}}"></script>
<script type="text/javascript">

</script>