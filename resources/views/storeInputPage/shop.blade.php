<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>店铺信息</title>
    <style>
        .sectionBox .section_item {
            width: 100%;
            line-height: 40px;
            border-bottom: 1px solid #ddd;
            font-size: 17px;
            overflow: hidden;
        }

        .sectionBox .section_item label {
            float: left;
        }

        .sectionBox .section_item input {
            float: right;
            text-align: right;
            padding-right: 30px;
            line-height: 40px;
            border: none;
        }

        .sectionBox .section_item text.sfzNum {
            float: right;
            padding-right: 30px;
            color: #B2B2B2;
        }

        .sectionBox .section_item .img input.cshanghuAddress {
            width: 440px;
            text-align: right;
            padding-right: 5px;
            font-size: 28px;
            color: #333;
            padding-right: 30px;
        }

        .pickerss {
            text-align: right;
            padding-right: 30px;
        }

        .shanghuPh {
            color: #B2B2B2;
            font-size: 28px;
        }

        .time_input {
            padding-right: 20px !important;
            z-index: 9;
            float: left !important;
            width: 430rpx !important;
        }


        .sectionBox .section_item .img text {
            display: block;
            width: 440px;
            float: left;
            color: #B2B2B2;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }

        .sectionBox .section_item .img image {
            padding: 38px 30px 30px 0px;
        }

        .plac_color {
            color: #B2B2B2;
        }

        .photo {
            display: flex;
            justify-content: center;
        }

        .tip {
            background-color: #FFFADA;
            padding-left: 30px;
            height: 80px;
            line-height: 80px;
            font-size: 26px;
            color: #666666;
        }

        .photoImg {
            margin: 10px 10px auto;
            width: 150px;
            height: 110px;
            border: 1px solid #000;
        }

        .desc {
            text-align: center;
            font-size: 18px;
            color: #999999;
            padding-top: 10px;
        }

        .btn {
            width: 239px;
            position: fixed;
            color: #fff;
            text-align: center;
            border-radius: 45px;
            font-size: 20px;
            margin-left: 14%;
            background: #108ee9;
            padding: 5px;
            margin-top: 20px;
        }
        .person{
            width: 100%;
            text-align: left;
            line-height: 20px;
            color: #E60012;
            font-size: 14px;
            margin-top: 5px;
        }
    </style>
</head>

<body>
  <form>
  <div class='section'>
        <div class='sectionBox'>
            <div class='section_item'>
                <label>门店名称</label>
                <input type='text' placeholder='请输入营业执照上的名称' name="storeName" value='' placeholder-class='plac_color' />
            </div>
            <div class='section_item'>
                <label>门店简称</label>
                <input type='text' placeholder='请输入门店简称' name="storeSimplename" value='' placeholder-class='plac_color' />
            </div>
            <div class='section_item storeAddress'>
                <label>门店地址</label>
                <div class='img'>
                    <input placeholder='请选择' value='' placeholder-class='plac_color' />
                </div>
            </div>

            <div class='section_item category'>
                <label>经营品类</label>
               <input class='category_input' value='' placeholder-class='plac_color'placeholder='请选择' />
            </div>
            <div class='section_item'>
                <label>联系人</label>
                <input type='text' placeholder='请输入联系人姓名' name="storePeople" value='' placeholder-class='plac_color' />
            </div>
            <div class='section_item'>
                <label>邮箱</label>
                <input type='text' placeholder='请输入邮箱' name="storeEmil" value='' placeholder-class='plac_color' />
            </div>
        </div>
    </div>

    <div class='photo'>
        <div class='imglist'>
            <img class='photoImg' src='' ></img>
            <div class='desc'>门头照</div>
        </div>
        <div class='imglist'>
            <img class='photoImg' src=''></img>
            <div class='desc'>收银台照</div>
        </div>
    </div>

    <div class='photo'>
        <div class='imglist'>
            <img class='photoImg' src='' ></img>
            <div class='desc'>经营内容照</div>
        </div>
        <div class='imglist'>
            <img class='photoImg' src=''></img>
            <div class='desc'>店内全景照</div>
        </div>
    </div>
    <div class="person"></div>
    <div class='btn bgcolor'>下一步</div>
  </form>
</body>
</html>
<script type="text/javascript" src="{{asset('/phone/js/jquery-2.1.4.js')}}"></script>
<script type="text/javascript">
    $(function(){
        $(".storeAddress").on("click",function(){
            window.location.href="/authorize/addaddress";
        });
        $(".category").on("click",function(){
            // 商户经营性质
            $.ajax({
                url : "{{url('/api/basequery/store_category')}}",
                data : {token:sessionStorage.getItem("Usertoken")},
                type : 'get',
                success : function(data) {
                    console.log(data);
                },
                error : function(data) {
                    alert('查找板块报错');
                }
            });
        })
        $(".btn").on("click",function(){
            var storeName                 = $("input[name=storeName]").val();
            var storeSimplename      = $("input[name=storeSimplename]").val();
            var storePeople              = $("input[name=storePeople]").val();
            var storeEmil                  = $("input[name=storeEmil]").val();
            window.sessionStorage.setItem("storeName",storeName);
            window.sessionStorage.setItem("storeSimplename",storeSimplename);
            window.sessionStorage.setItem("storePeople",storePeople);
            window.sessionStorage.setItem("storeEmil",storeEmil);
            $(".person").html("");
            if(!storeName){
                $(".person").html("门店名称不可为空");
                return false;
            }
            if(!storeSimplename){
                $(".person").html("门店简称不可为空");
                return false;
            }
        });
    });
</script>