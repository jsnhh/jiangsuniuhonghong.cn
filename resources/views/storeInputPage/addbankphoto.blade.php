<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>银行卡照片</title>
    <style>
        .tip {
            background-color: #FFFADA;
            height: 40px;
            line-height: 40px;
            font-size: 14px;
            color: #666666;
            text-align: center;
        }

        .img {
            width: 270px;
            height: 175px;
            overflow: hidden;
            margin: 38px auto;
            position: relative;
            border: 1px solid;
        }

        .img image {
            width: 440px;
            height: 280px;
        }

        .img text {
            position: absolute;
            bottom: 13px;
            left: 84px;
            font-size: 17px;
        }

        .btn {
            width: 300px;
            height: 40px;
            line-height: 40px;
            margin: 0 auto;
            background-color: #108EE9;
            /* opacity: 0.4; */
            color: #fff;
            text-align: center;
            border-radius: 45px;
            font-size: 17px;
        }

        .img .paizhao {
            position: absolute;
            bottom: 53px;
            width: 70px;
            height: 70px;
            left: 37%;
        }
    </style>
</head>

<body>

    <div class='tip'>上传你需要绑定的银行卡正反面照片</div>
    <div class='img'>
        <img></img>
        <img class='paizhao' src="{{url('/payviews/img/paishe-shenfenzheng@2x.png')}}"></img>
        <text>正面</text>
    </div>
    <div class='img' >
        <img></img>
        <img class='paizhao' src="{{url('/payviews/img/paishe-shenfenzheng@2x.png')}}"></img>
        <text>反面</text>
    </div>


    <div class='btn bgcolor'>完成</div>
</body>

</html>