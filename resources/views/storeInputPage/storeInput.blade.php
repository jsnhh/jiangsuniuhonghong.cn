<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>商户进件</title>
    <style>
        .regbox {
            width: 100%;
            background: #fff;
            padding-top: 15px;
        }

        .topbox,
        .botbox {
            display: flex;
            justify-content: space-around;
            padding-bottom: 10px;
        }

        .boxx {
            text-align: center;
        }


        .bb {
            margin-left: 25px;
        }

        .typejieshao {
            font-size: 17px;
            margin-left: 25px;
            margin-top: 66px;
        }

        .storetype {
            width: 100%;
            margin-top: 20px;
            border-top: 1px solid rgb(150, 149, 149);
            border-bottom: 1px solid rgb(150, 149, 149);
        }

        .getitype {
            width: 319px;
            height: 76px;
            margin-top: 7px;
            margin-left: 18px;
            border-bottom: solid 1px rgb(150, 149, 149);
        }

        .geticontent {
            font-size: 13px;
            color: gray;
            margin-top: 10px;
            width: 660px;
        }

        .flexbox {
            display: flex;
        }

        .xiaowei {
            border: none;
        }
        img {
            width: 60px;
        }

        .tishi {
            width: 21px !important;
            height: 21px;
        }
    </style>
</head>

<body>
    <!--pages/authentication/authentication.wxml-->
    <div class="regbox">
        <div class="topbox">
            <div class="boxx bright individual">
                <img src="{{url('/payviews/img/store.png')}}"></img>
                <div>个体工商户</div>
            </div>
            <div class="boxx enterprise">
                <img src="{{url('/payviews/img/qiyeguanli.png')}}"></img>
                <div>企业</div>
            </div>
        </div>
        <div class="botbox miniStore">
            <div class="boxx bb bright">
                <img src="{{url('/payviews/img/phone.png')}}"></img>
                <div>小微商户</div>
            </div>
            <div class="boxx bb school">
                <img src="{{url('/payviews/img/jiaoyu.png')}}"></img>
                <div>学校教育行业</div>
            </div>
        </div>
    </div>

    <!-- 个体类型说明 -->
    <div class="typejieshao">温馨提示</div>
    <div class="storetype">
        <!-- 个体 -->
        <div class="getitype">
            <text class="web-font">个体工商户</text>
            <div class="flexbox">
                <div class="geticontent">营业执照上的主体类型一般为个体户、个体工商户、个体经营</div>
                <img class="tishi" src="{{url('/payviews/img/tishi.png')}}"></img>
            </div>
        </div>
        <!-- 企业 -->
        <div class="getitype">
            <text class="web-font">企业</text>
            <div class="flexbox">
                <div class="geticontent">营业执照上的主体类型一般为有限公司、有限责任公司</div>
                <img class="tishi" src="{{url('/payviews/img/tishi.png')}}"></img>
            </div>
        </div>
        <!-- 小微 -->
        <div class="getitype xiaowei">
            <text class="web-font">小微商户</text>
            <div class="flexbox">
                <div class="geticontent">根据法规和相关监管规定免于办理工商注册登记、无营业执照的实体特约商户</div>
                <img class="tishi" src="{{url('/payviews/img/tishi.png')}}"></img>
            </div>
        </div>
    </div>
    
</body>
<script type="text/javascript" src="{{asset('/phone/js/jquery-2.1.4.js')}}"></script>
<script type="text/javascript"> 
    $(function(){
        $(".individual").on("click",function(){
           window.location.href="/authorize/parts";
        });
        $(".enterprise").on("click",function(){
           window.location.href="/authorize/parts";
        });
        $(".miniStore").on("click",function(){
           window.location.href="/authorize/partsTow";
        });
        $(".school").on("click",function(){
           window.location.href="/authorize/partsTow";
        });
    });
</script>

</html>