<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>门店信息</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
        <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
        <style type="text/css">
            body,html{background-color: #fff;}
            .img{width:130px;height:90px;overflow: hidden;}
            .img img{width:100%;height:100%;}
            
            .up{position: relative;display: inline-block;cursor: pointer;border-color: #1ab394; color: #FFF;width: auto !important;font-size: 10px !important;text-align: center !important;}
            .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
            
            input::-webkit-outer-spin-button,
            input::-webkit-inner-spin-button {-webkit-appearance: none !important;margin: 0;}

            .userbox,.branchbox{height:200px;overflow-y: auto;z-index: 999;position: absolute;left: 0px;top: 85px;width:400px;background-color:#ffffff;border: 1px solid #ddd;}
            .userbox .list,.branchbox .list{ height:38px;line-height: 38px;cursor:pointer;padding-left:10px;}
            .userbox .list:hover,.branchbox .list:hover{background-color:#eeeeee;}

            .layclearfloat:after{content:"";display: block;clear:both;overflow: hidden;height:0;}
            .layui-hide{display: none !important;}
            .layui-layer-nobg{width: none !important;}
            .layui-card-header{width:80px;text-align: right;float:left;}
            .layui-upload-img{width: 100px; height: auto;}
            .layui-upload-list{width: 100px;height:96px;overflow: hidden;margin: 10px auto;}
            .layui-form-label-time{float: left;display: block;padding: 9px 15px;font-weight: 400;line-height: 20px;}
        </style>
    </head>
    <body>
        <div class="layui-fluid">
            <form class="layui-form" lay-filter="createStoreInfo">
                <!--第一部分-->
                <div class="layui-card content_every storeInfo1">
                    <div class="layui-card-header">门店信息</div>
                    <div class="layui-card-body layui-row layui-col-space10">
                        <div class="layui-row layui-form">
                            <div class="layui-col-md6"> 
                                <div class="layui-form-item xingzhi">
                                    <label class="layui-form-label" style="text-align:center;">入驻性质</label>
                                    <div class="layui-input-block">
                                        <select name="xingzhi" id="xingzhi" lay-filter="xingzhi"></select>
                                    </div>
                                </div>
                                <div class="layui-form-item storeitem">
                                    <label class="layui-form-label" style="text-align:center;">门店分类</label>
                                    <div class="layui-input-block">
                                        <select name="storeitem" id="storeitem" lay-filter="storeitem"></select>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label" style="text-align:center;">营业执照</label>
                                    <div class="layui-input-block">
                                        <input type="text" placeholder="请输入门店名称" class="layui-input item3">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label" style="text-align:center;">门店简称</label>
                                    <div class="layui-input-block">
                                        <input type="text" placeholder="请输入门店简称" class="layui-input item17">
                                    </div>
                                </div>
                                <div class="layui-form-item js_phone">
                                    <label class="layui-form-label" style="text-align:center;">联&nbsp;&nbsp;系&nbsp;&nbsp;人</label>
                                    <div class="layui-input-block">
                                        <input type="text" placeholder="请输入联系人" class="layui-input store_people">
                                    </div>
                                </div>
                                <div class="layui-form-item js_phone">
                                    <label class="layui-form-label" style="text-align:center;">手&nbsp;&nbsp;机&nbsp;&nbsp;号</label>
                                    <div class="layui-input-block">
                                        <input type="text" placeholder="请输入联系人手机号" class="layui-input phone">
                                    </div>
                                </div>
                                
                                <div class="layui-form-item js_phone">
                                    <label class="layui-form-label" style="text-align:center;">邮&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;箱</label>
                                    <div class="layui-input-block">
                                        <input type="text" placeholder="请输入联系人邮箱" class="layui-input email">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label"style="text-align:center;">商户所在地</label>
                                    <div class="layui-input-block addressall">
                                        <div class="layui-inline">
                                            <select name="province" lay-filter="filterProvince" id="province"></select>
                                        </div>
                                        <div class="layui-inline">
                                            <select name="city" lay-filter="filterCity" id="city"></select>
                                        </div>
                                        <div class="layui-inline">
                                            <select name="area" lay-filter="filterArea" id="area"></select>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item" >
                                    <label class="layui-form-label" style="text-align:center;">详细地址</label>
                                    <div class="layui-input-block">
                                        <input type="text" placeholder="请输入地址" class="layui-input item4">
                                    </div>
                                </div>
                            </div>
                            <div class="layui-col-md6"style="margin-top:50px;"> 
                                <div class="layui-card layclearfloat">
                                    <div class="layui-card-body" style="float:left;">
                                        <div class="layui-upload">
                                            <button class="layui-btn up" style="border-radius:5px;left:6px"><input type="file" name="img_upload" class="test3" style="width: 83px;height: 37px;">上传门头</button>
                                            <div class="layui-upload-list">
                                                <img class="layui-upload-img" id="demo3">
                                                <p id="demoText"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="layui-card-body" style="float:right;">
                                        <div class="layui-upload">
                                            <button class="layui-btn up" style="border-radius:5px;left:6px"><input type="file" name="img_upload" class="test4">收银台照</button>
                                            <div class="layui-upload-list">
                                                <img class="layui-upload-img" id="demo4">
                                                <p id="demoText"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="layui-card layclearfloat">
                                    <div class="layui-card-body" style="float:left;">
                                        <div class="layui-upload">
                                            <button class="layui-btn up" style="border-radius:5px"><input type="file" name="img_upload" class="test5">经营内容照</button>
                                            <div class="layui-upload-list">
                                                <img class="layui-upload-img" id="demo5">
                                                <p id="demoText"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="layui-card-body" style="float:right;">
                                        <div class="layui-upload">
                                            <button class="layui-btn up" style="border-radius:5px"><input type="file" name="img_upload" class="test6">店内全景照</button>
                                            <div class="layui-upload-list">
                                                <img class="layui-upload-img" id="demo6">
                                                <p id="demoText"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <div class="layui-input-block">
                                        <div class="layui-footer" data-index="2">
                                            <div class="layui-btn nextStoreInfo submit2" style="border-radius:5px">下一步</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--第二部分-->
                <div class="layui-card content_every storeInfo2 layui-hide">
                    <div class="layui-card-header">法人信息</div>
                    <div class="layui-card-body">
                        <div class="layui-row layui-form" lay-filter="component-form-group">  
                            <div class="layui-col-md6">              
                                <div class="layui-form-item" >
                                    <label class="layui-form-label" style="text-align:center;">法人姓名</label>
                                    <div class="layui-input-block">
                                        <input type="text" placeholder="请输入法人姓名" class="layui-input item1">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label" style="text-align:center;">法人身份证</label>
                                    <div class="layui-input-block">
                                        <input type="text" placeholder="请输入身份证号" class="layui-input item2">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label-time" style="overflow:visible;text-align:center;font-size: 10px;padding: 9px 13px;">身份证开始时间</label>
                                    <div class="layui-input-block">    
                                        <input type="text" class="layui-input frs-item test-item item18" placeholder="法人身份证开始时间" lay-key="21">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label-time" style="overflow:visible;text-align:center;font-size: 10px;padding: 9px 13px;">身份证过期时间</label>
                                    <div class="layui-input-block">
                                        <input type="text" class="layui-input fre-item test-item item16" placeholder="法人身份证过期时间" lay-key="22">
                                    </div>
                                </div>                    
                            </div>
                            <div class="layui-col-md6">                
                                <div class="layui-form-item">
                                    <div class="layui-card layclearfloat">                        
                                        <div class="layui-card-body" style="float:left;">
                                            <div class="layui-upload">
                                                <button class="layui-btn up"style="border-radius:5px;padding-left:7px;padding-right:7px;"><input type="file" name="img_upload" class="test1">上传身份证正面</button>
                                                <div class="layui-upload-list">
                                                    <img class="layui-upload-img" id="demo1">
                                                    <p id="demoText"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-card-body" style="float:right;">
                                            <div class="layui-upload">
                                                <button class="layui-btn up"style="border-radius:5px;padding-left:7px;padding-right:7px;"><input type="file" name="img_upload" class="test2">上传身份证反面</button>
                                                <div class="layui-upload-list">
                                                    <img class="layui-upload-img" id="demo2">
                                                    <p id="demoText"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>   
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <div class="layui-footer" data-index="3">
                                        <div class="layui-btn nextStoreInfo2 submit1"style="border-radius:5px">下一步</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--第三部分-->
                <div class="layui-card content_every storeInfo3 layui-hide">
                    <div class="layui-card-header">账户信息</div>
                    <div class="layui-card-body layui-row layui-col-space10" style="margin-left:0px;padding:0px;">
                        <div class="layui-row layui-form">
                            <div class="layui-col-md6"> 
                                <div class="layui-form-item">
                                    <label class="layui-form-label" style="text-align:center;">卡&nbsp;&nbsp;类&nbsp;&nbsp;型</label>
                                    <div class="layui-input-block">
                                        <select name="cardtype" id="cardtype" lay-filter="cardtype">
                                            <option value="">选择卡类型</option>
                                            <option value="01">私人</option>
                                            <option value="02">对公</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label" style="text-align:center;">银行卡号</label>
                                    <div class="layui-input-block">
                                        <input type="text" placeholder="请输入银行卡号" class="layui-input item7">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label" style="text-align:center;">银行户主</label>
                                    <div class="layui-input-block">
                                        <input type="text" placeholder="请输入银行户主" class="layui-input item8">
                                    </div>
                                </div>                    
                                

                                <div class="layui-form-item">
                                    <label class="layui-form-label" style="text-align:center;">所属地区</label>
                                    <div class="layui-input-block addressall">
                                        <div class="layui-inline">
                                            <select name="provincebank" lay-filter="filterProvincebank" id="provincebank"></select>
                                        </div>
                                        <div class="layui-inline">
                                            <select name="citybank" lay-filter="filterCitybank" id="citybank"></select>
                                        </div>
                                        <div class="layui-inline">
                                            <select name="areabank" lay-filter="filterAreabank" id="areabank"></select>
                                        </div>
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label" style="text-align:center;">所属银行</label>
                                    <div class="layui-input-block js_bank">
                                        <select name="bank" id="bank" lay-filter="bank" lay-search>
                                        </select>
                                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="搜索所属银行" class="layui-input transfer" style='margin-top:10px;'>
                                        <div class="userbox" style='display: none'></div>
                                    </div>
                                    
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label" style="text-align:center;">所属支行</label>
                                    <div class="layui-input-block subbank">
                                        <select name="subbank" id="subbank" lay-filter="subbank" lay-search>
                                        </select>
                                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="搜索所属支行" class="layui-input branchbank" style='margin-top:10px;'>
                                        <div class="branchbox" style='display: none'></div>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label" style="text-align:center;">联&nbsp;&nbsp;行&nbsp;&nbsp;号</label>
                                    <div class="layui-input-block">
                                        <input type="text" placeholder="请输入联行号" class="layui-input item12" disabled="">
                                    </div>
                                </div>

                                <div class="layui-form-item different">
                                    <div class="" style="width:70px;font-size: 10px;margin-left:13px;display:inline-block;">持卡人身份证开始时间</div>
                                    <div class="layui-inline">                          
                                        <div class="layui-input-inline" style="margin-top: -40px;">
                                            <input type="text" class="layui-input test-item haves-item item20" placeholder="持卡人身份证开始时间" lay-key="25">
                                        </div>
                                    </div>
                                </div> 
                                <div class="layui-form-item different">
                                    <div class="" style="width:70px;font-size: 10px;margin-left:13px;display:inline-block;">持卡人身份证结束时间</div>
                                    <div class="layui-inline">                          
                                        <div class="layui-input-inline" style="margin-top: -40px;">
                                            <input type="text" class="layui-input test-item havee-item item21" placeholder="持卡人身份证过期时间" lay-key="26">
                                        </div>
                                    </div>
                                </div> 
                                <div class="layui-form-item different">
                                <div class="" style="width:70px;font-size: 10px;text-align:center;margin-left:10px;display:inline-block;">持卡人身份证号</div>
                                    <div class="layui-inline">                          
                                        <div class="layui-input-inline" style="margin-top: -40px;">
                                            <input type="text" class="layui-input test-item item23" placeholder="持卡人身份证号" lay-key="26">
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="layui-col-md6"> 
                                <div class="layui-form-item" style="width:100%">
                                    <div class="layui-card">                        
                                        <div class="layui-card-body bank" style="float:left;">
                                            <div class="layui-upload">
                                                <button class="layui-btn up" style="border-radius:5px;padding-left:7px;padding-right:7px;"><input type="file" name="img_upload" class="test7">上传银行卡正面</button>
                                                <div class="layui-upload-list">
                                                <img class="layui-upload-img" id="demo7">
                                                <p id="demoText"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-card-body bank" style="float:left;">
                                            <div class="layui-upload">
                                                <button class="layui-btn up" style="border-radius:5px;padding-left:7px;padding-right:7px;"><input type="file" name="img_upload" class="test8">上传银行卡反面</button>
                                                <div class="layui-upload-list">
                                                <img class="layui-upload-img" id="demo8">
                                                <p id="demoText"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-card-body bank" style="float:left;">
                                            <div class="layui-upload">
                                                <button class="layui-btn up" style="border-radius:5px;padding-left:7px;padding-right:7px;"><input type="file" name="img_upload" class="test17">手持身份证正面</button>
                                                <div class="layui-upload-list">
                                                <img class="layui-upload-img" id="demo17">
                                                <p id="demoText"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-card-body different" style="float:left;">
                                            <div class="layui-upload">
                                                <button class="layui-btn up" style="border-radius:5px;padding-left:7px;padding-right:7px;"><input type="file" name="img_upload" class="test14">持卡人身份证正面</button>
                                                <div class="layui-upload-list">
                                                <img class="layui-upload-img" id="demo14">
                                                <p id="demoText"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-card-body different" style="float:left;">
                                            <div class="layui-upload">
                                                <button class="layui-btn up" style="border-radius:5px;padding-left:7px;padding-right:7px;"><input type="file" name="img_upload" class="test15">持卡人身份证反面</button>
                                                <div class="layui-upload-list">
                                                <img class="layui-upload-img" id="demo15">
                                                <p id="demoText"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-card-body different" style="float:left;">
                                            <div class="layui-upload">
                                                <button class="layui-btn up" style="border-radius:5px"><input type="file" name="img_upload" class="test16">授权结算书</button>
                                                <div class="layui-upload-list">
                                                <img class="layui-upload-img" id="demo16">
                                                <p id="demoText"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <div class="layui-footer" data-index="4">
                                        <div class="layui-btn nextStoreInfo3 submit3"style="border-radius:5px">下一步</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--第四部分-->
                <div class="layui-card content_every storeInfo4 layui-hide">
                    <div class="layui-card-header">证件照信息</div>
                    <div class="layui-card-body layui-row layui-col-space10">
                        <div class="layui-row layui-form">
                            <div class="layui-col-md6">
                                <div class="layui-form-item per_lincse">
                                    <label class="layui-form-label" style="text-align:center;overflow: visible;">营业执照编号</label>
                                    <div class="layui-input-block">
                                        <input type="text" placeholder="请输入营业执照编号" class="layui-input item14">
                                    </div>
                                </div>
                                <div class="layui-form-item per_lincse">
                                <div class="" style="width:70px;font-size: 10px;margin-left:10px;display:inline-block;text-align:center;">营业执照开始时间</div>
                                    <div class="layui-inline" style="margin-top: -40px;">                          
                                        <div class="layui-input-inline">
                                            <input type="text" class="layui-input end-item test-item item22" placeholder="营业执照开始时间" lay-key="24">
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item per_lincse">
                                <div class="" style="width:70px;font-size: 10px;margin-left:10px;display:inline-block;text-align:center;">营业执照结束时间</div>
                                    <div class="layui-inline" style="margin-top: -40px;">                          
                                        <div class="layui-input-inline">
                                            <input type="text" class="layui-input start-item test-item item15" placeholder="营业执照过期时间" lay-key="23">
                                        </div>
                                        <div style="margin-left: 15px;">
                                            <input type="checkbox" name="long_time" value="2999-12-31" title="长期">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-col-md6" style="margin-top:-20px">
                                <div class="layui-form-item" style="width:100%"> 
                                    <div class="layui-card">                        
                                        <div class="layui-card-body public" style="float:left;">
                                            <div class="layui-upload">
                                                <button class="layui-btn up" style="border-radius:5px;padding-left:7px;padding-right:7px;"><input type="file" name="img_upload" class="test9">上传营业执照</button>
                                                <div class="layui-upload-list">
                                                    <img class="layui-upload-img" id="demo9">
                                                    <p id="demoText"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-card-body public" style="float:left;">
                                            <div class="layui-upload">
                                                <button class="layui-btn up" style="border-radius:5px;padding-left:7px;padding-right:7px;"><input type="file" name="img_upload" class="test10">上传开户许可证</button>
                                                <div class="layui-upload-list">
                                                    <img class="layui-upload-img" id="demo10">
                                                    <p id="demoText"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-card-body per" style="float:left;">
                                            <div class="layui-upload">
                                                <button class="layui-btn up" style="border-radius:5px;padding-left:7px;padding-right:7px;left:4px"><input type="file" name="img_upload" class="test11">手持身份证照</button>
                                                <div class="layui-upload-list">
                                                    <img class="layui-upload-img" id="demo11">
                                                    <p id="demoText"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-card-body per" style="float:left;">
                                            <div class="layui-upload">
                                                <button class="layui-btn up" style="border-radius:5px;padding-left:7px;padding-right:7px;left:4px"><input type="file" name="img_upload" class="test12">人站在门口照</button>
                                                <div class="layui-upload-list">
                                                    <img class="layui-upload-img" id="demo12">
                                                    <p id="demoText"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <div class="layui-footer">
                                        <button class="layui-btn submit4" lay-submit lay-filter="submit4" style="border-radius:5px">保存信息</button>
                                    </div>
                                </div>
                            </div>
                        </div>     
                    </div>
                </div>
            </form>
        </div>

        <!-- 商户性质 -->
        <input type="hidden" class="store_name" value="">
        <input type="hidden" class="store_type" value="">
        <input type="hidden" class="category_name" value="">
        <input type="hidden" class="category_id" value="">

        <!-- 卡类型 -->
        <input type="hidden" class="cardtype_id" value="">

        <!-- 银行卡 -->
        <input type="hidden" class="bankname" value="">
        <input type="hidden" class="sub_bank_name" value="">
        <input type="hidden" class="bank_no" value="">

        <!-- 地区 -->
        <input type="hidden" class="provincecode" value="">
        <input type="hidden" class="provincename" value="">
        <input type="hidden" class="citycode" value="">
        <input type="hidden" class="cityname" value="">
        <input type="hidden" class="areacode" value="">
        <input type="hidden" class="areaname" value="">

        <!-- 银行卡卡户地区 -->
        <input type="hidden" class="provincecodebank" value="">
        <input type="hidden" class="provincenamebank" value="">
        <input type="hidden" class="citycodebank" value="">
        <input type="hidden" class="citynamebank" value="">
        <input type="hidden" class="areacodebank" value="">
        <input type="hidden" class="areanamebank" value="">

    </body>
    <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script> 
    <script type="text/javascript">

            var token = sessionStorage.getItem("Usertoken");
            layui.config({
                base: '../../layuiadmin/' //静态资源所在路径
            }).extend({
                index: 'lib/index', //主入口模块
                formSelects: 'formSelects'
            }).use(['index', 'form','upload','formSelects','laydate','jquery'], function(){
                var $ = layui.$
                    ,admin = layui.admin
                    ,element = layui.element
                    ,layer = layui.layer
                    ,laydate = layui.laydate
                    ,form = layui.form
                    ,upload = layui.upload
                    ,formSelects = layui.formSelects;
                    var $ = jquery = layui.jquery;

                /**
                 * 封装生成门店id
                 */
                function createStoreId(){
                    var data = new Date();
                    var year = data.getFullYear();
                    var mon  = data.getMonth() + 1;
                    var day  = data.getDate();
                    var h    = data.getHours();
                    var m    = data.getMinutes();
                    var s    = data.getSeconds();
                    //0-9的随机数
                    var arr  = [];
                    //容器
                    for(var i =0;i<6;i++){
                        //循环六次
                        var num = Math.random()*9;
                        //Math.random();每次生成(0-1)之间的数;
                        num = parseInt(num,10);
                        arr.push(num);
                    }
                    return year+''+mon+''+day+''+h+''+m+''+s+''+arr.join('');
                }
                //获取并且生成门店id，保存到变量容器中
                var store_id = createStoreId();
                 console.log(store_id)
                /**
                 * 点击下一步，进行部分切换
                 */
                $(".nextStoreInfo").on("click",function(){
                    /**
                     * 第一部分信息验证
                     */
                    if(!$('.item3').val() && !$('.phone').val()&& !$('.store_people').val()){
                        layer.msg("请完善门店信息", {icon:1, shade:0.5, time:1000});
                    }else{
                        let index = $(this).parent().data("index");
                    $(".layui-fluid .content_every").addClass("layui-hide");
                    $(".storeInfo"+index).removeClass("layui-hide");
                    }
                });

                $(".nextStoreInfo2").on("click",function(){
                    /**
                     * 第二部分信息验证
                     */
                    if(!$('.item2').val()){
                        layer.msg("请上传法人信息及身份证照片", {icon:1, shade:0.5, time:1000});
                    }else{
                        let index = $(this).parent().data("index");
                    $(".layui-fluid .content_every").addClass("layui-hide");
                    $(".storeInfo"+index).removeClass("layui-hide");
                    }
                });

                $(".nextStoreInfo3").on("click",function(){
                    /**
                     * 第三部分信息验证
                     */
                    if(!$('.cardtype_id').val() && !$('.item8').val()){
                        layer.msg("请上传账户信息及照片", {icon:1, shade:0.5, time:1000});
                    }else{
                        let index = $(this).parent().data("index");
                    $(".layui-fluid .content_every").addClass("layui-hide");
                    $(".storeInfo"+index).removeClass("layui-hide");
                    }
                });

                //第一部分逻辑处理-----------------------------------------------------------------------------

                /**
                 * 入驻性质数据
                 */
                $.ajax({
                    url : "{{url('/api/basequery/store_type')}}",
                    data : {token:token},
                    type : 'get',
                    success : function(data) {
                        var optionStr = "";
                        for(var i=0;i<data.data.length;i++){
                            if(i == 0){
                                optionStr += "<option value='" + data.data[i].store_type + "' "+"selected"+">"+ data.data[i].store_type_desc + "</option>";
                            }else{
                                optionStr += "<option value='" + data.data[i].store_type + "'>"+ data.data[i].store_type_desc + "</option>";
                            }
                        }
                        $("#xingzhi").append('<option value="">请选择商户类型</option>'+optionStr);
                        layui.form.render('select');
                    },
                    error : function(data) {
                        alert('查找板块报错');
                    }
                });

                /**
                 * 门店分类数据
                 */
                $.ajax({
                    url : "{{url('/api/basequery/store_category')}}",
                    data : {token:token},
                    type : 'get',
                    success : function(data) {
                        var optionStr = "";
                        for(var i=0;i<data.data.length;i++){
                            if(i==0){
                                optionStr += "<option value='" + data.data[i].category_id + "' "+"selected"+">"+ data.data[i].category_name + "</option>";
                            }else{
                                optionStr += "<option value='" + data.data[i].category_id + "'>"+ data.data[i].category_name + "</option>";
                            }
                        }
                        $("#storeitem").append('<option value="">请选择商户经营性质</option>'+optionStr);
                        layui.form.render('select');
                    },
                    error : function(data) {
                        alert('查找板块报错');
                    }
                });

                /**
                 * 选择入驻性质的数据
                 */
                form.on('select(xingzhi)', function(data){            
                    category = data.value;
                    categoryName = data.elem[data.elem.selectedIndex].text; 
                    $('.store_type').val(category);    
                    $('.store_name').val(categoryName);    
                    if(category == 1){
                        $('.bank').show();
                        $('.per').hide();
                        $('.cardtype').show();
                        $('.public').show();
                        $('.per_lincse').show();
                        if($('.cardtype_id').val()=='01'){
                            if($(".item1").val() == $('.item8').val()){
                                $('.different').hide();
                            }else{
                                $('.different').show();
                            } 
                        }else{
                            $('.different').hide();
                            $('.bank').hide();
                        }
                        $('#cardtype option').each(function(){
                            if($(this).val()==='01'){
                                $(this).attr('selected','selected');
                            }
                            $('.cardtype_id').val('01')
                        });
                        layui.form.render('select');
                        if($('.cardtype_id').val() == '01'){
                            if($(".item1").val() == $('.item8').val()){
                                $('.different').hide();
                            }else{
                                $('.different').show();
                            }
                            $('.bank').show()
                        }else if($('.cardtype_id').val() == '02'){
                            if($(".item1").val() == $('.item8').val()){
                                $('.different').hide();
                            }else{
                                $('.different').show();
                            }
                            $('.bank').show();
                        }
                        $('.bank').show()
                    }else if(category == 2){
                        $('.per').hide();
                        $('.cardtype').show();
                        $('.public').show();
                        $('.per_lincse').show();
                        if($('.cardtype_id').val()=='01'){
                            if($(".item1").val() == $('.item8').val()){
                                $('.different').hide();
                            }else{
                                $('.different').show();
                            }
                        }else{
                            $('.bank').hide();
                        }
                        $('#cardtype option').each(function(){                        
                            if('02'==$(this).val()){
                                $(this).attr('selected','selected');
                            }
                            $('.cardtype_id').val('02')
                        });
                        layui.form.render('select');
                        $('.bank').hide();
                        $('.different').hide()
                    }else{
                        $('.per').show();
                        $('.public').hide();
                        $('.cardtype').hide();
                        $('.different').hide();
                        $('.per_lincse').hide();
                    }
                });

                // 选择商户经营性质
                form.on('select(storeitem)', function(data){            
                    category = data.value;  
                    categoryName = data.elem[data.elem.selectedIndex].text; 
                    $('.category_id').val(category);
                    $('.category_name').val(categoryName);
                });

                //调用获取地区数据函数，页面加载完成立即执行
                getProvince();

                /**
                * 地区数据：省
                */
                function getProvince(){
                    $.ajax({
                        url : "{{url('/api/basequery/city')}}",
                        data : {area_code:'1'},
                        type : 'get',
                        success : function(data) {
                            var optionStr = "";
                            for(var i=0;i<data.data.length;i++){
                                if(i==0){
                                    optionStr += "<option value='" + data.data[i].area_code + "' "+"selected"+">"+ data.data[i].area_name + "</option>";
                                }else{
                                    optionStr += "<option value='" + data.data[i].area_code + "'>"+ data.data[i].area_name + "</option>";
                                }
                            }
                            $("#province").append('<option value="">请选择省</option>'+optionStr);
                            layui.form.render('select');
                            getCity(data.data[0].area_code);
                        },
                        error : function(data) {
                            alert('查找板块报错');
                        }
                    });
                }
                
                /**
                * 地区：市
                */
                function getCity(area_code){
                    $.ajax({
                        url : "{{url('/api/basequery/city')}}",
                        data : {area_code:area_code ? area_code : $('.provincecode').val()},
                        type : 'get',
                        success : function(data) {
                            var optionStr = "";
                            for(var i=0;i<data.data.length;i++){
                                if(i==0){
                                    optionStr += "<option value='" + data.data[i].area_code + "' "+"selected"+">"+ data.data[i].area_name + "</option>";
                                }else{
                                    optionStr += "<option value='" + data.data[i].area_code + "'>"+ data.data[i].area_name + "</option>";
                                }
                            }    
                            $("#city").append('<option value="">请选择市</option>'+optionStr);
                            layui.form.render('select');
                            getArea(data.data[0].area_code);
                        },
                        error : function(data) {
                            alert('查找板块报错');
                        }
                    });
                }
                
                
                /**
                * 地区： 区
                */
                function getArea(area_code){
                    $.ajax({
                        url : "{{url('/api/basequery/city')}}",
                        data : {area_code:area_code ? area_code : $('.citycode').val()},
                        type : 'get',
                        success : function(data) {
                            var optionStr = "";
                            for(var i=0;i<data.data.length;i++){
                                if(i==0){
                                    optionStr += "<option value='" + data.data[i].area_code + "' "+"selected"+">"+ data.data[i].area_name + "</option>";
                                }else{
                                    optionStr += "<option value='" + data.data[i].area_code + "'>"+ data.data[i].area_name + "</option>";
                                }
                            } 
                            $("#area").append('<option value="">请选择区</option>'+optionStr);
                            layui.form.render('select');
                        },
                        error : function(data) {
                            alert('查找板块报错');
                        }
                    });
                }

                /**
                 * 上传门头照片
                 */
                var uploadInst = upload.render({
                    url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token,  //提交到的地址 可以自定义其他参数
                    elem : '.test3',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
                    method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
                    type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
                    ext : 'jpg|png|gif',    //自定义支持的文件格式
                    unwrap : true, //是否不改变input的样式风格。默认false 
                    size : 5120,
                    before : function(input){
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                    },
                    done: function(res){
                        if(res.status == 0){
                            layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                        }else{
                            layer.msg("文件上传成功", {icon:1, shade:0.5, time:1000});
                            layui.jquery('#demo3').attr("src", res.data.img_url);
                        }
                    }
                });
                
                /**
                 * 上传收银台照
                 */
                var uploadInst = upload.render({
                    url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token,  //提交到的地址 可以自定义其他参数
                    elem : '.test4',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
                    method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
                    type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
                    ext : 'jpg|png|gif',    //自定义支持的文件格式
                    unwrap : true, //是否不改变input的样式风格。默认false 
                    size : 5120,
                    before : function(input){
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                    },
                    done: function(res){
                        if(res.status == 0){
                            layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                        }else{
                            layer.msg("文件上传成功", {icon:1, shade:0.5, time:1000});
                            layui.jquery('#demo4').attr("src", res.data.img_url);
                        }
                    }
                });
                
                /**
                 * 上传经营内容照
                 */
                var uploadInst = upload.render({
                    url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token,  //提交到的地址 可以自定义其他参数
                    elem : '.test5',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
                    method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
                    type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
                    ext : 'jpg|png|gif',    //自定义支持的文件格式
                    unwrap : true, //是否不改变input的样式风格。默认false 
                    size : 5120,
                    before : function(input){
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                    },
                    done: function(res){
                        if(res.status == 0){
                            layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                        }else{
                            layer.msg("文件上传成功", {icon:1, shade:0.5, time:1000});
                            layui.jquery('#demo5').attr("src", res.data.img_url);
                        }
                    }
                });
                
                /**
                 * 上传店内全景照
                 */
                var uploadInst = upload.render({
                    url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token,  //提交到的地址 可以自定义其他参数
                    elem : '.test6',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
                    method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
                    type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
                    ext : 'jpg|png|gif',    //自定义支持的文件格式
                    unwrap : true, //是否不改变input的样式风格。默认false 
                    size : 5120,
                    before : function(input){
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                    },
                    done: function(res){
                        if(res.status == 0){
                            layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                        }else{
                            layer.msg("文件上传成功", {icon:1, shade:0.5, time:1000});
                            layui.jquery('#demo6').attr("src", res.data.img_url);
                        }
                    }
                });

                //第二部分逻辑处理-----------------------------------------------------------------
                
                /**
                 * 上传身份证正面
                 */
                var uploadInst = upload.render({
                    url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token+"&img_type="+"2"+"&attach_name="+"head_sfz_img_a_url"+"&type="+"img", //提交到的地址 可以自定义其他参数
                    elem : '.test1',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
                    method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
                    type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
                    ext : 'jpg|png|gif',    //自定义支持的文件格式
                    unwrap : true, //是否不改变input的样式风格。默认false 
                    size : 5120,
                    before : function(input){
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                    },
                    done: function(res){
                        if(res.status == 0){
                            layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                        }else{
                            layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                            layui.jquery('#demo1').attr("src", res.data.img_url);
                            layui.jquery('.item1').val(res.data.sfz_name);
                            layui.jquery('.item2').val(res.data.sfz_no);
                        }
                    }
                });

                /**
                 * 上传身份证反面
                 */
                var uploadInst = upload.render({
                    url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token+"&img_type="+"3"+"&attach_name="+"head_sfz_img_b_url"+"&type="+"img",  //提交到的地址 可以自定义其他参数
                    elem : '.test2',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
                    method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
                    type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
                    ext : 'jpg|png|gif',    //自定义支持的文件格式
                    unwrap : true, //是否不改变input的样式风格。默认false 
                    size : 5120,
                    before : function(input){
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                    },
                    done: function(res){
                        if(res.status == 0){
                            layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                        }else{
                            layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                            layui.jquery('#demo2').attr("src", res.data.img_url);
                            if(res.data.sfz_stime!=''){
                                layui.jquery('.item18').val(res.data.sfz_stime);
                            }
                            if(res.data.sfz_time!=''){
                                layui.jquery('.item16').val(res.data.sfz_time);
                            }
                        }
                    }
                });

                //第三部分逻辑处理-----------------------------------------------------------------

                //调用所属银行函数
                nostore();
                /**
                 * 所属银行
                 */
                function nostore(){
                    $.post("{{url('/api/basequery/bank')}}",{
                        token:token
                    },function(data){
                        if(data.status==1){
                            var optionStr = "";
                            for(var i=0;i<data.data.length;i++){
                                optionStr += "<option value='" + data.data[i].bankname + "'>" + data.data[i].bankname + "</option>";
                            }
                            $("#bank").append('<option value="">请选择银行</option>'+optionStr);
                            layui.form.render('select');
                        }
                    },"json");
                }

                
                $(".userbox").on("click",".list",function(){
                    $('.userbox').hide();
                    $(".transfer").val('');
                    var bankname=$(this).attr('data');
                    $('.bankname').val(bankname); 

                    $.post("{{url('/api/basequery/bank')}}",{
                        token:token,
                        bankname:$(this).attr('data')
                    },function(data){
                        if(data.status==1){
                            var optionStr = "";
                            for(var i=0;i<data.data.length;i++){
                                optionStr += "<option value='" + data.data[i].bankname + "' "+((bankname==data.data[i].bankname)?"selected":"")+">" + data.data[i].bankname + "</option>";
                            }    
                            $("#bank").append('<option value="">请选择银行</option>'+optionStr);
                            layui.form.render('select');                        
                        }
                    }, "json");

                    $.post("{{url('/api/basequery/sub_bank')}}",{
                        token:token,
                        bank_name:$(this).attr('data'),
                        l:'500'
                    },function(data){
                        if(data.status==1){
                            var optionStr = "";
                            for(var i=0;i<data.data.length;i++){
                                optionStr += "<option value='" + data.data[i].bank_no + "'>"+ data.data[i].sub_bank_name + "</option>";
                            }
                            $("#subbank").html('');
                            $("#subbank").append('<option value="">选择所属支行</option>'+optionStr);
                            layui.form.render('select');
                        }
                    },"json");

                });

                /**
                 * 输入框中搜索所属支行，实时搜索
                 */
                $(".branchbank").bind("input propertychange",function(event){
                  $.post("{{url('/api/basequery/sub_bank')}}",{
                      token:token,
                      bank_name:$('.bankname').val(),                        
                      sub_bank_keyword:$(this).val()
                  },function(res){
                      var html="";
                      for(var i=0;i<res.data.length;i++){
                          html+='<div class="list" data='+res.data[i].bank_no+'>'+res.data[i].sub_bank_name+'</div>';
                      }
                      $(".branchbox").show();
                      $('.branchbox').html('');
                      $('.branchbox').append(html);
                  },"json");
                });

                /**
                 * 点击选中搜索出来的所属支行数据
                 */
                $(".branchbox").on("click",".list",function(){
                    $('.branchbox').hide();
                    var bankno=$(this).attr('data');
                    var sub_bank_name=$(this).html();
                    $('.bank_no').val(bankno); 
                    $('.sub_bank_name').val(sub_bank_name);
                    $(".branchbank").val(sub_bank_name);
                    $('.item12').val(bankno);
                    $('.subbank .layui-form-select .layui-select-title input').val('')
                });

                /**
                 * 第三部分中，所属地区获取地区省份数据
                 */
                $.ajax({
                    url : "{{url('/api/basequery/city')}}",
                    data : {area_code:'1'},
                    type : 'get',
                    success : function(data) {
                        var optionStr = "";
                        for(var i=0;i<data.data.length;i++){
                            optionStr += "<option value='" + data.data[i].area_code + "'>"+ data.data[i].area_name + "</option>";
                            
                        }
                        $("#province").append('<option value="">请选择省</option>'+optionStr);
                        $("#provincebank").append('<option value="">请选择省</option>'+optionStr);
                        layui.form.render('select');
                    },
                    error : function(data) {
                        alert('查找板块报错');
                    }
                });
                
                /**
                 * 第三部分中，当选中省份之后，进行搜索该省份下面的市
                 */
                form.on('select(filterProvince)', function(data){
                    category = data.value;
                    categoryName = data.elem[data.elem.selectedIndex].text;
                    $('.provincecode').val(category);
                    $('.provincename').val(categoryName);
                    $("#city").html('');
                    $.ajax({
                        url : "{{url('/api/basequery/city')}}",
                        data : {area_code:category},
                        type : 'get',
                        success : function(data) {
                            var optionStr = "";
                            for(var i=0;i<data.data.length;i++){
                                if(i==0){
                                    optionStr += "<option value='" + data.data[i].area_code + "' "+"selected"+">"+ data.data[i].area_name + "</option>";
                                }else{
                                    optionStr += "<option value='" + data.data[i].area_code + "'>"+ data.data[i].area_name + "</option>";
                                }
                            }
                            $("#city").append('<option value="">请选择市</option>'+optionStr);
                            layui.form.render('select');
                        },
                        error : function(data) {
                            alert('查找板块报错');
                        }
                    });
                });

                /**
                 * 第三部分中，当选中市之后，进行搜索该市下面的区
                 */
                form.on('select(filterCity)', function(data){            
                    category = data.value;  
                    categoryName = data.elem[data.elem.selectedIndex].text; 
                    $('.citycode').val(category);
                    $('.cityname').val(categoryName);
                    $("#area").html('');
                    $.ajax({
                        url : "{{url('/api/basequery/city')}}",
                        data : {area_code:category},
                        type : 'get',
                        success : function(data) {
                            var optionStr = "";
                            for(var i=0;i<data.data.length;i++){
                                if(i==0){
                                    optionStr += "<option value='" + data.data[i].area_code + "' "+"selected"+">"+ data.data[i].area_name + "</option>";
                                }else{
                                    optionStr += "<option value='" + data.data[i].area_code + "'>"+ data.data[i].area_name + "</option>";
                                }
                            }
                            $("#area").append('<option value="">请选择县/区</option>'+optionStr);
                            layui.form.render('select');
                        },
                        error : function(data) {
                            alert('查找板块报错');
                        }
                    });
                });

                /**
                 * 暂时保留，？？？？？？？？？？？？？？？？？？？
                 */
                form.on('select(filterArea)', function(data){
                    category = data.value;
                    categoryName = data.elem[data.elem.selectedIndex].text;
                    $('.areacode').val(category);
                    $('.areaname').val(categoryName);
                });

                // 银行卡开户地区分界线-----------------------
                form.on('select(filterProvincebank)', function(data){            
                    category = data.value;  
                    categoryName = data.elem[data.elem.selectedIndex].text; 
                    $('.provincecodebank').val(category);
                    $('.provincenamebank').val(categoryName);
                    $("#citybank").html('');
                    $.ajax({
                        url : "{{url('/api/basequery/city')}}",
                        data : {area_code:category},
                        type : 'get',
                        success : function(data) {
                            var optionStr = "";
                                for(var i=0;i<data.data.length;i++){
                                    optionStr += "<option value='" + data.data[i].area_code + "'>"
                                        + data.data[i].area_name + "</option>";
                                }    
                                $("#citybank").append('<option value="">请选择市</option>'+optionStr);
                                layui.form.render('select');
                        },
                        error : function(data) {
                            alert('查找板块报错');
                        }
                    });
                });

                form.on('select(filterCitybank)', function(data){            
                    category = data.value;  
                    categoryName = data.elem[data.elem.selectedIndex].text; 
                    $('.citycodebank').val(category);
                    $('.citynamebank').val(categoryName);
                    $("#areabank").html('');
                    $.ajax({
                        url : "{{url('/api/basequery/city')}}",
                        data : {area_code:category},
                        type : 'get',
                        success : function(data) {
                            var optionStr = "";
                                for(var i=0;i<data.data.length;i++){
                                    optionStr += "<option value='" + data.data[i].area_code + "'>"
                                        + data.data[i].area_name + "</option>";
                                }    
                                $("#areabank").append('<option value="">请选择县/区</option>'+optionStr);
                                layui.form.render('select');
                        },
                        error : function(data) {
                            alert('查找板块报错');
                        }
                    });
                });
                form.on('select(filterAreabank)', function(data){            
                    category = data.value;  
                    categoryName = data.elem[data.elem.selectedIndex].text; 
                    $('.areacodebank').val(category);
                    $('.areanamebank').val(categoryName);           
                });

                
                /**
                 * 上传银行卡正面
                 */
                var uploadInst = upload.render({
                    url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token+'&img_type='+4,  //提交到的地址 可以自定义其他参数
                    elem : '.test7',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
                    method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
                    type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
                    ext : 'jpg|png|gif',    //自定义支持的文件格式
                    unwrap : true, //是否不改变input的样式风格。默认false 
                    size : 5120,
                    before : function(input){
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                    },
                    done: function(res){
                        if(res.status == 1){
                            layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                            layui.jquery('#demo7').attr("src", res.data.img_url); 
                            if(res.data.store_bank_no!=''){
                                $('.item7').val(res.data.store_bank_no)
                            }
                            if(res.data.bank_name!=''){
                                $('.bankname').val(res.data.bank_name)
                            }

                            $.post("{{url('/api/basequery/bank')}}",{
                                token:token,
                                bankname:res.data.bank_name
                            },function(data){
                                if(data.status==1){
                                    var optionStr = "";
                                    for(var i=0;i<data.data.length;i++){
                                        optionStr += "<option value='" + data.data[i].bankname + "' "+"selected"+">" + data.data[i].bankname + "</option>";
                                    }
                                    $("#bank").append('<option value="">请选择银行</option>'+optionStr);
                                    layui.form.render('select');                        
                                }
                            },"json");

                            $.post("{{url('/api/basequery/sub_bank')}}",{
                                token:token,
                                bank_name:res.data.bank_name,
                                l:'500'
                            },function(data){
                                if(data.status==1){
                                    var optionStr = "";
                                    for(var i=0;i<data.data.length;i++){
                                        optionStr += "<option value='" + data.data[i].bank_no + "'>"+ data.data[i].sub_bank_name + "</option>";
                                    }
                                    $("#subbank").html('');
                                    $("#subbank").append('<option value="">选择所属支行</option>'+optionStr);
                                    layui.form.render('select');                       
                                }
                            },"json");
                        }else{
                            layer.msg(res.message, {icon:2, shade:0.5, time:res.time});
                        }
                    }
                });

                /**
                 * 上传银行卡反面
                 */
                var uploadInst = upload.render({
                    url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token,  //提交到的地址 可以自定义其他参数
                    elem : '.test8',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
                    method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
                    type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
                    ext : 'jpg|png|gif',    //自定义支持的文件格式
                    unwrap : true, //是否不改变input的样式风格。默认false 
                    size : 5120,
                    before : function(input){
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                    },
                    done: function(res){
                        if(res.status == 0){
                            layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                        }else{
                            layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                            layui.jquery('#demo8').attr("src", res.data.img_url);
                        }
                    }
                });

                /**
                 * 持卡人身份证正面
                 */
                var uploadInst = upload.render({
                    url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token+"&img_type="+"2"+"&attach_name="+"bank_sfz_no"+"&type="+"img",  //提交到的地址 可以自定义其他参数
                    elem : '.test14',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
                    method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
                    type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
                    ext : 'jpg|png|gif',    //自定义支持的文件格式
                    unwrap : true, //是否不改变input的样式风格。默认false 
                    size : 5120,
                    before : function(input){
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                    },
                    done: function(res){
                        if(res.status == 0){
                            layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                        }else{
                            layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                            layui.jquery('#demo14').attr("src", res.data.img_url);
                            if(res.data.sfz_no != ''){
                                $('.item23').val(res.data.sfz_no)
                            }
                        }
                    }
                });
                
                /**
                 * 持卡人身份证反面
                 */
                var uploadInst = upload.render({
                    url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token+'&img_type='+ 3,  //提交到的地址 可以自定义其他参数
                    elem : '.test15',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
                    method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
                    type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
                    ext : 'jpg|png|gif',    //自定义支持的文件格式
                    unwrap : true, //是否不改变input的样式风格。默认false 
                    size : 5120,
                    before : function(input){
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                    },
                    done: function(res){
                        if(res.status == 0){
                            layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                        }else{
                            layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                            layui.jquery('#demo15').attr("src", res.data.img_url);
                            layui.jquery('.item20').val(res.data.sfz_stime);
                            layui.jquery('.item21').val(res.data.sfz_time);
                        }
                    }
                });

                /**
                 * 授权结算书
                 */
                var uploadInst = upload.render({
                    url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token,  //提交到的地址 可以自定义其他参数
                    elem : '.test16',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
                    method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
                    type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
                    ext : 'jpg|png|gif',    //自定义支持的文件格式
                    unwrap : true, //是否不改变input的样式风格。默认false 
                    size : 5120,
                    before : function(input){
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                    },
                    done: function(res){
                        if(res.status == 0){
                            layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                        }else{
                            layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                            layui.jquery('#demo16').attr("src", res.data.img_url);
                        }
                    }
                });
                
                /**
                 * 授权结算书
                 */
                var uploadInst = upload.render({
                    url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token,  //提交到的地址 可以自定义其他参数
                    elem : '.test17',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
                    method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
                    type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
                    ext : 'jpg|png|gif',    //自定义支持的文件格式
                    unwrap : true, //是否不改变input的样式风格。默认false 
                    size : 5120,
                    before : function(input){
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                    },
                    done: function(res){
                        if(res.status == 0){
                            layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                        }else{
                            layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                            layui.jquery('#demo17').attr("src", res.data.img_url);
                        }
                    }
                });

                //第四部分逻辑处理-----------------------------------------------------------------

                /**
                 * 营业执照
                 */
                var uploadInst = upload.render({
                    url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token+"&img_type="+"1"+"&attach_name="+"store_license_img"+"&type="+"img",  //提交到的地址 可以自定义其他参数
                    elem : '.test9',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
                    method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
                    type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
                    ext : 'jpg|png|gif',    //自定义支持的文件格式
                    unwrap : true, //是否不改变input的样式风格。默认false 
                    size : 5120,
                    before : function(input){
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                    },
                    done: function(res){
                        if(res.status == 0){
                            layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                        }else{
                            layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                            layui.jquery('#demo9').attr("src", res.data.img_url);
                            if(res.data.store_license_no !=''){
                                layui.jquery('.item14').val(res.data.store_license_no);
                            }
                            if(res.data.store_license_stime !=''){
                                layui.jquery('.item22').val(res.data.store_license_stime);
                                layui.jquery('.item15').val(res.data.store_license_time);
                            }
                        }
                    }
                });

                /**
                 * 开户许可证
                 */
                var uploadInst = upload.render({
                    url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token,  //提交到的地址 可以自定义其他参数
                    elem : '.test10',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
                    method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
                    type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
                    ext : 'jpg|png|gif',    //自定义支持的文件格式
                    unwrap : true, //是否不改变input的样式风格。默认false 
                    size : 5120,
                    before : function(input){
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                    },
                    done: function(res){
                        if(res.status == 0){
                            layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                        }else{
                            layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                            layui.jquery('#demo10').attr("src", res.data.img_url);
                        }
                    }
                });

                /**
                 * 手持身份证照正面
                 */
                var uploadInst = upload.render({
                    url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token,  //提交到的地址 可以自定义其他参数
                    elem : '.test11',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
                    method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
                    type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
                    ext : 'jpg|png|gif',    //自定义支持的文件格式
                    unwrap : true, //是否不改变input的样式风格。默认false 
                    size : 5120,
                    before : function(input){
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                    },
                    done: function(res){
                        if(res.status == 0){
                            layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                        }else{
                            layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                            layui.jquery('#demo11').attr("src", res.data.img_url);
                        }
                    }
                });

                /**
                 * 人站在门口照
                 */
                var uploadInst = upload.render({
                    url : "{{url('/api/basequery/webupload?act=images')}}"+'&token='+token,  //提交到的地址 可以自定义其他参数
                    elem : '.test12',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
                    method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
                    type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
                    ext : 'jpg|png|gif',    //自定义支持的文件格式
                    unwrap : true, //是否不改变input的样式风格。默认false 
                    size : 5120,
                    before : function(input){
                        //执行上传前的回调  可以判断文件后缀等等
                        layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                    },
                    done: function(res){
                        if(res.status == 0){
                            layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                        }else{
                            layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                            layui.jquery('#demo12').attr("src", res.data.img_url);
                        }
                    }
                });

                // // 卡类型
                form.on('select(cardtype)', function(data){            
                    category = data.value;  
                    categoryName = data.elem[data.elem.selectedIndex].text; 
                    $('.cardtype_id').val(category);
                    if(category=='01'){
                        //对私
                        if($('.store_type').val() == 1){
                            if($(".item1").val() == $('.item8').val()){
                                $('.different').hide()
                            }else{
                                $('.different').show()
                            }
                            $('.bank').show()
                        }else if($('.store_type').val() == 2){
                            if($(".item1").val() == $('.item8').val()){
                                $('.different').hide()
                            }else{
                                $('.different').show()
                            }
                            $('.bank').show()
                        }
                        $('.bank').show()
                    }else{
                        //对公
                        $('.bank').hide();
                        $('.different').hide()
                    }
                });

                // 选择银行卡
                form.on('select(bank)', function(data){            
                    category = data.value;  
                    categoryName = data.elem[data.elem.selectedIndex].text; 
                    $('.bankname').val(category);   
                    $.post("{{url('/api/basequery/sub_bank')}}",{
                        token:token,
                        bank_name:$('.bankname').val(),
                        bank_province_name:$('.provincenamebank').val(),
                        bank_city_name:$('.citynamebank').val(),
                        bank_area_name:$('.areanamebank').val(),
                        l:'500'
                    },function(data){
                        if(data.status==1){
                            var optionStr = "";
                            for(var i=0;i<data.data.length;i++){
                                optionStr += "<option value='" + data.data[i].bank_no + "'>"+ data.data[i].sub_bank_name + "</option>";
                            }    
                            $("#subbank").html('');
                            $("#subbank").append('<option value="">选择所属支行</option>'+optionStr);
                            layui.form.render('select');                       
                        }
                    },"json");       
                });

                // 选择支行
                form.on('select(subbank)', function(data){            
                    category = data.value;  
                    categoryName = data.elem[data.elem.selectedIndex].text; 
                    $('.item12').val(category);
                    $('.sub_bank_name').val(categoryName);
                    $('.bank_no').val(category);
                    $('.branchbank').val('');
                });
                
                // 地区end------------------------------------------------------  

                // 输入名称是否一致******************************** 
                $(".item8").bind('input propertychange', function () {
                    if($(".item1").val() == $(this).val()){
                        if($('.store_type').val()==1 && $('.cardtype_id').val()=='01'){
                            $('.different').hide()
                        }else if($('.store_type').val()==1 && $('.cardtype_id').val()=='02'){
                            $('.different').hide()
                        }
                        if($('.store_type').val()==2 && $('.cardtype_id').val()=='01'){
                            $('.different').hide()
                        }else if($('.store_type').val()==2 && $('.cardtype_id').val()=='02'){
                            $('.different').hide()
                        }
                        
                    }else{
                        if($('.store_type').val()==1 && $('.cardtype_id').val()=='01'){
                            $('.different').show()
                        }else if($('.store_type').val()==1 && $('.cardtype_id').val()=='02'){
                            $('.different').hide()
                        }
                        if($('.store_type').val()==2 && $('.cardtype_id').val()=='01'){
                            $('.different').show()
                        }else if($('.store_type').val()==2 && $('.cardtype_id').val()=='02'){
                            $('.different').hide()
                        }
                    }
                });
                // 用户信息star------------------------------------------------------ 

                // if(store_id_add==0){
                //     $('.js_phone').show();
                //     nostore()
                // }else{
                //     $('.js_phone').show();
                //     getBoards();
                // }

                

                function getBoards(){
                    // $.post("{{url('/api/user/store')}}",
                    // {
                    //     token:token,
                    //     store_id:store_id
                    // },function(res){
                    //     if(res.status==1){
                    //         // 一部分+++++++++++++++++++++++++++++++++++
                    //         $('.item1').val(res.data.head_info.head_name);
                    //         $('.item2').val(res.data.head_info.head_sfz_no);
                    //         $('.item16').val(res.data.head_info.head_sfz_time);
                    //         $('.item18').val(res.data.head_info.head_sfz_stime);
                    //         // 二部分+++++++++++++++++++++++++++++++
                    //         $('.item3').val(res.data.store_info.store_name);
                    //         $('.item17').val(res.data.store_info.store_short_name);
                    //         $('.store_people').val(res.data.store_info.people);
                    //         $('.phone').val(res.data.store_info.people_phone);
                    //         $('.email').val(res.data.store_info.store_email);
                    //         $('.wechatname').val(res.data.account_info.weixin_name);
                    //         $('.wechatno').val(res.data.account_info.weixin_no);
                    //         $('.alipayname').val(res.data.account_info.alipay_account);
                    //         $('.alipayno').val(res.data.account_info.alipay_name);
                    //         // 地区渲染
                        
                    //         $('.provincecode').val(res.data.store_info.province_code);
                    //         $('.citycode').val(res.data.store_info.city_code);
                    //         $('.areacode').val(res.data.store_info.area_code);
                            
                            // $('.item4').val(res.data.store_info.store_address);

                            // 所属银行                
                            $.post("{{url('/api/basequery/bank')}}",
                            {
                                token:token
                            },function(data){
                                if(data.status==1){
                                    var optionStr = "";
                                    for(var i=0;i<data.data.length;i++){

                                        optionStr += "<option value='" + data.data[i].bankname + "' "+"selected"+">" + data.data[i].bankname + "</option>";
                                    }    
                                    $("#bank").append('<option value="">请选择银行</option>'+optionStr);
                                    layui.form.render('select');                        
                                }

                            },"json");
                        
                            // if(res.data.account_info.bank_name==''){

                            // }else{

                                // $.post("{{url('/api/basequery/sub_bank')}}",
                                // {
                                //     token:token,
                                //     bank_name:res.data.account_info.bank_name,
                                //     bank_province_name:res.data.account_info.bank_province_name,
                                //     bank_city_name:res.data.account_info.bank_city_name,
                                //     bank_area_name:res.data.account_info.bank_area_name,

                                // },function(data){
                                //     if(data.status==1){
                                //         var optionStr = "";
                                //         for(var i=0;i<data.data.length;i++){

                                //             optionStr += "<option value='" + data.data[i].bank_no + "' "+"selected"+">"+ data.data[i].sub_bank_name + "</option>";
                                //         }    
                                //         $("#subbank").append('<option value="">选择所属支行</option>'+optionStr);
                                //         layui.form.render('select');                       
                                //     }

                                // },"json");
                            // }
                            
                        
                            //图片第一部分
                            // if(res.data.head_info.head_sfz_img_a!=''){
                            //     $('#demo1').attr('src',res.data.head_info.head_sfz_img_a);
                            // }
                            // if(res.data.head_info.head_sfz_img_b!=''){
                            //     $('#demo2').attr('src',res.data.head_info.head_sfz_img_b);
                            // }               
                            
                            // //***第二部分图片
                            // if(res.data.store_info.store_logo_img!=''){
                            //     $('#demo3').attr('src',res.data.store_info.store_logo_img);
                            // }
                            // if(res.data.store_info.store_img_a!=''){
                            //     $('#demo4').attr('src',res.data.store_info.store_img_a);
                            // }
                            // if(res.data.store_info.store_img_b!=''){
                            //     $('#demo5').attr('src',res.data.store_info.store_img_b);
                            // }
                            // if(res.data.store_info.store_img_c!=''){
                            //     $('#demo6').attr('src',res.data.store_info.store_img_c);
                            // }                
                            // //**第三部分图片
                            // if(res.data.account_info.bank_img_a!=''){
                            //     $('#demo7').attr('src',res.data.account_info.bank_img_a);
                            // }                
                            // if(res.data.account_info.bank_img_b!=''){
                            //     $('#demo8').attr('src',res.data.account_info.bank_img_b);
                            // }
                            // if(res.data.account_info.bank_sfz_img_a!=''){
                            //     $('#demo14').attr('src',res.data.account_info.bank_sfz_img_a);
                            // }
                            // if(res.data.account_info.bank_sfz_img_b!=''){
                            //     $('#demo15').attr('src',res.data.account_info.bank_sfz_img_b);
                            // }
                            // if(res.data.account_info.store_auth_bank_img!=''){
                            //     $('#demo16').attr('src',res.data.account_info.store_auth_bank_img);
                            // }
                            // if(res.data.account_info.bank_sc_img!=''){
                            //     $('#demo17').attr('src',res.data.account_info.bank_sc_img);
                            // }

                            
                            // if(res.data.store_info.store_type == 3 || res.data.store_info.store_type == 1 && res.data.account_info.store_bank_type=='01' || res.data.store_info.store_type == 2 && res.data.account_info.store_bank_type=='01'){
                            //     $('.bank').show()
                            //     if(res.data.account_info.store_bank_name == res.data.head_info.head_name){
                            //         $('.different').hide()
                            //     }else{
                            //         $('.different').show()
                            //     }
                            // }else{
                            //     $('.bank').hide()
                            //     $('.different').hide()
                            // }

                            // if(res.data.store_info.store_type == 3){
                            //     $('.cardtype').hide()
                            // }

                            

                            //**第四部分图片
                            // if(res.data.license_info.store_license_img!=''){
                            //     $('#demo9').attr('src',res.data.license_info.store_license_img);
                            // }
                            // if(res.data.license_info.store_industrylicense_img!=''){
                            //     $('#demo10').attr('src',res.data.license_info.store_industrylicense_img);
                            // }
                            // if(res.data.license_info.head_sc_img!=''){
                            //     $('#demo11').attr('src',res.data.license_info.head_sc_img);
                            // }
                            // if(res.data.license_info.head_store_img!=''){
                            //     $('#demo12').attr('src',res.data.license_info.head_store_img);
                            // }
                            // if(res.data.store_info.store_type == 3){
                            //     $('.per').show()
                            //     $('.public').hide()
                                
                            // }else{
                            //     $('.public').show()
                            //     $('.per').hide()
                            // }
                            // if(res.data.license_info.store_other_img_c!=''){
                            //     $('#demo13').attr('src',res.data.license_info.store_other_img_c);
                            // }

                        // }else{
                        //     layer.msg(res.message, {
                        //         offset: '50px'
                        //         ,icon: 2
                        //         ,time: 1000
                        //     });
                        // }
                    // },"json");
                    
                }
                
                

                // var active = {
                //     test: function(){
                //        layer.alert('你好，体验者');
                //     }
                // };

                // $('#LAY-component-layer-list .layadmin-layer-demo .layui-btn').on('click', function(){
                //   var type = $(this).data('type');
                //   active[type] && active[type].call(this);
                // });

                // 营业执照时间选择
                laydate.render({
                  elem: '.start-item'
                  ,done: function(value){
                            
                  }
                });
                laydate.render({
                  elem: '.end-item'
                  ,done: function(value){
                            
                  }
                });
                // -****法人身份证时间选择***
                laydate.render({
                  elem: '.frs-item' 
                  ,done: function(value){
                            
                  }
                });
                laydate.render({
                  elem: '.fre-item' 
                  ,done: function(value){
                            
                  }
                });
                // ****持卡人身份证时间选择****
                laydate.render({
                  elem: '.haves-item'
                  ,done: function(value){
                            
                  } 
                });
                laydate.render({
                  elem: '.havee-item' 
                  ,done: function(value){
                            
                  }
                });

                /**
                 * 最后的一步，点击保存信息按钮，进行提交保存
                 */
                form.on("submit(submit4)",function(data){
                    // var form_data = data.field;
                    console.log(form_data);
                    return false;
                    if($('.store_type').val() == 1 || $('.store_type').val() == 2){
                        if(form_data.long_time){
                            var request_data = {
                                token:token,
                                store_id:store_id,
                                //第四部分，营业执照
                                store_license_no:$('.item14').val(),
                                store_license_stime:$('.item22').val(),
                                store_license_time:form_data.long_time,
                                store_license_img:$('#demo9').attr('src'),
                                store_industrylicense_img:$('#demo10').attr('src'),
                                //第一部分，门店信息
                                store_name:$('.item3').val(),
                                store_short_name:$('.item17').val(),
                                province_code:$('.provincecode').val(),
                                city_code:$('.citycode').val(),
                                area_code :$('.areacode').val(),
                                store_address:$('.item4').val(),
                                store_type:$('.store_type').val(),
                                store_type_name:$('.store_name').val(),
                                category_id:$('.category_id').val(),
                                category_name:$('.category_name').val(),
                                store_logo_img:$('#demo3').attr('src'),
                                store_img_a:$('#demo4').attr('src'),
                                store_img_b:$('#demo5').attr('src'),
                                store_img_c:$('#demo6').attr('src'),
                                people_phone:$('.phone').val(),
                                store_email:$('.email').val(),
                                people:$('.store_people').val(),
                                //第二部分，法人信息
                                head_name:$('.item1').val(),
                                head_sfz_no:$('.item2').val(),
                                head_sfz_img_a:$('#demo1').attr('src'),
                                head_sfz_img_b:$('#demo2').attr('src'),
                                head_sfz_stime:$('.item18').val(),
                                head_sfz_time:$('.item16').val(),
                                //第三部分，账户信息
                                store_bank_no:$('.item7').val(),
                                store_bank_name:$('.item8').val(),
                                store_bank_phone:$('.item9').val(),
                                store_bank_type:$('.cardtype_id').val(),
                                bank_name:$('.bankname').val(),
                                bank_no:$('.item12').val(),
                                sub_bank_name:$('.sub_bank_name').val(),

                                bank_province_code:$('.provincecodebank').val(),
                                bank_city_code:$('.citycodebank').val(),
                                bank_area_code:$('.areacodebank').val(),
                                bank_img_a:$('#demo7').attr('src'),
                                bank_img_b:$('#demo8').attr('src'),
                                bank_sc_img:$('#demo17').attr('src'),

                                bank_sfz_img_a:$('#demo14').attr('src'),
                                bank_sfz_img_b:$('#demo15').attr('src'),
                                store_auth_bank_img:$('#demo16').attr('src'),

                                bank_sfz_stime:$('.item20').val(),
                                bank_sfz_time:$('.item21').val(),
                                bank_sfz_no:$('.item23').val(),
                            };
                        }else{
                            var request_data = {
                                token:token,
                                store_id:store_id,
                                //第四部分，营业执照
                                store_license_no:$('.item14').val(),
                                store_license_stime:$('.item22').val(),
                                store_license_time:$('.item15').val(),
                                store_license_img:$('#demo9').attr('src'),
                                store_industrylicense_img:$('#demo10').attr('src'),
                                //第一部分，门店信息
                                store_name:$('.item3').val(),
                                store_short_name:$('.item17').val(),
                                province_code:$('.provincecode').val(),
                                city_code:$('.citycode').val(),
                                area_code :$('.areacode').val(),
                                store_address:$('.item4').val(),
                                store_type:$('.store_type').val(),
                                store_type_name:$('.store_name').val(),
                                category_id:$('.category_id').val(),
                                category_name:$('.category_name').val(),
                                store_logo_img:$('#demo3').attr('src'),
                                store_img_a:$('#demo4').attr('src'),
                                store_img_b:$('#demo5').attr('src'),
                                store_img_c:$('#demo6').attr('src'),
                                people_phone:$('.phone').val(),
                                store_email:$('.email').val(),
                                people:$('.store_people').val(),
                                //第二部分，法人信息
                                head_name:$('.item1').val(),
                                head_sfz_no:$('.item2').val(),
                                head_sfz_img_a:$('#demo1').attr('src'),
                                head_sfz_img_b:$('#demo2').attr('src'),
                                head_sfz_stime:$('.item18').val(),
                                head_sfz_time:$('.item16').val(),
                                //第三部分，账户信息
                                store_bank_no:$('.item7').val(),
                                store_bank_name:$('.item8').val(),
                                store_bank_phone:$('.item9').val(),
                                store_bank_type:$('.cardtype_id').val(),
                                bank_name:$('.bankname').val(),
                                bank_no:$('.item12').val(),
                                sub_bank_name:$('.sub_bank_name').val(),

                                bank_province_code:$('.provincecodebank').val(),
                                bank_city_code:$('.citycodebank').val(),
                                bank_area_code:$('.areacodebank').val(),
                                bank_img_a:$('#demo7').attr('src'),
                                bank_img_b:$('#demo8').attr('src'),
                                bank_sc_img:$('#demo17').attr('src'),

                                bank_sfz_img_a:$('#demo14').attr('src'),
                                bank_sfz_img_b:$('#demo15').attr('src'),
                                store_auth_bank_img:$('#demo16').attr('src'),

                                bank_sfz_stime:$('.item20').val(),
                                bank_sfz_time:$('.item21').val(),
                                bank_sfz_no:$('.item23').val(),
                            };
                        }
                        $.post("{{url('/api/user/up_store')}}",request_data,function(res){
                            console.log(res)
                            if(res.status==1){
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 1
                                    ,time: 3000
                                });
                            }else{
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 2
                                    ,time: 3000
                                });
                            }
                        },"json");
                    }else{
                        if(form_data.long_time){
                            var request_data = {
                                token:token,
                                store_id:store_id,
                                store_license_no:$('.item14').val(),
                                store_license_stime:$('.item22').val(),
                                store_license_time:form_data.long_time,
                                head_sc_img:$('#demo11').attr('src'),
                                head_store_img:$('#demo12').attr('src'),
                                // store_other_img_c:$('#demo13').attr('src')
                                //第一部分，门店信息
                                store_name:$('.item3').val(),
                                store_short_name:$('.item17').val(),
                                province_code:$('.provincecode').val(),
                                city_code:$('.citycode').val(),
                                area_code :$('.areacode').val(),
                                store_address:$('.item4').val(),
                                store_type:$('.store_type').val(),
                                store_type_name:$('.store_name').val(),
                                category_id:$('.category_id').val(),
                                category_name:$('.category_name').val(),
                                store_logo_img:$('#demo3').attr('src'),
                                store_img_a:$('#demo4').attr('src'),
                                store_img_b:$('#demo5').attr('src'),
                                store_img_c:$('#demo6').attr('src'),
                                people_phone:$('.phone').val(),
                                store_email:$('.email').val(),
                                people:$('.store_people').val(),
                                //第二部分，法人信息
                                head_name:$('.item1').val(),
                                head_sfz_no:$('.item2').val(),
                                head_sfz_img_a:$('#demo1').attr('src'),
                                head_sfz_img_b:$('#demo2').attr('src'),
                                head_sfz_stime:$('.item18').val(),
                                head_sfz_time:$('.item16').val(),
                                //第三部分，账户信息
                                store_bank_no:$('.item7').val(),
                                store_bank_name:$('.item8').val(),
                                store_bank_phone:$('.item9').val(),
                                store_bank_type:$('.cardtype_id').val(),
                                bank_name:$('.bankname').val(),
                                bank_no:$('.item12').val(),
                                sub_bank_name:$('.sub_bank_name').val(),

                                bank_province_code:$('.provincecodebank').val(),
                                bank_city_code:$('.citycodebank').val(),
                                bank_area_code:$('.areacodebank').val(),
                                bank_img_a:$('#demo7').attr('src'),
                                bank_img_b:$('#demo8').attr('src'),
                                bank_sc_img:$('#demo17').attr('src'),

                                bank_sfz_img_a:$('#demo14').attr('src'),
                                bank_sfz_img_b:$('#demo15').attr('src'),
                                store_auth_bank_img:$('#demo16').attr('src'),

                                bank_sfz_stime:$('.item20').val(),
                                bank_sfz_time:$('.item21').val(),
                                bank_sfz_no:$('.item23').val(),
                            };
                        }else{
                            var request_data = {
                                token:token,
                                store_id:store_id,
                                store_license_no:$('.item14').val(),
                                store_license_stime:$('.item22').val(),
                                store_license_time:$('.item15').val(),
                                head_sc_img:$('#demo11').attr('src'),
                                head_store_img:$('#demo12').attr('src'),
                                // store_other_img_c:$('#demo13').attr('src')
                                //第一部分，门店信息
                                store_name:$('.item3').val(),
                                store_short_name:$('.item17').val(),
                                province_code:$('.provincecode').val(),
                                city_code:$('.citycode').val(),
                                area_code :$('.areacode').val(),
                                store_address:$('.item4').val(),
                                store_type:$('.store_type').val(),
                                store_type_name:$('.store_name').val(),/*-------*/
                                category_id:$('.category_id').val(),
                                category_name:$('.category_name').val(),
                                store_logo_img:$('#demo3').attr('src'),
                                store_img_a:$('#demo4').attr('src'),
                                store_img_b:$('#demo5').attr('src'),
                                store_img_c:$('#demo6').attr('src'),
                                people_phone:$('.phone').val(),
                                store_email:$('.email').val(),
                                people:$('.store_people').val(),
                                //第二部分，法人信息
                                head_name:$('.item1').val(),
                                head_sfz_no:$('.item2').val(),
                                head_sfz_img_a:$('#demo1').attr('src'),
                                head_sfz_img_b:$('#demo2').attr('src'),
                                head_sfz_stime:$('.item18').val(),
                                head_sfz_time:$('.item16').val(),
                                //第三部分，账户信息
                                store_bank_no:$('.item7').val(),
                                store_bank_name:$('.item8').val(),
                                store_bank_phone:$('.item9').val(),
                                store_bank_type:$('.cardtype_id').val(),
                                bank_name:$('.bankname').val(),
                                bank_no:$('.item12').val(),
                                sub_bank_name:$('.sub_bank_name').val(),

                                bank_province_code:$('.provincecodebank').val(),
                                bank_city_code:$('.citycodebank').val(),
                                bank_area_code:$('.areacodebank').val(),
                                bank_img_a:$('#demo7').attr('src'),
                                bank_img_b:$('#demo8').attr('src'),
                                bank_sc_img:$('#demo17').attr('src'),

                                bank_sfz_img_a:$('#demo14').attr('src'),
                                bank_sfz_img_b:$('#demo15').attr('src'),
                                store_auth_bank_img:$('#demo16').attr('src'),

                                bank_sfz_stime:$('.item20').val(),
                                bank_sfz_time:$('.item21').val(),
                                bank_sfz_no:$('.item23').val(),
                            };
                        }
                        //发送请求
                        $.post("{{url('/api/user/up_store')}}",request_data,function(res){
                            if(res.status==1){
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 1
                                    ,time: 3000
                                });
                            }else{
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 2
                                    ,time: 3000
                                });
                            }
                        },"json");
                    }
                });






                // if(store_id_add==0){
                
                //     // 提交法人信息1111111111
                //     $('.submit1').click(function(){
                //         $.post("{{url('/api/user/up_store')}}",
                //         {
                //             token:token,
                //             store_id:store_id,
                //             head_name:$('.item1').val(),
                //             head_sfz_no:$('.item2').val(),
                //             head_sfz_img_a:$('#demo1').attr('src'),
                //             head_sfz_img_b:$('#demo2').attr('src'),
                //             head_sfz_stime:$('.item18').val(),
                //             head_sfz_time:$('.item16').val()
                            
                //         },function(res){
                //             if(res.status==1){
                //                 layer.msg(res.message, {
                //                     offset: '50px'
                //                     ,icon: 1
                //                     ,time: 3000
                //                 });
                //             }else{
                //                 layer.msg(res.message, {
                //                     offset: '50px'
                //                     ,icon: 2
                //                     ,time: 3000
                //                 });
                //             }
                //         },"json");
                //     });
                //     // 提交门店信息22222222
                //     $('.submit2').click(function(){
                //         $.post("{{url('/api/user/up_store')}}",
                //         {
                //             token:token,
                //             store_id:store_id,
                //             store_name:$('.item3').val(),
                //             store_short_name:$('.item17').val(),
                //             province_code:$('.provincecode').val(),
                //             city_code:$('.citycode').val(),
                //             area_code :$('.areacode').val(),
                //             store_address:$('.item4').val(),
                //             store_type:$('.store_type').val(),
                //             store_type_name:$('.store_name').val(),/*-------*/
                //             category_id:$('.category_id').val(),
                //             category_name:$('.category_name').val(),
                //             store_logo_img:$('#demo3').attr('src'),
                //             store_img_a:$('#demo4').attr('src'),
                //             store_img_b:$('#demo5').attr('src'),
                //             store_img_c:$('#demo6').attr('src'),
                //             people_phone:$('.phone').val(),
                //             store_email:$('.email').val(),
                //             people:$('.store_people').val(),
                //             weixin_name:$('.wechatname').val(),
                //             weixin_no:$('.wechatno').val(),
                //             alipay_account:$('.alipayname').val(),
                //             alipay_name:$('.alipayno').val(),

                //         },function(res){
                //             if(res.status==1){
                //                 layer.msg(res.message, {
                //                     offset: '50px'
                //                     ,icon: 1
                //                     ,time: 3000
                //                 });
                //             }else{
                //                 layer.msg(res.message, {
                //                     offset: '50px'
                //                     ,icon: 2
                //                     ,time: 3000
                //                 });
                //             }
                //         },"json");
                //     });
                    // 提交账户信息33333333
                //     $('.submit3').click(function(){
                //         $.post("{{url('/api/user/up_store')}}",
                //         {
                //             token:token,
                //             store_id:store_id,

                //             store_bank_no:$('.item7').val(),
                //             store_bank_name:$('.item8').val(),
                //             store_bank_phone:$('.item9').val(),
                //             store_bank_type:$('.cardtype_id').val(),
                //             bank_name:$('.bankname').val(),
                //             bank_no:$('.item12').val(),
                //             sub_bank_name:$('.sub_bank_name').val(),

                //             bank_province_code:$('.provincecodebank').val(),
                //             bank_city_code:$('.citycodebank').val(),
                //             bank_area_code:$('.areacodebank').val(),
                //             bank_img_a:$('#demo7').attr('src'),
                //             bank_img_b:$('#demo8').attr('src'),
                //             bank_sc_img:$('#demo17').attr('src'),

                //             bank_sfz_img_a:$('#demo14').attr('src'),
                //             bank_sfz_img_b:$('#demo15').attr('src'),
                //             store_auth_bank_img:$('#demo16').attr('src'),

                //             bank_sfz_stime:$('.item20').val(),
                //             bank_sfz_time:$('.item21').val(),
                //             bank_sfz_no:$('.item23').val(),

                //         },function(res){
                //             if(res.status==1){
                //                 layer.msg(res.message, {
                //                     offset: '50px'
                //                     ,icon: 1
                //                     ,time: 3000
                //                 });
                //             }else{
                //                 layer.msg(res.message, {
                //                     offset: '50px'
                //                     ,icon: 2
                //                     ,time: 3000
                //                 });
                //             }
                //         },"json");
                //     });
                //     // 证件照信息44444444
                //     form.on("submit(submit4)",function(data){
                //         var form_data = data.field;
                        
                //         if($('.store_type').val() == 1 || $('.store_type').val() == 2){
                //             if(form_data.long_time){
                //                 var request_data = {
                //                     token:token,
                //                     store_id:store_id,
                //                     store_license_no:$('.item14').val(),
                //                     store_license_stime:$('.item22').val(),
                //                     store_license_time:form_data.long_time,
                //                     store_license_img:$('#demo9').attr('src'),
                //                     store_industrylicense_img:$('#demo10').attr('src')
                //                 };
                //             }else{
                //                 var request_data = {
                //                     token:token,
                //                     store_id:store_id,
                //                     store_license_no:$('.item14').val(),
                //                     store_license_stime:$('.item22').val(),
                //                     store_license_time:$('.item15').val(),
                //                     store_license_img:$('#demo9').attr('src'),
                //                     store_industrylicense_img:$('#demo10').attr('src')
                //                 };
                //             }
                //             $.post("{{url('/api/user/up_store')}}",request_data,function(res){
                //                 if(res.status==1){
                //                     layer.msg(res.message, {
                //                         offset: '50px'
                //                         ,icon: 1
                //                         ,time: 3000
                //                     });
                //                 }else{
                //                     layer.msg(res.message, {
                //                         offset: '50px'
                //                         ,icon: 2
                //                         ,time: 3000
                //                     });
                //                 }
                //             },"json");
                //         }else{
                //             if(form_data.long_time){
                //                 var request_data = {
                //                     token:token,
                //                     store_id:store_id,
                //                     store_license_no:$('.item14').val(),
                //                     store_license_stime:$('.item22').val(),
                //                     store_license_time:form_data.long_time,
                //                     head_sc_img:$('#demo11').attr('src'),
                //                     head_store_img:$('#demo12').attr('src')
                //                     // store_other_img_c:$('#demo13').attr('src')
                //                 };
                //             }else{
                //                 var request_data = {
                //                     token:token,
                //                     store_id:store_id,
                //                     store_license_no:$('.item14').val(),
                //                     store_license_stime:$('.item22').val(),
                //                     store_license_time:$('.item15').val(),
                //                     head_sc_img:$('#demo11').attr('src'),
                //                     head_store_img:$('#demo12').attr('src')
                //                     // store_other_img_c:$('#demo13').attr('src')
                //                 };
                //             }
                //             //开发发送请求
                //             $.post("{{url('/api/user/up_store')}}",request_data,function(res){
                //                 if(res.status==1){
                //                     layer.msg(res.message, {
                //                         offset: '50px'
                //                         ,icon: 1
                //                         ,time: 3000
                //                     });
                //                 }else{
                //                     layer.msg(res.message, {
                //                         offset: '50px'
                //                         ,icon: 2
                //                         ,time: 3000
                //                     });
                //                 }
                //             },"json");
                //         }
                //     });
                // }else{
                //     // 提交法人信息1111111111
                //     $('.submit1').click(function(){
                //         $.post("{{url('/api/user/up_store')}}",
                //         {
                //             token:token,
                //             store_id:store_id,
                //             head_name:$('.item1').val(),
                //             head_sfz_no:$('.item2').val(),
                //             head_sfz_img_a:$('#demo1').attr('src'),
                //             head_sfz_img_b:$('#demo2').attr('src'),
                //             head_sfz_stime:$('.item18').val(),
                //             head_sfz_time:$('.item16').val()
                            
                //         },function(res){
                //             if(res.status==1){
                //                 layer.msg(res.message, {
                //                     offset: '50px'
                //                     ,icon: 1
                //                     ,time: 3000
                //                 });
                //             }else{
                //                 layer.msg(res.message, {
                //                     offset: '50px'
                //                     ,icon: 2
                //                     ,time: 3000
                //                 });
                //             }
                //         },"json");
                //     });
                //     // 提交门店信息22222222
                //     $('.submit2').click(function(){
                //         $.post("{{url('/api/user/up_store')}}",
                //         {
                //             token:token,
                //             store_id:store_id,

                //             store_name:$('.item3').val(),
                //             store_short_name:$('.item17').val(),
                //             province_code:$('.provincecode').val(),
                //             city_code:$('.citycode').val(),
                //             area_code :$('.areacode').val(),
                //             store_address:$('.item4').val(),
                //             store_type:$('.store_type').val(),
                //             store_type_name:$('.store_name').val(),/*-------*/
                //             category_id:$('.category_id').val(),
                //             category_name:$('.category_name').val(),
                //             store_logo_img:$('#demo3').attr('src'),
                //             store_img_a:$('#demo4').attr('src'),
                //             store_img_b:$('#demo5').attr('src'),
                //             store_img_c:$('#demo6').attr('src'),
                //             people_phone:$('.phone').val(),
                //             store_email:$('.email').val(),
                //             people:$('.store_people').val(),
                //             weixin_name:$('.wechatname').val(),
                //             weixin_no:$('.wechatno').val(),
                //             alipay_account:$('.alipayname').val(),
                //             alipay_name:$('.alipayno').val(),

                //         },function(res){
                //             if(res.status==1){
                //                 layer.msg(res.message, {
                //                     offset: '50px'
                //                     ,icon: 1
                //                     ,time: 3000
                //                 });
                //             }else{
                //                 layer.msg(res.message, {
                //                     offset: '50px'
                //                     ,icon: 2
                //                     ,time: 3000
                //                 });
                //             }
                //         },"json");
                //     });
                //     // 提交账户信息33333333
                //     $('.submit3').click(function(){
                //         $.post("{{url('/api/user/up_store')}}",
                //         {
                //             token:token,
                //             store_id:store_id,

                //             store_bank_no:$('.item7').val(),
                //             store_bank_name:$('.item8').val(),
                //             store_bank_phone:$('.item9').val(),
                //             store_bank_type:$('.cardtype_id').val(),
                //             bank_name:$('.bankname').val(),
                //             bank_no:$('.item12').val(),
                //             sub_bank_name:$('.sub_bank_name').val(),

                //             bank_province_code:$('.provincecodebank').val(),
                //             bank_city_code:$('.citycodebank').val(),
                //             bank_area_code:$('.areacodebank').val(),
                //             bank_img_a:$('#demo7').attr('src'),
                //             bank_img_b:$('#demo8').attr('src'),
                //             bank_sc_img:$('#demo17').attr('src'),

                //             bank_sfz_img_a:$('#demo14').attr('src'),
                //             bank_sfz_img_b:$('#demo15').attr('src'),
                //             store_auth_bank_img:$('#demo16').attr('src'),

                //             bank_sfz_stime:$('.item20').val(),
                //             bank_sfz_time:$('.item21').val(),
                //             bank_sfz_no:$('.item23').val()

                //         },function(res){
                //             if(res.status==1){
                //                 layer.msg(res.message, {
                //                     offset: '50px'
                //                     ,icon: 1
                //                     ,time: 3000
                //                 });
                //             }else{
                //                 layer.msg(res.message, {
                //                     offset: '50px'
                //                     ,icon: 2
                //                     ,time: 3000
                //                 });
                //             }
                //         },"json");
                //     });
                //     // 证件照信息4444444
                
                // }
            });
    </script>
</html>
