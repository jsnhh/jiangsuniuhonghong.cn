<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>营业执照</title>
    <style>
        .photo {
            background-color: #fff;
            width: 100%;
        }

        .tip {
            background-color: #FFFADA;
            height: 40px;
            line-height: 40px;
            font-size: 14px;
            color: #666666;
            text-align: center;
        }

        .photoImg {
            width: 160px;
            height: 160px;
            padding: 50px 0 0 30px;
        }

        .desc {
            text-align: right;
            font-size: 16px;
            color: #999999;
            padding-top: 30px;
        }

        .btn {
            width: 300px;
            height: 40px;
            line-height: 40px;
            margin: 100px auto;
            text-align: center;
            color: #fff;
            background-color: #29A1F7;
            border-radius: 12px;
        }


        .permitNum {
            background-color: #fff;
            margin-top: 30px;
            font-size: 17px;
        }

        .permitNumList {
            border-bottom: 1px solid #ddd;
            height: 40px;
            line-height: 40px;
        }

        .permitNumList label {
            width: 200px;
        }

        .permitNumList input {
            width: 240px;
            line-height: 38px;
            float: right;
            padding-right: 30px;
            text-align: right;
            border: none;
        }

        .choice {
            width: 240px;
            float: right;
            text-align: right;
        }

        .choice img {
            width: 17px;
            height: 17px;
            padding-left: 20px;
        }

        .picker {
            display: inline-block;
        }

    </style>
</head>

<body>
    <div class='photo'>
        <div class="tip">请上传在有效期内的合格营业执照、需原件图片</div>
        <div>
            <img class='photoImg'></img>
            <div class='desc'>需上传1张图片</div>
        </div>
    </div>
    <div class='permitNum'>
        <div class='permitNumList'>
            <label>开始时间</label>
            <picker class="choice" mode="date" start="1900-01-01" end="2080-01-01">
                <div class="picker">

                </div>
                <img src="{{url('/payviews/img/zhishijiantou-zuo@2x.png')}}"></img>
            </picker>
        </div>
        <div class='permitNumList'>
            <label>结束时间</label>
            <picker class="choice" mode="date" start="1900-01-01" end="2080-01-01">
                <div class="picker">

                </div>
                <img src="{{url('/payviews/img/zhishijiantou-zuo@2x.png')}}"></img>
            </picker>
        </div>
        <div class='permitNumList'>
            <label>注册号</label>
            <input type='text' placeholder='请与营业执照上保持一致'></input>
        </div>
    </div>

    <div class='btn bgcolor'>下一步</div>
</body>

</html>