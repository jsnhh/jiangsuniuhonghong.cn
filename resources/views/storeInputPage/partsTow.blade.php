<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>商户进件</title>
    <style>
        .jinjbox {
            margin: 65px auto;
        }

        .wbox {
            width: 210px;
            padding: 13px;
            margin: 50px auto;
            border: 1px solid gray;
            font-size: 13px;
            border-radius: 5px;
            text-align: center;
        }

        .bottom {
            width: 100%;
            /* display: flex; */
            position: fixed;
            bottom: 0;
        }

        .left-botton {
            width: 50%;
            height: 40px;
            background: gray;
            color: #fff;
            line-height: 40px;
            text-align: center;
        }

        .right-botton {
            height: 40px;
            background: #108ee9;
            color: #fff;
            line-height: 40px;
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="wbox storeInfo">店铺信息</div>
    <div class="wbox people">法人认证</div>
    <div class="wbox information">结算信息</div>
    <div class="wbox licence">证照信息</div>
    <div class="bottom">
        <div data-type="4" class="right-botton rightbot">确认提交</div>
    </div>

</body>
<script type="text/javascript" src="{{asset('/phone/js/jquery-2.1.4.js')}}"></script>
<script type="text/javascript"> 
    $(function(){
        $(".storeInfo").on("click",function(){
           window.location.href="/authorize/shop";
        });
        $(".people").on("click",function(){
           window.location.href="/authorize/addidcardphoto";
        });
        $(".information").on("click",function(){
           window.location.href="/authorize/addbindcard";
        });
        $(".licence").on("click",function(){
           window.location.href="/authorize/addLicense";
        });
    });
</script>

</html>