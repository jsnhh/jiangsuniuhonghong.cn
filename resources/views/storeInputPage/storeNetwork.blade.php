<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>商户入网</title>
    <style>
        .shops { background-color: #108ee9;line-height: 40px;width: 100%;color: #fff;text-align: center;}

        .denglu {
            height: 80px;
            padding-top: 50px;
            background-color: #fff;
            margin: 0 auto;
        }

        .label-2__text {
            width: 300px;
            display: flex;
            margin-left: 35px;
        }

        .phones {
            width: 100px;
            height: 37px;
            line-height: 37px;
        }

        input {
            width: 130px;
        }

        .login {
            margin: 0 auto;
            border-radius: 6px;
            text-align: center;
            background-color: #108ee9;
            color: #fff;
            width: 310px;
            height: 40px;
            line-height: 40px;
            margin-top: 105px;
        }

        .listInput {
            background-color: #fff;
            border: 1px solid #eee;
            border-radius: 5px;
            height: 40px;
            width: 100%;
        }

        .tips {
            margin: 0 auto;
            width: 350px;
            height: 75px;
            margin-top: 325px;
        }

        .wxtips {
            font-size: 12px;
            color: rgb(114, 113, 113);
            text-align: left;
            margin-left: 5px;
        }

        .content {
            margin-left: 5px;
        }

        .content,
        .zhuce {
            font-size: 12px;
            color: rgb(114, 113, 113);
            text-align: left;
            margin-top: 5px;
        }

        .yuandian {
            width: 25px;
            height: 25px;
        }
        *{ margin: 0;padding: 0;list-style: none}
 
        .mask{background-color:rgba(0,0,0,0.2); width:100%; height:100%;position:fixed;top:0;left:0;}
        .prompt_box{width:400px;height:200px;background:#ffffff;border-radius:6px;
        position:fixed;top:25%; left:50%; margin-left:-200px;overflow: hidden;}
        .prompt_box .prompt_title{height:40px;line-height:40px;padding-left:20px;border-bottom:2px solid #1a9ebf;background:#e6e6e6;position: relative;}
        .prompt_box .prompt_title h3{font-size:16px;color: #333333; margin-top:0;}
        .prompt_box .prompt_cancel{width:24px;height:24px;background: url("img/cancel.png") no-repeat;position: absolute;right:0;top:0;}
        
        .prompt_box .prompt_cont{position: relative;height:158px;}
        .prompt_box .prompt_cont .prompt_text{line-height:140px;padding-left:100px;}
        .prompt_box .prompt_cont .prompt_sure{position: absolute;right:20px;bottom:34px;
        width:50px;height: 26px;background:#1a9ebf;border-radius:5px;color:#ffffff;
        font-size: 14px;line-height:26px;text-align: center;}
        
        .hide{display:none;}
        .show{display:block;}
    </style>
</head>
<body>
    <div class="shops">商户入网</div>
    <div class='denglu'>
        <div class='list'>
            <div class="label-2__text">
                <div class="phones">注册账号</div>
                <input class='listInput' type="text" name="phone" value="" placeholder="请输入手机号码" />
            </div>

        </div>
        <div class='login'>开始注册</div>
    </div>
    <div class="tips">
        <div class="wxtips">温馨提示</div>
        <div class="zhuce">
            <text>账号注册：请使用正确且没有注册过的手机号来进行注册；</text>
            <text>注册的手机号将作为商户账号来使用，初始密码 000000。</text>
        </div>
    </div>
</body>
<script type="text/javascript" src="{{asset('/phone/js/jquery-2.1.4.js')}}"></script>
<script type="text/javascript"> 
    $(function(){
        $(".login").on("click",function(){
            var phone      = $("input[name=phone]").val();
            window.sessionStorage.setItem("userPhone",phone);
            if(!phone){
                alert("手机号不正确！");
            }
           window.location.href="/authorize/storeInput";
        });
    });
</script>
</html>