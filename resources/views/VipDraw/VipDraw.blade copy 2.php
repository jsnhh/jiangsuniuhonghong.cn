<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>镇江地图</title>
</head>
<style type="text/css">

    body{
        background-color: rgba(0,0,0,0.4);
    }

    .map{
        width: 500px;
        height: 452px;
        margin: 0 auto;
        transform: translateY(50%);
    	position: relative;
    }

    .dtq{
        width: 226px;
        height: 260px;
        /* background: url(/mb/mapimg/dtq.png) no-repeat center; */
        position: absolute;
        top: 50px;
        left: 132px;
       background-size: 100% 100%;
    }

    .aninmention{
        animation:mymove 0.5s infinite;
        -webkit-animation:mymove 0.5s infinite; /*Safari and Chrome*/
    }

    @keyframes mymove
    {
        from {transform: scale(0.99)}
        to {transform: scale(1)}
    }

    @-webkit-keyframes mymove 
    {
        from {transform: scale(0.99)}
        to {transform: scale(1)}
    }

    .dys{
        width: 226px;
        height: 260px;
        /* background: url(/mb/mapimg/dys.png) no-repeat center; */
        position: absolute;
        top: 116px;
    	left: 214px;
    }
    
    .jkq{
        width: 226px;
        height: 260px;
        /* background: url(/mb/mapimg/jkq.png) no-repeat center; */
        position: absolute;
        top: -22px;
    	left: 207px;
    }
    
    .jrs{
        width: 226px;
        height: 328px;
        /* background: url(/mb/mapimg/jrs.png) no-repeat center; */
        position: absolute;
        left: 1px;
        top: 86px;
    }
    
    .rzq{
        width: 226px;
        height: 72px;
        /* background: url(/mb/mapimg/rzq.png) no-repeat center; */
        position: absolute;
        top:68px;
        left: 106px;
    }
    
    .yzs{
        width: 226px;
        height: 260px;
        /* background: url(/mb/mapimg/yzs.png) no-repeat center; */
        position: absolute;
        top: -6px;
    	left: 298px;
    }
    .dtq span{
        position: absolute;
        top: 114px;
        left: 83px;
        z-index: 99999;
        font-size: 12px;
        color: red;
    }
    
    .dys span{
        position: absolute;
        top: 122px;
        left: 94px;
        z-index: 99999;
        font-size: 12px;
        color: red;
    }

    .jkq > span{
        position: absolute;
        top: 122px;
        left: 50px;
        z-index: 99999;
        font-size: 12px;
        color: red;
    }

    .jrs span{
        position: absolute;
        top: 152px;
        left: 122px;
        z-index: 99999;
        font-size: 12px;
        color: red;
    }

    .rzq span{
        position: absolute;
        top: 24px;
        left: 102px;
        z-index: 99999;
        font-size: 12px;
        color: red;
    }
    .yzs span{
        position: absolute;
        top: 90px;
        left: 97px;
        z-index: 99999;
        font-size: 12px;
        color: red;
    }

    .jkq .xq{
        position: absolute;
        top: 146px;
        right: 68px;
        color: red;
        font-size: 12px;
        z-index: 99999;
    }

</style>
<body>
    <div class="map">
        <div class="dtq">
            <span></span>
        </div>
		<div class="dys">
            <span></span>
        </div>
		<div class="jkq">
            <span></span>
        </div>
        <div class="jrs">
            <span></span>
        </div>
        <div class="rzq">
            <span></span>
        </div>
        <div class="yzs">
            <span></span>
        </div>
    </div>
<script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script> 
<script type="text/javascript">
    
    $(function(){
		var token = window.sessionStorage.getItem("Usertoken");
        //获取数据
        const getdata = ()=> {
            return new Promise( resolve => $.ajax({
                type:"post",url:"/api/user/mapEditName",data:{token:token,type:2},success:data=>resolve(data),error:err=>console.log(err)
            }))
        }
        //每隔5秒就要获取数据更新
        setInterval(()=>getdata().then(data=>setdatato(data)),5000);
        //进来就会显示
        (async ()=>{const listdata = await getdata();setdatato(listdata)})()
        //将数据放到地图当中
        function setdatato(data){
            if( data.status == 1 ){
                let listData = JSON.parse(data.data[2].name)
                $(".dtq span").html(listData.dtq)
                if(listData.dtq != 0){
                    $(".dtq").css({'background':' url(/mb/mapimg/dtq.png) no-repeat center'})
                    $(".dtq").addClass("aninmention")
                }else{
                    $(".dtq").css({'background':' url(/mb/mapimg/dtq2.png) no-repeat center'})
                }
                $(".dys span").html(listData.dys)
                if(listData.dys != 0){
                    $(".dys").css({'background':' url(/mb/mapimg/dys.png) no-repeat center'})
                    $(".dys").addClass("aninmention")
                }else{
                    $(".dys").css({'background':' url(/mb/mapimg/dys2.png) no-repeat center'})
                }
                $(".jkq span").html(listData.jkq)
                if(listData.jkq != 0){
                    $(".jkq").css({'background':' url(/mb/mapimg/jkq.png) no-repeat center'})
                    $(".jkq").addClass("aninmention")
                }else{
                    $(".jkq").css({'background':' url(/mb/mapimg/jkq2.png) no-repeat center'})
                }
                $(".jrs span").html(listData.jrs)
                if(listData.jrs != 0){
                    $(".jrs").css({'background':' url(/mb/mapimg/jrs.png) no-repeat center'})
                    $(".jrs").addClass("aninmention")
                }else{
                    $(".jrs").css({'background':' url(/mb/mapimg/jrs2.png) no-repeat center'})
                }
                $(".rzq span").html(listData.rzq)
                if(listData.rzq != 0){
                    $(".rzq").css({'background':' url(/mb/mapimg/rzq.png) no-repeat center'})
                    $(".rzq").addClass("aninmention")
                }else{
                    $(".rzq").css({'background':' url(/mb/mapimg/rzq2.png) no-repeat center'})
                }
                $(".yzs span").html(listData.yzs)
                if(listData.yzs != 0){
                    $(".yzs").css({'background':' url(/mb/mapimg/yzs.png) no-repeat center'})
                    $(".yzs").addClass("aninmention")
                }else{
                    $(".yzs").css({'background':' url(/mb/mapimg/yzs2.png) no-repeat center'})
                }
            }else{
                console.log(data.message)
            }
        }

    })

</script>
</body>
</html>