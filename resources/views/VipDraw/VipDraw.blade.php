<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>南京地图</title>
</head>
<style type="text/css">
    *{
        margin: 0;
        padding: 0;
    }
    body{
        background-color: #060036;
    }

    .imgaddclass{
        -webkit-filter: grayscale(100%);
        -moz-filter: grayscale(100%);
        -ms-filter: grayscale(100%);
        -o-filter: grayscale(100%);
        filter: grayscale(100%);
        filter: gray;
        opacity:0.7;
    }


    .mapbox{

        width: 980px;
        height: 548px; 
        overflow: hidden;
        position: fixed;
        left: 50%;
        top: 50%;
        transform: translate(-50%,-50%);

    }
    .map{
        height: 770px;
        margin-top: -228px;
    	position: relative;
    }
    .map img{
        display: block;
        width: 100px;
    }
    .gcq img{
        width: 200px;
    }
    .lsq img{
        width: 328px;
    }
    .gcq{
        position: absolute;
        top: 440px;
        left: 0px;
    }
    .lsq{
        position: absolute;
        top: 508px;
        left: 100px;
    }
    .jnq{
        position: absolute;
        top: 328px;
        left: 262px;
    }
    .jnq img{
        width: 368px;
    }
    .yhtq{
        position: absolute;
        top: 406px;
        left: 444px;
    }
    .yhtq img{
        width: 94px;
    }

    .qhq{
        position: absolute;
        top: 466px;
        left: 484px;
    }
    .qhq img{
        width: 105px;
    }
    .jyq{
        position: absolute;
        top: 412px;
        left: 471px;
    }
    .jyq img{
        width: 126px;
    }


    .xwq{
        position: absolute;
        top: 476px;
        left: 511px;
    }

    .xwq img{
        width: 117px;
    }

    .xxq{
        position: absolute;
        top: 469px;
        left: 518px;
    }
    .xxq img{
        width: 205px;
    }

    .glq{
        position: absolute;
        top: 442px;
        left: 525px;
    }
    .glq img{
        width: 119px;
    }
    .pkq{
        position: absolute;
        top: 226px;
        left: 384px;
    }
    .pkq img{
        width: 350px;
    }
    .lhq{
        position: absolute;
        top: 348px;
        left: 626px;
    }
    .lhq img{
        width: 348px;
    }

    .hl1{
        position: absolute;
        top: 493px;
        left: 31px;
        z-index: -1;
    }
    .hl1 img{
        width: 210px;
    }
    .hl2{
        position: absolute;
        top: 349px;
        left: 383px;
        z-index: -1;
    }
    .hl2 img{
        width: 316px;
    }

    .header{
        width: 100%;
        height: 112px;
        background: url("/mb/njmapimg/top.png") no-repeat center;
        background-size: 100% 100%;
    }
    .header img{
        display: block;
        width: 960px;
        margin: 0 auto;
    }
    .leftbox{
        padding: 14px  0 0 20px;
    }
    .leftbox2{
        width: 300px;
        position: relative;
    }
    .leftbox img{
        width: 300px;
    }
    .leftbox p{

        position: absolute;
        left: 50px;
        top: 12px;
        display: block;
        text-align: center;
        color: #42d4ff;
        font-size: 18px;

    }
    .leftbox span{
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%,-50%);
        display: block;
        text-align: center;
        color: #42d4ff;
        font-size: 80px;
    }
    .shipin{
        position: fixed;
        width: 100%;
        height: 100%;
        left: 0;
        top: 0;
        z-index: -1;
        opacity: 0.2;
    }
    #bgmusic{
        position: fixed;
        right: 0;
        bottom: 0;
        border: none;
        display: none;
    }
</style>
<body>
    

    <div class="header">
        <img src="/mb/njmapimg/title.png" alt="">
    </div>
    <div class="shipin">
        <video src="/mb/njmapimg/shipin.mp4" style="width: 100%;height: 100%;object-fit: cover;position: absolute;top: 0;left: 0;" autoplay="autoplay" loop="loop" muted="muted"></video>
    </div>
    <div class="leftbox">
        <div class="leftbox2">
            <img src="/mb/njmapimg/left.png" alt="">
            <p>南京市级代理剩余名额</p>
            <span></span>
        </div>
    </div>


    <div class="mapbox">
        <div class="map">
            <div class="hl1">
                <img src="/mb/njmapimg/hl1.png" alt="">
                <span></span>
            </div>
            <div class="hl2">
                <img src="/mb/njmapimg/hl2.png" alt="">
                <span></span>
            </div>
            <div class="gcq">
                <img src="/mb/njmapimg/gcq.png" alt="">
                <p style="position: absolute;left:49px;top:208px;color:#ffffff;font-size:18px;z-index:99999;">高淳区(<span></span>)</p>
            </div>
            <div class="glq">
                <img src="/mb/njmapimg/glq.png" alt="">
                <p style="position: absolute;left:38px;top:32px;color:#ffffff;font-size:14px;z-index:99999;">鼓楼区<br/>(<span></span>)</p>
            </div>
            <div class="jnq">
                <img src="/mb/njmapimg/jnq.png" alt="">
                <p style="position: absolute;left:84px;top:182px;color:#ffffff;font-size:24px;z-index:99999;">江宁区(<span></span>)</p>
            </div>
            <div class="jyq">
                <img src="/mb/njmapimg/jyq.png" alt="">
                <p style="position: absolute;left:48px;top:36px;color:#ffffff;font-size:14px;z-index:99999;">建邺区<br/>(<span></span>)</p>
            </div>
            <div class="lhq">
                <img src="/mb/njmapimg/lhq.png" alt="">
                <p style="position: absolute;left:114px;top:176px;color:#ffffff;font-size:24px;z-index:99999;">六合区(<span></span>)</p>
            </div>
            <div class="lsq">
                <img src="/mb/njmapimg/lsq.png" alt="">
                <p style="position: absolute;left:104px;top:136px;color:#ffffff;font-size:26px;z-index:99999;">溧水区(<span></span>)</p>
            </div>
            <div class="pkq">
                <img src="/mb/njmapimg/pkq.png" alt="">
                <p style="position: absolute;left:128px;top:125px;color:#ffffff;font-size:24px;z-index:99999;">浦口区(<span></span>)</p>
            </div>
            <div class="qhq">
                <img src="/mb/njmapimg/qhq.png" alt="">
                <p style="position: absolute;left:23px;top:48px;color:#ffffff;font-size:14px;z-index:99999;">秦淮区<br/>(<span></span>)</p>
            </div>
            <div class="xwq">
                <img src="/mb/njmapimg/xwq.png" alt="">
                <p style="position: absolute;left:42px;top:50px;color:#ffffff;font-size:14px;z-index:99999;">玄武区(<span></span>)</p>
            </div>
            <div class="xxq">
                <img src="/mb/njmapimg/xxq.png" alt="">
                <p style="position: absolute;left:82px;top:86px;color:#ffffff;font-size:14px;z-index:99999;">栖霞区<br/>(<span></span>)</p>
            </div>
            <div class="yhtq">
                <img src="/mb/njmapimg/yhtq.png" alt="">
                <p style="position: absolute;left:14px;top:11px;color:#ffffff;font-size:14px;z-index:99999;">雨花台区<br/>(<span></span>)</p>
            </div>

        </div>
    </div>

    <audio autoplay="autoplay" controls="controls" loop="loop" id="bgmusic">
        <source src='' type="audio/mpeg"></source>
    </audio>
<script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script> 
<script type="text/javascript">
    
    $(function(){
        var music=document.getElementById("bgmusic");
        console.log(music)
        var isPlaying = false; 
        $("body").click(function(){
            if(!isPlaying){
                var player = document.querySelector('#bgmusic');
                if (isPlaying) {
                    // 如果正在播放, 停止播放并停止读取此音乐文件
                    player.pause();
                    player.src = '';
                } else { 
                    isPlaying = true; 
                    player.src = '/mb/njmapimg/music.mp3';
                    player.play();
                }
            }
        })

		var token = window.sessionStorage.getItem("Usertoken");
        //获取数据
        const getdata = ()=> {
            return new Promise( resolve => $.ajax({
                type:"post",url:"/api/user/mapEditName",data:{token:token,type:2},success:data=>resolve(data),error:err=>console.log(err)
            }))
        }
        //每隔5秒就要获取数据更新
        setInterval(()=>getdata().then(data=>setdatato(data)),5000);
        //进来就会显示
        (async ()=>{const listdata = await getdata();setdatato(listdata)})()
        //将数据放到地图当中
        function setdatato(data){
            if( data.status == 1 ){
                let listData = JSON.parse(data.data[2].name)
                console.log(listData)

let numbername = listData.njssyme;

                $(".leftbox span").html(numbername)

                if(listData.gcq != 0){
                    $(".gcq p span").html(listData.gcq)
                    $(".gcq img").removeClass("imgaddclass")
                }else{
                    $(".gcq p span").html(listData.gcq)
                    $(".gcq img").addClass("imgaddclass")
                }
                if(listData.glq != 0){
                    $(".glq p span").html(listData.glq)
                    $(".glq img").removeClass("imgaddclass")
                }else{
                    $(".glq p span").html(listData.glq)
                    $(".glq img").addClass("imgaddclass")
                }
                if(listData.jnq != 0){
                    $(".jnq p span").html(listData.jnq)
                    $(".jnq img").removeClass("imgaddclass")
                }else{
                    $(".jnq p span").html(listData.jnq)
                    $(".jnq img").addClass("imgaddclass")
                }
                if(listData.jyq != 0){
                    $(".jyq p span").html(listData.jyq)
                    $(".jyq img").removeClass("imgaddclass")
                }else{
                    $(".jyq p span").html(listData.jyq)
                    $(".jyq img").addClass("imgaddclass")
                }
                if(listData.lhq != 0){
                    $(".lhq p span").html(listData.lhq)
                    $(".lhq img").removeClass("imgaddclass")
                }else{
                    $(".lhq p span").html(listData.lhq)
                    $(".lhq img").addClass("imgaddclass")
                }
                if(listData.lsq != 0){
                    $(".lsq p span").html(listData.lsq)
                    $(".lsq img").removeClass("imgaddclass")
                }else{
                    $(".lsq p span").html(listData.lsq)
                    $(".lsq img").addClass("imgaddclass")
                }
                if(listData.pkq != 0){
                    $(".pkq p span").html(listData.pkq)
                    $(".pkq img").removeClass("imgaddclass")
                }else{
                    $(".pkq p span").html(listData.pkq)
                    $(".pkq img").addClass("imgaddclass")
                }
                if(listData.qhq != 0){
                    $(".qhq p span").html(listData.qhq)
                    $(".qhq img").removeClass("imgaddclass")
                }else{
                    $(".qhq p span").html(listData.qhq)
                    $(".qhq img").addClass("imgaddclass")
                }
                if(listData.xwq != 0){
                    $(".xwq p span").html(listData.xwq)
                    $(".xwq img").removeClass("imgaddclass")
                }else{
                    $(".xwq p span").html(listData.xwq)
                    $(".xwq img").addClass("imgaddclass")
                }
                if(listData.xxq != 0){
                    $(".xxq p span").html(listData.xxq)
                    $(".xxq img").removeClass("imgaddclass")
                }else{
                    $(".xxq p span").html(listData.xxq)
                    $(".xxq img").addClass("imgaddclass")
                }
                if(listData.yhtq != 0){
                    $(".yhtq p span").html(listData.yhtq)
                    $(".yhtq img").removeClass("imgaddclass")
                }else{
                    $(".yhtq p span").html(listData.yhtq)
                    $(".yhtq img").addClass("imgaddclass")
                }
                
            }else{
                console.log(data.message)
            }
        }

    })

</script>
</body>
</html>