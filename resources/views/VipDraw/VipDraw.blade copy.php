<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>VIP抽奖</title>
    <link href="/sweetalert2/sweetalert2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
</head>
<style type="text/css">
    body:before{
        content: ' ';
        position: fixed;
        z-index: -1;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background: url(/payviews/img/beijing1.jpg) top center no-repeat;
        background-size: cover;
    }
    h2 {text-align: center;color: #fff;font-size: 40px;margin-bottom: 30px;}
    .block{float: left;display: inline-block;font-size: 24px;line-height: 38px;color: #fff;}
    #search, input[type=text], input[type=password] {border: 1px solid #DCDEE0;font-size: 16px;text-align: center;width: 100px;height: 34px;box-sizing: border-box;display: inline-block;vertical-align: middle;}
    .td1 {border-right: 1px solid #fff;border-bottom: 1px solid #fff;color: #fff;font-size: 20px;text-align: center;line-height: 40px;}
    .td2 {border-bottom: 1px solid #fff;color: #fff;font-size: 20px;text-align: center;line-height: 40px;}
    .btn-box{width:800px;display:flex;margin:auto;}
    .btn{
        width:125px;
        height:50px;
        line-height: 50px;
        box-sizing: border-box;
        text-align: center;
        cursor:pointer;
        color: #000;
        background-color: #fff;
        margin:50px auto;
    }
    .endPao{
        color:red;
    }
    input{width:300px !important;}
    .submit{width:80px !important;}
    .layui-this{color:#000 !important;}
    dd{color:#000 !important;}
</style>
<body>
<h1 style="height: 80px;margin: 50px"></h1>
<div style="width:800px; margin:auto">
    <h2>会员抽奖</h2>
    <form method="post" class="layui-form" style="text-align:center;display:  inline-block;" autocomplete="off" >
        <div class="block" style="display:flex;"><span>门店名称</span>
            <div style="margin-left:30px;">
            <select name="agent" id="agent" lay-filter="agent" lay-search>

            </select>
            </div>
        </div>
        <div class="block">
            <span>　</span>
            <input type="button"  class="submit" id="search" value="查询">
        </div>
        <div class="clear"></div>
    </form>
</div>
<div style="height:20px"></div>
<div style="width:800px; margin:auto; border:1px solid #fff; border-bottom:none; color:#fff">
    <table width="800" cellspacing="0" cellpadding="0">
        <tbody id="list">
            <tr>
                <td class="td1">昵称</td>
                <td class="td1">电话</td>
            </tr>
            <tr>
               <td colspan="3" class="td2">暂无数据</td>
            </tr>
        </tbody>
    </table>
</div>
<!-- 操作按钮-->
<div class="btn-box">
    <div class="btn startPao">开始抽奖</div>
    <div class="btn endPao">结束抽奖</div>
</div>
<input type="hidden" class="store_id">
<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script type="text/javascript">
		layui.use(['form','jquery'],function(){

          //基础变量信息
		  var form     = layui.form;
          var $        = jquery = layui.jquery;//如果不写$ = jquery,默认是不会把jquery中的$对象引进来
          var token    = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3Rlc3QueXVuc295aS5jbi9hcGkvbWVyY2hhbnQvbG9naW4iLCJpYXQiOjE2MDY0NDM5MDUsImV4cCI6MTYxNTA4MzkwNSwibmJmIjoxNjA2NDQzOTA1LCJqdGkiOiJnOWR1Q1hjSTRzWTd6bnZVIiwic3ViIjp7InR5cGUiOiJtZXJjaGFudCIsIm1lcmNoYW50X2lkIjo5LCJtZXJjaGFudF9uYW1lIjoiXHU5NzU5XHU5NzU5IiwiY29uZmlnX2lkIjoiMTIzNCIsInBpZCI6MCwicGhvbmUiOiIxMzU4MTc2Mjc1MyIsImltZWkiOm51bGwsInVzZXJfaWQiOjEsIm1lcmNoYW50X3R5cGUiOiIxIiwiZW1haWwiOiIiLCJjcmVhdGVkX3N0b3JlX25vIjoiMjAyMDExMjcxMDI1MDUyMDM1In0sInBydiI6IjkzYmRjYzU4ZGQwMWNlMzZlYzU2ZTMyYjViYjU4MGQ4MzAzMmZkMTgifQ.SJLv1vmsaYrD3uoo40lt0zYrL3PiE5TvqH5hXaU3Tjs';
          var luckDrawPreize = {
              /**
               * 定义全局配置项
               */
              config:{
                  //定义是否全员参与，false:说明只能用户手机号用户参与 true:说明全员参与
                  is_all_user:false,
                  //定义门店id
                  store_id: "",
                  //门店下面的用户列表数据
                  store_member_user:[],
                  //选中的标识
                  drawSlectBackgroundColor:"#FFDEAD",
                  //抽奖动画的定时器
                  time:"",
                  //滚动的速率
                  speed:80
              },

              /**
               * 获取所有的门店
               */
              initStore:function(){
                  let that = this;
                  //发送请求
                  $.ajax({
                    type : "POST",
                    dataType : "json",
                    url : "/api/merchant/store_lists",
                    data: {token:token},
                    success : function(responseData) {
                        let status = responseData.status;
                        let data   = responseData.data;
                        //只有数据请求成功之后，才可以进行赋值
                        if(status == 1){
                            if(data.length > 0){
                                var optionStr = "";
                                for(var i=0;i < data.length;i++){
                                    if(i == 0){
                                        optionStr += "<option selected value='" + data[i].store_id + "'>" + data[i].store_name + "</option>";
                                    }else{
                                        optionStr += "<option value='" + data[i].store_id + "'>" + data[i].store_name + "</option>";
                                    }
                                }
                                $("#agent").append(optionStr);
                                layui.form.render('select');

                                var store_id = data[0].store_id;
                                $('.store_id').val(store_id);
                                that.config.store_id = store_id;
                                //获取该门店下面的用户
                                that.getStoreMemberUser(store_id);
                            }
                        }
                    }
                })

              },

              /**
               * 获取门店下面的所有用户，包括支付宝用户。微信用户
               */
              getStoreMemberUser:function(store_id){
                    let that = this;
                    $.ajax({
                        type : "POST",
                        dataType : "json",
                        url : "/api/member/mb_lists",
                        data: {
                            token:token,
                            store_id:store_id
                        },
                        success : function(responseData) {
                            var info = "";
                            var status = responseData.status;
                            var data   = responseData.data;
                            if(status == 1){
                                if(data.length > 0){

                                    that.config.store_member_user = data;

                                    for(var i=0;i<data.length;i++){
                                        info += '<tr>' +
                                        '<td class="td1">' +
                                            data[i].mb_name +
                                            '</td>' +
                                            '<td class="td1 pao pao-1">' +
                                            data[i].mb_phone +
                                            '</td>'+
                                            '</tr>';

                                        $(".btn-box").show();
                                    }
                                }else{
                                    info +=
                                    '<tr>'+
                                    '<td colspan="3" class="td2">'+'暂无数据'+'</td>'+
                                    '</tr>'
                                    $(".btn-box").hide();
                                }
                                document.getElementById("list").innerHTML = info;
                            }
                        }
                    });
              },

              /**
               * 切换门店
               */
               selectStoreId:function(store_id){
                   let that = this;
                   $('.store_id').val(store_id);
                   that.config.store_id = store_id;
                   //获取该门店下面的用户
                   that.getStoreMemberUser(store_id);
               },

               /**
                * 开始抽奖
                */
                startLuckDraw:function(){
                    let that = this;
                    //获取门店id
                    let store_id = that.config.store_id;
                    //获取门店下面的用户列表数据
                    let store_member_user = that.config.store_member_user;
                    //全员参与还是有用户手机号的参与的标识
                    let is_all_user = that.config.is_all_user;

                    let draw_user = [];

                    if(store_member_user.length > 0){
                        //判断是全员参与还是有用户手机号的参与
                        if(is_all_user){
                            //全员参与
                            draw_user = store_member_user;
                        }else{
                            //有用户手机号的参与
                            for(var i=0;i<store_member_user.length;i++){
                                if(store_member_user[i].mb_phone){
                                    draw_user.push(store_member_user[i]);
                                }
                            }
                        }

                        //进行开始抽奖
                        let child = $("#list").children();
                        let index = 0;
                        that.config.time = setInterval(() => {
                            $("#list").children().eq(index).siblings().css({"background":"transparent"}).attr("draw",2);
                            $("#list").children().eq(index).css({"background":that.config.drawSlectBackgroundColor}).attr("draw",1);
                            if(index >= child.length-1){
                                index = 0;
                            }else{
                                index = index + 1;
                            }
                        }, that.config.speed);
                    }
                },

                /**
                 * 结束抽奖
                 * 需要显示中奖结果
                 */
                endLuckDraw:function(){
                    let that= this;
                    clearInterval(that.config.time);
                    let child = $("#list").children();
                    $.each(child,function(index,item){
                        let draw = $(item).attr("draw");
                        if(draw == 1){
                            console.log("中奖手机号："+$(item).children().last().html());
                        }
                    });
                },
          };
          //进行调用获取门店的方法
          luckDrawPreize.initStore();

          // 选择门店
          form.on('select(agent)', function(data){
            var store_id = data.value;
            luckDrawPreize.selectStoreId(store_id);
          });

          //点击开始抽奖
          $(".startPao").on("click",function(){
              luckDrawPreize.startLuckDraw();
          });

          //结束抽奖
          $(".endPao").on("click",function(){
            luckDrawPreize.endLuckDraw();
          });
          //查询
          $("#search").on('click', function () {
            layer.msg("查询成功", {icon:1, shade:0.5, time:1000});
            //获取该门店下面的用户
            luckDrawPreize.getStoreMemberUser();
          });
        });
	</script>
</body>
</html>