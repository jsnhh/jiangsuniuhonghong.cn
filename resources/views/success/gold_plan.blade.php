<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset=utf-8>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name=referrer content="origin">
  <meta name=viewport
        content="width=device-width, viewport-fit=cover, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
  <title>支付完成</title>
  <script type=text/javascript charset=UTF-8 src=https://wx.gtimg.com/pay_h5/goldplan/js/jgoldplan-1.0.0.js></script>
  <script type=text/javascript src=https://libs.baidu.com/jquery/2.0.0/jquery.min.js></script>
  <script type="text/javascript" src="../zhifu/js/swiper.jquery.min.js"></script>

  <link rel="stylesheet" href="../zhifu/css/swiper.min.css">

  <style>
    body {
      font-family: PingFang SC, "Helvetica Neue", Arial, sans-serif;
    }

    .order_box {
      text-align: center;
    }

    .order_box .bussiness_avt img {
      width: 68px;
      height: 68px;
      border-radius: 100%;
      border: 1px solid #E0E0E0;
    }

    .b_name {
      font-size: 15px;
      font-weight: 500;
      color: #333333;
      margin-top: 10px;
    }

    .col_box {
      margin: 25px;

    }

    .col_box .col {
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
      -webkit-box-pack: justify;
      -ms-flex-pack: justify;
      -webkit-justify-content: space-between;
      justify-content: space-between;
      font-size: 15px;
      color: #333;
      margin-bottom: 15px;
    }


    .swiper-container {
      height: 180px;
      margin-top: 35px;
    }

    .swiper-container .swiper-wrapper .swiper-slide img {
      width: 100%;
    }


  </style>
</head>

<body>
<div class="order_box">
  <div class="b_name" id="b_name">
    商户名称
  </div>
  <div class="col_box">
    <div class="col">
      <div class="lab">订单状态</div>
      <div id="pay_status_desc" class="ct"></div>
    </div>
    <div class="col">
      <div class="lab">实付金额</div>
      <div id="price" class="ct price">¥</div>
    </div>
  </div>


  <div class="swiper-container">
    <div id="toPicture" class="swiper-wrapper">

    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
  </div>

</div>
</body>



<script>
  //获取返回页面参数
  function getQueryString(name) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split("=");
      if (pair[0] === name) {
        return pair[1];
      }
    }
    return null;
  };
  //获取参数
  var sub_mch_id = getQueryString("sub_mch_id"); //特约商户号
  //console.log("sub_mch_id is " + sub_mch_id)
  var out_trade_no = getQueryString("out_trade_no"); //商户订单号
  //console.log("out_trade_no is " + out_trade_no)
  var check_code = getQueryString("check_code"); //md5 校验码
  //console.log("check_code is " + check_code)

  // 通过接口获取订单信息
  $.ajax({
    type: "GET",
    url: "/api/merchant/orderDetails",
    data: {
      // sub_mch_id: sub_mch_id,
      out_trade_no: out_trade_no,
      // check_code: check_code
    },
    dataType: "json",
    success: function (data) {
      if (data.status == 1 && data.data.pay_status == 1) { // 将订单信息赋值当前html页面
        var info = data.data;

        $("#b_name").html(info.store_name)
        $("#pay_status_desc").html(info.pay_status_desc)
        $("#price").html('¥' + info.receipt_amount)

        var linkMap = {};

        if(info.adData.length > 0){
          var picObj = $('#toPicture');
          $("#toPicture").html("");

          var adData = info.adData;
          for (var i = 0; i < adData.length; i++) {

            linkMap["picitem"+i] = adData[i].click_url;
            //添加图片到容器中

            picObj.append('<div class="swiper-slide"> <a href=""> <img id="picitem'+i+'" style="max-height: 180px; z-index:'+i+'" src="'+adData[i].img_url+'" /> </a> </div>');

          }

          var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            spaceBetween: 30,
            centeredSlides: true,
            autoplay: 2000,
            autoplayDisableOnInteraction: false,
            loop : true,
          });

        }

        // 计算服务商页面当前高度
        var customPageHeight = document.body.scrollHeight; // 单位 px

        // 传给父页面的高度(单位:px) = customPageHeight * (640 / 子页面真实宽度)
        var height = customPageHeight * (640 / Math.round(document.documentElement.getBoundingClientRect().width));

        //初始化小票
        var mchData ={
          action:'onIframeReady',
          displayStyle:'SHOW_CUSTOM_PAGE',
          height: height
        }
        var postData = JSON. stringify(mchData)
        parent.postMessage(postData,'https://payapp.weixin.qq.com')

        //注册点击事件(去首页)
        document.getElementById("toPicture").onclick = function (e) {
          var imgId = e.target.id;
          var homeLink = "";
          for(var key in linkMap){
            if(key == imgId){
              homeLink = linkMap[key];
            }
          }

          var mchData = {
            action: 'jumpOut',
            jumpOutUrl: homeLink //跳转的页面
          }
          var postData = JSON.stringify(mchData)
          parent.postMessage(postData, 'https://payapp.weixin.qq.com')
        }

      }
    }
  });

</script>

</html>