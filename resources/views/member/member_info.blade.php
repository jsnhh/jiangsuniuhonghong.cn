<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="renderer" content="webkit">
    <meta name="imagemode" content="force">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>会员充值</title>
    <link rel="stylesheet" href="{{asset('/payviews/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('/phone/css/swiper.min.css')}}">
    <script type="text/javascript" src="{{asset('/phone/js/Screen.js')}}"></script>
    <style>
        .top {
            background-color: #fff;
            width: 100%;
            height: 3.92rem;
            padding-top: .3rem;
        }

        .bank {
            /*background: url("




        {{url('/zhifu/img/vip-bg-1.png')}}                 ") no-repeat;*/
            /*width: 6.22rem;
            height: 3.72rem;
            background-size: cover;
            margin: 0 auto;*/

            position: relative;

        }

        .bj {
            width: 6.22rem;
            height: 3.72rem;
            /*margin: 0 auto;*/

            position: absolute;
            left: 50%;
            margin-left: -3.11rem;
        }

        .bank .logo {
            width: .8rem;
            height: .8rem;
            float: left;
            padding: .2rem;

        }

        .bank .name {
            width: .8rem;
            height: .8rem;
            float: left;
            width: 4rem;
            padding-top: .2rem;
            color: #fff;
        }

        .bank .name span {
            display: block;
            font-size: .32rem;
        }

        .bank .name span:nth-child(2) {

            font-size: .28rem;
        }

        .bank .code {
            width: .42rem;
            height: .42rem;
            float: right;
            padding: .4rem .3rem 0 0;
        }

        .bankno {
            float: left;
            padding: 2.8rem 0 0 .2rem;
            color: #fff;
            font-size: .34rem;
            position: absolute;
            left: 50%;
            margin-left: -3rem;
        }

        .czlist {
            background-color: #fff;
            overflow: hidden;
            margin-top: .12rem;
        }

        .czcon {
            margin-left: .3rem;
            width: 6.9rem;
        }

        .czcon .czcon_l{
        	/*width: 6.22rem;*/
            height: 1.2rem;
            line-height: 1.2rem;
        }
        .czcon .czcon_l label{
        	float:left;
        	font-size:.28rem;
            color:#333333;
        }
        .czcon .czcon_l input{
        	float:right;
        	height: 1.2rem;
            line-height: 1.2rem;
            border:none;
            text-align: right;
            font-size:.28rem;
            outline:medium;
        }
        .czcon .czcon_l .yzm{
        	width:4rem;
        	float:left;
        	width:2.5rem;
        	padding-left:.8rem;
        	text-align: left;
        }
        .czcon .czcon_l p{        	
        	float:right;
        	width:1.56rem;
        	height:.6rem;
        	line-height: .6rem;
        	outline: none;
			-webkit-appearance: none;
			border:.02rem solid #009ee6;
			background-color: #fff;
			border-radius:0.1rem;
			color:#2aa1f7;
			text-align: center;
			margin-top:.3rem;
			font-size:.24rem;
        }

        .czlist .title {
            font-size: .32rem;
            color: #333;
            padding: .3rem 0 0 .3rem;

        }
        .btn {
            width: 4rem;
            height: .88rem;
            line-height: .88rem;
            background-color: #ddd;
            text-align: center;
            border-radius: .1rem;
            margin: .8rem auto 1rem;
            color: #fff;
        }
        .cur {
            background-color: #108ee9;
        }


    </style>
</head>
<body>
<div class="box">
    <div class="top">
        <div class="bank">
            <img class="bj" src="{{url('/zhifu/img/vip-bg-1.png')}}">

            <div style="overflow: hidden;position: absolute;z-index: 999; left: 50%; margin-left: -3rem;">
                <img class="logo" src="{{url('/zhifu/img/logo.png')}}">
                <div class="name">
                    <span class="bank_name"></span>
                    <span class="bank_desc"></span>
                </div>
                <img class="code" src="{{url('/zhifu/img/shuzima.png')}}">
            </div>

            <div class="bankno"></div>
        </div>
    </div>
    <div class="czlist">
        <div class="title">填写资料</div>
        <div class="czcon">
        	<div class="czcon_l">
        		<label>会员姓名</label>
        		<input type="text" placeholder="请输入会员姓名" class="name" id="name">
        	</div>
        	<div class="czcon_l">
        		<label>手机号码</label>
        		<input type="text" placeholder="请输入手机号码" class="phone" id="phone">
        	</div>
        	<div class="czcon_l">
        		<label>验证码</label>
        		<input class="yzm js-code" type="text" placeholder="请输入验证码">
        		<p id="btnSendCode">点击获取</p>
        	</div>
        </div>
        <div class="btn">确定开卡</div>

    </div>
    
</div>


<input type="hidden" class="js_cz">
<input type="hidden" class="js_cz_s">

<input type="hidden" class="js_name">
<input type="hidden" class="js_phone">
<input type="hidden" class="js_yzm">



<script type="text/javascript" src="{{asset('/phone/js/jquery-2.1.4.js')}}"></script>
<script type="text/javascript" src="{{asset('/phone/js/swiper.jquery.min.js')}}"></script>
<script src="{{asset('/phone/js/fastclick.js')}}"></script>
<script type="text/javascript" src="{{asset('/school/js/jsencrypt.min.js')}}"></script>
<script type="text/javascript">


    function GetQueryString(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null)return unescape(r[2]);
        return null;
    }
    var store_id = GetQueryString("store_id");
    var merchant_id = GetQueryString("merchant_id");
    var open_id = GetQueryString("open_id");
    var mb_id = GetQueryString("mb_id");
    var ways_source = GetQueryString("ways_source");
    var ways_type = GetQueryString("ways_type");


    $(document).ready(function () {
        
        
        // 银行卡信息
        $.post("{{url('api/member/query_tpl_web')}}",
        {
            store_id: store_id,
        }, function (res) {
            
            if (res.status == 1) {
                $('.logo').attr('src', res.data.logo_url)
                $('.bank_name').html(res.data.tpl_name)
                $('.bank_desc').html(res.data.tpl_desc)
                $('.bankno').html(mb_id)

                $('.bj').attr("src", res.data.tpl_bck_url);

            } else {

            }
        }, "json");
    });

    
    $('.name').bind('input propertychange', function() { 
	    $('.js_name').val($(this).val());  
	}); 
	$('.phone').bind('input propertychange', function() {  
	    $('.js_phone').val($(this).val());  
	});
	$('.yzm').bind('input propertychange', function() {  
	    $('.js_yzm').val($(this).val());
	    $('.btn').addClass('cur')

	});  

	//发送验证码
    var InterValObj; //timer变量，控制时间
    var count = 60; //间隔函数，1秒执行
    var curCount;//当前剩余秒数
	$('#btnSendCode').click(function(){
      var encrypt = new JSEncrypt();
      // phone=$('.js-tel').val()&info="2"&type=type
      var phone=$('.js_phone').val();
      encrypt.setPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4COVutRbOUfQNjvVOzwK49NzHIPRwwksnJ6QtdHwGmdUZiT2HZxVwfotcOjA5aY16D/2Ahq3gLH4yu2y42dS0lfeBMqUcm+bY7aZ54wClm75RI90uc54F8IgMkNz8J/VS9LYI/B4uHVsc+4KK4Ycr8S8O004ExtvQqu2QCl7Aai/WC4URIdCyNm8La2axoA1jjj3SzpytLvP6Z/iHSlx37Y9AMR0V94R13v4BFlMQDG+2REVJsk6LCyzHQfUvJlnsyKey0n/v8DLC070lQzLPYV0jsiit2AUkyURRLxEaZm2C0YYhfrGjl+x8n/kDteZbDVcyn7UsEdSicijv9DXkQIDAQAB");
      var data = encrypt.encrypt('phone='+phone);

      curCount = count;
      //设置button效果，开始计时
      $("#btnSendCode").attr("disabled", "true");
      $("#btnSendCode").val(curCount+'(s)');
      InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次
      //向后台发送处理数据
      $.post("{{url('/api/Sms/send')}}",
      {
          sign:data 
      },
      function (res){
         
          if(res.status==1){
              alert(res.message);
              $("#btnSendCode").attr("disabled", "true");
              $("#btnSendCode").html(curCount+'(s)');
              window.clearInterval(InterValObj);//停止计时器
              InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次
          }else{
              
              window.clearInterval(InterValObj);//停止计时器
              $("#btnSendCode").removeAttr("disabled");//启用按钮                    
              alert(res.message);
              $("#btnSendCode").html("获取验证码");
          }
      },'json');
    })
    //timer处理函数
    function SetRemainTime() {
        if (curCount == 0) {
            window.clearInterval(InterValObj);//停止计时器
            $("#btnSendCode").removeAttr("disabled");//启用按钮
            $("#btnSendCode").html("获取验证码");
        }
        else {
            curCount--;
            $("#btnSendCode").html(curCount+'(s)');
        }
    } 
    
    $('.btn').click(function () {
        if ($(this).hasClass('cur')) {
            $.post("{{url('/api/member/member_info_save')}}",
                {
                    store_id: store_id,
                    merchant_id: merchant_id,
                    open_id: open_id,
                    mb_name: $('#name').val(),
                    mb_phone: $('#phone').val(),
                    sms_code: $('.js-code').val(),
                    ways_source: ways_source

                }, function (data) {

                    if (data.status == 1) {

                       window.location.href="{{url('/web/member/cz_view?store_id=')}}"+store_id+'&merchant_id='+merchant_id+'&open_id='+open_id+'&mb_id='+mb_id+'&ways_type='+ways_type

                    } else {
                        alert(data.message)

                    }

                }, "json");
        } else {
            alert('请选择充值金额')
        }


    })
</script>
</body>
</html>