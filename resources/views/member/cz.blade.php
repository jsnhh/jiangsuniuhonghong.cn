<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="renderer" content="webkit">
    <meta name="imagemode" content="force">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title>充值</title>
    <link rel="stylesheet" href="{{asset('/payviews/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('/phone/css/swiper.min.css')}}">
    <script type="text/javascript" src="{{asset('/phone/js/Screen.js')}}"></script>
    <style>
        .top {
            background-color: #fff;
            width: 100%;
            height: 3.92rem;
            padding-top: .3rem;
        }

        .bank {
            /*background: url("{{url('/zhifu/img/vip-bg-1.png')}}") no-repeat;*/
            /*width: 6.22rem;
            height: 3.72rem;
            background-size: cover;
            margin: 0 auto;*/
            position: relative;
        }

        .bj {
            width: 6.22rem;
            height: 3.72rem;
            /*margin: 0 auto;*/
            position: absolute;
            left: 50%;
            margin-left: -3.11rem;
        }

        .bank .logo {
            width: .8rem;
            height: .8rem;
            float: left;
            padding: .2rem;
        }

        .bank .name {
            width: .8rem;
            height: .8rem;
            float: left;
            width: 4rem;
            padding-top: .2rem;
            color: #fff;
        }

        .bank .name span {
            display: block;
            font-size: .32rem;
        }

        .bank .name span:nth-child(2) {
            font-size: .28rem;
        }

        .bank .code {
            width: .42rem;
            height: .42rem;
            float: right;
            padding: .4rem .3rem 0 0;
        }

        .bankno {
            float: left;
            padding: 2.8rem 0 0 .2rem;
            color: #fff;
            font-size: .34rem;
            position: absolute;
            left: 50%;
            margin-left: -3rem;
        }

        .czlist {
            background-color: #fff;
            overflow: hidden;
            margin-top: .12rem;
        }

        .czcon {
            margin-left: .3rem;
            width: 6.9rem;
        }

        .czcon div {
            width: 2rem;
            height: .9rem;
            text-align: center;
            display: inline-block;
            border: .02rem solid #108ee9;
            border-radius: .1rem;
            padding: .18rem 0;
            margin-right: .3rem;
            margin-top: .28rem;
            color: #108ee9;
        }

        .czcon div:nth-child(3n) {
            margin-right: 0;
        }

        .czlist .title {
            font-size: .32rem;
            color: #333;
            padding: .3rem 0 0 .3rem;
        }

        .btn {
            width: 4rem;
            height: .88rem;
            line-height: .88rem;
            background-color: #ddd;
            text-align: center;
            border-radius: .1rem;
            margin: .8rem auto .2rem;
            color: #fff;
        }

        .cur {
            background-color: #108ee9;
        }

        .czlist .jlcon {
            width: 6.9rem;
            margin-left: .3rem;
            padding-bottom: .28rem;
        }

        .czlist .jlcon div {
            padding-top: .28rem;
            color: #999;
            font-size: .28rem;
            overflow: hidden;
        }

        .czlist .jlcon p {
            display: inline-block;
            float: right;
        }

        .czcon div.cur {
            background-color: #108ee9;
            color: #fff;
        }

    </style>
</head>
<body>
<div class="box">
    <div class="top">
        <div class="bank">
            <img class="bj" src="{{url('/zhifu/img/vip-bg-1.png')}}">

            <div style="overflow: hidden;position: absolute;z-index: 999; left: 50%; margin-left: -3rem;">
                <img class="logo" src="{{url('/zhifu/img/logo.png')}}">
                <div class="name">
                    <span class="bank_name"></span>
                    <span class="bank_desc"></span>
                </div>
                <img class="code" src="{{url('/zhifu/img/shuzima.png')}}">
            </div>

            <div class="bankno"></div>
        </div>
    </div>
    <div class="czlist">
        <div class="title">充值金额</div>
        <div class="czcon">

        </div>
        <div class="btn">立即充值</div>
    </div>
    <div class="czlist">
        <div class="title">最近30天余额记录</div>
        <div class="jlcon">

        </div>
    </div>
</div>

<input type="hidden" class="js_cz">
<input type="hidden" class="js_cz_s">
<script type="text/javascript" src="{{asset('/phone/js/jquery-2.1.4.js')}}"></script>
<script type="text/javascript" src="{{asset('/phone/js/swiper.jquery.min.js')}}"></script>
<script src="{{asset('/phone/js/fastclick.js')}}"></script>
<script type="text/javascript">

    function GetQueryString(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null)return unescape(r[2]);
        return null;
    }
    var store_id = GetQueryString("store_id");
    var merchant_id = GetQueryString("merchant_id");
    var open_id = GetQueryString("open_id");
    var mb_id = GetQueryString("mb_id");
    var ways_type = GetQueryString("ways_type");

    $(document).ready(function () {
        $.post("{{url('/api/member/cz_query')}}",
            {
                store_id: store_id
            }, function (res) {
                if (res.status == 1) {
                    var arr = JSON.parse(res.data.cz_list);

                    var str = "";
                    for (var i = 0; i < arr.length; i++) {
                        str += '<div class="bgoing">';
                        str += '<lable>' + arr[i].cz + '</lable>元<br>';
                        str += '送<span>' + arr[i].cz_s + '</span>元';
                        str += '</div>';
                    }

                    $('.czcon').html('');
                    $('.czcon').append(str)
                } else {

                }
            }, "json");

        // 30天余额记录
        $.post("{{url('api/member/cz_list_mb_ids')}}",
            {
                store_id: store_id
                ,open_id: open_id
                ,mb_id: mb_id
            }, function (res) {
                if (res.status == 1) {
                    var str = "";
                    for (var j = 0; j < res.data.length; j++) {
                        if (res.data.cz_s_money == '' || res.data.cz_s_money == 0) {
                            str += '<div>';
                            str += '<lable>' + res.data[j].pay_time + '</lable>';
                            str += '<p>' + res.data[j].cz_type_desc + ':<span>' + res.data[j].cz_money + '</span>元</p>';
                            str += '</div>';
                        } else {
                            str += '<div>';
                            str += '<lable>' + res.data[j].pay_time + '</lable>';
                            str += '<p>' + res.data[j].cz_type_desc + ':<span>' + res.data[j].cz_money + '</span>元(送' + res.data[j].cz_s_money + '元)</p>';
                            str += '</div>';
                        }
                    }
                    $('.jlcon').html('');
                    $('.jlcon').append(str)
                } else {

                }
            }, "json");

        // 银行卡信息
        $.post("{{url('api/member/query_tpl_web')}}",
            {
                store_id: store_id
            }, function (res) {
                if (res.status == 1) {
                    $('.logo').attr('src', res.data.logo_url);
                    $('.bank_name').html(res.data.tpl_name);
                    $('.bank_desc').html(res.data.tpl_desc);
                    $('.bankno').html(mb_id);
                    $('.bj').attr("src", res.data.tpl_bck_url);
                } else {

                }
            }, "json");
    });

    $('.czcon').on('click', '.bgoing', function () {
        $('.czcon .bgoing').removeClass('cur');
        $(this).addClass('cur');
        $('.btn').addClass('cur');
        $('.js_cz').val($(this).find('lable').html());
        $('.js_cz_s').val($(this).find('span').html());
    });

    $('.btn').click(function () {
        if ($(this).hasClass('cur')) {
            $.post("{{url('/api/member/cz')}}",
                {
                    store_id: store_id
                    ,merchant_id: merchant_id
                    ,open_id: open_id
                    ,mb_id: mb_id
                    ,ways_type: ways_type
                    ,cz: $('.js_cz').val()
                    ,cz_s: $('.js_cz_s').val()
                }, function (data) {
                    var data_url = "&total_amount=" + $('.js_cz').val() + "&store_id=" + $("#store_id").val();
                    var cz = $('.js_cz').val();
                    var cz_s = $('.js_cz_s').val();

                    if (data.status == 1) {
                        //官方支付宝
                        if (ways_type == "1000") {
                            AlipayJSBridge.call("tradePay", {
                                tradeNO: data.data.trade_no
                            }, function (result) {
                                //付款成功
                                if (result.resultCode == "9000") {
                                    var mb_money = data.mb_money;
                                    data_url = data_url + "&ad_p_id=1&cz=" + $('.js_cz').val() + "&cz_s=" + $('.js_cz_s').val() + "&mb_money=" + mb_money;

                                    window.location.href = "{{url('page/cz_success?message=支付成功')}}" + data_url;
                                }

                                if (result.resultCode == "6001") {
                                    data_url = data_url + "&ad_p_id=3";
                                    window.location.href = "{{url('page/pay_errors?message=取消支付')}}" + data_url;
                                }
                            });
                        }

                        //京东支付宝
                        if (ways_type == "6001") {
                            AlipayJSBridge.call("tradePay", {
                                tradeNO: data.trade_no
                            }, function (result) {
                                //付款成功
                                if (result.resultCode == "9000") {
                                    var mb_money = data.mb_money;
                                    data_url = data_url + "&ad_p_id=1&cz=" + $('.js_cz').val() + "&cz_s=" + $('.js_cz_s').val() + "&mb_money=" + mb_money;

                                    window.location.href = "{{url('page/cz_success?message=支付成功')}}" + data_url;
                                }

                                if (result.resultCode == "6001") {
                                    data_url = data_url + "&ad_p_id=3";
                                    window.location.href = "{{url('page/pay_errors?message=取消支付')}}" + data_url;
                                }
                            });
                        }

                        //和融通支付宝
                        if (ways_type == "9001") {
                            if (data.trade_no == "") {
                                window.location.href = data.url;//直接跳转链接
                            } else {
                                AlipayJSBridge.call("tradePay", {
                                    tradeNO: data.trade_no
                                }, function (result) {
                                    //付款成功
                                    if (result.resultCode == "9000") {
                                        var mb_money = data.mb_money;
                                        data_url = data_url + "&ad_p_id=1&cz=" + $('.js_cz').val() + "&cz_s=" + $('.js_cz_s').val() + "&mb_money=" + mb_money;

                                        window.location.href = "{{url('page/cz_success?message=支付成功')}}" + data_url;
                                    }

                                    if (result.resultCode == "6001") {
                                        data_url = data_url + "&ad_p_id=3";
                                        window.location.href = "{{url('page/pay_errors?message=取消支付')}}" + data_url;
                                    }
                                });
                            }
                        }

                        //和融通
                        if (ways_type == "9002") {
                            if (data.trade_no == "") {
                                window.location.href = data.url;//直接跳转到新大陆的链接
                            } else {
                                WeixinJSBridge.invoke(
                                    'getBrandWCPayRequest', {
                                        "appId": data.data.data.appId,     //公众号名称，由商户传入
                                        "timeStamp": data.data.data.timeStamp,         //时间戳，自1970年以来的秒数
                                        "nonceStr": data.data.data.nonceStr,  //随机串
                                        "package": data.data.data.package,
                                        "signType": data.data.data.signType,
                                        "paySign": data.data.data.paySign //微信签名
                                    },
                                    function (res) {
                                        if (res.err_msg == "get_brand_wcpay_request:ok") {
                                            data_url = data_url + "&ad_p_id=1&cz=" + $('.js_cz').val() + "&cz_s=" + $('.js_cz_s').val() + "&mb_money=" + mb_money;
                                            window.location.href = "{{url('page/cz_success?message=支付成功')}}" + data_url;
                                        }
                                        else {
                                            data_url = data_url + "&ad_p_id=4";
                                            window.location.href = "{{url('page/pay_errors?message=取消支付')}}" + data_url;
                                        }
                                    }
                                );
                            }
                        }

                        //新大陆
                        if (ways_type == "8001" || ways_type == "8002") {
                            window.location.href = data.url;//直接跳转链接
                        }

                        //网商银行支付宝
                        if (ways_type == "3001") {
                            AlipayJSBridge.call("tradePay", {
                                tradeNO: data.trade_no
                            }, function (result) {
                                //付款成功
                                if (result.resultCode == "9000") {
                                    var mb_money = data.mb_money;
                                    data_url = data_url + "&ad_p_id=1&cz=" + $('.js_cz').val() + "&cz_s=" + $('.js_cz_s').val() + "&mb_money=" + mb_money;

                                    window.location.href = "{{url('page/cz_success?message=支付成功')}}" + data_url;
                                }
                                if (result.resultCode == "6001") {
                                    data_url = data_url + "&ad_p_id=3";
                                    window.location.href = "{{url('page/pay_errors?message=取消支付')}}" + data_url;
                                }
                            });
                        }

                        //哆啦宝支付宝
                        if (ways_type == "15001") {
                            AlipayJSBridge.call("tradePay", {
                                tradeNO: data.reserved_transaction_id
                            }, function (result) {
                                //付款成功
                                if (result.resultCode == "9000") {
                                    var mb_money = data.mb_money;
                                    data_url = data_url + "&ad_p_id=1&cz=" + $('.js_cz').val() + "&cz_s=" + $('.js_cz_s').val() + "&mb_money=" + mb_money;

                                    window.location.href = "{{url('page/cz_success?message=支付成功')}}" + data_url;
                                }
                                if (result.resultCode == "6001") {
                                    data_url = data_url + "&ad_p_id=3";
                                    window.location.href = "{{url('page/pay_errors?message=取消支付')}}" + data_url;
                                }
                            });
                        }

                        //哆啦宝微信
                        if (ways_type == "15002") {
                            var json_data = eval('(' + data.data + ')');
                            WeixinJSBridge.invoke(
                                'getBrandWCPayRequest', {
                                    "appId": json_data.APPID,     //公众号名称，由商户传入
                                    "timeStamp": json_data.TIMESTAMP,         //时间戳，自1970年以来的秒数
                                    "nonceStr": json_data.NONCESTR, //随机串
                                    "package": json_data.PACKAGE,
                                    "signType": json_data.SIBGTYPE,         //微信签名方式：
                                    "paySign": json_data.PAYSIGN //微信签名
                                },
                                function (res) {
                                    if (res.err_msg == "get_brand_wcpay_request:ok") {
                                        data_url = data_url + "&ad_p_id=1&cz=" + $('.js_cz').val() + "&cz_s=" + $('.js_cz_s').val() + "&mb_money=" + mb_money;
                                        window.location.href = "{{url('page/cz_success?message=支付成功')}}" + data_url;
                                    } else {
                                        data_url = data_url + "&ad_p_id=4";
                                        window.location.href = "{{url('page/pay_errors?message=取消支付')}}" + data_url;
                                    }
                                }
                            );
                        }

                        //微信
                        if (ways_type == "2000" || ways_type == "3002" || ways_type == "6002") {
                            var json_data =  eval('(' + data.data + ')');
                            WeixinJSBridge.invoke(
                                'getBrandWCPayRequest', json_data,
                                function (res) {
                                    if (res.err_msg == "get_brand_wcpay_request:ok") {
                                        data_url = data_url + "&ad_p_id=1&cz=" + $('.js_cz').val() + "&cz_s=" + $('.js_cz_s').val() + "&mb_money=" + mb_money;
                                        window.location.href = "{{url('page/cz_success?message=支付成功')}}" + data_url;
                                    } else {
                                        data_url = data_url + "&ad_p_id=4";
                                        window.location.href = "{{url('page/pay_errors?message=取消支付')}}" + data_url;
                                    }
                                }
                            );
                        }

                    } else {
                        data_url = data_url + "&ad_p_id=3&no_ad=1";
                        window.location.href = "{{url('page/pay_errors?message=')}}" + data.message + data_url;
                    }

                }, "json");
        } else {
            alert('请选择充值金额')
        }


    })
</script>
</body>
</html>
