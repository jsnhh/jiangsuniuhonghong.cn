<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>添加信息</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
<style>
    .imgbox{
        width: 100px;
        height: 100px;
        background: #FFFFFF;
        border-radius: 4px;
        opacity: 0.23;
        border: 1px solid #979797;
        position: relative;
        cursor: pointer;
    }
    #uploadFile{
        position: absolute;
        left: 0;
        top: 0;
        display: block;
        opacity: .01;
        cursor: pointer;
    }
    .imgImg{
        display: none;
        cursor: pointer;
    }
</style>
</head>
<body>
<div class="layui-fluid" style="background-color: #ffffff;">
    <div class="layui-card" style="margin-top: 0px;overflow:hidden;">
      <div class="layui-card-header">门店信息</div>
      <form class="layui-form" action="" style="margin: 0px auto;width:520px;">
            <div class="layui-form-item" style="width:500px;margin-top: 20px;">
                <div class="layui-input-block" style="line-height: 36px;">
                    <span style="color: red;margin-right: 4px;">*</span>营业执照名称
                </div>
                <div class="layui-input-block">
                    <input type="text" name="terUserName" placeholder="营业执照名称" class="layui-input item3">
                </div>
            </div>
            <div class="layui-form-item" style="width:500px;margin-top: 20px;">
                <div class="layui-input-block" style="line-height: 36px;">
                    <span style="color: red;margin-right: 4px;">*</span>门店简称
                </div>
                <div class="layui-input-block">
                    <input type="text" name="terUserName" placeholder="门店简称" class="layui-input item3">
                </div>
            </div>
            <div class="layui-form-item" style="width:500px;margin-top: 20px;">
                <div class="layui-input-block" style="line-height: 36px;">
                    <span style="color: red;margin-right: 4px;">*</span>门店Logo
                </div>
                <div class="layui-input-block">
                    <div class="imgbox">
                        <img src="" class="imgImg imgImgNone" style="width:100px;height:100px;">
                        <input type="file" name="file" class="uploadFile" id="uploadFile" style="width:100px;height:100px;" /> 
                        <input type="hidden" name="agreement" value="" />
                    </div>
                </div>
            </div>
            <div class="layui-form-item" style="width:500px;margin-top: 20px;">
                <div class="layui-input-block" style="line-height: 36px;">
                    <span style="color: red;margin-right: 4px;">*</span>营业时间
                </div>
                <div class="layui-input-block">
                    <div style="width: 190px;float: left;margin-right: 8px;">
                        <select name="termArea1"  id="termArea1" lay-filter="termArea1">
                            <option value="">请选择地区</option>
                        </select>
                    </div>
                    <div style="width: 190px;float: left;">
                        <select name="termArea1"  id="termArea1" lay-filter="termArea1">
                            <option value="">请选择地区</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="layui-form-item" style="width:500px;margin-top: 20px;">
                <div class="layui-input-block" style="line-height: 36px;">
                    <span style="color: red;margin-right: 4px;">*</span>商户电话
                </div>
                <div class="layui-input-block">
                    <input type="text" name="terUserName" placeholder="商户电话" class="layui-input item3">
                </div>
            </div>
            <div class="layui-form-item" style="width:500px;margin-top: 20px;">
                <div class="layui-input-block" style="line-height: 36px;">
                    <span style="color: red;margin-right: 4px;">*</span>商户邮箱
                </div>
                <div class="layui-input-block">
                    <input type="text" name="terUserName" placeholder="商户邮箱" class="layui-input item3">
                </div>
            </div>
            <div class="layui-form-item" style="width:500px;margin-top: 20px;">
                <div class="layui-input-block" style="line-height: 36px;">
                    <span style="color: red;margin-right: 4px;">*</span>商户官网
                </div>
                <div class="layui-input-block">
                    <input type="text" name="terUserName" placeholder="商户官网" class="layui-input item3">
                </div>
            </div>
            <div class="layui-form-item" style="width:500px;margin-top: 20px;">
                <div class="layui-input-block" style="line-height: 36px;">
                    <span style="color: red;margin-right: 4px;">*</span>详细地址
                </div>
                <div class="layui-input-block">
                    <input type="text" name="terUserName" placeholder="详细地址" class="layui-input item3">
                </div>
            </div>

            <div class="layui-form-item" style="width:390px;margin-left:110px;">
              <button class="layui-btn" lay-submit lay-filter="save" style="background: #1E9FFF;margin-top: 15px;float: left;">保存</button>
            </div>
      </form>
      <br/><br/><br/><br/><br/><br/>
    </div>
</div>

<script src="/layuiadmin/layui/layui.js"></script>
<script>
   //获取 token ，用户登录是就会有
    var token = window.sessionStorage.getItem("Usertoken");
	var storeid = window.location.search;
	var store_id = storeid.substring(1,storeid.length)
	console.log(store_id)
    layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
      index: 'lib/index', //主入口模块
      formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','laydate','jquery'], function(){
      var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;
            var $ = jquery = layui.jquery;












    })




</script>
</body>
</html>
