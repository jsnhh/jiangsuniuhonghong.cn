<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>商户管理系统 - 首页</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">

    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">

    <link rel="icon" href="{{asset('/layuiadmin/layui/images/icon.png')}}">
    <link rel="icon" href="{{asset('/layuiadmin/layui/images/icon.png')}}" type="image/x-icon" />
    <link rel="shortcut icon" href="{{asset('/layuiadmin/layui/images/icon.png')}}">

    <link rel="stylesheet" href="{{asset('/layuiadmin/userweb/iconfont.css')}}" media="all">

    <style>

        .layadmin-side-shrink .layui-layout-admin .layui-logo {
            background-image: none !important;
        }

    </style>
</head>

<body class="layui-layout-body">

    <div id="LAY_app">
        <div class="layui-layout layui-layout-admin">
            <div class="layui-header">
                <!-- 头部区域 -->
                <ul class="layui-nav layui-layout-left">
                    <li class="layui-nav-item" lay-unselect>
                        <a href="javascript:;" layadmin-event="refresh" title="刷新">
                            <i class="layui-icon layui-icon-refresh-3"></i>
                        </a>
                    </li>
                </ul>
                <ul class="layui-nav layui-layout-right" lay-filter="layadmin-layout-right">
                    <li class="layui-nav-item" lay-unselect style="background: url(/mb/toptitleimg.png) no-repeat center;background-size: 100% 100%;height: 32px;line-height: 32px;">
                        <a href="/mb/index"><cite style="color: #ffffff;">商户后台</cite></a>
                    </li>
                    <li class="layui-nav-item layui-hide-xs" lay-unselect>
                        <a href="javascript:;" layadmin-event="fullscreen" class="icondynamic">
                            <i class="layui-icon layui-icon-screen-full"></i>
                        </a>
                    </li>
                    <li class="layui-nav-item layui-hide-xs" lay-unselect>
                        <a href="javascript:;" layadmin-event="" class="">帮助中心</a>
                    </li>
                    <li class="layui-nav-item layui-hide-xs" lay-unselect>
                        <a href="javascript:;" layadmin-event="logout" class="logout"><embed
                                src="{{asset('/layuiadmin/img/tuichudenglu.svg')}}" width="14px" height="14px"
                                type="image/svg+xml" />退出</a>
                    </li>
                    <li class="layui-nav-item layui-show-xs-inline-block layui-hide-sm" lay-unselect>
                        <a href="javascript:;" layadmin-event="more"><i
                                class="layui-icon layui-icon-more-vertical"></i></a>
                    </li>
                </ul>
            </div>

            <!-- 侧边菜单 -->
            <div class="layui-side layui-side-menu" style="background-color: #3475c3 !important;width:200px">
                <div class="layui-side-scroll">
                    <div class="layui-logo" style="background-color: #3475c3 !important;width:200px">
                        <span>微官网管理系统</span>
                    </div>

                    <ul class="layui-nav layui-nav-tree box" lay-shrink="all" id="LAY-system-side-menu"
                        lay-filter="layadmin-system-side-menu" style="width:200px">

                        <li data-name="component" class="layui-nav-item layui-nav-itemed">
                            <a lay-href="{{url('/userweb/home')}}" lay-tips="首页" lay-direction="2">
                                <i class="iconfont layui-icon" style="font-size: 14px;">&#xe695;</i>
                                <cite>首页</cite>
                            </a>
                        </li>
                        <!-- <li data-name="home" class="layui-nav-item">
              <a href="javascript:;" lay-tips="店铺设置" lay-direction="2">
                <i class="iconfont layui-icon" style="font-size: 14px;">&#xe668;</i>
                <cite>店铺设置</cite>
              </a>
              <dl class="layui-nav-child">
                <dd data-name="console">
                  <a lay-href="{{url('/userweb/setupshop')}}">店铺设置</a>
                </dd>
              </dl>
            </li> -->
                        <li data-name="home" class="layui-nav-item">
                            <a href="javascript:;" lay-tips="小程序模板" lay-direction="2">
                                <i class="iconfont layui-icon" style="font-size: 14px;">&#xe69a;</i>
                                <cite>小程序模板</cite>
                            </a>
                            <dl class="layui-nav-child">
                                <dd data-name="console">
                                    <a lay-href="{{url('/userweb/applettemplate')}}">小程序模板</a>
                                </dd>
                            </dl>
                        </li>
                        <li data-name="component" class="layui-nav-item">
                            <a href="javascript:;" lay-tips="商品管理" lay-direction="2">
                                <i class="iconfont layui-icon" style="font-size: 14px;">&#xe693;</i>
                                <cite>页面设计</cite>
                            </a>
                            <dl class="layui-nav-child">
                                <dd data-name="grid">
                                    <a lay-href="{{url('/userweb/smallprogram')}}">小程序首页</a>
                                </dd>
                                <dd data-name="grid">
                                    <a lay-href="{{url('/userweb/Exhibition')}}">展示页面</a>
                                </dd>
                                <dd data-name="grid">
                                    <a lay-href="{{url('/userweb/contactus')}}">联系我们</a>
                                </dd>
                                <dd data-name="grid">
                                    <a lay-href="{{url('/userweb/wechatfabu')}}">发布小程序</a>
                                </dd>
                            </dl>
                        </li>
                        <li data-name="component" class="layui-nav-item">
                            <a href="javascript:;" lay-tips="微信发券" lay-direction="2">
                                <i class="layui-icon iconfont" style="font-size: 14px;">&#xe697;</i>
                                <cite>创建小程序</cite>
                            </a>
                            <dl class="layui-nav-child">
                                <dd data-name="grid">
                                    <a lay-href="{{url('/userweb/createApplet')}}">创建小程序</a>
                                </dd>
                            </dl>
                        </li>
                        <li data-name="component" class="layui-nav-item">
                            <a href="javascript:;" lay-tips="支付宝发券" lay-direction="2">
                                <i class="layui-icon iconfont" style="font-size: 12px;">&#xe698;</i>
                                <cite>意向客户</cite>
                            </a>
                            <dl class="layui-nav-child">
                                <dd data-name="grid">
                                    <a lay-href="{{url('/userweb/interestedbuyers')}}">意向客户</a>
                                </dd>
                            </dl>
                        </li>

                    </ul>
                </div>
            </div>

            <!-- 页面标签 -->
            <div class="layadmin-pagetabs" id="LAY_app_tabs" style="position:fixed;top:50px">
                <div class="layui-icon layadmin-tabs-control layui-icon-prev" layadmin-event="leftPage"></div>
                <div class="layui-icon layadmin-tabs-control layui-icon-next" layadmin-event="rightPage"></div>
                <div class="layui-icon layadmin-tabs-control layui-icon-down">
                    <ul class="layui-nav layadmin-tabs-select" lay-filter="layadmin-pagetabs-nav">
                        <li class="layui-nav-item" lay-unselect>
                            <a href="javascript:;"></a>
                            <dl class="layui-nav-child layui-anim-fadein">
                                <dd layadmin-event="closeThisTabs"><a href="javascript:;">关闭当前标签页</a></dd>
                                <dd layadmin-event="closeOtherTabs"><a href="javascript:;">关闭其它标签页</a></dd>
                                <dd layadmin-event="closeAllTabs"><a href="javascript:;">关闭全部标签页</a></dd>
                            </dl>
                        </li>
                    </ul>
                </div>
                <div class="layui-tab" lay-unauto lay-allowClose="true" lay-filter="layadmin-layout-tabs">
                    <ul class="layui-tab-title" id="LAY_app_tabsheader">
                        <li lay-id="{{url('/userweb/home')}}" lay-attr="{{url('/userweb/home')}}" class="layui-this">首页</li>
                    </ul>
                </div>
            </div>

            <!-- 主体内容 -->
            <div class="layui-body" id="LAY_app_body">
                <div class="layadmin-tabsbody-item layui-show">
                    <iframe src="{{url('/userweb/home')}}" frameborder="0" class="layadmin-iframe"></iframe>
                </div>
            </div>

            <!-- 辅助元素，一般用于移动设备下遮罩 -->
            <div class="layadmin-body-shade" layadmin-event="shade"></div>
        </div>
    </div>

    <script type="text/javascript" src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
    <script type="text/javascript" src="{{asset('/phone/js/jquery-2.1.4.js')}}"></script>
    <script>
        layui.config({
            base: '../../layuiadmin/' //静态资源所在路径
        }).extend({
            index: 'lib/index' //主入口模块
        }).use(['index', 'element'], function() {
            var $ = layui.$;

            var token = sessionStorage.getItem("Publictoken");
            // console.log(token);
            var level = sessionStorage.getItem("mb_level");

            $('.logout').click(function() {
                sessionStorage.removeItem("token");
                sessionStorage.clear();
                window.location.reload();
            });

            // 未登录,跳转登录页面
            $(document).ready(function() {
                var token = sessionStorage.getItem("Publictoken");
                var admin = sessionStorage.getItem("admin");
                // var level = sessionStorage.getItem("level");
                if (token == null) {
                    window.location.href = "{{url('/mb/login')}}";
                }
            });

            var permissions = sessionStorage.getItem("mb_permissions");
            var str = JSON.parse(permissions);
            var arr = [];
            for (var i = 0; i < str.length; i++) {
                var aa = str[i].name;
                arr.push(aa);
            }

            //      if(level != 0){
            //          // 权限管理+++++++++++++
            //          $('ul.box li').each(function(index,item){
            ////             console.log($(this).attr('data'));
            ////             console.log(arr);
            ////             if($.inArray( $(this).attr('data'), arr ) == -1){
            ////                 $(this).hide();
            ////             }
            //          });
            //
            //          $('ul.box li dl dd').each(function(index,item){
            ////             console.log($(this).find('a').attr('data'));
            ////             console.log($.inArray($(this).find('a').attr('data'),arr));
            //              if($.inArray($(this).find('a').attr('data'),arr)==-1){
            //                  $(this).find('a').hide();
            //              }
            //          })
            //      }

        });
    </script>
</body>

</html>