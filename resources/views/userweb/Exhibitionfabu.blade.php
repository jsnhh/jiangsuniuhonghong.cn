<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>模板设置</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
        .outer3 {
            z-index: 50;
            background-color: rgba(255, 255, 255, 0);
            margin-top: 1px;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
            width: 388px;
            margin: 0 auto;
        }

        .outers3 {
            height: 960px;
            background-color: rgba(255, 255, 255, 0);
            margin-top: 1px;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }
        .layer4 {
            z-index: auto;
            height: 224px;
            flex-direction: row;
            display: flex;
        }

        .group10 {
            z-index: 237;
            height: 32px;
            border-radius: 2px;
            border-color: rgba(0, 0, 0, 0.15);
            border-width: 1px;
            border-style: solid;
            background-color: rgba(255, 255, 255, 1);
            align-self: center;
            margin-top: 12px;
            width: 332px;
            justify-content: center;
            align-items: center;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .group2 {
            z-index: auto;
            width: 375px;
            height: 224px;
            display: flex;
            flex-direction: column;
        }

        .box1 {
            z-index: 43;
            height: 64px;
            background-color: rgba(255, 255, 255, 1);
            align-self: center;
            width: 375px;
            justify-content: flex-start;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .main3 {
            z-index: auto;
            width: 375px;
            height: 56px;
            display: flex;
            flex-direction: column;
        }

        .section10 {
            z-index: 57;
            height: 20px;
            background-color: rgba(0, 0, 0, 0);
            align-self: center;
            width: 375px;
            justify-content: center;
            align-items: center;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .section11 {
            z-index: auto;
            width: 364px;
            height: 15px;
            flex-direction: row;
            display: flex;
        }

        .layer5 {
            z-index: 67;
            width: 17px;
            height: 10px;
            /* background-image: url(https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPngb6b55652f2f08145f1d49572d13078e588bb8335fb529b5d2ab0fadf0c8b5e4f); */
            background-repeat: no-repeat;
            background-size: 100%;
            margin-top: 2px;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .word5 {
            z-index: 66;
            width: 37px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(3, 3, 3, 1);
            font-size: 12px;
            font-family: Helvetica;
            line-height: 14px;
            margin-top: 1px;
            margin-left: 3px;
        }

        .layer6 {
            z-index: 72;
            width: 15px;
            height: 10px;
            /* background-image: url(https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPngd9803e4995c635e3e03c5ce32c50dd713fb4d97a0f52570dc4874bd116619928); */
            background-repeat: no-repeat;
            background-position: -0.32956276465120027px 0px;
            margin-top: 2px;
            margin-left: 9px;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .txt4 {
            z-index: 64;
            width: 45px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(0, 0, 0, 1);
            font-size: 12px;
            font-family: Helvetica;
            line-height: 14px;
            text-align: center;
            margin-left: 78px;
        }

        .info4 {
            z-index: 63;
            width: 38px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(0, 0, 0, 1);
            font-size: 12px;
            font-family: Helvetica;
            line-height: 14px;
            text-align: right;
            margin-left: 92px;
        }

        .label2 {
            z-index: 59;
            width: 27px;
            height: 12px;
            margin-top: 2px;
            margin-left: 3px;
        }

        .section12 {
            z-index: auto;
            width: 199px;
            height: 32px;
            margin-left: 169px;
            margin-top: 4px;
            flex-direction: row;
            display: flex;
            justify-content: space-between;
        }

        .info5 {
            z-index: 54;
            width: 37px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(0, 0, 0, 1);
            font-size: 18px;
            font-family: PingFangSC-Medium;
            line-height: 26px;
            text-align: center;
            margin-top: 5px;
        }

        .group3 {
            z-index: 44;
            height: 32px;
            border-radius: 16px;
            border-color: rgba(0, 0, 0, 0.08);
            border-width: 0.5px;
            border-style: solid;
            background-color: rgba(255, 255, 255, 0.6);
            width: 87px;
            justify-content: center;
            align-items: center;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .section13 {
            z-index: 45;
            width: 61px;
            height: 20px;
            /* background-image: url(https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng67a3ac6bc64b8325584d100bc411579f178333a7d9bb31b3d0c105cb9a96b79c); */
            background-repeat: no-repeat;
            background-position: -0.5px -0.5px;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .filebox {
            z-index: 143;
            width: 375px;
            height: 160px;
            background-color: rgba(255, 255, 255, 1);
            align-self: center;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .pic1 {
            z-index: 144;
            width: 375px;
            height: 160px;
        }

        .layer7 {
            z-index: auto;
            width: 375px;
            height: 464px;
            margin-top: 8px;
            flex-direction: row;
            display: flex;
        }

        .section14 {
            z-index: 76;
            height: 464px;
            width:375px;
            justify-content: flex-end;
            align-items: center;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
            background-color: #fff;
        }
        .section14::-webkit-scrollbar-track-piece{

        }
        .section1 {
            z-index: 48;
            background-color:#FFF;
            width: 375px;
            justify-content: flex-start;
            padding-top: 30px;
            align-items: center;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
            padding-bottom: 14px;
        }

        .main4 {
            z-index: auto;
            display: flex;
            width: 375px;
            flex-direction: column;
            height: 100%;
            overflow-y: scroll;
            position: relative;
            left: 20px;
            margin-top: 10px;
            padding-bottom: 10px;
        }


        .word33 {
            z-index: 167;
            width: 28px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(255, 255, 255, 1);
            font-size: 14px;
            line-height: 22px;
            text-align: center;
        }

        .word6 {
            z-index: 110;
            width: 30px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(51, 51, 51, 1);
            font-size: 14px;
            line-height: 21px;
            text-align: left;
        }

        .info6 {
            z-index: 111;
            width: 52px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(153, 153, 153, 1);
            font-size: 13px;
            line-height: 18px;
            text-align: left;
            margin-top: 2px;
        }

        .txt5 {
            z-index: 112;
            width: 52px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(153, 153, 153, 1);
            font-size: 13px;
            line-height: 18px;
            text-align: left;
            margin-top: 2px;
        }

        .bd3 {
            z-index: 108;
            width: 345px;
            height: 1px;
            border-color: rgba(238, 238, 238, 1);
            border-width: 0.5px;
            border-style: solid;
            align-self: center;
            margin-top: 12px;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .bd4 {
            z-index: auto;
            width: 337px;
            height: 80px;
            margin-top: 12px;
            flex-direction: row;
            display: flex;
            justify-content: space-between;
            position: relative;
        }

        .bd5 {
            z-index: 82;
            position: relative;
            width: 80px;
            height: 80px;
            border-radius: 4px;
            overflow: hidden;
            /* background-image: url(https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng2b8c5ed91a6867c21c02f3dd6ed81235ff52b9e31951b76cc08270f7efad74f9); */
            background-repeat: no-repeat;
            background-position: 0px -0.5px;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .pic2 {
            z-index: 83;
            position: absolute;
            left: 0;
            top: -1px;
            width: 80px;
            height: 81px;
        }

        .bd6 {
            z-index: auto;
            width: 189px;
            height: 80px;
        }

        .word7 {
            z-index: 80;
            width: 189px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(51, 51, 51, 1);
            font-size: 15px;
            line-height: 21px;
            text-align: left;
            align-self: center;
        }

        .info7 {
            z-index: 79;
            width: 183px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(153, 153, 153, 1);
            font-size: 13px;
            line-height: 18px;
            text-align: left;
            align-self: flex-start;
            margin-top: 4px;
        }

        .txt6 {
            z-index: 78;
            display: block;
            overflow-wrap: break-word;
            color: rgba(255, 59, 48, 1);
            font-size: 18px;
            line-height: 25px;
            text-align: left;
        }

        .bd7 {
            z-index: 84;
            width: 345px;
            height: 1px;
            border-color: rgba(238, 238, 238, 1);
            border-width: 0.5px;
            border-style: solid;
            align-self: center;
            margin-top: 12px;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .bd8 {
            z-index: auto;
            width: 336px;
            height: 80px;
            margin-top: 11px;
            flex-direction: row;
            display: flex;
        }

        .layer8 {
            z-index: 98;
            position: relative;
            width: 80px;
            height: 80px;
            border-radius: 4px;
            overflow: hidden;
            /* background-image: url(https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng2f4dfb5cc73cad9978ad7a08f1776b07da150c178515d39c3a8ad8a2fcc6b184); */
            background-repeat: no-repeat;
            background-size: 100%;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .pic3 {
            z-index: 99;
            position: absolute;
            left: 0;
            top: 0;
            width: 80px;
            height: 80px;
        }

        .layer9 {
            z-index: auto;
            width: 189px;
            height: 80px;
            margin-left: 12px;
            display: flex;
            flex-direction: column;
        }

        .info8 {
            z-index: 96;
            width: 189px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(51, 51, 51, 1);
            font-size: 15px;
            line-height: 21px;
            text-align: left;
            align-self: center;
        }

        .word8 {
            z-index: 95;
            width: 183px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(153, 153, 153, 1);
            font-size: 13px;
            line-height: 18px;
            text-align: left;
            align-self: flex-start;
            margin-top: 4px;
        }

        .word9 {
            z-index: 94;
            width: 33px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(255, 59, 48, 1);
            font-size: 18px;
            line-height: 25px;
            text-align: right;
            margin-left: 22px;
        }

        .bd9 {
            z-index: 100;
            width: 345px;
            height: 1px;
            border-color: rgba(238, 238, 238, 1);
            border-width: 0.5px;
            border-style: solid;
            align-self: center;
            margin-top: 12px;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .bd10 {
            z-index: auto;
            width: 337px;
            height: 80px;
            margin-top: 12px;
            flex-direction: row;
            display: flex;
            justify-content: space-between;
        }

        .bd11 {
            z-index: 146;
            position: relative;
            width: 80px;
            height: 80px;
            border-radius: 4px;
            overflow: hidden;
            /* background-image: url(https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng2b8c5ed91a6867c21c02f3dd6ed81235ff52b9e31951b76cc08270f7efad74f9); */
            background-repeat: no-repeat;
            background-position: 0px -0.5px;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .box3 {
            z-index: 147;
            height: 81px;
            /* background-image: url(https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng93ee484df5d1f68dc56843d7d4980b0532c563ca833a69efdf96635af87f3116); */
            background-repeat: no-repeat;
            background-size: 100%;
            width: 80px;
            justify-content: flex-start;
            position: absolute;
            left: 0;
            top: -1px;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .pic4 {
            z-index: 148;
            width: 80px;
            height: 44px;
        }

        .bd12 {
            z-index: auto;
            width: 189px;
            height: 80px;
            display: flex;
            flex-direction: column;
        }

        .word10 {
            z-index: 88;
            width: 189px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(51, 51, 51, 1);
            font-size: 15px;
            line-height: 21px;
            text-align: left;
            align-self: center;
        }

        .info9 {
            z-index: 87;
            width: 183px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(153, 153, 153, 1);
            font-size: 13px;
            line-height: 18px;
            text-align: left;
            align-self: flex-start;
            margin-top: 4px;
        }

        .word11 {
            z-index: 86;
            width: 44px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(255, 59, 48, 1);
            font-size: 18px;
            line-height: 25px;
            text-align: left;
        }

        .bd19 {
            z-index: 134;
            width: 22px;
            height: 22px;
            /* background-image: url(https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPngfbe827d16f1118ab6402390ba750ee26f46e947351049f939f424efff7e5b656); */
            background-repeat: no-repeat;
            background-position: -0.5px 0px;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .main5 {
            z-index: 149;
            width: 345px;
            height: 1px;
            border-color: rgba(238, 238, 238, 1);
            border-width: 0.5px;
            border-style: solid;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .bd14 {
            z-index: auto;
            width: 336px;
            height: 80px;
            margin-top: 11px;
            flex-direction: row;
            display: flex;
        }

        .bd15 {
            z-index: 105;
            position: relative;
            width: 80px;
            height: 80px;
            border-radius: 4px;
            overflow: hidden;
            /* background-image: url(https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng2f4dfb5cc73cad9978ad7a08f1776b07da150c178515d39c3a8ad8a2fcc6b184); */
            background-repeat: no-repeat;
            background-size: 100%;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .pic5 {
            z-index: 106;
            position: absolute;
            left: 0;
            top: 0;
            width: 80px;
            height: 80px;
        }

        .bd16 {
            z-index: auto;
            width: 189px;
            height: 80px;
            margin-left: 12px;
            display: flex;
            flex-direction: column;
        }

        .word12 {
            z-index: 103;
            width: 189px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(51, 51, 51, 1);
            font-size: 15px;
            line-height: 21px;
            text-align: left;
            align-self: center;
        }

        .txt7 {
            z-index: 102;
            width: 183px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(153, 153, 153, 1);
            font-size: 13px;
            line-height: 18px;
            text-align: left;
            align-self: flex-start;
            margin-top: 4px;
        }

        .txt8 {
            z-index: 101;
            width: 33px;
            display: block;
            overflow-wrap: break-word;
            color: rgba(255, 59, 48, 1);
            font-size: 18px;
            line-height: 25px;
            text-align: right;
            margin-left: 22px;
        }

        .bd17 {
            z-index: 107;
            width: 345px;
            height: 1px;
            border-color: rgba(238, 238, 238, 1);
            border-width: 0.5px;
            border-style: solid;
            align-self: center;
            margin-top: 12px;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .layer10 {
            z-index: auto;
            width: 375px;
            height: 49px;
            margin-top: 8px;
            flex-direction: row;
            display: flex;
        }

        .box4 {
            z-index: 115;
            height: 49px;
            background-color: rgba(255, 255, 255, 1);
            width: 375px;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .layer11 {
            z-index: auto;
            margin-top: 6px;
            width: 375px;
            display: flex;
            justify-content: space-around;
        }

        .outer4 {
            z-index: 116;
            width: 375px;
            height: 1px;
            background-color: rgba(0, 0, 0, 0.1);
            align-self: center;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .outer5 {
            z-index: auto;
            width: 228px;
            height: 22px;
            margin-left: 74px;
            margin-top: 6px;
            flex-direction: row;
            display: flex;
            justify-content: space-between;
        }

        .bd18 {
            z-index: 125;
            width: 22px;
            height: 22px;
            /* background-image: url(https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPnga1025489f6adaf6bd1345a6e667c19aa5d9ae1643abe37ad1c44f3ef6cedd40c); */
            background-repeat: no-repeat;
            background-position: -0.5px 0px;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .bd19 {
            z-index: 134;
            width: 22px;
            height: 22px;
            /* background-image: url(https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPngfbe827d16f1118ab6402390ba750ee26f46e947351049f939f424efff7e5b656); */
            background-repeat: no-repeat;
            background-position: -0.5px 0px;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .bd20 {
            z-index: 120;
            width: 22px;
            height: 22px;
            /* background-image: url(https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPngf8827966eadea88fd23542198abdb12a730ceb759a069106c39d7e9b74d35835); */
            background-repeat: no-repeat;
            background-position: -0.5px 0px;
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
        }

        .outer6 {
            z-index: auto;
            width: 237px;
            height: 14px;
            margin-left: 74px;
            margin-top: 6px;
            flex-direction: row;
            display: flex;
        }

        .txt9 {
            z-index: 119;
            display: block;
            overflow-wrap: break-word;
            color: rgba(153, 153, 153, 1);
            font-size: 10px;
            letter-spacing: 0.15000000596046448px;
            line-height: 14px;
            text-align: center;
        }

        .txt10 {
            z-index: 118;
            display: block;
            overflow-wrap: break-word;
            color: rgba(0, 124, 255, 1);
            font-size: 10px;
            line-height: 14px;
            text-align: center;
            margin-left: 83px;
        }

        .info10 {
            z-index: 117;
            display: block;
            overflow-wrap: break-word;
            color: rgba(127, 131, 137, 1);
            font-size: 10px;
            line-height: 14px;
            text-align: center;
            margin-left: 73px;
        }

        .bd2 {
            z-index: auto;
            height: 21px;
            flex-direction: row;
            display: flex;
        }

        .layer-top {
            margin-top: 10px;
            z-index: auto;
            width: 360px;
            display: flex;
            justify-content: space-around;
        }

        .zhanshi {
            margin-left: 89px;
        }

        .item1 {
            border: none;
            height: 21px;
            font-size: 15px;
            font-weight: 500;
            color: #333333;
            line-height: 21px;
            width: 100%;
            box-sizing: border-box;
            text-align: left;
        }

        .item2 {
            min-height: 65px !important;
            border: none;
            resize: none;
        }
  .banner {
    position: relative;
}
 .header .headerimg{
    display: block;
    width: 100%;
}
.banner .img1{
    display: block;
    width: 32px;
    height: 32px;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%,-50%);
}
.banner img{
    display: block;
    width: 100%;
    cursor: pointer;
}
input[name="file"]{
    opacity: 0;
}

#bannerfile{

    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    cursor: pointer;

}
input[type="file"]{
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    cursor: pointer;
    z-index: 99999;
}
.inputtitlelist{
    width: 100%;
    box-sizing: border-box;
    text-align: left;
    font-size: 16px;
    font-weight: 500;
    color: #333333;
    border: none;
    margin-left: 10px;
}
.minititle{
    width: 100%;
    box-sizing: border-box;
    text-align: left;
    font-size: 13px;
    font-weight: 500;
    color: #999;
    border: none;
    margin-left: 10px;
}
.inputtitlethree{
    width: 164px;
    margin: 2px auto;
    height: 60px;
    padding-left: 0;
    font-size: 14px;
    font-weight: 400;
    color: #999999;
    line-height: 18px;background: none;
    resize:none;
    border: none;
    text-align: justify;
  }

  .inputtitlethree2{
    width: 334px;
    margin: 0 auto;
    height: 272px;
    padding-left: 0;
    font-size: 14px;
    font-family: PingFangSC-Regular, PingFang SC;
    font-weight: 400;
    color: #333;
    line-height: 18px;background: none;
    resize:none;
    border: none;
    text-indent:-7em;
    text-align: justify;
  }
.details{
    background: #1E9FFF;
    margin-top: 15px;
    float: right;
    width: 60px;
    text-align: center;
    height: 25px;
    line-height: 25px;
    border-radius: 5px;
    color: #FFF;
    cursor:pointer;
}
.newadd{
    background: #1E9FFF;
    margin-top: 15px;
    float: right;
    width: 60px;
    text-align: center;
    height: 25px;
    line-height: 25px;
    border-radius: 5px;
    color: #FFF;
    cursor:pointer;
}
.mod1 {
    z-index: auto;
    width: 345px;
    display: flex;
    flex-direction: column;
  }
  .box2 {
    z-index: 97;
    overflow-wrap: break-word;
    text-align: justify;
    align-self: center;
    box-sizing: border-box;
    font-size: 14px;
    padding-top: 12px;
  }

  .zyywaddbtn{
  background-color: #FF5722;
  font-size: 16px;
  color: #FFFFFF;
  text-align: center;
  line-height: 40px;
  height: 40px;
  width: 100px;
  margin: 40px auto 10px;
  cursor: pointer;
  border-radius: 4px;
}
    .delect{
        position: absolute;
        right: 0;
        bottom: 2px;
        background: red;
        width: 60px;
        text-align: center;
        height: 25px;
        line-height: 25px;
        border-radius: 5px;
        color: #FFF;
        cursor: pointer;
        display: none;

    }
    .bd4:hover .delect{
        display: block;
    }
    .cancelfun{
        position: absolute;
        right: 0;
        bottom: 2px;
        background: red;
        width: 60px;
        text-align: center;
        height: 25px;
        line-height: 25px;
        border-radius: 5px;
        color: #FFF;
        cursor: pointer;
        display: none;
    }
    .bd4:hover .cancelfun{
        display: block;
    }
    .listbox .rightbox{
        position: absolute;
        right: 0;
        top: 0;
    }
    .objlistadd{

        background-color: #FF5722;
        color: #ffffff;
        padding:0 4px ;
        line-height: 21px;
        height: 21px;
        font-size: 12px;
        margin-left: 10px;
        cursor: pointer;

    }
    .objlistinput input{
        margin-right: 10px;
        width:58px;border:none;line-height: 21px;color:#666666;
    }
    .objlistinput{
        position: relative;
    }
    .biaoqiandelect{
        position: absolute;
        left: 0;
        top: 20px;
        background-color: red;
        color: #ffffff;
        font-size: 12px;
        padding:2px 4px;
        cursor: pointer;
        z-index: 999;
        display: none;
    }
    .objlistinput:hover .biaoqiandelect{
        display: block;
    }
    </style>
</head>

<body>

    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
           <form action="" class="layui-form">
               <div>
               <div class="outer3">
                <div class="layer4">
                    <div class="group1"></div>
                    <div class="group2">
                        <div class="box1">
                            <div class="main3">
                                <div class="section10">
                                    <div class="section11">
                                        <div class="layer5"></div>
                                        <span class="word5">Sketch</span>
                                        <div class="layer6"></div>
                                        <span class="txt4">1:21&nbsp;AM</span>
                                        <span class="info4">100%&nbsp;</span>
                                    </div>
                                </div>
                                <div class="section12">
                                    <span class="info5">展示</span>
                                </div>
                            </div>
                        </div>
                        <div class="banner">
                            <!-- 始终显示第一个banner -->
                            <img id="banner" src="{{asset('/userweb/smallprogramimg/hader.png')}}" >
                        </div>
                    </div>
                </div>
                <div class="layer7">
                    <div class="section14" style="overflow: hidden;">
                        <div class="main4">
                            <div class="bd2">
                                <span class="word6">全部</span>
                                <div class="objlist" style="display: flex;">
                                </div>
                            </div>
                            <div class="bd3"></div>

                            <div class="listbox">
                                <ul>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layer10">
                    <div class="box4">
                    <img class="headerimg" src="{{asset('/userweb/smallprogramimg/tapbar.png')}}" alt="">
                    </div>
                </div>
            </div>
           </form>
        </div>
    </div>

    <input type="hidden" class="store_id">
    <input type="hidden" class="js_tpl_sku">
    <input type="hidden" class="js_tpl_bck" value="1">

    <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
    <script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script>
    <script src="{{asset('/layuiadmin/layui/inputTags/inputTags.js')}}"></script>
    <script>
        var token = sessionStorage.getItem("Publictoken");
        var str = location.search;
        var store_id = sessionStorage.getItem("store_id");

        layui.config({
            base: '../../layuiadmin/' //静态资源所在路径
        }).extend({
            index: 'lib/index', //主入口模块
            formSelects: 'formSelects'
        }).use(['index', 'form', 'upload', 'formSelects', 'laydate', 'jquery'], function() {

            var $ = layui.$,
                admin = layui.admin,
                element = layui.element,
                layer = layui.layer,
                laydate = layui.laydate,
                form = layui.form,
                upload = layui.upload,
                formSelects = layui.formSelects;
            var $ = jquery = layui.jquery;

            /* 为了防止 list class 有重复的 定义一个全局 数据 ; 从 1 开始 */
            var classobjnum = 1;


            /* 接口 请求 */
            const getdatalist = (url,data)=>{
                return new Promise(resolve=>{
                    $.post(url,data,res=>{
                        resolve(res)
                    })
                })
            }

            /* 一进来就会请求的数据 */
            (()=>{

                /* 列表数据回显 */
                getlistdatafun();
                getlistbiaoqianfun();

            })()

            /* 标签回显 */
            async function getlistbiaoqianfun(){
                const biaoqiandata = await getdatalist("/api/miniweb/getMiniWebTag",{
                    token:token,
                    storeId:store_id,
                    p:1,
                    l:10,
                    status:1
                })
                if(biaoqiandata.status == 1){
                    
                    let biaoqianres = biaoqiandata.data;
                    let listbiaoqianlength = biaoqianres.length;
                    $(".objlist").html("")
                    console.log(biaoqianres)
                    let str = "";
                    for(let i=0;i<biaoqianres.length;i++){
                        str+=`
                            <div class="objlistinput">
                                <input  style="background-color: #ffffff;" type="text" disabled data-id="`+biaoqianres[i].id+`" name="listtitle" value="`+biaoqianres[i].name+`">
                            </div>
                        `
                    }
                    $(".objlist").append(str);
                }else{
                    layer.msg(biaoqiandata.message, {icon:2, shade:0.5, time:2000});
                }
            }

            /* 即是开始 也是结束 */
            async function getlistdatafun(){

                const getlistdata = await getdatalist("/api/miniweb/getMiniWebShowList",{
                    token:token,
                    storeId:store_id,
                    p:1,
                    l:10,
                    status:1
                })

                if(getlistdata.status == 1){
                    $(".main4 .listbox ul").html("");
                    let listdata = getlistdata.data;
                    if(listdata.length > 5){
                        $(".zyywaddbtn").css({"display":"none"})
                    }
                    let str = "";
                    for(let i = 0 ; i < listdata.length ; i++){

                        if(i == 0){
                            layui.jquery('#banner').attr("src", listdata[i].banner_url);
                        }

                        str += `
                                <li class="addbtnclass">
                                    <div class="bd4" style="border-bottom: 1px solid #EEEEEE;padding-bottom: 15px;">
                                        <div class="bd5">
                                            <img src="`+listdata[i].cover_url+`" class="imgImg`+classobjnum+` imgImgNone" id="imgImgid`+classobjnum+`">
                                        </div>
                                        <div class="bd6">
                                            <div class="layui">
                                                <input style="background-color: #ffffff;" class="inputtitlelist" disabled type="text" id="title" name="title" value="`+listdata[i].title+`" class="layui-input">
                                            </div>
                                                <textarea disabled onkeyup="if(this.value.length>30){this.value=this.value.slice(0,30)}" id="inputtitlethree" type="text" id="subTitle" name="subTitle" value="" class="layui-input inputtitlethree">`+listdata[i].sub_title+`</textarea>
                                        </div>
                                        <div>
                                            <div style="display:flex;width:70px;">
                                                <span class="txt6">¥</span>
                                                <input class="item1" disabled type="number" name="price" value="`+listdata[i].price+`" style="color: red;">
                                            </div>
                                        </div>
                                    </div>
                                </li>
                        `
                        classobjnum ++;
                    }
                    $(".main4 .listbox ul").append(str);
                }else{
                    layer.msg(getlistdata.message, {icon:2, shade:0.5, time:2000});
                }
            }

           


        })
    </script>

</body>

</html>