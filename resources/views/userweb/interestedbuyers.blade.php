<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>支付宝代金券</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/inputTags/inputTags.css')}}">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/formSelects-v4.css')}}" media="all">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .tongbu {
            background-color: #4c9ef8;
            color: #fff;
        }

        .cur {
            color: #009688;
        }

        .yname {
            font-size: 13px;
            color: #444;
        }

        .xgrate {
            color: #fff;
            font-size: 15px;
            padding: 7px;
            height: 30px;
            line-height: 30px;
            background-color: #3475c3;
        }

        .input_block {
            display: block;
        }
    </style>
</head>

<body>
    <div class="layui-fluid" style="margin-top:50px;">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">

                <div class="layui-fluid">
                    <div class="layui-row layui-col-space15">
                        <div class="layui-col-md12">
                            <div class="layui-card">
                                <div class="layui-card-header">意向客户</div>

                                <div class="layui-card-body">
                                    <div class="layui-btn-container" style="font-size:14px;">
                                        <!-- 缴费时间 -->
                                        <div class="layui-form" style="width:100%;display:flex;">
                                            <!-- 选择业务员 -->
                                            <div class="layui-form" lay-filter="component-form-group" style="margin-right:10px;display: inline-block;display:flex;width:1375px;flex-wrap:wrap;">
                                                <div class="layui-form-item">
                                                    <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">客户姓名</text>
                                                        <input type="text" name="capital" placeholder="请输入客户姓名" class="layui-input item3">
                                                    </div>
                                                </div>

                                                <div class="layui-form-item">
                                                    <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">客户手机号</text>
                                                        <input name="distributionstatus" id="distributionstatus" placeholder="请输入手机号" class="layui-input item3">
                                                    </div>
                                                </div>
                                                <div class="layui-form-item">
                                                    <div class="layui-input-block" style="margin-left:10px;width:300px;margin-right:0px;">
                                                        <text class="yname">意向度</text>
                                                        <select name="vouchername" id="vouchername" lay-filter="vouchername" lay-search>
                                                            <option value="">请选择意向度</option>
                                                            <option value="1">0～25%</option>
                                                            <option value="2">25%～50%</option>
                                                            <option value="3">50%～75%</option>
                                                            <option value="4">75%～100%</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div style="display:flex;align-items: center;margin:20px 10px 0px;">
                                                    <div class="layui-inline">
                                                        <button class="layui-btn layuiadmin-btn-list" lay-submit=""  lay-filter="LAY-app-contlist-search"  style="border-radius:5px;height:36px;line-height: 36px;">
                                                            <i  class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- <script type="text/html" id="table-content-list">
        <a class="layui-btn layui-btn-normal layui-btn-xs alipaydetails" lay-event="alipaydetails">备注</a>
        <a class="layui-btn layui-btn-normal  layui-btn-xs storecode" lay-event="storecode">修改备注</a>
    </script> -->

    <input type="hidden" class="classname">


    <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
    <script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script>
    <script src="{{asset('/layuiadmin/layui/inputTags/inputTags.js')}}"></script>
    <script src="{{asset('/layuiadmin/layui/jquery.qrcode.min.js')}}"></script>
    <script>
        var token = sessionStorage.getItem("Publictoken");
        var str = location.search;
        var store_id = sessionStorage.getItem("store_id");

        layui.config({
            base: '../../layuiadmin/', //静态资源所在路径
        }).extend({
            index: 'lib/index',//主入口模块
            formSelects: 'formSelects'
        }).use(['index', 'form', 'table', 'laydate', "upload",'formSelects'], function() {
            var $ = layui.$,
                admin = layui.admin,
                form = layui.form,
                table = layui.table,
                laydate = layui.laydate,
                upload = layui.upload,
                formSelects = layui.formSelects;
                formSelects.render('range');
                formSelects.btns('range', []);

            // 未登录,跳转登录页面
            $(document).ready(function() {
                if (token == null) {
                    window.location.href = "{{url('/mb/login')}}";
                }
            });

            // 渲染表格
            table.render({
                elem: '#test-table-page',
                url: "{{url('/api/miniweb/getIntentionality')}}", //接口需要替换
                method: 'post',
                where: {
                    token: token,
                    storeId: store_id,
                },
                request: {
                    pageName: 'p',
                    limitName: 'l'
                },
                page: true,
                cellMinWidth: 150,
                cols: [
                    [{align: 'center',field: 'wechat_username',title: '客户姓名'},
                    {align: 'center',field: 'wechat_mobile',title: '手机号'},
                    {align: 'center',field: 'intentionality',title: '意向度'},
                    {align: 'center',field: 'times',title: '访问次数'},
                    {align: 'center',field: 'long_times',title: '最长访问时长(秒)'},
                    {align: 'center',field: 'short_times',title: '最近访问时长(秒)'},
                    {align: 'center',field: 'updated_at',title: '最后访问日期'},
                    {align: 'center',field: 'remarks', edit: 'text',title: '备注'}
                    // {align: 'center',toolbar: '#table-content-list',title: '操作'}
                    ]
                ],
                response: {
                     statusName: 'status' //数据状态的字段名称，默认：code
                    ,statusCode: 1 //成功的状态码，默认：0
                    ,msgName: 'message' //状态信息的字段名称，默认：msg
                    ,countName: 't' //数据总数的字段名称，默认：count
                    ,dataName: 'data' //数据列表的字段名称，默认：data
                },
                done: function(res, curr, count) {
                    $('th').css({'font-weight': 'bold','font-size': '15','color': 'black','background':' #FAFAFA'}); //进行表头样式设置
                }
            });

           //备注
            table.on('edit(test-table-page)', function(obj){
                var value = obj.value //得到修改后的值
                ,data = obj.data //得到所在行所有键值
                ,field = obj.field; //得到字段
                layui.use('jquery',function(){
                    var $=layui.$;
                    $.ajax({
                        type: 'POST',
                        url: "{{url('/api/miniweb/remarksUp')}}",
                        data: {
                            token: token,
                            storeId: store_id,
                            id: data.id,
                            remarks: value,
                        },
                        success: function(res) {
                            console.log(res)
                            if (res.status == 200) {
                                layer.msg('更新成功', {
                                    offset: '50px',
                                    icon: 1
                                }, function() {
                                    window.location.reload();

                                });
                            } else {
                                layer.msg(res.msg, {
                                    offset: '50px',
                                    icon: 1
                                }, function() {
                                    window.location.reload();
                                });
                            }
                        }
                    });
                });
            });

            // 选择意向度
            form.on('select(vouchername)', function(data){
                var vouchername = data.value;
                $('.vouchername').val(vouchername);
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        intentionality:$('.vouchername').val()
                    }
                    ,page: {
                        curr: 1
                    }
                });
            });

            /*
             *查询
             */
            form.on('submit(LAY-app-contlist-search)', function(data) {
                var capital = data.field.capital;
                var distributionstatus = data.field.distributionstatus;
                var vouchername = data.field.vouchername;
                console.log(data);
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        wechat_username: capital,
                        wechat_mobile: distributionstatus,
                        vouchername:vouchername
                    }
                });
            });

        });
    </script>
</body>

</html>