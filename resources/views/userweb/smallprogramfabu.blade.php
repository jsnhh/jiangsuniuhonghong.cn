<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>小程序首页</title>
    

    <link rel="stylesheet" href="{{asset('/userweb/layuinew/layui.css')}}">
    <!-- css -->
    <link rel="stylesheet" href="{{asset('/userweb/smallprogram.css')}}">

</head> 
<body>


    <div class="centent" style="padding: 0;">
        <div class="smallprogram">
          <form action="" class="layui-form">
            <div class="header">
                <img class="headerimg" src="{{asset('/userweb/smallprogramimg/header.png')}}" alt="">
            </div>
            <div class="banner">

                <div class="bannerbox ">
                  <div class="layui-progress layuiprogressBar layuiprogressBarnone" lay-filter="progressBar">
                    <div class="layui-progress-bar layui-bg-blue" lay-percent="0%"></div>
                  </div>

                  <div class="videobox videonone">
                    <video id="video" src="/userweb/smallprogramimg/bannerimg.mp4">
                      <source src="myvideo.mp4" type="video/mp4">
                    </video>
                  </div>
                  
                  <img class="bannerfile" id="banner" src="{{asset('/userweb/smallprogramimg/banner.png')}}" alt="">
                  <div class="click">
                          <img
                          class="img1"
                          referrerpolicy="no-referrer"
                          src="https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng0c79604f2d7ee61e393ec23a8ece4e37cb811817807603d2a30968a57c36d0bf"
                          />
                  </div>
                </div>

            </div>
            <div class="row-one">
                <input id="inputtitle" type="text" disabled name="pagetitle" value="主营业务" class="layui-input">
                <input id="inputtitletwo" type="text" disabled name="pagesubTitle" value="Main business" class="layui-input">
            </div>
            <div class="listbox">
                <ul>
                </ul>
            </div>

            



            <div class="about">
              <div class="section1">
                <div class="mod1">
                    <span class="word4">
                        <input id="inputtitlefour" disabled type="text" name="aboutuser" value="关于我们" class="layui-input">
                    </span>
                    <span class="word5">
                        <input id="inputtitlefive" disabled type="text" name="subaboutuser" value="About&nbsp;us" class="layui-input">
                    </span>
                    <div class="box2">
                      <span class="txt3">
<textarea disabled id="inputtitlethree" type="text" name="aboutcentent" value="Main business" class="layui-input inputtitlethree" onKeyup="if(this.value.length>300){this.value=this.value.slice(0,300)}"></textarea>
                      </span>
                    </div>
                </div>
              </div>
            </div>

            <div class="news">
              <div class="outer8" style="padding-bottom: 10px;">
                <div class="section2">
                  <span class="word6">
                      <input id="endtitleone" disabled type="text" name="newstitle" value="最新资讯" class="layui-input">
                  </span>
                  <span class="word7">
                      <input id="endtitletwo" disabled type="text" name="newssubtitle" value="Latest&nbsp;news" class="layui-input">
                  </span>
                  <div class="ul">
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>

    </div>

    <!-- 因为上传视频进度条所以添加了新的 layui 版本 -->
    <script type="text/javascript" src="{{asset('/userweb/layuinew/layui.js')}}"></script>
    <!-- js -->
    <script>
      var token = sessionStorage.getItem("Publictoken");
      var store_id = sessionStorage.getItem("store_id");

      layui.config({
          base: '../../layuiadmin/' //静态资源所在路径
      }).extend({
      index: 'lib/index', //主入口模块
      formSelects: 'formSelects'
      }).use(['index', 'form','upload','formSelects','laydate','jquery','element'], function(){
          var $ = layui.$
              ,admin = layui.admin
              ,element = layui.element
              ,layer = layui.layer
              ,laydate = layui.laydate
              ,form = layui.form
              ,upload = layui.upload
              ,element = layui.element
              ,formSelects = layui.formSelects;
              var $ = jquery = layui.jquery;
              // 首页-查询 通过pid改变
              const getMiniWebTag = (pid)=>{
                  return new Promise(resolve=>{
                      $.post("/api/miniweb/getMiniWebIndex",{
                          token:token,
                          storeId:store_id,
                          pid:pid,
                      },v=>{
                          resolve(v)
                      })
                  })
              }
            /* 请求数据并且回显 */
            (()=>{
                /* 主营业务回显 */
                zyywdatahuixian();
                /* 最新资讯 */
                zxzxdatahuixian()
                /* 首页标题回显 */
                indexdatalisthuiixan()

            })()
                 /* 首页标题回显 */
            async function indexdatalisthuiixan(){
              const data = await getMiniWebTag(1);
              let datalist = data.data;
              if(data.status == 1){
                  
                  if(datalist.length == 0){

                      setindexdatatypeid = ""

                  }else{  //说明有数据进行回显

                      setindexdatatypeid = ""+datalist[0].id+"";

                      console.log(datalist[0])
                      //更换视频连接
                      layui.jquery('#video').attr("src",datalist[0].video_url);
                      //banner回显
                      layui.jquery('#banner').attr("src", datalist[0].cover_url);
                      //背景回显
                      layui.jquery('.about').css({"background":"url('"+datalist[0].icon_url+"') no-repeat center"});
                      //主营业务-标题
                      $('input[name="pagetitle"]').val(datalist[0].title)
                      //主营业务-副标题
                      $('input[name="pagesubTitle"]').val(datalist[0].sub_title)
                      let cententlist = datalist[0].content.split("#")
                      console.log(cententlist)
                      //关于我们-标题
                      $('input[name="aboutuser"]').val(cententlist[0]);
                      //关于我们-副标题
                      $('input[name="subaboutuser"]').val(cententlist[1]);
                      //关于我们-centent
                      $('#inputtitlethree').text(cententlist[2]);
                      //最新资讯-标题
                      $('input[name="newstitle"]').val(cententlist[3]);
                      //最新资讯-副标题
                      $('input[name="newssubtitle"]').val(cententlist[4]);




                  }
              }
            }
            /* 主营业务回显 */
            async function zyywdatahuixian(){
                //主营业务 回显
                const getMiniWebTagData = await getMiniWebTag(2);
                // console.log(getMiniWebTagData)
                if(getMiniWebTagData.status == 1){
                    let getMiniWebTagDataList =  getMiniWebTagData.data;
                    if(getMiniWebTagDataList.length >= 6){
                        $(".zyywaddbtn").hide();
                    }
                    let getMiniWebTagDataStr = "";
                    getMiniWebTagDataList.forEach((element,k) => {
                        let num = k + 1;   
                        getMiniWebTagDataStr += `<li>
                            <img src="`+element.sub_title+`">
                            <p><input disabled class="inputtitlelist" type="text" name="" value="`+element.title+`" class="layui-input"></p>
                        </li>`
                    });
                    $(".listbox ul").html("")
                    $(".listbox ul").append(getMiniWebTagDataStr);

                }
            }
            /* 最新资讯 */
            async function zxzxdatahuixian(){
                const getMiniWebTagData = await getMiniWebTag(4);
                let listdata = getMiniWebTagData.data;
                if(listdata.length >= 3){
                    $(".newszyywaddbtn").hide();
                }
                let str = ""
                listdata.forEach((element,k) => {
                        console.log(element)
                        let num = k + 1;   
                        str += `
                        <div>
                            <div class="main8">
                                <img src="`+element.cover_url+`" />
                            </div>
                            <span class="word10"><input style="background-color: #ffffff;" disabled class="endtitlelistone" type="text" name="" value="`+element.title+`" class="layui-input"></span>
                            <span class="word11"><input style="background-color: #ffffff;" disabled class="endtitlelisttwo" type="text" name="" value="`+element.sub_title+`" class="layui-input"></span>
                        </div>
                        `
                });
              
                $(".news .section2 .ul").append(str);

            }
      })
    </script>

</body>
</html>