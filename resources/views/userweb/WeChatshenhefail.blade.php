<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>小程序审核</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/modules/layuiicon/iconfont.css')}}" media="all">
    <style type="text/css">
        .xgrate {
            color: #fff;
            font-size: 15px;
            padding: 7px;
            height: 30px;
            line-height: 30px;
            background-color: #3475c3;
        }

        .up {
            position: relative;
        }

        .up #uploadFile {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            opacity: .01;
            width: 100px;
            height: 30px;
        }

        .up input[type=file] {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            opacity: .01;
            width: 100px;
            height: 30px;
        }

        .demo5 {
            width: 100px;
        }

        .box1 {
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            padding: 15px;
            box-shadow: 0 1px 3px rgba(0, 0, 0, .02), 0 16px 32px -4px rgba(0, 0, 0, .17);
        }

        .box2 {
            margin-left: 20px;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            padding: 15px;
            box-shadow: 0 1px 3px rgba(0, 0, 0, .02), 0 16px 32px -4px rgba(0, 0, 0, .17);
        }

        .person {
            width: 100%;
            height: 100px;
            text-align: center;
            line-height: 20px;
            color: #000;
            font-size: 14px;
        }

        .athorize {
            margin-top: 20px;
            width: 552px;
            height: 40px;
            line-height: 40px;
            text-align: center;
            font-size: 14px;
            color: #fff;
            background-color: #00a3fe;
        }

        .athorize:hover {
            cursor: pointer;
        }

        .button {
            margin: 20px auto;
            text-align: center;
        }
        .shenhe{
            display: block;
            text-align: center;
        }
        #addwechatapp img{
            display: block;
            margin: 0 auto;
        }
        .texttip{margin:0 auto;width: 373px;height: 20px;font-size: 14px;font-weight: 400;color: #666666;line-height: 20px;padding-top:8px;}
        .faliedinfo{margin:0 auto;width: 85%;height: 186px;background: rgba(0, 0, 0, 0.02);}
        .yuanyin{ width: 96px;height: 24px;font-size: 16px;font-weight: 500;color: #333333;line-height: 24px;margin-left: 40px;padding-top: 24px;}
    </style>
</head>

<body>
    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-header">微信小程序</div>
                    <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief" style="height:100%;padding-bottom:30px;">
                        <div class="layui-tab-content">
                            <!-- 创建 -->
                            <div class="layui-tab-item layui-show" id="addwechatapp">
                                <img src="{{asset('/mb/editerro.png')}}">
                            </div>
                            <div class="shenhe">审核失败</div>
                            <div class="button">
                                <button type="submit" class="layui-btn" lay-submit="" lay-filter="authentication" id="authentication">重新修改小程序</button>
                            </div>
                        </div>
                        <div class="faliedinfo">
                          <div class="yuanyin">失败原因</div>
                          <p id="reason" style="margin-left:38px;"></text></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
</body>

<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script src="{{asset('/js/qrcode.min.js')}}"></script>

<script type="text/javascript">
    var token = sessionStorage.getItem("Publictoken");
    var store_id = sessionStorage.getItem("store_id");
    var cateGory = [];
    var exter_list = [];
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
    }).use(['index', 'form', 'upload', 'table', 'laydate'], function() {
        var $ = layui.$,
            admin = layui.admin,
            form = layui.form,
            table = layui.table,
            laydate = layui.laydate,
            upload = layui.upload

        $.ajax({
            url: "{{url('/api/customer/weixin/getAppletsStatusByStore')}}",
            data: {
                store_id: store_id,
                type: 1
            },
            type: 'post',
            success: function(data) {
                //失败原因
                $.ajax({
                    url: "{{url('/api/customer/weixin/getLatestAuditStatus')}}",
                    data: {
                        authorizerAppId: data.data.AuthorizerAppid,
                        refreshToken:  data.data.authorizer_refresh_token,
                    },
                    type: 'post',
                    success: function(data) {
                        if (data.status == 200) {
                            console.log(data,"999999999999999999999999")
                            $('#reason').html(data.data.authorizer_appid_status_reason);
                        }
                    }
                })
            }
        })


        /*
         *点击重新新创建
         */
        form.on('submit(authentication)', function(data) {
            //更新步骤
            $.ajax({
                url: "{{url('/api/customer/weixin/updateAppletsStatus')}}",
                data: {
                    store_id: store_id,
                    type: 1,
                    created_step: 4
                },
                type: 'post',
                success: function(data) {
                    if (data.status == 200) {
                        console.log("步骤状态更新成功")
                        window.parent.location.reload();
                    }else{
                        console.log("步骤状态更新失败")
                    }
                }
            })
        })


    });
</script>

</html>