<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>小程序首页</title>
    

    <link rel="stylesheet" href="{{asset('/userweb/layuinew/layui.css')}}">
    <!-- css -->
    <link rel="stylesheet" href="{{asset('/userweb/smallprogram.css')}}">
    <style>
        img{
            height: 100%;
        }
    </style>
</head>
<body>

        
    <div class="centent">
        <div class="smallprogram">
          <form action="" class="layui-form">
            <div class="header">
                <img class="headerimg" src="{{asset('/userweb/smallprogramimg/header.png')}}" alt="">
            </div>
            <div class="banner" style="position: relative;">
                <div style="position: absolute;top:0;left:378px;background-color: #eeeeee;padding: 6px;color: #666666;width: 150px;border-radius: 4px;">
                  建议上传 （690 * 389）
                </div>
                <div class="bannerbox ">
                
                  <div class="layui-progress layuiprogressBar layuiprogressBarnone" lay-filter="progressBar">
                    <div class="layui-progress-bar layui-bg-blue" lay-percent="0%"></div>
                  </div>

                  <div class="videobox videonone">
                    <video id="video" src="/userweb/smallprogramimg/bannerimg.mp4">
                      <source src="myvideo.mp4" type="video/mp4">
                    </video>
                  </div>
                  
                  <img class="bannerfile" id="banner" src="{{asset('/userweb/smallprogramimg/banner.png')}}" alt="">
                  <input type="file" name="file" class="uploadFile" id="bannerfile"/> 
                  <input type="hidden" name="bannerfile" value=""/>
                  <div class="click">
                          <img
                          class="img1"
                          referrerpolicy="no-referrer"
                          src="https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng0c79604f2d7ee61e393ec23a8ece4e37cb811817807603d2a30968a57c36d0bf"
                          />
                  </div>
                </div>

                <div class="getvieo" id="getvieo">
                    上<br/>传<br/>视<br/>频
                </div>

            </div>

            <div class="row-one">
                <input id="inputtitle" type="text" name="pagetitle" value="请输入标题"  placeholder="" class="layui-input">
                <input id="inputtitletwo" type="text" name="pagesubTitle" value="请输入副标题"  placeholder="" class="layui-input">
            </div>
 
            <div class="listbox"  style="position: relative;">
            
                <div style="position: absolute;top:0;left:378px;background-color: #eeeeee;padding: 6px;color: #666666;width: 150px;border-radius: 4px;">
                  建议上传 （336 * 252）
                </div>
                <ul>
                </ul>
                <div class="zyywaddbtn">
                  新增内容
                </div>
                
            </div>

            <div class="about">
              <div class="section1"  style="position: relative;">
                <div style="position: absolute;top:0;left:378px;background-color: #eeeeee;padding: 6px;color: #666666;width: 150px;border-radius: 4px;">
                  建议上传 （750 * 750）
                </div>
                <div class="mod1">
                    <span class="word4">
                        <input id="inputtitlefour" type="text" name="aboutuser" value="请输入标题" class="layui-input" placeholder="">
                    </span>
                    <span class="word5">
                        <input id="inputtitlefive" type="text" name="subaboutuser" value="请输入副标题" placeholder="" class="layui-input">
                    </span>
                    <div class="box2">
                      <span class="txt3">
<textarea id="inputtitlethree" type="text" placeholder="请输入文本内容" name="aboutcentent" value="Main business" class="layui-input inputtitlethree" onKeyup="if(this.value.length>300){this.value=this.value.slice(0,300)}">
请输入文本内容
</textarea>
                      </span>
                    </div>
                </div>
              </div>
              <div class="aboutbgmupload">
                  <input type="file" name="file" class="aboutbgmupload" id="aboutbgmupload"/>
                  <input type="hidden" name="aboutbgmuploadvalue" value=""/>
                  上<br/>传<br/>背<br/>景<br/>图
              </div>
            </div>



            <div class="news">
              <div class="outer8">
                <div class="section2" style="position: relative;">
                  <span class="word6">
                      <input id="endtitleone" type="text" name="newstitle" value="请输入标题"  placeholder="" class="layui-input">
                  </span>
                  <span class="word7">
                      <input id="endtitletwo" type="text" name="newssubtitle" value="请输入副标题"  placeholder="" class="layui-input">
                  </span>
                  <div style="position: absolute;top:58px;left:378px;background-color: #eeeeee;padding: 6px;color: #666666;width: 150px;border-radius: 4px;">
                    建议上传 （690 * 280）
                  </div>
                  <div class="ul">

                  </div>
                </div>
              </div>
              <div class="newszyywaddbtn">
                    新增内容
                </div>
            </div>
            <div class="layui-form-item">
              <button class="layui-btn" lay-submit lay-filter="save" style="background: #1E9FFF;margin-top: 15px;float: right;width: 100px;">保存</button>
            </div>
          </form>
        </div>

    </div>

    <!-- 因为上传视频进度条所以添加了新的 layui 版本 -->
    <script type="text/javascript" src="{{asset('/userweb/layuinew/layui.js')}}"></script>
    <!-- js -->
    <script type="text/javascript" src="{{asset('/userweb/smallprogram.js')}}"></script>

</body>
</html>