<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,initial-scale=1.0, user-scalable=no">
    <title>联系我们</title>
    <link rel="stylesheet" href="{{asset('/userweb/layuinew/layui.css')}}">

    <!-- css -->
    <link rel="stylesheet" href="{{asset('/userweb/contactus.css')}}">
    <style>
        img{
            width: 100%;
            height: 100%;
        }
    </style>

</head> 
<body>
    
    <div class="centetnt">
        
        <form action="" class="layui-form">
            <div class="smallprogram">

                <div class="top">
                    <img src="/userweb/contactusimg/usertop.png">
                </div>

                <div class="map">
                    <div id="container"></div>
                </div>

                <div class="listtext">
                    <div class="topline">
                    </div>
                    <div class="row_one">
                        <img class="usertouimg" src="/userweb/contactusimg/tou.png">
                        <input type="hidden" name="usertouimg" value=""/>
                        <div>
                            <div class="title"><input id="title" type="text" name="username" value="" class="layui-input"></div>
                            <p><input id="p" type="text" name="usertime" value="" class="layui-input"></p>
                        </div>
                    </div>
                    <div class="row_two">
                        <div class="list">
                            <img src="/userweb/contactusimg/userphone.png" alt="">
                            <div class="phone">电话：</div>
                            <p><input id="phone" type="text" name="userphone" value="" class="layui-input"></p>
                        </div>
                        <div class="list">
                            <img src="/userweb/contactusimg/xx.png" alt="">
                            <div class="phone">邮箱：</div>
                            <p><input id="emil" type="text" name="useremil" value="" class="layui-input"></p>
                        </div>
                        <div class="list">
                            <img src="/userweb/contactusimg/gw.png" alt="">
                            <div class="phone">官网：</div>
                            <p><input id="web" type="text" name="userweb" value="" class="layui-input"></p>
                        </div>
                        <div class="list">
                            <img src="/userweb/contactusimg/dz.png" alt="">
                            <div class="phone">地址：</div>
                            <p><input id="address" type="text" name="userress" value="" class="layui-input"></p>
                        </div>
                    </div>
                    <div class="secrch">
                        <div class="layui-btn layuiadmin-btn-comm layui-btn-sm secrchuser">搜索位置</div>  
                        <input type="hidden" name="userlng" value="">
                        <input type="hidden" name="userlat" value="">
                    </div>
                </div>

                <div class="footer">
                    <img src="/userweb/contactusimg/footer.png" alt="">
                </div>
                    
            </div>
            <div class="layui-form-item formbtn">
              <button class="layui-btn" lay-submit lay-filter="save" style="background: #1E9FFF;margin-top: 15px;width: 100%;">保存</button>
            </div>
        </form>
    </div>

    <!-- 因为上传视频进度条所以添加了新的 layui 版本 -->
    <script type="text/javascript" src="{{asset('/userweb/layuinew/layui.js')}}"></script>
    
    <!-- 高德地图 -->
    <script type="text/javascript" src="https://webapi.amap.com/maps?v=2.0&key=bf7f6a7aff4ec471640429999862349f"></script> 
        
    <!-- js -->
    <script type="text/javascript" src="{{asset('/userweb/contactus.js')}}"></script>

</body>
</html>