<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>模板设置</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/layuiadmin/style/admin.css')}}" media="all">
    <style>
        .tetle {
            width: 90px;
            height: 25px;
            font-size: 18px;
            font-weight: 500;
            color: #333333;
            line-height: 25px;
            margin-left: 30px;
        }

        .qrcode {
            width: 160px;
            height: 172px;
            background: #FFFFFF;
            border: 1px solid #EEEEEE;
            margin-left: 30px;
            margin-top: 20px;
            position: relative;
        }

        .imgImg {
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -60px;
            /* 高度的一半 */
            margin-left: -60px;
            /* 宽度的一半 */
        }

        .tips {
            width: 98px;
            height: 22px;
            font-size: 14px;
            font-weight: 400;
            color: #666666;
            line-height: 22px;
            position: absolute;
            top: 86%;
            left: 26%;
        }

        .weixin {
            width: 80px;
            height: 32px;
            background: rgba(52, 117, 195, 0.1);
            border: 1px solid rgba(52, 117, 195, 0.5);
            font-weight: 400;
            color: #3475C3;
            line-height: 32px;
            text-align: center;
        }

        .apples {
            height: 32px;
            background: #FFFFFF;
            border-bottom: 1px solid #EEEEEE;
            border-right: 1px solid #EEEEEE;
            border-left: 1px solid #EEEEEE;
            font-weight: 400;
            color: #999999;
            line-height: 32px;
            text-align: center;
        }

        .buttonshiyong {
            width: 240px;
            height: 48px;
            background: #3475C3;
            border-radius: 8px;
            text-align: center;
            line-height: 48px;
            font-size: 20px;
            font-weight: 400;
            color: #FFFFFF;
            margin-top: 420px;
            margin-left: 30px;
        }
    </style>
</head>

<body>

    <div class="layui-fluid" style="margin-top:50px;">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">

                <div class="layui-fluid">
                    <div class="layui-row layui-col-space15">
                        <div class="layui-col-md12">
                            <div class="layui-card">
                                <div class="layui-card-header">小程序模板</div>

                                <div class="layui-card-body">
                                    <div class="layui-row" style="display:flex;width: 710px;margin:auto;">
                                        <div style="width: 435px;height: 727px;background: #D8D8D8; ">
                                            <div class="layui-carousel" id="test1" lay-filter="test1"
                                                style="text-align: center;margin: 0 auto;">
                                                <div carousel-item="">
                                                    <div><img src="{{asset('/mb/webstits/pic1.png')}}"></div>
                                                    <div><img src="{{asset('/mb/webstits/pic2.png')}}"></div>
                                                    <div><img src="{{asset('/mb/webstits/pic3.png')}}"></div>
                                                </div>
                                            </div>

                                        </div>
                                        <div>
                                            <text class="tetle">微官网模版</text>
                                            <div class="qrcodebox">
                                                <div class="qrcode">
                                                    <img src="/mb/webstits/erweima.png" class="imgImg imgImgNone"
                                                        style="width:120px;height:120px;">
                                                    <div class="tips">
                                                        扫二维码预览
                                                    </div>
                                                </div>
                                                <div style="width:162px;margin-left:30px;">
                                                    <div class="apples">小程序</div>
                                                </div>
                                            </div>
                                            <div class="buttonshiyong">
                                                <a style="color: #fff;" lay-href="{{url('/userweb/smallprogram')}}">立即使用</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <input type="hidden" class="store_id">
        <input type="hidden" class="js_tpl_sku">
        <input type="hidden" class="js_tpl_bck" value="1">

        <script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
        <script src="{{asset('/layuiadmin/layui/jquery-2.1.4.js')}}"></script>
        <script>
            var token = sessionStorage.getItem("Publictoken");
            var str = location.search;
            var store_id = sessionStorage.getItem("store_store_id");


            layui.config({
                base: '../../layuiadmin/' //静态资源所在路径
            }).extend({
                index: 'lib/index' //主入口模块
            }).use(['index', 'form', 'table', 'laydate', 'carousel'], function() {
                var $ = layui.$,
                    admin = layui.admin,
                    form = layui.form,
                    table = layui.table,
                    laydate = layui.laydate;
                // 未登录,跳转登录页面
                $(document).ready(function() {
                    if (token == null) {
                        window.location.href = "{{url('/mb/login')}}";
                    }
                });

                var carousel = layui.carousel;
                //建造实例
                carousel.render({
                    elem: '#test1',
                    width: '435px',
                    height: '727px',
                    interval: 2000 //每2秒，自动轮播
                        ,
                    arrow: 'always'
                });

                //监听轮播切换事件
                carousel.on('change(test1)', function(obj) { //test1来源于对应HTML容器的 lay-filter="test1" 属性值
                });

            });
        </script>

</body>

</html>