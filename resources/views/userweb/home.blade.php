<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>首页</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">

<link rel="stylesheet" href="{{asset('/layuiadmin/layui/css/layui.css')}}" media="all">
<link rel="stylesheet" href="{{asset('/layuiadmin/style/wgw/index.css')}}">

</head>
<body style="padding:20px;">
    <div class="center">

        <div class="topbox">
            <div class="list dome1">
                <div class="p">
                    <span>今日点击量</span>
                    <div class="imghover">
                        <img id="img1" src="/mb/wgw/wenhao.png">
                        <div class="tishi">
                            今日进入微官网的总点击次数
                        </div>
                    </div>
                </div>
                <div class="data">
                    <h3>-</h3><span>-</span><img src="/mb/wgw/top.png">
                </div>
                
            </div>
            <div class="list dome2">
                <div class="p">
                    <span>今日意向客户人均访问时长(秒)</span>
                    <div class="imghover">
                        <img id="img1" src="/mb/wgw/wenhao.png">
                        <div class="tishi">
                        今日意向客户的访问时长/意向客户人数
                        </div>
                    </div>
                </div>
                <div class="data">
                    <h3>-</h3><span>-</span><img src="/mb/wgw/top.png">
                </div>
            </div>
            <div class="list dome3">
                <div class="p">
                    <span>今日意向客户</span>
                    <div class="imghover">
                        <img id="img1" src="/mb/wgw/wenhao.png">
                        <div class="tishi">
                        今日在微官网小程序里授权手机号码的客户人数
                        </div>
                    </div>
                </div>
                <div class="data">
                    <h3>-</h3><span>-</span><img src="/mb/wgw/top.png">
                </div>
            </div>
            <div class="list dome4">
                <div class="p">
                    <span>今日访问人数</span>
                    <div class="imghover">
                        <img id="img1" src="/mb/wgw/wenhao.png">
                        <div class="tishi">
                        今日进入微官网小程序里的总人数
                        </div>
                    </div>
                </div>
                <div class="data">
                    <h3>-</h3><span>-</span><img src="/mb/wgw/top.png">
                </div>
            </div>
            <div class="list dome5">
                <div class="p">
                    <span>今日意向客户占比</span>
                    <div class="imghover">
                        <img id="img1" src="/mb/wgw/wenhao.png">
                        <div class="tishi">
                        今日意向客户占总访问人数的比例
                        </div>
                    </div>
                </div>
                <div class="data">
                    <h3>-</h3><span>-</span><img src="/mb/wgw/top.png">
                </div>
            </div>
        </div>

        <div class="bottonbox">
            <div class="leftbox">
                <div class="leftboxtop">
                    <div class="leftleft">
                        <p>总点击量</p>
                        <h3>-</h3>
                    </div>
                    <div class="leftright">
                        <p>意向客户人均访问时长（秒）</p>
                        <h3>-</h3>
                    </div>
                </div>

                <div class="leftboxbotton" id="biaoone">

                </div>



            </div>
            <div class="rightbox">
                <div class="rightboxmin" id="biaotwo">

                </div>
            </div>
        </div> 




    </div>
</body>
<script src="{{asset('/layuiadmin/layui/layui.js')}}"></script>
<script src="{{asset('/layuiadmin/echarts.min.js')}}"></script>
<script>

var token = sessionStorage.getItem("Publictoken");
var store_id = sessionStorage.getItem("store_id");

layui.config({
    base: '../../layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index','form', 'upload','table','laydate'], function(){
    var $ = layui.$
        ,admin = layui.admin
        ,table = layui.table
        ,form = layui.form
        ,upload = layui.upload
        ,laydate = layui.laydate;



    /* 请求函数 */
    const getdatalist = (url,data)=>{
        return new Promise(resolve=>{
            $.post(url,data,(res)=>{
                resolve(res)
            })
        })
    }


    /* 获取统计数据 */
    (async ()=>{


        getdatalist("/api/miniweb/memberCounts",{

            token:token,
            storeId:store_id,

        }).then(v=>{

            let pagedata = JSON.parse(v);
            console.log(pagedata.data)

            /* 今日点击量 */
            $(".dome1 h3").html(pagedata.data.day_click);
            /* 今日点击量百分比 */
            if(pagedata.data.yesterday_click > 0){
                $(".dome1 .data span").html(getPercent(pagedata.data.day_click-pagedata.data.yesterday_click, pagedata.data.yesterday_click));
                if(pagedata.data.day_click < pagedata.data.yesterday_click){
                    $(".dome1 .data img").attr("src","/mb/wgw/bottom.png")
                }else{
                    $(".dome1 .data img").attr("src","/mb/wgw/top.png")
                }
            }else{
                $(".dome1 .data img").attr("src","/mb/wgw/top.png")
                $(".dome1 .data span").html("100%")
            }

            /* 今日意向客户 */
            $(".dome3 h3").html(pagedata.data.day_customer)
            /* 今日意向客户百分比 */
            if(pagedata.data.yesterday_customer > 0){
                $(".dome3 .data span").html(getPercent(pagedata.data.day_customer-pagedata.data.yesterday_customer, pagedata.data.yesterday_customer));
                if(pagedata.data.day_customer < pagedata.data.yesterday_customer){
                    $(".dome3 .data img").attr("src","/mb/wgw/bottom.png")
                }else{
                    $(".dome3 .data img").attr("src","/mb/wgw/top.png")
                }
            }else{
                $(".dome3 .data img").attr("src","/mb/wgw/top.png")
                $(".dome3 .data span").html("100%")
            }

            /* 今日访问人数 */
            $(".dome4 h3").html(pagedata.data.day_visitor)
            /* 今日访问人数百分比 */
            if(pagedata.data.yesterday_visitor > 0){
                $(".dome4 .data span").html(getPercent(pagedata.data.day_visitor-pagedata.data.yesterday_visitor, pagedata.data.yesterday_visitor));
                if(pagedata.data.day_visitor < pagedata.data.yesterday_visitor){
                    $(".dome4 .data img").attr("src","/mb/wgw/bottom.png")
                }else{
                    $(".dome4 .data img").attr("src","/mb/wgw/top.png")
                }
            }else{
                $(".dome4 .data img").attr("src","/mb/wgw/top.png") 
                $(".dome4 .data span").html("100%")
            }
            /* 总点击量 */
            $(".bottonbox .leftleft h3").html(pagedata.data.all_click)
            /* 意向客户人均访问时长（秒） */
            $(".bottonbox .leftright h3").html(pagedata.data.all_customer_seconds)
            /* 今日意向客户占比 */
            let todayusergooddata = getPercent(pagedata.data.day_customer, pagedata.data.day_visitor)
            $(".dome5 h3").html(todayusergooddata)
            /* 今日意向客户占比百分比 */
            if(pagedata.data.yesterday_customer > 0){
                $(".dome5 .data span").html(getPercent(pagedata.data.day_customer-pagedata.data.yesterday_customer, pagedata.data.yesterday_customer));
                if(pagedata.data.day_customer < pagedata.data.yesterday_customer){
                    $(".dome5 .data img").attr("src","/mb/wgw/bottom.png")
                }else{
                    $(".dome5 .data img").attr("src","/mb/wgw/top.png")
                }
            }else{
                $(".dome5 .data img").attr("src","/mb/wgw/top.png")  
                $(".dome5 .data span").html("100%")
            }
            /* 今日意向客户人均访问时长(秒) */
            $(".dome2 h3").html(pagedata.data.day_customer_seconds)
            /* 今日意向客户人均访问时长百分比 */
            if(pagedata.data.yesterday_customer_seconds > 0){
                $(".dome2 .data span").html(getPercent(pagedata.data.day_customer_seconds-pagedata.data.yesterday_customer_seconds, pagedata.data.yesterday_customer_seconds));
                if(pagedata.data.day_customer_seconds < pagedata.data.yesterday_customer_seconds){
                    $(".dome2 .data img").attr("src","/mb/wgw/bottom.png")
                }else{
                    $(".dome2 .data img").attr("src","/mb/wgw/top.png")
                }
            }else{
                $(".dome2 .data img").attr("src","/mb/wgw/top.png")
                $(".dome2 .data span").html("100%")
            }

            /* 饼图 */
            biaoonefun(pagedata.data.customer_count,pagedata.data.visitor_count);

        })

        /* 线型图数据 */
        getdatalist("/api/miniweb/sevenCounts",{

            token:token,
            storeId:store_id,
 
        }).then(v=>{
            let newv = JSON.parse(v);
            if(newv.status == 1){
                let customer = [];
                let visitor = [];
                let listdata = newv.data

                customer.push(listdata.six_customer)
                customer.push(listdata.five_customer)
                customer.push(listdata.four_customer)
                customer.push(listdata.three_customer)
                customer.push(listdata.before_customer)
                customer.push(listdata.yesterday_customer)
                customer.push(listdata.day_customer)


                visitor.push(listdata.six_visitor)
                visitor.push(listdata.five_visitor)
                visitor.push(listdata.four_visitor)
                visitor.push(listdata.three_visitor)
                visitor.push(listdata.before_visitor)
                visitor.push(listdata.yesterday_visitor)
                visitor.push(listdata.day_visitor)
    
                console.log(customer)
                console.log(visitor)
                biaotwofun(customer,visitor);
            }
        })




    })();

    /* 求百分比 */
    function getPercent(num, total){
        num = parseFloat(num);
        total = parseFloat(total);
        if (isNaN(num) || isNaN(total)) {
            return "-";
        }
        return total <= 0? "0%" : Math.round((num / total) * 10000) / 100.0 + "%";
    }


    function biaotwofun(customer,visitor){

        var myChart = echarts.init(document.getElementById('biaotwo'));
        option = {
            title: {
                text: '7日统计图',
            },
            grid:{
                top:"16%"
            },
            legend: {
                data: ['访客人数', '意向客户'],
                left:"left",
                top:"36px",
                textStyle:{ 
                    color: '#8C8C8C'
                }
            },
            tooltip: {
                trigger: 'axis'
            },
            color:[
                "#5B8FF9","#5AD8A6"
            ],
            xAxis: {
                type: 'category',
                data: ['6天前', '5天前', '4天前', '3天前', '2天前', '1天前', '今天'],
                axisLine:{
                    lineStyle:{
                        color: '#000000' //x轴坐标颜色
                    }
                },
                axisTick:{show:false}, //坐标轴 刻度线

            },
            yAxis: {
                type: 'value',
                axisTick:{show:false},//坐标轴 刻度线
                axisLine:{show:false} //坐标轴
            },
            series: [
                {   
                    name:"访客人数",
                    data: visitor,
                    type: 'line',
                    smooth: true,
                    itemStyle : { normal: {label : {show: true,color:"#999999"}}},
                    // areaStyle: {
                    //     color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                    //         offset: 0,
                    //         color: 'rgba(91,143,249,0.8)'
                    //     }, {
                    //         offset: 1,
                    //         color: 'rgba(91,143,249,0.3)'
                    //     }])
                    // },
                },
                {   
                    name:"意向客户",
                    data: customer,
                    type: 'line',
                    smooth: true,
                    itemStyle : { normal: {label : {show: true,color:"#999999"}}},
                    // areaStyle: {
                    //     color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                    //         offset: 0,
                    //         color: 'rgba(90,216,166,0.8)'
                    //     }, {
                    //         offset: 1,
                    //         color: 'rgba(90,216,166,0.3)'
                    //     }])
                    // },
                }

            ]
        };
        myChart.setOption(option);

    }

    function biaoonefun(yxkh,fyxkh){
        //数据
        let datalist = [{value: yxkh, name: '意向客户'},{value: fyxkh, name: '非意向客户'}];
        //容器
        var myChart = echarts.init(document.getElementById('biaoone'));
        option = {
            title: {
                text: '累计意向客户占比',
            },
            legend: {
                icon: "circle",  //圆点
                itemWidth: 9,
                itemHeight: 9,
                orient: 'vertical',  //上下排
                left:"left",
                top:"40px",
                textStyle:{ //字体颜色
                    color: '#8C8C8C'
                }
            },
            color:[
                "#5AD8A6","#5B8FF9"
            ],
            series: [
                {
                    name: '访问来源',
                    type: 'pie',
                    radius: '70%',
                    data: datalist,
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    },
                    label:{ //让数据显示在饼图内
                        normal:{
                            show:true,
                            position:"inner",
                        }
                    },
                    itemStyle:{ //让饼图内显示 数据， 而不是name
                        normal:{ 
                            label:{ 
                            show: true, 
                            formatter: '{c}' 
                            }, 
                            labelLine :{show:true} 
                        }
                    }
                }
            ]
        };
        myChart.setOption(option);
    }

});
</script>
</html>
