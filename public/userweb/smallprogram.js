
var token = localStorage.getItem("Publictoken");
var store_id = localStorage.getItem("store_id");

layui.config({
    base: '../../layuiadmin/' //静态资源所在路径
}).extend({
index: 'lib/index', //主入口模块
formSelects: 'formSelects'
}).use(['index', 'form','upload','formSelects','laydate','jquery','element'], function(){

    var $ = layui.$
        ,admin = layui.admin
        ,element = layui.element
        ,layer = layui.layer
        ,laydate = layui.laydate
        ,form = layui.form
        ,upload = layui.upload
        ,element = layui.element
        ,formSelects = layui.formSelects;
        var $ = jquery = layui.jquery;



    //setindexdatatype = 0;
    var setindexdatatypeid;

    // 首页-查询 通过pid改变
    const getMiniWebTag = (pid)=>{
        return new Promise(resolve=>{
            $.post("/api/miniweb/getMiniWebIndex",{
                token:token,
                storeId:store_id,
                pid:pid,
            },v=>{
                resolve(v)
            })
        })
    }

    /* 请求数据并且回显 */
    (()=>{
        /* 主营业务回显 */
        zyywdatahuixian();
        /* 最新资讯 */
        zxzxdatahuixian()
        /* 首页标题回显 */
        indexdatalisthuiixan()

    })()
     /* 首页标题回显 */
    async function indexdatalisthuiixan(){

        const data = await getMiniWebTag(1);

        let datalist = data.data;
        if(data.status == 1){
            
            if(datalist.length == 0){

                setindexdatatypeid = ""

            }else{  //说明有数据进行回显

                setindexdatatypeid = ""+datalist[0].id+"";

                console.log(datalist[0])
                //更换视频连接
                layui.jquery('#video').attr("src",datalist[0].video_url);
                //banner回显
                layui.jquery('#banner').attr("src", datalist[0].cover_url);
                //背景回显
                layui.jquery('.about').css({"background":"url('"+datalist[0].icon_url+"') no-repeat center"});
                //主营业务-标题
                $('input[name="pagetitle"]').val(datalist[0].title)
                //主营业务-副标题
                $('input[name="pagesubTitle"]').val(datalist[0].sub_title)
                let cententlist = datalist[0].content.split("#")
                console.log(cententlist)
                //关于我们-标题
                $('input[name="aboutuser"]').val(cententlist[0]);
                //关于我们-副标题
                $('input[name="subaboutuser"]').val(cententlist[1]);
                //关于我们-centent
                $('#inputtitlethree').text(cententlist[2]);
                //最新资讯-标题
                $('input[name="newstitle"]').val(cententlist[3]);
                //最新资讯-副标题
                $('input[name="newssubtitle"]').val(cententlist[4]);

            }
        }
    }

    /* 主营业务回显 */
    async function zyywdatahuixian(){
        //主营业务 回显
        const getMiniWebTagData = await getMiniWebTag(2);
        // console.log(getMiniWebTagData)
        if(getMiniWebTagData.status == 1){
            let getMiniWebTagDataList =  getMiniWebTagData.data;
            if(getMiniWebTagDataList.length >= 6){
                $(".zyywaddbtn").hide();
            }
            let getMiniWebTagDataStr = "";
            getMiniWebTagDataList.forEach((element,k) => {
                let num = k + 1;   
                getMiniWebTagDataStr += `<li class="updataid">
                    <img class="listboxlistimg listboxlistimg`+num+`"  id="imglistboxlistimg`+num+`" src="`+element.sub_title+`" alt="">
                    <input type="file" name="file" class="uploadFile" id="listboxlistimg`+num+`"/>
                    <input type="hidden" name="imghiddenlistboxlistimg`+num+`" value=""/>
                    <div class="delect" data-id="`+element.id+`">删除</div>
                    <p><input class="inputtitlelist" type="text" name="" value="`+element.title+`" class="layui-input"></p>
                </li>`
            });
            $(".listbox ul").html("")
            $(".listbox ul").append(getMiniWebTagDataStr);
            endfundata ();
            btnsetimg();
            Deletion_of_Main_Business();
        }
    }

    /* 最新资讯 */
    async function zxzxdatahuixian(){
        const getMiniWebTagData = await getMiniWebTag(4);
        let listdata = getMiniWebTagData.data;
        if(listdata.length >= 3){
            $(".newszyywaddbtn").hide();
        }
        let str = ""
        listdata.forEach((element,k) => {
                console.log(element)
                let num = k + 1;   
                str += `
                <div class="li" id="main8addlist`+num+`">
                    <div class="main8">
                        <img
                        class="img`+num+` endlistimglist`+num+`"
                        referrerpolicy="no-referrer"
                        src="`+element.cover_url+`"
                        />
                        <input type="file" name="file" class="uploadFile" id="endlistimg`+num+`"/> 
                        <input type="hidden" name="endlistimginput`+num+`" value=""/>
                    </div>
                    <div class="newdelect" data-id="`+element.id+`">删除</div>
                    <span class="word10"><input class="endtitlelistone" type="text" name="" value="`+element.title+`" class="layui-input"></span>
                    <span class="word11"><input class="endtitlelisttwo" type="text" name="" value="`+element.sub_title+`" class="layui-input"></span>
                </div>
                `
        });
       
        $(".news .section2 .ul").append(str);
        zxzximg();
        Main_Business();
    }

    /* 主营业务新增 */
    $(".zyywaddbtn").click(()=>{
        //获取li
        let listdata = $(".listbox ul li");
        if(listdata.length >= 6 ){
            layer.msg("最多展示 6 个", {icon:2, shade:0.5, time:2000});
            return false;
        }else{
            if(listdata.length == 5){
                $(".zyywaddbtn").hide();
            }
            let num = listdata.length + 1;
            let getMiniWebTagDataStr = `<li class="inputtitlelistAdd" id="inputtitlelistdel`+num+`">
                        <img class="listboxlistimg  listboxlistimg`+num+`" id="imglistboxlistimg`+num+`" src="/userweb/smallprogramimg/list1.png" alt="">
                        <input type="file" name="file" class="uploadFile" id="listboxlistimg`+num+`"/> 
                        <input type="hidden" name="imghiddenlistboxlistimg`+num+`" value=""/>
                        <div class="delect" data-id="">删除</div>
                        <p><input class="inputtitlelist" type="text" name="" value=""   placeholder="请输入..."  class="layui-input"></p>
                    </li>`;
            $(".listbox ul").append(getMiniWebTagDataStr)
            endfundata ();
            btnsetimg();
            Deletion_of_Main_Business("formclick");
        }
    })
    /* 最新资讯新增 */
    $(".newszyywaddbtn").click(()=>{

        let num =$(".outer8 .ul .li").length;
        if(num >= 3){
            layer.msg("最多展示 3 个", {icon:2, shade:0.5, time:2000});
            return false;
        }else{
            if(num == 2){
                $(".newszyywaddbtn").hide();
            }
            let number = num + 1 ;
            let liststr = `
            <div class="li main8addlist" id="main8addlist`+number+`">
                <div class="main8">
                    <img
                    class="img`+number+` endlistimglist`+number+`"
                    referrerpolicy="no-referrer"
                    src="https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPnge371d0e1d2e897c2867baf882f4e7fa476c05b5fcb9f578416370e9d380b822d"
                    />
                    <input type="file" name="file" class="uploadFile" id="endlistimg`+number+`"/> 
                    <input type="hidden" name="endlistimginput`+number+`" value=""/>
                </div>
                <div class="newdelect" data-id="">删除</div>
                <span class="word10"><input class="endtitlelistone" type="text" name="" value=""   placeholder="请输入..."  class="layui-input"></span>
                <span class="word11"><input class="endtitlelisttwo" type="text" name="" value=""   placeholder="请输入..."  class="layui-input"></span>
            </div>
            `
            $(".outer8 .ul").append(liststr)
            endfundata ();
            zxzximg();
            Main_Business("formclick")
        }

    })

    /* 新增加 最新资讯 */
    function zuiNewAdd(){
        let listBoxArr = [];
        $(".news .li").each((k,v)=>{
            let listBoxObj = {}
            listBoxObj.name1 = $(v).find(".endtitlelistone").val();
            listBoxObj.name2 = $(v).find(".endtitlelisttwo").val();
            listBoxObj.img  = $(v).find("img").attr("src");
            listBoxObj.id  = $(v).find(".newdelect").attr("data-id")
            listBoxArr.push(listBoxObj)
        })
        console.log(listBoxArr)
        listBoxArr.forEach(v=>{
            $.post("/api/miniweb/updateMiniWebIndex",{
                token:token,
                storeId:store_id,
                type:4,
                pid:4,
                id:v.id,
                title:v.name1,
                subTitle:v.name2,
                coverUrl:v.img
            },res=>{
                console.log(res);
                if(res.status == 1){
                    let showtitle = $("#endtitleone").val();
                    layer.msg(showtitle+`上传成功`, {icon:1, shade:0.5, time:1000});
                    
                }else{
                    let showtitle = $("#endtitleone").val();
                    layer.msg(showtitle+`上传失败`, {icon:1, shade:0.5, time:1000});
                }
            })
        })
    }

    /* 保存按钮 ： 如果没有数据 就 给 form 加上 class="layui-form" */ 
    form.on("submit(save)",function(data){
        
        let pagedata = data.field;
        // 最新资讯
        zuiNewAdd();

        //新增加 新增内容
        let listBoxArr = [];
        $(".listbox li").each((k,v)=>{
            let listBoxObj = {}
            listBoxObj.name = $(v).find(".inputtitlelist").val();
            listBoxObj.img  = $(v).find("img").attr("src");
            listBoxObj.id = $(v).find(".delect").attr("data-id");
            listBoxArr.push(listBoxObj)
        })
        listBoxArr.forEach(element => {
            $.post("/api/miniweb/updateMiniWebIndex",{
                token:token,
                storeId:store_id,
                type:2,
                pid:2,
                id:element.id,
                title:element.name,
                subTitle:element.img
            },res=>{
                console.log(res);
                if(res.status == 1){
                    let showtitle = $("#inputtitle").val();
                    layer.msg(showtitle+`上传成功`, {icon:1, shade:0.5, time:1000});
                }else{
                    let showtitle = $("#inputtitle").val();
                    layer.msg(showtitle+`上传失败`, {icon:1, shade:0.5, time:1000});
                }
            })
        });
        /* 获取视频连接 */

        let videourl = layui.jquery('#video').attr("src");  
        console.log(videourl)
        
        /* 首页-模块编辑 */
        let indexdatalist = pagedata.aboutuser+"#"+pagedata.subaboutuser+"#"+pagedata.aboutcentent+"#"+pagedata.newstitle+"#"+pagedata.newssubtitle
        console.log(pagedata)
        $.post("/api/miniweb/updateMiniWebIndex",{

            token:token,
            type:1,
            pid:1,
            storeId:store_id,
            id:setindexdatatypeid,

            title:pagedata.pagetitle,
            subTitle:pagedata.pagesubTitle,
            coverUrl:pagedata.bannerfile,
            videoUrl:videourl,
            iconUrl:pagedata.aboutbgmuploadvalue,
            content:indexdatalist

        },(res)=>{
            console.log(res)
            if(res.status == 1){
                layer.msg(res.message, {icon:1, shade:0.5, time:2000});
            }else{
                layer.msg(res.message, {icon:2, shade:0.5, time:2000});
            }
        })

        return false;
       
    })

    /* 循环给最新图片上传 */
    zxzximg()
    function zxzximg(){
        // 最新资讯 图片上传 1
        //获取li
        let listdata = $(".news .ul .li").length;
        for(let i=0;i<listdata;i++){
            let num = i +1 ;
            upload.render({
                elem : '#endlistimg'+num+'',
                url : "/api/basequery/upload",
                data:{
                    token:token,
                    type:'img',
                    attach_name: 'file',
                },
                method : 'POST',   
                type : 'images',   
                ext : 'jpg|png|gif',    
                unwrap : true, 
                // size : 5120,
                before : function(input){
                    layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                },
                done: function(res){
                    console.log(res);
                    if(res.status == 1){
                        layer.msg("文件上传成功", {icon:1, shade:0.5, time:1000});
                        layui.jquery('.endlistimglist'+num+'').attr("src", res.data.img_url);
                        $('input[name="endlistimginput'+num+'"]').val(res.data.img_url);
                    }else{
                        layer.msg(res.message, {icon:2, shade:0.5, time:1000});
                    }
                }
            });
        }
    }

    /* 最新资讯-删除 */
    function Main_Business(data){
        if(data == "formclick"){
            $(".main8addlist .newdelect").click(function(){
                console.log($(".section2 .li").length)

                if($(".section2 .li").length <= 3){
                    $(".newszyywaddbtn").show()
                }

                let dataid = $(this).attr("data-id");
                if(dataid){
                    let thit = this;
                    $.post("/api/miniweb/delMiniWebIndex",{
                        token:token,
                        id:dataid
                    },data=>{
                        let objdelid = $(thit).parent().index();
                        console.log(objdelid)
                        $(".outer8 .ul .li").eq(objdelid).remove();
                        if(data.status == 1){

                            layer.msg("删除成功", {icon:1, shade:0.5, time:1000});

                        }

                    })
                }else{
                    let objdelid = $(this).parent().attr("id");
                    $("#"+objdelid+"").remove()
                }
            })
        }else{
            $(".news .ul .newdelect").click(function(){
                if($(".section2 .li").length <= 3){
                    $(".newszyywaddbtn").show()
                }

                let dataid = $(this).attr("data-id");
                if(dataid){
                    let thit = this;
                    $.post("/api/miniweb/delMiniWebIndex",{
                        token:token,
                        id:dataid
                    },data=>{
                        let objdelid = $(thit).parent().index();
                        console.log(objdelid)
                        $(".outer8 .ul .li").eq(objdelid).remove();
                        if(data.status == 1){
                            layer.msg("删除成功", {icon:1, shade:0.5, time:1000});
                        }
                    })

                }else{
                    let objdelid = $(this).parent().attr("id");
                    $("#"+objdelid+"").remove()
                }
            })
        }
    }
    
    /* 主营业务-删除 */
    function Deletion_of_Main_Business(data){
        if(data == "formclick"){
            $(".inputtitlelistAdd .delect").click(function(){
                console.log($(".updataid").length)
                if($(".updataid").length <= 6){
                    $(".zyywaddbtn").show();
                }
                let dataid = $(this).attr("data-id");
                if(dataid){
                    let thit = this;
                    $.post("/api/miniweb/delMiniWebIndex",{
                        token:token,
                        id:dataid
                    },data=>{
                        let objdelid = $(thit).parent().index();
                        console.log(objdelid)
                        $(".smallprogram .listbox li").eq(objdelid).remove();
                        if(data.status == 1){
                            if($(".updataid").length <= 6){
                                $(".zyywaddbtn").show();
                            }
                            layer.msg("删除成功", {icon:1, shade:0.5, time:1000});
                        }
                    })
                }else{
                    let objdelid = $(this).parent().attr("id");
                    $("#"+objdelid+"").remove()
                }
            })
        }else{
            $(".listbox ul li .delect").click(function(){
                if($(".updataid").length <= 6){
                    $(".zyywaddbtn").show();
                }
                let dataid = $(this).attr("data-id");
                if(dataid){
                    let thit = this;
                    $.post("/api/miniweb/delMiniWebIndex",{
                        token:token,
                        id:dataid
                    },data=>{
                        let objdelid = $(thit).parent().index();
                        console.log(objdelid)
                        $(".smallprogram .listbox li").eq(objdelid).remove();
                        if(data.status == 1){
                            layer.msg("删除成功", {icon:1, shade:0.5, time:1000});
                        }
                        btnsetimg()
                    })
                }else{
                    let objdelid = $(this).parent().attr("id");
                    $("#"+objdelid+"").remove()
                }

            })
        }
    }

    /* 给循环的主营业务添加上传图片的按钮 */
    function btnsetimg(){
        //获取li
        let listdata = $(".listbox ul li").length;
        // 主营业务：图片1，图片上传
        for(let i=0;i<listdata;i++){
            let num = i +1 ;
            upload.render({
                elem : '#listboxlistimg'+num+'',
                url : "/api/basequery/upload",
                data:{
                    token:token,
                    type:'img',
                    attach_name: 'file',
                    is_img_compress :2,
                    width: 336,
                    height: 252,
                },
                method : 'POST',   
                type : 'images',   
                ext : 'jpg|png|gif',    
                unwrap : true, 
                // size : 5120,
                before : function(input){
                    layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
                },
                done: function(res){
                    console.log(res);
                    if(res.status == 1){
                        layer.msg("文件上传成功", {icon:1, shade:0.5, time:1000});
                        layui.jquery('#imglistboxlistimg'+num+'').attr("src", res.data.img_url);
                        $('input[name="imghiddenlistboxlistimg'+num+'"]').val(res.data.img_url);
                    }else{
                        layer.msg(res.message, {icon:2, shade:0.5, time:1000});
                    }
                }
            });
        }
    }

    aaaaaa();
    function aaaaaa(){
        var xhrOnProgress = function (fun) {
            xhrOnProgress.onprogress = fun; //绑定监听
            //使用闭包实现监听绑
            return function () {
                //通过$.ajaxSettings.xhr();获得XMLHttpRequest对象
                var xhr = $.ajaxSettings.xhr();
                //判断监听函数是否为函数
                if (typeof xhrOnProgress.onprogress !== 'function')
                    return xhr;
                //如果有监听函数并且xhr对象支持绑定时就把监听函数绑定上去
                if (xhrOnProgress.onprogress && xhr.upload) {
                    xhr.upload.onprogress = xhrOnProgress.onprogress;
                }
                return xhr;
            }
        }
        
        /* 视频播放或暂停点击按钮 */
        $(".img1").click(function(){
            $(".videobox").removeClass("videonone")
            $('video').trigger('play');
        })
        /* 监听视频是否播放完毕 */
        var video = document.getElementById("video");
        video.addEventListener('ended', function () { 
            $(".videobox").addClass("videonone")
        }, false);
    
        /* 上传视频 */
        upload.render({
            elem: '#getvieo'
            ,url: '/api/basequery/webupload?file_type=file'+'&token='+token
            ,accept: 'video' //视频
            ,xhr: xhrOnProgress
            ,auto: false
            ,choose :function(obj){ //上传前限制 图片的宽高
                obj.preview(function(index, file, result){
                    console.log(file.size)
                    if(file.size > 20971520){
                        layer.msg("上传视频不能大于20M", {icon:2, shade:0.5, time:3000});
                    }else{
                        obj.upload(index, file); 
                    }
                });
            }
            ,before:function(){
                $(".layuiprogressBar").removeClass("layuiprogressBarnone")
            }
            ,progress: function(value,elem) {
                console.log("进度：" + value + '%');
                element.progress('progressBar',value + '%');
            }
            ,done: function(res){
                $(".layuiprogressBar").addClass("layuiprogressBarnone"); //取消进度条
                layui.jquery('#video').attr("src",res.data.img_url);     //将 上传的 视频连接 放到 播放器
                layer.msg('上传成功');
                console.log(res)
            }
        });
        
        // banner图片图片上传
        upload.render({
            elem : '#bannerfile',
            url : "/api/basequery/upload",
            data:{
                token:token,
                type:'img',
                attach_name: 'file',
                is_img_compress : 2,
                width: 690,
                height: 389,
            },
            method : 'POST',   
            type : 'images',   
            ext : 'jpg|png|gif',    
            unwrap : true, 
            // size : 5120,
            before : function(obj){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                console.log(res);
                if(res.status == 1){
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:1000});
                    layui.jquery('#banner').attr("src", res.data.img_url);
                    $('input[name="bannerfile"]').val(res.data.img_url);
                }else{
                    layer.msg(res.message, {icon:2, shade:0.5, time:1000});
                }
    
            }
        });
    
        // 关于我们背景图上传
        upload.render({
            elem : '#aboutbgmupload',
            url : "/api/basequery/upload",
            data:{
                token:token,
                type:'img',
                attach_name: 'file',
            },
            method : 'POST',   
            type : 'images',   
            ext : 'jpg|png|gif',    
            unwrap : true, 
            // size : 5120,
            before : function(input){
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                console.log(res);
                if(res.status == 1){
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:1000});
                    $('input[name="aboutbgmuploadvalue"]').val(res.data.img_url);
                    layui.jquery('.about').css({"background":"url('"+res.data.img_url+"') no-repeat center"});
                }else{
                    layer.msg(res.message, {icon:2, shade:0.5, time:1000});
                }
    
            }
        });
    }

    endfundata ()
    function endfundata (){

        var subtips;
        /* input:hover事件 */
        $("input[type='file']").hover(function(e) {
            openMsg(this,"点击上传图片");
        }, function() {
            layer.close(subtips);
        });
    
        /* 视频 */
        $(".getvieo").hover(function(e) {
            openMsg(this,"点击上传视频");
        }, function() {
            layer.close(subtips);
        });
    
        /* 视频 */
        $(".img1").hover(function(e) {
            openMsg(this,"点击播放视频");
        }, function() {
            layer.close(subtips);
        });
    
        /* banner：input */
        $(".bannerfile").hover(function(e) {
            openMsg(this,"点击上传视频封面");
        }, function() {
            layer.close(subtips);
        });
    
        function openMsg(obj,text) {
            subtips = layer.tips(text, obj,{tips:[2,'#1E9FFF'],time: 30000});
        }
    
    }

})