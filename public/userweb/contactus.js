// 地图初始化应该在地图容器div已经添加到DOM树之后
var map = new AMap.Map('container', {
    resizeEnable: true,
    zoom:11,
})

var marker1;

/* 用户信息js */
var token = localStorage.getItem("Publictoken");
var store_id = localStorage.getItem("store_id");

layui.use(['layer', 'form'], function(){
    var layer = layui.layer
        ,form = layui.form
        ,upload = layui.upload
    $ = jquery = layui.jquery;

    /* 头像上传 */
    upload.render({
        elem : '.usertouimg',
        url : "/api/basequery/upload",
        data:{
            token:token,
            type:'img',
            attach_name: 'file',
        },
        method : 'POST',   
        type : 'images',   
        ext : 'jpg|png|gif',    
        unwrap : true, 
        size : 5120,
        before : function(input){
            //执行上传前的回调  可以判断文件后缀等等
            layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
        },
        done: function(res){
            if(res.status == 1){
                layer.msg("文件上传成功", {icon:1, shade:0.5, time:1000});
                layui.jquery('.usertouimg').attr("src", res.data.img_url);
                $('input[name="usertouimg"]').val(res.data.img_url);
            }else{
                layer.msg(res.message, {icon:2, shade:0.5, time:1000});
            }
        }
    });

    /* input:hover事件 */
    $(".usertouimg").hover(function(e) {
        openMsg(this,"点击上传图片");
    }, function() {
        layer.close(subtips);
    });
    function openMsg(obj,text) {
        subtips = layer.tips(text, obj,{tips:[2,'#1E9FFF'],time: 30000});
    } 

    /* 获取坐标 接口 */
    const getuserloction = (data)=>{
        return new Promise(resolve=>{
            $.get("https://restapi.amap.com/v3/geocode/geo",data,res=>{
                resolve(res)
            })
        })
    }

    /* 点击搜索获取用户的坐标 */
    $(".secrchuser").on("click",()=>{

        if(marker1){
            map.remove(marker1);
        }
        const userlocation = $('input[name="userress"]').val();
        console.log(userlocation)
        getuserloction({
            address:userlocation,
            output:"JSON",
            key:"78f3706f45d94d54f1bc083b452fee7c",
        }).then(v=>{
            console.log(v)
            if(v.status == 1){
                if(v.geocodes.length == 0){
                    layer.msg("定位不成功，请输入新的位置", {icon:2, shade:0.5, time:2000});
                }else{
                    layer.msg("定位成功", {icon:1, shade:0.5, time:1000});

                    let position = v.geocodes[0].location.split(",");

                    marker1 = new AMap.Marker({
                        position: new AMap.LngLat(position[0], position[1]),   // 经纬度对象，如 new AMap.LngLat(116.39, 39.9); 也可以是经纬度构成的一维数组[116.39, 39.9]
                    });

                    //将位置坐标放到 input 中

                    $('input[name="userlng"]').val(position[0]);
                    $('input[name="userlat"]').val(position[1]);

                    map.setCenter([position[0], position[1]]); 
                    map.setZoom(18);
                    map.add(marker1);

                }
            }else if(v == "undefined"){
                layer.msg("定位不成功，请输入新的位置", {icon:2, shade:0.5, time:2000});
            }else{
                layer.msg(v.info, {icon:2, shade:0.5, time:2000});
            }
        })

    })

    /* input 点击 */
    // let inputdata = "";
    // let inputobj = "";
    // $("input[type='text']").click(function(e){
    //     if( inputdata == "" && inputobj == ""){
    //         inputdata = $(e.target).val();
    //         inputobj = e.target;
    //         $(e.target).val("");
    //     }else{

    //         if($(e.target).val() == inputobj){
    //             return false;
    //         }else{
    //             if($(inputobj).val() == ""){
    //                 $(inputobj).val(inputdata)
    //                 inputobj = e.target;
    //                 inputdata = $(e.target).val();
    //                 $(e.target).val("")
    //             }else{
    //                 inputdata = $(e.target).val();
    //                 inputobj = e.target;
    //                 $(e.target).val("");
    //             }
    //         }

    //     }
    // })

    /* 查询,编辑-接口 */
    const userdatafun = (url,data)=>{
        return new Promise(resolve=>{
            $.post(url,data,res=>{
                resolve(res)
            })
        })
    }
    /* 一进来就请求的数据 */
    (()=>{
        showdatalist();
    })();

    /* 数据回显 */
    /* 判断id是否存在，如果存在说明有数据，后期更新数据，没有就是添加 */
    var userlistdataid;
    async function showdatalist(){
        /* 数据查询 */
        const showdata = await userdatafun("/api/miniweb/getMiniWebContact",{
            token:token,
            storeId:store_id,
        });
        let listdata = showdata.data;
        console.log(listdata)
        if(listdata.id){ //id 存在说明 有数据 进行回显
            //显示用户位置
            marker1 = new AMap.Marker({
                position: new AMap.LngLat(listdata.lng, listdata.lat),
            });
            map.setCenter([listdata.lng, listdata.lat])
            map.setZoom(18);
            map.add(marker1);

            userlistdataid = listdata.id; //存储id，方便更新数据
            $('input[name="username"]').val(listdata.store_name)
            $('input[name="usertime"]').val(listdata.e_time)
            $('input[name="userphone"]').val(listdata.telephone)
            $('input[name="useremil"]').val(listdata.email)
            $('input[name="userweb"]').val(listdata.website)
            $('input[name="userress"]').val(listdata.address)
            layui.jquery('.usertouimg').attr("src",listdata.cover_url);

        }else{

            userlistdataid = "";
            $('input[name="username"]').val("XXXX")
            $('input[name="usertime"]').val("08:00-20:00")
            $('input[name="userphone"]').val("18800000000")
            $('input[name="useremil"]').val("123456@163.com")
            $('input[name="userweb"]').val("XXX.XXXX.com")
            $('input[name="userress"]').val("XX省XX市XX区XX号")

        }
    }

    /* 提交数据 */
    form.on("submit(save)",function(data){

        let userdata = data.field;

        // 判断用户是否进行过 位置查询
        if( !userdata.userlng && !userdata.userlat ){
            layer.msg("请搜索位置，进行定位", {icon:2, shade:0.5, time:2000});
            return false;
        }

        console.log(userdata)
        userdatafun("/api/miniweb/updateMiniWebContact",{

            token:token,
            storeId:store_id,
            id:userlistdataid,
            storeName:userdata.username,
            sTime:userdata.usertime,
            eTime:userdata.usertime,
            coverUrl:userdata.usertouimg,
            telephone:userdata.userphone,
            email:userdata.useremil,
            website:userdata.userweb,
            address:userdata.userress,
            lng:userdata.userlng,
            lat:userdata.userlat,

        }).then(v=>{

            console.log(v)
            if(v.status == 1){
                layer.msg(v.message, {icon:1, shade:0.5, time:2000});
            }else{
                layer.msg(v.message, {icon:2, shade:0.5, time:2000});
            }

        })

        return false;
        
    })


});













