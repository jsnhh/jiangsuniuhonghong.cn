<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MeituanpsConfigs extends Model
{

    protected $table = 'meituanps_configs';

    protected $fillable = [
        'store_id',
        'appkey',
        'secret',
        'version'
    ];

}
