<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class HltxStore extends Model
{

    protected $table = 'hltx_stores';

    protected $fillable = [
        'config_id',
        'qd',
        'store_id',
        'mer_no',
        'serial_no',
        'req_id',
        'wx_merchant_no',
        'ali_merchant_no',
        'union_merchant_no',
        'sub_scribe_appid',
        'status',
        'result_status',
        'fail_reason',
        'created_at ',
        'updated_at'
    ];

}
