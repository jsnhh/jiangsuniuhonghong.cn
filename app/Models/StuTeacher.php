<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StuTeacher extends Model
{

    protected $table='stu_teachers';

    protected $fillable = [
        'store_id',
        'merchant_id',
        'name',
        'phone',
        'password',
        'status',
        'school_no',
    ];
}
