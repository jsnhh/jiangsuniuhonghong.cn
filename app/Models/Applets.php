<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Applets extends Model
{
    protected $table = "alipay_applets";
    protected $fillable = [
        'config_id',
        'store_id' ,
        'account',
        'contact_name',
        'contact_mobile',
        'contact_email',
        'batch_no',
    ];

}