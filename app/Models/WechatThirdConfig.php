<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WechatThirdConfig extends Model
{
    protected $table = 'wechat_third_config';
    protected $primaryKey = "id";

    private function getMap()
    {
        $map = [];

        $map['config_id'] = 1234;

        return $map;
    }

    public function getInfo()
    {
        $map = $this->getMap();

        return $this->where($map)->first();
    }

    public function saveData($data)
    {
        return $this->insert($data);
    }

    public function updateData($data)
    {
        return $this->where('config_id', 1234)->update($data);
    }
}
