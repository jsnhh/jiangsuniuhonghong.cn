<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DongguanConfig extends Model
{
    protected $table = 'dongguan_configs';

    protected $fillable = [
        'config_id',
        'appId',
        'appSecret',
        'wechatAppId',
        'wechatAppSecret',
        'applyKey',
        'token',
        'devitk',
        'selfPublicKey',
        'selfPrivateKey',
        'BODPublicKey',
        'privateKey'
    ];

}
