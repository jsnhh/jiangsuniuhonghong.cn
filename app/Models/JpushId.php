<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JpushId extends Model
{
    protected $table = "jpush_ids";

    protected $fillable = [
        'config_id',
        'store_id',
        'merchant_id',
        'jpush_id',
        "device_id",
        'device_type',
    ];
}
