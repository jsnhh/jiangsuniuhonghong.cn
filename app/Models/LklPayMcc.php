<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LklPayMcc extends Model
{
    protected $table = 'lkl_mcc';

    protected $fillable = [
        'code',
        'name',
        'mcc_parent',
        'parent_code',
        'business_scene'
    ];

}
