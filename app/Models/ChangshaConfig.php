<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChangshaConfig extends Model
{
    //

    protected $table = "changsha_configs";
    protected $fillable = [
        'config_id',
        'rsa_pr',
        'rsa_pub',
        'created_at',
        'updated_at'
    ];
}
