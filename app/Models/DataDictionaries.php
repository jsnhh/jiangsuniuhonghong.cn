<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataDictionaries extends Model
{

    protected $fillable = [
        'title',
        'name',
        'code',
        'code_value',
        'remark'
    ];

}
