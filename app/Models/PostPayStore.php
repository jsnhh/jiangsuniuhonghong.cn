<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostPayStore extends Model
{
    protected $table = 'post_pay_stores';

    protected $fillable = [
        'config_id',
        'store_id',
        'cust_id',
        'drive_no',
        'audit_status',
        'audit_msg'
    ];

}
