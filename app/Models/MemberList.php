<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberList extends Model
{

    protected $table="member_lists";
    protected $fillable = [
        'store_id',
        'config_id',
        'merchant_id',
        'mb_id',
        'mb_status',
        'mb_ver',
        'mb_ver_desc',
        'pay_counts',
        'pay_moneys',
        'mb_logo',
        'mb_name',
        'mb_phone',
        'mb_nick_name',
        'mb_jf',
        'mb_money',
        'mb_time',
        'xcx_openid',
        'wx_openid',
        'ali_user_id',
        'mb_from_type',
        'updated_at'
    ];

    public function editMember($id, $data)
    {
        return $this->where('id', $id)->update($data);
    }

    private function getMemberInfoMap($input)
    {
        $map = [];
        if (!empty($input['mb_id'])) {
            $map['mb_id'] = $input['mb_id'];
        }

        if (!empty($input['xcx_openid'])) {
            $map['xcx_openid'] = $input['xcx_openid'];
        }

        return $map;
    }

    // 查询会员信息
    public function getMemberInfo($input)
    {
        $map = $this->getMemberInfoMap($input);

        return $this->where($map)->first();
    }

    public function getMemberInfoByAppletOpenid($input)
    {
        $map = [];
        if (!empty($input['xcx_openid'])) {
            $map['xcx_openid'] = $input['xcx_openid'];
        }

        return $this->where($map)->first();
    }
}
