<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppMyIndex extends Model
{

    protected $table = 'app_my_indexs';
    protected $fillable =
        [
            'type',
            'config_id',
            'title',
            'icon',
            'url',
            'sort'
        ];
}
