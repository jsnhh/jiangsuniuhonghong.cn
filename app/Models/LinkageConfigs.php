<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class LinkageConfigs extends Model
{

    protected $table = 'linkage_configs';

    protected $fillable = [
        'config_id',
        'mch_id',
        'wechatChannelId',
        'alipayChannelId',
        'publicKey',
        'privateKey',
        'wxAppid',
        'wxSecret'
    ];

}
