<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantStoreAppidSecret extends Model
{
    protected $table      = "merchant_store_appid_secrets";
    protected $primaryKey = "id";

    /**
     * 更新该门店的access_token信息
     * @param string $appid
     * @param array $data
     * @return bool
     */
    public function updateWeChatInfo($appid, $data)
    {
        if(!isset($appid) || empty($appid)){
            return false;
        }

        $result = $this->where(['wechat_appid' => $appid])->update([
            'wechat_access_token'               => $data['access_token'],
            'wechat_access_token_expires_in'    => $data['expires_in'],
            'wechat_access_token_invalid_date'  => date("Y-m-d H:i:s",time()+$data['expires_in']),
            'updated_at'                        => date("Y-m-d H:i:s",time())
        ]);

        return $result;
    }

    /**
     * 更新该门店的access_token信息
     * @param $store_id
     * @param $data
     * @return bool
     */
    public function updateAliPayInfo($appid,$data){
        if(!isset($appid) || empty($appid)){
            return false;
        }

        $result = $this->where(['alipay_appid' => $appid])->update([
            'alipay_access_token'               => $data['access_token'],
            'alipay_access_token_expires_in'    => $data['expires_in'],
            'alipay_access_token_invalid_date'  => date("Y-m-d H:i:s",time()+$data['expires_in']),
            'alipay_refresh_token'              => $data['refresh_token'],
            'alipay_refresh_token_expires_in'   => $data['re_expires_in'],
            'alipay_refresh_token_invalid_date' => date("Y-m-d H:i:s",time()+$data['re_expires_in']),
            'updated_at'                        => date("Y-m-d H:i:s",time())
        ]);

        return $result;
    }


    /**
     * 获取门店的appid等信息
     * @param $store_id
     * @return bool
     */
    public function getStoreAppIdSecret($store_id){
        if(!isset($store_id) || empty($store_id)){
            return false;
        }

        $result = $this->where(['store_id' => $store_id])->first();

        return $result;
    }
}
