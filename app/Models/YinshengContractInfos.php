<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class YinshengContractInfos extends Model
{

    protected $table = 'yinsheng_contract_infos';

    protected $fillable = [
        'store_id',
        'contract_type',
        'bus_open_type',
        'd_calc_type',
        'c_calc_type',
        'd_calc_val',
        'd_stlm_max_amt',
        'd_fee_low_limit',
        'c_calc_val',
        'c_fee_low_limit',
        'sign_id',
        'sign_url',
        'auth_id',
        'sign_msg',
        'sign_status',
        'mer_code',
        'remark',
        'd0_is_open'

    ];

}
