<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AppLogo extends Model
{
    protected $table = 'app_logos';

    protected $fillable = [
        'config_id',
        'face_logo',
        'login_logo',
        'start_bk_img',
        'ht_img', //平台logo
        'qr_img', //二维码
        'ali_ad_default', //
        'wechat_ad_default', //
        'favicon_url', //
    ];

}
