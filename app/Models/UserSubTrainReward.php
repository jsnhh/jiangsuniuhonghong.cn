<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class UserSubTrainReward extends Model
{

    protected $table = "user_sub_train_reward";
    protected $fillable = [
        'user_id',
        'p_user_id',
        'p_user_name',
        'amount',
        'transaction_amount',
        'status',
        'time_start',
        'time_end'
    ];

}
