<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantConsumerDetails extends Model
{
    protected $fillable = [
        'merchant_id',
        'user_id',
        'pay_amount',
        'amount',
        'avail_amount',
        'profit',
        'profit_amount',
        'type',
        'remark',
        'total_profit_amount',
        'order_id',
        'rate'
    ];
}
