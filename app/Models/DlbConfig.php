<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DlbConfig extends Model
{
    //
    protected $fillable = [
        'config_id',
        'access_key',
        'secret_key',
        'agent_num',
        'device_timewait',
        'qwx_timewait',
        'created_at ',
        'updated_at'
    ];
}
