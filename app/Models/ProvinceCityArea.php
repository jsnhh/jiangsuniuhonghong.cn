<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProvinceCityArea extends Model
{
    //
    protected $table = 'province_city_areas';

    protected $fillable = [
        'area_code',
        'area_name',
        'area_parent_id',
        'area_type',
        'dada_city_code'
    ];

    protected $primaryKey = "id";

    /**
     * 获取省份
     * @return mixed
     */
    public function getProvince(){
        $result = $this->where(['area_parent_id' => 1,'area_type' => 2])->get(['id','area_code',DB::raw('area_name as text')]);
        return $result;
    }

    /**
     * 获取市区
     * @param $code
     * @return mixed
     */
    public function getCity($code){
        $result = $this->where(['area_parent_id' => $code , 'area_type' => 3])->get(['id','area_code',DB::raw('area_name as text')]);
        return $result;
    }

    /**
     * 获取县级
     * @param $code
     * @return mixed
     */
    public function getArea($code){
        $result = $this->where(['area_parent_id' => $code , 'area_type' => 4])->get(['id','area_code',DB::raw('area_name as text')]);
        return $result;
    }
}
