<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreDayAllOrder extends Model
{
    //
    protected $table = 'store_day_all_orders';

    protected $fillable = [
        'store_id',
        'user_id',
        'day',
        'total_amount',
        'order_sum',
        'refund_amount',
        'refund_count',
        'fee_amount',
    ];

}
