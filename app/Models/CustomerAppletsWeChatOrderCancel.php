<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerAppletsWeChatOrderCancel extends Model
{
    protected $table = 'customerapplets_wechat_order_cancels';

    protected $fillable = [
        'store_id',
        'app_key',
        'shop_order_id',
        'shop_no',
        'delivery_id',
        'waybill_id',
        'cancel_reason_id',
        'cancel_reason',
        'deduct_fee',
        'desc'
    ];

}
