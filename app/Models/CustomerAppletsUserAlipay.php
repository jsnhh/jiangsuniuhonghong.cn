<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CustomerAppletsUserAlipay extends Model
{
    protected $table      = "customerapplets_user_alipays";
    protected $primaryKey = "id";

    /**
     * 根据openid查询信息是否存在
     * @param $store_id
     * @param $alipay_user_id
     * @param $code
     * @return array|bool|int
     */
    public function getUserAliPayUserIdInfo($store_id,$alipay_user_id,$code){
        if(!isset($store_id) || empty($store_id)){
            return false;
        }
        if(!isset($alipay_user_id) || empty($alipay_user_id)){
            return false;
        }

        $date = date("Y-m-d H:i:s",time());

        //先判断该openid用户有没有再该门店登陆过
        $userInfo = DB::table("customerapplets_users")->where(['store_id' => $store_id,'third_party_id' => $alipay_user_id])->first();

        if(!empty($userInfo)){

            $userAliPayResult = $this->where(['alipay_user_id' => $alipay_user_id,'store_id' => $store_id])->first();

            if(!empty($userAliPayResult)){
                $this->where(['alipay_user_id' => $alipay_user_id])->update([
                    'updated_at'         => $date
                ]);
            }else{
                $user_AliPay_info['user_id']        = $userInfo->id;
                $user_AliPay_info['store_id']       = $store_id;
                $user_AliPay_info['alipay_user_id'] = $alipay_user_id;
                $user_AliPay_info['created_at']     = $date;
                $user_AliPay_info['updated_at']     = $date;
                $user_AliPay_result = $this->insert($user_AliPay_info);
                if(!$user_AliPay_result){
                    return ["status" => "202","message" => "用户支付宝数据插入失败"];
                }
            }
            return $userInfo->id;
        }else{
            //开启事务
            DB::beginTransaction();
            try{
                $user_id = DB::table("customerapplets_users")->insertGetId([
                    'store_id'           => $store_id,
                    'code'               => $code,
                    'user_source'        => 2,
                    'third_party_id'     => $alipay_user_id,
                    'created_at'         => $date,
                    'updated_at'         => $date
                ]);

                if($user_id){
                    $user_AliPay_info['user_id']        = $user_id;
                    $user_AliPay_info['store_id']       = $store_id;
                    $user_AliPay_info['alipay_user_id'] = $alipay_user_id;
                    $user_AliPay_info['created_at']     = $date;
                    $user_AliPay_info['updated_at']     = $date;
                    $user_AliPay_result = $this->insert($user_AliPay_info);
                    if(!$user_AliPay_result){
                        return ["status" => "202","message" => "用户微信数据插入失败"];
                    }
                }else{
                    return ["status" => "202","message" => "用户数据插入失败"];
                }
                DB::commit();
                return $user_id;
            }catch (\Exception $e) {
                DB::rollBack();
                return false;
            }
        }
    }

    /**
     * 更新用户的支付宝基本信息
     * @param $user_id
     * @param $store_id
     * @param $aliPay_data_info
     * @return array|bool
     */
    public function updateUserAliPayInfo($user_id,$store_id,$aliPay_data_info){
        if(!$user_id || !$store_id){
            return false;
        }

        $userInfo = $this->where(['user_id' => $user_id,'store_id' => $store_id])->first();

        if(empty($userInfo)){
            return ["status" => 202,"message" => "用户信息不存在，请授权"];
        }else{
            //开启事务
            DB::beginTransaction();
            try{
                $result = $this->where(['user_id' => $user_id,'store_id' => $store_id])->update([
                    'alipay_gender'         => $aliPay_data_info['gender'] == "m" ? 1 : 2,
                    "alipay_username"       => $aliPay_data_info['nickName'],
                    "alipay_avatar"         => $aliPay_data_info['avatarUrl'],
                    "alipay_user_country"   => $aliPay_data_info['country'],
                    "alipay_user_province"  => $aliPay_data_info['province'],
                    "alipay_user_city"      => $aliPay_data_info['city'],
                    'updated_at'            => date("Y-m-d H:i:s",time())
                ]);
                DB::commit();
                return ["status" => 200,"message" => $result];
            }catch (\Exception $e) {
                DB::rollBack();
                return ["status" => 202,"message" => $e->getMessage()];
            }
        }
    }

    /**
     * 查找用户支付宝信息
     * @param $user_id
     * @param $store_id
     * @return bool
     */
    public function getUserInfo($user_id,$store_id){
        if(!isset($user_id) || empty($user_id)){
            return false;
        }
        if(!isset($store_id) || empty($store_id)){
            return false;
        }

        $result = $this->where(['user_id' => $user_id,'store_id' => $store_id])->first();

        if(!empty($result)){
            return $result;
        }else{
            return false;
        }
    }
}
