<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberCzSet extends Model
{


    protected $table = 'member_cz_sets';

    protected $fillable = [
        'yx_type',
        'yx_id',
        'cz_list',
        'b_time',
        'e_time'
    ];
}
