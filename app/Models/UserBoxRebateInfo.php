<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBoxRebateInfo extends Model
{
    protected $table = 'user_box_rebate_info';

    protected $fillable = [
        'user_id',
        'user_name',
        'store_id',
        'store_name',
        'amount',
    ];

}
