<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuzhouConfig extends Model
{
    //

    protected $table = "shuzou_configs";
    protected $fillable = [
        'config_id',
        'certId',
        'rsa_pr',
        'rsa_pub',
        'created_at',
        'updated_at'
    ];
}
