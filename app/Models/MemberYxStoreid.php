<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberYxStoreid extends Model
{


    protected $table = 'member_yx_store_ids';

    protected $fillable = [
        'store_id',
        'yx_type',
        'yx_id',
    ];
}
