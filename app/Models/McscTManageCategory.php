<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class McscTManageCategory extends Model
{
    protected $table = 'mcsc_t_manage_category';

    protected $fillable = [
        'first_level_code', //一级类目编码
        'first_level_name', //一级类目名称
        'second_level_code', //二级类目编码
        'second_level_name', //二级类目名称
        'wechat_code', //微信编码
        'wechat_type_name', //微信类型名称
        'alipay_type_name', //支付宝一级分类
        'alipay_manage_category', //支付宝经营类目
        'alipay_clsld', //支付宝clsld
        'unionpay_category', //银联类目
        'unionpay_mcc', //银联mcc
        'wechat_code_individual', //微信类型名称 个体工商户
        'wechat_code_government', //微信类型名称 事业单位
    ];

}
