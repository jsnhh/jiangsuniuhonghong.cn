<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EasypayConfig extends Model
{
    protected $table = 'easypay_configs';

    protected $fillable = [
        'config_id',
        'getSignKey',
        'client_code',
        'channel_id',
        'sign',
        'key',
        'wx_appid',
        'wx_secret',
        'channel_key',
        'channel_public_key',
        'is_use'
    ];

}
