<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LianfuStores extends Model
{
    protected $table = 'lianfu_stores';

    protected $fillable = [
        'config_id',
        'store_id',
        'apikey',
        'signkey',
    ];

}