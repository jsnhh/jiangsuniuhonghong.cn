<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerAppletsUserOrderGoodStandards extends Model
{

    protected $table      = "customerapplets_user_order_good_standards";
    protected $primaryKey = "id";

}
