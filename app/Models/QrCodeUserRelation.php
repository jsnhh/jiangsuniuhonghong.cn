<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class QrCodeUserRelation extends Model
{

    protected $table = "qr_code_user_relation";
    protected $fillable = [
        'code_num',
        'user_id',
        'p_id',
        'level',
        'award_amount',
        'amount',
        'status'

    ];

}
