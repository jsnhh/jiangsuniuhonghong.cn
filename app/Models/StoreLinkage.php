<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class StoreLinkage extends Model
{

    protected $table = 'store_linkages';

    protected $fillable = [
        'config_id',
        'store_id' ,
        'CategoryId',
        'signType',
        'signName',
        'signMobileNo',
        'signCertNo',
        'appName',
        'webSite',
        'terminalType',
        'installProvinceCode',
        'installCityCode',
        'installAreaCode',
        'installDetailAddr',
        'terminals'
    ];

}
