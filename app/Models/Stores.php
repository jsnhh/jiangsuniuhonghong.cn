<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stores extends Model
{
    protected $table      = "stores";
    protected $primaryKey = "id";

    // 查询条件处理
    private function getMap($input)
    {
        $map = [];
        // 商户id
        if (!empty($input['merchant_id'])) {
            $map['merchant_id'] = $input['merchant_id'];
        }

        return $map;
    }

    // 获取商户门店信息
    public function getMerchantStore($input)
    {
        $map = $this->getMap($input);

        return $this->where($map)->select('store_id')->get();
    }

}
