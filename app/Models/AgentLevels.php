<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class AgentLevels extends Model
{

    protected $table = "agent_levels";
    protected $fillable = [
        'level',
        'level_weight',
        'rate',
        'start_transaction',
        'end_transaction',
        'code_num',
        'sid',
        'sid_num',
        'activation_amount',
        'reward_month',
        'activation_integral'
    ];

}
