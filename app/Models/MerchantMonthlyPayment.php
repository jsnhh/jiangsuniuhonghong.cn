<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantMonthlyPayment extends Model
{

    protected $fillable = [
        'merchant_id',
        'user_id',
        'month_package',
        'amount',
        'quarterly_package',
        'quarterly_amount',
    ];
}
