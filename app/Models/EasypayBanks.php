<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EasypayBanks extends Model
{
    protected $table = 'easypay_banks';

    protected $fillable = [
        'bank_no',
        'hbdm',
        'sshh',
        'csdm',
        'bank_name',
        'bank_head_name',
        'status'
    ];

}
