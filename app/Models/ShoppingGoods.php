<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShoppingGoods extends Model
{

    protected $table = "shopping_goods";

    protected $fillable = [
        'goods_sn',
        'goods_name',
        'category_id',
        'brief',
        'is_on_sale',
        'pic_url',
        'unit',
        'original_price',
        'present_price',
        'stock',
        'quantity',
        'is_integral',
        'integral_num',
        'integral_consume'
    ];

}
