<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantStoreMoneyQrcode extends Model
{
    protected $table      = "merchant_store_money_qrcode";
    protected $primaryKey = "id";

    // 查询条件处理
    private function getMap($input)
    {
        $map = [];
        // 该商户名下所有门店id
        if (!empty($input['store_ids'])) {
            $store_ids = $input['store_ids'];
            $map[] = [function($query) use ($store_ids){
                $query->whereIn('merchant_store_money_qrcode.store_id', $store_ids);
            }];
        }
        // 门店id
        if (!empty($input['store_id'])) {
            $map['merchant_store_money_qrcode.store_id'] = $input['store_id'];
        }

        return $map;
    }

    // 数据个数
    public function getMoneyQrcodeCount($input)
    {
        $map = $this->getMap($input);

        return $this->where($map)->count();
    }

    // 获取门店桌号列表
    public function getMoneyQrcodePageList($input, $start, $limit)
    {
        $map = $this->getMap($input);

        return $this->where($map)
            ->leftjoin('stores', 'merchant_store_money_qrcode.store_id', '=', 'stores.store_id')
            ->select('merchant_store_money_qrcode.*', 'stores.store_name')
            ->skip($start)->limit($limit)
            ->get();
    }

    // 插入门店桌号信息
    public function insertData($data)
    {
        return $this->insert($data);
    }
}
