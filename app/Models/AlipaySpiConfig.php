<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AlipaySpiConfig extends Model
{
    protected $fillable = [
        'app_id',
        'app_name',
        'config_id',
        'config_type',
        'rsa_private_key',
        'alipay_rsa_public_key',
        'callback',
        'alipay_gateway',
    ];

}