<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class GoodsTag extends Model
{

    protected $table = 'goods_tags';

    protected $fillable = [
        'store_id',
        'name',
        'status',
        'sort'
    ];


}
