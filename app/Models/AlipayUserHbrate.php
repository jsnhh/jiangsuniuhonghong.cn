<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AlipayUserHbrate extends Model
{
    //
    protected $fillable = [
        'user_id',
        'num',
        'rate',
        'status',
    ];
}
