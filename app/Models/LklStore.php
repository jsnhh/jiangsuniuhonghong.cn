<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LklStore extends Model
{
    protected $table = 'lkl_stores';

    protected $fillable = [
        'config_id',
        'store_id',
        'merchant_no',
        'customer_no',
        'term_no',
        'email',
        'mcc_type',
        'mcc',
        'mcc_name',
        'settle_type',
        'id_card_front',
        'id_card_behind',
        'business_licence',
        'bank_card',
        'agree_ment',
        'agree_ment_url',
        'opening_permit',
        'checkstand_img',
        'shop_outside_img',
        'shop_inside_img',
        'others',
        'others_url',
        'settle_id_card_front',
        'settle_id_card_behind',
        'letter_of_authorization',
        'province_code',
        'province_name',
        'city_code',
        'city_name',
        'area_code',
        'area_name',
        'settle_province_code',
        'settle_province_name',
        'settle_city_code',
        'settle_city_name',
        'openning_bank_code',
        'openning_bank_name',
        'clearing_bank_code',
        'audit_msg',
        'audit_status',
        'wei_mer',
        'ali_mer'
    ];

}
