<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreMccs extends Model
{

    protected $fillable = [
        'pid',
        'mcc_name',
        'mcc_code',
    ];

    public function childUser()
    {
        return $this->hasMany('App\Models\StoreMccs', 'pid', 'mcc_code');
    }
    public function children()
    {
        return $this->childUser()->select('pid','mcc_name', 'mcc_code')->with('children');
    }
}
