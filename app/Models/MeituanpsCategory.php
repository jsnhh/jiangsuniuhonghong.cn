<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MeituanpsCategory extends Model
{

    protected $table = 'meituanps_categorys';

    protected $fillable = [
        'p_id',
        'code',
        'name',
        'type'
    ];

    public function childUser()
    {
        return $this->hasMany('App\Models\MeituanpsCategory', 'p_id', 'id');
    }

    public function children()
    {
        return $this->childUser()->with('children');
    }

}
