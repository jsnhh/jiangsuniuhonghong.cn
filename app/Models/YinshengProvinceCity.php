<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class YinshengProvinceCity extends Model
{
    protected $table = 'yinsheng_province_cities';

    protected $fillable = [
        'area_code',
        'area_name',
        'area_parent_id',
        'area_type',
        'code'
    ];

    protected $primaryKey = "id";
}
