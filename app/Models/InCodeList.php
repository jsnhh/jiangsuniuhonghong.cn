<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InCodeList extends Model
{
    protected $table = "in_code_list";

    protected $fillable = [
        'config_id',
        'store_id',
        'merchant_id',
        "device_id",
    ];
}
