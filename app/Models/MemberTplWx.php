<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberTplWx extends Model
{


    protected $table = "member_tpl_wxs";
    protected $fillable = [
        'store_id',
        'config_id',
        'custom_a',
        'custom_b',
        'custom_c',
        'custom_d',
        'wx_status',
        'wx_status_desc',
    ];
}
