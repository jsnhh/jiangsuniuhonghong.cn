<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class UserInfos extends Model
{
    protected $fillable = [
        'user_id',
        'full_name' ,
        'id_card',
        'bank_user_name',
        'card_no',
        'id_card_front',
        'id_card_back',
        'bank_card_front',
    ];

}