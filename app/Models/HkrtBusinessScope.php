<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HkrtBusinessScope extends Model
{
    protected $table = 'hkrt_business_scope';

    protected $fillable = [
        'code',
        'name'
    ];
}
