<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuzhouStore extends Model
{
    //

    protected $table="shuzou_stores";
    protected $fillable = [
        'store_id',
        'config_id',
        'MerchantId'
    ];
}
