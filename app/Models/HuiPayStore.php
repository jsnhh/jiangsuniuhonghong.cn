<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HuiPayStore extends Model
{
    protected $table = 'hui_pay_stores';

    protected $fillable = [
        'apply_id',
        'user_cust_id',
        'config_id',
        'store_id',
        'status',
        'shop_id',
        'device_id',
        'mer_id',
    ];

}
