<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShoppingOrder extends Model
{

    protected $table = "shopping_order";

    protected $fillable = [
        'order_id',
        'user_id',
        'goods_id',
        'quantity',
        'price',
        'total_amount',
        'deduction',
        'amount',
        'address_id',
        'express_name',
        'express_id',
        'tracking_code',
        'order_status',
        'order_time',
        'recipient',
        'address',
        'phone',
        'remark',
        'payment',
        'balance_amount',
        'integral_amount'

    ];

}
