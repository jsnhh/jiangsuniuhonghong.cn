<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EasePayMcc extends Model
{
    protected $table = 'ease_pay_mcc';

    protected $fillable = [
        'name',
        'code',
        'p_code',
    ];

}
