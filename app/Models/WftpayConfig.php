<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class WftpayConfig extends Model
{
    protected $table = 'wftpay_configs';

    protected $fillable = [
        'config_id',
        'url',
        'version',
        'sign_type',
        'public_rsa_key',
        'private_rsa_key',
        'wx_appid',
        'wx_secret'
    ];


}
