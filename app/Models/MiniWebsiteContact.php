<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MiniWebsiteContact extends Model
{
    protected $table = 'mini_website_contact';

    protected $fillable = [
        'config_id',
        'store_id',
        'store_name',
        's_time',
        'e_time',
        'cover_url',
        'telephone',
        'email',
        'website',
        'address',
        'status',
        'lat',
        'lng'
    ];

}
