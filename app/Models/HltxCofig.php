<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class HltxCofig extends Model
{
    //
    protected $table = 'hltx_configs';

    protected $fillable = [
        'config_id',
        'isvid',
        'qd',
        'public_key',
        'private_key',
        'wx_appid',
        'wx_secret',
        'subscribe_appid',
        'pay_appid',
        'ali_pid',
        'wx_channelid',
        'status',
        'created_at ',
        'updated_at'
    ];

}
