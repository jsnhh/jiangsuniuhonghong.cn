<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EasypayProject extends Model
{
    protected $table = 'easypay_project';

    protected $fillable = [
        'config_id',
        'project_id',
        'project_name'
    ];
}
