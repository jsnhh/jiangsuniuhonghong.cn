<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class UserArchitectureRewards extends Model
{

    protected $table = "user_architecture_rewards";
    protected $fillable = [
        'user_id',
        'reward_amount',
        'sub_user_id',
        'start_time',
        'end_time',
        'sub_user_name',
        'user_name'
    ];

}
