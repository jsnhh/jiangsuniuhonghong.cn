<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class UserShoppingReward extends Model
{

    protected $table = "user_shopping_reward";
    protected $fillable = [
        'user_id',
        'p_user_id',
        'amount',
        'quantity',
        'p_user_name'
    ];

}
