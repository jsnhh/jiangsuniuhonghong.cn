<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class YinshengConfig extends Model
{

    protected $table = 'yinsheng_configs';

    protected $fillable = [
        'config_id',
        'wx_appid',
        'wx_secret',
        'pfx_password',
        'private_key',
        'public_key',
        'partner_id'
    ];

}
