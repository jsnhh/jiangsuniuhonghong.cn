<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QrListInfo extends Model
{
    //
    protected $fillable = [
        'cno',
        'code_num',
        'user_id',
        'store_id',
        'merchant_id',
        'status',
        'status_desc',
        'is_buy',
        'del_flag',
        'bind_time'
    ];

}
