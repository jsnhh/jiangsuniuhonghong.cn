<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CustomerAppletsAliPayCoupons extends Model
{
    protected $table      = "customerapplets_aliPay_coupons";
    protected $primaryKey = "id";

    /**
     * 查询门店下面的卡券
     * @param $store_id 门店id
     * @param $status   卡券状态
     * @return mixed
     */
    public function getStoreCoupon($store_id,$status = ""){
        $date = date("Y-m-d H:i:s",time());
        if(!empty($status)){
            if($status == 1){
                //商家卡券状态进行中的
                $couponResult = $this
                    ->select("id","store_id","tpl_id","coupon_stock_name","available_begin_time","available_end_time","discount_amount","transaction_minimum","serialNumber","coupon_message","created_at")
                    ->where(['store_id' => $store_id,'coupon_source' => 2])
                    ->where('available_end_time','>',$date)
                    ->get();
            }else{
                //商家卡券状态已失效的
                $couponResult = $this
                    ->select("id","store_id","tpl_id","coupon_stock_name","available_begin_time","available_end_time","discount_amount","transaction_minimum","serialNumber","coupon_message","created_at")
                    ->where(['store_id' => $store_id,'coupon_source' => 2])
                    ->where('available_end_time','<=',$date)
                    ->get();
            }
        }else{
            $couponResult = $this
                ->select("id","store_id","tpl_id","coupon_stock_name","available_begin_time","available_end_time","discount_amount","transaction_minimum","serialNumber","coupon_message","created_at")
                ->where(['store_id' => $store_id,'coupon_source' => 2])
                ->where('available_end_time','>',$date)
                ->get();
        }
        return $couponResult;
    }
}
