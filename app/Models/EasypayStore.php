<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EasypayStore extends Model
{
    protected $table = 'easypay_stores';

    protected $fillable = [
        'config_id',
        'store_id',
        'mer_code',
        'opera_trace',
        'mer_trace',
        'term_mercode',
        'term_termcode',
        'audit_status',
        'audit_msg',
        'username',
        'bang_ding_commer_code',
        'bang_ding_ter_mno',
        'alipay_id',
        'wechat_id'
    ];

}
