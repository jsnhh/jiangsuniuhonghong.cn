<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MeituanpsCity extends Model
{

    protected $table = 'meituanps_citys';

    protected $fillable = [
        'city_code',
        'city_name'
    ];

}
