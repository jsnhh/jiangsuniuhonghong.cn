<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UpPackage extends Model
{
    protected $table = 'up_packages';

    protected $fillable = [
        'token',
        'version',
        'description'
    ];

}
