<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberType extends Model
{


    protected $table = "member_types";
    protected $fillable = [
        'store_id',
        'type',
        'type_desc',
        'pay_amount',
    ];
}
