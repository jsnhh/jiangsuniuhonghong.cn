<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VbillConfig extends Model
{
    protected $table = 'vbill_configs';

    protected $fillable = [
        'config_id',
        'orgId',
        'privateKey',
        'sxfpublic',
        'wx_appid',
        'wx_secret',
        'wx_channel_appid',
    ];

    // 获取信息
    public function getVbillConfigInfo()
    {
        return $this->where(['config_id' => 1234])->first();
    }

}
