<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdaPayStore extends Model
{
    protected $table = 'ada_pay_stores';

    protected $fillable = [
        'config_id',
        'store_id',
        'cus_id',
        'audit_status',
        'audit_msg'
    ];

}
