<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QfpayStore extends Model
{
    protected $table = 'qfpay_stores';

    protected $fillable = [
        'config_id',
        'store_id',
        'mchid',
        'status'
    ];


}
