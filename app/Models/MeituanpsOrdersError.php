<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MeituanpsOrdersError extends Model
{

    protected $table = 'meituanps_orders_errors';

    protected $fillable = [
        'delivery_id',
        'mt_peisong_id',
        'order_id',
        'exception_id',
        'exception_code',
        'exception_descr',
        'courier_name',
        'courier_phone',
        'appkey',
        'error_time'
    ];

}
