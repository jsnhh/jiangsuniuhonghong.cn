<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DadaConfig extends Model
{

    protected $table = 'dada_configs';

    protected $fillable = [
        'config_id',
        'store_id',
        'app_key',
        'app_secret',
        'source_id'
    ];

}
