<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HkrtStore extends Model
{
    protected $table = 'hkrt_stores';

    protected $fillable = [
        'config_id',
        'store_id',
        'merch_no',
        'agent_apply_no',
        'status'
    ];

}
