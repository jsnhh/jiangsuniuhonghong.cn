<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BulletinBoard extends Model
{
    //
    protected $table = 'bulletin_boards';

    protected $fillable = [
        'title',
        'desc',
        'enclosure',
        'user_id',
    ];
}
