<?php
/**
 * Created by PhpStorm.
 * User: dmk
 * Date: 2017/1/3
 * Time: 15:16
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{


    protected $fillable = [
        'pid',
        'name',
        'display_name',
        'guard_name',
        'path',
        'component',
        'menu_type',
        'perms',
        'icon',
        'order_num'
    ];


    public function children()
    {
        return $this->childPermi()->with('children');
    }

    public function childPermi()
    {
        return $this->hasMany('App\Models\Permission', 'pid', 'id');
    }
}