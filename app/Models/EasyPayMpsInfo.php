<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EasyPayMpsInfo extends Model
{
    protected $table = 'easypay_mps_infos';

    protected $fillable = [
        'store_id',
        'tx_code',
        'person_name',
        'enterprise_name',
        'ident_type_code',
        'ident_no',
        'mobile_phone',
        'authentication_mode',
        'landline_phone',
        'transactor_name',
        'transactor_ident_type_code',
        'transactor_ident_no',
        'address',
        'created_step',
        'template_id',
        'user_id',
        'merchant_id',
        'project_code',
        'contract_no',
        'contract_state',
        'remark',
    ];

}
