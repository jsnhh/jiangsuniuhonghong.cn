<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WechatMerchantCashCouponConfig extends Model
{
    protected $table = 'wechat_merchant_cash_coupon_config';
    protected $primaryKey = "id";

    private function getMap($input)
    {
        $map['config_id'] = 1234;

        return $map;
    }

    public function getWechatCashCouponConfig($input = [])
    {
        $map = $this->getMap($input);

        return $this->where($map)->first();
    }

    public function saveWechatCashCouponConfig($data)
    {
        return $this->insert($data);
    }

    public function updateWechatCashCouponConfig($input, $data)
    {
        $map = $this->getMap($input);

        return $this->where($map)->update($data);
    }

    public function getInfo()
    {
        $map['config_id'] = 1234;

        return $this->where($map)->first();
    }

}