<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantRecharge extends Model
{
    protected $table = 'merchant_recharges';

    protected $fillable = [
        'merchant_id',
        'user_id',
        'amount',
        'remark',
        'order_id',
        'status',
        'pay_type',
        'end_time',
        'pay_way'
    ];
}
