<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AllinPayConfig extends Model
{
    protected $table = 'allinpay_config';

    protected $fillable = [
        'config_id',
        'org_id',
        'appid',
        'wx_appid',
        'wx_secret'
    ];

}
