<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EasePayStore extends Model
{
    protected $table = 'ease_pay_stores';

    protected $fillable = [
        'store_id',
        'sub_merchant_id',
        'request_id',
        'contact_email',
        'mcc_id',
        'mcc_name',
        'profession',
        'profession_name',
        'wechat_applet',
        'alipay_id',
        'alipay_status',
        'alipay_status_desc',
        'ali_report_terminal_no',
        'ali_report_serial_no',
        'wechat_id',
        'wechat_status',
        'wechat_status_desc',
        'wx_request_id',
        'wx_pay_status',
        'wx_pay_status_desc',
        'wx_report_serial_no',
        'wx_report_terminal_no',
        'certificate_supplement_url',
        'audit_status',
        'audit_msg',


    ];

}
