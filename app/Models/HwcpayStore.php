<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class HwcpayStore extends Model
{
    protected $table = 'hwcpay_stores';

    protected $fillable = [
        'config_id',
        'store_id',
        'mch_id', //门店号
        'merchant_name',
        'merchant_num', //商户号
        'status'
    ];


}
