<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRateProfit extends Model
{
    protected $table = 'user_rate_profit';

    protected $fillable = [
        'pid',
        'user_id',
        'level',
        'profit',
    ];
}
