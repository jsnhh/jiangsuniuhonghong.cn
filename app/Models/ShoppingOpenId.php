<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShoppingOpenId extends Model
{
    protected $table = "shopping_open_id";

    protected $fillable = [
        'open_id',
    ];
}
