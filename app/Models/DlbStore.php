<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DlbStore extends Model
{
    //
    protected $fillable = [
        'config_id',
        'store_id',
        'mch_num',
        'shop_num',
        'machine_num',
        'created_at',
        'updated_at'
    ];
}
