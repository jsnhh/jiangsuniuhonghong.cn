<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MeituanpsCancels extends Model
{

    protected $table = 'meituanps_cancels';

    protected $fillable = [
        'store_id',
        'order_id',
        'delivery_id',
        'mt_peisong_id',
        'cancel_reason_id',
        'cancel_reason'
    ];

}
