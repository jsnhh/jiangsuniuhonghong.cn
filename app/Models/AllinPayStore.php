<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AllinPayStore extends Model
{
    protected $table = 'allinpay_stores';

    protected $fillable = [
        'config_id',
        'store_id',
        'cus_id',
        'termno',
        'audit_status',
        'audit_msg',
        'appid',
        'private_key'
    ];

}
