<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityStoreRate extends Model
{

    protected $table = "activity_store_rate";

    protected $fillable = [
        'store_id',
        'store_name',
        'config_id',
        'user_id',
        'company',
        'time_start',
        'time_end',
        'total_amount_e',
        'total_amount_s',
        'alipay',
        'weixin',
    ];

}
