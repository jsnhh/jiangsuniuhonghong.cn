<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class LinkageStores extends Model
{

    protected $table = 'linkage_stores';

    protected $fillable = [
        'config_id',
        'store_id',
        'acqMerId',
        'merId',
        'subAppid',
        'subMchId',
        'status'
    ];

}
