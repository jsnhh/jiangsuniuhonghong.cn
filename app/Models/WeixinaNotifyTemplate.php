<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WeixinaNotifyTemplate extends Model
{
    //
    protected $table = 'weixina_notify_templates';

    protected $fillable = [
        'config_id',
        'type',
        'template_id',
    ];


}
