<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProvinceCity extends Model
{
    protected $table = 'province_cities';

    protected $fillable = [
        'area_code',
        'area_name',
        'area_parent_id',
        'area_type'
    ];

    protected $primaryKey = "id";
}
