<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CustomerAppletsStore extends Model
{

    protected $table = 'customerapplets_stores';

    protected $fillable = [
        'type',
        'store_id',
        'store_applets_desc',
        'template_id',
        'user_version',
        'nick_name',
        'license',
        'name',
        'code',
        'code_type',
        'legal_persona_wechat',
        'legal_persona_name',
        'component_phone',
        'created_step',
        'logo',
        'first_class',
        'second_class',
        'first_id',
        'second_id',
    ];

}
