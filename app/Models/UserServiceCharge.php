<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class UserServiceCharge extends Model
{

    protected $table = "user_service_charge";
    protected $fillable = [
        'user_id',
        'user_name',
        'cost',
        'transaction_amount',
        'commission_month',
    ];

}
