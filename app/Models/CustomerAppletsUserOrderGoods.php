<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerAppletsUserOrderGoods extends Model
{

    protected $table      = "customerapplets_user_order_goods";
    protected $primaryKey = "id";

    // 搜索条件
    private function getMap($input)
    {
        $map = [];
        // 订单id
        if (!empty($input['order_id'])) {
            $map['a.order_id'] = $input['order_id'];
        }

        return $map;
    }

    // 查询订单信息
    public function getOrderGoodsInfo($input)
    {
        $map = $this->getMap($input);

        return $this->from('customerapplets_user_order_goods as a')->where($map)
            ->leftjoin('goods as b', 'a.good_id', 'b.id')
            ->selectRaw("
                 a.good_id, a.good_price, a.good_num, a.good_money, a.is_print, b.name
            ")
            ->get();
    }

    public function updateOrderGoodsByOrderId($order_id, $data)
    {
        return $this->where(['order_id' => $order_id])->update($data);
    }

}
