<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettlementMonthList extends Model
{
    protected $table = 'settlement_month_list';

    protected $fillable = [
        'user_id',
        'user_name',
        'total_amount',
        'transaction_amount',
        'start_time',
        'end_time',
        'settlement_type',
        'rate',
        'p_user_id'
    ];

}
