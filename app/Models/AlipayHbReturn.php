<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AlipayHbReturn extends Model
{
    //
    protected $table="alipay_hb_return";
    protected $fillable = [
        'store_id',
        'num',
        'rate',
        'r_type',
        'zc_type',
    ];
}
