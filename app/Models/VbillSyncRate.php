<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class VbillSyncRate extends Model
{

    protected $table = 'vbill_sync_rates';

    protected $fillable = [
        'config_id',
        'store_id',
        'rate',
        'new_rate',
        'ori_type',
        'act_status'
    ];

}
