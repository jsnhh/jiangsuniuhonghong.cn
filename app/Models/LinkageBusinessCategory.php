<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class LinkageBusinessCategory extends Model
{

    protected $table = 'linkage_business_categorys';

    protected $fillable = [
        'categoryId',
        'categoryName'
    ];

}
