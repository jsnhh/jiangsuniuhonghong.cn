<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class AlipayBlueseaList extends Model
{
    protected $fillable = [
        'config_id',
        'store_id',
        'biz_scene',
        'store_name',
        'order_id',
        'memo',
        'merchant_no',
        'app_id',
        'user_id',
        'status',
        'type',
    ];

}
