<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CcBankPayStore extends Model
{
    protected $table = 'ccbank_pay_stores';

    protected $fillable = [
        'config_id',
        'store_id',
        'cust_id',
        'pos_id',
        'branch_id',
        'termno1',
        'termno2',
        'public_key',
        'audit_status',
        'audit_msg'

    ];

}
