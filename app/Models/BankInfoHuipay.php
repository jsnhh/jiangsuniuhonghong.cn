<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankInfoHuipay extends Model
{
    protected $table = 'bank_info_huipay';

    protected $fillable = [
        'bank_id', //银行编码
        'bank_name', //银行1名称
        'bank_short_chn', //银行中文简称
        'bank_short_eng', //银行英文简称
        'stat_flag', //卡状态
        'public_flag', //是否支持对公 为1 支持
        'cash_t0_flag', //是否支持T0取现，对公支持银行
        'pin_yin', //银行拼音
        'correspondent_flag', //1：不需要联行号，其他值需要联行号
        'bank_logo_pic', //银行logo图片
    ];

}
