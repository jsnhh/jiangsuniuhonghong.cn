<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserKeyUrl extends Model
{
    //
    protected $fillable = [

        'user_id',
        'user_name',
        'private_key',
        'public_key',
        'notify_url'
    ];


}
