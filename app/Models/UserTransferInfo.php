<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class UserTransferInfo extends Model
{

    protected $table = "user_transfer_info";
    protected $fillable = [
        'user_id',
        'sub_user_id',
        'transfer_type',
        'transfer_num',
        'code_start',
        'code_end',
        'sub_user_name'
    ];

}
