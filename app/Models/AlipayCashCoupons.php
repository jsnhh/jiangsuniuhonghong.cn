<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class AlipayCashCoupons extends Model
{
    protected $fillable = [
        'config_id',
        'store_id',
        'template_id',
        'voucher_type',
        'brand_name',
        'publish_start_time',
        'publish_end_time',
        'voucher_start',
        'voucher_end',
        'out_biz_no',
        'voucher_description',
        'amount',
        'floor_amount',
        'voucher_available_time',
        'voucher_quantity',
    ];

}