<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class AlipayCashCards extends Model
{
    protected $fillable = [
        'config_id',
        'store_id',
        'template_id',
        'voucher_id',
        'alipay_user_id',
        'out_biz_no',
        'status',
        'brand_name',
        'use_time',
    ];

}