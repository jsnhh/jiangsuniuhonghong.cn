<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HnaPayStore extends Model
{
    protected $table = 'hnapay_stores';

    protected $fillable = [

        'config_id',
        'store_id',
        'merchant_no',
        'mcc_type',
        'mcc',
        'mcc_name',
        'email',
        'bank_id',
        'settle_way',
        'identity_phone',
        'protocol_photo',
        'protocol_photo_id',
        'lawyer_cert_front_photo_id',
        'lawyer_cert_back_photo_id',
        'main_photo_id',
        'store_hall_photo_id',
        'store_cashier_photo_id',
        'license_photo_id',
        'opening_license_account_photo_id',
        'bank_card_front_photo_id',
        'authorized_cert_front_photo_id',
        'authorized_cert_back_photo_id',
        'settle_auth_letter_photo_id',
        'hold_identity_pic_id',
        'merchant_info_modify_photo',
        'merchant_info_modify_photo_id',
        'province_code',
        'province_name',
        'city_code',
        'city_name',
        'area_code',
        'area_name',
        'bank_province_name',
        'bank_province_code',
        'bank_city_name',
        'bank_city_code',
        'audit_msg',
        'audit_status',
        'wei_mer',
        'ali_mer'
    ];

}
