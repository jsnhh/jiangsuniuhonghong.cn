<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberXyList extends Model
{


    protected $table = 'member_yxlists';

    protected $fillable = [
        'yx_type',
        'yx_id',
        'yx_name',
        'yx_desc',
        'merchant_id',
    ];
}
