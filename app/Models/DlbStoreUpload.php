<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DlbStoreUpload extends Model
{
    //
    protected $table="dlb_store_uploads";

    protected $fillable = [
        'config_id',
        'store_id',
        'mch_num',
        'attach_num', //附件编号
        'attach_name',
        'attach_type',
        'attach_url',
        'created_at',
        'updated_at'
    ];

}
