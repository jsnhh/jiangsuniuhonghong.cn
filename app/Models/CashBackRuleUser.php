<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CashBackRuleUser extends Model
{

    protected $table = "cash_back_rule_user";

    protected $fillable = [
        'id',
        'user_id',
        'user_name',
        'rule_id',
        'status',
        'user_name',
        'standard_sum',
        'first_sum',
        'again_sum',
        'lower_standard_sum',
        'lower_first_sum',
        'lower_again_sum',
        'standard_store_amt',
        'first_store_amt',
        'again_store_amt',
        'lower_standard_store_amt',
        'lower_first_store_amt',
        'lower_again_store_amt',
        'standard_total_sum',
        'first_total_sum',
        'again_total_sum',
        'lower_standard_total_sum',
        'lower_first_total_sum',
        'lower_again_total_sum',
        'total_sum',
        'created_at',
        'updated_at'
    ];

}
