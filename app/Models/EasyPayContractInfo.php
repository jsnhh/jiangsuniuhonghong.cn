<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EasyPayContractInfo extends Model
{
    protected $table = 'easypay_contract_infos';

    protected $fillable = [
        'store_id',
        'merName',
        'merLegal',
        'legalCode',
        'legalPhone',
        'merAddr',
        'contractDateYear',
        'contractDateMonth',
        'contractDateDay',
        'bizName',
        'businLic',
        'bizAddr',
        'linkMan',
        'linkmanPhone',
        'accName',
        'bankName',
        'account',
        'chargeTypeDebit',
        'chargeTypeDebitMax',
        'chargeTypeCredit',
        'checkSameCard',
        'chargeTypeUnionpayDebit',
        'chargeTypeUnionpayDebitMax',
        'chargeTypeUnionpayCredit',
        'chargeTypeUnionpayDisDebit',
        'chargeTypeUnionpayDisDebitMax',
        'chargeTypeUnionpayDisCredit',
        'chargeTypeWxpay',
        'chargeTypeAlipay',
        'checkSettleT1',
        'checkSettleD1',
        'checkSettleD1Ratio',
        'd1SettlementRatio',
        'checkSettleD1Fee',
        'd1SettlementFee',
        'checkSettleD0',
        'd0SettlementRatio',
        'manager',
        'managerPhone',
        'contractDate',
        'templateId',
        'contractNo',
        'contractState',
        'remark',
    ];

}
