<?php
/**
 * Created by PhpStorm.
 * User: dmk
 * Date: 2017/1/3
 * Time: 14:51
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    protected  $fillable=['pid','name','display_name','guard_name','created_id'];
}