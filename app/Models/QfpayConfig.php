<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QfpayConfig extends Model
{
    protected $table = 'qfpay_configs';

    protected $fillable = [
        'config_id',
        'key',
        'code',
        'wx_appid',
        'wx_secret'
    ];


}
