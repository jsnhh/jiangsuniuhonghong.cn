<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class AdLinks extends Model
{

    protected $fillable = [
        'config_id',
        'store_id' ,
        'store_name',
        'links',
        'total_url',
    ];

}
