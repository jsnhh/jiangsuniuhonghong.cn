<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\CustomerAppletsArea;
use App\Models\CustomerAppletsUser;

class CustomerAppletsUserAddress extends Model
{
    protected $table      = "customerapplets_user_address";
    protected $primaryKey = "id";

    /**
     * 根据地址id查询某一条地址
     * @param $address_id
     * @return bool
     */
    public function getUserAddressFirst($address_id){
        if(!isset($address_id) || empty($address_id)){
            return false;
        }

        $result = $this->select("customerapplets_user_address.id","customerapplets_user_address.username","customerapplets_user_address.gender","customerapplets_user_address.user_mobile","customerapplets_user_address.address_source","customerapplets_user_address.province_id","customerapplets_user_address.city_id","customerapplets_user_address.area_id","customerapplets_user_address.longitude","customerapplets_user_address.latitude","customerapplets_areas1.area_name as province_name","customerapplets_areas1.area_code as province_code","customerapplets_areas1.dada_city_code as province_dada_city_code","customerapplets_areas2.dada_city_code as city_dada_city_code","customerapplets_areas2.area_name as city_name","customerapplets_areas3.area_name as area_name","customerapplets_user_address.address_detail","customerapplets_user_address.house_number","customerapplets_user_address.status","customerapplets_user_address.is_default","customerapplets_user_address.created_at")
            ->leftJoin("province_city_areas as customerapplets_areas1","customerapplets_areas1.id","=","customerapplets_user_address.province_id")
            ->leftJoin("province_city_areas as customerapplets_areas2","customerapplets_areas2.id","=","customerapplets_user_address.city_id")
            ->leftJoin("province_city_areas as customerapplets_areas3","customerapplets_areas3.id","=","customerapplets_user_address.area_id")
            ->where(['customerapplets_user_address.id' => $address_id])
            ->where(['customerapplets_user_address.status' => 1])
            ->first();
        if(!empty($result)){
            $result->address_detail = $result->address_detail.$result->house_number;
        }
        return $result;
    }

    /**
     * 获取用户的默认地址数据
     * @param $user_id
     * @param $store_id
     * @return bool
     */
    // public function getUserDefaultAddress($user_id,$mobile,$store_id){
    public function getUserDefaultAddress($user_id,$store_id){
        if(!isset($user_id) || empty($user_id)){
            return false;
        }

        // if(!isset($mobile) || empty($mobile)){
        //     return false;
        // }

        if(!isset($store_id) || empty($store_id)){
            return false;
        }

        // $customerAppletsUserModel = new CustomerAppletsUser();
        //
        // $user_id = $customerAppletsUserModel->getUserCollectId($store_id,$mobile);

        //先判断有没有默认的地址数据
        $addressDefault = $this->select("id")->where('user_id',$user_id)->where(['store_id' => $store_id])->where(['is_default' => 1,'status' => 1])->first();
        if($addressDefault){
            $result = $this->select("customerapplets_user_address.id","customerapplets_user_address.username","customerapplets_user_address.gender","customerapplets_user_address.user_mobile","customerapplets_user_address.longitude","customerapplets_user_address.latitude","customerapplets_user_address.address_source","customerapplets_areas1.area_name as province_name","customerapplets_areas1.dada_city_code as province_dada_city_code","customerapplets_areas2.dada_city_code as city_dada_city_code","customerapplets_areas2.area_name as city_name","customerapplets_areas3.area_name as area_name","customerapplets_user_address.address_detail","customerapplets_user_address.house_number","customerapplets_user_address.status","customerapplets_user_address.is_default","customerapplets_user_address.created_at")
                ->leftJoin("province_city_areas as customerapplets_areas1","customerapplets_areas1.id","=","customerapplets_user_address.province_id")
                ->leftJoin("province_city_areas as customerapplets_areas2","customerapplets_areas2.id","=","customerapplets_user_address.city_id")
                ->leftJoin("province_city_areas as customerapplets_areas3","customerapplets_areas3.id","=","customerapplets_user_address.area_id")
                ->where('customerapplets_user_address.user_id',$user_id)
                ->where(['customerapplets_user_address.store_id' => $store_id])
                ->where(['customerapplets_user_address.is_default' => 1,'customerapplets_user_address.status' => 1])
                ->first();

            if(!empty($result)){
                $result->address_detail = $result->address_detail.$result->house_number;
            }
        }else{
            $result = $this->select("customerapplets_user_address.id","customerapplets_user_address.username","customerapplets_user_address.gender","customerapplets_user_address.user_mobile","customerapplets_user_address.longitude","customerapplets_user_address.latitude","customerapplets_user_address.address_source","customerapplets_areas1.area_name as province_name","customerapplets_areas1.dada_city_code as province_dada_city_code","customerapplets_areas2.dada_city_code as city_dada_city_code","customerapplets_areas2.area_name as city_name","customerapplets_areas3.area_name as area_name","customerapplets_user_address.address_detail","customerapplets_user_address.house_number","customerapplets_user_address.status","customerapplets_user_address.is_default","customerapplets_user_address.created_at")
                ->leftJoin("province_city_areas as customerapplets_areas1","customerapplets_areas1.id","=","customerapplets_user_address.province_id")
                ->leftJoin("province_city_areas as customerapplets_areas2","customerapplets_areas2.id","=","customerapplets_user_address.city_id")
                ->leftJoin("province_city_areas as customerapplets_areas3","customerapplets_areas3.id","=","customerapplets_user_address.area_id")
                ->where('customerapplets_user_address.user_id',$user_id)
                ->where(['customerapplets_user_address.store_id' => $store_id])
                ->where(['customerapplets_user_address.status' => 1])
                ->first();
            if(!empty($result)){
                $result->address_detail = $result->address_detail.$result->house_number;
            }
        }
        return $result;
    }

    /**
     * 查询用户的地址
     * @param $addressData
     *        $addressData['terminal_type']
     *               如果为空，则查询的是用户下的所有地址，不管是微信还是支付宝
     * @return bool
     */
    public function getUserAddress($addressData){
        if(!isset($addressData['user_id']) || empty($addressData['user_id'])){
            return false;
        }

        if(!isset($addressData['store_id']) || empty($addressData['store_id'])){
            return false;
        }

        // if(!isset($addressData['mobile']) || empty($addressData['mobile'])){
        //     return false;
        // }
        //
        // $customerAppletsUserModel = new CustomerAppletsUser();

        // $user_id = $customerAppletsUserModel->getUserCollectId($addressData['store_id'],$addressData['mobile']);

        $result = $this->select("customerapplets_user_address.id","customerapplets_user_address.username","customerapplets_user_address.gender","customerapplets_user_address.user_mobile","customerapplets_user_address.address_source","customerapplets_areas1.area_name as province_name","customerapplets_areas2.area_name as city_name","customerapplets_areas3.area_name as area_name","customerapplets_user_address.address_detail","customerapplets_user_address.house_number","customerapplets_user_address.status","customerapplets_user_address.is_default","customerapplets_user_address.created_at")
                        ->leftJoin("province_city_areas as customerapplets_areas1","customerapplets_areas1.id","=","customerapplets_user_address.province_id")
                        ->leftJoin("province_city_areas as customerapplets_areas2","customerapplets_areas2.id","=","customerapplets_user_address.city_id")
                        ->leftJoin("province_city_areas as customerapplets_areas3","customerapplets_areas3.id","=","customerapplets_user_address.area_id")
                        ->where('customerapplets_user_address.user_id', $addressData['user_id'])
                        ->where(['customerapplets_user_address.store_id' => $addressData['store_id']]);

       if(!empty($addressData['terminal_type'])){
           if($addressData['terminal_type'] == config("api.weChat")){
               $result = $result->where(['customerapplets_user_address.address_source' => 1]);
           }else if($addressData['terminal_type'] == config("api.aliPay")){
               $result = $result->where(['customerapplets_user_address.address_source' => 2]);
           }
       }

        $result = $result->where(['customerapplets_user_address.status' => 1])->orderBy("customerapplets_user_address.created_at","desc")->paginate(20);

        foreach ($result as $key => $val){
            $val->address_detail = $val->address_detail.$val->house_number;
        }
        return $result;
    }

    /**
     * 创建用户地址数据
     * @param $addressData
     * @return array|bool
     */
    public function addUserAddress($addressData){
        if(!isset($addressData['user_id']) || empty($addressData['user_id'])){
            return false;
        }

        if(!isset($addressData['store_id']) || empty($addressData['store_id'])){
            return false;
        }

        if(!isset($addressData['terminal_type']) || empty($addressData['terminal_type'])){
            return false;
        }

        if(!isset($addressData['mobile']) || empty($addressData['mobile'])){
            return false;
        }

        $customerAppletsUserModel = new CustomerAppletsUser();

        $user_id = $customerAppletsUserModel->getUserCollectId($addressData['store_id'],$addressData['mobile']);

        //每个用户在每个店面的默认地址只能有一个
        if($addressData['is_default']){
            $userDefaultData = $this->whereIn('user_id',$user_id)->where(['store_id' => $addressData['store_id'],'is_default' => 1])->get();
            if(!empty($userDefaultData)){
                $this->whereIn('user_id',$user_id)->where(['store_id' => $addressData['store_id'],'is_default' => 1])->update(['is_default' => 2 , 'updated_at' => date("Y-m-d H:i:s",time())]);
            }
        }

        $date = date("Y-m-d H:i:s",time());
        $createdAddressData['user_id']         = $addressData['user_id'];
        $createdAddressData['username']        = $addressData['username'];
        $createdAddressData['store_id']        = $addressData['store_id'];
        $createdAddressData['address_source']  = $addressData['terminal_type'] == config("api.weChat") ? 1 : 2;
        $createdAddressData['gender']          = $addressData['gender'];
        $createdAddressData['user_mobile']     = $addressData['user_mobile'];
        $createdAddressData['province_id']     = $addressData['province_id'];
        $createdAddressData['city_id']         = $addressData['city_id'];
        $createdAddressData['area_id']         = $addressData['area_id'];
        $createdAddressData['address_detail']  = $addressData['address_detail'];
        $createdAddressData['house_number']    = isset($addressData['house_number']) ? $addressData['house_number'] : "";
        $createdAddressData['is_default']      = $addressData['is_default'] ? 1 : 2;
        $createdAddressData['longitude']       = isset($addressData['longitude']) ? $addressData['longitude'] : "";
        $createdAddressData['latitude']        = isset($addressData['latitude']) ? $addressData['latitude'] : "";
        $createdAddressData['created_at']      = $date;
        $createdAddressData['updated_at']      = $date;

        $result = $this->insert($createdAddressData);

        if($result){
            return ["status" => 200 , "message" => "创建成功"];
        }else{
            return ["status" => 202 , "message" => "创建失败"];
        }

    }

    /**
     * 修改用户地址数据
     * @param $addressData
     * @return array|bool
     */
    public function updateUserAddress($addressData){
        if(!isset($addressData['address_id']) || empty($addressData['address_id'])){
            return false;
        }

        if(!isset($addressData['user_id']) || empty($addressData['user_id'])){
            return false;
        }

        if(!isset($addressData['store_id']) || empty($addressData['store_id'])){
            return false;
        }

        if(!isset($addressData['terminal_type']) || empty($addressData['terminal_type'])){
            return false;
        }

        if(!isset($addressData['mobile']) || empty($addressData['mobile'])){
            return false;
        }

        $customerAppletsUserModel = new CustomerAppletsUser();

        $user_id = $customerAppletsUserModel->getUserCollectId($addressData['store_id'],$addressData['mobile']);

        //每个用户在每个店面的默认地址只能有一个
        if($addressData['is_default']){
            $userDefaultData = $this->whereIn('user_id',$user_id)->where(['store_id' => $addressData['store_id'],'is_default' => 1])->get();
            if(!empty($userDefaultData)){
                $this->whereIn('user_id',$user_id)->where(['store_id' => $addressData['store_id'],'is_default' => 1])->update(['is_default' => 2 , 'updated_at' => date("Y-m-d H:i:s",time())]);
            }
        }

        $date = date("Y-m-d H:i:s",time());
        $createdAddressData['username']        = $addressData['username'];
        $createdAddressData['gender']          = $addressData['gender'];
        $createdAddressData['user_mobile']     = $addressData['user_mobile'];
        $createdAddressData['province_id']     = $addressData['province_id'];
        $createdAddressData['city_id']         = $addressData['city_id'];
        $createdAddressData['area_id']         = $addressData['area_id'];
        $createdAddressData['address_detail']  = $addressData['address_detail'];
        $createdAddressData['house_number']    = isset($addressData['house_number']) ? $addressData['house_number'] : "";
        $createdAddressData['is_default']      = $addressData['is_default'] ? 1 : 2;
        $createdAddressData['longitude']       = isset($addressData['longitude']) ? $addressData['longitude'] : "";
        $createdAddressData['latitude']        = isset($addressData['latitude']) ? $addressData['latitude'] : "";
        $createdAddressData['updated_at']      = $date;

        $result = $this->where(['id' => $addressData['address_id']])->update($createdAddressData);

        if($result){
            return ["status" => 200 , "message" => "修改成功"];
        }else{
            return ["status" => 202 , "message" => "修改失败"];
        }

    }

    /**
     * 删除用户地址数据 ，不是物理删除数据，软删除
     * @param $addressData
     * @return array|bool
     */
    public function deleteUserAddress($addressData){
        if(!isset($addressData['address_id']) || empty($addressData['address_id'])){
            return false;
        }

        if(!isset($addressData['user_id']) || empty($addressData['user_id'])){
            return false;
        }

        if(!isset($addressData['store_id']) || empty($addressData['store_id'])){
            return false;
        }

        if(!isset($addressData['terminal_type']) || empty($addressData['terminal_type'])){
            return false;
        }

        $userAddressInfo = $this->where(['id' => $addressData['address_id']])->first();

        if(empty($userAddressInfo)){
            return ["status" => 202 , "message" => "该地址数据不存在"];
        }

        $result = $this->where(['id' => $addressData['address_id']])->update([
            'status'     => 2,
            'updated_at' => date("Y-m-d H:i:s",time())
        ]);

        if($result){
            return ["status" => 200 , "message" => "删除成功"];
        }else{
            return ["status" => 202 , "message" => "删除失败"];
        }

    }

}
