<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class AlipayOperation extends Model
{
    protected $table = 'alipay_operation';
    protected $fillable = [
        'config_id',
        'store_id',
        'user_id',
        'app_id',
        'store_type',
        'merchant_no',
        'alipay_account',
        'operate_type',
        'access_product_code',
        'batch_no',
        'handle_status',
        'bind_user_id',
    ];

}
