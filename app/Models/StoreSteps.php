<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreSteps extends Model
{
    //
    protected $fillable = [
        'store_id',
        'step', //1执照信息 、2账户信息 3、门店信息 4、功能信息 5、提交审核
        'remark',
        'company_type',
    ];

}
