<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HuiPayConfig extends Model
{
    protected $table = 'hui_pay_configs';

    protected $fillable = [
        'config_id',
        'wx_appid',
        'wx_secret',
        'mer_cust_id',
        'org_id',
        'private_key',
        'public_key',
    ];

}
