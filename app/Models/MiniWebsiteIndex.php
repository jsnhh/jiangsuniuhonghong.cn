<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MiniWebsiteIndex extends Model
{
    protected $table = 'mini_website_indexs';

    protected $fillable = [
        'config_id',
        'store_id',
        'pid',
        'type',
        'sort',
        'title',
        'sub_title',
        'icon_url',
        'cover_url',
        'video_url',
        'content',
        'status'
    ];

}
