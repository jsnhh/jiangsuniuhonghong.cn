<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EasySkpayConfig extends Model
{
    protected $table = 'easyskpay_configs';

    protected $fillable = [
        'config_id',
        'org_id',
        'wx_appid',
        'wx_secret'
    ];

}
