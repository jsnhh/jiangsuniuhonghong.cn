<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class AlipayAntStores extends Model
{
    protected $fillable = [
        'shop_name',
        'shop_category',
        'shop_id',
        'contact_mobile',
        'store_id',
        'ip_role_id',
    ];

}
