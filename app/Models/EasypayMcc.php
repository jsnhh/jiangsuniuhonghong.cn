<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EasypayMcc extends Model
{
    protected $table = 'eayspay_mcc';

    protected $fillable = [
        'mcc_id',
        'mcc_name',
        'mcc_parent',
        'mcc_flag',
        'mcc_remark'
    ];

}
