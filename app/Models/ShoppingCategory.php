<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShoppingCategory extends Model
{

    protected $table = "shopping_category";

    protected $fillable = [
        'name',
        'describe',
    ];

}
