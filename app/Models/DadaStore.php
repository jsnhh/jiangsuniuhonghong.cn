<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DadaStore extends Model
{

    protected $table = 'dada_stores';

    protected $fillable = [
        'source_id',
        'store_id',
        'origin_shop_id',
        'shop_no',
        'phone',
        'business',
        'lng',
        'lat',
        'station_name',
        'contact_name',
        'station_address',
        'city_name',
        'area_name',
        'id_card',
        'username',
        'password',
        'status'
    ];

}
