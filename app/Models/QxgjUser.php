<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QxgjUser extends Model
{
    //
    protected $fillable = [
        'id',
        'user_id',
        'name',
        'phone',
        'document_type',
        'id_number',
        'types_work_name',
        'is_sign',
    ];

}
