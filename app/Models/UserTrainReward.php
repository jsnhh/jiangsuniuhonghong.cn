<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class UserTrainReward extends Model
{

    protected $table = "user_train_reward";
    protected $fillable = [
        'user_id',
        'user_name',
        'sub_user_id',
        'transaction_amount',
        'train_reward_amount',
        'train_reward_rate',

    ];

}
