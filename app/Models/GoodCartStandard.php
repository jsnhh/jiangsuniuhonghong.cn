<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodCartStandard extends Model
{
    protected $table      = "goods_cart_standards";
    protected $primaryKey = "id";

}
