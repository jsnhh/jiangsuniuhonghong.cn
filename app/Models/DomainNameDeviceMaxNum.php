<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DomainNameDeviceMaxNum extends Model
{
    protected $table      = "domain_name_device_max_num";
    protected $primaryKey = "id";

    private function getDomainNameDeviceMaxNumMap($input)
    {
        $map = [];

        // 域名
        if (!empty($input['domain_name'])) {
            $map['domain_name'] = $input['domain_name'];
        }

        return $map;
    }

    // 域名设定设备数量
    public function getDomainNameDeviceMaxNum($input)
    {
        $map = $this->getDomainNameDeviceMaxNumMap($input);

        return $this->where($map)->select(["device_max_num"])->first();
    }
}