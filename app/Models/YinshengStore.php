<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class YinshengStore extends Model
{

    protected $table = 'yinsheng_stores';

    protected $fillable = [
        'config_id',
        'store_id',
        'mch_type',
        'mcc_cd',
        'mcc_name',
        'province_code',
        'city_code',
        'area_code',
        'province_name',
        'city_name',
        'area_name',
        'is_settInPlatAcc',
        'bus_open_type',
        'd_calc_type',
        'c_calc_type',
        'd_calc_val',
        'd_stlm_max_amt',
        'd_fee_low_limit',
        'c_calc_val',
        'c_fee_low_limit',
        'sys_flowId',
        'cust_id',
        'mer_code',
        'status',
        'aduit_status',
        'audit_msg',
        'notify_type',
        'remark',
        'business_application_img',
        'change_sys_flowId',
        'nucc_wechat_mer',
        'cups_wechat_mer',
        'nucc_alipay_mer',
        'cups_alipay_mer'

    ];

}
