<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberCzList extends Model
{


    protected $table = 'member_cz_lists';

    protected $fillable = [
        'store_id',
        'store_name',
        'ways_source_desc',
        'ways_source',
        'company',
        'rate',
        'cz_money',//'充值支付金额',
        'cz_s_money',//'充值送金额',
        'pay_status',
        'pay_time',
        'cz_type',
        'cz_type_desc',
        'total_amount',//'订单总金额',
        'fee_amount',//'手续费',
        'receipt_amount',//'商家实收',
        'out_trade_no',
        'mb_id',//会员ID
        'mb_phone',//会员手机号
    ];
}
