<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\GoodsAttribute;
use App\Models\GoodsCategory;
use App\Models\GoodsStandard;
use App\Models\GoodsTag;
use App\Models\GoodCart;
use App\Models\GoodCartStandard;

class Goods extends Model
{

    protected $table = 'goods';

    protected $fillable = [
        'store_id',
        'cid',
        'c_name',
        'bar_code',
        'name',
        'price',
        'main_image',
        'sub_images',
        'detail',
        'subtitle',
        'tag_ids', //商品标签id集,多个用,隔开
        'standard_ids', //商品规格id集,多个用,隔开
        'attribute_ids', //商品属性id集,多个用,隔开
        'unit',
        'is_recommend',
        'stock',
        'status',
        'sort'
    ];


    /**
     * 查询该门店下的商品
     * @param String $user_id
     * @param $store_id
     * @param $order_id
     * @return array
     */
    public function getGoods($user_id = "",$store_id, $order_id = ""){

        if(!isset($store_id) || empty($store_id)){
            return ["status" => 4002,"message" => "store_id参数不可为空"];
        }

        //查询推荐的商品
        $goodRecommendResult = $this->select("id")->where(['store_id' =>$store_id,'is_recommend' => 1,'status' => 1])->get();
        $goodRecommendId = [];
        if(!empty($goodRecommendResult)){
            foreach ($goodRecommendResult as $k => $v){
                array_push($goodRecommendId,$v['id']);
            }
        }

        //按照分类进行分组
        $goodResult = $this->select("cid",DB::raw('group_concat(id) as good_id'))->where(['store_id' => $store_id,'status' => 1])->groupBy('cid')->orderBy("created_at","desc")->get();

        // 2021-03-18 18:03
        // 数据为空的话给默认数据（为了微信创建小程序审核通过）
        if ($goodResult->isEmpty()) {
            $goodResult = $this->select("cid",DB::raw('group_concat(id) as good_id'))->where(['store_id' => '1234','status' => 1])->groupBy('cid')->orderBy("created_at","desc")->get();
        }

        $goodResultCollect = [];

        if(!empty($goodResult)){
            foreach ($goodResult as $k => $v){
                $goodResultCollect[$k] = ['cid' => $v['cid'],'is_recommend' => 0,'good_id' => $v['good_id']];
            }
            if(!empty($goodRecommendId)){
                array_unshift($goodResultCollect,["cid" => "推荐" ,'is_recommend' => 1,"good_id" => implode(",",$goodRecommendId)]);
            }
        }

        if(!empty($goodResultCollect)){

            $goodsCategoryModel = new GoodsCategory();
            $goodsTagModel      = new GoodsTag();
            $goodCartModel      = new GoodCart();

            foreach ($goodResultCollect as $key => $val){

                //获取分类的名称
                if(is_int($val['cid']) && is_numeric($val['cid'])){
                    $goodResultCollect[$key]['cate_name'] = $goodsCategoryModel->where(['id' => $val['cid'],'store_id' => $store_id,'status' => 1])->value("name");
                    // 增加默认分类
                    if (empty($goodResultCollect[$key]['cate_name'])) {
                        $goodResultCollect[$key]['cate_name'] = $goodsCategoryModel->where(['id' => $val['cid'],'store_id' => 1234,'status' => 1])->value("name");
                    }
                }else{
                    $goodResultCollect[$key]['cate_name'] = $val['cid'];
                }

                $good_id_array    = explode(",",$val['good_id']);

                $good_info_array = [];

                foreach ($good_id_array as $good_key => $good_val){

                    //获取商品信息
                    if (empty($order_id)) {
                        $good_info = $this->select("id","store_id","cid","bar_code","name","price","main_image","tag_ids","standard_ids","attribute_ids","unit","is_recommend","stock","status","created_at")
                            ->where(['id' => $good_val,'status' => 1])->first();
                    } else {
                        // DB::enableQueryLog();  //开启日志
                        $map['goods.id'] = $good_val;
                        $map['goods.status'] = 1;
                        $good_info = $this
                            ->select("goods.id","goods.store_id","goods.cid","goods.bar_code","goods.name","goods.price","goods.main_image",
                                "goods.tag_ids","goods.standard_ids","goods.attribute_ids", "goods.unit","goods.is_recommend","goods.stock",
                                "goods.status","goods.created_at", DB::raw('ifnull(customerapplets_user_order_goods.good_num, 0) as good_num'))
                            ->leftjoin('customerapplets_user_order_goods', function ($join) use ($order_id) {
                                $join->on('goods.id', '=', 'customerapplets_user_order_goods.good_id');
                                $join->where('customerapplets_user_order_goods.order_id', '=', $order_id);
                            })
                            ->where($map)->first();
                        // var_dump(DB::getQueryLog());die; //打印日志
                    }


                    if(!empty($user_id)){
                        if(empty($good_info->standard_ids)){
                            $cartGoodResult = $goodCartModel->where(['user_id' => $user_id,'store_id' => $store_id,'good_id' => $good_val])->first();
                            if(!empty($cartGoodResult)){
                                $good_info->cart_good_number = $cartGoodResult->num;
                            }else{
                                $good_info->cart_good_number = 0;
                            }
                        }else{
                            $good_info->cart_good_number = 0;
                        }
                    }else{
                        $good_info->cart_good_number = 0;
                    }

                    //判断商品标签是否为空
                    if(!empty($good_info->tag_ids)){
                        $good_tag_id = explode(",",$good_info->tag_ids);

                        $good_tag_array = [];

                        foreach ($good_tag_id as $good_tag_key => $good_tag_val){

                            //获取商品标签名称
                            $name = $goodsTagModel->where(['id' => $good_tag_val,'store_id' => $store_id,'status' => 1])->value("name");

                            array_push($good_tag_array,$name);
                        }

                        $good_info->tag_name = $good_tag_array;

                    }else{
                        $good_info->tag_name = "";
                    }
                    unset($good_info->tag_ids);

                    //月销售量
                    $good_info->month_number = 10;

                    array_push($good_info_array,$good_info);
                }

                $goodResultCollect[$key]['children'] = $good_info_array;

                unset($goodResultCollect[$key]['good_id']);

                // 如果分类没有的话删除商品
                if (empty($goodResultCollect[$key]['cate_name'])) {
                    unset($goodResultCollect[$key]);
                    // array_splice($goodResultCollect, $key, 1);
                }
            }

            $goodResultCollect = array_merge($goodResultCollect);

            return ["status" => 200,"message" => $goodResultCollect];
        }else{
            return ["status" => 202,"message" => "数据为空"];
        }
    }

    /**
     * 获取商品规格信息
     * @param string $user_id
     * @param $store_id
     * @param $good_id
     * @return array
     */
    public function getGoodStandard($user_id = "",$store_id,$good_id){

        if(!isset($store_id) || empty($store_id)){
            return ["status" => 4002,"message" => "store_id参数不可为空"];
        }

        if(!isset($good_id) || empty($good_id)){
            return ["status" => 202,"message" => "good_id参数不可为空"];
        }

        $good_info = $this->select("id","name","price","store_id","standard_ids","unit","stock","status","created_at")->where(['store_id' => $store_id,'id' => $good_id])->first();

        switch ($good_info->status){
            case 2:
                return ["status" => 202,"message" => "该商品已经下架"];
                break;
            case 3:
                return ["status" => 202,"message" => "该商品已经删除"];
                break;
        }

        $goodsStandardModel = new GoodsStandard();
        //获取商品的规格
        $goodsStandard      = $good_info->standard_ids;

        if(!empty($goodsStandard)){

            $goodsStandardArray     = explode(",",$goodsStandard);

            $goodsStandardResult    = $goodsStandardModel->whereIn("id",$goodsStandardArray)->where(['status' => 1])->get();

            $goodsStandardNameArray = [];

            $goodCartStandardResult = [];

            if(!empty($user_id)){
                //说明用户授权登录了，这时候需要把用户已经在购物车中的商品的规格查出来
                $goodCartModel  = new GoodCart();

                $goodCartResult = $goodCartModel->where(['user_id' => $user_id,'store_id' => $store_id , 'good_id' => $good_id])->first();

                if(!empty($goodCartResult)){
                    //说明该用户在该门店将该商品存到了购物车
                    $goodCartStandardModel = new GoodCartStandard();

                    $good_info->num = $goodCartResult->num;

                    $goodCartStandardResult = $goodCartStandardModel->where(['cart_id' => $goodCartResult->id,'good_id' => $good_id])->get();

                }else{

                    $good_info->num = 0;

                }
            }

            foreach ($goodsStandardResult as $key => $val){

                $val['is_cart_select'] = false;

                if(!empty($goodCartStandardResult) && count($goodCartStandardResult) > 0){

                    foreach ($goodCartStandardResult as $k => $v){

                        if($val['id'] == $v['stand_id']){

                            $val['is_cart_select'] = true;

                        }

                    }

                }

                array_push($goodsStandardNameArray,["id" => $val['id'],"name" => $val['name'],"status" => 1,"is_cart_select" => $val["is_cart_select"]]);
            }

            $good_info->standard_data = $goodsStandardNameArray;

            return ["status" => 200,"message" => $good_info];
        }else{
            return ["status" => 202,"message" => "该商品没有规格可选"];
        }

    }

    // 获取默认数据
    public function getDefaultData()
    {
        return $this->select("id","name","price","store_id","standard_ids","unit","stock","status","created_at")->where(['store_id' => '1234'])->get();
    }
}
