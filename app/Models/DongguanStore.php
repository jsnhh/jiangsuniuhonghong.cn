<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DongguanStore extends Model
{
    protected $table = 'dongguan_stores';

    protected $fillable = [
        'config_id',
        'store_id',
        'mercId',
        'status',
        'status_desc'
    ];

}
