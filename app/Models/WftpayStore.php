<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class WftpayStore extends Model
{
    protected $table = 'wftpay_stores';

    protected $fillable = [
        'config_id',
        'store_id',
        'mch_id',
        'merchant_name',
        'status'
    ];


}
