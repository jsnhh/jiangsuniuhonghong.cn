<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EasePayConfig extends Model
{
    protected $table = 'ease_pay_configs';

    protected $fillable = [
        'config_id',
        'partner_id',
        'merchant_id',
        'wx_appid',
        'wx_secret',
        'wx_distributor_no',
        'ali_distributor_no',
        'authorization_directory'
    ];

}
