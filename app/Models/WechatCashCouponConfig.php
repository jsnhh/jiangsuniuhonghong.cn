<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WechatCashCouponConfig extends Model
{
    protected $table = 'wechat_cash_coupon_config';
    protected $primaryKey = "id";

    private function getMap($input)
    {
        $map = [];

        if (!empty($input['user_id'])) {
            $map['user_id'] = $input['user_id'];
        }
        if (!empty($input['wx_merchant_id'])) {
            $map['wx_merchant_id'] = $input['wx_merchant_id'];
        }

        return $map;
    }

    public function getWechatCashCouponConfig($input = [])
    {
        $map = $this->getMap($input);

        return $this->where($map)->first();
    }

    public function saveWechatCashCouponConfig($data)
    {
        return $this->insert($data);
    }

    public function updateWechatCashCouponConfig($input, $data)
    {
        return $this->where('user_id', $input['user_id'])->update($data);
    }

}