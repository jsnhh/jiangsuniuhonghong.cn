<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VbillaStore extends Model
{
    protected $table = 'vbilla_stores';

    protected $fillable = [
        'config_id',
        'mno',
        'store_id',
        'applicationId',
        'taskStatus',
        'childNo',
        'wx_channel_appid',
        'xcx_channel_appid',
        'wxApplyNo',
        'idenStatus',
        'infoQrcode',
        'rejectCode',
        'rejectInfo',
        'authStatus',
    ];

}
