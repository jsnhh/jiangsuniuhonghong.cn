<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EasypayAreaNo extends Model
{
    protected $table = 'easypay_area_no';

    protected $fillable = [
        'code',
        'name',
        'yl_pr',
        'yl_pr_name'
    ];

}
