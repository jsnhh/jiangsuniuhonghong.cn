<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AppMerchantIndex extends Model
{

    protected $table = 'app_merchant_indexs';

    protected $fillable = [
        'pid',
        'config_id',
        'type',
        'title',
        'icon',
        'url',
        'sort'
    ];

}
