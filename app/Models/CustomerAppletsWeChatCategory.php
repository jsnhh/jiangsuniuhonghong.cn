<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerAppletsWeChatCategory extends Model
{
    protected $table = 'customerapplets_wechat_categorys';

    protected $fillable = [
        'pid',
        'name'
    ];

}
