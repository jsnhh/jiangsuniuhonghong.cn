<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChangshaStore extends Model
{
    //

    protected $table="changsha_stores";
    protected $fillable = [
        'store_id',
        'config_id',
        'ECustId',
        'DeptId',
        'StaffId',
    ];
}
