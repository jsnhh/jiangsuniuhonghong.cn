<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MeituanpsOrders extends Model
{

    protected $table = 'meituanps_orders';

    protected $fillable = [
        'shop_id',
        'delivery_id',
        'order_id',
        'mt_peisong_id',
        'outer_order_source_desc',
        'outer_order_source_no',
        'delivery_service_code',
        'pick_up_type',
        'receiver_name',
        'receiver_address',
        'receiver_phone',
        'receiver_province',
        'receiver_city',
        'receiver_country',
        'receiver_town',
        'receiver_detail_address',
        'receiver_lng',
        'receiver_lat',
        'coordinate_type',
        'goods_value',
        'goods_height',
        'goods_width',
        'goods_length',
        'goods_weight',
        'goods_detail',
        'goods_pickup_info',
        'goods_delivery_info',
        'expected_pickup_time',
        'expected_delivery_time',
        'order_type',
        'poi_seq',
        'note',
        'cash_on_delivery',
        'cash_on_pickup',
        'invoice_title',
        'road_area',
        'destination_id',
        'status',
        'operate_time'
    ];

}
