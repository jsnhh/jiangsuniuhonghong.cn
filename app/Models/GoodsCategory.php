<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class GoodsCategory extends Model
{

    protected $table = 'goods_categorys';

    protected $fillable = [
        'pid',
        'store_id',
        'name',
        'status',
        'sort'
    ];

    public function childUser()
    {
        return $this->hasMany('App\Models\GoodsCategory', 'pid', 'id')->where('status', 1);
    }

    public function children()
    {
        return $this->childUser()->with('children');
    }

    // 获取默认数据
    public function getDefaultData()
    {
        return $this->where(['store_id' => '1234'])->get();
    }


}
