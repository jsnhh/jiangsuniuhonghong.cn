<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DadaMerchant extends Model
{

    protected $table = 'dada_merchants';

    protected $fillable = [
        'config_id',
        'store_id',
        'source_id',
        'status'
    ];

}
