<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostPayConfig extends Model
{
    protected $table = 'post_pay_configs';

    protected $fillable = [

        'config_id',
        'wx_appid',
        'wx_secret',
        'ali_appid',
        'org_id'


    ];

}
