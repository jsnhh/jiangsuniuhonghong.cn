<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WeixinaStore extends Model
{
    //
    protected $table = 'weixina_stores';

    protected $fillable = [
        'config_id',
        'store_id',
        'secret',
        'wx_sub_merchant_id',
        'status',
        'status_desc',
        'wx_shop_id',
        'is_profit_sharing',
        'wx_sharing_type',
        'wx_sharing_account',
        'wx_sharing_name',
        'wx_sharing_relation_type',
        'wx_sharing_status',
        'wx_sharing_rate'
    ];


}
