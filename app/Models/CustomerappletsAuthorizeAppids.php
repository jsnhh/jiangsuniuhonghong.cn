<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerappletsAuthorizeAppids extends Model
{
    protected $table      = "customerapplets_authorize_appids";
    protected $primaryKey = "id";

    // 查询条件处理
    private function getMap($input)
    {
        $map = [];
        // 第三方平台id
        if (!empty($input['t_appid'])) {
            $map['t_appid'] = $input['t_appid'];
        }
        // 授权方小程序id
        if (!empty($input['AuthorizerAppid'])) {
            $map['AuthorizerAppid'] = $input['AuthorizerAppid'];
        }

        return $map;
    }

    public function getInfo($input)
    {
        $map = $this->getMap($input);

        return $this->where($map)->first();
    }

    public function updateDataByAuthorizerAppid($authorizerAppid, $data)
    {

        return $this->where('AuthorizerAppid',$authorizerAppid)->update($data);
    }

}