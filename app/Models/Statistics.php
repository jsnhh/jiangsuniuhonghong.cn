<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Statistics extends Model
{
    protected $table = 'statistics';
    protected $fillable = [
        'config_id',
        'store_id',
        'times',
        'time_start',
        'time_end',
    ];

}