<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WincodeStore extends Model
{
    protected $table = 'wincode_stores';

    //
    protected $fillable = [
        'store_id',
        'store_name',
        'merchant_id',
        'merchant_name',
        'device_id',
        'activation_code',
        'appid',
        'secret'
    ];

}
