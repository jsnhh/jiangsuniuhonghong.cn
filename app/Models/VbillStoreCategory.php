<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class VbillStoreCategory extends Model
{

    protected $table = 'vbill_store_category';

    protected $fillable = [
        'id',
        'pid',
        'level',
        'mcc',
        'category_name',
        'business_type'
    ];

}
