<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StorePayWay extends Model
{
    protected $table = 'store_pay_ways';

    protected $fillable = [
        'store_id',
        'rate',
        'rate_a',
        'rate_b',
        'rate_b_top',
        'rate_c',
        'rate_d',
        'rate_d_top',
        'rate_e',//刷卡贷记卡
        'rate_f',
        'rate_f_top',
        'settlement_type',
        'ways_type',
        'ways_source',
        'company',
        'ways_desc',
        'is_close',
        'is_close_desc',
        'sort',
        'status',
        'status_desc',
        'pcredit',
        'credit',
        'pay_amount_e'
    ];

    public function store_pay_ways()
    {
        return $this->hasMany('App\Models\StorePayWay', 'store_id', 'store_id');
    }
}
