<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AlipayZftStore extends Model
{
    //
    protected $fillable = [
        'store_id',
        'config_id',
        'order_id',
        'ip_role_id',
        'store_name',
        'status',
        'ext_info',
        'smid', //
        'SettleModeType', //
        'card_alias_no', //
        'alipay_account' //
    ];

}
