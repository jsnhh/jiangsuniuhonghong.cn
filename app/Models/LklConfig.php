<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LklConfig extends Model
{
    protected $table = 'lkl_configs';

    protected $fillable = [
        'user_no',
        'wx_appid',
        'wx_secret',
    ];

}
