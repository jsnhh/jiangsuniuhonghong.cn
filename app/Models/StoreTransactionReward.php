<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreTransactionReward extends Model
{

    protected $table = "store_transaction_reward";

    protected $fillable = [
        'id',
        'user_id',
        'store_id',
        'store_name',
        'agent_id',
        'agent_name',
        'rule_id',
        'store_bind_time',
        'standard_done_amt',
        'standard_done_total',
        'standard_end_time',
        'standard_time',
        'first_done_amt',
        'first_done_total',
        'first_total_amt',
        'first_start_time',
        'first_end_time',
        'again_done_amt',
        'again_done_total',
        'again_total_amt',
        'again_start_time',
        'again_end_time',
        'total_amt',
        'standard_status',
        'first_status',
        'again_status',
        'created_at',
        'updated_at',

    ];

}
