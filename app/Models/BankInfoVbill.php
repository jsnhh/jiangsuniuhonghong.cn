<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankInfoVbill extends Model
{
    protected $table = 'bank_info_vbills';

    protected $fillable = [
        'instOutCode',
        'bankName',
        'bankCode',
        'bankCodeName',
        'provinceCode',
        'provinceName',
        'cityCode',
        'cityCodeName'
    ];

}
