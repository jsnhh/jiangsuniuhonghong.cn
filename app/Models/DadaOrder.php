<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DadaOrder extends Model
{

    protected $table = 'dada_orders';

    protected $fillable = [
        'config_id',
        'store_id',
        'shop_no',
        'out_trade_no',
        'origin_id',
        'dada_order_id',
        'city_code',
        'cargo_price',
        'is_prepay',
        'receiver_name',
        'receiver_address',
        'receiver_lat',
        'receiver_lng',
        'cargo_weight',
        'receiver_phone',
        'receiver_tel',
        'distance',
        'fee',
        'deliver_fee',
        'coupon_fee',
        'tips',
        'insurance_fee',
        'deduct_fee',
        'info',
        'cargo_type',
        'cargo_num',
        'invoice_title',
        'origin_mark',
        'origin_mark_no',
        'is_use_insurance',
        'is_finish_code_needed',
        'delay_publish_time',
        'is_direct_delivery',
        'sku_name',
        'src_product_no',
        'count',
        'unit',
        'pick_up_pos',
        'transporter_name',
        'transporter_phone',
        'transporter_lng',
        'transporter_lat',
        'delivery_fee',
        'actual_fee',
        'create_time',
        'accept_time',
        'fetch_time',
        'finish_time',
        'cancel_time',
        'order_finish_code',
        'status',
        'status_code',
        'status_msg',
        'message_type'
    ];

}
