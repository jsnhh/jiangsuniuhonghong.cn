<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreAllPayWayList extends Model
{

    protected $table = 'store_all_pay_way_lists';

    protected $fillable = [
        'company',
        'company_desc',
        'ways_count',
    ];

}
