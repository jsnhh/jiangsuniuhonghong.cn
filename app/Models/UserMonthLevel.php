<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMonthLevel extends Model
{
    protected $table = 'user_month_level';

    protected $fillable = [
        'user_id',
        'transaction_amount',
        'level_weight',
        'start_time',
        'end_time',
        'is_deduct'
    ];

}
