<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class WeixinSharingOrder extends Model
{
    protected $table = 'weixin_sharing_orders';

    protected $fillable = [
        'user_id',
        'store_id',
        'out_trade_no',
        'transaction_id',
        'fz_out_trade_no',
        'total_amount',
        'fz_amount',
        'fz_rate',
        'is_finish',
        'is_finish_desc',
        'wx_sharing_account',
        'merchant_id',
        'type',
        'ways_source',
        'status',
        'status_desc',
    ];

}
