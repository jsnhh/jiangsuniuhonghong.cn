<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VbillaConfig extends Model
{
    protected $table = 'vbilla_configs';

    protected $fillable = [
        'config_id',
        'orgId',
        'privateKey',
        'sxfpublic',
        'wx_appid',
        'wx_secret',
        'wx_channel_appid',
    ];

}
