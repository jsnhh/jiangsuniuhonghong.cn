<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AreaCodes extends Model
{

    protected $fillable = [
        'area_code',
        'area_name',
        'area_parent_id',
        'code',
        'area_type',
    ];

    public function childUser()
    {
        return $this->hasMany('App\Models\AreaCodes', 'area_parent_id', 'area_code');
    }
    public function children()
    {
        return $this->childUser()->with('children');
    }
}
