<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankCodes extends Model
{

    protected $fillable = [
        'bank_code',
        'bank_name',
        'city_code',
        'bank_type',
        'bank_type_name',
    ];
}
