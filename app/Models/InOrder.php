<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InOrder extends Model
{
    protected $table = "in_orders";

    protected $fillable = [
        'store_id',
        'merchant_id',
        "device_no",
        "pay_status",
        "pay_type",
        "refund_fee",
        "time_end",
        "transaction_id",
        "total_fee",
        'out_trade_no',
        'pp_trade_no',
    ];
}
