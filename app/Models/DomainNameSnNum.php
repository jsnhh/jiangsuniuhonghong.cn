<?php
namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class DomainNameSnNum extends Model
{
    protected $table      = "domain_name_sn_num";
    protected $primaryKey = "id";

    private function getDomainNameSnNumMap($input)
    {
        $map = [];

        // 域名
        if (!empty($input['domain_name'])) {
            $map['domain_name'] = $input['domain_name'];
        }

        return $map;
    }

    private function getDomainNameSnNumInfoMap($input)
    {
        $map = [];

        // 域名
        if (!empty($input['domain_name'])) {
            $map['domain_name'] = $input['domain_name'];
        }
        // 设备编号
        if (!empty($input['sn_num'])) {
            $map['sn_num'] = $input['sn_num'];
        }

        return $map;
    }

    // 域名下设备数量
    public function getDomainNameSnNumCount($input)
    {
        $map = $this->getDomainNameSnNumMap($input);

        return $this->where($map)->distinct('domain_name')->count('id');
    }

    // 获取设备编号信息
    public function getDomainNameSnNumInfo($input)
    {
        $map = $this->getDomainNameSnNumInfoMap($input);

        return $this->where($map)->first();
    }

    // 新增设备
    public function addSn($input)
    {
        return $this->insert($input);
    }
}