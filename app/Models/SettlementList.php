<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettlementList extends Model
{
    protected $table = 'settlement_lists';

    protected $fillable = [
        'config_id',
        'user_id',
        'user_name',
        's_time',
        'e_time',
        'total_amount',
        'commission_amount',
        'rate',
        'out_trade_no'

    ];

}
