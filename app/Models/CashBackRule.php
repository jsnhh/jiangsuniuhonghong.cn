<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CashBackRule extends Model
{

    protected $table = "cash_back_rule";

    protected $fillable = [
        'id',
        'user_id',
        'rule_name',
        'start_time',
        'end_time',
        'standard_amt',
        'standard_single_amt',
        'standard_total',
        'first_amt',
        'first_single_amt',
        'first_total',
        'again_amt',
        'again_single_amt',
        'again_total',
        'standard_term',
        'standard_satisfy_total',
        'first_satisfy_total',
        'again_satisfy_total',
        'updated_at',
        'created_at'

    ];

}
