<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAuths extends Model
{

    protected $fillable = [
        'user_id',
        'user_name',
        'id_card',
        'user_type', //实名认证类型：0 未认证，1 已认证
        'nonce',
        'order_no',
        'face_id',
        'union_user_id',
        'id_card_front_url',
        'id_card_back_url'
    ];

}
