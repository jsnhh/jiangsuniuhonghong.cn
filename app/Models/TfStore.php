<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TfStore extends Model
{
    protected $table = 'tf_stores';

    protected $fillable = [
        'store_id',
        'config_id',
        'qd',
        'agent_id',
        'sub_mch_id',
        'alipay_pid',
        'wechat_channel_no',
        'status'
    ];

}
