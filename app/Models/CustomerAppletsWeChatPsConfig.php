<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerAppletsWeChatPsConfig extends Model
{
    protected $table = 'customerapplets_wechat_ps_configs';

    protected $fillable = [
        'config_id',
        'store_id',
        'delivery_id',
        'app_key',
        'app_secret',
        'open_status',
        'open_status_desc',
        'bind_status',
        'bind_status_desc'
    ];

}
