<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class VoiceStatistic extends Model
{

    protected $table = 'voice_statistics';

    protected $fillable = [
        'config_id',
        'package',
        'price',
        'total_num',
        'avali_num',
        'start_time',
        'end_time',
    ];


    public function packageList()
    {
        $data = [
            '1' => [
                'service_count' => 1000000,
                'concurrent' => 100,
                'total_amount' => 5800,
                'price' => 0.0058
            ],
            '2' => [
                'service_count' => 5000000,
                'concurrent' => 100,
                'total_amount' => 27000,
                'price' => 0.0054
            ],
            '3' => [
                'service_count' => 10000000,
                'concurrent' => 100,
                'total_amount' => 50000,
                'price' => 0.005
            ],
        ];

        return $data;
    }


}
