<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MerchantDevice extends Model
{
    protected $fillable=['store_id',
        'source',
        'device_type',
        'device_name',
        'type',
        'device_sn',
        'biz_tid',
        'merchant_id_type',
        'merchant_id',
        'shop_id',
        'external_shop_id'
    ];
}