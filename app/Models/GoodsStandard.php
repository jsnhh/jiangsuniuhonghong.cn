<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class GoodsStandard extends Model
{

    protected $table = 'goods_standards';

    protected $fillable = [
        'store_id',
        'name',
        'status',
        'sort'
    ];


}
