<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantStoreQrcodeRule extends Model
{
    protected $table      = "merchant_store_qrcode_rule";
    // protected $primaryKey = "id";

    // 查询条件处理
    private function getMap($input)
    {
        $map = [];
        // 门店id
        if (!empty($input['store_id'])) {
            $map['store_id'] = $input['store_id'];
        }
        // 二维码类型
        if (!empty($input['qrcode_type'])) {
            $map['qrcode_type'] = $input['qrcode_type'];
        }

        return $map;
    }

    public function getInfo($input)
    {
        $map = $this->getMap($input);

        return $this->where($map)->first();
    }

    // 插入
    public function insertData($data)
    {
        return $this->insert($data);
    }
}