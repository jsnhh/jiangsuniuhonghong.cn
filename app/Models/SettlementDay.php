<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettlementDay extends Model
{
    protected $table = 'settlement_day';

    protected $fillable = [
        'user_id',
        'name',
        'config_id',
        'source_type',
        'rate',
        'status',
    ];

}

