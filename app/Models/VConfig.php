<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VConfig extends Model
{
    protected $table = 'v_configs';

    protected $fillable = [
        'config_id',
        'zlbz_token',
        'zw_token',
        'old_zw_token',
        'yly_user_name',
        'yly_user_id',
        'yly_api_key',
        'cls_prefix',
        'cls_product_id',
        'tencent_cloud_secretid',
        'tencent_cloud_secretkey'
    ];

}
