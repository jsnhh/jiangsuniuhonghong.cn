<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AligenieList extends Model
{

    protected $table = 'aligenie_lists';

    protected $fillable = [
        'config_id',
        'store_id',
        'activation_code',
        'device_open_id'
    ];

}
