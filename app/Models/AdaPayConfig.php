<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdaPayConfig extends Model
{
    protected $table = 'ada_pay_configs';

    protected $fillable = [
        'config_id',
        'org_id',
        'appid',
    ];

}
