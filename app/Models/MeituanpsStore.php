<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MeituanpsStore extends Model
{

    protected $table = 'meituanps_stores';

    protected $fillable = [
        'shop_id',
        'shop_name',
        'category',
        'second_category',
        'contact_name',
        'contact_phone',
        'shop_address',
        'shop_lng',
        'shop_lat',
        'coordinate_type',
        'business_hours',
        'contact_email',
        'shop_address_detail',
        'delivery_service_codes',
        'status',
        'status_desc'
    ];

}
