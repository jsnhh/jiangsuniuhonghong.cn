<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\GoodCartStandard;
use Illuminate\Support\Facades\DB;

class GoodCart extends Model
{
    protected $table      = "goods_carts";
    protected $primaryKey = "id";

    /**
     * 获取购物车信息
     * @param $user_id
     * @param $store_id
     * @return array
     */
    public function getGoodCartInfo($user_id,$store_id){

        if(!isset($user_id) || empty($user_id)){
            return ["status" => 4001,"message" => "user_id参数不可为空"];
        }

        if(!isset($store_id) || empty($store_id)){
            return ["status" => 4002,"message" => "store_id参数不可为空"];
        }

        $goodCart = $this->select("goods.name","goods.unit","goods.price","goods.main_image","goods_carts.id","goods_carts.user_id","goods_carts.store_id","goods_carts.good_id","goods_carts.num","goods_carts.created_at")
            ->leftJoin("goods","goods.id","=","goods_carts.good_id")
            ->where(['goods_carts.user_id' => $user_id , 'goods_carts.store_id' => $store_id])
            ->where('goods_carts.num','>',0)
            ->orderBy("goods_carts.created_at","asc")
            ->get();

        if(!empty($goodCart)){

            $goodCartStandardModel = new GoodCartStandard();

            $cart_good_settlement_money = 0;
            $cart_good_number           = 0;

            foreach ($goodCart as $key => $val){

                $goodCartStandardResult = $goodCartStandardModel->where(['cart_id' => $val['id'] , 'good_id' => $val['good_id']])->get();

                //购物车中每个商品需要付的钱数
                $val['money'] = (string)($val['num'] * $val['price']);

                //购物车总共需要付的钱数
                $cart_good_settlement_money = (string)($cart_good_settlement_money + ($val['num'] * $val['price']));

                $cart_good_number           = $cart_good_number + $val['num'];

                if(!empty($goodCartStandardResult)){

                    $goodCartStandardArray = [];

                    foreach ($goodCartStandardResult as $stand_key => $stand_val){
                        array_push($goodCartStandardArray,$stand_val['stand_id']);
                    }

                    $val['standard'] = $goodCartStandardArray;

                }else{
                    $val['standard'] = [];
                }
            }

            return ["status" => 200 , "message" => ['cart_good_settlement_money' => $cart_good_settlement_money , 'cart_good_number' => $cart_good_number,'good_list' => $goodCart]];

        }else{
            return ["status" => 202,"message" => "购物车"];
        }

    }


    /**
     * 插入购物车数据
     * @param $user_id
     * @param $store_id
     * @param $good_id
     * @param $num
     * @param string $standard
     * @return array
     */
    public function addGoodCart($user_id,$store_id,$good_id,$num,$standard = ""){

        if(!isset($user_id) || empty($user_id)){
            return ["status" => 4001,"message" => "user_id参数不可为空"];
        }

        if(!isset($store_id) || empty($store_id)){
            return ["status" => 4002,"message" => "store_id参数不可为空"];
        }

        if(!isset($good_id) || empty($good_id)){
            return ["status" => 202,"message" => "good_id参数不可为空"];
        }

        if(!isset($num)){
            return ["status" => 202,"message" => "num参数不可为空"];
        }

        //开启事务
        DB::beginTransaction();

        try{

            $date = date("Y-m-d H:i:s",time());
            //先判断购物车有没有这条商品的信息
            $goodResult = $this->where(['user_id' => $user_id,'store_id' => $store_id,'good_id' => $good_id])->first();

            if(!empty($goodResult)){

                //先更新购物车中的数量
                $updateGoodCartNum = $this->where(['user_id' => $user_id,'store_id' => $store_id,'good_id' => $good_id])->update([
                    "num"        => $num,
                    "updated_at" =>$date
                ]);

                if($updateGoodCartNum){

                    $good_cart_result = DB::table("goods_cart_standards")->where(['cart_id' => $goodResult->id,'good_id' => $good_id])->get();

                    if(!empty($good_cart_result) && count($good_cart_result) > 0){

                        $good_cart_delete_result = DB::table("goods_cart_standards")->where(['cart_id' => $goodResult->id,'good_id' => $good_id])->delete();

                        if(!$good_cart_delete_result){
                            return ['status' => 202,"message" => "购物车商品规格信息删除失败"];
                        }
                    }

                    if(!empty($standard)){

                        $standard_data = [];

                        if(is_array($standard)){

                            foreach ($standard as $k => $v){
                                $standard_data_list['cart_id']    = $goodResult->id;
                                $standard_data_list['good_id']    = $good_id;
                                $standard_data_list['stand_id']   = $v;
                                $standard_data_list['created_at'] = $date;
                                $standard_data_list['updated_at'] = $date;

                                array_push($standard_data,$standard_data_list);
                            }

                        }else if(is_string($standard)){

                            $standard_array = explode(",",$standard);
                            if(!empty($standard_array) && count($standard_array) > 0){
                                foreach ($standard_array as $k => $v){
                                    $standard_data_list['cart_id']    = $goodResult->id;
                                    $standard_data_list['good_id']    = $good_id;
                                    $standard_data_list['stand_id']   = $v;
                                    $standard_data_list['created_at'] = $date;
                                    $standard_data_list['updated_at'] = $date;
                                    array_push($standard_data,$standard_data_list);
                                }
                            }

                        }

                        if(!empty($standard_data)){

                            $result = DB::table("goods_cart_standards")->insert($standard_data);

                            if(!$result){
                                return ['status' => 202,"message" => "购物车商品规格信息写入失败"];
                            }
                        }

                    }

                }else{
                    return ['status' => 202,"message" => "购物车更新商品数量失败"];
                }

                DB::commit();
                return ['status' => 200 , 'message' => ["cart_id" => $goodResult->id]];
            }else{

                $good_cart['user_id']    = $user_id;
                $good_cart['store_id']   = $store_id;
                $good_cart['good_id']    = $good_id;
                $good_cart['num']        = $num;
                $good_cart['created_at'] = $date;
                $good_cart['updated_at'] = $date;
                $good_cart_id = $this->insertGetId($good_cart);

                if($good_cart_id){

                    if(!empty($standard)){

                        $standard_data = [];

                        if(is_array($standard)){

                            foreach ($standard as $k => $v){
                                $standard_data_list['cart_id']    = $good_cart_id;
                                $standard_data_list['good_id']    = $good_id;
                                $standard_data_list['stand_id']   = $v;
                                $standard_data_list['created_at'] = $date;
                                $standard_data_list['updated_at'] = $date;
                                array_push($standard_data,$standard_data_list);
                            }

                        }else if(is_string($standard)){

                            $standard_array = explode(",",$standard);
                            if(!empty($standard_array) && count($standard_array) > 0){
                                foreach ($standard_array as $k => $v){
                                    $standard_data_list['cart_id']    = $good_cart_id;
                                    $standard_data_list['good_id']    = $good_id;
                                    $standard_data_list['stand_id']   = $v;
                                    $standard_data_list['created_at'] = $date;
                                    $standard_data_list['updated_at'] = $date;
                                    array_push($standard_data,$standard_data_list);
                                }
                            }

                        }

                        if(!empty($standard_data)){

                            $result = DB::table("goods_cart_standards")->insert($standard_data);

                            if(!$result){
                                return ['status' => 202,"message" => "购物车商品规格信息写入失败"];
                            }
                        }

                    }
                }else{
                    return ['status' => 202,"message" => "购物车信息写入失败"];
                }
                DB::commit();
                return ['status' => 200 , 'message' => ["cart_id" => $good_cart_id]];
            }
        }catch (\Exception $e) {
            DB::rollBack();
            return ['status' => 202,'message' => $e->getMessage()];
        }

    }

    /**
     * 清空购物车
     * @param $user_id
     * @param $store_id
     * @return array
     */
    public function clearCart($user_id,$store_id){

        if(!isset($user_id) || empty($user_id)){
            return ["status" => 4001,"message" => "user_id参数不可为空"];
        }

        if(!isset($store_id) || empty($store_id)){
            return ["status" => 4002,"message" => "store_id参数不可为空"];
        }

        //开启事务
        DB::beginTransaction();

        try{

            //先查询购物车相关数据
            $goodCartResult = $this->where(['user_id' => $user_id , 'store_id' => $store_id])->get();

            if(!empty($goodCartResult)){

                foreach ($goodCartResult as $k => $v){

                    DB::table("goods_cart_standards")->where(['cart_id' => $v['id'],'good_id' => $v['good_id']])->delete();

                }

                //删除购物车相关数据
                $deleteGoodCart = $this->where(['user_id' => $user_id , 'store_id' => $store_id])->delete();

                if(!$deleteGoodCart){
                    return ['status' => 202,'message' => "清空购物车失败"];
                }
            }

            DB::commit();
            return ['status' => 200 , 'message' => "清空购物车成功"];
        }catch (\Exception $e) {
            DB::rollBack();
            return ['status' => 202,'message' => $e->getMessage()];
        }

    }
}
