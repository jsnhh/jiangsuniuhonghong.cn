<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberSetJf extends Model
{


    protected $table = "member_set_jfs";
    protected $fillable = [
        'store_id',
        'xf',
        'xf_s_jf',
        'dk_jf_m',
        'dk_rmb',
        'new_mb_s_jf',
    ];
}
