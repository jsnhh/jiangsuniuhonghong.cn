<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    //
    protected $fillable = [
        'config_id',
        'pid',
        'store_id',
        'user_id',
        'merchant_id',
        'user_pid',
        'store_name',
        'store_short_name',
        'people',//负责人
        'people_phone',
        'province_code',
        'city_code',
        'area_code',
        'province_name',
        'city_name',
        'area_name',
        'store_address',
        'head_name',//法人
        'head_sfz_no',
        'head_sfz_time',
        'head_sfz_stime',
        'category_id',
        'category_name',
        'store_license_no',
        'store_license_time',
        'store_license_stime',
        'weixin_name',
        'weixin_no',
        'alipay_name',
        'alipay_account',
        'store_email',
        'is_close',
        'is_delete',
        'is_admin_close',
        'is_admin_close_desc',
        'is_send_tpl_mess',
        'store_type',
        'store_type_name',
        'status',
        'status_desc',
        'admin_status',
        'admin_status_desc',
        'blacklist',
        'wx_AppId',
        'wx_Secret',
        'wx_SubscribeAppId',
        'store_pay_ways_open',
        'pay_ways_type', //0-自己的通道，1-上级的通道
        's_time',
        'e_time',
        'lat',
        'lng',
        'dlb_industry',
        'dlb_second_industry',
        'dlb_province',
        'dlb_city',
        'dlb_bank',
        'dlb_sub_bank',
        'dlb_pay_bank_list',
        'dlb_micro_biz_type',
        'hkrt_bus_scope_code',
        'installStatus',
        'installUser',
        'linkageCategoryId',
        'announcement',
        'logo',
        'bg_image',
        'user_pid_name',
        'disable_pay_channels',
        'alipay_account',
        'is_get_phone',
        'source', //来源,01:畅立收，02:河南畅立收
        'zero_rate_type', //零费率状态 0关闭，1开启
        'openid'//商户openid
    ];

    public function store_pay_ways()
    {
        return $this->hasMany('App\Models\StorePayWay', 'store_id', 'store_id');
    }

    // 获取门店基本信息
    public function getStoreBaseInfo($input)
    {
        $map['store_id'] = $input['store_id'];

        return $this->where($map)->select(["store_id", "is_send_tpl_mess", "is_get_phone"])->first();
    }
}
