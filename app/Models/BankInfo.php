<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankInfo extends Model
{
    protected $table = 'bank_info';

    protected $fillable = [
        'instOutCode', //清算行号
        'bankName', //银行名称
        'cityCode', //银行所在地区码
    ];

}
