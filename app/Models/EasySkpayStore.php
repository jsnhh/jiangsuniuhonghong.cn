<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EasySkpayStore extends Model
{
    protected $table = 'easyskpay_stores';

    protected $fillable = [
        'config_id',
        'store_id',
        'mer_id',
        'audit_status',
        'audit_msg',
    ];

}
