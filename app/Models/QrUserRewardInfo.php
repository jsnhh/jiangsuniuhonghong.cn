<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class QrUserRewardInfo extends Model
{

    protected $table = "qr_user_reward_info";
    protected $fillable = [
        'code_num',
        'user_id',
        'award_amount',
        'store_id',
        'store_name',
        'status',
        'status_desc',
        'activation_integral',

    ];

}
