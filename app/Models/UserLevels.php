<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class UserLevels extends Model
{
    protected $fillable = [
        'user_id',
        'transaction' ,
        'level',
        'remark',
        'status'
    ];

}