<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WeixinConfig extends Model
{
    //
    protected $table = 'weixin_config';

    protected $fillable = [
        'config_id',
        'config_type',
        'authorizer_appid',
        'authorizer_refresh_token',
        'authorizer_time',
        'app_id',
        'app_name',
        'app_secret',
        'wx_notify_appid',
        'wx_notify_secret',
        'template_id',
        'wx_merchant_id',
        'key',
        'cert_path',
        'key_path',
        'auth_path',
        'notify_url',
        'authorize_url',
        'qrconnect_url',
        'check_wx_merchant_id',
        'face_md_key',
    ];


}
