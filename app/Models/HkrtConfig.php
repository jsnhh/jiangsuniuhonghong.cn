<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HkrtConfig extends Model
{
    protected $table = 'hkrt_configs';

    protected $fillable = [
        'config_id',
        'agent_no',
        'access_id',
        'access_key',
        'wx_appid',
        'wx_secret'
    ];

}
