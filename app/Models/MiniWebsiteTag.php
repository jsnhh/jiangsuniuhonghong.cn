<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MiniWebsiteTag extends Model
{
    protected $table = 'mini_website_tags';

    protected $fillable = [
        'config_id',
        'store_id',
        'name',
        'status',
        'sort'
    ];

}
