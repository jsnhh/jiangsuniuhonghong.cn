<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class AgentLevelsInfos extends Model
{

    protected $table = "agent_levels_infos";
    protected $fillable = [
        'level',
        'level_weight',
        'user_id',
        'start_time',
        'end_time',
        'up_status',
        'up_level',
    ];

}
