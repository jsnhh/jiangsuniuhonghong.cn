<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MiniWebsiteShow extends Model
{
    protected $table = 'mini_website_shows';

    protected $fillable = [
        'config_id',
        'store_id',
        'tag_id',
        'price_show',
        'price',
        'sort',
        'title',
        'sub_title',
        'cover_url',
        'banner_url',
        'video_url',
        'content',
        'status'
    ];

}
