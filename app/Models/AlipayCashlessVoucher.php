<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AlipayCashlessVoucher extends Model
{

    protected $table = 'alipay_cashless_voucher';

    protected $fillable = [
        'config_id',
        'store_id',
        'voucher_type',
        'brand_name',
        'activity_name',
        'publish_start_time',
        'publish_end_time',
        'voucher_start',
        'voucher_end',
        'voucher_time_start',
        'voucher_time_end',
        'voucher_description',
        'amount',
        'floor_amount',
        'day_rule',
        'voucher_quantity',
        'status',
        'del_status',
    ];

}