<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberTpl extends Model
{


    protected $table = "member_tpls";
    protected $fillable = [
        'store_id',
        'config_id',
        'tpl_name',
        'tpl_desc',
        'tpl_bck',
        'tpl_sku',
        'tpl_content',
        'tpl_status',
        'tpl_phone'
    ];
}
