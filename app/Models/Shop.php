<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    //

    protected $table = 'shops';
    protected $fillable =
        [
            'store_id',
            'merchant_id',
            'shop_id',
        ];
}
