<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantWalletDayCount extends Model
{
    //
    protected $table="merchant_wallet_day_counts";
    protected $fillable = [
        'store_id',
        'config_id',
        'source_type',
        'source_desc',
        'settlement',
        'money',
        'merchant_id',
        'day',
    ];

}
