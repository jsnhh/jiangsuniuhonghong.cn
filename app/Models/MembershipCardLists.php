<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MembershipCardLists extends Model
{
    protected $table="membership_card_lists";

    protected $fillable = [
        'store_id',
        'terminal_type',
        'card_code',
        'card_id',
        'status',
    ];

}
