<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class StoreDeliverSetting extends Model
{

    protected $table = 'store_deliver_settings';

    protected $fillable = [
        'store_id',
        'packing_fee_method',
        'packing_charge',
        'is_same_day',
        'delivery_method',
        'delivery_range',
        'delivery_start_time',
        'delivery_end_time',
        'delivery_start_amount',
        'free_delivery_amount',
        'delivery_amount',
        'status'
    ];

}
