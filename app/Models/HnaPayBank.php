<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HnaPayBank extends Model
{
    protected $table = 'hnapay_bank';

    protected $fillable = [

        'bank_id',
        'branch_no',
        'bank_name',
        'province_code',
        'province_name',
        'city_code',
        'city_name',
    ];

}
