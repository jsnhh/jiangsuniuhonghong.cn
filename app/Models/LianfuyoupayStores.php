<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LianfuyoupayStores extends Model
{
    protected $table = 'lianfuyoupay_stores';

    protected $fillable = [
        'config_id',
        'store_id',
        'apikey',
        'signkey',
        'pos_sn'
    ];

}