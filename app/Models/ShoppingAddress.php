<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShoppingAddress extends Model
{

    protected $table = "shopping_address";

    protected $fillable = [
        'name',
        'user_id',
        'user_name',
        'province',
        'city',
        'county',
        'address_detail',
        'tel',
        'e_mail',
        'is_default',
    ];

}
