<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantStoreTableManage extends Model
{
    protected $table      = "merchant_store_table_manage";
    protected $primaryKey = "id";

    // 查询条件处理
    private function getMap($input)
    {
        $map = [];
        // 该商户名下所有门店id
        if (!empty($input['store_ids'])) {
            $store_ids = $input['store_ids'];
            $map[] = [function($query) use ($store_ids){
                $query->whereIn('merchant_store_table_manage.store_id', $store_ids);
            }];
        }
        // 门店id
        if (!empty($input['store_id'])) {
            $map['merchant_store_table_manage.store_id'] = $input['store_id'];
        }
        // 桌号
        if (!empty($input['table_id'])) {
            $map['merchant_store_table_manage.table_id'] = $input['table_id'];
        }
        // 桌名称
        if (!empty($input['table_name'])) {
            $map['merchant_store_table_manage.table_name'] = $input['table_name'];
        }

        return $map;
    }

    // 数据个数
    public function getTableCount($input)
    {
        $map = $this->getMap($input);

        return $this->where($map)->count();
    }

    // 获取门店桌号列表
    public function getTablePageList($input, $start, $limit)
    {
        $map = $this->getMap($input);

         return $this->where($map)
             ->leftjoin('stores', 'merchant_store_table_manage.store_id', '=', 'stores.store_id')
             ->select(
                 'merchant_store_table_manage.id', 'merchant_store_table_manage.store_id', 'merchant_store_table_manage.table_id',
                 'merchant_store_table_manage.table_name', 'merchant_store_table_manage.table_status',
                 'merchant_store_table_manage.wechat_applet_qrcode', 'merchant_store_table_manage.alipay_applet_qrcode',
                 'merchant_store_table_manage.common_qrcode', 'merchant_store_table_manage.created_at',
                 'merchant_store_table_manage.updated_at', 'stores.store_name'
             )
             ->skip($start)->limit($limit)
             ->get();
    }

    // 插入门店桌号信息
    public function insertTableInfo($data)
    {
        return $this->insert($data);
    }

    // 获取门店桌号信息
    public function getTableInfo($input)
    {
        $map = $this->getMap($input);

        return $this->where($map)->first();
    }

    // 删除
    public function deleteTable($input)
    {
        $map = $this->getMap($input);

        return $this->where($map)->delete();
    }

    // 编辑
    public function editTable($id, $data)
    {
        return $this->where('id', $id)->update($data);
    }

}
