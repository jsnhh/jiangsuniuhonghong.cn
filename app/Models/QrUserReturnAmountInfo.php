<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class QrUserReturnAmountInfo extends Model
{

    protected $table = "qr_user_return_amount_info";
    protected $fillable = [
        'code_num',
        'user_id',
        'amount',
        'store_id',
        'store_name',
        'store_type',
        'integral',
        'return_type'
    ];

}
