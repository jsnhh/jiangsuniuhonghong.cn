<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EasePayOccupation extends Model
{
    protected $table = 'ease_pay_occupation';

    protected $fillable = [
        'code',
        'name',
    ];

}
