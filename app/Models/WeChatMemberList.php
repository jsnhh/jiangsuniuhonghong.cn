<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WeChatMemberList extends Model
{
    protected $table = 'wechat_member_lists';
    protected $primaryKey = "id";
    // protected $fillable = [
    //     'store_id',
    //     'mb_id',
    //     'mb_status',
    //     'mb_ver',
    //     'mb_ver_desc',
    //     'applet_openid',
    //     'wechat_openid',
    //     'mb_logo',
    //     'mb_nickname',
    //     'mb_phone',
    //     'mb_points',
    //     'mb_money',
    //     'mb_virtual_money',
    //     'mb_pay_counts',
    //     'mb_pay_money',
    //     'mb_time',
    //     'updated_at',
    //     'created_at'
    // ];

    // 新增数据
    public function addData($data)
    {
        return $this->insert($data);
    }

    // 更新数据
    public function updateData($mb_id, $data)
    {
        return $this->where('mb_id', $mb_id)->update($data);
    }

    // 条件集合
    public function getWechatMemberMap($input)
    {
        $map = [];
        // 门店id
        if (!empty($input['store_id'])) {
            $map['store_id'] = $input['store_id'];
        }
        // 用户对应微信公众号openid
        if (!empty($input['wecaht_openid'])) {
            $map['wecaht_openid'] = $input['wecaht_openid'];
        }
        // 用户对应微信小程序openid
        if (!empty($input['applet_openid'])) {
            $map['applet_openid'] = $input['applet_openid'];
        }
        // 用户 mb_id
        if (!empty($input['mb_id'])) {
            $map['mb_id'] = $input['mb_id'];
        }
        // id
        if (!empty($input['id'])) {
            $map['id'] = $input['id'];
        }

        return $map;
    }

    // 获取微信正式会员信息
    public function getWechatMemberInfo($input)
    {
        // DB::enableQueryLog();  //开启日志
        // var_dump(DB::getQueryLog());die; //打印日志

        $map = $this->getWechatMemberMap($input);

        return  $this->where($map)->first();
    }

}
