<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TfConfig extends Model
{
    protected $table = 'tf_configs';

    protected $fillable = [
        'config_id',
        'qd',
        'mch_id',
        'md_key',
        'pri_key',
        'pub_key',
        'wx_appid',
        'wx_secret',
        'wechat_channel_no',
        'alipay_pid',
    ];

}
