<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserHbfqRate extends Model
{
    //
    protected $fillable = [
        'user_id',
        'hbfq_rate_3',
        'hbfq_rate_6',
        'hbfq_rate_12',
        'hbfq_rate_24',
        'hbfq_store_rate_3',
        'hbfq_store_rate_6',
        'hbfq_store_rate_12',
        'hbfq_store_rate_34',
    ];


}
