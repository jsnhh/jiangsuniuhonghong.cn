<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Customer extends Model
{
    protected $table = 'customer';
    protected $fillable = [
        'config_id',
        'store_id',
        'open_id',
        'name',
        'cellphone',
        'intentionality',
        'frequency',
        'short_time',
        'long_time',
        'remarks',
    ];

}
