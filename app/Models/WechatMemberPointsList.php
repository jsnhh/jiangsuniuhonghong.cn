<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WechatMemberPointsList extends Model
{
    protected $table = 'wechat_member_points_list';
    protected $primaryKey = "id";

    // 新增数据
    public function addData($data)
    {
        return $this->insert($data);
    }

    // 更新数据
    public function updateData($mb_id, $data)
    {
        return $this->where('mb_id', $mb_id)->update($data);
    }

}
