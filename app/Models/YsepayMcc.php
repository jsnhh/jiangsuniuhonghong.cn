<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class YsepayMcc extends Model
{
    protected $table = 'ysepay_mcc';

    protected $fillable = [
        'mcc_id',
        'mcc_name',
        'mch_type',
        'mch_type_name'
    ];

}
