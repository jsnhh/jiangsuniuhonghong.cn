<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EsignAuths extends Model
{

    protected $fillable = [
        'user_id',
        'psn_account',
        'user_name',
        'id_card',
        'user_address',
        'org_name',
        'legal_user_name',
        'legal_id_card',
        'license',
        'user_type', //实名认证类型：1 个人，2 企业
        'id_card_front_url',
        'id_card_back_url',
        'license_url',
        'realname_status', //认证状态:0 - 未实名，1 - 已实名
        'authorize_status', //授权状态:0 - 未授权，1 - 已授权
        'psn_id',
        'org_id',
        'auth_flow_id',
        'auth_url',
        'auth_short_url',
        'auth2_flow_id',
        'auth2_url',
        'auth2_short_url',
        'file_id',
        'sign_flow_id',
        'sign_result',  //签署结果:2 - 签署完成，3 - 失败，4 - 拒签
        'result_description',
        'sign_flow_status', //签署流程最终状态:2 - 已完成 3 - 已撤销 5 - 已过期 7 - 已拒签
        'status_description'
    ];

}
