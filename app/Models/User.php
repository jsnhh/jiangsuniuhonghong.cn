<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements JWTSubject
{
    // use EntrustUserTrait;
    use HasRoles;

    protected $guard_name = 'user.api';

    // use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'config_id',
        's_code',
        's_code_url',
        'sub_code_url',
        'money',
        'profit_ratio',
        'sub_profit_ratio',
        'settlement_money',
        'unsettlement_money',
        'pid',
        'pid_name',
        'level',
        'level_name',
        'name',
        'logo',
        'phone',
        'email',
        'password',
        'pay_password',
        'remember_token',
        'is_delete',
        'is_admin',
        'wx_openid',
        'province_name',
        'city_name',
        'area_name',
        'address',
        'is_withdraw',
        'invite_no',
        'invite_code',
        'auth_status',
        'is_zero_rate', //代理零费用开关：0 关闭，1开启
        'source', //来源,01:畅立收，02:河南畅立收
        'settlement_type',//结算方式1=月结2=日结
        'reward_money',//奖励金额
        'activation_integral',//积分
        'isMarketing',
        'qr_gap_num',
        'likeness_url',
        'id_card',
        'is_incoming',
        'is_bind_qr',
        'marketing_province_name',
        'marketing_province_code',
        'shopping_reward_num',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'pay_password'
    ];

    public function owns($related)
    {
        return $this->id == $related->user_id;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getUserId();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getUserId()
    {

        return [
            'type' => 'user',
            'user_id' => $this->id,
            'config_id' => $this->config_id,
            's_code' => $this->s_code,
            'pid' => $this->pid,
            'level' => $this->level,
            'name' => $this->name,
            'logo' => $this->logo,
            'phone' => $this->phone,
            'email' => $this->email,
            'is_delete' => $this->is_delete,
            'is_admin' => $this->is_admin,
            'is_withdraw' => $this->is_withdraw,
            'source' => $this->source,
            'settlement_type' => $this->settlement_type,
            'invite_no' => $this->invite_no,
            'invite_code' => $this->invite_code,
            'auth_status' => $this->auth_status,


        ];//返回用户id
    }

    public function childUser()
    {
        return $this->hasMany('App\Models\User', 'pid', 'id')->where('is_delete', 0);
    }

    public function children()
    {
        return $this->childUser()->with('children');
    }
}
