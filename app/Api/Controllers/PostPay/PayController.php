<?php

namespace App\Api\Controllers\PostPay;

use App\Api\Controllers\Config\VbillConfigController;
use App\Models\VbillConfig;

use App\Models\CustomerAppletsUserOrders;
use App\Models\CustomerAppletsUserOrderGoods;
use function EasyWeChat\Kernel\Support\get_client_ip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PayController extends BaseController
{
    //扫一扫 0-系统错误 1-成功 2-正在支付 3-失败
    public function scan_pay($data)
    {
        try {
            $orderNo = $data['out_trade_no'];
            $code = $data['code'];
            $total_amount = $data['total_amount'];
            $device_id = $data['device_id'];
            $orgId = $data['orgId'];
            $custLogin = $data['phone'];
            $txamt = isset($total_amount) ? intval($total_amount * 100) : '';
            $custId = $data['custId'];
            $payChannel = $data['payChannel'];
            $data_re = [
                'access' => '3',
                'agetId' => $orgId,
                'custId' => $custId,
                'driveNo' => $device_id,
                'tradingIp' => '154.8.143.104',
                'custLogin' => $custLogin,
                'type' => 'C',
                'txamt' => $txamt,
                'txamtOrder' => $txamt,
                'code' => $code,
                'orderNo' => $orderNo,
                'payChannel' => $payChannel,
                'timestamp' => date('Y-m-d H:i:m', time()),
                'version' => '1.0.0',
                'signType' => 'RSA'

            ];
            $sign = $this->getSign($data_re);
            $data_re['sign'] = $sign;
            Log::info('邮驿付-静态二维码-params：');
            Log::info($data_re);
            $re = $this->request($data_re, $this->scan_url);
            Log::info("邮驿付商户主扫支付--" . $re);
            $res_arr = json_decode($re, true);
            //系统错误
            if (!$re) {
                return $re;
            }

            //业务成功
            if ($res_arr['code'] == "000000") {
                //交易成功
                return [
                    'status' => 1,
                    'message' => $res_arr['msg'],
                    'data' => $res_arr['data']
                ];
            } elseif ($res_arr['code'] == "222222") {
                return [
                    'status' => '2',
                    'message' => $res_arr['msg'],
                    'data' => $res_arr['data']
                ];
            } else {
                return [
                    'status' => '0',
                    'message' => $res_arr['msg'],
                    'data' => $res_arr['data']
                ];
            }

        } catch (\Exception $ex) {
            return [
                'status' => '0',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine(),
            ];
        }
    }


    //查询订单 0-系统错误 1-成功 2-正在支付 3-失败 4.已经退款
    public function order_query($data)
    {
        try {
            $orderNo = $data['out_trade_no'];
            $custLogin = $data['custLogin'];
            $custId = $data['custId'];
            $agetId = $data['agetId'];

            $data = [
                'orderNo' => $orderNo,
                'agetId' => $agetId,
                'custId' => $custId,
                'custLogin' => $custLogin,
                'timestamp' => date('Y-m-d H:i:m', time()),
                'version' => '1.0.0',
                'signType' => 'RSA'
            ];
            $sign = $this->getSign($data);
            $data['sign'] = $sign;
            Log::info('邮驿付订单查询-params：');
            Log::info($data);
            $re = $this->request($data, $this->tradeQuery_url);
            Log::info("邮驿付订单查询结果--" . $re);
            $res_arr = json_decode($re, true);
            //系统错误
            if (!$res_arr) {
                return $res_arr;
            }
            //业务成功
            if ($res_arr['code'] == "000000") {
                //交易成功
                return [
                    'status' => '1',
                    'message' => $res_arr['msg'],
                    'data' => $res_arr['data'],
                ];
            } elseif ($res_arr['code'] == "222222") {
                return [
                    'status' => '2',
                    'message' => $res_arr['msg'],
                    'data' => $res_arr['data'],
                ];
            } elseif ($res_arr['code'] == "006668") {
                return [
                    'status' => 3,
                    'message' => $res_arr['msg'],
                ];
            } else {
                return [
                    'status' => 3,
                    'message' => $res_arr['msg'],
                ];
            }


        } catch (\Exception $ex) {
            return [
                'status' => '0',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine(),
            ];
        }
    }


    //退款 0-系统错误 1-成功
    public function refund($data)
    {
        try {
            $tOrderNo = $data['out_trade_no'];
            $custLogin = $data['phone'];
            $tag = $data['tag'];
            $agetId = $data['agetId'];
            $custId = $data['custId'];
            $refund_amount = $data['refund_amount'];
            $driveNo = $data['driveNo'];
            $orderNo = $data['orderNo'];
            $refundAmount = isset($refund_amount) ? intval($refund_amount * 100) : '';
            $data = [
                'orderNo' => $orderNo,
                'reOrderNo' => $tOrderNo,
                'agetId' => $agetId,
                'custId' => $custId,
                'refundAmount' => $refundAmount,
                'custLogin' => $custLogin,
                'tag' => $tag,
                'driveNo' => $driveNo,
                'timestamp' => date('Y-m-d H:i:m', time()),
                'version' => '1.0.0',
                'signType' => 'RSA'

            ];
            $sign = $this->getSign($data);
            $data['sign'] = $sign;
            Log::info('邮驿付-退款-params:');
            Log::info($data);
            $re = $this->request($data, $this->refund_url);
            Log::info("邮驿付退款--" . $re);
            $res_arr = json_decode($re, true);
            //系统错误
            if (!$res_arr) {
                return $res_arr;
            }
            //业务成功
            if ($res_arr['code'] == "000000") {
                //退款成功
                if ($res_arr['data']['orderStatus'] == "4") {
                    return [
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $res_arr['data'],
                    ];
                } elseif ($res_arr['data']['orderStatus'] == "3") {
                    return [
                        'status' => 2,
                        'message' => '退款失败',
                        'data' => $res_arr['data'],
                    ];
                } else {
                    sleep(10); //10秒后在发起查询
                    //退款失败，查询退款结果
                    $refund_query_data = [
                        'orderNo' => $orderNo, //系统订单号
                        'custId' => $data['custId'],
                        'agetId' => $data['agetId']
                    ];
                    $refund_query = $this->refund_query($refund_query_data);
                    if ($refund_query['code'] == "000000") {
                        if ($refund_query['data']['orderStatus'] == "4") {
                            return [
                                'status' => 1,
                                'message' => '退款成功',
                                'data' => $refund_query['data'],
                            ];
                        } elseif ($refund_query['data']['orderStatus'] == "3") {
                            return [
                                'status' => 2,
                                'message' => '退款失败',
                                'data' => $refund_query['data'],
                            ];
                        } else {
                            return [
                                'status' => 2,
                                'message' => '退款中',
                                'data' => $refund_query['data'],
                            ];
                        }
                        return [
                            'status' => 2,
                            'message' => $refund_query['msg'],
                            'data' => $refund_query['data'],
                        ];
                    } else {
                        return [
                            'status' => 0,
                            'message' => $refund_query['msg']
                        ];
                    }
                }
            } else {
                return [
                    'status' => 0,
                    'message' => $res_arr['msg']
                ];
            }

        } catch (\Exception $ex) {
            return [
                'status' => 0,
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }


    //退款查询
    public function refund_query($data)
    {
        try {
            $orderNo = $data['out_trade_no']; //系统订单号
            $custId = $data['custId'];
            $agetId = $data['agetId'];
            $data = [
                'orderNo' => $orderNo,
                '$agetId' => $agetId,
                'custId' => $custId,
                'timestamp' => date('Y-m-d H:i:m', time()),
                'version' => '1.0.0',
                'signType' => 'RSA'
            ];
            $sign = $this->getSign($data);
            $data['sign'] = $sign;
            Log::info('邮驿付退款查询-params:');
            Log::info($data);
            $re = $this->request($data, $this->refundQuery_url);
            Log::info("邮驿付退款查询--" . $re);
            $res_arr = json_decode($re, true);
            //系统错误
            if (!$res_arr) {
                return $res_arr;
            }
            //业务成功
            if ($res_arr['code'] == "000000") {
                //退款成功
                if ($res_arr['data']['orderStatus'] == "4") {
                    return [
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $res_arr['data'],
                    ];
                } elseif ($res_arr['data']['orderStatus'] == '5') {
                    return [
                        'status' => 2,
                        'message' => '正在退款中',
                        'data' => $res_arr['data'],
                    ];
                } elseif ($res_arr['respData']['tranSts'] == '3') {
                    return [
                        'status' => 3,
                        'message' => '退款失败',
                        'data' => $res_arr['data'],
                    ];
                } else {
                    return [
                        'status' => 0,
                        'message' => $res_arr['msg'],
                        'data' => $res_arr['data'],
                    ];
                }

            } else {
                return [
                    'status' => 0,
                    'message' => $res_arr['msg']
                ];
            }


        } catch (\Exception $ex) {
            return [
                'status' => '0',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }


    //生成动态二维码-公共
    public function send_qr($data)
    {
        try {
            $out_trade_no = $data['out_trade_no'];
            $total_amount = $data['total_amount'];
            $remark = $data['remark'];
            $notify_url = $data['notify_url'];
            $url = $data['request_url'];
            $merchant_no = $data['merchant_no'];
            $returnParams = $data['return_params'];//原样返回
            $des_key = $data['des_key'];
            $md_key = $data['md_key'];
            $systemId = $data['systemId'];
            //请求数据
            $data = [
                'merchantNo' => $merchant_no,
                'businessCode' => 'AGGRE',
                'version' => '3.0',
                'outTradeNo' => $out_trade_no,
                'amount' => $total_amount * 100,
                'remark' => $remark,//
                'returnParams' => $returnParams,
                'businessType' => '00',//返回二维码
                'successNotifyUrl' => $notify_url,
            ];

            $obj = new BaseController();
            $obj->des_key = $des_key;
            $obj->md_key = $md_key;
            $obj->systemId = $systemId;
//            Log::info('随行付-生成动态二维码');
//            Log::info($data);
            $re = $obj->execute($data, $url);
//            Log::info($re);

            //系统错误
            if ($re['status'] == 0) {
                return $re;
            }

            //业务成功
            if ($re['data']['resultCode'] == "SUCCESS") {
                return [
                    'status' => 1,
                    'message' => '返回成功',
                    'code_url' => $re['data']['scanUrl'],
                    'data' => $re['data']
                ];
            } else {
                return [
                    'status' => 0,
                    'message' => $re['data']['errCodeDes']
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => '0',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }


    //静态码提交-公共
    public function qr_submit($data)
    {
        try {
            $orderNo = $data['out_trade_no'];
            $total_amount = $data['total_amount'];
            $driveNo = $data['device_id'];
            $payWay = $data['pay_type'];
            $orgId = $data['orgId'];
            $open_id = $data['code'];
            $custId = $data['custId'];
            $txamt = isset($total_amount) ? intval($total_amount * 100) : '';
            $reqData = [
                'orderNo' => $orderNo,//商户订单号
                'driveNo' => $driveNo,//设备号
                'agetId' => $orgId,//机构号
                'custId' => $custId,//商户号
                'txamt' => $txamt,//订单总金额分
                'txamtOrder' => $txamt,//原订单金额
                'access' => '3',//
                'ip' => get_client_ip(),
                'openid' => $open_id,
                'timestamp' => date('Y-m-d H:i:m', time()),
                'version' => '1.0.0',
                'signType' => 'RSA'
            ];
            if ($payWay == 'ALIPAY') {
                $reqData['zfbappid'] = $data['zfbappid'];
                $reqData['payWay'] = '2';
            }
            if ($payWay == 'WECHAT') {
                $reqData['traType'] = '5';
                $reqData['wxAppid'] = $data['wxAppid'];
                $reqData['payWay'] = '1';
            }
            if ($payWay == 'UNIONPAY') {
                $reqData['payWay'] = '3';
                $reqData['custLogin'] = $data['custLogin'];
                $reqData['atqTag'] = $data['atqTag'];
            }

            $sign = $this->getSign($reqData);
            $reqData['sign'] = $sign;
            Log::info('邮驿付客户主扫支付--params：');
            Log::info($reqData);
            $re = $this->request($reqData, $this->jsapi_url);
            Log::info("邮驿付客户主扫支付--" . $re);
            $res_arr = json_decode($re, true);
            //系统错误
            if (!$res_arr) {
                return $res_arr;
            }

            //业务成功
            if ($res_arr['code'] == "000000") {
                //交易成功
                return [
                    'status' => 1,
                    'message' => $res_arr['msg'],
                    'data' => $res_arr['data'],
                ];

            } elseif ($res_arr['code'] == "222222") {
                return [
                    'status' => '2',
                    'message' => $res_arr['msg'],
                    'data' => $res_arr['data']
                ];
            } else {
                return [
                    'status' => '0',
                    'message' => $res_arr['msg'],
                    'data' => $res_arr['data']
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => '0',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }


    //获取银联标志
    public function get_un_openid($data)
    {
        try {
            $agetId = $data['agetId'];
            $custId = $data['custId'];
            $ip = '154.8.143.104';
            $payCode = $data['payCode'];
            $authCode = $data['authCode'];
            $trmNo = $data['trmNo'];
            $data = [
                'agetId' => $agetId,
                'custId' => $custId,
                'ip' => $ip,
                'payCode' => $payCode,
                'authCode' => $authCode,
                'trmNo' => $trmNo,
                'access' => '3',
                'timestamp' => date('Y-m-d H:i:m', time()),
                'version' => '1.0.0',
                'signType' => 'RSA'
            ];
            $sign = $this->getSign($data);
            $data['sign'] = $sign;
            $re = $this->request($data, $this->getPaypalTag_url);
            $res_arr = json_decode($re, true);
            //系统错误
            if (!$res_arr) {
                return $res_arr;
            }

            //业务成功
            if ($res_arr['code'] == "000000") {
                return [
                    'status' => 1,
                    'message' => $res_arr['msg'],
                    'data' => $res_arr['data'],
                ];
            } else {
                return [
                    'status' => 0,
                    'message' => $res_arr['msg']
                ];
            }
        } catch
        (\Exception $exception) {
            return [
                'status' => 0,
                'message' => $exception->getMessage(),
            ];
        }
    }

    //查询商户审核结果
    public function getCustResult($data)
    {
        try {
            $agetId = $data['agetId'];
            $custId = $data['custId'];
            $data = [
                'agetId' => $agetId,
                'custId' => $custId,
                'version' => '1.0.0',
            ];

            $sign = $this->getSign($data);
            $data['sign'] = $sign;
            $url = $this->getCustResult_url . "?agetId=" . $agetId . "&custId=" . $custId . "&version=1.0.0". "&sign=" . $sign;
            Log::info('获取用户授权--data');
            Log::info($url);
            $re = $this->request_get($url);
            Log::info('获取用户授权返回--'.$re);
            $res_arr = json_decode($re, true);
            //系统错误
            if (!$res_arr) {
                return $res_arr;
            }

            //业务成功
            if ($res_arr['code'] == "000000") {
                return [
                    'status' => 1,
                    'message' => $res_arr['msg'],
                    'data' => $res_arr['data'],
                ];
            } else {
                return [
                    'status' => 0,
                    'message' => $res_arr['msg']
                ];
            }
        } catch
        (\Exception $exception) {
            return [
                'status' => 0,
                'message' => $exception->getMessage(),
            ];
        }
    }

}
