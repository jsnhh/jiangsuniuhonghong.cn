<?php

namespace App\Api\Controllers\PostPay;


use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class BaseController
{
    public $public_key = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAh+0nQKyePvMO9hji8AH01VvyfHr6Xje1Yc1HjO+GdmR3oOxylrjejvZLMTCrnMvW/ObqEnday6j5moLVCbJ2XGg15V9ML8l2wOREyP8Nf2D+21yNFRB3ivgu3JPPJu6b8Q5y/98CBfUffZ9tKga4P+IKGW7EaYpCYEnDOCXQU7ds2Vd7/TLcWug5ILtzqdFFDdp6EQbrNR4/VSGy25ZmN9/FCHXwDvMRD6mb5GW5Mj8WrXK2E6hLMYqKuqWcBDBdVgqIY3GAyuWQlgduoHE2H4L7/jh4o1c6BJCczTCHu6UyQydN6oIPvZXGStULPIcVCcDrwu6KhVHXyhqigCk6xQIDAQAB';
    public $scan_url = 'http://testterpay.postar.cn/yyfsevr/order/scanByMerchant';//商户主扫接口
    public $jsapi_url = 'http://testterpay.postar.cn/yyfsevr/order/pay';//客户主扫接口
    public $getPaypalTag_url = 'http://testterpay.postar.cn/yyfsevr/order/getPaypalTag';//获取银联标识接口
    public $refund_url = 'http://testterpay.postar.cn/yyfsevr/order/refund';//退款接口
    public $tradeQuery_url = 'http://testterpay.postar.cn/yyfsevr/order/tradeQuery';//订单查询接口
    public $refundQuery_url = 'http://testterpay.postar.cn/yyfsevr/order/refundQuery';//退款查询接口
    public $getCustResult_url='http://testterpay.postar.cn/yyfsevr/addCust/getResult';//查询商户审核接口
    public function getSign($data)
    {
        $str = $this->formatBizQueryParaMap($data, false);
        $data = hash("sha256", $str);
        $pubKey = "-----BEGIN PUBLIC KEY-----\n" .
            wordwrap($this->public_key, 64, "\n", true) .
            "\n-----END PUBLIC KEY-----";
        $return_en = openssl_public_encrypt($data, $crypted, $pubKey);
        if (!$return_en) {
            return ('加密失败,请检查RSA公钥');
        }
        $str_encrypt_base64 = base64_encode($crypted);
        return $str_encrypt_base64;

    }

    //组装签名函数
    function formatBizQueryParaMap($paraMap, $urlencode)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if ($urlencode) {
                $v = urlencode($v);
            }
            $buff .= $k . "=" . $v . "&";
        }
        $reqPar = '';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }

    public function request($params, $url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); //访问超时时间
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        if (!empty($params)) {
            $data = stripslashes(json_encode($params));
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json; charset=UTF-8',
            ]);
        }
        //本地忽略校验证书
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $tmpInfo = curl_exec($curl);
        return $tmpInfo;
    }
    public function request_get($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        //本地忽略校验证书
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $tmpInfo = curl_exec($curl);
        return $tmpInfo;
    }
}