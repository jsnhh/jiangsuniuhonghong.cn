<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/8/21
 * Time: 16:04
 */
namespace App\Api\Controllers\MeiTuanPeiSong;


use App\Models\MeituanpsCancels;
use App\Models\MeituanpsConfigs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CancelController extends BaseController
{

    //取消订单
    public function orderDelete(Request $request)
    {
        $token = $this->parseToken();
        $shop_id = $request->get('storeId', '');
        $delivery_id = $request->get('deliveryId', '');
        $mt_peisong_id = $request->get('mtPeisongId', '');
        $cancel_reason_id = $request->get('cancelReasonId', '');
        $cancel_reason = $request->get('cancelReason', '');

        $obj = MeituanpsConfigs::where('store_id', $shop_id)
            ->first();
        if (!$obj) {
            return json_encode([
                'status' => '2',
                'message' => '美团配送参数未配置'
            ]);
        }

        $secret = $obj->secret;

        try {
            $requestData = [
                'delivery_id' => $delivery_id, //配送活动标识
                'mt_peisong_id' => $mt_peisong_id, //美团配送内部订单id，最长不超过32个字符
                'cancel_reason_id' => $cancel_reason_id, //取消原因类别，默认为接入方原因.101-顾客主动取消;102-顾客更改配送时间/地址;103-备货、包装、货品质量问题取消;199-其他接入方原因;201-配送服务态度问题取消;202-骑手配送不及时;203-骑手取货不及时;299-其他美团配送原因;399-其他原因
                'cancel_reason' => $cancel_reason, //详细取消原因，最长不超过256个字符
            ];
            Log::info('美团配送-取消订单');
            Log::info($requestData);
            $res = $this->http_post($this->order_delete, $requestData, $secret); //-1 系统错误 1-成功 2-失败
            Log::info($res);
            if ($res['status'] == '1') {
                $cancelData = [
                    'order_id' => $res['data']['order_id'],
                    'delivery_id' => $delivery_id,
                    'mt_peisong_id' => $mt_peisong_id,
                    'cancel_reason_id' => $cancel_reason_id,
                    'cancel_reason' => $cancel_reason
                ];
                if ($shop_id) $cancelData['store_id'] = $shop_id;
                $result = MeituanpsCancels::create($cancelData);
                if (!$result) {
                    return json_encode([
                        'status' => '2',
                        'message' => '美团配送取消订单,写入失败'
                    ]);
                }
            }

            return $res;
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


//    //
//    public function orderCreateByShop22(Request $request)
//    {
//        $token = $this->parseToken();
//        $config_id = $request->get('config_id', '');
//
//        $obj = MeituanpsConfigs::where('config_id', $config_id)
//            ->first();
//        if (!$obj) {
//            return json_encode([
//                'status' => '2',
//                'message' => '美团配送参数未配置'
//            ]);
//        }
//
//        $secret = $obj->secret;
//
//        try {
//            $requestData = [
//                '' => $, //
//                '' => $, //
//            ];
//            $res = $this->http_post($this->url, $requestData, $secret);
//            return $res;
//        } catch (\Exception $ex) {
//            return json_encode([
//                'status' => '-1',
//                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
//            ]);
//        }
//    }


}
