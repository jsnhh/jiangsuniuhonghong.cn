<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/8/21
 * Time: 16:05
 */
namespace App\Api\Controllers\MeiTuanPeiSong;


use App\Models\MeituanpsConfigs;
use App\Models\MeituanpsOrders;
use App\Models\MeituanpsOrdersError;
use App\Models\MeituanpsStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CallBackController extends BaseController
{

    //门店状态回调
    public function callBackStoreStatus(Request $request)
    {
        $data = $request->all();
        Log::info('美团配送门店状态回调');
        Log::info($data);

        try {
            if (isset($data)) {
                $data = json_decode($data, true);
                if (isset($data['code']) && $data['code'] == 0) {
                    if (isset($data['data']['shop_id'])) {
                        $store_id = $data['data']['shop_id'];
                        $store_obj = MeituanpsStore::where('shop_id', $store_id)->first();
                        if ($store_obj && $store_obj->status == 0) {
                            $update_data = ['status' => $data['data']['status']];
                            if (isset($data['data']['reject_message'])) $update_data['status_desc'] = $data['data']['reject_message'];
                            $res = $store_obj->update($update_data);
                            if (!$res) {
                                Log::info($store_id.': 门店状态回调更新失败');
                            }
                        }

                        return json_encode(['code' => 0]);
                    }
                }
            }
        } catch (\Exception $ex) {
            Log::info('美团配送门店状态回调error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    //订单状态回调
    public function callBackOrderStatus(Request $request)
    {
        $data = $request->all();
        Log::info('美团配送订单状态回调');
        Log::info($data);

        try {
            if (isset($data)) {
                $data = json_decode($data, true);
                if (isset($data['code']) && $data['code'] == 0) {
                    if (isset($data['data']['delivery_id'])) {
                        $delivery_id = $data['data']['delivery_id']; //配送活动标识
                        $mt_peisong_id = $data['data']['mt_peisong_id']; //美团配送内部订单id
                        $order_id = $data['data']['order_id']; //外部订单号

                        $order_obj = MeituanpsOrders::where('delivery_id', $delivery_id)
                            ->where('order_id', $order_id)
                            ->where('mt_peisong_id', $mt_peisong_id)
                            ->first();
                        if ($order_obj && $order_obj->status == 0) {
                            $update_data = [
                                'status' => $data['data']['status'] //状态代码
                            ];
                            $res = $order_obj->update($update_data);
                            if (!$res) {
                                Log::info($order_id.': 美团配送订单状态,更新失败');
                            }
                        } else {
                            Log::info($order_id.': 美团配送订单未找到');
                        }

                        return json_encode(['code' => 0]);
                    }
                }
            }
        } catch (\Exception $ex) {
            Log::info('美团配送订单状态回调error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    //订单异常回调
    public function callBackOrderErrStatus(Request $request)
    {
        $data = $request->all();
        Log::info('美团配送订单异常回调');
        Log::info($data);

        try {
            if (isset($data)) {
                $data = json_decode($data, true);
                if (isset($data['code']) && $data['code'] == 0) {
                    if (isset($data['data']['delivery_id'])) {
                        $delivery_id = $data['data']['delivery_id']; //配送活动标识
                        $mt_peisong_id = $data['data']['mt_peisong_id']; //美团配送内部订单id
                        $order_id = $data['data']['order_id']; //外部订单号

                        $order_obj = MeituanpsOrders::where('delivery_id', $delivery_id)
                            ->where('order_id', $order_id)
                            ->where('mt_peisong_id', $mt_peisong_id)
                            ->first();
                        if ($order_obj && $order_obj->status == 30) { //0-待调度;20-已接单;30-已取货;50-已送达;99-已取消
                            $order_err_data = [
                                'delivery_id' => $data['data']['delivery_id'],
                                'mt_peisong_id' => $data['data']['mt_peisong_id'],
                                'order_id' => $data['data']['order_id'],
                                'exception_id' => $data['data']['exception_id'],
                                'exception_code' => $data['data']['exception_code'],
                                'exception_descr' => $data['data']['exception_descr'],
                                'courier_name' => $data['data']['courier_name'],
                                'courier_phone' => $data['data']['courier_phone'],
                                'appkey' => $data['data']['appkey'],
                                'error_time' => date('Y-m-d H:i:s', $data['data']['exception_time']),
                                'updated_at' => date('Y-m-d H:i:s', $data['data']['timestamp']),
                            ];
                            $res = MeituanpsOrdersError::create($order_err_data);
                            if (!$res) {
                                Log::info($order_id.': 美团配送订单异常状态创建失败');
                            }
                        } else {
                            Log::info($order_id.': 美团配送订单未找到');
                        }

                        return json_encode(['code' => 0]);
                    }
                }
            }
        } catch (\Exception $ex) {
            Log::info('美团配送订单状态回调error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


}
