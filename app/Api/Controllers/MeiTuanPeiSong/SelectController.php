<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/8/21
 * Time: 16:02
 */
namespace App\Api\Controllers\MeiTuanPeiSong;


use App\Models\MeituanpsCategory;
use App\Models\MeituanpsCity;
use App\Models\MeituanpsConfigs;
use App\Models\MeituanpsOrders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class SelectController extends BaseController
{

    //查询订单状态
    public function orderStatusQuery(Request $request)
    {
        $token = $this->parseToken();
        $store_id = $request->get('storeId', '');
        $delivery_id = $request->get('deliveryId', '');
        $mt_peisong_id = $request->get('mtPeisongId', '');

        $obj = MeituanpsConfigs::where('store_id', $store_id)
            ->first();
        if (!$obj) {
            return json_encode([
                'status' => '2',
                'message' => '美团配送参数未配置'
            ]);
        }

        $order = MeituanpsOrders::where('delivery_id', $delivery_id)
            ->where('mt_peisong_id', $mt_peisong_id)
            ->first();
        if (!$order) {
            return json_encode([
                'status' => '2',
                'message' => '美团配送订单不存在'
            ]);
        }

        $secret = $obj->secret;

        try {
            $requestData = [
                'delivery_id' => $delivery_id, //配送活动标识
                'mt_peisong_id' => $mt_peisong_id //美团配送内部订单id，最长不超过32个字符
            ];
            Log::info('美团配送-查询订单状态');
            Log::info($requestData);
            $res = $this->http_post($this->order_status_query, $requestData, $secret);
            Log::info($res);
            if ($res['status'] == '1') {
//                if ($order->status != 20) {
                    $updateData = [
                        'status' => $res['data']['status']
                    ];
                    if (isset($res['data']['operate_time'])) $updateData['operate_time'] = date('Y-m-d H:i:s', $res['data']['operate_time']);
                    $result = $order->update($updateData);
//                }
            }

            return $res;
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    //获取骑手当前位置
    public function orderRiderLocation(Request $request)
    {
        $token = $this->parseToken();
        $store_id = $request->get('storeId', '');
        $delivery_id = $request->get('deliveryId', ''); //配送活动标识
        $mt_peisong_id = $request->get('mtPeisongId', ''); //美团配送内部订单id，最长不超过32个字符

        $config_obj = MeituanpsConfigs::where('store_id', $store_id)->first();
        if (!$config_obj) {
            return json_encode([
                'status' => '2',
                'message' => '美团配送参数未配置'
            ]);
        }

        $order = MeituanpsOrders::where('delivery_id', $delivery_id)
            ->where('mt_peisong_id', $mt_peisong_id)
            ->first();
        if (!$order) {
            return json_encode([
                'status' => '2',
                'message' => '美团配送订单不存在'
            ]);
        }

        $secret = $config_obj->secret;

        try {
            $requestData = [
                'delivery_id' => $delivery_id,
                'mt_peisong_id' => $mt_peisong_id
            ];
            Log::info('美团配送-获取骑手当前位置');
            Log::info($requestData);
            $res = $this->http_post($this->order_rider_location, $requestData, $secret);
            Log::info($res);
            if ($res['status'] == '1') {
               return json_encode([
                   'status' => '1',
                   'message' => '获取位置成功',
                   'data' => [
                       'lat' => ($res['data']['lat']/1000000), //纬度
                       'lng' => ($res['data']['lng']/1000000) //经度
                   ]
               ]);
            }

            return $res;
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    //查询合作方配送范围
    public function shopAreaQuery(Request $request)
    {
        $token = $this->parseToken();
        $store_id = $request->get('storeId', ''); //取货门店id
        $delivery_service_code = $request->get('deliveryServiceCode', ''); //配送服务代码,回传时区分,服务包code和新服务产品code

        $config_obj = MeituanpsConfigs::where('store_id', $store_id)->first();
        if (!$config_obj) {
            return json_encode([
                'status' => '2',
                'message' => '美团配送参数未配置'
            ]);
        }

        $secret = $config_obj->secret;

        try {
            $requestData = [
                'delivery_service_code' => $delivery_service_code,
                'shop_id' => $store_id
            ];
            Log::info('美团配送-查询合作方配送范围');
            Log::info($requestData);
            $res = $this->http_post($this->shop_area_query, $requestData, $secret);
            Log::info($res);

            return $res;
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    //查询门店信息
    public function shopQuery(Request $request)
    {
        $token = $this->parseToken();
        $store_id = $request->get('storeId', ''); //取货门店id，即合作方向美团提供的门店id

        $config_obj = MeituanpsConfigs::where('store_id', $store_id)->first();
        if (!$config_obj) {
            return json_encode([
                'status' => '2',
                'message' => '美团配送参数未配置'
            ]);
        }

        $secret = $config_obj->secret;

        try {
            $requestData = [
                'shop_id' => $store_id
            ];
            Log::info('美团配送-查询门店信息');
            Log::info($requestData);
            $res = $this->http_post($this->shop_query, $requestData, $secret);
            Log::info($res);

            return $res;
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    //城市id列表
    public function getMeiTuanCityList(Request $request)
    {
        try {
            $city_list = Cache::has('mtps_city');
            if ($city_list) {
                $city_list = Cache::get('mtps_city');
            } else {
                $city_list = MeituanpsCity::get();
                if (!$city_list) {
                    $this->status = 2;
                    $this->message = '未找到城市信息';
                    return $this->format();
                }

                Cache::put('mtps_city', $city_list, 1000000);
            }

            $this->status = 1;
            $this->message = '美团城市信息';
            return $this->format($city_list);
        } catch (\Exception $e) {
            $this->status = -1;
            $this->message = $e->getMessage().' | '.$e->getFile().' | '.$e->getLine();
            return $this->format();
        }
    }


    //品类列表
    public function getMeiTuanCategoryList(Request $request)
    {
        try {
            $category_list = Cache::has('mtps_category');
            if ($category_list) {
                $category_list = Cache::get('mtps_category');
            } else {
                $category_list = MeituanpsCategory::where('p_id', 0)->with('children')->get();
                if (!$category_list) {
                    $this->status = 2;
                    $this->message = '未匹配品类列表';
                    return $this->format();
                }

                Cache::put('mtps_category', $category_list, 1000000);
            }

            $this->status = 1;
            $this->message = '美团品类列表';
            return $this->format($category_list);
        } catch (\Exception $e) {
            $this->status = -1;
            $this->message = $e->getMessage().' | '.$e->getFile().' | '.$e->getLine();
            return $this->format();
        }
    }


}
