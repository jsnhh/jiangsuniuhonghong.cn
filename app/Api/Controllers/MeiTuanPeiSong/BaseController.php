<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/8/21
 * Time: 12:01
 */
namespace App\Api\Controllers\MeiTuanPeiSong;


use Illuminate\Support\Facades\Log;
use App\Api\Controllers\BaseController as BBaseController;

class BaseController extends BBaseController
{
    private $url = 'https://peisongopen.meituan.com/api/';
    protected $order_create_by_shop = 'order/createByShop'; //订单创建(门店方式),合作方根据已录入的门店信息 将订单发送给美团配送平台
    protected $order_create_by_coordinates = 'order/createByCoordinates'; //订单创建(送货分拣方式),根据合作方的送件地址，自动选择合适的中间仓库作为线下货物的交接地点，并完成末端配送
    protected $order_delete = 'order/delete'; //取消订单.订单在未完成状态下，合作方可以取消订单
    protected $order_status_query = 'order/status/query'; //查询订单状态,查询订单状态及对应的骑手信息（仅支持90天内产生的订单）
    protected $order_evaluate = 'order/evaluate'; //评价骑手,仅接受已完成订单的评价， 未完成及删除的订单不接受评价；每笔订单只可进行一次评价，不允许重复评价
    protected $order_check = 'order/check'; //配送能力校验,根据合作方提供的模拟发单参数，确定美团是否可配送。主要校验项：门店是否存在、门店配送范围、门店营业时间、门店支持的服务包
    protected $order_rider_location = 'order/rider/location'; //获取骑手当前位置
    protected $shop_area_query = 'shop/area/query'; //查询合作方配送范围,根据配送服务代码、取货门店id获取该门店支持的配送范围
    protected $shop_create = 'shop/create'; //门店创建,合作方提交门店信息（注意测试推单仍然用系统默认test_0001门店）
    protected $shop_query = 'shop/query'; //查询门店信息,查询门店基本信息
    private $version = '1.0'; //API协议版本,可选值：1.0


    //发起请求 -1 系统错误 1-成功 2-失败
    public function http_post($url, $data, $secret, $type = 'post', $json = 'json')
    {
        $sign = $this->sortReqFields($data, $secret); //加签
        $data['sign'] = $sign;
        $data = json_encode($data, JSON_UNESCAPED_UNICODE);
        $url = $this->url.$url;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("content-type: application/x-www-form-urlencoded; charset=UTF-8"));
        if ($type == 'post'){
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
//        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, true);
        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($response == NULL) {
            curl_close($curl);
            return [
                'status' => '-1',
                'message' => '系统错误'
            ];
        }
        elseif ($httpCode != "200") {
            curl_close($curl);
            return [
                'status' => '-1',
                'message' => '系统错误'
            ];
        }
        else {
            curl_close($curl);
        }
//        Log::info('美团配送-原始返回');
//        Log::info($url);
//        Log::info($response);

        if ($json == 'json') {
            $response = json_decode($response, true);
        }

        if ($response['code'] == 0) {
            return [
                'status' => '1',
                'message' => '成功',
                'data' => $response['data']
            ];
        } else {
            return [
                'status' => '2',
                'message' => $response['message'] ?? $this->getMessageByCode($response['code'])
            ];
        }
    }


    /**
     * 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
     * @param $para
     * @return string
     */
    function createLinkstring($para)
    {
        $params = [];
        foreach ($para as $key => $value) {
            if (is_array($value)) {
                $value = stripslashes(json_encode($value, JSON_UNESCAPED_UNICODE));
            }
            $params[] = $key . '=' . $value;
        }
        $data = implode("&", $params);

        return $data;
    }


    //拼接需要签名的内容
    private function sortReqFields($data, $secret)
    {
        //递归解嵌套操作
        $Parameters = [];
        foreach ($data as $k => $v) {
            if (is_array($v)) {
                $Parameters[$k] = $this->sortReqFields($v, $secret);
            } else {
                $Parameters[$k] = $v;
            }
        }

        //按字典序排序参数
        ksort($Parameters);

        //将所有系统参数及业务参数（其中sign，byte[]及值为空的参数除外）按照参数名的字典顺序排序
        $sign = '';
        foreach ($Parameters as $k => $v) {
            if ($v == null || $v == '') {
                continue;
            }
            //如果值非空则添加
            $sign .= $k . $v;
        }
        $sign = $secret.$sign;
        Log::info('加签排序后值');
        Log::info($sign);
        return strtolower(sha1($sign));
    }


    //状态码定义
    public function getMessageByCode($code)
    {
        switch ($code) {
            case '0':  return '成功';  break;
            case '1':  return '系统错误,建议三次重试,每次间隔10S';  break;
            case '2':  return '缺少系统参数';  break;
            case '3':  return '缺少参数,数据不完整';  break;
            case '4':  return '参数格式错误';  break;
            case '5':  return 'app_key不存在或合作方不合作';  break;
            case '6':  return '签名验证错误';  break;
            case '7':  return '没有权限操作这条数据,检查是否正式账号操作测试账号数据或测试账号操作正式账号数据';  break;
            case '8':  return 'api版本号不支持';  break;
            case '9':  return '请求时间超出最大时限,检查入参的timestamp是否满足:推单时间-timestamp<600秒';  break;
            case '10':  return '当前ip不在可访问列表中,请联系技术人员排查';  break;
            case '11':  return '接口流控,三次重试,每次间隔10S';  break;
            case '101':  return '订单id正在创建配送订单,三次重试,每次间隔10S';  break;
            case '102':  return '订单不存在,检查入参delivery_id、mt_peisong_id';  break;
            case '103':  return '货品信息有误,金额过大,体积过大,重量过沉等';  break;
            case '104':  return '订单已完成,不能取消';  break;
            case '105':  return '订单预计完成时间不在可接受范围';  break;
            case '106':  return '所选城市未合作,请联系技术人员排查';  break;
            case '107':  return '所选配送服务类型无效';  break;
            case '108':  return '非合作时间';  break;
            case '109':  return '取货地址超区';  break;
            case '110':  return '送货地址超区';  break;
            case '111':  return '不在美团配送站点营业时间内';  break;
            case '112':  return '门店不存在';  break;
            case '113':  return '不在门店营业时间内';  break;
            case '114':  return '门店非营业状态';  break;
            case '115':  return '门店未开通所选服务包';  break;
            case '116':  return '超过所选服务包允许的时效限制';  break;
            case '117':  return '预约时间超出范围';  break;
            case '118':  return '订单类型不符合要求';  break;
            case '119':  return '门店不支持预约单';  break;
            case '120':  return '预约时间超出范围';  break;
            case '121':  return '门店不支持营业时间外发预订单';  break;
            case '122':  return '保价服务异常	三次重试,每次间隔10S';  break;
            case '123':  return '价格缺失,无法投保';  break;
            case '124':  return '模拟操作失败';  break;
            case '125':  return '订单尚未完成,不允许进行评价操作';  break;
            case '126':  return '订单已评价,不允许重复评价';  break;
            case '127':  return '坐标方式发出的订单,不允许评价操作';  break;
            case '128':  return '所选位置暂未开通服务';  break;
            case '129':  return '订单未调度,请分配骑手后重试';  break;
            case '130':  return '订单已送达,无法获取位置信息';  break;
            case '131':  return '订单已取消,无法获取位置信息';  break;
            case '132':  return '骑手目前没有位置信息,请稍后重试';  break;
            case '133':  return '创建订单接口禁用中,请咨询您的销售经理';  break;
            case '144':  return '当前区域运力紧张,无法创建订单';  break;
            case '145':  return '坐标没有落在区域内';  break;
            case '152':  return '因配送范围管控,取货/送货地址超区';  break;
            default:  return '未知错误,请请联系技术人员';
        }
    }


}
