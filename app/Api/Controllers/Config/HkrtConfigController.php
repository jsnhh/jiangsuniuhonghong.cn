<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2019/05/19
 * Time: 17:14 PM
 */
namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\HkrtConfig;
use App\Models\HkrtStore;

class HkrtConfigController extends Controller
{
    //配置读取
    public function hkrt_config($config_id)
    {
        $config = HkrtConfig::where('config_id', $config_id)->first();
        if (!$config) {
            $config = HkrtConfig::where('config_id', '1234')->first();
        }

        return $config;
    }


    //进件门店读取
    public function hkrt_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $hkrt_Store = HkrtStore::where('store_id', $store_id)->first();
            if (!$hkrt_Store) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $hkrt_Store = HkrtStore::where('store_id', $store_pid_id)->first();
            }
        } else {
            $hkrt_Store = HkrtStore::where('store_id', $store_id)->first();
        }

        return $hkrt_Store;
    }


}
