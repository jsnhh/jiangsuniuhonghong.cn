<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2021/03/02
 * Time: 13:44
 */
namespace App\Api\Controllers\Config;

use App\Http\Controllers\Controller;
use App\Models\QfpayConfig;
use App\Models\QfpayStore;
use App\Models\Store;

class QfPayConfigController extends Controller
{

    public function qfpay_config($config_id)
    {
        //配置取缓存
        $qfPayConfig = QfpayConfig::where('config_id', $config_id)
            ->first();
        if (!$qfPayConfig) {
            $qfPayConfig = QfpayConfig::where('config_id', '1234')->first();
        }

        return $qfPayConfig;
    }


    public function qfpay_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $qfPayStore = QfpayStore::where('store_id', $store_id)->first();
            if (!$qfPayStore) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $qfPayStore = QfpayStore::where('store_id', $store_pid_id)->first();
            }
        } else {
            $qfPayStore = QfpayStore::where('store_id', $store_id)->first();
        }

        return $qfPayStore;
    }


}
