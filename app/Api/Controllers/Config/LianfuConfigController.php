<?php
namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\LianfuStores;
use App\Models\LinkageConfigs;
use App\Models\Store;

class LianfuConfigController extends Controller
{

    public function lianfu_config($config_id)
    {
        //配置取缓存
        $config = LinkageConfigs::where('config_id', $config_id)->first();
        if (!$config) {
            $config = LinkageConfigs::where('config_id', '1234')->first();
        }

        return $config;
    }


    public function lianfu_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $H_merchant = LianfuStores::where('store_id', $store_id)->first();
            if (!$H_merchant) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $H_merchant = LianfuStores::where('store_id', $store_pid_id)->first();
            }
        } else {
            $H_merchant = LianfuStores::where('store_id', $store_id)->first();
        }

        return $H_merchant;
    }


}
