<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/4/20
 * Time: 10:38 AM
 */

namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\AdaPayConfig;
use App\Models\AdaPayStore;
use App\Models\Store;


class AdaPayConfigController extends Controller
{

    public function ada_pay_config($config_id)
    {
        //配置取缓存
        $config = AdaPayConfig::where('config_id', $config_id)
            ->first();
        if (!$config) {
            $config = AdaPayConfig::where('config_id', '1234')->first();
        }

        return $config;
    }


    public function ada_pay_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $AdaPayStore = AdaPayStore::where('store_id', $store_id)->first();
            if (!$AdaPayStore) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $AdaPayStore = AdaPayStore::where('store_id', $store_pid_id)->first();
            }
        } else {
            $AdaPayStore = AdaPayStore::where('store_id', $store_id)->first();
        }

        return $AdaPayStore;
    }


}
