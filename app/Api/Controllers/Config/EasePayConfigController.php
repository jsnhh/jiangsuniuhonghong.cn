<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/4/20
 * Time: 10:38 AM
 */

namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\EasePayConfig;
use App\Models\EasePayStore;
use App\Models\Store;
use App\Models\EasypayConfig;
use App\Models\EasypayStore;

class EasePayConfigController extends Controller
{

    public function easepay_config($config_id)
    {
        //配置取缓存
        $config = EasePayConfig::where('config_id', $config_id)
            ->first();
        if (!$config) {
            $config = EasePayConfig::where('config_id', '1234')->first();
        }

        return $config;
    }


    public function easepay_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $easePayStore = EasePayStore::where('store_id', $store_id)->first();
            if (!$easePayStore) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $easePayStore = EasePayStore::where('store_id', $store_pid_id)->first();
            }
        } else {
            $easePayStore = EasePayStore::where('store_id', $store_id)->first();
        }

        return $easePayStore;
    }


}
