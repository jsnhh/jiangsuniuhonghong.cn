<?php
namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\WeixinaConfig;
use App\Models\WeixinaNotifyTemplate;
use App\Models\WeixinaStore;
use App\Models\WeixinConfig;
use App\Models\WeixinNotifyTemplate;
use App\Models\WeixinStore;
use Illuminate\Support\Facades\Log;

class WeixinConfigController extends Controller
{
    //微信官方配置 easywechat
    public function weixin_config($config_id)
    {
        $config = WeixinConfig::where('config_id', $config_id)->first();
        if (!$config) {
            $config = WeixinConfig::where('config_id', '1234')->first();
        }

        $options = [
            'app_id' => $config->app_id,
            'app_secret' => $config->app_secret,
            'payment' => [
                'merchant_id' => $config->wx_merchant_id,
                'key' => $config->key,
                'cert_path' => public_path() . $config->cert_path, // XXX: 绝对路径！！！！
                'key_path' => public_path() . $config->key_path, // XXX: 绝对路径！！！！
                'notify_url' => $config->notify_url // 你也可以在下单时单独设置来想覆盖它
            ]
        ];

        return $options;
    }


    public function weixina_config($config_id)
    {
        $config = WeixinaConfig::where('config_id', $config_id)->first();
        if (!$config) {
            $config = WeixinaConfig::where('config_id', '1234')->first();
        }

        $options = [
            'app_id' => $config->app_id,
            'app_secret' => $config->app_secret,
            'payment' => [
                'merchant_id' => $config->wx_merchant_id,
                'key' => $config->key,
                'cert_path' => public_path() . $config->cert_path, // XXX: 绝对路径！！！！
                'key_path' => public_path() . $config->key_path, // XXX: 绝对路径！！！！
                'notify_url' => $config->notify_url // 你也可以在下单时单独设置来想覆盖它
            ]
        ];

        return $options;
    }


    //微信官方配置
    public function weixin_config_obj($config_id)
    {
        $config = WeixinConfig::where('config_id', $config_id)->first();
        if (!$config) {
            $config = WeixinConfig::where('config_id', '1234')->first();
        }

        return $config;
    }


    public function weixina_config_obj($config_id)
    {
        $config = WeixinaConfig::where('config_id', $config_id)->first();
        if (!$config) {
            $config = WeixinaConfig::where('config_id', '1234')->first();
        }

        return $config;
    }


    //微信官方 商户入网
    public function weixin_merchant($store_id, $store_pid = "")
    {
        if ($store_pid) {
            //分店配置
            $WeixinStore = WeixinStore::where('store_id', $store_id)->first();
            if (!$WeixinStore) {
                $store_pid_id = ""; //上级id
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $WeixinStore = WeixinStore::where('store_id', $store_pid_id)->first();
            }
        } else {
            $WeixinStore = WeixinStore::where('store_id', $store_id)->first();
        }

        return $WeixinStore;
    }


    public function weixina_merchant($store_id, $store_pid = "")
    {
        if ($store_pid) {
            //分店配置
            $WeixinStore = WeixinaStore::where('store_id', $store_id)->first();
            if (!$WeixinStore) {
                $store_pid_id = ""; //上级id
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $WeixinStore = WeixinaStore::where('store_id', $store_pid_id)->first();
            }
        } else {
            $WeixinStore = WeixinaStore::where('store_id', $store_id)->first();
        }

        return $WeixinStore;
    }


    //微信官方 推送消息模板
    public function weixin_template($config_id, $type)
    {
        $WeixinNotifyTemplate = WeixinNotifyTemplate::where('config_id', $config_id)
            ->where('type', $type)
            ->first();
        if (!$WeixinNotifyTemplate) {
            $WeixinNotifyTemplate = WeixinNotifyTemplate::where('config_id', '1234')
                ->where('type', $type)
                ->first();
        }

        return $WeixinNotifyTemplate;
    }


    public function weixina_template($config_id, $type)
    {
        $WeixinNotifyTemplate = WeixinaNotifyTemplate::where('config_id', $config_id)
            ->where('type', $type)
            ->first();
        if (!$WeixinNotifyTemplate) {
            $WeixinNotifyTemplate = WeixinaNotifyTemplate::where('config_id', '1234')
                ->where('type', $type)
                ->first();
        }

        return $WeixinNotifyTemplate;
    }


}
