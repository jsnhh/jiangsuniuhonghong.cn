<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/4/20
 * Time: 10:38 AM
 */

namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\PostPayConfig;
use App\Models\PostPayStore;
use App\Models\Store;
use App\Models\EasypayConfig;
use App\Models\EasypayStore;

class PostPayConfigController extends Controller
{

    public function post_pay_config($config_id)
    {
        //配置取缓存
        $config = PostPayConfig::where('config_id', $config_id)
            ->first();
        if (!$config) {
            $config = PostPayConfig::where('config_id', '1234')->first();
        }

        return $config;
    }


    public function post_pay_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $PostPayStore = PostPayStore::where('store_id', $store_id)->first();
            if (!$PostPayStore) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $PostPayStore = PostPayStore::where('store_id', $store_pid_id)->first();
            }
        } else {
            $PostPayStore = PostPayStore::where('store_id', $store_id)->first();
        }

        return $PostPayStore;
    }


}
