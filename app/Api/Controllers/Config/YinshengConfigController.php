<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2021/01/25
 * Time: 18:04
 */
namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\YinshengConfig;
use App\Models\YinshengStore;

class YinshengConfigController extends Controller
{

    public function yinsheng_config($config_id)
    {
        //配置取缓存
        $config = YinshengConfig::where('config_id', $config_id)
            ->first();
        if (!$config) {
            $config = YinshengConfig::where('config_id', '1234')->first();
        }

        return $config;
    }


    public function yinsheng_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $yinshengStore = YinshengStore::where('store_id', $store_id)->first();
            if (!$yinshengStore) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $yinshengStore = YinshengStore::where('store_id', $store_pid_id)->first();
            }
        } else {
            $yinshengStore = YinshengStore::where('store_id', $store_id)->first();
        }

        return $yinshengStore;
    }


}
