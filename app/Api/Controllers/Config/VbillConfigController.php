<?php
namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\VbillaConfig;
use App\Models\VbillaStore;
use App\Models\VbillConfig;
use App\Models\VbillStore;

class VbillConfigController extends Controller
{

    public function vbill_config($config_id)
    {
        //配置取缓存
        $config = VbillConfig::where('config_id', $config_id)
            ->first();
        if (!$config) {
            $config = VbillConfig::where('config_id', '1234')->first();
        }

        return $config;
    }


    public function vbill_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $VbillStore = VbillStore::where('store_id', $store_id)->first();
            if (!$VbillStore) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $VbillStore = VbillStore::where('store_id', $store_pid_id)->first();
            }
        } else {
            $VbillStore = VbillStore::where('store_id', $store_id)->first();
        }

        return $VbillStore;
    }


    public function vbilla_config($config_id)
    {
        //配置取缓存
        $config = VbillaConfig::where('config_id', $config_id)
            ->first();
        if (!$config) {
            $config = VbillaConfig::where('config_id', '1234')->first();
        }

        return $config;
    }


    public function vbilla_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $VbillStore = VbillaStore::where('store_id', $store_id)->first();
            if (!$VbillStore) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $VbillStore = VbillaStore::where('store_id', $store_pid_id)->first();
            }
        } else {
            $VbillStore = VbillaStore::where('store_id', $store_id)->first();
        }

        return $VbillStore;
    }


}
