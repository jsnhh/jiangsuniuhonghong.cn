<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/07/27
 * Time: 13:14 PM
 */
namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\LinkageConfigs;
use App\Models\LinkageStores;

class LinkageConfigController extends Controller
{
    //配置读取
    public function linkage_config($config_id)
    {
        $config = LinkageConfigs::where('config_id', $config_id)->first();
        if (!$config) {
            $config = LinkageConfigs::where('config_id', '1234')->first();
        }

        return $config;
    }


    //进件门店读取
    public function linkage_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $linkage_Store = LinkageStores::where('store_id', $store_id)->first();
            if (!$linkage_Store) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $linkage_Store = LinkageStores::where('store_id', $store_pid_id)->first();
            }
        } else {
            $linkage_Store = LinkageStores::where('store_id', $store_id)->first();
        }

        return $linkage_Store;
    }


}
