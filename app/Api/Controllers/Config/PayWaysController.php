<?php

namespace App\Api\Controllers\Config;


use App\Models\Store;
use App\Models\StorePayWay;
use App\Models\UserRate;

class PayWaysController
{

    //支付用到 分店没有直接公用总店的 通过ways_source查询
    public function ways_source($ways_source, $store_id, $srore_id_pid)
    {
        $ways = StorePayWay::where('ways_source', $ways_source)
            ->where('store_id', $store_id)
            ->where('status', 1)
            ->orderBy('sort', 'asc')
            ->first();
        if (!$ways) {
            $store_id = "";
            $srore_id_pid = Store::where('id', $srore_id_pid)
                ->select('store_id')
                ->first();
            if ($srore_id_pid) {
                $store_id = $srore_id_pid->store_id;
            }

            $ways = StorePayWay::where('ways_source', $ways_source)
                ->where('store_id', $store_id)
                ->where('status', 1)
                ->orderBy('sort', 'asc')
                ->first();
        }

        return $ways;
    }


    //支付用到 分店没有直接公用总店的 通过ways_source查询 只返回银联扫码
    public function ways_source_un_qr($ways_source, $store_id, $srore_id_pid)
    {
        $ways = StorePayWay::where('ways_source', $ways_source)
            ->where('store_id', $store_id)
            ->whereIn('ways_type', [6004, 8004, 13004, 15004, 17004, 19004, 20204, 21004, 26004, 27004, 28004, 21004, 29003, 31003, 32003])
            ->where('status', 1)
            ->orderBy('sort', 'asc')
            ->first();
        if (!$ways) {
            $store_id = "";
            $srore_id_pid = Store::where('id', $srore_id_pid)
                ->select('store_id')->first();
            if ($srore_id_pid) {
                $store_id = $srore_id_pid->store_id;
            }

            $ways = StorePayWay::where('ways_source', $ways_source)
                ->where('store_id', $store_id)
                ->whereIn('ways_type', [6004, 8004, 13004, 15004, 17004, 19004, 20204, 21004, 26004, 27004, 28004, 21004, 29003, 31003, 32003])
                ->where('status', 1)
                ->orderBy('sort', 'asc')
                ->first();
        }

        return $ways;
    }


    //支付用到 分店没有直接公用总店的 通过ways_type查询
    public function ways_type($ways_type, $store_id, $srore_id_pid)
    {
        $ways = StorePayWay::where('ways_type', $ways_type)
            ->where('store_id', $store_id)
            ->where('status', 1)
            ->first();
        if (!$ways) {
            $store_id = "";
            $srore_id_pid = Store::where('id', $srore_id_pid)
                ->select('store_id')
                ->first();
            if ($srore_id_pid) {
                $store_id = $srore_id_pid->store_id;
            }

            $ways = StorePayWay::where('ways_type', $ways_type)
                ->where('store_id', $store_id)
                ->where('status', 1)
                ->first();
        }

        return $ways;
    }


    //支付用到 分店没有直接公用总店的 通过company,ways_source查询
    public function company_ways_source($company, $ways_source, $store_id, $srore_id_pid)
    {
        $ways = StorePayWay::where('ways_source', $ways_source)
            ->where('company', $company)
            ->where('status', 1)
            ->where('store_id', $store_id)
            ->first();
        if (!$ways) {
            $store_id = "";
            $srore_id_pid = Store::where('id', $srore_id_pid)
                ->select('store_id')
                ->first();
            if ($srore_id_pid) {
                $store_id = $srore_id_pid->store_id;
            }

            $ways = StorePayWay::where('ways_source', $ways_source)
                ->where('company', $company)
                ->where('status', 1)
                ->where('store_id', $store_id)
                ->first();
        }

        return $ways;
    }


    //支付用到 得到成本费率  通过ways_type查询
    public function getCostRate($ways_type, $user_id)
    {
        $user_rate_obj = UserRate::where('ways_type', $ways_type)
            ->where('user_id', $user_id)
            ->where('status', 1)
            ->orderby('created_at', 'desc')
            ->first();

        return $user_rate_obj;
    }


}
