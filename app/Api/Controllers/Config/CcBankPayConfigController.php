<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/4/20
 * Time: 10:38 AM
 */

namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\CcBankPayStore;
use App\Models\Store;

class CcBankPayConfigController extends Controller
{

    public function ccBank_pay_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $CcBankPayStore = CcBankPayStore::where('store_id', $store_id)->first();
            if (!$CcBankPayStore) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $CcBankPayStore = CcBankPayStore::where('store_id', $store_pid_id)->first();
            }
        } else {
            $CcBankPayStore = CcBankPayStore::where('store_id', $store_id)->first();
        }

        return $CcBankPayStore;
    }


}
