<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/4/20
 * Time: 10:38 AM
 */

namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\EasySkpayConfig;
use App\Models\EasySkpayStore;
use App\Models\Store;

class EasySkPayConfigController extends Controller
{

    public function easyskpay_config($config_id)
    {
        //配置取缓存
        $config = EasySkpayConfig::where('config_id', $config_id)
            ->first();
        if (!$config) {
            $config = EasySkpayConfig::where('config_id', '1001')->first();
        }

        return $config;
    }


    public function easyskpay_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $EasySkpayStore = EasySkpayStore::where('store_id', $store_id)->first();
            if (!$EasySkpayStore) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $EasySkpayStore = EasySkpayStore::where('store_id', $store_pid_id)->first();
            }
        } else {
            $EasySkpayStore = EasySkpayStore::where('store_id', $store_id)->first();
        }

        return $EasySkpayStore;
    }


}
