<?php
namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\TfConfig;
use App\Models\TfStore;

class TfConfigController extends Controller
{

    public function tf_config($config_id, $qd = 1)
    {
        //配置取缓存
        $config = TfConfig::where('config_id', $config_id)
            ->where('qd', $qd)
            ->first();
        if (!$config) {
            $config = TfConfig::where('config_id', '1234')
                ->where('qd', $qd)
                ->first();
        }

        return $config;
    }


    public function tf_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $H_merchant = TfStore::where('store_id', $store_id)->first();
            if (!$H_merchant) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $H_merchant = TfStore::where('store_id', $store_pid_id)->first();
            }
        } else {
            $H_merchant = TfStore::where('store_id', $store_id)->first();
        }

        return $H_merchant;
    }


}
