<?php
namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\SuzhouConfig;
use App\Models\SuzhouStore;

class SuzhouConfigController extends Controller
{

    public function sz_config($config_id)
    {
        //配置取缓存
        $config = SuzhouConfig::where('config_id', $config_id)
            ->first();
        if (!$config) {
            $config = SuzhouConfig::where('config_id', '1234')
                ->first();
        }

        return $config;
    }


    public function sz_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $LtfStore = SuzhouStore::where('store_id', $store_id)->first();
            if (!$LtfStore) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $LtfStore = SuzhouStore::where('store_id', $store_pid_id)->first();
            }
        } else {
            $LtfStore = SuzhouStore::where('store_id', $store_id)->first();
        }

        return $LtfStore;
    }


}
