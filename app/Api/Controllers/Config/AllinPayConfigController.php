<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/4/20
 * Time: 10:38 AM
 */

namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\AllinPayConfig;
use App\Models\AllinPayStore;
use App\Models\Store;


class AllinPayConfigController extends Controller
{

    public function allin_pay_config($config_id)
    {
        //配置取缓存
        $config = AllinPayConfig::where('config_id', $config_id)
            ->first();
        if (!$config) {
            $config = AllinPayConfig::where('config_id', '1234')->first();
        }

        return $config;
    }


    public function allin_pay_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $AllinPayStore = AllinPayStore::where('store_id', $store_id)->first();
            if (!$AllinPayStore) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $AllinPayStore = AllinPayStore::where('store_id', $store_pid_id)->first();
            }
        } else {
            $AllinPayStore = AllinPayStore::where('store_id', $store_id)->first();
        }

        return $AllinPayStore;
    }


}
