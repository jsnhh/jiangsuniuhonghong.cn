<?php
namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\HuiPayConfig;
use App\Models\HuiPayStore;

class HuiPayConfigController extends Controller
{
    //读取配置信息
    public function hui_pay_config($config_id)
    {
        //配置取缓存
        $config = HuiPayConfig::where('config_id', $config_id)
            ->first();
        if (!$config) {
            $config = HuiPayConfig::where('config_id', '1234')->first();
        }

        return $config;
    }


    //读取店铺信息
    public function hui_pay_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $hui_pay_store = HuiPayStore::where('store_id', $store_id)->first();
            if (!$hui_pay_store) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $hui_pay_store = HuiPayStore::where('store_id', $store_pid_id)->first();
            }

        } else {
            $hui_pay_store = HuiPayStore::where('store_id', $store_id)->first();
        }

        return $hui_pay_store;
    }


}
