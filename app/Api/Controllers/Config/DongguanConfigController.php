<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/11/30
 * Time: 11:14 AM
 */
namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\DongguanConfig;
use App\Models\DongguanStore;
use App\Models\Store;

class DongguanConfigController extends Controller
{
    //配置读取
    public function dongguan_config($config_id)
    {
        $config = DongguanConfig::where('config_id', $config_id)->first();
        if (!$config) {
            $config = DongguanConfig::where('config_id', '1234')->first();
        }

        return $config;
    }


    //进件门店读取
    public function dongguan_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $dongguan_Store = DongguanStore::where('store_id', $store_id)->first();
            if (!$dongguan_Store) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $dongguan_Store = DongguanStore::where('store_id', $store_pid_id)->first();
            }
        } else {
            $dongguan_Store = DongguanStore::where('store_id', $store_id)->first();
        }

        return $dongguan_Store;
    }


}
