<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/10/22
 * Time: 13:38
 */
namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\HwcpayConfig;
use App\Models\HwcpayStore;

class HwcPayConfigController extends Controller
{

    public function hwcpay_config($config_id)
    {
        //配置取缓存
        $config = HwcpayConfig::where('config_id', $config_id)
            ->first();
        if (!$config) {
            $config = HwcpayConfig::where('config_id', '1234')->first();
        }

        return $config;
    }


    public function hwcpay_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $hwcpayStore = HwcpayStore::where('store_id', $store_id)->first();
            if (!$hwcpayStore) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $hwcpayStore = HwcpayStore::where('store_id', $store_pid_id)->first();
            }
        } else {
            $hwcpayStore = HwcpayStore::where('store_id', $store_id)->first();
        }

        return $hwcpayStore;
    }


}
