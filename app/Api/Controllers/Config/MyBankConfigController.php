<?php
namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\MyBankConfig;
use App\Models\MyBankStore;
use App\Models\Store;

class MyBankConfigController extends Controller
{

    public function MyBankConfig($config_id, $wx_AppId = "")
    {
        $config = MyBankConfig::where('config_id', $config_id)->first();
        //代表有新渠道
        if (!$config) {
            $config = MyBankConfig::where('config_id', '1234')->first();
        }

        return $config;
    }


    public function mybank_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $MyBankStore = MyBankStore::where('OutMerchantId', $store_id)
                ->inRandomOrder()
                ->first();
            if (!$MyBankStore) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $MyBankStore = MyBankStore::where('OutMerchantId', $store_pid_id)
                    ->inRandomOrder()
                    ->first();
            }
        } else {
            $MyBankStore = MyBankStore::where('OutMerchantId', $store_id)
                ->inRandomOrder()
                ->first();
        }

        return $MyBankStore;
    }


}
