<?php
namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\ChangshaConfig;
use App\Models\ChangshaStore;
use App\Models\Store;

class ChangshaConfigController extends Controller
{

    public function cs_config($config_id)
    {
        //配置取缓存
        $config = ChangshaConfig::where('config_id', $config_id)
            ->first();
        if (!$config) {
            $config = ChangshaConfig::where('config_id', '1234')
                ->first();
        }

        return $config;
    }


    public function cs_merchant($store_id, $store_pid)
    {
        if ($store_pid) {
            //分店配置
            $LtfStore = ChangshaStore::where('store_id', $store_id)->first();
            if (!$LtfStore) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $LtfStore = ChangshaStore::where('store_id', $store_pid_id)->first();
            }
        } else {
            $LtfStore = ChangshaStore::where('store_id', $store_id)->first();
        }

        return $LtfStore;
    }


}
