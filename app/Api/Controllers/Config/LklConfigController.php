<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2021/01/25
 * Time: 18:04
 */
namespace App\Api\Controllers\Config;


use App\Http\Controllers\Controller;
use App\Models\LklConfig;
use App\Models\LklStore;
use App\Models\Store;
use App\Models\YinshengConfig;
use App\Models\YinshengStore;

class LklConfigController extends Controller
{

    public function lkl_config($config_id)
    {
        //配置取缓存
        $config = LklConfig::where('config_id', $config_id)
            ->first();
        if (!$config) {
            $config = LklConfig::where('config_id', '1234')->first();
        }

        return $config;
    }


    public function lkl_merchant($store_id, $store_pid)
    {
        $lklStore = LklStore::where('store_id', $store_id)->first();
        if ($store_pid) {
            //分店配置
            if (!$lklStore) {
                $store_pid_id = '';
                $store_p = Store::where('id', $store_pid)
                    ->select('store_id')
                    ->first();
                if ($store_p) {
                    $store_pid_id = $store_p->store_id;
                }

                $lklStore = LklStore::where('store_id', $store_pid_id)->first();
            }
        }

        return $lklStore;
    }


}
