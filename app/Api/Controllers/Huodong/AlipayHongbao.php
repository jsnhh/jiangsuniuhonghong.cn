<?php
namespace App\Api\Controllers\Huodong;


use App\Api\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\AlipayHongba;
use App\Models\Store;
use Illuminate\Http\Request;

class AlipayHongbao extends BaseController
{

    public function add(Request $request)
    {
        try {
            $public = $this->parseToken();
            $content = $request->get('content', ''); //是
            $store_id = $request->get('store_id', '');
            $store_name = $request->get('store_name', '');
            $remark = $request->get('remark', '');
            $id = $request->get('id', ''); //编辑标识

            if ($public->type == "merchant") {
                $user_id = $public->merchant_id;
                $user_name = $public->name;
            }

            if ($public->type == "user") {
                $user_id = $public->user_id;
                $user_name = $public->name;
            }

            $check_data = [
                'content' => '公告内容'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $data = [
                'create_user_id' => $user_id,
                'create_user_name' => $user_name,
                'content' => $content,
                'model_type' => $public->type
            ];
            if ($store_id) $data['store_id'] = $store_id;
            if ($store_name) $data['store_name'] = $store_name;
            if ($remark) $data['remark'] = $remark;

            if ($id) {
                $edit_res = AlipayHongba::find($id)->update($data);
                if ($edit_res) {
                    return $this->responseDataJson(160003, $data);
                } else {
                    return $this->responseDataJson(160004, $data);
                }
            } else {
                $obj = AlipayHongba::create($data);
                if ($obj) {
                    return $this->responseDataJson(160001, $data);
//                $this->status = 1;
//                $this->message = '添加成功';
//                return $this->format($data);
                } else {
                    return $this->responseDataJson(160002, $data);
//                $this->status = 2;
//                $this->message = '添加失败';
//                return $this->format($data);
                }
            }
        } catch (\Exception $ex) {
            return $this->responseDataJson(-1, $ex->getMessage().' | '.$ex->getLine());
//            return json_encode([
//                'status' => -1,
//                'message' => $exception->getMessage()
//            ]);
        }
    }


    public function del(Request $request)
    {
        try {
            $public = $this->parseToken();
            $id = $request->get('id', '');

            $check_data = [
                'id' => '红包id'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

//            $where[] = ['model_type', '=', $public->type];
//            if ($id) {
//                $where[] = ['id', '=', $id];
//            }

//            $obj = AlipayHongba::where($where)->delete();
            $obj = AlipayHongba::find($id)->delete();
            if ($obj) {
                return $this->responseDataJson(160005);
            } else {
                return $this->responseDataJson(160006);
            }

//            $this->status = 1;
//            $this->message = '删除成功';
//            return $this->format($data);
        } catch (\Exception $ex) {
//            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
            return $this->responseDataJson(-1, $ex->getMessage().' | '.$ex->getLine());
        }
    }


    public function get_list(Request $request)
    {
        try {
            $public = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $content = $request->get('content', '');
            $where = [];

            if ($public->type == "merchant") {
                $user_id = $public->merchant_id;
            }

            if ($public->type == "user") {
                $user_id = $public->user_id;
            }

            $create_user_id = $request->get('create_user_id', $user_id);

            $where[] = ['model_type', '=', $public->type];
            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
            }
            if ($create_user_id) {
                $where[] = ['create_user_id', '=', $create_user_id];
            }
            if ($content) {
                $where[] = ['content', 'like', $content.'%'];
            }

            $obj = AlipayHongba::where($where);
            $this->t = $obj->count();
            $data = $this->page($obj)->get();

            return $this->responseDataJson(1, $data);
//            $this->status = 1;
//            $this->message = '数据返回成功';
//            return $this->format($data);
        } catch (\Exception $ex) {
//            return json_encode(['status' => -1, 'message' => $ex->getMessage()]);
            return $this->responseDataJson(-1, $ex->getMessage().' | '.$ex->getLine());
        }
    }


}
