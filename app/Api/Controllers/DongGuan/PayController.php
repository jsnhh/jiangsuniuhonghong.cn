<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/11/30
 * Time: 11:46 AM
 */
namespace App\Api\Controllers\DongGuan;


use Illuminate\Support\Facades\Log;

class PayController extends BaseController
{
    //被扫 0-系统错误 1-成功 2-失败 3-交易进行中
    public function scan_pay($data)
    {
        try {
            $access_id = $data['access_id'] ?? '';
            $merch_no = $data['merch_no'] ?? '';
            $out_trade_no = $data['out_trade_no'] ?? '';
            $total_amount = $data['total_amount'];
            $total_amount = number_format($total_amount, 2, '.', '');
            $auth_code = $data['code'] ?? '';
            $notify_url = $data['notify_url'] ?? '';
            $sn = $data['sn'] ?? '';
            $pn = $data['pn'] ?? '';
            $remark = $data['remark'] ?? '';
            $limit_pay = $data['limit_pay'] ?? '0';
            $receive_no = $data['receive_no'] ?? '';
            $amt = $data['amt'] ?? '';
            $goods_name = $data['goods_name'] ?? '';
            $access_key = $data['access_key'] ?? '';

            $obj = new BaseController();
            $hkrt_data = [
                'accessid' => $access_id
//                'merch_no' => $merch_no, //商户编号
//                'out_trade_no' => $out_trade_no, //服务商交易订单号
//                'total_amount' => $total_amount, //订单总金额，以元为单位
//                'auth_code' => $auth_code, //支付授权码
//                'notify_url' => $notify_url, //支付成功后的通知地址
//                'sn' => $sn, //厂商终端号
//                'pn' => $pn, //SAAS终端号,标准服务商必填
//                'remark' => $remark, //交易备注
//                'limit_pay' => $limit_pay, //限制贷记卡支付,0-不限制贷记卡支付;1-禁止使用贷记卡支付;默认为0
//                'ledger_relation' => json_encode([ //分账交易关系组
//                    'receive_no' => $receive_no, //收账方(海科商户号)ledger_relation组内参数
//                    'amt' => $amt //分账金额（单位：元）
//                ]),
//                'goods_name' => $goods_name, //商品名称(仅限支付宝微信被扫，银联二维码上送字段也不会被使用)
            ];
            if(isset($merch_no) && !empty($merch_no)) $hkrt_data['merch_no'] = $merch_no; //商户编号
            if(isset($out_trade_no) && !empty($out_trade_no)) $hkrt_data['out_trade_no'] = $out_trade_no; //服务商交易订单号
            if(isset($total_amount) && !empty($total_amount)) $hkrt_data['total_amount'] = $total_amount; //
            if(isset($auth_code) && !empty($auth_code)) $hkrt_data['auth_code'] = $auth_code; //
            if(isset($notify_url) && !empty($notify_url)) $hkrt_data['notify_url'] = $notify_url; //
            if(isset($sn) && !empty($sn)) $hkrt_data['sn'] = $sn; //
            if(isset($pn) && !empty($pn)) $hkrt_data['pn'] = $pn; //
            if(isset($remark) && !empty($remark)) $hkrt_data['remark'] = $remark; //
            if(isset($limit_pay) && !empty($limit_pay)) $hkrt_data['limit_pay'] = $limit_pay; //
            if(isset($goods_name) && !empty($goods_name)) $hkrt_data['goods_name'] = $goods_name; //
            $hkrt_data['sign'] = $this->getSignContent($hkrt_data, $access_key);
            Log::info('海科融通-被扫入参');
            Log::info($hkrt_data);
            $re = $obj->execute($hkrt_data, $this->url.$this->polymeric_passivepay);
            Log::info($re);
//            $re = '{
//                "out_trade_no":"aliscan20200528103800950648811",
//                "channel_trade_no":"011220052810381308990MC",
//                "trade_no":"AL200528103801605753925627",
//                "trade_status":1,
//                "trade_type":"ALI",
//                "trade_end_time":"2020-05-28 10:38:03",
//                "userid":"2088012904744331",
//                "trade_channel_end_time":"2020-05-28 10:38:02",
//                "return_code":10000,
//                "sign":"31E7A6AD0908910C0067F5402A4BFBE1"
//            }';
            if (isset($re) && !empty($re)) {
                $result = json_decode($re, true);

                //成功
                if ($result['return_code'] == '10000') {
                    //微信支付宝支付
                    if (($result['trade_type'] == 'WX') || ($result['trade_type'] == 'ALI')) {
                        //交易成功
                        if ($result['trade_status'] == '1') {
                            return [
                                'status' => 1,
                                'message' => '交易成功',
                                'data' => $result
                            ];
                        } //交易进行中
                        elseif ($result['trade_status'] == '3') {
                            return [
                                'status' => 3,
                                'message' => '交易进行中',
                                'data' => $result,
                            ];
                        } else { //交易失败
                            return [
                                'status' => 2,
                                'message' => $result['return_msg'],
                                'data' => $result,
                            ];
                        }
                    } else { //银联二维码支付
                        if ($result['accept_status'] == '1') {
                            //受理成功
                            return [
                                'status' => 1,
                                'message' => '受理成功',
                                'data' => $result
                            ];
                        } else { //受理拒绝
                            return [
                                'status' => 0,
                                'message' => '受理拒绝'
                            ];
                        }
                    }
                } else {
                    return [
                        'status' => 0,
                        'message' => $result['return_msg']
                    ];
                }
            } else {
                return [
                    'status' => '0',
                    'message' => '系统错误',
                ];
            }
        } catch (\Exception $ex) {
            Log::info('海科融通-被扫支付-错误');
            Log::info($ex->getMessage());
            return [
                'status' => '0',
                'message' => $ex->getMessage(),
            ];
        }
    }


    //聚合公众号支付API 静态二维码 0-失败 1-成功
    public function qr_submit($data)
    {
        try {
            $access_id = $data['access_id'] ?? '';
            $trade_type = $data['trade_type'] ?? '';
            $merch_no = $data['merch_no'] ?? '';
            $channel_no = $data['channel_no'] ?? '';
            $channel_merch_no = $data['channel_merch_no'] ?? '';
            $out_trade_no = $data['out_trade_no'] ?? '';
            $total_amount = $data['total_amount'] ?? '';
            $userid = $data['userid'] ?? '';
            $appid = $data['appid'] ?? '';
            $openid = $data['open_id'] ?? '';
            $sn = $data['sn'] ?? '';
            $pn = $data['pn'] ?? '';
            $remark = $data['remark'] ?? '';
            $limit_pay = $data['limit_pay'] ?? '';
            $subsidy_amount = $data['subsidy_amount'] ?? '';
            $receive_no = $data['receive_no'] ?? '';
            $amt = $data['amt'] ?? '';
            $notify_url = $data['notify_url'] ?? '';
            $goods_name = $data['goods_name'] ?? '';
            $access_key = $data['access_key'] ?? '';

            $hkrt_data = [
                'accessid' => $access_id, //
                'trade_type' => $trade_type, //M,支付方式,微信支付-WX;支付宝支付-ALI;银联二维码支付-UNIONQR
                'merch_no' => $merch_no, //M,商户在SaaS平台的编号
                'channel_no' => $channel_no, //服务商通过海科在(支付宝/微信)申请的渠道编号
                'channel_merch_no' => $channel_merch_no, //商户在(支付宝/微信)申请的编号
                'out_trade_no' => $out_trade_no, //M,服务商的交易订单编号
                'total_amount' => $total_amount, //M,订单总金额，以元为单位
                'userid' => $userid, //M,(支付宝/微信/银联二维码)的用户唯一标识
                'appid' => $appid, //微信公众号appid,微信交易必须上送
                'openid' => $openid, //???
//                'ledger_relation' => json_encode([ //分账交易关系组,如果上送此字段，需要保证服务商分账业务已开通,否则不允许交易
//                    'receive_no' => $receive_no, //收账方(海科商户号)
//                    'amt' => $amt //分账金额
//                ]),
                'notify_url' => $notify_url,
                'sn' => $sn, //厂商终端号
                'pn' => $pn, //SAAS终端号
                'remark' => $remark, //交易备注
                'limit_pay' => $limit_pay, //限制贷记卡支付,0-不限制贷记卡支付;1-禁止使用贷记卡支付.默认为0
//                'subsidy_amount' => $subsidy_amount, //补贴金额.订单总金额，以元为单位，如果上送此字段，需要保证服务商补贴业务已开通,否则不允许交易
                'goods_name' => $goods_name //商品名称(支付宝和微信支持)
            ];
            $hkrt_data['sign'] = $this->getSignContent($hkrt_data, $access_key);
            Log::info('聚合公众号支付API-静态二维码');
            Log::info($hkrt_data);
            $polymeric_jsapipay_res = $this->execute($hkrt_data, $this->url.$this->polymeric_jsapipay);
            Log::info($polymeric_jsapipay_res);

            if (isset($polymeric_jsapipay_res) && !empty($polymeric_jsapipay_res)) {
                $return_data = json_decode($polymeric_jsapipay_res, true);
                if ($return_data['return_code'] == '10000') {
                    return [
                        'status' => 1,
                        'message' => '交易成功',
                        'data' => $return_data,
                    ];
                } else {
                    return [
                        'status' => 0,
                        'message' => $return_data['return_msg']
                    ];
                }
            } else {
                return [
                    'status' => 0,
                    'message' => '系统错误',
                ];
            }
        } catch (\Exception $ex) {
            Log::info('聚合公众号支付API-静态二维码-错误');
            Log::info($ex->getMessage());
            return [
                'status' => 0,
                'message' => $ex->getMessage(),
            ];
        }
    }


    //查询订单 0-系统错误 1-交易成功；2-交易失败；3-交易进行中；4-交易超时
    public function order_query($data)
    {
        try {
            $trade_no = $data['trade_no'] ?? '';
            $out_trade_no = $data['out_trade_no'] ?? '';
            $channel_trade_no = $data['channel_trade_no'] ?? '';
            $access_id = $data['access_id'] ?? '';
            $access_key = $data['access_key'] ?? '';

            $obj = new BaseController();
            $hkrt_data = [
                'accessid' => $access_id,
                'trade_no' => $trade_no, //交易订单号,trade_no和out_trade_no和channel_trade_no必传其中一个，三个都传则以trade_no为准，推荐使用trade_no
                'out_trade_no' => $out_trade_no, //服务商交易订单号
                'channel_trade_no' => $channel_trade_no //凭证条码订单号
            ];
            $hkrt_data['sign'] = $this->getSignContent($hkrt_data, $access_key);
            Log::info('海科融通-交易查询');
            Log::info($hkrt_data);
            $re = $obj->execute($hkrt_data, $this->url.$this->polymeric_query);
            Log::info($re);

            if (isset($re) && !empty($re)) {
                $result = json_decode($re, true);
                //业务成功
                if ($result['return_code'] == '10000') {
                    //交易成功
                    if ($result['trade_status'] == '1') {
                        return [
                            'status' => 1,
                            'message' => '交易成功',
                            'data' => $result,
                        ];
                    } //交易失败
                    elseif ($result['trade_status'] == '2') {
                        return [
                            'status' => 2,
                            'message' => '交易失败',
                            'data' => $result,
                        ];
                    } //交易进行中
                    elseif ($result['trade_status'] == '3') {
                        return [
                            'status' => 3,
                            'message' => '交易进行中',
                            'data' => $result,
                        ];
                    } else { //交易超时
                        return [
                            'status' => 4,
                            'message' => $result['return_msg'],
                            'data' => $result,
                        ];
                    }
                } else {
                    return [
                        'status' => 0,
                        'message' => $result['return_msg']
                    ];
                }
            } else {
                return [
                    'status' => 0,
                    'message' => '系统错误'
                ];
            }
        } catch (\Exception $ex) {
            Log::info('海科融通-交易查询错误');
            Log::info($ex->getMessage());
            return [
                'status' => 0,
                'message' => $ex->getMessage(),
            ];
        }
    }


    //关闭订单 0-失败 1-成功
    public function order_close($data)
    {
        try {
            $access_id = $data['access_id'] ?? '';
            $trade_no = $data['trade_no'] ?? '';
            $out_trade_no = $data['out_trade_no'] ?? '';
            $access_key = $data['access_key'] ?? '';

            $obj = new BaseController();
            $hkrt_data = [
                'accessid' => $access_id,
                'trade_no' => $trade_no, //交易订单号,trade_no和out_trade_no和channel_trade_no必传其中一个，三个都传则以trade_no为准，推荐使用trade_no
                'out_trade_no' => $out_trade_no //服务商交易订单号
            ];
            $hkrt_data['sign'] = $this->getSignContent($hkrt_data, $access_key);
            Log::info('海科融通-关闭订单');
            Log::info($hkrt_data);
            $re = $obj->execute($hkrt_data, $this->url.$this->polymeric_closeorder);
            Log::info($re);

            if (isset($re) && !empty($re)) {
                $result = json_decode($re, true);
                //交易成功
                if ($result['return_code'] == '10000') {
                    return [
                        'status' => 1,
                        'message' => '交易成功',
                        'data' => $result,
                    ];
                } else {
                    return [
                        'status' => 0,
                        'message' => $result['return_msg']
                    ];
                }
            } else {
                return [
                    'status' => 0,
                    'message' => '系统错误'
                ];
            }

        } catch (\Exception $ex) {
            Log::info('海科融通-关闭订单-错误');
            Log::info($ex->getMessage());
            return [
                'status' => 0,
                'message' => $ex->getMessage(),
            ];
        }
    }


    //退款 0-系统错误 1-成功 2-失败 3-结果未知
    public function refund($data)
    {
        try {
            $access_id = $data['access_id'] ?? '';
            $refund_amount = $data['refund_amount'] ?? '';
            $trade_no = $data['trade_no'] ?? '';
            $out_trade_no = $data['out_trade_no'] ?? '';
            $refund_notify_url = $data['notify_url'] ?? '';
            $access_key = $data['access_key'] ?? '';
            $access_key = $data['access_key'] ?? '';
            $out_refund_no = $out_trade_no . rand(1000, 9999);

            $obj = new BaseController();
            $hkrt_data = [
                'accessid' => $access_id,
                'out_refund_no' => $out_refund_no, //服务商退款订单号
                'refund_amount' => $refund_amount, //（银联二维码只能全额退款）退款金额，以元为单位
                'trade_no' => $trade_no, //SaaS平台的交易订单编号,trade_no、out_trade_no、channel_trade_no必传其中一个，都传则以trade_no为准，推荐使用trade_no
                'out_trade_no' => $out_trade_no, //原服务商交易订单号
                'notify_url' => $refund_notify_url //退款成功后的通知地址
            ];
            $hkrt_data['sign'] = $this->getSignContent($hkrt_data, $access_key);
            Log::info('海科融通-退款');
            Log::info($hkrt_data);
            $re = $obj->execute($hkrt_data, $this->url.$this->polymeric_refund);
            Log::info($re);

            if (isset($re) && !empty($re)) {
                $result = json_decode($re, true);
                //业务成功
                if ($result['return_code'] == '10000') {
                    //成功
                    if ($result['refund_status'] == '1') {
                        return [
                            'status' => 1,
                            'message' => '退款成功',
                            'data' => $result,
                        ];
                    } //失败
                    elseif ($result['refund_status'] == '2') {
                        return [
                            'status' => 2,
                            'message' => '退款失败',
                            'data' => $result,
                        ];
                    } //结果未知
                    else {
                        //TODO:调用退款查询接口获取退款状态
                        $hkrt_data = [
                            'trade_no' => $trade_no,
                            'out_trade_no' => $out_trade_no,
                            'access_id' => $access_id,
                            'access_key' => $access_key
                        ];
                        $hkrt_res = $this->refund_query($hkrt_data); //0-系统错误 1-成功 2-失败 3-未知
                        if ($hkrt_res['status'] == '1') {
                            return [
                                'status' => 1,
                                'message' => '退款成功',
                                'data' => $result,
                            ];
                        } else {
                            return [
                                'status' => '2',
                                'message' => '退款失败',
                                'data' => $result,
                            ];
                        }
                    }
                } else {
                    return [
                        'status' => 0,
                        'message' => $result['return_msg']
                    ];
                }
            } else {
                return [
                    'status' => 0,
                    'message' => '系统错误'
                ];
            }

        } catch (\Exception $ex) {
            Log::info('海科融通-退款-错误');
            Log::info($ex->getMessage());
            return [
                'status' => 0,
                'message' => $ex->getMessage()
            ];
        }
    }


    //退款查询 0-系统错误 1-成功 2-失败 3-未知
    public function refund_query($data)
    {
        try {
            $access_id = $data['access_id'];
            $refund_no = $data['refund_no'] ?? '';
            $out_refund_no = $data['out_refund_no'] ?? '';
            $access_key = $data['access_key'] ?? '';

            $obj = new BaseController();
            $hkrt_data = [
                'accessid' => $access_id,
                'refund_no' => $refund_no, //退款订单号,refund_no和 out_refund_no和channel_trade_no必传其中一个，三个都传则以refund_no为准，推荐使用refund_no
                'out_refund_no' => $out_refund_no //服务商退款订单号
            ];
            $hkrt_data['sign'] = $this->getSignContent($hkrt_data, $access_key);
            Log::info('海科融通-退款查询');
            Log::info($hkrt_data);
            $re = $obj->execute($hkrt_data, $this->url.$this->polymeric_refundquery);
            Log::info($re);

            if (isset($re) && !empty($re)) {
                $result = json_decode($re, true);
                //业务成功
                if ($result['return_code'] == '10000') {
                    //成功
                    if ($result['refund_status'] == '1') {
                        return [
                            'status' => '1',
                            'message' => '退款成功',
                            'data' => $result,
                        ];
                    } //失败
                    elseif ($result['refund_status'] == '2') {
                        return [
                            'status' => '2',
                            'message' => '交易失败',
                            'data' => $result,
                        ];
                    }
                    else { //未知
                        return [
                            'status' => '3',
                            'message' => $result['return_msg'],
                            'data' => $result,
                        ];
                    }
                } else {
                    return [
                        'status' => 0,
                        'message' => $result['return_msg']
                    ];
                }
            } else {
                return [
                    'status' => 0,
                    'message' => '系统错误'
                ];
            }
        } catch (\Exception $ex) {
            Log::info('海科融通-退款查询-错误');
            Log::info($ex->getMessage());
            return [
                'status' => 0,
                'message' => $ex->getMessage()
            ];
        }
    }


    //商户业务查询 0-系统错误 1-提交失败 2-已受理 3-自动审核 4-待审核 5-审核失败 6-审核拒绝 7-审核成功 8-审核中 9-待报备 10-报备中 11-报备失败 12-报备成功
    public function merchant_query($data)
    {
        try {
            $access_id = $data['access_id'];
            $agent_no = $data['agent_no'] ?? '';
            $apply_type = $data['apply_type'] ?? '';
            $access_key = $data['access_key'] ?? '';
            $agent_apply_no = $data['agent_apply_no'] ?? '';

            $obj = new BaseController();
            $hkrt_data = [
                'accessid' => $access_id,
                'agent_no' => $agent_no,
                'agent_apply_no' => $agent_apply_no,
                'apply_type' => $apply_type //1-商户入网申请;2-微信业务申请;3-支付宝业务申请;4-银行卡业务申请;10-商户分账业务申请;11-商户分账关系申请;12-商户结算周期变更申请;13-商户分账业务修改
            ];
            $hkrt_data['sign'] = $this->getSignContent($hkrt_data, $access_key);
            Log::info('海科融通-商户业务查询');
            Log::info($hkrt_data);
            $re = $obj->execute($hkrt_data, $this->url.$this->merchant_biz_query);
            Log::info($re);

            if (isset($re) && !empty($re)) {
                $result = json_decode($re, true);
                if ($result['return_code'] == '10000') {
                    if ($apply_type == '1') {
                        //apply_type=1时:1-提交失败 2-已受理 3-自动审核 4-待审核 5-审核失败 6-审核拒绝 7-审核成功
                        switch ($result['status']) {
                            case '1': //提交失败
                                return [
                                    'status' => '1',
                                    'message' => '商户入网申请-提交失败',
                                    'data' => $result['msg'],
                                ];
                                break;
                            case '2': //已受理
                                return [
                                    'status' => '2',
                                    'message' => '商户入网申请-已受理',
                                    'data' => $result['msg'],
                                ];
                                break;
                            case '3': //自动审核
                                return [
                                    'status' => '3',
                                    'message' => '商户入网申请-自动审核',
                                    'data' => $result['msg'],
                                ];
                                break;
                            case '4': //待审核
                                return [
                                    'status' => '4',
                                    'message' => '商户入网申请-待审核',
                                    'data' => $result['msg'],
                                ];
                                break;
                            case '5': //审核失败
                                return [
                                    'status' => '5',
                                    'message' => '商户入网申请-审核失败',
                                    'data' => $result['msg'],
                                ];
                                break;
                            case '6': //审核拒绝
                                return [
                                    'status' => '6',
                                    'message' => '商户入网申请-审核拒绝',
                                    'data' => $result['msg'],
                                ];
                                break;
                            case '7': //审核成功
                                return [
                                    'status' => '7',
                                    'message' => '商户入网申请-审核成功',
                                    'data' => $result['msg'],
                                ];
                                break;
                            default:
                                return [
                                    'status' => '-1',
                                    'message' => '商户入网申请-状态未知',
                                    'data' => $result['msg'],
                                ];
                        }
                    } elseif($apply_type == '10' || $apply_type == '11' || $apply_type == '12' || $apply_type == '13') {
                        //apply_type=10,11,12,13时:0-审核中,1-审核成功,2-审核失败
                        switch ($result['status']) {
                            case '0': //审核中
                                return [
                                    'status' => '8',
                                    'message' => '审核中',
                                    'data' => $result['msg'],
                                ];
                                break;
                            case '1': //审核成功
                                return [
                                    'status' => '7',
                                    'message' => '审核成功',
                                    'data' => $result['msg'],
                                ];
                                break;
                            case '2': //审核失败
                                return [
                                    'status' => '5',
                                    'message' => '审核失败',
                                    'data' => $result['msg'],
                                ];
                                break;
                            default:
                                return [
                                    'status' => '-1',
                                    'message' => '状态未知',
                                    'data' => $result['msg'],
                                ];
                        }
                    } else {
                        //其他:0-待报备 1-报备中 2-报备失败 3-报备成功
                        switch ($result['status']) {
                            case '0': //待报备
                                return [
                                    'status' => '9',
                                    'message' => '待报备',
                                    'data' => $result['msg'],
                                ];
                                break;
                            case '1': //报备中
                                return [
                                    'status' => '10',
                                    'message' => '报备中',
                                    'data' => $result['msg'],
                                ];
                                break;
                            case '2': //报备失败
                                return [
                                    'status' => '11',
                                    'message' => '报备失败',
                                    'data' => $result['msg'],
                                ];
                                break;
                            case '3': //报备成功
                                return [
                                    'status' => '12',
                                    'message' => '报备成功',
                                    'data' => $result['msg'],
                                ];
                                break;
                            default:
                                return [
                                    'status' => '-1',
                                    'message' => '状态未知',
                                    'data' => $result['msg'],
                                ];
                        }
                    }
                } else {
                    return [
                        'status' => 0,
                        'message' => $result['return_msg']
                    ];
                }
            } else {
                return [
                    'status' => 0,
                    'message' => '系统错误'
                ];
            }
        } catch (\Exception $ex) {
            Log::info('海科融通-商户业务查询-错误');
            Log::info($ex->getMessage());
            return [
                'status' => 0,
                'message' => $ex->getMessage()
            ];
        }
    }


    //配置微信公众号(可报备微信授权目录) 0-失败 1-成功
    public function wx_appid_conf_add($data)
    {
        try {
            $access_id = $data['access_id'];
            $agent_no = $data['agent_no'] ?? '';
            $merch_no = $data['merch_no'] ?? '';
            $conf_key = $data['conf_key'] ?? '';
            $conf_value = env('APP_URL') . '/api/hkrt/weixin/';
            $access_key = $data['access_key'] ?? '';

            $obj = new BaseController();
            $hkrt_data = [
                'accessid' => $access_id,
                'agent_no' => $agent_no,
                'merch_no' => $merch_no,
                'conf_key' => $conf_key,
                'conf_value' => $conf_value
            ];
            $hkrt_data['sign'] = $this->getSignContent($hkrt_data, $access_key);
            Log::info('海科融通-配置微信公众号');
            Log::info($hkrt_data);
            $re = $obj->execute($hkrt_data, $this->url.$this->wx_appid_conf_add);
            Log::info($re);

            if (isset($re) && !empty($re)) {
                $result = json_decode($re, true);
                if ($result['return_code'] == '10000') {
                    return [
                        'status' => '1',
                        'message' => $result['msg']
                    ];
                } else {
                    return [
                        'status' => '0',
                        'message' => $result['return_msg']
                    ];
                }
            } else {
                return [
                    'status' => '0',
                    'message' => '系统错误'
                ];
            }
        } catch (\Exception $ex) {
            Log::info('海科融通-配置微信公众号-错误');
            Log::info($ex->getMessage());
            return [
                'status' => 0,
                'message' => $ex->getMessage()
            ];
        }
    }


    //图片上传 0-失败 1-成功
    public function merchant_image_upload($data)
    {
        try {
            $access_id = $data['access_id'] ?? '';
            $imageBase64 = $data['imageBase64'] ?? '';
            $access_key = $data['access_key'] ?? '';

            $obj = new BaseController();
            $hkrt_data = [
                'accessid' => $access_id,
                'image' => $imageBase64
            ];
            $hkrt_data['sign'] = $this->getSignContent($hkrt_data, $access_key);
            Log::info('海科融通-图片上传');
//            Log::info($hkrt_data);
            $re = $obj->execute($hkrt_data, $this->url.$this->merchant_image_upload);
            Log::info($re);

            if (isset($re) && !empty($re)) {
                $return_data = json_decode($re, true);
                if ($return_data['return_code'] == '10000') {
                    return [
                        'status' => '1',
                        'data' => $return_data
                    ];
                } else {
                    return [
                        'status' => '0',
                        'message' => $return_data['return_msg']
                    ];
                }
            } else {
                return [
                    'status' => '0',
                    'message' => '系统错误'
                ];
            }
        } catch (\Exception $ex) {
            Log::info('海科融通-图片上传-错误');
            Log::info($ex->getMessage());
            return [
                'status' => 0,
                'message' => $ex->getMessage()
            ];
        }
    }
    

    //获取微信刷脸调用凭证(***)
    public function lose_face($data)
    {
        try {
            $out_trade_no = $data['out_trade_no'];
            $url = $data['url'];
            $mno = $data['mno'];
            $orgId = $data['orgId'];
            $privateKey = $data['privateKey'];
            $sxfpublic = $data['sxfpublic'];
            $raw_data = $data['raw_data'];
            $device_id = $data['device_id'];
            $store_no = $data['store_no'];

            $obj = new BaseController();
            $data = [
                'orgId' => $orgId,
                'reqId' => time(),
                'version' => '1.0',
                'timestamp' => date('Ymdhis', time()),
                'signType' => 'RSA',
                'reqData' => [
                    'mno' => $mno, //商户编号
                    'storeNo' => $store_no, //门店编号
                    'orderNo' => $out_trade_no, //商户订单号
                    'rawdata' => $raw_data, //初始数据
                    'deviceId' => $device_id, //微信终端编号（WXtrmno）
                ],
            ];

            Log::info('随行付-获取微信刷脸调用凭证：');
            Log::info($data);
            $re = $obj->execute($data, $url, $privateKey, $sxfpublic);
            Log::info($re);
            //系统错误
            if ($re['status'] == 0) {
                return $re;
            }

            //业务成功
            if ($re['data']['code'] == '0000') {
                //成功
                if ($re['data']['respData']['bizCode'] == '0000') {
                    return [
                        'status' => 1,
                        'message' => '交易成功',
                        'data' => $re['data']
                    ];
                } else {
                    return [
                        'status' => 0,
                        'message' => $re['data']['respData']['bizMsg']
                    ];
                }
            } else {
                return [
                    'status' => 0,
                    'message' => $re['data']['msg']
                ];
            }
        } catch (\Exception $exception) {
            return [
                'status' => 0,
                'message' => $exception->getMessage(),
            ];
        }
    }


    //生成动态二维码-公共(***)
    public function send_qr($data)
    {
        try {
            $out_trade_no = $data['out_trade_no'];
            $total_amount = $data['total_amount'];
            $remark = $data['remark'];
            $notify_url = $data['notify_url'];
            $url = $data['request_url'];
            $merchant_no = $data['merchant_no'];
            $returnParams = $data['return_params'];//原样返回
            $des_key = $data['des_key'];
            $md_key = $data['md_key'];
            $systemId = $data['systemId'];
            //请求数据
            $data = [
                'merchantNo' => $merchant_no,
                'businessCode' => 'AGGRE',
                'version' => '3.0',
                'outTradeNo' => $out_trade_no,
                'amount' => $total_amount * 100,
                'remark' => $remark, //
                'returnParams' => $returnParams,
                'businessType' => '00', //返回二维码
                'successNotifyUrl' => $notify_url,
            ];

            $obj = new BaseController();
            $obj->des_key = $des_key;
            $obj->md_key = $md_key;
            $obj->systemId = $systemId;
            $re = $obj->execute($data, $url);
            //系统错误
            if ($re['status'] == 0) {
                return $re;
            }

            //业务成功
            if ($re['data']['resultCode'] == 'SUCCESS') {
                return [
                    'status' => 1,
                    'message' => '返回成功',
                    'code_url' => $re['data']['scanUrl'],
                    'data' => $re['data']
                ];
            } else {
                return [
                    'status' => 0,
                    'message' => $re['data']['errCodeDes']
                ];
            }
        } catch (\Exception $exception) {
            return [
                'status' => 0,
                'message' => $exception->getMessage(),
            ];
        }
    }


}
