<?php
namespace App\Api\Controllers\DuoLaBao;


use App\Models\MemberList;
use App\Models\MemberSetJf;
use App\Models\MemberTpl;
use App\Models\Store;
use EasyWeChat\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WxOauthController extends WxBaseController
{

    //拼接链接获取
    public function oauth(Request $request)
    {
        $state = $this->decode($request->get('state'));
        $options = $this->Options($state['config_id']);
        $scope_type = $state['scope_type'];//snsapi_base snsapi_userinfo获取的信息不一样
        $sub_info = $request->get('state');
        $config = [
            'app_id' => $options['app_id'],
            'scope' => $scope_type,
            'oauth' => [
                'scopes' => [$scope_type],
                'response_type' => 'code',
                'callback' => url('/api/dlb/weixin/callback?sub_info=' . $sub_info),
            ],

        ];

        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;

        return $oauth->redirect();
    }


    public function callback(Request $request)
    {
        $sub_info = $request->get('sub_info');
        $sub_info = $this->decode($sub_info);
        $options = $this->Options($sub_info['config_id']);
        $sub_info['code'] = $request->get('code');
        $users = $this->users($sub_info, $options);

        //微信支付
        if ($sub_info['bank_type'] == "dlb_weixin") {
            $sub_info['open_id'] = $users->getId();
            $sub_info = $this->encode($sub_info);

            return redirect('/api/dlb/weixin/qr_pay_view?sub_info=' . $sub_info);
        }
    }


    //微信支付视图页面
    public function qr_pay_view(Request $request)
    {
        $sub_info = $request->get('sub_info');
        $sub_info = $this->decode($sub_info);
        $store_id = $sub_info['store_id'];//门店id
        $merchant_id = $sub_info['merchant_id'];//收银员id
        $store_name = $sub_info['store_name'];
        $store_address = $sub_info['store_address'];
        $open_id = $sub_info['open_id'];
        $type = $sub_info['bank_type'];
        $data = [
            //'oem_name' => $AppOem->name,
            'store_id' => $store_id,
            'store_name' => $store_name,
            'store_address' => $store_address,
            'open_id' => $open_id,
            'merchant_id' => $merchant_id,
        ];

        //查询是否有开启会员
        $is_member = 0;
        $MemberTpl = MemberTpl::where('store_id', $store_id)
            ->select('tpl_status')
            ->first();
        if ($MemberTpl && $MemberTpl->tpl_status == 1) {
            $is_member = 1;
        }

        //如果是会员
        if ($is_member) {
            //判断是否是会员
            $MemberList = MemberList::where('store_id', $store_id)
                ->where('wx_openid', $open_id)
                ->select('mb_jf', 'mb_money', 'mb_id')
                ->first();
            $data['mb_jf'] = "";
            $data['mb_id'] = "";
            $data['mb_money'] = "";
            $data['dk_money'] = "";
            $data['dk_jf'] = "0";
            $data['ways_source'] = "weixin";
            if ($MemberList) {
                $data['mb_jf'] = $MemberList->mb_jf;
                $data['mb_id'] = $MemberList->mb_id;
                $data['mb_money'] = $MemberList->mb_money;
                //判断是否有积分抵扣
                $MemberSetJf = MemberSetJf::where('store_id', $store_id)
                    ->select('dk_jf_m', 'dk_rmb')
                    ->first();

                if ($MemberSetJf) {
                    //3.用户的积分一共可以抵扣多少钱
                    $data['dk_money'] = ($MemberList->mb_jf / $MemberSetJf->dk_jf_m) * $MemberSetJf->dk_rmb;
                    $data['dk_jf'] = $MemberList->mb_jf;
                }

            }
        }

        if ($type == 'dlb_weixin') {
            if ($is_member) {
                $data['ways_type'] = "15002";
                $data['company'] = "weixin";

                return view('dlb.memberweixin', compact('data'));
            } else {
                return view('dlb.weixin', compact('data'));
            }
        }
    }


    //微信充值
    public function member_cz_pay_view(Request $request)
    {
        return view('member.cz');
    }


    //学校发起支付页面
    public function paydetails()
    {
        return view('school.paydetails');
    }


    //获取users
    public function users($sub_info, $options)
    {
        $code = $sub_info['code'];
        $config = [
            'app_id' => $options['app_id'],
            "secret" => $options['app_secret'],
            "code" => $code,
            "grant_type" => "authorization_code",
        ];
        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;

        // 获取 OAuth 授权结果用户信息
        return $user = $oauth->user();
    }


}
