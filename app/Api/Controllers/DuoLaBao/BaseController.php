<?php
namespace App\Api\Controllers\DuoLaBao;


class BaseController extends \App\Api\Controllers\BaseController
{
    public $accessKey="10a11a456ad44502ab23c3daff256c07b19d0674";
    public $secretKey="4c76428d4f2c4e379a18ba8e29f13aafaa2fcd05";
    public $agentNum="10001015595555597122969";//代理商
    public $mchNum="10001115612747679703810";//交易商户号
    public $shopNum="10001215612749719175207";//店铺编号
    public $callbackUrl;
    public $dlb_createorder_path="/v1/agent/order/pay/create";
    public $dlb_queryorder_path="/v1/agent/order/payresult";
    public $dlb_scanorder_path="/v1/agent/passive/create";
    public $dlb_payurlorder_path="/v1/agent/order/payurl/create";
    public $dlb_refund_path="/v1/agent/order/refund";//全退
    public $dlb_refund_part_path="/v1/agent/order/refund/part";//部分退款
    public $dlb_open_store_path="/v2/agent/declare/customerinfo/create";//部分退款
    public $dlb_city="/v1/agent/province/list";
    public $dlb_category="/v1/agent/industry/list";
    public $dlb_host_url="https://openapi.duolabao.com";

    public function __construct()
    {
        $this->callbackUrl=url('api/dlb/pay_notify');
    }

    //签名方法
    public function dlb_sign($timestamp,$url_path,$data,$secret_key='')
    {
        $sign_data = array(
            "secretKey"=>empty($secret_key)?$this->secretKey:$secret_key,
            "timestamp"=>$timestamp,
            "path"=>$url_path
        );
        if($data){
            $sign_data["body"] = json_encode($data);
        }
        $o = '';

        foreach ($sign_data as $k => $v )
        {
            $o.="$k=".$v."&";
        }
        $sign_data = substr ($o, 0, -1);

        return strtoupper(sha1($sign_data));
    }


    //发起POST请求
    public function post_func($url, $post_data = '', $header_data = '', $timeout = 10, $type = '')
    {
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        if($type == ''){
            curl_setopt ($ch, CURLOPT_POST, 1);
        }else{
            curl_setopt ($ch, CURLOPT_POST, false);
        }

        if($post_data != ''){
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        }

        $o = array();
        foreach ( $header_data as $k => $v )
        {
            $o[] = "$k: $v";
        }
        $header_data = $o;

        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header_data);
        //本地忽略校验证书
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $file_contents = curl_exec($ch);
        $errmsg='';
        if(curl_errno($ch)){
            $errmsg= '请求错误:'.curl_error($ch);
        }

        curl_close($ch);
        if($errmsg!=''){
            return ['status'=>0,'message'=>$errmsg];
        }

        return [
            'status'=>1,
            'data'=>$file_contents
        ];
    }


}
