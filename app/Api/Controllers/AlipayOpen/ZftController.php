<?php
namespace App\Api\Controllers\AlipayOpen;


use Alipayopen\Sdk\AopClient;
use Alipayopen\Sdk\AopUploadClient;
use Alipayopen\Sdk\Request\AntMerchantExpandOrderQueryRequest;
use App\Api\Controllers\Config\AlipayIsvConfigController;
use App\Models\AlipayZftStore;
use Illuminate\Http\Request;

class ZftController
{

    //查询直付通审核状态
    public function query_store_status(Request $request)
    {
        try {
            $store_id = $request->get('store_id', '');
            $order_id = $request->get('order_id', '');

            $where = [];
            if ($order_id) {
                $where[] = ['order_id', '=', $order_id];
            }

            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
            }

            $AlipayZftStore = AlipayZftStore::where($where)
                ->select(
                    'config_id',
                    'order_id'
                )
                ->first();
            if (!$AlipayZftStore) {
                $data = [
                    'status' => 2,
                    "message" => '门店ID不存在或者未申请通道',
                ];
                return json_encode($data);
            }

            $config_id = $AlipayZftStore->config_id;
            $order_id = $AlipayZftStore->order_id;
            $store_id = $AlipayZftStore->store_id;

            //配置
            $isvconfig = new AlipayIsvConfigController();
            $config_type = '03'; //直付通
            $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);
            if (!$config) {
                return json_encode(['status' => 2, 'message' => '直付通未配置']);
            }

            $aop = new AopClient();
            $aop->useHeader = false;
            $aop->appId = $config->app_id;
            $aop->rsaPrivateKey = $config->rsa_private_key;
            $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
            $aop->gatewayUrl = $config->alipay_gateway;
            $aop->apiVersion = '1.0';
            $aop->signType = 'RSA2';
            $aop->format = 'json';
            $aop->method = "ant.merchant.expand.order.query	";
            $request = new AntMerchantExpandOrderQueryRequest();
            $request->setBizContent("{" .
                "\"order_id\":\"" . $order_id . "\"" .
                "  }");
            $result = $aop->execute($request);

//            dd($result);
            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            if (!empty($resultCode) && $resultCode == 10000) {
                return $result->$responseNode->image_id;
            } else {
                return '';
            }

        } catch (\Exception $exception) {
            return '';
        }
    }


    //查询代理的成本费率
    public function user_rate(Request $request)
    {
        try {
            $data = [
                'status' => 1,
                'message' => '数据查询成功',
                'data' => [
                    [
                        'num' => '3',
                        'rate' => '1.8'
                    ],
                    [
                        'num' => '6',
                        'rate' => '2.8'
                    ],
                    [
                        'num' => '12',
                        'rate' => '7.5'
                    ]
                ]
            ];

            return json_encode($data);
        } catch (\Exception $exception) {
            $data = [
                'status' => 2,
                "message" => $exception->getMessage(),
            ];
            return json_encode($data);
        }
    }


    //设置代理的成本费率
    public function set_user_rate(Request $request)
    {
        try {
            $data = [
                'status' => 1,
                'message' => '设置成功',
                'data' => $request->except('token')

            ];

            return json_encode($data);
        } catch (\Exception $exception) {
            $data = [
                'status' => 2,
                "message" => $exception->getMessage(),
            ];
            return json_encode($data);
        }
    }


    //查询门店的花呗分期费率
    public function store_fq_rate(Request $request)
    {
        try {
            $data = [
                'status' => 1,
                'message' => '数据查询成功',
                'data' => [
                    [
                        'num' => '3',
                        'rate' => '1.8'
                    ],
                    [
                        'num' => '6',
                        'rate' => '2.8'
                    ],
                    [
                        'num' => '12',
                        'rate' => '7.5'
                    ]
                ]
            ];

            return json_encode($data);
        } catch (\Exception $exception) {
            $data = [
                'status' => 2,
                "message" => $exception->getMessage(),
            ];
            return json_encode($data);
        }
    }


    //设置门店的花呗分期费率
    public function set_store_fq_rate(Request $request)
    {
        try {
            $data = [
                'status' => 1,
                'message' => '设置成功',
                'data' => $request->except('token')
            ];

            return json_encode($data);
        } catch (\Exception $exception) {
            $data = [
                'status' => 2,
                "message" => $exception->getMessage()
            ];
            return json_encode($data);
        }
    }


}
