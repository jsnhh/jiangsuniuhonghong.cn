<?php

namespace App\Api\Controllers\AlipayOpen;

use Alipayopen2020\Request\AlipayMerchantOrderSyncRequest;
use App\Api\Controllers\BaseController;
use Alipayopen\Sdk\AopClient;
use Alipayopen\Sdk\AopUploadClient;
use Alipayopen\Sdk\Request\AlipayOpenIotDeviceBindRequest;
use Alipayopen\Sdk\Request\AntMerchantExpandOrderQueryRequest;
use App\Api\Controllers\Config\AlipayIsvConfigController;
use App\Models\AlipayHbOrder;
use App\Models\AlipayHbrate;
use App\Models\AlipayUserHbrate;
use App\Models\AlipayZftStore;
use App\Models\CustomerappletsAuthorizeAppids;
use App\Models\CustomerAppletsUserOrderGoods;
use App\Models\Goods;
use App\Models\MerchantStoreAppidSecret;
use App\Models\Order;
use App\Models\Store;
use App\Models\StoreBank;
use App\Models\StoreImg;
use App\Models\User;
use Illuminate\Http\Request;
use Alipayopen2019\AopClient as AopClient2019;
use Alipayopen2020\AopClient as AopClient2020;
use Alipayopen2019\Request\AlipayOpenSpOperationApplyRequest;
use Alipayopen2019\Request\AlipayOpenSpOperationResultQueryRequest;
use Alipayopen2019\Request\AntMerchantExpandShopQueryRequest;
use Alipayopen2019\Request\AlipayMerchantIotDeviceBindRequest;
use Alipayopen2019\Request\AlipayOpenSpImageUploadRequest;
use Alipayopen2019\Request\AlipayOpenSpBlueseaactivityModifyRequest;
use Alipayopen2019\Request\AlipayOpenSpBlueseaactivityQueryRequest;
use Alipayopen2019\Request\AlipayOpenSpBlueseaactivityCreateRequest;
use App\Models\AlipayOperation;
use App\Models\AlipayAntStores;
use App\Models\AlipayBlueseaList;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class IotController extends BaseController
{
    //
    public function iot_device_bind(Request $request)
    {
        try {
            $user_token = $this->parseToken();

            $store_id = $request->get('store_id', '');
            $merchant_id_type = $request->get('merchant_id_type', 'direct');
            $merchant_alipid = $request->get('merchant_alipid', '');
            $device_id = $request->get('device_id', '');
            $mini_app_id = $request->get('mini_app_id', '');
            $config_id = $request->get('config_id', $user_token->config_id);
            $supplier_id = $request->get('supplier_id', '');
            $biz_tid = $request->get('biz_tid', '');

            $config_type = '01';
            //配置
            $isvconfig = new AlipayIsvConfigController();
            $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);

            $aop = new AopClient();
            $aop->apiVersion = "2.0";
            $aop->appId = $config->app_id;
            $aop->rsaPrivateKey = $config->rsa_private_key;
            $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
            $aop->method = 'alipay.open.iot.device.bind';
            $aop->signType = "RSA2";//升级算法
            $aop->gatewayUrl = $config->alipay_gateway;
            $aop->format = "json";
            $aop->charset = "GBK";
            $aop->version = "2.0";
            $requests = new AlipayOpenIotDeviceBindRequest();

            $data_re_arr = array(
                'mini_app_id' => $mini_app_id,
                'device_id_type' => 'ID',
                'biz_tid' => $biz_tid,
                'supplier_id' => $supplier_id,
                'device_sn' => $device_id,
                'external_id' => $store_id,
                'source' => $config->alipay_pid,
                'merchant_id_type' => $merchant_id_type,
                'merchant_id' => $merchant_alipid,
            );

            $data_re = json_encode($data_re_arr);

            $requests->setBizContent($data_re);
            $result = $aop->execute($requests, null, '');

            $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            //异常
            if ($resultCode == 40004) {
                return json_encode([
                    'status' => 2,
                    'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                    'result_code' => $resultCode,
                ]);
            } else {

                if (!empty($resultCode) && $resultCode == 10000) {
                    return json_encode([
                        'status' => 1,
                        'message' => '绑定成功',
                        'data' => $data_re_arr,
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                        'result_code' => $resultCode,
                    ]);
                }

            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'message' => $exception->getMessage(),
            ]);
        }
    }


    /**
     * 服务商向商户发起代运营操作(账号绑定或者代运营授权)
     *
     * @param Request $request
     * @return string
     */
    public function iotOperationBind(Request $request)
    {
        try {
            $user_token = $this->parseToken();
            $config_id = $request->get('config_id', $user_token->config_id);
            $user_id = $request->get('user_id', $user_token->user_id);
            $alipayAccount = $request->get('alipay_account', ''); //String,特殊可选,32,支付宝登录账号,通常为手机号或者邮箱 间连场景必填 直连场景选填,特别注意merchant_no和alipay_account不能同时为空,都有值优先取merchant_no
            $store_id = $request->get('store_id', '');
            $operate_type = $request->get('operate_type', '1'); //类型,商户类型(1-直连,2-间连)
            $merchant_no = $request->get('merchant_no'); //String,特殊可选,32,支付宝商户号.间连场景,merchant_no必填,传入商户smid,特别注意仅支持2088开头的间连商户.直连场景,merchant_no选填,传入商户支付宝pid,特别注意merchant_no和alipay_account不能同时为空,优先取merchant_no

            $check_data = [
                'alipay_account' => '支付宝账号',
                'operate_type' => '选择类型'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $out_biz_no = date('Ymdhis', time());
            if ($operate_type == '2') {
                if (!isset($merchant_no) || empty($merchant_no)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请填写间连商户号'
                    ]);
                }
                $store_type = 2;
                $data_re_arr = array(
                    'out_biz_no' => $out_biz_no, //String	必选,64,外部操作流水,ISV自定义,每次操作需要确保唯一
                    'operate_type' => 'OPERATION_AUTH', //String,必选,32,代运营操作类型.枚举如下：ACCOUNT_BIND-代表绑定支付宝账号,仅对于间连商户  OPERATION_AUTH-代表代运营授权,支持间连和直连商户,其中间连场景包含绑定支付宝账号
                    'alipay_account' => $alipayAccount,
                    'access_product_code' => 'OPENAPI_AUTH_DEFAULT', //String,必选,32,接入的产品编号.枚举如下：OPENAPI_BIND_DEFAULT-操作类型为账号绑定 OPENAPI_AUTH_DEFAULT-操作类型为代运营授权时
                    'merchant_no' => $merchant_no
                );
            } else {
                $store_type = 1;
                $data_re_arr = array(
                    'out_biz_no' => $out_biz_no,
                    'operate_type' => 'OPERATION_AUTH',
                    'alipay_account' => $alipayAccount,
                    'access_product_code' => 'OPENAPI_AUTH_DEFAULT'
                );
            }
            $config_type = '01';
            //配置
            $isvconfig = new AlipayIsvConfigController();
            $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);
            $aop = new AopClient2019();
            $aop->gatewayUrl = $config->alipay_gateway;
            $aop->appId = $config->app_id;
            $aop->rsaPrivateKey = $config->rsa_private_key;
            $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
            $aop->apiVersion = '1.0';
            $aop->signType = 'RSA2';
            $aop->postCharset = 'utf-8';
            $aop->charset = 'utf-8';
            $aop->timestamp = date("Y-m-d H:i:s", time());
            $aop->format = 'json';
            $aop->method = 'alipay.open.sp.operation.apply';
            $requests = new AlipayOpenSpOperationApplyRequest();
            $data_re = json_encode($data_re_arr);
            $requests->setBizContent($data_re);
            $result = $aop->execute($requests, null, '');
            $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            if (isset($result->$responseNode->batch_no)) {
                $resultBatchNo = $result->$responseNode->batch_no;
            }

            //异常
            if ($resultCode == 40004) {
                return json_encode([
                    'status' => 2,
                    'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                    'result_code' => $resultCode,
                ]);
            } else {
                if (!empty($resultCode) && $resultCode == 10000) {
                    $operationInfo = AlipayOperation::where('alipay_account', $alipayAccount)->first();
                    if (!$operationInfo) {
                        $data = [
                            'config_id' => $config_id,
                            'store_id' => $store_id,
                            'app_id' => $config->app_id,
                            'alipay_account' => $alipayAccount, //支付宝账号
                            'operate_type' => $operate_type, //代运营操作类型
                            'handle_status' => '1', //处理状态
                            'store_type' => $store_type,
                            'access_product_code' => 'OPENAPI_AUTH_DEFAULT',    //操作类型为代运营授权时,填OPENAPI_AUTH_DEFAULT
                            'batch_no' => $resultBatchNo,
                            // 'bind_user_id'=> isset($bind_user_id) ? $bind_user_id : "",
                            'merchant_no' => isset($merchant_no) ? $merchant_no : "",
                            'user_id' => $user_id,
                        ];
                        $obj = AlipayOperation::create($data);
                        if (!$obj) {
                            return json_encode([
                                'status' => 2,
                                'message' => '创建失败'
                            ]);
                        }
                    } else {
                        $data = [
                            'config_id' => $config_id,
                            'app_id' => $config->app_id,
                            'operate_type' => 'OPERATION_AUTH', //代运营操作类型
                            'handle_status' => '1', //处理状态
                            'store_type' => $store_type,
                            'access_product_code' => 'OPENAPI_AUTH_DEFAULT',    //操作类型为代运营授权时,填OPENAPI_AUTH_DEFAULT
                            'batch_no' => $resultBatchNo,
                            // 'bind_user_id'=> isset($bind_user_id) ? $bind_user_id : "",
                            'merchant_no' => isset($merchant_no) ? $merchant_no : "",
                            'user_id' => $user_id,
                        ];
                        $obj = AlipayOperation::where('alipay_account', $alipayAccount)->update($data);
                        if (!$obj) {
                            return json_encode([
                                'status' => 2,
                                'message' => '创建失败'
                            ]);
                        }

                    }
                    return json_encode([
                        'status' => 1,
                        'message' => '绑定成功',
                        'data' => $data_re_arr,
                        'batch_no' => $resultBatchNo,
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                        'result_code' => $resultCode,
                    ]);
                }

            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'message' => $exception->getMessage(),
            ]);
        }
    }


    //绑定列表
    public function operationList(Request $request)
    {
        try {
            $public = $this->parseToken();
            $config_id = $public->config_id;
            $store_id = $request->get('store_id', '');
            $where = [];
            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
            } else {
                $where[] = ['config_id', '=', $config_id];
            }
            $obj = DB::table('alipay_operation')->where($where)
                ->orderBy('updated_at', 'desc');
            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $data = $this->page($obj)->orderBy('updated_at', 'desc')->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //查询单条状态
    public function iotOperationBindQuery(Request $request)
    {
        try {
            $user_token = $this->parseToken();
            $id = $request->get('id', '');
            if ($id) {
                $info = AlipayOperation::where('id', $id)->first();
                $batch_no = $info['batch_no'];
                $alipayAccount = $info['alipay_account'];
            } else {
                $batch_no = $request->get('batch_no', '');
                $info = AlipayOperation::where('batch_no', $batch_no)->first();
                $alipayAccount = $info['alipay_account'];
            }
            $config_id = $request->get('config_id', $user_token->config_id);
            $config_type = '01';
            //配置
            $isvconfig = new AlipayIsvConfigController();
            $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);
            $aop = new AopClient2019();
            $aop->gatewayUrl = $config->alipay_gateway;
            $aop->appId = $config->app_id;
            $aop->rsaPrivateKey = $config->rsa_private_key;
            $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
            $aop->apiVersion = '1.0';
            $aop->signType = 'RSA2';
            $aop->postCharset = 'utf-8';
            $aop->charset = 'utf-8';
            $aop->timestamp = date("Y-m-d H:i:s", time());
            $aop->format = 'json';
            $aop->method = 'alipay.open.sp.operation.result.query';

            $data_re_arr = array(
                'operate_type' => 'OPERATION_AUTH',
                'alipay_account' => $alipayAccount,
                'batch_no' => $batch_no,
                'access_product_code' => 'OPENAPI_AUTH_DEFAULT',
            );
            $data_re = json_encode($data_re_arr);
            $requests = new AlipayOpenSpOperationResultQueryRequest();
            $requests->setBizContent($data_re);
            $result = $aop->execute($requests, null, '');
            $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            if (isset($result->$responseNode->handle_status)) {
                $resultHandleStatus = $result->$responseNode->handle_status;
                if ($resultHandleStatus == 'PROCESS') {
                    $status = 1;
                } elseif ($resultHandleStatus == 'SUCCESS') {
                    $status = 2;
                } else {
                    $status = 3; //未知错误
                }

            }
            if (isset($result->$responseNode->merchant_no)) {
                $resultMerchantNo = $result->$responseNode->merchant_no;

            }
            if (isset($result->$responseNode->bind_user_id)) {
                $resultBindUserId = $result->$responseNode->bind_user_id;
            }
            //异常
            if ($resultCode == 40004) {
                return json_encode([
                    'status' => 2,
                    'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                    'result_code' => $resultCode,
                ]);
            } else {
                if (!empty($resultCode) && $resultCode == 10000) {
                    $data = [
                        'handle_status' => $status, //处理状态
                        'bind_user_id' => isset($resultBindUserId) ? $resultBindUserId : "",
                        'merchant_no' => isset($resultMerchantNo) ? $resultMerchantNo : "",
                    ];
                    $obj = AlipayOperation::where('batch_no', $batch_no)->update($data);
                    if (!$obj) {
                        return json_encode([
                            'status' => 2,
                            'message' => '查询操作失败'
                        ]);
                    }
//                    return json_encode([
//                        'status' => 1,
//                        'message' => '查询操作失败'
//                    ]);
                    if ($status == 2) {
                        return json_encode([
                            'status' => 1,
                            'message' => '用户已操作,查询成功',
                            'data' => $data_re_arr,
                        ]);
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '用户未操作,去支付宝授权',
                            'data' => $data_re_arr,
                        ]);
                    }

                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                        'result_code' => $resultCode,
                    ]);
                }

            }
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }


    //用于服务商或商户查询其自己的店铺信息
    public function iotOperationStoreQuery(Request $request)
    {
        try {
            $user_token = $this->parseToken();
            $store_id = $request->get('store_id', '');

            $info = AlipayOperation::where('store_id', $store_id)->first();
            $alipayAccount = $info['alipay_account'];
            $bind_user_id = $info['bind_user_id']; //对应直连门店ID
            $merchant_no = $info['merchant_no']; //对应间连门店SMID
            $ali_store_id = $info['merchant_no']; //2021-08-24 update store_id为支付宝上的门店编号
            $store_id = $info['store_id'];
            $store_type = $info['store_type']; //类型,商户类型(1-直连,2-间连)
            $config_id = $request->get('config_id', $user_token->config_id);
            $config_type = '01';

            //配置
            $isvconfig = new AlipayIsvConfigController();
            $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);
            $aop = new AopClient2019();
            $aop->gatewayUrl = $config->alipay_gateway;
            $aop->appId = $config->app_id;
            $aop->rsaPrivateKey = $config->rsa_private_key;
            $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
            $aop->apiVersion = '1.0';
            $aop->signType = 'RSA2';
            $aop->postCharset = 'utf-8';
            $aop->charset = 'utf-8';
            $aop->timestamp = date("Y-m-d H:i:s", time());
            $aop->format = 'json';
            $aop->method = 'ant.merchant.expand.shop.query';

            if ($store_type == 1) {
                $data_re_arr = array(
                    'store_id' => $ali_store_id,
                    'ip_role_id' => $bind_user_id,   //直联商家pid,
                );
            } else {
                $data_re_arr = array(
                    'store_id' => $ali_store_id,
                    'ip_role_id' => $merchant_no,   //间联商家smid,
                );
            }

            $data_re = json_encode($data_re_arr);
            $requests = new AntMerchantExpandShopQueryRequest();
            $requests->setBizContent($data_re);
            $result = $aop->execute($requests, null, '');
            $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;

            //异常
            if ($resultCode == 40004) {
                return json_encode([
                    'status' => 2,
                    'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                    'result_code' => $resultCode,
                ]);
            } else {
                if (!empty($resultCode) && $resultCode == 10000) {
                    $antStoresInfo = AlipayAntStores::where('shop_id', $result->$responseNode->shop_id)->first();
                    if (!$antStoresInfo) {
                        $data = [
                            "shop_category" => $result->$responseNode->shop_category,
                            "contact_mobile" => isset($result->$responseNode->contact_phone) ? $result->$responseNode->contact_phone : "",
                            'shop_id' => $result->$responseNode->shop_id,
                            // "store_id" => $result->$responseNode->store_id,
                            "store_id" => $store_id,
                            'shop_name' => $result->$responseNode->shop_name,
                            // 'ip_role_id' => $result->$responseNode->ip_role_id,
                            'ip_role_id' => $info['merchant_no'] ?? '',
                        ];
                        $obj = AlipayAntStores::create($data);
                        if (!$obj) {
                            return json_encode([
                                'status' => 2,
                                'message' => '同步失败'
                            ]);
                        }
                    } else {
                        $data = [
                            "shop_category" => $result->$responseNode->shop_category,
                            "contact_mobile" => isset($result->$responseNode->contact_phone) ? $result->$responseNode->contact_phone : "",
                            // "store_id" => $result->$responseNode->store_id,
                            "store_id" => $store_id,
                            'shop_name' => $result->$responseNode->shop_name,
                            // 'ip_role_id' => $result->$responseNode->ip_role_id,
                            'ip_role_id' => $info['merchant_no'] ?? '',
                        ];
                        $obj = AlipayAntStores::where('shop_id', $result->$responseNode->shop_id)->update($data);
                        if (!$obj) {
                            return json_encode([
                                'status' => 2,
                                'message' => '同步失败'
                            ]);
                        }
                    }
                    return json_encode([
                        'status' => 1,
                        'message' => '同步成功',
                        'shop_id' => $result->$responseNode->shop_id,
                        'shop_name' => $result->$responseNode->shop_name,
                        // 'ip_role_id' => $result->$responseNode->ip_role_id,
                        'ip_role_id' => $info['merchant_no'] ?? '',
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                        'result_code' => $resultCode,
                    ]);
                }

            }
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }


    //查询蚂蚁门店信息
    public function iotOperationStorelist(Request $request)
    {
        try {
            $public = $this->parseToken();
            $config_id = $public->config_id;
            $id = $request->get('id', '');
            $where = [];
            if ($id) {
                $where[] = ['alipay_operation_id', '=', $id];
            }
            $obj = DB::table('alipay_ant_stores')->where($where)
                ->orderBy('created_at', 'desc');
            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $data = $this->page($obj)->orderBy('updated_at', 'desc')->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //返回蚂蚁门店绑定设备信息
    public function iotAntStorelist(Request $request)
    {
        try {
            $public = $this->parseToken();
            $config_id = $public->config_id;
            $id = $request->get('id', '');
            $where = [];
            if ($id) {
                $where[] = ['id', '=', $id];
            }
            $id = $request->get('id', '');
            $data = AlipayAntStores::where('id', $id)->first();
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //
    public function zfbAccount(Request $request)
    {
        try {
            $public = $this->parseToken();
            $config_id = $public->config_id;
            $store_id = $request->get('store_id', '');
            $data = AlipayOperation::where('store_id', $store_id)->first();
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //绑定设备
    public function iotBindDevice(Request $request)
    {
        try {
            $user_token = $this->parseToken();
            $shop_id = $request->get('shop_id', '');
            $shop_name = $request->get('shop_name', '');
            $store_type = $request->get('store_type', '1');
            $smid = $request->get('smid', '');  //支付宝2088/pid
            $biz_tid = $request->get('biz_tid', '');
            $config_id = $request->get('config_id', $user_token->config_id);
            if ($store_type == '1') {
                $merchant_type = 'direct';
            } else {
                $merchant_type = 'indirect';
            }
            $config_type = '01';
            //配置
            $isvconfig = new AlipayIsvConfigController();
            $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);

            $aop = new AopClient2019();
            $aop->apiVersion = "1.0";
            $aop->appId = $config->app_id;
            $aop->rsaPrivateKey = $config->rsa_private_key;
            $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
            $aop->method = 'alipay.merchant.iot.device.bind';
            $aop->signType = "RSA2";//升级算法
            $aop->gatewayUrl = $config->alipay_gateway;
            $aop->format = "json";
            $aop->charset = "utf-8";
            $aop->version = "1.0";
            $requests = new AlipayMerchantIotDeviceBindRequest();

            $data_re_arr = array(
                'device_id_type' => 'ID',
                'biz_tid' => $biz_tid,
                'shop_id' => $shop_id,
                'merchant_type' => $merchant_type,
                // 'pid' => $smid,
            );
            $data_re_arr['pid'] = $smid;

            // 2021-05-10，在之前的逻辑上仅做修改
            if ($store_type == '2') {
                if (empty($smid)) {
                    return json_encode([
                        'status' => 2,
                        'message' => "商户收单pid不能为空"
                    ]);
                }

                $alipayOperationInfo = DB::table('alipay_operation')->where(['merchant_no' => $smid])->first();
                if (empty($alipayOperationInfo)) {
                    return json_encode([
                        'status' => 2,
                        'message' => "支付宝运营数据为空"
                    ]);
                }

                $data_re_arr['pid'] = $alipayOperationInfo->bind_user_id;
                $data_re_arr['smid'] = $smid;
            }

            Log::info("支付宝IOT授权-参数");
            Log::info($data_re_arr);

            $data_re = json_encode($data_re_arr);

            $requests->setBizContent($data_re);
            $result = $aop->execute($requests, null, '');

            $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            Log::info("支付宝IOT授权-结果");

            if (isset($result->$responseNode->sub_code) && isset($result->$responseNode->sub_msg)) {
                Log::info($result->$responseNode->sub_code);
                Log::info($result->$responseNode->sub_msg);
            }

            //异常
            if ($resultCode == 40004) {
                return json_encode([
                    'status' => 2,
                    'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                    'result_code' => $resultCode,
                ]);
            } else {
                if (!empty($resultCode) && $resultCode == 10000) {
                    return json_encode([
                        'status' => 1,
                        'message' => '绑定成功',
                        'data' => $data_re_arr,
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                        'result_code' => $resultCode,
                    ]);
                }

            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'message' => $exception->getMessage(),
            ]);
        }
    }


    //新蓝海报名
    public function iotBlueseaCreate(Request $request)
    {
        try {
            $user_token = $this->parseToken();
            $config_id = $request->get('config_id', $user_token->config_id);
            $user_id = $request->get('user_id', $user_token->user_id);
            $store_id = $request->get('store_id', '');
            $type = $request->get('type', '');
            $biz_scene = $request->get('biz_scene', 'BLUE_SEA_FOOD_APPLY');
            $merchant_logon = $request->get('merchant_no', '');
            $sub_merchant_id = $request->get('sub_merchant_id', '');
            $check_data = [
                'store_id' => '平台门店ID',
                'biz_scene' => '选择场景',
//                'merchant_no' => '支付宝账号',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $store = Store::where('store_id', $store_id)->first();
            $store_img = StoreImg::where('store_id', $store_id)->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店未认证'
                ]);
            }
            //* 直连餐饮(BLUE_SEA_FOOD_APPLY)；
            //* 直连快消(BLUE_SEA_FMCG_APPLY)；
            if ($biz_scene == 'BLUE_SEA_FOOD_APPLY' || $biz_scene == 'BLUE_SEA_FMCG_APPLY') {
                if (!isset($merchant_logon) || empty($merchant_logon)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '参与蓝海活动的商户支付宝账号,只有当参与直连蓝海活动场景'
                    ]);
                }
            } else {
                if (!isset($sub_merchant_id) || empty($sub_merchant_id)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '参与蓝海活动的间连商户账号,只有当参与间连蓝海活动场景2088开头'
                    ]);
                }
            }
            $config_type = '01';
            //配置
            $isvconfig = new AlipayIsvConfigController();
            $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);
            $aop = new AopClient2019();
            $aop->apiVersion = "1.0";
            $aop->appId = $config->app_id;
            $aop->rsaPrivateKey = $config->rsa_private_key;
            $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
            $aop->method = 'alipay.open.sp.blueseaactivity.create';
            $aop->signType = "RSA2";//升级算法
            $aop->gatewayUrl = $config->alipay_gateway;
            $aop->format = "json";
            $aop->charset = "utf-8";
            $aop->version = "1.0";

            //必传参数
            $store_name = $store->store_name;
            $province_code = $store->province_code;
            $city_code = $store->city_code;
            $district_code = $store->area_code;
            $address = $store->store_address;
            if (!$province_code && $city_code && $district_code && $address) {
                return json_encode([
                    'status' => 2,
                    'message' => '请到门店管理补全详细地址'
                ]);
            }
            //必传照片
            //门头
            $store_logo_img = $store_img->store_logo_img;
            if (!$store_logo_img) {
                return json_encode([
                    'status' => 2,
                    'message' => '请到门店管理传门头照'
                ]);
            }
            //内景照(收银)
            $store_logo_img_a = $store_img->store_img_a;
            if (!$store_logo_img_a) {
                return json_encode([
                    'status' => 2,
                    'message' => '请到门店管理传内景照(收银)'
                ]);
            }

            //内景照(经营)
            $store_logo_img_b = $store_img->store_img_b;
            if (!$store_logo_img_b) {
                return json_encode([
                    'status' => 2,
                    'message' => '请到门店管理传内景照(经营)'
                ]);
            }
            //内景照(全景)
            $store_logo_img_c = $store_img->store_img_c;
            if (!$store_logo_img_c) {
                return json_encode([
                    'status' => 2,
                    'message' => '请到门店管理传内景照(全景)'
                ]);
            }
            //营业执照
            $store_license_img = $store_img->store_license_img;
            if (!$store_license_img) {
                return json_encode([
                    'status' => 2,
                    'message' => '请到门店管理传营业执照'
                ]);
            }
            //必须上传《餐饮服务许可证》、《食品卫生许可证》、《食品经营许可证》、《食品流通许可证》、《食品生产许可证》
            if ($type == '5499' && $biz_scene == 'BLUE_SEA_FOOD_APPLY') {
                //餐饮服务许可证
                $food_service = $store_img->food_service_lic;
                //食品卫生许可证
                $food_health = $store_img->food_health_lic;
                //食品经营许可证
                $food_business = $store_img->food_business_lic;
                //食品流通许可证
                $food_circulate = $store_img->food_circulate_lic;
                //食品生产许可证
                $food_production = $store_img->food_production_lic;
                if (!$food_service && !$food_health && !$food_business && !$food_circulate && !$food_production) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请到门店管理传餐饮其余证件'
                    ]);
                }
                //餐饮服务许可证
                $food_service_lic = $this->alipayBlueImage($food_service, $config, $aop);
                $data_re_arr['food_service_lic'] = $food_service_lic;
                //食品卫生许可证
                $food_health_lic = $this->alipayBlueImage($food_health, $config, $aop);
                $data_re_arr['food_health_lic'] = $food_health_lic;
                //食品经营许可证
                $food_business_lic = $this->alipayBlueImage($food_business, $config, $aop);
                $data_re_arr['food_business_lic'] = $food_business_lic;
                //食品流通许可证
                $food_circulate_lic = $this->alipayBlueImage($food_circulate, $config, $aop);
                $data_re_arr['food_circulate_lic'] = $food_circulate_lic;
                //食品生产许可证
                $food_production_lic = $this->alipayBlueImage($food_production, $config, $aop);
                $data_re_arr['food_production_lic'] = $food_production_lic;
            }
            if ($type == '5993') {
                //烟草许可证
                $tobacco_img = $store_img->tobacco_img;
                if (!$tobacco_img) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请到门店管理传烟草许可证'
                    ]);
                }
                $tobacco_lic = $this->alipayBlueImage($tobacco_img, $config, $aop);
                $data_re_arr['tobacco_lic'] = $tobacco_lic;
            }

            //必传图片(接口传输)
            //门头
            $shop_entrance_pic = $this->alipayBlueImage($store_logo_img, $config, $aop);

            //内景照(收银)
            $indoor_url_a = $this->alipayBlueImage($store_logo_img_a, $config, $aop);

            //内景照(经营)
            $indoor_url_b = $this->alipayBlueImage($store_logo_img_b, $config, $aop);

            //内景照(全景)
            $indoor_url_c = $this->alipayBlueImage($store_logo_img_c, $config, $aop);

            //内景照
            $data_re_arr['indoor_pic'] = [
                0 => $indoor_url_a,
                1 => $indoor_url_b,
                2 => $indoor_url_c,
            ];

            //营业执照
            $img_url = $store_img->store_license_img;
            $business_lic = $this->alipayBlueImage($img_url, $config, $aop);
            $requests = new AlipayOpenSpBlueseaactivityCreateRequest();
            if ($biz_scene == 'BLUE_SEA_FOOD_APPLY' || $biz_scene == 'BLUE_SEA_FMCG_APPLY') {
                $data_re_arr = array(
                    'biz_scene' => $biz_scene,
                    'merchant_logon' => $merchant_logon,
                    'business_lic' => $business_lic, //营业执照
                    'shop_entrance_pic' => $shop_entrance_pic,  //门头照
                    'province_code' => $province_code,
                    'city_code' => $city_code,
                    'district_code' => $district_code,
                    'address' => $address,
                    'indoor_pic' => $indoor_url_c,
                    'food_service_lic' => isset($food_service_lic) ? $food_service_lic : "",
                    'food_health_lic' => isset($food_health_lic) ? $food_health_lic : "",
                    'food_business_lic' => isset($food_business_lic) ? $food_business_lic : "",
                    'food_circulate_lic' => isset($food_circulate_lic) ? $food_circulate_lic : "",
                    'food_production_lic' => isset($food_production_lic) ? $food_production_lic : "",
                    'tobacco_lic' => isset($tobacco_lic) ? $tobacco_lic : "",
                );
            } else {
                $data_re_arr = array(
                    'biz_scene' => $biz_scene,
                    'sub_merchant_id' => $sub_merchant_id,
                    'business_lic' => $business_lic, //营业执照
                    'shop_entrance_pic' => $shop_entrance_pic,  //门头照
                    'province_code' => $province_code,
                    'city_code' => $city_code,
                    'district_code' => $district_code,
                    'address' => $address,
                    'indoor_pic' => $indoor_url_c,
                    'food_service_lic' => isset($food_service_lic) ? $food_service_lic : "",
                    'food_health_lic' => isset($food_health_lic) ? $food_health_lic : "",
                    'food_business_lic' => isset($food_business_lic) ? $food_business_lic : "",
                    'food_circulate_lic' => isset($food_circulate_lic) ? $food_circulate_lic : "",
                    'food_production_lic' => isset($food_production_lic) ? $food_production_lic : "",
                    'tobacco_lic' => isset($tobacco_lic) ? $tobacco_lic : "",
                );
            }
            $data_re = json_encode($data_re_arr);
            $requests->setBizContent($data_re);
            $result = $aop->execute($requests, null, '');
            $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            //异常
            if ($resultCode == 40004) {
                return json_encode([
                    'status' => 2,
                    'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                    'result_code' => $resultCode,
                ]);
            } else {
                if (!empty($resultCode) && $resultCode == 10000) {
                    $blueseaInfo = AlipayBlueseaList::where('store_id', $store_id)->first();
                    if (!$blueseaInfo) {
                        $data = [
                            'config_id' => $config_id,
                            'app_id' => $config->app_id,
                            'store_id' => $store_id,
                            'biz_scene' => $biz_scene,
                            'store_name' => $store_name, //处理状态
                            'order_id' => $result->$responseNode->order_id,
                            'status' => '1',    //处理中
                            'user_id' => $user_id,
                            'merchant_no' => $merchant_logon,
                            'memo' => isset($memo) ? $memo : "",
                            'type' => $type
                        ];
                        $obj = AlipayBlueseaList::create($data);
                        if (!$obj) {
                            return json_encode([
                                'status' => 2,
                                'message' => '创建失败'
                            ]);
                        }
                    }
                    return json_encode([
                        'status' => 1,
                        'message' => '创建成功',
                        'data' => $data_re_arr,
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                        'result_code' => $resultCode,
                    ]);
                }

            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'message' => $exception->getMessage(),
            ]);
        }
    }


    //新蓝海活动单修改
    public function iotBlueseaUp(Request $request)
    {
        try {
            $user_token = $this->parseToken();
            $config_id = $request->get('config_id', $user_token->config_id);
            $store_id = $request->get('store_id', '');
            $type = $request->get('type', '');
            $store = Store::where('store_id', $store_id)->first();
            $store_img = StoreImg::where('store_id', $store_id)->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店未认证'
                ]);

            }
            $blueseaInfo = AlipayBlueseaList::where('store_id', $store_id)->first();
            if (!$blueseaInfo) {
                return json_encode([
                    'status' => 2,
                    'message' => '创建时错误'
                ]);

            }
            $config_type = '01';
            //配置
            $isvconfig = new AlipayIsvConfigController();
            $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);
            $aop = new AopClient2019();
            $aop->apiVersion = "1.0";
            $aop->appId = $config->app_id;
            $aop->rsaPrivateKey = $config->rsa_private_key;
            $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
            $aop->method = 'alipay.open.sp.blueseaactivity.modify';
            $aop->signType = "RSA2";//升级算法
            $aop->gatewayUrl = $config->alipay_gateway;
            $aop->format = "json";
            $aop->charset = "utf-8";
            $aop->version = "1.0";

            //必传参数
            $province_code = $store->province_code;
            $city_code = $store->city_code;
            $district_code = $store->area_code;
            $address = $store->store_address;
            if (!$province_code && $city_code && $district_code && $address) {
                return json_encode([
                    'status' => 2,
                    'message' => '请到门店管理补全详细地址'
                ]);
            }
            //必传照片
            //门头
            $store_logo_img = $store_img->store_logo_img;
            if (!$store_logo_img) {
                return json_encode([
                    'status' => 2,
                    'message' => '请到门店管理传门头照'
                ]);
            }
            //内景照(收银)
            $store_logo_img_a = $store_img->store_img_a;
            if (!$store_logo_img_a) {
                return json_encode([
                    'status' => 2,
                    'message' => '请到门店管理传内景照(收银)'
                ]);
            }

            //内景照(经营)
            $store_logo_img_b = $store_img->store_img_b;
            if (!$store_logo_img_b) {
                return json_encode([
                    'status' => 2,
                    'message' => '请到门店管理传内景照(经营)'
                ]);
            }
            //内景照(全景)
            $store_logo_img_c = $store_img->store_img_c;
            if (!$store_logo_img_c) {
                return json_encode([
                    'status' => 2,
                    'message' => '请到门店管理传内景照(全景)'
                ]);
            }
            //营业执照
            $store_license_img = $store_img->store_license_img;
            if (!$store_license_img) {
                return json_encode([
                    'status' => 2,
                    'message' => '请到门店管理传营业执照'
                ]);
            }
            //必传图片(接口传输)
            //门头
            $shop_entrance_pic = $this->alipayBlueImage($store_logo_img, $config, $aop);

            //内景照(收银)
            $indoor_url_a = $this->alipayBlueImage($store_logo_img_a, $config, $aop);

            //内景照(经营)
            $indoor_url_b = $this->alipayBlueImage($store_logo_img_b, $config, $aop);

            //内景照(全景)
            $indoor_url_c = $this->alipayBlueImage($store_logo_img_c, $config, $aop);

            //内景照
//            $data_re_arr['indoor_url'] = [
//                0 => $indoor_url_a,
//                1 => $indoor_url_b,
//                2 => $indoor_url_c,
//            ];

            //营业执照
            $img_url = $store_img->store_license_img;
            $business_lic = $this->alipayBlueImage($img_url, $config, $aop);
            $requests = new AlipayOpenSpBlueseaactivityModifyRequest();

            $data_re_arr = array(
                'order_id' => $blueseaInfo->order_id,
                'business_lic' => $business_lic, //营业执照
                'shop_entrance_pic' => $shop_entrance_pic,  //门头照
                'province_code' => $province_code,
                'city_code' => $city_code,
                'district_code' => $district_code,
                'address' => $address,
                'indoor_pic' => $indoor_url_c, //门头照,要求内景照片清晰可见
            );
            //必须上传《餐饮服务许可证》、《食品卫生许可证》、《食品经营许可证》、《食品流通许可证》、《食品生产许可证》
            if ($type == '5499') {
                //餐饮服务许可证
                $food_service = $store_img->food_service_lic;
                //食品卫生许可证
                $food_health = $store_img->food_health_lic;
                //食品经营许可证
                $food_business = $store_img->food_business_lic;
                //食品流通许可证
                $food_circulate = $store_img->food_circulate_lic;
                //食品生产许可证
                $food_production = $store_img->food_production_lic;
                if (!$food_service && !$food_health && !$food_business && !$food_circulate && !$food_production) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请到门店管理传餐饮其余证件'
                    ]);
                }
                //餐饮服务许可证
                $food_service_lic = $this->alipayBlueImage($food_service, $config, $aop);
                $data_re_arr['food_service_lic'] = $food_service_lic;
                //食品卫生许可证
                $food_health_lic = $this->alipayBlueImage($food_health, $config, $aop);
                $data_re_arr['food_health_lic'] = $food_health_lic;
                //食品经营许可证
                $food_business_lic = $this->alipayBlueImage($food_business, $config, $aop);
                $data_re_arr['food_business_lic'] = $food_business_lic;
                //食品流通许可证
                $food_circulate_lic = $this->alipayBlueImage($food_circulate, $config, $aop);
                $data_re_arr['food_circulate_lic'] = $food_circulate_lic;
                //食品生产许可证
                $food_production_lic = $this->alipayBlueImage($food_production, $config, $aop);
                $data_re_arr['food_production_lic'] = $food_production_lic;
            }
            if ($type == '5993') {
                //烟草许可证
                $tobacco_img = $store_img->tobacco_img;
                if (!$tobacco_img) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请到门店管理传烟草许可证'
                    ]);
                }
                $tobacco_lic = $this->alipayBlueImage($tobacco_img, $config, $aop);
                $data_re_arr['tobacco_lic'] = $tobacco_lic;
            }
            $data_re = json_encode($data_re_arr);
            $requests->setBizContent($data_re);
            $result = $aop->execute($requests, null, '');
            $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            //异常
            if ($resultCode == 40004) {
                return json_encode([
                    'status' => 2,
                    'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                    'result_code' => $resultCode,
                ]);
            } else {
                if (!empty($resultCode) && $resultCode == 10000) {
                    $data = [
                        'status' => 1,
                        'memo' => '',
                        'type' => $type
                    ];
                    $obj = AlipayBlueseaList::where('order_id', $blueseaInfo->order_id)->update($data);
                    if (!$obj) {
                        return json_encode([
                            'status' => 2,
                            'message' => '更新失败'
                        ]);
                    }
                    return json_encode([
                        'status' => 1,
                        'message' => '更新成功',
                        'data' => $data_re_arr,
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                        'result_code' => $resultCode,
                    ]);
                }

            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'message' => $exception->getMessage(),
            ]);
        }
    }


    //新蓝海活动列表
    public function iotBlueseaList(Request $request)
    {
        try {
            $public = $this->parseToken();
            $config_id = $public->config_id;
            $store_id = $request->get('store_id', '');
            $where = [];
            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
            }
            $obj = DB::table('alipay_bluesea_lists')->where($where)
                ->orderBy('updated_at', 'desc');
            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $data = $this->page($obj)->orderBy('updated_at', 'desc')->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //查询新蓝海状态
    public function iotBluesQuery(Request $request)
    {
        try {
            $user_token = $this->parseToken();
            $id = $request->get('id', '');
            $info = AlipayBlueseaList::where('id', $id)->first();
            if (!$info) {
                return json_encode([
                    'status' => 2,
                    'message' => '查无此数据'
                ]);

            }
            $order_id = $info['order_id'];
            $config_id = $request->get('config_id', $user_token->config_id);
            $config_type = '01';
            //配置
            $isvconfig = new AlipayIsvConfigController();
            $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);
            $aop = new AopClient2019();
            $aop->gatewayUrl = $config->alipay_gateway;
            $aop->appId = $config->app_id;
            $aop->rsaPrivateKey = $config->rsa_private_key;
            $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
            $aop->apiVersion = '1.0';
            $aop->signType = 'RSA2';
            $aop->postCharset = 'utf-8';
            $aop->charset = 'utf-8';
            $aop->timestamp = date("Y-m-d H:i:s", time());
            $aop->format = 'json';
            $aop->method = 'alipay.open.sp.blueseaactivity.query';

            $data_re_arr = array(
                'order_id' => $order_id,
            );
            $data_re = json_encode($data_re_arr);
            $requests = new AlipayOpenSpBlueseaactivityQueryRequest();
            $requests->setBizContent($data_re);
            $result = $aop->execute($requests, null, '');
            $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            if (isset($result->$responseNode->status)) {
                $status = $result->$responseNode->status;
                if ($status == 'AUDITING') {
                    $lastStatus = 1; //处理中
                } elseif ($status == 'FAIL') {
                    $lastStatus = 2; //失败
                } elseif ($status == 'PASS') {
                    $lastStatus = 3; //成功
                } else {
                    $lastStatus = 4; //未知

                }
            }
            if (isset($result->$responseNode->memo)) {
                $memo = $result->$responseNode->memo;

            }
            if (isset($result->$responseNode->bind_user_id)) {
                $resultBindUserId = $result->$responseNode->bind_user_id;
            }
            //异常
            if ($resultCode == 40004) {
                return json_encode([
                    'status' => 2,
                    'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                    'result_code' => $resultCode,
                ]);
            } else {
                if (!empty($resultCode) && $resultCode == 10000) {
                    $data = [
                        'status' => $lastStatus, //处理状态
                        'memo' => isset($memo) ? $memo : "",
                    ];
                    $obj = AlipayBlueseaList::where('id', $id)->update($data);
                    if (!$obj) {
                        return json_encode([
                            'status' => 2,
                            'message' => '查询操作失败'
                        ]);
                    }
                    return json_encode([
                        'status' => 1,
                        'message' => '查询成功',
                        'data' => $data_re_arr,
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                        'result_code' => $resultCode,
                    ]);
                }

            }
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }


    //获取路径
    public function alipayBlueImage($img_url, $config, $aop)
    {
        try {
            $img_url = explode('/', $img_url);
            $img_url = end($img_url);
            $img = public_path() . '/upload/images/' . $img_url;
            $aop = new AopUploadClient();
            $aop->useHeader = false;
            $aop->appId = $config->app_id;
            $aop->rsaPrivateKey = $config->rsa_private_key;
            $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
            $aop->gatewayUrl = $config->alipay_gateway;
            $aop->apiVersion = '1.0';
            $aop->signType = 'RSA2';
            $aop->format = 'json';
            $aop->method = "alipay.open.sp.image.upload";
            $request = new AlipayOpenSpImageUploadRequest();
            $request->setImageContent("@" . $img);
            $result = $aop->execute($request);
            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            if (!empty($resultCode) && $resultCode == 10000) {
                return $result->$responseNode->image_id;
            } else {
                return '';
            }


        } catch (\Exception $exception) {
            return '';
        }
    }


    //查询新蓝海店铺信息
    public function shopType(Request $request)
    {
        try {
            $public = $this->parseToken();
            $biz_scene = $request->get('biz_scene', '');
            if ($biz_scene == 'BLUE_SEA_FOOD_APPLY' || $biz_scene == 'BLUE_SEA_FOOD_INDIRECT_APPLY') {
                $data = [
                    [
                        'shop_value' => '5812',
                        'shop_type' => '餐厅、订餐服务',
                    ],
                    [
                        'shop_value' => '5814',
                        'shop_type' => '快餐店',
                    ],
                    [
                        'shop_value' => '5815',
                        'shop_type' => '咖啡厅、茶馆',
                    ],
                    [
                        'shop_value' => '5462',
                        'shop_type' => '面包糕点',
                    ],
                    [
                        'shop_value' => '5811',
                        'shop_type' => '宴会提供商',
                    ],
                    [
                        'shop_value' => '5499',
                        'shop_type' => '其他食品零售',
                    ],
                    [
                        'shop_value' => '5451',
                        'shop_type' => '乳制品/冷饮',
                    ],
                    [
                        'shop_value' => '5423',
                        'shop_type' => '水果店',
                    ],
                    [
                        'shop_value' => '5441',
                        'shop_type' => '糖果及坚果商店',
                    ],
                ];

            } else {
                $data = [
                    [
                        'shop_value' => '5331',
                        'shop_type' => '杂货店',
                    ],
                    [
                        'shop_value' => '5411',
                        'shop_type' => '超市,非平台类',
                    ],
                    [
                        'shop_value' => '5641',
                        'shop_type' => '肉、禽、蛋及水产品',
                    ],
                    [
                        'shop_value' => '5499',
                        'shop_type' => '其他食品零售',
                    ],
                    [
                        'shop_value' => '5641',
                        'shop_type' => '化妆品',
                    ],
                    [
                        'shop_value' => '5977',
                        'shop_type' => '糖果及坚果商店',
                    ],
                    [
                        'shop_value' => '5993',
                        'shop_type' => '烟草/雪茄',
                    ],
                ];

            }
            $this->status = 1;
            $this->message = '数据返回成功';

            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //查询新蓝海报名单条信息
    public function blueseaInfo(Request $request)
    {
        try {
            $public = $this->parseToken();

            $id = $request->get('id', '');
            $blueseaList = AlipayBlueseaList::where('id', $id)->first();
            $this->message = '数据返回成功';
            return $this->format($blueseaList);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }


    /**
     * 订单数据同步接口
     *
     * 通用场景: 商户可以调用此接口同步交易对应的订单数据
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function aliPayMerchantOrderSync(Request $request)
    {
        try {
            $token = $this->parseToken();
            $out_trade_no = $request->get('outTradeNo', ''); //系统订单号

            $discount_info_list_external_discount_id = $request->get('discountInfoListExternalDiscountId', ''); //String,必填,128,(可选,订单优惠信息)外部优惠id
            $discount_info_list_discount_name = $request->get('discountInfoListDiscountName', ''); //String,必填,20,(可选,订单优惠信息)优惠名称
            $discount_info_list_discount_amount = $request->get('discountInfoListDiscountAmount', ''); //Price,必填,10,(可选,订单优惠信息)优惠金额
            $order_type = $request->get('orderType', 'TRADE_ORDER'); //String,特殊可选,32,订单类型,若为空,默认为交易订单,每次请求必传 服务订单-SERVICE_ORDER 交易订单-TRADE_ORDER
            $order_auth_code = $request->get('orderAuthCode', ''); //String,特殊可选,64,订单授权码(与订单一一对应,不是请求维度的,服务订单首次同步必传)利用订单同步前获取的formId作为订单授权码. 订单授权码对应的小程序id与买家uid与同步订单的小程序id和买家uid一致.注:当order_type为SERVICE_ORDER时必填
            $order_modified_time = $request->get('orderModifiedTime', ''); //Date,特殊可选,32,订单修改时间,一般不需要传入 用于订单状态或数据变化较快的顺序控制,order_modified_time较晚的同步会被最终存储,order_modified_time相同的两次同步可能会被幂等处理,SERVICE_ORDER按照行业标准化接入场景必传
            $ticket_info_type = $request->get('ticketInfoType', ''); //String,必填,64,(可选,凭证信息)凭证类型 MEAL_NUM-取餐号;PICKUP_CODE-取件码;SELF_PICK_CODE-自提码;PASSWORD-口令
            $ticket_info_ticket_no = $request->get('ticketInfoTicketNo', ''); //String,必填,64,(可选,凭证信息)单据号,不同类型下单据号含义不同. 若类型为MEAL_NUM,则ticket_no表示取餐号;若类型为PICKUP_CODE,则ticket_no表示取件码
            $ticket_info_time = $request->get('ticketInfoTime', ''); //Date,特殊可选,64,(可选,凭证信息)时间,如果传入了end_time则该字段必传 不同类型下的地址含义不同. 若type为MEAL_NUM,time则代表取餐时间; 若type为PICKUP_CODE,address则代表取件时间
            $order_create_time = $request->get('orderCreateTime', ''); //Date,特殊可选,32,订单创建时间,当order_type为SERVICE_ORDER时必传
            $order_pay_time = $request->get('orderPayTime', ''); //Date,特殊可选,32,订单支付时间  当pay_channel为非ALIPAY时,且订单状态已流转到“支付”或支付后时,需要将支付时间传入
            $record_id = $request->get('recordId', '');  //String,可选,34,商户订单同步记录id
            $buyer_info_name = $request->get('buyerInfoName', ''); //String,可选,16,(可选,buyer_info与buyer_id必选其一)姓名
            $buyer_info_mobile = $request->get('buyerInfoMobile', ''); //String,可选,20,可选,buyer_info与buyer_id必选其一手机号
            $buyer_info_cert_no = $request->get('buyerInfoCertNo', ''); //String,可选,64,可选,buyer_info与buyer_id必选其一证件号
            $discount_amount = $request->get('discountAmount', ''); //Price,可选,11,优惠金额
            $pay_timeout_express = $request->get('payTimeoutExpress', ''); //String,可选,7,支付超时时间,超过时间支付宝自行关闭订单
            $send_msg = $request->get('sendMsg', ''); //String,可选,16,是否需要小程序订单代理发送模版消息,Y代表需要发送,N代表不需要发送,不传默认不发送
            $discount_info_list_discount_quantity = $request->get('discountInfoListDiscountQuantity', ''); //Number,可选,10,(可选,订单优惠信息)优惠数量
            $discount_info_list_discount_page_link = $request->get('discountInfoListDiscountPageLink', ''); //String,可选,128,(可选,订单优惠信息)优惠跳转链接地址
            $ticket_info_resv_type = $request->get('ticketInfoResvType', ''); //String,可选,64,(可选,凭证信息)凭证预约类型,一般不需要传入. 可选值为INSTANT/RESERVATION,其中INSTANT代表是实时凭证,RESERVATION代表是预约凭证,不传入默认为实时凭证. 区别在于预约凭证一般不是当场可取,而是用户下单后的很多天之后才可以凭借凭证提取
            $ticket_info_address = $request->get('ticketInfoAddress', ''); //String,可选,128,(可选,凭证信息)地址 不同类型下的地址含义不同. 若type为MEAL_NUM,address则代表取餐地点; 若type为PICKUP_CODE,address则代表取件地点
            $ticket_info_shop = $request->get('ticketInfoShop', ''); //String,可选,128,(可选,凭证信息)凭证可核销门店/货品自提门店,如果自提门店与购买门店不一致,可传入该字段提示用户自提门店
            $ticket_info_end_time = $request->get('ticketInfoEndTime', ''); //Date,可选,64,(可选,凭证信息)截止时间,如果凭证自提/核销时间是一个时间段可传入该字段,time代表自提开始时间,end_time代表自提结束时间,结束时间必须晚于开始时间
            $logistics_info_list_tracking_no = $request->get('logisticsInfoListTrackingNo', ''); //String,必填,128,(可选,物流信息,列表最多支持物流信息个数)物流单号
            $logistics_info_list_logistics_code = $request->get('logisticsInfoListLogisticsCode', ''); //String,可选,128,(可选,物流信息,列表最多支持物流信息个数)物流公司编号,注:该值为空时,有可能匹配不到物流信息.若有则必传

            $check_data = [
                'outTradeNo' => '系统订单号'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                $this->status = 2;
                $this->message = $check;
                return $this->format();
            }

            $orderInfo = Order::where('out_trade_no', $out_trade_no)
                ->select('config_id', 'user_id', 'trade_no', 'other_no', 'store_id', 'merchant_id', 'ways_type', 'ways_source', 'pay_method', 'company', 'total_amount', 'receipt_amount', 'mdiscount_amount', 'device_id', 'buyer_id', 'pay_status')
                ->first();
            if (!$orderInfo) {
                $this->status = 2;
                $this->message = '未找到订单信息';
                return $this->format();
            }
            $pay_status = $orderInfo->pay_status;
            if ($pay_status != 1) {
                $this->status = 2;
                $this->message = '该订单尚未完成支付';
                return $this->format();
            }
            $config_id = $orderInfo->config_id;
            $store_id = $orderInfo->store_id; //系统门店id
            $trade_no = $orderInfo->trade_no;
            $total_amount = $orderInfo->total_amount;
            $receipt_amount = $orderInfo->receipt_amount;
            $mdiscount_amount = $orderInfo->mdiscount_amount;
            $buyer_id = $orderInfo->buyer_id;
            $amount = $total_amount; //Price,特殊可选,11,订单金额,单位为元. SERVICE_ORDER且不涉及金额可不传入该字段,其他场景必传
            $pay_amount = $receipt_amount ?? round(($total_amount * 100 - $mdiscount_amount * 100) / 100, 2); //Price,特殊可选,11,支付金额,需要实际支付的金额,SERVICE_ORDER且不涉及金额可不传入该字段,其他场景必传

            $storesObj = Store::where('store_id', $store_id)
                ->select('store_name', 'store_short_name', 'province_name', 'city_name', 'area_name', 'store_address', 'is_close', 'is_delete')
                ->first();
            if (!$storesObj) {
                $this->status = 2;
                $this->message = '没找到系统门店信息';
                return $this->format();
            }
            $store_name = $storesObj->store_name;
            $store_short_name = $storesObj->store_short_name;
            $people_phone = $storesObj->people_phone;
            $province_name = $storesObj->province_name;
            $city_name = $storesObj->city_name;
            $area_name = $storesObj->area_name;
            $store_address = $storesObj->store_address;

            $aliPayAntStoresObj = AlipayAntStores::where('store_id', $store_id)->first();
            if (!$aliPayAntStoresObj) {
                $this->status = 2;
                $this->message = '没找到支付宝蚂蚁门店信息';
                return $this->format();
            }
            $shop_info_merchant_shop_id = $aliPayAntStoresObj->shop_id; //String,可选,64,(可选,门店信息.扫码点餐获取返佣时必填),商户门店id  支持英文、数字的组合
            $shop_info_name = $aliPayAntStoresObj->shop_name ?? $store_name; //String,可选,64,(可选,门店信息.扫码点餐获取返佣时必填),店铺名称
            $shop_info_address = $province_name . $city_name . $area_name . $store_address; //String,可选,128,(可选,门店信息.扫码点餐获取返佣时必填),店铺地址
            $shop_info_phone_num = $aliPayAntStoresObj->contact_mobile ?? $people_phone; //String,可选,16,(可选,门店信息.扫码点餐获取返佣时必填),联系电话-支持固话或手机号 仅支持数字、+、-
//            $shop_info_merchant_shop_link_page = $aliPayAntStoresObj->url; //String,可选,256,(可选,门店信息.扫码点餐获取返佣时必填),店铺详情链接地址

            $storeInfo = MerchantStoreAppidSecret::select('alipay_appid')
                ->where('store_id', $store_id)
                ->first();
            if (empty($storeInfo)) {
                $this->status = 2;
                $this->message = '请先完善该门店小程序信息';
                return $this->format();
            }

            $mini_app_id = $storeInfo->alipay_appid;
            $appletsInfo = CustomerappletsAuthorizeAppids::where('applet_type', 2)
                ->where('AuthorizerAppid', $mini_app_id)
                ->first();
            if (empty($appletsInfo)) {
                $this->status = 2;
                $this->message = '该支付宝小程序暂未授权';
                return $this->format();
            }

            $CustomerAppletsUserOrderGoodsArrs = CustomerAppletsUserOrderGoods::where('out_trade_no', $out_trade_no)->get()->toArray();
            if (!$CustomerAppletsUserOrderGoodsArrs) {
                $this->status = 2;
                $this->message = '未找到订单商品信息';
                return $this->format();
            }
            $ItemOrderInfo = [];
            foreach ($CustomerAppletsUserOrderGoodsArrs as $keys => $values) {
//                print_r($values);die;
                $goodsObj = Goods::where('id', $values['good_id'])->select('name')->first();
//                print_r($goodsObj->name);die;
                if ($goodsObj) {
                    $ItemOrderInfo[$keys]['item_name'] = $goodsObj->name; //String,必填,256,(可选,商品信息列表)商品名称
                } else {
                    continue;
                }
                $ItemOrderInfo[$keys]['unit_price'] = $values['good_price']; //Price,特殊可选,18,(可选,商品信息列表)商品单价(单位:元)小程序订单助手业务中,为必传
                $ItemOrderInfo[$keys]['quantity'] = $values['good_num']; //Number,特殊可选,9,(可选,商品信息列表)商品数量(单位:自拟)小程序订单助手业务中,为必传
//                $ItemOrderInfo[$keys]['sku_id'] = $itemOrderListSkuId; //String,可选,34,(可选,商品信息列表)商品sku id
//                $ItemOrderInfo[$keys]['item_id'] = $itemOrderListItemId; //String,可选,34,(可选,商品信息列表)商品id
            }

            $data_re_arr = [
                'out_biz_no' => $out_trade_no,
                'order_type' => $order_type,
                'order_auth_code' => $order_auth_code,
                'amount' => $amount,
                'pay_amount' => $pay_amount,
                'trade_no' => $trade_no, //String,特殊可选,64,订单所对应的支付宝交易号
                'trade_type' => 'TRADE', //String,可选,32,交易号类型 1.TRADE-交易,为空默认为TRADE 2.TRANSFER-转账 3.ENTRUST-受托
                'order_create_time' => $order_create_time,
                'order_modified_time' => date('Y-m-d H:i:s', strtotime($order_modified_time)),
                'order_pay_time' => date('Y-m-d H:i:s', strtotime($order_pay_time)),
                'sync_content' => 'ALL', //String,可选,64,同步内容 JOURNEY_ONLY-仅行程信息 ALL-全部(默认)
//                'journey_order_list' => [ //可选	,行程信息
//                    'merchant_journey_no' => $journey_order_list_merchant_journey_no, //String,必填,128,商户行程单号 注:同一个pid下的行程单号需唯一. 同一个pid下外部行程单号唯一
//                    'journey_desc' => $journey_order_list_journey_desc, //String,可选,256,行程描述
//                    'journey_index' => $journey_order_list_journey_index, //String,可选,10,描述本行程为整个行程中的第几程
//                    'title' => $journey_order_list_title, //String,可选,256,行程标题
//                    'status' => $journey_order_list_status, //String,可选,128,行程状态  注：行程状态必须与支付宝侧进行约定
//                    'status_desc' => $journey_order_list_status_desc, //String,可选,128,行程状态描述
//                    'type' => $journey_order_list_type, //String,必填,64,行程类型
//                    'sub_type' => $journey_order_list_sub_type, //String,必填,64,行程子类型
//                    'journey_elements' => [ //必填,行程元素列表
//                        'departure' => [ //可选,出发地信息
//                            'merchant_id' => $departure_merchant_id, //String,可选,64,商户侧地点id
//                            'name' => $departure_name, //String,必填,512,地点名称
//                            'city' => $departure_city, //String,可选,216,城市名
//                            'merchant_poi' => $departure_merchant_poi, //String,可选,64,商户侧poi信息
//                            'merchant_division_id' => $departure_merchant_division_id, //String,可选,64,商户侧行政区划代码
////                            'ext_info' => [
////                                'ext_key' => MY_KEY, //
////                                'ext_value' => MY_VALUE
////                            ]
//                        ],
//                        'arrival' => [ //可选		达到地信息
//                            'merchant_id' => $arrival_merchant_id, //String,可选,64,商户侧地点id
//                            'name' => $arrival_name, //String,必填,512,地点名称
//                            'city' => $arrival_city, //String,可选,216,城市名
//                            'merchant_poi' => $arrival_merchant_poi, //String,可选,64,商户侧poi信息
//                            'merchant_division_id' => $arrival_merchant_division_id, //String,可选,64,商户侧行政区划代码
////                            'ext_info' => [
////                                'ext_key' => MY_KEY, //
////                                'ext_value' => MY_VALUE
////                            ]
//                        ],
//                        'start_time' => $journey_elements_start_time, //String,可选,64,开始时间格式化 2021-01-01 12:21:22
//                        'start_time_desc' => $journey_elements_start_time_desc, //String,可选,128,开始时间描述(非格式化) 1月1号
//                        'end_time' => $journey_elements_end_time, //String,可选,64,结束时间(格式化)
//                        'end_time_desc' => $journey_elements_end_time_desc, //String,可选,128,结束时间描述(非结构化)
//                        'duration' => $journey_elements_duration, //String,可选,64,行程时长,单位为秒
//                        'passagers' => [ //可选,出行人
//                            'name' => $passagers_name, //String,可选,16,姓名
//                            'mobile' => $passagers_mobile, //String,可选,20,手机号
//                            'cert_type' => $passagers_cert_type, //String,可选,32,身份证：IDENTITY_CARD、护照：PASSPORT、军官证：OFFICER_CARD、士兵证：SOLDIER_CARD、户口本：HOKOU等
//                            'cert_no' => $passagers_cert_no, //String,可选,64,证件号
////                            'ext_info' => [
////                                'ext_key' => MY_KEY, //
////                                'ext_value' => MY_VALUE
////                            ]
//                        ],
//                        'service_provider' => [ //可选,服务提供方信息
//                            'name' => $service_provider_name, //String,必填,256,商家名称
//                            'short_name' => $service_provider_short_name, //String,可选,256,商家简称
//                            'logo' => $service_provider_logo, //String,可选,512,商户logo链接
////                            'ext_info' => [
////                                'ext_key' => MY_KEY, //
////                                'ext_value' => MY_VALUE
////                            ]
//                        ],
//                        'functional_services' => [ //可选,功能服务列表
//                            'function_code' => $functional_services_function_code, //String,必填,128,功能码 支持的功能码请与产品或对应技术确认
//                            'function_name' => $functional_services_function_name, //String,必填,128,功能名称
//                            'function_type' => $functional_services_function_type, //String,必填,64,功能类型
//                            'function_url' => $functional_services_function_url, //String,可选,512,功能入口
//                            'content' => $functional_services_content, //String,可选,512,功能内容
//                            'memo' => $functional_services_memo, //String,可选,512,备注信息
////                            'ext_info' => [
////                                'ext_key' => MY_KEY, //
////                                'ext_value' => MY_VALUE
////                            ]
//                        ],
//                        'service_change_info' => [ //可选,服务变更信息
//                            'change_status' => $service_change_info_change_status, //String,必填,128,变更状态
//                            'remind_content' => $service_change_info_remind_content, //String,可选,512,提醒内容
//                            'detail_url' => $service_change_info_detail_url, //String,可选,512,详情url
////                            'ext_info' => [
////                                'ext_key' => 'MY_KEY', //
////                                'ext_value' => 'MY_VALUE' //
////                            ]
//                        ],
////                        'ext_info' => [
////                            'ext_key' => 'MY_KEY', //
////                            'ext_value' => 'MY_VALUE'
////                        ]
//                    ],
//                    'journey_create_time' => $journey_create_time, //Date,必填,20,行程创建时间
//                    'journey_modify_time' => $journey_modify_time, //Date,必填,20,行程修改时间
//                    'action' => $action, //String,可选,64,操作动作-DELETE 删除,删除后的行程不再展示
//                    'ext_info' => [
//                        'ext_key' => 'MY_KEY', //
//                        'ext_value' => 'MY_VALUE'
//                    ]
//                ],
//                'ext_info' => [ //
//                    'ext_key' => 'MY_KEY', //
//                    'ext_value' => 'MY_VALUE'
//                ]
            ];
            if ($buyer_id) {
                $data_re_arr['buyer_id'] = $buyer_id;
            } else {
                //可选,buyer_info与buyer_id必选其一
                $data_re_arr['buyer_info']['name'] = $buyer_info_name;
                $data_re_arr['buyer_info']['mobile'] = $buyer_info_mobile;
                $data_re_arr['buyer_info']['cert_type'] = 'IDENTITY_CARD'; //String,可选,32,身份证-IDENTITY_CARD;护照-PASSPORT;军官证-OFFICER_CARD;士兵证-SOLDIER_CARD;户口本-HOKOU
                $data_re_arr['buyer_info']['cert_no'] = $buyer_info_cert_no;
            }
            if ($record_id) $data_re_arr['record_id'] = $record_id;
            if ($discount_amount) $data_re_arr['discount_amount'] = $discount_amount;
            if ($pay_timeout_express) $data_re_arr['pay_timeout_express'] = $pay_timeout_express;
            if ($ItemOrderInfo && $ItemOrderInfo != []) {
                $data_re_arr['item_order_list'] = $ItemOrderInfo;
            }
            if ($logistics_info_list_tracking_no) $data_re_arr['logistics_info_list']['tracking_no'] = $logistics_info_list_tracking_no;
            if ($logistics_info_list_logistics_code) $data_re_arr['logistics_info_list']['logistics_code'] = $logistics_info_list_logistics_code;
            if ($shop_info_merchant_shop_id) $data_re_arr['shop_info']['merchant_shop_id'] = $shop_info_merchant_shop_id;
            if ($shop_info_name) $data_re_arr['shop_info']['name'] = $shop_info_name;
            if ($shop_info_address) $data_re_arr['shop_info']['address'] = $shop_info_address;
            if ($shop_info_phone_num) $data_re_arr['shop_info']['phone_num'] = $shop_info_phone_num;
//            if ($shop_info_merchant_shop_link_page) $data_re_arr['shop_info']['merchant_shop_link_page'] = $shop_info_merchant_shop_link_page;
            if ($ticket_info_type) $data_re_arr['ticket_info']['type'] = $ticket_info_type;
            if ($ticket_info_resv_type) $data_re_arr['ticket_info']['resv_type'] = $ticket_info_resv_type;
            if ($ticket_info_address) $data_re_arr['ticket_info']['address'] = $ticket_info_address;
            if ($ticket_info_shop) $data_re_arr['ticket_info']['shop'] = $ticket_info_shop;
            if ($ticket_info_end_time) $data_re_arr['ticket_info']['end_time'] = $ticket_info_end_time;
            if ($send_msg) $data_re_arr['send_msg'] = $send_msg;
            if ($discount_info_list_discount_quantity) $data_re_arr['discount_info_list']['discount_quantity'] = $discount_info_list_discount_quantity;
            if ($discount_info_list_discount_page_link) $data_re_arr['discount_info_list']['discount_page_link'] = $discount_info_list_discount_page_link;
            if ($discount_info_list_external_discount_id) $data_re_arr['discount_info_list']['external_discount_id'] = $discount_info_list_external_discount_id;
            if ($discount_info_list_discount_name) $data_re_arr['discount_info_list']['discount_name'] = $discount_info_list_discount_name;
            if ($discount_info_list_discount_amount) $data_re_arr['discount_info_list']['discount_amount'] = $discount_info_list_discount_amount;
            if ($discount_info_list_discount_page_link) $data_re_arr['discount_info_list']['discount_page_link'] = $discount_info_list_discount_page_link;
//            if ($sync_content) $data_re_arr['sync_content'] = $sync_content;
//            if ($journey_order_list_journey_desc) $data_re_arr['journey_order_list']['journey_desc'] = $journey_order_list_journey_desc;
//            if ($journey_order_list_journey_index) $data_re_arr['journey_order_list']['journey_index'] = $journey_order_list_journey_index;
//            if ($journey_order_list_title) $data_re_arr['journey_order_list']['title'] = $journey_order_list_title;
//            if ($journey_order_list_status) $data_re_arr['journey_order_list']['status'] = $journey_order_list_status;
//            if ($journey_order_list_status_desc) $data_re_arr['journey_order_list']['status_desc'] = $journey_order_list_status_desc;
//            if ($departure_merchant_id) $data_re_arr['journey_order_list']['journey_elements']['departure']['merchant_id'] = $departure_merchant_id;
//            if ($departure_city) $data_re_arr['journey_order_list']['journey_elements']['departure']['city'] = $departure_city;
//            if ($departure_merchant_poi) $data_re_arr['journey_order_list']['journey_elements']['departure']['merchant_poi'] = $departure_merchant_poi;
//            if ($departure_merchant_division_id) $data_re_arr['journey_order_list']['journey_elements']['departure']['merchant_division_id'] = $departure_merchant_division_id;
//            if ($arrival_merchant_id) $data_re_arr['journey_order_list']['journey_elements']['arrival']['merchant_id'] = $arrival_merchant_id;
//            if ($arrival_city) $data_re_arr['journey_order_list']['journey_elements']['arrival']['city'] = $arrival_city;
//            if ($arrival_merchant_poi) $data_re_arr['journey_order_list']['journey_elements']['arrival']['merchant_poi'] = $arrival_merchant_poi;
//            if ($arrival_merchant_division_id) $data_re_arr['journey_order_list']['journey_elements']['arrival']['merchant_division_id'] = $arrival_merchant_division_id;
//            if ($journey_elements_start_time) $data_re_arr['journey_order_list']['journey_elements']['start_time'] = $journey_elements_start_time;
//            if ($journey_elements_start_time_desc) $data_re_arr['journey_order_list']['journey_elements']['start_time_desc'] = $journey_elements_start_time_desc;
//            if ($journey_elements_end_time) $data_re_arr['journey_order_list']['journey_elements']['end_time'] = $journey_elements_end_time;
//            if ($journey_elements_end_time_desc) $data_re_arr['journey_order_list']['journey_elements']['end_time_desc'] = $journey_elements_end_time_desc;
//            if ($journey_elements_duration) $data_re_arr['journey_order_list']['journey_elements']['duration'] = $journey_elements_duration;
//            if ($passagers_name) $data_re_arr['journey_order_list']['journey_elements']['passagers']['name'] = $passagers_name;
//            if ($passagers_mobile) $data_re_arr['journey_order_list']['journey_elements']['passagers']['mobile'] = $passagers_mobile;
//            if ($passagers_cert_type) $data_re_arr['journey_order_list']['journey_elements']['passagers']['cert_type'] = $passagers_cert_type;
//            if ($passagers_cert_no) $data_re_arr['journey_order_list']['journey_elements']['passagers']['cert_no'] = $passagers_cert_no;
//            if ($service_provider_short_name) $data_re_arr['journey_order_list']['journey_elements']['service_provider']['short_name'] = $service_provider_short_name;
//            if ($service_provider_logo) $data_re_arr['journey_order_list']['journey_elements']['service_provider']['logo'] = $service_provider_logo;
//            if ($functional_services_function_url) $data_re_arr['journey_order_list']['journey_elements']['functional_services']['function_url'] = $functional_services_function_url;
//            if ($functional_services_content) $data_re_arr['journey_order_list']['journey_elements']['functional_services']['content'] = $functional_services_content;
//            if ($functional_services_memo) $data_re_arr['journey_order_list']['journey_elements']['functional_services']['memo'] = $functional_services_memo;
//            if ($service_change_info_detail_url) $data_re_arr['journey_order_list']['journey_elements']['service_change_info']['detail_url'] = $service_change_info_detail_url;
//            if ($action) $data_re_arr['journey_order_list']['action'] = $action;
            Log::info('支付宝-订单数据同步接口-入参：');
            Log::info($data_re_arr);
            $data_re = json_encode($data_re_arr);

            $config_type = '01';
            $isvconfig = new AlipayIsvConfigController();
            $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);
            $aop = new AopClient2020();
            $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aop->appId = $config->app_id;
            $aop->rsaPrivateKey = $config->rsa_private_key;
            $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
            $aop->apiVersion = '1.0';
            $aop->signType = 'RSA2';
            $aop->postCharset = 'utf-8';
            $aop->charset = 'utf-8';
            $aop->timestamp = date("Y-m-d H:i:s", time());
            $aop->format = 'json';
            $aop->method = 'alipay.merchant.order.sync';
            $request = new AlipayMerchantOrderSyncRequest();
            $request->setBizContent($data_re);
            $result = $aop->execute($request);
            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            $msg = $result->$responseNode->msg;
            Log::info('支付宝-订单数据同步接口-结果：');
            Log::info($resultCode);
            if (!empty($resultCode) && $resultCode == 10000) {
                $record_id = $result->$responseNode->record_id; //同步订单记录id,(同步小程序订单成功必返回record_id)
                $order_id = $result->$responseNode->order_id; //支付宝订单号
                $order_status = $result->$responseNode->order_status; //订单状态

                $this->status = 1;
                $this->message = $msg;
                return $this->format([
                    'record_id' => $record_id ?? '',
                    'order_id' => $order_id ?? '',
                    'order_status' => $order_status ?? ''
                ]);
            } else {
                $sub_code = isset($result->$responseNode->sub_code) ? $result->$responseNode->sub_code : ''; //业务返回码
                $sub_msg = isset($result->$responseNode->sub_msg) ? $result->$responseNode->sub_msg : ''; //业务返回码描述

                $this->status = 2;
                $this->message = $sub_msg ?? $msg;
                return $this->format();
            }
        } catch (\Exception $e) {
            $this->status = -1;
            $this->message = $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine();
            return $this->format();
        }
    }


}
