<?php


namespace App\Api\Controllers\AlipayOpen;


use Alipayopen\Sdk\AopClient;
use Alipayopen\Sdk\Request\AlipayOpenAgentCreateRequest;
use App\Api\Controllers\Config\AlipayIsvConfigController;
use App\Models\AlipayCashCoupons;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DmfController extends \App\Api\Controllers\BaseController
{
    //代签约当面付产品
    public function agentCreate(Request $request)
    {
        try {
            //batch_no	String	必选	25	代商户操作事务编号，通过https://opendocs.alipay.com/apis/api_50/alipay.open.agent.create (开启代商户签约、创建应用事务)接口进行事务创建后获取。	2017110616474516400082883
            //mcc_code	String	必选	16	商家经营类目编码。详情可参考
            //商家经营类目 中的“经营类目编码”	A_A03_4582
            //special_license_pic	Byte_array	特殊可选	5242880	企业特殊资质图片，当mcc_code为需要特殊资质类目时必填。可参考
            //商家经营类目 中的“需要的特殊资质证书”，最小5KB ，最大5M（暂不限制图片宽高），图片格式必须为：png、bmp、gif、jpg、jpeg	-
            //rate	String	特殊可选	3	服务费率（%），0.38~0.6 之间（小数点后两位，可取0.38%及0.6%）。
            //当签约且授权标识 sign_and_auth=true 时，该费率信息必填。	0.38
            //sign_and_auth	Boolean	可选	1	签约且授权标识，默认为false，只进行签约操作； 如果设置为true，则表示签约成功后，会自动进行应用授权操作。	false
            //business_license_no	String	可选	32	营业执照号码	1532501100006302
            //business_license_pic	Byte_array	可选	5242880	营业执照图片。被代创建商户运营主体为个人账户必填，企业账户无需填写，最小5KB，最大5M（暂不限制图片宽高），图片格式必须为：png、bmp、gif、jpg、jpeg	-
            //business_license_auth_pic	Byte_array	可选	5242880	营业执照授权函图片，个体工商户如果使用总公司或其他公司的营业执照认证需上传该授权函图片，最小5KB，最大5M（暂不限制图片宽高），图片格式必须为：png、bmp、gif、jpg、jpeg	-
            //long_term	Boolean	可选	1	营业期限是否长期有效	true
            //date_limitation	String	可选	10	营业期限	2017-11-11
            //shop_scene_pic	Byte_array	可选	5242880	店铺内景图片，最小5KB，最大5M（暂不限制图片宽高），图片格式必须为：png、bmp、gif、jpg、jpeg	-
            //shop_sign_board_pic

            $user_token = $this->parseToken();
            $config_id = $request->get('config_id', $user_token->config_id);
            $user_id = $request->get('user_id', $user_token->user_id);
            $store_id = $request->get('store_id', $user_token->user_id);
            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }
            $auth = DB::table('alipay_app_oauth_users')->where('store_id', $store_id)->first();
            if(!$auth) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店未进行授权请去授权操作'
                ]);
            }
            if(!$auth->alipay_user_id){
                return json_encode([
                    'status' => 2,
                    'message' => '门店未绑定正确的支付宝门店id'
                ]);
            }
            if(!$auth->app_auth_token){
                return json_encode([
                    'status' => 2,
                    'message' => '门店未绑定正确的授权码'
                ]);
            }
            //isv代操作的商户账号，可以是支付宝账号，也可以是pid（2088开头）
            $account = $request->get('account', '');
            //联系人名称
            $contactName = $request->get('contactName', '');
            //联系人手机号码
            $contactMobile = $request->get('contactMobile', '');
            //联系人邮箱
            $contactEmail= $request->get('contactEmail', '');

            $check_data = [
                'account' => '支付宝账号',
                'contactName' => '联系人名称',
                'contactMobile' => '联系人手机号码',
                'contactEmail' => '联系人邮箱',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            //商户联系人信息，包含联系人名称、手机、邮箱信息。联系人信息将用于接受签约后的重要通知，如确认协议、到期提醒等。
            $contact_info = array(
                array(
                    'contactName' => $contactName,
                    'contactMobile' => $contactMobile,
                    'contactEmail' => $contactEmail,
                )
            );

            $config_type = '01';
            //配置
            $isvconfig = new AlipayIsvConfigController();
            $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);
            $aop = new AopClient();
            $aop->gatewayUrl = $config->alipay_gateway;
            $aop->appId = $config->app_id;
            $aop->rsaPrivateKey = $config->rsa_private_key;
            $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
            $aop->apiVersion = '1.0';
            $aop->signType = 'RSA2';
            $aop->postCharset='utf-8';
            $aop->charset='utf-8';
            $aop->timestamp= date("Y-m-d H:i:s",time());
            $aop->format='json';
            $aop->method = 'alipay.open.agent.create';
            $requests = new AlipayOpenAgentCreateRequest();
            $data_re_arr = array(
                'account' => $account,
                'contact_info' => $contact_info,
            );
            $data_re = json_encode($data_re_arr);
            $requests->setBizContent($data_re);
            $result = $aop->execute($requests, null, '');
            $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            //异常
            if ($resultCode == 40004) {
                return json_encode([
                    'status' => 2,
                    'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                    'result_code' => $resultCode,
                ]);
            } else {
                if (!empty($resultCode) && $resultCode == 10000) {
                    $operationInfo = AlipayCashCoupons::where('template_id', $result->$responseNode->template_id)->first();
                    if(!$operationInfo){
                        $data = [
                            'config_id' => $config_id,
                            'store_id' => $store_id,
                            'template_id' => $result->$responseNode->template_id,
                            'voucher_type' => 1,
                            'brand_name' => $brand_name,
                            'publish_start_time' => $publish_start_time,
                            'publish_end_time' => $publish_end_time,
                            'voucher_start' => $start,
                            'voucher_end' => $end,
                            'voucher_available_time'=> $voucher_available,
                            'out_biz_no'=> $out_biz_no,
                            'voucher_description'=> isset($voucher_description) ? $voucher_description : "",
                            'amount' => $amount,
                            'voucher_quantity' => $voucher_quantity,
                            'floor_amount' => $floor_amount,
                        ];
                        $obj = AlipayCashCoupons::create($data);
                        if (!$obj) {
                            return json_encode([
                                'status' => 2,
                                'message' => '创建失败'
                            ]);
                        }
                    }
                    return json_encode([
                        'status' => 1,
                        'message' => '创建成功',
                        'data' => $data_re_arr,
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                        'result_code' => $resultCode,
                    ]);
                }

            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'message' => $exception->getMessage(),
            ]);
        }
    }

}