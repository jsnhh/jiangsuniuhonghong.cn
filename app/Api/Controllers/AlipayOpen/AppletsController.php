<?php

namespace App\Api\Controllers\AlipayOpen;

use App\Api\Controllers\BaseController;
use Alipayopen\Sdk\AopClient;
use Alipayopen\Sdk\Request\AlipayOpenAgentCreateRequest;
use Alipayopen2019\Request\AlipayOpenAgentMiniCreateRequest;
use Alipayopen2019\AopClient as AopClient2019;
use Alipayopen2019\Request\AlipayMarketingCashlessvoucherTemplateCreateRequest;
use Alipayopen2019\Request\AlipayOpenMiniCategoryQueryRequest;
use App\Api\Controllers\Config\AlipayIsvConfigController;
use App\Models\Applets;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Alipayopen2019\Request\AlipayOpenAppQrcodeCreateRequest;
use Alipayopen2019\Request\AlipayOpenMiniQrcodeBindRequest;
use Alipayopen2019\Request\AlipayOpenAgentFacetofaceSignRequest;

class AppletsController extends BaseController
{

    //小程序授权获得batch_no
    public function agentCreate(Request $request)
    {
        try {

            $user_token = $this->parseToken();
            $store_id = $request->get('storeId', $user_token->user_id);
            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }
            $config_id = '1234';
            $storeInfo = DB::table("ali_third_config")
                ->where(['config_id' => $config_id])->first();
            if(empty($storeInfo)){
                return $this->responseDataJson(202,"请先完善该门店小程序配置");
            }
            //isv代操作的商户账号，可以是支付宝账号，也可以是pid（2088开头）
            $account = $request->get('account', '');
            //联系人名称
            $contactName = $request->get('contactName', '');
            //联系人手机号码
            $contactMobile = $request->get('contactMobile', '');
            //联系人邮箱
            $contactEmail= $request->get('contactEmail', '');

            $check_data = [
                'account' => '支付宝账号',
                'contactName' => '联系人名称',
                'contactMobile' => '联系人手机号码',
                'contactEmail' => '联系人邮箱',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            //商户联系人信息，包含联系人名称、手机、邮箱信息。联系人信息将用于接受签约后的重要通知，如确认协议、到期提醒等。
            $contact_info = array(
                'contact_name' => $contactName,
                'contact_mobile' => $contactMobile,
                'contact_email' => $contactEmail,
            );


            $aop = new AopClient();
            $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aop->appId = $storeInfo->aliPay_applet_id;
            $aop->rsaPrivateKey = $storeInfo->rsa_private_key;
            $aop->alipayrsaPublicKey = $storeInfo->rsa_public_key;
            $aop->apiVersion = '1.0';
            $aop->signType = 'RSA2';
            $aop->postCharset='utf-8';
            $aop->charset='utf-8';
            $aop->timestamp= date("Y-m-d H:i:s",time());
            $aop->format='json';
            $aop->method = 'alipay.open.agent.create';
            $requests = new AlipayOpenAgentCreateRequest();
            $data_re_arr = array(
                'account' => $account,
                'contact_info' => $contact_info,
            );
            $data_re = json_encode($data_re_arr);
            $requests->setBizContent($data_re);
            $result = $aop->execute($requests, null, '');
            $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            //异常
            if ($resultCode == 40004) {
                return json_encode([
                    'status' => 2,
                    'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                    'result_code' => $resultCode,
                ]);
            } else {
                if (!empty($resultCode) && $resultCode == 10000) {
                    $appletsInfo = Applets::where('store_id', $store_id)->first();
                    if(!$appletsInfo){
                        $data = [
                            'config_id' => $config_id,
                            'store_id' => $store_id,
                            'batch_no' => $result->$responseNode->batch_no,
                            'account' => $account,
                            'contact_name' => $contactName,
                            'contact_mobile' => $contactMobile,
                            'contact_email' => $contactEmail,
                        ];
                        $obj = Applets::create($data);
                        if (!$obj) {
                            return json_encode([
                                'status' => 2,
                                'message' => '创建失败'
                            ]);
                        }
                    }else{
                        $data = [
                            'config_id' => $config_id,
                            'store_id' => $store_id,
                            'batch_no' => $result->$responseNode->batch_no,
                            'account' => $account,
                            'contact_name' => $contactName,
                            'contact_mobile' => $contactMobile,
                            'contact_email' => $contactEmail,
                        ];
                        $obj = Applets::where('store_id', $store_id)->update($data);
                        if (!$obj) {
                            return json_encode([
                                'status' => 2,
                                'message' => '创建失败'
                            ]);
                        }
                    }
                    return json_encode([
                        'status' => 1,
                        'message' => '创建成功',
                        'data' => $data,
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                        'result_code' => $resultCode,
                    ]);
                }

            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'message' => $exception->getMessage(),
            ]);
        }
    }


    //代商家创建小程序应用
    public function agentMiniCreate(Request $request)
    {
        try {
            $user_token = $this->parseToken();
            $config_id = $request->get('config_id', $user_token->config_id);
            $user_id = $request->get('user_id', $user_token->user_id);
            $store_id = $request->get('storeId', $user_token->user_id);
            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();

            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }
            $config_id = '1234';
            $storeInfo = DB::table("ali_third_config")
                ->where(['config_id' => $config_id])->first();
            if(empty($storeInfo)){
                return $this->responseDataJson(202,"请先完善该门店小程序配置");
            }
            //代商家操作事务编号
            $batchNo = $request->get('batchNo', '');
            //新小程序前台类目
            $miniCategoryIds = $request->get('miniCategoryIds', '');
            //代商家创建的小程序应用名称
            $appName = $request->get('appName', '');
            //小程序英文名称
            $appEnglishName = $request->get('appEnglishName', '');
            //代商家创建的小程序的简介
            $appSlogan = $request->get('appSlogan', '');
            //商家小程序的客服电话
            $servicePhone= $request->get('servicePhone', '');
            //商家小程序客服邮箱
            $serviceEmail = $request->get('serviceEmail', '');
            //商家小程序应用图标
            $appLogo = $request->get('appLogo', '');
            //商家小程序描述信息
            $appDesc = $request->get('appDesc', '');
            $check_data = [
                'batchNo' => '代商家操作事务编号',
                'miniCategoryIds' => '新小程序前台类目',
                'appName' => '代商家创建的小程序应用名称',
                'appSlogan' => '代商家创建的小程序的简介',
                'servicePhone' => '商家小程序的客服电话',
                'serviceEmail' => '商家小程序客服邮箱',
                'appLogo' => '商家小程序应用图标',
                'appDesc' => '商家小程序描述信息',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $img_url = explode('/', $appLogo);
            $img_url = end($img_url);
            $img = public_path() . '/upload/images/' . $img_url;
            //配置
            $aop = new AopClient2019();
            $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aop->appId = $storeInfo->aliPay_applet_id;
            $aop->rsaPrivateKey = $storeInfo->rsa_private_key;
            $aop->alipayrsaPublicKey = $storeInfo->rsa_public_key;
            $aop->apiVersion = '1.0';
            $aop->signType = 'RSA2';
            $aop->postCharset='utf-8';
            $aop->charset='utf-8';
            $aop->timestamp= date("Y-m-d H:i:s",time());
            $aop->format='json';
            $aop->method = 'alipay.open.agent.mini.create';



            $requests = new AlipayOpenAgentMiniCreateRequest();
            $requests->setBatchNo($batchNo);
            $requests->setMiniCategoryIds($miniCategoryIds);
            $requests->setAppName($appName);
            $requests->setAppEnglishName($appEnglishName);
            $requests->setAppSlogan($appSlogan);
            $requests->setServicePhone($servicePhone);
            $requests->setServiceEmail($serviceEmail);
            $requests->setAppLogo("@". $img);
            $requests->setAppDesc($appDesc);
            $data_re_arr = array(
                'batch_no' => $batchNo,
                'mini_category_ids' => $miniCategoryIds,
                'app_name' => $appName,
                'app_english_name' => $appEnglishName,
                'app_slogan' => $appSlogan,
                'service_phone' => $servicePhone,
                'service_email' => $serviceEmail,
                'app_logo' => $appLogo,
                'app_desc' => $appDesc,
            );
            $result = $aop->execute($requests);
            $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            //异常
            if ($resultCode == 40004) {
                return json_encode([
                    'status' => 2,
                    'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                    'result_code' => $resultCode,
                ]);
            } else {
                if (!empty($resultCode) && $resultCode == 10000) {
                    return json_encode([
                        'status' => 1,
                        'message' => '创建成功',
                        'data' => $data_re_arr,
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                        'result_code' => $resultCode,
                    ]);
                }
            }
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'message' => $exception->getMessage(),
            ]);
        }
    }


//    public function agentMiniCreate($data)
//    {
//        try {
//
//            $user_token = $this->parseToken();
//            $config_id = $request->get('config_id', $user_token->config_id);
//            $user_id = $request->get('user_id', $user_token->user_id);
//            $store_id = $request->get('store_id', $user_token->user_id);
//
//            //代商家操作事务编号
//            $batchNo = $data['batch_no'];
//            //新小程序前台类目
//            $miniCategoryIds = $request->get('miniCategoryIds', '');
//            //代商家创建的小程序应用名称
//            $appName = $request->get('appName', '');
//            //小程序英文名称
//            $appEnglishName = $request->get('appEnglishName', '');
//            //小程序类目
//            $appCategoryIds = $request->get('appCategoryIds', '');
//            //代商家创建的小程序的简介
//            $appSlogan = $request->get('appSlogan', '');
//            //商家小程序的客服电话
//            $servicePhone= $request->get('servicePhone', '');
//            //商家小程序客服邮箱
//            $serviceEmail = $request->get('serviceEmail', '');
//            //商家小程序应用图标
//            $appLogo = $request->get('appLogo', '');
//            //商家小程序描述信息
//
//            $config_type = '01';
//            //配置
//            $isvconfig = new AlipayIsvConfigController();
//            $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);
//            $aop = new AopClient();
//            $aop->gatewayUrl = $config->alipay_gateway;
//            $aop->appId = $config->app_id;
//            $aop->rsaPrivateKey = $config->rsa_private_key;
//            $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
//            $aop->apiVersion = '1.0';
//            $aop->signType = 'RSA2';
//            $aop->postCharset='utf-8';
//            $aop->charset='utf-8';
//            $aop->timestamp= date("Y-m-d H:i:s",time());
//            $aop->format='json';
//            $aop->method = 'alipay.open.agent.mini.create';
//            $requests = new AlipayOpenAgentMiniCreateRequest();
//            $data_re_arr = array(
//                'batch_no' => $batchNo,
//                'mini_category_ids' => $miniCategoryIds,
//                'app_name' => $appName,
//                'app_english_name' => $appEnglishName,
//                'app_category_ids' => $appCategoryIds,
//                'app_slogan' => $appSlogan,
//                'service_phone' => $servicePhone,
//                'service_email' => $serviceEmail,
//                'app_logo' => $appLogo,
//                'app_desc' => $appDesc,
//            );
//            $data_re = json_encode($data_re_arr);
//            $requests->setBizContent($data_re);
//            $result = $aop->execute($requests, null, '');
//            $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
//            $resultCode = $result->$responseNode->code;
//            //异常
//            if ($resultCode == 40004) {
//                return json_encode([
//                    'status' => 2,
//                    'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
//                    'result_code' => $resultCode,
//                ]);
//            } else {
//                if (!empty($resultCode) && $resultCode == 10000) {
//                    return json_encode([
//                        'status' => 1,
//                        'message' => '创建成功',
//                        'data' => $data_re_arr,
//                    ]);
//                } else {
//                    return json_encode([
//                        'status' => 2,
//                        'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
//                        'result_code' => $resultCode,
//                    ]);
//                }
//
//            }
//        } catch (\Exception $exception) {
//            return json_encode([
//                'status' => 2,
//                'message' => $exception->getMessage(),
//            ]);
//        }
//    }


    /**
     * 小程序类目树查询
     *
     * @param Request $request
     * @return array
     */
    public function aliPayOpenMiniCategoryQuery(Request $request)
    {
        try {
            $user_token = $this->parseToken();
            $store_id = $request->get('storeId', $user_token->user_id);

            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }

            $config_id = '1234';
            $storeInfo = DB::table("ali_third_config")
                ->where(['config_id' => $config_id])->first();
            if(empty($storeInfo)){
                return $this->responseDataJson(202, "请先完善该门店小程序配置");
            }

            $aop = new AopClient2019();
            $aop->gatewayUrl            = 'https://openapi.alipay.com/gateway.do';
            $aop->appId = $storeInfo->aliPay_applet_id;
            $aop->rsaPrivateKey = $storeInfo->rsa_private_key;
            $aop->alipayrsaPublicKey = $storeInfo->rsa_public_key;
            $aop->method                = "alipay.open.mini.category.query";
            $aop->charset               = "utf-8";
            $aop->timestamp             = date("Y-m-d H:i:s", time());
            $aop->apiVersion            = '1.0';
            $aop->signType              = 'RSA2';
            $aop->postCharset           = 'utf-8';
            $aop->format                = 'json';
            $queryRequest = new AlipayOpenMiniCategoryQueryRequest();
            $queryRequest->setBizContent("{" .
                "\"is_filter\":true" .
                "  }");
            $queryResult = $aop->execute($queryRequest, null, '');
            $queryResponse = str_replace(".", "_", $queryRequest->getApiMethodName()) . "_response";
            $queryResponseNode = $queryResult->$queryResponse->code;
            if (!empty($queryResponseNode) && $queryResponseNode == 10000) {
                $mini_category_list = $queryResult->$queryResponse->mini_category_list;
                return ['status' => 200,'message' => '查询成功','data' => $mini_category_list];
            } else {
                return ['status' => 202,'message' => $queryResult->$queryResponse->msg.'|'.$queryResult->$queryResponse->sub_code.'|'.$queryResult->$queryResponse->sub_msg];
            }
        } catch (\Exception $e) {
            return ['status' => 202,'message' => $e->getMessage()];
        }
    }


    /**
     * 小程序生成二维码
     *
     * @param $data['url_param']	String	必选	256	page/component/component-pages/view/view为小程序中能访问到的页面路径	page/component/component-pages/view/view
     * @param $data['query_param']	 String	必选	256	小程序的启动参数，打开小程序的query ，在小程序 onLaunch的方法中获取	x=1
     * @param $data['describe']	  String	必选	32	对应的二维码描述
     * @param $data['app_auth_token']  授权码
     * @return string
     */
    public function qrcodeCreate($data)
    {
        try {
            $url_param = $data['url_param']; //小程序中能访问到的页面路径
            $query_param = $data['query_param']; //小程序的启动参数
            $describe = $data['describe']; //对应的二维码描述
            $app_auth_token = $data['app_auth_token']; //授权码

            $config_id = '1234';
            $storeInfo = DB::table("ali_third_config")
                ->where(['config_id' => $config_id])->first();
            if(empty($storeInfo)){
                return $this->responseDataJson(202,"请先完善该门店小程序配置");
            }

            //配置
            $aop = new AopClient();
            $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aop->appId = $storeInfo->aliPay_applet_id;
            $aop->rsaPrivateKey = $storeInfo->rsa_private_key;
            $aop->alipayrsaPublicKey = $storeInfo->rsa_public_key;
            $aop->apiVersion = '1.0';
            $aop->signType = 'RSA2';
            $aop->postCharset='utf-8';
            $aop->charset='utf-8';
            $aop->timestamp= date("Y-m-d H:i:s",time());
            $aop->format='json';
            $aop->method = 'alipay.open.app.qrcode.create';
            $requests = new AlipayOpenAppQrcodeCreateRequest();
            $data_re_arr = array(
                'url_param' => $url_param,
                'query_param' => $query_param,
                'describe' => $describe
            );
            $data_re = json_encode($data_re_arr);
            $requests->setBizContent($data_re);
            $result = $aop->execute($requests, null, $app_auth_token);
            $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            //异常
            if ($resultCode == 40004) {
                return json_encode([
                    'status' => 2,
                    'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                    'result_code' => $resultCode
                ]);
            } else {
                if (!empty($resultCode) && $resultCode == 10000) {
                    return json_encode([
                        'status' => 1,
                        'message' => '创建成功',
                        'data' => $result->$responseNode->qr_code_url
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                        'result_code' => $resultCode
                    ]);
                }
            }

        } catch (\Exception $ex) {
            return json_encode([
                'status' => 2,
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    public function qrcodeBind($data)
    {
        try {
            //小程序中能访问到的页面路径
            $route_url = $data['route_url'];
            //小程序的启动参数
            $page_redirection = $data['page_redirection'];
            $app_auth_token = $data['app_auth_token'];
            $config_id = '1234';
            $storeInfo = DB::table("ali_third_config")
                ->where(['config_id' => $config_id])->first();
            if(empty($storeInfo)){
                return $this->responseDataJson(202,"请先完善该门店小程序配置");
            }
            //配置
            $aop = new AopClient();
            $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aop->appId = $storeInfo->aliPay_applet_id;
            $aop->rsaPrivateKey = $storeInfo->rsa_private_key;
            $aop->alipayrsaPublicKey = $storeInfo->rsa_public_key;
            $aop->apiVersion = '1.0';
            $aop->signType = 'RSA2';
            $aop->postCharset='utf-8';
            $aop->charset='utf-8';
            $aop->timestamp= date("Y-m-d H:i:s",time());
            $aop->format='json';
            $aop->method = 'alipay.open.mini.qrcode.bind';
            $requests = new AlipayOpenMiniQrcodeBindRequest();
            $data_re_arr = array(
                'route_url' => $route_url,
                'page_redirection' => $page_redirection,
                'mode' => 'FUZZY',
            );
            $data_re = json_encode($data_re_arr);
            $requests->setBizContent($data_re);
            $result = $aop->execute($requests, null, $app_auth_token);
            $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            //异常
            if ($resultCode == 40004) {
                return json_encode([
                    'status' => 2,
                    'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                    'result_code' => $resultCode,
                ]);
            } else {
                if (!empty($resultCode) && $resultCode == 10000) {
                    return json_encode([
                        'status' => 1,
                        'message' => '创建成功',
                        'data' => $result->$responseNode->route_group,
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                        'result_code' => $resultCode,
                    ]);
                }

            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'message' => $exception->getMessage(),
            ]);
        }
    }


    public function aliPayApplet(Request $request)
    {
        try {
            $user_token = $this->parseToken();
            $store_id = $request->get('storeId', $user_token->user_id);
            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }
            $data = DB::table('customerapplets_authorize_appids')->where(['store_id' => $store_id,'applet_type' => 2])->first();
            if ($data) {
                $this->status = 1;
                $this->message = '查询成功';
                return $this->format($data);
            } else {
                $this->status = 2;
                $this->message = '查询失败';
                return $this->format($store_id);
            }
        } catch (\Exception $e) {
            return ['status' => 202,'message' => $e->getMessage()];
        }
    }


    //(代签约当面付产品)alipay.open.agent.facetoface.sign
    public function facetofaceSign(Request $request)
    {
        try {

            $user_token = $this->parseToken();
            $config_id = $request->get('config_id', $user_token->config_id);
            $user_id = $request->get('user_id', $user_token->user_id);
            $store_id = $request->get('storeId', '');
            $batch_no = $request->get('batchNo', ''); //代商户操作事务编号
            $mcc_code = $request->get('mccCode', ''); //商家经营类目编码
            $rate = $request->get('rate', '0.38'); //服务费率（%），0.38~0.6 之间（小数点后两位，可取0.38%及0.6%）。
//            $sign_and_auth = $request->get('sign_and_auth', '');

            $check_data = [
                'mccCode' => '商家经营类目编码',
                'batchNo' => '事务编号'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $store = Store::where('store_id', $store_id)->first();
            $store_img = StoreImg::where('store_id', $store_id)->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店未认证'
                ]);
            }
            //营业执照号码
            $business_license_no = $store_img->store_license_no;
            if(!$business_license_no){
                return json_encode([
                    'status' => 2,
                    'message' => '请到门店管理传营业执照号码'
                ]);
            }
            //营业执照照片
            $business_license_pic = $store_img->store_license_img;
            //营业期限是否长期有效
            $long_term = 'true';
            //营业期限
            $date_limitation = $a;
            //必传照片
            //门头
            $shop_sign_board_pic = $store_img->store_logo_img;
            if(!$shop_sign_board_pic){
                return json_encode([
                    'status' => 2,
                    'message' => '请到门店管理传门头照'
                ]);
            }
            //内景照(收银)
            $store_logo_img_a = $store_img->store_img_a;
            if(!$store_logo_img_a){
                return json_encode([
                    'status' => 2,
                    'message' => '请到门店管理传内景照(收银)'
                ]);
            }

            //内景照(经营)
            $store_logo_img_b = $store_img->store_img_b;
            if(!$store_logo_img_b){
                return json_encode([
                    'status' => 2,
                    'message' => '请到门店管理传内景照(经营)'
                ]);
            }
            //内景照(全景)
            $store_logo_img_c = $store_img->store_img_c;
            if(!$store_logo_img_c){
                return json_encode([
                    'status' => 2,
                    'message' => '请到门店管理传内景照(全景)'
                ]);
            }
            //营业执照
            $store_license_img = $store_img->store_license_img;
            if(!$store_license_img){
                return json_encode([
                    'status' => 2,
                    'message' => '请到门店管理传营业执照'
                ]);
            }
            $config_type = '01';
            //配置
            $isvconfig = new AlipayIsvConfigController();
            $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);
            $aop = new AopClient2019();
            $aop->gatewayUrl = $config->alipay_gateway;
            $aop->appId = $config->app_id;
            $aop->rsaPrivateKey = $config->rsa_private_key;
            $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
            $aop->apiVersion = '1.0';
            $aop->signType = 'RSA2';
            $aop->postCharset='utf-8';
            $aop->charset='utf-8';
            $aop->timestamp= date("Y-m-d H:i:s",time());
            $aop->format='json';
            $aop->method = 'alipay.open.agent.facetoface.sign';
            $requests = new AlipayOpenAgentFacetofaceSignRequest();
            $requests->setBatchNo($batch_no);
            $requests->setMccCode($mcc_code);
            //企业特殊资质图片，当mcc_code为需要特殊资质类目时必填
            //$request->setSpecialLicensePic("@"."本地文件路径");
            $requests->setRate($rate);
            $requests->setSignAndAuth(false);
            $requests->setBusinessLicenseNo("1532501100006302");
//            $requests->setBusinessLicensePic("@"."本地文件路径");
//            $requests->setBusinessLicenseAuthPic("@"."本地文件路径");
            $requests->setLongTerm(true);
//            $requests->setDateLimitation("2017-11-11");
//            $requests->setShopScenePic("@"."本地文件路径");
//            $requests->setShopSignBoardPic("@"."本地文件路径");
            $result = $aop->execute ( $request);

            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            $resultCode = $result->$responseNode->code;
            //异常
            if ($resultCode == 40004) {
                return json_encode([
                    'status' => 2,
                    'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                    'result_code' => $resultCode,
                ]);
            } else {
                if (!empty($resultCode) && $resultCode == 10000) {

                    return json_encode([
                        'status' => 1,
                        'message' => '绑定成功',
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                        'result_code' => $resultCode,
                    ]);
                }

            }
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'message' => $exception->getMessage(),
            ]);
        }
    }


}
