<?php

namespace App\Api\Controllers\AllinPay;


use App\Models\AllinPayStore;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;

class BaseController
{
    public $private_key = '';
    public $public_key = '';
    public $scan_url = 'https://syb-test.allinpay.com/apiweb/unitorder/scanqrpay';//商户主扫接口
    public $jsapi_url = 'https://syb-test.allinpay.com/apiweb/unitorder/pay';//客户主扫接口
    public $cancel_url = 'https://syb-test.allinpay.com/apiweb/tranx/cancel';//交易撤销接口 当天交易退款接口
    public $refund_url = 'https://syb-test.allinpay.com/apiweb/tranx/refund';//退款接口
    public $query_url = 'https://syb-test.allinpay.com/apiweb/tranx/query';//订单查询接口
    public $get_userid_url = 'https://syb-test.allinpay.com/apiweb/unitorder/authcodetouserid';//获取银联标识

    //RSA签名
    public function getSign(array $array)
    {
        $allinPayStore = AllinPayStore::where('cus_id', $array['cusid'])->select('private_key')->first();
        if ($allinPayStore) {
            $this->private_key = $allinPayStore->private_key;
        } else {
            return [
                'status' => '-1',
                'message' => '请填写商户私钥',
            ];
        }

        ksort($array);

        $bufSignSrc = $this->ToUrlParams($array);
        $private_key = chunk_split($this->private_key, 64, "\n");
        $key = "-----BEGIN RSA PRIVATE KEY-----\n" . wordwrap($private_key) . "-----END RSA PRIVATE KEY-----";

        if (openssl_sign($bufSignSrc, $signature, $key)) {

        } else {
            Log::info('sign fail');
        }
        $sign = base64_encode($signature);//加密后的内容通常含有特殊字符，需要编码转换下，在网络间通过url传输时要注意base64编码是否是url安全的
        return [
            'status' => '1',
            'sign' => $sign,
        ];;
    }


    public function ToUrlParams(array $array)
    {
        $buff = "";
        foreach ($array as $k => $v) {
            if ($v != "" && !is_array($v)) {
                $buff .= $k . "=" . $v . "&";
            }
        }

        $buff = trim($buff, "&");
        return $buff;
    }

    public function request($url, $params)
    {
        $http = new Client();
        $response = $http->request('POST', $url, [
            'Content-Type' => 'application/x-www-form-urlencoded;charset=UTF-8',
            'form_params' => $params
        ]);
        return $response->getBody();
    }

    public function ValidSign(array $array)
    {
        $sign = $array['sign'];
        unset($array['sign']);
        ksort($array);
        $bufSignSrc = $this->ToUrlParams($array);
        $public_key = chunk_split($this->public_key, 64, "\n");
        $key = "-----BEGIN PUBLIC KEY-----\n$public_key-----END PUBLIC KEY-----\n";
        $result = openssl_verify($bufSignSrc, base64_decode($sign), $key);
        return $result;
    }
}