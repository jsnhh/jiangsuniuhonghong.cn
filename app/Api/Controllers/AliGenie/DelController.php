<?php
/**
 * Created by PhpStorm.
 * User: EDZ
 * Date: 2021/1/20
 * Time: 15:38
 */
namespace App\Api\Controllers\AliGenie;


use App\Api\Controllers\BaseController;
use App\Models\AligenieList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DelController extends BaseController
{

    //天猫精灵 删除记录
    public function delAliGenie(Request $request)
    {
        $token = $this->parseToken();
        $activation_code = $request->post('activationCode', ''); //激活码

        $check_data = [
            'activationCode' => '激活码'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            return $this->responseDataJson(150007);
        }

        try {
            $data_obj = AligenieList::where('activation_code', $activation_code)->delete();
            if ($data_obj) {
                return $this->responseDataJson(150008);
            } else {
                return $this->responseDataJson(150009);
            }
        } catch (\Exception $e) {
            Log::info('天猫精灵-删除记录-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return $this->responseDataJson(-1);
        }
    }


}