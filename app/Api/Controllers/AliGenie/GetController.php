<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2021/1/20
 * Time: 15:39
 */
namespace App\Api\Controllers\AliGenie;


use App\Api\Controllers\BaseController;
use App\Models\AligenieList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetController extends BaseController
{

    //天猫精灵 绑定列表
    public function aliGenieList(Request $request)
    {
        $token = $this->parseToken();
        $store_id = $request->post('storeId', ''); //门店id
        $activation_code = $request->post('activationCode', ''); //激活码
        $device_open_id = $request->post('deviceOpenId', ''); //猫精唯一编号

        $check_data = [
            'storeId' => '门店id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            return json_encode([
                'status' => 2,
                'message' => $check
            ]);
        }

        $where = [];
        if ($activation_code) {
            $where[] = ['activation_code', '=', $activation_code];
        }
        if ($device_open_id) {
            $where[] = ['device_open_id', '=', $device_open_id];
        }

        try {
            $data = AligenieList::where('store_id', $store_id)
                ->where($where)
                ->orderBy('created_at', 'desc')
                ->get();
            if ($data) {
                return $this->responseDataJson(1, $data);
            } else {
                return $this->responseDataJson(2);
            }

        } catch (\Exception $e) {
            Log::info('天猫精灵-绑定列表-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return $this->responseDataJson(-1);
        }
    }


    //通过猫精编号获取绑定的门店信息
    public function storeIdByDeviceOpenId($data)
    {
        $deviceOpenId = $data['deviceOpenId']; //猫精唯一编号
        $time = $data['time']; //时间

        if (!isset($deviceOpenId) && empty($deviceOpenId)) {
            return $this->responseDataJson(150006);
        }

        if (isset($time) && !empty($time)) {
            $start_time = "$time 00:00:00";
            $end_time = "$time 23:59:59";
        } else {
            $start_time = date('Y-m-d 00:00:00', time());
            $end_time = date('Y-m-d h:i:s', time());
        }

        try {
            $data = AligenieList::where('device_open_id', $deviceOpenId)
                ->select(['store_id', 'activation_code'])
                ->first();
            if ($data) {
                $store_id = $data->store_id;
                if (!$store_id) {
                    return $this->responseDataJson(150013); //尚未绑定门店
                }

                if (env('DB_D1_HOST')) {
                    $orders_obj = DB::connection("mysql_d1")->table('orders');
                } else {
                    $orders_obj = DB::table('orders');
                }
                $orderObj = $orders_obj->where('pay_status', 1)
                    ->where('pay_time', '>=', $start_time)
                    ->where('pay_time', '<=', $end_time)
                    ->where('store_id', $store_id)
                    ->select(['total_amount', 'receipt_amount', 'refund_amount', 'mdiscount_amount']);
                $sum_amount = $orderObj->sum('total_amount');

                $return_data = [
                    'sum_amount' => number_format($sum_amount, 2, '.', ''),
                    'store_id' => $store_id
                ];

                return $this->responseDataJson(1, $return_data);
            } else {
                return $this->responseDataJson(150014); //尚未生成激活码
            }

        } catch (\Exception $e) {
            Log::info('通过猫精编号获取绑定的门店信息-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return $this->responseDataJson(-1);
        }
    }


}
