<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2021/1/20
 * Time: 15:38
 */
namespace App\Api\Controllers\AliGenie;


use App\Api\Controllers\BaseController;
use App\Models\AligenieList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AddController extends BaseController
{

    //天猫精灵 绑定门店
    public function addAliGenie(Request $request)
    {
        $token = $this->parseToken();
        $store_id = $request->post('storeId', ''); //门店id
        $activation_code = $request->post('activationCode', ''); //激活码

        $check_data = [
            'storeId' => '门店id',
            'activationCode' => '激活码'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            return json_encode([
                'status' => 2,
                'message' => $check
            ]);
        }

        try {
            $data_obj = AligenieList::where('activation_code', $activation_code)
                ->first();
            if ($data_obj) {
                if ($data_obj->store_id) {
                    return $this->responseDataJson(150011);
                }

                if ($data_obj->store_id == $store_id) {
                    return $this->responseDataJson(150010);
                }

                $update_data = [
                    'store_id' => $store_id,
                    'activation_code' => $activation_code
                ];
                $res = $data_obj->update($update_data);
                if ($res) {
                    return $this->responseDataJson(150002);
                } else {
                    return $this->responseDataJson(150003);
                }
            } else {
                return $this->responseDataJson(150001);
            }
        } catch (\Exception $e) {
            Log::info('天猫精灵-绑定-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return $this->responseDataJson(-1);
        }
    }


    //天猫精灵 生成激活码
    public function addAliGenieCode($data)
    {
        $device_open_id = $data['deviceOpenId']; //猫精唯一编号
        if (!isset($device_open_id) && empty($device_open_id)) {
            return $this->responseDataJson(150006);
        }

        try {
            $data = AligenieList::where('device_open_id', $device_open_id)
                ->first();
            if ($data) {
                return $this->responseDataJson(150012);
            }

            $activation_code = $this->createActivationCode();
            $add_data = [
                'device_open_id' => $device_open_id,
                'activation_code' => $activation_code
            ];
            $res = AligenieList::create($add_data);
            if ($res) {
                return $this->responseDataJson(150004, $activation_code);
            } else {
                return $this->responseDataJson(150005);
            }
        } catch (\Exception $e) {
            Log::info('天猫精灵-生成激活码-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return $this->responseDataJson(-1);
        }
    }


    /**
     * 生成激活码
     * @return int
     */
    protected function createActivationCode()
    {
        $code = rand(1000, 9999);

        $isExits = AligenieList::where('activation_code', $code)->exists();
        if ($isExits) {
            $this->createActivationCode();
        } else {
            return $code;
        }
    }


}
