<?php
namespace App\Api\Controllers; 


use Dingo\Api\Http\Request;
use EasyWeChat\Factory;

class TestController extends BaseController
{
	/*
		客户端请求服务器示例
	*/
    public function index()
    {
    	try{
	    	$request=app('request');
	    	$this->status=2;
	    	$name =  $request->get('name','获取姓名');

	    	if(empty($name))
	    	{
	    		$this->message='参数不能为空！';
	    		return $this->format();
	    	}

	    	$order=new Order;
	    	$order = $order->where('id','>',1);

	    	$this->t=$order->count();
	    	$data = $this->page($order,$request)->get();
	    	$this->status=1;
	    	return $this->format($data);
    	}catch(\Exception $e){
    		$this->status= -1 ;
    		$this->message='系统错误'.$e->getMessage();
    		return $this->format();
    	}
	}

	// 获取微信沙箱商户config
    public function get_sandbox_app()
    {
        $cert_path = public_path() . '/upload/images/158952122021608105468.pem';

        $config = [
            'app_id' => 'wx0170de6f53f2ab17',
            'mch_id' => '1604565281',
            'key' => 'yunshouyi88yunshouyi88yunshouyi8',
            'cert_path' => $cert_path, // XXX: 绝对路径！！！！
            'key_path' => public_path() . '/upload/images/158952122021608105477.pem', // XXX: 绝对路径！！！！
            'sub_mch_id' => '1604951125',
            'sandbox' => true, // 设置为 false 或注释则关闭沙箱模式
            'spbill_create_ip' => '139.196.106.13',
        ];

        $app = Factory::payment($config);

        return $app;
    }

    /**
     * 微信接口验收测试
     */
    public function wechat_sandbox_pay()
    {
        $app = $this->get_sandbox_app();

        $notify_url = url('api/testluyu/qr_pay_notify');

        $attributes = [
            'trade_type' => 'JSAPI', // JSAPI，NATIVE，APP...
            'body' => '充值中心-会员充值',
            'out_trade_no' => '2021020222001444331434668411',
            'total_fee' => 5.52 * 100,
            'notify_url' => $notify_url, // 支付结果通知网址，如果不设置则会使用配置里的默认地址
        ];
        $attributes['openid'] = 'oxHuEuE8ql4Fzo51u0pndzAKfsZs';

        $result = $app->order->unify($attributes);

        return $this->responseDataJson(200, $result);
    }

    // 查询订单
    public function wechat_sandbox_order_query()
    {
        $app = $this->get_sandbox_app();

        $res = $app->order->queryByOutTradeNumber('2021020222001444331434668411');

        return $this->responseDataJson(200, $res);
    }

    // 退款
    public function wechat_sandbox_refund()
    {
        $app = $this->get_sandbox_app();

        $number = '2021020222001444331434668411';
        $refundNumber = '2021020222001444331434668412';
        $totalFee = 552;
        $refundFee = 552;

        // 参数分别为：商户订单号、商户退款单号、订单金额、退款金额、其他参数
        $res = $app->refund->byOutTradeNumber($number, $refundNumber, $totalFee, $refundFee, $config = []);

        return $this->responseDataJson(200, $res);
    }

    // 退款查询
    public function wechat_sandbox_query_refund()
    {
        $app = $this->get_sandbox_app();

        $number = '2021020222001444331434668411';

        $res = $app->refund->queryByOutTradeNumber($number);

        return $this->responseDataJson(200, $res);
    }

    // 对账单下载
    public function wechat_sandbox_bill()
    {
        $app = $this->get_sandbox_app();

        $res = $bill = $app->bill->get('20210410');

        return $this->responseDataJson(200, $res);
    }

    // 获取沙箱key
    public function get_sign_key()
    {
        // $mch_id = '1604565281'; // 商户号
        // $key = 'rKVuqAv2zlum1JQkfR7OSeRHc1Bg7poD'; // 商户支付密钥
        $nonce_str = strtoupper(md5('123456789')); // 随机字符串
        // var_dump($nonce_str);die;
        //
        // // 开始生成sign
        // $str = "mch_id=".$mch_id."&nonce_str=".$nonce_str."&key=".$key;
        // $sign = strtoupper(md5($str));
        // var_dump($sign);die;

        $xml = "<xml>
                 <nonce_str><![CDATA[25F9E794323B453885F5181F1B624D0B]]></nonce_str>
                 <mch_id><![CDATA[1604565281]]></mch_id>
                 <sign>B9D5F6392A7C70DD0C1AA374E31333E8</sign>
                </xml>";
        $url = "https://api.mch.weixin.qq.com/sandboxnew/pay/getsignkey";

        $result = $this->curl_post_xml($xml,$url);
        var_dump($result);


        $sandbox_signkey = '61bfd136202572fd357daaff73b72715';
    }

    public function curl_post_xml($xmlData,$url)
    {
        $ch = curl_init();  // 初始一个curl会话
        $timeout = 30;  // php运行超时时间，单位秒
        curl_setopt($ch, CURLOPT_URL, $url);    // 设置url
        curl_setopt($ch, CURLOPT_POST, 1);  // post 请求
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:text/xml; charset=utf-8"));    // 一定要定义content-type为xml，要不然默认是text/html！
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlData);//post提交的数据包
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3); // PHP脚本在成功连接服务器前等待多久，单位秒
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $result = curl_exec($ch);   // 抓取URL并把它传递给浏览器
        // 是否报错
        if(curl_errno($ch))
        {
            print curl_error($ch);
        }

        curl_close($ch);    // //关闭cURL资源，并且释放系统资源

        echo $result;
    }

    public function test_ly(Request $request)
    {
        $requestData = $request->all();

        // 参数校验
        $check_data = [
            'order_type'  => '订单类型参数',
            'terminal_type'  => 'terminal_type',
        ];

        $check_data['value_a'] = "参数A";


        $check = $this->check_required($requestData, $check_data);
        if ($check) {
            return $this->responseDataJson(400001, $check);
        }

    }

}