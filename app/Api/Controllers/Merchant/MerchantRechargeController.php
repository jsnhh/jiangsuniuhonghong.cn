<?php

namespace App\Api\Controllers\Merchant;


use App\Api\Controllers\BaseController;
use App\Api\Controllers\Config\EasyPayConfigController;
use App\Common\PaySuccessAction;
use App\Models\EasypayStore;
use App\Models\EasypayStoresImages;
use App\Models\MerchantConsumerDetails;
use App\Models\MerchantRecharge;
use App\Models\Order;
use App\Models\Store;
use App\Models\StorePayWay;
use App\Models\Stores;
use EasyWeChat\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;


class MerchantRechargeController extends BaseController
{
    //private $config_id = '1234';
    private $appid = '';
    private $mch_id = '';
    private $notify_url = '/api/merchant/win_pay_notify';
    private $key = '';
    private $payUrl = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
    private $store_id = '';
    public $jsapiUrl = 'https://platform.eycard.cn:8443/standard/jsapi';//jsapi支付地址 生产线

    //客户在线充值
    public function onlinePayment(Request $request)
    {
        try {
            $merchant_id = $request->get('merchant_id', "");
            $amount = $request->get('amount', "");
            $payMods = $request->get('payMods', "");
            $total = round($amount * 100); // 将元转成分
            $out_trade_no = date('YmdHis') . mt_rand(1000, 9999) . sprintf('%03d', rand(0, 999));
            $pay_type = $request->get('pay_type', "");
            if ($pay_type == '2' || $pay_type == '3') {
                $MerchantConsumerDetails = MerchantConsumerDetails::where('merchant_id', $merchant_id)
                    ->whereIn('type', [1, 2])
                    ->orderBy('created_at', 'desc')
                    ->first();//获取最新一条数据
                if (!empty($MerchantConsumerDetails)) {
                    if ($MerchantConsumerDetails->avail_amount < 0.00) {
                        return json_encode([
                            'status' => "2",
                            'message' => "支付失败，请先选择按流量充值还清欠款金额，欠款金额为 " . $MerchantConsumerDetails->avail_amount,
                        ]);
                    }
                }

            }
            $store = Store::where('merchant_id', $merchant_id)
                ->select('user_id')
                ->first();
            if ($payMods == 'wxApp') {
                $body = '下单支付';
                $res = $this->wxpay($total, $out_trade_no, $body);
                Log::info($res);
                if ($res['return_code'] == 'FAIL') {
                    return json_encode([
                        'status' => "2",
                        'message' => "支付失败，请联系管理员",
                    ]);
                }
                $TransactionDeductionController = new TransactionDeductionController();
                $pay_date = date("Y-m-d H:i:s");
                $MerchantRecharge = MerchantRecharge::where('merchant_id', $merchant_id)
                    ->whereIn('pay_type', [2, 3])
                    ->where('status', '1')
                    ->orderBy('created_at', 'desc')
                    ->first();//获取最新一条数据
                $end_time = date("Y-m-d H:i:s");
                if (!empty($MerchantRecharge)) {
                    $end_date = $MerchantRecharge->end_time;
                    if (strtotime($end_date) > strtotime($pay_date)) {
                        if ($pay_type == '2') {
                            $end_time = $TransactionDeductionController->getNextMonthCurDayOrLastDay($end_date);
                        }
                        if ($pay_type == '3') {
                            $end_time = $TransactionDeductionController->getQuarterCurDayOrLastDay($end_date);
                        }
                    } else {
                        if ($pay_type == '2') {
                            $end_time = $TransactionDeductionController->getNextMonthCurDayOrLastDay($pay_date);
                        }
                        if ($pay_type == '3') {
                            $end_time = $TransactionDeductionController->getQuarterCurDayOrLastDay($pay_date);
                        }
                    }
                } else {
                    if ($pay_type == '2') {
                        $end_time = $TransactionDeductionController->getNextMonthCurDayOrLastDay($pay_date);
                    }
                    if ($pay_type == '3') {
                        $end_time = $TransactionDeductionController->getQuarterCurDayOrLastDay($pay_date);
                    }
                }

                $data_in = [
                    'merchant_id' => $merchant_id,
                    'user_id' => $store->user_id,
                    'amount' => $amount,
                    'remark' => '商户充值',
                    'order_id' => $out_trade_no,
                    'pay_type' => $pay_type,
                    'end_time' => $end_time,
                    'pay_way' =>'weixin'
                ];

                MerchantRecharge::create($data_in);//添加商户充值表
                $data['orderInfo'] = $res['orderInfo'];
                $data['status'] = "1";
                $data['payMods'] = $payMods;
                return json_encode($data);
            }

        } catch
        (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    //客户在线充值易生
    public function onlinePaymentEasy(Request $request)
    {
        try {
            $merchant_id = $request->get('merchant_id', "");
            $amount = $request->get('amount', "");
            $payMods = $request->get('payMods', "");
            $openid = $request->get('openid', "");
            $pay_type = $request->get('pay_type', "");
            $config = new EasyPayConfigController();
            $easyPayConfig = $config->easypay_config('1234');
            if (!$easyPayConfig) {
                return json_encode([
                    'status' => '2',
                    'message' => '易生支付配置不存在请检查配置'
                ]);
            }

            $easyPayMerchant = EasypayStore::where('store_id', $this->store_id)->first();
            if (!$easyPayMerchant) {
                return json_encode([
                    'status' => '2',
                    'message' => '易生支付商户号不存在'
                ]);
            }
            $out_trade_no = substr($easyPayConfig->channel_id, 0, 4) . substr($easyPayConfig->channel_id, -4) . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%02d', rand(0, 99));
            if ($pay_type == '2' || $pay_type == '3') {
                $MerchantConsumerDetails = MerchantConsumerDetails::where('merchant_id', $merchant_id)
                    ->whereIn('type', [1, 2])
                    ->orderBy('created_at', 'desc')
                    ->first();//获取最新一条数据
                if (!empty($MerchantConsumerDetails)) {
                    if ($MerchantConsumerDetails->avail_amount < 0.00) {
                        return json_encode([
                            'status' => "2",
                            'message' => "支付失败，请先选择按流量充值还清欠款金额，欠款金额为 " . $MerchantConsumerDetails->avail_amount,
                        ]);
                    }
                }

            }
            $store = Store::where('merchant_id', $merchant_id)
                ->select('user_id')
                ->first();
            if ($payMods == 'wx_applet') {
                $data = [
                    'pay_type' => $payMods,
                    'total_amount' => $amount,
                    'code' => $openid,
                    'channel_id' => $easyPayConfig->channel_id,
                    'term_mercode' => $easyPayMerchant->term_mercode,
                    'term_termcode' => $easyPayMerchant->term_termcode,
                    'out_trade_no' => $out_trade_no
                ];
                $res = $this->easyPay($data);
                Log::info("res---".json_encode($res));
                if ($res['status'] == '2') {
                    return json_encode([
                        'status' => "2",
                        'message' => "支付失败，请联系管理员",
                    ]);
                }
                $TransactionDeductionController = new TransactionDeductionController();
                $pay_date = date("Y-m-d H:i:s");
                $MerchantRecharge = MerchantRecharge::where('merchant_id', $merchant_id)
                    ->whereIn('pay_type', [2, 3])
                    ->where('status', '1')
                    ->orderBy('created_at', 'desc')
                    ->first();//获取最新一条数据
                $end_time = date("Y-m-d H:i:s");
                if (!empty($MerchantRecharge)) {
                    $end_date = $MerchantRecharge->end_time;
                    if (strtotime($end_date) > strtotime($pay_date)) {
                        if ($pay_type == '2') {
                            $end_time = $TransactionDeductionController->getNextMonthCurDayOrLastDay($end_date);
                        }
                        if ($pay_type == '3') {
                            $end_time = $TransactionDeductionController->getQuarterCurDayOrLastDay($end_date);
                        }
                    } else {
                        if ($pay_type == '2') {
                            $end_time = $TransactionDeductionController->getNextMonthCurDayOrLastDay($pay_date);
                        }
                        if ($pay_type == '3') {
                            $end_time = $TransactionDeductionController->getQuarterCurDayOrLastDay($pay_date);
                        }
                    }
                } else {
                    if ($pay_type == '2') {
                        $end_time = $TransactionDeductionController->getNextMonthCurDayOrLastDay($pay_date);
                    }
                    if ($pay_type == '3') {
                        $end_time = $TransactionDeductionController->getQuarterCurDayOrLastDay($pay_date);
                    }
                }

                $data_in = [
                    'merchant_id' => $merchant_id,
                    'user_id' => $store->user_id,
                    'amount' => $amount,
                    'remark' => '商户充值',
                    'order_id' => $out_trade_no,
                    'pay_type' => $pay_type,
                    'end_time' => $end_time,
                    'pay_way' =>'easy'
                ];

                MerchantRecharge::create($data_in);//添加商户充值表
                $data['orderInfo'] = $res['orderInfo'];
                $data['status'] = "1";
                $data['payMods'] = $payMods;
                return json_encode($data);
            }

        } catch
        (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    public function win_pay_notify(Request $request)
    {
        try {
            $data = $request->getContent();
            Log::info('微信充值-支付回调');
            Log::info($data);
            $jsonxml = json_encode(simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA));
            $result = json_decode($jsonxml, true);//转成数组，
            //Log::info(json_encode($result));
            if ($result) {
                //如果成功返回了
                $out_trade_no = $result['out_trade_no'];
                if ($result['return_code'] == 'SUCCESS' && $result['result_code'] == 'SUCCESS') {
                    $MerchantOrder = MerchantRecharge::where('order_id', $out_trade_no)->first();//获取最新一条数据
                    if (empty($MerchantOrder)) {
                        return false;
                    }
                    if ($MerchantOrder->status == '1') {
                        return false;
                    }
                    $merchant_id = $MerchantOrder->merchant_id;
                    $user_id = $MerchantOrder->user_id;
                    $amount = $MerchantOrder->amount;
                    $pay_type = $MerchantOrder->pay_type;
                    $MerchantConsumerDetails = MerchantConsumerDetails::where('merchant_id', $merchant_id)
                        ->whereIn('type', [1, 2])
                        ->orderBy('created_at', 'desc')
                        ->first();//获取最新一条数据

                    $now_avail_amount = 0;
                    if ($MerchantConsumerDetails) {

                        $now_avail_amount = $MerchantConsumerDetails->avail_amount; //当前可用金额

                        if ($pay_type == '2' || $pay_type == '3') {
                            $avail_amount = 0.00;
                        } else {
                            $avail_amount = $MerchantConsumerDetails->avail_amount + $amount;
                        }
                    } else {
                        if ($pay_type == '2' || $pay_type == '3') {
                            $avail_amount = 0.00;
                        } else {
                            $avail_amount = $amount;
                        }

                    }
                    $data_in = [
                        'merchant_id' => $merchant_id,
                        'user_id' => $user_id,
                        'amount' => $amount,
                        'avail_amount' => $avail_amount,
                        'type' => '1',
                        'remark' => '商户充值'
                    ];

                    MerchantRecharge::where('order_id', $out_trade_no)->update(['status' => '1']);
                    MerchantConsumerDetails::create($data_in);//添加到商户消费明细表
                    //月包季包直接分润到代理
                    if ($pay_type == '2' || $pay_type == '3') {
                        $TransactionDeductionController = new TransactionDeductionController();
                        $TransactionDeductionController->userProfit($user_id, $amount, $merchant_id, $amount, 0);

                    }else if($pay_type == '1' && $now_avail_amount < 0){ //流量充值，如果存在负数金额，负数金额进行分润操作

                        Log::info('流量充值，如果存在负数金额，负数金额进行分润操作,now_avail_amount:' . $now_avail_amount);
                        $avail_amount = abs($now_avail_amount) ; //转为正数
                        Log::info('---------换算正数，total_amount ---->' . $avail_amount );

                        if($amount < $avail_amount){ //充值金额小于欠费金额
                            $avail_amount = $amount;
                        }

                        $Store = Stores::where('merchant_id', $merchant_id)
                            ->where('user_id', $user_id)
                            ->select('store_id')
                            ->first();

                        $store_id = $Store->store_id;

                        $storePayWay = StorePayWay::where('store_id', $store_id)
                            ->where('status', 1)
                            ->where('company', 'ccbankpay')
                            ->select('rate')
                            ->first();
                        if ($storePayWay) {
                            $TransactionDeductionController = new TransactionDeductionController();

                            //支付宝微信扫码费率
                            $user_rate = $storePayWay->rate;

                            $deductionData = [
                                'merchant_id' => $merchant_id,
                                'total_amount' => $avail_amount, // 乘以 -1 换算正数
                                'user_id' => $user_id,
                                'order_id' => $out_trade_no,
                                'company' => 'ccbankpay', //建行通道
                                'rate' => $user_rate, //结算费率  0.1
                            ];

                            Log::info('------存在负数金额，换算正数,操作金额，avail_amount ---->' . $avail_amount );


                            $TransactionDeductionController->payUserRateDetail($deductionData);
                        }

                    }
                    $params = [
                        'return_code' => 'SUCCESS',
                        'return_msg' => 'OK'
                    ];
                    return $this->array_to_xml($params);
                } else {
                    MerchantRecharge::where('order_id', $out_trade_no)->update(['status' => '0']);
                    $params = [
                        'return_code' => 'FAIL',
                        'return_msg' => 'err'
                    ];
                    return $this->array_to_xml($params);
                }
            }
            $params = [
                'return_code' => 'SUCCESS',
                'return_msg' => 'OK'
            ];
            return $this->array_to_xml($params);
        } catch (\Exception $ex) {
            Log::info('微信支付异步');
            Log::info($ex->getMessage() . ' | ' . $ex->getLine());
        }
    }

    //easy 支付回调
    public function easy_pay_notify(Request $request)
    {
        try {
            $data = $request->all();
            Log::info('微信充值-支付回调');
            Log::info($data);

            //Log::info(json_encode($result));
            if (isset($data) && !empty($data)) {
                if (isset($data['data']['oriOrgTrace'])) {
                    $out_trade_no = $data['data']['oriOrgTrace']; //商户订单号(原交易流水)
                    $MerchantOrder = MerchantRecharge::where('order_id', $out_trade_no)->first();//获取最新一条数据
                    if (empty($MerchantOrder)) {
                        return false;
                    }
                    if ($MerchantOrder->status == '1') {
                        return false;
                    }
                    $merchant_id = $MerchantOrder->merchant_id;
                    $user_id = $MerchantOrder->user_id;
                    $amount = $MerchantOrder->amount;
                    $pay_type = $MerchantOrder->pay_type;
                    $MerchantConsumerDetails = MerchantConsumerDetails::where('merchant_id', $merchant_id)
                        ->whereIn('type', [1, 2])
                        ->orderBy('created_at', 'desc')
                        ->first();//获取最新一条数据

                    $now_avail_amount = 0;
                    if ($MerchantConsumerDetails) {

                        $now_avail_amount = $MerchantConsumerDetails->avail_amount; //当前可用金额

                        if ($pay_type == '2' || $pay_type == '3') {
                            $avail_amount = $MerchantConsumerDetails->avail_amount;
                        } else {
                            $avail_amount = $MerchantConsumerDetails->avail_amount + $amount;
                        }
                    } else {
                        if ($pay_type == '2' || $pay_type == '3') {
                            $avail_amount = 0.00;
                        } else {
                            $avail_amount = $amount;
                        }

                    }
                    $data_in = [
                        'merchant_id' => $merchant_id,
                        'user_id' => $user_id,
                        'amount' => $amount,
                        'avail_amount' => $avail_amount,
                        'type' => '1',
                        'remark' => '商户充值'
                    ];

                    MerchantRecharge::where('order_id', $out_trade_no)->update(['status' => '1']);
                    MerchantConsumerDetails::create($data_in);//添加到商户消费明细表
                    //月包季包直接分润到代理
                    if ($pay_type == '2' || $pay_type == '3') {
                        $TransactionDeductionController = new TransactionDeductionController();
                        $TransactionDeductionController->userProfit($user_id, $amount, $merchant_id, $amount, 0);

                    }else if($pay_type == '1' && $now_avail_amount < 0){ //流量充值，如果存在负数金额，负数金额进行分润操作

                        Log::info('easy_pay_notify--流量充值，如果存在负数金额，负数金额进行分润操作,now_avail_amount:' . $now_avail_amount);

                        $avail_amount = abs($now_avail_amount) ; //转为正数
                        Log::info('easy_pay_notify---------换算正数，total_amount ---->' . $avail_amount );

                        if($amount < $avail_amount){ //充值金额小于欠费金额
                            $avail_amount = $amount;
                        }

                        $Store = Stores::where('merchant_id', $merchant_id)
                            ->where('user_id', $user_id)
                            ->select('store_id')
                            ->first();

                        $store_id = $Store->store_id;

                        $storePayWay = StorePayWay::where('store_id', $store_id)
                            ->where('status', 1)
                            ->where('company', 'ccbankpay')
                            ->select('rate')
                            ->first();

                        if ($storePayWay) {
                            $TransactionDeductionController = new TransactionDeductionController();

                            //支付宝微信扫码费率
                            $user_rate = $storePayWay->rate;

                            $deductionData = [
                                'merchant_id' => $merchant_id,
                                'total_amount' => $avail_amount,
                                'user_id' => $user_id,
                                'order_id' => $out_trade_no,
                                'company' => 'ccbankpay', //建行通道
                                'rate' => $user_rate, //结算费率
                            ];

                            Log::info('easy_pay_notify------存在负数金额，换算正数，avail_amount ---->' . $avail_amount );

                            $TransactionDeductionController->payUserRateDetail($deductionData);
                        }

                    }
                    return json_encode([
                        'resultcode' => '00'
                    ]);
                }
            }
        } catch (\Exception $ex) {
            Log::info('商户充值-易生支付回调-异步报错');
            Log::info($ex->getMessage() . ' | ' . $ex->getLine());
        }
    }
    //easy 支付查询
    public function query_easy($data)
    {
        try {
            if (isset($data) && !empty($data)) {
                if (isset($data['oriOrgTrace'])) {
                    $out_trade_no =$data['oriOrgTrace']; //商户订单号(原交易流水)
                    $MerchantOrder = MerchantRecharge::where('order_id', $out_trade_no)->first();//获取最新一条数据
                    if (empty($MerchantOrder)) {
                        return false;
                    }
                    if ($MerchantOrder->status == '1') {
                        return false;
                    }
                    $merchant_id = $MerchantOrder->merchant_id;
                    $user_id = $MerchantOrder->user_id;
                    $amount = $MerchantOrder->amount;
                    $pay_type = $MerchantOrder->pay_type;
                    $MerchantConsumerDetails = MerchantConsumerDetails::where('merchant_id', $merchant_id)
                        ->whereIn('type', [1, 2])
                        ->orderBy('created_at', 'desc')
                        ->first();//获取最新一条数据

                    $now_avail_amount = 0;
                    if ($MerchantConsumerDetails) {

                        $now_avail_amount = $MerchantConsumerDetails->avail_amount; //当前可用金额

                        if ($pay_type == '2' || $pay_type == '3') {
                            $avail_amount = $MerchantConsumerDetails->avail_amount;
                        } else {
                            $avail_amount = $MerchantConsumerDetails->avail_amount + $amount;
                        }
                    } else {
                        if ($pay_type == '2' || $pay_type == '3') {
                            $avail_amount = 0.00;
                        } else {
                            $avail_amount = $amount;
                        }

                    }
                    $data_in = [
                        'merchant_id' => $merchant_id,
                        'user_id' => $user_id,
                        'amount' => $amount,
                        'avail_amount' => $avail_amount,
                        'type' => '1',
                        'remark' => '商户充值'
                    ];

                    MerchantRecharge::where('order_id', $out_trade_no)->update(['status' => '1']);
                    MerchantConsumerDetails::create($data_in);//添加到商户消费明细表
                    //月包季包直接分润到代理
                    if ($pay_type == '2' || $pay_type == '3') {
                        $TransactionDeductionController = new TransactionDeductionController();
                        $TransactionDeductionController->userProfit($user_id, $amount, $merchant_id, 0, 0);

                    } else if ($pay_type == '1' && $now_avail_amount < 0) { //流量充值，如果存在负数金额，负数金额进行分润操作

                        Log::info('easy_pay_notify--流量充值，如果存在负数金额，负数金额进行分润操作,now_avail_amount:' . $now_avail_amount);

                        $avail_amount = abs($now_avail_amount); //转为正数
                        Log::info('easy_pay_notify---------换算正数，total_amount ---->' . $avail_amount);

                        $StorePayWay = Stores::where('merchant_id', $merchant_id)
                            ->where('user_id', $user_id)
                            ->select('store_id')
                            ->first();

                        $store_id = $StorePayWay->store_id;

                        $storePayWay = StorePayWay::where('store_id', $store_id)
                            ->where('status', 1)
                            ->select('rate')
                            ->first();
                        if ($storePayWay) {
                            $TransactionDeductionController = new TransactionDeductionController();

                            //支付宝微信扫码费率
                            $user_rate = $storePayWay->rate;

                            $deductionData = [
                                'merchant_id' => $merchant_id,
                                'total_amount' => $avail_amount,
                                'user_id' => $user_id,
                                'order_id' => $out_trade_no,
                                'company' => 'ccbankpay', //建行通道
                                //'rate' => $user_rate, //结算费率
                                'rate' => 100 //结算费率
                            ];

                            Log::info('easy_pay_notify------存在负数金额，换算正数，avail_amount ---->' . $avail_amount);

                            $TransactionDeductionController->userRateDetail($deductionData);
                        }

                    }
                    return json_encode([
                        'resultcode' => '00'
                    ]);
                }
            }
        } catch (\Exception $ex) {
            Log::info('商户充值-易生支付回调-异步报错');
            Log::info($ex->getMessage() . ' | ' . $ex->getLine());
        }
    }
    /*
      * 微信支付统一下单
      */
    public function wxpay($total_fee, $out_trade_no, $body)
    {

        $xml = $this->arrayToXml($this->getOptions($total_fee, $out_trade_no, $body));
        // 微信支付post提交
        $data = $this->postXmlCurl($xml, $this->payUrl);
        // 把xml转成array
        $array_data = json_decode(json_encode(simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA)), true);

        if ($array_data['return_code'] == 'SUCCESS') {
            $conf = [
                'appid' => $this->appid,
                'partnerid' => $this->mch_id,
                'prepayid' => $array_data['prepay_id'],
                'package' => 'Sign=WXPay',
                'noncestr' => md5('app' . time()),
                'timestamp' => time(),
            ];
            $conf['sign'] = strtoupper(MD5('appid=' . $conf['appid'] . '&noncestr=' . $conf['noncestr'] . '&package=Sign=WXPay&partnerid=' . $conf['partnerid'] . '&prepayid=' . $conf['prepayid'] . '&timestamp=' . $conf['timestamp'] . '&key=' . $this->key));
            $array_data['return_code'] = 'SUCCESS';
            $array_data['orderInfo'] = $conf;
            return $array_data;
        } else {
            return $array_data;
        }
    }

    /*
     * 商户充值easy
     */
    public function easyPay($data)
    {

        $store = Store::where('store_id', $this->store_id)->first();
        $location = '';
        if (!$store->lat || !$store->lng) {
            $storeController = new \App\Api\Controllers\User\StoreController();
            $address = $store->province_name . $store->city_name . $store->area_name . $store->store_address;//获取经纬度的地址
            $api_res = $storeController->query_address($address, env('LBS_KEY'));
            if ($api_res['status'] == '1') {
                $store_lng = $api_res['result']['lng'];
                $store_lat = $api_res['result']['lat'];
                $store->update([
                    'lng' => $store_lng,
                    'lat' => $store_lat,
                ]);
                $location = "+" . $store_lat . "/-" . $store_lng;
            } else {
//                            Log::info('添加门店-获取经纬度错误');
//                            Log::info($api_res['message']);
            }

        } else {
            $location = "+" . $store->lat . "/-" . $store->lng;
        }
        //获取到账标识
        $EasyPayStoresImages = EasypayStoresImages::where('store_id', $this->store_id)
            ->select('real_time_entry')
            ->first();
        $patnerSettleFlag = '0'; //秒到
        if ($EasyPayStoresImages) {
            if ($EasyPayStoresImages->real_time_entry == 1) {
                $patnerSettleFlag = '0';
            } else {
                $patnerSettleFlag = '1';
            }
        }
        $easypay_data = [
            'channel_id' => $data['channel_id'],
            'mno' => $data['term_mercode'],
            'device_id' => $data['term_termcode'],
            'out_trade_no' => $data['out_trade_no'],
            'pay_type' => $data['pay_type'],
            'total_amount' => $data['total_amount'],
            'code' => $data['code'],
            'notify_url' => url('/api/merchant/easy_pay_notify'),
            'location' => $location,
            'patnerSettleFlag' => $patnerSettleFlag,
            'subAppid' => $this->min_appid,
            'store_name' => $store->store_name
        ];
        $return = $this->qr_submit($easypay_data);
        if ($return['status'] == '1') {
            if ($data['pay_type'] == 'wx_applet') {
                $weChatData = json_decode($return['data']['wxWcPayData'], true);
                if ($weChatData) {
                    if (isset($weChatData['timeStamp'])) $return['data']['payTimeStamp'] = $weChatData['timeStamp'];
                    if (isset($weChatData['package'])) $return['data']['payPackage'] = $weChatData['package'];
                    if (isset($weChatData['paySign'])) $return['data']['paySign'] = $weChatData['paySign'];
                    if (isset($weChatData['signType'])) $return['data']['paySignType'] = $weChatData['signType'];
                    if (isset($weChatData['nonceStr'])) $return['data']['paynonceStr'] = $weChatData['nonceStr'];

                    return [
                        'status' => '1',
                        'orderInfo' => $weChatData,
                    ];
                }
            }
        }
        if ($return['status'] == '0') {
            return [
                'status' => '2',
                'message' => '支付失败',

            ];
        }
    }

    /*
     * 设置统一下单参数
     * $total_fee  总金额
     * $out_trade_no  订单号
     * $body  商品描述
     */
    public function getOptions($total_fee, $out_trade_no, $body)
    {
        $conf = [
            'appid' => $this->appid,
            'mch_id' => $this->mch_id,
            'nonce_str' => md5('app' . time()),
            'trade_type' => 'APP',
            'notify_url' => url($this->notify_url),
            'spbill_create_ip' => $_SERVER['REMOTE_ADDR'],
            'total_fee' => $total_fee,
            'out_trade_no' => $out_trade_no,
            'body' => $body,
        ];

        $conf['sign'] = $this->getSign($conf);

        return $conf;
    }


    /**
     *  作用：生成签名
     */
    private function getSign($Parameters)
    {
        //签名步骤一：按字典序排序参数
        ksort($Parameters);
        $String = $this->formatBizQueryParaMap($Parameters, false);
        //echo '【string1】'.$String.'</br>';
        //签名步骤二：在string后加入KEY
        $String = $String . "&key=" . $this->key;
        //echo "【string2】".$String."</br>";
        //签名步骤三：MD5加密
        $String = md5($String);
        //echo "【string3】 ".$String."</br>";
        //签名步骤四：所有字符转为大写
        $result_ = strtoupper($String);
        //echo "【result】 ".$result_."</br>";
        return $result_;
    }

    /**
     *  作用：格式化参数，签名过程拼接字符串需要使用
     */
    private function formatBizQueryParaMap($paraMap, $urlencode)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if ($urlencode) {
                $v = urlencode($v);
            }
            $buff .= $k . "=" . $v . "&";
        }
        $reqPar = '';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }

    /**
     *  作用：array转xml
     */
    private function arrayToXml($arr)
    {
        $xml = "<xml>";
        foreach ($arr as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";
        return $xml;
    }

    /**
     *  作用：以post方式提交xml到对应的接口url
     */
    private function postXmlCurl($xml, $url, $second = 30)
    {
        //初始化curl
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        //这里设置代理，如果有的话
        //curl_setopt($ch,CURLOPT_PROXY, '8.8.8.8');
        //curl_setopt($ch,CURLOPT_PROXYPORT, 8080);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        //设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //post提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        //运行curl
        $data = curl_exec($ch);
        curl_close($ch);
        //返回结果
        if ($data) {
            return $data;
        } else {
            $error = curl_errno($ch);
            curl_close($ch);
            return false;
        }
    }

    public function array_to_xml($params)
    {
        if (!is_array($params) || count($params) <= 0) {
            return false;
        }
        $xml = "<xml>";
        foreach ($params as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";
        return $xml;
    }

    public function qr_submit($data)
    {
        try {
            $channel_id = $data['channel_id'] ?? '';
            $mer_id = $data['mno'] ?? '';
            $term_id = $data['device_id'] ?? '';
            $out_trade_no = $data['out_trade_no'] ?? '';
            $pay_type = $data['pay_type'] ?? '';
            $total_amount = $data['total_amount'] ?? '';
            $trade_amt = isset($total_amount) ? intval($total_amount * 100) : '';
            $code = $data['code'] ?? '';
            $notify_url = $data['notify_url'] ?? '';
            $patnerSettleFlag = $data['patnerSettleFlag'] ?? '';
            $subAppid = $data['subAppid'];
            if ($pay_type == 'ALIPAY') {
                $opt = 'WAJS1';
            } else if ($pay_type == 'WECHAT') {
                $opt = 'WTJS1';
            } else if ($pay_type == 'UNIONPAY') {
                $opt = 'WUJS1';
            } else if ($pay_type == 'wx_applet') {
                $opt = 'WTJS2';
            }
            $terminalinfo = [
                'location' => $data['location'],
                'terminalip' => '154.8.143.104'
            ];
            $appendData = [
                'terminalinfo' => $terminalinfo
            ];
            $data = [
                'tradeCode' => $opt,
                'tradeAmt' => $trade_amt,
                'orderInfo' => 'qrPay',
                'orgBackUrl' => $notify_url,
                'wxSubAppid' => $subAppid,
                'payerId' => $code,
                'patnerSettleFlag' => $patnerSettleFlag,
                'delaySettleFlag' => $patnerSettleFlag,
                'splitSettleFlag' => '0'//分账使用0=不分账1=分账
            ];
            if ($opt == 'WUJS1') {
                $data['orgFrontUrl'] = url('/api/easypay/pay_notify_url_u');
            }
            $BaseController = new \App\Api\Controllers\EasyPay\BaseController();
            $sign = $BaseController->getSign($data, $BaseController->key);
            $easypay_data = [
                'orgId' => $channel_id,
                'orgMercode' => $mer_id,
                'orgTermno' => $term_id,
                'orgTrace' => $out_trade_no,
                'sign' => $sign,
                'signType' => 'RSA2',
                'appendData' => $appendData,
                'data' => $data
            ];
            Log::info('商户充值-easy-params：');
            Log::info($easypay_data);

            $res_obj = $BaseController->payment->request($easypay_data, $this->jsapiUrl);
            Log::info('商户充值-easy-res：');
            Log::info($res_obj);

            //系统错误
            if (!$res_obj) {
                return [
                    'status' => '-1',
                    'message' => '系统错误',
                ];
            }

            $res_arr = json_decode($res_obj, true);

            //成功
            if ($res_arr['sysRetcode'] != '000000') {
                return [
                    'status' => '0',
                    'message' => $res_arr['sysRetmsg']
                ];
            } else {
                if ($res_arr['data']['finRetcode'] == '00') {
                    return [
                        'status' => '1',
                        'message' => '交易成功',
                        'data' => $res_arr['data'],
                    ];
                } elseif ($res_arr['data']['finRetcode'] == '99') {
                    return [
                        'status' => '1',
                        'message' => '待支付',
                        'data' => $res_arr['data'],
                    ];
                } else {
                    return [
                        'status' => '0',
                        'message' => '支付失败',
                        'data' => $res_arr['data'],
                    ];
                }

            }
        } catch (\Exception $ex) {
            Log::info('易生支付-静态码提交-错误');
            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine(),
            ];
        }
    }
}
