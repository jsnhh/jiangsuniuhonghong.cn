<?php


namespace App\Api\Controllers\Merchant;


use App\Api\Controllers\BaseController;
use App\Api\Controllers\Config\WeixinConfigController;
use App\Models\Goods;
use App\Models\Order;
use App\Models\ShoppingGoods;
use App\Models\ShoppingOpenId;
use App\Models\ShoppingOrder;
use App\Models\Store;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

class TemplateMessageController extends BaseController
{
    public $getTokeUrl = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential';//获取token
    public $template_id = 'bCNRvYqBu-ndI5MofIgPyL-qXk-XPsKRVA5B6j4shIs';
    public $templateUrl = 'https://api.weixin.qq.com/cgi-bin/message/template/send';
    public $shopping_template_id = 'j2qdHEYoojORsA16hoh8tAxSiabjpasy3vA4Rf5RITM';

    public function get_token($store_id)
    {
        $access_token = Cache::get('access_token');//获取缓存  存在直接返回
        if ($access_token) {
            return [
                'access_token' => $access_token
            ];
        }

        $config = new WeixinConfigController();
        $weiXinConfig = $config->weixin_config_obj($store_id);
        if (!$weiXinConfig) {
            $message = "微信配置不存在~";
            return view('errors.page_errors', compact('message'));
        };
        $url = $this->getTokeUrl . '&appid=' . $weiXinConfig->app_id . '&secret=' . $weiXinConfig->app_secret;
        $res = $this->request_get($url);
        Log::info($res);
        Cache::put('access_token', $res['access_token'], 110);//将access_token存入缓存 有效时间为7200秒 设置提前10秒失效
        return $res;
    }

    public function template_message($data)
    {
        try {
            $store_id = $data['store_id'];
            $total_amount = $data['total_amount'];
            $store_name = $data['store_name'];
            $ways_type_desc = $data['ways_type_desc'];
            $out_trade_no = $data['out_trade_no'];
            $pay_time = $data['pay_time'];
            // $store_id=$request->get('store_id', '');
            // $total_amount=$request->get('total_amount', '');
            // $store_name=$request->get('store_name', '');
            // $ways_type_desc=$request->get('ways_type_desc', '');
            // $out_trade_no=$request->get('out_trade_no', '');
            // $pay_time=$request->get('pay_time', '');
            $Order = Order::where('out_trade_no', $data['out_trade_no'])->where('push_type', '2')->select('out_trade_no')->first();
            if ($Order) {
                return;
            }
            Order::where('out_trade_no', $data['out_trade_no'])->update(['push_type' => '2']);
            $store = Store::where('store_id', $store_id)->select('openid', 'config_id')->first();
            if ($store->openid) {
                $table = DB::table('orders');
                $order_obj = $table->select([
                    'total_amount',
                    'company',
                    DB::raw("COUNT(`id`) AS `total_commission_num`"),
                    DB::raw("SUM(`total_amount`) AS `transaction_amount`"),
                ])
                    ->where('store_id', $store_id)
                    ->whereIn('pay_status', [1, 6])
                    ->first();
                $total_commission_num = $order_obj->total_commission_num;
                $transaction_amount = $order_obj->transaction_amount;
                $details = [
                    'first' => [
                        'value' => '您有新的交易收款，请注意查收',
                    ],
                    'keyword1' => [
                        'value' => $total_amount
                    ],
                    'keyword2' => [
                        'value' => $store_name
                    ],
                    'keyword3' => [
                        'value' => $ways_type_desc
                    ],
                    'keyword4' => [
                        'value' => $out_trade_no
                    ],
                    'keyword5' => [
                        'value' => $pay_time
                    ],
                    'remark' => [
                        'value' => '当日累计收款笔数' . $total_commission_num . '笔，累计收款金额' . $transaction_amount . '元。',
                        'color' => '#173177'
                    ]
                ];
                $templateData = [
                    'touser' => $store->openid,
                    'template_id' => $this->template_id,
                    'appid' => '',
                    'data' => $details
                ];
                $res = $this->get_token($store->config_id);
                if (isset($res['errcode'])) {
                    Log::info('获取access_token失败');
                    return;
                };
                //Log::info("推送数据-----".json_encode($templateData));
                $url = $this->templateUrl . '?access_token=' . $res['access_token'];
                $response = $this->request($templateData, $url);
                if ($response['errcode'] != 0) {
                    Log::info('推送失败');
                    Log::info(json_encode($response));
                    return;
                }

                Log::info('推送成功');
            }
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine();
            return $this->format();
        }
    }

    //商城订单推送
    public function shopping_template_message($order_id)
    {
        try {
            $shoppingOrder = ShoppingOrder::where('order_id', $order_id)->select('phone', 'user_id', 'goods_id')->first();
            if (!$shoppingOrder) {
                Log::info('商城订单采购-----订单不存在');
                return;
            }
            $user = User::where('id', $shoppingOrder->user_id)->select('name')->first();
            $ShoppingGoods = ShoppingGoods::where('id', $shoppingOrder->goods_id)->select('goods_name')->first();
            if (!$user) {
                Log::info('商城订单采购-----代理不存在');
                return;
            }
            if (!$ShoppingGoods) {
                Log::info('商城订单采购-----商品不存在');
                return;
            }
            $user_name = $user->name;
            $detail = $ShoppingGoods->goods_name;
            $phone = $shoppingOrder->phone;
            $openIds = ShoppingOpenId::select('*')->get();
            if (!$openIds) {
                return;
            }
            foreach ($openIds as $openid) {
                $details = [
                    'thing3' => [
                        'value' => $user_name,
                    ],
                    'character_string10' => [
                        'value' => $order_id
                    ],
                    'thing5' => [
                        'value' => $detail
                    ],
                    'phone_number12' => [
                        'value' => $phone
                    ]
                ];
                $templateData = [
                    'touser' => $openid->open_id,
                    'template_id' => $this->shopping_template_id,
                    'page' => 'index',
                    'data' => $details
                ];
                $res = $this->get_token('1234');
                if (isset($res['errcode'])) {
                    Log::info('获取access_token失败');
                    return;
                };
                $url = $this->templateUrl . '?access_token=' . $res['access_token'];
                $response = $this->request($templateData, $url);
                if ($response['errcode'] != 0) {
                    Log::info('推送失败');
                    Log::info(json_encode($response));
                    return;
                }

                Log::info('商城订单推送成功');
            }

        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine();
            return $this->format();
        }
    }

    public function request($params, $url)
    {
        $data = stripslashes(json_encode($params, JSON_UNESCAPED_UNICODE));//JSON_UNESCAPED_UNICODE 发送数据中文Unicode乱码处理
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); //访问超时时间
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json; charset=UTF-8',
        ]);
        //本地忽略校验证书
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $tmpInfo = curl_exec($curl);
        return json_decode($tmpInfo, true);
    }

    public function request_get($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        //本地忽略校验证书
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $tmpInfo = curl_exec($curl);
        return json_decode($tmpInfo, true);
    }

    public function curl_post($url, $data = '', $timeout = 10)
    {
        if ($url == "" || $timeout <= 0) {
            return false;
        }

        $con = curl_init($url);
        curl_setopt($con, CURLOPT_HEADER, false);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($con, CURLOPT_TIMEOUT, (int)$timeout);
        curl_setopt($con, CURLOPT_SSL_VERIFYPEER, false);//调试去掉ssl验证
        // POST数据
        curl_setopt($con, CURLOPT_POST, true);

        // 把post的变量加上
        curl_setopt($con, CURLOPT_POSTFIELDS, $data);

        return curl_exec($con);
    }
}