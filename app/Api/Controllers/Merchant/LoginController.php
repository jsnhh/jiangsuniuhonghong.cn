<?php
namespace App\Api\Controllers\Merchant;


use App\Api\Controllers\ApiController;
use App\Api\Controllers\BaseController;
use App\Api\Controllers\Push\JpushController;
use App\Models\Device;
use App\Models\Merchant;
use App\Models\MerchantStore;
use App\Models\MqttConfig;
use App\Models\Store;
use App\Models\StoreBank;
use App\Models\StoreImg;
use App\Models\User;
use App\Models\WeixinAppConfig;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use MyBank\Tools;
use Tymon\JWTAuth\Facades\JWTAuth;
use WeixinApp\WXBizDataCrypt;

class LoginController extends BaseController
{
    use AuthenticatesUsers;


    protected function guard()
    {

        return auth()->guard('merchantApi');//检查用户是否是登陆
    }


    public function __construct()
    {
        $this->middleware('guest.merchant', ['except' => 'logout']);
    }


    //app登录
    public function login(Request $request)
    {
        $account = $request->get('account', '');//登录账户 商户号80* 手机号 1*
        $password = $request->get('password', '');

        if ($account == "") {
            return json_encode([
                'status' => -1,
                'message' => '登录账号必填'
            ]);
        }
        if ($password == "") {
            return json_encode([
                'status' => -1,
                'message' => '登录密码必填'
            ]);
        }

        try {
            $str = substr($account, 0, 2);
            if (in_array($str, ['80'])) {
                //商户号登录
                Log::info('-------商户号登录----merchant_no：' . $account );
                $merchant_no = $account;

                $checkMerchant = Merchant::where('merchant_no', $merchant_no)->first();
                if(!$checkMerchant){
                    return json_encode([
                        'status' => -1,
                        'message' => '登录账号不存在'
                    ]);
                }
                //添加一个request字段
                $request->offsetSet('merchant_no', $account); //商户号
            }else{
                Log::info('-------手机号登录----phone：' . $account );
                //手机号登录
                //检查手机号是否存在唯一
                $checkMerchant = Merchant::where('phone', $account)->get();
                if(count($checkMerchant) == 1){
                    //唯一
                    if($checkMerchant[0]->login_num == '1'){ //已登录过
                        return json_encode([
                            'status' => -1,
                            'message' => '请使用商户号进行登录'
                        ]);
                    }else{
                        $merchant_no = $this->generateMerchantNo(); //重新获取
                        $upIN = [
                            'merchant_no' => $merchant_no,
                        ];

                        $checkMerchant[0]->update($upIN);

                        Log::info('-------update---->' . $merchant_no );

                        //添加一个request字段
                        $request->offsetSet('merchant_no', $merchant_no); //商户号
                    }

                }else{
                    //数据为空，或者多个数据
                    return json_encode([
                        'status' => -1,
                        'message' => '登录账号不存在'
                    ]);
                }
            }

            //商户号登录操作

            $this->validateLogin($request);
            if ($this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);

                return $this->sendLockoutResponse($request);
            }

            $credentials = $this->credentials($request);
            if($merchant_no){
                $customClaims = ['merchant_no' => $merchant_no];
            }

            if ($this->guard()->attempt($credentials, $customClaims)) {
                return $this->sendLoginResponse($request, $merchant_no);
            }
            $this->incrementLoginAttempts($request);

            return $this->sendFailedLoginResponse($request);

        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }


    //小程序登录
    public function weixinapp_login(Request $request)
    {
        try {
            $register = 0;
            //1.通过code 拿到 openid  session_key
            $url = 'https://api.weixin.qq.com/sns/jscode2session';
            $appid = $request->get('wx_app_id', 'wx3c51a880e84492d7');
            //微信小程序配置

            $WeixinAppConfig = WeixinAppConfig::where('wx_appid', $appid)->first();
            if (!$WeixinAppConfig) {
                return json_encode([
                    'status' => 2,
                    'message' => '微信小程序未配置',
                ]);
            }
            $secret = $WeixinAppConfig->wx_secret;
            $js_code = $request->get('code');
            $encryptedData = $request->get('encryptedData', '');
            $iv = $request->get('iv', '');
            $url = $url . '?appid=' . $appid . '&secret=' . $secret . '&js_code=' . $js_code . '&grant_type=authorization_code';
            $re = Tools::curl([], $url);
            $re_arr = json_decode($re, true);

            if (isset($re_arr['errcode'])) {
                return json_encode([
                    'status' => 2,
                    'message' => $re_arr['errmsg'],
                ]);
            }
            $open_id = $re_arr['openid'];
            $session_key = $re_arr['session_key'];
            $store_id = "";
            $store_name = "未注册门店";

            //微信登录
            if ($open_id && $iv == "" && $encryptedData == "") {

                $merchant = Merchant::where('wxapp_openid', $open_id)->first();
                if (!$merchant) {
                    //未知
                    return json_encode([
                        'status' => 1,
                        'message' => '数据返回成功',
                        'data' => [
                            'register' => $register,
                            'store_id' => $store_id,
                            'store_name' => $store_name,
                            'token' => '',
                            'wxapp_openid' => $open_id,
                            'name' => '',
                            'pid' => 0,
                        ],
                    ]);

                }
                $phone = $merchant->phone;
                $name = $merchant->name;
                $pid = $merchant->pid;
                $source = $merchant->source;
                $merchant_no = $merchant->merchant_no;

                $register = '2';
                $token = JWTAuth::fromUser($merchant);//根据用户得到token
                $MerchantStore = MerchantStore::where('merchant_id', $merchant->id)
                    ->first();
                if ($MerchantStore) {
                    $Store = Store::where('store_id', $MerchantStore->store_id)
                        ->select('head_sfz_no')
                        ->first();
                    //
                    if ($Store && $Store->head_sfz_no) {
                        $StoreBank = StoreBank::where('store_id', $MerchantStore->store_id)
                            ->select('store_bank_no')
                            ->first();
                        if ($StoreBank && $StoreBank->store_bank_no) {
                            $register = '1';
                        }
                    }
                }

                return json_encode([
                    'status' => 1,
                    'message' => '数据返回成功',
                    'data' => [
                        'register' => $register,
                        'token' => $token,
                        'store_id' => $store_id,
                        'store_name' => $store_name,
                        'wxapp_openid' => $open_id,
                        'phone' => $phone,
                        'name' => $name,
                        'pid' => $pid,
                        'source' => $source,
                        'merchant_no' => $merchant_no,
                    ],
                ]);

            }


            //2.解析数据
            $pc = new WXBizDataCrypt($appid, $session_key);
            $errCode = $pc->decryptData($encryptedData, $iv, $re_data);
            if ($errCode != 0) {
                return json_encode([
                    'status' => 2,
                    'message' => $errCode,
                ]);
            }

            $re_data = json_decode($re_data, true);

            $phone = $re_data['purePhoneNumber'];//没有区号的手机号

            $merchant = Merchant::where('phone', $phone)->first();
            //未注册
            if (!$merchant) {
                return json_encode([
                    'status' => 1,
                    'message' => '数据返回成功',
                    'data' => [
                        'register' => $register,
                        'token' => '',
                        'store_id' => $store_id,
                        'store_name' => $store_name,
                        'wxapp_openid' => $open_id,
                        'phone' => $phone,
                        'name' => $phone,
                        'pid' => 0,
                    ],
                ]);
            }

            //保存微信小程序号
            $merchant->wxapp_openid = $open_id;
            $merchant->save();
            $config_id = $merchant->config_id;
            $name = $merchant->name;
            $pid = $merchant->pid;
            $token = JWTAuth::fromUser($merchant);//根据用户得到token
            //未认证
            $merchant_store = MerchantStore::where('merchant_id', $merchant->id)
                ->select('id', 'store_id')
                ->first();

            if (!$merchant_store) {
                return json_encode([
                    'status' => 1,
                    'message' => '数据返回成功',
                    'data' => [
                        'register' => '2',
                        'token' => $token,
                        'store_id' => $store_id,
                        'store_name' => $store_name,
                        'wxapp_openid' => $open_id,
                        'phone' => $phone,
                        'name' => $name,
                        'pid' => $pid,
                    ],
                ]);
            }
            $store_id = $merchant_store->store_id;
            $store = Store::where('store_id', $store_id)
                ->select('store_short_name', 'head_sfz_no')
                ->first();
            if ($store) {
                $register = '2';

                $store_name = $store->store_short_name;
            }

            if ($store && $store->head_sfz_no) {
                $StoreBank = StoreBank::where('store_id', $store_id)
                    ->select('store_bank_no')
                    ->first();
                if ($StoreBank && $StoreBank->store_bank_no) {
                    $register = '1';
                }
            }


            return json_encode([
                'status' => 1,
                'message' => '数据返回成功',
                'data' => [
                    'register' => $register,
                    'token' => $token,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'wxapp_openid' => $open_id,
                    'phone' => $phone,
                    'name' => $name,
                    'pid' => $pid,

                ],
            ]);


        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getLine()
            ]);
        }

    }


    //微信小程序获取openid
    public function wx_get_openid(Request $request)
    {
        try {
            $url = 'https://api.weixin.qq.com/sns/jscode2session';
            $appid = 'wxf13a83e484d83e79';
            $secret = '42022f0db7d1b3cbec9b946dee094b7e';
            $js_code = $request->get('code');
            $url = $url . '?appid=' . $appid . '&secret=' . $secret . '&js_code=' . $js_code . '&grant_type=authorization_code';
            $re = Tools::curl([], $url);
            $re_arr = json_decode($re, true);


            if (isset($re_arr['errcode'])) {
                return json_encode([
                    'status' => 2,
                    'message' => $re_arr['errmsg'],
                ]);
            }
            $open_id = $re_arr['openid'];

            return json_encode([
                'status' => 1,
                'message' => '数据返回成功',
                'data' => [
                    'openid' => $open_id,
                ],
            ]);


        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getLine()
            ]);
        }

    }


    //注册
    public function register(Request $request)
    {
        try {
            $data = $request->all();
            $phone = $request->get('phone', '');
            $name = $request->get('name', '');//没有就是手机号
            $password = $request->get('password', '000000');
            $password_confirmed = $request->get('password_confirmed', $password);
            $msn_code = $request->get('msn_code', '');
            $s_code = $request->get('s_code', '');//推荐码
            $wx_openid = $request->get('wx_openid', '');//微信公众号openid
            $wxapp_openid = $request->get('wxapp_openid', '');//微信小程序openid
            $wx_logo = $request->get('wx_logo', '');//头像
            $register_type = $request->get('register_type', '');//
            $store_id = date('Ymdhis', time()) . rand(1000, 9999);
            $url = "";
            $token = '';

            //如果登录名字名字为空
            if ($name == "") {
                $name = $phone;
            }
            //如果微信登录logo 为空
            if ($wx_logo == "") {
                $wx_logo = "";
            }


            if ($phone == '' && $password == '' && $msn_code == '' && $s_code == '' && $wx_openid == "") {
                return json_encode([
                    'status' => 2,
                    'message' => '参数填写不正确'
                ]);
            }

            $user = User::where('s_code', $s_code)->first();
            //验证激活码是否正确
            if ($s_code && $phone == '' && $msn_code == '' && $wx_openid == "") {
                if ($user) {
                    return json_encode([
                        'status' => 1,
                        'message' => '你输入的激活码正确'
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '你输入的激活码不正确'
                    ]);
                }

            }

            //验证短信验证码是否正确
            if ($msn_code && $phone && $s_code == "" && $wx_openid == "" && $msn_code != '0726') {
                //验证验证码
                $msn_local = Cache::get($phone . 'register-2');
                if ((string)$msn_code != (string)$msn_local) {
                    $message = "短信验证码有误，请重新输入";
                    $count = Cache::get('' . $phone . 'register_count');//次数
                    if ($msn_local == "" && $count) {
                        $message = "验证码失效，今日还有" . $count . '次机会';
                    }

                    return json_encode([
                        'status' => 2,
                        'message' => $message
                    ]);
                } else {
                    return json_encode([
                        'status' => 1,
                        'message' => '短信验证码匹配'
                    ]);
                }


            }

            //如果有确认密码 就判断
            if ($password_confirmed) {
                if ($password !== $password_confirmed) {
                    return json_encode([
                        'status' => 2,
                        'message' => '两次密码不一致'
                    ]);
                }
            }

            //验证激活码
            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '你输入的激活码不正确'
                ]);
            }

            //验证微信
            if ($wx_openid) {
                $user_wx_openid = Merchant::where('wx_openid', $wx_openid)->first();
                if ($user_wx_openid) {
                    return json_encode([
                        'status' => 2,
                        'message' => '此微信号已经绑定过账户了请重新更换'
                    ]);
                }
            }

            if ($phone && $msn_code && $s_code) {
                //验证手机号
                if (!preg_match("/^1[3456789]{1}\d{9}$/", $phone)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '手机号码格式不正确'
                    ]);
                }

                //验证验证码
                $msn_local = Cache::get($phone . 'register-2');

                if ((string)$msn_code != (string)$msn_local && $msn_code != '0726') {
                    return json_encode([
                        'status' => 2,
                        'message' => '验证码错误，请重新输入'
                    ]);
                }


                $rules = [
                    'phone' => 'required|min:11|max:11|unique:merchants',
                ];
                $validator = Validator::make($data, $rules);
                if ($validator->fails()) {
                    return json_encode([
                        'status' => 2,
                        'message' => '账号注册成功请直接登录',
                    ]);
                }
                //验证密码
                if (strlen($password) < 6) {
                    return json_encode([
                        'status' => 2,
                        'message' => '密码长度不符合要求'
                    ]);
                }

                //微信头像 保存本地
                if ($wx_logo) {
                    try {
                        $state = @file_get_contents($wx_logo, 0, null, 0, 1);//获取网络资源的字符内容
                        if ($state) {
                            $filename = public_path() . '/images/' . date("dMYHis") . '.png';//文件名称生成
                            $wx_logo = $filename;
                            ob_start();//打开输出
                            readfile($url);//输出图片文件
                            $img = ob_get_contents();//得到浏览器输出
                            ob_end_clean();//清除输出并关闭
                            $size = strlen($img);//得到图片大小
                            $fp2 = @fopen($filename, "a");
                            fwrite($fp2, $img);//向当前目录写入图片文件，并重新命名
                            fclose($fp2);
                        }
                    } catch (\Exception $exception) {
                    }
                }

                //80开头+当前日期+3位随机数
                $merchantNo = $this->generateMerchantNo();
                //检查是否存在商户号
                $existMerchantNo = Merchant::where('merchant_no', $merchantNo)->first();
                if(!$existMerchantNo){
                    $merchantNo = $this->generateMerchantNo(); //重新获取
                }

                $config_id = $user->config_id;
                $dataIN = [
                    'pid' => 0,
                    'type' => 1,
                    'name' => $name,
                    'email' => '',
                    'password' => isset($password) ? bcrypt($password) : "",
                    'phone' => $data['phone'],
                    'user_id' => $user->id,//推广员id
                    'config_id' => $config_id,
                    'wx_openid' => $wx_openid,
                    'wx_logo' => $wx_logo,
                    'wxapp_openid' => $wxapp_openid,
                    'source' => $user->source,  //来源,01:畅立收，02:河南畅立收
                    'merchant_no' => $merchantNo
                ];
                $merchant = Merchant::create($dataIN);
                $mid = $merchant->id;


                if ($merchant) {
                    $token = JWTAuth::fromUser($merchant);//根据用户得到token
                }


            } else {

                return json_encode([
                    'status' => 2,
                    'message' => '参数填写不完整'
                ]);
            }


            //教育二维码行业注册
            if ($register_type == "school") {
                try {
                    $phone = $data['phone'];
                    //开启事务
                    try {
                        DB::beginTransaction();
                        //中间逻辑代码 DB::commit();
                        MerchantStore::create([
                            'merchant_id' => $mid,
                            'store_id' => $store_id
                        ]);
                        $in_data = [
                            'config_id' => $config_id,
                            'user_id' => $merchant->user_id,
                            'merchant_id' => $mid,
                            'store_id' => $store_id,
                            'store_name' => $name,
                        ];
                        Store::create($in_data);
                        StoreBank::create([
                            'store_id' => $store_id,
                        ]);
                        StoreImg::create([
                            'store_id' => $store_id,
                        ]);

                        DB::commit();
                    } catch (\Exception $e) {
                        DB::rollBack();
                    }

                    // $url = url('/merchant/appAlipay?store_id=' . $store_id . '&merchant_id=' . $mid . '&config_id=' . $config_id . '&auth_type=03');
                    $message = '注册成功！请使用注册账户电脑登录创建学校';
                    $url = url('page/success?message=' . $message);

                } catch (\Exception $exception) {

                }
            } else {
                if ($wxapp_openid == "") {
                    //注册时给他个默认门店
                    Store::create([
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'store_name' => $name,
                        'user_id' => $user->id,//推广员id
                        'merchant_id' => $mid,
                    ]);
                    StoreBank::create([
                        'store_id' => $store_id,
                    ]);
                    StoreImg::create([
                        'store_id' => $store_id,
                    ]);

                    MerchantStore::create([
                        'store_id' => $store_id,
                        'merchant_id' => $mid,
                    ]);
                }
            }


            return json_encode([
                'status' => 1,
                'data' => [
                    'token' => $token,
                    'store_id' => $store_id,
                    'merchant_id' => $mid,
                    'url' => $url,
                    'register' => '2',
                ]
            ]);


        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getLine()
            ]);
        }
    }


    //修改密码
    public function edit_password(Request $request)
    {
        try {
            $phone = $request->get('phone', '');
            $merchant_no = $request->get('merchant_no', '');
            $password = $request->get('new_password', '');
            $code = $request->get('code', '');

            //验证参数不能为空
            if ($merchant_no == "" && $password == "" && $code == "") {
                return json_encode([
                    'status' => 2,
                    'message' => '参数必须有一项填写'
                ]);
            }

            //验证验证码
            if ($merchant_no && $password == "" && $code) {
                //验证验证码
                $msn_local = Cache::get($merchant_no . 'editpassword-2');
                if ((string)$code != (string)$msn_local) {
                    return json_encode([
                        'status' => 2,
                        'message' => '短信验证码不匹配'
                    ]);
                } else {
                    return json_encode([
                        'status' => 1,
                        'message' => '短信验证码匹配'
                    ]);
                }
            }

            //有密码的话修改密码
            if ($password && $merchant_no && $code) {

                //验证密码
                if (strlen($password) < 6) {
                    return json_encode([
                        'status' => 2,
                        'message' => '密码长度不符合要求'
                    ]);
                }

                $merchant = Merchant::where('merchant_no', $merchant_no)->first();
                if (!$merchant) {
                    return json_encode(['status' => 2, 'message' => '此账号未注册']);
                }

                //验证验证码
                $msn_local = Cache::get($merchant_no . 'editpassword-2');
                if ((string)$code != (string)$msn_local) {
                    return json_encode([
                        'status' => 2,
                        'message' => '短信验证码不匹配'
                    ]);
                }

                Merchant::where('merchant_no', $merchant_no)->update(['password' => bcrypt($password)]);
                $token = JWTAuth::fromUser($merchant);//根据用户得到token

                return json_encode([
                    'status' => 1,
                    'message' => '密码修改成功',
                    'data' => [
                        'token' => $token
                    ]
                ]);
            }
            return json_encode([
                'status' => 2,
                'message' => '参数填写不正确'
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'password' => 'required',
        ], [
            "required" => "账号密码必填"
        ]);
    }


    protected function sendFailedLoginResponse(Request $request)
    {
        //throw new AuthenticationException("账号密码有误");
        return json_encode(['status' => 303, 'message' => '账号不存在或者密码有误']);

    }


    protected function sendLoginResponse(Request $request, $merchant_no)
    {
        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user(),$merchant_no);
    }


    public function username()
    {
        return 'merchant_no';
    }


    public function authenticated(Request $request, $user, $merchant_no)
    {
        $jpush_id = $request->get('jpush_id');
        $device_type = $request->get('device_type', 'app');
        $device_id = $request->get('device_id', '');
        $login_type = $request->get('login_type', '');
        $permission = $request->get('permission', 0);

        $token = JWTAuth::fromUser($user);
        $url = '';
        //清除旧的登陆状态
        $old = Merchant::where('merchant_no', $merchant_no)->first();
        $config_id = $old->config_id;
        if ($old->t_type == "pos_newland91001") {
            $config_id = 'pos_newland91001';
        }
        $data_insert = [];
        $wx_openid = $request->get('wx_openid', '');
        $wxapp_openid = $request->get('wxapp_openid', '');
        //传设备极光识别码
        if ($jpush_id) {
            try {
                if ($old && $old->jpush_id != $jpush_id) {
                    $push = new JpushController();
                    $push->push_out($old->jpush_id, $config_id);
                }

                $data_insert['jpush_id'] = $jpush_id;
                $data_insert['device_type'] = $device_type;


            } catch (\Exception $exception) {
                Log::info($exception);
            }
        }

        if ($wx_openid) {
            $data_insert['wx_openid'] = $wx_openid;
        } else {
            $wx_openid = $old->wx_openid;

        }

        if ($wxapp_openid) {
            $data_insert['wxapp_openid'] = $wxapp_openid;
        } else {
            $wxapp_openid = $old->wxapp_openid;

        }

        Merchant::where('merchant_no', $merchant_no)->update($data_insert);
        $store_id = '';
        $store_type = '';
        $merchant_store_id = MerchantStore::where('merchant_id', $old->id)
            ->orderBy('created_at', 'asc')
            ->first();
        $store_short_name = "暂无门店";
        $store_name = "暂无门店";

        if ($merchant_store_id) {
            $store_id = $merchant_store_id->store_id;
            $store = Store::where('store_id', $store_id)
                ->select('pid', 'store_short_name', 'store_name')
                ->first();
            if ($store) {
                $store_type = $store->pid;
                $store_short_name = $store->store_short_name;
                $store_name = $store->store_name;
            }
        }

        //教育二维码行业登录
        if ($login_type == "school") {
            try {
                $url = url('/merchant/appAlipay?store_id=' . $store_id . '&merchant_id=' . $old->id . '&config_id=' . $config_id . '&auth_type=03');
            } catch (\Exception $exception) {

            }
        }

        //蜻蜓F4 账户
        if ($device_type == "face_f4") {

            $store = Store::where('store_id', $store_id)
                ->select('pid')
                ->first();
            if ($store) {
                //分店
                if ($store->pid) {
                    $store = Store::where('id', $store->pid)
                        ->select('store_id')
                        ->first();
                    $device = Device::where('device_no', $device_id)
                        ->whereIn('store_id', [$store_id, $store->store_id])
                        ->where('device_type', $device_type)
                        ->select('id')
                        ->first();
                } else {
                    $device = Device::where('device_no', $device_id)
                        ->where('store_id', $store_id)
                        ->where('device_type', $device_type)
                        ->select('id')
                        ->first();
                }

                if (!$device) {
                    return json_encode([
                        'status' => 2,
                        'message' => '账户和设备绑定关系不一致'
                    ]);
                }
            }

        }

        $return_data = [
            'status' => 1,
            'data' => [
                'token' => $token,
                'store_id' => $store_id,
                'type' => $old->type,
                'store_type' => (int)$store_type,
                'url' => $url,
                'phone' => $old->phone,
                'merchant_no' => $merchant_no,
                'wxapp_openid' => $wxapp_openid,
                'name' => $old->name,
                'pid' => $old->pid,
                'store_name' => $store_name,
                'store_short_name' => $store_short_name,
                'merchant_id' => $old->id,
                'source' => $old->source,
                'userType' =>'merchant',
            ]
        ];

        //返回权限集合
        if ($permission) {
            $permissions = $old->getAllPermissions();
            $data = [];
            foreach ($permissions as $k => $v) {
                $data[$k]['name'] = $v->name;
            }
            $return_data['permissions'] = $data;
        }

        return json_encode($return_data);
    }


    public function getAuthenticatedUser(Request $request)
    {
        JWTAuth::setToken(JWTAuth::getToken());
        $claim = JWTAuth::getPayload();
        try {
            if (!$claim = JWTAuth::getPayload()) {
                return response()->json(array('message' => 'user_not_found'), 404);
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(array('message' => 'token_expired'), $e->getCode());
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(array('message' => 'token_invalid'), $e->getCode());
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(array('message' => 'token_absent'), $e->getCode());
        }
        return response()->json(array('status' => 1, 'data' => ['merchant' => $claim['sub']]));

    }


    //小程序获取浏览过的账户密码
    public function login_list(Request $request)
    {
        $wxapp_openid = $request->get('wxapp_openid', '');
        if (isset($wxapp_openid) && $wxapp_openid) {
            $data = Merchant::where('wxapp_openid', $wxapp_openid)
                ->select('merchant_no','phone')
                ->orderBy('updated_at', 'desc')
                ->take(3)
                ->get();
        } else {
            $data = [];
        }


        return json_encode([
            'status' => 1,
            'data' => $data
        ]);

    }


    //小程序清除登录账户
    public function login_del(Request $request)
    {

        $phone = $request->get('phone', '');
        $merchant_no = $request->get('merchant_no', '');

        $check_data = [
//            'phone' => '手机号',
            'merchant_no' => '商户号',
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            return json_encode([
                'status' => 2,
                'message' => $check
            ]);
        }

        Merchant::where('merchant_no', $merchant_no)
            ->update(['wxapp_openid' => '']);


        return json_encode([
            'status' => 1,
            'message' => '清除成功'
        ]);
    }


    //获取阿里云mqtt
    public function get_mq_info(Request $request)
    {
        try {
            //获取请求参数
            $device_id = $request->get('device_id');
            $device_type = $request->get('device_type');
            $store_id = $request->get('store_id');
            $store_name = $request->get('store_name', '测试');
            $config_id = $request->get('config_id', '1234');
            $merchant_id = $request->get('merchant_id');

            $check_data = [
                'device_id' => '设备编号',
                'device_type' => '设备类型',
                'merchant_id' => 'merchant_id',
                'store_id' => 'store_id',
            ];
            $check = $this->check_required($request->all(), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            //
            $MqttConfig = MqttConfig::where('config_id', $config_id)->first();
            if (!$MqttConfig) {
                $MqttConfig = MqttConfig::where('config_id', '1234')->first();
            }
            if (!$MqttConfig) {
                return json_encode([
                    'status' => 2,
                    'message' => '未配置消息推送'
                ]);
            }

            $mq_server = $MqttConfig->server;
            $mq_topic = $MqttConfig->topic;
            $mq_port = $MqttConfig->port;
            $mq_group_id = $MqttConfig->group_id;
            $mq_user_name = "Signature|" . $MqttConfig->access_key_id . "|" . $MqttConfig->instance_id . "";
            $str = '' . $MqttConfig->group_id . '@@@' . $device_id . '';
            $key = $MqttConfig->access_key_secret;
            $str = mb_convert_encoding($str, "UTF-8");
            $mq_user_password = base64_encode(hash_hmac("sha1", $str, $key, true));

            //公共返回参数
            $re_data = [
                'store_id' => $store_id,
                'merchant_id' => $merchant_id,
                'store_name' => $store_name,
                'mq_server' => $mq_server,
                'mq_topic' => $mq_topic,
                'mq_port' => $mq_port,
                'mq_group_id' => $mq_group_id,
                'client_id' => $mq_group_id . '@@@' . $device_id,
                'mq_user_name' => $mq_user_name,
                'mq_user_password' => $mq_user_password,
                'xfyun_APPID'=>'5ef1b862',
                'xfyun_APISecret'=>'71164dcc0fb7ae771ea2e4a5466fb7f8',
                'xfyun_APIKey'=>'63feadc80897f88695b0687f205dd916',
            ];
	    
            return json_encode([
                'status' => 1,
                'message' => '数据返回成功',
                'data' => $re_data,
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //播报状态查询
    public function select_bb_status(Request $request)
    {
        try {
            $merchant = $this->parseToken();

            //获取请求参数
            $store_id = $request->get('store_id', '');
            $merchant_id = $request->get('merchant_id', '');
            $is_open = $request->get('is_open', '');

            $config_id = $merchant->config_id;

            $check_data = [
                'merchant_id' => 'merchant_id',
            ];
            $check = $this->check_required($request->all(), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $device = Device::where('device_type', 'phone')
                ->where('store_id', $store_id)
                ->where('type', 'v')
                ->where('merchant_id', $merchant_id)
                ->first();

            //查询状态
            if ($is_open > 1) {
                $is_open = '0';

                if ($device) {
                    $is_open = '1';
                }
            } else {
                //开启
                if ($is_open == "1") {
                    if (!$device) {
                        Device::create([
                            'store_id' => $store_id,
                            'config_id' => $config_id,
                            'store_name' => $store_id,
                            'merchant_id' => $merchant_id,
                            'merchant_name' => $merchant_id,
                            'device_type' => 'phone',
                            'device_name' => '手机播报',
                            'device_no' => $merchant_id,
                            'device_key' => $merchant_id,
                            'status' => '1',
                            'type' => 'v',
                        ]);
                    }
                }
                //关闭
                if ($is_open == "0") {
                    Device::where('device_type', 'phone')
                        ->where('store_id', $store_id)
                        ->where('merchant_id', $merchant_id)
                        ->delete();
                }
            }

            $data = [
                    'is_open' => $is_open
                ];
		
            return json_encode([
                'status' => 1,
                'message' => '数据操作成功',
                'data' => $data,
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * 检查是否手机号登录
     * 如果是提示用户商户号，下次需使用商户号登录
     */
    public function checkLoginInfo(Request $request)
    {
        $merchant = $this->parseToken();

        $merchant_no = $merchant->merchant_no;

        $merchantNo = Merchant::where('merchant_no', $merchant_no)->where('login_num', '0')->first();
        if($merchantNo){
            $upIN = [
                'login_num' => '1',
            ];
            $merchantNo->update($upIN);

            return json_encode([
                'status' => 1,
                'message' => '操作成功',
                'data' => $merchant_no
            ]);
        }

        return json_encode([
            'status' => 1,
            'message' => '操作成功',
            'data' => null
        ]);
    }


}
