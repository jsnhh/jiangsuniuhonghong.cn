<?php
namespace App\Api\Controllers\Merchant;


use App\Api\Controllers\BaseController;
use App\Models\MerchantStore;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class OrderCountController extends BaseController
{

//    获取当前登陆者门店信息
    public function get_merchant()
    {
        try {
            $user = $this->parseToken();
            $merchant_id = $user->merchant_id;
            $store_s = MerchantStore::join('stores','merchant_stores.store_id','stores.store_id')
                ->where('merchant_stores.merchant_id',$merchant_id) //$merchant_id
                ->select('merchant_stores.*','stores.store_name')
                ->get();
//            dd($store_s);
            $this->status = 1;
            $this->message = '数据返回成功';

            return $this->format($store_s);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage() . $exception->getLine();
            return $this->format();
        }
    }


    //门店按月统计
    public function Order_mouth_count(Request $request)
    {
        try {
            $user = $this->parseToken();
            //获取登陆者信息
            $merchant_id = $user->merchant_id;
            //有则接受有的  没有则默认
            $store_id = $request->get('store_id',''); //
            $month = $request->get('month',date('Ym'));//获取月份 设置或默认当月
            $source_type = $request->get('source_type','');   //设置或默认支付类型
            $company = $request->get('company','');   //设置或默认通道类型   alipay
            $where = [];
            if ($month==''){
                $month =  date('Ym');
            }
            if ($store_id) {
                $where[] = ['store_month_orders.store_id', '=', $store_id];
                $store_ids = [
                    [
                        'store_id' => $store_id,
                    ]
                ];

            } else {
                $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)
                    ->select('store_id')
                    ->get();

                if (!$MerchantStore->isEmpty()) {
                    $store_ids = $MerchantStore->toArray();
                }

            }

            if ($month) {
                $where[] = ['month', '=', $month];
            }
            if ($source_type) {
                $where[] = ['source_type', '=', $source_type];
            }
            if ($company) {
                $where[] = ['company', '=', $company];
            }
//            dd($where);
            if (env('DB_D1_HOST')) {
                if (Schema::hasTable('store_month_orders')) {
                    $obj = DB::connection("mysql_d1")
                        ->table('store_month_orders')
                        ->join('stores','store_id','stores.id')
                        ->where($where)
                        ->whereIn('store_month_orders.store_id',$store_ids)
                        ->select('store_month_orders.store_id','store_month_orders.month','store_month_orders.total_amount','store_month_orders.company','store_month_orders.source_type','store_month_orders.order_sum','store_month_orders.refund_amount','store_month_orders.refund_amount','store_month_orders.refund_count','store_month_orders.fee_amount','stores.store_name');
                }
            } else {
                if (Schema::hasTable('store_month_orders')) {
                    $obj = DB::table('store_month_orders')
                        ->join('stores','store_month_orders.store_id','stores.store_id')
                        ->where($where)
                        ->whereIn('store_month_orders.store_id',$store_ids)
                        ->select('store_month_orders.store_id','store_month_orders.month','store_month_orders.total_amount','store_month_orders.company','store_month_orders.source_type','store_month_orders.order_sum','store_month_orders.refund_amount','store_month_orders.refund_amount','store_month_orders.refund_count','store_month_orders.fee_amount','stores.store_name');
                }
            }

            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage() . $exception->getLine();
            return $this->format();
        }
    }


}
