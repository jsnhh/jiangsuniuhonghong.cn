<?php

namespace App\Api\Controllers\Merchant;


use Alipayopen\Sdk\AopClient;
use Alipayopen\Sdk\Request\AlipayTradeCancelRequest;
use Alipayopen\Sdk\Request\AlipayTradePayRequest;
use Alipayopen\Sdk\Request\AlipayTradeQueryRequest;
use App\Api\Controllers\AlipayOpen\PayController;
use App\Api\Controllers\BaseController;
use App\Api\Controllers\Config\AlipayIsvConfigController;
use App\Api\Controllers\Config\AllinPayConfigController;
use App\Api\Controllers\Config\ChangshaConfigController;
use App\Api\Controllers\Config\CcBankPayConfigController;
use App\Api\Controllers\Config\EasyPayConfigController;
use App\Api\Controllers\Config\EasySkPayConfigController;
use App\Api\Controllers\Config\FuiouConfigController;
use App\Api\Controllers\Config\HConfigController;
use App\Api\Controllers\Config\HkrtConfigController;
use App\Api\Controllers\Config\HuiPayConfigController;
use App\Api\Controllers\Config\HwcPayConfigController;
use App\Api\Controllers\Config\JdConfigController;
use App\Api\Controllers\Config\LianfuConfigController;
use App\Api\Controllers\Config\LianfuyoupayConfigController;
use App\Api\Controllers\Config\LinkageConfigController;
use App\Api\Controllers\Config\LklConfigController;
use App\Api\Controllers\Config\LtfConfigController;
use App\Api\Controllers\Config\MyBankConfigController;
use App\Api\Controllers\Config\NewLandConfigController;
use App\Api\Controllers\Config\PayWaysController;
use App\Api\Controllers\Config\PostPayConfigController;
use App\Api\Controllers\Config\QfPayConfigController;
use App\Api\Controllers\Config\SuzhouConfigController;
use App\Api\Controllers\Config\TfConfigController;
use App\Api\Controllers\Config\VbillConfigController;
use App\Api\Controllers\Config\WeixinConfigController;
use App\Api\Controllers\Config\WftPayConfigController;
use App\Api\Controllers\Config\YinshengConfigController;
use App\Api\Controllers\DuoLaBao\ManageController;
use App\Api\Controllers\EasyPay\PayController as EasyPayPayController;
use App\Api\Controllers\Linkage\PayController as LinkagePayController;
use App\Api\Controllers\Weixin\PayController as WeixinPayController;
use App\Api\Controllers\HwcPay\PayController as HwcpayPayController;
use App\Api\Controllers\WftPay\PayController as WftpayPayController;
use App\Api\Controllers\YinSheng\PayController as YinShengPayController;
use App\Api\Controllers\QfPay\PayController as QfPayController;
use App\Api\Controllers\MyBank\QrpayController;
use App\Api\Controllers\MyBank\TradePayController;
use App\Common\PaySuccessAction;
use App\Jobs\WXProfitSharing;
use App\Models\AlipayAccount;
use App\Models\AlipayHbOrder;
use App\Models\AlipayHbrate;
use App\Models\DadaOrder;
use App\Models\Device;
use App\Models\EasypayStoresImages;
use App\Models\Merchant;
use App\Models\MerchantConsumerDetails;
use App\Models\MerchantStore;
use App\Models\MerchantStoreAppidSecret;
use App\Models\Store;
use App\Models\UserKeyUrl;
use App\Models\WeixinaStore;
use App\Models\WeixinStore;
use EasyWeChat\Factory;
use function EasyWeChat\Kernel\Support\get_client_ip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Api\Controllers\SayDodge\SayDodgePayController;

class PayBaseController extends BaseController
{

    //扫一扫 提交
    public function scan_pay(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $config_id = $merchant->config_id;
            $merchant_id = $merchant->merchant_id;
            $merchant_name = $merchant->merchant_name;
            $code = $request->get('code');
            $total_amount = $request->get('total_amount');
            $shop_price = $request->get('shop_price', $total_amount);
            $remark = $request->get('remark', '');
//            $device_id = $request->get('device_id', 'app');
            $device_id = $request->get('device_id', '收银系统');
            $shop_name = $request->get('shop_name', '扫一扫收款');
            $shop_desc = $request->get('shop_desc', '扫一扫收款');
            $store_id = $request->get('store_id', '');
            $other_no = $request->get('other_no', '');
            $pay_method = $request->get('pay_method', 'scan');

            $check_data = [
                'total_amount' => '付款金额',
                'code' => '付款码编号'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

//            $strlen = strlen($code);
//            if ($strlen > 18) {
//                $substr = substr($code, -1); //截取判断最后一个
//                $code = substr($code, 0, -1); //去掉最后一个
//                $str = substr($code, 0, 2);
//
//                //支付宝刷脸
//                if ($substr == '2' && in_array($str, ['28'])) {
//                    $pay_method = 'alipay_face';
//                    $device_id = '刷脸设备';
//
//                }
//
//                //微信刷脸
//                if ($substr == '2' && in_array($str, ['13', '14'])) {
//                    $pay_method = 'weixin_face';
//                    $device_id = '刷脸设备';
//                }
//
//            }
            //刷脸处理
            $str = substr($code, 0, 2);
            if (in_array($str, ['28']) || in_array($str, ['13', '14'])) {
                $strlen = strlen($code);
                if ($strlen > 18) {
                    $substr = substr($code, -1); //截取判断最后一个
                    $code = substr($code, 0, -1); //去掉最后一个

                    //支付宝刷脸
                    if ($substr == '2' && in_array($str, ['28'])) {
                        $pay_method = 'alipay_face';
                        $device_id = $request->get('device_id', '刷脸设备');
                    }

                    //微信刷脸
                    if ($substr == '2' && in_array($str, ['13', '14'])) {
                        $pay_method = 'weixin_face';
                        $device_id = $request->get('device_id', '刷脸设备');
                    }

                }

            }

            $data = [
                'config_id' => $config_id,
                'merchant_id' => $merchant_id,
                'merchant_name' => $merchant_name,
                'code' => $code,
                'total_amount' => $total_amount,
                'shop_price' => $shop_price,
                'remark' => $remark,
                'device_id' => $device_id,
                'shop_name' => $shop_name,
                'shop_desc' => $shop_desc,
                'store_id' => $store_id,
                'other_no' => $other_no,
                'pay_method' => $pay_method
            ];
            return $this->scan_pay_public($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //扫一扫 公共
    public function scan_pay_public($data)
    {
        try {
            $config_id = isset($data['config_id']) ? $data['config_id'] : "";
            $merchant_id = isset($data['merchant_id']) ? $data['merchant_id'] : "0";
            $merchant_name = isset($data['merchant_name']) ? $data['merchant_name'] : "无";
            $code = $data['code'];
            $total_amount = $data['total_amount'];
            $shop_price = $data['shop_price'];
            $remark = $data['remark'];
            $device_id = isset($data['device_id']) ? $data['device_id'] : '';
            $device_type = isset($data['device_type']) ? $data['device_type'] : '';
            $shop_name = $data['shop_name'];
            $shop_desc = $data['shop_desc'];
            $store_id = $data['store_id'];
            $other_no = $data['other_no'];
            $out_trade_no = isset($data['out_trade_no']) ? $data['out_trade_no'] : "";
            $pay_method = isset($data['pay_method']) ? $data['pay_method'] : "scan"; //支付方式,alipay_face-支付宝刷脸;weixin_face-微信刷脸;wx_applet-微信小程序;ali_applet-支付宝小程序
            //查询设备名称
            $device_name = $device_id;
            if ($device_id != '收银系统') {
                $device = Device::where('device_no', $device_id)->select('device_name')
                    ->first();
                if ($device) {
                    $device_name = $device->device_name;
                }
            }
            //没有传门店
            if ($store_id == "") {
                $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)
                    ->orderBy('created_at', 'asc')
                    ->select('store_id')
                    ->first();
                if (!$MerchantStore) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请提交门店资料认证'
                    ]);
                }
                $store_id = $MerchantStore->store_id;
            }
            $store = Store::where('store_id', $store_id)
                ->select('store_name', 'config_id', 'pid', 'user_id', 'is_admin_close', 'is_delete', 'alipay_account', 'is_close', 'people_phone', 'source')
                ->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '平台门店ID不正确'
                ]);
            }

            //关闭的商户禁止交易
            if ($store->is_close || $store->is_admin_close || $store->is_delete) {
                return json_encode([
                    'status' => 2,
                    'message' => '商户已经被服务商关闭'
                ]);
            }

            $store_name = $store->store_name;
            $store_pid = $store->pid;
            $tg_user_id = $store->user_id;
            $config_id = $store->config_id;
            $source = $store->source;  //来源,01:畅立收，02:河南畅立收

            //插入数据库
            $data_insert = [
                'trade_no' => '',
                'user_id' => $tg_user_id,
                'store_id' => $store_id,
                'store_name' => $store_name,
                'buyer_id' => '',
                'total_amount' => $total_amount,
                'shop_price' => $shop_price,
                'payment_method' => '',
                'status' => '',
                'pay_status' => 2,
                'pay_status_desc' => '等待支付',
                'merchant_id' => $merchant_id,
                'merchant_name' => $merchant_name,
                'remark' => $remark,
                'device_id' => $device_id,
                'config_id' => $config_id,
                'other_no' => $other_no,
                'pay_method' => $pay_method,
                'source' => $source,
                'device_name' => $device_name
            ];

            $str = substr($code, 0, 2);
            Log::info("扫一扫 公共-weixni");
            Log::info($str);

            /**支付宝渠道开始**/
            if (in_array($str, ['28'])) {
                //读取优先为高级的通道
                $obj_ways = new PayWaysController();
                $ways = $obj_ways->ways_source('alipay', $store_id, $store_pid);
                if (!$ways) {
                    return json_encode([
                        'status' => 2,
                        'message' => '没有开通此类型通道'
                    ]);
                }
                if ($ways->is_close) {
                    return json_encode([
                        'status' => 2,
                        'message' => '此通道已关闭'
                    ]);
                }

                $user_rate_obj = $obj_ways->getCostRate($ways->ways_type, $tg_user_id);
                $cost_rate = $user_rate_obj ? $user_rate_obj->rate : 0.00; //成本费率
                if (!$out_trade_no) {
                    $out_trade_no = 'aliscan' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                }

                //扫码费率入库
                if ($cost_rate) $data_insert['cost_rate'] = $cost_rate;
                $data_insert['rate'] = $ways->rate;
                $data_insert['fee_amount'] = $ways->rate * $total_amount / 100;

                //官方支付宝扫一扫
                if ($ways && $ways->ways_type == 1000) {
                    $data['out_trade_no'] = $out_trade_no;
                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $config_type = '01';

                    //配置
                    $isvconfig = new AlipayIsvConfigController();
                    $storeInfo = $isvconfig->alipay_auth_info($store_id, $store_pid);
                    $out_user_id = $storeInfo->user_id; //商户的id
                    //分成模式 服务商
                    if ($storeInfo->settlement_type == "set_a") {
                        if ($storeInfo->config_type == '02') {
                            $config_type = '02';
                        }
                        $storeInfo = AlipayAccount::where('config_id', $config_id)
                            ->where('config_type', $config_type)
                            ->first(); //服务商的
                    }

                    if (!$storeInfo) {
                        $msg = '支付宝授权信息不存在';
                        return [
                            'status' => 2,
                            'message' => $msg
                        ];
                    }

                    $disable_pay_channels = ''; //仅用方
                    //禁用信用通道
                    if ($ways->pcredit == "00") {
                        $disable_pay_channels = 'credit_group'; //禁用信用方式
                    }

                    $alipay_store_id = $storeInfo->alipay_store_id;
                    $out_store_id = $storeInfo->out_store_id;
                    $app_auth_token = $storeInfo->app_auth_token;
                    $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);

                    //订单数据库
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    $notify_url = url('/api/alipayopen/qr_pay_notify');

                    $aop = new AopClient();
                    $aop->apiVersion = "2.0";
                    $aop->appId = $config->app_id;
                    $aop->rsaPrivateKey = $config->rsa_private_key;
                    $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                    $aop->method = 'alipay.trade.pay';
                    $aop->notify_url = $notify_url;
                    $aop->signType = "RSA2"; //升级算法
                    $aop->gatewayUrl = $config->alipay_gateway;
                    $aop->format = "json";
                    $aop->charset = "GBK";
                    $aop->version = "2.0";

                    $requests = new AlipayTradePayRequest();
                    $requests->setNotifyUrl($notify_url);
                    //提交到支付宝
                    if ($alipay_store_id) {
                        $data_re = "{" .
                            "\"out_trade_no\":\"" . $out_trade_no . "\"," .
                            "\"seller_id\":\"" . $out_user_id . "\"," .//商户收款账号
                            "\"disable_pay_channels\":\"" . $disable_pay_channels . "\"," .
                            "    \"scene\":\"bar_code\"," .
                            "    \"auth_code\":\"" . $code . "\"," .
                            "    \"subject\":\"" . $shop_name . "\"," .
                            "    \"total_amount\":" . $total_amount . "," .
                            "    \"timeout_express\":\"90m\"," .
                            "    \"body\":\"" . $shop_desc . "\"," .
                            "      \"goods_detail\":[{" .
                            "        \"goods_id\":\"" . $store_id . "\"," .
                            "        \"goods_name\":\"" . $shop_name . "\"," .
                            "        \"quantity\":1," .
                            "        \"price\":" . $total_amount . "," .
                            "        \"body\":\"" . $shop_name . "\"" .
                            "        }]," .
                            "    \"store_id\":\"" . $out_store_id . "\"," .
                            "    \"shop_id\":\"" . $alipay_store_id . "\"," .
                            "    \"terminal_id\":\"" . $device_id . "\"," .
                            "    \"operator_id\":\"D_001_" . $merchant_id . "\"," .
                            "    \"extend_params\":{" .
                            "      \"sys_service_provider_id\":\"" . $config->alipay_pid . "\"" .
                            "}" .
                            "  }";
                    } else {
                        $data_re = "{" .
                            "\"out_trade_no\":\"" . $out_trade_no . "\"," .
                            "\"seller_id\":\"" . $out_user_id . "\"," .//商户收款账号
                            "\"disable_pay_channels\":\"" . $disable_pay_channels . "\"," .
                            "    \"scene\":\"bar_code\"," .
                            "    \"auth_code\":\"" . $code . "\"," .
                            "    \"subject\":\"" . $shop_name . "\"," .
                            "    \"total_amount\":" . $total_amount . "," .
                            "    \"timeout_express\":\"90m\"," .
                            "    \"body\":\"" . $shop_desc . "\"," .
                            "      \"goods_detail\":[{" .
                            "        \"goods_id\":\"" . $store_id . "\"," .
                            "        \"goods_name\":\"" . $shop_name . "\"," .
                            "        \"quantity\":1," .
                            "        \"price\":" . $total_amount . "," .
                            "        \"body\":\"" . $shop_name . "\"" .
                            "        }]," .
                            "    \"store_id\":\"" . $store_id . "\"," .
                            "    \"terminal_id\":\"" . $device_id . "\"," .
                            "    \"operator_id\":\"D_001_" . $merchant_id . "\"," .
                            "    \"extend_params\":{" .
                            "      \"sys_service_provider_id\":\"" . $config->alipay_pid . "\"" .
                            "}" .
                            "  }";
                    }
                    $requests->setBizContent($data_re);
                    $result = $aop->execute($requests, null, $app_auth_token);
                    $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
                    $resultCode = $result->$responseNode->code;
                    //异常
                    if ($resultCode == 40004) {
                        return json_encode([
                            'status' => 2,
                            'message' => $result->$responseNode->msg . $result->$responseNode->sub_code,
                            'result_code' => $resultCode,
                        ]);
                    }
                    //支付成功
                    if (!empty($resultCode) && $resultCode == 10000) {
                        $buyer_id = $result->$responseNode->buyer_user_id; //必选,28,买家在支付宝的用户ID
                        $buyer_logon_id = $result->$responseNode->buyer_logon_id; //必选	,100,买家支付宝账号
//                        $payment_method = $result->$responseNode->fund_bill_list[0]->fund_channel; //必填,32,交易使用的资金渠道(COUPON-支付宝红包,ALIPAYACCOUNT-支付宝账户,POINT-集分宝,DISCOUNT-折扣券,PCARD-预付卡,MCARD-商家储值卡;MDISCOUNT-商户优惠券,MCOUPON-商户红包,BANKCARD-银行卡)
                        $amount = $result->$responseNode->fund_bill_list[0]->amount; //必填,32,该支付工具类型所使用的金额
                        $real_amount = isset($result->$responseNode->fund_bill_list[0]->real_amount) ? $result->$responseNode->fund_bill_list[0]->real_amount : 0.00; //可选,11,渠道实际付款金额
                        $trade_no = $result->$responseNode->trade_no; //必选,64,支付宝交易号
                        $gmt_payment = $result->$responseNode->gmt_payment; //必选,32,交易支付时间,2014-11-27 15:45:57
                        $receipt_amount = $result->$responseNode->receipt_amount; //必选,实收金额,2位小数
                        $buyer_pay_amount = isset($result->$responseNode->buyer_pay_amount) ? $result->$responseNode->buyer_pay_amount : 0.00; //可选，买家付款的金额
                        //优惠券入
                        $fee_amount = $ways->rate * $receipt_amount / 100;
                        if (isset($result->$responseNode->fund_bill_list[1])) {
                            switch ($result->$responseNode->fund_bill_list[1]->fund_channel) {
                                //支付宝红包
                                case "COUPON":
                                    $coupon_amount = 0.00;
                                    $coupon_type = '';
                                    $payment_method = $result->$responseNode->fund_bill_list[0]->fund_channel;
                                    $discount_amount = isset($result->$responseNode->fund_bill_list[1]->amount) ? $result->$responseNode->fund_bill_list[1]->amount : 0.00; //三方优惠金额
                                    break;
                                case "PCREDIT":
                                    if ($result->$responseNode->fund_bill_list[0]->fund_channel == 'COUPON') {
                                        $coupon_amount = 0.00;
                                        $coupon_type = '';
                                        $payment_method = $result->$responseNode->fund_bill_list[1]->fund_channel;
                                        $discount_amount = isset($result->$responseNode->fund_bill_list[0]->amount) ? $result->$responseNode->fund_bill_list[0]->amount : 0.00; //三方优惠金额
                                    } else {
                                        $coupon_amount = isset($result->$responseNode->fund_bill_list[0]->amount) ? $result->$responseNode->fund_bill_list[0]->amount : 0.00;
                                        $coupon_type = isset($result->$responseNode->fund_bill_list[0]->fund_channel) ? $result->$responseNode->fund_bill_list[0]->fund_channel : '';
                                        $payment_method = $result->$responseNode->fund_bill_list[1]->fund_channel;
                                        $discount_amount = 0.00;
                                    }
                                    break;
                                case "ALIPAYACCOUNT":
                                    if ($result->$responseNode->fund_bill_list[0]->fund_channel == 'COUPON' || $result->$responseNode->fund_bill_list[0]->fund_channel == 'POINT') {
                                        $coupon_amount = 0.00;
                                        $coupon_type = '';
                                        $payment_method = $result->$responseNode->fund_bill_list[1]->fund_channel;
                                        $discount_amount = isset($result->$responseNode->fund_bill_list[0]->amount) ? $result->$responseNode->fund_bill_list[0]->amount : 0.00; //三方优惠金额
                                    } else {
                                        $coupon_amount = isset($result->$responseNode->fund_bill_list[0]->amount) ? $result->$responseNode->fund_bill_list[0]->amount : 0.00;
                                        $coupon_type = isset($result->$responseNode->fund_bill_list[0]->fund_channel) ? $result->$responseNode->fund_bill_list[0]->fund_channel : '';
                                        $payment_method = $result->$responseNode->fund_bill_list[1]->fund_channel;
                                        $discount_amount = 0.00;
                                    }
                                    break;
                                case "POINT":
                                    $coupon_amount = 0.00;
                                    $coupon_type = '';
                                    $payment_method = $result->$responseNode->fund_bill_list[0]->fund_channel;
                                    $discount_amount = isset($result->$responseNode->fund_bill_list[1]->amount) ? $result->$responseNode->fund_bill_list[1]->amount : 0.00; //三方优惠金额
                                    break;
                                case "DISCOUNT":
                                    if ($result->$responseNode->fund_bill_list[0]->fund_channel == 'COUPON') {
                                        $coupon_amount = 0.00;
                                        $coupon_type = '';
                                        $payment_method = $result->$responseNode->fund_bill_list[1]->fund_channel;
                                        $discount_amount = isset($result->$responseNode->fund_bill_list[0]->amount) ? $result->$responseNode->fund_bill_list[0]->amount : 0.00; //三方优惠金额
                                    } else {
                                        $coupon_amount = isset($result->$responseNode->fund_bill_list[1]->amount) ? $result->$responseNode->fund_bill_list[1]->amount : 0.00;
                                        $coupon_type = isset($result->$responseNode->fund_bill_list[1]->fund_channel) ? $result->$responseNode->fund_bill_list[1]->fund_channel : '';
                                        $payment_method = $result->$responseNode->fund_bill_list[0]->fund_channel;
                                        $discount_amount = 0.00;
                                    }
                                    break;
                                case "PCARD":
                                    if ($result->$responseNode->fund_bill_list[0]->fund_channel == 'COUPON') {
                                        $coupon_amount = 0.00;
                                        $coupon_type = '';
                                        $payment_method = $result->$responseNode->fund_bill_list[1]->fund_channel;
                                        $discount_amount = isset($result->$responseNode->fund_bill_list[0]->amount) ? $result->$responseNode->fund_bill_list[0]->amount : 0.00; //三方优惠金额
                                    } else {
                                        $coupon_amount = isset($result->$responseNode->fund_bill_list[0]->amount) ? $result->$responseNode->fund_bill_list[0]->amount : 0.00;
                                        $coupon_type = isset($result->$responseNode->fund_bill_list[0]->fund_channel) ? $result->$responseNode->fund_bill_list[0]->fund_channel : '';
                                        $payment_method = $result->$responseNode->fund_bill_list[1]->fund_channel;
                                        $discount_amount = 0.00;
                                    }
                                    break;
                                case "MCARD":
                                    if ($result->$responseNode->fund_bill_list[0]->fund_channel == 'COUPON') {
                                        $coupon_amount = 0.00;
                                        $coupon_type = '';
                                        $payment_method = $result->$responseNode->fund_bill_list[1]->fund_channel;
                                        $discount_amount = isset($result->$responseNode->fund_bill_list[0]->amount) ? $result->$responseNode->fund_bill_list[0]->amount : 0.00; //三方优惠金额
                                    } else {
                                        $coupon_amount = isset($result->$responseNode->fund_bill_list[0]->amount) ? $result->$responseNode->fund_bill_list[0]->amount : 0.00;
                                        $coupon_type = isset($result->$responseNode->fund_bill_list[0]->fund_channel) ? $result->$responseNode->fund_bill_list[0]->fund_channel : '';
                                        $payment_method = $result->$responseNode->fund_bill_list[1]->fund_channel;
                                        $discount_amount = 0.00;
                                    }
                                    break;
                                case "MDISCOUNT":
                                    $coupon_amount = isset($result->$responseNode->fund_bill_list[1]->amount) ? $result->$responseNode->fund_bill_list[1]->amount : 0.00;
                                    $coupon_type = isset($result->$responseNode->fund_bill_list[1]->fund_channel) ? $result->$responseNode->fund_bill_list[1]->fund_channel : '';
                                    $payment_method = $result->$responseNode->fund_bill_list[0]->fund_channel;
                                    $discount_amount = 0.00;
                                    break;
                                case "MCOUPON":
                                    $coupon_amount = isset($result->$responseNode->fund_bill_list[1]->amount) ? $result->$responseNode->fund_bill_list[1]->amount : 0.00;
                                    $coupon_type = isset($result->$responseNode->fund_bill_list[1]->fund_channel) ? $result->$responseNode->fund_bill_list[1]->fund_channel : '';
                                    $payment_method = $result->$responseNode->fund_bill_list[0]->fund_channel;
                                    $discount_amount = 0.00;
                                    break;
                                case "BANKCARD":
                                    if ($result->$responseNode->fund_bill_list[0]->fund_channel == 'COUPON') {
                                        $coupon_amount = 0.00;
                                        $coupon_type = '';
                                        $payment_method = $result->$responseNode->fund_bill_list[1]->fund_channel;
                                        $discount_amount = isset($result->$responseNode->fund_bill_list[0]->amount) ? $result->$responseNode->fund_bill_list[0]->amount : 0.00; //三方优惠金额
                                    } else {
                                        $coupon_amount = isset($result->$responseNode->fund_bill_list[0]->amount) ? $result->$responseNode->fund_bill_list[0]->amount : 0.00;
                                        $coupon_type = isset($result->$responseNode->fund_bill_list[0]->fund_channel) ? $result->$responseNode->fund_bill_list[0]->fund_channel : '';
                                        $payment_method = $result->$responseNode->fund_bill_list[1]->fund_channel;
                                        $discount_amount = 0.00;
                                    }
                                    break;
                            }
//                            if($result->$responseNode->fund_bill_list[1]->fund_channel == 'MDISCOUNT'){
//                                $coupon_amount = isset($result->$responseNode->fund_bill_list[1]->amount) ? $result->$responseNode->fund_bill_list[1]->amount : 0.00;
//                                $coupon_type = isset($result->$responseNode->fund_bill_list[1]->fund_channel) ? $result->$responseNode->fund_bill_list[1]->fund_channel : '';
//                                $payment_method = $result->$responseNode->fund_bill_list[0]->fund_channel;
//                            }else{
//                                $coupon_amount = isset($result->$responseNode->fund_bill_list[0]->amount) ? $result->$responseNode->fund_bill_list[0]->amount : 0.00;
//                                $coupon_type = isset($result->$responseNode->fund_bill_list[0]->fund_channel) ? $result->$responseNode->fund_bill_list[0]->fund_channel : '';
//                                $payment_method = $result->$responseNode->fund_bill_list[1]->fund_channel;
//                            }
                        } else {
                            $payment_method = $result->$responseNode->fund_bill_list[0]->fund_channel;
                            $coupon_amount = 0.00;
                            $coupon_type = '';
                            $discount_amount = 0.00;
                        }
                        $update_data = [
                            'trade_no' => $trade_no,
                            'buyer_id' => $buyer_id,
                            'buyer_logon_id' => $buyer_logon_id,
                            'status' => 'TRADE_SUCCESS',
                            'pay_status_desc' => '支付成功',
                            'pay_status' => 1,
                            'payment_method' => $payment_method,
                            'receipt_amount' => $receipt_amount,
                            'fee_amount' => $fee_amount,
                            'mdiscount_amount' => $coupon_amount,
                            'discount_amount' => $discount_amount, //三方优惠金额
                            'coupon_amount' => $coupon_amount,
                            'coupon_type' => $coupon_type
                        ];
                        if ($buyer_pay_amount) $update_data['buyer_pay_amount'] = $buyer_pay_amount;
//                        if ($payment_method) $update_data['coupon_type'] = $payment_method;
//                        if ($amount) $update_data['coupon_amount'] = $amount; 之前的优惠金额
                        $this->update_day_order($update_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $ways->ways_type,
                            'ways_type_desc' => $ways->ways_desc,
                            'company' => $data_insert['company'],
                            'source_type' => '1000',//返佣来源
                            'source_desc' => '支付宝',//返佣来源说明
                            'total_amount' => $total_amount,
                            'out_trade_no' => $out_trade_no,
                            'other_no' => $other_no,
                            'rate' => $data_insert['rate'],
                            'fee_amount' => $fee_amount,
                            'merchant_id' => $merchant_id,
                            'store_id' => $store_id,
                            'user_id' => $tg_user_id,
                            'config_id' => $config_id,
                            'store_name' => $store_name,
                            'ways_source' => $ways->ways_source,
                            'pay_time' => $gmt_payment,
                            'device_id' => $device_id,
                        ];
                        PaySuccessAction::action($data);

                        return json_encode([
                            'status' => 1,
                            'pay_status' => '1',
                            'message' => '支付成功',
                            'data' => [
                                'out_trade_no' => $out_trade_no,
                                'trade_no' => $trade_no,
                                'ways_type' => $ways->ways_type,
                                'ways_source' => $ways->ways_source,
                                'total_amount' => $total_amount,
                                'store_id' => $store_id,
                                'store_name' => $store_name,
                                'config_id' => $config_id,
                                'pay_time' => $gmt_payment,
                                'discount_amount' => 0.00, //第三方优惠金额
                                'promotion_name' => '', //优惠名称
                                'promotion_amount' => $coupon_amount, //优惠总额；单位元，保留两位小数
                            ]
                        ]);
                    }
                    //正在支付
                    if (!empty($resultCode) && $resultCode == 10003) {
                        return json_encode([
                            'status' => 1,
                            'pay_status' => '2',
                            'message' => '正在支付',
                            'data' => [
                                'out_trade_no' => $out_trade_no,
                                'ways_type' => $ways->ways_type,
                                'ways_source' => $ways->ways_source,
                                'total_amount' => $total_amount,
                                'store_id' => $store_id,
                                'store_name' => $store_name,
                                'config_id' => $config_id,
                            ]
                        ]);
                    }
                    $msg = $result->$responseNode->sub_msg; //错误信息

                    //其他支付失败
                    return json_encode([
                        'status' => 2,
                        'pay_status' => '3',
                        'message' => $msg,
                        'data' => [
                            'out_trade_no' => $out_trade_no,
                            'ways_type' => $ways->ways_type,
                            'ways_source' => $ways->ways_source,
                            'total_amount' => $total_amount,
                            'store_id' => $store_id,
                            'store_name' => $store_name,
                            'config_id' => $config_id,
//                            'discount_amount' => $discount_amount, //todo:第三方优惠金额
//                            'promotion_name' => '', //todo:优惠名称
//                            'promotion_amount' => $promotion_amount, //todo:优惠总额；单位元，保留两位小数
                        ]
                    ]);
                }

                //直付通支付宝扫一扫
                if ($ways && $ways->ways_type == 16001) {
                    $data['out_trade_no'] = $out_trade_no;
                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $config_type = '03';

                    //配置
                    $isvconfig = new AlipayIsvConfigController();
                    $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);
                    if (!$config) {
                        return json_encode([
                            'status' => 2,
                            'message' => '直付通配置不存在'
                        ]);
                    }

                    //判断直付通
                    $AlipayZftStore = $isvconfig->AlipayZftStore($store_id, $store_pid);
                    if (!$AlipayZftStore) {
                        return json_encode([
                            'status' => 2,
                            'message' => '直付通门店不存在'
                        ]);
                    }

                    //订单数据库
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    $disable_pay_channels = ''; //禁用方式方式

                    //禁用信用通道
                    if ($ways->pcredit == "00") {
                        $disable_pay_channels = 'credit_group'; //禁用信用方式
                    }

                    $smid = $AlipayZftStore->smid;
                    $notify_url = url('/api/alipayopen/zft_qr_pay_notify');
                    $aop = new AopClient();
                    $aop->apiVersion = "2.0";
                    $aop->appId = $config->app_id;
                    $aop->rsaPrivateKey = $config->rsa_private_key;
                    $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                    $aop->method = 'alipay.trade.pay';
                    $aop->notify_url = $notify_url;
                    $aop->signType = "RSA2"; //升级算法
                    $aop->gatewayUrl = $config->alipay_gateway;
                    $aop->format = "json";
                    $aop->charset = "GBK";
                    $aop->version = "2.0";
                    $requests = new AlipayTradePayRequest();
                    $requests->setNotifyUrl($notify_url);
                    $data_re = array(
                        'out_trade_no' => $out_trade_no,
                        'seller_id' => '',
                        'disable_pay_channels' => $disable_pay_channels,
                        'scene' => 'bar_code',
                        'auth_code' => $code,
                        'subject' => $shop_name,
                        'total_amount' => $total_amount,
                        'timeout_express' => '90m',
                        'body' => $shop_name,
                        'goods_detail' => array(
                            array(
                                'goods_id' => $store_id,
                                'goods_name' => $shop_name,
                                'quantity' => '1',
                                'price' => $total_amount,
                                'body' => $shop_name,
                            )
                        ),
                        'sub_merchant' => array(
                            'merchant_id' => $smid
                        ),

                        'store_id' => '',
                        'shop_id' => '',
                        'terminal_id' => $device_id,
                        'operator_id' => $merchant_id,
                        'extend_params' => array(
                            'sys_service_provider_id' => $config->alipay_pid
                        ),
                    );

                    //01-到卡
                    if ($AlipayZftStore->SettleModeType == "01") {
                        $data_re['settle_info'] = array(
                            'settle_detail_infos' => array(
                                array(
                                    'amount' => $total_amount,
                                    'trans_in_type' => 'cardAliasNo',
                                    'trans_in' => $AlipayZftStore->card_alias_no,
                                ))
                        );
                    } else {
                        $data_re['settle_info'] = array(
                            'settle_detail_infos' => array(
                                array(
                                    'amount' => $total_amount,
                                    'trans_in_type' => 'loginName',
                                    'trans_in' => $AlipayZftStore->alipay_account,
                                ))
                        );
                    }

                    $data_re = json_encode($data_re);

                    $requests->setBizContent($data_re);
                    $result = $aop->execute($requests, null, '');
                    $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
                    $resultCode = $result->$responseNode->code;

                    //异常
                    if ($resultCode == 40004) {
                        return json_encode([
                            'status' => 2,
                            'message' => $result->$responseNode->msg . $result->$responseNode->sub_code,
                            'result_code' => $resultCode,
                        ]);
                    }
                    //支付成功
                    if (!empty($resultCode) && $resultCode == 10000) {
//                        $buyer_id = $result->$responseNode->buyer_user_id;
//                        $buyer_logon_id = $result->$responseNode->buyer_logon_id;
//                        $payment_method = $result->$responseNode->fund_bill_list[0]->fund_channel;
//                        $trade_no = $result->$responseNode->trade_no;
//                        $gmt_payment = $result->$responseNode->gmt_payment;
                        $buyer_id = $result->$responseNode->buyer_user_id; //必选,28,买家在支付宝的用户ID
                        $buyer_logon_id = $result->$responseNode->buyer_logon_id; //必选	,100,买家支付宝账号
                        $payment_method = $result->$responseNode->fund_bill_list[0]->fund_channel; //必填,32,交易使用的资金渠道(COUPON-支付宝红包,ALIPAYACCOUNT-支付宝账户,POINT-集分宝,DISCOUNT-折扣券,PCARD-预付卡,MCARD-商家储值卡;MDISCOUNT-商户优惠券,MCOUPON-商户红包,BANKCARD-银行卡)
                        $amount = $result->$responseNode->fund_bill_list[0]->amount; //必填,32,该支付工具类型所使用的金额
                        $real_amount = isset($result->$responseNode->fund_bill_list[0]->real_amount) ? $result->$responseNode->fund_bill_list[0]->real_amount : 0.00; //可选,11,渠道实际付款金额
                        $trade_no = $result->$responseNode->trade_no; //必选,64,支付宝交易号
                        $gmt_payment = $result->$responseNode->gmt_payment; //必选,32,交易支付时间,2014-11-27 15:45:57
                        $receipt_amount = $result->$responseNode->receipt_amount; //必选,实收金额,2位小数
                        $buyer_pay_amount = isset($result->$responseNode->buyer_pay_amount) ? $result->$responseNode->buyer_pay_amount : 0.00; //可选，买家付款的金额

                        $fee_amount = $ways->rate * $receipt_amount / 100;

                        $update_data = [
                            'trade_no' => $trade_no,
                            'buyer_id' => $buyer_id,
                            'buyer_logon_id' => $buyer_logon_id,
                            'status' => 'TRADE_SUCCESS',
                            'pay_status_desc' => '支付成功',
                            'pay_status' => 1,
                            'payment_method' => $payment_method,
                            'receipt_amount' => $receipt_amount,
                            'fee_amount' => $fee_amount
                        ];
                        if ($buyer_pay_amount) $update_data['buyer_pay_amount'] = $buyer_pay_amount;
                        if ($payment_method) $update_data['coupon_type'] = $payment_method;
                        if ($amount) $update_data['coupon_amount'] = $amount;
                        $this->update_day_order($update_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $ways->ways_type,
                            'ways_type_desc' => $ways->ways_desc,
                            'company' => $data_insert['company'],
                            'source_type' => '16000', //返佣来源
                            'source_desc' => '支付宝-ZFT', //返佣来源说明
                            'total_amount' => $total_amount,
                            'out_trade_no' => $out_trade_no,
                            'other_no' => $other_no,
                            'rate' => $data_insert['rate'],
                            'fee_amount' => $fee_amount,
                            'merchant_id' => $merchant_id,
                            'store_id' => $store_id,
                            'user_id' => $tg_user_id,
                            'config_id' => $config_id,
                            'store_name' => $store_name,
                            'ways_source' => $ways->ways_source,
                            'pay_time' => $gmt_payment,
                            'device_id' => $device_id,
                        ];
                        PaySuccessAction::action($data);

                        return json_encode([
                            'status' => 1,
                            'pay_status' => '1',
                            'message' => '支付成功',
                            'data' => [
                                'out_trade_no' => $out_trade_no,
                                'trade_no' => $trade_no,
                                'ways_type' => $ways->ways_type,
                                'ways_source' => $ways->ways_source,
                                'total_amount' => $total_amount,
                                'store_id' => $store_id,
                                'store_name' => $store_name,
                                'config_id' => $config_id,
                                'pay_time' => $gmt_payment,
                            ]
                        ]);
                    }
                    //正在支付
                    if (!empty($resultCode) && $resultCode == 10003) {
                        return json_encode([
                            'status' => 1,
                            'pay_status' => '2',
                            'message' => '正在支付',
                            'data' => [
                                'out_trade_no' => $out_trade_no,
                                'ways_type' => $ways->ways_type,
                                'ways_source' => $ways->ways_source,
                                'total_amount' => $total_amount,
                                'store_id' => $store_id,
                                'store_name' => $store_name,
                                'config_id' => $config_id,
                            ]
                        ]);
                    }
                    $msg = $result->$responseNode->sub_msg; //错误信息

                    //其他支付失败
                    return json_encode([
                        'status' => 2,
                        'pay_status' => '3',
                        'message' => $msg,
                        'data' => [
                            'out_trade_no' => $out_trade_no,
                            'ways_type' => $ways->ways_type,
                            'ways_source' => $ways->ways_source,
                            'total_amount' => $total_amount,
                            'store_id' => $store_id,
                            'store_name' => $store_name,
                            'config_id' => $config_id,
                        ]
                    ]);
                }

                //京东收银支付宝
                if ($ways && $ways->ways_type == 6001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->jd_pay_public($data_insert, $data_public);

                }

                //随行付支付宝
                if ($ways && $ways->ways_type == 13001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->vbill_pay_public($data_insert, $data_public);

                }

                //随行付a 支付宝
                if ($ways && $ways->ways_type == 19001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->vbilla_pay_public($data_insert, $data_public);
                }

                //快钱支付宝
                if ($ways && $ways->ways_type == 3001) {
                    $data_public = [
                        'pay_type' => 'alipay',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->mybank_pay_public($data_insert, $data_public);
                }

                //新大陆支付宝
                if ($ways && $ways->ways_type == 8001) {
                    $config = new NewLandConfigController();
                    $new_land_config = $config->new_land_config($config_id);
                    if (!$new_land_config) {
                        return json_encode([
                            'status' => 2,
                            'message' => '新大陆配置不存在请检查配置'
                        ]);
                    }

                    $new_land_merchant = $config->new_land_merchant($store_id, $store_pid);
                    if (!$new_land_merchant) {
                        return json_encode([
                            'status' => 2,
                            'message' => '商户新大陆通道未开通'
                        ]);
                    }

                    $request_data = [
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'code' => $code,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'key' => $new_land_merchant->nl_key,
                        'org_no' => $new_land_config->org_no,
                        'merc_id' => $new_land_merchant->nl_mercId,
                        'trm_no' => $new_land_merchant->trmNo,
                        'op_sys' => '3',
                        'opr_id' => $merchant_id,
                        'trm_typ' => 'T',
                        'payChannel' => 'ALIPAY',
                    ];

                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = $ways->ways_source;
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['company'] = $ways->company;
                    $data_insert['out_store_id'] = $new_land_merchant->nl_mercId;

                    return $this->newland_pay_public($data_insert, $request_data);
                }

                //和融通支付宝
                if ($ways && $ways->ways_type == 9001) {
                    $data_public = [
                        'pay_type' => 'ali',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->h_pay_public($data_insert, $data_public);
                }

                //ltf收银支付宝
                if ($ways && $ways->ways_type == 10001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->ltf_pay_public($data_insert, $data_public);
                }

                //富友支付宝
                if ($ways && $ways->ways_type == 11001) {
                    $data_public = [
                        'pay_type' => 'ali',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->fuiou_pay_public($data_insert, $data_public);
                }

                //哆啦宝支付宝
                if ($ways && $ways->ways_type == 15001) {
                    $data_public = [
                        'pay_type' => 'ali',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->dlb_pay_pubulic($data_insert, $data_public);
                }

                //传化支付宝
                if ($ways && $ways->ways_type == 12001) {
                    $data_public = [
                        'channel' => 'ALIPAY_POS',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->tf_pay_pubulic($data_insert, $data_public);
                }

                //联动优势支付宝
                if ($ways && $ways->ways_type == 5001) {
                    $data_public = [
                        'orderType' => 'alipay',
                        'goodsId' => time(),
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id
                    ];

                    return $this->linkage_pay_public($data_insert, $data_public);
                }

                //工行支付宝
                if ($ways && $ways->ways_type == 20001) {
                    $data_public = [
                        'payment' => '2',
                        'goodsId' => time(),
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->lianfu_public($data_insert, $data_public);
                }

                //汇付支付宝
                if ($ways && $ways->ways_type == 18001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $this->hui_pay_public($data_insert, $data_public);
                }

                //海科融通 支付宝 被扫
                if ($ways && $ways->ways_type == 22001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $this->hkrt_pay_public($data_insert, $data_public);
                }

                //易生支付 支付宝 被扫
                if ($ways && $ways->ways_type == 21001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $this->easypay_pay_public($data_insert, $data_public);
                }

                //邮驿付支付 支付宝 被扫
                if ($ways && $ways->ways_type == 29001) {
                    if (strlen($out_trade_no) > 20) {
                        $out_trade_no = substr($out_trade_no, 0, 15) . substr(microtime(), 2, 5);
                    }
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                        'phone' => $store->people_phone
                    ];
                    return $this->post_pay_public($data_insert, $data_public);
                }

                //邮政支付宝
                if ($ways && $ways->ways_type == 26001) {
                    $data_public = [
                        'payment' => '2',
                        'goodsId' => time(),
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->lianfuyoupay_public($data_insert, $data_public);
                }

                //苏州支付宝
                if ($ways && $ways->ways_type == 17001) {
                    $data_public = [
                        'service' => 'ALCardPay',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->sz_pay_public($data_insert, $data_public);
                }

                //葫芦天下支付宝
                if ($ways && $ways->ways_type == 23001) {
                    $data_public = [
                        'pay_type' => 'ali',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->hltx_pay_public($data_insert, $data_public);
                }

                //长沙支付宝
                if ($ways && $ways->ways_type == 25001) {
                    $data_public = [
                        'PayMethod' => '5',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->cs_pay_public($data_insert, $data_public);
                }

                //威富通支付 支付宝 被扫
                if ($ways && $ways->ways_type == 27001) {
                    $out_trade_no = 'aliWft' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_public = [
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->wft_pay_public($data_insert, $data_public);
                }

                //汇旺财支付 支付宝 被扫
                if ($ways && $ways->ways_type == 28001) {
                    $out_trade_no = 'aliHwc' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_public = [
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->hwc_pay_public($data_insert, $data_public);
                }

                //银盛 支付宝 被扫
                if ($ways && $ways->ways_type == 14001) {
                    //$out_trade_no = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_public = [
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->yinsheng_pay_public($data_insert, $data_public);
                }

                //钱方 支付宝 被扫
                if ($ways && $ways->ways_type == 24001) {
                    $out_trade_no = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_public = [
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->qf_pay_public($data_insert, $data_public);
                }

                //云闪付  支付宝
                if ($ways && $ways->ways_type == 27009) {
                    $config = new EasyPayConfigController();
                    $easypay_config = $config->easypay_config($data['config_id']);
                    if (!$easypay_config) {
                        return json_encode([
                            'status' => '2',
                            'message' => '易生支付配置不存在请检查配置'
                        ]);
                    }
                    $sayDodge = new SayDodgePayController();
                    $out_trade_no = substr($easypay_config->client_code, 0, 4) . date('YmdHis', time()) . substr($easypay_config->client_code, -4) . $sayDodge->rand_code();
                    $data_public = [
                        'orderType' => 'ALIPAY',
                        'goodsId' => time(),
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id
                    ];
                    return $this->ysf_pay_public($data_insert, $data_public);
                }

                //建设银行支付 支付宝 被扫
                if ($ways && $ways->ways_type == 31001) {
                    if (strlen($out_trade_no) > 30) {
                        $out_trade_no = substr($out_trade_no, 0, 25) . substr(microtime(), 2, 5);
                    }
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $this->ccBank_pay_public($data_insert, $data_public);
                }

                //易生数科支付  支付宝  被扫
                if ($ways && $ways->ways_type == 32001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $this->easysk_pay_public($data_insert, $data_public);
                }

                //通联支付  支付宝  被扫
                if ($ways && $ways->ways_type == 33001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $this->allin_pay_public($data_insert, $data_public);
                }

                //拉卡拉支付  支付宝  被扫
                if ($ways && $ways->ways_type == 34001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $this->lkl_pay_public($data_insert, $data_public);
                }


            }

            /**微信渠道开始**/
            if (in_array($str, ['13', '14'])) {
                $obj_ways = new PayWaysController();
                $ways = $obj_ways->ways_source('weixin', $store_id, $store_pid);
                Log::info($ways->ways_type);
                if (!$ways) {
                    return json_encode([
                        'status' => 2,
                        'message' => '没有开通此类型通道'
                    ]);
                }

                if ($ways->is_close) {
                    return json_encode([
                        'status' => 2,
                        'message' => '此通道已关闭'
                    ]);
                }

                $user_rate_obj = $obj_ways->getCostRate($ways->ways_type, $tg_user_id);
                $cost_rate = $user_rate_obj ? $user_rate_obj->rate : 0.00; //成本费率

                //扫码费率入库
                if ($cost_rate) $data_insert['cost_rate'] = $cost_rate;
                $data_insert['rate'] = $ways->rate;
                $data_insert['fee_amount'] = ($ways->rate * $total_amount) / 100;

                //如果没有传订单号，就生成
                if (!$out_trade_no) {
                    $out_trade_no = $data['out_trade_no'] ?? 'wxscan' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                }

                //官方微信扫一扫
                if ($ways && $ways->ways_type == 2000) {
                    $config = new WeixinConfigController();
                    $options = $config->weixin_config($config_id);
                    $weixin_store = $config->weixin_merchant($store_id, $store_pid);
                    if (!$weixin_store) {
                        return json_encode([
                            'status' => 2,
                            'message' => '微信商户号不存在'
                        ]);
                    }

                    $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;

                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';

                    $type = $ways->ways_type;
                    $attach = $store_id . ',' . $config_id; //附加信息原样返回

                    //入库参数
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'pay_status' => '3',
                            'message' => '订单未入库'
                        ]);
                    }

                    $config = [
                        'app_id' => $options['app_id'],
                        'mch_id' => $options['payment']['merchant_id'],
                        'key' => $options['payment']['key'],
                        'cert_path' => $options['payment']['cert_path'], // XXX: 绝对路径！！！！
                        'key_path' => $options['payment']['key_path'],     // XXX: 绝对路径！！！！
                        'sub_mch_id' => $wx_sub_merchant_id,
                    ];

                    $payment = Factory::payment($config);

                    if (isset($weixin_store->wx_shop_id) && $weixin_store->wx_shop_id) {
                        $wx_shop_id = $weixin_store->wx_shop_id;
                        $wx_shop_id = explode(',', $wx_shop_id);
                        $goods_detail = [];
                        foreach ($wx_shop_id as $value) {
                            $goods_detail[] =
                                [
                                    'goods_id' => $value,
                                    'quantity' => 1,
                                    'price' => $total_amount * 100
                                ];
                        }

                        $goods_detail = [
                            'goods_detail' => $goods_detail
                        ];

                        $attributes = [
                            'version' => '1.0',
                            'body' => $shop_name,
                            'detail' => json_encode($goods_detail),
                            'out_trade_no' => $out_trade_no,
                            'total_fee' => $total_amount * 100,
                            'auth_code' => $code,
                            'attach' => $attach, //原样返回
                            'device_info' => $device_id,
                        ];
                    } else {
                        $attributes = [
                            'body' => $shop_name,
                            'detail' => $shop_desc,
                            'out_trade_no' => $out_trade_no,
                            'total_fee' => $total_amount * 100,
                            'auth_code' => $code,
                            'attach' => $attach,//原样返回
                            'device_info' => $device_id,
                        ];
                    }

                    $wx_store_obj = WeixinStore::where('store_id', $store_id)->first();
                    if ($wx_store_obj) {
                        $is_profit_sharing = $wx_store_obj->is_profit_sharing; //是否分账(0-不分;1-分)
                        $wx_sharing_rate = $wx_store_obj->wx_sharing_rate; //分账比例%
                        if ($is_profit_sharing && ($wx_sharing_rate > 0)) {
                            $attributes['profit_sharing'] = 'Y'; //String，是否指定服务商分账,Y-是，需要分账；N-否，不分账；字母要求大写，不传默认不分账
                        }
                    } else {
                        Log::info('官方微信扫一扫:' . $store_id . '微信信息未设置');
                    }

                    $result = $payment->pay($attributes);
                    // Log::info('微信支付-被扫-返回结果');
                    // Log::info($result);
                    //请求状态
                    if ($result['return_code'] == 'SUCCESS') {
                        //支付成功
                        if ($result['result_code'] == 'SUCCESS') {
                            //商家优惠券下，使用实际付款金额计算手续费
                            $data_update = [
                                'receipt_amount' => 0, //商家实际收到的款项
                                'status' => $result['result_code'],
                                'pay_status' => 1, //系统状态
                                'pay_status_desc' => '支付成功',
                                'payment_method' => $result['bank_type'],
                                'buyer_id' => $result['openid'],
                                'trade_no' => $result['transaction_id'],
                            ];
                            if (isset($result['settlement_total_fee']) && !empty($result['settlement_total_fee'])) {
                                $data_update['receipt_amount'] = $result['settlement_total_fee'] / 100;
                                $data_update['mdiscount_amount'] = $result['coupon_fee'] / 100;
                                $fee_amount = ($data_insert['rate'] * $result['settlement_total_fee']) / 10000;
                            } else {
                                $data_update['receipt_amount'] = $result['total_fee'] / 100;
                                if (isset($result['coupon_fee']) && !empty($result['coupon_fee'])) {
                                    $data_update['discount_amount'] = $result['coupon_fee'] / 100;
                                }
                                $fee_amount = ($data_insert['rate'] * $result['total_fee']) / 10000;
                            }
                            // if (isset($result['cash_fee']) && !empty($result['cash_fee'])) {
                            //     $data_update['receipt_amount'] = $result['cash_fee']/100;
                            //     $fee_amount = ($data_insert['rate']*$result['cash_fee'])/10000;
                            // } else {
                            //     $fee_amount = 0;
                            // }
                            if (isset($result['coupon_fee'])) {
                                $data_update['coupon_amount'] = $result['coupon_fee'] / 100;
                                $receipt_amount = $result['total_fee'] - $result['coupon_fee'];
                                $data_update['buyer_pay_amount'] = $receipt_amount / 100;
                                $data_update['receipt_amount'] = $receipt_amount / 100;
                                $fee_amount = ($data_insert['rate'] * $receipt_amount) / 10000;
                            } else {
                                $fee_amount = ($data_insert['rate'] * $result['total_fee']) / 10000;
                            }
                            $update_re = $this->update_day_order($data_update, $out_trade_no);
                            if (!$update_re) {
                                return json_encode([
                                    'status' => 2,
                                    'message' => '订单更新入库失败'
                                ]);
                            }

                            //支付成功后的动作
                            $data = [
                                'ways_type' => $ways->ways_type,
                                'ways_type_desc' => $ways->ways_desc,
                                'company' => $data_insert['company'],
                                'source_type' => '2000',//返佣来源
                                'source_desc' => '微信支付',//返佣来源说明
                                'total_amount' => (isset($result['total_fee']) && !empty($result['total_fee'])) ? $result['total_fee'] : $total_amount,
                                'out_trade_no' => $out_trade_no,
                                'other_no' => $data_insert['other_no'],
                                'rate' => $data_insert['rate'],
                                'fee_amount' => !empty($fee_amount) ? $fee_amount : $data_insert['fee_amount'],
                                'merchant_id' => $merchant_id,
                                'store_id' => $store_id,
                                'user_id' => $tg_user_id,
                                'config_id' => $config_id,
                                'store_name' => $store_name,
                                'ways_source' => $ways->ways_source,
                                'pay_time' => $result['time_end'],
                                'device_id' => $device_id,
                            ];
                            PaySuccessAction::action($data);

                            //微信分账
                            $is_profit_sharing = $weixin_store->is_profit_sharing; //是否分账(0-不分;1-分)
                            $fz_rate = $weixin_store->wx_sharing_rate; //分账比例
                            if ($is_profit_sharing && ($fz_rate > 0)) {
                                $wx_pay_obj = new \App\Api\Controllers\Weixin\BaseController();
                                $profit_sharing_data = [
                                    'user_id' => $tg_user_id,
                                    'out_trade_no' => $out_trade_no,
                                    'config_id' => $config_id,
                                    'store_id' => $store_id,
                                    'options' => $options,
                                    'wx_sub_merchant_id' => $wx_sub_merchant_id, //微信支付分配的子商户号
                                    'transaction_id' => $result['transaction_id'], //微信支付订单号
                                    'total_amount' => $total_amount, //订单总金额
                                    'ways_type' => $ways->ways_type
                                ];

                                //TODO:调用支付成功之后，进行分账的队列

                                $profit_sharing_res = $wx_pay_obj->profit_sharing($profit_sharing_data);
                                Log::info('官方微信扫一扫,单次分账');
                                Log::info($profit_sharing_data);
                                Log::info($profit_sharing_res);
                            }

                            return json_encode([
                                'status' => 1,
                                'pay_status' => '1',
                                'message' => '支付成功',
                                'data' => [
                                    'out_trade_no' => $out_trade_no,
                                    'trade_no' => $result['transaction_id'],
                                    'ways_type' => $ways->ways_type,
                                    'ways_source' => $ways->ways_source,
                                    'total_amount' => $total_amount,
                                    'store_id' => $store_id,
                                    'store_name' => $store_name,
                                    'config_id' => $config_id,
                                    'pay_time' => $result['time_end'],
                                    'discount_amount' => 0, //todo:第三方优惠金额
                                    'promotion_name' => '', //todo:优惠名称
                                    'promotion_amount' => isset($result['coupon_fee']) ? $result['coupon_fee'] / 100 : 0, //todo:优惠总额；单位元，保留两位小数
                                ]
                            ]);
                        } else {
                            if ($result['err_code'] == "USERPAYING") {
                                return json_encode([
                                    'status' => 1,
                                    'pay_status' => '2',
                                    'message' => '正在支付',
                                    'data' => [
                                        'out_trade_no' => $out_trade_no,
                                        'ways_type' => $ways->ways_type,
                                        'ways_source' => $ways->ways_source,
                                        'total_amount' => $total_amount,
                                        'store_id' => $store_id,
                                        'store_name' => $store_name,
                                        'config_id' => $config_id,
                                    ]
                                ]);
                            } else {
                                $msg = $result['err_code_des']; //错误信息
                                return json_encode([
                                    'status' => 2,
                                    'message' => $msg,
                                    'data' => [
                                        'out_trade_no' => $out_trade_no,
                                        'ways_type' => $ways->ways_type,
                                        'ways_source' => $ways->ways_source,
                                        'total_amount' => $total_amount,
                                        'store_id' => $store_id,
                                        'store_name' => $store_name,
                                        'config_id' => $config_id,
                                    ]
                                ]);
                            }
                        }
                    } else {
                        $data = [
                            'status' => 2,
                            "message" => $result['return_msg'],
                            'data' => [
                                'out_trade_no' => $out_trade_no,
                                'ways_type' => $ways->ways_type,
                                'ways_source' => $ways->ways_source,
                                'total_amount' => $total_amount,
                                'store_id' => $store_id,
                                'store_name' => $store_name,
                                'config_id' => $config_id,
                            ]
                        ];
                    }

                    return json_encode($data);
                }

                //官方微信a 扫一扫
                if ($ways && $ways->ways_type == 4000) {
                    $config = new WeixinConfigController();
                    $options = $config->weixina_config($config_id);
                    $weixin_store = $config->weixina_merchant($store_id, $store_pid);
                    if (!$weixin_store) {
                        return json_encode([
                            'status' => 2,
                            'message' => '官方微信a商户号不存在'
                        ]);
                    }

                    $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;

                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_type_desc'] = '微信支付a';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';

                    $type = $ways->ways_type;
                    $attach = $store_id . ',' . $config_id; //附加信息原样返回

                    //入库参数
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'pay_status' => '3',
                            'message' => '订单未入库'
                        ]);
                    }

                    $config = [
                        'app_id' => $options['app_id'],
                        'mch_id' => $options['payment']['merchant_id'],
                        'key' => $options['payment']['key'],
                        'cert_path' => $options['payment']['cert_path'], // XXX: 绝对路径！！！！
                        'key_path' => $options['payment']['key_path'],     // XXX: 绝对路径！！！！
                        'sub_mch_id' => $wx_sub_merchant_id,
                    ];
                    $payment = Factory::payment($config);

                    if (isset($weixin_store->wx_shop_id) && $weixin_store->wx_shop_id) {
                        $wx_shop_id = $weixin_store->wx_shop_id;
                        $wx_shop_id = explode(',', $wx_shop_id);
                        $goods_detail = [];
                        foreach ($wx_shop_id as $value) {
                            $goods_detail[] = [
                                'goods_id' => $value,
                                'quantity' => 1,
                                'price' => $total_amount * 100
                            ];
                        }

                        $goods_detail = [
                            'goods_detail' => $goods_detail
                        ];

                        $attributes = [
                            'version' => '1.0',
                            'body' => $shop_name,
                            'detail' => json_encode($goods_detail),
                            'out_trade_no' => $out_trade_no,
                            'total_fee' => $total_amount * 100,
                            'auth_code' => $code,
                            'attach' => $attach, //原样返回
                            'device_info' => $device_id,
                        ];
                    } else {
                        $attributes = [
                            'body' => $shop_name,
                            'detail' => $shop_desc,
                            'out_trade_no' => $out_trade_no,
                            'total_fee' => $total_amount * 100,
                            'auth_code' => $code,
                            'attach' => $attach,//原样返回
                            'device_info' => $device_id,
                        ];
                    }

                    $wx_store_obj = WeixinaStore::where('store_id', $store_id)->first();
                    if ($wx_store_obj) {
                        $is_profit_sharing = $wx_store_obj->is_profit_sharing; //是否分账(0-不分;1-分)
                        $wx_sharing_rate = $wx_store_obj->wx_sharing_rate; //分账比例%
                        if ($is_profit_sharing && ($wx_sharing_rate > 0)) {
                            $attributes['profit_sharing'] = 'Y'; //String，是否指定服务商分账,Y-是，需要分账；N-否，不分账；字母要求大写，不传默认不分账
                        }
                    } else {
                        Log::info('官方微信扫一扫:' . $store_id . '微信信息未设置');
                    }

                    $result = $payment->pay($attributes);
                    // Log::info('微信支付a-被扫-返回结果');
                    // Log::info($result);

                    //请求状态
                    if ($result['return_code'] == 'SUCCESS') {
                        //支付成功
                        if ($result['result_code'] == 'SUCCESS') {
//                            Log::info('微信a-被扫-成功');
//                            Log::info($result);
                            //商家优惠券下，使用实际付款金额计算手续费
                            $data_update = [
                                'receipt_amount' => 0, //商家实际收到的款项
                                'status' => $result['result_code'],
                                'pay_status' => 1, //系统状态
                                'pay_status_desc' => '支付成功',
                                'payment_method' => $result['bank_type'],
                                'buyer_id' => $result['openid'],
                                'trade_no' => $result['transaction_id'],
                            ];

                            if (isset($result['settlement_total_fee']) && !empty($result['settlement_total_fee'])) {
                                $data_update['receipt_amount'] = $result['settlement_total_fee'] / 100;
                                $data_update['mdiscount_amount'] = $result['coupon_fee'] / 100;
                                $fee_amount = ($data_insert['rate'] * $result['settlement_total_fee']) / 10000;
                            } else {
                                $data_update['receipt_amount'] = $result['total_fee'] / 100;
                                if (isset($result['coupon_fee']) && !empty($result['coupon_fee'])) {
                                    $data_update['discount_amount'] = $result['coupon_fee'] / 100;
                                }
                                $fee_amount = ($data_insert['rate'] * $result['total_fee']) / 10000;
                            }
//                            if (isset($result['settlement_total_fee']) && !empty($result['settlement_total_fee'])) {
//                                $data_update['receipt_amount'] = $result['settlement_total_fee']/100;
//                                $fee_amount = ($data_insert['rate']*$result['settlement_total_fee'])/10000;
//                            } else {
//                                $fee_amount = 0;
//                            }
                            if (isset($result['coupon_fee'])) {
                                $data_update['coupon_amount'] = $result['coupon_fee'] / 100;
                                $receipt_amount = $result['total_fee'] - $result['coupon_fee'];
                                $data_update['buyer_pay_amount'] = $receipt_amount / 100;
                                $data_update['receipt_amount'] = $receipt_amount / 100;
                                $fee_amount = ($data_insert['rate'] * $receipt_amount) / 10000;
                            } else {
                                $fee_amount = ($data_insert['rate'] * $result['total_fee']) / 10000;
                            }
                            $update_re = $this->update_day_order($data_update, $out_trade_no);
                            if (!$update_re) {
                                return json_encode([
                                    'status' => 2,
                                    'message' => '订单更新入库失败'
                                ]);
                            }

                            //支付成功后的动作
                            $data = [
                                'ways_type' => $ways->ways_type,
                                'ways_type_desc' => $ways->ways_desc,
                                'company' => $data_insert['company'],
                                'source_type' => '4000', //返佣来源
                                'source_desc' => '微信支付a', //返佣来源说明
                                'total_amount' => $total_amount,
                                'out_trade_no' => $out_trade_no,
                                'other_no' => $data_insert['other_no'],
                                'rate' => $data_insert['rate'],
                                'fee_amount' => !empty($fee_amount) ? $fee_amount : $data_insert['fee_amount'],
                                'merchant_id' => $merchant_id,
                                'store_id' => $store_id,
                                'user_id' => $tg_user_id,
                                'config_id' => $config_id,
                                'store_name' => $store_name,
                                'ways_source' => $ways->ways_source,
                                'pay_time' => $result['time_end'],
                                'device_id' => $device_id,
                            ];
                            PaySuccessAction::action($data);

                            //微信分账
                            $is_profit_sharing = $weixin_store->is_profit_sharing; //是否分账(0-不分;1-分)
                            $fz_rate = $weixin_store->wx_sharing_rate; //分账比例
                            if ($is_profit_sharing && ($fz_rate > 0)) {
                                $wx_pay_obj = new WeixinPayController();
                                $profit_sharing_data = [
                                    'user_id' => $tg_user_id,
                                    'out_trade_no' => $out_trade_no,
                                    'config_id' => $config_id,
                                    'store_id' => $store_id,
                                    'options' => $options,
                                    'wx_sub_merchant_id' => $wx_sub_merchant_id, //微信支付分配的子商户号
                                    'transaction_id' => $result['transaction_id'], //微信支付订单号
                                    'total_amount' => $total_amount, //订单总金额
                                    'ways_type' => $ways->ways_type
                                ];

                                //TODO:调用支付成功之后，进行分账的队列

                                $profit_sharing_res = $wx_pay_obj->profit_sharing($profit_sharing_data);
                                Log::info('官方微信a扫一扫,单次分账');
                                Log::info($profit_sharing_data);
                                Log::info($profit_sharing_res);
                            }

                            return json_encode([
                                'status' => 1,
                                'pay_status' => '1',
                                'message' => '支付成功',
                                'data' => [
                                    'out_trade_no' => $out_trade_no,
                                    'trade_no' => $result['transaction_id'],
                                    'ways_type' => $ways->ways_type,
                                    'ways_source' => $ways->ways_source,
                                    'total_amount' => $total_amount,
                                    'store_id' => $store_id,
                                    'store_name' => $store_name,
                                    'config_id' => $config_id,
                                    'pay_time' => $result['time_end'],
                                    'discount_amount' => 0, //todo:第三方优惠金额
                                    'promotion_name' => '', //todo:优惠名称
                                    'promotion_amount' => isset($result['coupon_fee']) ? $result['coupon_fee'] / 100 : 0, //todo:优惠总额；单位元，保留两位小数
                                ]
                            ]);
                        } else {
                            if ($result['err_code'] == "USERPAYING") {
                                return json_encode([
                                    'status' => 1,
                                    'pay_status' => '2',
                                    'message' => '正在支付',
                                    'data' => [
                                        'out_trade_no' => $out_trade_no,
                                        'ways_type' => $ways->ways_type,
                                        'ways_source' => $ways->ways_source,
                                        'total_amount' => $total_amount,
                                        'store_id' => $store_id,
                                        'store_name' => $store_name,
                                        'config_id' => $config_id,
                                    ]
                                ]);
                            } else {
                                $msg = $result['err_code_des']; //错误信息
                                return json_encode([
                                    'status' => 2,
                                    'message' => $msg,
                                    'data' => [
                                        'out_trade_no' => $out_trade_no,
                                        'ways_type' => $ways->ways_type,
                                        'ways_source' => $ways->ways_source,
                                        'total_amount' => $total_amount,
                                        'store_id' => $store_id,
                                        'store_name' => $store_name,
                                        'config_id' => $config_id,
                                    ]
                                ]);
                            }
                        }
                    } else {
                        $data = [
                            'status' => 2,
                            "message" => $result['return_msg'],
                            'data' => [
                                'out_trade_no' => $out_trade_no,
                                'ways_type' => $ways->ways_type,
                                'ways_source' => $ways->ways_source,
                                'total_amount' => $total_amount,
                                'store_id' => $store_id,
                                'store_name' => $store_name,
                                'config_id' => $config_id,
                            ]
                        ];
                    }

                    return json_encode($data);
                }

                //京东收银-微信
                if ($ways && $ways->ways_type == 6002) {
                    $data_public = [
                        'pay_type' => 'WX',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->jd_pay_public($data_insert, $data_public);
                }

                //随行付-微信
                if ($ways && $ways->ways_type == 13002) {
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'merchant_name' => $merchant_name,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->vbill_pay_public($data_insert, $data_public);
                }

                //随行付a-微信
                if ($ways && $ways->ways_type == 19002) {
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'merchant_name' => $merchant_name,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->vbilla_pay_public($data_insert, $data_public);
                }

                //快钱微信
                if ($ways && $ways->ways_type == 3002) {
                    $data_public = [
                        'pay_type' => 'weixin',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->mybank_pay_public($data_insert, $data_public);
                }

                //新大陆微信
                if ($ways && $ways->ways_type == 8002) {
                    $config = new NewLandConfigController();
                    $new_land_config = $config->new_land_config($config_id);
                    if (!$new_land_config) {
                        return json_encode([
                            'status' => 2,
                            'message' => '新大陆配置不存在请检查配置'
                        ]);
                    }

                    $new_land_merchant = $config->new_land_merchant($store_id, $store_pid);
                    if (!$new_land_merchant) {
                        return json_encode([
                            'status' => 2,
                            'message' => '商户新大陆通道未开通'
                        ]);
                    }
                    $request_data = [
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'code' => $code,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'key' => $new_land_merchant->nl_key,
                        'org_no' => $new_land_config->org_no,
                        'merc_id' => $new_land_merchant->nl_mercId,
                        'trm_no' => $new_land_merchant->trmNo,
                        'op_sys' => '3',
                        'opr_id' => $merchant_id,
                        'trm_typ' => 'T',
                        'payChannel' => 'WXPAY',
                    ];

                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = $ways->ways_source;
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['company'] = $ways->company;
                    $data_insert['out_store_id'] = $new_land_merchant->nl_mercId;

                    return $this->newland_pay_public($data_insert, $request_data);
                }

                //和融通微信
                if ($ways && $ways->ways_type == 9002) {
                    $data_public = [
                        'pay_type' => 'wx',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->h_pay_public($data_insert, $data_public);
                }

                //ltf收银微信
                if ($ways && $ways->ways_type == 10002) {
                    $data_public = [
                        'pay_type' => 'WEIXIN',
                        'return_params' => '原样返回',
                        'title' => '微信支付',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->ltf_pay_public($data_insert, $data_public);

                }

                //富友微信
                if ($ways && $ways->ways_type == 11002) {
                    $data_public = [
                        'pay_type' => 'wx',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->fuiou_pay_public($data_insert, $data_public);
                }

                //哆啦宝微信
                if ($ways && $ways->ways_type == 15002) {
                    $data_public = [
                        'pay_type' => 'wx',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->dlb_pay_pubulic($data_insert, $data_public);
                }

                //传化微信
                if ($ways && $ways->ways_type == 12002) {
                    $data_public = [
                        'channel' => 'WECHAT_POS',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->tf_pay_pubulic($data_insert, $data_public);
                }

                //工行微信
                if ($ways && $ways->ways_type == 20002) {
                    $data_public = [
                        'payment' => '1',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->lianfu_public($data_insert, $data_public);
                }

                //汇付-微信
                if ($ways && $ways->ways_type == 18002) {
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->hui_pay_public($data_insert, $data_public);
                }

                //海科融通 微信 被扫
                if ($ways && $ways->ways_type == 22002) {
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->hkrt_pay_public($data_insert, $data_public);
                }

                //易生支付 微信 被扫
                if ($ways && $ways->ways_type == 21002) {
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->easypay_pay_public($data_insert, $data_public);
                }

                //邮驿付支付 微信 被扫
                if ($ways && $ways->ways_type == 29002) {
                    if (strlen($out_trade_no) > 20) {
                        $out_trade_no = substr($out_trade_no, 0, 15) . substr(microtime(), 2, 5);
                    }

                    $data_public = [
                        'pay_type' => 'WXPAY',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                        'phone' => $store->people_phone
                    ];

                    return $this->post_pay_public($data_insert, $data_public);
                }

                //邮政微信
                if ($ways && $ways->ways_type == 26002) {
                    $data_public = [
                        'payment' => '1',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->lianfuyoupay_public($data_insert, $data_public);
                }

                //苏州银行微信
                if ($ways && $ways->ways_type == 17002) {
                    $data_public = [
                        'service' => 'WXCardPay',
                        'return_params' => '原样返回',
                        'title' => '微信支付',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->sz_pay_public($data_insert, $data_public);
                }

                //葫芦天下微信
                if ($ways && $ways->ways_type == 23002) {
                    $data_public = [
                        'pay_type' => 'wx',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->hltx_pay_public($data_insert, $data_public);
                }

                //长沙银行微信
                if ($ways && $ways->ways_type == 25002) {
                    $data_public = [
                        'PayMethod' => '7',
                        'return_params' => '原样返回',
                        'title' => '微信支付',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->cs_pay_public($data_insert, $data_public);
                }

                //联动优势 微信 被扫
                if ($ways && $ways->ways_type == 5002) {
                    $data_public = [
                        'orderType' => 'wechat',
                        'goodsId' => time(),
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id
                    ];

                    return $this->linkage_pay_public($data_insert, $data_public);
                }

                //威富通 微信 被扫
                if ($ways && $ways->ways_type == 27002) {
                    $out_trade_no = 'wxWft' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_public = [
                        'orderType' => 'weChat',
                        'goodsId' => time(),
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id
                    ];

                    return $this->wft_pay_public($data_insert, $data_public);
                }

                //汇旺财 微信 被扫
                if ($ways && $ways->ways_type == 28002) {
                    $out_trade_no = 'wxHwc' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_public = [
                        'orderType' => 'weChat',
                        'goodsId' => time(),
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id
                    ];

                    return $this->hwc_pay_public($data_insert, $data_public);
                }

                //银盛 微信 被扫
                if ($ways && $ways->ways_type == 14002) {
                    //$out_trade_no = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_public = [
                        'orderType' => 'weChat',
                        'goodsId' => time(),
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id
                    ];

                    return $this->yinsheng_pay_public($data_insert, $data_public);
                }

                //钱方 微信 被扫
                if ($ways && $ways->ways_type == 24002) {
                    $out_trade_no = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_public = [
                        'orderType' => 'weChat',
                        'goodsId' => time(),
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id
                    ];

                    return $this->qf_pay_public($data_insert, $data_public);
                }

                //云闪付  微信
                if ($ways && $ways->ways_type == 27008) {
                    $config = new EasyPayConfigController();
                    $easypay_config = $config->easypay_config($data['config_id']);
                    if (!$easypay_config) {
                        return json_encode([
                            'status' => '2',
                            'message' => '易生支付配置不存在请检查配置'
                        ]);
                    }
                    $sayDodge = new SayDodgePayController();
                    $out_trade_no = substr($easypay_config->client_code, 0, 4) . date('YmdHis', time()) . substr($easypay_config->client_code, -4) . $sayDodge->rand_code();
                    $data_public = [
                        'orderType' => 'weChat',
                        'goodsId' => time(),
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id
                    ];
                    return $this->ysf_pay_public($data_insert, $data_public);
                }

                //建设银行支付 微信 被扫
                if ($ways && $ways->ways_type == 31002) {
                    if (strlen($out_trade_no) > 30) {
                        $out_trade_no = substr($out_trade_no, 0, 25) . substr(microtime(), 2, 5);
                    }
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->ccBank_pay_public($data_insert, $data_public);
                }

                //易生数科支付 微信 被扫
                if ($ways && $ways->ways_type == 32002) {
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->easysk_pay_public($data_insert, $data_public);
                }

                //通联支付 微信 被扫
                if ($ways && $ways->ways_type == 33002) {
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->allin_pay_public($data_insert, $data_public);
                }

                //拉卡拉支付 微信 被扫
                if ($ways && $ways->ways_type == 34002) {
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->lkl_pay_public($data_insert, $data_public);
                }

            }

            //银联或者京东支付
            if (in_array($str, ['62'])) {
                $obj_ways = new PayWaysController();
                $ways = $obj_ways->ways_source_un_qr('unionpay', $store_id, $store_pid);
                if (!$ways) {
                    return json_encode([
                        'status' => 2,
                        'message' => '没有开通此类型通道'
                    ]);
                }

                if ($ways->is_close) {
                    return json_encode([
                        'status' => 2,
                        'message' => '此通道已关闭'
                    ]);
                }

                $user_rate_obj = $obj_ways->getCostRate($ways->ways_type, $tg_user_id);
                $cost_rate = $user_rate_obj ? $user_rate_obj->rate : 0.00; //成本费率

                //扫码费率入库
                if ($cost_rate) $data_insert['cost_rate'] = $cost_rate;
                $data_insert['rate'] = $ways->rate;
                $data_insert['fee_amount'] = $ways->rate * $total_amount / 100;
                if (!$out_trade_no) {
                    $out_trade_no = 'unscan' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                }

                //京东收银-京东支付
                if ($ways && $ways->ways_type == 6003) {
                    $data_public = [
                        'pay_type' => 'JDPAY',
                        'return_params' => '原样返回',
                        'title' => '京东金融',
                        'ways_source_desc' => '京东金融',
                        'ways_type_desc' => '京东金融',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->jd_pay_public($data_insert, $data_public);
                }

                //哆啦宝-京东支付
                if ($ways && $ways->ways_type == 15003) {
                    $data_public = [
                        'pay_type' => 'jd',
                        'return_params' => '原样返回',
                        'title' => '京东支付',
                        'ways_source_desc' => '京东支付',
                        'ways_type_desc' => '京东支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->dlb_pay_pubulic($data_insert, $data_public);
                }

                //威富通 京东 被扫
                if ($ways && $ways->ways_type == 27003) {
                    $out_trade_no = 'unWft' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_public = [
                        'orderType' => 'jd',
                        'goodsId' => time(),
                        'return_params' => '原样返回',
                        'title' => '京东支付',
                        'ways_source_desc' => '京东支付',
                        'ways_type_desc' => '京东支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id
                    ];

                    return $this->wft_pay_public($data_insert, $data_public);
                }

                //汇旺财 京东 被扫
                if ($ways && $ways->ways_type == 28003) {
                    $out_trade_no = 'unHwc' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_public = [
                        'orderType' => 'jd',
                        'goodsId' => time(),
                        'return_params' => '原样返回',
                        'title' => '京东支付',
                        'ways_source_desc' => '京东支付',
                        'ways_type_desc' => '京东支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id
                    ];
                    return $this->hwc_pay_public($data_insert, $data_public);
                }

                //哆啦宝-银联
                if ($ways && $ways->ways_type == 15004) {
                    $data_public = [
                        'pay_type' => 'jd',
                        'return_params' => '原样返回',
                        'title' => '京东支付',
                        'ways_source_desc' => '京东支付',
                        'ways_type_desc' => '京东支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->dlb_pay_pubulic($data_insert, $data_public);
                }

                //京东 银联
                if ($ways && $ways->ways_type == 6004) {
                    if ($total_amount < 1000) {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_a;
                        $data_insert['fee_amount'] = $ways->rate_a * $total_amount / 100;
                    } else {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_c;
                        $data_insert['fee_amount'] = $ways->rate_c * $total_amount / 100;
                    }

                    $data_public = [
                        'pay_type' => 'JDPAY',
                        'return_params' => '原样返回',
                        'title' => '银联扫码',
                        'ways_source_desc' => '银联扫码',
                        'ways_type_desc' => '银联扫码',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->jd_pay_public($data_insert, $data_public);
                }

                //新大陆银联
                if ($ways && $ways->ways_type == 8004) {
                    $config = new NewLandConfigController();
                    $new_land_config = $config->new_land_config($config_id);
                    if (!$new_land_config) {
                        return json_encode([
                            'status' => 2,
                            'message' => '新大陆配置不存在请检查配置'
                        ]);
                    }

                    $new_land_merchant = $config->new_land_merchant($store_id, $store_pid);
                    if (!$new_land_merchant) {
                        return json_encode([
                            'status' => 2,
                            'message' => '商户新大陆通道未开通'
                        ]);
                    }

                    if ($total_amount < 1000) {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_a;
                        $data_insert['fee_amount'] = $ways->rate_a * $total_amount / 100;
                    } else {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_c;
                        $data_insert['fee_amount'] = $ways->rate_c * $total_amount / 100;
                    }

                    $request_data = [
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'code' => $code,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'key' => $new_land_merchant->nl_key,
                        'org_no' => $new_land_config->org_no,
                        'merc_id' => $new_land_merchant->nl_mercId,
                        'trm_no' => $new_land_merchant->trmNo,
                        'op_sys' => '3',
                        'opr_id' => $merchant_id,
                        'trm_typ' => 'T',
                        'payChannel' => 'YLPAY',
                    ];

                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_source'] = $ways->ways_source;
                    $data_insert['ways_source_desc'] = '银联扫码';

                    return $this->newland_pay_public($data_insert, $request_data);
                }

                //汇付银联
                if ($ways && $ways->ways_type == 18004) {
                    $config = new HuiPayConfigController();
                    $huipay_config = $config->hui_pay_config($config_id);
                    if (!$huipay_config) {
                        return json_encode([
                            'status' => 2,
                            'message' => '汇付配置不存在请检查配置'
                        ]);
                    }

                    $hui_pay_merchant = $config->hui_pay_merchant($store_id, $store_pid);
                    if (!$hui_pay_merchant) {
                        return json_encode([
                            'status' => 2,
                            'message' => '商户汇付通道未开通'
                        ]);
                    }

                    if ($total_amount < 1000) {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_a;
                        $data_insert['fee_amount'] = $ways->rate_a * $total_amount / 100;
                    } else {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_c;
                        $data_insert['fee_amount'] = $ways->rate_c * $total_amount / 100;
                    }

                    $request_data = [
                        'pay_type' => 'YLPAY',
                        'return_params' => '原样返回',
                        'title' => '银联扫码收款',
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'code' => $code,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'merc_id' => $hui_pay_merchant->merc_id,
                        'trm_no' => $hui_pay_merchant->trmNo,
                        'op_sys' => '3',
                        'trm_typ' => 'T',
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_source'] = $ways->ways_source;
                    $data_insert['ways_source_desc'] = '银联扫码';

                    return $this->hui_pay_public($data_insert, $request_data);
                }

                //海科融通 银联扫码
                if ($ways && $ways->ways_type == 22004) {
                    $config = new HkrtConfigController();
                    $hkrt_config = $config->hkrt_config($config_id);
                    if (!$hkrt_config) {
                        return json_encode([
                            'status' => 2,
                            'message' => '海科融通支付配置不存在，请检查配置'
                        ]);
                    }

                    $hkrt_merchant = $config->hkrt_merchant($store_id, $store_pid);
                    if (!$hkrt_merchant) {
                        return json_encode([
                            'status' => 2,
                            'message' => '海科融通通道未开通'
                        ]);
                    }

                    $request_data = [
                        'pay_type' => 'YLPAY',
                        'return_params' => '原样返回',
                        'title' => '银联扫码收款',
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'code' => $code,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'merch_no' => $hkrt_merchant->merch_no,
                        'agent_apply_no' => $hkrt_merchant->agent_apply_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_source'] = $ways->ways_source;
                    $data_insert['ways_source_desc'] = '银联扫码';

                    return $this->hkrt_pay_public($data_insert, $request_data);
                }

                //易生支付 银联扫码
//                if ($ways && $ways->ways_type == 21004) {
//                    $config = new EasyPayConfigController();
//                    $easypay_config = $config->easypay_config($config_id);
//                    if (!$easypay_config) {
//                        return json_encode([
//                            'status' => '2',
//                            'message' => '易生支付配置不存在请检查配置'
//                        ]);
//                    }
//
//                    $easypay_merchant = $config->easypay_merchant($store_id, $store_pid);
//                    if (!$easypay_merchant) {
//                        return json_encode([
//                            'status' => '2',
//                            'message' => '商户易生支付通道未开通'
//                        ]);
//                    }
//
//                    if ($total_amount < 1000) {
//                        //扫码费率入库
//                        $data_insert['rate'] = $ways->rate_a;
//                        $data_insert['fee_amount'] = $ways->rate_a * $total_amount / 100;
//                    } else {
//                        //扫码费率入库
//                        $data_insert['rate'] = $ways->rate_c;
//                        $data_insert['fee_amount'] = $ways->rate_c * $total_amount / 100;
//                    }
//
//                    $request_data = [
//                        'pay_type' => 'UNIONPAY',
//                        'return_params' => '原样返回',
//                        'title' => '银联扫码收款',
//                        'ways_source_desc' => '银联扫码',
//                        'ways_type_desc' => '银联扫码',
//                        'code' => $code,
//                        'out_trade_no' => $out_trade_no,
//                        'other_no' => $other_no,
//                        'config_id' => $config_id,
//                        'store_id' => $store_id,
//                        'store_pid' => $store_pid,
//                        'ways_type' => '' . $ways->ways_type . '',
//                        'ways_source' => $ways->ways_source,
//                        'company' => $ways->company,
//                        'total_amount' => $total_amount,
//                        'remark' => $remark,
//                        'device_id' => $device_id,
//                        'shop_name' => $shop_name,
//                        'merchant_id' => $merchant_id,
//                        'store_name' => $store_name,
//                        'tg_user_id' => $tg_user_id,
//                    ];
//
//                    //入库参数
//                    $data_insert['out_trade_no'] = $out_trade_no;
//                    $data_insert['ways_type'] = $ways->ways_type;
//                    $data_insert['ways_type_desc'] = '银联扫码';
//                    $data_insert['company'] = $ways->company;
//                    $data_insert['ways_source'] = $ways->ways_source;
//                    $data_insert['ways_source_desc'] = '银联扫码';
//
//                    return $this->easypay_pay_public($data_insert, $request_data);
//                }

                //邮驿付支付 银联扫码
                if ($ways && $ways->ways_type == 29003) {
                    $config = new PostPayConfigController();
                    $post_config = $config->post_pay_config($config_id);
                    if (!$post_config) {
                        return json_encode([
                            'status' => '2',
                            'message' => '邮驿付支付配置不存在请检查配置'
                        ]);
                    }

                    $post_merchant = $config->post_pay_merchant($store_id, $store_pid);
                    if (!$post_merchant) {
                        return json_encode([
                            'status' => '2',
                            'message' => '商户邮驿付支付通道未开通'
                        ]);
                    }

                    if ($total_amount < 1000) {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_a;
                        $data_insert['fee_amount'] = $ways->rate_a * $total_amount / 100;
                    } else {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_c;
                        $data_insert['fee_amount'] = $ways->rate_c * $total_amount / 100;
                    }
                    if (strlen($out_trade_no) > 20) {
                        $out_trade_no = substr($out_trade_no, 0, 15) . substr(microtime(), 2, 5);
                    }
                    $request_data = [
                        'pay_type' => 'YLPAY',
                        'return_params' => '原样返回',
                        'title' => '银联扫码收款',
                        'ways_source_desc' => '银联扫码',
                        'ways_type_desc' => '银联扫码',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                        'phone' => $store->people_phone
                    ];

                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_source'] = $ways->ways_source;
                    $data_insert['ways_source_desc'] = '银联扫码';

                    return $this->post_pay_public($data_insert, $request_data);
                }

                //工行-银联
                if ($ways && $ways->ways_type == 20004) {
                    $data_public = [
                        'pay_type' => 'jd',
                        'return_params' => '原样返回',
                        'title' => '银联收款',
                        'ways_source_desc' => '银联收款',
                        'ways_type_desc' => '银联收款',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->lianfu_public($data_insert, $data_public);
                }

                //邮政-银联
                if ($ways && $ways->ways_type == 26004) {
                    $data_public = [
                        'pay_type' => 'jd',
                        'return_params' => '原样返回',
                        'title' => '银联收款',
                        'ways_source_desc' => '银联收款',
                        'ways_type_desc' => '银联收款',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->lianfuyoupay_public($data_insert, $data_public);
                }

                //苏州银行银联
                if ($ways && $ways->ways_type == 17004) {
                    $data_public = [
                        'service' => 'UPOPCardPay',
                        'return_params' => '原样返回',
                        'title' => '银联收款',
                        'ways_source_desc' => '银联收款',
                        'ways_type_desc' => '银联收款',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->sz_pay_public($data_insert, $data_public);
                }

                //随行付银行银联
                if ($ways && $ways->ways_type == 13004) {
                    $data_public = [
                        'pay_type' => 'UNIONPAY',
                        'service' => 'UPOPCardPay',
                        'return_params' => '原样返回',
                        'title' => '银联收款',
                        'ways_source_desc' => '银联收款',
                        'ways_type_desc' => '银联收款',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->vbill_pay_public($data_insert, $data_public);
                }

                //随行付A银行银联
                if ($ways && $ways->ways_type == 19004) {
                    $data_public = [
                        'pay_type' => 'UNIONPAY',
                        'service' => 'UPOPCardPay',
                        'return_params' => '原样返回',
                        'title' => '银联收款',
                        'ways_source_desc' => '银联收款',
                        'ways_type_desc' => '银联收款',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->vbilla_pay_public($data_insert, $data_public);
                }

                //葫芦天下-银联
                if ($ways && $ways->ways_type == 23004) {
                    $data_public = [
                        'pay_type' => 'unionpay',
                        'return_params' => '原样返回',
                        'title' => '银联收款',
                        'ways_source_desc' => '银联收款',
                        'ways_type_desc' => '银联收款',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->hltx_pay_public($data_insert, $data_public);
                }

                //联动优势 银联 被扫
                if ($ways && $ways->ways_type == 5004) {
                    $data_public = [
                        'orderType' => 'unionpay',
                        'goodsId' => time(),
                        'return_params' => '原样返回',
                        'title' => '银联收款',
                        'ways_source_desc' => '银联收款',
                        'ways_type_desc' => '银联收款',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id
                    ];

                    return $this->linkage_pay_public($data_insert, $data_public);
                }

                //银盛 银联 被扫
//                if ($ways && $ways->ways_type == 14004) {
//                    $data_public = [
//                        'pay_type' => 'UNIONPAY',
//                        'service' => 'UPOPCardPay',
//                        'return_params' => '原样返回',
//                        'title' => '银联收款',
//                        'ways_source_desc' => '银联收款',
//                        'ways_type_desc' => '银联收款',
//                        'code' => $code,
//                        'out_trade_no' => $out_trade_no,
//                        'other_no' => $other_no,
//                        'config_id' => $config_id,
//                        'store_id' => $store_id,
//                        'store_pid' => $store_pid,
//                        'ways_type' => '' . $ways->ways_type . '',
//                        'ways_source' => $ways->ways_source,
//                        'company' => $ways->company,
//                        'total_amount' => $total_amount,
//                        'remark' => $remark,
//                        'device_id' => $device_id,
//                        'shop_name' => $shop_name,
//                        'merchant_id' => $merchant_id,
//                        'store_name' => $store_name,
//                        'tg_user_id' => $tg_user_id,
//                    ];
//
//                    return $this->yinsheng_pay_public($data_insert, $data_public);
//                }

                //钱方 银联 被扫
                if ($ways && $ways->ways_type == 24004) {
                    $data_public = [
                        'pay_type' => 'UNIONPAY',
                        'service' => 'UPOPCardPay',
                        'return_params' => '原样返回',
                        'title' => '银联收款',
                        'ways_source_desc' => '银联收款',
                        'ways_type_desc' => '银联收款',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->qf_pay_public($data_insert, $data_public);
                }

                //云闪付 银联
                if ($ways && $ways->ways_type == 27010) {
                    $config = new EasyPayConfigController();
                    $easypay_config = $config->easypay_config($data['config_id']);
                    if (!$easypay_config) {
                        return json_encode([
                            'status' => '2',
                            'message' => '易生支付配置不存在请检查配置'
                        ]);
                    }
                    $sayDodge = new SayDodgePayController();
                    $out_trade_no = substr($easypay_config->client_code, 0, 4) . date('YmdHis', time()) . substr($easypay_config->client_code, -4) . $sayDodge->rand_code();
                    $data_public = [
                        'pay_type' => 'UNIONPAY',
                        'service' => 'UPOPCardPay',
                        'return_params' => '原样返回',
                        'title' => '银联收款',
                        'ways_source_desc' => '银联收款',
                        'ways_type_desc' => '银联收款',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'device_type' => $device_type,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id
                    ];
                    return $this->ysf_pay_public($data_insert, $data_public);
                }

                //建设银行 银联
                if ($ways && $ways->ways_type == 31003) {
                    if (strlen($out_trade_no) > 30) {
                        $out_trade_no = substr($out_trade_no, 0, 25) . substr(microtime(), 2, 5);
                    }
                    $data_public = [
                        'pay_type' => 'UNIONPAY',
                        'service' => 'UPOPCardPay',
                        'return_params' => '原样返回',
                        'title' => '银联收款',
                        'ways_source_desc' => '银联收款',
                        'ways_type_desc' => '银联收款',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $this->ccBank_pay_public($data_insert, $data_public);
                }

                //易生支付 银联扫码
                if ($ways && $ways->ways_type == 32003) {
                    $config = new EasySkPayConfigController();
                    $easyskpay_config = $config->easyskpay_config($config_id);
                    if (!$easyskpay_config) {
                        return json_encode([
                            'status' => '2',
                            'message' => '易生数科支付配置不存在请检查配置'
                        ]);
                    }

                    $easyskpay_merchant = $config->easyskpay_merchant($store_id, $store_pid);
                    if (!$easyskpay_merchant) {
                        return json_encode([
                            'status' => '2',
                            'message' => '商户易生数科支付通道未开通'
                        ]);
                    }

                    if ($total_amount < 1000) {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_a;
                        $data_insert['fee_amount'] = $ways->rate_a * $total_amount / 100;
                    } else {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_c;
                        $data_insert['fee_amount'] = $ways->rate_c * $total_amount / 100;
                    }

                    $request_data = [
                        'pay_type' => 'UNIONPAY',
                        'return_params' => '原样返回',
                        'title' => '银联扫码收款',
                        'ways_source_desc' => '银联扫码',
                        'ways_type_desc' => '银联扫码',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_source'] = $ways->ways_source;
                    $data_insert['ways_source_desc'] = '银联扫码';

                    return $this->easysk_pay_public($data_insert, $request_data);
                }

                //通联支付 银联扫码
                if ($ways && $ways->ways_type == 33003) {
                    $config = new AllinPayConfigController();
                    $allin_config = $config->allin_pay_config($config_id);
                    if (!$allin_config) {
                        return json_encode([
                            'status' => '2',
                            'message' => '通联支付配置不存在请检查配置'
                        ]);
                    }

                    $allin_merchant = $config->allin_pay_merchant($store_id, $store_pid);
                    if (!$allin_merchant) {
                        return json_encode([
                            'status' => '2',
                            'message' => '商户通联支付通道未开通'
                        ]);
                    }

                    if ($total_amount < 1000) {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_a;
                        $data_insert['fee_amount'] = $ways->rate_a * $total_amount / 100;
                    } else {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_c;
                        $data_insert['fee_amount'] = $ways->rate_c * $total_amount / 100;
                    }
                    $request_data = [
                        'pay_type' => 'UNIONPAY',
                        'return_params' => '原样返回',
                        'title' => '银联扫码收款',
                        'ways_source_desc' => '银联扫码',
                        'ways_type_desc' => '银联扫码',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_source'] = $ways->ways_source;
                    $data_insert['ways_source_desc'] = '银联扫码';

                    return $this->allin_pay_public($data_insert, $request_data);
                }

                //富友支付 银联扫码
                if ($ways && $ways->ways_type == 11003) {
                    if ($total_amount < 1000) {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_a;
                        $data_insert['fee_amount'] = $ways->rate_a * $total_amount / 100;
                    } else {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_c;
                        $data_insert['fee_amount'] = $ways->rate_c * $total_amount / 100;
                    }
                    $request_data = [
                        'pay_type' => 'UNIONPAY',
                        'return_params' => '原样返回',
                        'title' => '银联扫码收款',
                        'ways_source_desc' => '银联扫码',
                        'ways_type_desc' => '银联扫码',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $other_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_source'] = $ways->ways_source;
                    $data_insert['ways_source_desc'] = '银联扫码';

                    return $this->fuiou_pay_public($data_insert, $request_data);
                }

                //拉卡拉支付 银联扫码
//                if ($ways && $ways->ways_type == 34003) {
//                    $config = new LklConfigController();
//                    $lkl_config = $config->lkl_config($config_id);
//                    if (!$lkl_config) {
//                        return json_encode([
//                            'status' => '2',
//                            'message' => '拉卡拉支付配置不存在请检查配置'
//                        ]);
//                    }
//
//                    $lkl_merchant = $config->lkl_merchant($store_id, $store_pid);
//                    if (!$lkl_merchant) {
//                        return json_encode([
//                            'status' => '2',
//                            'message' => '商户拉卡拉支付通道未开通'
//                        ]);
//                    }
//
//                    if ($total_amount < 1000) {
//                        //扫码费率入库
//                        $data_insert['rate'] = $ways->rate_a;
//                        $data_insert['fee_amount'] = $ways->rate_a * $total_amount / 100;
//                    } else {
//                        //扫码费率入库
//                        $data_insert['rate'] = $ways->rate_c;
//                        $data_insert['fee_amount'] = $ways->rate_c * $total_amount / 100;
//                    }
//                    $request_data = [
//                        'pay_type' => 'UNIONPAY',
//                        'return_params' => '原样返回',
//                        'title' => '银联扫码收款',
//                        'ways_source_desc' => '银联扫码',
//                        'ways_type_desc' => '银联扫码',
//                        'code' => $code,
//                        'out_trade_no' => $out_trade_no,
//                        'other_no' => $other_no,
//                        'config_id' => $config_id,
//                        'store_id' => $store_id,
//                        'store_pid' => $store_pid,
//                        'ways_type' => '' . $ways->ways_type . '',
//                        'ways_source' => $ways->ways_source,
//                        'company' => $ways->company,
//                        'total_amount' => $total_amount,
//                        'remark' => $remark,
//                        'device_id' => $device_id,
//                        'shop_name' => $shop_name,
//                        'merchant_id' => $merchant_id,
//                        'store_name' => $store_name,
//                        'tg_user_id' => $tg_user_id,
//                    ];
//
//                    //入库参数
//                    $data_insert['out_trade_no'] = $out_trade_no;
//                    $data_insert['ways_type'] = $ways->ways_type;
//                    $data_insert['ways_type_desc'] = '银联扫码';
//                    $data_insert['company'] = $ways->company;
//                    $data_insert['ways_source'] = $ways->ways_source;
//                    $data_insert['ways_source_desc'] = '银联扫码';
//
//                    return $this->lkl_pay_public($data_insert, $request_data);
//                }

            }

            return json_encode([
                'status' => 2,
                'message' => '暂不支持此二维码'
            ]);
        } catch (\Exception $ex) {
            Log::info('商户被扫-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ]);
        }
    }


    //生成二维码收款接口
    public function qr_pay(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $config_id = $merchant->config_id ?? '';
            $merchant_id = $merchant->merchant_id ?? '';
            $store_id = $request->get('store_id', '');
            $total_amount = $request->get('total_amount', '');
            $shop_price = $request->get('shop_price', $total_amount);
            $remark = $request->get('remark', '');
            $device_id = $request->get('device_id', 'app');
            $shop_name = $request->get('shop_name', '二维码收款');
            $shop_desc = $request->get('shop_desc', '二维码收款');
            $ways_source = $request->get('ways_source');
            $ways_type = $request->get('ways_type');
            $notify_url = $request->get('notify_url', '');
            $other_no = $request->get('other_no', '');
            $pay_method = isset($data['pay_method']) ? $data['pay_method'] : "";

            if (!$merchant_id) {
                $merchant_id = $request->get('merchant_id');
                $merchant = Merchant::where('id', $merchant_id)->first();
                $merchant_name = '';
                $config_id = '';
                if ($merchant) {
                    $merchant_name = $merchant->name;
                    $config_id = $merchant->config_id;
                }
            } else {
                $merchant_name = $merchant->merchant_name;
            }

            $data = [
                'config_id' => $config_id,
                'merchant_id' => $merchant_id,
                'merchant_name' => $merchant_name,
                'total_amount' => $total_amount,
                'shop_price' => $shop_price,
                'remark' => $remark,
                'device_id' => $device_id,
                'shop_name' => $shop_name,
                'shop_desc' => $shop_desc,
                'store_id' => $store_id,
                'other_no' => $other_no,
                'ways_source' => $ways_source,
                'notify_url' => $notify_url,
                'ways_type' => $ways_type,
            ];
            if ($pay_method) $data['pay_method'] = $pay_method;
            return $this->qr_pay_public($data);
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }


    //生成二维码-公共
    public function qr_pay_public($data)
    {
        try {
            $config_id = $data['config_id'];
            $merchant_id = $data['merchant_id'];
            $merchant_name = $data['merchant_name'];
            $total_amount = isset($data['total_amount']) ? $data['total_amount'] : '';
            $shop_price = isset($data['shop_price']) ? $data['shop_price'] : "";
            $remark = $data['remark'];
            $device_id = $data['device_id'];
            $shop_name = $data['shop_name'];
            $shop_desc = $data['shop_desc'];
            $store_id = $data['store_id'];
            $other_no = $data['other_no'];
            $ways_source = $data['ways_source'];
            $ways_type = $data['ways_type'];
            $notify_url = $data['notify_url'];
            $pay_method = isset($data['pay_method']) ? $data['pay_method'] : "";

            //没有传门店
            if ($store_id == "") {
                $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)
                    ->orderBy('created_at', 'asc')
                    ->select('store_id')
                    ->first();
                if (!$MerchantStore) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请提交门店资料认证'
                    ]);
                }
                $store_id = $MerchantStore->store_id;
            }
            $store = Store::where('store_id', $store_id)->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '平台门店ID不正确'
                ]);
            }
            //关闭的商户禁止交易
            if ($store->is_close || $store->is_admin_close || $store->is_delete) {
                return json_encode([
                    'status' => 2,
                    'message' => '商户已经被服务商关闭'
                ]);
            }

            $store_name = $store->store_short_name;
            $store_pid = $store->pid;
            $tg_user_id = $store->user_id;

            //返回静态码 store_id 必须传
            if ($total_amount == "") {
                //收银员的聚合收款码
                $code_url = url('/qr?store_id=' . $store_id . '&merchant_id=' . $merchant_id . '&notify_url=' . $notify_url . '&other_no=' . $other_no);
                $return = json_encode([
                    'status' => 1,
                    'data' => [
                        'code_url' => $code_url,
                        'store_name' => $store_name,
                        'out_trade_no' => '',
                        'other_no' => $other_no,
                    ]
                ]);

                return $return;
            }

            $obj_ways = new  PayWaysController();
            $ways = $obj_ways->ways_type($ways_type, $store_id, $store_pid);
            if (!$ways) {
                return json_encode([
                    'status' => 2,
                    'message' => '没有开通此类型通道'
                ]);
            }

            if ($ways->is_close) {
                return json_encode([
                    'status' => 2,
                    'message' => '此通道已关闭'
                ]);
            }

            if (isset($ways->pay_amount_e) && ($total_amount > $ways->pay_amount_e)) {
                return json_encode([
                    'status' => 2,
                    'message' => '单笔金额不能超过' . $ways->pay_amount_e
                ]);
            }

            $data = [
                'config_id' => $config_id,
                'store_id' => $store_id,
                'merchant_id' => $merchant_id,
                'total_amount' => $total_amount,
                'shop_price' => $shop_price,
                'remark' => $remark,
                'device_id' => $device_id,
                'config_type' => '01',
                'shop_name' => $shop_name,
                'shop_desc' => $shop_desc,
                'store_name' => $store_name,
                'is_fq' => 0,
            ];

            //插入数据库
            $data_insert = [
                'trade_no' => '',
                'store_id' => $store_id,
                'store_name' => $store_name,
                'buyer_id' => '',
                'total_amount' => $total_amount,
                'shop_price' => $shop_price,
                'payment_method' => '',
                'status' => '',
                'pay_status' => 2,
                'pay_status_desc' => '等待支付',
                'merchant_id' => $merchant_id,
                'merchant_name' => $merchant_name,
                'remark' => $remark ?? '',
                'device_id' => $device_id,
                'config_id' => $config_id,
                'user_id' => $tg_user_id,
                'notify_url' => $notify_url,
                'other_no' => $other_no,
                'company' => $ways->company,
            ];
            $out_trade_no = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));

            //扫码费率入库
            $data_insert['rate'] = $ways->rate;
            $data_insert['fee_amount'] = $ways->rate * $total_amount / 100;

            /*官方支付宝*/
            if (999 < $ways_type && $ways_type < 1099) {
                $config_type = '01';
                $notify_url = url('/api/alipayopen/qr_pay_notify');

                //配置
                $isvconfig = new AlipayIsvConfigController();
                $storeInfo = $isvconfig->alipay_auth_info($store_id, $store_pid);
                $out_user_id = $storeInfo->user_id; //商户的id
                $alipay_store_id = $storeInfo->alipay_store_id;
                $out_store_id = $storeInfo->out_store_id;
                //分成模式 服务商
                if ($storeInfo->settlement_type == "set_a") {
                    if ($storeInfo->config_type == '02') {
                        $config_type = '02';
                    }
                    $storeInfo = AlipayAccount::where('config_id', $config_id)
                        ->where('config_type', $config_type)
                        ->first(); //服务商的
                }

                if (!$storeInfo) {
                    $msg = '支付宝授权信息不存在';

                    return json_encode([
                        'status' => 2,
                        'message' => $msg
                    ]);
                }

                $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);

                $pay_obj = new PayController();
                $out_trade_no = 'DQR' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                $data['out_trade_no'] = $out_trade_no;
                $data['alipay_store_id'] = $alipay_store_id;
                $data['out_store_id'] = $out_store_id;
                $data['out_user_id'] = $out_user_id;
                $data['app_auth_token'] = $storeInfo->app_auth_token;
                $data['config'] = $config;
                $data['notify_url'] = $notify_url;

                //禁用信用通道
                if ($ways->credit == "00") {
                    $disable_pay_channels = 'credit_group'; //禁用信用方式
                    $data['disable_pay_channels'] = $disable_pay_channels;
                }

                $return = $pay_obj->qr_pay($data);
                $return_aray = json_decode($return, true);
                if ($return_aray['status'] == '1') {
                    //入库参数
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => '2',
                            'message' => '订单未入库'
                        ]);
                    }
                }

                return $return;
            }

            /*官方微信*/
            if (1999 < $ways_type && $ways_type < 2999) {
                $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                $data['goods_detail'] = [];
                $data['open_id'] = '';
                $data['out_trade_no'] = $out_trade_no;
                $data['attach'] = $store_id . ',' . $config_id; //附加信息原样返回

                $config = new WeixinConfigController();
                $options = $config->weixin_config($config_id);
                $weixin_store = $config->weixin_merchant($store_id, $store_pid);
                if (!$weixin_store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '微信商户号不存在'
                    ]);
                }

                //分账
                $weixin_store_obj = WeixinStore::where('store_id', $store_id)->first();
                if ($weixin_store_obj) {
                    $data['is_profit_sharing'] = $weixin_store_obj->is_profit_sharing ?? ''; //是否分账(0-不分;1-分)
                    $data['wx_sharing_rate'] = $weixin_store_obj->wx_sharing_rate ?? ''; //分账比例%
                }

                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;
                $data['wx_sub_merchant_id'] = $wx_sub_merchant_id;
                $data['options'] = $options;

                $pay_obj = new \App\Api\Controllers\Weixin\PayController();
                $return = $pay_obj->qr_pay($data, 'NATIVE');
                $return_array = json_decode($return, true);
                if ($return_array['status'] == 1) {
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_source_desc'] = '微信支付';

                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    $return = json_encode([
                        'status' => 1,
                        'data' => [
                            'code_url' => $return_array['data']['code_url'],
                            'store_name' => $store_name,
                            'out_trade_no' => $out_trade_no
                        ]
                    ]);
                }

                return $return;
            }

            /*官方微信a*/
            if (3999 < $ways_type && $ways_type < 4999) {
                $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                $data['goods_detail'] = [];
                $data['open_id'] = '';
                $data['out_trade_no'] = $out_trade_no;
                $data['attach'] = $store_id . ',' . $config_id; //附加信息原样返回

                $config = new WeixinConfigController();
                $options = $config->weixina_config($config_id);
                $weixin_store = $config->weixina_merchant($store_id, $store_pid);
                if (!$weixin_store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '微信a商户号不存在'
                    ]);
                }

                //分账
                $weixin_store_obj = WeixinaStore::where('store_id', $store_id)->first();
                if ($weixin_store_obj) {
                    $data['is_profit_sharing'] = $weixin_store_obj->is_profit_sharing ?? ''; //是否分账(0-不分;1-分)
                    $data['wx_sharing_rate'] = $weixin_store_obj->wx_sharing_rate ?? ''; //分账比例%
                }

                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;
                $data['wx_sub_merchant_id'] = $wx_sub_merchant_id;
                $data['options'] = $options;

                $pay_obj = new \App\Api\Controllers\Weixin\PayController();
                $notify_url_a = url('api/weixin/qr_pay_notify_a');
                $return = $pay_obj->qr_pay($data, 'NATIVE', $notify_url_a);
                $return_array = json_decode($return, true);
                if ($return_array['status'] == 1) {
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '微信支付a';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_source_desc'] = '微信支付';

                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    $return = json_encode([
                        'status' => 1,
                        'data' => [
                            'code_url' => $return_array['data']['code_url'],
                            'store_name' => $store_name,
                            'out_trade_no' => $out_trade_no
                        ]
                    ]);
                }

                return $return;
            }

            //京东支付二维码
            if (5999 < $ways_type && $ways_type < 6999) {
                $config = new JdConfigController();
                $jd_config = $config->jd_config($data['config_id']);
                if (!$jd_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '京东配置不存在请检查配置'
                    ]);
                }

                $jd_merchant = $config->jd_merchant($store_id, $store_pid);
                if (!$jd_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '京东商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Jd\PayController();
                if ($ways_type == 6001) {
                    $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }
                if ($ways_type == 6002) {
                    $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }
                if ($ways_type == 6003) {
                    $out_trade_no = 'jd_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '京东支付';
                    $data_insert['ways_source'] = 'jd';
                    $data_insert['ways_source_desc'] = '京东支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }
                if ($ways_type == 6004) {
                    $out_trade_no = 'un_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '银联扫码';
                    $data_insert['out_trade_no'] = $out_trade_no;

                    if ($total_amount < 1000) {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_a;
                        $data_insert['fee_amount'] = $ways->rate_a * $total_amount / 100;
                    } else {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_c;
                        $data_insert['fee_amount'] = $ways->rate_c * $total_amount / 100;
                    }
                }

                $insert_re = $this->insert_day_order($data_insert);
                if (!$insert_re) {
                    return json_encode([
                        'status' => 2,
                        'message' => '订单未入库'
                    ]);
                }

                $data = [];
                $data['out_trade_no'] = $out_trade_no;
                $data['request_url'] = $obj->send_qr_url; //请求地址;
                $data['notify_url'] = url('/api/jd/notify_url'); //地址;
                $data['merchant_no'] = $jd_merchant->merchant_no;
                $data['md_key'] = $jd_merchant->md_key; //
                $data['des_key'] = $jd_merchant->des_key; //
                $data['systemId'] = $jd_config->systemId; //
                $data['total_amount'] = $total_amount;
                $data['remark'] = $remark;
                $data['return_params'] = '原样返回';

                $return = $obj->send_qr($data);
                if ($return['status'] == 1) {
                    return json_encode([
                        'status' => 1,
                        'data' => [
                            'code_url' => $return['code_url'],
                            'store_name' => $store_name,
                            'out_trade_no' => $out_trade_no
                        ]
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }
            }

            //快钱支付二维码
            if (2999 < $ways_type && $ways_type < 3999) {
                $config = new MyBankConfigController();

                $mybank_merchant = $config->mybank_merchant($store_id, $store_pid);
                if (!$mybank_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '快钱不存在'
                    ]);
                }

                $wx_AppId = $mybank_merchant->wx_AppId;
                $MyBankConfig = $config->MyBankConfig($data['config_id'], $wx_AppId);
                if (!$MyBankConfig) {
                    return json_encode([
                        'status' => 2,
                        'message' => '快钱配置不存在请检查配置'
                    ]);
                }

                if ($ways_type == 3001) {
                    $ChannelType = 'ALI';
                    $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }
                if ($ways_type == 3002) {
                    $ChannelType = 'WX';
                    $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }

                $insert_re = $this->insert_day_order($data_insert);
                if (!$insert_re) {
                    return json_encode([
                        'status' => 2,
                        'message' => '订单未入库'
                    ]);
                }

                $redata = [
                    'MerchantId' => $mybank_merchant->MerchantId,
                    'Goodsid' => $store_id,
                    'Body' => $data_insert['ways_type_desc'],
                    'TotalAmount' => $total_amount * 100,//金额
                    'OutTradeNo' => $out_trade_no,
                    'Currency' => 'CNY',
                    'ChannelType' => $ChannelType,
                    'OperatorId' => $merchant_id,//操作员id
                    'StoreId' => $store_id,
                    'DeviceId' => $device_id,
                    'DeviceCreateIp' => get_client_ip(),
                    'ExpireExpress' => '1',//过期时间
                    'SettleType' => 'T1',//'T1',//清算方式
                    'Attach' => url('/api/mybank/notify'),//附加信息，原样返回。
                    'PayLimit' => '',//禁用方式
                    'AlipayStoreId' => '',
                    'SysServiceProviderId' => $MyBankConfig->ali_pid,
                    'NotifyUrl' => url('/api/mybank/notify')
                ];

                $obj = new QrpayController();
                $return = $obj->gdqr($redata, $config_id);

                if ($return['status'] == 1) {
                    return json_encode([
                        'status' => 1,
                        'data' => [
                            'code_url' => $return['code_url'],
                            'store_name' => $store_name,
                            'out_trade_no' => $out_trade_no
                        ]
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }
            }

            //新大陆支付二维码
            if (7999 < $ways_type && $ways_type < 8999) {
                $config = new NewLandConfigController();
                $new_land_config = $config->new_land_config($config_id);
                if (!$new_land_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '新大陆配置不存在请检查配置'
                    ]);
                }

                $new_land_merchant = $config->new_land_merchant($store_id, $store_pid);
                if (!$new_land_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户新大陆通道未开通'
                    ]);
                }

                //入库参数
                $data_insert['ways_type'] = $ways->ways_type;
                $data_insert['ways_type_desc'] = '支付宝';
                $data_insert['ways_source'] = $ways->ways_source;
                $data_insert['ways_source_desc'] = '支付宝';
                $data_insert['out_store_id'] = $new_land_merchant->nl_mercId;

                if ($ways_type == 8001) {
                    $payChannel = 'ALIPAY';
                    $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }
                if ($ways_type == 8002) {
                    $payChannel = 'WXPAY';
                    $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }
                if ($ways_type == 8004) {
                    $payChannel = 'YLPAY';
                    $out_trade_no = 'un_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '银联扫码';
                    $data_insert['out_trade_no'] = $out_trade_no;

                    if ($total_amount < 1000) {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_a;
                        $data_insert['fee_amount'] = $ways->rate_a * $total_amount / 100;
                    } else {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_c;
                        $data_insert['fee_amount'] = $ways->rate_c * $total_amount / 100;
                    }
                }

                $insert_re = $this->insert_day_order($data_insert);
                if (!$insert_re) {
                    return json_encode([
                        'status' => 2,
                        'message' => '订单未入库'
                    ]);
                }

                $request_data = [
                    'out_trade_no' => $out_trade_no,
                    'total_amount' => $total_amount,
                    'remark' => $remark,
                    'device_id' => $device_id,
                    'shop_name' => $shop_name,
                    'key' => $new_land_merchant->nl_key,
                    'org_no' => $new_land_config->org_no,
                    'merc_id' => $new_land_merchant->nl_mercId,
                    'trm_no' => $new_land_merchant->trmNo,
                    'op_sys' => '3',
                    'opr_id' => $merchant_id,
                    'trm_typ' => 'T',
                    'payChannel' => $payChannel,
                ];

                $obj = new \App\Api\Controllers\Newland\PayController();
                $return = $obj->send_qr($request_data);
                if ($return['status'] == 1) {
                    return json_encode([
                        'status' => 1,
                        'data' => [
                            'code_url' => $return['code_url'],
                            'store_name' => $store_name,
                            'out_trade_no' => $out_trade_no
                        ]
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }
            }

            //和融通支付二维码
            if (8999 < $ways_type && $ways_type < 9999) {
                $config = new HConfigController();
                $h_config = $config->h_config($data['config_id']);
                if (!$h_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '和融通配置不存在请检查配置'
                    ]);
                }

                $h_merchant = $config->h_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '和融通商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Huiyuanbao\PayController();
                $payChannel = 'ali';
                //支付宝
                if ($ways_type == 9001) {
                    $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $payChannel = 'ali';
                }

                if ($ways_type == 9002) {
                    $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $payChannel = 'wx';
                }

                $insert_re = $this->insert_day_order($data_insert);
                if (!$insert_re) {
                    return json_encode([
                        'status' => 2,
                        'message' => '订单未入库'
                    ]);
                }

                $data = [];
                $data['out_trade_no'] = $out_trade_no;
                $data['request_url'] = $obj->send_qr_url; //请求地址;
                $data['notify_url'] = url('/api/huiyuanbao/pay_notify'); //地址;
                $data['md_key'] = $h_config->md_key; //
                $data['total_amount'] = $total_amount;
                $data['mid'] = $h_merchant->h_mid;
                $data['md_key'] = $h_config->md_key; //
                $data['orgNo'] = $h_merchant->orgNo; //
                $data['payChannel'] = $payChannel; //
                $data['remark'] = $remark;
                $data['return_params'] = '原样返回';

                $return = $obj->send_qr($data);
                if ($return['status'] == 1) {
                    return json_encode([
                        'status' => 1,
                        'data' => [
                            'code_url' => $return['code_url'],
                            'store_name' => $store_name,
                            'out_trade_no' => $out_trade_no
                        ]
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }
            }

            //联拓富二维码
            if (9999 < $ways_type && $ways_type < 10999) {
                $config = new LtfConfigController();
                $ltf_merchant = $config->ltf_merchant($store_id, $store_pid);
                if (!$ltf_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Ltf\PayController();
                if ($ways_type == 10001) {
                    $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $channel = "ALIPAY";
                }
                if ($ways_type == 10002) {
                    $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $channel = "WXPAY";
                }

                $insert_re = $this->insert_day_order($data_insert);
                if (!$insert_re) {
                    return json_encode([
                        'status' => 2,
                        'message' => '订单未入库'
                    ]);
                }

                $data = [];
                $data['out_trade_no'] = $out_trade_no;
                $data['request_url'] = $obj->send_qr_url; //请求地址;
                $data['notify_url'] = url('/api/ltf/pay_notify'); //地址;
                $data['merchant_no'] = $ltf_merchant->merchantCode;
                $data['appId'] = $ltf_merchant->appId; //
                $data['key'] = $ltf_merchant->md_key; //
                $data['total_amount'] = $total_amount;
                $data['remark'] = $remark;
                $data['return_params'] = '原样返回';
                $data['channel'] = $channel;
                $data['tradeType'] = 'NATIVE';

                $return = $obj->send_qr($data);
                if ($return['status'] == 1) {
                    return json_encode([
                        'status' => 1,
                        'data' => [
                            'code_url' => $return['code_url'],
                            'store_name' => $store_name,
                            'out_trade_no' => $out_trade_no
                        ]
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }

            }

            //哆啦宝支付二维码
            if (14999 < $ways_type && $ways_type < 15999) {
                $manager = new \App\Api\Controllers\DuoLaBao\BaseController();
                $dlb_config = $manager->pay_config($data['config_id']);
                if (!$dlb_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '哆啦宝配置配置不存在请检查配置'
                    ]);
                }

                $dlb_merchant = $manager->dlb_merchant($store_id, $store_pid);
                if (!$dlb_merchant && !empty($dlb_merchant->mch_num) && !empty($dlb_merchant->shop_num)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '哆啦宝配置商户未补充商户编号等信息!'
                    ]);
                }

                $orderheader = $ways_type == '15001' ? 'ali_qr' : 'wx_qr';
                $out_trade_no = $orderheader . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                $dlb_data = array(
                    "accessKey" => $dlb_config->access_key,
                    "secretKey" => $dlb_config->secret_key,
                    "agentNum" => $dlb_config->agent_num,
                    "customerNum" => $dlb_merchant->mch_num,
                    "shopNum" => $dlb_merchant->shop_num,
                    "requestNum" => $out_trade_no,
                    "amount" => $total_amount,
                    "source" => 'API',
                    "callbackUrl" => url('api/dlb/pay_notify')
                );

                //支付宝
                if ($ways_type == 15001) {
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }
                if ($ways_type == 15002) {
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }
                if ($ways_type == 15003) {
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '京东支付';
                    $data_insert['ways_source'] = 'jd';
                    $data_insert['ways_source_desc'] = '京东支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }
                if ($ways_type == 15004) {
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '银联扫码';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }

                $insert_re = $this->insert_day_order($data_insert);
                if (!$insert_re) {
                    return json_encode([
                        'status' => 2,
                        'message' => '订单未入库'
                    ]);
                }
                $return = $manager->pay_url($dlb_data);
                if ($return['status'] == 1) {
                    return json_encode([
                        'status' => 1,
                        'data' => [
                            'code_url' => $return['url'],
                            'store_name' => $store_name,
                            'out_trade_no' => $out_trade_no
                        ]
                    ]);
                } else {
                    return json_encode($return);
                }
            }

            //易生支付 动态二维码
            if (20999 < $ways_type && $ways_type < 21999) {
                $manager = new EasyPayConfigController();
                $easypay_config = $manager->easypay_config($data['config_id']);
                if (!$easypay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生支付配置不存在请检查配置'
                    ]);
                }

                $easypay_merchant = $manager->easypay_merchant($store_id, $store_pid);
                if (!$easypay_merchant && !empty($easypay_merchant->mer_trace)) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生支付配置商户未补充商户编号等信息!'
                    ]);
                }
                $store = Store::where('store_id', $store_id)->first();
                $location = '';
                if (!$store->lat || !$store->lng) {
                    $storeController = new \App\Api\Controllers\User\StoreController();
                    $address = $store->province_name . $store->city_name . $store->area_name . $store->store_address;//获取经纬度的地址
                    $api_res = $storeController->query_address($address, env('LBS_KEY'));
                    if ($api_res['status'] == '1') {
                        $store_lng = $api_res['result']['lng'];
                        $store_lat = $api_res['result']['lat'];
                        $store->update([
                            'lng' => $store_lng,
                            'lat' => $store_lat,
                        ]);
                        $location = "+" . $store_lat . "/-" . $store_lng;
                    } else {
//                            Log::info('添加门店-获取经纬度错误');
//                            Log::info($api_res['message']);
                    }

                } else {
                    $location = "+" . $store->lat . "/-" . $store->lng;
                }
                $orderheader = $ways_type == '21001' ? 'ali_qr' : 'wx_qr';
                $out_trade_no = $orderheader . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                $easypay_data = array(
                    'channel_id' => $easypay_config->channel_id,
                    'mno' => $easypay_merchant->term_mercode,
                    'device_id' => $easypay_merchant->term_termcode,
                    'out_trade_no' => $out_trade_no,
                    'total_amount' => $total_amount,
                    'shop_name' => $shop_name,
                    'code' => $code ?? '', //否，支付宝（买家支付宝用户 ID，参考文档 ），上送支付宝用户 ID 时，当前订单仅当前支付宝用户支付
                    'notifyurl' => url('api/easypay/pay_notify_url'), //客户端接收微信支付成功通知的地址，通知报文请见“后台通知接口”
                    'location' => $location
                );

                if ($ways_type == 21001) {
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }

                if ($ways_type == 21002) {
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }

                if ($ways_type == 21003) {
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '京东支付';
                    $data_insert['ways_source'] = 'jd';
                    $data_insert['ways_source_desc'] = '京东支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }

                if ($ways_type == 21004) {
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '银联扫码';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }

                $insert_re = $this->insert_day_order($data_insert);
                if (!$insert_re) {
                    return json_encode([
                        'status' => '2',
                        'message' => '订单未入库'
                    ]);
                }

                $easypay_data['pay_type'] = $data_insert['ways_source'];

                $easypay_pay_obj = new \App\Api\Controllers\EasyPay\PayController();
                $return = $easypay_pay_obj->send_qr($easypay_data); //-1 系统错误 0-其他 1-成功 2-待支付

                if ($return['status'] == '1') {
                    return json_encode([
                        'status' => '1',
                        'data' => [
                            'code_url' => $return['code_url'],
                            'store_name' => $store_name,
                            'out_trade_no' => $out_trade_no
                        ]
                    ]);
                } else {
                    return json_encode($return);
                }

            }

            //威富通 动态二维码
            if (26999 < $ways_type && $ways_type < 27999) {
                $manager = new WftPayConfigController();
                $wftpay_config = $manager->wftpay_config($data['config_id']);
                if (!$wftpay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '威富通支付不存在请检查配置'
                    ]);
                }

                $wftpay_merchant = $manager->wftpay_merchant($store_id, $store_pid);
                if (!$wftpay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '威富通支付商户号未配置'
                    ]);
                }

                $order_header = $ways_type == '27001' ? 'ali_qr' : 'wx_qr';
                $out_trade_no = $order_header . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));

                if ($ways_type == 27001) {
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }

                if ($ways_type == 27002) {
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }

                if ($ways_type == 27003) {
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '京东支付';
                    $data_insert['ways_source'] = 'jd';
                    $data_insert['ways_source_desc'] = '京东支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }

                if ($ways_type == 27004) {
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '银联扫码';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }

                $insert_re = $this->insert_day_order($data_insert);
                if (!$insert_re) {
                    return json_encode([
                        'status' => '2',
                        'message' => 'wftpay订单未入库'
                    ]);
                }

                $wftpay_data = [
                    'mch_id' => $wftpay_merchant->mch_id,
                    'out_trade_no' => $out_trade_no,
                    'body' => $shop_name,
                    'total_amount' => $total_amount,
                    'private_rsa_key' => $wftpay_config->private_rsa_key,
                    'public_rsa_key' => $wftpay_config->public_rsa_key
                ];
                $wftpay_obj = new WftPayPayController();
                $return = $wftpay_obj->send_qr($wftpay_data); //0-系统错误 1-成功 2-失败
                if ($return['status'] == 1) {
                    return [
                        'status' => '1',
                        'message' => $return['message'],
                        'data' => [
                            'code_url' => $return['data']['code_url'],
                            'code_img_url' => $return['data']['code_img_url'],
                            'store_name' => $store_name,
                            'out_trade_no' => $out_trade_no
                        ]
                    ];
                } else {
                    return $return;
                }

            }

            //汇旺财 动态二维码
            if (27999 < $ways_type && $ways_type < 28999) {
                $manager = new HwcPayConfigController();
                $hwcpay_config = $manager->hwcpay_config($data['config_id']);
                if (!$hwcpay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '汇旺财支付不存在请检查配置'
                    ]);
                }

                $hwcpay_merchant = $manager->hwcpay_merchant($store_id, $store_pid);
                if (!$hwcpay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '汇旺财支付商户号未配置'
                    ]);
                }

                $order_header = $ways_type == '28001' ? 'ali_qr' : 'wx_qr';
                $out_trade_no = $order_header . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));

                if ($ways_type == 28001) {
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }

                if ($ways_type == 28002) {
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }

                if ($ways_type == 28003) {
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '京东支付';
                    $data_insert['ways_source'] = 'jd';
                    $data_insert['ways_source_desc'] = '京东支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }

                if ($ways_type == 28004) {
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '银联扫码';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }

                $insert_re = $this->insert_day_order($data_insert);
                if (!$insert_re) {
                    return json_encode([
                        'status' => '2',
                        'message' => '汇旺财支付订单未入库'
                    ]);
                }

                $hwcpay_data = [
                    'mch_id' => $hwcpay_merchant->mch_id,
                    'out_trade_no' => $out_trade_no,
                    'body' => $shop_name,
                    'total_amount' => $total_amount,
                    'private_rsa_key' => $hwcpay_config->private_rsa_key,
                    'public_rsa_key' => $hwcpay_config->public_rsa_key
                ];
                $hwcpay_obj = new HwcpayPayController();
                $return = $hwcpay_obj->send_qr($hwcpay_data); //0-系统错误 1-成功 2-失败
                if ($return['status'] == 1) {
                    return [
                        'status' => '1',
                        'message' => $return['message'],
                        'data' => [
                            'code_url' => $return['data']['code_url'],
                            'code_img_url' => $return['data']['code_img_url'],
                            'store_name' => $store_name,
                            'out_trade_no' => $out_trade_no
                        ]
                    ];
                } else {
                    return $return;
                }

            }

            //银盛 动态二维码
            if (13999 < $ways_type && $ways_type < 14999) {
                $manager = new YinshengConfigController();
                $yinsheng_config = $manager->yinsheng_config($data['config_id']);
                if (!$yinsheng_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '银盛支付不存在,请检查配置'
                    ]);
                }

                $yinsheng_merchant = $manager->yinsheng_merchant($store_id, $store_pid);
                if (!$yinsheng_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '银盛支付,商户号未配置'
                    ]);
                }

                $out_trade_no = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));

                if ($ways_type == 14001) {
                    $ways_source = 'alipay';
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }
                if ($ways_type == 14002) {
                    $ways_source = 'weixin';
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }
                if ($ways_type == 14004) {
                    $ways_source = 'unionpay';
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '银联扫码';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }

                $insert_re = $this->insert_day_order($data_insert);
                if (!$insert_re) {
                    return json_encode([
                        'status' => 2,
                        'message' => '银盛支付订单未入库'
                    ]);
                }

                $yinsheng_data['partner_id'] = $yinsheng_merchant->partner_id; //商户号
                $yinsheng_data['return_url'] = ''; //可空,同步通知地址
                $yinsheng_data['out_trade_no'] = $out_trade_no; //商户生成的订单号,生成规则前8位必须为交易日期,如20180525,范围跨度支持包含当天在内的前后一天,且只能由大小写英文字母、数字、下划线及横杠组成
                $yinsheng_data['body'] = $shop_name; //商品的标题/交易标题/订单标题/订单关键字等。该参数最长为 250 个汉字
                $yinsheng_data['total_amount'] = $total_amount; //订单的资金总额，单位为 RMB-Yuan。取值范围为[0.01，100000000.00]，精确到小数点后两位。Number(10,2)指10位长度，2位精度
                $yinsheng_data['store_name'] = $yinsheng_merchant->seller_name; //收款方银盛支付客户名
                $yinsheng_data['business_code'] = $yinsheng_merchant->business_code; //业务代码
                $yinsheng_data['ways_source'] = $ways_source;
                $yinsheng_data['private_key'] = $yinsheng_config->private_key; //私钥
                $yinsheng_data['public_key'] = $yinsheng_config->public_key; //公钥
                $yinsheng_data['pfx_password'] = $yinsheng_config->pfx_password; //商户私钥证书密码
                $yinsheng_obj = new \App\Api\Controllers\BaseController();
                Log::info('银盛-动态二维码-入参');
                Log::info($yinsheng_data);
                $return = $yinsheng_obj->send_qr($yinsheng_data); //-1系统错误 1-交易成功 2-失败 3-等待买家付款 4-客户主动关闭订单 5-交易正在处理中 6-部分退款成功 7-全部退款成功
                Log::info('银盛-动态二维码-返回结果');
                Log::info($return);
                if ($return['status'] == 1) {
                    return [
                        'status' => '1',
                        'message' => $return['message'],
                        'data' => [
                            'code_url' => $return['data']['qr_code_url'], //不可空,二维码地址
                            'bank_type' => $return['data']['bank_type'], //不可空,二维码行别
                            'expire_time' => isset($return['data']['expire_time']) ? $return['data']['expire_time'] : 10, //过期时间,单位分钟
                            'store_name' => $store_name,
                            'out_trade_no' => $out_trade_no
                        ]
                    ];
                } else {
                    return $return;
                }

            }

            //钱方 生成二维码
            if (23999 < $ways_type && $ways_type < 24999) {
                $qfPayConfigObj = new QfPayConfigController();
                $qfpay_merchant = $qfPayConfigObj->qfpay_merchant($store_id, $store_pid);
                if (!$qfpay_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户未开通钱方通道'
                    ]);
                }

                $qfpay_config = $qfPayConfigObj->qfpay_config($data['config_id']);
                if (!$qfpay_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '钱方支付配置不存在请检查配置'
                    ]);
                }

                //支付宝
                if ($ways_type == '24001') {
                    $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;

                    $pay_type = 800101;  //支付类型: 支付宝扫码-800101; 支付宝反扫-800108; 支付宝服务窗支付-800107;;微信扫码-800201; 微信反扫-800208; 微信公众号支付-800207; 微信小程序支付-800213;QQ扫码-800601; QQ反扫-800608(暂不可用);;云闪付扫码-800701; 云闪付反扫-800708;;统一聚合反扫-800008
                }

                //微信
                if ($ways_type == '24002') {
                    $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;

                    $pay_type = 800201;  //支付类型: 支付宝扫码-800101; 支付宝反扫-800108; 支付宝服务窗支付-800107;;微信扫码-800201; 微信反扫-800208; 微信公众号支付-800207; 微信小程序支付-800213;QQ扫码-800601; QQ反扫-800608(暂不可用);;云闪付扫码-800701; 云闪付反扫-800708;;统一聚合反扫-800008
                }

                //银联
                if ($ways_type == '24004') {
                    $out_trade_no = 'un_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '银联扫码';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $pay_type = 800701;  //支付类型: 支付宝扫码-800101; 支付宝反扫-800108; 支付宝服务窗支付-800107;;微信扫码-800201; 微信反扫-800208; 微信公众号支付-800207; 微信小程序支付-800213;QQ扫码-800601; QQ反扫-800608(暂不可用);;云闪付扫码-800701; 云闪付反扫-800708;;统一聚合反扫-800008
                }

                //微信小程序
                if ($pay_method == 'wx_applet') {
                    $wechat_appid = MerchantStoreAppidSecret::where('store_id', $store_id)->first();
                    $out_trade_no = 'wx_applet' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信小程序';
                    $data_insert['ways_source'] = 'applet';
                    $data_insert['ways_source_desc'] = '微信小程序';
                    $data_insert['out_trade_no'] = $out_trade_no;

                    $pay_type = 800213;  //支付类型: 支付宝扫码-800101; 支付宝反扫-800108; 支付宝服务窗支付-800107;;微信扫码-800201; 微信反扫-800208; 微信公众号支付-800207; 微信小程序支付-800213;QQ扫码-800601; QQ反扫-800608(暂不可用);;云闪付扫码-800701; 云闪付反扫-800708;;统一聚合反扫-800008
                }

                $obj = new QfPayController();
                $reqData = [
                    'key' => $qfpay_config->key,  //加签key
                    'code' => $qfpay_config->code,  //开发唯一标识
                    'total_amount' => $total_amount,  //订单支付金额
                    'mchid' => $qfpay_merchant->mchid,  //子商户号,标识子商户身份,由钱方分配(渠道系统后台查看对应商户(非业务员)子商户号,被视为对应商户的交易)
                    'out_trade_no' => $out_trade_no,  //外部订单号,开发者平台订单号,同子商户(mchid)下,每次成功调用支付(含退款)接口下单,该参数值均不能重复使用,保证单号唯一,长度不超过128字符
                    'pay_type' => $pay_type  //支付类型: 支付宝扫码-800101; 支付宝反扫-800108; 支付宝服务窗支付-800107;;微信扫码-800201; 微信反扫-800208; 微信公众号支付-800207; 微信小程序支付-800213;QQ扫码-800601; QQ反扫-800608(暂不可用);;云闪付扫码-800701; 云闪付反扫-800708;;统一聚合反扫-800008
                ];
                Log::info('钱方-生成二维码-入参：');
                Log::info($reqData);
                $return = $obj->payment($reqData); //1-成功 3-交易中
                Log::info('钱方-生成二维码-结果：');
                Log::info($return);
                if ($return['status'] == 1) {
//                    if (isset($return['data']['respcd']) && $return['data']['respcd'] == '0000') {
//                        $data_insert['pay_status'] = 1;
//                        $data_insert['pay_status_desc'] = '支付成功';
//                        if (isset($return['data']['syssn']) && $return['data']['syssn']) $data_insert['trade_no'] = $return['data']['syssn'];
//                        if (isset($return['data']['qrcode']) && $return['data']['qrcode']) $data_insert['notify_url'] = $return['data']['qrcode'];
//                        if (isset($return['data']['sysdtm']) && $return['data']['sysdtm']) $data_insert['pay_time'] = $return['data']['sysdtm'];
//                        if (isset($return['data']['txamt']) && $return['data']['txamt']) {
//                            $data_insert['buyer_pay_amount'] = $return['data']['txamt']/100;
//                            $data_insert['receipt_amount'] = $return['data']['txamt']/100;
//                        }
//                    }
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    return json_encode([
                        'status' => 1,
                        'data' => $return['data']
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }

            }

            return json_encode([
                'status' => 2,
                'message' => '暂不支持此通道'
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => '-1',
                'message' => $exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine()
            ]);
        }
    }


    //静态二维码 提交 中转站
    public function qr_auth_pay(Request $request)
    {
        try {
            // Log::info('total_amount' . $request->get('merchant_id'));
            // Log::info('store_id' . $request->get('store_id'));
            // Log::info('remark' . $request->get('remark'));
            // Log::info('_token' . $request->get('_token'));
            // Log::info('merchant_id' . $request->get('merchant_id'));
            // Log::info('open_id' . $request->get('open_id'));
            // Log::info('ways_type' . $request->get('ways_type'));
            $merchant_id = $request->get('merchant_id');
            $merchant = Merchant::where('id', $merchant_id)
                ->select('name')
                ->first();
            $merchant_name = '';
            if ($merchant) {
                $merchant_name = $merchant->name;
            }

            $store_id = $request->get('store_id');
            $store = Store::where('store_id', $store_id)
                ->select('config_id', 'store_name', 'pid', 'user_id', 'is_delete', 'is_admin_close', 'is_close', 'source')
                ->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '没有关联认证门店请联系服务商'
                ]);
            }
            //关闭的商户禁止交易
            if ($store->is_close || $store->is_admin_close || $store->is_delete) {
                return json_encode([
                    'status' => 2,
                    'message' => '商户已经被服务商关闭'
                ]);
            }

            $config_id = $store->config_id;
            $store_name = $store->store_name;
            $store_pid = $store->pid;
            $tg_user_id = $store->user_id;
            $total_amount = $request->get('total_amount');
            $shop_price = $request->get('shop_price', $total_amount);
            $remark = $request->get('remark', '');
            $device_id = $request->get('device_id', '码牌');
            $shop_name = $request->get('shop_name', '扫码收款');
            $shop_desc = $request->get('shop_desc', '扫码收款');
            $open_id = $request->get('open_id');
            $ways_type = $request->get('ways_type');
            $other_no = $request->get('other_no', '');
            $notify_url = $request->get('notify_url', '');
            $is_fq = $request->get('is_fq', '');
            $auth_code = $request->get('auth_code', ''); //支付宝、微信授权码
            $origin_id = $request->get('originId', ''); //系统达达订单号
            $pay_method = $request->get('pay_method', 'qr_code'); //支付方式
            $atqTag = $request->get('atqTag', ''); //银联支付 邮驿付必传
            $source = $store->source;
            $check_data = [
                'total_amount' => '付款金额',
                'ways_type' => '通道类型',
                'open_id' => '购买者id'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            if ($other_no == "" || $other_no == "NULL") {
                $other_no = "";
            }

            if ($notify_url == "" || $notify_url == "NULL") {
                $notify_url = "";
            }

            $obj_ways = new  PayWaysController();
            $ways = $obj_ways->ways_type($ways_type, $store_id, $store_pid);
            if (!$ways) {
                return json_encode([
                    'status' => '2',
                    'message' => '没有开通此类型通道'
                ]);
            }

            if ($ways->is_close) {
                return json_encode([
                    'status' => 2,
                    'message' => '此通道已关闭'
                ]);
            }

            if (isset($ways->pay_amount_e) && $total_amount > $ways->pay_amount_e) {
                return json_encode([
                    'status' => 2,
                    'message' => '单笔金额不能超过' . $ways->pay_amount_e
                ]);
            }

            $user_rate_obj = $obj_ways->getCostRate($ways->ways_type, $tg_user_id);
            $cost_rate = $user_rate_obj ? $user_rate_obj->rate : 0.00; //成本费率

            $data = [
                'merchant_name' => $merchant_name,
                'store_id' => $store_id,
                'merchant_id' => $merchant_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'store_pid' => $store_pid,
                'tg_user_id' => $tg_user_id,
                'total_amount' => $total_amount,
                'shop_price' => $shop_price,
                'remark' => $remark,
                'device_id' => $device_id,
                'shop_name' => $shop_name,
                'shop_desc' => $shop_desc,
                'open_id' => $open_id,
                'ways_type' => $ways_type,
                'other_no' => $other_no,
                'notify_url' => $notify_url,
                'auth_code' => $auth_code,
                'atqTag' => $atqTag,
                'source' => $source
            ];

            if ($is_fq) {
                $hb_fq_num = $request->get('hb_fq_num', ''); //花呗分期数
                $hb_fq_seller_percent = $request->get('hb_fq_seller_percent', 0);
                $data['hb_fq_num'] = $hb_fq_num;
                $data['hb_fq_seller_percent'] = $hb_fq_seller_percent;

                return $this->fq_qr_auth_pay_public($data, $ways);
            } else {
                if ($origin_id) $data['origin_id'] = $origin_id;
                if ($pay_method) $data['pay_method'] = $pay_method;
                if ($cost_rate) $data['cost_rate'] = $cost_rate;
                return $this->qr_auth_pay_public($data, $ways);
            }

        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }


    //静态二维码 提交 中转站 公共
    public function qr_auth_pay_public($data, $ways)
    {
        try {
//            Log::info('静态二维码');
//            Log::info($data);
            $merchant_name = isset($data['merchant_name']) ? $data['merchant_name'] : "";
            $store_id = isset($data['store_id']) ? $data['store_id'] : "";
            $merchant_id = isset($data['merchant_id']) ? $data['merchant_id'] : "0";
            $config_id = isset($data['config_id']) ? $data['config_id'] : "";
            $store_name = isset($data['store_name']) ? $data['store_name'] : "";
            $store_pid = isset($data['store_pid']) ? $data['store_pid'] : "";
            $tg_user_id = isset($data['tg_user_id']) ? $data['tg_user_id'] : "";
            $total_amount = isset($data['total_amount']) ? $data['total_amount'] : "";
            $shop_price = isset($data['shop_price']) ? $data['shop_price'] : "";
            $remark = isset($data['remark']) ? $data['remark'] : "";
            $device_id = isset($data['device_id']) ? $data['device_id'] : "";
            $shop_name = isset($data['shop_name']) ? $data['shop_name'] : "";
            $shop_desc = isset($data['shop_desc']) ? $data['shop_desc'] : "";
            $open_id = isset($data['open_id']) ? $data['open_id'] : "";
            $ways_type = isset($data['ways_type']) ? $data['ways_type'] : "";
            $other_no = isset($data['other_no']) ? $data['other_no'] : "";
            $notify_url = isset($data['notify_url']) ? $data['notify_url'] : "";
            $coupon_type = isset($data['coupon_type']) ? $data['coupon_type'] : ""; //优惠类型 1 充值
            $pay_amount = isset($data['pay_amount']) ? $data['pay_amount'] : $total_amount; //支付金额
            $dk_jf = isset($data['dk_jf']) ? $data['dk_jf'] : '0'; //抵扣积分
            $dk_money = isset($data['dk_money']) ? $data['dk_money'] : '0'; //抵扣金额
            $auth_code = $data['auth_code'] ?? ''; //支付宝、微信授权码
            $origin_id = $data['origin_id'] ?? ''; //系统达达订单号
            $pay_method = $data['pay_method'] ?? 'qr_code'; //系统支付方式;qr_code-主扫,scan-被扫;alipay_face-支付宝刷脸;weixin_face-微信刷脸;wx_applet-微信小程序;ali_applet-支付宝小程序
            $cost_rate = $data['cost_rate'] ?? ''; //成本费率
            $source = $data['source'];

            //发起请求
            $data = [
                'config_id' => $config_id,
                'store_id' => $store_id,
                'merchant_id' => $merchant_id,
                'total_amount' => $pay_amount,
                'shop_price' => $shop_price,
                'remark' => $remark,
                'device_id' => $device_id,
                'config_type' => '01',
                'shop_name' => $shop_name,
                'shop_desc' => $shop_desc,
                'store_name' => $store_name,
                'open_id' => $open_id,
            ];

            //插入数据库
            $data_insert = [
                'trade_no' => '',
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'store_name' => $store_name,
                'buyer_id' => $open_id,
                'total_amount' => $total_amount,
                'shop_price' => $shop_price,
                'pay_amount' => $pay_amount,
                'payment_method' => '',
                'status' => '',
                'pay_status' => 2,
                'pay_status_desc' => '等待支付',
                'merchant_id' => $merchant_id,
                'merchant_name' => $merchant_name,
                'remark' => $remark,
                'device_id' => $device_id,
                'config_id' => $config_id,
                'company' => $ways->company,
                'other_no' => $other_no,
                'notify_url' => $notify_url,
                'coupon_type' => $coupon_type,
                'dk_jf' => $dk_jf,
                'dk_money' => $dk_money,
                'source' => $source,
                'device_name' => $device_id
            ];

            //扫码费率入库
            $data_insert['rate'] = $ways->rate;
            $data_insert['fee_amount'] = $ways->rate * $pay_amount / 100;
            if ($pay_method) $data_insert['pay_method'] = $pay_method;
            if ($cost_rate) $data_insert['cost_rate'] = $cost_rate;

            /*官方支付宝*/
            if ($ways_type == '1000') {
                //配置
                $isvconfig = new AlipayIsvConfigController();
                $config_type = '01';
                $notify_url = url('/api/alipayopen/qr_pay_notify');

                $storeInfo = $isvconfig->alipay_auth_info($store_id, $store_pid);
                if (!$storeInfo) {
                    $msg = '支付宝授权信息不存在';
                    return json_encode([
                        'status' => 2,
                        'message' => $msg
                    ]);

                }

                $out_user_id = $storeInfo->alipay_user_id; //商户的id

                //分成模式 服务商
                if ($storeInfo->settlement_type == "set_a") {
                    if ($storeInfo->config_type == '02') {
                        $config_type = '02';
                    }
                    $storeInfo = AlipayAccount::where('config_id', $config_id)
                        ->where('config_type', $config_type)
                        ->first(); //服务商的
                }

                if (!$storeInfo) {
                    $msg = '支付宝授权信息不存在';
                    return json_encode([
                        'status' => 2,
                        'message' => $msg
                    ]);

                }
                $alipay_store_id = $storeInfo->alipay_store_id;
                $out_store_id = $storeInfo->out_store_id;

                $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);

                $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                $pay_obj = new PayController();

                $data['out_trade_no'] = $out_trade_no;
                $data['alipay_store_id'] = $alipay_store_id;
                $data['out_store_id'] = $out_store_id;
                $data['out_user_id'] = $out_user_id;
                $data['app_auth_token'] = $storeInfo->app_auth_token;
                $data['config'] = $config;
                $data['notify_url'] = $notify_url;

                if ($ways->credit == "00") {
                    $disable_pay_channels = 'credit_group'; //禁用信用方式
                    $data['disable_pay_channels'] = $disable_pay_channels;
                }

                $return = $pay_obj->qr_auth_pay($data);
                $return_array = json_decode($return, true);
                if ($return_array['status'] == 1) {
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;

                    //入库参数
                    $insert_re = $this->insert_day_order($data_insert);


                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }
                }

                return $return;
            }

            /*直付通支付宝*/
            if ($ways_type == '16002') {
                //配置
                $notify_url = url('/api/alipayopen/zft_qr_pay_notify');
                $config_type = '03';
                //配置
                $isvconfig = new AlipayIsvConfigController();
                $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);

                if (!$config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '直付通配置不存在'
                    ]);
                }

                //判断直付通
                $AlipayZftStore = $isvconfig->AlipayZftStore($store_id, $store_pid);
                if (!$AlipayZftStore) {
                    return json_encode([
                        'status' => 2,
                        'message' => '直付通门店不存在'
                    ]);
                }

                //禁用信用通道
                if ($ways->credit == "00") {
                    $disable_pay_channels = 'credit_group'; //禁用信用方式
                    $data['disable_pay_channels'] = $disable_pay_channels;
                }

                $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                $pay_obj = new PayController();

                $data['out_trade_no'] = $out_trade_no;
                $data['config'] = $config;
                $data['notify_url'] = $notify_url;
                $data['smid'] = $AlipayZftStore->smid;
                $data['alipay_account'] = $AlipayZftStore->alipay_account;
                $data['SettleModeType'] = $AlipayZftStore->SettleModeType;
                $data['card_alias_no'] = $AlipayZftStore->card_alias_no;

                $return = $pay_obj->zft_qr_auth_pay($data);
                $return_array = json_decode($return, true);
                if ($return_array['status'] == 1) {
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;

                    //入库参数
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }
                }

                return $return;
            }

            /*官方微信*/
            if ($ways_type == "2000") {
                $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                $data['goods_detail'] = [];
                $data['out_trade_no'] = $out_trade_no;
                $data['attach'] = $store_id . ',' . $config_id; //附加信息原样返回

                $config = new WeixinConfigController();
                $options = $config->weixin_config($config_id);
                $weixin_store = $config->weixin_merchant($store_id, $store_pid);
                if (!$weixin_store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '微信商户号不存在'
                    ]);
                }

                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;
                $data['wx_sub_merchant_id'] = $wx_sub_merchant_id;
                $data['options'] = $options;

                //分账
                $weixin_store_obj = WeixinStore::where('store_id', $store_id)->first();
                if ($weixin_store_obj) {
                    $data['is_profit_sharing'] = $weixin_store_obj->is_profit_sharing ?? ''; //是否分账(0-不分;1-分)
                    $data['wx_sharing_rate'] = $weixin_store_obj->wx_sharing_rate ?? ''; //分账比例%
                }

                $pay_obj = new \App\Api\Controllers\Weixin\PayController();
                if ($pay_method) $data['pay_method'] = $pay_method;
                $return = $pay_obj->qr_pay($data, 'JSAPI');

                $return_array = json_decode($return, true);
                if ($return_array['status'] == 1) {
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;

                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }
                }

                return $return;
            }

            /*官方微信a*/
            if ($ways_type == "4000") {
                $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                $data['goods_detail'] = [];
                $data['out_trade_no'] = $out_trade_no;
                $data['attach'] = $store_id . ',' . $config_id; //附加信息原样返回

                $config = new WeixinConfigController();
                $options = $config->weixina_config($config_id);
                $weixin_store = $config->weixina_merchant($store_id, $store_pid);
                if (!$weixin_store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '微信a商户号不存在'
                    ]);
                }

                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;
                $data['wx_sub_merchant_id'] = $wx_sub_merchant_id;
                $data['options'] = $options;

                //分账
                $weixin_store_obj = WeixinaStore::where('store_id', $store_id)->first();
                if ($weixin_store_obj) {
                    $data['is_profit_sharing'] = $weixin_store_obj->is_profit_sharing ?? ''; //是否分账(0-不分;1-分)
                    $data['wx_sharing_rate'] = $weixin_store_obj->wx_sharing_rate ?? ''; //分账比例%
                }

                $pay_obj = new \App\Api\Controllers\Weixin\PayController();
                $notify_url_a = url('api/weixin/qr_pay_notify_a');
                if ($pay_method) $data['pay_method'] = $pay_method;
                $return = $pay_obj->qr_pay($data, 'JSAPI', $notify_url_a);

                $return_array = json_decode($return, true);
                if ($return_array['status'] == 1) {
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '微信支付a';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;

                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }
                }

                return $return;
            }

            /*快钱支付宝-微信支付*/
            if ($ways_type == '3001' || $ways_type == '3002') {
                $config = new MyBankConfigController();
                $MyBankConfig = $config->MyBankConfig($config_id);
                if (!$MyBankConfig) {
                    return json_encode([
                        'status' => 2,
                        'message' => '快钱配置不存在请检查配置'
                    ]);
                }

                $mybank_merchant = $config->mybank_merchant($store_id, $store_pid);
                if (!$mybank_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '快钱商户号不存在'
                    ]);
                }

                $wx_AppId = $mybank_merchant->wx_AppId;
                $MyBankConfig = $config->MyBankConfig($config_id, $wx_AppId);
                if (!$MyBankConfig) {
                    return json_encode([
                        'status' => 2,
                        'message' => '快钱配置不存在请检查配置'
                    ]);
                }

                $pay_type = '';
                $out_trade_no = '';
                $SubAppId = $MyBankConfig->wx_AppId;
                $ali_pid = $MyBankConfig->ali_pid;
                $SpecifySubMerchId = "";
                $WechatMerchId = "";
                if ($ways_type == "3001") {
                    $pay_type = "ALI";
                    $out_trade_no = 'aliQR' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                }

                if ($ways_type == "3002") {
                    $pay_type = "WX";
                    $out_trade_no = 'wxQR' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';

                    if ($MyBankConfig->SubscribeAppId) {
                        $SubAppId = $MyBankConfig->SubscribeAppId;
                        $SpecifySubMerchId = "Y";

                        if ($mybank_merchant->WechatChannelList) {
                            $WechatChannelList = base64_decode($mybank_merchant->WechatChannelList);
                            $WechatChannelList = json_decode($WechatChannelList, true);
                            $WechatMerchId = isset($WechatChannelList[0]['WechatMerchId']) ? $WechatChannelList[0]['WechatMerchId'] : "";
                        }
                    }
                }

                $MerchantId = $mybank_merchant->MerchantId;

                $data['total_amount'] = $pay_amount;
                $data['merchant_id'] = $merchant_id;
                $data['store_id'] = $store_id;
                $data['open_id'] = $open_id;
                $data['pay_type'] = $pay_type;
                $data['remark'] = $remark;
                $data['out_trade_no'] = $out_trade_no;
                $data['MerchantId'] = $MerchantId;
                $data['SettleType'] = 'T1';
                $data['PayLimit'] = '';
                $data['Body'] = $shop_name;
                $data['DeviceCreateIp'] = \EasyWeChat\Kernel\Support\get_client_ip();
                $data['Attach'] = url('/api/mybank/notify'); //'原样输出';
                $data['NotifyUrl'] = url('/api/mybank/notify');
                $data['ali_pid'] = $ali_pid;
                $data['SubAppId'] = $SubAppId;
                $data['SpecifySubMerchId'] = $SpecifySubMerchId;
                $data['ChannelId'] = $MyBankConfig->WechatChannel;
                $data['WechatMerchId'] = $WechatMerchId;

                $obj = new QrpayController();
                $return = $obj->auth_qr($data);

                if ($return['status'] != 1) {
                    return json_encode($return);
                }

                $insert_re = $this->insert_day_order($data_insert);
                if (!$insert_re) {
                    return json_encode([
                        'status' => 2,
                        'out_trade_no' => $out_trade_no,
                        'message' => '订单未入库'
                    ]);
                }

                //支付宝返回
                if ($ways_type == "3001") {
                    return json_encode([
                        'status' => 1,
                        'message' => 'ok',
                        'out_trade_no' => $out_trade_no,
                        'trade_no' => $return['data']['PrePayId']
                    ]);
                }

                //微信支付返回
                if ($ways_type == "3002") {
                    return json_encode([
                        'status' => 1,
                        'message' => 'ok',
                        'out_trade_no' => $out_trade_no,
                        'data' => $return['data']['PayInfo']
                    ]);
                }
            }

            /*京东支付宝-微信支付-京东*/
            if ($ways_type == '6001' || $ways_type == '6002' || $ways_type == '6003') {
                $config = new JdConfigController();
                $jd_config = $config->jd_config($config_id);
                if (!$jd_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '京东配置不存在请检查配置'
                    ]);
                }

                $jd_merchant = $config->jd_merchant($store_id, $store_pid);
                if (!$jd_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '京东商户号不存在'
                    ]);
                }

                $out_trade_no = '';
                $pay_type = "";
                $appId = '';

                if ($ways_type == "6002") {
                    $pay_type = "WX";
                    $appId = $jd_config->wx_appid;
                    $out_trade_no = 'wxQR' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                }

                if ($ways_type == "6001") {
                    $pay_type = "ALIPAY";
                    $appId = $jd_config->ali_appid;
                    $out_trade_no = 'aliQR' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                }

                if ($ways_type == "6003") {
                    $pay_type = "JDPAY";

                    //是否开通白条
                    if ($jd_merchant->bt_true) {
                        $pay_type = "JIOU";
                    }

                    $appId = $jd_config->ali_appid;
                    $out_trade_no = 'jdQR' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '京东金融';
                    $data_insert['ways_source'] = 'jdjr';
                    $data_insert['ways_source_desc'] = '京东金融';
                }

                $obj = new \App\Api\Controllers\Jd\PayController();
                $jd_data = [
                    "out_trade_no" => $out_trade_no,
                    "userId" => $open_id,
                    "total_amount" => $pay_amount,
                    "remark" => '' . $remark . '',
                    "device_id" => $device_id,
                    "shop_name" => $store_name,
                    "notify_url" => url("api/jd/notify_url"),
                    "request_url" => $obj->unified_url,
                    "pay_type" => $pay_type,
                    "merchant_no" => $jd_merchant->merchant_no,
                    "return_params" => "原样返回",
                    "des_key" => $jd_merchant->des_key,
                    "md_key" => $jd_merchant->md_key,
                    "systemId" => $jd_config->systemId,
                    'gatewayPayMethod' => 'SUBSCRIPTION',//MINIPROGRAM：小程序 SUBSCRIPTION：公众号，服务号
                    'appId' => $appId,
                ];

                $return = $obj->qr_submit($jd_data);
                if ($return['status'] == 0) {
                    return json_encode($return);
                }
                $insert_re = $this->insert_day_order($data_insert);
                if (!$insert_re) {
                    return json_encode([
                        'status' => 2,
                        'message' => '订单未入库'
                    ]);
                }

                //支付宝返回
                if ($ways_type == "6001") {
                    return json_encode([
                        'status' => 1,
                        'message' => '返回成功',
                        'out_trade_no' => $out_trade_no,
                        'trade_no' => json_decode($return['data']['payInfo'], true)['tradeNO']
                    ]);
                }

                //微信返回
                if ($ways_type == "6002") {
                    return json_encode([
                        'status' => 1,
                        'out_trade_no' => $out_trade_no,
                        'message' => '返回成功',
                        'data' => $return['data']['payInfo'],
                    ]);
                }

                //京东返回
                if ($ways_type == "6003") {
                    return json_encode([
                        'status' => 1,
                        'out_trade_no' => $out_trade_no,
                        'message' => '返回成功',
                        'data' => $return['data']['payInfo'],
                    ]);
                }

            }

            /*新大陆支付宝-微信支付*/
            if ($ways_type == '8001' || $ways_type == '8002') {
                $config = new NewLandConfigController();
                $new_land_config = $config->new_land_config($config_id);
                if (!$new_land_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '新大陆配置不存在请检查配置'
                    ]);
                }

                $new_land_merchant = $config->new_land_merchant($store_id, $store_pid);
                if (!$new_land_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户新大陆通道未开通'
                    ]);
                }

                $wx_appid = $new_land_config->wx_appid;
                $data_insert['out_store_id'] = $new_land_merchant->nl_mercId;
                $request_data = [
                    'total_amount' => $pay_amount,
                    'remark' => $remark,
                    'device_id' => $device_id,
                    'shop_name' => $shop_name,
                    'key' => $new_land_merchant->nl_key,
                    'org_no' => $new_land_config->org_no,
                    'merc_id' => $new_land_merchant->nl_mercId,
                    'trm_no' => $new_land_merchant->trmNo,
                    'op_sys' => '3',
                    'opr_id' => $merchant_id,
                    'trm_typ' => 'T',
                    'open_id' => $open_id,
                    'wx_appid' => $wx_appid,
                    'paysuccurl' => url('/api/newland/pay_action'), //支付以后跳转
                ];

                if ($ways_type == "8001") {
                    $request_data['payChannel'] = "ALIPAY";
                    $out_trade_no = 'aliQR' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                }

                if ($ways_type == "8002") {
                    $request_data['payChannel'] = "WXPAY";
                    $out_trade_no = 'wxQR' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                }

                $request_data['out_trade_no'] = $out_trade_no;

                $obj = new \App\Api\Controllers\Newland\PayController();

                $is_qd = $new_land_merchant->is_qd; //是否是渠道
                //微信自己的渠道
                if ($is_qd && $request_data['payChannel'] == "WXPAY") {
                    $return = $obj->qr_submit_appid($request_data);
                } else {
                    $return = $obj->qr_submit($request_data);
                }
                //Log::info('新大陆-静态主扫');
                //Log::info($return);
                if ($return['status'] == 0) {
                    return json_encode($return);
                }

                $insert_re = $this->insert_day_order($data_insert);
                if (!$insert_re) {
                    return json_encode([
                        'status' => 2,
                        'message' => '订单未入库'
                    ]);
                }

                return json_encode([
                    'status' => 1,
                    'message' => '返回成功',
                    'data' => isset($return['data']) ? $return['data'] : "",
                    'out_trade_no' => $out_trade_no,
                    'url' => isset($return['data']['url']) ? $return['data']['url'] : "",//跳转的url
                ]);
            }

            /*和融通支付宝-微信支付*/
            if ($ways_type == '9001' || $ways_type == '9002') {
                $config = new HConfigController();
                $h_config = $config->h_config($config_id);
                if (!$h_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '和融通配置不存在请检查配置'
                    ]);
                }

                $h_merchant = $config->h_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '和融通商户号不存在'
                    ]);
                }

                $out_trade_no = '';
                $app_id = '';
                $payChannel = '';
                if ($ways_type == "9002") {
                    $payChannel = "wx";
                    $app_id = $h_config->wx_appid;

                    //判断以下域名的微信渠道
                    $array = [
                        'https://pay.ycfgd.com',
                        'https://pay.umxnt.com',
                        'https://pay.shouqupay.com',
                        'https://ss.tonlot.com',
                        'https://yb.xiangyongpay.com',
                        'https://pay.fuya18.com',
                        'https://pay.mynkj.cn',
                        'https://x.umxnt.com',
                    ];

                    //走想用渠道的商户
                    if (in_array(url(''), $array)) {
                        //看下配置是不是想用和融通的是的话就去想用那边拿openID
                        //$StorePayWay

                        //以前用的还是想用的渠道 必须传
                        $updated_at = $ways->updated_at;
                        if ($updated_at < '2019-06-21 12:00:00') {
                            $app_id = 'wxa68056bcca84d4dc'; //想用科技服务号
                        } else {
                            //用和融通的渠道 app_id
                            $app_id = "";
                        }
                    } else {
                        $updated_at = $ways->updated_at;
                        if ($updated_at < '2019-06-21 12:00:00') {

                        } else {
                            //用和融通的渠道 app_id
                            $app_id = "";
                        }
                    }

                    $out_trade_no = 'wxQR' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                }
                if ($ways_type == "9001") {
                    //暂时读取支付宝官方的appid
                    $isvconfig = new AlipayIsvConfigController();
                    $alipay_config = $isvconfig->AlipayIsvConfig($config_id);
                    $app_id = $alipay_config->alipay_pid;
                    $payChannel = "ali";
                    $out_trade_no = 'aliQR' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                }

                $obj = new \App\Api\Controllers\Huiyuanbao\PayController();

                $request_url = $obj->oriJsPay_url; //有渠道号原生
                $callback_url = url('api/huiyuanbao/pay_action?out_trade_no=') . $out_trade_no;
                $h_data = [
                    "out_trade_no" => $out_trade_no,
                    "userId" => $open_id,
                    "total_amount" => $pay_amount,
                    "remark" => '' . $remark . '',
                    "device_id" => $device_id,
                    "shop_name" => $store_name,
                    "notify_url" => url("api/huiyuanbao/pay_notify"),
                    "request_url" => $request_url,
                    "return_params" => "原样返回",
                    "md_key" => $h_config->md_key,
                    "payChannel" => $payChannel,
                    'orgNo' => $h_config->orgNo,
                    'mid' => $h_merchant->h_mid,
                    'app_id' => $app_id,
                    'callBackUrl' => $callback_url,
                ];

                $return = $obj->qr_submit($h_data);
                if ($return['status'] == 0) {
                    return json_encode($return);
                }
                $insert_re = $this->insert_day_order($data_insert);

                if (!$insert_re) {
                    return json_encode([
                        'status' => 2,
                        'message' => '订单未入库'
                    ]);
                }

                //支付宝返回
                if ($ways_type == "9001") {
                    return json_encode([
                        'status' => 1,
                        'url' => $return['code_url'],
                        'trade_no' => isset($return['data']['package']) && $return['data']['package'] != "" ? $return['data']['package'] : "",
                        'message' => '返回成功',
                        'out_trade_no' => $out_trade_no,
                        'data' => $return,
                    ]);
                }

                //微信返回
                if ($ways_type == "9002") {
                    return json_encode([
                        'status' => 1,
                        'url' => $return['code_url'],
                        'message' => '返回成功',
                        'out_trade_no' => $out_trade_no,
                        'trade_no' => isset($return['data']['package']) && $return['data']['package'] != "" ? $return['data']['package'] : "",
                        'data' => $return,
                    ]);
                }


            }

            /*联拓富支付宝-微信支付*/
            if ($ways_type == '10001' || $ways_type == '10002') {
                $config = new LtfConfigController();

                $ltf_merchant = $config->ltf_merchant($store_id, $store_pid);
                if (!$ltf_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户号不存在'
                    ]);
                }
                $data_insert['out_store_id'] = $ltf_merchant->merchantCode;
                $obj = new \App\Api\Controllers\Ltf\PayController();
                $request_data = [
                    'total_amount' => $pay_amount,
                    'remark' => $remark,
                    'device_id' => $device_id,
                    'shop_name' => $shop_name,
                    'outTradeNo' => '',
                    'totalAmount' => $pay_amount,
                    'merchant_no' => $ltf_merchant->merchantCode,
                    'appId' => $ltf_merchant->appId,
                    'key' => $ltf_merchant->md_key,
                    'returnUrl' => url('/api/ltf/pay_action'),
                    'notify_url' => url('/api/ltf/pay_notify'),
                    'request_url' => $obj->jspay_url,
                    'channel' => '',
                    'return_params' => '',
                    'userId' => $open_id,
                    'tradeType' => 'JSAPI',
                ];


                if ($ways_type == "10001") {
                    $out_trade_no = 'aliQR' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $request_data['channel'] = 'ALIPAY';


                }


                if ($ways_type == "10002") {
                    $out_trade_no = 'wxQR' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $request_data['channel'] = 'WXPAY';

                }

                $request_data['out_trade_no'] = $out_trade_no;
                $return = $obj->qr_submit($request_data);

                if ($return['status'] == 0) {
                    return json_encode($return);
                }


                $insert_re = $this->insert_day_order($data_insert);

                if (!$insert_re) {
                    return json_encode([
                        'status' => 2,
                        'message' => '订单未入库'
                    ]);
                }

                $return_data = [
                    'status' => 1,
                    'message' => '返回成功',
                    'out_trade_no' => $out_trade_no,
                    'data' => $return['data'],//跳转的url
                ];

                if ($request_data['channel'] == "ALIPAY") {
                    $return_data['tradeNO'] = $return['data']['transactionId'];
                }
                return json_encode($return_data);

            }

            /*富友通支付宝-微信支付*/
            if ($ways_type == '11001' || $ways_type == '11002' || $ways_type == '11003') {
                $config = new FuiouConfigController();
                $fuiou_config = $config->fuiou_config($data['config_id']);

                if (!$fuiou_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '富友配置不存在请检查配置'
                    ]);
                }

                $fuiou__merchant = $config->fuiou_merchant($store_id, $store_pid);
                if (!$fuiou__merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '富友商户号不存在'
                    ]);
                }

                $trade_type = "";
                $out_trade_no = '';
                if ($ways_type == "11002") {
                    $out_trade_no = '1300' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $trade_type = "JSAPI";

                }
                if ($ways_type == "11001") {
                    //暂时读取支付宝官方的appid
                    $out_trade_no = '1300' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $trade_type = "FWC";

                }
                if ($ways_type == "11003") {
                    $out_trade_no = '1300' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '云闪付';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '云闪付';
                    $trade_type = "UNIONPAY";

                }

                $obj = new \App\Api\Controllers\Fuiou\PayController();

                $fuiou_data = [
                    'ins_cd' => $fuiou_config->ins_cd,//机构号
                    'mchnt_cd' => $fuiou__merchant->mchnt_cd,//商户号
                    'trade_type' => $trade_type,//订单类型订单类型:ALIPAY，WECHAT，UNIONPAY(银联二维码），BESTPAY(翼支付)
                    'goods_des' => $store_name,//商品描述
                    'mchnt_order_no' => $out_trade_no,//商户订单号
                    'order_amt' => $pay_amount * 100,//总金额 分
                    'openid' => $open_id,
                    'pem' => $fuiou_config->my_private_key,
                ];

                $return = $obj->qr_submit($fuiou_data);

                if ($return['status'] == 0) {
                    return json_encode($return);
                }
                $insert_re = $this->insert_day_order($data_insert);

                if (!$insert_re) {
                    return json_encode([
                        'status' => 2,
                        'message' => '订单未入库'
                    ]);
                }

                //支付宝返回
                if ($ways_type == "11001") {
                    return json_encode([
                        'status' => 1,
                        'out_trade_no' => $out_trade_no,
                        'message' => '数据返回成功',
                        'reserved_transaction_id' => $return['data']['reserved_transaction_id'],
                        'data' => $return,
                    ]);
                }

                //微信返回
                if ($ways_type == "11002") {
                    return json_encode([
                        'status' => 1,
                        'out_trade_no' => $out_trade_no,
                        'message' => '数据返回成功',
                        'data' => $return,
                        'pay' => $return['data']['reserved_pay_info']
                    ]);
                }


            }

            /*哆啦宝支付宝-微信支付*/
            if ($ways_type == '15001' || $ways_type == '15002' || $ways_type == '15003' || $ways_type == '15004') {
                $manager = new ManageController();
                $dlb_config = $manager->pay_config($data['config_id']);

                if (!$dlb_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '哆啦宝配置配置不存在请检查配置'
                    ]);
                }

                $dlb_merchant = $manager->dlb_merchant($store_id, $store_pid);
                if (!$dlb_merchant && !empty($dlb_merchant->mch_num) && !empty($dlb_merchant->shop_num)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '哆啦宝配置商户未补充商户编号等信息!'
                    ]);
                }
                $orderheader = $ways_type == '15001' ? 'alidlb' : 'wxdlb';
                $out_trade_no = $orderheader . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                $dlb_data = array(
                    "agentNum" => $dlb_config->agent_num,
                    "accessKey" => $dlb_config->access_key,
                    "secretKey" => $dlb_config->secret_key,
                    "customerNum" => $dlb_merchant->mch_num,
                    "shopNum" => $dlb_merchant->shop_num,
                    "requestNum" => $out_trade_no,
                    "amount" => $pay_amount,
                    "authId" => $open_id,
                    "callbackUrl" => url('api/dlb/pay_notify'),
                    // "payType" => "NATIVE",
                );
                if ($ways_type == "15002") {
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $dlb_data['bankType'] = 'WX';
                }
                if ($ways_type == "15001") {
                    //暂时读取支付宝官方的appid
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $dlb_data['bankType'] = 'ALIPAY';
                }

                if ($ways_type == "15003") {
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '京东支付';
                    $data_insert['ways_source'] = 'jd';
                    $data_insert['ways_source_desc'] = '京东支付';
                    $dlb_data['bankType'] = 'JD';
                }

                $return = $manager->pay($dlb_data);
                if ($return['status'] == 0) {
                    return json_encode($return);
                }
                $insert_re = $this->insert_day_order($data_insert);
                if (!$insert_re) {
                    return json_encode([
                        'status' => 2,
                        'message' => '订单未入库'
                    ]);
                }

                //支付宝返回
                if ($ways_type == "15001") {
                    return json_encode([
                        'status' => 1,
                        'out_trade_no' => $out_trade_no,
                        'reserved_transaction_id' => $return['data']['TRADENO'],
                        'data' => $return,
                    ]);
                }

                //微信返回
                if ($ways_type == "15002") {
                    return json_encode($return);
                }

                //京东返回
                if ($ways_type == "15003") {
                    return json_encode([
                        'status' => 1,
                        'out_trade_no' => $out_trade_no,
                        'pay_url' => $return['data']['PAY_URL'],
                    ]);
                }


            }

            //传化支付二维码
            if ($ways_type == '12001' || $ways_type == '12002') {
                $config = new TfConfigController();


                $h_merchant = $config->tf_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '传化商户号不存在'
                    ]);
                }

                $tf_config = $config->tf_config($data['config_id'], $h_merchant->qd);

                if (!$tf_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '传化配置不存在请检查配置'
                    ]);
                }

                $payChannel = 'ALIPAY_MP';
                $out_trade_no = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));

                //支付宝
                if ($ways_type == '12001') {
                    $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $payChannel = 'ALIPAY_MP';

                }
                if ($ways_type == '12002') {
                    $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $payChannel = 'WECHAT_MP';
                    $data['wx_appid'] = $tf_config->wx_appid;
                }


                $obj = new \App\Api\Controllers\Tfpay\PayController();
                $data['mch_id'] = $tf_config->mch_id; //
                $data['pub_key'] = $tf_config->pub_key; //
                $data['pri_key'] = $tf_config->pri_key; //
                $data['sub_mch_id'] = $h_merchant->sub_mch_id; //
                $data['notify_url'] = url('/api/tfpay/notify_url');
                $data['channel'] = $payChannel; //
                $data['out_trade_no'] = $out_trade_no; //


                $return = $obj->qr_submit($data);

                if ($return['status'] == 1) {

                    if ($return['data']['trade_status'] == 6) {
                        return json_encode([
                            'status' => 2,
                            'message' => $return['data']['remark']
                        ]);
                    }


                    $insert_re = $this->insert_day_order($data_insert);

                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }


                    //支付宝
                    if ($ways_type == 12001) {
                        return json_encode([
                            'status' => 1,
                            'trade_no' => substr($return['data']['pay_info']['trade_no'], 2),
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => $return['data']['pay_info'],
                        ]);

                    }


                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }

            }

            //随行付二维码
            if ($ways_type == '13001' || $ways_type == '13002') {
                $config = new VbillConfigController();
                $vbill_config = $config->vbill_config($data['config_id']);
                if (!$vbill_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付配置不存在请检查配置'
                    ]);
                }

                $vbill_merchant = $config->vbill_merchant($store_id, $store_pid);
                if (!$vbill_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付商户号不存在'
                    ]);
                }
                //支付宝
                if ($ways_type == '13001') {
                    $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'ALIPAY';
                    $data['pay_way'] = '02'; //02 公众号/服务窗/js支付;03-小程序
                }

                //微信
                if ($ways_type == '13002') {
                    $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'WECHAT';
                    $data['subAppid'] = $vbill_merchant->wx_channel_appid ? $vbill_merchant->wx_channel_appid : 'wx3fc7983c75d88330';
                    $data['pay_way'] = '02'; //02 公众号/服务窗/js支付;03-小程序
                }

                //微信小程序
                if ($pay_method == 'wx_applet') {
                    $wechat_appid = MerchantStoreAppidSecret::where('store_id', $store_id)->first();
                    $out_trade_no = 'wx_applet' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信小程序';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信小程序';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'WECHAT';
                    $data['subAppid'] = $wechat_appid->wechat_appid ? $wechat_appid->wechat_appid : '';
                    $data['pay_way'] = '03'; //02 公众号/服务窗/js支付;03-小程序
                }

                //支付宝小程序
                if ($pay_method == 'ali_applet') {
                    $out_trade_no = 'ali_applet' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝小程序';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝小程序';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'ALIPAY';
                    $data['pay_way'] = '02'; //02 公众号/服务窗/js支付;03-小程序
                }

                $obj = new \App\Api\Controllers\Vbill\PayController();
                $data['notify_url'] = url('/api/vbill/pay_notify_url'); //回调地址
                $data['request_url'] = $obj->qr_pay_url; //请求地址
                $data['mno'] = $vbill_merchant->mno;
                $data['privateKey'] = $vbill_config->privateKey; //
                $data['sxfpublic'] = $vbill_config->sxfpublic; //
                $data['orgId'] = $vbill_config->orgId; //
                $data['out_trade_no'] = $out_trade_no;

                $return = $obj->qr_submit($data);
                if ($return['status'] == 1) {
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    //如果是小程序付款，且走的达达配送
                    if (($pay_method == 'wx_applet' || $pay_method == 'ali_applet') && $origin_id) {
                        $dada_order_obj = DadaOrder::where('origin_id', $origin_id)->first();
                        if ($dada_order_obj) {
                            $dada_order_res = $dada_order_obj->update([
                                'out_trade_no' => $out_trade_no
                            ]);
                            if (!$dada_order_res) {
                                Log::info('达达配送单：' . $origin_id . '，支付订单：' . $out_trade_no . '更新失败');
                            }
                        }
                    }

                    //支付宝
                    if ($ways_type == 13001) {
                        return json_encode([
                            'status' => 1,
                            'trade_no' => $return['data']['respData']['source'],
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => $return['data']['respData'],
                        ]);
                    }
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }
            }

            //汇付二维码
            if ($ways_type == '18001' || $ways_type == '18002') {
                $config = new HuiPayConfigController();
                $hui_pay_config = $config->hui_pay_config($data['config_id']);
                if (!$hui_pay_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '汇付配置不存在请检查配置'
                    ]);
                }

                $hui_pay_merchant = $config->hui_pay_merchant($store_id, $store_pid);
                if (!$hui_pay_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '汇付商户不存在'
                    ]);
                }
                if (!$hui_pay_merchant->mer_id) {
                    return json_encode([
                        'status' => 2,
                        'message' => '汇付商户号不存在'
                    ]);
                }

                //支付宝
                if ($ways_type == '18001') {
                    $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'ALIPAY';
                }

                //微信
                if ($ways_type == '18002') {
                    $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'WECHAT';
                    $data['subAppid'] = $hui_pay_config->wx_appid ? $hui_pay_config->wx_appid : 'wx3fc7983c75d88330';
                }

                $obj = new \App\Api\Controllers\HuiPay\PayController();
                $data['notify_url'] = url('/api/huipay/pay_notify_url'); //回调地址
                $data['request_url'] = $obj->back_scan; //反扫请求地址
                $data['mer_cust_id'] = $hui_pay_config->mer_cust_id; //商户客户号
                $data['user_cust_id'] = $hui_pay_merchant->user_cust_id; //用户客户号
                $data['private_key'] = $hui_pay_config->private_key;
                $data['public_key'] = $hui_pay_config->public_key;
                $data['org_id'] = $hui_pay_config->org_id; //机构号
                $data['out_trade_no'] = $out_trade_no;
                $data['pay_way'] = '02'; //02 公众号/服务窗/js支付
                $data['user_id_query_url'] = $obj->user_info; //获取正扫支付时的用户id请求地址
                $data['auth_code'] = $auth_code; //支付宝、微信授权码
                $data['device_id'] = $hui_pay_merchant->device_id ?? $device_id; //机具id

                $return = $obj->qr_submit($data); //0-系统错误 1-成功 2-处理中 3-失败
                if ($return['status'] == 1) {
                    $data_insert['pay_status'] = '1';
                    $data_insert['pay_status_desc'] = '支付成功';
                    $data_insert['trade_no'] = $return['data']['party_order_id']; //渠道支付凭证号
                    $data_insert['fee_amount'] = $return['data']['fee_amt'] ?? ($ways->rate * $pay_amount / 100); //手续费金额
                    $data_insert['device_id'] = $hui_pay_merchant->device_id ?? $device_id;
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    //支付宝
                    if ($ways_type == 18001) {
                        return json_encode([
                            'status' => 1,
                            'trade_no' => $return['data']['party_order_id'], //渠道支付凭证号
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => $return['data'],
                        ]);
                    }
                } elseif ($return['status'] == '2') {
                    $data_insert['trade_no'] = $return['data']['party_order_id']; //渠道支付凭证号
                    $data_insert['fee_amount'] = $return['data']['fee_amt'] ?? ($ways->rate * $pay_amount / 100); //手续费金额
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    //支付宝
                    if ($ways_type == 18001) {
                        return json_encode([
                            'status' => 1,
                            'trade_no' => $return['data']['party_order_id'], //渠道支付凭证号
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => $return['data'],
                        ]);
                    }
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }
            }

            //随行付银联 二维码
            if ($ways_type == '13004') {
                $config = new VbillConfigController();
                $vbill_config = $config->vbill_config($data['config_id']);
                if (!$vbill_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付配置不存在请检查配置'
                    ]);
                }

                $vbill_merchant = $config->vbill_merchant($store_id, $store_pid);
                if (!$vbill_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付商户号不存在'
                    ]);
                }

                //银联扫码
                if ($ways_type == '13004') {
                    $out_trade_no = 'un_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '银联扫码';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'UNIONPAY';
                }

                $obj = new \App\Api\Controllers\Vbill\PayController();
                $data['notify_url'] = url('/api/vbill/pay_notify_url'); //回调地址
                $data['request_url'] = $obj->qr_pay_url; //请求地址;
                $data['mno'] = $vbill_merchant->mno;
                $data['privateKey'] = $vbill_config->privateKey; //
                $data['sxfpublic'] = $vbill_config->sxfpublic; //
                $data['orgId'] = $vbill_config->orgId; //
                $data['out_trade_no'] = $out_trade_no;
                $data['pay_way'] = '02'; //02 公众号/服务窗/js支付

                $return = $obj->qr_submit($data);
                if ($return['status'] == 1) {
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    return json_encode([
                        'status' => 1,
                        'data' => $return['data']['respData'],
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }
            }

            //随行付A二维码
            if ($ways_type == '19001' || $ways_type == '19002') {
                $config = new VbillConfigController();
                $vbill_config = $config->vbilla_config($data['config_id']);
                if (!$vbill_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付配置不存在请检查配置'
                    ]);
                }

                $vbill_merchant = $config->vbilla_merchant($store_id, $store_pid);
                if (!$vbill_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付商户号不存在'
                    ]);
                }

                //支付宝
                if ($ways_type == '19001') {
                    $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'ALIPAY';
                    $data['pay_way'] = '02'; //02 公众号/服务窗/js支付
                }

                //微信小程序
                if ($pay_method == 'wx_applet') {
                    $wechat_appid = MerchantStoreAppidSecret::where('store_id', $store_id)->first();
                    $out_trade_no = 'wx_applet' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信小程序';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信小程序';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'WECHAT';
                    $data['subAppid'] = $wechat_appid->wechat_appid ? $wechat_appid->wechat_appid : '';
                    $data['pay_way'] = '03'; //02 公众号/服务窗/支付宝小程序;03-微信小程序
                } else if ($ways_type == '19002' && $pay_method != 'wx_applet') { //微信
                    $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'WECHAT';
                    $data['subAppid'] = $vbill_merchant->wx_channel_appid ? $vbill_merchant->wx_channel_appid : 'wx69adc3683ef01a09';
                    $data['pay_way'] = '02'; //02 公众号/服务窗/js支付
                }

                // //微信
                // if ($ways_type == '19002') {
                //     $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                //     //入库参数
                //     $data_insert['ways_type'] = $ways_type;
                //     $data_insert['ways_type_desc'] = '微信支付';
                //     $data_insert['ways_source'] = 'weixin';
                //     $data_insert['ways_source_desc'] = '微信支付';
                //     $data_insert['out_trade_no'] = $out_trade_no;
                //     $data['pay_type'] = 'WECHAT';
                //     $data['subAppid'] = $vbill_merchant->wx_channel_appid ? $vbill_merchant->wx_channel_appid : 'wx69adc3683ef01a09';
                //     $data['pay_way'] = '02'; //02 公众号/服务窗/js支付
                // }

                //支付宝小程序
                if ($pay_method == 'ali_applet') {
                    $out_trade_no = 'ali_applet' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝小程序';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝小程序';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'ALIPAY';
                    $data['pay_way'] = '02'; //02 公众号/服务窗/js支付/支付宝小程序;03-微信小程序
                }

                $obj = new \App\Api\Controllers\Vbill\PayController();
                $data['notify_url'] = url('/api/vbill/pay_notify_a_url'); //回调地址
                $data['request_url'] = $obj->qr_pay_url; //请求地址;
                $data['mno'] = $vbill_merchant->mno;
                $data['privateKey'] = $vbill_config->privateKey; //
                $data['sxfpublic'] = $vbill_config->sxfpublic; //
                $data['orgId'] = $vbill_config->orgId; //
                $data['out_trade_no'] = $out_trade_no;

                $return = $obj->qr_submit($data);
                if ($return['status'] == 1) {
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    //如果是小程序付款，且走的达达配送
                    if (($pay_method == 'wx_applet' || $pay_method == 'ali_applet') && $origin_id) {
                        $dada_order_obj = DadaOrder::where('origin_id', $origin_id)->first();
                        if ($dada_order_obj) {
                            $dada_order_res = $dada_order_obj->update([
                                'out_trade_no' => $out_trade_no
                            ]);
                            if (!$dada_order_res) {
                                Log::info('达达配送2单：' . $origin_id . '，支付订单：' . $out_trade_no . '更新失败');
                            }
                        }
                    }

                    //支付宝
                    if ($ways_type == 19001) {
                        return json_encode([
                            'status' => 1,
                            'trade_no' => $return['data']['respData']['source'],
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => $return['data']['respData'],
                        ]);
                    }
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }
            }

            //海科融通 二维码
            if ($ways_type == '22001' || $ways_type == '22002') {
                $config = new HkrtConfigController();
                $hkrt_config = $config->hkrt_config($data['config_id']);
                if (!$hkrt_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '海科融通配置不存在请检查配置'
                    ]);
                }

                $hkrt_merchant = $config->hkrt_merchant($store_id, $store_pid);
                if (!$hkrt_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '海科融通进件门店不存在'
                    ]);
                }

                //支付宝
                if ($ways_type == '22001') {
                    $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'ALIPAY';
                }

                //微信
                if ($ways_type == '22002') {
                    $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'WECHAT';
                    $data['subAppid'] = $hkrt_merchant->wx_channel_appid ? $hkrt_merchant->wx_channel_appid : 'wx3fc7983c75d88330';
                }

                $obj = new \App\Api\Controllers\Hkrt\PayController();
                $hkrt_pay_type = $data['pay_type'];
                if ($hkrt_pay_type) {
                    if ($hkrt_pay_type == 'ALIPAY') {
                        $hkrt_trade_type = 'ALI';
                    } else {
                        $hkrt_trade_type = 'WX';
                    }
                } else {
                    $hkrt_trade_type = '';
                }
                $polymeric_jsapipay_data = [
                    'access_id' => $hkrt_config->access_id, //
                    'trade_type' => $hkrt_trade_type, //M,支付方式,微信支付-WX;支付宝支付-ALI;银联二维码支付-UNIONQR
                    'merch_no' => $hkrt_merchant->merch_no, //M,商户在SaaS平台的编号
                    'out_trade_no' => $out_trade_no, //M,服务商的交易订单编号
                    'total_amount' => $data['total_amount'], //M,订单总金额，以元为单位
                    'userid' => $open_id, //M,(支付宝/微信/银联二维码)的用户唯一标识
                    'appid' => $hkrt_config->wx_appid, //微信公众号appid,微信交易必须上送
                    'open_id' => $open_id, //???
                    'notify_url' => url('/api/hkrt/pay_notify_url'), //支付成功后的通知地址
//                    'sn' => $sn, //厂商终端号
//                    'pn' => $pn, //SAAS终端号
                    'remark' => $remark, //交易备注
                    'limit_pay' => '0', //限制贷记卡支付,0-不限制贷记卡支付;1-禁止使用贷记卡支付.默认为0
                    'access_key' => $hkrt_config->access_key
                ];
                Log::info('海科融通-主扫-加签参数');
                Log::info($polymeric_jsapipay_data);
                $return = $obj->qr_submit($polymeric_jsapipay_data); //0-失败 1-成功
                if ($return['status'] == 1) {
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    //支付宝
                    if ($ways_type == 22001) {
                        return json_encode([
                            'status' => 1,
                            'trade_no' => $return['data']['trade_no'],
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => $return['data'],
                        ]);
                    }
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }
            }

            //易生支付 二维码
            if ($ways_type == '21001' || $ways_type == '21002' || $ways_type == '21004') {
                $easyPayStoresImages = EasypayStoresImages::where('store_id', $store_id)->select('new_config_id')->first();
                if (!$easyPayStoresImages) {
                    $easyPayStoresImages = EasypayStoresImages::where('store_id', $store_pid)->select('new_config_id')->first();
                }
                $config = new EasyPayConfigController();
                $easypay_config = $config->easypay_config($easyPayStoresImages->new_config_id);
                if (!$easypay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生支付配置不存在请检查配置'
                    ]);
                }

                $easypay_merchant = $config->easypay_merchant($store_id, $store_pid);
                if (!$easypay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生支付商户号不存在'
                    ]);
                }
                $store = Store::where('store_id', $store_id)->first();
                $location = '';
                if (!$store->lat || !$store->lng) {
                    $storeController = new \App\Api\Controllers\User\StoreController();
                    $address = $store->province_name . $store->city_name . $store->area_name . $store->store_address;//获取经纬度的地址
                    $api_res = $storeController->query_address($address, env('LBS_KEY'));
                    if ($api_res['status'] == '1') {
                        $store_lng = $api_res['result']['lng'];
                        $store_lat = $api_res['result']['lat'];
                        $store->update([
                            'lng' => $store_lng,
                            'lat' => $store_lat,
                        ]);
                        $location = "+" . $store_lat . "/-" . $store_lng;
                    } else {
//                            Log::info('添加门店-获取经纬度错误');
//                            Log::info($api_res['message']);
                    }

                } else {
                    $location = "+" . $store->lat . "/-" . $store->lng;
                }
                //获取到账标识
                $EasypayStoresImages = EasypayStoresImages::where('store_id', $store_id)
                    ->select('real_time_entry')
                    ->first();
                $patnerSettleFlag = '0'; //秒到
                if ($EasypayStoresImages) {
                    if ($EasypayStoresImages->real_time_entry == 1) {
                        $patnerSettleFlag = '0';
                    } else {
                        $patnerSettleFlag = '1';
                    }
                }

                $out_trade_no = substr($easypay_config->channel_id, 0, 4) . substr($easypay_config->channel_id, -4) . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%02d', rand(0, 99));

                if ($ways_type == '21001') {
                    //$out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'ALIPAY';
                    $data['subAppid'] = $easypay_config->wx_appid ? $easypay_config->wx_appid : 'wx913c373c6271cb3a';
                }

                if ($ways_type == '21002') {
                    //$out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'WECHAT';
                    $data['subAppid'] = $easypay_config->wx_appid ? $easypay_config->wx_appid : 'wx913c373c6271cb3a';
                }
                if ($ways_type == '21004') {
                    //$out_trade_no = 'un_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '云闪付';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '云闪付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'UNIONPAY';
                    $data['subAppid'] = $easypay_config->wx_appid ? $easypay_config->wx_appid : 'wx913c373c6271cb3a';
                }
                if ($pay_method == 'wx_applet' && $ways_type == '21002') {
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付小程序';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付小程序';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['device_name'] = '微信小程序';
                    $data_insert['device_id'] = '微信小程序';
                    $data['pay_type'] = 'wx_applet';
                    $data['subAppid'] = $this->min_appid;

                }

                $obj = new \App\Api\Controllers\EasyPay\PayController();
                $easypay_data = [
                    'channel_id' => $easypay_config->channel_id,
                    'mno' => $easypay_merchant->term_mercode,
                    'device_id' => $easypay_merchant->term_termcode,
                    'out_trade_no' => $out_trade_no,
                    'pay_type' => $data['pay_type'],
                    'total_amount' => $total_amount,
                    'shop_name' => $shop_name,
                    'code' => $open_id,
                    'notify_url' => url('/api/easypay/pay_notify_url'),
                    'location' => $location,
                    'patnerSettleFlag' => $patnerSettleFlag,
                    'subAppid' => $data['subAppid'],
                    'store_name' => $store_name,
                    'key' => $easypay_config->channel_key
                ];
                $return = $obj->qr_submit($easypay_data);
                if ($return['status'] == '1') {
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }
                    //微收单1.0
//                    if ($ways_type == 21001) { //支付宝
//
//                        return json_encode([
//                            'status' => '1',
//                            'trade_no' => $return['data']['prepayid'],
//                            'data' => $return['data'],
//                        ]);
//
//                    } elseif ($ways_type == 21002) { //微信
//
//                        $weChatData = json_decode($return['data']['prepayid'], true);
//                        if ($weChatData) {
//                            if (isset($weChatData['timeStamp'])) $return['data']['payTimeStamp'] = $weChatData['timeStamp'];
//                            if (isset($weChatData['package'])) $return['data']['payPackage'] = $weChatData['package'];
//                            if (isset($weChatData['paySign'])) $return['data']['paySign'] = $weChatData['paySign'];
//                            if (isset($weChatData['signType'])) $return['data']['paySignType'] = $weChatData['signType'];
//                            if (isset($weChatData['nonceStr'])) $return['data']['paynonceStr'] = $weChatData['nonceStr'];
//                        }
//                        $return['data']['ordNo'] = $out_trade_no;
//
//                        return json_encode([
//                            'status' => '1',
//                            'trade_no' => $return['data']['wtorderid'],
//                            'data' => $return['data'],
//                            'wx_data' => $weChatData
//                        ]);
//
//                    } else {
//                        return json_encode([
//                            'status' => '1',
//                            'data' => $return['data'],
//                        ]);
//                    }

                    //微收单2.0
                    if ($ways_type == 21001) { //支付宝

                        return json_encode([
                            'status' => '1',
                            'trade_no' => $return['data']['pcTrace'],//支付请求后拉起支付宝支付订单号
                            'data' => $return['data'],
                        ]);
                    } elseif ($ways_type == 21002) { //微信
                        //2.0
                        $weChatData = json_decode($return['data']['wxWcPayData'], true);
                        if ($weChatData) {
                            if (isset($weChatData['timeStamp'])) $return['data']['payTimeStamp'] = $weChatData['timeStamp'];
                            if (isset($weChatData['package'])) $return['data']['payPackage'] = $weChatData['package'];
                            if (isset($weChatData['paySign'])) $return['data']['paySign'] = $weChatData['paySign'];
                            if (isset($weChatData['signType'])) $return['data']['paySignType'] = $weChatData['signType'];
                            if (isset($weChatData['nonceStr'])) $return['data']['paynonceStr'] = $weChatData['nonceStr'];
                        }

                        $return['data']['ordNo'] = $out_trade_no;
                        return json_encode([
                            'status' => '1',
                            'trade_no' => $return['data']['wxPrepayId'],
                            'data' => $return['data'],
                            'wx_data' => $weChatData
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'data' => $return['data'],
                        ]);
                    }

                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => $return['message']
                    ]);
                }
            }

            //邮驿付支付 二维码
            if ($ways_type == '29001' || $ways_type == '29002' || $ways_type == '29003') {
                $config = new PostPayConfigController();
                $post_config = $config->post_pay_config($data['config_id']);
                if (!$post_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '邮驿付支付配置不存在请检查配置'
                    ]);
                }

                $post_merchant = $config->post_pay_merchant($store_id, $store_pid);
                if (!$post_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '邮驿付支付商户号不存在'
                    ]);
                }

                if ($ways_type == '29001') {
                    $out_trade_no = 'aliqr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    if (strlen($out_trade_no) > 20) {
                        $out_trade_no = substr($out_trade_no, 0, 15) . substr(microtime(), 2, 5);
                    }
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'ALIPAY';
                }

                if ($ways_type == '29002') {
                    $out_trade_no = 'wxqr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    if (strlen($out_trade_no) > 20) {
                        $out_trade_no = substr($out_trade_no, 0, 15) . substr(microtime(), 2, 5);
                    }
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'WECHAT';
                    $data['subAppid'] = $post_merchant->wx_appid ? $post_merchant->wx_appid : 'wx3fc7983c75d88330';
                }
                if ($ways_type == '29003') {
                    $out_trade_no = 'unqr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    if (strlen($out_trade_no) > 20) {
                        $out_trade_no = substr($out_trade_no, 0, 15) . substr(microtime(), 2, 5);
                    }
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '云闪付';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '云闪付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'UNIONPAY';
                    $data['subAppid'] = $post_merchant->wx_appid ? $post_merchant->wx_appid : 'wx3fc7983c75d88330';
                }
                $post_store = Store::where('store_id', $store_id)->select('people_phone')->first();
                $obj = new \App\Api\Controllers\PostPay\PayController();
                $post_data = [
                    'orgId' => $post_config->org_id,
                    'custId' => $post_merchant->cust_id,
                    'device_id' => $post_merchant->drive_no,
                    'out_trade_no' => $out_trade_no,
                    'pay_type' => $data['pay_type'],
                    'total_amount' => $total_amount,
                    'code' => $open_id,
                    'notify_url' => '',
                    'wxAppid' => $post_config->wx_appid,
                    'zfbappid' => $post_config->ali_appid,
                    'custLogin' => $post_store->people_phone,
                ];
                $post_data['atqTag'] = isset($data['atqTag']) ? $data['atqTag'] : "0";

                $return = $obj->qr_submit($post_data);
                if ($return['status'] == '1') {
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }
                    if ($ways_type == 29001) { //支付宝

                        return json_encode([
                            'status' => '1',
                            'trade_no' => $return['data']['prepayid'],
                            'data' => $return['data'],
                        ]);
                    } elseif ($ways_type == 29002) { //微信
                        //2.0
                        $weChatData = $return['data'];
//                        if ($weChatData) {
//                            if (isset($weChatData['timeStamp'])) $return['data']['jsapiTimestamp'] = $weChatData['jsapiTimestamp'];
//                            if (isset($weChatData['package'])) $return['data']['jsapiPackage'] = $weChatData['jsapiPackage'];
//                            if (isset($weChatData['paySign'])) $return['data']['jsapiPaySign'] = $weChatData['jsapiPaySign'];
//                            if (isset($weChatData['signType'])) $return['data']['jsapiSignType'] = $weChatData['jsapiSignType'];
//                            if (isset($weChatData['nonceStr'])) $return['data']['jsapiNoncestr'] = $weChatData['jsapiNoncestr'];
//                            if (isset($weChatData['appId'])) $return['data']['jsapiAppid'] = $weChatData['jsapiAppid'];
//                        }

                        $return['data']['ordNo'] = $out_trade_no;
                        return json_encode([
                            'status' => '1',
                            'trade_no' => $return['data']['prePayId'],
                            'data' => $return['data'],
                            'wx_data' => $weChatData
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'data' => $return['data'],
                        ]);
                    }

                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => $return['message']
                    ]);
                }
            }

            //云闪付 微信27008 支付宝27009 银联27010
            if ($ways_type == '27008' || $ways_type == '27009' || $ways_type == '27010') {
                $config = new EasyPayConfigController();
                $easypay_config = $config->easypay_config($data['config_id']);
                if (!$easypay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生支付配置不存在请检查配置'
                    ]);
                }

                $easypay_merchant = $config->easypay_merchant($store_id, $store_pid);
                if (!$easypay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生支付商户号不存在'
                    ]);
                }
                $sayDodge = new SayDodgePayController();
                $tradeCode = "";
                if ($ways_type == '21009') {
                    $out_trade_no = substr($easypay_config->client_code, 0, 4) . date('YmdHis', time()) . substr($easypay_config->client_code, -4) . $sayDodge->rand_code();
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'ALIPAY';
                    $tradeCode = "WAC2B";
                }

                if ($ways_type == '21008') {
                    return json_encode([
                        'status' => '2',
                        'message' => '主扫--微信暂不支持'
                    ]);
                    $out_trade_no = substr($easypay_config->client_code, 0, 4) . date('YmdHis', time()) . substr($easypay_config->client_code, -4) . $sayDodge->rand_code();
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'WECHAT';
                    $data['subAppid'] = $easypay_merchant->wx_channel_appid ? $easypay_merchant->wx_channel_appid : 'wx3fc7983c75d88330';
                }
                if ($ways_type == '21010') {
                    $out_trade_no = substr($easypay_config->client_code, 0, 4) . date('YmdHis', time()) . substr($easypay_config->client_code, -4) . $sayDodge->rand_code();
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '银联支付';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '银联支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'UNIONPAY';
                    $tradeCode = "WUC2B";
                }
                $ysf_data = [
                    'orgId' => $easypay_config->channel_id,
                    'orgMercode' => $easypay_merchant->term_mercode,
                    'orgTermno' => $easypay_merchant->term_termcode,
                    'orgTrace' => $data['out_trade_no'],
                    'tradeAmt' => $data['total_amount'],
                    'encryptInfo' => "",
                    'fixBuyer' => "F",
                    'minage' => "18",
                    'needCheckInfo' => 'F',
                    'type' => '01',
                    'tradeCode' => $tradeCode,
                    'orgBackUrl' => url('/api/sayDodge/pay_notify_url')
                ];
                $resp = $sayDodge->native($ysf_data);
                if ($resp['status'] == '1') {
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'data' => $resp['data'],
                        ]);
                    }

                }

            }

            //随行付A银联 二维码
            if ($ways_type == '19004') {
                $config = new VbillConfigController();
                $vbill_config = $config->vbilla_config($data['config_id']);
                if (!$vbill_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付a配置不存在请检查配置'
                    ]);
                }

                $vbill_merchant = $config->vbilla_merchant($store_id, $store_pid);
                if (!$vbill_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付a商户号不存在'
                    ]);
                }

                //银联扫码
                if ($ways_type == '19004') {
                    $out_trade_no = 'un_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '银联扫码';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'UNIONPAY';
                }

                $obj = new \App\Api\Controllers\Vbill\PayController();
                $data['notify_url'] = url('/api/vbill/pay_notify_a_url'); //回调地址
                $data['request_url'] = $obj->qr_pay_url; //请求地址;
                $data['mno'] = $vbill_merchant->mno;
                $data['privateKey'] = $vbill_config->privateKey;
                $data['sxfpublic'] = $vbill_config->sxfpublic;
                $data['orgId'] = $vbill_config->orgId;
                $data['out_trade_no'] = $out_trade_no;
                $data['pay_way'] = '02'; //02 公众号/服务窗/js支付
                Log::info('随行付A银联');
                Log::info($data);
                $return = $obj->qr_submit($data);
                Log::info($return);
                if ($return['status'] == 1) {
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    return json_encode([
                        'status' => 1,
                        'data' => $return['data']['respData']
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }
            }

            //联动优势支付 二维码
            if ($ways_type == '5001' || $ways_type == '5002' || $ways_type == '5004') {
                $config = new LinkageConfigController();
                $linkage_config = $config->linkage_config($data['config_id']);
                if (!$linkage_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '联动支付配置不存在，请检查配置'
                    ]);
                }

                $linkage_merchant = $config->linkage_merchant($store_id, $store_pid);
                if (!$linkage_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '联动商户号不存在'
                    ]);
                }

                $payChannel = 'alipayJs';
                $out_trade_no = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));

                //支付宝
                if ($ways_type == '5001') {
                    $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }

                //微信
                if ($ways_type == '5002') {
                    $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $payChannel = 'wechatJs';
                }

                //银联
                if ($ways_type == '5004') {
                    $out_trade_no = 'un_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '云闪付';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '云闪付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $payChannel = 'unionpayJs';
                }

                $obj = new LinkagePayController();
                $linkageData['acqSpId'] = $linkage_config->mch_id; //代理商编号
                $linkageData['acqMerId'] = $linkage_merchant->acqMerId; //商户号
                $linkageData['out_trade_no'] = $out_trade_no; //商户订单号
                $linkageData['total_amount'] = $total_amount; //交易金额
                $linkageData['orderType'] = $payChannel; //订单类型
                $linkageData['userId'] = $open_id; //用户标识
                $linkageData['appId'] = $linkage_config->wxAppid; //C,微信及支付宝的AppId，如获取OpenID所使用的AppID非下单商户主体资质，则该字段无需上传
                $linkageData['subAppId'] = $linkage_merchant->subAppid; //O,二级商户的appId
                $linkageData['goodsInfo'] = $shop_name; //O,可上送商品描述、商户订单号等信息，用户付款成功后会在微信账单页面展示
                $linkageData['paymentValidTime'] = 600; //O,todo:订单有效时间(秒),订单有效时间从调起用户密码键盘开始算起，超时之后,用户无法继续支付
                $linkageData['notify_url'] = url('/api/linkage/pay_notify_url'); //回调地址
                $linkageData['privateKey'] = $linkage_config->privateKey; //
                $linkageData['publicKey'] = $linkage_config->publicKey; //
                $linkageReturn = $obj->qr_submit($linkageData); //-1 系统错误 0-其他 1-成功 2-验签失败 3-支付中 4-交易失败
                Log::info('联动优势二维码-结果');
                Log::info($linkageReturn);
                if ($linkageReturn['status'] == '1') {
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => '2',
                            'message' => '订单未入库'
                        ]);
                    }

                    //支付宝
                    if ($ways_type == 5001) {
                        $prepayId = $linkageReturn['data']['prepayId']; //预支付ID,调起支付插件需要
                        return json_encode([
                            'status' => '1',
                            'trade_no' => $prepayId
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'data' => $linkageReturn['data']
                        ]);
                    }
                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => $linkageReturn['message']
                    ]);
                }
            }

            /*葫芦天下 支付宝-微信支付-银联支付*/
            if ($ways_type == '23001' || $ways_type == '23002' || $ways_type == '23004') {
                $manager = new \App\Api\Controllers\Hltx\ManageController();
                $hltx_merchant = $manager->pay_merchant($store_id, $store_pid);
                $qd = $hltx_merchant->qd;

                $hltx_config = $manager->pay_config($data['config_id'], $qd);
                if (!$hltx_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => 'HL支付配置不存在请检查配置'
                    ]);
                }

                $manager->init($hltx_config);

                switch ($ways_type) {
                    case 23001:
                        $pay_channel = 'ALI';
                        break;
                    case 23002:
                        $pay_channel = 'WX';
                        break;
                    case 23004:
                        $pay_channel = 'UPAY';
                        break;
                    default:
                        $pay_channel = 'ALI';
                        break;
                }

                if (!$hltx_merchant || empty($hltx_merchant->mer_no)) {
                    return json_encode([
                        'status' => '2',
                        'message' => '商户未成功开通HL通道!'
                    ]);
                }

                $orderheader = $pay_channel . "hltx";
                $out_trade_no = $orderheader . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                $hltx_data = [
                    'merNo' => $hltx_merchant->mer_no,
                    'orderNo' => $out_trade_no,
                    'tradeType' => $pay_channel,
                    'subTradeType' => 'JSAPI',
                    'amount' => $pay_amount * 100,//转换为分
                    'orderInfo' => '商户收款',
                    'notifyUrl' => url('api/hltx/pay_notify'),
                    'deviceNo' => (empty($device_id) ? '商户收款' : $device_id),
                    'deviceIp' => \EasyWeChat\Kernel\Support\get_client_ip(),
                    "userId" => $open_id,
                ];
                if ($ways_type == "23001") {
                    //暂时读取支付宝官方的appid
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                }
                if ($ways_type == "23002") {
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $hltx_data['subAppId'] = $hltx_config['pay_appid'];
                }
                if ($ways_type == "23004") {
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '银联支付';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '银联支付';
                }
                $return = $manager->pay($hltx_data);
                if ($return['status'] != 1) {
                    return json_encode($return);
                }
                $return = $return['data'];
                if ($return['tradeStatus'] == 'E') {
                    return json_encode(['status' => 0, 'message' => $return['message']]);
                }
                $insert_re = $this->insert_day_order($data_insert);
                if (!$insert_re) {
                    return json_encode([
                        'status' => 2,
                        'message' => '订单未入库'
                    ]);
                }
                $payInfo = json_decode($return['payInfo'], true);
                //支付宝返回
                if ($ways_type == "23001") {
                    return json_encode([
                        'status' => 1,
                        'out_trade_no' => $out_trade_no,
                        'reserved_transaction_id' => $payInfo['tradeNO'],
                        'data' => $return,
                    ]);
                }

                //微信返回
                if ($ways_type == "23002") {
                    return json_encode(['status' => 1, 'data' => $payInfo]);
                }

                //银联返回
                if ($ways_type == "23004") {
                    return json_encode([
                        'status' => 1,
                        'out_trade_no' => $out_trade_no,
                        'pay_url' => $payInfo['redirectUrl'],
                    ]);
                }
            }

            //威富通 支付宝 微信 微信&公众号支付
            if ($ways_type == '27001' || $ways_type == '27002') {
                $wftPayConfig = new WftPayConfigController();
                $wftpay_merchant = $wftPayConfig->wftpay_merchant($store_id, $store_pid);
                if (!$wftpay_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户未成功开通威富通通道'
                    ]);
                }

                $wftpay_config = $wftPayConfig->wftpay_config($data['config_id']);
                if (!$wftpay_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '威富通支付配置不存在请检查配置'
                    ]);
                }

                //支付宝
                if ($ways_type == '27001') {
                    $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }

                //微信
                if ($ways_type == '27002') {
                    $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
//                    $data['sub_openid'] = ''; //todo:用户openid,微信用户关注商家公众号的openid（注：使用测试号时此参数置空，即不要传这个参数，使用正式商户号时才传入，参数名是sub_openid
//                    $data['sub_appid'] = $wftpay_config->wx_appid ? $wftpay_config->wx_appid : ''; //公众账号或小程序ID,当发起公众号支付时，值是微信公众平台基本配置中的AppID(应用ID)；当发起小程序支付时，值是对应小程序的AppID
                }

                //微信小程序
                if ($pay_method == 'wx_applet') {
                    $wechat_appid = MerchantStoreAppidSecret::where('store_id', $store_id)->first();
                    $out_trade_no = 'wx_applet' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信小程序';
                    $data_insert['ways_source'] = 'applet';
                    $data_insert['ways_source_desc'] = '微信小程序';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['sub_appid'] = $wechat_appid->wechat_appid ? $wechat_appid->wechat_appid : ''; //公众账号或小程序ID,当发起公众号支付时，值是微信公众平台基本配置中的AppID(应用ID)；当发起小程序支付时，值是对应小程序的AppID
                    $data['is_minipg'] = 1; //是否小程序支付,值为1，表示小程序支付；不传或值不为1，表示公众账号内支付
                }

                //支付宝小程序
                if ($pay_method == 'ali_applet') {
                    $wechat_appid = MerchantStoreAppidSecret::where('store_id', $store_id)->first();
                    $out_trade_no = 'ali_applet' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝小程序';
                    $data_insert['ways_source'] = 'applet';
                    $data_insert['ways_source_desc'] = '支付宝小程序';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['sub_appid'] = $wechat_appid->wechat_appid ? $wechat_appid->wechat_appid : ''; //公众账号或小程序ID,当发起公众号支付时，值是微信公众平台基本配置中的AppID(应用ID)；当发起小程序支付时，值是对应小程序的AppID
                    $data['is_minipg'] = 1; //是否小程序支付,值为1，表示小程序支付；不传或值不为1，表示公众账号内支付
                }

                $obj = new WftpayPayController();
                $data['mch_id'] = $wftpay_merchant->mch_id;
                $data['out_trade_no'] = $out_trade_no;
                $data['body'] = $shop_name;
                $data['total_amount'] = $total_amount;
                $data['private_rsa_key'] = $wftpay_config->private_rsa_key;
                $data['public_rsa_key'] = $wftpay_config->public_rsa_key;
                $return = $obj->qr_submit($data); //0-系统错误 1-成功 2-失败
                if ($return['status'] == 1) {
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    //微信 小程序
                    if ($ways_type == '27002' || $pay_method == 'wx_applet') {
                        $pay_info = json_decode($return['data']['pay_info'], true);
                        return json_encode([
                            'status' => 1,
                            'pay_info' => $pay_info,
                            'data' => $return['data']
                        ]);
                    } else {
                        array_walk($return['data'], function ($value, $key) {
                            if ($key == 'pay_info') {
                                $value = json_decode($value, true);
                            }
                            return $value;
                        });
                        return json_encode([
                            'status' => 1,
                            'data' => $return['data']
                        ]);
                    }
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }

            }

            //汇旺财 支付宝 微信 微信&公众号支付
            if ($ways_type == '28001' || $ways_type == '28002') {
                $hwcPayConfig = new HwcPayConfigController();
                $hwcpay_merchant = $hwcPayConfig->hwcpay_merchant($store_id, $store_pid);
                if (!$hwcpay_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户未成功开通汇旺财通道'
                    ]);
                }

                $hwcpay_config = $hwcPayConfig->hwcpay_config($data['config_id']);
                if (!$hwcpay_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '汇旺财支付配置不存在请检查配置'
                    ]);
                }

                //支付宝
                if ($ways_type == '28001') {
                    $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }

                //微信
                if ($ways_type == '28002') {
                    $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
//                    $data['sub_openid'] = ''; //todo:用户openid,微信用户关注商家公众号的openid（注：使用测试号时此参数置空，即不要传这个参数，使用正式商户号时才传入，参数名是sub_openid
//                    $data['sub_appid'] = $hwcpay_config->wx_appid ? $hwcpay_config->wx_appid : ''; //公众账号或小程序ID,当发起公众号支付时，值是微信公众平台基本配置中的AppID(应用ID)；当发起小程序支付时，值是对应小程序的AppID
                }

                //微信小程序
                if ($pay_method == 'wx_applet') {
                    $wechat_appid = MerchantStoreAppidSecret::where('store_id', $store_id)->first();
                    $out_trade_no = 'wx_applet' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信小程序';
                    $data_insert['ways_source'] = 'applet';
                    $data_insert['ways_source_desc'] = '微信小程序';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['sub_appid'] = $wechat_appid->wechat_appid ? $wechat_appid->wechat_appid : ''; //公众账号或小程序ID,当发起公众号支付时，值是微信公众平台基本配置中的AppID(应用ID)；当发起小程序支付时，值是对应小程序的AppID
                    $data['is_minipg'] = 1; //是否小程序支付,值为1，表示小程序支付；不传或值不为1，表示公众账号内支付
                }

                //支付宝小程序
                if ($pay_method == 'ali_applet') {
                    $wechat_appid = MerchantStoreAppidSecret::where('store_id', $store_id)->first();
                    $out_trade_no = 'ali_applet' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝小程序';
                    $data_insert['ways_source'] = 'applet';
                    $data_insert['ways_source_desc'] = '支付宝小程序';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['sub_appid'] = $wechat_appid->wechat_appid ? $wechat_appid->wechat_appid : ''; //公众账号或小程序ID,当发起公众号支付时，值是微信公众平台基本配置中的AppID(应用ID)；当发起小程序支付时，值是对应小程序的AppID
                    $data['is_minipg'] = 1; //是否小程序支付,值为1，表示小程序支付；不传或值不为1，表示公众账号内支付
                }

                $obj = new HwcpayPayController();
                $data['mch_id'] = $hwcpay_merchant->mch_id;
                $data['out_trade_no'] = $out_trade_no;
                $data['body'] = $shop_name;
                $data['total_amount'] = $total_amount;
                $data['private_rsa_key'] = $hwcpay_config->private_rsa_key;
                $data['public_rsa_key'] = $hwcpay_config->public_rsa_key;
                $return = $obj->qr_submit($data); //0-系统错误 1-成功 2-失败
                if ($return['status'] == 1) {
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    //微信 小程序
                    if ($ways_type == '28002' || $pay_method == 'wx_applet') {
                        $pay_info = json_decode($return['data']['pay_info'], true);
                        return json_encode([
                            'status' => 1,
                            'pay_info' => $pay_info,
                            'data' => $return['data']
                        ]);
                    } else {
                        array_walk($return['data'], function ($value, $key) {
                            if ($key == 'pay_info') {
                                $value = json_decode($value, true);
                            }
                            return $value;
                        });
                        return json_encode([
                            'status' => 1,
                            'data' => $return['data']
                        ]);
                    }
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }

            }

            //钱方 二维码
            if ($ways_type == '24001' || $ways_type == '24002') {
                $qfPayConfigObj = new QfPayConfigController();
                $qfpay_merchant = $qfPayConfigObj->qfpay_merchant($store_id, $store_pid);
                if (!$qfpay_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户未开通钱方通道'
                    ]);
                }

                $qfpay_config = $qfPayConfigObj->qfpay_config($data['config_id']);
                if (!$qfpay_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '钱方支付配置不存在请检查配置'
                    ]);
                }

                //支付宝
                if ($ways_type == '24001') {
                    $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;

                    $pay_type = 800101;  //支付类型: 支付宝扫码-800101; 支付宝反扫-800108; 支付宝服务窗支付-800107;;微信扫码-800201; 微信反扫-800208; 微信公众号支付-800207; 微信小程序支付-800213;QQ扫码-800601; QQ反扫-800608(暂不可用);;云闪付扫码-800701; 云闪付反扫-800708;;统一聚合反扫-800008
                }

                //微信
                if ($ways_type == '24002') {
                    $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;

                    $pay_type = 800201;  //支付类型: 支付宝扫码-800101; 支付宝反扫-800108; 支付宝服务窗支付-800107;;微信扫码-800201; 微信反扫-800208; 微信公众号支付-800207; 微信小程序支付-800213;QQ扫码-800601; QQ反扫-800608(暂不可用);;云闪付扫码-800701; 云闪付反扫-800708;;统一聚合反扫-800008
                }

                //银联
                if ($ways_type == '24004') {
                    $out_trade_no = 'un_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '银联扫码';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $pay_type = 800701;  //支付类型: 支付宝扫码-800101; 支付宝反扫-800108; 支付宝服务窗支付-800107;;微信扫码-800201; 微信反扫-800208; 微信公众号支付-800207; 微信小程序支付-800213;QQ扫码-800601; QQ反扫-800608(暂不可用);;云闪付扫码-800701; 云闪付反扫-800708;;统一聚合反扫-800008
                }

                //微信小程序
                if ($pay_method == 'wx_applet') {
                    $wechat_appid = MerchantStoreAppidSecret::where('store_id', $store_id)->first();
                    $out_trade_no = 'wx_applet' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信小程序';
                    $data_insert['ways_source'] = 'applet';
                    $data_insert['ways_source_desc'] = '微信小程序';
                    $data_insert['out_trade_no'] = $out_trade_no;

                    $pay_type = 800213;  //支付类型: 支付宝扫码-800101; 支付宝反扫-800108; 支付宝服务窗支付-800107;;微信扫码-800201; 微信反扫-800208; 微信公众号支付-800207; 微信小程序支付-800213;QQ扫码-800601; QQ反扫-800608(暂不可用);;云闪付扫码-800701; 云闪付反扫-800708;;统一聚合反扫-800008
                }

                $obj = new QfPayController();
                $reqData = [
                    'key' => $qfpay_config->key,  //加签key
                    'code' => $qfpay_config->code,  //开发唯一标识
                    'total_amount' => $total_amount,  //订单支付金额
                    'mchid' => $qfpay_merchant->mchid,  //子商户号,标识子商户身份,由钱方分配(渠道系统后台查看对应商户(非业务员)子商户号,被视为对应商户的交易)
                    'out_trade_no' => $out_trade_no,  //外部订单号,开发者平台订单号,同子商户(mchid)下,每次成功调用支付(含退款)接口下单,该参数值均不能重复使用,保证单号唯一,长度不超过128字符
                    'pay_type' => $pay_type  //支付类型: 支付宝扫码-800101; 支付宝反扫-800108; 支付宝服务窗支付-800107;;微信扫码-800201; 微信反扫-800208; 微信公众号支付-800207; 微信小程序支付-800213;QQ扫码-800601; QQ反扫-800608(暂不可用);;云闪付扫码-800701; 云闪付反扫-800708;;统一聚合反扫-800008
                ];
                Log::info('钱方-二维码-入参：');
                Log::info($reqData);
                $return = $obj->payment($reqData); //1-成功 3-交易中
                Log::info('钱方-二维码-结果：');
                Log::info($return);
                if ($return['status'] == 1) {
//                    if (isset($return['data']['respcd']) && $return['data']['respcd'] == '0000') {
//                        $data_insert['pay_status'] = 1;
//                        $data_insert['pay_status_desc'] = '支付成功';
//                        if (isset($return['data']['syssn']) && $return['data']['syssn']) $data_insert['trade_no'] = $return['data']['syssn'];
//                        if (isset($return['data']['qrcode']) && $return['data']['qrcode']) $data_insert['notify_url'] = $return['data']['qrcode'];
//                        if (isset($return['data']['sysdtm']) && $return['data']['sysdtm']) $data_insert['pay_time'] = $return['data']['sysdtm'];
//                        if (isset($return['data']['txamt']) && $return['data']['txamt']) {
//                            $data_insert['buyer_pay_amount'] = $return['data']['txamt']/100;
//                            $data_insert['receipt_amount'] = $return['data']['txamt']/100;
//                        }
//                    }
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    return json_encode([
                        'status' => 1,
                        'data' => $return['data']
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }

            }

            //河南畅立收
            //易生数科支付 二维码
            if ($ways_type == '32001' || $ways_type == '32002' || $ways_type == '32003') {
                $config = new EasySkPayConfigController();
                $easyskpay_config = $config->easyskpay_config($data['config_id']);
                if (!$easyskpay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生数科支付配置不存在请检查配置'
                    ]);
                }

                $easyskpay_merchant = $config->easyskpay_merchant($store_id, $store_pid);
                if (!$easyskpay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生数科支付商户号不存在'
                    ]);
                }
                $store = Store::where('store_id', $store_id)->first();
                $location = '';
                if (!$store->lat || !$store->lng) {
                    $storeController = new \App\Api\Controllers\User\StoreController();
                    $address = $store->province_name . $store->city_name . $store->area_name . $store->store_address;//获取经纬度的地址
                    $api_res = $storeController->query_address($address, env('LBS_KEY'));
                    if ($api_res['status'] == '1') {
                        $store_lng = $api_res['result']['lng'];
                        $store_lat = $api_res['result']['lat'];
                        $store->update([
                            'lng' => $store_lng,
                            'lat' => $store_lat,
                        ]);
                        $location = "+" . $store_lat . "/-" . $store_lng;
                    } else {
//                            Log::info('添加门店-获取经纬度错误');
//                            Log::info($api_res['message']);
                    }

                } else {
                    $location = "+" . $store->lat . "/-" . $store->lng;
                }

                $out_trade_no = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));

                if ($ways_type == '32001') {
                    $out_trade_no = 'aliQr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'ALIPAY';
                }

                if ($ways_type == '32002') {
                    $out_trade_no = 'wxQr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'WECHAT';
                    $data['subAppid'] = $easyskpay_merchant->wx_appid ? $easyskpay_merchant->wx_appid : 'wx3fc7983c75d88330';
                }
                if ($ways_type == '32003') {
                    $out_trade_no = 'unQr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '云闪付';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '云闪付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'UNIONPAY';
                    $data['subAppid'] = $easyskpay_merchant->wx_appid ? $easyskpay_merchant->wx_appid : 'wx3fc7983c75d88330';
                }

                $obj = new \App\Api\Controllers\EasySkPay\PayController();
                $easypay_data = [
                    'org_id' => $easyskpay_config->org_id,
                    'mer_id' => $easyskpay_merchant->mer_id,
                    'request_no' => $out_trade_no,
                    'pay_type' => $data['pay_type'],
                    'amount' => $total_amount,
                    'open_id' => $open_id,
                    'notify_url' => url('/api/easyskpay/pay_notify_url'),
                    'location' => $location,
                    'sub_app_id' => $easyskpay_config->wx_appid
                ];
                $return = $obj->codePayByCtoB($easypay_data);
                if ($return['status'] == '1') {
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    if ($ways_type == 32001) { //支付宝
                        return json_encode([
                            'status' => '1',
                            'trade_no' => $return['data']['bizData']['prePayId'],
                            'data' => $return['data'],
                        ]);
                    } elseif ($ways_type == 32002) { //微信
                        $weChatData = json_decode($return['data']['bizData']['prePayId'], true);
                        if ($weChatData) {
                            if (isset($weChatData['timeStamp'])) $return['data']['payTimeStamp'] = $weChatData['timeStamp'];
                            if (isset($weChatData['package'])) $return['data']['payPackage'] = $weChatData['package'];
                            if (isset($weChatData['paySign'])) $return['data']['paySign'] = $weChatData['paySign'];
                            if (isset($weChatData['signType'])) $return['data']['paySignType'] = $weChatData['signType'];
                            if (isset($weChatData['nonceStr'])) $return['data']['paynonceStr'] = $weChatData['nonceStr'];
                        }

                        $return['data']['ordNo'] = $out_trade_no;
                        return json_encode([
                            'status' => '1',
                            'trade_no' => $return['data']['tradeNo'],
                            'data' => $return['data'],
                            'wx_data' => $weChatData
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'data' => $return['data'],
                        ]);
                    }

                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => $return['message']
                    ]);
                }
            }

            //银盛 二维码
            if ($ways_type == '14001' || $ways_type == '14002' || $ways_type == '14004') {
                $manager = new YinshengConfigController();
                $yinsheng_config = $manager->yinsheng_config($data['config_id']);
                if (!$yinsheng_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '银盛支付不存在,请检查配置'
                    ]);
                }

                $yinsheng_merchant = $manager->yinsheng_merchant($store_id, $store_pid);
                if (!$yinsheng_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '银盛支付,商户号未配置'
                    ]);
                }

                $out_trade_no = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));

                if ($ways_type == 14001) {
                    $ways_source = 'alipay';
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }
                if ($ways_type == 14002) {
                    $ways_source = 'weixin';
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }
                if ($ways_type == 14004) {
                    $ways_source = 'unionpay';
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '银联扫码';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }
                $bus_open_type = $yinsheng_merchant->bus_open_type;
                $openTypeArr = explode('|', $bus_open_type);
                $business_code = '';
                foreach ($openTypeArr as $openType) {
                    if ($openType == '00') { //00-扫码工作日到账
                        $business_code = '00510030';
                    }
                    if ($openType == '01') { //01-扫码实时到账 00510032
                        $business_code = '00510032';
                    }
                    if ($openType == '20') { //20-扫码次日到账
                        $business_code = '00510030';
                    }
                }
                $yinsheng_data['partner_id'] = $yinsheng_config->partner_id; //商户号
                $yinsheng_data['return_url'] = ''; //可空,同步通知地址
                $yinsheng_data['out_trade_no'] = $out_trade_no; //商户生成的订单号,生成规则前8位必须为交易日期,如20180525,范围跨度支持包含当天在内的前后一天,且只能由大小写英文字母、数字、下划线及横杠组成
                $yinsheng_data['body'] = $store_name; //商品的标题/交易标题/订单标题/订单关键字等。该参数最长为 250 个汉字
                $yinsheng_data['total_amount'] = $total_amount; //订单的资金总额，单位为 RMB-Yuan。取值范围为[0.01，100000000.00]，精确到小数点后两位。Number(10,2)指10位长度，2位精度
                $yinsheng_data['seller_id'] = $yinsheng_merchant->mer_code; //商户号
                $yinsheng_data['business_code'] = $business_code; //业务代码
                $yinsheng_data['ways_source'] = $ways_source;
                $yinsheng_data['appid'] = $yinsheng_config->wx_appid;
                $yinsheng_data['sub_openid'] = $open_id;
                $yinsheng_obj = new \App\Api\Controllers\YinSheng\PayController();
                $return = $yinsheng_obj->qr_submit($yinsheng_data); //-1系统错误 1-交易成功 2-失败 3-等待买家付款 4-客户主动关闭订单 5-交易正在处理中 6-部分退款成功 7-全部退款成功
                if ($return['status'] == 1) {
                    $data_insert['trade_no'] = $return['data']['trade_no'];
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '银盛支付订单未入库'
                        ]);
                    }
                    if ($ways_type == 14001) { //支付宝
                        $trade_no = json_decode($return['data']['jsapi_pay_info'], true);
                        return json_encode([
                            'status' => '1',
                            'trade_no' => $trade_no['tradeNO'],
                            'data' => $return['data'],
                        ]);
                    } elseif ($ways_type == 14002) { //微信
                        $trade_no = json_decode($return['data']['jsapi_pay_info'], true);
                        $weChatData = [
                            'timeStamp' => $trade_no['timeStamp'],
                            'package' => $trade_no['package'],
                            'paySign' => $trade_no['paySign'],
                            'signType' => $trade_no['signType'],
                            'nonceStr' => $trade_no['nonceStr'],
                            'appId' => $trade_no['appId'],
                        ];
                        $return['data']['ordNo'] = $out_trade_no;
                        return json_encode([
                            'status' => '1',
                            'trade_no' => $return['data']['trade_no'],
                            'data' => $return['data'],
                            'wx_data' => $weChatData
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'data' => $return['data'],
                        ]);
                    }
                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => $return['message']
                    ]);
                }

            }

            //通联支付 二维码
            if ($ways_type == '33001' || $ways_type == '33002' || $ways_type == '33003') {
                $config = new AllinPayConfigController();
                $allin_config = $config->allin_pay_config($data['config_id']);
                if (!$allin_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '通联支付配置不存在请检查配置'
                    ]);
                }

                $allin_merchant = $config->allin_pay_merchant($store_id, $store_pid);
                if (!$allin_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '通联支付商户号不存在'
                    ]);
                }

                if ($ways_type == '33001') {
                    $out_trade_no = 'aliqr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    if (strlen($out_trade_no) > 20) {
                        $out_trade_no = substr($out_trade_no, 0, 15) . substr(microtime(), 2, 5);
                    }
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'ALIPAY';
                    $data['subAppid'] = $allin_config->wx_appid ? $allin_config->wx_appid : 'wxa72b1b67413a8f6e';
                }

                if ($ways_type == '33002') {
                    $out_trade_no = 'wxqr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'WECHAT';
                    $data['subAppid'] = $allin_config->wx_appid ? $allin_config->wx_appid : 'wxa72b1b67413a8f6e';
                }
                if ($ways_type == '33003') {
                    $out_trade_no = 'unqr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '云闪付';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '云闪付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'UNIONPAY';
                    $data['subAppid'] = $allin_config->wx_appid ? $allin_config->wx_appid : 'wxa72b1b67413a8f6e';
                }
                $allin_store = Store::where('store_id', $store_id)->select('store_name')->first();
                $obj = new \App\Api\Controllers\AllinPay\PayController();
                $post_data = [
                    'orgId' => $allin_config->org_id,
                    'cusId' => $allin_merchant->cus_id,
                    'appid' => $allin_merchant->appid,
                    'out_trade_no' => $out_trade_no,
                    'pay_type' => $data['pay_type'],
                    'total_amount' => $total_amount,
                    'code' => $open_id,
                    'notify_url' => url('/api/allinPay/pay_notify_url'),
                    'wxAppid' => $allin_config->wx_appid,
                    'body' => $allin_store->store_name,
                ];
                $return = $obj->qr_submit($post_data);
                if ($return['status'] == '1') {
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }
                    if ($ways_type == 33001) { //支付宝

                        return json_encode([
                            'status' => '1',
                            'trade_no' => $return['data']['payinfo'],
                            'data' => $return['data'],
                        ]);
                    } elseif ($ways_type == 33002) { //微信

                        $weChatData = $return['data']['payinfo'];
                        if ($weChatData) {
                            if (isset($weChatData['timeStamp'])) $return['data']['jsapiTimestamp'] = $weChatData['jsapiTimestamp'];
                            if (isset($weChatData['package'])) $return['data']['jsapiPackage'] = $weChatData['jsapiPackage'];
                            if (isset($weChatData['paySign'])) $return['data']['jsapiPaySign'] = $weChatData['jsapiPaySign'];
                            if (isset($weChatData['signType'])) $return['data']['jsapiSignType'] = $weChatData['jsapiSignType'];
                            if (isset($weChatData['nonceStr'])) $return['data']['jsapiNoncestr'] = $weChatData['jsapiNoncestr'];
                            if (isset($weChatData['appId'])) $return['data']['jsapiAppid'] = $weChatData['jsapiAppid'];
                        }
                        $return['data']['ordNo'] = $out_trade_no;
                        return json_encode([
                            'status' => '1',
                            'trade_no' => $return['data']['trxid'],
                            'data' => $return['data'],
                            'wx_data' => $weChatData
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'data' => $return['data'],
                        ]);
                    }

                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => $return['message']
                    ]);
                }
            }

            //拉卡拉支付 二维码
            if ($ways_type == '34001' || $ways_type == '34002' || $ways_type == '34003') {
                $config = new LklConfigController();
                $lkl_config = $config->lkl_config($data['config_id']);
                if (!$lkl_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '拉卡拉配置不存在请检查配置'
                    ]);
                }

                $lkl_merchant = $config->lkl_merchant($store_id, $store_pid);
                if (!$lkl_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '拉卡拉商户号不存在'
                    ]);
                }

                $out_trade_no = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));

                if ($ways_type == '34001') {
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'ALIPAY';
                }
                if ($ways_type == '34002') {
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'WECHAT';
                }
                if ($ways_type == '34003') {
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '云闪付';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '云闪付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'UNIONPAY';
                }

                $obj = new \App\Api\Controllers\LklPay\PayController();
                $lkl_data = [
                    'merchant_no' => $lkl_merchant->customer_no,
                    'term_no' => $lkl_merchant->term_no,
                    'out_trade_no' => $out_trade_no,
                    'pay_type' => $data['pay_type'],
                    'total_amount' => $total_amount,
                    'code' => $open_id,
                    'notify_url' => url('/api/lklpay/pay_notify_url'),
                ];
                $return = $obj->qr_submit($lkl_data);
                if ($return['status'] == '1') {
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }
                    if ($ways_type == 34001) { //支付宝

                        return json_encode([
                            'status' => '1',
                            'trade_no' => $return['data']['acc_resp_fields']['prepay_id'],//支付请求后拉起支付宝支付订单号
                            'data' => $return['data'],
                        ]);
                    } elseif ($ways_type == 34002) { //微信
                        $weChatData = [
                            'timeStamp' => $return['data']['acc_resp_fields']['time_stamp'],
                            'package' => $return['data']['acc_resp_fields']['package'],
                            'paySign' => $return['data']['acc_resp_fields']['pay_sign'],
                            'signType' => $return['data']['acc_resp_fields']['sign_type'],
                            'nonceStr' => $return['data']['acc_resp_fields']['nonce_str'],
                            'appId' => $return['data']['acc_resp_fields']['app_id']
                        ];

                        $return['data']['ordNo'] = $out_trade_no;
                        return json_encode([
                            'status' => '1',
                            'trade_no' => $return['data']['trade_no'],
                            'data' => $return['data'],
                            'wx_data' => $weChatData
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'data' => $return['data']['acc_resp_fields'],
                        ]);
                    }

                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => $return['message']
                    ]);
                }
            }

            return json_encode([
                'status' => 2,
                'message' => '暂不支持此通道'
            ]);
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }


    //分期 提交 中转站 公共
    public function fq_qr_auth_pay_public($data, $ways)
    {
        try {
            $merchant_name = isset($data['merchant_name']) ? $data['merchant_name'] : "";
            $store_id = isset($data['store_id']) ? $data['store_id'] : "";
            $merchant_id = isset($data['merchant_id']) ? $data['merchant_id'] : "0";
            $config_id = isset($data['config_id']) ? $data['config_id'] : "";
            $store_name = isset($data['store_name']) ? $data['store_name'] : "";
            $store_pid = isset($data['store_pid']) ? $data['store_pid'] : "";
            $tg_user_id = isset($data['tg_user_id']) ? $data['tg_user_id'] : "";
            $total_amount = isset($data['total_amount']) ? $data['total_amount'] : "";
            $shop_price = isset($data['shop_price']) ? $data['shop_price'] : "";
            $remark = isset($data['remark']) ? $data['remark'] : "";
            $device_id = isset($data['device_id']) ? $data['device_id'] : "";
            $shop_name = isset($data['shop_name']) ? $data['shop_name'] : "";
            $shop_desc = isset($data['shop_desc']) ? $data['shop_desc'] : "";
            $open_id = isset($data['open_id']) ? $data['open_id'] : "";
            $ways_type = isset($data['ways_type']) ? $data['ways_type'] : "";
            $other_no = isset($data['other_no']) ? $data['other_no'] : "";
            $notify_url = isset($data['notify_url']) ? $data['notify_url'] : "";
            $coupon_type = isset($data['coupon_type']) ? $data['coupon_type'] : ""; //优惠类型 1 充值
            $pay_amount = isset($data['pay_amount']) ? $data['pay_amount'] : $total_amount; //支付金额
            $dk_jf = isset($data['dk_jf']) ? $data['dk_jf'] : '0'; //抵扣积分
            $dk_money = isset($data['dk_money']) ? $data['dk_money'] : '0'; //抵扣金额
            $hb_fq_num = isset($data['hb_fq_num']) ? $data['hb_fq_num'] : ''; //抵扣金额
            $hb_fq_seller_percent = isset($data['hb_fq_seller_percent']) ? $data['hb_fq_seller_percent'] : ''; //抵扣金额

            //发起请求
            $data = [
                'config_id' => $config_id,
                'store_id' => $store_id,
                'merchant_id' => $merchant_id,
                'total_amount' => $pay_amount,
                'shop_price' => $shop_price,
                'remark' => $remark,
                'device_id' => $device_id,
                'config_type' => '01',
                'shop_name' => $shop_name,
                'shop_desc' => $shop_desc,
                'store_name' => $store_name,
                'open_id' => $open_id,
                'is_fq' => 1,
                'is_fq_data' => [
                    'hb_fq_num' => $hb_fq_num,
                    'hb_fq_seller_percent' => $hb_fq_seller_percent
                ]
            ];

            $rate = $ways->rate;
            $trade_pay_rate = number_format($rate, 2, '.', ''); //当面付费率

            // }
            /*******************商户贴息模式**********************/
            $shop_name = '门店分期购:' . $hb_fq_num . '期' . $shop_name;
            $desc = '门店分期购:' . $hb_fq_num . '期' . $shop_name;

            //交易手续费
            $pay_sxf = ($trade_pay_rate * $shop_price) / 100;

            //分期手续费
            $hb_query_rate = [
                'store_id' => $store_id,
                'hb_fq_seller_percent' => $hb_fq_seller_percent,
                'shop_price' => $shop_price,
                'hb_fq_num' => $hb_fq_num
            ];

            $AlipayHbrate = AlipayHbrate::where('store_id', $store_id)->first();
            if (!$AlipayHbrate) {
                return json_encode([
                    'status' => 2,
                    'message' => '商户未设置分期手续费'
                ]);
            }

            $status = 'hb_fq_num_' . $hb_fq_num . '_status';
            if ($AlipayHbrate->$status == "00") {
                return json_encode([
                    'status' => 2,
                    'message' => '不支持' . $hb_fq_num . '期,请选择其他期数'
                ]);
            }

            if ($store_id == "") {
                $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)
                    ->orderBy('created_at', 'asc')
                    ->first();
                if ($MerchantStore) {
                    $store_id = $MerchantStore->store_id;
                }
            }

            $store = Store::where('store_id', $store_id)->first();
            $tg_user_id = $store->user_id;
            $store_name = $store->store_name;
            $store_pid = $store->pid;

            $obj = new AliFqPayController();
            $hb_query_rate = $obj->hb_query_rate($hb_query_rate, $AlipayHbrate);
            $xy_rate = $hb_query_rate['data']['xy_rate'];
            $hb_fq_sxf = $hb_query_rate['data']['hb_fq_sxf'];
            $hb_fq_sxf_z = $hb_query_rate['data']['hb_fq_sxf_z'];
            $data['total_amount'] = number_format($hb_query_rate['data']['pay_total_amount'], 2, '.', '');

            //商户实际净额
            $receipt_amount = $shop_price - $pay_sxf;

            //商户承担服务费
            if ($hb_fq_seller_percent == "100") {
                $receipt_amount = $receipt_amount - $hb_fq_sxf;
            }

            //入库参数
            $data_insert = [
                'user_id' => $tg_user_id,
                'store_id' => $store_id,
                'merchant_id' => $merchant_id,
                'store_name' => $store_name,
                'merchant_name' => $merchant_name,
                'buyer_user' => '暂无',
                'buyer_phone' => '暂无',
                'shop_name' => $shop_name,
                'shop_desc' => $desc,
                'total_amount' => $data['total_amount'],
                'shop_price' => $shop_price,
                'receipt_amount' => $receipt_amount,
                'pay_status' => 2,
                'pay_status_desc' => '等待支付',
                'hb_fq_num' => $hb_fq_num,
                'hb_fq_seller_percent' => $hb_fq_seller_percent,
                'hb_fq_sxf' => $hb_fq_sxf, //$this->hb_fq_sxf($hb_fq_num, $total_amount, $hb_fq_seller_percent),
                'xy_rate' => $xy_rate,
                'total_amount_out' => '0.00',
                'out_status' => 2,
                'config_id' => $config_id,
                'pay_sxf' => $pay_sxf,//支付手续费
            ];

            $out_trade_no = 'fq_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));

            /*官方支付宝*/
            if ($ways_type == '1000') {
                //配置
                $isvconfig = new AlipayIsvConfigController();
                $config_type = '01';
                $notify_url = url('/api/alipayopen/fq_pay_notify');

                $storeInfo = $isvconfig->alipay_auth_info($store_id, $store_pid);
                if (!$storeInfo) {
                    $msg = '支付宝授权信息不存在';
                    return json_encode([
                        'status' => 2,
                        'message' => $msg
                    ]);
                }
                $out_user_id = $storeInfo->alipay_user_id; //商户的id

                //分成模式 服务商
                if ($storeInfo->settlement_type == "set_a") {
                    if ($storeInfo->config_type == '02') {
                        $config_type = '02';
                    }
                    $storeInfo = AlipayAccount::where('config_id', $config_id)
                        ->where('config_type', $config_type)
                        ->first(); //服务商的
                }

                if (!$storeInfo) {
                    $msg = '支付宝授权信息不存在';
                    return json_encode([
                        'status' => 2,
                        'message' => $msg
                    ]);
                }

                $alipay_store_id = $storeInfo->alipay_store_id;
                $out_store_id = $storeInfo->out_store_id;

                $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);

                $pay_obj = new PayController();

                $data['out_trade_no'] = $out_trade_no;
                $data['alipay_store_id'] = $alipay_store_id;
                $data['out_store_id'] = $out_store_id;
                $data['out_user_id'] = $out_user_id;
                $data['app_auth_token'] = $storeInfo->app_auth_token;
                $data['config'] = $config;
                $data['notify_url'] = $notify_url;

                if ($ways->credit == "00") {
                    $disable_pay_channels = 'credit_group'; //禁用信用方式
                    $data['disable_pay_channels'] = $disable_pay_channels;
                }

                $return = $pay_obj->qr_auth_pay($data);
                $return_array = json_decode($return, true);
                if ($return_array['status'] == 1) {
                    $data['out_trade_no'] = $out_trade_no;
                    //入库参数
                    $data_insert['ways_type'] = 1007;
                    $data_insert['ways_type_desc'] = '二维码分期';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;

                    $insert_re = AlipayHbOrder::create($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    //转出入库
                    if ($hb_fq_sxf_z > 0) {
                        //结算入库
                        $updatedata = [
                            'trade_no' => '',
                            'config_id' => $config_id,
                            'order_settle_amount' => $hb_fq_sxf_z,
                            'store_id' => $store_id,
                            'user_id' => $tg_user_id,
                            'trans_out' => $config->alipay_pid,
                            'out_trade_no' => $out_trade_no,
                            'total_amount' => $total_amount,
                            'status' => '0',
                            'status_desc' => '等待支付',
                        ];
                        $this->insert_settle_order($updatedata);
                    }
                }

                return $return;
            }

            /*直付通支付宝*/
            if ($ways_type == '16002') {
                //配置
                $notify_url = url('/api/alipayopen/zft_fq_pay_notify');
                $config_type = '03';
                //配置
                $isvconfig = new AlipayIsvConfigController();
                $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);

                if (!$config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '直付通配置不存在'
                    ]);
                }
                //判断直付通
                $AlipayZftStore = $isvconfig->AlipayZftStore($store_id, $store_pid);
                if (!$AlipayZftStore) {
                    return json_encode([
                        'status' => 2,
                        'message' => '直付通门店不存在'
                    ]);
                }

                //禁用信用通道
                if ($ways->credit == "00") {
                    $disable_pay_channels = 'credit_group'; //禁用信用方式
                    $data['disable_pay_channels'] = $disable_pay_channels;
                }

                $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                $pay_obj = new PayController();

                $data['out_trade_no'] = $out_trade_no;
                $data['config'] = $config;
                $data['notify_url'] = $notify_url;
                $data['smid'] = $AlipayZftStore->smid;
                $data['alipay_account'] = $AlipayZftStore->alipay_account;
                $data['SettleModeType'] = $AlipayZftStore->SettleModeType;
                $data['card_alias_no'] = $AlipayZftStore->card_alias_no;

                $return = $pay_obj->zft_qr_auth_pay($data);
                $return_array = json_decode($return, true);
                if ($return_array['status'] == 1) {
                    $data['out_trade_no'] = $out_trade_no;
                    //入库参数
                    $data_insert['ways_type'] = 16001;
                    $data_insert['ways_type_desc'] = '二维码分期';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $insert_re = AlipayHbOrder::create($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    //转出入库
                    if ($hb_fq_sxf_z > 0) {
                        //结算入库
                        $updatedata = [
                            'trade_no' => '',
                            'user_id' => $tg_user_id,
                            'config_id' => $config_id,
                            'trans_out' => $config->alipay_pid,
                            'order_settle_amount' => $hb_fq_sxf_z,
                            'store_id' => $store_id,
                            'out_trade_no' => $out_trade_no,
                            'total_amount' => $total_amount,
                            'status' => '0',
                            'status_desc' => '等待支付',
                        ];
                        $this->insert_settle_order($updatedata);
                    }
                }

                return $return;
            }

            return json_encode([
                'status' => 2,
                'message' => '暂不支持此通道'
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getFile() . $exception->getLine()
            ]);
        }
    }


    //小程序 提交 中转站 公共
    public function mini_app_pay_public($data, $ways)
    {
        try {
            $merchant_name = isset($data['merchant_name']) ? $data['merchant_name'] : "";
            $store_id = isset($data['store_id']) ? $data['store_id'] : "";
            $merchant_id = isset($data['merchant_id']) ? $data['merchant_id'] : "0";
            $config_id = isset($data['config_id']) ? $data['config_id'] : "";
            $store_name = isset($data['store_name']) ? $data['store_name'] : "";
            $store_pid = isset($data['store_pid']) ? $data['store_pid'] : "";
            $tg_user_id = isset($data['tg_user_id']) ? $data['tg_user_id'] : "";
            $total_amount = isset($data['total_amount']) ? $data['total_amount'] : "";
            $shop_price = isset($data['shop_price']) ? $data['shop_price'] : "";
            $remark = isset($data['remark']) ? $data['remark'] : "";
            $device_id = isset($data['device_id']) ? $data['device_id'] : "";
            $shop_name = isset($data['shop_name']) ? $data['shop_name'] : "";
            $shop_desc = isset($data['shop_desc']) ? $data['shop_desc'] : "";
            $open_id = isset($data['open_id']) ? $data['open_id'] : "";
            $ways_type = isset($data['ways_type']) ? $data['ways_type'] : "";
            $other_no = isset($data['other_no']) ? $data['other_no'] : "";
            $notify_url = isset($data['notify_url']) ? $data['notify_url'] : "";
            $coupon_type = isset($data['coupon_type']) ? $data['coupon_type'] : ""; //优惠类型 1 充值
            $pay_amount = isset($data['pay_amount']) ? $data['pay_amount'] : $total_amount; //支付金额
            $dk_jf = isset($data['dk_jf']) ? $data['dk_jf'] : '0'; //抵扣积分
            $dk_money = isset($data['dk_money']) ? $data['dk_money'] : '0'; //抵扣金额

            //发起请求
            $data = [
                'config_id' => $config_id,
                'store_id' => $store_id,
                'merchant_id' => $merchant_id,
                'total_amount' => $pay_amount,
                'shop_price' => $shop_price,
                'remark' => $remark,
                'device_id' => $device_id,
                'config_type' => '01',
                'shop_name' => $shop_name,
                'shop_desc' => $shop_desc,
                'store_name' => $store_name,
                'open_id' => $open_id,
            ];

            //插入数据库
            $data_insert = [
                'trade_no' => '',
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'store_name' => $store_name,
                'buyer_id' => $open_id,
                'total_amount' => $total_amount,
                'shop_price' => $shop_price,
                'pay_amount' => $pay_amount,
                'payment_method' => '',
                'status' => '',
                'pay_status' => 2,
                'pay_status_desc' => '等待支付',
                'merchant_id' => $merchant_id,
                'merchant_name' => $merchant_name,
                'remark' => $remark,
                'device_id' => $device_id,
                'config_id' => $config_id,
                'company' => $ways->company,
                'other_no' => $other_no,
                'notify_url' => $notify_url,
                'coupon_type' => $coupon_type,
                'dk_jf' => $dk_jf,
                'dk_money' => $dk_money,
            ];

            //扫码费率入库
            $data_insert['rate'] = $ways->rate;
            $data_insert['fee_amount'] = $ways->rate * $pay_amount / 100;

            //随行付二维码
            if ($ways_type == '13001' || $ways_type == '13002') {
                $config = new VbillConfigController();
                $vbill_config = $config->vbill_config($data['config_id']);
                if (!$vbill_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付配置不存在请检查配置'
                    ]);
                }
                $vbill_merchant = $config->vbill_merchant($store_id, $store_pid);
                if (!$vbill_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付商户号不存在'
                    ]);
                }
                //支付宝
                if ($ways_type == '13001') {
                    $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'ALIPAY';
                }
                if ($ways_type == '13002') {
                    $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    //入库参数
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data['pay_type'] = 'WECHAT';
                    $data['subAppid'] = $vbill_merchant->wx_channel_appid ? $vbill_merchant->wx_channel_appid : 'wx3fc7983c75d88330';
                }

                $obj = new \App\Api\Controllers\Vbill\PayController();
                $data['notify_url'] = url('/api/vbill/pay_notify_url'); //回调地址
                $data['request_url'] = $obj->qr_pay_url; //请求地址;
                $data['mno'] = $vbill_merchant->mno;
                $data['privateKey'] = $vbill_config->privateKey; //
                $data['sxfpublic'] = $vbill_config->sxfpublic; //
                $data['orgId'] = $vbill_config->orgId; //
                $data['out_trade_no'] = $out_trade_no;
                $data['pay_way'] = '03'; //02 公众号/服务窗/js支付

                $return = $obj->qr_submit($data);
                if ($return['status'] == 1) {
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    return json_encode([
                        'status' => 1,
                        'data' => $return['data']['respData'],
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }
            }

            return json_encode([
                'status' => 2,
                'message' => '暂不支持此通道'
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getLine()
            ]);
        }
    }


    //学校教育缴费
    public function school_pay(Request $request)
    {
        $data = $request->all();
        $items_array = json_decode($data['items'], true); //item_serial_number

        $other_no = $request->get('out_trade_no'); //外部商户号
        $open_id = $request->get('open_id');
        $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));


        //小项编号
        $items_id = '';
        $total_amount = 0;
        foreach ($items_array as $k => $v) {
            $items_id = $items_id . '-' . $v['item_serial_number'];
            $total_amount = $total_amount + ($v['item_number'] * $v['item_price']);
        }

        $check_data = [
            'out_trade_no' => '订单号',
            'items' => '缴费项',
            'open_id' => '付款人id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            return json_encode([
                'status' => 2,
                'message' => $check
            ]);
        }

        $store_id = $request->get('store_id'); //'20181815585595187';
        $store = Store::where('store_id', $store_id)->first();
        if (!$store) {
            return json_encode([
                'status' => 2,
                'message' => '门店不存在'
            ]);
        }

        $config_id = $store->config_id; //2345
        $merchant_id = $store->merchant_id;
        $tg_user_id = $store->user_id;
        $shop_price = $total_amount;
        $remark = '';
        $device_id = 'qr';
        $shop_name = $request->get('shop_name', '教育缴费');
        $shop_desc = $request->get('shop_name', '教育缴费');
        $store_name = $store->store_name;
        $store_pid = $store->pid;
        $merchant_name = '';

        //发起请求
        $data = [
            'config_id' => $config_id,
            'store_id' => $store_id,
            'merchant_id' => $merchant_id,
            'total_amount' => $total_amount,
            'shop_price' => $shop_price,
            'remark' => $remark,
            'device_id' => $device_id,
            'config_type' => '01',
            'shop_name' => $shop_name,
            'shop_desc' => $shop_desc,
            'store_name' => $store_name,
            'open_id' => $open_id,
        ];
        //插入数据库
        $data_insert = [
            'trade_no' => '',
            'other_no' => $other_no,
            'store_id' => $store_id,
            'store_name' => $store_name,
            'buyer_id' => '',
            'total_amount' => $total_amount,
            'shop_price' => $shop_price,
            'payment_method' => '',
            'status' => '',
            'pay_status' => 2,
            'pay_status_desc' => '等待支付',
            'merchant_id' => $merchant_id,
            'merchant_name' => $merchant_name,
            'remark' => $remark,
            'device_id' => $device_id,
            'config_id' => $config_id,
            'user_id' => $tg_user_id,
            'rate' => '0.00',
        ];

        $data['goods_detail'] = [];
        $data['out_trade_no'] = $out_trade_no;
        $data['attach'] = $store_id . ',' . $config_id . ',' . $items_id; //附加信息原样返回

        $config = new WeixinConfigController();
        $options = $config->weixin_config($config_id);
        $weixin_store = $config->weixin_merchant($store_id, $store_pid);
        if (!$weixin_store) {
            return json_encode([
                'status' => 2,
                'message' => '微信商户号不存在'
            ]);
        }

        $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;
        $data['wx_sub_merchant_id'] = $wx_sub_merchant_id;
        $data['options'] = $options;
        $notify_url = url('api/weixin/school_pay_notify');

        //分账
        $weixin_store_obj = WeixinStore::where('store_id', $store_id)->first();
        if ($weixin_store_obj) {
            $data['is_profit_sharing'] = $weixin_store_obj->is_profit_sharing ?? ''; //是否分账(0-不分;1-分)
            $data['wx_sharing_rate'] = $weixin_store_obj->wx_sharing_rate ?? ''; //分账比例%
        }

        $pay_obj = new \App\Api\Controllers\Weixin\PayController();
        $return = $pay_obj->qr_pay($data, 'JSAPI', $notify_url);
        $return_array = json_decode($return, true);
        if ($return_array['status'] == 1) {
            $data_insert['ways_type'] = '2005';
            $data_insert['ways_type_desc'] = '微信支付';
            $data_insert['ways_source'] = 'weixin';
            $data_insert['ways_source_desc'] = '微信支付';
            $data_insert['out_trade_no'] = $out_trade_no;

            $insert_re = $this->insert_day_order($data_insert);
            if (!$insert_re) {
                return json_encode([
                    'status' => 2,
                    'message' => '订单未入库'
                ]);
            }
        }

        return $return;
    }


    //查询支付宝的订单状态
    public function AlipayTradePayQuery($out_trade_no, $app_auth_token, $configs)
    {
        $aop = new AopClient();
        $aop->rsaPrivateKey = $configs->rsa_private_key;
        $aop->appId = $configs->app_id;
        $aop->method = 'alipay.trade.query';

        $aop->signType = "RSA2"; //升级算法
        $aop->gatewayUrl = $configs->alipay_gateway;
        $aop->format = "json";
        $aop->charset = "GBK";
        $aop->version = "2.0";
        $requests = new AlipayTradeQueryRequest();
        $requests->setBizContent("{" .
            "    \"out_trade_no\":\"" . $out_trade_no . "\"" .
            "  }");
        $result = $aop->execute($requests, '', $app_auth_token);

        return $result;
    }


    //支付宝取消接口
    public function AlipayTradePayCancel($out_trade_no, $app_auth_token, $configs)
    {
        $aop = new AopClient();
        $aop->rsaPrivateKey = $configs->rsa_private_key;
        $aop->appId = $configs->app_id;
        $aop->method = 'alipay.trade.cancel';

        $aop->signType = "RSA2"; //升级算法
        $aop->gatewayUrl = $configs->alipay_gateway;
        $aop->format = "json";
        $aop->charset = "GBK";
        $aop->version = "2.0";
        $requests = new AlipayTradeCancelRequest();
        $requests->setBizContent("{" .
            "    \"out_trade_no\":\"" . $out_trade_no . "\"" .
            "  }");
        $result = $aop->execute($requests, '', $app_auth_token);
        return $result;
    }


    //随行付收银支付宝微信等扫一扫公共部分
    public function vbill_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $device_id = isset($data['device_id']) ? $data['device_id'] : '';
        $device_type = isset($data['device_type']) ? $data['device_type'] : '';
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $merchant_name = $data['merchant_name'] ?? '';
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $wechat_face = isset($data_insert['pay_method']) ? $data_insert['pay_method'] : ''; //刷脸支付方式weixin_face
        $rate = $data_insert['rate'];

        $config = new VbillConfigController();
        $vbill_config = $config->vbill_config($data['config_id']);
        if (!$vbill_config) {
            return json_encode([
                'status' => 2,
                'message' => '随行付配置不存在请检查配置'
            ]);
        }

        $vbill_merchant = $config->vbill_merchant($store_id, $store_pid);
        if (!$vbill_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '随行付商户号不存在'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\Vbill\PayController();
        $data['notify_url'] = url('/api/vbill/pay_notify_url'); //回调地址
        $data['request_url'] = $obj->scan_url; //请求地址;
        $data['mno'] = $vbill_merchant->mno;
        $data['privateKey'] = $vbill_config->privateKey; //
        $data['sxfpublic'] = $vbill_config->sxfpublic; //
        $data['orgId'] = $vbill_config->orgId; //
        $data['subAppid'] = $vbill_merchant->wx_channel_appid ? $vbill_merchant->wx_channel_appid : 'wx3fc7983c75d88330';

        $return = $obj->scan_pay($data);
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = $return['data']['respData']['sxfUuid'];
            $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['respData']['payTime']));
            $buyer_pay_amount = $return['data']['respData']['totalOffstAmt']; //消费者付款金额
            $discount_amount = isset($return['data']['respData']['pointAmount']) ? $return['data']['respData']['pointAmount'] : 0; //代金券金额,积分支付金额，优惠金额或折扣券的金额
            $receipt_amount = isset($return['data']['respData']['settleAmt']) ? $return['data']['respData']['settleAmt'] : $total_amount - $discount_amount; //商家入账金额,说明：包含手续费、预充值、平台补贴（优惠），不含免充值代金券（商家补贴）
            $recFeeAmt = isset($return['data']['respData']['recFeeAmt']) ? $return['data']['respData']['recFeeAmt'] : ''; //交易手续费；单位元
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
            $buyer_id = isset($return['data']['respData']['buyerId']) ? $return['data']['respData']['buyerId'] : "";
            $promotionDetail = isset($return['data']['respData']['promotionDetail']) ? $return['data']['respData']['promotionDetail'] : ''; //优惠信息（jsonArray格式字符串）
            $promotion_name = '';
            $promotion_amount = 0;
            if ($promotionDetail) {
                $promotion_detail_arr = json_decode($promotionDetail, true);
                if ($promotion_detail_arr && is_array($promotion_detail_arr)) {
//                    $promotion_name = isset($promotion_detail_arr[0]['name']) ? $promotion_detail_arr[0]['name'] : ''; //优惠名称
//                    $promotion_amount = isset($promotion_detail_arr[0]['amount']) ? $promotion_detail_arr[0]['amount']: 0.00; //优惠总额；单位元，保留两位小数
                    $promotion_name = '刷脸支付天天有优惠';
                    $promotion_amount = array_sum(array_column($promotion_detail_arr, 'amount'));
                }
            }

            $fee_amount = $recFeeAmt ? $recFeeAmt : round(($rate * $receipt_amount) / 100, 2);

            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $buyer_id,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount, //用户实际支付
                'receipt_amount' => $receipt_amount, //实收支付
                'discount_amount' => $promotion_amount, //第三方平台优惠金额
                'fee_amount' => $fee_amount
            ];
            if ($merchant_name) $update_data['merchant_name'] = $merchant_name;
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '13000',//返佣来源
                'source_desc' => '随行付',//返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'other_no' => $data_insert['other_no'],
                'rate' => $rate,
                'fee_amount' => $fee_amount,
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                    'buyer_pay_amount' => $buyer_pay_amount,
                    'discount_amount' => $promotion_amount, //第三方优惠
                    'promotion_name' => $promotion_name, //优惠名称
                    'promotion_amount' => $discount_amount, //商家优惠
                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 1,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }
    }


    //随行付A收银支付宝微信等扫一扫公共部分
    public function vbilla_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $device_id = isset($data['device_id']) ? $data['device_id'] : '';
        $device_type = isset($data['device_type']) ? $data['device_type'] : '';
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $merchant_name = $data['merchant_name'] ?? '';
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $wechat_face = isset($data_insert['pay_method']) ? $data_insert['pay_method'] : ''; //刷脸支付方式weixin_face
        $rate = $data_insert['rate'];

        $config = new VbillConfigController();
        $vbill_config = $config->vbilla_config($data['config_id']);
        if (!$vbill_config) {
            return json_encode([
                'status' => 2,
                'message' => '随行付a配置不存在请检查配置'
            ]);
        }

        $vbill_merchant = $config->vbilla_merchant($store_id, $store_pid);
        if (!$vbill_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '随行付a商户号不存在'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\Vbill\PayController();
        $data['notify_url'] = url('/api/vbill/pay_notify_a_url'); //回调地址
        $data['request_url'] = $obj->scan_url; //请求地址;
        $data['mno'] = $vbill_merchant->mno;
        $data['privateKey'] = $vbill_config->privateKey; //
        $data['sxfpublic'] = $vbill_config->sxfpublic; //
        $data['orgId'] = $vbill_config->orgId; //
        $data['subAppid'] = $vbill_merchant->wx_channel_appid ? $vbill_merchant->wx_channel_appid : 'wx3fc7983c75d88330';

        $return = $obj->scan_pay($data);

        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
//            Log::info('随行付A-被扫-成功-返回');
//            Log::info($return['data']['respData']);
            $trade_no = $return['data']['respData']['sxfUuid'];
            $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['respData']['payTime']));
            $buyer_pay_amount = $return['data']['respData']['totalOffstAmt']; //消费者付款金额
            $discount_amount = isset($return['data']['respData']['pointAmount']) ? $return['data']['respData']['pointAmount'] : 0; //代金券金额,积分支付金额，优惠金额或折扣券的金额
            $receipt_amount = isset($return['data']['respData']['settleAmt']) ? $return['data']['respData']['settleAmt'] : $total_amount - $discount_amount; //商家入账金额,说明：包含手续费、预充值、平台补贴（优惠），不含免充值代金券（商家补贴）
            $recFeeAmt = isset($return['data']['respData']['recFeeAmt']) ? $return['data']['respData']['recFeeAmt'] : ''; //交易手续费；单位元
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
            $buyer_id = isset($return['data']['respData']['buyerId']) ? $return['data']['respData']['buyerId'] : "";
            $promotionDetail = isset($return['data']['respData']['promotionDetail']) ? $return['data']['respData']['promotionDetail'] : ''; //优惠信息（jsonArray格式字符串）
            $promotion_name = '';
            $promotion_amount = 0;
            if ($promotionDetail) {
                $promotion_detail_arr = json_decode($promotionDetail, true);
                if ($promotion_detail_arr && is_array($promotion_detail_arr)) {
//                    $promotion_name = isset($promotion_detail_arr[0]['name']) ? $promotion_detail_arr[0]['name'] : ''; //优惠名称
//                    $promotion_amount = isset($promotion_detail_arr[0]['amount']) ? $promotion_detail_arr[0]['amount']: 0.00; //优惠总额；单位元，保留两位小数
                    $promotion_name = '刷脸支付天天有优惠';
                    $promotion_amount = array_sum(array_column($promotion_detail_arr, 'amount'));
                }
            }

            $fee_amount = $recFeeAmt ? $recFeeAmt : round(($rate * $receipt_amount) / 100, 2);

            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $buyer_id,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount, //用户实际支付
                'receipt_amount' => $receipt_amount, //实收
                'discount_amount' => $promotion_amount, //第三方平台优惠金额
                'fee_amount' => $fee_amount
            ];
            if ($merchant_name) $update_data['merchant_name'] = $merchant_name;
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '19000',//返佣来源
                'source_desc' => '随行付A',//返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'other_no' => $data_insert['other_no'],
                'rate' => $rate,
                'fee_amount' => $fee_amount,
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount, //实际付款
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                    'buyer_pay_amount' => $buyer_pay_amount,
                    'discount_amount' => $promotion_amount, //第三方优惠
                    'promotion_name' => $promotion_name, //优惠名称
                    'promotion_amount' => $discount_amount //商家优惠
                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 1,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }
    }


    //联拓富收银支付宝微信等扫一扫公共部分
    public function ltf_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $config = new LtfConfigController();

        $ltf_merchant = $config->ltf_merchant($store_id, $store_pid);
        if (!$ltf_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '商户号不存在'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\Ltf\PayController();
        $data['notify_url'] = url('/api/ltf/pay_notify_url'); //回调地址
        $data['request_url'] = $obj->scan_url; //请求地址;
        $data['merchant_no'] = $ltf_merchant->merchantCode;
        $data['appId'] = $ltf_merchant->appId; //
        $data['key'] = $ltf_merchant->md_key; //

        $return = $obj->scan_pay($data);
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = $return['data']['outTransactionId'];
            $buyer_id = $return['data']['buyerId'];
            $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['payTime']));
            $buyer_pay_amount = $return['data']['receiptAmount'];
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $buyer_id,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount,//用户实际支付
                'receipt_amount' => $buyer_pay_amount //用户实际支付
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '10000',//返佣来源
                'source_desc' => '联拓富',//返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'other_no' => $data_insert['other_no'],
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",

            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],

                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 1,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }


    }


    //京东收银支付宝微信等扫一扫公共部分
    public function jd_pay_public($data_insert, $data)
    {

        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $config = new JdConfigController();
        $jd_config = $config->jd_config($data['config_id']);
        if (!$jd_config) {
            return json_encode([
                'status' => 2,
                'message' => '京东配置不存在请检查配置'
            ]);
        }

        $jd_merchant = $config->jd_merchant($store_id, $store_pid);
        if (!$jd_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '京东商户号不存在'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];


        $insert_re = $this->insert_day_order($data_insert);

        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\Jd\PayController();
        $data['notify_url'] = url('/api/jd/pay_notify_url'); //回调地址
        $data['request_url'] = $obj->scan_url; //请求地址;
        $data['merchant_no'] = $jd_merchant->merchant_no;
        $data['md_key'] = $jd_merchant->md_key; //
        $data['des_key'] = $jd_merchant->des_key; //
        $data['systemId'] = $jd_config->systemId; //
        $return = $obj->scan_pay($data);
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = $return['data']['tradeNo'];
            $channelNoSeq = isset($return['data']['channelNoSeq']) ? $return['data']['channelNoSeq'] : $trade_no; //条码
            $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['payFinishTime']));
            $buyer_pay_amount = $return['data']['piAmount'] / 100;
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $update_data = [
                'trade_no' => $channelNoSeq,
                'buyer_id' => '',
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount,//用户实际支付
                'receipt_amount' => $buyer_pay_amount //用户实际支付
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '6000',//返佣来源
                'source_desc' => '京东金融',//返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'other_no' => $data_insert['other_no'],
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : ""
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],

                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 1,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }


    }


    //快钱收银支付宝微信等扫一扫公共部分
    public function mybank_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $remark = $data['remark'];
        $code = $data['code'];
        $type_source = $data['pay_type'];
        $config = new MyBankConfigController();


        $mybank_merchant = $config->mybank_merchant($store_id, $store_pid);
        if (!$mybank_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '快钱商户号不存在'
            ]);
        }
        $wx_AppId = $mybank_merchant->wx_AppId;
        $MyBankConfig = $config->MyBankConfig($data['config_id'], $wx_AppId);
        if (!$MyBankConfig) {
            return json_encode([
                'status' => 2,
                'message' => '快钱配置不存在请检查配置'
            ]);
        }
        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);

        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }


        $data['type_source'] = $type_source; //alipay,weixin
        $data['out_trade_no'] = $out_trade_no;
        $data['mybank_merchant_id'] = $mybank_merchant->MerchantId;
        $data['merchant_id'] = $merchant_id;
        $data['code'] = $code;
        $data['TotalAmount'] = $total_amount * 100; //单位分
        $data['is_fq'] = 0; //0
        $data['fq_num'] = 3; //3
        $data['hb_fq_seller_percent'] = 0; //0
        $data['buydata'] = []; //数组
        $data['SettleType'] = 'T1'; //T1
        $data['remark'] = $remark;
        $data['body'] = $title;
        $data['store_id'] = $store_id;
        $data['attach'] = $remark; //附加信息，原样返回。
        $data['PayLimit'] = ""; //禁用方式
        $data['config_id'] = $config_id;

        $obj = new TradePayController();
        $return = $obj->TradePay($data);


        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = $return['data']['MerchantOrderNo']; //条码订单号
            $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['GmtPayment']));
            $payment_method = strtolower($return['data']['Credit']);
            $buyer_id = '';
            //微信付款的id
            if ($data['type_source'] == 'weixin') {
                $buyer_id = $return['data']['SubOpenId'];
            }
            if ($data['type_source'] == 'alipay') {
                $buyer_id = $return['data']['BuyerUserId'];
            }

            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $buyer_id,
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => $payment_method,
                'pay_time' => $pay_time,
            ];

            $this->update_day_order($update_data, $out_trade_no);


            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '3000',//返佣来源
                'source_desc' => '快钱支付',//返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'other_no' => $data['other_no'],
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",


            ];


            PaySuccessAction::action($data);


            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],

                ]
            ]);

        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 1,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }


    }


    //新大陆
    public function newland_pay_public($data_insert, $data_request)
    {
        try {
            $insert_re = $this->insert_day_order($data_insert);
            if (!$insert_re) {
                return json_encode([
                    'status' => 2,
                    'message' => '订单未入库'
                ]);
            }

            $obj = new \App\Api\Controllers\Newland\PayController();
            $return = $obj->scan_pay($data_request);

            if ($return['status'] == 0) {
                return json_encode([
                    'status' => 2,
                    'message' => $return['message']
                ]);
            }

            //返回支付成功
            if ($return['status'] == 1) {
                $trade_no = $return['data']['orderNo'];
                $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['sysTime']));
                $payment_method = '';
                $buyer_id = isset($return['data']['openId']) ? $return['data']['openId'] : "";

                $update_data = [
                    'trade_no' => $trade_no,
                    'buyer_id' => $buyer_id,
                    'status' => '1',
                    'pay_status_desc' => '支付成功',
                    'pay_status' => 1,
                    'payment_method' => $payment_method,
                    'pay_time' => $pay_time
                ];
                $this->update_day_order($update_data, $data_insert['out_trade_no']);

                //支付成功后的动作
                $data = [
                    'ways_type' => $data_insert['ways_type'],
                    'ways_type_desc' => $data_insert['ways_type_desc'],
                    'company' => $data_insert['company'],
                    'source_type' => '8000',//返佣来源
                    'source_desc' => '新大陆',//返佣来源说明
                    'total_amount' => $data_insert['total_amount'],
                    'out_trade_no' => $data_insert['out_trade_no'],
                    'other_no' => $data_insert['other_no'],
                    'rate' => $data_insert['rate'],
                    'fee_amount' => $data_insert['fee_amount'],
                    'merchant_id' => $data_insert['merchant_id'],
                    'store_id' => $data_insert['store_id'],
                    'user_id' => $data_insert['user_id'],
                    'config_id' => $data_insert['config_id'],
                    'store_name' => $data_insert['store_name'],
                    'ways_source' => $data_insert['ways_source'],
                    'pay_time' => $pay_time,
                    'device_id' => isset($data_insert['device_id']) ? $data_insert['device_id'] : "",
                ];
                PaySuccessAction::action($data);

                return json_encode([
                    'status' => 1,
                    'pay_status' => '1',
                    'message' => '支付成功',
                    'data' => [
                        'out_trade_no' => $data_insert['out_trade_no'],
                        'ways_type' => $data_insert['ways_type'],
                        'total_amount' => $data_insert['total_amount'],
                        'store_id' => $data_insert['store_id'],
                        'store_name' => $data_insert['store_name'],
                        'config_id' => $data_insert['config_id'],
                        'pay_time' => $pay_time,
                        'trade_no' => $trade_no,
                        'ways_source' => $data_insert['ways_source'],
                    ]
                ]);
            }

            //正在支付
            if ($return['status'] == 2) {
                return json_encode([
                    'status' => 1,
                    'pay_status' => '2',
                    'message' => '正在支付',
                    'data' => [
                        'out_trade_no' => $data_insert['out_trade_no'],
                        'ways_type' => $data_insert['ways_type'],
                        'ways_source' => $data_insert['ways_source'],
                        'total_amount' => $data_insert['total_amount'],
                        'store_id' => $data_insert['store_id'],
                        'store_name' => $data_insert['store_name'],
                        'config_id' => $data_insert['config_id'],
                    ]
                ]);
            }

            //支付失败
            if ($return['status'] == 3) {
                return json_encode([
                    'status' => 2,
                    'pay_status' => '3',
                    'message' => '支付失败',
                    'data' => [
                        'out_trade_no' => $data_insert['out_trade_no'],
                        'ways_type' => $data_insert['ways_type'],
                        'ways_source' => $data_insert['ways_source'],
                        'total_amount' => $data_insert['total_amount'],
                        'store_id' => $data_insert['store_id'],
                        'store_name' => $data_insert['store_name'],
                        'config_id' => $data_insert['config_id'],
                    ]
                ]);
            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => $exception->getMessage()
            ]);
        }
    }


    //和融通
    public function h_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];

        $config = new HConfigController();
        $h_config = $config->h_config($data['config_id']);

        if (!$h_config) {
            return json_encode([
                'status' => 2,
                'message' => '和融通配置不存在请检查配置'
            ]);
        }

        $h_merchant = $config->h_merchant($store_id, $store_pid);
        if (!$h_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '和融通商户号不存在'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);

        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\Huiyuanbao\PayController();
        $data['notify_url'] = url('/api/huiyuanbao/pay_notify'); //回调地址
        $data['request_url'] = $obj->scan_url; //请求地址;
        $data['mid'] = $h_merchant->h_mid;
        $data['md_key'] = $h_config->md_key; //
        $data['orgNo'] = $h_config->orgNo; //
        $return = $obj->scan_pay($data);
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = '112121' . $return['data']['transactionId'];
            $pay_time = date('Y-m-d H:i:s', time());
            $buyer_pay_amount = $total_amount;
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => '',
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount,//用户实际支付
                'receipt_amount' => $buyer_pay_amount //用户实际支付
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '9000',//返佣来源
                'source_desc' => '和融通',//返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'other_no' => $data_insert['other_no'],
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : ""
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 1,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

    }


    //富友
    public function fuiou_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $code = $data['code'];

        $config = new FuiouConfigController();
        $fuiou_config = $config->fuiou_config($data['config_id']);
        if (!$fuiou_config) {
            return json_encode([
                'status' => 2,
                'message' => '富友配置不存在请检查配置'
            ]);
        }

        $fuiou__merchant = $config->fuiou_merchant($store_id, $store_pid);
        if (!$fuiou__merchant) {
            return json_encode([
                'status' => 2,
                'message' => '富友商户号不存在'
            ]);
        }

        $out_trade_no = '1300' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\Fuiou\PayController();

        $order_type = '';
        if ($data['ways_source'] == "alipay") {
            $order_type = 'ALIPAY';
        }
        if ($data['ways_source'] == "weixin") {
            $order_type = 'WECHAT';
        }
        if ($data['ways_source'] == "unionpay") {
            $order_type = 'UNIONPAY';
        }

        $request = [
            'ins_cd' => $fuiou_config->ins_cd, //机构号
            'mchnt_cd' => $fuiou__merchant->mchnt_cd, //商户号
            'order_type' => $order_type, //订单类型订单类型:ALIPAY，WECHAT，UNIONPAY(银联二维码），BESTPAY(翼支付)
            'goods_des' => $store_id, //商品描述
            'mchnt_order_no' => $out_trade_no, //商户订单号
            'order_amt' => $total_amount * 100,//总金额 分
            'auth_code' => $code,//付款码,
            'pem' => $fuiou_config->my_private_key,
            'url' => $obj->scpay_url,
        ];
        $return = $obj->scan_pay($request);
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = $return['data']['transaction_id'];
            $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['reserved_txn_fin_ts']));
            $buyer_pay_amount = $total_amount;
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => '',
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount,//用户实际支付
                'receipt_amount' => $buyer_pay_amount //用户实际支付
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '11000',//返佣来源
                'source_desc' => '富友',//返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'other_no' => $data_insert['other_no'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",


            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 1,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }
    }


    //哆啦宝
    public function dlb_pay_pubulic($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $code = $data['code'];

        $manager = new ManageController();
        $dlb_config = $manager->pay_config($data['config_id']);
        if (!$dlb_config) {
            return json_encode([
                'status' => 2,
                'message' => '哆啦宝配置配置不存在请检查配置'
            ]);
        }

        $dlb_merchant = $manager->dlb_merchant($store_id, $store_pid);
        if (!$dlb_merchant && !empty($dlb_merchant->mch_num) && !empty($dlb_merchant->shop_num) && !empty($dlb_merchant->machine_num)) {
            return json_encode([
                'status' => 2,
                'message' => '哆啦宝配置商户未补充商户编号等信息!'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $request = [
            "accessKey" => $dlb_config->access_key,
            "secretKey" => $dlb_config->secret_key,
            "agentNum" => $dlb_config->agent_num,
            "customerNum" => $dlb_merchant->mch_num,
            "authCode" => $code,
            "machineNum" => $dlb_merchant->machine_num,
            "shopNum" => $dlb_merchant->shop_num,
            "requestNum" => $out_trade_no,
            "amount" => $total_amount,
            "source" => 'API',
            'callbackUrl' => url('api/dlb/pay_notify'),
            // "payType" => "NATIVE",
        ];
        $return = $manager->pay_scan($request);
        if ($return['status'] == 1) {
            $scanpay_data = $return['data'];
            //下单成功
            $timewait = !empty($dlb_config) && $dlb_config->device_timewait > 0 && $dlb_config->device_timewait < 10 ? $dlb_config->device_timewait : 3;
            sleep($timewait);
            $query_data = [
                "accessKey" => $dlb_config->access_key,
                "secretKey" => $dlb_config->secret_key,
                "agentNum" => $dlb_config->agent_num,
                "customerNum" => $dlb_merchant->mch_num,
                "shopNum" => $dlb_merchant->shop_num,
                "requestNum" => $out_trade_no
            ];
            $return = $manager->query_bill($query_data);

            if ($return['status'] == 1) {
                $query_result = $return['data'];
                if ($query_result['status'] == "SUCCESS") {
                    $pay_time = $query_result['completeTime'];
                    $trade_no = $query_result['orderNum'];
                    $buyer_pay_amount = $query_result['orderAmount'];
                    $buyer_id = isset($query_result['openId']) ? $query_result['openId'] : "";

                    $update_data = [
                        'trade_no' => $trade_no,
                        'buyer_id' => $buyer_id,
                        'buyer_logon_id' => '',
                        'status' => '1',
                        'pay_status_desc' => '支付成功',
                        'pay_status' => 1,
                        'payment_method' => '',
                        'pay_time' => $pay_time,
                        'buyer_pay_amount' => $buyer_pay_amount,//用户实际支付
                        'receipt_amount' => $buyer_pay_amount //用户实际支付
                    ];
                    $this->update_day_order($update_data, $out_trade_no);

                    //支付成功后的动作
                    $data = [
                        'ways_type' => $data['ways_type'],
                        'ways_type_desc' => $data['ways_type_desc'],
                        'source_type' => '15000',//返佣来源
                        'source_desc' => '哆啦宝',//返佣来源说明
                        'company' => 'dlb',
                        'total_amount' => $total_amount,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $data_insert['other_no'],
                        'rate' => $data_insert['rate'],
                        'merchant_id' => $merchant_id,
                        'store_id' => $store_id,
                        'user_id' => $tg_user_id,
                        'config_id' => $config_id,
                        'store_name' => $store_name,
                        'ways_source' => $data['ways_source'],
                        'pay_time' => $pay_time,
                        'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
                    ];
                    PaySuccessAction::action($data);

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'out_trade_no' => $out_trade_no,
                            'ways_type' => $ways_type,
                            'total_amount' => $total_amount,
                            'store_id' => $store_id,
                            'store_name' => $store_name,
                            'config_id' => $config_id,
                            'pay_time' => $pay_time,
                            'trade_no' => $trade_no,
                            'ways_source' => $data['ways_source'],
                        ]
                    ]);
                } elseif ($query_result['status'] == "INIT") {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '正在支付',
                        'data' => [
                            'out_trade_no' => $out_trade_no,
                            'ways_type' => $ways_type,
                            'ways_source' => $data['ways_source'],
                            'total_amount' => $total_amount,
                            'store_id' => $store_id,
                            'store_name' => $store_name,
                            'config_id' => $config_id,
                        ]
                    ]);
                } elseif ($query_result['status'] == "CANCEL") {
                    return json_encode([
                        'status' => 2,
                        'pay_status' => '3',
                        'message' => '支付失败',
                        'data' => [
                            'out_trade_no' => $out_trade_no,
                            'ways_type' => $ways_type,
                            'ways_source' => $data['ways_source'],
                            'total_amount' => $total_amount,
                            'store_id' => $store_id,
                            'store_name' => $store_name,
                            'config_id' => $config_id,
                        ]
                    ]);
                }
            } else {
                return json_encode($return);
            }
        } else {
            $return = [
                'status' => 2,
                'message' => $return['message']
            ];
            return json_encode($return);
        }
    }


    //传化
    public function tf_pay_pubulic($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $channel = $data['channel'];

        $config = new TfConfigController();

        $h_merchant = $config->tf_merchant($store_id, $store_pid);
        if (!$h_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '传化商户号不存在'
            ]);
        }

        $h_config = $config->tf_config($data['config_id'], $h_merchant->qd);
        if (!$h_config) {
            return json_encode([
                'status' => 2,
                'message' => '传化配置不存在请检查配置'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\Tfpay\PayController();
        $data['mch_id'] = $h_config->mch_id; //
        $data['pub_key'] = $h_config->pub_key; //
        $data['pri_key'] = $h_config->pri_key; //
        $data['sub_mch_id'] = $h_merchant->sub_mch_id; //
        $data['notify_url'] = url('/api/tfpay/notify_url');
        $return = $obj->scan_pay($data);
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = $return['data']['channel_no'];
            $user_info = $return['data']['user_info'];
            $pay_time = date('Y-m-d H:i:s', time());
            $buyer_pay_amount = $total_amount;
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $user_info,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount, //用户实际支付
                'receipt_amount' => $buyer_pay_amount //用户实际支付
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '12000',//返佣来源
                'source_desc' => 'TF',//返佣来源说明
                'total_amount' => $total_amount,
                'other_no' => $data_insert['other_no'],
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 1,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

    }


    //联动优势 被扫 公共
    public function linkage_pay_public($data_insert, $data)
    {
        $orderType = $data['orderType'];
        $goodsId = $data['goodsId'];
        $title = $data['title'];
        $ways_source_desc = $data['ways_source_desc'];
        $ways_type_desc = $data['ways_type_desc'];
        $code = $data['code'];
        $out_trade_no = $data['out_trade_no'];
        $other_no = $data['other_no'];
        $config_id = $data['config_id'];
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $ways_type = $data['ways_type'];
        $ways_source = $data['ways_source'];
        $company = $data['company'];
        $total_amount = $data['total_amount'];
        $remark = $data['remark'];
        $device_id = $data['device_id'];
        $shop_name = $data['shop_name'];
        $merchant_id = $data['merchant_id'];
        $store_name = $data['store_name'];
        $tg_user_id = $data['tg_user_id'];

        $config = new LinkageConfigController();
        $linkage_config = $config->linkage_config($data['config_id']);
        if (!$linkage_config) {
            return json_encode([
                'status' => '2',
                'message' => '联动配置不存在请检查配置'
            ]);
        }

        $linkage_merchant = $config->linkage_merchant($store_id, $store_pid);
        if (!$linkage_merchant) {
            return json_encode([
                'status' => '2',
                'message' => '联动商户号不存在'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $ways_type;
        $data_insert['ways_type_desc'] = $ways_type_desc;
        $data_insert['ways_source'] = $ways_source;
        $data_insert['company'] = $company;
        $data_insert['ways_source_desc'] = $ways_source_desc;

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => '2',
                'message' => '订单未入库'
            ]);
        }

//        $obj = new \App\Api\Controllers\Linkage\PayController();
        $obj = new LinkagePayController();
        $linkageData['acqSpId'] = $linkage_config->mch_id; //代理商编号
        $linkageData['acqMerId'] = $linkage_merchant->acqMerId; //商户号
        $linkageData['out_trade_no'] = $out_trade_no; //商户订单号
        $linkageData['total_amount'] = $total_amount; //订单金额
        $linkageData['ways_source'] = $ways_source; //订单类型
        $linkageData['authCode'] = $code; //付款码
        $linkageData['backUrl'] = url('/api/linkage/pay_notify_url'); //O,通知地址
        $linkageData['privateKey'] = $linkage_config->privateKey; //私钥
        $linkageData['publicKey'] = $linkage_config->publicKey; //公钥
        $return = $obj->scan_pay($linkageData); //-1 系统错误 0-其他 1-成功 2-验签失败 3-支付中 4-交易失败
        //支付成功
        if ($return['status'] == '1') {
//            $out_trade_no = $return['data']['orderNo'] ?? ''; //O,商户订单号
            $paySeq = $return['data']['paySeq'] ?? ''; //O,支付流水号（条形码），成功返回
            $trade_no = $return['data']['transactionId']; //联动优势的订单号
            $total_amount = isset($return['data']['txnAmt']) ? ($return['data']['txnAmt'] / 100) : 0; //订单金额
            $user_info = '';
            $pay_time = date('Y-m-d H:i:s', time());
            $buyer_pay_amount = $total_amount;
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $user_info,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status' => 1,
                'pay_status_desc' => '支付成功',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount, //用户实际支付
                'receipt_amount' => $buyer_pay_amount //用户实际支付
            ];
            if ($paySeq) $update_data['auth_code'] = $paySeq;
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $ways_type,
                'ways_type_desc' => $ways_type_desc,
                'company' => $data_insert['company'],
                'source_type' => '5000', //返佣来源
                'source_desc' => 'linkage', //返佣来源说明
                'total_amount' => $total_amount,
                'other_no' => $data_insert['other_no'],
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $ways_source,
                'pay_time' => $pay_time,
                'device_id' => $device_id ?? ''
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => '1',
                'pay_status' => 1,
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $ways_source
                ]
            ]);
        } //正在支付
        elseif ($return['status'] == '3') {
            return json_encode([
                'status' => '1',
                'pay_status' => 2,
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $ways_source,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id
                ]
            ]);
        } //支付失败
        elseif ($return['status'] == '4') {
            return json_encode([
                'status' => '2',
                'pay_status' => 3,
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $ways_source,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id
                ]
            ]);
        } //验签失败
        elseif ($return['status'] == '2') {
            return json_encode([
                'status' => '2',
                'message' => $return['message'] ?? '验签失败'
            ]);
        } //系统错误
        elseif ($return['status'] == '-1') {
            return json_encode([
                'status' => '2',
                'message' => $return['message'] ?? '系统错误'
            ]);
        } //其他
        else {
            return json_encode([
                'status' => '2',
                'message' => $return['message']
            ]);
        }

    }


    //工行
    public function lianfu_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $tg_user_id = $data['tg_user_id'];

        $config = new LianfuConfigController();
        $h_merchant = $config->lianfu_merchant($store_id, $store_pid);
        if (!$h_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '工行商户号不存在'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\lianfu\PayController();
        $data['apikey'] = $h_merchant->apikey; //
        $data['signkey'] = $h_merchant->signkey; //
        $return = $obj->scan_pay($data);
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = isset($return['data']['tp_order_id']) ? $return['data']['tp_order_id'] : $out_trade_no;
            $user_info = '';
            $pay_time = date('Y-m-d H:i:s', time());
            $buyer_pay_amount = $total_amount;
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $user_info,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount, //用户实际支付
                'receipt_amount' => $buyer_pay_amount //用户实际支付
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '20000', //返佣来源
                'source_desc' => 'lianfu', //返佣来源说明
                'total_amount' => $total_amount,
                'other_no' => $data_insert['other_no'],
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 1,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }
    }


    //汇付收银,支付宝、微信、银联--b_扫_c--公共部分
    public function hui_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $config = new HuiPayConfigController();
        $hui_pay_config = $config->hui_pay_config($data['config_id']);
        if (!$hui_pay_config) {
            return json_encode([
                'status' => 2,
                'message' => '汇付配置不存在请检查配置'
            ]);
        }

        $hui_pay_merchant = $config->hui_pay_merchant($store_id, $store_pid);
        if (!$hui_pay_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '汇付商户不存在'
            ]);
        }
        if (!$hui_pay_merchant->mer_id) {
            return json_encode([
                'status' => 2,
                'message' => '汇付商户号不存在'
            ]);
        }
        if (!$hui_pay_merchant->user_cust_id) {
            return json_encode([
                'status' => 2,
                'message' => '汇付用户客户号不存在'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $this->insert_day_order($data_insert);
        die;

        $obj = new \App\Api\Controllers\HuiPay\PayController();
//        $data['notify_url'] = url('/api/huipay/pay_notify_url'); //回调地址，交易成功后跳转地址
        $data['request_url'] = $obj->back_scan; //反扫请求地址
        $data['mer_cust_id'] = $hui_pay_config->mer_cust_id; //商户客户号
        $data['user_cust_id'] = $hui_pay_merchant->user_cust_id; //用户客户号
        $data['private_key'] = $hui_pay_config->private_key;
        $data['public_key'] = $hui_pay_config->public_key;
        $data['org_id'] = $hui_pay_config->org_id; //机构号,渠道来源

        $return = $obj->scan_pay($data); //1-成功 2-正在支付 3-失败

        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $return_res = json_decode($return, true);
            $trade_no = $return_res['order_id']; //订单号
            $pay_time = date('Y-m-d H:i:s', strtotime($return_res['trans_time']));
            $buyer_pay_amount = $return_res['trans_amt'];
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
            $buyer_id = $return_res['cust_id']; //客户号
//            Order::where('out_trade_no', $out_trade_no)->update(
            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $buyer_id,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount, //用户实际支付
                'receipt_amount' => $buyer_pay_amount //用户实际支付
            ];
//        );
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '18000',//返佣来源
                'source_desc' => '汇付',//返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'other_no' => $data_insert['other_no'],
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];

            PaySuccessAction::action($data);

            return json_encode([
                'status' => '1',
                'pay_status' => 1,
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],

                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => '1',
                'pay_status' => 2,
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => '2',
                'pay_status' => 3,
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }
    }


    //海科融通 被扫 公共部分
    public function hkrt_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $tg_user_id = $data['tg_user_id'];

        $config = new HkrtConfigController();
        $hkrt_config = $config->hkrt_config($data['config_id']);
        if (!$hkrt_config) {
            return json_encode([
                'status' => 2,
                'message' => '海科融通支付配置不存在，请检查'
            ]);
        }

        $hkrt_merchant = $config->hkrt_merchant($store_id, $store_pid);
        if (!$hkrt_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '海科融通通道未开通成功'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];
        $total_amount = $data['total_amount'];
        $total_amount = number_format($total_amount, 2, '.', '');

        $obj = new \App\Api\Controllers\Hkrt\PayController();

        $polymeric_passivepay_data = [
            'access_id' => $hkrt_config->access_id,
            'merch_no' => $hkrt_merchant->merch_no,
            'out_trade_no' => $data['out_trade_no'] ?? '',
            'total_amount' => $data['total_amount'] ?? '',
            'code' => $data['code'] ?? '',
            'notify_url' => url('/api/hkrt/pay_notify_url'), //支付回调地址
            'sn' => '', //O,厂商终端号
            'pn' => '', //O,SAAS终端号,标准服务商必填
            'remark' => $data['remark'],
            'limit_pay' => '0', //O,限制贷记卡支付,0-不限制贷记卡支付;1-禁止使用贷记卡支付;默认为0
            'receive_no' => '', //O,收账方(海科商户号)
            'amt' => '', //O,分账金额
            'goods_name' => '', //O,商品名称
            'access_key' => $hkrt_config->access_key
        ];
        Log::info('海科融通-被扫-公共部分');
        Log::info($polymeric_passivepay_data);
        $return = $obj->scan_pay($polymeric_passivepay_data); //0-系统错误 1-成功 2-失败 3-交易进行中
        Log::info($return);
        if ($return['status'] == '0') {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == '1') {
            $trade_no = $return['data']['trade_no'];
            $pay_time = $return['data']['trade_end_time'];

            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => '',
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => $return['data']['trade_type'],
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $total_amount, //用户实际支付
                'receipt_amount' => $total_amount //用户实际支付
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '22000', //返佣来源
                'source_desc' => '海科融通', //返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'other_no' => $data_insert['other_no'],
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => '1',
                'pay_status' => 1,
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == '2') {
            return json_encode([
                'status' => '2',
                'pay_status' => 3,
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }

        //正在支付
        if ($return['status'] == '3') {
            return json_encode([
                'status' => '1',
                'pay_status' => 2,
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }

    }


    //易生支付 被扫 公共
    public function easypay_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $tg_user_id = $data['tg_user_id'];
        $easyPayStoresImages = EasypayStoresImages::where('store_id', $store_id)->select('new_config_id')->first();
        if (!$easyPayStoresImages) {
            $easyPayStoresImages = EasypayStoresImages::where('store_id', $store_pid)->select('new_config_id')->first();
        }
        $config = new EasyPayConfigController();
        $easypay_config = $config->easypay_config($easyPayStoresImages->new_config_id);
        if (!$easypay_config) {
            return json_encode([
                'status' => '2',
                'message' => '易生支付支付配置不存在，请检查'
            ]);
        }

        $easypay_merchant = $config->easypay_merchant($store_id, $store_pid);
        if (!$easypay_merchant) {
            return json_encode([
                'status' => '2',
                'message' => '易生支付通道未开通成功'
            ]);
        }
        $store = Store::where('store_id', $store_id)->first();
        $location = '';
        if (!$store->lat || !$store->lng) {
            $storeController = new \App\Api\Controllers\User\StoreController();
            $address = $store->province_name . $store->city_name . $store->area_name . $store->store_address;//获取经纬度的地址
            $api_res = $storeController->query_address($address, env('LBS_KEY'));
            if ($api_res['status'] == '1') {
                $store_lng = $api_res['result']['lng'];
                $store_lat = $api_res['result']['lat'];
                $store->update([
                    'lng' => $store_lng,
                    'lat' => $store_lat,
                ]);
                $location = "+" . $store_lat . "/-" . $store_lng;
            } else {
//                            Log::info('添加门店-获取经纬度错误');
//                            Log::info($api_res['message']);
            }

        } else {
            $location = "+" . $store->lat . "/-" . $store->lng;
        }
        //获取到账标识
        $EasypayStoresImages = EasypayStoresImages::where('store_id', $store_id)
            ->select('real_time_entry')
            ->first();

        $patnerSettleFlag = '0'; //秒到
        if ($EasypayStoresImages) {
            if ($EasypayStoresImages->real_time_entry == 1) {
                $patnerSettleFlag = '0';
            } else {
                $patnerSettleFlag = '1';
            }
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => '2',
                'message' => '订单未入库'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];
        $total_amount = $data['total_amount'];
        $total_amount = number_format($total_amount, 2, '.', '');

        $obj = new \App\Api\Controllers\EasyPay\PayController();
        $easypay_data = [
            'channel_id' => $easypay_config->channel_id,
            'mno' => $easypay_merchant->term_mercode,
            'device_id' => $easypay_merchant->term_termcode,
            'out_trade_no' => $data['out_trade_no'],
            'total_amount' => $data['total_amount'],
            'code' => $data['code'],
            'shop_name' => $data['shop_name'],
            'location' => $location,
            'patnerSettleFlag' => $patnerSettleFlag,
            'store_name' => $store_name,
            'subAppid' => $easypay_config->wx_appid,
            'key' => $easypay_config->channel_key,
        ];
//        Log::info('易生支付-被扫-公共部分');
//        Log::info($easypay_data);
        $return = $obj->scan_pay($easypay_data); //-1 系统错误 0-其他 1-成功 2-待支付
//        Log::info($return);

        //支付成功
        if ($return['status'] == '1') {
            //1.0
//            $trade_no = $return['data']['wtorderid']; //系统订单号
//            $pay_time = (isset($return['data']['wxtimeend']) && !empty($return['data']['wxtimeend'])) ? date('Y-m-d H:i:m', strtotime($return['data']['wxtimeend'])) : ''; //支付完成时间，格式为 yyyyMMddhhmmss，如 2009年12月27日9点10分10秒表示为 20091227091010
//            $acctype = $return['data']['acctype'] ?? ''; //交易账户类型
//            $wxopenid = $return['data']['wxopenid'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
//            $clearamt = isset($return['data']['clearamt']) ? ($return['data']['clearamt'] / 100) : $total_amount; //结算金额，单位分
//            $payamt = isset($return['data']['payamt']) ? ($return['data']['payamt'] / 100) : $total_amount; //实付金额，单位分
            //2.0
            $trade_no = $return['data']['outTrace']; //系统订单号
            $pay_time = (isset($return['data']['timeEnd']) && !empty($return['data']['timeEnd'])) ? date('Y-m-d H:i:m', strtotime($return['data']['timeEnd'])) : date('Y-m-d H:i:m', time()); //支付完成时间，格式为 yyyyMMddhhmmss，如 2009年12月27日9点10分10秒表示为 20091227091010
            $acctype = ''; //交易账户类型
            $wxopenid = $return['data']['payerId'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
            $clearamt = isset($return['data']['settleAmt']) ? ($return['data']['settleAmt'] / 100) : $total_amount; //结算金额，单位分
            $payamt = isset($return['data']['payerAmt']) ? ($return['data']['payerAmt'] / 100) : $total_amount; //实付金额，单位分

            $payment_method = '';
            if ($acctype) {
                switch ($acctype) {
                    case 'C': //信用卡
                        $payment_method = 'credit';
                        break;
                    case 'D': //借记卡
                        $payment_method = 'debit';
                        break;
                    case 'E': //零钱
                        $payment_method = 'balance';
                        break;
                    default: //U其他
                        $payment_method = 'unknown';
                }
            }

            $fee_amount = ($data_insert['rate'] * $payamt) / 100;
            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $wxopenid,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status' => 1,
                'pay_status_desc' => '支付成功',
                'payment_method' => $payment_method, //credit:信用卡 pcredit:花呗（仅支付宝） debit:借记卡 balance:余额 unknown:未知
                'buyer_pay_amount' => $clearamt, //买家付款的金额
                'receipt_amount' => $payamt, //实付
                'pay_time' => $pay_time,
                'fee_amount' => $fee_amount
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '21000', //返佣来源
                'source_desc' => '易生支付', //返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'other_no' => $data_insert['other_no'],
                'rate' => $data_insert['rate'],
                'fee_amount' => $fee_amount,
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => $data['device_id'] ?? '',
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => 1,
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } //待支付
        elseif ($return['status'] == '2') {
            return json_encode([
                'status' => 1,
                'pay_status' => 2,
                'message' => '待支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } else {
            return json_encode([
                'status' => '3',
                'message' => $return['message']
            ]);
        }

    }

    //邮驿付 被扫  公共
    public function post_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $tg_user_id = $data['tg_user_id'];
        $phone = $data['phone'];
        $payChannel = $data['pay_type'];

        $config = new PostPayConfigController();
        $post_config = $config->post_pay_config($data['config_id']);
        if (!$post_config) {
            return json_encode([
                'status' => '2',
                'message' => '邮驿付支付支付配置不存在，请检查'
            ]);
        }

        $post_merchant = $config->post_pay_merchant($store_id, $store_pid);
        if (!$post_merchant) {
            return json_encode([
                'status' => '2',
                'message' => '邮驿付支付通道未开通成功'
            ]);
        }
        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => '2',
                'message' => '订单未入库'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];
        $total_amount = $data['total_amount'];
        $total_amount = number_format($total_amount, 2, '.', '');

        $obj = new \App\Api\Controllers\PostPay\PayController();
        $post_data = [
            'out_trade_no' => $data['out_trade_no'],
            'code' => $data['code'],
            'device_id' => $post_merchant->drive_no,
            'orgId' => $post_config->org_id,
            'phone' => $phone,
            'custId' => $post_merchant->cust_id,
            'payChannel' => $payChannel,
            'total_amount' => $data['total_amount'],
        ];

        $return = $obj->scan_pay($post_data); //-1 系统错误 0-其他 1-成功 2-待支付

        //支付成功
        if ($return['status'] == '1') {
            $trade_no = $return['data']['orderNo']; //系统订单号
            $pay_time = (isset($return['data']['orderTime']) && !empty($return['data']['orderTime'])) ? date('Y-m-d H:i:m', strtotime($return['data']['orderTime'])) : ''; //支付完成时间，格式为 yyyyMMddhhmmss，如 2009年12月27日9点10分10秒表示为 20091227091010
            $acctype = $return['data']['cardType']; //交易账户类型
            $wxopenid = $return['data']['openId'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
            $clearamt = isset($return['data']['txamt']) ? ($return['data']['txamt'] / 100) : $total_amount; //结算金额，单位分
            $payamt = isset($return['data']['netrAmt']) ? ($return['data']['netrAmt'] / 100) : $total_amount; //实付金额，单位分

            $payment_method = '';
            if ($acctype) {
                switch ($acctype) {
                    case '02': //信用卡
                        $payment_method = 'credit';
                        break;
                    case '01': //借记卡
                        $payment_method = 'debit';
                        break;
                    default: //U其他
                        $payment_method = 'unknown';
                }
            }

            $fee_amount = ($data_insert['rate'] * $payamt) / 100;
            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $wxopenid,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status' => 1,
                'pay_status_desc' => '支付成功',
                'payment_method' => $payment_method, //credit:信用卡 pcredit:花呗（仅支付宝） debit:借记卡 balance:余额 unknown:未知
                'buyer_pay_amount' => $clearamt, //买家付款的金额
                'receipt_amount' => $payamt, //实付
                'pay_time' => $pay_time,
                'fee_amount' => $fee_amount
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '29000', //返佣来源
                'source_desc' => '邮驿付支付', //返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'other_no' => $data_insert['other_no'],
                'rate' => $data_insert['rate'],
                'fee_amount' => $fee_amount,
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => $post_merchant->drive_no ?? '',
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => 1,
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } //待支付
        elseif ($return['status'] == '2') {
            return json_encode([
                'status' => 1,
                'pay_status' => 2,
                'message' => '待支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } else {
            return json_encode([
                'status' => '3',
                'message' => $return['message']
            ]);
        }


    }

    //建设银行 被扫  公共
    public function ccBank_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $tg_user_id = $data['tg_user_id'];


        $config = new CcBankPayConfigController();

        $ccBank_merchant = $config->ccBank_pay_merchant($store_id, $store_pid);
        if (!$ccBank_merchant) {
            return json_encode([
                'status' => '2',
                'message' => '建设银行支付通道未开通成功'
            ]);
        }
        $store = Store::where('store_id', $store_id)->select('zero_rate_type')->first();
        if ($store->zero_rate_type == '0') {//0收费  1不收费
            //查询商户是否充值
            $MerchantConsumerDetails = MerchantConsumerDetails::where('merchant_id', $merchant_id)
                ->where('type', '1')
                ->first();
            if (!$MerchantConsumerDetails) {
                return json_encode([
                    'status' => '2',
                    'message' => '该商户没有充值，请商户及时充值'
                ]);
            }
            $MerchantConsumerDetails = MerchantConsumerDetails::where('merchant_id', $merchant_id)
                ->whereIn('type', [1, 2])
                ->orderBy('created_at', 'desc')
                ->first();
            if ($MerchantConsumerDetails->avail_amount < 0) {
                return json_encode([
                    'status' => '2',
                    'message' => '该商户可用余额已为负值，请商户及时充值'
                ]);
            }
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => '2',
                'message' => '订单未入库'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];
        $total_amount = $data['total_amount'];
        $total_amount = number_format($total_amount, 2, '.', '');

        $obj = new \App\Api\Controllers\CcBankPay\PayController();
        $ccBank_data = [
            'cust_id' => $ccBank_merchant->cust_id,
            'pos_id' => $ccBank_merchant->pos_id,
            'branch_id' => $ccBank_merchant->branch_id,
            'out_trade_no' => $out_trade_no,
            'code' => $data['code'],
            'total_amount' => $data['total_amount'],
            'public_key' => $ccBank_merchant->public_key,
            'termno1' => $ccBank_merchant->termno1,
            'termno2' => $ccBank_merchant->termno2,
        ];

        $return = $obj->scan_pay($ccBank_data); //-1 系统错误 0-其他 1-成功 2-待支付

        //支付成功
        if ($return['status'] == '1') {
            $trade_no = $return['data']['TRACEID']; //系统订单号
            $pay_time = date('Y-m-d H:i:m', time());
            $acctype = $return['data']['PAYMENT_DETAILS']['DEBIT_CREDIT_TYPE'] ?? ''; //交易账户类型
            $wxopenid = $return['data']['OPENID'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
            $clearamt = isset($return['data']['AMOUNT']) ? ($return['data']['AMOUNT']) : $total_amount; //结算金额
            $payamt = isset($return['data']['AMOUNT']) ? ($return['data']['AMOUNT']) : $total_amount; //实付金额

            $payment_method = '';
            if ($acctype) {
                switch ($acctype) {
                    case '02': //信用卡
                        $payment_method = 'CREDIT';
                        break;
                    case '01': //借记卡
                        $payment_method = 'DEBIT';
                        break;
                    default: //U其他
                        $payment_method = 'unknown';
                }
            }

            $fee_amount = ($data_insert['rate'] * $payamt) / 100;
            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $wxopenid,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status' => 1,
                'pay_status_desc' => '支付成功',
                'payment_method' => $payment_method, //credit:信用卡 pcredit:花呗（仅支付宝） debit:借记卡 balance:余额 unknown:未知
                'buyer_pay_amount' => $clearamt, //买家付款的金额
                'receipt_amount' => $payamt, //实付
                'pay_time' => $pay_time,
                'fee_amount' => $fee_amount
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '31000', //返佣来源
                'source_desc' => '建设银行', //返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'other_no' => $data_insert['other_no'],
                'rate' => $data_insert['rate'],
                'fee_amount' => $fee_amount,
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => $post_merchant->drive_no ?? '',
                'no_user_money' => '1',//不计算返佣
            ];
            PaySuccessAction::action($data);

            if ($store->zero_rate_type == '0') {//0收费  1不收费
                $TransactionDeductionController = new TransactionDeductionController();
                $deduction = [
                    'merchant_id' => $merchant_id,
                    'total_amount' => $total_amount,
                    'user_id' => $tg_user_id,
                    'order_id' => $out_trade_no,
                    'company' => $data_insert['company'],
                    'rate' => $data_insert['rate'] //结算费率
                ];
                $TransactionDeductionController->deduction($deduction);//交易扣款
            }

            return json_encode([
                'status' => 1,
                'pay_status' => 1,
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } //待支付
        elseif ($return['status'] == '2') {
            $trade_no = $return['data']['TRACEID']; //系统订单号
            $pay_time = date('Y-m-d H:i:m', time());
            $wxopenid = $return['data']['OPENID'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
            $clearamt = isset($return['data']['AMOUNT']) ? ($return['data']['AMOUNT']) : $total_amount; //结算金额
            $payamt = isset($return['data']['AMOUNT']) ? ($return['data']['AMOUNT']) : $total_amount; //实付金额
            $fee_amount = ($data_insert['rate'] * $payamt) / 100;
            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $wxopenid,
                //'payment_method' => $payment_method, //credit:信用卡 pcredit:花呗（仅支付宝） debit:借记卡 balance:余额 unknown:未知
                'buyer_pay_amount' => $clearamt, //买家付款的金额
                'receipt_amount' => $payamt, //实付
                'pay_time' => $pay_time,
                'fee_amount' => $fee_amount
            ];
            $this->update_day_order($update_data, $out_trade_no);
            return json_encode([
                'status' => 1,
                'pay_status' => 2,
                'message' => '待支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                    'trade_no' => $trade_no
                ]
            ]);
        } else {
            return json_encode([
                'status' => '3',
                'message' => $return['message']
            ]);
        }


    }

    //易生数科支付 被扫 公共
    public function easysk_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $tg_user_id = $data['tg_user_id'];

        $config = new EasySkPayConfigController();
        $easyskpay_config = $config->easyskpay_config($data['config_id']);
        if (!$easyskpay_config) {
            return json_encode([
                'status' => '2',
                'message' => '易生数科支付支付配置不存在，请检查'
            ]);
        }

        $easyskpay_merchant = $config->easyskpay_merchant($store_id, $store_pid);
        if (!$easyskpay_merchant) {
            return json_encode([
                'status' => '2',
                'message' => '易生数科支付通道未开通成功'
            ]);
        }
        $store = Store::where('store_id', $store_id)->first();
        $location = '';
        if (!$store->lat || !$store->lng) {
            $storeController = new \App\Api\Controllers\User\StoreController();
            $address = $store->province_name . $store->city_name . $store->area_name . $store->store_address;//获取经纬度的地址
            $api_res = $storeController->query_address($address, env('LBS_KEY'));
            if ($api_res['status'] == '1') {
                $store_lng = $api_res['result']['lng'];
                $store_lat = $api_res['result']['lat'];
                $store->update([
                    'lng' => $store_lng,
                    'lat' => $store_lat,
                ]);
                $location = "+" . $store_lat . "/-" . $store_lng;
            } else {
//                            Log::info('添加门店-获取经纬度错误');
//                            Log::info($api_res['message']);
            }

        } else {
            $location = "+" . $store->lat . "/-" . $store->lng;
        }
        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => '2',
                'message' => '订单未入库'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];
        $total_amount = $data['total_amount'];
        $total_amount = number_format($total_amount, 2, '.', '');

        $obj = new \App\Api\Controllers\EasySkPay\PayController();
        $easypay_data = [
            'org_id' => $easyskpay_config->org_id,
            'mer_id' => $easyskpay_merchant->mer_id,
            'request_no' => $data['out_trade_no'],
            'amount' => $data['total_amount'],
            'authCode' => $data['code'],
            'pay_type' => $data['pay_type'],
            'location' => $location
        ];
//        Log::info('易生数科支付-被扫-公共部分');
//        Log::info($easypay_data);
        $return = $obj->codePayByBtoC($easypay_data); //-1 系统错误 0-其他 1-成功 2-待支付
//        Log::info($return);

        //支付成功
        if ($return['status'] == '1') {

            $trade_no = $return['data']['tradeNo']; //系统订单号
            $pay_time = (isset($return['data']['bizData']['payTime']) && !empty($return['data']['bizData']['payTime'])) ? date('Y-m-d H:i:m', strtotime($return['data']['bizData']['payTime'])) : '';
            $acctype = $return['data']['bizData']['payAccType']; //交易账户类型
            $wxopenid = $return['data']['bizData']['channelOpenId'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
            $clearamt = isset($return['data']['bizData']['stlmAmt']) ? ($return['data']['bizData']['stlmAmt'] / 100) : $total_amount; //结算金额，单位分
            $payamt = isset($return['data']['bizData']['amount']) ? ($return['data']['bizData']['amount'] / 100) : $total_amount; //实付金额，单位分

            $payment_method = '';
            if ($acctype) {
                switch ($acctype) {
                    case '00': //信用卡
                        $payment_method = 'credit';
                        break;
                    case '01': //借记卡
                        $payment_method = 'debit';
                        break;
                    case '06': //零钱
                        $payment_method = 'balance';
                        break;
                    default: //U其他
                        $payment_method = 'unknown';
                }
            }

            $fee_amount = ($data_insert['rate'] * $payamt) / 100;
            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $wxopenid,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status' => 1,
                'pay_status_desc' => '支付成功',
                'payment_method' => $payment_method, //credit:信用卡 pcredit:花呗（仅支付宝） debit:借记卡 balance:余额 unknown:未知
                'buyer_pay_amount' => $clearamt, //买家付款的金额
                'receipt_amount' => $payamt, //实付
                'pay_time' => $pay_time,
                'fee_amount' => $fee_amount
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '32000', //返佣来源
                'source_desc' => '易生数科支付', //返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'other_no' => $data_insert['other_no'],
                'rate' => $data_insert['rate'],
                'fee_amount' => $fee_amount,
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => $data['device_id'] ?? '',
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => 1,
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } //待支付
        elseif ($return['status'] == '2') {
            return json_encode([
                'status' => 1,
                'pay_status' => 2,
                'message' => '待支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } else {
            return json_encode([
                'status' => '3',
                'message' => $return['message']
            ]);
        }

    }

    //通联 被扫  公共
    public function allin_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $tg_user_id = $data['tg_user_id'];
        $config = new AllinPayConfigController();
        $allin_config = $config->allin_pay_config($data['config_id']);
        if (!$allin_config) {
            return json_encode([
                'status' => '2',
                'message' => '通联支付支付配置不存在，请检查'
            ]);
        }

        $allin_merchant = $config->allin_pay_merchant($store_id, $store_pid);
        if (!$allin_merchant) {
            return json_encode([
                'status' => '2',
                'message' => '通联支付通道未开通成功'
            ]);
        }
        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => '2',
                'message' => '订单未入库'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];
        $total_amount = $data['total_amount'];
        $total_amount = number_format($total_amount, 2, '.', '');

        $obj = new \App\Api\Controllers\AllinPay\PayController();
        $allin_data = [
            'out_trade_no' => $data['out_trade_no'],
            'total_amount' => $data['total_amount'],
            'code' => $data['code'],
            'orgId' => $allin_config->org_id,
            'cusId' => $allin_merchant->cus_id,
            'body' => $data['store_name'],
            'appid' => $allin_merchant->appid,
            'sub_appid' => $allin_config->wx_appid,
            'termno' => $allin_merchant->termno,
            'notify_url' => url('/api/allinPay/pay_notify_url'),

        ];

        $return = $obj->scan_pay($allin_data); //-1 系统错误 0-其他 1-成功 2-待支付

        //支付成功
        if ($return['status'] == '1') {
            $trade_no = $return['data']['trxid']; //系统订单号
            $pay_time = date('Y-m-d H:i:m', time()); //支付完成时间，格式为 yyyyMMddhhmmss
            $openid = $return['data']['acct'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
            $clearamt = isset($return['data']['trxamt']) ? ($return['data']['trxamt'] / 100) : $total_amount; //结算金额，单位分
            $payamt = $total_amount; //实付金额，单位分
            $payment_method = '';
            $fee_amount = ($data_insert['rate'] * $payamt) / 100;
            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $openid,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status' => 1,
                'pay_status_desc' => '支付成功',
                'payment_method' => $payment_method, //credit:信用卡 pcredit:花呗（仅支付宝） debit:借记卡 balance:余额 unknown:未知
                'buyer_pay_amount' => $clearamt, //买家付款的金额
                'receipt_amount' => $payamt, //实付
                'pay_time' => $pay_time,
                'fee_amount' => $fee_amount
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '33000', //返佣来源
                'source_desc' => '通联支付', //返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'other_no' => $data_insert['other_no'],
                'rate' => $data_insert['rate'],
                'fee_amount' => $fee_amount,
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => $post_merchant->drive_no ?? '',
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => 1,
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } //待支付
        elseif ($return['status'] == '2') {
            return json_encode([
                'status' => 1,
                'pay_status' => 2,
                'message' => '待支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } else {
            return json_encode([
                'status' => '3',
                'message' => $return['message']
            ]);
        }
    }

    //邮政
    public function lianfuyoupay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $tg_user_id = $data['tg_user_id'];

        $config = new LianfuyoupayConfigController();
        $h_merchant = $config->lianfu_merchant($store_id, $store_pid);
        if (!$h_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '商户号不存在'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\lianfuyouzheng\PayController();
        $data['apikey'] = $h_merchant->apikey; //
        $data['signkey'] = $h_merchant->signkey; //
        $data['pos_sn'] = $h_merchant->pos_sn; //
        $data['goods_name'] = '交易'; //
        $return = $obj->scan_pay($data);
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = isset($return['data']['tp_order_id']) ? $return['data']['tp_order_id'] : $out_trade_no;
            $user_info = '';
            $pay_time = date('Y-m-d H:i:s', time());
            $buyer_pay_amount = $total_amount;
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $user_info,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount, //用户实际支付
                'receipt_amount' => $buyer_pay_amount //用户实际支付
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '26000', //返佣来源
                'source_desc' => 'lianfuyoupay', //返佣来源说明
                'total_amount' => $total_amount,
                'other_no' => $data_insert['other_no'],
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 1,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }
    }


    //苏州银行
    public function sz_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];

        $config = new SuzhouConfigController();

        $h_merchant = $config->sz_merchant($store_id, $store_pid);
        if (!$h_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '苏州银行商户号不存在'
            ]);
        }

        $h_config = $config->sz_config($data['config_id']);
        if (!$h_config) {
            return json_encode([
                'status' => 2,
                'message' => '苏州银行支付配置不存在请检查配置'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\Suzhou\PayController();
        $data['certId'] = $h_config->certId; //
        $data['pri_key'] = $h_config->rsa_pr; //
        $data['merchantId'] = $h_merchant->MerchantId; //
        $data['notify_url'] = url('/api/suzhou/notify_url');
        $return = $obj->scan_pay($data);

        Log::info('苏州银行');
        Log::info($return);
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = $return['data']['tradeNo'];
            $user_info = '';
            $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['orderTime']));
            $buyer_pay_amount = $total_amount;
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $user_info,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount, //用户实际支付
                'receipt_amount' => $buyer_pay_amount //用户实际支付
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '17000',//返佣来源
                'source_desc' => 'SZ',//返佣来源说明
                'total_amount' => $total_amount,
                'other_no' => $data_insert['other_no'],
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 1,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

    }


    //葫芦天下
    public function hltx_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $code = $data['code'];
        $manager = new \App\Api\Controllers\Hltx\ManageController();

        $hltx_merchant = $manager->pay_merchant($store_id, $store_pid);
        $qd = $hltx_merchant->qd;
        $hltx_config = $manager->pay_config($config_id, $qd);
        if (!$hltx_config) {
            return json_encode([
                'status' => 2,
                'message' => 'HL支付配置不存在请检查配置'
            ]);
        }

        $manager->init($hltx_config);
        switch ($ways_type) {
            case 23001:
                $pay_channel = 'ALI';
                break;
            case 23002:
                $pay_channel = 'WX';
                break;
            case 23004:
                $pay_channel = 'UPAY';
                break;
            default:
                $pay_channel = 'ALI';
                break;
        }
        if (!$hltx_merchant || empty($hltx_merchant->mer_no)) {
            return json_encode([
                'status' => 2,
                'message' => '商户未成功开通HL通道!'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $hltx_data = [
            'merNo' => $hltx_merchant->mer_no,
            'orderNo' => $out_trade_no,
            'tradeType' => $pay_channel,
            'amount' => $total_amount * 100, //转换为分
            'paymentCode' => $code,
            'orderInfo' => '商户刷卡收款',
            'deviceNo' => (empty($data['device_id']) ? '商户收款' : $data['device_id']),
            'deviceIp' => \EasyWeChat\Kernel\Support\get_client_ip(),
        ];
        \App\Common\Log::write('刷卡交易:', 'log_return.txt');
        $return = $manager->scan_pay($hltx_data);
        if ($return['status'] != 1) {
            $return_data = [
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败' . $return['message'],
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ];
        } else {
            $return = $return['data'];
            if ($return['tradeStatus'] == 'E') {
                $return_data = [
                    'status' => 2,
                    'pay_status' => '3',
                    'message' => '支付失败' . $return['message'],
                    'data' => [
                        'out_trade_no' => $out_trade_no,
                        'ways_type' => $ways_type,
                        'ways_source' => $data['ways_source'],
                        'total_amount' => $total_amount,
                        'store_id' => $store_id,
                        'store_name' => $store_name,
                        'config_id' => $config_id,
                    ]
                ];
            } elseif ($return['tradeStatus'] == 'S') {
                //成功
                if ($return['subTradeStatus'] == 'COMPLETE') {
                    $pay_time = $return['payCompleteTime'];
                    $trade_no = $return['transIndex'];
                    $buyer_pay_amount = $return['amount'] / 100;

                    $update_data = [
                        'trade_no' => $trade_no,
                        'buyer_id' => '',
                        'buyer_logon_id' => '',
                        'status' => '1',
                        'pay_status_desc' => '支付成功',
                        'pay_status' => 1,
                        'payment_method' => '',
                        'pay_time' => $pay_time,
                        'buyer_pay_amount' => $buyer_pay_amount, //用户实际支付
                        'receipt_amount' => $buyer_pay_amount //用户实际支付
                    ];
                    $this->update_day_order($update_data, $out_trade_no);

                    //支付成功后的动作
                    $data = [
                        'ways_type' => $data['ways_type'],
                        'ways_type_desc' => $data['ways_type_desc'],
                        'source_type' => '23000', //返佣来源
                        'source_desc' => 'HL', //返佣来源说明
                        'company' => 'hltx',
                        'total_amount' => $total_amount,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $data_insert['other_no'],
                        'rate' => $data_insert['rate'],
                        'merchant_id' => $merchant_id,
                        'store_id' => $store_id,
                        'user_id' => $tg_user_id,
                        'config_id' => $config_id,
                        'store_name' => $store_name,
                        'ways_source' => $data['ways_source'],
                        'pay_time' => $pay_time,
                        'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
                    ];
                    PaySuccessAction::action($data);

                    $return_data = [
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'out_trade_no' => $out_trade_no,
                            'ways_type' => $ways_type,
                            'total_amount' => $total_amount,
                            'store_id' => $store_id,
                            'store_name' => $store_name,
                            'config_id' => $config_id,
                            'pay_time' => $pay_time,
                            'trade_no' => $trade_no,
                            'ways_source' => $data['ways_source'],
                        ]
                    ];
                } elseif (in_array($return['subTradeStatus'], ['FAILED', 'SYS_FAILED', 'CANCEL', 'CLOSE'])) {
                    $return_data = [
                        'status' => 2,
                        'pay_status' => '3',
                        'message' => '支付失败',
                        'data' => [
                            'out_trade_no' => $out_trade_no,
                            'ways_type' => $ways_type,
                            'ways_source' => $data['ways_source'],
                            'total_amount' => $total_amount,
                            'store_id' => $store_id,
                            'store_name' => $store_name,
                            'config_id' => $config_id,
                        ]
                    ];
                } elseif ($return['subTradeStatus'] == 'PAYING') {
                    $return_data = [
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '正在支付',
                        'data' => [
                            'out_trade_no' => $out_trade_no,
                            'ways_type' => $ways_type,
                            'ways_source' => $data['ways_source'],
                            'total_amount' => $total_amount,
                            'store_id' => $store_id,
                            'store_name' => $store_name,
                            'config_id' => $config_id,
                        ]
                    ];
                }
            } else {
                $return_data = [
                    'status' => 1,
                    'pay_status' => '2',
                    'message' => '正在支付',
                    'data' => [
                        'out_trade_no' => $out_trade_no,
                        'ways_type' => $ways_type,
                        'ways_source' => $data['ways_source'],
                        'total_amount' => $total_amount,
                        'store_id' => $store_id,
                        'store_name' => $store_name,
                        'config_id' => $config_id,
                    ]
                ];
            }
        }

        return json_encode($return_data);
    }


    //长沙银行
    public function cs_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];

        $config = new ChangshaConfigController();

        $h_merchant = $config->cs_merchant($store_id, $store_pid);
        if (!$h_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '长沙银行商户号不存在'
            ]);
        }

        $h_config = $config->cs_config($data['config_id']);
        if (!$h_config) {
            return json_encode([
                'status' => 2,
                'message' => '长沙银行支付配置不存在请检查配置'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\Changsha\PayController();
        $data['ECustId'] = $h_merchant->ECustId; //
        $data['DeptId'] = $h_merchant->DeptId; //
        $data['StaffId'] = $h_merchant->StaffId; //
        $data['Longitude'] = '123'; //
        $data['Latitude'] = '123'; //
        $data['notify_url'] = url('/api/changsha/notify_url');
        $return = $obj->scan_pay($data);
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = $return['data']['OrderId'];
            $user_info = '';
            $pay_time = date('Y-m-d H:i:s', time());
            $buyer_pay_amount = $total_amount;
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $user_info,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount, //用户实际支付
                'receipt_amount' => $buyer_pay_amount //用户实际支付
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '25000',//返佣来源
                'source_desc' => 'changsha',//返佣来源说明
                'total_amount' => $total_amount,
                'other_no' => $data_insert['other_no'],
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 1,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }
    }


    //威富通支付
    public function wft_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'] ?? '';
        $store_pid = $data['store_pid'] ?? '';
        $merchant_id = $data['merchant_id'] ?? '';
        $total_amount = $data['total_amount'] ?? '';
        $ways_type = $data['ways_type'] ?? '';
        $store_name = $data['store_name'] ?? '';
        $config_id = $data['config_id'] ?? '';
        $ways_type_desc = $data['ways_type_desc'] ?? '';
        $ways_source = $data['ways_source'] ?? '';
        $title = $data['title'] ?? '';
        $tg_user_id = $data['tg_user_id'] ?? '';
        $out_trade_no = $data['out_trade_no'] ?? '';
        $code = $data['code'] ?? '';
        $company = $data['company'] ?? '';
        $ways_source_desc = $data['ways_source_desc'] ?? '';

        $config = new WftPayConfigController();
        $wftPayConfig = $config->wftpay_config($config_id);
        if (!$wftPayConfig) {
            return json_encode([
                'status' => 2,
                'message' => '威富通支付配置不存在请检查配置'
            ]);
        }

        $wftPayMerchant = $config->wftpay_merchant($store_id, $store_pid);
        if (!$wftPayMerchant) {
            return json_encode([
                'status' => 2,
                'message' => '威富通支付商户号不存在'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $ways_type;
        $data_insert['ways_type_desc'] = $ways_type_desc;
        $data_insert['ways_source'] = $ways_source;
        $data_insert['company'] = $company;
        $data_insert['ways_source_desc'] = $ways_source_desc;

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new WftpayPayController();
        $data['mch_id'] = $wftPayMerchant->mch_id;
        $data['out_trade_no'] = $out_trade_no;
        $data['body'] = $title;
        $data['total_amount'] = $total_amount;
        $data['code'] = $code;
        $data['private_rsa_key'] = $wftPayConfig->private_rsa_key;
        $data['public_rsa_key'] = $wftPayConfig->public_rsa_key;
        $return = $obj->scan_pay($data); //0-系统错误 1-成功 2-失败
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = $return['data']['transaction_id']; //平台交易号
            $other_no = $return['data']['out_transaction_id']; //第三方订单号
            $buyer_user_id = $return['data']['buyer_user_id'] ?? ''; //	买家在支付宝的用户id
            $buyer_logon_id = $return['data']['buyer_logon_id'] ?? ''; //买家支付宝账号
            $pay_time = date('Y-m-d H:i:s', time());
            $buyer_pay_amount = $total_amount;
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $update_data = [
                'trade_no' => $trade_no,
                'other_no' => $other_no,
                'buyer_id' => $buyer_user_id,
                'buyer_logon_id' => $buyer_logon_id,
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '', //credit：信用卡 pcredit：花呗（仅支付宝） debit：借记卡 balance：余额 unknown：未知
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount, //用户实际支付
                'receipt_amount' => $buyer_pay_amount //用户实际支付
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '27000', //返佣来源
                'source_desc' => 'wftpay', //返佣来源说明
                'total_amount' => $total_amount,
                'other_no' => $data_insert['other_no'],
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 2,
                'pay_status' => 3,
                'message' => $return['message'] ?? '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id
                ]
            ]);
        }
    }


    //汇旺财支付
    public function hwc_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'] ?? '';
        $store_pid = $data['store_pid'] ?? '';
        $merchant_id = $data['merchant_id'] ?? '';
        $total_amount = $data['total_amount'] ?? '';
        $ways_type = $data['ways_type'] ?? '';
        $store_name = $data['store_name'] ?? '';
        $config_id = $data['config_id'] ?? '';
        $ways_type_desc = $data['ways_type_desc'] ?? '';
        $ways_source = $data['ways_source'] ?? '';
        $title = $data['title'] ?? '';
        $tg_user_id = $data['tg_user_id'] ?? '';
        $out_trade_no = $data['out_trade_no'] ?? '';
        $code = $data['code'] ?? '';
        $company = $data['company'] ?? '';
        $ways_source_desc = $data['ways_source_desc'] ?? '';

        $config = new HwcPayConfigController();
        $hwcPayConfig = $config->hwcpay_config($config_id);
        if (!$hwcPayConfig) {
            return json_encode([
                'status' => 2,
                'message' => '威富通支付配置不存在请检查配置'
            ]);
        }

        $hwcPayMerchant = $config->hwcpay_merchant($store_id, $store_pid);
        if (!$hwcPayMerchant) {
            return json_encode([
                'status' => 2,
                'message' => '威富通支付商户号不存在'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $ways_type;
        $data_insert['ways_type_desc'] = $ways_type_desc;
        $data_insert['ways_source'] = $ways_source;
        $data_insert['company'] = $company;
        $data_insert['ways_source_desc'] = $ways_source_desc;

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new HwcpayPayController();
        $data['mch_id'] = $hwcPayMerchant->mch_id;
        $data['out_trade_no'] = $out_trade_no;
        $data['body'] = $title;
        $data['total_amount'] = $total_amount;
        $data['code'] = $code;
        $data['private_rsa_key'] = $hwcPayConfig->private_rsa_key;
        $data['public_rsa_key'] = $hwcPayConfig->public_rsa_key;
        $return = $obj->scan_pay($data); //0-系统错误 1-成功 2-失败
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = isset($return['data']['third_order_no']) ? $return['data']['third_order_no'] : $return['data']['transaction_id']; //第三方商户单号，可在支持的商户扫码退款
            $other_no = $return['data']['out_transaction_id']; //第三方订单号
            $buyer_user_id = $return['data']['buyer_user_id'] ?? ''; //	买家在支付宝的用户id
            $buyer_logon_id = $return['data']['buyer_logon_id'] ?? ''; //买家支付宝账号
            $pay_time = date('Y-m-d H:i:s', time());
            $buyer_pay_amount = $total_amount;
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $update_data = [
                'trade_no' => $trade_no,
                'other_no' => $other_no,
                'buyer_id' => $buyer_user_id,
                'buyer_logon_id' => $buyer_logon_id,
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '', //credit：信用卡 pcredit：花呗（仅支付宝） debit：借记卡 balance：余额 unknown：未知
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount, //用户实际支付
                'receipt_amount' => $buyer_pay_amount //用户实际支付
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '28000', //返佣来源
                'source_desc' => 'hwcpay', //返佣来源说明
                'total_amount' => $total_amount,
                'other_no' => $data_insert['other_no'],
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 2,
                'pay_status' => 3,
                'message' => $return['message'] ?? '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id
                ]
            ]);
        }
    }


    //银盛支付
    public function yinsheng_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'] ?? '';
        $store_pid = $data['store_pid'] ?? '';
        $merchant_id = $data['merchant_id'] ?? '';
        $total_amount = $data['total_amount'] ?? '';
        $ways_type = $data['ways_type'] ?? '';
        $store_name = $data['store_name'] ?? '';
        $config_id = $data['config_id'] ?? '';
        $ways_type_desc = $data['ways_type_desc'] ?? '';
        $ways_source = $data['ways_source'] ?? '';
        $tg_user_id = $data['tg_user_id'] ?? '';
        $out_trade_no = $data['out_trade_no'] ?? '';
        $code = $data['code'] ?? '';
        $company = $data['company'] ?? '';
        $ways_source_desc = $data['ways_source_desc'] ?? '';
        $device_id = $data['device_id'] ?? ''; //终端设备号.注:当中国银联时,为必填

        $config = new YinshengConfigController();
        $yinshengConfig = $config->yinsheng_config($config_id);
        if (!$yinshengConfig) {
            return json_encode([
                'status' => 2,
                'message' => '银盛支付配置不存在,请检查配置'
            ]);
        }

        $yinshengMerchant = $config->yinsheng_merchant($store_id, $store_pid);
        if (!$yinshengMerchant) {
            return json_encode([
                'status' => 2,
                'message' => '银盛支付,商户入网信息未配置'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $ways_type;
        $data_insert['ways_type_desc'] = $ways_type_desc;
        $data_insert['ways_source'] = $ways_source;
        $data_insert['company'] = $company;
        $data_insert['ways_source_desc'] = $ways_source_desc;

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }
        $bus_open_type = $yinshengMerchant->bus_open_type;
        $openTypeArr = explode('|', $bus_open_type);
        $business_code = '';
        foreach ($openTypeArr as $openType) {
            if ($openType == '00') { //00-扫码工作日到账
                $business_code = '00510030';
            }
            if ($openType == '01') { //01-扫码实时到账 00510080
                $business_code = '00510080';
            }
            if ($openType == '20') { //20-扫码次日到账
                $business_code = '00510030';
            }
        }
        $obj = new \App\Api\Controllers\YinSheng\PayController();
        $data['partner_id'] = $yinshengConfig->partner_id; //服务商商户号
        $data['seller_id'] = $yinshengMerchant->mer_code; //商户号
        $data['out_trade_no'] = $out_trade_no; //商户生成的订单号,生成规则前8位必须为交易日期,如20180525,范围跨度支持包含当天在内的前后一天,且只能由大小写英文字母,数字,下划线及横杠组成
        $data['ways_source'] = $ways_source; //
        $data['body'] = $store_name; //商品的标题,交易标题,订单标题,订单关键字等,最长为250个汉字
        $data['total_amount'] = $total_amount; //该笔订单的资金总额,单位为RMB-元.取值范围为[0.01,100000000.00],精确到小数点后两位
        $data['store_name'] = $store_name; //收款方银盛支付客户名(注册时公司名称)
        $data['business_code'] = $business_code; //业务代码
        $data['code'] = $code; //扫码支付授权码,设备读取用户展示的条码或者二维码信息
        $data['device_id'] = $device_id; //终端设备号.注:当bank_type域为中国银联-9001002时,为必填
        $return = $obj->scan_pay($data); //-1系统错误 1-交易成功 2-失败 3-等待买家付款 4-客户主动关闭订单 5-交易正在处理中 6-部分退款成功 7-全部退款成功
        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = isset($return['data']['trade_no']) ? $return['data']['trade_no'] : ''; //不可空,银盛支付合作商户网站唯一订单号
            $other_no = isset($return['data']['channel_send_sn']) ? $return['data']['channel_send_sn'] : '';
            $buyer_user_id = $return['data']['buyer_user_id'] ?? ''; //
            $buyer_logon_id = $return['data']['buyer_logon_id'] ?? ''; //
            $pay_time = isset($return['data']['pay_success_time']) ? $return['data']['pay_success_time'] : date('Y-m-d H:i:s', time());
            $buyer_pay_amount = isset($return['data']['total_amount']) ? $return['data']['total_amount'] : $total_amount; //不可空,该笔订单的资金总额，单位为RMB-Yuan。取值范围为[0.01，100000000.00]，精确到小数点后两位

            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $update_data = [
                'trade_no' => $trade_no,
                'other_no' => $other_no,
                'buyer_id' => $buyer_user_id,
                'buyer_logon_id' => $buyer_logon_id,
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '', //credit：信用卡 pcredit：花呗（仅支付宝） debit：借记卡 balance：余额 unknown：未知
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount, //用户实际支付
                'receipt_amount' => $buyer_pay_amount //用户实际支付
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '14000', //返佣来源
                'source_desc' => 'yinsheng', //返佣来源说明
                'total_amount' => $total_amount,
                'other_no' => $data_insert['other_no'],
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } //支付失败
        elseif ($return['status'] == 2) {
            $trade_no = isset($return['data']['trade_no']) ? $return['data']['trade_no'] : ''; //不可空,银盛支付合作商户网站唯一订单号
            $other_no = isset($return['data']['channel_send_sn']) ? $return['data']['channel_send_sn'] : ''; //可空,该交易在银盛支付系统中的交易流水号
            //Log::info("银盛paybase---------".json_encode($return['data']));
            $update_data = [
                'trade_no' => $trade_no,
                'other_no' => $other_no,
            ];
            $this->update_day_order($update_data, $out_trade_no);
            return json_encode([
                'status' => 1,
                'pay_status' => 2,
                'message' => $return['message'] ?? '等待支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id
                ]
            ]);
        }//系统错误
        elseif ($return['status'] == -1) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        } //其他错误
        else {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

    }

    //拉卡拉支付
    public function lkl_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'] ?? '';
        $store_pid = $data['store_pid'] ?? '';
        $merchant_id = $data['merchant_id'] ?? '';
        $total_amount = $data['total_amount'] ?? '';
        $ways_type = $data['ways_type'] ?? '';
        $store_name = $data['store_name'] ?? '';
        $config_id = $data['config_id'] ?? '';
        $ways_type_desc = $data['ways_type_desc'] ?? '';
        $ways_source = $data['ways_source'] ?? '';
        $tg_user_id = $data['tg_user_id'] ?? '';
        $out_trade_no = $data['out_trade_no'] ?? '';
        $code = $data['code'] ?? '';
        $company = $data['company'] ?? '';
        $ways_source_desc = $data['ways_source_desc'] ?? '';

        $config = new LklConfigController();
        $lklConfig = $config->lkl_config($config_id);
        if (!$lklConfig) {
            return json_encode([
                'status' => 2,
                'message' => '拉卡拉支付配置不存在,请检查配置'
            ]);
        }

        $lklMerchant = $config->lkl_merchant($store_id, $store_pid);
        if (!$lklMerchant) {
            return json_encode([
                'status' => 2,
                'message' => '拉卡拉支付,商户入网信息未配置'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $ways_type;
        $data_insert['ways_type_desc'] = $ways_type_desc;
        $data_insert['ways_source'] = $ways_source;
        $data_insert['company'] = $company;
        $data_insert['ways_source_desc'] = $ways_source_desc;

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\LklPay\PayController();
        $data['merchant_no'] = $lklMerchant->customer_no;
        $data['term_no'] = $lklMerchant->term_no;
        $data['out_trade_no'] = $out_trade_no;
        $data['total_amount'] = $total_amount;
        $data['auth_code'] = $code;
        $return = $obj->scan_pay($data);
        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = isset($return['data']['trade_no']) ? $return['data']['trade_no'] : '';
            $other_no = isset($return['data']['acc_trade_no']) ? $return['data']['acc_trade_no'] : '';
            $pay_time = isset($return['data']['trade_time']) ? $return['data']['trade_time'] : date('Y-m-d H:i:s', time());
            $buyer_pay_amount = isset($return['data']['total_amount']) ? $return['data']['total_amount'] / 100 : $total_amount; //不可空,该笔订单的资金总额，单位为RMB-Yuan。取值范围为[0.01，100000000.00]，精确到小数点后两位

            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $update_data = [
                'trade_no' => $trade_no,
                'other_no' => $other_no,
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '', //credit：信用卡 pcredit：花呗（仅支付宝） debit：借记卡 balance：余额 unknown：未知
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount, //用户实际支付
                'receipt_amount' => $buyer_pay_amount //用户实际支付
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '34000', //返佣来源
                'source_desc' => 'lklpay', //返佣来源说明
                'total_amount' => $total_amount,
                'other_no' => $data_insert['other_no'],
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } //支付失败
        elseif ($return['status'] == 2) {
            return json_encode([
                'status' => 1,
                'pay_status' => 2,
                'message' => $return['message'] ?? '等待支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id
                ]
            ]);
        }//系统错误
        elseif ($return['status'] == -1) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        } //其他错误
        else {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

    }


    //钱方支付
    public function qf_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'] ?? '';
        $store_pid = $data['store_pid'] ?? '';
        $merchant_id = $data['merchant_id'] ?? '';
        $total_amount = $data['total_amount'] ?? '';
        $ways_type = $data['ways_type'] ?? '';
        $store_name = $data['store_name'] ?? '';
        $config_id = $data['config_id'] ?? '';
        $ways_type_desc = $data['ways_type_desc'] ?? '';
        $ways_source = $data['ways_source'] ?? '';
        $title = $data['title'] ?? '';
        $tg_user_id = $data['tg_user_id'] ?? '';
        $out_trade_no = $data['out_trade_no'] ?? '';
        $code = $data['code'] ?? '';
        $company = $data['company'] ?? '';
        $ways_source_desc = $data['ways_source_desc'] ?? '';
        $device_id = $data['device_id'] ?? ''; //

        $config = new QfPayConfigController();
        $qfPayConfig = $config->qfpay_config($config_id);
        if (!$qfPayConfig) {
            return json_encode([
                'status' => 2,
                'message' => '钱方支付配置不存在,请检查配置'
            ]);
        }

        $qfPayMerchant = $config->qfpay_merchant($store_id, $store_pid);
        if (!$qfPayMerchant) {
            return json_encode([
                'status' => 2,
                'message' => '钱方支付,商户入网信息未配置'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $ways_type;
        $data_insert['ways_type_desc'] = $ways_type_desc;
        $data_insert['ways_source'] = $ways_source;
        $data_insert['company'] = $company;
        $data_insert['ways_source_desc'] = $ways_source_desc;

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new QfPayController();
        $reqData = [
            'ways_source' => $ways_source,  //支付类型,alipay-支付宝;weixin-微信;jd-京东;unionpay-银联
            'key' => $qfPayConfig->key,  //加签key
            'code' => $qfPayConfig->code,  //开发唯一标识
            'total_amount' => $total_amount,  //订单支付金额
            'mchid' => $qfPayMerchant->mchid,  //子商户号,标识子商户身份,由钱方分配(渠道系统后台查看对应商户(非业务员)子商户号,被视为对应商户的交易)
            'out_trade_no' => $out_trade_no,  //外部订单号,开发者平台订单号,同子商户(mchid)下,每次成功调用支付(含退款)接口下单,该参数值均不能重复使用,保证单号唯一,长度不超过128字符
            'auth_code' => $code,  //微信或者支付宝的授权码
            'pay_type' => 800008  //支付类型: 支付宝扫码-800101; 支付宝反扫-800108; 支付宝服务窗支付-800107;;微信扫码-800201; 微信反扫-800208; 微信公众号支付-800207; 微信小程序支付-800213;QQ扫码-800601; QQ反扫-800608(暂不可用);;云闪付扫码-800701; 云闪付反扫-800708;;统一聚合反扫-800008
        ];
        Log::info('钱方支付-商户统一聚合反扫-入参：');
        Log::info($reqData);
        $return = $obj->payment($reqData); //1-成功 3-交易中
        Log::info('钱方支付-商户统一聚合反扫-结果：');
        Log::info($return);
        //支付成功
        if ($return['status'] == 1) {
            $trade_no = isset($return['data']['syssn']) ? $return['data']['syssn'] : ''; //钱方订单号
            $txamt = isset($return['data']['txamt']) ? $return['data']['txamt'] : 0; //订单支付金额，单位分
            $txamt = number_format($txamt / 100, 2, '.', '');
            $pay_time = isset($return['data']['txdtm']) ? $return['data']['txdtm'] : ''; //请求交易时间,格式为：YYYY-MM-DD HH:MM:SS
            $sysdtm = isset($return['data']['sysdtm']) ? $return['data']['sysdtm'] : $pay_time; //系统时间
            $other_no = isset($return['data']['out_trade_no']) ? $return['data']['out_trade_no'] : ''; //外部订单号，开发者平台订单号
            $pay_type = isset($return['data']['pay_type']) ? $return['data']['pay_type'] : ''; //支付类型:微信反扫:800208；支付宝反扫:800108;云闪付反扫:800708;QQ反扫:800608（暂不可用）;统一聚合反扫-800008
            $resperr = isset($return['data']['resperr']) ? $return['data']['resperr'] : ''; //信息描述
            $txcurrcd = isset($return['data']['txcurrcd']) ? $return['data']['txcurrcd'] : ''; //币种:港币-HKD；人民币-CNY
            $respmsg = isset($return['data']['respmsg']) ? $return['data']['respmsg'] : ''; //调试信息
            $respcd = isset($return['data']['respcd']) ? $return['data']['respcd'] : ''; //交易返回码0000表示交易支付成功；1143、1145表示交易中，需继续查询交易结果； 其他返回码表示交易失败

            $update_data = [
                'trade_no' => $trade_no,
                'status' => '1',
                'pay_status' => 1,
                'pay_status_desc' => '支付成功',
                'pay_time' => $sysdtm,
                'buyer_pay_amount' => $txamt, //买家付款的金额
                'receipt_amount' => $txamt //用户实际支付
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '24000', //返佣来源
                'source_desc' => 'qfpay', //返佣来源说明
                'total_amount' => $txamt,
                'other_no' => $data_insert['other_no'],
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $sysdtm,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : ""
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $txamt,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $sysdtm,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source']
                ]
            ]);
        } elseif ($return['status'] == 3) { //交易中
            return json_encode([
                'status' => 1,
                'pay_status' => '2',
                'message' => '正在支付',
//                'message' => $return['message'] ?? '交易中，建议调用查询接口',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source']
                ]
            ]);
        } else { //其他错误
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

    }


    //云闪付 被扫 公共
    public function ysf_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $tg_user_id = $data['tg_user_id'];

        $config = new EasyPayConfigController();
        $easypay_config = $config->easypay_config($data['config_id']);
        if (!$easypay_config) {
            return json_encode([
                'status' => '2',
                'message' => '易生支付支付配置不存在，请检查'
            ]);
        }
        //判断是否开通云闪付通道
        $easypay_merchant = $config->easypay_merchant($store_id, $store_pid);
        if (!$easypay_merchant) {
            return json_encode([
                'status' => '2',
                'message' => '易生支付通道未开通成功'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        if ($data['ways_source'] == 'weixin') {
            $tradeCode = 'WTB2C';
        } elseif ($data['ways_source'] == 'alipay') {
            $tradeCode = 'WAB2C';
        } elseif ($data['ways_source'] == 'unionpay') {
            $tradeCode = 'WUB2C';
        }
        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => '2',
                'message' => '订单未入库'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];
        $total_amount = $data['total_amount'];
        $total_amount = number_format($total_amount, 2, '.', '');

        $sayDodgepay = new SayDodgePayController();
        $ysf_data = [
            'orgId' => $easypay_config->channel_id,
            'orgMercode' => $easypay_merchant->term_mercode,
            'orgTermno' => $easypay_merchant->term_termcode,
            'orgTrace' => $data['out_trade_no'],
            'tradeAmt' => $data['total_amount'],
            'encryptInfo' => "",
            'fixBuyer' => "F",
            'minage' => "18",
            'needCheckInfo' => 'F',
            'type' => '01',
            'tradeCode' => $tradeCode,
        ];

        $return = $sayDodgepay->scanPay($ysf_data); //-1 系统错误 0-其他 1-成功 2-待支付

        //支付成功
        if ($return['status'] == '1') {
            $trade_no = ''; //系统订单号
            $pay_time = ''; //支付完成时间，格式为 yyyyMMddhhmmss，如 2009年12月27日9点10分10秒表示为 20091227091010
            $acctype = $return['data']['acctClass'] ?? ''; //交易账户类型
            //$wxopenid = $return['data']['wxopenid'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
            $clearamt = isset($return['payDetail']['aliFundBillList']->amount) ? ($return['payDetail']['aliFundBillList']->amount / 100) : $total_amount; //结算金额，单位分
            $payamt = isset($return['payDetail']['aliFundBillList']->real_amount) ? ($return['payDetail']['aliFundBillList']->real_amount / 100) : $total_amount; //实付金额，单位分

            $payment_method = '';
            if ($acctype) {
                switch ($acctype) {
                    case 'C': //信用卡
                        $payment_method = 'credit';
                        break;
                    case 'D': //借记卡
                        $payment_method = 'debit';
                        break;
                    case 'E': //零钱
                        $payment_method = 'balance';
                        break;
                    default: //U其他
                        $payment_method = 'unknown';
                }
            }

            $fee_amount = ($data_insert['rate'] * $payamt) / 100;
            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => "",
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status' => 1,
                'pay_status_desc' => '支付成功',
                'payment_method' => $payment_method, //credit:信用卡 pcredit:花呗（仅支付宝） debit:借记卡 balance:余额 unknown:未知
                'buyer_pay_amount' => $clearamt, //买家付款的金额
                'receipt_amount' => $payamt, //实付
                'pay_time' => $pay_time,
                'fee_amount' => $fee_amount
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '21000', //返佣来源
                'source_desc' => '云闪付', //返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'other_no' => $data_insert['other_no'],
                'rate' => $data_insert['rate'],
                'fee_amount' => $fee_amount,
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => $data['device_id'] ?? '',
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => 1,
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } //待支付
        elseif ($return['status'] == '2') {
            return json_encode([
                'status' => 1,
                'pay_status' => 2,
                'message' => '待支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } else {
            return json_encode([
                'status' => '3',
                'message' => $return['message']
            ]);
        }

        //支付失败
//        if ($return['status'] == '2') {
//            return json_encode([
//                'status' => 3,
//                'pay_status' => '3',
//                'message' => '支付失败',
//                'data' => [
//                    'out_trade_no' => $out_trade_no,
//                    'ways_type' => $ways_type,
//                    'total_amount' => $total_amount,
//                    'store_id' => $store_id,
//                    'store_name' => $store_name,
//                    'config_id' => $config_id,
//                    'ways_source' => $data['ways_source'],
//                ]
//            ]);
//        }

    }


    //云闪付js支付 提交 中转站 公共
    public function sayDodge_js_pay_public($data, $ways)
    {
        try {
            $merchant_name = isset($data['merchant_name']) ? $data['merchant_name'] : "";
            $store_id = isset($data['store_id']) ? $data['store_id'] : "";
            $merchant_id = isset($data['merchant_id']) ? $data['merchant_id'] : "0";
            $config_id = isset($data['config_id']) ? $data['config_id'] : "";
            $store_name = isset($data['store_name']) ? $data['store_name'] : "";
            $store_pid = isset($data['store_pid']) ? $data['store_pid'] : "";
            $tg_user_id = isset($data['tg_user_id']) ? $data['tg_user_id'] : "";
            $total_amount = isset($data['total_amount']) ? $data['total_amount'] : "";
            $shop_price = isset($data['shop_price']) ? $data['shop_price'] : "";
            $remark = isset($data['remark']) ? $data['remark'] : "";
            $device_id = isset($data['device_id']) ? $data['device_id'] : "";
            $shop_name = isset($data['shop_name']) ? $data['shop_name'] : "";
            $shop_desc = isset($data['shop_desc']) ? $data['shop_desc'] : "";
            $open_id = isset($data['open_id']) ? $data['open_id'] : "";
            $ways_type = isset($data['ways_type']) ? $data['ways_type'] : "";
            $other_no = isset($data['other_no']) ? $data['other_no'] : "";
            $notify_url = isset($data['notify_url']) ? $data['notify_url'] : "";
            $coupon_type = isset($data['coupon_type']) ? $data['coupon_type'] : ""; //优惠类型 1 充值
            $pay_amount = isset($data['pay_amount']) ? $data['pay_amount'] : $total_amount; //支付金额
            $dk_jf = isset($data['dk_jf']) ? $data['dk_jf'] : '0'; //抵扣积分
            $dk_money = isset($data['dk_money']) ? $data['dk_money'] : '0'; //抵扣金额
            $tradeCode = isset($data['tradeCode']) ? $data['tradeCode'] : ''; //抵扣金额

            //插入数据库
            $data_insert = [
                'trade_no' => '',
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'store_name' => $store_name,
                'buyer_id' => $open_id,
                'total_amount' => $total_amount,
                'shop_price' => $shop_price,
                'pay_amount' => $pay_amount,
                'payment_method' => '',
                'status' => '',
                'pay_status' => 2,
                'pay_status_desc' => '等待支付',
                'merchant_id' => $merchant_id,
                'merchant_name' => $merchant_name,
                'remark' => $remark,
                'device_id' => $device_id,
                'config_id' => $config_id,
                'company' => $ways->company,
                'other_no' => $other_no,
                'notify_url' => $notify_url,
                'coupon_type' => $coupon_type,
                'dk_jf' => $dk_jf,
                'dk_money' => $dk_money,
            ];

            //扫码费率入库
            $data_insert['rate'] = $ways->rate;
            $data_insert['fee_amount'] = $ways->rate * $pay_amount / 100;

            //云闪付 微信27008 支付宝27009 银联27010
            if ($ways_type == '27008' || $ways_type == '27009' || $ways_type == '27010') {
                $config = new EasyPayConfigController();
                $easypay_config = $config->easypay_config($config_id);
                if (!$easypay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生支付配置不存在请检查配置'
                    ]);
                }

                $easypay_merchant = $config->easypay_merchant($store_id, $store_pid);
                if (!$easypay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生支付商户号不存在'
                    ]);
                }
                $sayDodge = new SayDodgePayController();

                if ($ways_type == '21009') {
                    $out_trade_no = substr($easypay_config->client_code, 0, 4) . date('YmdHis', time()) . substr($easypay_config->client_code, -4) . $sayDodge->rand_code();
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['out_trade_no'] = $out_trade_no;

                }
                if ($ways_type == '21008') {
                    $out_trade_no = substr($easypay_config->client_code, 0, 4) . date('YmdHis', time()) . substr($easypay_config->client_code, -4) . $sayDodge->rand_code();
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['ways_source_desc'] = '微信支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }
                if ($ways_type == '21010') {
                    $out_trade_no = substr($easypay_config->client_code, 0, 4) . date('YmdHis', time()) . substr($easypay_config->client_code, -4) . $sayDodge->rand_code();
                    $data_insert['ways_type'] = $ways_type;
                    $data_insert['ways_type_desc'] = '银联支付';
                    $data_insert['ways_source'] = 'unionpay';
                    $data_insert['ways_source_desc'] = '银联支付';
                    $data_insert['out_trade_no'] = $out_trade_no;
                }
                $ysf_data = [
                    'orgId' => $easypay_config->channel_id,
                    'orgMercode' => $easypay_merchant->term_mercode,
                    'orgTermno' => $easypay_merchant->term_termcode,
                    'orgTrace' => $data['out_trade_no'],
                    'tradeAmt' => $data['total_amount'],
                    'encryptInfo' => "",
                    'fixBuyer' => "F",
                    'minage' => "18",
                    'needCheckInfo' => 'F',
                    'type' => '01',
                    'tradeCode' => $tradeCode,
                    'orgBackUrl' => url('/api/sayDodge/pay_notify_url')
                ];
                $resp = $sayDodge->jsapi($ysf_data);
                if ($resp['status'] == '1') {
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'data' => $resp['data'],
                        ]);
                    }

                }

            }

            return json_encode([
                'status' => 2,
                'message' => '暂不支持此通道'
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getLine()
            ]);
        }
    }

    //付款码支付
    public function pay_scanPay(Request $request)
    {
        try {
            $orgId = $request->get('orgId');
            $data = $request->get('data');
            $sign = $request->get('sign');
            $storeId = $request->get('storeId');
            if ($orgId == '') {
                return json_encode([
                    'status' => 2,
                    'message' => 'orgId不能为空'
                ]);
            }
            if ($storeId == '') {
                return json_encode([
                    'status' => 2,
                    'message' => 'storeId不能为空'
                ]);
            }
            if (!$data['total_amount']) {
                return json_encode([
                    'status' => 2,
                    'message' => 'total_amount不能为空'
                ]);
            }
            if (!$data['code']) {
                return json_encode([
                    'status' => 2,
                    'message' => 'code不能为空'
                ]);
            }
            if (!$data['out_trade_no']) {
                return json_encode([
                    'status' => 2,
                    'message' => 'out_trade_no不能为空'
                ]);
            }
            if ($sign == '') {
                return json_encode([
                    'status' => 2,
                    'message' => 'sign不能为空'
                ]);
            }
            $code = $data['code'];

            //处理ERP系统订单重复问题
            $out_trade_no = $data['out_trade_no'];
            $out_trade_no = $out_trade_no . sprintf('%03d', rand(0, 999));

            $userKeyUrl = UserKeyUrl::where('org_id', $orgId)->first();
            if (!$userKeyUrl) {
                return json_encode([
                    'status' => 2,
                    'message' => '代理商不存在',
                ]);
            }
            $verify = $this->verifySign($data, $sign, $userKeyUrl->public_key);//验签
            if (!$verify) {
                return json_encode([
                    'status' => 2,
                    'message' => '验签失败',
                ]);
            }
            $store = Store::where('store_id', $storeId)
                ->select('config_id', 'user_id', 'pid', 'merchant_id', 'store_short_name', 'store_address', 'store_name', 'is_delete', 'is_admin_close', 'is_close')
                ->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '没有关联认证门店请联系服务商'
                ]);
            }
            $merchant_id = $store->merchant_id;
            $merchant = Merchant::where('id', $merchant_id)
                ->select('name')
                ->first();
            $merchant_name = '';
            if ($merchant) {
                $merchant_name = $merchant->name;
            }
            $shop_price = $request->get('shop_price', $data['total_amount']);
            $remark = $request->get('remark', '');
            $device_id = $request->get('device_id', '收银系统');
            $shop_name = $request->get('shop_name', '扫一扫收款');
            $shop_desc = $request->get('shop_desc', '扫一扫收款');
            $other_no = $request->get('other_no', '');
            $pay_method = $request->get('pay_method', 'scan');
            //刷脸处理
            $str = substr($code, 0, 2);
            if (in_array($str, ['28']) || in_array($str, ['13', '14'])) {
                $strlen = strlen($code);
                if ($strlen > 18) {
                    $substr = substr($code, -1); //截取判断最后一个
                    $code = substr($code, 0, -1); //去掉最后一个

                    //支付宝刷脸
                    if ($substr == '2' && in_array($str, ['28'])) {
                        $pay_method = 'alipay_face';
                        $device_id = $request->get('device_id', '刷脸设备');
                    }

                    //微信刷脸
                    if ($substr == '2' && in_array($str, ['13', '14'])) {
                        $pay_method = 'weixin_face';
                        $device_id = $request->get('device_id', '刷脸设备');
                    }

                }

            }
            $data = [
                'config_id' => $store->config_id,
                'merchant_id' => $merchant_id,
                'merchant_name' => $merchant_name,
                'code' => $code,
                'total_amount' => $data['total_amount'] / 100,
                'shop_price' => $shop_price / 100,
                'remark' => $remark,
                'device_id' => $device_id,
                'shop_name' => $shop_name,
                'shop_desc' => $shop_desc,
                'store_id' => $storeId,
                'other_no' => $other_no,
                'pay_method' => $pay_method,
                'out_trade_no' => $out_trade_no
            ];
            return $this->scan_pay_public($data);
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }

    //客户jsapi支付
    public function pay_jsapi(Request $request)
    {

        try {
            $orgId = $request->get('orgId');
            $data = $request->get('data');
            $sign = $request->get('sign');
            $storeId = $request->get('storeId');
            $notify_url = $request->get('notify_url');
            if ($orgId == '') {
                return json_encode([
                    'status' => 2,
                    'message' => 'orgId不能为空'
                ]);
            }
            if ($storeId == '') {
                return json_encode([
                    'status' => 2,
                    'message' => 'storeId不能为空'
                ]);
            }
            if (!$data['total_amount']) {
                return json_encode([
                    'status' => 2,
                    'message' => 'total_amount不能为空'
                ]);
            }
            if (!$data['total_code']) {
                return json_encode([
                    'status' => 2,
                    'message' => 'total_code不能为空'
                ]);
            }
            if (!$data['payerId']) {
                return json_encode([
                    'status' => 2,
                    'message' => 'payerId不能为空'
                ]);
            }
            if (!$data['out_trade_no']) {
                return json_encode([
                    'status' => 2,
                    'message' => 'out_trade_no不能为空'
                ]);
            }
            if ($sign == '') {
                return json_encode([
                    'status' => 2,
                    'message' => 'sign不能为空'
                ]);
            }
            if ($notify_url == '') {
                return json_encode([
                    'status' => 2,
                    'message' => 'notify_url不能为空'
                ]);
            }
            $userKeyUrl = UserKeyUrl::where('org_id', $orgId)->first();
            if (!$userKeyUrl) {
                return json_encode([
                    'status' => 2,
                    'message' => '代理商不存在',
                ]);
            }
            $userKeyUrl->where('user_id', $orgId)->update(['notify_url' => $notify_url]);
            $verify = $this->verifySign($data, $sign, $userKeyUrl->public_key);//验签
            if (!$verify) {
                return json_encode([
                    'status' => 2,
                    'message' => '验签失败',
                ]);
            }
            $store = Store::where('store_id', $storeId)
                ->select('config_id', 'user_id', 'pid', 'merchant_id', 'store_short_name', 'store_address', 'store_name', 'is_delete', 'is_admin_close', 'is_close', 'source')
                ->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '没有关联认证门店请联系服务商'
                ]);
            }
            $merchant_id = $store->merchant_id;
            $merchant = Merchant::where('id', $merchant_id)
                ->select('name')
                ->first();
            $merchant_name = '';
            if ($merchant) {
                $merchant_name = $merchant->name;
            }

            //关闭的商户禁止交易
            if ($store->is_close || $store->is_admin_close || $store->is_delete) {
                return json_encode([
                    'status' => 2,
                    'message' => '商户已经被服务商关闭'
                ]);
            }

            $source = $store->source;
            $config_id = $store->config_id;
            $store_name = $store->store_name;
            $store_pid = $store->pid;
            $tg_user_id = $store->user_id;
            $total_amount = $data['total_amount'];
            $open_id = $data['payerId'];
            $ways_type = $data['total_code'];
            $out_trade_no = $data['out_trade_no'];
            $shop_price = $request->get('shop_price', $total_amount);
            $remark = $request->get('remark', '');
            $device_id = $request->get('device_id', 'app');
            $shop_name = $request->get('shop_name', '扫码收款');
            $shop_desc = $request->get('shop_desc', '扫码收款');
            $other_no = $request->get('other_no', '');
            $notify_url = $request->get('notify_url', '');
            $is_fq = $request->get('is_fq', '');
            $auth_code = $request->get('auth_code', ''); //支付宝、微信授权码
            $origin_id = $request->get('originId', ''); //系统达达订单号
            $pay_method = $request->get('pay_method', 'qr_code'); //支付方式
            if ($other_no == "" || $other_no == "NULL") {
                $other_no = "";
            }

            if ($notify_url == "" || $notify_url == "NULL") {
                $notify_url = "";
            }

            $obj_ways = new  PayWaysController();
            $ways = $obj_ways->ways_type($ways_type, $storeId, $store_pid);
            if (!$ways) {
                return json_encode([
                    'status' => '2',
                    'message' => '没有开通此类型通道'
                ]);
            }

            if ($ways->is_close) {
                return json_encode([
                    'status' => 2,
                    'message' => '此通道已关闭'
                ]);
            }

            if (isset($ways->pay_amount_e) && $total_amount > $ways->pay_amount_e) {
                return json_encode([
                    'status' => 2,
                    'message' => '单笔金额不能超过' . $ways->pay_amount_e
                ]);
            }

            $user_rate_obj = $obj_ways->getCostRate($ways->ways_type, $tg_user_id);
            $cost_rate = $user_rate_obj ? $user_rate_obj->rate : 0.00; //成本费率

            $data = [
                'merchant_name' => $merchant_name,
                'store_id' => $storeId,
                'merchant_id' => $merchant_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'store_pid' => $store_pid,
                'tg_user_id' => $tg_user_id,
                'total_amount' => $total_amount / 100,
                'shop_price' => $shop_price / 100,
                'remark' => $remark,
                'device_id' => $device_id,
                'shop_name' => $shop_name,
                'shop_desc' => $shop_desc,
                'open_id' => $open_id,
                'ways_type' => $ways_type,
                'other_no' => $other_no,
                'notify_url' => $notify_url,
                'auth_code' => $auth_code,
                'out_trade_no' => $out_trade_no,
                'source' => $source
            ];

            if ($is_fq) {
                $hb_fq_num = $request->get('hb_fq_num', ''); //花呗分期数
                $hb_fq_seller_percent = $request->get('hb_fq_seller_percent', 0);
                $data['hb_fq_num'] = $hb_fq_num;
                $data['hb_fq_seller_percent'] = $hb_fq_seller_percent;

                return $this->fq_qr_auth_pay_public($data, $ways);
            } else {
                if ($origin_id) $data['origin_id'] = $origin_id;
                if ($pay_method) $data['pay_method'] = $pay_method;
                if ($cost_rate) $data['cost_rate'] = $cost_rate;
                return $this->qr_auth_pay_public($data, $ways);
            }
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }

    //校验 RSA2 签名
    function verifySign($data, $sign, $pubKey)
    {
        $sign = base64_decode($sign);
        $data = $this->formatBizQueryParaMap($data, false);

        $pubKey = "-----BEGIN PUBLIC KEY-----\n" .
            wordwrap($pubKey, 64, "\n", true) .
            "\n-----END PUBLIC KEY-----";
        $key = openssl_pkey_get_public($pubKey);

        $result = openssl_verify($data, $sign, $key, OPENSSL_ALGO_SHA256) === 1;
        return $result;
    }

    //组装签名函数
    function formatBizQueryParaMap($paraMap, $urlencode)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if ($urlencode) {
                $v = urlencode($v);
            }
            $buff .= $k . "=" . $v . "&";
        }
        $reqPar = '';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }
}
