<?php
namespace App\Api\Controllers\Merchant;


use App\Api\Controllers\BaseController;
use App\Api\Controllers\AlipayOpen\AppletsController;
use App\Api\Controllers\Rsa\RsaE;
use App\Models\DadaConfig;
use App\Models\Merchant;
use App\Models\MerchantStore;
use App\Models\MerchantStoreAppidSecret;
use App\Models\QrList;
use App\Models\QrListInfo;
use App\Models\QrPayInfo;
use App\Models\Store;
use App\Models\StorePayWay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class SetController extends BaseController
{
    // protected $app_id; // 第三方开放平台appid
    // protected $wechat_pages_url; // 微信小程序扫码点餐页面地址
    //
    // public function __construct()
    // {
    //     // $this->app_id = config('api.app_id');
    //     $model = new WechatThirdConfig();
    //     $data = $model->getInfo();
    //     $this->app_id = $data->app_id;
    //     $this->wechat_pages_url = 'pages/payment/payment';
    // }

    //设置登录密码
    public function set_password(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $oldpassword = $request->get('oldpassword', '');
            $newpassword = $request->get('newpassword', '');
            $newpassword_confirmed = $request->get('newpassword_confirmed', '');


            $check_data = [
                'oldpassword' => '旧密码',
                'newpassword' => '新密码',
                'newpassword_confirmed' => '确认新密码'
            ];

            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $local = Merchant::where('id', $merchant->merchant_id)->first();
            //验证旧的密码
            if (!Hash::check($oldpassword, $local->password)) {
                return json_encode([
                    'status' => 2,
                    'message' => '旧的登陆密码不匹配'
                ]);
            }
            if ($newpassword !== $newpassword_confirmed) {
                return json_encode([
                    'status' => 2,
                    'message' => '两次密码不一致'
                ]);
            }
            if (strlen($newpassword) < 6) {
                return json_encode([
                    'status' => 2,
                    'message' => '密码长度不符合要求'
                ]);
            }
            $dataIN = [
                'password' => bcrypt($newpassword),

            ];
            Merchant::where('id', $merchant->merchant_id)->update($dataIN);


            $data = [
                'status' => 1,
                'message' => '密码修改成功',
                'data' => []
            ];
            return json_encode($data);

        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }


    //修改登录手机号
    public function edit_login_phone(Request $request)
    {

        try {
            $merchant = $this->parseToken();
            $data = $request->all();
            $password = $request->get('password', '');
            $new_phone = $request->get('phone', '');
            $code_b = $request->get('code_b', '');
            //如果只传password代表校验
            if ($password && $new_phone == "" && $code_b == "") {
                //验证验证码
                $local = Merchant::where('id', $merchant->merchant_id)->first();
                //验证旧的密码
                if (!Hash::check($password, $local->password)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '密码不匹配'
                    ]);
                } else {
                    return json_encode([
                        'status' => 1,
                        'message' => '密码匹配成功',
                        'data' => [],
                    ]);
                }
            } else {
                if ($code_b && $new_phone == '' & $password == "") {
                    //验证新手机验证码
                    $msn_local = Cache::get($new_phone . 'editphone-2');
                    if (0 && $code_b != $msn_local) {
                        return json_encode([
                            'status' => 2,
                            'message' => '新手机号码短信验证码不匹配'
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'message' => '短信验证码匹配成功',
                            'data' => [],
                        ]);
                    }
                }

                //换手机号码
                //验证新的手机号
                if (!preg_match("/^1[3456789]{1}\d{9}$/", $new_phone)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '手机号码不正确'
                    ]);
                }
                if ($new_phone == $merchant->phone) {
                    return json_encode([
                        'status' => 2,
                        'message' => '手机号码未更改'
                    ]);
                }
                $rules = [
                    'phone' => 'required|min:11|max:11|unique:merchants',
                ];
                $validator = Validator::make($data, $rules);
                if ($validator->fails()) {
                    return json_encode([
                        'status' => 2,
                        'message' => '账号已注册请更换'
                    ]);
                }


                //验证新手机验证码
                $msn_local = Cache::get($new_phone . 'editphone-2');
                if (0 && $code_b != $msn_local) {
                    return json_encode([
                        'status' => 2,
                        'message' => '新手机号码短信验证码不匹配'
                    ]);
                }

                Merchant::where('id', $merchant->merchant_id)->update(['phone' => $new_phone]);


                $data = [
                    'status' => 1,
                    'message' => '手机号修改成功',
                    'data' => [

                    ]
                ];
                return json_encode($data);

            }
        } catch
        (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }


    //扣款顺序列表
    public function pay_ways_sort(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $merchant_id = $merchant->merchant_id;
            $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)
                ->orderBy('created_at', 'asc')
                ->first();
            if ($MerchantStore) {
                $store_id = $MerchantStore->store_id;
            }
            if ($store_id) {
                $alipay = DB::table('store_pay_ways')
                    ->where('store_id', $store_id)
                    ->where('ways_source', 'alipay')
                    ->select('id as store_pay_ways_id', 'ways_desc', 'ways_type', 'store_id', 'sort', 'ways_source')
                    ->where('status', 1)
                    ->orderBy('store_pay_ways.sort', 'asc')
                    ->get();

                $weixin = DB::table('store_pay_ways')
                    ->where('store_id', $store_id)
                    ->where('ways_source', 'weixin')
                    ->select('id as store_pay_ways_id', 'ways_desc', 'ways_type', 'store_id', 'sort', 'ways_source')
                    ->where('status', 1)
                    ->orderBy('store_pay_ways.sort', 'asc')
                    ->get();

                $jd = DB::table('store_pay_ways')
                    ->where('store_id', $store_id)
                    ->where('ways_source', 'jd')
                    ->select('id as store_pay_ways_id', 'ways_desc', 'ways_type', 'store_id', 'sort', 'ways_source')
                    ->where('status', 1)
                    ->orderBy('store_pay_ways.sort', 'asc')
                    ->get();

                $unionpayqr = DB::table('store_pay_ways')
                    ->where('store_id', $store_id)
                    ->where('ways_source', 'unionpay')
                    ->select('id as store_pay_ways_id', 'ways_desc', 'ways_type', 'store_id', 'sort', 'ways_source')
                    ->where('status', 1)
                    ->orderBy('store_pay_ways.sort', 'asc')
                    ->get();

//                $applet = DB::table('store_pay_ways')
//                    ->where('store_id', $store_id)
//                    ->where('ways_source', 'applet')
//                    ->select('id as store_pay_ways_id', 'ways_desc', 'ways_type', 'store_id', 'sort', 'ways_source')
//                    ->where('status', 1)
//                    ->orderBy('store_pay_ways.sort', 'asc')
//                    ->get();

                return json_encode([
                    'status' => 1,
                    'is_open' => 1,
                    'message' => '数据返回成功',
                    'data' => [
                        'ailpay' => $alipay,
                        'weixin' => $weixin,
                        'jd' => $jd,
                        'unionpayqr' => $unionpayqr,
                        'applet' => $applet,
                    ]
                ]);

            } else {
                return json_encode(['status' => 2, 'message' => '没有绑定店铺']);
            }
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }


    //扣款顺序修改
    public function pay_ways_sort_edit(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $store_pay_ways_id = (int)$request->get('store_pay_ways_id');
            $new_sort = $request->get('new_sort');

            $check_data = [
                'store_pay_ways_id' => '通道类型id',
                'new_sort' => '新位置',
            ];

            //收银员
            if ($merchant->merchant_type == 2) {
                return json_encode([
                    'status' => 2,
                    'message' => '收银员没有权限'
                ]);
            }

            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $ch_storePayWay = StorePayWay::where('id', $store_pay_ways_id)->first();
            $store_id = $ch_storePayWay->store_id;

            $ways_source = $ch_storePayWay->ways_source;
            $sort = $ch_storePayWay->sort; //旧的位置
            if ((int)$sort == (int)$new_sort) {
                return json_encode(['status' => 2, 'message' => '位置没有任何改动']);
            }

            $old_StorePayWay = StorePayWay::where('sort', $new_sort)
                ->where('store_id', $store_id)
                ->where('ways_source', $ways_source)
                ->first();

            //开启事务
            try {
                DB::beginTransaction();

                //先零时配置一个
                $old_StorePayWay->update([
                    'sort' => 100,
                ]);

                $ch_storePayWay->update([
                    'sort' => $new_sort,
                ]);
                $ch_storePayWay->save();
                $old_StorePayWay->save();

                //修正
                $old_StorePayWay->update([
                    'sort' => $sort,
                ]);
                $old_StorePayWay->save();

                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                return json_encode(['status' => 2, 'message' => $e->getMessage()]);
            }

            return json_encode(['status' => 1, 'message' => '顺序修改成功']);
        } catch (\Exception $exception) {

            return json_encode(['status' => 0, 'message' => $exception->getMessage()]);
        }
    }


    public function pay_ways_open(Request $request)
    {
        try {
            $merchant = $this->parseToken();//
            $is_open = $request->get('is_open', '');
            $store_id = $request->get('store_id', '');
            $merchant_id = $merchant->merchant_id;
            $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)
                ->orderBy('created_at', 'asc')
                ->first();
            if ($MerchantStore) {
                $store_id = $MerchantStore->store_id;
            }
            $store_pay_ways_open = 0;
            $store = Store::where('store_id', $store_id)->first();
            if ($store) {
                $store_pay_ways_open = $store->store_pay_ways_open;
            }
            if ($is_open == "") {
                return json_encode([
                    'status' => 1,
                    'message' => '数据返回成功',
                    'data' => [
                        'store_pay_ways_open' => $store_pay_ways_open,
                    ],
                ]);
            } else {
                $store->update([
                    'store_pay_ways_open' => $is_open
                ]);

                return json_encode([
                        'status' => 1,
                        'message' => '修改成功',
                        'data' => [
                            'store_pay_ways_open' => $is_open,
                        ],
                    ]
                );
            }
        } catch (\Exception $exception) {
            return json_encode(['status' => 0, 'message' => $exception->getMessage()]);
        }
    }


    //添加支付密码
    public function add_pay_password(Request $request)
    {
        $rsa = new RsaE();
        try {
            $merchant = $this->parseToken();
            $merchant_id = $merchant->merchant_id;

            //客户端用的我的公钥加密 我用私钥解密
            $data = $rsa->privDecrypt($request->get('sign'));//解密
            parse_str($data, $output);
            $pay_password = isset($output['pay_password']) ? $output['pay_password'] : "";
            $pay_password_confirmed = isset($output['pay_password_confirmed']) ? $output['pay_password_confirmed'] : "";

            if ($pay_password !== $pay_password_confirmed) {
                return json_encode([
                    'status' => 2,
                    'message' => '两次密码不一致'
                ]);
            }

            if ($pay_password) {
                //验证密码
                if (strlen($pay_password) != 6) {
                    return json_encode([
                        'status' => 2,
                        'message' => '密码长度不符合要求'
                    ]);
                }
                $dataIN = [
                    'pay_password' => bcrypt($pay_password),

                ];
                Merchant::where('id', $merchant_id)->update($dataIN);
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '参数填写不完整'
                ]);
            }

            return json_encode([
                'status' => 1,
                'message' => '支付密码添加成功',
                'data' => []
            ]);


        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //添加登录密码
    public function add_password(Request $request)
    {
        $rsa = new RsaE();
        try {
            $merchant = $this->parseToken();
            $merchant_id = $merchant->merchant_id;

            //客户端用的我的公钥加密 我用私钥解密
            $data = $rsa->privDecrypt($request->get('sign'));//解密
            parse_str($data, $output);
            $pay_password = isset($output['password']) ? $output['password'] : "";
            $pay_password_confirmed = isset($output['password_confirmed']) ? $output['password_confirmed'] : "";

            if ($pay_password !== $pay_password_confirmed) {
                return json_encode([
                    'status' => 2,
                    'message' => '两次密码不一致'
                ]);
            }

            if ($pay_password) {
                //验证密码
                if (strlen($pay_password) != 6) {
                    return json_encode([
                        'status' => 2,
                        'message' => '密码长度不符合要求'
                    ]);
                }
                $dataIN = [
                    'password' => bcrypt($pay_password),

                ];
                Merchant::where('id', $merchant_id)->update($dataIN);
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '参数填写不完整'
                ]);
            }

            return json_encode([
                'status' => 1,
                'message' => '支付密码添加成功',
                'data' => []
            ]);


        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //修改支付密码
    public function edit_pay_password(Request $request)
    {
        $rsa = new RsaE();
        try {
            $data = $rsa->privDecrypt($request->get('sign'));//解密
            parse_str($data, $output);
            $merchant = $this->parseToken();
            $merchant_id = $merchant->merchant_id;

            $oldpassword = isset($output['old_pay_password']) ? $output['old_pay_password'] : "";
            $newpassword = isset($output['new_pay_password']) ? $output['new_pay_password'] : "";

            if ($oldpassword && $newpassword == "") {
                $local = Merchant::where('id', $merchant_id)->first();
                if (!Hash::check($oldpassword, $local->pay_password)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '旧的支付密码不匹配'
                    ]);
                } else {
                    return json_encode([
                        'status' => 1,
                        'message' => '旧的支付密码匹配'
                    ]);
                }
            }

            if (strlen($newpassword) != 6) {
                return json_encode([
                    'status' => 2,
                    'msg' => '密码长度不符合要求'
                ]);
            }
            $dataIN = [
                'pay_password' => bcrypt($newpassword),

            ];

            Merchant::where('id', $merchant_id)->update($dataIN);

            return json_encode([
                'status' => 1,
                'message' => '支付密码修改成功',
                'data' => []
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //忘记支付密码
    public function forget_pay_password(Request $request)
    {
        $rsa = new RsaE();
        try {
            $merchant = $this->parseToken();
            $merchant_id = $merchant->merchant_id;
            //客户端用的我的公钥加密 我用私钥解密
            $data = $rsa->privDecrypt($request->get('sign'));//解密
            parse_str($data, $output);
            $newpassword = isset($output['new_pay_password']) ? $output['new_pay_password'] : "";
            $code = isset($output['code']) ? $output['code'] : "";
            $msn_local = Cache::get($merchant->phone . 'editpassword-2');

            //验证验证码
            if ($code != "" && $newpassword == "") {
                if ($code != $msn_local) {
                    return json_encode([
                        'status' => 2,
                        'message' => '短信验证码不匹配'
                    ]);
                } else {
                    return json_encode([
                        'status' => 1,
                        'message' => '短信验证码正确'
                    ]);
                }
            }

            //验证密码
            if (strlen($newpassword) < 6) {
                return json_encode([
                    'status' => 2,
                    'message' => '密码长度不符合要求'
                ]);
            }


            $Merchant = Merchant::where('id', $merchant_id)->first();

            //验证验证码
            $msn_local = Cache::get($Merchant->phone . 'editpassword-2');
            if ((string)$code != (string)$msn_local) {
                return json_encode([
                    'status' => 2,
                    'message' => '短信验证码不匹配'
                ]);
            }

            $Merchant->update(['pay_password' => bcrypt($newpassword)]);
            $Merchant->save();

            return json_encode([
                'status' => 1,
                'message' => '支付密码修改成功',
                'data' => [],
            ]);


        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }


    //校验支付密码
    public function check_pay_password(Request $request)
    {
        $rsa = new RsaE();
        try {
            $merchant = $this->parseToken();
            $merchant_id = $merchant->merchant_id;
            $data = $rsa->privDecrypt($request->get('sign'));//解密
            parse_str($data, $output);

            //收银员
            if ($merchant->merchant_type == 2) {
                return json_encode([
                    'status' => 2,
                    'message' => '收银员没有退款权限'
                ]);
            }

            $pay_password = isset($output['pay_password']) ? $output['pay_password'] : "";
            if (strlen($pay_password) != 6) {
                return json_encode([
                    'status' => 0,
                    'message' => '密码长度不符合要求'
                ]);
            }

            $local_pay_password = Merchant::where('id', $merchant_id)->first();
            if ($local_pay_password && $local_pay_password->pay_password) {
                if (Hash::check($pay_password, $local_pay_password->pay_password)) {
                    return json_encode([
                        'status' => 1,
                        'message' => '支付密码匹配'
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '支付密码不匹配'
                    ]);
                }
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '账号未设置支付密码'
                ]);
            }
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //检测是否设置过支付密码
    public function is_pay_password(Request $request)
    {
        $merchant = $this->parseToken();
        $merchant_id = $merchant->merchant_id;
        $Merchant = Merchant::where('id', $merchant_id)->first();

        if ($Merchant->pay_password) {

            $is_pay_password = 1;
        } else {
            $is_pay_password = 0;
        }

        return json_encode([
            'status' => 1,
            'data' => [
                'is_pay_password' => $is_pay_password,
            ]
        ]);
    }


    //检测是否设置过登录密码
    public function is_password(Request $request)
    {
        $merchant = $this->parseToken();
        $merchant_id = $merchant->merchant_id;
        $Merchant = Merchant::where('id', $merchant_id)->first();
        if (isset($Merchant->password)) {
            $is_pay_password = 1;
        } else {
            $is_pay_password = 0;
        }

        return json_encode([
            'status' => 1,
            'data' => [
                'is_password' => $is_pay_password,
            ]
        ]);
    }


    //扫一扫
    public function bind_store_qr(Request $request)
    {
        try {
            $store_id = $request->get('store_id', '');
            $code = $request->get('code', '');
            $code_number = $request->get('code_number', $code);
            $merchant_id = $request->get('merchant_id', '');

            $is_qr = substr($code_number, 0, 4);

            if ($is_qr == "http") {
                $url = basename($code_number);//获取链接
                $data = $this->getParams($url);
                $code_number = $data['no'];
            }
            $storePayWay = StorePayWay::where('store_id', $store_id)
                ->where('status', 1)
                ->select('status')->get();
            if (!$storePayWay) {
                return json_encode(['status' => -1, 'message' => '商户未完成入网']);
            }
            $QrListInfo = QrListInfo::where('code_num', $code_number)->first();

            if (!$QrListInfo) {
                return json_encode(['status' => 2, 'message' => '二维码不存在']);
            }

            $store = Store::where('store_id', $store_id)
                ->select('id', 'merchant_id', 'user_id', 'store_name', 'store_type')
                ->first();
            if (!$store) {
                return json_encode(['status' => 2, 'message' => '门店ID不存在']);
            }

            if ($QrListInfo->status) {
                $store_id = $QrListInfo->store_id;
                return json_encode(['status' => 2, 'message' => '二维码已经被' . $store_id . '绑定']);
            }
            if (!$merchant_id || $merchant_id == "NULL") {
                $merchant_id = $store->merchant_id;
            }
            $dataInfo['status'] = 1;
            $dataInfo['status_desc'] = '已绑定';
            $dataInfo['merchant_id'] = $merchant_id;
            $dataInfo['bind_time'] = date('Y-m-d H:i:m', time());
            $dataInfo['store_id'] = $store_id;
            //开启事务
            try {
                QrListInfo::where('code_num', $code_number)->update($dataInfo);
                $QrList = QrList::where('cno', $QrListInfo->cno)->first();
                $s_num = $QrList->s_num;
                $QrList->s_num = $s_num + 1;
                $QrList->save();
                return json_encode(['status' => 1, 'message' => '绑定收款二维码成功']);

            } catch (\Exception $e) {
                Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
                DB::rollBack();
                return json_encode([
                    'status' => '2',
                    'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
                ]);
            }


        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }

    // 空二维码绑定小程序
    public function bind_applet_store_qr(Request $request)
    {
        $code = $request->get('code');
        $store_id = $request->get('store_id', '');
        $merchant_id = $request->get('merchant_id', '');
        $is_qr = substr($code, 0, 4);

        $check_data = [
            'store_id' => '门店ID',
            'code' => '码编号',
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            return json_encode([
                'status' => 2,
                'message' => $check
            ]);
        }

        //绑定空码
        if ($is_qr == "http") {
            $url = basename($code);//获取链接
            $data = $this->getParams($url);
            $code = $data['no'];
            $QrListInfo = QrListInfo::where('code_number', $code)->first();

            // 空二维码已经绑定的小程序数量
            $bindAppletNum = QrListInfo::where('cno', $QrListInfo->cno)->where('type', 2)->groupBy('cno')->count();
            if ($bindAppletNum >= 100) {
                return $this->sys_response(4000, '绑定数量不得大于100');
            }

            if ($QrListInfo) {
                //已经绑定支付码
                if ($QrListInfo->bind_ali_type == 2 && $QrListInfo->bind_wechat_type == 2) {
                    return json_encode(['status' => 2, 'message' => '二维码已经被其他店铺绑定！请更换']);
                } else {
                    if ($store_id == "") {
                        return json_encode(['status' => 2, 'message' => '请先开通门店']);
                    }
                    //未绑定
                    $datainfo = $QrListInfo->toArray();
                    $datainfo['store_id'] = $store_id;
                    $datainfo['code_type'] = 1;
                    $datainfo['merchant_id'] = $merchant_id;

                    // 创建扫普通码打开小程序二维码规则
                    $input['store_id'] = $store_id;
                    $merchantStoreModel = new MerchantStoreAppidSecret();
                    $merchantStoreInfo = $merchantStoreModel->getStoreAppIdSecret($input['store_id']);
                    if (!$merchantStoreInfo) {
                        return $this->sys_response(40000, '门店对应小程序信息未配置，请完善');
                    }
                    $input['wechat_appid'] = $merchantStoreInfo->wechat_appid ?? '';
                    $input['wechat_secret'] = $merchantStoreInfo->wechat_secret ?? '';

                    if ($QrListInfo->bind_wechat_type != 2) {
                        // 小程序页面
                        $input['pages_url'] = $this->wechat_pages_url;
                        // 二维码地址
                        if (env('APP_URL') == 'https://pay.jiangsuniuhonghong.cn') {
                            $input['prefix'] = env('APP_URL') . "/qr?no=" . $code;
                        } else {
                            $input['prefix'] = env('APP_URL') . "/applet/qr?no=" . $code;
                        }
                        $input['app_id'] = $this->app_id;

                        $controller = new StoreWechatTicketController();
                        $wechatRes = $controller->createQrcodeRule($input);
                        $wechatResJson = json_decode($wechatRes, true);
                        if ($wechatResJson['errcode'] != 0) {
                            return $this->sys_response(202, '绑定微信小程序失败');
                        }

                        // 更新绑定微信状态
                        $QrListInfo->update(
                            [
                                'code_type' => 1,
                                'bind_wechat_type' => 2,
                                'store_id' => $store_id,
                                'type' => '2',
                            ]
                        );
                        $QrListInfo->save();
                    }

                    if ($QrListInfo->bind_ali_type != 2) {
                        // 支付宝参数
                        $mini_app_id = $merchantStoreInfo->alipay_appid ?? '';
                        $ali_data['page_redirection'] = $this->wechat_pages_url;
                        $appletsInfo = DB::table("customerapplets_authorize_appids")->where(['applet_type' => 2,'AuthorizerAppid' => $mini_app_id])->first();
                        if (empty($appletsInfo)) {
                            return $this->sys_response(202, "该支付宝小程序暂未授权");
                        }
                        $ali_data['app_auth_token'] = $appletsInfo->AuthorizationCode;
                        $ali_data['route_url'] = $request->get('code');

                        // 创建支付宝普通二维码规则
                        $appletsController = new AppletsController();
                        $aliQrcodeBind = $appletsController->qrcodeBind($ali_data);
                        $aliQrcodeBindJson = json_decode($aliQrcodeBind, true);
                        if ($aliQrcodeBindJson['status'] != 1) {
                            return $this->sys_response($aliQrcodeBindJson['status'], '支付宝：' . $aliQrcodeBindJson['message']);
                        }
                        // 更新绑定支付宝状态
                        $QrListInfo->update(
                            [
                                'code_type' => 1,
                                'bind_ali_type' => 2,
                                'store_id' => $store_id,
                                'type' => '2',
                            ]
                        );
                        $QrListInfo->save();
                    }

                    //开启事务
                    try {
                        DB::beginTransaction();
                        QrPayInfo::create($datainfo);

                        //已经使用加 1
                        $QrList = QrList::where('cno', $QrListInfo->cno)->first();
                        $s_num = $QrList->s_num;
                        $QrList->s_num = $s_num + 1;
                        $QrList->save();

                        DB::commit();
                    } catch (\Exception $e) {
                        DB::rollBack();
                    }
                    return json_encode(['status' => 1, 'message' => '绑定收款二维码成功']);
                }
            } else {

                //空码不存在
                if ($store_id == "") {
                    return json_encode(['status' => 2, 'message' => '请先开通门店']);
                }

                //未绑定
                $datainfo = [
                    'user_id' => '1',
                    'code_number' => $code,
                    'code_type' => 1,
                    'store_id' => $store_id,
                    'type' => '2',
                    'cno' => '1',
                ];
                //开启事务
                try {
                    DB::beginTransaction();
                    QrListInfo::create($datainfo);
                    $datainfo['merchant_id'] = $merchant_id;
                    QrPayInfo::create($datainfo);
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollBack();
                }
                return json_encode(['status' => 1, 'message' => '绑定收款二维码成功']);
            }

        }

        return json_encode(['status' => 2, 'message' => '不识别的二维码']);
    }


    public function getParams($url)
    {

        $refer_url = parse_url($url);
        $params = $refer_url['query'];

        $arr = array();
        if (!empty($params)) {
            $paramsArr = explode('&', $params);

            foreach ($paramsArr as $k => $v) {
                $a = explode('=', $v);
                $arr[$a[0]] = $a[1];
            }
        }
        return $arr;
    }


    /**
     * 达达配送设置or获取
     * @param Request $request
     * @return string
     */
    public function dada_delivery_setting(Request $request)
    {
        $token = $this->parseToken();
        $merchant_id = $token->merchant_id;
        $config_id = $token->config_id;
        $merchant_type = $token->merchant_type;

        $config_id = $request->post('config_id', $config_id);
        $app_key = $request->post('appKey', '');
        $app_secret = $request->post('appSecret', '');
        $source_id = $request->post('sourceId', '');
        $type = $request->post('type', ''); //有值为更新or新建

        try {
            if ($type) {
                $check_data = [
                    'appKey' => '应用Key',
                    'appSecret' => '应用Secret',
                    'sourceId' => '商户编号'
                ];
                $check = $this->check_required($request->except(['token']), $check_data);
                if ($check) {
                    return json_encode([
                        'status' => 2,
                        'message' => $check
                    ]);
                }

                $insert_dada = [];
                $insert_obj = DadaConfig::create($insert_dada);

            } else {
                $dada_config_obj = DadaConfig::where('config_id', $config_id)->first();
                if (!$dada_config_obj) {
                    $merchant_obj = Merchant::find($merchant_id);
                    if ($merchant_obj) {
                        $merchant_pid = $merchant_obj->pid;
                        if ($merchant_pid) {
                            $pid_merchant_obj = Merchant::find($merchant_pid);
                            if ($pid_merchant_obj) {
                                $dada_config_obj = DadaConfig::where('config_id', $pid_merchant_obj->config_id)->first();
                            }
                        } else {
                            $dada_config_obj = DadaConfig::where('config_id', '1234')->first();
                        }
                    }
                }

                if ($dada_config_obj) {
                    $this->status = 1;
                    $this->message = '查询成功';
                    return $this->format($dada_config_obj);
                } else {
                    $this->status = 2;
                    $this->message = '查询失败';
                    return $this->format();
                }
            }

        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }

    public function nonce_str($length = 32){
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $str ="";
        for ( $i = 0; $i < $length; $i++ )  {
            $str.= substr($chars, mt_rand(0, strlen($chars)-1), 1);
        }
        return $str;
    }

    //上传证件图片
    public function test_luyu(Request $request){
        $url = "https://api.mch.weixin.qq.com/v3/merchant/media/upload";//微信的上传地址

        $input = $request->all();
        $path = $input['path'];

        $merchant_id        = 1552612291;         //商户号
        $serial_no          = '285014E37CFCE2C0449668070846492AF520FA37';      //商户证书序列号
        // $mch_private_key = $this->getPrivateKey($input['apiclient_key']);//读取商户api证书公钥 getPublicKey()获取方法
        $mch_private_key = openssl_get_privatekey(file_get_contents('upload/images/158952122021608112618.pem'));
        $timestamp = time();//时间戳
        $nonce = $this->nonce_str();//随机字符串
        $fi = new \finfo(FILEINFO_MIME_TYPE);
        $mime_type=$fi->file($path);
        $meta=[
            'filename'=> 'luyu_pic.png',
            'sha256'=>hash_file('sha256',$path)
        ];
        $sign = $this->sign($url,'POST',$timestamp,$nonce,json_encode($meta),$mch_private_key,$merchant_id,$serial_no);
        $boundary = uniqid();
        $header=[
            'Content-Type:multipart/form-data;boundary=' . $boundary,
            'Authorization:WECHATPAY2-SHA256-RSA2048 ' . $sign,
            'Accept:application/json',
            'User-Agent:' . $merchant_id
        ];
        $body='--' . $boundary . "\r\n";
        $body.='Content-Disposition:form-data; name="meta"' . "\r\n";
        $body.='Content-Type:application/json' . "\r\n\r\n";
        $body.=json_encode($meta)."\r\n";
        $body.='--' . $boundary . "\r\n";
        $body.='Content-Disposition:form-data;name="file";filename="' . $meta['filename'] . '"' . "\r\n";
        $body.='Content-Type:' .$mime_type."\r\n\r\n";
        $body.=file_get_contents($path)."\r\n";
        $body.='--'.$boundary .'--'."\r\n";
        $result=$this->curl_post($url,$body,$header);
        $result=json_decode($result,true);
        return $result;//返回微信返回来的图片标识
    }

    //签名
    public function sign($url,$http_method,$timestamp,$nonce,$body,$mch_private_key,$merchant_id,$serial_no){
        $url_parts=parse_url($url);
        $canonical_url=($url_parts['path'].(!empty($url_parts['query'])?"?${url_parts['query']}":""));
        $message=
            $http_method . "\n".
            $canonical_url . "\n".
            $timestamp . "\n".
            $nonce . "\n".
            $body . "\n";
        openssl_sign($message,$raw_sign,$mch_private_key,'sha256WithRSAEncryption');
        $sign=base64_encode($raw_sign);
        $schema='WECHATPAY2-SHA256-RSA2048';
        $token=sprintf(
            'mchid="%s",nonce_str="%s",signature="%s",timestamp="%d",serial_no="%s"',
            $merchant_id,
            $nonce,
            $sign,
            $timestamp,
            $serial_no
        );
        return $token;
    }

    //curl提交
    public function curl_post($url,$data=[],$header,$method='POST'){
        $curl=curl_init();
        curl_setopt($curl,CURLOPT_URL,$url);
        curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
        curl_setopt($curl,CURLOPT_HEADER,false);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,false);
        if($method=="POST"){
            curl_setopt($curl,CURLOPT_POST,TRUE);
            curl_setopt($curl,CURLOPT_POSTFIELDS,$data);
        }
        $result=curl_exec($curl);
        curl_close($curl);
        return $result;
    }


}
