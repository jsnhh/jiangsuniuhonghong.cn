<?php

namespace App\Api\Controllers\Merchant;


use App\Api\Controllers\AliRuyi\RuyiPayController;
use App\Api\Controllers\Basequery\StorePayWaysController;
use App\Api\Controllers\BaseController;
use App\Api\Controllers\Config\TfConfigController;
use App\Api\Controllers\Config\WeixinConfigController;
use App\Api\Controllers\Device\YlianyunAopClient;
use App\Api\Controllers\MyBank\MyBankController;
use App\Models\AlipayAntStores;
use App\Models\AlipayAppOauthUsers;
use App\Models\AlipayOperation;
use App\Models\CustomerAppletsUserOrderGoods;
use App\Models\CustomerAppletsUserOrders;
use App\Models\Device;
use App\Models\Goods;
use App\Models\Merchant;
use App\Models\MerchantDevice;
use App\Models\MerchantStore;
use App\Models\MerchantStoreAppidSecret;
use App\Models\MyBankCategory;
use App\Models\MyBankStore;
use App\Models\Order;
use App\Models\ProvinceCity;
use App\Models\QrListInfo;
use App\Models\Shop;
use App\Models\Store;
use App\Models\StoreBank;
use App\Models\StoreDeliverSetting;
use App\Models\StoreImg;
use App\Models\StorePayWay;
use App\Models\TfConfig;
use App\Models\User;
use App\Models\UserRate;
use App\Models\VConfig;
use App\Models\WeixinNotify;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\Image;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use EasyWeChat\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use PhpParser\Node\Scalar\String_;

class StoreController extends BaseController
{

    public function check_store(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $merchant_id = $merchant->merchant_id;
            $merchant_store = MerchantStore::where('merchant_id', $merchant_id)
                ->orderBy('created_at', 'asc')
                ->first();
            $store_id = $request->get('store_id', '');
            if ($store_id == "" && $merchant_store) {
                $store_id = $merchant_store->store_id;
            }
            $is_store = 0;
            $is_wx_open_id = 0;
            $is_email = 0;
            $is_pay_ways = 0;

            $Store = Store::where('store_id', $store_id)
                ->select('head_sfz_no')
                ->first();
            //
            if ($Store && $Store->head_sfz_no) {
                $StoreBank = StoreBank::where('store_id', $store_id)
                    ->select('store_bank_no')
                    ->first();
                if ($StoreBank && $StoreBank->store_bank_no) {
                    $is_store = 1;
                }
            }
            $merchants = Merchant::where('id', $merchant->merchant_id)->first();
            if (!$merchants) {
                return json_encode(['status' => 2, 'message' => '商户不存在']);

            }
            if ($merchants->wx_openid) {
                $is_wx_open_id = 1;
            }

            if ($merchants->email) {
                $is_email = 1;
            }

            $StorePayWay = StorePayWay::where('store_id', $store_id)
                ->where('status', 1)
                ->select('id')->first();

            if ($StorePayWay) {
                $is_pay_ways = 1;
            }

            $data = [
                'is_store' => $is_store,
                'is_wx_open_id' => $is_wx_open_id,
                'is_email' => $is_email,
                'is_pay_ways' => $is_pay_ways,
            ];
            $this->status = 1;
            $this->message = '数据返回成功';

            return $this->format($data);

        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage() . '-' . $exception->getLine();
            return $this->format();
        }
    }


    /**
     * 门店信息查看
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $merchant_id = $merchant->merchant_id;
            if ($merchant->merchant_type == 2) {
                return json_encode([
                    'status' => 2,
                    'message' => '收银员没有权限'
                ]);
            }

            $merchant_store = MerchantStore::where('merchant_id', $merchant_id)
                ->orderBy('created_at', 'asc')
                ->first();
            $store_id = $request->get('store_id', '');
            $type = $request->get('type', '');

            if ("" . $store_id . "" == "" && $merchant_store) {
                $store_id = $merchant_store->store_id;
            }

            $store = Store::where('store_id', $store_id)->first();
            $store_bank = StoreBank::where('store_id', $store_id)->first();
            $store_img = StoreImg::where('store_id', $store_id)->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '平台门店ID不正确'
                ]);

            }
            $store_alipay_account = '';
            $AlipayAppOauthUsers = AlipayAppOauthUsers::where('store_id', $store_id)
                ->select('alipay_user_account')
                ->first();

            if ($AlipayAppOauthUsers) {
                $store_alipay_account = $AlipayAppOauthUsers->alipay_user_account;
            }

            if ($type == "") {
                $type = 'head_info,store_info,account_info,license_info';
            }
            $type_array = explode(",", $type);
            foreach ($type_array as $k => $v) {
                if ($v == 'head_info') {
                    $data = [
                        'head_name' => $store->head_name,//法人
                        'head_sfz_no' => $store->head_sfz_no,
                        'head_sfz_img_a' => $store_img->head_sfz_img_a,
                        'head_sfz_img_b' => $store_img->head_sfz_img_b,
                        'people' => $store->people,
                        'people_phone' => $store->people_phone,
                        'head_sfz_time' => $store->head_sfz_time,
                        'head_sfz_stime' => $store->head_sfz_stime,
                    ];

                    $data_r[$v] = $data;
                }

                if ($v == 'store_info') {
                    $data = [
                        'people' => $store->people,
                        'people_phone' => $store->people_phone,
                        'store_name' => $store->store_name,
                        'province_code' => $store->province_code,
                        'city_code' => $store->city_code,
                        'area_code' => $store->area_code,
                        'province_name' => $store->province_name,
                        'city_name' => $store->city_name,
                        'area_name' => $store->area_name,
                        'store_address' => $store->store_address,
                        'store_type' => $store->store_type,
                        'store_type_name' => $store->store_type_name,
                        'category_id' => $store->category_id,
                        'category_name' => $store->category_name,
                        'store_logo_img' => $store_img->store_logo_img,
                        'store_img_a' => $store_img->store_img_a,
                        'store_img_b' => $store_img->store_img_b,
                        'store_img_c' => $store_img->store_img_c,
                        'store_email' => $store->store_email,
                        'alipay_name' => $store->alipay_name,
                        'alipay_account' => $store->alipay_account,
                        'source' => $store->source
                    ];

                    $data_r[$v] = $data;
                }


                if ($v == 'account_info') {
                    $data = [
                        'store_alipay_account' => $store_alipay_account,
                        'store_bank_no' => $store_bank->store_bank_no,
                        'store_bank_phone' => $store_bank->store_bank_phone,
                        'store_bank_name' => $store_bank->store_bank_name,
                        'store_bank_type' => $store_bank->store_bank_type,
                        'bank_name' => $store_bank->bank_name,
                        'bank_no' => $store_bank->bank_no,
                        'sub_bank_name' => $store_bank->sub_bank_name,
                        'bank_province_code' => $store_bank->bank_province_code,
                        'bank_city_code' => $store_bank->bank_city_code,
                        'bank_area_code' => $store_bank->bank_area_code,
                        'bank_province_name' => $this->city_name($store_bank->bank_province_code),
                        'bank_city_name' => $this->city_name($store_bank->bank_city_code),
                        'bank_area_name' => $this->city_name($store_bank->bank_area_code),
                        'bank_img_a' => $store_img->bank_img_a,
                        'bank_img_b' => $store_img->bank_img_b,
                        'bank_sfz_img_a' => $store_img->bank_sfz_img_a,
                        'bank_sfz_img_b' => $store_img->bank_sfz_img_b,
                        'bank_sc_img' => $store_img->bank_sc_img,
                        'store_auth_bank_img' => $store_img->store_auth_bank_img,
                        'bank_sfz_time' => $store_bank->bank_sfz_time,
                        'bank_sfz_stime' => $store_bank->bank_sfz_stime,
                        'bank_sfz_no' => $store_bank->bank_sfz_no,
                        'weixin_name' => $store->weixin_name,
                        'weixin_no' => $store->weixin_no,
                        'alipay_name' => $store->alipay_name,
                        'alipay_account' => $store->alipay_account,
                        'store_email' => $store->store_email,
                    ];

                    $data_r[$v] = $data;
                }

                if ($v == 'license_info') {
                    $data = [
                        'store_license_no' => $store->store_license_no,
                        'store_license_time' => $store->store_license_time,
                        'store_license_stime' => $store->store_license_stime,
                        'store_license_img' => $store_img->store_license_img,
                        'store_other_img_a' => $store_img->store_other_img_a,
                        'store_other_img_b' => $store_img->store_other_img_b,
                        'store_other_img_c' => $store_img->store_other_img_c,
                        'store_industrylicense_img' => $store_img->store_industrylicense_img,
                        'head_sc_img' => $store_img->head_sc_img,
                        'head_store_img' => $store_img->head_store_img,

                    ];

                    $data_r[$v] = $data;
                }
            }

            $this->status = 1;
            $this->message = '成功';
            return $this->format($data_r);

        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage() . '-' . $exception->getLine();
            return $this->format();
        }
    }


    //城市名称
    public function city_name($city_code)
    {
        $city_name = "";
        $data = ProvinceCity::where('area_code', $city_code)->first();
        if ($data) {
            $city_name = $data->area_name;
        }

        return $city_name;

    }


    /**
     * 门店认证修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function add_store(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $data = $request->except(['token']);
            $merchant_id = $merchant->merchant_id;
            $phone = $merchant->phone;
            $name = $merchant->merchant_name;
            $store_id = $request->get('store_id', '');

            if ($merchant->merchant_type == 2) {
                return json_encode([
                    'status' => 2,
                    'message' => '收银员没有权限'
                ]);
            }
            if (!isset($store_id) || $store_id == "") {
                $merchant_store = MerchantStore::where('merchant_id', $merchant_id)
                    ->orderBy('created_at', 'asc')
                    ->first();
                $store_id = $merchant->created_store_no;//预创建编号
                if ($merchant_store) {
                    $store_id = $merchant_store->store_id;
                }
            }

            //法人信息
            $head_name = $request->get('head_name', '');
            $people = $request->get('people', $head_name);
            $head_sfz_no = $request->get('head_sfz_no', '');
            $people_phone = $request->get('people_phone', $phone);
            $bank_sfz_no = $request->get('bank_sfz_no', '');

            $head_sfz_img_a = $request->get('head_sfz_img_a', '');
            $head_sfz_img_b = $request->get('head_sfz_img_b', '');

            $head_sfz_time = $request->get('head_sfz_time', '');
            $head_sfz_stime = $request->get('head_sfz_stime', '');
            $head_sfz_time = $this->time($head_sfz_time);
            $head_sfz_stime = $this->time($head_sfz_stime);

            $bank_sfz_time = $request->get('bank_sfz_time', '');
            $bank_sfz_stime = $request->get('bank_sfz_stime', '');
            $bank_sfz_time = $this->time($bank_sfz_time);
            $bank_sfz_stime = $this->time($bank_sfz_stime);


            //门店信息
            $store_name = $request->get('store_name', '');
            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');
            $store_address = $request->get('store_address', '');
            $store_type = $request->get('store_type', '');
            $store_type_name = $request->get('store_type_name', '');

            $category_id = $request->get('category_id', '');
            $category_name = $request->get('category_name', '');
            $store_logo_img = $request->get('store_logo_img', '');
            $store_img_a = $request->get('store_img_a', '');
            $store_img_b = $request->get('store_img_b', '');
            $store_img_c = $request->get('store_img_c', '');
            $weixin_name = $request->get('weixin_name', '');
            $weixin_no = $request->get('weixin_no', '');

            $alipay_name = $request->get('alipay_name', '');
            $alipay_account = $request->get('alipay_account', '');
            $store_email = $request->get('store_email', '');


            //收款信息
            $store_alipay_account = $request->get('store_alipay_account', '');
            $store_bank_no = $request->get('store_bank_no', '');
            $store_bank_phone = $request->get('store_bank_phone', '');
            $store_bank_name = $request->get('store_bank_name', '');
            $store_bank_type = $request->get('store_bank_type', '');
            $bank_name = $request->get('bank_name', '');
            $bank_no = $request->get('bank_no', '');
            $sub_bank_name = $request->get('sub_bank_name', '');
            $bank_province_code = $request->get('bank_province_code', '');
            $bank_city_code = $request->get('bank_city_code', '');
            $bank_area_code = $request->get('bank_area_code', '');
            $bank_img_a = $request->get('bank_img_a', '');
            $bank_img_b = $request->get('bank_img_b', '');

            $bank_sfz_img_a = $request->get('bank_sfz_img_a', '');
            $bank_sfz_img_b = $request->get('bank_sfz_img_b', '');
            $bank_sc_img = $request->get('bank_sc_img', '');

            $store_auth_bank_img = $request->get('store_auth_bank_img', '');


            //证照信息
            $store_license_no = $request->get('store_license_no', '');
            $store_license_time = $request->get('store_license_time', '');
            $store_license_stime = $request->get('store_license_stime', '');

            $head_sc_img = $request->get('head_sc_img', '');
            $head_store_img = $request->get('head_store_img', '');

            $store_license_img = $request->get('store_license_img', '');
            $store_industrylicense_img = $request->get('store_industrylicense_img', '');
            $store_other_img_a = $request->get('store_other_img_a', '');
            $store_other_img_b = $request->get('store_other_img_b', '');
            $store_other_img_c = $request->get('store_other_img_c', '');

            //拼装门店信息
            $stores = [
                'config_id' => $merchant->config_id,
                'user_id' => $merchant->user_id,
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'store_name' => $store_name,
                'store_type' => $store_type,
                'store_type_name' => $store_type_name,
                'store_email' => $store_email,
                'store_short_name' => $store_name,
                'people' => $people,//负责人
                'people_phone' => $people_phone,
                'province_code' => $province_code,
                'city_code' => $city_code,
                'area_code' => $area_code,
                'store_address' => $store_address,
                'head_name' => $head_name,//法人
                'head_sfz_no' => $head_sfz_no,
                'head_sfz_time' => $head_sfz_time,
                'head_sfz_stime' => $head_sfz_stime,
                'category_id' => $category_id,
                'category_name' => $category_name,
                'store_license_no' => $store_license_no,
                'store_license_time' => $store_license_time,
                'store_license_stime' => $store_license_stime,
                'weixin_name' => isset($weixin_name) ? $weixin_name : "",
                'weixin_no' => isset($weixin_no) ? $weixin_no : "",
                'alipay_name' => isset($alipay_name) ? $alipay_name : "",
                'alipay_account' => isset($alipay_account) ? $alipay_account : "",
                'source' => $merchant->source  //来源,01:畅立收，02:河南畅立收
            ];
            if ($province_code) {
                $stores['province_name'] = $this->city_name($province_code);
            }
            if ($city_code) {
                $stores['city_name'] = $this->city_name($city_code);
            }
            if ($area_code) {
                $stores['area_name'] = $this->city_name($area_code);
            }


            //图片信息
            $store_imgs = [
                'store_id' => $store_id,
                'head_sfz_img_a' => $head_sfz_img_a,
                'head_sfz_img_b' => $head_sfz_img_b,
                'store_license_img' => $store_license_img,
                'store_industrylicense_img' => $store_industrylicense_img,
                'store_logo_img' => $store_logo_img,
                'store_img_a' => $store_img_a,
                'store_img_b' => $store_img_b,
                'store_img_c' => $store_img_c,
                'bank_img_a' => $bank_img_a,
                'bank_img_b' => $bank_img_b,
                'store_other_img_a' => $store_other_img_a,
                'store_other_img_b' => $store_other_img_b,
                'store_other_img_c' => $store_other_img_c,
                'head_sc_img' => $head_sc_img,
                'head_store_img' => $head_store_img,
                'bank_sfz_img_a' => $bank_sfz_img_a,
                'bank_sfz_img_b' => $bank_sfz_img_b,
                'bank_sc_img' => $bank_sc_img,
                'store_auth_bank_img' => $store_auth_bank_img,
            ];

            //银行卡信息
            $store_banks = [
                'store_id' => $store_id,
                'store_bank_no' => $store_bank_no,
                'store_bank_name' => $store_bank_name,
                'store_bank_phone' => $store_bank_phone,
                'store_bank_type' => isset($store_bank_type) ? $store_bank_type : "01",
                'bank_name' => $bank_name,
                'bank_no' => $bank_no,
                'sub_bank_name' => $sub_bank_name,
                'bank_province_code' => $bank_province_code,
                'bank_city_code' => $bank_city_code,
                'bank_area_code' => $bank_area_code,
                'bank_sfz_no' => $bank_sfz_no,//持卡人身份证
                'bank_sfz_time' => $bank_sfz_time,
                'bank_sfz_stime' => $bank_sfz_stime,
            ];


            $store = Store::where('store_id', $store_id)->first();
            $store_bank = StoreBank::where('store_id', $store_id)->first();
            $store_img = StoreImg::where('store_id', $store_id)->first();

            //开启事务
            try {
                DB::beginTransaction();

                if ($store) {
                    $stores = array_filter($stores, function ($v) {
                        if ($v == "") {
                            return false;
                        } else {
                            return true;
                        }
                    });
                    $store->update($stores);
                    $store->save();
                } else {

                    $rules = [
                        'store_name' => 'unique:stores',
                        'store_short_name' => 'unique:stores',
                    ];
                    $validator = Validator::make($data, $rules);
                    if ($validator->fails()) {
                        return json_encode([
                            'status' => 2,
                            'message' => '门店名称或者简称在已经存在',
                        ]);
                    }

                    $stores['store_type'] = 1;
                    $stores['store_type_name'] = '个体';
                    Store::create($stores);
                    MerchantStore::create([
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id
                    ]);
                }
                if ($store_img) {
                    $store_imgs = array_filter($store_imgs);
                    $store_img->update($store_imgs);
                    $store_img->save();
                } else {
                    StoreImg::create($store_imgs);
                }

                //快钱支付修改资料
                $StorePayWay = StorePayWay::where('store_id', $store_id)
                    ->where('company', 'mybank')
                    ->select('status')
                    ->first();

                if ($StorePayWay && $StorePayWay->status == 1) {
                    $MyBankStore = MyBankStore::where('OutMerchantId', $store_id)
                        ->select('MerchantId')
                        ->first();

                    if ($MyBankStore && $MyBankStore->MerchantId) {
                        //修改银行资料
                        $obj = new MyBankController();


                        if ($store_bank_name && $store_bank_no && $bank_no) {

                            $BankCardParam = [
                                "config_id" => $merchant->config_id,
                                "MerchantId" => $MyBankStore->MerchantId,
                                "BankCertName" => $store_bank_name,//名称
                                "BankCardNo" => $store_bank_no,//银行卡号
                                "AccountType" => $store_bank_type,//账户类型。可选值：01：对私账 02对公账户
                                "BankCode" => $bank_name,//开户行名称
                                "BranchName" => $sub_bank_name,//开户支行名称
                                "ContactLine" => $bank_no,//联航号
                                "BranchProvince" => $bank_province_code,//省编号
                                "BranchCity" => $bank_city_code,//市编号
                                "CertType" => '01',//持卡人证件类型。可选值： 01：身份证
                                "CertNo" => $store_bank->bank_sfz_no ? $store_bank->bank_sfz_no : $store_bank->head_sfz_no,//持卡人证件号码
                                "CardHolderAddress" => $store->store_address,//持卡人地址
                            ];

                            $re = $obj->up_bank($BankCardParam);


                            if ($re['status'] == 2) {
                                return json_encode($re);
                            }
                            $body = $re['data']['document']['response']['body'];
                            if ($body['RespInfo']['ResultStatus'] != 'S') {
                                return json_encode([
                                    'status' => 2,
                                    'message' => '换卡失败：' . $body['RespInfo']['ResultMsg']
                                ]);
                            }
                        }

                    }

                }


                //传化修改资料
                $tfpay = StorePayWay::where('store_id', $store_id)
                    ->where('company', 'tfpay')
                    ->select('status')
                    ->first();

                if ($tfpay && $tfpay->status == 1) {

                    if ($store_bank_name && $store_bank_no && $bank_no) {


                        $config = new TfConfigController();

                        $tf_merchant = $config->tf_merchant($store_id, $store->pid);
                        if (!$tf_merchant) {
                            return json_encode([
                                'status' => 2,
                                'message' => '传化商户不存在请检查'
                            ]);
                        }

                        $tf_config = $config->tf_config($store->config_id, $tf_merchant->qd);

                        if (!$tf_config) {
                            return json_encode([
                                'status' => 2,
                                'message' => '传化配置不存在请检查配置'
                            ]);
                        }

                        //传化报备

                        $tfpay_banks = DB::table('tfpay_banks')
                            ->where('bank_name', $bank_name)
                            ->first();

                        if (!$tfpay_banks) {
                            return json_encode([
                                'status' => 2,
                                'message' => '暂不支持该银行卡进件'
                            ]);
                        }


                        $obj = new \App\Api\Controllers\Tfpay\BaseController();
                        $obj->mch_id = $tf_config->mch_id;
                        $obj->pub_key = $tf_config->pub_key;
                        $obj->pri_key = $tf_config->pri_key;


                        //费率同步
                        $sub_mch_id = $tf_merchant->sub_mch_id;
                        $post_data = [
                            'sub_mch_id' => $sub_mch_id,
                            'type' => $store_bank_type == "02" ? '2' : '1',//1 个人 2 企业
                            'bank_code' => $tfpay_banks->bank_no,
                            'account_name' => $store_bank_name,
                            'account_number' => $store_bank_no,
                            'bank_province_code' => $bank_province_code ? $bank_province_code : $province_code,
                            'bank_city_code' => $bank_city_code ? $bank_city_code : $city_code,
                            'branch_name' => $sub_bank_name,
                        ];

                        $method = '/openapi/merchant/settle-bankcard';
                        $re = $obj->api($post_data, $method, false);
                        if ($re['code'] != 0) {
                            return json_encode([
                                'status' => 2,
                                'message' => $re['msg']
                            ]);
                        }

                    }

                }

                if ($store_bank) {
                    $store_banks = array_filter($store_banks);
                    $store_bank->update($store_banks);
                    $store_bank->save();
                } else {

                    StoreBank::create($store_banks);
                }


                DB::commit();
            } catch (\Exception $e) {
                Log::info($e);
                DB::rollBack();
                return json_encode(['status' => -1, 'message' => $e->getMessage()]);
            }


            return json_encode([
                'status' => 1,
                'message' => '资料添加成功',
                'data' => [
                    'store_id' => $store_id,
                    'pid' => '0',
                ]
            ]);


        } catch (\Exception $exception) {

            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }


    /**
     * 门店类型
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store_type(Request $request)
    {

        $data = [
            [
                'store_type' => 1,
                'store_type_desc' => '门店-个体',
            ], [
                'store_type' => 2,
                'store_type_desc' => '门店-企业',
            ], [
                'store_type' => 3,
                'store_type_desc' => '门店-个人',
            ],
        ];
        $this->status = 1;
        $this->message = '返回数据成功';
        return $this->format($data);

    }


    /**
     * 查询门店经营类目
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store_category(Request $request)
    {

        $data = MyBankCategory::select('category_id', 'category_name')->get();
        $this->status = 1;
        $this->message = '返回数据成功';
        return $this->format($data);

    }


    /**
     * 支付宝授权
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function alipay_auth(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $merchant_id = $merchant->merchant_id;
            if ($merchant->pid != 0) {
                return json_encode(['status' => 2, 'message' => '你没有权限添加支付宝授权']);
            }
            $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)
                ->orderBy('created_at', 'asc')
                ->first();
            if ($MerchantStore) {
                $store_id = $MerchantStore->store_id;
            } else {
                $store_id = $merchant->created_store_no;//预创建编号
            }

            $data = [
                'redirect_url' => 'alipays://platformapi/startapp?appId=20000067&url=' . url('/merchant/appAlipay?store_id=' . $store_id . '&merchant_id=' . $merchant_id . "&config_id=" . $merchant->config_id),
                'qr_url' => url('/merchant/appAlipay?store_id=' . $store_id . '&merchant_id=' . $merchant_id . "&config_id=" . $merchant->config_id)
            ];
            $this->status = 1;
            $this->message = '数据请求成功';
            return $this->format($data);

        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    /**
     * 门店列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store_lists(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $merchant_id = $request->get('merchant_id', '');
            $return_type = $request->get('return_type', '');

            //门店创建者id
            if ($merchant_id == "" || $merchant_id == "NULL") {
                $merchant_id = $merchant->merchant_id;
            }
            //Log::info($merchant_id);
            $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)
                ->select('store_id')
                ->first();

            $Store = Store::where('store_id', $MerchantStore->store_id)->select('id')->first();
            $stores = Store::where('pid', $Store->id)->select('store_id')->get();
            //Log::info($stores);
            if ($stores->isEmpty()) {
                $stores = [];

            } else {
                $stores = $stores->toArray();
            }
            array_push($stores, ['store_id' => $MerchantStore->store_id]);
            //Log::info($stores);

            $obj = DB::table('stores');

            $obj = $obj->leftjoin('merchants', 'merchants.id', 'stores.merchant_id');
            //查询 pid =0列表
            if ($return_type == '0') {
                $obj->whereIn('stores.store_id', $stores)
                    ->where('stores.pid', 0)
                    ->select('stores.*', 'merchants.merchant_no');

            } //分店
            elseif ($return_type == '1') {
                $obj->whereIn('stores.store_id', $stores)
                    ->where('stores.pid', '>', 0)
                    ->select('stores.*', 'merchants.merchant_no');

            } //所有
            else {
                $obj->whereIn('stores.store_id', $stores)->select('stores.*', 'merchants.merchant_no');
            }


            $this->t = $obj->count();
            $data = $this->page($obj)->orderBy('id', 'asc')->get();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);


        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }


    /**
     * 单个收银员信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function merchant_info(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $merchant_id = $request->get('merchant_id');
            $store_id = $request->get('store_id', '');

            $check_data = [
                'merchant_id' => '收银员',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }


            $data = Merchant::where('id', $merchant_id)->first();
            if (!$data) {
                $this->status = 2;
                $this->message = '收银员不存在';
                return $this->format();
            }


            //门店id存在
            if ($store_id) {
                //收银员的收款聚合码
                $data->pay_qr = url('/qr?store_id=' . $store_id . '&merchant_id=' . $merchant_id . '');
            }

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);


        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }


    /**
     * 添加分店
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add_sub_store(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $store_name = $request->get('store_name', '');
            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');
            $store_address = $request->get('store_address', '');
            $merchant_id = $merchant->merchant_id;
            $pay_ways_type = $request->get('pay_ways_type', 1);

            $obj = DB::table('stores');
            $obj = $obj->join('merchant_stores', 'stores.store_id', '=', 'merchant_stores.store_id')
                ->where('merchant_stores.merchant_id', $merchant_id)
                ->where('stores.pid', 0)
                ->select('stores.*')
                ->first();

            if (!$obj) {
                $this->status = 2;
                $this->message = '添加分店前必须要认证一家总店';
                return $this->format();
            }

            if ($merchant->merchant_type == 2) {
                $this->status = 2;
                $this->message = '收银员没有权限添加门店';
                return $this->format();
            }

            $pid_store_id = $obj->store_id;

            if ($store_name == "") {
                $this->status = 2;
                $this->message = '店铺名称必须填写';
                return $this->format();
            }

            if ($province_code == "") {
                $province_code = $obj->province_code;
            }
            if ($city_code == "") {
                $city_code = $obj->city_code;
            }
            if ($area_code == "") {
                $area_code = $obj->area_code;
            }
            //80开头+当前日期+4位随机数
            $merchantNo = $this->generateMerchantNo();
            //检查是否存在商户号
            $existMerchantNo = Merchant::where('merchant_no', $merchantNo)->first();
            if (!$existMerchantNo) {
                $merchantNo = $this->generateMerchantNo(); //重新获取
            }

            $store_id = date('YmdHis', time()) . rand(10000, 99999) . $merchant_id;
            $data = [
                'store_id' => $store_id,
                'config_id' => $obj->config_id,
                'pid' => $obj->id,
                'store_name' => $store_name,
                'store_short_name' => $store_name,
                'province_code' => $province_code,
                'city_code' => $city_code,
                'area_code' => $area_code,
                'province_name' => $this->city_name($province_code),
                'city_name' => $this->city_name($city_code),
                'area_name' => $this->city_name($area_code),
                'store_address' => $store_address,
                'user_id' => $obj->user_id,
                'user_pid' => $obj->user_pid,
                'store_type' => $obj->store_type,
                'store_type_name' => $obj->store_type_name,
                'category_id' => $obj->category_id,
                'category_name' => $obj->category_name,
                'pay_ways_type' => $pay_ways_type,//走上级通道
                'source' => $obj->source, //来源,01:畅立收，02:河南畅立收
                'source_type' => $obj->source_type //1 中原畅立收
            ];

            //注册merchants账户
            $merchantData = [
                'pid' => 0,
                'type' => 1, //1、店长 2 收银员
                'name' => $store_name,
                'password' => bcrypt('000000'),
                'pay_password' => bcrypt('000000'),
                'phone' => $obj->people_phone,
                'user_id' => $obj->user_id,
                'config_id' => $obj->config_id,
                'wx_openid' => '',
                'source' => $obj->source, //来源,01:畅立收，02:河南畅立收
                'merchant_no' => $merchantNo,
                'is_login' => '2'
            ];
            $merchant = Merchant::create($merchantData);
            $merchant_id = $merchant->id;

            $data['merchant_id'] = $merchant_id;

            Store::create($data);
            StoreImg::create(['store_id' => $store_id]);
            StoreBank::create(['store_id' => $store_id]);

            MerchantStore::create([
                'store_id' => $store_id,
                'merchant_id' => $merchant_id
            ]);


            //共享上级通道 暂时未启用
            $pay_ways_type = 0;
            if ($pay_ways_type == 1) {

                $StorePayWay = StorePayWay::where('store_id', $pid_store_id)
                    ->where('status', 1)
                    ->get();


                foreach ($StorePayWay as $k => $v) {
                    $StorePayWay_new = StorePayWay::where('store_id', $store_id)
                        ->where('ways_type', $v->ways_type)
                        ->first();
                    $sp = [
                        'store_id' => $store_id,
                        'status' => $v->status,
                        'rate' => $v->rate,
                        'settlement_type' => $v->settlement_type,
                        'status_desc' => $v->status_desc,
                        'ways_type' => $v->ways_type,
                        'ways_source' => $v->ways_source,
                        'company' => $v->company,
                        'ways_desc' => $v->ways_desc,
                        'pcredit' => $v->pcredit,
                        'credit' => $v->credit,
                        'is_close' => $v->is_close,
                        'is_close_desc' => $v->is_close_desc,
                        'sort' => $v->sort,
                    ];
                    if ($StorePayWay_new) {
                        StorePayWay::where('store_id', $store_id)
                            ->where('ways_type', $v->ways_type)
                            ->update($sp);
                    } else {
                        StorePayWay::create($sp);
                    }

                }

            }

            //共享上级通道结束

            $this->status = 1;
            $this->message = '分店添加成功';
            $data = [
                'store_id' => $store_id,
                'pid' => $obj->id,
            ];
            return $this->format($data);


        } catch (\Exception $exception) {
            Log::info($exception);
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }


    /**
     * 修改分店
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function up_sub_store(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $store_id = $request->get('store_id');
            $merchant_id = $merchant->merchant_id;
            $data = $request->except(['token']);
            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');


            $store = Store::where('store_id', $store_id)->first();
            if (!$store && $store->pid == 0) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或者不是分店',
                ]);
            }

            if ($merchant->merchant_type == 2) {
                $this->status = 2;
                $this->message = '收银员没有权限修改门店';
                return $this->format();
            }

            if ($province_code) {
                $data['province_name'] = $this->city_name($province_code);
            }
            if ($city_code) {
                $data['city_name'] = $this->city_name($city_code);
            }
            if ($area_code) {
                $data['area_name'] = $this->city_name($area_code);
            }

            Store::where('store_id', $store_id)->update($data);
            $this->status = 1;
            $this->message = '分店修改成功';


            //共享上级通道
            if ($store->pay_ways_type == 1) {

                $pid_store_id = Store::where('id', $store->pid)->first();
                $StorePayWay = StorePayWay::where('store_id', $pid_store_id)
                    ->where('status', 1)
                    ->get();

                foreach ($StorePayWay as $k => $v) {
                    $StorePayWay_new = StorePayWay::where('store_id', $store_id)
                        ->where('ways_type', $v->ways_type)
                        ->first();
                    $sp = [
                        'store_id' => $store_id,
                        'status' => $v->status,
                        'rate' => $v->rate,
                        'settlement_type' => $v->settlement_type,
                        'status_desc' => $v->status_desc,
                        'ways_type' => $v->ways_type,
                        'ways_source' => $v->ways_source,
                        'company' => $v->company,
                        'ways_desc' => $v->ways_desc,
                        'pcredit' => $v->pcredit,
                        'credit' => $v->credit,
                        'is_close' => $v->is_close,
                        'is_close_desc' => $v->is_close_desc,
                        'sort' => $v->sort,
                    ];
                    if ($StorePayWay_new) {
                        StorePayWay::where('store_id', $store_id)
                            ->where('ways_type', $v->ways_type)
                            ->update($sp);
                    } else {
                        StorePayWay::create($sp);
                    }
                }

            }
            //共享上级通道结束


            $datare = [
                'store_id' => $store_id,
            ];
            return $this->format($datare);


        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }


    /**
     * 根据门店ID查看收银员列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function merchant_lists(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $merchant_id = $merchant->merchant_id;
            $store_id = $request->get('store_id', '');
            $name = $request->get('name', '');
            $l = $request->get('l', '200');

            if ($store_id == "") {
                $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)
                    ->orderBy('created_at', 'asc')
                    ->first();
                if ($MerchantStore) {
                    $store_id = $MerchantStore->store_id;
                }
            }

            $where = [];

            if ($name) {
                $where[] = ['merchants.name', 'like', '%' . $name . '%'];
            }

            //角色是收银员 只返回自己
            if ($merchant->merchant_type == 2) {
                $where[] = ['merchants.id', '=', $merchant_id];

            }

            if ($store_id) {
                $where[] = ['stores.store_id', '=', $store_id];
            }
            //默认店长不显示商户端
//            $where[] = ['merchants.pid', '>', 0];

            $obj = DB::table('merchant_stores');
            $obj->join('merchants', 'merchant_stores.merchant_id', '=', 'merchants.id')
                ->join('stores', 'merchant_stores.store_id', '=', 'stores.store_id')
                ->where($where)
                ->select('stores.store_name', 'merchants.id as merchant_id', 'merchants.name', 'merchants.phone', 'merchants.type', 'merchants.logo', 'merchants.wx_logo','merchants.merchant_no');


            $this->t = $obj->count();
            $data = $this->page($obj)->get();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);


        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }


    /**
     * 根据登录账户查看收银员列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sub_merchant_lists(Request $request)
    {
        try {

            $merchant = $this->parseToken();
            $merchant_id = $merchant->merchant_id;
            $l = $request->get('l', '200');


            $obj = DB::table('merchants');
            $obj->where('pid', $merchant_id)
                ->select('name', 'id as merchant_id');

            $this->l = $l;
            $this->t = $obj->count();
            $data = $this->page($obj)->get();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);


        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }


    /**
     * 添加收银员二维码
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add_merchant_qr(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $data = [
                'add_url' => url('api/merchant/add_wx_merchant_qr')
            ];
            $this->status = 1;
            $this->message = '收银员添加成功';
            return $this->format($data);


        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }


    //扫二维码跳转到这里
    public function add_wx_merchant_qr(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $message = '暂时无法添加,请使用其他方式';
            return view('errors.page_errors', compact('message'));

        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }


    /**
     * 添加收银员
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add_merchant(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $type = $request->get('type', '2');
            $name = $request->get('name', '');
            $phone = $request->get('phone', '');
            $data = $request->except(['token']);
            $password = $request->get('password', '');
            $stores = Store::where('store_id', $store_id)->first();
            if ($password == "") {
                $password = '000000';
            }
            if ($name == "") {
                $this->status = 2;
                $this->message = '姓名必填';
                return $this->format();
            }

            if ($phone == "") {
                $this->status = 2;
                $this->message = '手机号必填';
                return $this->format();
            }

            if ($merchant->merchant_type == 2) {
                $this->status = 2;
                $this->message = '收银员没有权限添加收银员';
                return $this->format();
            }


            if (!$stores) {
                $this->status = 2;
                $this->message = '门店不存在';
                return $this->format();
            }


            $rules = [
                'phone' => 'required|min:11|max:11',
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                return json_encode([
                    'status' => 2,
                    'message' => '手机号位数不正确',
                ]);
            }


            //验证密码
            if (strlen($password) < 6) {
                return json_encode([
                    'status' => 2,
                    'message' => '密码长度不符合要求'
                ]);
            }

            $rules = [
                'phone' => 'required|unique:merchants',
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {

                return json_encode([
                    'status' => 2,
                    'message' => '手机号已经注册'
                ]);
            } else {
                //80开头+当前日期+4位随机数
                $merchantNo = $this->generateMerchantNo();
                //检查是否存在商户号
                $existMerchantNo = Merchant::where('merchant_no', $merchantNo)->first();
                if (!$existMerchantNo) {
                    $merchantNo = $this->generateMerchantNo(); //重新获取
                }

                $dataIN = [
                    'pid' => $merchant->merchant_id,
                    'type' => $type,
                    'name' => $name,
                    'email' => '',
                    'password' => bcrypt($password),
                    'phone' => $phone,
                    'user_id' => $merchant->user_id,//推广员id
                    'config_id' => $merchant->config_id,
                    'wx_openid' => '',
                    'source' => $stores->source, //来源,01:畅立收，02:河南畅立收
                    'merchant_no' => $merchantNo
                ];
                $insert = Merchant::create($dataIN);
                $merchant_id = $insert->id;
            }

            MerchantStore::create([
                'store_id' => $store_id,
                'merchant_id' => $merchant_id,
            ]);


            $this->status = 1;
            $this->message = '收银员添加成功';
            return $this->format();


        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }


    /**
     * 绑定收银员
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bind_merchant(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $store_id = $request->get('store_id');
            $type = $request->get('type');
            $merchant_id = $request->get('merchant_id');


            $check_data = [
                'store_id' => '门店ID',
                'type' => '角色类型',
                'merchant_id' => '收银员',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $MerchantStore = MerchantStore::where('store_id', $store_id)
                ->where('merchant_id', $merchant_id)
                ->first();

            if ($MerchantStore) {
                return json_encode([
                    'status' => 2,
                    'message' => '收银员已经绑定无需重复绑定'
                ]);
            }

            MerchantStore::create([
                'store_id' => $store_id,
                'merchant_id' => $merchant_id,
                'type' => $type,
            ]);


            $this->status = 1;
            $this->message = '收银员绑定成功';
            return $this->format();


        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }


    /**
     * 修改收银员
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function up_merchant(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $merchant_id = $request->get('merchant_id');
            $store_id = $request->get('store_id');
            $name = $request->get('name', '');
            $password = $request->get('password', '');
            $data = $request->except(['token', 'merchant_id', 'store_id', 'password']);

            if ($password == "") {
                $password = '000000';
            }
            if ($name == "") {
                $this->status = 2;
                $this->message = '姓名必填';
                return $this->format();
            }
            if ($password && $password != '000000') {
                //验证密码
                if (strlen($password) < 6) {
                    return json_encode([
                        'status' => 2,
                        'message' => '密码长度不符合要求'
                    ]);
                }

                $data['password'] = bcrypt($password);
            }

            $dataIN = $data;
            Merchant::where('id', $merchant_id)
                ->update($dataIN);

            $this->status = 1;
            $this->message = '收银员修改成功';
            return $this->format();
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    /**
     * 删除收银员
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del_merchant(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $merchant_id = $request->get('merchant_id', '');
            $store_id = $request->get('store_id', '');

            $check_data = [
                'merchant_id' => '收银员',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            if ($merchant_id == "") {
                $this->status = 2;
                $this->message = '请选择收银员';
                return $this->format();
            }
            $Merchant = Merchant::where('id', $merchant_id)->first();
            if (!$Merchant) {
                $this->status = 2;
                $this->message = '收银员不存在';
                return $this->format();
            }

            //不允许删除创建者
            if ($merchant->merchant_id == $merchant_id) {
                $this->status = 2;
                $this->message = '无法删除自己';
                return $this->format();
            }

            $res = '';
            try {
                DB::beginTransaction();  //开启事务
                if ($store_id) {
                    //删除收银员关联表
                    $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)
                        ->where('store_id', $store_id)
                        ->delete();
                } else {
                    //删除收银员关联表
                    $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)
                        ->delete();
                }

                $res = $Merchant->delete();
                DB::commit(); //提交事务
            } catch (\Exception $ex) {
                DB::rollBack();  //事务回滚
                return json_encode([
                    'status' => '-1',
                    'message' => $ex->getCode() . '|' . $ex->getMessage() . '|' . $ex->getLine()
                ]);
            }

            if ($res) {
                $this->status = 1;
                $this->message = '收银员删除成功';
                return $this->format();
            } else {
                $this->status = 2;
                $this->message = '收银员删除成功';
                return $this->format();
            }
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    //删除门店
    public function del_store(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $store_id = $request->get('store_id', '');

            if ($merchant->pid) {
                $this->status = 2;
                $this->message = '没有权限删除门店';
                return $this->format();
            }

            $Store = Store::where('store_id', $store_id)
                ->first();
            if (!$Store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在!'
                ]);
            }

            $Order = Order::where('store_id', $store_id)
                ->where('pay_status', 1)
                ->select('id')
                ->first();
            if ($Order) {
                return json_encode([
                    'status' => 2,
                    'message' => '有交易订单的门店不支持删除!'
                ]);
            }

            $MerchantStore = MerchantStore::where('store_id', $store_id)
                ->select('id')
                ->get();
            if (count($MerchantStore) > 1) {
                return json_encode([
                    'status' => 2,
                    'message' => '请先删除收银员'
                ]);
            }
            Merchant::where('id', $Store->merchant_id)->delete();
            Store::where('store_id', $store_id)->delete();
            StoreImg::where('store_id', $store_id)->delete();
            StoreBank::where('store_id', $store_id)->delete();
            MerchantStore::where('store_id', $store_id)->delete();

            $this->status = 1;
            $this->message = '门店删除成功';

            $data = [
                'store_id' => $store_id
            ];
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }


    //所有系统通道
    public function pay_ways_all(Request $request)
    {
        try {
            $user = $this->parseToken();
            $MerchantStore = MerchantStore::where('merchant_id', $user->merchant_id)
                ->orderBy('created_at', 'asc')
                ->first();
            $store_id = '';
            if ($MerchantStore) {
                $store_id = $MerchantStore->store_id;
            }
            $data = [];
            $store_ways_desc = DB::table('store_ways_desc')->get();
            $store_ways_desc = json_decode(json_encode($store_ways_desc), true);
            foreach ($store_ways_desc as $k => $value) {

                $has = StorePayWay::where('store_id', $store_id)->where('ways_type', $value['ways_type'])
                    ->first();
                if ($has) {
                    $data[$k] = $value;
                    $data[$k]['rate'] = $has->rate;
                    $data[$k]['status'] = $has->status;
                    $data[$k]['status_desc'] = $has->status_desc;
                    $data[$k]['icon'] = '';

                    //如果是刷卡费率读取
                    //新大陆刷卡
                    if (in_array($value['ways_type'], [8005, 6005])) {

                        $data[$k]['rate'] = $has->rate_e;
                    }

                    //银联扫码
                    if (in_array($value['ways_type'], [8004, 6004])) {
                        $data[$k]['rate'] = $has->rate_c;
                    }


                    $data = array_values($data);

                } else {
                    //未开通的支付宝微信不显示
                    if (in_array($value['ways_type'], [1001, 2001])) {

                    }
                    $rate = $value['store_all_rate'];//默认是系统费率
                    $rate_a = $value['store_all_rate_a'];//默认是系统费率
                    $rate_b = $value['store_all_rate_b'];//默认是系统费率
                    $rate_e = $value['store_all_rate_e'];//默认是系统费率
                    $rate_c = $value['store_all_rate_c'];//默认是系统费率
                    $data[$k] = $value;
                    //代理商的费率
                    $user_rate = UserRate::where('user_id', $user->user_id)
                        ->where('ways_type', $value['ways_type'])
                        ->first();
                    if ($user_rate) {
                        $rate = $user_rate->store_all_rate;
                        $rate_c = $user_rate->store_all_rate_c;
                        $rate_e = $user_rate->store_all_rate_e;
                    }


                    $data[$k]['rate'] = $rate; //
                    $data[$k]['status'] = 0;
                    $data[$k]['status_desc'] = '未开通';
                    $data[$k]['icon'] = '';


                    //如果是刷卡费率读取
                    if (in_array($value['ways_type'], [8005, 6005])) {
                        $data[$k]['rate'] = $rate_e;
                    }

                    //银联扫码
                    if (in_array($value['ways_type'], [8004, 6004])) {
                        $data[$k]['rate'] = $rate_c;
                    }

                }

            }
            $help_url = 'https://www.baidu.com';
            return json_encode(['status' => 1, 'help_url' => $help_url, 'data' => $data]);


        } catch (\Exception $exception) {
            return json_encode(['status' => 0, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }
    }


    //所有系统通道-new
    public function store_all_pay_way_lists(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');


            if ($store_id == "") {
                $MerchantStore = MerchantStore::where('merchant_id', $user->merchant_id)
                    ->orderBy('created_at', 'asc')
                    ->select('store_id')
                    ->first();
                if ($MerchantStore) {
                    $store_id = $MerchantStore->store_id;
                }
            }

            $store = Store::where('store_id', $store_id)
                ->select('user_id')
                ->first();

            $data = [];
            $store_all_pay_way_lists = DB::table('store_all_pay_way_lists')
                ->select('ways_count', 'company', 'company_desc')
                ->get();

            $i = 0;

            $store_all_pay_way_lists = json_decode(json_encode($store_all_pay_way_lists), true);
            foreach ($store_all_pay_way_lists as $k => $value) {

                //代理不开通不
                $UserRate = UserRate::where('company', $value['company'])
                    ->where('user_id', $store->user_id)
                    ->select('id')
                    ->first();
                if (!$UserRate) {
                    continue;
                }

                $has = StorePayWay::where('store_id', $store_id)
                    ->where('company', $value['company'])
                    ->select('status')
                    ->first();
                $data[$i] = $value;
                $data[$i]['status'] = '0';
                $data[$i]['status_desc'] = '未开通';
                //
                if ($has && $has->status == 1) {
                    $data[$i]['status'] = '1';
                    $data[$i]['status_desc'] = '开通成功';
                }

                if ($has && $has->status == 2) {
                    $data[$i]['status'] = '2';
                    $data[$i]['status_desc'] = '审核中';
                }

                if ($has && $has->status == 3) {
                    $data[$i]['status'] = '3';
                    $data[$i]['status_desc'] = '开通失败';
                }

                $i = $i + 1;


            }
            return json_encode(['status' => 1, 'data' => $data]);


        } catch (\Exception $exception) {
            return json_encode(['status' => 0, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }
    }


    //单个通道信息查询-new
    public function company_pay_ways_info(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id');
            $company = $request->get('company');
            $data = [];
            $store_ways_desc = DB::table('store_ways_desc')
                ->where('company', $company)->get();


            $store = Store::where('store_id', $store_id)
                ->select('user_id')
                ->first();

            $ways_desc = "";
            $ways_status = "";
            foreach ($store_ways_desc as $k => $v) {

                $data[$k]['ways_desc'] = $v->ways_desc;
                $data[$k]['ways_source'] = $v->ways_source;
                $data[$k]['settlement_type'] = $v->settlement_type;
                $data[$k]['ways_type'] = $v->ways_type;

                $has = StorePayWay::where('store_id', $store_id)
                    ->where('ways_type', $v->ways_type)
                    ->first();

                $ways_desc = $ways_desc . $data[$k]['ways_desc'] . ',';


                if ($has) {
                    $data[$k]['rate'] = $has->rate;
                    $data[$k]['status'] = $has->status;
                    $data[$k]['status_desc'] = $has->status_desc;
                    $data[$k]['status_content'] = $has->status_desc;

                } else {

                    $rate = $v->rate;//默认是系统费率
                    //代理商的费率
                    $user_rate = UserRate::where('user_id', $store->user_id)
                        ->where('ways_type', $v->ways_type)
                        ->select('store_all_rate')
                        ->first();
                    if ($user_rate) {
                        $rate = $user_rate->store_all_rate;
                    }
                    $data[$k]['rate'] = $rate; //
                    $data[$k]['status'] = 0;
                    $data[$k]['status_desc'] = '未开通';
                    $data[$k]['status_content'] = '未开通';

                }

                $ways_status = $data[$k]['status'];
            }


            return json_encode(['status' => 1, 'message' => '数据返回成功', 'ways_status' => $ways_status, 'ways_desc' => $ways_desc, 'data' => $data]);


        } catch (\Exception $exception) {
            return json_encode(['status' => 0, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }
    }


    //申请通道-new
    public function open_company_pay_ways(Request $request)
    {
        try {

            $user = $this->parseToken();
            $company = $request->get('company');
            $store_id = $request->get('store_id');

            $code = $request->get('code', '888888');

            //$SettleModeType = $request->get('Settle_mode_type', '01'); //结算方式
            $SettleModeType = $request->get('settle_mode_type');//结算方式
            if (!$SettleModeType) {
                return json_encode(['status' => 2, 'message' => '请选择结算类型/方式']);
            }
            $sp = new StorePayWaysController();

            $store_ways_desc = DB::table('store_ways_desc')
                ->where('company', $company)
                ->first();
            $type = (int)$store_ways_desc->ways_type;

            return $sp->base_open_ways($type, $code, $store_id, $SettleModeType, '', '');
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'message' => $exception->getMessage() . $exception->getLine(),
            ]);
        }
    }


    //结算方式查询
    public function settle_mode_type(Request $request)
    {

        try {
            $type = $request->get('ways_type', '3001');
            $data = [
                [
                    'settle_mode_type' => '01',
                    'settle_mode_type_desc' => '结算到银行卡',
                    'agreement' => '银行收单结算协议',
                    'url' => url(''),
                ]
            ];
            if ($type == '1000') {
                $data = [
                    [
                        'settle_mode_type' => '01',
                        'settle_mode_type_desc' => '结算到支付宝',
                        'agreement' => '支付宝协议',
                        'url' => url(''),
                    ]
                ];
            }

            if ($type == '16001' || $type == '16002') {
                $data = [
                    [
                        'settle_mode_type' => '02',
                        'settle_mode_type_desc' => '结算到支付宝',
                        'agreement' => '支付宝协议',
                        'url' => url('')
                    ],
                    [
                        'settle_mode_type' => '01',
                        'settle_mode_type_desc' => '结算到银行卡',
                        'agreement' => '支付宝协议',
                        'url' => url('')
                    ]
                ];
            }

            if ($type == '2000') {
                $data = [
                    [
                        'settle_mode_type' => '01',
                        'settle_mode_type_desc' => '结算到微信商户号',
                        'agreement' => '微信支付协议',
                        'url' => url(''),
                    ],
                ];
            }

            if ($type == '3001' || $type == '3002') {
                $data = [
                    [
                        'settle_mode_type' => '01',
                        'settle_mode_type_desc' => '结算到银行卡',
                        'agreement' => '快钱支付支付协议',
                        'url' => url(''),
                    ]
                ];
            }

            $this->status = 1;
            $this->message = '数据返回成功';

            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    //申请通道
    public function open_pay_ways(Request $request)
    {
        try {
            $user = $this->parseToken();
            $type = $request->get('ways_type');
            $code = $request->get('code', '888888');
            $merchant_id = $user->merchant_id;
            $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)
                ->select('store_id')
                ->first();
            if (!$MerchantStore) {
                return json_encode(['status' => 2, 'message' => '没有添加店铺']);
            }
            $store_id = $MerchantStore->store_id;

            $Merchant = Merchant::where('id', $merchant_id)
                ->select('phone', 'email', 'config_id')
                ->first();
            $phone = $Merchant->phone;
            $email = $Merchant->email;
            $SettleModeType = $request->get('Settle_mode_type', '01');//结算方式

            $config = new TfConfigController();
            $tf_config = $config->tf_config($Merchant->config_id, 2);
            $qd = 1;
            if ($tf_config) {
                $qd = 2;
            }

            $sp = new StorePayWaysController();

            return $sp->base_open_ways($type, $code, $store_id, $SettleModeType, $phone, $email, '', $qd);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'message' => $exception->getMessage() . $exception->getLine(),
            ]);
        }
    }


    //店铺通道开通类型
    public function store_pay_ways(Request $request)
    {
        try {
            $merchant = $this->parseToken();//
            $merchant_id = $merchant->merchant_id;
            $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)
                ->orderBy('created_at', 'asc')
                ->first();
            $store_id = $MerchantStore->store_id;
            if ($store_id) {
                $data = DB::table('store_ways_desc')
                    ->join('store_pay_ways', 'store_pay_ways.ways_type', '=', 'store_ways_desc.ways_type')
                    ->where('store_pay_ways.store_id', $store_id)
                    ->where('store_pay_ways.status', 1)
                    ->where('store_pay_ways.is_close', 0)
                    ->select('store_ways_desc.*', 'store_pay_ways.ways_source', 'store_pay_ways.sort', 'store_pay_ways.rate')
                    ->orderBy('store_pay_ways.sort', 'asc')
                    ->get();

                $this->status = 1;
                $this->message = '数据返回成功';
                return $this->format($data);


            } else {
                return json_encode(['status' => 2, 'message' => '没有绑定店铺']);
            }

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }


    //我的 绑定简称
    public function add_store_short_name(Request $request)
    {

        try {
            $merchant = $this->parseToken();
            $store_short_name = $request->get('store_short_name', '');
            $store_id = $request->get('store_id', '');

            if ($store_id == "") {
                $MerchantStore = MerchantStore::where('merchant_id', $merchant->merchant_id)
                    ->orderBy('created_at', 'asc')
                    ->first();
                $store_id = $MerchantStore->store_id;
            }

            $store = Store::where('store_id', $store_id)->first();

            //添加修改
            if ($store_short_name) {
                $store->update([
                    'store_short_name' => $store_short_name
                ]);
                $store->save();
                $message = "门店简称修改成功";

            } else {
                $store_short_name = '';
                if ($MerchantStore) {
                    $store_short_name = $store->store_short_name;
                }
                $message = "数据返回成功";
            }


            $data = [
                'status' => 1,
                'message' => $message,
                'data' => [
                    'store_short_name' => $store_short_name
                ]
            ];
            return json_encode($data);

        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }


    //获取门店收银员的收款微信公众号提醒二维码
    public function get_wx_notify(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $config_id = $merchant->config_id;
            $store_id = $request->get('store_id', '');
            $merchant_id = $request->get('merchant_id', $merchant->merchant_id);

            $config = new WeixinConfigController();
            $config_obj = $config->weixin_config_obj($config_id);

            if ($store_id == "") {
                $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)->first();
                if ($MerchantStore) {
                    $store_id = $MerchantStore->store_id;
                }
            }

            $config = [
                'app_id' => $config_obj->wx_notify_appid,
                'secret' => $config_obj->wx_notify_secret,
            ];
            $app = Factory::officialAccount($config);

            $key = 'gztx&' . $config_id . '&' . $store_id . '&' . $merchant_id;

            $result = $app->qrcode->temporary($key, 6 * 24 * 3600);
            if (isset($result['url'])) {
                $data = [
                    'status' => 1,
                    'message' => '返回成功',
                    'data' => [
                        'url' => $result['url'],
                        'name' => $config_obj->app_name,
                    ]
                ];
            } else {
                $data = [
                    'status' => 2,
                    'message' => '生成失败',
                ];
            }
            return json_encode($data);


        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }


    //检查是否有关注
    public function check_wx_notify(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $config_id = $merchant->config_id;
            $store_id = $request->get('store_id', '');
            $merchant_id = $request->get('merchant_id', $merchant->merchant_id);

            if ($store_id == "") {
                $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)->first();
                if ($MerchantStore) {
                    $store_id = $MerchantStore->store_id;
                }
            }
            $is_wx_notify = '0';
            $WeixinNotify = WeixinNotify::where('store_id', $store_id)
                ->where('merchant_id', $merchant_id)
                ->first();
            if ($WeixinNotify) {
                $is_wx_notify = '1';
            }
            return json_encode([
                'status' => 1,
                'message' => '返回成功',
                'data' => [
                    'is_wx_notify' => $is_wx_notify,
                ]
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }


    //关注删除
    public function get_wx_notify_del(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $config_id = $merchant->config_id;
            $store_id = $request->get('store_id', '');
            $merchant_id = $request->get('merchant_id', $merchant->merchant_id);

            if ($store_id == "") {
                $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)->first();
                if ($MerchantStore) {
                    $store_id = $MerchantStore->store_id;
                }
            }
            $WeixinNotify = WeixinNotify::where('store_id', $store_id)
                ->where('merchant_id', $merchant_id)
                ->delete();

            return json_encode([
                'status' => 1,
                'message' => '取消成功',
                'data' => [
                    'is_wx_notify' => '0',
                ]
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }


    public function time($time)
    {
        try {
            //去除中文
            $time = str_replace(".", "-", $time);
            $time = preg_replace('/([\x80-\xff]*)/i', '', $time);

            $is_date = strtotime($time) ? strtotime($time) : false;
            if ($is_date) {
                $time = date('Y-m-d', strtotime($time));
            }

            return $time;
        } catch (\Exception $exception) {

        }

        return $time;
    }


    public function add_shops(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $shop_id = $request->get('shop_id', '');
            $store_id = $request->get('store_id', '');
            $merchant_id = $request->get('merchant_id', $merchant->merchant_id);

            if (Schema::hasTable('shops')) {
                if ($store_id == "") {
                    $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)
                        ->select('merchant_id')
                        ->first();
                    if ($MerchantStore) {
                        $store_id = $MerchantStore->store_id;
                    }
                }

                $shop = Shop::where('store_id', $store_id)
                    ->where('shop_id', $shop_id)
                    ->where('merchant_id', $merchant_id)
                    ->first();
                if (!$shop) {
                    Shop::create(
                        [
                            'store_id' => $store_id,
                            'merchant_id' => $merchant_id,
                            'shop_id' => $shop_id,
                        ]
                    );
                }
            }

            return json_encode([
                'status' => 1,
                'message' => '插入成功',
                'data' => [
                    'shop_id' => $shop_id,
                    'store_id' => $store_id,
                    'merchant_id' => $merchant_id,
                ]
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }


    //易联云 打印小票
    public function yly_print(Request $request)
    {
        $token = $this->parseToken();
        $out_trade_no = $request->post('outTradeNo', '');

        try {
            $order_obj = Order::where('out_trade_no', $out_trade_no)->first();
            if (!$order_obj) {
                return json_encode([
                    'status' => '2',
                    'message' => '订单不存在'
                ]);
            }

            $store_id = $order_obj->store_id;
            $merchant_id = $order_obj->merchant_id;

            $store_obj = Store::where('store_id', $store_id)->first();
            if (!$store_obj) {
                return json_encode([
                    'status' => '2',
                    'message' => '门店不存在'
                ]);
            }

            $devices_obj = Device::where('store_id', $store_id)
                ->where('merchant_id', $merchant_id)
                ->where('type', 'p')
                ->get();

            //收银员未绑定走门店机器
            if ($devices_obj->isEmpty()) {
                $devices_obj = Device::where('store_id', $store_id)
                    ->where('merchant_id', '')
                    ->where('type', 'p')
                    ->get();
            }

            if (!$devices_obj->isEmpty()) {
                foreach ($devices_obj as $values) {
                    if ($values->device_type == "p_yly_k4") {
                        $VConfig = VConfig::where('config_id', $values->config_id)
                            ->select(
                                'yly_user_id',
                                'yly_api_key'
                            )->first();
                        if (!$VConfig) {
                            $VConfig = VConfig::where('config_id', '1234')
                                ->select(
                                    'yly_user_id',
                                    'yly_api_key'
                                )->first();
                        }
                        if (!$VConfig) {
                            \Illuminate\Support\Facades\Log::info('易联云打印未配置app/Api/Controllers/User/StoreController.php/yly_print');
                            continue;
                        }

                        try {
                            $da = new YlianyunAopClient();
                            $push_id = $VConfig->yly_user_id; //用户id
                            $push_key = $VConfig->yly_api_key; //api密钥
                            $data['device_key'] = $values->device_key;
                            $data['device_no'] = $values->device_no;
                            $data['push_id'] = $push_id;
                            $data['push_key'] = $push_key;
                            $data['type'] = $order_obj->ways_type_desc;
                            $data['store_name'] = $order_obj->store_name;
                            $data['out_trade_no'] = $order_obj->out_trade_no;
                            $data['total_amount'] = $order_obj->total_amount;
                            $data['remark'] = $order_obj->remark;
                            $da->send_print($data);
                        } catch (\Exception $exc) {
                            \Illuminate\Support\Facades\Log::info('易联云打印');
                            \Illuminate\Support\Facades\Log::info($exc->getMessage() . ' | ' . $exc->getFile() . ' | ' . $exc->getLine());
                            continue;
                        }
                    }
                }

                return json_encode([
                    'status' => '1',
                    'message' => '打印成功'
                ]);
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '没有匹配到打印设备'
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ]);
        }
    }


    //修改门店信息
    public function update_store(Request $request)
    {
        try {
            $token = $this->parseToken();
            $merchant_id = $token->merchant_id;
            $config_id = $token->config_id;
            $user_id = $token->user_id;

            $data = $request->except(['token']);
            $store_id = $request->post('storeId', ''); //门店id
            $type = $request->post('type', '2'); //操作类型(1-修改;2-读取)

            //门店信息
            $store_name = $request->post('storeName', ''); //门店名称
            $store_short_name = $request->post('storeShortName', ''); //门店简称
            $province_code = $request->post('provinceCode', ''); //
            $city_code = $request->post('cityCode', ''); //
            $area_code = $request->post('areaCode', ''); //
            $store_address = $request->post('storeAddress', ''); //门店地址
            $announcement = $request->post('announcement', ''); //门店公告
            $logo = $request->post('logo', ''); //门店logo
            $category_id = $request->post('categoryId', ''); //门店经营类目id
            $category_name = $request->post('categoryName', ''); //门店经营类目
            $s_time = $request->post('sTime', ''); //门店营业开始时间
            $e_time = $request->post('eTime', ''); //门店营业结束时间
            $bg_image = $request->post('bgImage', ''); //顾客端背景图

            //门店照片信息
            $store_logo_img = $request->post('storeLogoImg', ''); //门店照片,门头照
            $qr_code_bg_img = $request->post('qrCodeBgImg', ''); //推广二维码照
            $store_qr_code = $request->post('storeQrCode', ''); //店家二维码

            $check_data = [
                'storeId' => '门店ID',
                'type' => '操作类型'
            ];
            $check = $this->check_required($data, $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $store_obj = DB::table('stores as s')
                ->select('s.*', 'si.head_sfz_img_a', 'si.head_sfz_img_b', 'si.store_license_img', 'si.store_industrylicense_img', 'si.store_logo_img', 'si.store_img_a', 'si.store_img_b', 'si.store_img_c',
                    'si.bank_img_a', 'si.bank_img_b', 'si.store_other_img', 'si.store_other_img_a', 'si.store_other_img_b', 'si.store_other_img_c', 'si.head_sc_img', 'si.head_store_img',
                    'si.bank_sfz_img_a', 'si.bank_sfz_img_b', 'si.bank_sc_img', 'si.store_auth_bank_img', 'si.qr_code_bg_img', 'si.store_qr_code')
                ->leftjoin('store_imgs as si', function ($join) use ($store_id) {
                    $join->where('si.store_id', '=', $store_id);
                })
                ->where('s.store_id', $store_id)
                ->where('s.is_close', 0)
                ->where('s.is_delete', 0)
                ->first();
            if (!$store_obj) {
                $this->status = 2;
                $this->message = '门店不存在或状态异常';
                return $this->format();
            }

            if ($type == 2) {
                $this->status = 1;
                $this->message = '门店信息';
                return $this->format($store_obj);
            }

            //拼装门店信息
            $stores_data = [
                'store_id' => $store_id
            ];
            if ($province_code) {
                $stores_data['province_code'] = $province_code;
                $stores_data['province_name'] = $this->city_name($province_code);
            }
            if ($city_code) {
                $stores_data['city_code'] = $city_code;
                $stores_data['city_name'] = $this->city_name($city_code);
            }
            if ($area_code) {
                $stores_data['area_code'] = $area_code;
                $stores_data['area_name'] = $this->city_name($area_code);
            }
            if ($store_name) $stores_data['store_name'] = $store_name;
            if ($store_short_name) $stores_data['store_short_name'] = $store_short_name;
            if ($store_address) $stores_data['store_address'] = $store_address;
            if ($announcement) $stores_data['announcement'] = $announcement;
            if ($logo) $stores_data['logo'] = $logo;
            if ($category_id) $stores_data['category_id'] = $category_id;
            if ($category_name) $stores_data['category_name'] = $category_name;
            if ($s_time) $stores_data['s_time'] = $s_time;
            if ($e_time) $stores_data['e_time'] = $e_time;
            if ($bg_image) $stores_data['bg_image'] = $bg_image;

            //图片信息
            $store_imgs_data = [];
            if ($store_logo_img) $store_imgs_data['store_logo_img'] = $store_logo_img;
            if ($qr_code_bg_img) $store_imgs_data['qr_code_bg_img'] = $qr_code_bg_img;
            if ($store_qr_code) $store_imgs_data['store_qr_code'] = $store_qr_code;

            if ($store_logo_img || $qr_code_bg_img || $store_qr_code) {
                $store_img_obj = StoreImg::where('store_id', $store_id)->first();
                if ($store_img_obj) {
                    $res2 = $store_img_obj->update($store_imgs_data);
                } else {
                    $store_imgs_data['store_id'] = $store_id;
                    $res2 = StoreImg::create($store_imgs_data);
                }
            }

            $stores_data = array_filter($stores_data, function ($v) {
                if ($v == "") {
                    return false;
                } else {
                    return true;
                }
            });

            $res = Store::where('store_id', $store_id)
                ->update($stores_data);
            if ($res) {
                $this->status = 1;
                $this->message = '更新成功';
                return $this->format($store_id);
            } else {
                $this->status = 2;
                $this->message = '更新失败';
                return $this->format($store_id);
            }
        } catch (\Exception $ex) {
            Log::info('商户端-编辑门店信息error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }


    //门店配送设置or更新or查询
    public function store_deliver_settings(Request $request)
    {
        $token = $this->parseToken();
        $store_id = $request->post('storeId', ''); //门店id
        $packing_fee_method = $request->post('packingFeeMethod', ''); //打包费收费方式
        $packing_charge = $request->post('packingCharge', 0); //打包费
        $is_same_day = $request->post('isSameDay', ''); //是否跨天预定
        $delivery_method = $request->post('deliveryMethod', ''); //配送方式
        $delivery_range = $request->post('deliveryRange', ''); //配送范围
        $delivery_start_time = $request->post('delivery_start_time', ''); //配送开始时段
        $delivery_end_time = $request->post('delivery_end_time', ''); //配送开始时段
        $delivery_start_amount = $request->post('delivery_start_amount', ''); //最低配送金额
        $free_delivery_amount = $request->post('free_delivery_amount', ''); //免配送门槛
        $delivery_amount = $request->post('delivery_amount', ''); //配送费

        $status = $request->post('status', ''); //是否开启外卖配送
        $type = $request->post('type', false); //操作类型,true为查询，其他为新增or更新

        $check_data = [
            'storeId' => '门店id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = 2;
            $this->message = $check;
            return $this->format();
        }

        if ($type) {
            $store_deliver_obj = StoreDeliverSetting::where('store_id', $store_id)->first();
            if ($store_deliver_obj) {
                $this->status = 1;
                $this->message = '查询成功';
                return $this->format($store_deliver_obj);
            } else {
                $this->status = 2;
                $this->message = '该门店尚未设置,请先设置信息';
                return $this->format();
            }
        }

        try {
            $data = [
                'store_id' => $store_id
            ];
            if ($packing_fee_method) $data['packing_fee_method'] = $packing_fee_method;
            if ($packing_charge || $packing_charge === 0 || $packing_charge === 0.00) $data['packing_charge'] = $packing_charge;
            if ($is_same_day || $is_same_day === 0) $data['is_same_day'] = $is_same_day;
            if ($delivery_method) $data['delivery_method'] = $delivery_method;
            if ($delivery_range || $delivery_range === 0) $data['delivery_range'] = $delivery_range;
            if ($delivery_start_time) $data['delivery_start_time'] = $delivery_start_time;
            if ($delivery_end_time) $data['delivery_end_time'] = $delivery_end_time;
            if ($delivery_start_amount || $delivery_start_amount === 0 || $delivery_start_amount === 0.00) $data['delivery_start_amount'] = $delivery_start_amount;
            if ($free_delivery_amount || $free_delivery_amount === 0 || $free_delivery_amount === 0.00) $data['free_delivery_amount'] = $free_delivery_amount;
            if ($status || $status === 0 || $status === '0') $data['status'] = $status;
            if ($delivery_amount || $delivery_amount === 0 || $delivery_amount === 0.00) $data['delivery_amount'] = $delivery_amount;

            $store_deliver_setting_obj = StoreDeliverSetting::where('store_id', $store_id)->first();
            if ($store_deliver_setting_obj) {
                $update_re = $store_deliver_setting_obj->update($data);
                if ($update_re) {
                    $this->status = '1';
                    $this->message = '更新成功';
                    return $this->format();
                } else {
                    $this->status = '2';
                    $this->message = '更新失败';
                    return $this->format($data);
                }
            } else {
                $create_re = StoreDeliverSetting::create($data);
                if ($create_re) {
                    $this->status = '1';
                    $this->message = '创建成功';
                    return $this->format($create_re);
                } else {
                    $this->status = '2';
                    $this->message = '创建失败';
                    return $this->format($data);
                }
            }

        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }


    //获取小程序码
    public function createXcxQr(Request $request)
    {
        $pub_token = $this->parseToken();
        $store_id = $request->post('storeId', '');
        $path = $request->post('path', '');
        $code_number = 'NO_' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999)); //编号
//        $url = env('APP_URL').'/'.$path.'?store_id='.$store_id;
//        $renderer = new ImageRenderer(new RendererStyle(400), new Image\ImagickImageBackEnd());
//        $writer = new Writer($renderer);
//        $writer->writeFile($url, public_path('XcxQrCode/' . $code_number . '.png'));

        try {
            //生成二维码文件
            if (!is_dir(public_path('XcxQrCode/'))) {
                mkdir(public_path('XcxQrCode/'), 0777);
            }

            $xcx_obj = MerchantStoreAppidSecret::where('store_id', $store_id)->first();
            if (!$xcx_obj) {
                $this->status = 2;
                $this->message = '门店对应小程序信息未配置，请完善';
                return $this->format();
            }

            $wechat_applet_qr_code = $xcx_obj->wechat_applet_qr_code;
            if ($wechat_applet_qr_code) {
                return json_encode([
                    'status' => 1,
                    'message' => '获取小程序二维码成功',
                    'img_path' => $wechat_applet_qr_code
                ]);
            } else {
                $config = [
                    'app_id' => $xcx_obj->wechat_appid,
                    'secret' => $xcx_obj->wechat_secret,
                ];
                $app = Factory::miniProgram($config);

                //获取小程序二维码
                $path = '/' . $path;
                $response = $app->app_code->getQrCode($path);
                // $response 成功时为 EasyWeChat\Kernel\Http\StreamResponse 实例，失败为数组或你指定的 API 返回类型
                if ($response instanceof \EasyWeChat\Kernel\Http\StreamResponse) {
                    $filename = $response->saveAs(public_path('XcxQrCode/'), $code_number . '.png');
                }

                $res = DB::table("merchant_store_appid_secrets")->where('store_id', $store_id)->update([
                    'wechat_applet_qr_code' => $wechat_applet_qr_code,
                    'updated_at' => date("Y-m-d H:i:s", time())
                ]);
                if (!$res) {
                    Log::info('门店' . $store_id . '更新小程序二维码失败');
                }

                return json_encode([
                    'status' => 1,
                    'message' => '获取小程序二维码成功',
                    'img_path' => env('APP_URL') . '/XcxQrCode/' . $code_number . '.png'
                ]);
            }
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }


    /**
     * 易联云打印小票 堂食确认收款 打印单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function confirmReceiptPrint(Request $request)
    {
        $token = $this->parseToken();
        $out_trade_no = $request->post('outTradeNo', ''); //系统支付订单号
        $payment = $request->post('payment', ''); //付款金额

        if (!isset($out_trade_no) || empty($out_trade_no)) {
            return $this->responseDataJson(202, "系统支付订单号不可为空");
        }

//        $order_obj = Order::where('out_trade_no', $out_trade_no)->first();
//        if (!$order_obj) {
//            return $this->responseDataJson(2, "订单不存在");
//        }
//        $store_id = $order_obj->store_id;
//        $merchant_id = $order_obj->merchant_id;
//        $pay_status = $order_obj->pay_status;
//        $receipt_amount = $order_obj->receipt_amount;
//        $total_amount = $order_obj->total_amount;
//        $mdiscount_amount = $order_obj->mdiscount_amount;
//        if (!$receipt_amount) {
//            $receipt_amount = ($total_amount*100 - $mdiscount_amount*100)/100;
//        }
//        if ($payment*100 < $receipt_amount*100) {
//            return $this->responseDataJson(2, "付款金额小于商品价格");
//        }
//
//        if ($pay_status != 1) {
//            $updateRes = $order_obj->update([
//                'pay_status' => 1,
//                'pay_status_desc' => '支付成功'
//            ]);
//            if (!$updateRes) {
//                return $this->responseDataJson(2, "系统支付订单更新状态失败");
//            }
//        }

        $user_order_obj = CustomerAppletsUserOrders::where('out_trade_no', $out_trade_no)
            ->first();
        if (!$user_order_obj) {
            return $this->responseDataJson(2, "用户订单不存在");
        }
        $created_at = $user_order_obj->created_at;
        $packing_fee = $user_order_obj->packing_fee; //打包费
        $use_coupon_money = $user_order_obj->use_coupon_money; //如果使用优惠券，优惠券优惠额度
        $order_pay_money = $user_order_obj->order_pay_money; //订单实际支付金额
        $table_name = $user_order_obj->table_name; //桌号
        $order_pay_status = $user_order_obj->order_pay_status; //订单状态：1(已支付)2(待支付)3(已取消)4(订单开始配送)5(订单配送结束)6(退款订单)7(骑手正在取货)8(订单已指定配送员)9(妥投异常之物品返回中)10(妥投异常之物品返回完成)11(订单已完成)100( 骑士到店)1000(创建达达运单失败)
        $store_id = $user_order_obj->store_id;
        $remark = $user_order_obj->order_remarks;

        $store_obj = Store::where('store_id', $store_id)->first();
        if (!$store_obj) {
            return $this->responseDataJson(2, "门店不存在");
        }
        $store_name = $store_obj->store_name;

        $devices_obj = Device::where('store_id', $store_id)
//            ->where('merchant_id', $merchant_id)
            ->where('type', 'p')
            ->get();

        //收银员未绑定走门店机器
        if ($devices_obj->isEmpty()) {
            $devices_obj = Device::where('store_id', $store_id)
//                ->where('merchant_id', '')
                ->where('type', 'p')
                ->get();
        }

        if (!$devices_obj->isEmpty()) {
            foreach ($devices_obj as $values) {
                if ($values->device_type == "p_yly_k4") {
                    $VConfig = VConfig::where('config_id', $values->config_id)
                        ->select('yly_user_id', 'yly_api_key')
                        ->first();
                    if (!$VConfig) {
                        $VConfig = VConfig::where('config_id', '1234')
                            ->select('yly_user_id', 'yly_api_key')
                            ->first();
                    }
                    if (!$VConfig) {
                        Log::info('易联云打印未配置app/Api/Controllers/CustomerApplets/StoreController.php/yly_print');
                        continue;
                    }

                    try {
                        $yLiObj = new YlianyunAopClient();
                        $push_id = $VConfig->yly_user_id; //用户id
                        $push_key = $VConfig->yly_api_key; //api密钥
                        $device_key = $values->device_key;
                        $device_no = $values->device_no;
//                        $type = $order_obj->ways_type_desc;
//                        $store_name = $order_obj->store_name;
//                        $out_trade_no = $order_obj->out_trade_no;
//                        $total_amount = $order_obj->total_amount;
//                        $remark = $order_obj->remark;

                        $order_goods_obj = CustomerAppletsUserOrderGoods::where('out_trade_no', $out_trade_no)
                            ->where('store_id', $store_id)
                            ->get();
                        if (!$order_goods_obj) {
                            return $this->responseDataJson(2, "商品订单不存在");
                        }
                        $order_goods_list_arr = $order_goods_obj->toArray();
                        $goods_list = '';
                        if ($order_goods_list_arr && is_array($order_goods_list_arr)) {
                            foreach ($order_goods_list_arr as $value) {
                                $goods_obj = Goods::find($value['good_id']);
                                $length = strlen($goods_obj->name);
                                if ($length <= 9) {
                                    $goods_list .= str_pad($goods_obj->name, $length + 10, " ") . '  X' . $value['good_num'] . '     ' . $value['good_price'] . "\n\r";
                                } elseif ((9 < $length) && ($length <= 27)) {
                                    $goods_list .= str_pad($goods_obj->name, $length + 4, " ") . '  X' . $value['good_num'] . '     ' . $value['good_price'] . "\n\r";
                                } else {
                                    $goods_list .= str_pad($goods_obj->name, 45 - $length, " ") . '  X' . $value['good_num'] . '     ' . $value['good_price'] . "\n\r";
                                }
                            }
                        }

                        if ($order_pay_status != 1) {
                            $user_order_res = $user_order_obj->update(['order_pay_status' => 1]);
                            if (!$user_order_res) {
                                return $this->responseDataJson(2, "小程序用户订单更新状态失败");
                            }
                        }

                        $give_change = min(0, ($payment * 100 - $order_pay_money * 100) / 100);
                        if ($order_pay_status == 1) {
                            $pay_status_desc = '已完成线上支付';
                        } else {
                            $pay_status_desc = '未支付,等待前台支付';
                        }

                        $content_str = "<center>" . '***#1 堂食单***' . "</center>
<FB>" . $store_name . "</FB>
[下单时间]" . $created_at . "
[订单号]" . $out_trade_no . "
--------------------------------
<FB>" . "<FS>" . '桌号: ' . $table_name . "
$pay_status_desc
</FS></FB>" . "
--------------------------------
名称               数量    售价
--------------------------------
" . $goods_list . "
--------------------------------";
                        if ($packing_fee && $packing_fee !== '0.00') {
                            $content_str .= '打包费: ' . round($packing_fee, 2);
                        }
                        if ($use_coupon_money && $use_coupon_money !== '0.00') {
                            $content_str .= '优惠金额: ' . round($use_coupon_money, 2);
                        }
                        if (($packing_fee && $packing_fee !== '0.00') || ($use_coupon_money && $use_coupon_money !== '0.00')) {
                            $content_str .= '--------------------------------';
                        }
                        $content_str .= '总计:                ￥' . round($order_pay_money, 2) . "
付款:                ￥" . round($payment, 2) . "
找零:                ￥" . round($give_change, 2) .
                            $remark .
                            "<center>            ***#1 完***    </center>";
                        $content = urlencode($content_str);

                        $yLiObj->action_print($push_id, $device_no, $content, $push_key, $device_key);
                    } catch (\Exception $exc) {
                        Log::info('易联云打印-error');
                        Log::info($exc->getMessage() . ' | ' . $exc->getFile() . ' | ' . $exc->getLine());
                        continue;
                    }
                }
            }

            return json_encode([
                'status' => '1',
                'message' => '打印成功'
            ]);
        } else {
            return json_encode([
                'status' => '2',
                'message' => '没有匹配到打印设备'
            ]);
        }
    }

    /**
     * 绑定设备
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bind_device(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $store_id = $request->get('store_id');
            $type = $request->get('type');  //scanBox
            $device_type = $request->get('device_type'); //ruyi_lite
            $device_name = $request->get('device_name');    //如意Lite
            $device_sn = $request->get('device_sn');

            $check_data = [
                'store_id' => '门店ID',
                'device_sn' => '设备编号',
                'type' => '设备归类',
                'device_type' => '设备类型'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $MerchantDevice = MerchantDevice::where('store_id', $store_id)
                ->where('device_sn', $device_sn)
                ->first();

            if ($MerchantDevice) {
                return json_encode([
                    'status' => 2,
                    'message' => '设备已经绑定无需重复绑定'
                ]);
            }
            $alipayAntStores = AlipayAntStores::where('store_id', $store_id)->first();

            if (!$alipayAntStores) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店暂未绑定，请绑定门店'
                ]);
            }

            $alipayOperation = AlipayOperation::where('store_id', $store_id)
                ->where('merchant_no', $alipayAntStores->ip_role_id)
                ->first();


            if ($alipayOperation->store_type == 1) {
                $merchant_id_type = 'direct';
            } else {
                $merchant_id_type = 'indirect';
            }
            $deviceData = [
                'storeId' => $store_id,
                'device_sn' => $device_sn,//设备sn
                'source' => '2088121668676708',//设备来源ISV在支付宝的pid
                'external_id' => $store_id,//商户编号，由ISV定义，需要保证在ISV下唯一
                'merchant_id_type' => $merchant_id_type,//区分商户ID类型，直连商户填写direct，间连商户填写indirect
                'merchant_id' => $alipayOperation->merchant_no,//商户角色id。对于直连开店场景，填写商户pid；对于间连开店场景，填写商户smid
                'shop_id' => $alipayAntStores->shop_id,//店铺ID
                'bind_user_id' => $alipayOperation->bind_user_id,//店铺PID
            ];
            $ruyiPay = new RuyiPayController();
            $deviceResp = $ruyiPay->deviceBind($deviceData);

            $bindStatus = $deviceResp['status'];
            if ($bindStatus == 1) {
                MerchantDevice::create([
                    'store_id' => $store_id,//系统商户id
                    'source' => '2088121668676708',//设备来源ISV在支付宝的pid
                    'device_type' => $device_type,//设备类型
                    'device_sn' => $device_sn,  //sn
                    'external_id' => $store_id,//商户编号，由ISV定义，需要保证在ISV下唯一
                    'type' => $type,   //scanBox
                    'device_name' => $device_name,  //设备名称
                    'merchant_id_type' => $merchant_id_type, //区分商户ID类型，直连商户填写direct，间连商户填写indirec
                    'merchant_id' => $alipayOperation->merchant_no,//商户角色id。对于直连开店场景，填写商户pid；对于间连开店场景，填写商户smid
                    'shop_id' => $alipayAntStores->shop_id,//店铺ID
                    'external_shop_id' => $store_id        //外部门店id
                ]);
                if ($store_id) {
                    $where[] = ['merchant_stores.store_id', '=', $store_id];
                }
                $store_name = DB::table('stores')
                    ->where('store_id', $store_id)
                    ->select('store_name')->first();

                $cashier = DB::table('merchant_stores')
                    ->join('merchants', 'merchant_stores.merchant_id', '=', 'merchants.id')
                    ->where($where)
                    ->where('merchants.pid', '0')
                    ->select('merchants.id as cashier_id', 'merchants.name as cashier_name')->first();

                $cashier_id = json_encode($cashier->cashier_id);
                $cashier_name = json_encode($cashier->cashier_name);
                $device = [
                    'store_id' => $store_id,
                    'merchant_id' => $cashier_id,
                    'merchant_name' => json_decode($cashier_name),
                    'store_name' => json_encode($store_name),
                    'device_type' => $device_type,
                    'device_name' => $device_name,
                    'device_no' => $device_sn,
                    'device_key' => '0',
                    'type' => 's',
                    'cj' => '0',
                    'config_id' => '1234'
                ];
                Device::create($device);
                return json_encode([
                    'status' => 1,
                    'message' => '绑定成功',
                ]);
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => $deviceResp['message'],
                ]);
            }


        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine()
            ]);
        }

    }

    /**
     * 设备列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function device_list(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $store_id = $request->get('store_id');
            $l = $request->get('l', '200');

            $check_data = [
                'store_id' => '门店ID'
            ];

            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $obj = DB::table('merchant_devices');
            $obj->join('stores', 'merchant_devices.store_id', '=', 'stores.store_id')
                ->where('merchant_devices.store_id', '=', $store_id)
                ->select('stores.store_name', 'merchant_devices.store_id', 'merchant_devices.device_sn', 'merchant_devices.type', 'merchant_devices.device_name');

            $this->t = $obj->count();
            $data = $this->page($obj)->get();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }

    /**
     * 解绑设备
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function unbind_device(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $device_sn = $request->get('device_sn', '');
            $store_id = $request->get('store_id', '');

            $check_data = [
                'device_sn' => '设备编号',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            if ($device_sn == "") {
                $this->status = 2;
                $this->message = '请选择设备';
                return $this->format();
            }
            $MerchantDevice = MerchantDevice::where('device_sn', $device_sn)->where('store_id', $store_id)->first();
            if (!$MerchantDevice) {
                $this->status = 2;
                $this->message = '设备不存在';
                return $this->format();
            }

            try {
                DB::beginTransaction();  //开启事务

                $deviceData = [
                    'storeId' => $store_id,
                    'device_sn' => $device_sn,//设备sn
                    'source' => '2088121668676708',//设备来源ISV在支付宝的pid
                    'external_id' => $MerchantDevice->external_shop_id,//商户编号，由ISV定义，需要保证在ISV下唯一
                    'merchant_id_type' => $MerchantDevice->merchant_id_type,//区分商户ID类型，直连商户填写direct，间连商户填写indirec
                    'merchant_id' => $MerchantDevice->merchant_id,//商户角色id。对于直连开店场景，填写商户pid；对于间连开店场景，填写商户smid
                    'shop_id' => $MerchantDevice->shop_id,//店铺ID
                ];
                $ruyiPay = new RuyiPayController();
                $deviceResp = $ruyiPay->deviceUnbind($deviceData);
                if ($deviceResp['status'] == 1) {
                    $MerchantDevice->delete();
                    Device::where('device_no', $device_sn)->where('store_id', $store_id)->delete();
                    DB::commit(); //提交事务
                }
                return $deviceResp;
            } catch (\Exception $ex) {
                DB::rollBack();  //事务回滚
                return json_encode([
                    'status' => '-1',
                    'message' => $ex->getCode() . '|' . $ex->getMessage() . '|' . $ex->getLine()
                ]);
            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => '-1',
                'message' => $exception->getCode() . '|' . $exception->getMessage() . '|' . $exception->getLine()
            ]);
        }

    }

    public function device_type(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $type = $request->get('type', 'scanBox');
            $data = [];
            if ($type == "scanBox") {
                $data = [
                    [
                        'device_type' => 'ruyi_lite',
                        'device_name' => '如意Lite'
                    ]
                ];
            }
            $this->status = 1;
            $this->message = "数据返回成功";

            return $this->format($data);
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getLine()
            ]);
        }
    }

    public function store_pay_qr(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $merchant_id = $request->get('merchant_id', $merchant->merchant_id);
            $store_id = $request->get('store_id', '');
            $ways_type = $request->get('ways_type', '');
            $qrListInfo = QrListInfo::where('store_id', $store_id)->where('merchant_id', $merchant_id)->select('*')->first();
            $store = Store::where('store_id', $store_id)->where('is_delete', 0)->select('store_name')->first();
            $this->status = 1;
            $this->message = '数据返回成功';
            if (!$qrListInfo) {
                $this->status = -1;
                $this->message = '暂未绑定收款码';
                $data = [];
                return $this->format($data);
            }
            $data = [
                'code_url' => url('/qr?no=' . $qrListInfo->code_num),
                'store_name' => $store->store_name,
            ];
            return $this->format($data);
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

}
