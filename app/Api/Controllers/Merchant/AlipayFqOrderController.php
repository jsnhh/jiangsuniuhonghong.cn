<?php
namespace App\Api\Controllers\Merchant;


use Alipayopen\Sdk\AopClient;
use Alipayopen\Sdk\Request\AlipayTradeRefundRequest;
use App\Api\Controllers\BaseController;
use App\Api\Controllers\Config\AlipayIsvConfigController;
use App\Models\AlipayHbOrder;
use App\Models\AlipayHbrate;
use App\Models\MerchantStore;
use App\Models\MerchantWalletDetail;
use App\Models\RefundOrder;
use App\Models\Store;
use App\Models\UserWalletDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class AlipayFqOrderController extends BaseController
{

    public function order(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $store_id = isset($store_id) ? $store_id : "";
            $merchant_id = $request->get('merchant_id', '');
            $merchant_id = isset($merchant_id) ? $merchant_id : "";
            $pay_status = $request->get('pay_status', '');
            $pay_status = isset($pay_status) ? $pay_status : "";

            $time_start = $request->get('time_start', '');
            $time_start = $time_start ? $time_start : "";

            $time_end = $request->get('time_end', '');
            $time_end = $time_end ? $time_end : "";

            $hb_fq_num = $request->get('hb_fq_num', '');
            $hb_fq_num = $hb_fq_num ? $hb_fq_num : "";

            $out_trade_no = $request->get('out_trade_no', '');
            $out_trade_no = $out_trade_no ? $out_trade_no : "";


            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("alipay_hb_orders");
            } else {
                $obj = DB::table('alipay_hb_orders');
            }

            $where = [];

            //没有传入收银员id 角色是收银员 只返回自己
            if ($merchant_id == '') {
                $merchant_id = $merchant->merchant_id;
            }

            if ($store_id == "") {
                $MyBankStore = MerchantStore::where('merchant_id', $merchant_id)
                    ->orderBy('created_at', 'asc')
                    ->first();
                $store_id = $MyBankStore->store_id;
            }


            if ($out_trade_no) {
                $where[] = ['out_trade_no', 'like', '%' . $out_trade_no . '%'];
            }

            if ($hb_fq_num) {
                $where[] = ['hb_fq_num', '=', $hb_fq_num];
            }

            //收银员
            if ($merchant->merchant_type == 2) {
                $where[] = ['merchant_id', '=', $merchant->merchant_id];
            }

            //是否传收银员ID
            if ($request->get('merchant_id', '')) {
                $where[] = ['merchant_id', '=', $request->get('merchant_id', '')];
            }

            if ($pay_status) {
                $where[] = ['pay_status', '=', $pay_status];
            }

            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
            }

            if ($time_start) {
                $where[] = ['updated_at', '>=', $time_start];
            }

            if ($time_end) {
                $where[] = ['updated_at', '<=', $time_end];
            }

            $obj = $obj->where($where);
            $this->t = $obj->count();
            $data = $this->page($obj)
		    ->orderBy('created_at', 'desc')
		    ->get();

            $this->status = 1;
            $this->message = '数据返回成功';

            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine();
            return $this->format();
        }
    }


    public function order_info(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $out_trade_no = $request->get('out_trade_no', '');
            $data = AlipayHbOrder::where('out_trade_no', $out_trade_no)->first();
            if (!$data) {
                $this->status = 2;
                $this->message = '订单号不存在';
                return $this->format();
            }
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    //花呗分期退款
    public function refund(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $out_trade_no = $request->get('out_trade_no', '');
            $refund_amount = $request->get('refund_amount', '');
            $refund_no = $request->get('refund_no', '');
            $refund_no = isset($refund_no) ? $refund_no : $out_trade_no . '123';

            $data = [
                'merchant_id' => $merchant->merchant_id,
                'out_trade_no' => $out_trade_no,
                'refund_amount' => $refund_amount,
                'refund_no' => $refund_no,
            ];

            return $this->refund_public($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }
    

    //花呗分期退款
    public function refund_public($data)
    {
        try {
            $merchant_id = $data['merchant_id'];
            $out_trade_no = $data['out_trade_no'];
            $refund_amount = $data['refund_amount'];
            $refund_no = $data['refund_no'];

            $order = AlipayHbOrder::where('out_trade_no', $out_trade_no)
                ->select(
                    'id',
                    'total_amount',
                    'refund_amount',
                    'device_id',
                    'out_trade_no',
                    'store_id',
                    'ways_type',
                    'config_id',
                    'trade_no',
                    'ways_source'
                )
                ->first();
            if (!$order) {
                $this->status = 2;
                $this->message = '订单号不存在';
                return $this->format();
            }

            $store_id = $order->store_id;
            $ways_type = $order->ways_type;
            $store = Store::where('store_id', $store_id)
                ->select('config_id', 'merchant_id', 'pid')
                ->first();
            $config_id = $store->config_id;
            $store_id = $order->store_id;
            $store_pid = $store->pid;
            $other_no = $order->other_no;
            $out_trade_no = $order->out_trade_no;
            $total_amount = $order->total_amount;
            if ($refund_amount == "") {
                $refund_amount = $total_amount;
            }
            //支付宝官方
            if (999 < $ways_type && $ways_type < 1999) {
                //配置
                $isvconfig = new AlipayIsvConfigController();
                $config_type = '01';
                $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);

                //获取token
                $storeInfo = $isvconfig->alipay_auth_info($store_id, $store_pid);

                $aop = new AopClient();
                $aop->apiVersion = "2.0";
                $aop->appId = $config->app_id;
                $aop->rsaPrivateKey = $config->rsa_private_key;
                $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                $aop->signType = "RSA2";//升级算法
                $aop->gatewayUrl = $config->alipay_gateway;
                $aop->format = "json";
                $aop->charset = "GBK";
                $aop->version = "2.0";
                $aop->method = "alipay.trade.refund";

                $requests = new AlipayTradeRefundRequest();
                $data_req_ali = "{" .
                    "\"out_trade_no\":\"" . $out_trade_no . "\"," .
                    "\"refund_amount\":\"" . $refund_amount . "\"," .
                    "\"out_request_no\":\"" . $refund_no . "\"," .
                    "\"refund_reason\":\"正常退款\"" .
                    "}";
                $requests->setBizContent($data_req_ali);
                $result = $aop->execute($requests, null, $storeInfo->app_auth_token);
                $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
                $resultCode = $result->$responseNode->code;

                //退款成功
                if (!empty($resultCode) && $resultCode == 10000) {
                    $insert_data = [
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $order->refund_amount + $refund_amount,
                    ];

                    AlipayHbOrder::where('out_trade_no', $out_trade_no)
                        ->update($insert_data);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount,//退款金额
                        'refund_no' => $refund_no,//退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $refund_no,//退款单号
                        'other_no' => $other_no
                    ];

                    try {
                        $update_data = [
                            'settlement' => '03',
                            'settlement_desc' => '退款订单',
                        ];

                        $day = date('Ymd', time());
                        $table = 'user_wallet_details_' . $day;

                        if (Schema::hasTable($table)) {
                            DB::table($table)
                                ->where('out_trade_no', $out_trade_no)
                                ->update($update_data);
                        } else {
                            UserWalletDetail::where('out_trade_no', $out_trade_no)
                                ->update($update_data);
                        }

                        $day = date('Ymd', time());
                        $table = 'merchant_wallet_details_' . $day;

                        if (Schema::hasTable($table)) {
                            DB::table($table)
                                ->where('out_trade_no', $out_trade_no)
                                ->update($update_data);
                        } else {
                            MerchantWalletDetail::where('out_trade_no', $out_trade_no)
                                ->update($update_data);
                        }

                    } catch (\Exception $exception) {
                        \Illuminate\Support\Facades\Log::info('退款update_order');
                        \Illuminate\Support\Facades\Log::info($exception);
                    }

                    return json_encode([
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ]);
                } else {
                    //退款失败
                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                    ];

                    return json_encode([
                        'status' => 2,
                        'message' => $result->$responseNode->sub_msg,
                        'data' => $data,
                    ]);
                }
            }

            //直付通
            if (16000 < $ways_type && $ways_type < 16999) {
                $config_type = '03';
                //配置
                $isvconfig = new AlipayIsvConfigController();
                $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);

                if (!$config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '直付通配置不存在'
                    ]);
                }
                //判断直付通
                $AlipayZftStore = $isvconfig->AlipayZftStore($store_id, $store_pid);
                if (!$AlipayZftStore) {
                    return json_encode([
                        'status' => 2,
                        'message' => '直付通门店不存在'
                    ]);
                }

                $aop = new AopClient();
                $aop->apiVersion = "2.0";
                $aop->appId = $config->app_id;
                $aop->rsaPrivateKey = $config->rsa_private_key;
                $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                $aop->signType = "RSA2";//升级算法
                $aop->gatewayUrl = $config->alipay_gateway;
                $aop->format = "json";
                $aop->charset = "GBK";
                $aop->version = "2.0";
                $aop->method = "alipay.trade.refund";

                $requests = new AlipayTradeRefundRequest();
                $data_re = array(
                    'out_trade_no' => $out_trade_no,
                    'refund_amount' => $refund_amount,
                    'out_request_no' => $refund_no,
                    'refund_reason' => '正常退款',
                );

                $a = array('a', '_', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
                $b = str_ireplace($a, "", $out_trade_no);
                $day = substr($b, 0, 8);
                $table = 'settle_orders_' . $day;
                if (Schema::hasTable($table)) {
                    $settle_orders = DB::table($table)->where('out_trade_no', $out_trade_no)
                        ->where('store_id', $store_id)
                        ->first();
                    if ($settle_orders && $settle_orders->order_settle_amount > 0) {
                        $data_re['refund_royalty_parameters'] = array(
                            0 => array(
                                'trans_out' => $settle_orders->trans_out,
                                'amount' => $settle_orders->order_settle_amount,
                                'desc' => '退款分账'
                            )
                        );
                    }
                }

                $data_re = json_encode($data_re);

                $requests->setBizContent($data_re);
                $result = $aop->execute($requests, null, '');
                $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
                $resultCode = $result->$responseNode->code;

                //退款成功
                if (!empty($resultCode) && $resultCode == 10000) {
                    $insert_data = [
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $order->refund_amount + $refund_amount,
                    ];

                    AlipayHbOrder::where('out_trade_no', $out_trade_no)
                        ->update($insert_data);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount,//退款金额
                        'refund_no' => $refund_no,//退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $refund_no,//退款单号
                        'other_no' => $other_no
                    ];

                    try {
                        $update_data = [
                            'settlement' => '03',
                            'settlement_desc' => '退款订单',
                        ];

                        $day = date('Ymd', time());
                        $table = 'user_wallet_details_' . $day;

                        if (Schema::hasTable($table)) {
                            DB::table($table)
                                ->where('out_trade_no', $out_trade_no)
                                ->update($update_data);
                        } else {
                            UserWalletDetail::where('out_trade_no', $out_trade_no)
                                ->update($update_data);
                        }

                        $day = date('Ymd', time());
                        $table = 'merchant_wallet_details_' . $day;

                        if (Schema::hasTable($table)) {
                            DB::table($table)
                                ->where('out_trade_no', $out_trade_no)
                                ->update($update_data);
                        } else {
                            MerchantWalletDetail::where('out_trade_no', $out_trade_no)
                                ->update($update_data);
                        }

                    } catch (\Exception $exception) {
                        \Illuminate\Support\Facades\Log::info('退款update_order');
                        \Illuminate\Support\Facades\Log::info($exception);
                    }

                    return json_encode([
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ]);
                } else {
                    //退款失败
                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                    ];

                    return json_encode([
                        'status' => 2,
                        'message' => $result->$responseNode->sub_msg,
                        'data' => $data,
                    ]);
                }
            }

            $this->status = 2;
            $this->message = '支付类型不存在';
	    
            return $this->format();
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    //查询这个门店获得前端展示的服务费多少
    public function hb_query_rate(Request $request)
    {
        try {
            $store_id = $request->get('store_id', '');
            $hb_fq_seller_percent = $request->get('hb_fq_seller_percent', 0);
            $shop_price = $request->get('shop_price', '');
            $hb_fq_num = $request->get('hb_fq_num', '');

            $AlipayHbrate = AlipayHbrate::where('store_id', $store_id)->first();

            //用户承担
            if ($hb_fq_seller_percent == 0) {
                $xy_ra_3 = 2.3;
                $xy_ra_6 = 4.5;
                $xy_ra_12 = 7.5;
                $base_ra_3 = 2.3;
                $base_ra_6 = 4.5;
                $base_ra_12 = 7.5;

                //商户设置的费率
                if ($AlipayHbrate) {
                    $xy_ra_3 = $AlipayHbrate->hb_fq_num_3;
                    $xy_ra_6 = $AlipayHbrate->hb_fq_num_6;
                    $xy_ra_12 = $AlipayHbrate->hb_fq_num_12;
                }

                $fqfwf_all_3 = ($xy_ra_3 * $shop_price) / 100;//界面显示总服务费
                $total_amount_3 = $fqfwf_all_3 + $shop_price;//界面显示总付款金额
                $total_amount_3_h = ($fqfwf_all_3 + $shop_price) / 3;//每期还款
                $pay_total_amount_3 = $total_amount_3 / (1 + ($base_ra_3 / 100));//传给支付宝
                $hb_fq_sxf_3 = $pay_total_amount_3 - $shop_price;//转出来的部分

                $fqfwf_all_6 = ($xy_ra_6 * $shop_price) / 100;//界面显示总服务费
                $total_amount_6 = $fqfwf_all_6 + $shop_price;//界面显示总付款金额
                $total_amount_6_h = ($fqfwf_all_6 + $shop_price) / 6;//每期还款
                $pay_total_amount_6 = $total_amount_6 / (1 + ($base_ra_6 / 100));//传给支付宝
                $hb_fq_sxf_6 = $pay_total_amount_6 - $shop_price;//转出来的部分

                $fqfwf_all_12 = ($xy_ra_12 * $shop_price) / 100;//界面显示总服务费
                $total_amount_12 = $fqfwf_all_12 + $shop_price;//界面显示总付款金额
                $total_amount_12_h = ($fqfwf_all_12 + $shop_price) / 12;//每期还款
                $pay_total_amount_12 = $total_amount_12 / (1 + ($base_ra_12 / 100));//传给支付宝
                $hb_fq_sxf_12 = $pay_total_amount_12 - $shop_price;//转出来的部分

                if ($hb_fq_num == 3) {
                    $fq_data = [
                        'hb_fq_num' => 3,
                        'hb_fq_seller_percent' => $hb_fq_seller_percent,
                        'shop_price' => $shop_price,
                        'hb_mq_h' => number_format($total_amount_3_h, 2, '.', ''),
                        'hb_fq_sxf' => number_format($fqfwf_all_3, 2, '.', ''),
                        'total_amount' => number_format($total_amount_3, 2, '.', ''),
                    ];
                } elseif ($hb_fq_num == 6) {
                    $fq_data = [
                        'hb_fq_num' => 6,
                        'hb_fq_seller_percent' => $hb_fq_seller_percent,
                        'shop_price' => $shop_price,
                        'hb_mq_h' => number_format($total_amount_6_h, 2, '.', ''),
                        'hb_fq_sxf' => number_format($fqfwf_all_6, 2, '.', ''),
                        'total_amount' => number_format($total_amount_6, 2, '.', ''),
                    ];
                } elseif ($hb_fq_num == 12) {
                    $fq_data = [
                        'hb_fq_num' => 12,
                        'hb_fq_seller_percent' => $hb_fq_seller_percent,
                        'shop_price' => $shop_price,
                        'hb_mq_h' => number_format($total_amount_12_h, 2, '.', ''),
                        'hb_fq_sxf' => number_format($fqfwf_all_12, 2, '.', ''),
                        'total_amount' => number_format($total_amount_12, 2, '.', ''),
                    ];
                } else {
                    $fq_data = [
                        [
                            'hb_fq_num' => 3,
                            'hb_fq_seller_percent' => $hb_fq_seller_percent,
                            'shop_price' => $shop_price,
                            'hb_mq_h' => number_format($total_amount_3_h, 2, '.', ''),
                            'hb_fq_sxf' => number_format($fqfwf_all_3, 2, '.', ''),
                            'total_amount' => number_format($total_amount_3, 2, '.', ''),
                        ], 
			[
                            'hb_fq_num' => 6,
                            'hb_fq_seller_percent' => $hb_fq_seller_percent,
                            'shop_price' => $shop_price,
                            'hb_mq_h' => number_format($total_amount_6_h, 2, '.', ''),
                            'hb_fq_sxf' => number_format($fqfwf_all_6, 2, '.', ''),
                            'total_amount' => number_format($total_amount_6, 2, '.', ''),
                        ], 
			[
                            'hb_fq_num' => 12,
                            'hb_fq_seller_percent' => $hb_fq_seller_percent,
                            'shop_price' => $shop_price,
                            'hb_mq_h' => number_format($total_amount_12_h, 2, '.', ''),
                            'hb_fq_sxf' => number_format($fqfwf_all_12, 2, '.', ''),
                            'total_amount' => number_format($total_amount_12, 2, '.', ''),
                        ]
                    ];
                }
            } else {

                //买家自己承担
                $xy_ra_3 = 0;
                $xy_ra_6 = 0;
                $xy_ra_12 = 0;

                if ($hb_fq_num == 3) {
                    $fq_data = [
                        'hb_fq_num' => 3,
                        'hb_fq_seller_percent' => $hb_fq_seller_percent,
                        'shop_price' => $shop_price,
                        'hb_mq_h' => number_format(($shop_price + ($shop_price * $xy_ra_3) / 100) / 3, 2, '.', ''),
                        'hb_fq_sxf' => number_format(($shop_price * $xy_ra_3) / 100, 2, '.', ''),
                        'total_amount' => number_format(($shop_price + ($shop_price * $xy_ra_3) / 100), 2, '.', ''),
                    ];
                } elseif ($hb_fq_num == 6) {
                    $fq_data = [
                        'hb_fq_num' => 6,
                        'hb_fq_seller_percent' => $hb_fq_seller_percent,
                        'shop_price' => $shop_price,
                        'hb_mq_h' => number_format(($shop_price + ($shop_price * $xy_ra_6) / 100) / 6, 2, '.', ''),
                        'hb_fq_sxf' => number_format(($shop_price * $xy_ra_6) / 100, 2, '.', ''),
                        'total_amount' => number_format(($shop_price + ($shop_price * $xy_ra_6) / 100), 2, '.', ''),
                    ];
                } elseif ($hb_fq_num == 12) {
                    $fq_data = [
                        'hb_fq_num' => 12,
                        'hb_fq_seller_percent' => $hb_fq_seller_percent,
                        'shop_price' => $shop_price,
                        'hb_mq_h' => number_format(($shop_price + ($shop_price * $xy_ra_12) / 100) / 12, 2, '.', ''),
                        'hb_fq_sxf' => number_format(($shop_price * $xy_ra_12) / 100, 2, '.', ''),
                        'total_amount' => number_format(($shop_price + ($shop_price * $xy_ra_12) / 100), 2, '.', ''),
                    ];
                } else {
                    $fq_data = [
                        [
                            'hb_fq_num' => 3,
                            'hb_fq_seller_percent' => $hb_fq_seller_percent,
                            'shop_price' => $shop_price,
                            'hb_mq_h' => number_format(($shop_price + ($shop_price * $xy_ra_3) / 100) / 3, 2, '.', ''),
                            'hb_fq_sxf' => number_format(($shop_price * $xy_ra_3) / 100, 2, '.', ''),
                            'total_amount' => number_format(($shop_price + ($shop_price * $xy_ra_3) / 100), 2, '.', ''),
                        ],
			[
                            'hb_fq_num' => 6,
                            'hb_fq_seller_percent' => $hb_fq_seller_percent,
                            'shop_price' => $shop_price,
                            'hb_mq_h' => number_format(($shop_price + ($shop_price * $xy_ra_6) / 100) / 6, 2, '.', ''),
                            'hb_fq_sxf' => number_format(($shop_price * $xy_ra_6) / 100, 2, '.', ''),
                            'total_amount' => number_format(($shop_price + ($shop_price * $xy_ra_6) / 100), 2, '.', ''),
                        ],
			[
                            'hb_fq_num' => 12,
                            'hb_fq_seller_percent' => $hb_fq_seller_percent,
                            'shop_price' => $shop_price,
                            'hb_mq_h' => number_format(($shop_price + ($shop_price * $xy_ra_12) / 100) / 12, 2, '.', ''),
                            'hb_fq_sxf' => number_format(($shop_price * $xy_ra_12) / 100, 2, '.', ''),
                            'total_amount' => number_format(($shop_price + ($shop_price * $xy_ra_12) / 100), 2, '.', ''),
                        ]
                    ];
                }
            }
	    
            $data = [
                'status' => 1,
                'message' => '数据返回成功',
                'data' => $fq_data
            ];

            return json_encode($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    //订单撤销
    public function hb_order_cancel(Request $request)
    {

        try {
            $store_id = $request->get('store_id', '');
            $out_trade_no = $request->get('out_trade_no', '');
            $ways_type = $request->get('ways_type', '');
            $config_id = $request->get('config_id', '');

            $check_data = [
                'store_id' => '门店ID',
                'out_trade_no' => '订单号',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $data = [
                'status' => 1,
                'message' => '订单撤销成功',
                'data' => $request->except('token')
            ];

            return json_encode($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


}
