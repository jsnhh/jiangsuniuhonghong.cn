<?php


namespace App\Api\Controllers\Merchant;


use App\Api\Controllers\BaseController;
use App\Models\MerchantConsumerDetails;
use App\Models\MerchantMonthlyPayment;
use App\Models\MerchantRecharge;
use App\Models\Store;
use App\Models\User;
use App\Models\UserRate;
use App\Models\UserRateProfit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class TransactionDeductionController extends BaseController
{
    public function deduction($data)
    {
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $user_id = $data['user_id'];
        $order_id = $data['order_id'] ?? '0';
        $rate = $data['rate'];//结算费率
        $company = $data['company'];
        $pay_amount = $total_amount;

        //查询是否存在数据，存在不处理
        $existMerchantConsumerDetails = MerchantConsumerDetails::where('merchant_id', $merchant_id)
            ->where('order_id', $order_id)
            ->where('pay_amount', $pay_amount)
            ->first();//获取最新一条数据

        if($existMerchantConsumerDetails){
            Log::info('建行支付分润商户已存在数据---error.  ---order_id:' . $order_id);
            return;
        }

        $MerchantConsumerDetails = MerchantConsumerDetails::where('merchant_id', $merchant_id)
            ->whereIn('type', [1, 2])
            ->orderBy('created_at', 'desc')
            ->first();//获取最新一条数据

        if (empty($MerchantConsumerDetails)) {
            Log::info('建行支付分润商户未获取到充值信息---error.   merchant_id:' . $merchant_id);
            return;
        }
        $MerchantRecharge = MerchantRecharge::where('merchant_id', $merchant_id)
            ->where('status', '1')
            ->whereIn('pay_type', [2, 3])
            ->orderBy('created_at', 'desc')
            ->first();//获取最新一条数据
        if (!empty($MerchantRecharge)) {
            if ($MerchantRecharge->pay_type == '2') {//月包
                $pay_date = date("Y-m-d H:i:s");//交易时间
                $end_date = $MerchantRecharge->end_time;//到期时间
                if (strtotime($end_date) < strtotime($pay_date)) {
                    $amount = ($total_amount * $rate) / 100; //通道佣金
                    if ($amount > 0.001) {
                        $avail_amount = $MerchantConsumerDetails->avail_amount - $amount;
                        $data_in = [
                            'merchant_id' => $merchant_id,
                            'user_id' => $user_id,
                            'pay_amount' => $pay_amount,
                            'amount' => $amount,
                            'avail_amount' => $avail_amount,
                            'type' => '2',
                            'remark' => '交易扣款',
                            'order_id' => $order_id
                        ];
                        MerchantConsumerDetails::create($data_in);
                        //代理分润
                        if ($avail_amount > 0) {
                            $this->userRateDetail($data);
                        }

                    } else {
                        Log::info('代理商赏金-建行通道-赏金小于0-error');
                    }

                }
            } else if ($MerchantRecharge->pay_type == '3') {//季包
                $pay_date = date("Y-m-d H:i:s");//交易时间
                $end_date = $MerchantRecharge->end_time;//到期时间
                if (strtotime($end_date) < strtotime($pay_date)) {
                    $amount = ($total_amount * $rate) / 100; //通道佣金
                    if ($amount > 0.001) {
                        $avail_amount = $MerchantConsumerDetails->avail_amount - $amount;
                        $data_in = [
                            'merchant_id' => $merchant_id,
                            'user_id' => $user_id,
                            'pay_amount' => $pay_amount,
                            'amount' => $amount,
                            'avail_amount' => $avail_amount,
                            'type' => '2',
                            'remark' => '交易扣款',
                            'order_id' => $order_id
                        ];
                        MerchantConsumerDetails::create($data_in);
                        //代理分润
                        if ($avail_amount > 0) {
                            $this->userRateDetail($data);
                        }

                    } else {
                        Log::info('代理商赏金-建行通道-赏金小于0-error');
                    }
                }
            }

        } else {
            $MerchantRecharge = MerchantRecharge::where('merchant_id', $merchant_id)
                ->where('status', '1')
                ->where('pay_type', '1')
                ->orderBy('created_at', 'desc')
                ->first();//获取最新一条数据

            if (!empty($MerchantRecharge)) {
                $amount = ($total_amount * $rate) / 100; //通道佣金
                if ($amount > 0.001) {
                    if (!$MerchantConsumerDetails) {
                        $avail_amount = 0.00;
                    } else {
                        $avail_amount = $MerchantConsumerDetails->avail_amount;//获取可用余额
                    }

                    $avail_amount = $avail_amount - $amount;
                    $data_in = [
                        'merchant_id' => $merchant_id,
                        'user_id' => $user_id,
                        'pay_amount' => $pay_amount,
                        'amount' => $amount,
                        'avail_amount' => $avail_amount,
                        'type' => '2',
                        'remark' => '交易扣款',
                        'order_id' => $order_id
                    ];
                    MerchantConsumerDetails::create($data_in);
                    //代理分润
                    if ($avail_amount <= 0) {//商户余额小于0代理不分润
                        Log::info('商户余额小于0代理不分润');
                        return;
                    }

                    $this->userRateDetail($data);

                    if ($avail_amount <= 0) {//余额为负的业务处理

                    }
                } else {
                    Log::info('代理商赏金-建行通道-赏金小于0-error');
                }

            } else {
                Log::info('建行支付分润商户未获取到充值信息---error.   merchant_id:' . $merchant_id);
            }
        }
        return [
            'status' => '1',
            'message' => '交易扣款成功'
        ];
    }

    //获取商户可用余额
    public function getStoreBalance(Request $request)
    {
        $merchant_id = $request->get('merchant_id', "");

        if (!$merchant_id) {
            return json_encode([
                'status' => '2',
                'msg' => '商户id不能为空'
            ]);
        }
        $MerchantConsumerDetails = MerchantConsumerDetails::where('merchant_id', $merchant_id)
            ->whereIn('type', [1, 2])
            ->orderBy('created_at', 'desc')
            ->first();//获取最新一条数据
        if (!$MerchantConsumerDetails) {
            return json_encode([
                'status' => '1',
                'avail_amount' => 0.00
            ]);
        } else {
            return json_encode([
                'status' => '1',
                'avail_amount' => $MerchantConsumerDetails->avail_amount
            ]);
        }
    }

    public function getMonthAndSeasonPackage(Request $request)
    {
        $merchant_id = $request->get('merchant_id', "");

        if (!$merchant_id) {
            return json_encode([
                'status' => '2',
                'msg' => '商户id不能为空'
            ]);
        }
        $monthMerchantRecharge = MerchantRecharge::where('merchant_id', $merchant_id)
            ->where('pay_type', '2')
            ->where('status', '1')
            ->orderBy('created_at', 'desc')
            ->first();//获取月包最新一条数据
        $data = [];
        if($monthMerchantRecharge){
            $data['month_package']=$monthMerchantRecharge;
            $data['month_package_status']='1';
        }else{
            $data['month_package']='';
            $data['month_package_status']='2';
        }
        $seasonMerchantRecharge = MerchantRecharge::where('merchant_id', $merchant_id)
            ->where('pay_type', '3')
            ->where('status', '1')
            ->orderBy('created_at', 'desc')
            ->first();//获取季包最新一条数据
        if($seasonMerchantRecharge){
            $data['season_package']=$seasonMerchantRecharge;
            $data['season_package_status']='1';
        }else{
            $data['season_package']='';
            $data['season_package_status']='2';
        }
        return json_encode([
            'status' => '1',
            'data' => $data
        ]);
    }

    //获取交易记录
    public function getTransaction(Request $request)
    {
        try {
            $merchant_id = $request->get('merchant_id', '');
            $type = $request->get('type', '');
            $where = [];
            if ($merchant_id) {
                $where[] = ['merchant_id', '=', $merchant_id];
            }
            if ($type) {
                if ($type != '0') {
                    $where[] = ['type', '=', $type];
                }

            }
            $obj = DB::table('merchant_consumer_details');
            $obj = $obj->where($where)
                ->whereIn('type', [1, 2])
                ->orderBy('created_at', 'desc');
            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage() . $exception->getLine();
            return $this->format();
        }


    }

    public function getMerchantMonthlyPayments(Request $request)
    {
        $userToken = $this->parseToken();
        $merchant_id = $request->get('merchant_id', "");
        if (!$merchant_id) {
            return json_encode([
                'status' => '2',
                'msg' => '商户id不能为空'
            ]);
        }
        $store = Store::where('merchant_id', $merchant_id)->first();
        if (!$store) {
            return json_encode([
                'status' => '2',
                'msg' => '门店信息不存在'
            ]);
        }

        $MerchantMonthlyPayment = MerchantMonthlyPayment::where('merchant_id', $merchant_id)
            ->where('user_id', $store->user_id)->first();
        if ($MerchantMonthlyPayment) {

            return json_encode([
                'status' => '1',
                'data' => $MerchantMonthlyPayment
            ]);
        } else {
            return json_encode([
                'status' => '1',
                'data' => [
                    'amount' => 0,
                    'quarterly_amount' => 0
                ]

            ]);
        }

    }

    //商户所属代理分润-月包、季包
    public function userProfit($user_id, $amount, $merchant_id, $pay_amount, $order_id)
    {
        $user = User::where('id', $user_id)->first();
        if (!$user) {
            return;
        }
        $pid = $user->pid;
        if ($pid == 0) { //顶级代理
            $profit = 100;
        } else {
            $UserRateProfit = UserRateProfit::where('user_id', $user_id)->first();
            if ($UserRateProfit) {
                $profit = $UserRateProfit->profit;//分润比例
            } else {
                //当前代理没有设置分润比例
//                $userRateReturn = $this->userRateProfitForeach($pid);//循环
//                if ($userRateReturn['status'] == '1') {
//                    $profit = $userRateReturn['data']['profit'];
//                    $user_id = $userRateReturn['data']['user_id'];
//                }
                $profit = 100;
            }
        }
        $profit_amount = $amount * $profit / 100;//分润金额
        $MerchantConsumerDetails = MerchantConsumerDetails::where('user_id', $user_id)
            ->where('type', '3')
            ->orderBy('created_at', 'desc')
            ->first();//获取最新一条数据
        if (!$MerchantConsumerDetails) {
            $total_profit_amount = $profit_amount;//代理总分润
        } else {
            $total_profit_amount = $MerchantConsumerDetails->total_profit_amount + $profit_amount;//代理总分润
        }
        $total_profit_amount = number_format($total_profit_amount, 4,'.','');
        $profit_data = [
            'user_id' => $user_id,
            'merchant_id' => $merchant_id,
            'pay_amount' => $pay_amount,
            'amount' => $amount,
            'profit' => $profit,
            'profit_amount' => $profit_amount,
            'type' => '3',
            'remark' => '商户交易产生的分润',
            'total_profit_amount' => $total_profit_amount,
            'order_id' => $order_id
        ];
        MerchantConsumerDetails::create($profit_data);
        //循环上级分润
        $existUserRateProfit = UserRateProfit::where('user_id', $user_id)->first();
        if ($existUserRateProfit) {
            $this->userProfitForeach($user_id, $amount, $merchant_id, $pay_amount, $order_id);
        }
    }

    //上级代理分润-月包、季包
    public function userProfitForeach($user_id, $amount, $merchant_id, $pay_amount, $order_id)
    {

        $user = User::where('id', $user_id)->first();
        if (!$user) {
            return;
        }
        $pid = $user->pid;

        if ($pid == 0) { //顶级代理
            return;
        } else {
            $UserRateProfit = UserRateProfit::where('user_id', $user_id)->first();
            $user_pid = $UserRateProfit->pid;

            $newUser = User::where('id', $user_pid)->first();

            if ($newUser->pid == 0) { //顶级代理
                $p_profit = 100;
            } else {
                $pUserRateProfit = UserRateProfit::where('user_id', $user_pid)->first();//获取上级分润比例
                if ($pUserRateProfit) {
                    $p_profit = $pUserRateProfit->profit;
                }
            }
        }

        $p_profit = $p_profit - $UserRateProfit->profit;//上级分润比例
        $p_profit_amount = $amount * $p_profit / 100;//上级分润金额
        $MerchantConsumerDetails = MerchantConsumerDetails::where('user_id', $user_pid)
            ->where('type', '3')
            ->orderBy('created_at', 'desc')
            ->first();//获取最新一条数据
        if (!$MerchantConsumerDetails) {
            $total_profit_amount = $p_profit_amount;//代理总分润
        } else {
            $total_profit_amount = $MerchantConsumerDetails->total_profit_amount + $p_profit_amount;//代理总分润
        }

        $total_profit_amount = number_format($total_profit_amount, 4, '.', '');
        $data_in = [
            'user_id' => $user_pid,
            'merchant_id' => $merchant_id,
            'pay_amount' => $pay_amount,
            'amount' => $amount,
            'profit' => $p_profit,
            'profit_amount' => $p_profit_amount,
            'type' => '3',
            'remark' => '商户交易产生的分润',
            'total_profit_amount' => $total_profit_amount,
            'order_id' => $order_id
        ];
        MerchantConsumerDetails::create($data_in);
        $this->userProfitForeach($user_pid, $amount, $merchant_id, $pay_amount, $order_id);//循环

    }

    //循环上级设置比例-月包、季包
    public function userRateProfitForeach($user_id)
    {
        $user = User::where('id', $user_id)->first();

        $pid = $user->pid;

        if ($pid == 0) {

            $res_arr = [
                'profit' => 100,
                'user_id' => $user->id
            ];

            return [
                'status' => '1',
                'message' => 'success',
                'data' => $res_arr
            ];
        }
        $UserRateProfit = UserRateProfit::where('user_id', $pid)->first();
        if ($UserRateProfit) {
            $res_arr = [
                'profit' => $UserRateProfit->profit,
                'user_id' => $UserRateProfit->user_id,
            ];
            return [
                'status' => '1',
                'message' => 'success',
                'data' => $res_arr
            ];
        } else {
            return $this->userRateProfitForeach($pid);//循环
        }
    }

    //商户所属代理分润
    public function userRateDetail($data){

        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $user_id = $data['user_id'];
        $order_id = $data['order_id'] ?? '0';
        $rate = $data['rate'];//结算费率
        $company = $data['company'];

        $amount = $total_amount * $rate / 100;

        //顶级代理分润入库
        $pUserRateReturn = $this->getTopUserRate($user_id,$company);

        if ($pUserRateReturn['status'] == '1') {
            $p_rate = $pUserRateReturn['data']['p_rate'];
            $newUserId = $pUserRateReturn['data']['user_id'];

            $profit_amount = $total_amount * $p_rate / 100;

            $MerchantConsumerDetails = MerchantConsumerDetails::where('user_id', $newUserId)
                ->where('type', '3')
                ->orderBy('created_at', 'desc')
                ->first();//获取最新一条数据
            if (!$MerchantConsumerDetails) {
                $total_profit_amount = $profit_amount;//代理总分润
            } else {
                $total_profit_amount = $MerchantConsumerDetails->total_profit_amount + $profit_amount;//代理总分润
            }

            $total_profit_amount = number_format($total_profit_amount, 4, '.', '');
            $profit_data = [
                'user_id' => $newUserId,
                'merchant_id' => $merchant_id,
                'pay_amount' => $total_amount,
                'amount' => $amount,
                'profit_amount' => $profit_amount,
                'type' => '3',
                'remark' => '商户交易产生的分润',
                'total_profit_amount' => $total_profit_amount,
                'order_id' => $order_id,
                'rate' => $p_rate
            ];
            MerchantConsumerDetails::create($profit_data);

        }else{
            Log::info('赏金入库-顶级代理分润入库--不存在');
        }

        //代理分润
        $UserRateProfit = UserRateProfit::where('user_id', $user_id)->first();
        if($UserRateProfit){
            $level = $UserRateProfit->level; //商户直接归属的代理商等级

            $merchantRate = $rate;
            $sub_user_ids = $this->getSubUserIds($user_id, $level);
            for ($i = 0; $i < count($sub_user_ids); $i++) {

                $userId = $sub_user_ids[$i];

                $userRate = UserRate::where('user_id', $userId)
                    ->where('company','ccbankpay')
                    ->where('status',1)
                    ->first();

                if($userRate){
                    $user_rate = $userRate->rate;
                    Log::info('---------计算上级代理分润---------user_rate:'.$user_rate);
                }else{
                    Log::info('代理商佣金入库-userRate-未查到');
                    continue;
                }

                $new_rate = $merchantRate - $user_rate;
                $profitAmount = $total_amount * $new_rate / 100;

                $merchantRate = $user_rate; //重新赋值计算上级代理分润

                // Log::info('---------重新赋值计算上级代理分润---------rate:'.$merchantRate);

                $MerchantConsumerDetails = MerchantConsumerDetails::where('user_id', $userId)
                    ->where('type', '3')
                    ->orderBy('created_at', 'desc')
                    ->first();//获取最新一条数据
                if (!$MerchantConsumerDetails) {
                    $total_profit_amount = $profitAmount;//代理总分润
                } else {
                    $total_profit_amount = $MerchantConsumerDetails->total_profit_amount + $profitAmount;//代理总分润
                }

                $total_profit_amount = number_format($total_profit_amount, 4, '.', '');
                $profit_data = [
                    'user_id' => $userId,
                    'merchant_id' => $merchant_id,
                    'pay_amount' => $total_amount,
                    'amount' => $amount,
                    'profit_amount' => $profitAmount,
                    'type' => '3',
                    'remark' => '商户交易产生的分润',
                    'total_profit_amount' => $total_profit_amount,
                    'order_id' => $order_id,
                    'rate' => $new_rate
                ];
                MerchantConsumerDetails::create($profit_data);

            }

        }else{

            $profitAmount = $total_amount * $rate / 100;

            $MerchantConsumerDetails = MerchantConsumerDetails::where('user_id', $user_id)
                ->where('type', '3')
                ->orderBy('created_at', 'desc')
                ->first();//获取最新一条数据
            if (!$MerchantConsumerDetails) {
                $total_profit_amount = $profitAmount;//代理总分润
            } else {
                $total_profit_amount = $MerchantConsumerDetails->total_profit_amount + $profitAmount;//代理总分润
            }

            $total_profit_amount = number_format($total_profit_amount, 4,'.','');
            $profit_data = [
                'user_id' => $user_id,
                'merchant_id' => $merchant_id,
                'pay_amount' => $total_amount,
                'amount' => $amount,
                'profit_amount' => $profitAmount,
                'type' => '3',
                'remark' => '商户交易产生的分润',
                'total_profit_amount' => $total_profit_amount,
                'order_id' => $order_id,
                'rate' => $rate
            ];
            MerchantConsumerDetails::create($profit_data);

        }

    }

    //商户所属代理分润(产生负金额，充值后的操作，充值金额/商户费率*支付成本)
    public function payUserRateDetail($data){

        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount']; //100
        $user_id = $data['user_id'];
        $order_id = $data['order_id'] ?? '0';
        $rate = $data['rate'];//结算费率  0.1
        $company = $data['company'];

        if($rate < 1){ //给上级分润
            $amount_rate = $total_amount / $rate; // 100 / 0.2 = 500
            //顶级代理分润入库
            $pUserRateReturn = $this->getTopUserRate($user_id,$company);
            if ($pUserRateReturn['status'] == '1') { //顶级代理分润
                $p_rate = $pUserRateReturn['data']['p_rate']; //0.04
                $newUserId = $pUserRateReturn['data']['user_id'];

                $profit_amount = $amount_rate * $p_rate; // 100/0.2 * 0.04 = 20

                $MerchantConsumerDetails = MerchantConsumerDetails::where('user_id', $newUserId)
                    ->where('type', '3')
                    ->orderBy('created_at', 'desc')
                    ->first();//获取最新一条数据
                if (!$MerchantConsumerDetails) {
                    $total_profit_amount = $profit_amount;//代理总分润
                } else {
                    $total_profit_amount = $MerchantConsumerDetails->total_profit_amount + $profit_amount;//代理总分润
                }

                $total_profit_amount = number_format($total_profit_amount, 4, '.', '');
                $profit_data = [
                    'user_id' => $newUserId,
                    'merchant_id' => $merchant_id,
                    'pay_amount' => $total_amount,
                    'amount' => $total_amount,
                    'profit_amount' => $profit_amount,
                    'type' => '3',
                    'remark' => '商户交易产生的分润',
                    'total_profit_amount' => $total_profit_amount,
                    'order_id' => $order_id,
                    'rate' => $p_rate/$rate
                ];
                MerchantConsumerDetails::create($profit_data);

            }else{
                Log::info('赏金入库-顶级代理分润入库--不存在');
            }

            //代理分润
            $UserRateProfit = UserRateProfit::where('user_id', $user_id)->first();
            if($UserRateProfit){

                $level = $UserRateProfit->level; //商户直接归属的代理商等级

                $merchantRate = $rate;
                $sub_user_ids = $this->getSubUserIds($user_id, $level);
                for ($i = 0; $i < count($sub_user_ids); $i++) {

                    $userId = $sub_user_ids[$i];

                    $userRate = UserRate::where('user_id', $userId)
                        ->where('company','ccbankpay')
                        ->where('status',1)
                        ->first();

                    if($userRate){
                        $user_rate = $userRate->rate;
                        Log::info('---------计算上级代理分润---------user_rate:'.$user_rate);
                    }else{
                        Log::info('代理商佣金入库-userRate-未查到');
                        continue;
                    }

                    $new_rate = $merchantRate - $user_rate;
                    $profitAmount = $amount_rate * $new_rate;  // 100 / 0.2 * (0.2-0.04) = 80

                    $merchantRate = $user_rate; //重新赋值计算上级代理分润

                    // Log::info('---------重新赋值计算上级代理分润---------rate:'.$merchantRate);

                    $MerchantConsumerDetails = MerchantConsumerDetails::where('user_id', $userId)
                        ->where('type', '3')
                        ->orderBy('created_at', 'desc')
                        ->first();//获取最新一条数据
                    if (!$MerchantConsumerDetails) {
                        $total_profit_amount = $profitAmount;//代理总分润
                    } else {
                        $total_profit_amount = $MerchantConsumerDetails->total_profit_amount + $profitAmount;//代理总分润
                    }

                    $total_profit_amount = number_format($total_profit_amount, 4, '.', '');
                    $profit_data = [
                        'user_id' => $userId,
                        'merchant_id' => $merchant_id,
                        'pay_amount' => $total_amount,
                        'amount' => $total_amount,
                        'profit_amount' => $profitAmount,
                        'type' => '3',
                        'remark' => '商户交易产生的分润',
                        'total_profit_amount' => $total_profit_amount,
                        'order_id' => $order_id,
                        'rate' => $new_rate/$rate
                    ];
                    MerchantConsumerDetails::create($profit_data);

                }

            }else{

                $profitAmount = $total_amount * $rate ;

                $MerchantConsumerDetails = MerchantConsumerDetails::where('user_id', $user_id)
                    ->where('type', '3')
                    ->orderBy('created_at', 'desc')
                    ->first();//获取最新一条数据
                if (!$MerchantConsumerDetails) {
                    $total_profit_amount = $profitAmount;//代理总分润
                } else {
                    $total_profit_amount = $MerchantConsumerDetails->total_profit_amount + $profitAmount;//代理总分润
                }

                $total_profit_amount = number_format($total_profit_amount, 4,'.','');
                $profit_data = [
                    'user_id' => $user_id,
                    'merchant_id' => $merchant_id,
                    'pay_amount' => $total_amount,
                    'amount' => $total_amount,
                    'profit_amount' => $profitAmount,
                    'type' => '3',
                    'remark' => '商户交易产生的分润',
                    'total_profit_amount' => $total_profit_amount,
                    'order_id' => $order_id,
                    'rate' => $rate
                ];
                MerchantConsumerDetails::create($profit_data);

            }

        }else{
            $profitAmount = $total_amount * $rate ;

            $MerchantConsumerDetails = MerchantConsumerDetails::where('user_id', $user_id)
                ->where('type', '3')
                ->orderBy('created_at', 'desc')
                ->first();//获取最新一条数据
            if (!$MerchantConsumerDetails) {
                $total_profit_amount = $profitAmount;//代理总分润
            } else {
                $total_profit_amount = $MerchantConsumerDetails->total_profit_amount + $profitAmount;//代理总分润
            }

            $total_profit_amount = number_format($total_profit_amount, 4,'.','');
            $profit_data = [
                'user_id' => $user_id,
                'merchant_id' => $merchant_id,
                'pay_amount' => $total_amount,
                'amount' => $total_amount,
                'profit_amount' => $profitAmount,
                'type' => '3',
                'remark' => '商户交易产生的分润',
                'total_profit_amount' => $total_profit_amount,
                'order_id' => $order_id,
                'rate' => $rate
            ];
            MerchantConsumerDetails::create($profit_data);
        }

    }

    //获取分润代理IDs
    public function getSubUserIds($userId, $level){
        $userIDs = [];
        for ($x = $level; $x >= 1; $x--) {
            $UserRateProfit = UserRateProfit::where('user_id', $userId)->first();
            $userId = $UserRateProfit->pid;

            array_push($userIDs, $UserRateProfit->user_id);
        }
        return $userIDs;
    }

    //获取顶级代理支付成本
    public function getTopUserRate($userId, $company){
        try {
            $UserRateProfit = UserRateProfit::where('user_id', $userId)->first();
            $p_rate = 0.04; //默认支付成本
            if ($UserRateProfit) {
                $pid = $UserRateProfit->pid;
                $level = $UserRateProfit->level;
                $new_company = $company;

                if($level == 1){
                    $userRate=UserRate::where('user_id',$userId)
                        ->where('company',$new_company)
                        ->where('status',1)
                        ->first();

                    if($userRate){
                        $p_rate = $userRate->rate;//结算成本
                    }
                    $res_arr = [
                        'p_rate' => $p_rate,
                        'user_id' => $pid
                    ];
                    return [
                        'status' => '1',
                        'message' => 'success',
                        'data' => $res_arr
                    ];
                }else{
                    return $this->getTopUserRate($pid, $new_company);
                }
            }else{
                return [
                    'status' => '-1',
                    'message' => 'success'
                ];
            }
        } catch (\Exception $exception) {
            Log::info('获取顶级代理支付成本-进库-error');
            Log::info($exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine());
        }
    }

    //获取下月的今天
    function getNextMonthCurDayOrLastDay($date)
    {
        //$date = date('Y-m-d H:i:s');
        // 先判断下个月有没有当前月份的今天，有的话下个月取今天，没有的话下个月取最后一天
        $curDay = date('d', strtotime($date));
        $lastDayOfNextMonth = date('d', strtotime("last day of next month", strtotime($date)));
        if ($curDay <= $lastDayOfNextMonth) {
            $retDate = date('Y-m-d H:i:s', strtotime("+1 month", strtotime(($date))));
        } else {
            $retDate = date('Y-m-d H:i:s', strtotime("last day of next month", strtotime($date)));
        }
        return $retDate;
    }

    //获取季度末的今天
    function getQuarterCurDayOrLastDay($date)
    {
        //$date = date('Y-m-d H:i:s');
        // 先判断下个月有没有当前月份的今天，有的话下个月取今天，没有的话下个月取最后一天
        $curDay = date('d', strtotime($date));
        $lastDayOfNextMonth = date('d', strtotime("last day of next month", strtotime($date)));
        if ($curDay <= $lastDayOfNextMonth) {
            $retDate = date('Y-m-d H:i:s', strtotime("+3 month", strtotime(($date))));
        } else {
            $retDate = date('Y-m-d H:i:s', strtotime("last day of next month", strtotime($date)));
        }
        return $retDate;
    }

}