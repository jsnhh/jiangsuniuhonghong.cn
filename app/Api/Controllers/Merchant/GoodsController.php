<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/8/28
 * Time: 16:15
 */
namespace App\Api\Controllers\Merchant;


use App\Api\Controllers\BaseController;
use App\Models\Goods;
use App\Models\GoodsAttribute;
use App\Models\GoodsCategory;
use App\Models\GoodsStandard;
use App\Models\GoodsTag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GoodsController extends BaseController
{

    //新增or更新商品分类
    public function updateGoodsCategory(Request $request)
    {
        $cid = $request->post('cid', ''); //分类id,更新必传
        $pid = $request->post('pid', ''); //上级分类id
        $store_id = $request->post('storeId', ''); //门店id
        $name = $request->post('name', ''); //分类名称，必传
        $status = $request->post('status', ''); //1-正常,2-已废弃
        $sort = $request->post('sort', '');

        $check_data = [
            'name' => '分类名称',
            'storeId' => '门店id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = -1;
            $this->message = $check;
            return $this->format();
        }

        $data = [
            'name' => $name,
            'store_id' => $store_id
        ];

        if (($pid === '0') || $pid) $data['pid'] = $pid;
        if ($status) $data['status'] = $status;
        if ($sort) $data['sort'] = $sort;

        try {
            if ($cid) {
                $obj = GoodsCategory::find($cid);
                $res = $obj->update($data);
                if ($res) {
                    $this->status = 1;
                    $this->message = '更新成功';
                    return $this->format();
                } else {
                    $this->status = 2;
                    $this->message = '更新失败';
                    return $this->format();
                }
            } else {
                $res = GoodsCategory::create($data);
                if ($res) {
                    $this->status = 1;
                    $this->message = '添加成功';
                    return $this->format($res);
                } else {
                    $this->status = 2;
                    $this->message = '添加失败';
                    return $this->format();
                }
            }
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //删除商品分类
    public function delGoodsCategory(Request $request)
    {
        $cid = $request->post('cid', ''); //分类id,必传

        $check_data = [
            'cid' => '分类id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = -1;
            $this->message = $check;
            return $this->format();
        }

        try {
            $res = GoodsCategory::find($cid)->delete();
            if ($res) {
                $this->status = 1;
                $this->message = '删除成功';
                return $this->format();
            } else {
                $this->status = 2;
                $this->message = '删除失败';
                return $this->format();
            }
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //商品分类列表
    public function goodsCategoryList(Request $request)
    {
        $pid = $request->get('pid', 0); //上级分类id
        $store_id = $request->get('storeId', ''); //门店id
        $name = $request->get('name', ''); //分类名称，必传
        $status = $request->get('status', ''); //1-正常,2-已废弃
        $sort_method = $request->get('sortMethod', 'ASC'); //排序方式

        $check_data = [
            'storeId' => '门店id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = 2;
            $this->message = $check;
            return $this->format();
        }

        $where = [];
        if ($pid || ($pid === '0')) {
            $where[] = ['pid', '=', $pid];
        }

        if ($name) {
            $where[] = ['name', '=', $name];
        }

        if ($store_id) {
            $where[] = ['store_id', '=', $store_id];
        }

        if ($status) {
            $where[] = ['status', '=', $status];
        }

        try {
            $data = GoodsCategory::where($where)
                ->where(['store_id' => $store_id])
                ->with('children')
                ->orderBy('sort', $sort_method)
                ->get()
                ->toArray();

            if (empty($data)) {
                $model = new GoodsCategory();
                $data = $model->getDefaultData();
            }

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //新增or更新商品属性
    public function updateGoodsAttribute(Request $request)
    {
        $id = $request->post('id', ''); //id,更新必传
        $store_id = $request->post('storeId', ''); //门店id
        $name = $request->post('name', ''); //商品属性名称，必传
        $status = $request->post('status', ''); //1-正常,0-废弃
        $sort = $request->post('sort', '');

        $check_data = [
            'name' => '商品属性名称',
            'storeId' => '门店id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = -1;
            $this->message = $check;
            return $this->format();
        }

        $data = [
            'name' => $name,
            'store_id' => $store_id
        ];

        if ($status) $data['status'] = $status;
        if ($sort) $data['sort'] = $sort;

        try {
            if ($id) {
                $obj = GoodsAttribute::find($id);
                $res = $obj->update($data);
                if ($res) {
                    $this->status = 1;
                    $this->message = '更新成功';
                    return $this->format();
                } else {
                    $this->status = 2;
                    $this->message = '更新失败';
                    return $this->format();
                }
            } else {
                $res = GoodsAttribute::create($data);
                if ($res) {
                    $this->status = 1;
                    $this->message = '添加成功';
                    return $this->format($res);
                } else {
                    $this->status = 2;
                    $this->message = '添加失败';
                    return $this->format();
                }
            }
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //删除商品属性
    public function delGoodsAttribute(Request $request)
    {
        $id = $request->post('id', ''); //id,必传

        $check_data = [
            'id' => '商品属性id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = -1;
            $this->message = $check;
            return $this->format();
        }

        try {
            $res = GoodsAttribute::find($id)->delete();
            if ($res) {
                $this->status = 1;
                $this->message = '删除成功';
                return $this->format();
            } else {
                $this->status = 2;
                $this->message = '删除失败';
                return $this->format();
            }
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //商品属性列表
    public function goodsAttributeList(Request $request)
    {
        $name = $request->get('name', ''); //属性名称
        $store_id = $request->get('storeId', ''); //门店id
        $status = $request->get('status', '1'); //1-使用,2-禁用
        $sort_method = $request->get('sortMethod', 'ASC'); //排序方式

        $where = [];

        if ($name) {
            $where[] = ['name', '=', $name];
        }

        if ($store_id) {
            $where[] = ['store_id', '=', $store_id];
        }

        if ($status) {
            $where[] = ['status', '=', $status];
        }

        try {
            $data = GoodsAttribute::where($where)
                ->orderBy('sort', $sort_method)
                ->get()
                ->toArray();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //新增or更新商品规格
    public function updateGoodsStandard(Request $request)
    {
        $id = $request->post('id', ''); //id,更新必传
        $store_id = $request->post('storeId', ''); //门店id
        $name = $request->post('name', ''); //商品规格名称，必传
        $status = $request->post('status', ''); //1-正常,0-废弃
        $sort = $request->post('sort', '');

        $check_data = [
            'name' => '商品规格名称',
            'storeId' => '门店id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = -1;
            $this->message = $check;
            return $this->format();
        }

        $data = [
            'name' => $name,
            'store_id' => $store_id
        ];

        if ($status) $data['status'] = $status;
        if ($sort) $data['sort'] = $sort;

        try {
            if ($id) {
                $obj = GoodsStandard::find($id);
                $res = $obj->update($data);
                if ($res) {
                    $this->status = 1;
                    $this->message = '更新成功';
                    return $this->format();
                } else {
                    $this->status = 2;
                    $this->message = '更新失败';
                    return $this->format();
                }
            } else {
                $res = GoodsStandard::create($data);
                if ($res) {
                    $this->status = 1;
                    $this->message = '添加成功';
                    return $this->format($res);
                } else {
                    $this->status = 2;
                    $this->message = '添加失败';
                    return $this->format();
                }
            }
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //删除商品规格
    public function delGoodsStandard(Request $request)
    {
        $id = $request->post('id', ''); //id,必传

        $check_data = [
            'id' => '商品规格id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = -1;
            $this->message = $check;
            return $this->format();
        }

        try {
            $res = GoodsStandard::find($id)->delete();
            if ($res) {
                $this->status = 1;
                $this->message = '删除成功';
                return $this->format();
            } else {
                $this->status = 2;
                $this->message = '删除失败';
                return $this->format();
            }
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //商品规格列表
    public function goodsStandardList(Request $request)
    {
        $name = $request->get('name', ''); //属性名称
        $store_id = $request->get('storeId', ''); //门店id
        $status = $request->get('status', '1'); //1-使用,2-禁用
        $sort_method = $request->get('sortMethod', 'ASC'); //排序方式

        $where = [];

        if ($name) {
            $where[] = ['name', '=', $name];
        }

        if ($store_id) {
            $where[] = ['store_id', '=', $store_id];
        }

        if ($status) {
            $where[] = ['status', '=', $status];
        }

        try {
            $data = GoodsStandard::where($where)
                ->orderBy('sort', $sort_method)
                ->get()
                ->toArray();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //新增or更新商品标签
    public function updateGoodsTag(Request $request)
    {
        $id = $request->post('id', ''); //id,更新必传
        $store_id = $request->post('storeId', ''); //门店id
        $name = $request->post('name', ''); //商品标签名称，必传
        $status = $request->post('status', ''); //1-正常,0-废弃
        $sort = $request->post('sort', '');

        $check_data = [
            'name' => '商品标签名称',
            'storeId' => '门店id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = -1;
            $this->message = $check;
            return $this->format();
        }

        $data = [
            'name' => $name,
            'store_id' => $store_id
        ];

        if ($status) $data['status'] = $status;
        if ($sort) $data['sort'] = $sort;

        try {
            if ($id) {
                $obj = GoodsTag::find($id);
                $res = $obj->update($data);
                if ($res) {
                    $this->status = 1;
                    $this->message = '更新成功';
                    return $this->format();
                } else {
                    $this->status = 2;
                    $this->message = '更新失败';
                    return $this->format();
                }
            } else {
                $res = GoodsTag::create($data);
                if ($res) {
                    $this->status = 1;
                    $this->message = '添加成功';
                    return $this->format($res);
                } else {
                    $this->status = 2;
                    $this->message = '添加失败';
                    return $this->format();
                }
            }
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //删除商品标签
    public function delGoodsTag(Request $request)
    {
        $id = $request->post('id', ''); //id,必传

        $check_data = [
            'id' => '商品标签id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = -1;
            $this->message = $check;
            return $this->format();
        }

        try {
            $res = GoodsTag::find($id)->delete();
            if ($res) {
                $this->status = 1;
                $this->message = '删除成功';
                return $this->format();
            } else {
                $this->status = 2;
                $this->message = '删除失败';
                return $this->format();
            }
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //商品标签列表
    public function goodsTagList(Request $request)
    {
        $name = $request->get('name', ''); //属性名称
        $store_id = $request->get('storeId', ''); //门店id
        $status = $request->get('status', '1'); //1-使用,2-禁用
        $sort_method = $request->get('sortMethod', 'ASC'); //排序方式

        $where = [];

        if ($name) {
            $where[] = ['name', '=', $name];
        }

        if ($store_id) {
            $where[] = ['store_id', '=', $store_id];
        }

        if ($status) {
            $where[] = ['status', '=', $status];
        }

        try {
            $data = GoodsTag::where($where)
                ->orderBy('sort', $sort_method)
                ->get()
                ->toArray();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //新增or更新商品
    public function updateGoods(Request $request)
    {
        $token = $this->parseToken();
        $id = $request->post('id', ''); //商品id,更新必传
        $cid = $request->post('cid', ''); //分类id
        $bar_code = $request->post('barCode', ''); //商品条码
        $name = $request->post('name', ''); //商品名称
        $store_id = $request->post('storeId', ''); //门店id
        $price = $request->post('price', ''); //价格
        $main_image = $request->post('mainImage', ''); //产品主图
        $sub_images = $request->post('subImages', ''); //图片地址,json格式
        $detail = $request->post('detail', ''); //商品详情
        $subtitle = $request->post('subtitle', ''); //商品副标题,描述
        $tag_ids = $request->post('tagIds', ''); //商品标签id集,多个用,隔开
        $standard_ids = $request->post('standardIds', ''); //商品规格id集,多个用,隔开
        $attribute_ids = $request->post('attributeIds', ''); //商品属性id集,多个用,隔开
        $unit = $request->post('unit', ''); //商品单位
        $is_recommend = $request->post('isRecommend', ''); //是否推荐
        $stock = $request->post('stock', ''); //库存数量
        $status = $request->post('status', ''); //商品状态.1-在售 2-下架 3-删除
        $sort = $request->post('sort', ''); //排序权重,1--99,顺序

        $check_data = [
            'name' => '商品名称',
            'storeId' => '门店id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = -1;
            $this->message = $check;
            return $this->format();
        }

        $data = [
            'name' => $name,
            'store_id' => $store_id
        ];
        if ($cid) {
            $cateogry_info = GoodsCategory::find($cid);
            if (!$cateogry_info) {
                $this->status = 2;
                $this->message = '该商品分类不存在';
                return $this->format();
            }
            $data['cid'] = $cid;
            $data['c_name'] = $cateogry_info->name;
        }
        if ($bar_code) $data['bar_code'] = $bar_code;
        if ($price) $data['price'] = $price;
        if ($main_image) $data['main_image'] = $main_image;
        if ($sub_images) $data['sub_images'] = $sub_images;
        if ($detail) $data['detail'] = $detail;
        if ($subtitle) $data['subtitle'] = $subtitle;
        if ($tag_ids) $data['tag_ids'] = $tag_ids;
        if ($standard_ids) $data['standard_ids'] = $standard_ids;
        if ($attribute_ids) $data['attribute_ids'] = $attribute_ids;
        if ($unit) $data['unit'] = $unit;
        if ($is_recommend || $is_recommend === 0 || $is_recommend === '0') $data['is_recommend'] = $is_recommend;
        if ($stock) $data['stock'] = $stock;
        if ($status) $data['status'] = $status;
        if ($sort) $data['sort'] = $sort;

        try {
            if ($id) {
                $obj = Goods::find($id);
                $res = $obj->update($data);
                if ($res) {
                    $this->status = 1;
                    $this->message = '更新成功';
                    return $this->format();
                } else {
                    $this->status = 2;
                    $this->message = '更新失败';
                    return $this->format();
                }
            } else {
                $res = Goods::create($data);
                if ($res) {
                    $this->status = 1;
                    $this->message = '添加成功';
                    return $this->format($res);
                } else {
                    $this->status = 2;
                    $this->message = '添加失败';
                    return $this->format();
                }
            }
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //更改商品状态
    public function updateGoodsStatus(Request $request)
    {
        $token = $this->parseToken();
        $id = $request->post('id', ''); //id
        $status = $request->post('status', ''); //商品状态:1-在售 2-下架 3-删除

        $check_data = [
            'id' => '商品id',
            'status' => '商品状态'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = -1;
            $this->message = $check;
            return $this->format();
        }

        if (!in_array($status, [1, 2, 3])) {
            $this->status = 2;
            $this->message = '商品状态未知,请确认';
            return $this->format();
        }

        try {
            $goods_obj = Goods::find($id);
            if (!$goods_obj) {
                $this->status = 2;
                $this->message = '商品不存在';
                return $this->format();
            }

            if ($goods_obj->status == $status) {
                $this->status = 2;
                $this->message = '商品状态未更改,请勿重复操作';
                return $this->format();
            }

            $res = $goods_obj->update([
                'status' => $status
            ]);
            if ($res) {
                $this->status = 1;
                $this->message = '更新成功';
                return $this->format();
            } else {
                $this->status = 2;
                $this->message = '更新失败';
                return $this->format();
            }
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //删除商品
    public function delGoods(Request $request)
    {
        $token = $this->parseToken();
        $id = $request->post('id', ''); //id,必传
        $delTag = $request->post('delTag', false); //真删除标志

        $check_data = [
            'id' => '商品id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = -1;
            $this->message = $check;
            return $this->format();
        }

        try {
            $goods_obj = Goods::find($id);
            if (!$goods_obj) {
                $this->status = 2;
                $this->message = '商品不存在';
                return $this->format();
            }

            if ($goods_obj->status == 1) {
                $this->status = 2;
                $this->message = '商品在售,不支持删除,请先下架';
                return $this->format();
            }

            if ($delTag) {
                $res = $goods_obj->delete();
            } else {
                $res = $goods_obj->update([
                    'status' => 3
                ]);
            }

            if ($res) {
                $this->status = 1;
                $this->message = '删除成功';
                return $this->format();
            } else {
                $this->status = 2;
                $this->message = '删除失败';
                return $this->format();
            }
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //小程序 商品列表
    public function goodsList(Request $request)
    {
        $token = $this->parseToken();
        $store_id = $request->get('storeId', ''); //门店id
        $cid = $request->get('cid', ''); //分类id
        $bar_code = $request->get('barCode', ''); //商品条码
        $name = $request->get('name', ''); //商品名称
        $status = $request->get('status', ''); //商品状态.1-在售 2-下架 3-删除
        $stock = $request->get('stock', ''); //库存数量,为0则售空
        $sort_method = $request->get('sortMethod', 'ASC'); //排序方式

        $check_data = [
            'storeId' => '门店id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = 2;
            $this->message = $check;
            return $this->format();
        }

        $where = [];
        if ($store_id) {
            $where[] = ['store_id', '=', $store_id];
        }
        if ($cid) {
            $where[] = ['cid', '=', $cid];
        }
        if ($bar_code) {
            $where[] = ['bar_code', '=', $bar_code];
        }
        if ($name) {
            $where[] = ['name', '=', $name];
        }
        if ($stock || $stock === '0') {
            $where[] = ['stock', '=', $stock];
        }

        try {
            if ($status) {
                $statusArr  = explode(",",$status);
                if(count($statusArr) == 1){
                    $where[] = ['status', '=', $status];
                    $arr_count = Goods::select('c_name', DB::raw('count(c_name) as num'))
                        ->where($where)
                        ->orderBy('is_recommend', 'desc')
                        ->orderBy('sort', $sort_method)
                        ->groupBy('c_name')
                        ->pluck('num', 'c_name');
                }else{
                    $arr_count = Goods::select('c_name', DB::raw('count(c_name) as num'))
                        ->where($where)
                        ->whereIn('status',$statusArr)
                        ->orderBy('is_recommend', 'desc')
                        ->orderBy('sort', $sort_method)
                        ->groupBy('c_name')
                        ->pluck('num', 'c_name');
                }
            }else{
                $arr_count = Goods::select('c_name', DB::raw('count(c_name) as num'))
                    ->where($where)
                    ->orderBy('is_recommend', 'desc')
                    ->orderBy('sort', $sort_method)
                    ->groupBy('c_name')
                    ->pluck('num', 'c_name');
            }

            $pluck_num = $arr_count->toArray();

            $sold_out_num = Goods::where(['store_id' => $store_id])->where('stock', 0)->count(); //售空
            $sold_off_num = Goods::where(['store_id' => $store_id])->where('status', 2)->count(); //已下架
            $solding_num = Goods::where(['store_id' => $store_id])->where('status', 1)->count(); //售卖中
            $no_image_num = Goods::where(['store_id' => $store_id])->where('main_image', '')->count(); //无图片

            $Statistics_arr = [
                'sold_out_num' => $sold_out_num,
                'sold_off_num' => $sold_off_num,
                'solding_num' => $solding_num,
                'no_image_num' => $no_image_num
            ];
            $statistics_count = array_merge($pluck_num, $Statistics_arr);

            $goods_category_arr = GoodsCategory::where('status', 1)
                ->where(['store_id' => $store_id])
                ->get()
                ->toArray();
            if ($goods_category_arr && is_array($goods_category_arr)) {
                foreach ($goods_category_arr as $k => $categotys) {
                    $goods_category_arr[$k]['goods_list'] = Goods::where('cid', $categotys['id'])
                        ->where($where)
                        ->orderBy('is_recommend', 'desc')
                        ->orderBy('sort', $sort_method)
                        ->get()
                        ->toArray();
                }
//            $this->t = $obj->count();
//            $new_arr = $this->page($obj)->get()->groupBy('c_name');
//            $new_arr = $new_arr->toArray();
                if (is_array($goods_category_arr)) {
                    array_walk_recursive($goods_category_arr, function(&$values, $key) {
                        if ($key == 'sub_images') {
                            if ($values) $values = explode(",", $values);
                        }
                        if ($key == 'tag_ids') {
                            if ($values) {
                                $tag_arrs = explode(",", $values);
                                $values = [];
                                foreach ($tag_arrs as $ke => $val) {
                                    $goods_tag_obj = GoodsTag::find($val);
                                    if ($goods_tag_obj) {
                                        $values[$ke] = $goods_tag_obj->name;
                                    }
                                }
                            }
                        }
                        if ($key == 'standard_ids') {
                            if ($values) {
                                $standard_arrs = explode(",", $values);
                                $values = [];
                                foreach ($standard_arrs as $ke => $val) {
                                    $goods_standard_obj = GoodsStandard::find($val);
                                    if ($goods_standard_obj) {
                                        $values[$ke] = $goods_standard_obj->name;
                                    }
                                }
                            }
                        }
                        if ($key == 'attribute_ids') {
                            if ($values) {
                                $attribute_arrs = explode(",", $values);
                                $values = [];
                                foreach ($attribute_arrs as $ke => $val) {
                                    $goods_attribute_obj = GoodsAttribute::find($val);
                                    if ($goods_attribute_obj) {
                                        $values[$ke] = $goods_attribute_obj->name;
                                    }
                                }
                            }
                        }
                    });
                } else {
                    $goods_category_arr = [];
                }
            } else {
                $goods_category_arr = [];
            }

            $this->order_data = $statistics_count;
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($goods_category_arr);
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //批量更改商品状态
    public function batchUpdateGoods(Request $request)
    {
        $token = $this->parseToken();
        $id = $request->post('id', ''); //商品id,多个用','隔开,必传
        $cid = $request->post('cid', ''); //分类id
        $is_recommend = $request->post('isRecommend', ''); //是否推荐
        $stock = $request->post('stock', ''); //库存数量
        $status = $request->post('status', ''); //商品状态.1-在售 2-下架 3-删除
        $sort = $request->post('sort', ''); //排序权重,1--99,顺序
        $delTag = $request->post('delTag', 0); //删除标志(0-修改商品状态为'删除',1-彻底删除商品数据)

        $check_data = [
            'id' => '商品id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = -1;
            $this->message = $check;
            return $this->format();
        }

        $data = [];
        if ($cid) {
            $cateogry_info = GoodsCategory::find($cid);
            if (!$cateogry_info) {
                $this->status = 2;
                $this->message = '该商品分类不存在';
                return $this->format();
            }
            $data['cid'] = $cid;
            $data['c_name'] = $cateogry_info->name;
        }
        if ($is_recommend) $data['is_recommend'] = $is_recommend;
        if ($stock) $data['stock'] = $stock;
        if ($status) $data['status'] = $status;
        if ($sort) $data['sort'] = $sort;

        try {
            if ($data) {
                $res = '';
                $ids = explode(',', $id);
                if (is_array($ids)) {
                    if ($delTag) {
                        foreach ($ids as $values) {
                            $obj = Goods::find($values);
                            $res = $obj->delete($data);
                        }

                        if ($res) {
                            $this->status = 1;
                            $this->message = '删除成功';
                            return $this->format();
                        } else {
                            $this->status = 2;
                            $this->message = '删除失败';
                            return $this->format();
                        }
                    } else {
                        foreach ($ids as $values) {
                            $obj = Goods::find($values);
                            $res = $obj->update($data);
                        }

                        if ($res) {
                            $this->status = 1;
                            $this->message = '更新成功';
                            return $this->format();
                        } else {
                            $this->status = 2;
                            $this->message = '更新失败';
                            return $this->format();
                        }
                    }
                } else {
                    if ($delTag) {
                        $obj = Goods::find($id);
                        $res = $obj->delete($data);

                        if ($res) {
                            $this->status = 1;
                            $this->message = '删除成功';
                            return $this->format();
                        } else {
                            $this->status = 2;
                            $this->message = '删除失败';
                            return $this->format();
                        }
                    } else {
                        $obj = Goods::find($id);
                        $res = $obj->update($data);

                        if ($res) {
                            $this->status = 1;
                            $this->message = '更新成功';
                            return $this->format();
                        } else {
                            $this->status = 2;
                            $this->message = '更新失败';
                            return $this->format();
                        }
                    }
                }

            }

            $this->status = 1;
            $this->message = '无更新数据';
            return $this->format();
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //商品详情
    public function goodsDetails(Request $request)
    {
        $token = $this->parseToken();
        $id = $request->get('id', ''); //商品id
        $status = $request->get('status', ''); //

        $check_data = [
            'id' => '商品id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = 2;
            $this->message = $check;
            return $this->format();
        }

        $where = [];
        if ($status) {
            $where[] = ['status', $status];
        }

        try {
            $goods_category_arr = Goods::where('id', $id)
                ->where($where)
                ->first()
                ->toArray();
//            print_r($goods_category_arr);die;
            if (is_array($goods_category_arr)) {
                foreach ($goods_category_arr as $key => &$values) {
                    if ($key == 'sub_images') {
                        if ($values) $values = explode(",", $values);
                    }
//                    if ($key == 'tag_ids') {
//                        if ($values) {
//                            $tag_arrs = explode(",", $values);
//                            $values = [];
//                            foreach ($tag_arrs as $ke => $val) {
//                                $values[$ke] = [$val => GoodsTag::find($val)->name];
//                            }
//                        }
//                    }
//                    if ($key == 'standard_ids') {
//                        if ($values) {
//                            $standard_arrs = explode(",", $values);
//                            $values = [];
//                            foreach ($standard_arrs as $ke => $val) {
//                                $values[$ke] = [$val => GoodsStandard::find($val)->name];
//                            }
//                        }
//                    }
//                    if ($key == 'attribute_ids') {
//                        if ($values) {
//                            $attribute_arrs = explode(",", $values);
//                            $values = [];
//                            foreach ($attribute_arrs as $ke => $val) {
//                                $values[$ke] = [$val => GoodsAttribute::find($val)->name];
//                            }
//                        }
//                    }
                }
            } else {
                $this->status = 2;
                $this->message = '未找到该商品';
                return $this->format();
            }

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($goods_category_arr);
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //PC端 商品列表
    public function goodsListNew(Request $request)
    {
        $token = $this->parseToken();
        $store_id = $request->get('storeId', ''); //门店id
        $cid = $request->get('cid', ''); //分类id
        $bar_code = $request->get('barCode', ''); //商品条码
        $name = $request->get('name', ''); //商品名称
        $status = $request->get('status', ''); //商品状态.1-在售 2-下架 3-删除
        $stock = $request->get('stock', ''); //库存数量,为0则售空
        $sort_method = $request->get('sortMethod', 'ASC'); //排序方式
        $is_recommend = $request->get('isRecommend', ''); //是否推荐

        $check_data = [
            'storeId' => '门店id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = 2;
            $this->message = $check;
            return $this->format();
        }

        $where = [];
        if ($cid) {
            $where[] = ['cid', '=', $cid];
        }
        if ($bar_code) {
            $where[] = ['bar_code', '=', $bar_code];
        }
        if ($name) {
            $where[] = ['name', '=', $name];
        }
        if ($stock || $stock === '0') {
            $where[] = ['stock', '=', $stock];
        }
        if ($is_recommend || $is_recommend === '0') {
            $where[] = ['is_recommend', '=', $is_recommend];
        }
        if ($status) {
            $where[] = ['status', '=', $status];
        }

        try {
            if (env('DB_D1_HOST')) {
                $goods_table = DB::connection("mysql_d1")->table('goods');
            } else {
                $goods_table = DB::table('goods');
            }

            $goodsObj = $goods_table->where('store_id', $store_id)
                ->where($where)
                ->orderBy('is_recommend', 'desc')
                ->orderBy('sort', $sort_method);
            $goods_arr = $goodsObj->get()->toArray();

            if (empty($goods_arr)) {
                $model = new Goods();
                $goods_arr = $model->getDefaultData()->toArray();
            }

            if ($goods_arr && is_array($goods_arr)) {
                foreach ($goods_arr as $keys => $goods_obj) {
                    array_walk_recursive($goods_obj, function(&$values, $key) {
                        if ($key == 'sub_images') {
                            if ($values) $values = explode(",", $values);
                        }
                        if ($key == 'tag_ids') {
                            if ($values) {
                                $tag_arrs = explode(",", $values);
                                $values = [];
                                foreach ($tag_arrs as $ke => $val) {
                                    $goods_tag_obj = GoodsTag::find($val);
                                    if ($goods_tag_obj) {
                                        $values[] = $goods_tag_obj->name;
                                    }
                                }
                            }
                        }
                        if ($key == 'standard_ids') {
                            if ($values) {
                                $standard_arrs = explode(",", $values);
                                $values = [];
                                foreach ($standard_arrs as $ke => $val) {
                                    $goods_standard_obj = GoodsStandard::find($val);
                                    if ($goods_standard_obj) {
                                        $values[] = $goods_standard_obj->name;
                                    }
                                }
                            }
                        }
                        if ($key == 'attribute_ids') {
                            if ($values) {
                                $attribute_arrs = explode(",", $values);
                                $values = [];
                                foreach ($attribute_arrs as $ke => $val) {
                                    $goods_attribute_obj = GoodsAttribute::find($val);
                                    if ($goods_attribute_obj) {
                                        $values[] = $goods_attribute_obj->name;
                                    }
                                }
                            }
                        }
                    });
                }
            } else {
                $goods_arr = [];
            }

            $this->t = $goodsObj->count();
            $this->page($goodsObj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($goods_arr);
        } catch (\Exception $ex) {
            Log::info('PC端,商品列表-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            return $this->responseDataJson(-1);
        }
    }


}
