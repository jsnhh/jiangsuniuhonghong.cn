<?php
namespace App\Api\Controllers\Merchant;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Api\Controllers\BaseController;
use App\Models\CustomerAppletsCoupons;
use App\Models\CustomerAppletsCouponUsers;

class WxMerchantCouponController extends BaseController
{
    /**
     * 商户端后台微信商家券列表
     */
    public function getWechatMerchantCouponList(Request $request)
    {
        $requestData = $request->all();
        $page = $requestData['page'] ?? 1;
        $limit = $requestData['limit'] ?? 10;
        $start = ($page - 1) * $limit;

        // 参数校验
        $checkData = [
            'store_id' => '门店号'
        ];
        $check = $this->check_required($requestData, $checkData);
        if ($check) {
            return $this->sys_response(40000, $check);
        }

        $model = new CustomerAppletsCoupons();
        $list = [];
        $count = $model->getMerchantCouponCount($requestData);
        $date = date('Y-m-d H:i:s', time());

        if ($count > 0) {
            $list = $model->getMerchantCouponPageList($requestData, $start, $limit);
            foreach ($list as $k => $v) {
                if ($v['coupon_stock_type'] == 'NORMAL') {
                    $list[$k]['coupon_stock_type_name'] = '满减券';
                }

                if ($v['available_end_time'] < $date) {
                    $list[$k]['coupon_status'] = '已过期';
                } else {
                    $list[$k]['coupon_status'] = '运行中';
                }

                $list[$k]['channel'] = '官方';
                $list[$k]['sent_percent'] = ($v['sent_num'] / $v['max_coupons']) * 100 . '%';
            }
        }

        return $this->sys_response_layui(200, '请求成功', $list, $count);
    }

    /**
     * 获取微信商家券核销明细
     */
    public function getWechatMerchantCouponUsedList(Request $request)
    {
        // 接收参数
        $input = $request->all();
        $page = $input['page'] ?? 1;
        $limit = $input['limit'] ?? 10;
        $start = abs(($page - 1) * $limit);

        // 参数校验
        $checkData = [
            'store_id' => '门店号'
        ];
        $check = $this->check_required($input, $checkData);
        if ($check) {
            return $this->sys_response(40000, $check);
        }

        $model = new customerappletsCouponUsers();

        $count = $model->getMerchantCouponUsedCount($input);
        $list = [];
        if ($count > 0) {
            $list = $model->getMerchantCouponUsedPageList($input, $start, $limit);
        }

        return $this->sys_response_layui(1, '请求成功', $list, $count);
    }

    /**
     * 删除微信商家券
     */
    public function delUserStoreCoupon(Request $request)
    {
        $requestData = $request->all();
        $id = $requestData['id'];

        // 参数校验
        $checkData = [
            'store_id' => '门店号',
            'id'       => 'id'
        ];
        $check = $this->check_required($requestData, $checkData);
        if ($check) {
            return $this->sys_response(40000, $check);
        }

        // 查询是否有该商家券
        $model = new CustomerAppletsCoupons();
        $delRes = $model->delUserStoreCoupon($id);

        if ($delRes) {
            return $this->sys_response(200, "操作成功");
        } else {
            return $this->sys_response(202, "操作失败");
        }
    }

    /**
     * 微信商家券批次使用、核销数量
     */
    public function wxMerchantCouponStocksCount(Request $request)
    {
        $input = $request->all();
        // 参数校验
        $checkData = [
            'store_id' => '门店号',
            'stock_id' => '批次号'
        ];
        $check = $this->check_required($input, $checkData);
        if ($check) {
            return $this->sys_response(40000, $check);
        }

        $customerAppletsCouponsModel = new CustomerAppletsCoupons();

        // 微信代金券领用、核销数量、总数量
        $totalCount = $customerAppletsCouponsModel->getMerchantCouponDetailCount($input);
        if (!empty($totalCount['max_coupons'])) {
            $totalCount['total_sent_num_percent'] = ($totalCount['total_sent_num'] / $totalCount['max_coupons']) * 100 . '%';
            $totalCount['total_used_num_percent'] = ($totalCount['total_used_num'] / $totalCount['max_coupons']) * 100 . '%';
        }

        return $this->sys_response_layui(1, '请求成功', $totalCount);
    }

    /**
     * 微信代金券批次详情
     */
    public function wxMerchantCouponStocksDetail(Request $request)
    {
        $input = $request->all();
        $page = $input['p'] ?? 1;
        $limit = $input['l'] ?? 10;
        $start = abs(($page - 1) * $limit);

        // 参数校验
        $checkData = [
            'store_id' => '门店号',
            'stock_id' => '批次号'
        ];
        $check = $this->check_required($input, $checkData);
        if ($check) {
            return $this->sys_response(40000, $check);
        }

        $customerAppletsCouponUsersModel = new customerappletsCouponUsers();

        // 用户领用商家券数量
        $userCouponCount = $customerAppletsCouponUsersModel->getUserGetMerchCouponCount($input);

        $userCouponPageList = [];
        $date_now = date('Y-m-d H:i:s', time());
        if ($userCouponCount > 0) {
            // 获取微信代金券领用、核销分页列表（用户）
            $userCouponPageList = $customerAppletsCouponUsersModel->getUserMerchantCashCouponPageList($input, $start, $limit);
            foreach ($userCouponPageList as $k => $v) {
                $userCouponPageList[$k]['user_name'] = json_decode($v['wechat_username']);

                $userCouponPageList[$k]['past_coupon_num'] = 0;
                if ($v['available_end_time'] < $date_now) {
                    $userCouponPageList[$k]['past_num'] += 1;
                }
            }
        }

        return $this->sys_response_layui(1, '请求成功', $userCouponPageList, $userCouponCount);
    }

}
