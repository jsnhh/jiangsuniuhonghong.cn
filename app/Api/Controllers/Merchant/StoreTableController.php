<?php
/**
 * 后台桌号管理接口
 */
namespace App\Api\Controllers\Merchant;

use App\Models\CustomerappletsAuthorizeAppids;
use App\Models\WechatThirdConfig;
use EasyWeChat\Factory;
use Illuminate\Http\Request;
use App\Models\MerchantStoreTableManage;
use App\Models\MerchantStoreQrcodeRule;
use App\Models\MerchantStoreAppidSecret;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Generator;
use App\Api\Controllers\Merchant\StoreWechatTicketController;
use App\Api\Controllers\AlipayOpen\AppletsController;
use Illuminate\Support\Facades\Log;

class StoreTableController extends StoreWechatTicketController
{
    protected $wechat_qrcode_rule;  // 微信小程序二维码规则地址
    protected $wechat_pages_url;    // 微信小程序扫码点餐页面地址
    //第三方开放平台appid
    protected $app_id;
    //第三方开放平台secret
    protected $secret;

    public function __construct()
    {
        $this->wechat_pages_url     = 'pages/ordermenu/ordermenu';
        $this->wechat_qrcode_rule   = env('APP_URL') . '/applet/applet?store_id=';
        $model = new WechatThirdConfig();
        $data = $model->getInfo();

        $this->app_id  = isset($data->app_id) ? $data->app_id : '';
        $this->secret  = isset($data->app_secret) ? $data->app_secret : '';
    }

    /**
     * 获取桌号信息
     * @param Request $request
     * @return array
     */
    public function getTableLists(Request $request)
    {
        // 接收参数
        $input = $request->all();
        $page = $input['p'] ?? 1;
        $limit = $input['l'] ?? 10;
        $start = abs(($page - 1) * $limit);

        // 获取该商户名下所有门店id
        $store_ids = $this->getMerchantStores();
        $input['store_ids'] = $store_ids;

        // 查询门店列表信息
        $model = new MerchantStoreTableManage();
        $list = [];
        $count = $model->getTableCount($input);
        if ($count > 0) {
            $list = $model->getTablePageList($input, $start, $limit);
            foreach ($list as $k => $v) {
                if ($v['table_status'] == 1) {
                    $list[$k]['table_status_name'] = '空闲';
                } else if ($v['table_status'] == 2) {
                    $list[$k]['table_status_name'] = '开台';
                }

                $list[$k]['wechat_applet_qrcode_url_full'] = $this->changeImgUrl($v['wechat_applet_qrcode'], 1);
                $list[$k]['common_qrcode_url_full'] = $this->changeImgUrl($v['common_qrcode'], 1);
                $list[$k]['alipay_applet_qrcode_full'] = $v['alipay_applet_qrcode'].'png' ;
            }
        }

        return $this->sys_response_layui(1, '请求成功', $list, $count);
    }

    /**
     * 删除桌号
     * @param Request $request
     * @return array
     */
    public function deleteTable(Request $request)
    {
        $input = $input = $request->all();

        // 参数校验
        $check_data = [
            'store_id' => '门店id',
            'table_id' => '桌id',
        ];
        $check = $this->check_required($input, $check_data);
        if ($check) {
            return $this->sys_response(40000, $check);
        }

        $merchant = $this->parseToken();
        if ($merchant->pid) {
            return $this->sys_response(40000, '没有权限删除');
        }

        $model = new MerchantStoreTableManage();
        $tableInfo = $model->getTableInfo($input);
        if (empty($tableInfo)) {
            return $this->sys_response(40000, '没有相关桌号信息');
        }

        // 删除
        $deleteRes = $model->deleteTable($input);
        if ($deleteRes) {
            return $this->sys_response_layui(1, '删除成功');
        } else {
            return $this->sys_response(40000, '删除失败');
        }
    }

    /**
     * 创建桌号信息
     * @param Request $request
     * @return array
     */
    public function createTable(Request $request)
    {
        $applet_common_qrcode_url = 'tableAppletCommonQrCode/';

        // 接收参数
        $input = $request->all();
        $data = [];

        try {
            // 检查存放小程序码文件是否存在
            if (!is_dir(public_path($applet_common_qrcode_url))) {
                mkdir(public_path($applet_common_qrcode_url), 0777);
            }

            // 参数校验
            $check_data = [
                'store_id' => '门店号',
                'table_name' => '桌号',
            ];
            $check = $this->check_required($input, $check_data);
            if ($check) {
                return $this->sys_response(40000, $check);
            }
            $storeInfo = DB::table("merchant_store_appid_secrets")
                ->select("alipay_appid")
                ->where(['store_id' => $input['store_id']])->first();
            if(empty($storeInfo)){
                return $this->sys_response(202,"请先完善该门店小程序信息");
            }

            // 小程序页面-扫码点餐
            $input['pages_url'] = $this->wechat_pages_url;
            // 生成桌id
            $table_id = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
            $input['table_id'] = $table_id;
            // 二维码地址
            // $input['prefix'] = $this->wechat_qrcode_rule . $input['store_id'];
            if (env('APP_URL') == 'https://pay.jiangsuniuhonghong.cn') {
                $input['prefix'] = env('APP_URL') . '/applet?store_id=' . $input['store_id'];
            } else {
                $input['prefix'] = $this->wechat_qrcode_rule . $input['store_id'];
            }
            // 二维码规则类型
            $input['qrcode_type'] = 1;

            $data['page_redirection'] = 'pages/ordermenu/ordermenu';
            $data['route_url'] = $input['prefix'] . '&store_id=' . $input['store_id'] . '&table_name=' . $input['table_name'];

             //支付寶參數
             $data['url_param'] = 'pages/ordermenu/ordermenu';
             $data['query_param'] = 'x=1';
             $data['describe'] = '这是一个小程序';
             $mini_app_id = $storeInfo-> alipay_appid;
             $appletsInfo = DB::table("customerapplets_authorize_appids")->where(['applet_type' => 2,'AuthorizerAppid' => $mini_app_id])->first();
             if (empty($appletsInfo)) {
                 return $this->sys_response(202, "该支付宝小程序暂未授权");
             }
             $data['app_auth_token'] = $appletsInfo->AuthorizationCode;
            $model = new MerchantStoreTableManage();
            $merchantStoreModel = new MerchantStoreAppidSecret();
            $merchantStoreQrcodeRuleModel = new MerchantStoreQrcodeRule();

            // 获取门店小程序信息
            $merchantStoreInfo = $merchantStoreModel->getStoreAppIdSecret($input['store_id']);
            $merchantStoreQrcodeRuleInfo = $merchantStoreQrcodeRuleModel->getInfo($input);
            if (!$merchantStoreInfo) {
                return $this->sys_response(40000, '门店对应小程序信息未配置，请完善');
            }
            $input['wechat_appid'] = $merchantStoreInfo->wechat_appid ?? '';
            $input['wechat_secret'] = $merchantStoreInfo->wechat_secret ?? '';
            $input['alipay_appid'] = $merchantStoreInfo->wechat_appid ?? '';
            $input['alipay_secret'] = $merchantStoreInfo->alipay_secret ?? '';
            $wechat_common_qrcode = $merchantStoreQrcodeRuleInfo->wechat_common_qrcode_rule ?? '';
            $alipay_common_qrcode = $merchantStoreQrcodeRuleInfo->wechat_common_qrcode_rule ?? '';
            $input['app_id'] = $this->app_id;

            // 获取微信小程序码
            $wechat_applet_qrcode = $this->getWechatAppletQrcode($input);

            //获取支付宝小程序
            $AppletsController = new AppletsController();

            $AppletsControllerResult = $AppletsController->qrcodeCreate($data);
            $zfbQr = json_decode($AppletsControllerResult, true);
            if($zfbQr['status'] != '1') {
                $alipay_applet_qrcode = '';
            }else{
                //成功后的二维码图片
                $alipay_applet_qrcode = $zfbQr['data'];
            }

            if($zfbQr['status'] == '1'){
                //支付宝绑定三方二维码
                $AlipayThirdResult = $AppletsController->qrcodeBind($data);
                $zfbBindQr = json_decode($AlipayThirdResult, true);

                if($zfbBindQr['status'] != '1') {
                    $alipay_common_qrcode_rule = '';
                }else{
                    $alipay_common_qrcode_rule = $zfbBindQr['data'];
                }
            }

            if ($wechat_common_qrcode != $input['prefix']) {
                // 三方配置微信聚合码地址
                $this->createQrcodeRule($input);
                // 插入数据
                $data = [
                    'store_id' => $input['store_id'],
                    'wechat_common_qrcode_rule' => $input['prefix'],
                    'qrcode_type' => 1,
                    'alipay_common_qrcode_rule' => $alipay_common_qrcode_rule ?? '',
                    'created_at' => date('Y-m-d h:i:s', time()),
                    'updated_at' => date('Y-m-d h:i:s', time()),
                ];
                $merchantStoreQrcodeRuleModel->insertData($data);
            }

            $common_qrcode_name = 'common_' . $input['table_name'] . '_' . $input['table_id'] . '.png';
            $common_qrcode_name_full = '/' . $applet_common_qrcode_url . $common_qrcode_name;
            $generator = new Generator();
            $generator->format('png')->size(200)->encoding('UTF-8')
                ->generate($input['prefix'] . '&store_id=' . $input['store_id'] . '&table_name=' . $input['table_name']. '&table_id=' . $input['table_id'],
                // ->generate($input['prefix'] . '&store_id=' . $input['store_id'] . '&table_name=' . $input['table_name'],
                    public_path($applet_common_qrcode_url . $common_qrcode_name));

            // 插入数据
            $data = [
                'store_id' => $input['store_id'],
                'table_id' => $input['table_id'] ?? '',
                'table_name' => $input['table_name'],
                'table_status' => 1,
                'wechat_appid' => $merchantStoreInfo->wechat_appid ?? '',
                'alipay_appid' => $merchantStoreInfo->alipay_appid ?? '',
                'wechat_applet_qrcode' => $wechat_applet_qrcode ?? '',
                'alipay_applet_qrcode' => $alipay_applet_qrcode ?? '',
                'common_qrcode' => $common_qrcode_name_full,
                'created_at' => date('Y-m-d h:i:s', time()),
                'updated_at' => date('Y-m-d h:i:s', time()),
            ];
            $insertRes = $model->insertTableInfo($data);
            if (!$insertRes) {
                return $this->sys_response(40000);
            }

            return $this->sys_response_layui(1, '请求成功');
        } catch (\Exception $e) {
            return $this->sys_response(40001, $e->getMessage());
        }
    }

    /**
     * 获取微信小程序码
     * @param $input
     * @return string
     * @throws string EasyWeChat 异常
     */
    public function getWechatAppletQrcode_bak($input)
    {
        // 检查存放小程序码文件是否存在
        if (!is_dir(public_path('tableAppletQrCode/'))) {
            mkdir(public_path('tableAppletQrCode/'), 0777);
        }
        // 小程序码编号
        $code_number = 'wx_' . $input['table_name'];

        // 获取微信小程序码
        $config = [
            'app_id' => $input['wechat_appid'],
            'secret' => $input['wechat_secret'],
        ];
        $app = Factory::miniProgram($config);
        $table_id = $input['table_id'];
        $table_name = $input['table_name'];

        $response = $app->app_code->get($this->wechat_pages_url . '?table_id=' . $table_id . '&table_name=' . $table_name);

        $filename = '';
        if ($response instanceof \EasyWeChat\Kernel\Http\StreamResponse) {
            $filename = $response->saveAs(public_path('tableAppletQrCode/'), $code_number . '.png');
        }

        return '/tableAppletQrCode/' . $filename;
    }

    public function getWechatAppletQrcode($requestAllData)
    {
        $table_id = $requestAllData['table_id'];
        $table_name = $requestAllData['table_name'];

        // 检查存放小程序码文件是否存在
        if (!is_dir(public_path('tableAppletQrCode/'))) {
            mkdir(public_path('tableAppletQrCode/'), 0777);
        }

        // 获取authorizer_access_token
        $authorizer_access_token = $this->getAuthorizerAccessTokenCommon($requestAllData);

        // 获取小程序二维码
        $getAppletQrcodeData = [
            "path"  => $this->wechat_pages_url . '?store_id=' . $requestAllData['store_id'] . '&table_name=' . $table_name,
            "width" => 300
        ];
        $getAppletQrcodeUrl = 'https://api.weixin.qq.com/wxa/getwxacode?access_token='.$authorizer_access_token;
        $getAppletQrcodeRes = $this->curl_post_https($getAppletQrcodeUrl,json_encode($getAppletQrcodeData,JSON_UNESCAPED_UNICODE));

        $dir = 'tableAppletQrCode/';
        // 检查是否存在
        if (!is_dir(public_path($dir))) {
            mkdir(public_path($dir), 0777);
        }

        // 小程序码编号
        $fileName = 'wx_' . $requestAllData['table_name'] . '.jpg';

        $save_dir = public_path($dir);
        $resource = fopen($save_dir . $fileName, 'a');
        fwrite($resource, $getAppletQrcodeRes);
        fclose($resource);

        $resData['qrcode_url'] = env('APP_URL') . '/' . $dir . $fileName;

        return '/' . $dir . $fileName;
        // return $this->responseDataJson(200,'请求成功', $resData);
    }

    /**
     * 获取支付宝小程序二维码（预留）
     */
    public function getAlipayAppletQrcode()
    {

    }

    /**
     * 编辑桌号信息
     */
    public function editTable(Request $request)
    {
        $input = $request->all();
        // 参数校验
        $check_data = [
            'id' => 'id',
        ];
        $check = $this->check_required($input, $check_data);
        if ($check) {
            return $this->sys_response(2, $check);
        }

        $model = new MerchantStoreTableManage();

        // 编辑
        $data['table_name'] = $input['table_name'];
        $data['updated_at'] = time();
        $deleteRes = $model->editTable($input['id'], $data);

        if ($deleteRes) {
            return $this->sys_response_layui(1, '编辑成功');
        } else {
            return $this->sys_response_layui(2, '编辑失败');
        }
    }

    /**
     * 闭台
     */
    public function closeTable(Request $request)
    {
        $input = $request->all();
        // 参数校验
        $check_data = [
            'id' => 'id',
        ];
        $check = $this->check_required($input, $check_data);
        if ($check) {
            return $this->sys_response(40000, $check);
        }

        $model = new MerchantStoreTableManage();

        // 编辑
        $data['table_status'] = 1;
        $data['updated_at'] = time();
        $deleteRes = $model->editTable($input['id'], $data);
        if ($deleteRes) {
            return $this->sys_response_layui(1, '编辑成功');
        } else {
            return $this->sys_response_layui(2, '编辑失败');
        }
    }


    /**
     * 生成小程序推广二维码
     *
     * @param Request $request
     * @return array
     */
    public function aliPayOpenAppQrCodeCreate(Request $request)
    {
        $applet_common_qrcode_url = 'tableAppletCommonQrCode/';

        $store_id = $request->get('storeId', ''); //系统门店id
        $url_param = $request->get('urlParam', 'pages/payment/payment'); //String,必,256,小程序中能访问到的页面路径
        $query_param = $request->get('queryParam', 'x=1'); //String,必选,256,小程序的启动参数,打开小程序的query,在小程序onLaunch的方法中获取
        $describe = $request->get('describe', '小程序推广二维码'); //String,必选,32,二维码描述

        try {
            // 检查存放小程序码文件是否存在
            if (!is_dir(public_path($applet_common_qrcode_url))) {
                mkdir(public_path($applet_common_qrcode_url), 0777);
            }

            // 参数校验
            $check_data = [
                'storeId' => '系统门店id',
                'urlParam' => '小程序中能访问到的页面路径',
                'describe' => '二维码描述'
            ];
            $check = $this->check_required($request->all(), $check_data);
            if ($check) {
                $this->status = 2;
                $this->message = $check;
                return $this->format();
            }

            $storeInfo = DB::table("merchant_store_appid_secrets")
                ->select("alipay_appid")
                ->where('store_id', $store_id)
                ->first();
            if (empty($storeInfo)) {
                $this->status = 2;
                $this->message = '请先完善该门店小程序信息';
                return $this->format();
            }

            $mini_app_id = $storeInfo-> alipay_appid;
            $appletsInfo = CustomerappletsAuthorizeAppids::where('applet_type', 2)
                ->where('AuthorizerAppid', $mini_app_id)
                ->first();
            if (empty($appletsInfo)) {
                $this->status = 2;
                $this->message = '该支付宝小程序暂未授权';
                return $this->format();
            }

            $reqData = [
                'url_param' => $url_param,
                'query_param' => $query_param,
                'describe' => $describe,
                'app_auth_token' => $appletsInfo->AuthorizationCode
            ];
            $AppletsController = new AppletsController();
            $AppletsControllerResult = $AppletsController->qrcodeCreate($reqData); //小程序生成二维码
            $zfbQr = json_decode($AppletsControllerResult, true);
            if ($zfbQr['status'] != '1') {
                $alipay_applet_qrcode = '';
            } else {
                //成功后的二维码图片
                $alipay_applet_qrcode = $zfbQr['data'];

                //支付宝绑定三方二维码
//                $AlipayThirdResult = $AppletsController->qrcodeBind($data);
//                $zfbBindQr = json_decode($AlipayThirdResult, true);
//                if ($zfbBindQr['status'] != '1') {
//                    $alipay_common_qrcode_rule = '';
//                } else {
//                    $alipay_common_qrcode_rule = $zfbBindQr['data'];
//                }
            }

            $this->status = 1;
            $this->message = '请求成功';
            return $this->format($alipay_applet_qrcode);
        } catch (\Exception $e) {
            $this->status = 40001;
            $this->message = $e->getMessage().' | '.$e->getFile().' | '.$e->getLine();
            return $this->format();
        }
    }


}
