<?php

namespace App\Api\Controllers\Merchant;


use Alipayopen\Sdk\AopClient;
use Alipayopen\Sdk\Request\AlipayTradeQueryRequest;
use Alipayopen\Sdk\Request\AlipayTradeRefundRequest;
use App\Api\Controllers\BaseController;
use App\Api\Controllers\Basequery\AdSelectController;
use App\Api\Controllers\Config\AlipayIsvConfigController;
use App\Api\Controllers\Config\AllinPayConfigController;
use App\Api\Controllers\Config\ChangshaConfigController;
use App\Api\Controllers\Config\CcBankPayConfigController;
use App\Api\Controllers\Config\EasyPayConfigController;
use App\Api\Controllers\Config\EasySkPayConfigController;
use App\Api\Controllers\Config\FuiouConfigController;
use App\Api\Controllers\Config\HConfigController;
use App\Api\Controllers\Config\HkrtConfigController;
use App\Api\Controllers\Config\HuiPayConfigController;
use App\Api\Controllers\Config\HwcPayConfigController;
use App\Api\Controllers\Config\JdConfigController;
use App\Api\Controllers\Config\LianfuConfigController;
use App\Api\Controllers\Config\LianfuyoupayConfigController;
use App\Api\Controllers\Config\LinkageConfigController;
use App\Api\Controllers\Config\LklConfigController;
use App\Api\Controllers\Config\LtfConfigController;
use App\Api\Controllers\Config\MyBankConfigController;
use App\Api\Controllers\Config\NewLandConfigController;
use App\Api\Controllers\Config\PostPayConfigController;
use App\Api\Controllers\Config\QfPayConfigController;
use App\Api\Controllers\Config\SuzhouConfigController;
use App\Api\Controllers\Config\TfConfigController;
use App\Api\Controllers\Config\VbillConfigController;
use App\Api\Controllers\Config\WeixinConfigController;
use App\Api\Controllers\Config\WftPayConfigController;
use App\Api\Controllers\Config\YinshengConfigController;
use App\Api\Controllers\DuoLaBao\ManageController;
use App\Api\Controllers\MyBank\TradePayController;
use App\Api\Controllers\Newland\PayController;
use App\Api\Controllers\HwcPay\PayController as HwcPayPayController;
use App\Api\Controllers\WftPay\PayController as WftPayPayController;
use App\Api\Controllers\QfPay\PayController as QfPayController;
use App\Common\PaySuccessAction;
use App\Common\StoreDayMonthOrder;
use App\Common\UserGetMoney;
use App\Models\AlipayAppOauthUsers;
use App\Models\AlipayHbOrder;
use App\Models\AlipayIsvConfig;
use App\Models\EasypayStoresImages;
use App\Models\Merchant;
use App\Models\MerchantFuwu;
use App\Models\MerchantStore;
use App\Models\MerchantStoreDayOrder;
use App\Models\MerchantStoreMonthOrder;
use App\Models\MerchantWalletDetail;
use App\Models\MyBankConfig;
use App\Models\MyBankStore;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\RefundOrder;
use App\Models\Store;
use App\Models\StoreDayOrder;
use App\Models\StoreMonthOrder;
use App\Models\UserKeyUrl;
use App\Models\UserWalletDetail;
use EasyWeChat\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class OrderController extends BaseController
{

    public function order(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $merchant_id = $request->get('merchant_id', '');
            $pay_status = $request->get('pay_status', '1');
            $ways_source = $request->get('ways_source', '');
            $ways_type = $request->get('ways_type', '');
            $time_start_s = date('Y-m-d 00:00:00', time());
            $time_start_e = date('Y-m-d 23:59:59', time());
            $company = $request->get('company', '');

            $time_start = $request->get('time_start', $time_start_s);
            $time_end = $request->get('time_end', $time_start_e);

            $out_trade_no = $request->get('out_trade_no', '');
            $device_id = $request->get('device_id', '');

            $trade_no = $request->get('trade_no', '');
            $return_type = $request->get('return_type', '1');

            //限制时间
            $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
            $day = 31;
            $now_time = date('Y-m-d H:i:s', time());
            $end = date('Y-m-d 22:00:00', time());
            if ($now_time > $end) {
                $day = 31;
            }

            if ($date > $day) {
                $time_start_s = date('Y-m-d 00:00:00', time());
                $time_start_e = date('Y-m-d 23:59:59', time());
                $time_start = $time_start_s;
                $time_end = $time_start_e;
            }

            //跨天操作
            $time_start_db = date('Ymd', strtotime($time_start));
            $time_end_db = date('Ymd', strtotime($time_end));
            $is_ct_time = 0;
            if ($time_start_db != $time_end_db) {
                $is_ct_time = 1;
            }

            $sort = $request->get('sort', '');

            $day = date('Ymd', strtotime($time_end));
            $table = 'orders_' . $day;

            if (env('DB_D1_HOST')) {
                //有没有跨天
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj = DB::connection("mysql_d1")->table('order_items');
                    } elseif (Schema::hasTable('orders2020') && $time_start < "2021-01-01 00:00:00") {
                        $obj = DB::connection("mysql_d1")->table('orders2020');
                    } else {
                        $obj = DB::connection("mysql_d1")->table('orders');
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj = DB::connection("mysql_d1")->table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj = DB::connection("mysql_d1")->table('order_items');
                        } elseif (Schema::hasTable('orders2020') && $time_start < "2021-01-01 00:00:00") {
                            $obj = DB::connection("mysql_d1")->table('orders2020');
                        } else {
                            $obj = DB::connection("mysql_d1")->table('orders');
                        }
                    }
                }
            } else {
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj = DB::table('order_items');
                    } elseif (Schema::hasTable('orders2020') && $time_start < "2021-01-01 00:00:00") {
                        $obj = DB::table('orders2020');
                    } else {
                        $obj = DB::table('orders');
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj = DB::table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj = DB::table('order_items');
                        } elseif (Schema::hasTable('orders2020') && $time_start < "2021-01-01 00:00:00") {
                            $obj = DB::table('orders2020');
                        } else {
                            $obj = DB::table('orders');
                        }
                    }
                }
            }

            $where = [];
            $store_ids = [];
            if ($out_trade_no) {
                $where[] = ['out_trade_no', 'like', $out_trade_no . '%'];
            }

            if ($trade_no) {
                $where[] = ['trade_no', 'like', $trade_no . '%'];
            }
            //收银员
            if ($merchant->merchant_type == 2) {
                $where[] = ['merchant_id', '=', '' . $merchant->merchant_id . ''];
            }

            //是否传收银员ID
            if ($merchant_id && $merchant_id != "NULL") {
                $where[] = ['merchant_id', '=', '' . $merchant_id . ''];
            }

            if ($pay_status) {
                $where[] = ['pay_status', '=', $pay_status];
            }

            if ($device_id) {
                $where[] = ['device_id', '=', $device_id];
            }

            if ($store_id && $store_id != "NULL") {
                $store_ids = [
                    [
                        'store_id' => $store_id,
                    ]
                ];
            } else {
                $MerchantStore = MerchantStore::where('merchant_id', $merchant->merchant_id)
                    ->select('store_id')
                    ->get();

                if (!$MerchantStore->isEmpty()) {
                    $store_ids = $MerchantStore->toArray();
                }
            }

            if ($company) {
                $where[] = ['company', '=', $company];
            }

            if ($ways_source) {
                if (in_array($ways_source, ['alipay_face', 'weixin_face'])) {
                    $where[] = ['pay_method', '=', $ways_source];

                } else {
                    $where[] = ['ways_source', '=', $ways_source];
                }
            }

            if ($ways_type) {
                $where[] = ['ways_type', '=', $ways_type];
            }

            if ($time_start) {
                $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                $where[] = ['created_at', '>=', $time_start];
            }

            if ($time_end) {
                $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                $where[] = ['created_at', '<=', $time_end];
            }

            if ($sort) {
                //返回基础数据
                if ($return_type == '1') {
                    $obj = $obj->where($where)
                        ->whereIn('store_id', $store_ids)
                        ->orderBy('total_amount', $sort);
                } else {
                    $obj = $obj->where($where)
                        ->whereIn('store_id', $store_ids)
                        ->orderBy('total_amount', $sort);
                }

            } else {
                if ($return_type == '1') {
                    //返回基础数据
                    $obj = $obj->where($where)
                        ->whereIn('store_id', $store_ids)
                        ->orderBy('created_at', 'desc');
                } else {
                    $obj = $obj->where($where)
                        ->whereIn('store_id', $store_ids)
                        ->orderBy('created_at', 'desc');
                }
            }

            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';

            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage() . $exception->getLine();
            return $this->format();
        }
    }


    public function order_info(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $out_trade_no = $request->get('out_trade_no', '');

            $a = array('a', '_', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
            $b = str_ireplace($a, "", $out_trade_no);
            $day = substr($b, 0, 8);
            $table = 'orders_' . $day;

            if (env('DB_D1_HOST')) {
                if (Schema::hasTable($table)) {
                    $data = DB::connection("mysql_d1")->table($table)->orWhere('out_trade_no', $out_trade_no)
                        ->orWhere('trade_no', $out_trade_no)
                        ->first();
                } else {
                    $data = DB::connection("mysql_d1")->table("orders")->orWhere('out_trade_no', $out_trade_no)
                        ->orWhere('trade_no', $out_trade_no)
                        ->first();
                }
            } else {
                if (Schema::hasTable($table)) {
                    $data = DB::table($table)->orWhere('out_trade_no', $out_trade_no)
                        ->orWhere('trade_no', $out_trade_no)
                        ->first();
                } else {
                    $data = DB::table('orders')
                        ->where('out_trade_no', $out_trade_no)
                        ->first();
                    if (!$data) {
                        $data = DB::table('orders')
                            ->where('trade_no', $out_trade_no)
                            ->first();
                        if (!$data) {
                            $data = DB::table('orders')
                                ->where('other_no', $out_trade_no)
                                ->first();
                        }
                    }
                }
            }

            if (!$data && Schema::hasTable('orders2020')) {
                $data = DB::table('orders2020')
                    ->orWhere('out_trade_no', $out_trade_no)
                    ->first();
                if (!$data) {
                    $data = DB::table('orders2020')
                        ->where('trade_no', $out_trade_no)
                        ->first();
                    if (!$data) {
                        $data = DB::table('orders2020')
                            ->where('auth_code', $out_trade_no)
                            ->first();
                    }
                }
            }

            if (!$data) {
                return json_encode([
                    'status' => 2,
                    'message' => '订单号不存在111'
                ]);
            }

            //保证订单号是此门店的
            $MerchantStore = MerchantStore::where('store_id', $data->store_id)
                ->where('merchant_id', $merchant->merchant_id)
                ->select('id')
                ->first();
            if (!$MerchantStore) {
                return json_encode([
                    'status' => 2,
                    'message' => '订单号不在你的查询范围'
                ]);
            }

            try {
                if ($data->pay_status == 2) {
                    $order_foreach_public = [
                        'out_trade_no' => $data->out_trade_no,
                        'store_id' => $data->store_id,
                        'ways_type' => $data->ways_type,
                        'config_id' => $data->config_id,
                        'table' => $table,
                    ];
                    $obj = new OrderController();
                    $obj->order_foreach_public($order_foreach_public);
                }
            } catch (\Exception $exception) {

            }

            $this->status = 1;
            $this->message = '数据返回成功';

            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    //app退款
    public function refund(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $merchant_id = $merchant->merchant_id;
            $out_trade_no = $request->get('out_trade_no', '');
            $refund_amount = $request->get('refund_amount', '');

            //收银员
            if ($merchant->merchant_type == 2) {
                return json_encode([
                    'status' => '2',
                    'message' => '收银员没有退款权限'
                ]);
            }

            $data = [
                'merchant_id' => $merchant_id,
                'out_trade_no' => $out_trade_no,
                'refund_amount' => $refund_amount
            ];
            $return = $this->refund_public($data);

            return $return;
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ]);
        }
    }


    //app退款-公共
    public function refund_public($data)
    {
        try {
            $merchant_id = $data['merchant_id'];
            $out_trade_no = $data['out_trade_no'];
            $refund_amount = isset($data['refund_amount']) ? $data['refund_amount'] : 0; //退款金额
            $refund_no = isset($data['refund_no']) ? $data['refund_no'] : $out_trade_no . '123';
            if ($refund_no == "") {
                $refund_no = time();
            }

            $a = array('a', '_', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
            $b = str_ireplace($a, "", $out_trade_no);
            $day = substr($b, 0, 8);
            $table = 'orders_' . $day;

            if (!Schema::hasTable($table)) {
                $day = date('Ymd', time());
                $table = 'orders_' . $day;
            }

            if (Schema::hasTable($table)) {
                $order = DB::table($table)->where('trade_no', $out_trade_no);
            } else {
                $order = Order::where('trade_no', $out_trade_no);
            }

            $order = $order->select(
                'id',
                'total_amount',
                'refund_amount',
                'device_id',
                'out_trade_no',
                'trade_no',
                'store_id',
                'ways_type',
                'config_id',
                'other_no',
                'trade_no',
                'ways_source',
                'created_at',
                'pay_time',
                'rate',
                'buyer_pay_amount',
                'receipt_amount',
                'pay_amount',
                'mdiscount_amount'
            )
                ->first();
            if (!$order) {
                if (Schema::hasTable($table)) {
                    $order = DB::table($table)->where('out_trade_no', $out_trade_no);
                } else {
                    $order = Order::where('out_trade_no', $out_trade_no);
                }
                $order = $order->select(
                    'id',
                    'total_amount',
                    'refund_amount',
                    'device_id',
                    'out_trade_no',
                    'trade_no',
                    'store_id',
                    'ways_type',
                    'config_id',
                    'other_no',
                    'trade_no',
                    'ways_source',
                    'created_at',
                    'pay_time',
                    'rate',
                    'buyer_pay_amount',
                    'receipt_amount',
                    'pay_amount',
                    'mdiscount_amount'
                )
                    ->first();
            }

            if (!$order) {
                if (Schema::hasTable($table)) {
                    $order = DB::table($table)->where('other_no', $out_trade_no);
                } else {
                    $order = Order::where('other_no', $out_trade_no);
                }
                $order = $order->select(
                    'id',
                    'total_amount',
                    'refund_amount',
                    'device_id',
                    'out_trade_no',
                    'trade_no',
                    'store_id',
                    'ways_type',
                    'config_id',
                    'other_no',
                    'trade_no',
                    'ways_source',
                    'created_at',
                    'pay_time',
                    'rate',
                    'buyer_pay_amount',
                    'receipt_amount',
                    'pay_amount',
                    'mdiscount_amount'
                )
                    ->first();
            }

            if (!$order) {
                return json_encode([
                    'status' => 2,
                    'message' => '订单号不存在'
                ]);
            }

            //判断退款是否达到
            if ($order->refund_amount >= $order->total_amount) {
                return json_encode([
                    'status' => '2',
                    'message' => '此订单已全部退款'
                ]);
            }

            //判断退款金额是否超交易金额
            $already_refund_amount = ($order->refund_amount * 100); //已退款金额,分
            $refund_amount = isset($refund_amount) && $refund_amount ? $refund_amount : $order->receipt_amount;
            if ((($refund_amount * 100) + $already_refund_amount) > ($order->total_amount * 100)) {
                return json_encode([
                    'status' => '2',
                    'message' => '退款金额大于总金额'
                ]);
            }

            //暂时只支持退全款
//            if ($refund_amount && $refund_amount != $order->total_amount) {
//                return json_encode([
//                    'status' => 2,
//                    'message' => '只支持退全额'
//                ]);
//            }

            //充值订单不支持退款
            if ($order->device_id == "member_cz") {
                return json_encode([
                    'status' => '2',
                    'message' => '充值订单不支持退款'
                ]);
            }

            $out_trade_no = $order->out_trade_no;
            $OutRefundNo = $refund_no;
            $refund_amount = isset($refund_amount) && $refund_amount ? $refund_amount : $order->total_amount;

            if (($order->receipt_amount * 100 > 0) && ($refund_amount * 100 - $order->receipt_amount * 100) > 0) {
                $refund_amount = !empty($order->receipt_amount * 100) ? ($order->receipt_amount * 100) / 100 : ($order->total_amount * 100 - $order->mdiscount_amount * 100) / 100;
            }

            //保证订单号是此门店的收银员
//            $MerchantStore = MerchantStore::where('store_id', $order->store_id)
//                ->where('merchant_id', $merchant_id)
//                ->select('id')
//                ->first();
//
//            if (!$MerchantStore) {
//                return json_encode([
//                    'status' => 2,
//                    'message' => '订单号不在你的查询范围'
//                ]);
//            }

            $store_id = $order->store_id;
            $ways_type = $order->ways_type;
            $store = Store::where('store_id', $store_id)
                ->select('config_id', 'merchant_id', 'pid', 'people_phone', 'source')
                ->first();
            $config_id = $store->config_id;
            $store_id = $order->store_id;
            $store_pid = $store->pid;
            $other_no = $order->other_no;
            $out_trade_no = $order->out_trade_no;
            $total_amount = $order->total_amount; //交易总额
            $trade_no = $order->trade_no;
            $rate = $order->rate; //订单但是费率

            $new_refund_amount = max(($order->refund_amount * 100 + $refund_amount * 100) / 100, 0); //总退款

            $new_fee_amount = (max(($order->total_amount * 100 - $order->mdiscount_amount * 100 - $new_refund_amount * 100), 0)) * ($rate / 100); //退款后的手续费

            //支付宝官方
            if (999 < $ways_type && $ways_type < 1999) {
                //配置
                $isvconfig = new AlipayIsvConfigController();
                $config_type = '01';
                $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);

                //获取token
                $storeInfo = $isvconfig->alipay_auth_info($store_id, $store_pid);

                $aop = new AopClient();
                $aop->apiVersion = "2.0";
                $aop->appId = $config->app_id;
                $aop->rsaPrivateKey = $config->rsa_private_key;
                $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                $aop->signType = "RSA2"; //升级算法
                $aop->gatewayUrl = $config->alipay_gateway;
                $aop->format = "json";
                $aop->charset = "GBK";
                $aop->version = "2.0";
                $aop->method = "alipay.trade.refund";

                $requests = new AlipayTradeRefundRequest();
                $data_req_ali = "{" .
                    "\"out_trade_no\":\"" . $out_trade_no . "\"," .
                    "\"refund_amount\":\"" . $refund_amount . "\"," .
                    "\"out_request_no\":\"" . $OutRefundNo . "\"," .
                    "\"refund_reason\":\"正常退款\"" .
                    "}";
                $requests->setBizContent($data_req_ali);
                $result = $aop->execute($requests, null, $storeInfo->app_auth_token);
                $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
                $resultCode = $result->$responseNode->code;

                //退款成功
                if (!empty($resultCode) && $resultCode == 10000) {

                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount,//退款金额
                        'refund_no' => $OutRefundNo,//退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $OutRefundNo,//退款单号
                        'other_no' => $other_no
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    return json_encode([
                        'status' => '1',
                        'message' => '退款成功',
                        'data' => $data
                    ]);
                } else {
                    //退款失败
                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                    ];

                    return json_encode([
                        'status' => 2,
                        'message' => $result->$responseNode->sub_msg,
                        'data' => $data,
                    ]);
                }
            }

            //直付通
            if (16000 < $ways_type && $ways_type < 16999) {
                $config_type = '03';
                //配置
                $isvconfig = new AlipayIsvConfigController();
                $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);
                if (!$config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '直付通配置不存在'
                    ]);
                }

                $aop = new AopClient();
                $aop->apiVersion = "2.0";
                $aop->appId = $config->app_id;
                $aop->rsaPrivateKey = $config->rsa_private_key;
                $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                $aop->signType = "RSA2"; //升级算法
                $aop->gatewayUrl = $config->alipay_gateway;
                $aop->format = "json";
                $aop->charset = "GBK";
                $aop->version = "2.0";
                $aop->method = "alipay.trade.refund";

                $requests = new AlipayTradeRefundRequest();
                $data_re = array(
                    'out_trade_no' => $out_trade_no,
                    'refund_amount' => $refund_amount,
                    'out_request_no' => $OutRefundNo,
                    'refund_reason' => '正常退款',
                );

                $a = array('a', '_', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
                $b = str_ireplace($a, "", $out_trade_no);
                $day = substr($b, 0, 8);
                $table = 'settle_orders_' . $day;
                if (Schema::hasTable($table)) {
                    $settle_orders = DB::table($table)->where('out_trade_no', $out_trade_no)
                        ->where('store_id', $store_id)
                        ->first();
                    if ($settle_orders && $settle_orders->order_settle_amount > 0) {
                        $data_re['refund_royalty_parameters'] = array(
                            0 => array(
                                'trans_out' => $settle_orders->trans_out,
                                'amount' => $settle_orders->order_settle_amount,
                                'desc' => '退款分账'
                            )
                        );
                    }
                }

                $data_re = json_encode($data_re);
                $requests->setBizContent($data_re);
                $result = $aop->execute($requests, null, '');
                $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
                $resultCode = $result->$responseNode->code;

                //退款成功
                if (!empty($resultCode) && $resultCode == 10000) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $OutRefundNo, //退款单号
                        'other_no' => $other_no
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    return json_encode([
                        'status' => '1',
                        'message' => '退款成功',
                        'data' => $data
                    ]);
                } else {
                    //退款失败
                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                    ];

                    return json_encode([
                        'status' => '2',
                        'message' => $result->$responseNode->sub_msg,
                        'data' => $data
                    ]);
                }
            }

            //微信官方扫码退款
            if (1999 < $ways_type && $ways_type < 2999) {
                $config = new WeixinConfigController();
                $options = $config->weixin_config($config_id);
                $weixin_store = $config->weixin_merchant($store_id, $store_pid);
                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;

                $config = [
                    'app_id' => $options['app_id'],
                    'mch_id' => $options['payment']['merchant_id'],
                    'key' => $options['payment']['key'],
                    'cert_path' => $options['payment']['cert_path'], // XXX: 绝对路径！！！！
                    'key_path' => $options['payment']['key_path'],     // XXX: 绝对路径！！！！
                    'sub_mch_id' => $wx_sub_merchant_id,
                    // 'device_info'     => '013467007045764',
                    // 'sub_app_id'      => '',
                    // ...
                ];

                $payment = Factory::payment($config);
                // 参数分别为：商户订单号、商户退款单号、订单金额、退款金额、其他参数
                $refund = $payment->refund->byOutTradeNumber($order->out_trade_no, $OutRefundNo, $order->total_amount * 100, $refund_amount * 100);

                if ($refund['return_code'] == "SUCCESS") {
                    //退款成功
                    if ($refund['result_code'] == "SUCCESS") {
                        $insert_data = [
                            'status' => 6,
                            'pay_status' => 6,
                            'pay_status_desc' => '已退款',
                            'refund_amount' => $new_refund_amount,
                            'fee_amount' => $new_fee_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        RefundOrder::create([
                            'ways_source' => $order->ways_source,
                            'type' => $ways_type,
                            'refund_amount' => $refund_amount, //退款金额
                            'refund_no' => $OutRefundNo, //退款单号
                            'store_id' => $store_id,
                            'merchant_id' => $merchant_id,
                            'out_trade_no' => $order->out_trade_no,
                            'trade_no' => $order->trade_no
                        ]);

                        $data = [
                            'refund_amount' => $refund_amount,
                            'out_trade_no' => $out_trade_no,
                            'refund_no' => $OutRefundNo, //退款单号
                            'other_no' => $other_no
                        ];

                        //返佣去掉
                        UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                            'settlement' => '03',
                            'settlement_desc' => '退款订单',
                        ]);
                        MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                            'settlement' => '03',
                            'settlement_desc' => '退款订单',
                        ]);

                        return json_encode([
                            'status' => 1,
                            'message' => '退款成功',
                            'data' => $data,
                        ]);
                    } else {
                        $data = [
                            'refund_amount' => $refund_amount,
                            'out_trade_no' => $out_trade_no,
                        ];

                        return json_encode([
                            'status' => 2,
                            'message' => $refund['err_code_des'],
                            'data' => $data,
                        ]);
                    }
                } else {
                    //退款失败
                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                    ];

                    return json_encode([
                        'status' => '2',
                        'message' => $refund['return_msg'],
                        'data' => $data
                    ]);
                }
            }

            //微信官方a 扫码退款
            if (3999 < $ways_type && $ways_type < 4999) {
                $config = new WeixinConfigController();
                $options = $config->weixina_config($config_id);
                $weixin_store = $config->weixina_merchant($store_id, $store_pid);
                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;

                $config = [
                    'app_id' => $options['app_id'],
                    'mch_id' => $options['payment']['merchant_id'],
                    'key' => $options['payment']['key'],
                    'cert_path' => $options['payment']['cert_path'], // XXX: 绝对路径！！！！
                    'key_path' => $options['payment']['key_path'],     // XXX: 绝对路径！！！！
                    'sub_mch_id' => $wx_sub_merchant_id,
                    // 'device_info'     => '013467007045764',
                    // 'sub_app_id'      => '',
                    // ...
                ];
                $payment = Factory::payment($config);

                // 参数分别为：商户订单号、商户退款单号、订单金额、退款金额、其他参数
                $refund = $payment->refund->byOutTradeNumber($order->out_trade_no, $OutRefundNo, $order->total_amount * 100, $refund_amount * 100);
                if ($refund['return_code'] == "SUCCESS") {
                    //退款成功
                    if ($refund['result_code'] == "SUCCESS") {
                        $insert_data = [
                            'status' => 6,
                            'pay_status' => 6,
                            'pay_status_desc' => '已退款',
                            'refund_amount' => $new_refund_amount,
                            'fee_amount' => $new_fee_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        RefundOrder::create([
                            'ways_source' => $order->ways_source,
                            'type' => $ways_type,
                            'refund_amount' => $refund_amount, //退款金额
                            'refund_no' => $OutRefundNo, //退款单号
                            'store_id' => $store_id,
                            'merchant_id' => $merchant_id,
                            'out_trade_no' => $order->out_trade_no,
                            'trade_no' => $order->trade_no
                        ]);

                        $data = [
                            'refund_amount' => $refund_amount,
                            'out_trade_no' => $out_trade_no,
                            'refund_no' => $OutRefundNo, //退款单号
                            'other_no' => $other_no
                        ];

                        //返佣去掉
                        UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                            'settlement' => '03',
                            'settlement_desc' => '退款订单',
                        ]);
                        MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                            'settlement' => '03',
                            'settlement_desc' => '退款订单',
                        ]);

                        return json_encode([
                            'status' => 1,
                            'message' => '退款成功',
                            'data' => $data,
                        ]);
                    } else {
                        $data = [
                            'refund_amount' => $refund_amount,
                            'out_trade_no' => $out_trade_no,
                        ];

                        return json_encode([
                            'status' => 2,
                            'message' => $refund['err_code_des'],
                            'data' => $data,
                        ]);
                    }
                } else {
                    //退款失败
                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                    ];

                    return json_encode([
                        'status' => '2',
                        'message' => $refund['return_msg'],
                        'data' => $data
                    ]);
                }
            }

            //京东收银通道
            if (5999 < $ways_type && $ways_type < 6999) {
                //读取配置
                $config = new JdConfigController();
                $jd_config = $config->jd_config($config_id);
                if (!$jd_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '京东配置不存在请检查配置'
                    ]);
                }

                $jd_merchant = $config->jd_merchant($store_id, $store_pid);
                if (!$jd_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '京东商户号不存在'
                    ]);
                }
                $obj = new \App\Api\Controllers\Jd\PayController();
                $data = [];
                $data['out_trade_no'] = $out_trade_no;
                $data['request_url'] = $obj->refund_url; //请求地址;
                $data['notifyUrl'] = url('/api/jd/refund_url'); //通知地址;
                $data['merchant_no'] = $jd_merchant->merchant_no;
                $data['md_key'] = $jd_merchant->md_key; //
                $data['des_key'] = $jd_merchant->des_key; //
                $data['systemId'] = $jd_config->systemId; //
                $data['outRefundNo'] = $OutRefundNo;
                $data['amount'] = $order->total_amount;

                $return = $obj->refund($data);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $OutRefundNo, //退款单号
                        'other_no' => $other_no

                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    return json_encode([
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 2,
                        'message' => $message
                    ]);
                }
            }

            //快钱支付通道
            if (2999 < $ways_type && $ways_type < 3999) {
                //读取配置
                $config = new MyBankConfigController();
                $MyBankConfig = $config->MyBankConfig($config_id);
                if (!$MyBankConfig) {
                    return json_encode([
                        'status' => 2,
                        'message' => '快钱配置不存在请检查配置'
                    ]);
                }

                $mybank_merchant = $config->mybank_merchant($store_id, $store_pid);
                if (!$mybank_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '快钱商户号不存在'
                    ]);
                }

                $obj = new TradePayController();
                $MerchantId = $mybank_merchant->MerchantId;
                $return = $obj->mybankrefund($MerchantId, $out_trade_no, $OutRefundNo, $refund_amount, $config_id);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount,//退款金额
                        'refund_no' => $OutRefundNo,//退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $OutRefundNo,//退款单号
                        'other_no' => $other_no

                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    return json_encode([
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 2,
                        'message' => $message
                    ]);
                }
            }

            //新大陆
            if (7999 < $ways_type && $ways_type < 8999) {
                if ($refund_amount && $refund_amount != $order->total_amount) {
                    return json_encode([
                        'status' => 2,
                        'message' => '只支持退全额'
                    ]);
                }

                //读取配置
                $config = new NewLandConfigController();
                $new_land_config = $config->new_land_config($config_id);
                if (!$new_land_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '新大陆配置不存在请检查配置'
                    ]);
                }

                $new_land_merchant = $config->new_land_merchant($store_id, $store_pid);
                if (!$new_land_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户新大陆通道未开通'
                    ]);
                }
                $request_data = [
                    'out_trade_no' => $out_trade_no,
                    'trade_no' => $order->trade_no,
                    'key' => $new_land_merchant->nl_key,
                    'org_no' => $new_land_config->org_no,
                    'merc_id' => $new_land_merchant->nl_mercId,
                    'trm_no' => $new_land_merchant->trmNo,
                    'op_sys' => '3',
                    'opr_id' => $store->merchant_id,
                    'trm_typ' => 'T',
                ];
                $obj = new PayController();
                $return = $obj->refund($request_data);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount,//退款金额
                        'refund_no' => $OutRefundNo,//退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $OutRefundNo,//退款单号
                        'other_no' => $other_no

                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    return json_encode([
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 2,
                        'message' => $message
                    ]);
                }
            }

            //和融通
            if (8999 < $ways_type && $ways_type < 9999) {
                if ($refund_amount && $refund_amount != $order->total_amount) {
                    return json_encode([
                        'status' => 2,
                        'message' => '只支持退全额'
                    ]);
                }
                //读取配置
                $config = new HConfigController();
                $h_config = $config->h_config($config_id);
                if (!$h_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '和融通配置不存在请检查配置'
                    ]);
                }

                $h_merchant = $config->h_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '和融通商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Huiyuanbao\PayController();
                $data = [];
                $data['trade_no'] = $order->trade_no;
                $data['request_url'] = $obj->refund_url; //请求地址;
                $data['notifyUrl'] = url('/api/jd/refund_url'); //通知地址;
                $data['mid'] = $h_merchant->h_mid;
                $data['md_key'] = $h_config->md_key; //
                $data['orgNo'] = $h_merchant->orgNo; //
                $data['outRefundNo'] = $OutRefundNo;
                $data['amount'] = $order->total_amount;
                $return = $obj->refund($data);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount,//退款金额
                        'refund_no' => $return['data']['refundOrderNo'],//退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $return['data']['refundOrderNo'],//退款单号
                        'other_no' => $other_no

                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    return json_encode([
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 2,
                        'message' => $message
                    ]);
                }
            }

            //lft收银通道
            if (9999 < $ways_type && $ways_type < 10999) {
                //读取配置
                $config = new LtfConfigController();
                $ltf_merchant = $config->ltf_merchant($store_id, $store_pid);
                if (!$ltf_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Ltf\PayController();
                $data = [];
                $data['out_trade_no'] = $out_trade_no;
                $data['request_url'] = $obj->refund_url; //请求地址;
                $data['notifyUrl'] = url('/api/jd/refund_url'); //通知地址;
                $data['merchant_no'] = $ltf_merchant->merchantCode;
                $data['appId'] = $ltf_merchant->appId; //
                $data['key'] = $ltf_merchant->md_key; //
                $data['outRefundNo'] = $OutRefundNo;
                $data['amount'] = $refund_amount;
                $return = $obj->refund($data);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount,//退款金额
                        'refund_no' => $OutRefundNo,//退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $OutRefundNo,//退款单号
                        'other_no' => $other_no
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    return json_encode([
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 2,
                        'message' => $message
                    ]);
                }
            }

            //随行付收银通道
            if (12999 < $ways_type && $ways_type < 13999) {
                //读取配置
                $config = new VbillConfigController();
                $vbill_config = $config->vbill_config($config_id);
                if (!$vbill_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付配置不存在请检查配置'
                    ]);
                }

                $vbill_merchant = $config->vbill_merchant($store_id, $store_pid);
                if (!$vbill_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Vbill\PayController();
                $data['notify_url'] = url('/api/vbill/refund_notify_url'); //退款回调地址
                $data['request_url'] = $obj->refund_url; //请求地址;
                $data['mno'] = $vbill_merchant->mno;
                $data['privateKey'] = $vbill_config->privateKey; //
                $data['sxfpublic'] = $vbill_config->sxfpublic; //
                $data['orgId'] = $vbill_config->orgId; //
                $data['out_trade_no'] = $order->out_trade_no;
                $data['refund_amount'] = $refund_amount;

                $return = $obj->refund($data);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $OutRefundNo, //退款单号
                        'other_no' => $other_no
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    return json_encode([
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 2,
                        'message' => $message
                    ]);
                }
            }

            //随行付a收银通道
            if (18999 < $ways_type && $ways_type < 19999) {
                //读取配置
                $config = new VbillConfigController();
                $vbill_config = $config->vbilla_config($config_id);
                if (!$vbill_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付A配置不存在请检查配置'
                    ]);
                }

                $vbill_merchant = $config->vbilla_merchant($store_id, $store_pid);
                if (!$vbill_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付A商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Vbill\PayController();
                $data['notify_url'] = url('/api/vbill/refund_notify_url'); //退款回调地址
                $data['request_url'] = $obj->refund_url; //请求地址;
                $data['mno'] = $vbill_merchant->mno;
                $data['privateKey'] = $vbill_config->privateKey; //
                $data['sxfpublic'] = $vbill_config->sxfpublic; //
                $data['orgId'] = $vbill_config->orgId; //
                $data['out_trade_no'] = $order->out_trade_no;
                $data['refund_amount'] = $refund_amount;

                $return = $obj->refund($data);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $OutRefundNo,//退款单号
                        'other_no' => $other_no
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    return json_encode([
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 2,
                        'message' => $message
                    ]);
                }
            }

            //哆啦宝
            if (14999 < $ways_type && $ways_type < 15999) {
                if ($refund_amount && $refund_amount != $order->total_amount) {
                    return json_encode([
                        'status' => 2,
                        'message' => '只支持退全额'
                    ]);
                }

                $OutRefundNo = "dlbscan" . date('YmdHis') . str_pad(rand(0, 9999), 4, 0);
                $manager = new ManageController();
                $dlb_config = $manager->pay_config($config_id);

                if (!$dlb_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '哆啦宝配置配置不存在请检查配置'
                    ]);
                }
                $dlb_merchant = $manager->dlb_merchant($store_id, $store_pid);
                if (!$dlb_merchant && !empty($dlb_merchant->mch_num) && !empty($dlb_merchant->shop_num)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '哆啦宝配置商户未补充商户编号等信息!'
                    ]);
                }
                $refund_data = [
                    "accessKey" => $dlb_config->access_key,
                    "secretKey" => $dlb_config->secret_key,
                    "agentNum" => $dlb_config->agent_num,
                    "customerNum" => $dlb_merchant->mch_num,
                    "shopNum" => $dlb_merchant->shop_num,
                    "requestNum" => $out_trade_no,
                    "refundRequestNum" => $OutRefundNo,
                ];
                $return = $manager->pay_refund($refund_data);
                if ($return['status'] == 1) {
                    //退款请求成功
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount,//退款金额
                        'refund_no' => $OutRefundNo,//退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $OutRefundNo,//退款单号
                        'other_no' => $other_no
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    return json_encode([
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ]);
                } else {
                    return json_encode($return);
                }
            }

            //传化
            if (11999 < $ways_type && $ways_type < 12999) {
                //读取配置
                $config = new TfConfigController();

                $h_merchant = $config->tf_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '传化商户号不存在'
                    ]);
                }

                $h_config = $config->tf_config($config_id, $h_merchant->qd);
                if (!$h_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '传化配置不存在请检查配置'
                    ]);
                }

                $obj = new \App\Api\Controllers\Tfpay\PayController();
                $data['mch_id'] = $h_config->mch_id; //
                $data['pub_key'] = $h_config->pub_key; //
                $data['pri_key'] = $h_config->pri_key; //
                $data['sub_mch_id'] = $h_merchant->sub_mch_id; //
                $data['out_trade_no'] = $out_trade_no; //
                $data['date'] = date('Y-m-d', time()); //
                $data['refund_trade_no'] = time(); //
                $data['refund_fee'] = $refund_amount; //

                $return = $obj->refund($data);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount,//退款金额
                        'refund_no' => $return['data']['refund_trade_no'],//退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $return['data']['refund_trade_no'],//退款单号
                        'other_no' => $other_no
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    $re_data = [
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ];

                    return json_encode($re_data);
                } else {
                    //其他情况
                    $message = $return['message'];

                    return json_encode([
                        'status' => 2,
                        'message' => $message
                    ]);
                }
            }

            //工行
            if (19999 < $ways_type && $ways_type < 20999) {
                $config = new LianfuConfigController();
                $h_merchant = $config->lianfu_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\lianfu\PayController();
                $data['apikey'] = $h_merchant->apikey; //
                $data['signkey'] = $h_merchant->signkey; //
                $data['out_trade_no'] = $out_trade_no; //
                $data['refund_no'] = time(); //
                $data['pay_amount'] = $refund_amount; //
                $return = $obj->refund($data);
                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $out_trade_no, //退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $out_trade_no, //退款单号
                        'other_no' => $other_no
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    $re_data = [
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ];

                    return json_encode($re_data);
                } else {
                    //其他情况
                    $message = $return['message'];

                    return json_encode([
                        'status' => 2,
                        'message' => $message
                    ]);
                }
            }

            //海科融通 退款
            if (21999 < $ways_type && $ways_type < 22999) {
                //读取配置
                $config = new HkrtConfigController();
                $hkrt_config = $config->hkrt_config($config_id);
                if (!$hkrt_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '海科融通配置不存在请检查配置'
                    ]);
                }

                $hkrt_merchant = $config->hkrt_merchant($store_id, $store_pid);
                if (!$hkrt_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '海科融通商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Hkrt\PayController();
                $hkrt_return_data = [
                    'access_id' => $hkrt_config->access_id,
                    'refund_amount' => $refund_amount, //退款金额,（银联二维码只能全额退款）退款金额，以元为单位
                    'trade_no' => $order->trade_no, //SaaS平台的交易订单编号,trade_no、out_trade_no、channel_trade_no必传其中一个，都传则以trade_no为准，推荐使用trade_no
                    'out_trade_no' => $order->out_trade_no, //服务商退款订单号
                    'notify_url' => url('/api/hkrt/refund_notify_url'), //退款成功后的通知地址
                    'access_key' => $hkrt_config->access_key
                ];
                Log::info('海科融通-退款');
                Log::info($hkrt_return_data);
                $return = $obj->refund($hkrt_return_data); //0-系统错误 1-成功 2-失败 3-结果未知
                Log::info($return);

                //退款请求成功
                if ($return['status'] == '1') {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $order->refund_amount + $return['data']['refunded_amount'],
                        'fee_amount' => 0,
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $return['data']['refunded_amount'], //退款金额
                        'refund_no' => $return['data']['refund_no'], //退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $return['data']['refunded_amount'],
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $return['data']['refund_no'], //退款单号
                        'other_no' => $other_no
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    return json_encode([
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ]);
                } else {
                    //其他情况
                    $message = $return['data']['message'];
                    return json_encode([
                        'status' => 2,
                        'message' => $message
                    ]);
                }
            }

            //易生支付 退款
            if (20999 < $ways_type && $ways_type < 21999) {
                //读取配置
                $easyPayStoresImages = EasypayStoresImages::where('store_id', $store_id)->select('new_config_id')->first();
                if (!$easyPayStoresImages) {
                    $easyPayStoresImages = EasypayStoresImages::where('store_id', $store_pid)->select('new_config_id')->first();
                }
                $config = new EasyPayConfigController();
                $easypay_config = $config->easypay_config($easyPayStoresImages->new_config_id);
                if (!$easypay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生支付配置不存在请检查配置'
                    ]);
                }

                $easypay_merchant = $config->easypay_merchant($store_id, $store_pid);
                if (!$easypay_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生支付商户号不存在'
                    ]);
                }

                //当天的交易只能全额撤销,日切时间为渠道服务器的00:00；非当日才可退款
                $obj = new \App\Api\Controllers\EasyPay\PayController();
                $easypay_return_data = [
                    'channel_id' => $easypay_config->channel_id, //渠道编号
                    'mer_id' => $easypay_merchant->term_mercode, //终端商戶编号
                    'term_id' => $easypay_merchant->term_termcode, //终端编号
                    'out_trade_no' => $out_trade_no, //订单号
                    'trade_no' => $order->trade_no, //系统订单号
                    'refund_amount' => $refund_amount, //退货金额,不大于原交易金额与已成功退货金额之差
                    'key' => $easypay_config->channel_key
                ];
                Log::info('易生支付-退款');
                Log::info($easypay_return_data);
                $return = $obj->refund($easypay_return_data); // -1 系统错误 0-其他 1-成功 2-失败
                Log::info($return);

                //退款请求成功
                if ($return['status'] == '1') {
                    $amount = $order->total_amount - $order->refund_amount;
                    if ($amount > $refund_amount) {
                        $status = 7;
                        $pay_status = 7;
                        $pay_status_desc = '有退款';
                    } else {
                        $status = 6;
                        $pay_status = 6;
                        $pay_status_desc = '已退款';
                    }
                    $insert_data = [
                        'status' => $status,
                        'pay_status' => $pay_status,
                        'pay_status_desc' => $pay_status_desc,
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount, //退款金额
                        //'refund_no' => $return['data']['oriwtorderid'], //易生退货单号 1.0
                        'refund_no' => $return['data']['orgTrace'], //易生退货单号 2.0
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        //'refund_no' => $return['data']['oriwtorderid'], //易生退款单号1.0
                        'refund_no' => $return['data']['orgTrace'], //易生退款单号
                        'other_no' => $other_no
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    return json_encode([
                        'status' => '1',
                        'message' => '退款成功',
                        'data' => $data,
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => '2',
                        'message' => $message
                    ]);
                }
//                }
            }

            //邮驿付  退款
            if (29000 < $ways_type && $ways_type < 29010) {
                //读取配置
                $config = new PostPayConfigController();
                $post_config = $config->post_pay_config($config_id);
                if (!$post_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '邮驿付配置不存在请检查配置'
                    ]);
                }

                $post_merchant = $config->post_pay_merchant($store_id, $store_pid);
                if (!$post_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '邮驿付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\PostPay\PayController();
                if (strlen($OutRefundNo) > 20) {
                    $OutRefundNo = substr($OutRefundNo, 0, 15) . substr(microtime(), 2, 5);
                }
                $data = [
                    'out_trade_no' => $order->trade_no, //订单号
                    'phone' => $store->people_phone,
                    'agetId' => $post_config->org_id,
                    'custId' => $post_merchant->cust_id,
                    'driveNo' => $post_merchant->drive_no,
                    'refund_amount' => $refund_amount,
                    'orderNo' => $OutRefundNo
                ];
                if ($ways_type == '29001') {
                    $data['tag'] = '1';//支付宝
                } elseif ($ways_type == '29002') {
                    $data['tag'] = '2';//微信
                } else {
                    $data['tag'] = '9';//银联
                }

                $return = $obj->refund($data);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $OutRefundNo, //退款单号
                        'other_no' => $other_no
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    return json_encode([
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 2,
                        'message' => $message
                    ]);
                }
            }

            //邮政 退款
            if (26000 < $ways_type && $ways_type < 26999) {
                $config = new LianfuyoupayConfigController();
                $h_merchant = $config->lianfu_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '邮政商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\lianfuyouzheng\PayController();
                $data['apikey'] = $h_merchant->apikey; //
                $data['signkey'] = $h_merchant->signkey; //
                $data['out_trade_no'] = $out_trade_no; //
                $data['refund_no'] = time(); //
                $data['pay_amount'] = $refund_amount; //
                $data['pos_sn'] = $h_merchant->pos_sn; //

                $return = $obj->refund($data);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $out_trade_no, //退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $out_trade_no, //退款单号
                        'other_no' => $other_no
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    $re_data = [
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ];

                    return json_encode($re_data);
                } else {
                    //其他情况
                    $message = $return['message'];

                    return json_encode([
                        'status' => 2,
                        'message' => $message
                    ]);
                }
            }

            //苏州银行
            if (17000 < $ways_type && $ways_type < 17999) {
                $config = new SuzhouConfigController();
                $h_merchant = $config->sz_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户号不存在'
                    ]);
                }

                $h_config = $config->sz_config($config_id);
                if (!$h_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '配置不存在请检查配置'
                    ]);
                }

                $obj = new \App\Api\Controllers\Suzhou\PayController();
                $data['certId'] = $h_config->certId; //
                $data['pri_key'] = $h_config->rsa_pr; //
                $data['merchantId'] = $h_merchant->MerchantId; //
                $data['orderTime'] = date('YmdHis', time()); //
                $data['oldOrderTime'] = date('YmdHis', strtotime($order->created_at)); //
                $data['refundAmount'] = number_format($refund_amount * 100, 0, '.', '');

                $return = $obj->refund($data);
                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount,//退款金额
                        'refund_no' => $return['data']['tradeNo'],//退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $return['data']['tradeNo'],//退款单号
                        'other_no' => $other_no
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    $re_data = [
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ];

                    return json_encode($re_data);
                } else {
                    //其他情况
                    $message = $return['message'];

                    return json_encode([
                        'status' => 2,
                        'message' => $message
                    ]);
                }
            }

            //葫芦天下
            if (22999 < $ways_type && $ways_type < 23999) {
                $manager = new \App\Api\Controllers\Hltx\ManageController();

                $hltx_merchant = $manager->pay_merchant($store_id, $store_pid);
                $qd = $hltx_merchant->qd;
                $hltx_config = $manager->pay_config($config_id, $qd);
                if (!$hltx_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => 'HL支付配置不存在请检查配置'
                    ]);
                }

                $manager->init($hltx_config);
                switch ($ways_type) {
                    case 23001:
                        $pay_channel = 'ALI';
                        break;
                    case 23002:
                        $pay_channel = 'WX';
                        break;
                    case 23004:
                        $pay_channel = 'UPAY';
                        break;
                    default:
                        $pay_channel = 'ALI';
                        break;
                }

                if (!$hltx_merchant || empty($hltx_merchant->mer_no)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户未成功开通HL通道!'
                    ]);
                }

                $OutRefundNo = "hltx" . date('YmdHis') . str_pad(rand(0, 9999), 4, 0);
                $hltx_data = [
                    'merNo' => $hltx_merchant->mer_no,
                    'amount' => $refund_amount * 100,
                    'orderNo' => $OutRefundNo,
                    'oriOrderNo' => $out_trade_no,
                    'orderInfo' => '用户退款',
                    'deviceIp' => \EasyWeChat\Kernel\Support\get_client_ip(),
                ];

                $return = $manager->refund_order($hltx_data);
                if ($return['status'] == 1) {
                    $return = $return['data'];
                    if ($return['tradeStatus'] == 'S' || $return['tradeStatus'] == 'R') {
                        //退款请求成功
                        $insert_data = [
                            'status' => 6,
                            'pay_status' => 6,
                            'pay_status_desc' => '已退款',
                            'refund_amount' => $new_refund_amount,
                            'fee_amount' => $new_fee_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        RefundOrder::create([
                            'ways_source' => $order->ways_source,
                            'type' => $ways_type,
                            'refund_amount' => $refund_amount,//退款金额
                            'refund_no' => $OutRefundNo,//退款单号
                            'store_id' => $store_id,
                            'merchant_id' => $merchant_id,
                            'out_trade_no' => $order->out_trade_no,
                            'trade_no' => $order->trade_no
                        ]);

                        $data = [
                            'refund_amount' => $refund_amount,
                            'out_trade_no' => $out_trade_no,
                            'refund_no' => $OutRefundNo,//退款单号
                            'other_no' => $other_no
                        ];

                        //返佣去掉
                        UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                            'settlement' => '03',
                            'settlement_desc' => '退款订单',
                        ]);
                        MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                            'settlement' => '03',
                            'settlement_desc' => '退款订单',
                        ]);

                        return json_encode([
                            'status' => 1,
                            'message' => '退款成功',
                            'data' => $data,
                        ]);
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '退款失败' . (isset($return['message']) ? $return['message'] : ''),
                        ]);
                    }
                } else {
                    return json_encode($return);
                }
            }

            //长沙银行
            if (25000 < $ways_type && $ways_type < 25999) {
                $config = new ChangshaConfigController();

                $h_merchant = $config->cs_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户号不存在'
                    ]);
                }

                $h_config = $config->cs_config($config_id);
                if (!$h_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '配置不存在请检查配置'
                    ]);
                }

                $obj = new \App\Api\Controllers\Changsha\PayController();
                $data['ECustId'] = $h_merchant->ECustId; //
                $data['OrderId'] = $trade_no; //
                $data['refundAmount'] = $refund_amount; //

                $return = $obj->refund($data);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount,//退款金额
                        'refund_no' => $out_trade_no,//退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $out_trade_no,//退款单号
                        'other_no' => $other_no
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    $re_data = [
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ];

                    return json_encode($re_data);
                } else {
                    //其他情况
                    $message = $return['message'];

                    return json_encode([
                        'status' => 2,
                        'message' => $message
                    ]);
                }
            }

            //联动优势 退款
            if (4999 < $ways_type && $ways_type < 5999) {
                $config = new LinkageConfigController();
                $linkage_config = $config->linkage_config($config_id);
                if (!$linkage_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '联动优势支付配置不存在请检查配置'
                    ]);
                }

                $linkage_merchant = $config->linkage_merchant($store_id, $store_pid);
                if (!$linkage_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '联动优势支付商户号不存在'
                    ]);
                }

                //针对交易当天的成功订单可以进行撤销，过了账期则需要走退费接口。仅支持微信支付宝刷卡交易（即用户被扫）的撤销，其他支付成功订单如需实现相同功能请调用退款接口
                $obj = new \App\Api\Controllers\Linkage\PayController();
                $now_time_start = strtotime(date('Y-m-d 00:00:00', time()));
                $now_time_end = strtotime(date('Y-m-d 23:59:59', time()));
                $order_pay_time = strtotime($order->pay_time);

                if (($order_pay_time > $now_time_start) && ($order_pay_time <= $now_time_end)) {
                    if ($refund_amount && $refund_amount != $order->total_amount) {
                        return json_encode([
                            'status' => '2',
                            'message' => '当日交易只支持退全额'
                        ]);
                    }

                    $linkage_revoke_data = [
                        'acqSpId' => $linkage_config->mch_id, //代理商编号
                        'acqMerId' => $linkage_merchant->acqMerId, //商户号
                        'out_trade_no' => $out_trade_no, //订单号
                        'privateKey' => $linkage_config->privateKey, //
                        'publicKey' => $linkage_config->publicKey //
                    ];
                    Log::info('联动优势-当日交易撤销');
                    Log::info($linkage_revoke_data);
                    $return = $obj->order_revoke($linkage_revoke_data); //-1系统错误；0-其他；1-处理成功；2-验签失败
                    Log::info($return);

                    if ($return['data']['respCode'] == '42') { //不支持撤销，调用退款接口
                        $linkage_return_data = [
                            'acqSpId' => $linkage_config->mch_id, //代理商编号
                            'acqMerId' => $linkage_merchant->acqMerId, //商户号
                            'total_amount' => $total_amount, //原单交易金额
                            'out_trade_no' => $out_trade_no, //原单流水号,订单号
                            'trade_no' => $trade_no, //系统订单号
                            'refund_amount' => $refund_amount, //退货金额
                            'privateKey' => $linkage_config->privateKey, //
                            'publicKey' => $linkage_config->publicKey //
                        ];
                        Log::info('联动优势-当日撤销不支持下退款');
                        Log::info($linkage_return_data);
                        $return = $obj->refund($linkage_return_data); //-1系统错误；0-其他；1-成功；2-验签失败；3-失败
                        Log::info($return);

                        //退款请求成功
                        if ($return['status'] == '1') {
                            $insert_data = [
                                'status' => '6',
                                'pay_status' => 6,
                                'pay_status_desc' => '已退款',
                                'refund_amount' => $new_refund_amount,
                                'fee_amount' => $new_fee_amount
                            ];
                            $this->update_day_order($insert_data, $out_trade_no);

                            RefundOrder::create([
                                'ways_source' => $order->ways_source,
                                'type' => $ways_type,
                                'refund_amount' => $refund_amount, //退款金额
                                'refund_no' => $return['data']['transactionId'], //联动退款流水号
                                'store_id' => $store_id,
                                'merchant_id' => $merchant_id,
                                'out_trade_no' => $out_trade_no,
                                'trade_no' => $trade_no
                            ]);

                            $data = [
                                'refund_amount' => $refund_amount,
                                'out_trade_no' => $out_trade_no,
                                'refund_no' => $return['data']['transactionId'], //联动退款流水号
                                'other_no' => $other_no
                            ];

                            //返佣去掉
                            UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                                'settlement' => '03',
                                'settlement_desc' => '退款订单'
                            ]);
                            MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                                'settlement' => '03',
                                'settlement_desc' => '退款订单'
                            ]);

                            return json_encode([
                                'status' => '1',
                                'message' => '退款成功',
                                'data' => $data
                            ]);
                        } else {
                            //其他情况
                            $message = $return['message'];
                            return json_encode([
                                'status' => '2',
                                'message' => $message
                            ]);
                        }
                    }

                    //成功
                    if ($return['status'] == '1') {
                        $insert_data = [
                            'status' => '6',
                            'pay_status' => 6,
                            'pay_status_desc' => '已退款',
                            'refund_amount' => $new_refund_amount,
                            'fee_amount' => $new_fee_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        $refund_amount = ($refund_amount / 100); //金额，单位分

                        RefundOrder::create([
                            'ways_source' => $order->ways_source,
                            'type' => $ways_type,
                            'refund_amount' => $refund_amount, //退款金额
                            'refund_no' => $return['data']['transactionId'], //联动流水号
                            'store_id' => $store_id,
                            'merchant_id' => $merchant_id,
                            'out_trade_no' => $out_trade_no,
                            'trade_no' => $trade_no
                        ]);

                        $data = [
                            'refund_amount' => $refund_amount,
                            'out_trade_no' => $out_trade_no,
                            'refund_no' => $return['data']['transactionId'], //联动流水号
                            'other_no' => $other_no
                        ];

                        //返佣去掉
                        UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                            'settlement' => '03',
                            'settlement_desc' => '退款订单',
                        ]);
                        MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                            'settlement' => '03',
                            'settlement_desc' => '退款订单',
                        ]);

                        return json_encode([
                            'status' => '1',
                            'message' => '退款成功',
                            'data' => $data,
                        ]);
                    } else {
                        //其他情况
                        $message = $return['message'];
                        return json_encode([
                            'status' => '2',
                            'message' => $message
                        ]);
                    }
                } else {
                    $linkage_return_data = [
                        'acqSpId' => $linkage_config->mch_id, //代理商编号
                        'acqMerId' => $linkage_merchant->acqMerId, //商户号
                        'total_amount' => $total_amount, //原单交易金额
                        'out_trade_no' => $out_trade_no, //原单流水号,订单号
                        'trade_no' => $trade_no, //系统订单号
                        'refund_amount' => $refund_amount, //退货金额
                        'privateKey' => $linkage_config->privateKey, //
                        'publicKey' => $linkage_config->publicKey //
                    ];
                    Log::info('联动优势-退款');
                    Log::info($linkage_return_data);
                    $return = $obj->refund($linkage_return_data); //-1系统错误；0-其他；1-成功；2-验签失败；3-失败
                    Log::info($return);

                    //退款请求成功
                    if ($return['status'] == '1') {
                        $insert_data = [
                            'status' => '6',
                            'pay_status' => 6,
                            'pay_status_desc' => '已退款',
                            'refund_amount' => $new_refund_amount,
                            'fee_amount' => $new_fee_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        RefundOrder::create([
                            'ways_source' => $order->ways_source,
                            'type' => $ways_type,
                            'refund_amount' => $refund_amount, //退款金额
                            'refund_no' => $return['data']['transactionId'], //联动退款流水号
                            'store_id' => $store_id,
                            'merchant_id' => $merchant_id,
                            'out_trade_no' => $out_trade_no,
                            'trade_no' => $trade_no
                        ]);

                        $data = [
                            'refund_amount' => $refund_amount,
                            'out_trade_no' => $out_trade_no,
                            'refund_no' => $return['data']['transactionId'], //联动退款流水号
                            'other_no' => $other_no
                        ];

                        //返佣去掉
                        UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                            'settlement' => '03',
                            'settlement_desc' => '退款订单'
                        ]);
                        MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                            'settlement' => '03',
                            'settlement_desc' => '退款订单'
                        ]);

                        return json_encode([
                            'status' => '1',
                            'message' => '退款成功',
                            'data' => $data
                        ]);
                    } else {
                        //其他情况
                        $message = $return['message'];
                        return json_encode([
                            'status' => '2',
                            'message' => $message
                        ]);
                    }
                }
            }

            //威富通 退款
            if (26999 < $ways_type && $ways_type < 27999) {
                $config = new WftPayConfigController();
                $wftpay_config = $config->wftpay_config($config_id);
                if (!$wftpay_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '威富通支付配置不存在请检查配置'
                    ]);
                }

                $wftpay_merchant = $config->wftpay_merchant($store_id, $store_pid);
                if (!$wftpay_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '威富通支付商户号不存在'
                    ]);
                }

                $wftpay_return_data = [
                    'mch_id' => $wftpay_merchant->mch_id,
                    'out_trade_no' => $out_trade_no,
                    'total_amount' => $total_amount,
                    'refund_fee' => $refund_amount,
                    'private_rsa_key' => $wftpay_config->private_rsa_key,
                    'public_rsa_key' => $wftpay_config->public_rsa_key
                ];
                Log::info('威富通-退款-入参');
                Log::info($wftpay_return_data);
                $obj = new WftPayPayController();
                $return = $obj->refund($wftpay_return_data); //0-系统错误 1-成功 2-退款失败 3-退款处理中 4-退款到银行发现用户的卡作废或者冻结了,导致原路退款银行卡失败
                Log::info('威富通-退款');
                Log::info($return);

                //退款请求成功
                if ($return['status'] == '1') {
                    $refund_count = $return['data']['refund_count'] - 1; //退款记录数
                    $insert_data = [
                        'status' => '6',
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $return['data']['refund_id_' . $refund_count], //平台退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $out_trade_no,
                        'trade_no' => $trade_no
                    ]);

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单'
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单'
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $return['data']['refund_id_' . $refund_count], //平台退款单号
                        'other_no' => $other_no
                    ];
                    return json_encode([
                        'status' => '1',
                        'message' => '退款成功',
                        'data' => $data
                    ]);
                } //退款处理中
                elseif ($return['status'] == '3') {
                    $insert_data = [
                        'status' => '5',
                        'pay_status' => 5,
                        'pay_status_desc' => '退款中'
                    ];
                    $res = $this->update_day_order($insert_data, $out_trade_no);
                    if (!$res) {
                        Log::info('威富通-退款中-更新失败');
                    }

                    $refund_count = $return['data']['refund_count'] - 1; //退款记录数
                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $return['data']['refund_id_' . $refund_count], //平台退款单号
                        'other_no' => $other_no
                    ];
                    return json_encode([
                        'status' => '1',
                        'message' => '退款中',
                        'data' => $data
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => '2',
                        'message' => $message
                    ]);
                }
            }

            //汇旺财 退款
            if (27999 < $ways_type && $ways_type < 28999) {
                $config = new HwcPayConfigController();
                $hwcpay_config = $config->hwcpay_config($config_id);
                if (!$hwcpay_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '汇旺财支付配置不存在请检查配置'
                    ]);
                }

                $hwcpay_merchant = $config->hwcpay_merchant($store_id, $store_pid);
                if (!$hwcpay_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '汇旺财支付商户号不存在'
                    ]);
                }

                $hwcpay_return_data = [
                    'mch_id' => $hwcpay_merchant->mch_id, //门店号
                    'merchant_num' => $hwcpay_merchant->merchant_num, //商户号
                    'out_trade_no' => $out_trade_no,
                    'total_amount' => $total_amount,
                    'refund_fee' => $refund_amount,
                    'private_rsa_key' => $hwcpay_config->private_rsa_key,
                    'public_rsa_key' => $hwcpay_config->public_rsa_key
                ];
//                Log::info('汇旺财-退款-入参');
//                Log::info($hwcpay_return_data);
                $obj = new HwcPayPayController();
                $return = $obj->refund($hwcpay_return_data); //0-系统错误 1-成功 2-退款失败 3-退款处理中 4-退款到银行发现用户的卡作废或者冻结了,导致原路退款银行卡失败
                Log::info('汇旺财-退款');
                Log::info($return);

                //退款请求成功
                if ($return['status'] == '1') {
                    $refund_count = $return['data']['refund_count'] - 1; //退款记录数
                    $insert_data = [
                        'status' => '6',
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $return['data']['refund_id_' . $refund_count], //平台退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $out_trade_no,
                        'trade_no' => $trade_no
                    ]);

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单'
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单'
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $return['data']['refund_id_' . $refund_count], //平台退款单号
                        'other_no' => $other_no
                    ];
                    return json_encode([
                        'status' => '1',
                        'message' => '退款成功',
                        'data' => $data
                    ]);
                } //退款处理中
                elseif ($return['status'] == '3') {
                    $insert_data = [
                        'status' => '5',
                        'pay_status' => 5,
                        'pay_status_desc' => '退款中'
                    ];
                    $res = $this->update_day_order($insert_data, $out_trade_no);
                    if (!$res) {
                        Log::info('汇旺财-退款中-更新失败');
                    }

                    $refund_count = $return['data']['refund_count'] - 1; //退款记录数
                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $return['data']['refund_id_' . $refund_count], //平台退款单号
                        'other_no' => $other_no
                    ];
                    return json_encode([
                        'status' => '1',
                        'message' => '退款中',
                        'data' => $data
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => '2',
                        'message' => $message
                    ]);
                }

            }

            //钱方 退款
            if (23999 < $ways_type && $ways_type < 24999) {
                $config = new QfPayConfigController();
                $qfpay_config = $config->qfpay_config($config_id);
                if (!$qfpay_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '钱方支付配置不存在请检查配置'
                    ]);
                }

                $qfpay_merchant = $config->qfpay_merchant($store_id, $store_pid);
                if (!$qfpay_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '钱方支付商户号不存在'
                    ]);
                }

                $qfpay_return_data = [
                    'key' => $qfpay_config->key,  //加签key
                    'code' => $qfpay_config->code,  //钱方唯一标识
                    'mchid' => $qfpay_merchant->mchid,  //子商户号,标识子商户身份
                    'trade_no' => $trade_no,  //钱方订单号,退款对应的原订单syssn
                    'out_trade_no' => $out_trade_no,  //外部订单号,同子商户(mchid)下,每次成功调用支付与退款接口,该参数值均不能重复使用,也不能与支付时的外部订单号相同,保证单号唯一,长度不超过128字符
                    'total_amount' => $refund_amount  //订单支付金额
                ];
                Log::info('钱方-退款-入参');
                Log::info($qfpay_return_data);
                $obj = new QfPayController();
                $return = $obj->refund($qfpay_return_data); //1-成功
                Log::info('钱方-退款-结果');
                Log::info($return);

                //退款请求成功
                if ($return['status'] == 1) {
                    $syssn = isset($return['data']['syssn']) ? $return['data']['syssn'] : ''; //退款交易唯一流水号
                    $orig_syssn = isset($return['data']['orig_syssn']) ? $return['data']['orig_syssn'] : ''; //原订交易流水号
                    $txamt = isset($return['data']['txamt']) ? $return['data']['txamt'] : 0; //订单支付金额，单位分
                    $txdtm = isset($return['data']['txdtm']) ? $return['data']['txdtm'] : ''; //请求方交易时间 格式为：YYYY-mm-dd HH:MM:DD
                    $sysdtm = isset($return['data']['sysdtm']) ? $return['data']['sysdtm'] : ''; //系统时间
                    $notifyurl = isset($return['data']['notifyurl']) ? $return['data']['notifyurl'] : ''; //异步通知地址用于请求POS的SDK（HF POS交易特有返回参数）
                    $termid = isset($return['data']['termid']) ? $return['data']['termid'] : ''; //逻辑终端号（HF POS交易额外返回参数）
                    $meroperid = isset($return['data']['meroperid']) ? $return['data']['meroperid'] : ''; //操作员号（HF POS交易额外返回参数）
                    $mchntid = isset($return['data']['mchntid']) ? $return['data']['mchntid'] : ''; //汇付商户号（HF POS交易额外返回参数）

                    $insert_data = [
                        'status' => '6',
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'refund_no' => $syssn,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $txamt / 100, //退款金额
                        'refund_no' => $syssn, //平台退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $out_trade_no,
                        'trade_no' => $orig_syssn
                    ]);

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单'
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单'
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $syssn, //平台退款单号
                        'other_no' => $other_no
                    ];
                    return json_encode([
                        'status' => '1',
                        'message' => '退款成功',
                        'data' => $data
                    ]);
                } else { //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => '2',
                        'message' => $message
                    ]);
                }

            }

            //易生数科支付 退款
            if (32000 < $ways_type && $ways_type < 32010) {
                //读取配置
                $config = new EasySkPayConfigController();
                $easyskpay_config = $config->easyskpay_config($config_id);
                if (!$easyskpay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生数科支付配置不存在请检查配置'
                    ]);
                }

                $easyskpay_merchant = $config->easyskpay_merchant($store_id, $store_pid);
                if (!$easyskpay_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生数科支付商户号不存在'
                    ]);
                }

                //当天的交易只能全额撤销,日切时间为渠道服务器的00:00；非当日才可退款
                $obj = new \App\Api\Controllers\EasySkPay\PayController();
//                $now_time_start = strtotime(date('Y-m-d 00:00:00', time()));
//                $now_time_end = strtotime(date('Y-m-d 23:59:59', time()));
//                $order_pay_time = strtotime($order->pay_time);
//
//                if (($order_pay_time > $now_time_start) && ($order_pay_time <= $now_time_end)) {
//                    if ($refund_amount && $refund_amount != $order->total_amount) {
//                        return json_encode([
//                            'status' => '2',
//                            'message' => '当日交易只支持退全额'
//                        ]);
//                    }
//
//                    $easysk_data_close = [
//                        'org_id' => $easyskpay_config->org_id, //渠道编号
//                        'mer_id' => $easyskpay_merchant->mer_id, //终端商戶编号
//                        'request_no' => $OutRefundNo,
//                        'orig_request_no' => $out_trade_no, //订单号
//                        'orig_trade_no' => $order->trade_no //系统订单号
//                    ];
//                    Log::info('易生数科支付-当日交易撤销');
//                    Log::info($easysk_data_close);
//                    $return = $obj->order_close($easysk_data_close); //-1 系统错误 0-其他 1-成功
//                    Log::info($return);
//
//                    //成功
//                    if ($return['status'] == '1') {
//                        $insert_data = [
//                            'status' => '6',
//                            'pay_status' => 6,
//                            'pay_status_desc' => '已退款',
//                            'refund_amount' => $new_refund_amount,
//                            'fee_amount' => $new_fee_amount
//                        ];
//                        $this->update_day_order($insert_data, $out_trade_no);
//                        $refund_amount = isset($return['data']['bizData']['amount']) ? ($return['data']['bizData']['amount'] / 100) : $refund_amount;
//
//                        RefundOrder::create([
//                            'ways_source' => $order->ways_source,
//                            'type' => $ways_type,
//                            'refund_amount' => $refund_amount, //退款金额
//                            'refund_no' => $return['data']['requestNo'],
//                            'store_id' => $store_id,
//                            'merchant_id' => $merchant_id,
//                            'out_trade_no' => $order->out_trade_no,
//                            'trade_no' => $return['data']['tradeNo'],
//                        ]);
//
//                        $data = [
//                            'refund_amount' => $refund_amount,
//                            'out_trade_no' => $out_trade_no,
//                            'refund_no' => $return['data']['requestNo'],
//                            'other_no' => $return['data']['tradeNo']
//                        ];
//
//                        //返佣去掉
//                        UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
//                            'settlement' => '03',
//                            'settlement_desc' => '退款订单',
//                        ]);
//                        MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
//                            'settlement' => '03',
//                            'settlement_desc' => '退款订单',
//                        ]);
//
//                        return json_encode([
//                            'status' => '1',
//                            'message' => '退款成功',
//                            'data' => $data,
//                        ]);
//                    } else {
//                        //其他情况
//                        $message = $return['message'];
//                        return json_encode([
//                            'status' => '2',
//                            'message' => $message
//                        ]);
//                    }
//                } else {
                $easysk_data_refund = [
                    'org_id' => $easyskpay_config->org_id, //渠道编号
                    'mer_id' => $easyskpay_merchant->mer_id, //终端商戶编号
                    'request_no' => $OutRefundNo,
                    'orig_request_no' => $out_trade_no, //订单号
                    'orig_trade_no' => $order->trade_no //系统订单号
                ];
                Log::info('易生数科支付-退款');
                Log::info($easysk_data_refund);
                $return = $obj->refund($easysk_data_refund); // -1 系统错误 0-其他 1-成功 2-失败
                Log::info($return);

                //退款请求成功
                if ($return['status'] == '1') {
                    $insert_data = [
                        'status' => '6',
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $return['data']['requestNo'],
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $return['data']['tradeNo'],
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $return['data']['requestNo'], //易生退款单号
                        'other_no' => $return['data']['tradeNo']
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    return json_encode([
                        'status' => '1',
                        'message' => '退款成功',
                        'data' => $data,
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => '2',
                        'message' => $message
                    ]);
                }
//                }
            }

            //银盛  退款
            if (14000 < $ways_type && $ways_type < 14010) {
                //读取配置
                $config = new YinshengConfigController();
                $yinsheng_config = $config->yinsheng_config($config_id);
                if (!$yinsheng_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '银盛配置不存在请检查配置'
                    ]);
                }

                $yinsheng_merchant = $config->yinsheng_merchant($store_id, $store_pid);
                if (!$yinsheng_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '银盛商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\YinSheng\PayController();

                $data = [
                    'out_trade_no' => $order->out_trade_no, //订单号
                    'partner_id' => $yinsheng_config->partner_id,
                    'shop_date' => $order->created_at,
                    'tran_type' => '1',
                    'refund_amount' => $refund_amount,
                    'orderNo' => $OutRefundNo,
                    'trade_no' => $order->trade_no
                ];

                $return = $obj->refund($data);

                //退款请求成功
                if ($return["status"] == 1) {
                    $amount = $order->total_amount - $order->refund_amount;
                    if ($amount > $refund_amount) {
                        $status = 7;
                        $pay_status = 7;
                        $pay_status_desc = '有退款';
                    } else {
                        $status = 6;
                        $pay_status = 6;
                        $pay_status_desc = '已退款';
                    }
                    $insert_data = [
                        'status' => $status,
                        'pay_status' => $pay_status,
                        'pay_status_desc' => $pay_status_desc,
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $OutRefundNo, //退款单号
                        'other_no' => $other_no
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    return json_encode([
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 2,
                        'message' => $message
                    ]);
                }
            }

            //通联支付 退款
            if (33000 < $ways_type && $ways_type < 33010) {
                //读取配置
                $config = new AllinPayConfigController();
                $allin_config = $config->allin_pay_config($config_id);
                if (!$allin_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '通联支付配置不存在请检查配置'
                    ]);
                }

                $allin_merchant = $config->allin_pay_merchant($store_id, $store_pid);
                if (!$allin_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '通联支付支付商户号不存在'
                    ]);
                }

                //当天的交易只能全额撤销,日切时间为渠道服务器的00:00；非当日才可退款
                $obj = new \App\Api\Controllers\AllinPay\PayController();
                $now_time_start = strtotime(date('Y-m-d 00:00:00', time()));
                $now_time_end = strtotime(date('Y-m-d 23:59:59', time()));
                $order_pay_time = strtotime($order->pay_time);
                $allin_refund_data = [
                    'ageId' => $allin_config->org_id, //代理编号
                    'appid' => $allin_merchant->appid,//平台分配的appid
                    'cusId' => $allin_merchant->cus_id, //商户编号
                    'orderNo' => $OutRefundNo, //退款单号
                    'oldreqsn' => $out_trade_no, //订单号
                    'oldtrxid' => $order->trade_no, //系统订单号
                    'refund_amount' => $refund_amount, //退货金额,不大于原交易金额与已成功退货金额之差
                ];
                Log::info('通联支付-退款');
                Log::info($allin_refund_data);
                //当天交易-撤销
                if (($order_pay_time > $now_time_start) && ($order_pay_time <= $now_time_end)) {
                    $return = $obj->cancel($allin_refund_data); // -1 系统错误 0-其他 1-成功 2-失败
                } else {//隔天交易-退款
                    $return = $obj->refund($allin_refund_data); // -1 系统错误 0-其他 1-成功 2-失败
                }
                Log::info($return);

                //退款请求成功
                if ($return['status'] == '1') {
                    $insert_data = [
                        'status' => '6',
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount,
                        'refund_no' => $OutRefundNo,
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $OutRefundNo,
                        'other_no' => $other_no
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    return json_encode([
                        'status' => '1',
                        'message' => '退款成功',
                        'data' => $data,
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => '2',
                        'message' => $message
                    ]);
                }
//                }
            }

            //富友支付 退款
            if (11000 < $ways_type && $ways_type < 11010) {
                //读取配置
                $config = new FuiouConfigController();
                $fuiou_config = $config->fuiou_config($config_id);
                if (!$fuiou_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '富友支付配置不存在请检查配置'
                    ]);
                }

                $fuiou_merchant = $config->fuiou_merchant($store_id, $store_pid);
                if (!$fuiou_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '富友支付商户号不存在'
                    ]);
                }
                $obj = new \App\Api\Controllers\Fuiou\PayController();
                $fuiou_refund_data = [
                    'ins_cd' => $fuiou_config->ins_cd, //代理编号
                    'mchnt_cd' => $fuiou_merchant->mchnt_cd,//平台分配的appid
                    'mchnt_order_no' => $out_trade_no, //订单号
                    'refund_order_no' => $OutRefundNo, //退款单号
                    'total_amt' => $refund_amount, //订单金额
                    'refund_amt' => $refund_amount, //退货金额,不大于原交易金额与已成功退货金额之差
                ];
                if ($order->ways_source == 'alipay') {
                    $fuiou_refund_data['order_type'] = 'ALIPAY';
                }
                if ($order->ways_source == 'weixin') {
                    $fuiou_refund_data['order_type'] = 'WECHAT';
                }
                if ($order->ways_source == 'unionpay') {
                    $fuiou_refund_data['order_type'] = 'UNIONPAY';
                }
                $return = $obj->refund($fuiou_refund_data); // -1 系统错误 0-其他 1-成功 2-失败

                //退款请求成功
                if ($return['status'] == '1') {
                    $insert_data = [
                        'status' => '6',
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount,
                        'refund_no' => $OutRefundNo,
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $OutRefundNo,
                        'other_no' => $other_no
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    return json_encode([
                        'status' => '1',
                        'message' => '退款成功',
                        'data' => $data,
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => '2',
                        'message' => $message
                    ]);
                }
//                }
            }

            //拉卡拉 退款
            if (34000 < $ways_type && $ways_type < 34010) {
                //读取配置
                $config = new LklConfigController();
                $lkl_config = $config->lkl_config($config_id);
                if (!$lkl_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '拉卡拉支付配置不存在请检查配置'
                    ]);
                }

                $lkl_merchant = $config->lkl_merchant($store_id, $store_pid);
                if (!$lkl_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '拉卡拉支付支付商户号不存在'
                    ]);
                }

                //当天的交易只能全额撤销,日切时间为渠道服务器的00:00；非当日才可退款
                $obj = new \App\Api\Controllers\LklPay\PayController();
                $lkl_refund_data = [
                    'merchant_no' => $lkl_merchant->customer_no,
                    'term_no' => $lkl_merchant->term_no,
                    'OutRefundNo' => $OutRefundNo,
                    'origin_out_trade_no' => $out_trade_no,
                    'refund_amount' => $refund_amount,
                ];
                Log::info('拉卡拉支付-退款');
                Log::info($lkl_refund_data);
                $return = $obj->refund($lkl_refund_data); // -1 系统错误 0-其他 1-成功 2-失败
                Log::info($return);

                //退款请求成功
                if ($return['status'] == '1') {
                    $amount = $order->total_amount - $order->refund_amount;
                    if ($amount > $refund_amount) {
                        $status = 7;
                        $pay_status = 7;
                        $pay_status_desc = '有退款';
                    } else {
                        $status = 6;
                        $pay_status = 6;
                        $pay_status_desc = '已退款';
                    }
                    $insert_data = [
                        'status' => $status,
                        'pay_status' => $pay_status,
                        'pay_status_desc' => $pay_status_desc,
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'ways_source' => $order->ways_source,
                        'type' => $ways_type,
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $data = [
                        'refund_amount' => $refund_amount,
                        'out_trade_no' => $out_trade_no,
                        'refund_no' => $OutRefundNo, //退款单号
                        'other_no' => $other_no
                    ];

                    //返佣去掉
                    UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);
                    MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                        'settlement' => '03',
                        'settlement_desc' => '退款订单',
                    ]);

                    return json_encode([
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $data,
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => '2',
                        'message' => $message
                    ]);
                }
//                }
            }

            $re_data = [
                'status' => 2,
                'message' => '等待收银员确认',
            ];
            return json_encode($re_data);
        } catch (\Exception $ex) {
            Log::info('商户端-退款');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ]);
        }
    }


    //对账统计-比较全-实时
    public function order_count(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $time_start_s = date('Y-m-d 00:00:00', time());
            $time_start_e = date('Y-m-d 23:59:59', time());
            $store_id = $request->get('store_id', '');
            $merchant_id = $request->get('merchant_id', '');
            $ways_source = $request->get('ways_source', '');
            $time_start = $request->get('time_start', $time_start_s); //开始时间
            $time_end = $request->get('time_end', $time_start_e); //结束时间
            $time_type = $request->get('time_type', '2');
            $return_type = $request->get('return_type', '01');
            $pay_status = $request->get('pay_status', '');
            $device_id = $request->get('device_id', '');

            $day = date('Ymd', strtotime($time_start));
            $table = 'orders_' . $day;

            $check_data = [
                'time_start' => '开始时间',
                'time_end' => '结束时间',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $where = [];
            $whereIn = [];
            $store_ids = [];
            //条件查询
            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
                $store_ids = [
                    [
                        'store_id' => $store_id,
                    ]
                ];
            } else {
                $MerchantStore = MerchantStore::where('merchant_id', $merchant->merchant_id)
                    ->select('store_id')
                    ->get();
                if (!$MerchantStore->isEmpty()) {
                    $store_ids = $MerchantStore->toArray();
                }
            }

            //收银员
            if ($merchant->merchant_type == 2) {
                $where[] = ['merchant_id', '=', $merchant->merchant_id];
            }

            if ($device_id) {
                $where[] = ['device_id', '=', $device_id];
            }

            //是否传收银员ID
            if ($request->get('merchant_id', '')) {
                $where[] = ['merchant_id', '=', $request->get('merchant_id', '')];
            }

            if ($time_type == "1") {
                $y = date("Y", time());
                $time_start = $y . $time_start;
//                $time_end = $y . $time_end;
                $new_time_end = substr($time_end, 0, 2); //月份
                $time_end = date('Y-m-t', strtotime("$y-$new_time_end"));

                $day = date('Ymd', strtotime($time_start));
                $table = 'orders_' . $day;
            }

            if ($time_start) {
                if ($time_type == "1") {
                    $time_start = date('Y-m-d 00:00:00', strtotime($time_start));
                } else {
                    $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                }

                $where[] = ['created_at', '>=', $time_start];
            }

            if ($time_end) {
                if ($time_type == "1") {
                    $time_end = date('Y-m-d 23:59:59', strtotime($time_end));
                } else {
                    $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                }

                $where[] = ['created_at', '<=', $time_end];
            }

            //区间
            $e_order = '0.00';

            //限制时间
            $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
            $day = 31;
            $now_time = date('Y-m-d H:i:s', time());
            $end = date('Y-m-d 22:00:00', time());
            if ($now_time > $end) {
                $day = 31;
            }

            if ($date > $day) {
                $time_start_s = date('Y-m-d 00:00:00', time());
                $time_start_e = date('Y-m-d 23:59:59', time());

                $time_start = $time_start_s;
                $time_end = $time_start_e;
            }

            //跨天操作
            $time_start_db = date('Ymd', strtotime($time_start));
            $time_end_db = date('Ymd', strtotime($time_end));
            $is_ct_time = 0;
            if ($time_start_db != $time_end_db) {
                $is_ct_time = 1;
            }

            if (env('DB_D1_HOST')) {
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj = DB::connection("mysql_d1")->table('order_items');
                    } else {
                        $obj = DB::connection("mysql_d1")->table('orders');
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj = DB::connection("mysql_d1")->table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj = DB::connection("mysql_d1")->table('order_items');
                        } else {
                            $obj = DB::connection("mysql_d1")->table('orders');
                        }
                    }
                }
            } else {
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj = DB::table('order_items');
                    } else {
                        $obj = DB::table('orders');
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj = DB::table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj = DB::table('order_items');
                        } else {
                            $obj = DB::table('orders');
                        }
                    }
                }
            }

            if (env('DB_D1_HOST')) {
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj1 = DB::connection("mysql_d1")->table('order_items');
                    } else {
                        $obj1 = DB::connection("mysql_d1")->table('orders');
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj1 = DB::connection("mysql_d1")->table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj1 = DB::connection("mysql_d1")->table('order_items');
                        } else {
                            $obj1 = DB::connection("mysql_d1")->table('orders');
                        }
                    }
                }
            } else {
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj1 = DB::table('order_items');
                    } else {
                        $obj1 = DB::table('orders');
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj1 = DB::table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj1 = DB::table('order_items');
                        } else {
                            $obj1 = DB::table('orders');
                        }
                    }
                }
            }

            if (env('DB_D1_HOST')) {
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj2 = DB::connection("mysql_d1")->table('order_items');
                    } else {
                        $obj2 = DB::connection("mysql_d1")->table('orders');
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj2 = DB::connection("mysql_d1")->table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj2 = DB::connection("mysql_d1")->table('order_items');
                        } else {
                            $obj2 = DB::connection("mysql_d1")->table('orders');
                        }
                    }
                }
            } else {
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj2 = DB::table('order_items');
                    } else {
                        $obj2 = DB::table('orders');
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj2 = DB::table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj2 = DB::table('order_items');
                        } else {
                            $obj2 = DB::table('orders');
                        }
                    }
                }
            }

            if (env('DB_D1_HOST')) {
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj3 = DB::connection("mysql_d1")->table('order_items');
                    } else {
                        $obj3 = DB::connection("mysql_d1")->table('orders');
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj3 = DB::connection("mysql_d1")->table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj3 = DB::connection("mysql_d1")->table('order_items');
                        } else {
                            $obj3 = DB::connection("mysql_d1")->table('orders');
                        }
                    }
                }
            } else {
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj3 = DB::table('order_items');
                    } else {
                        $obj3 = DB::table('orders');
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj3 = DB::table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj3 = DB::table('order_items');
                        } else {
                            $obj3 = DB::table('orders');
                        }
                    }
                }
            }

            if (env('DB_D1_HOST')) {
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj4 = DB::connection("mysql_d1")->table('order_items');
                    } else {
                        $obj4 = DB::connection("mysql_d1")->table('orders');
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj4 = DB::connection("mysql_d1")->table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj4 = DB::connection("mysql_d1")->table('order_items');
                        } else {
                            $obj4 = DB::connection("mysql_d1")->table('orders');
                        }
                    }
                }
            } else {
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj4 = DB::table('order_items');
                    } else {
                        $obj4 = DB::table('orders');
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj4 = DB::table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj4 = DB::table('order_items');
                        } else {
                            $obj4 = DB::table('orders');
                        }
                    }
                }
            }

            if (env('DB_D1_HOST')) {
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj5 = DB::connection("mysql_d1")->table('order_items');
                    } else {
                        $obj5 = DB::connection("mysql_d1")->table('orders');
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj5 = DB::connection("mysql_d1")->table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj5 = DB::connection("mysql_d1")->table('order_items');
                        } else {
                            $obj5 = DB::connection("mysql_d1")->table('orders');
                        }
                    }
                }
            } else {
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj5 = DB::table('order_items');
                    } else {
                        $obj5 = DB::table('orders');
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj5 = DB::table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj5 = DB::table('order_items');
                        } else {
                            $obj5 = DB::table('orders');
                        }
                    }
                }
            }

            if ($merchant_id) {
                $order_data = $obj->where($where)
                    ->whereIn('pay_status', [1, 6]) //成功+退款
                    ->where('merchant_id', $merchant_id)
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $order_mdiscount_data = Order::where($where)
                    ->where('pay_status', 1) //成功
                    ->where('merchant_id', $merchant_id)
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $refund_obj = Order::where('merchant_id', $merchant_id)
                    ->where($where)
                    ->where('pay_status', 6) //退款
                    ->select('refund_amount');

                //会员卡
                $member_data = Order::where('merchant_id', $merchant_id)
                    ->where($where)
                    ->where('pay_status', 1)
                    ->where('ways_type', '18888')
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount', 'member_money');

                $member_refund_obj = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 6)
                    ->where('ways_type', '18888')
                    ->select('refund_amount');

                //支付宝
                $alipay_order_data = $obj1->where($where)
                    ->whereIn('pay_status', [1, 6]) //成功+退款
                    ->where('ways_source', 'alipay')
                    ->where('merchant_id', $merchant_id)
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                //支付宝 优惠金额
                $alipay_order_mdiscount_data = Order::where($where)
                    ->where('pay_status', 1) //成功
                    ->where('ways_source', 'alipay')
                    ->where('merchant_id', $merchant_id)
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $alipay_refund_obj = Order::where('merchant_id', $merchant_id)
                    ->where('ways_source', 'alipay')
                    ->where($where)
                    ->where('pay_status', 6)
                    ->select('refund_amount');

                //支付宝刷脸
                $alipay_face_order_data = Order::where($where)
                    ->whereIn('pay_status', [1, 6]) //成功+退款
                    ->where('ways_source', 'alipay')
                    ->where('merchant_id', $merchant_id)
                    ->where('pay_method', 'alipay_face') //支付宝刷脸
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $alipay_face_order_mdiscount_data = Order::where($where)
                    ->where('pay_status', 1) //成功
                    ->where('ways_source', 'alipay')
                    ->where('merchant_id', $merchant_id)
                    ->where('pay_method', 'alipay_face') //支付宝刷脸
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $alipay_face_refund_obj = Order::where('merchant_id', $merchant_id)
                    ->where('ways_source', 'alipay')
                    ->where($where)
                    ->where('pay_status', 6)
                    ->where('pay_method', 'alipay_face') //支付宝刷脸
                    ->select('refund_amount');

                //微信
                $weixin_order_data = $obj2->where($where)
                    ->whereIn('pay_status', [1, 6]) //成功+退款
                    ->where('ways_source', 'weixin')
                    ->where('merchant_id', $merchant_id)
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                //微信 优惠券
                $weixin_order_mdiscount_data = Order::where($where)
                    ->where('pay_status', 1) //成功
                    ->where('ways_source', 'weixin')
                    ->where('merchant_id', $merchant_id)
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $weixin_refund_obj = Order::where('merchant_id', $merchant_id)
                    ->where('ways_source', 'weixin')
                    ->where($where)
                    ->where('pay_status', 6)
                    ->select('refund_amount');

                //微信刷脸
                $weixin_face_order_data = Order::where('merchant_id', $merchant_id)
                    ->where('ways_source', 'weixin')
                    ->where('pay_method', 'weixin_face') //微信刷脸
                    ->where($where)
                    ->whereIn('pay_status', [1, 6]) //成功+退款
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $weixin_face_order_mdiscount_data = Order::where('merchant_id', $merchant_id)
                    ->where('ways_source', 'weixin')
                    ->where('pay_method', 'weixin_face') //微信刷脸
                    ->where($where)
                    ->where('pay_status', 1) //成功
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $weixin_face_refund_obj = Order::where('merchant_id', $merchant_id)
                    ->where('ways_source', 'weixin')
                    ->where($where)
                    ->where('pay_status', 6)
                    ->where('pay_method', 'weixin_face') //微信刷脸
                    ->select('refund_amount');

                //京东
                $jd_order_data = $obj3->where($where)
                    ->whereIn('pay_status', [1, 6]) //成功+退款
//                    ->where('ways_source', 'jd')
                    ->whereIn('ways_type', [15001, 15002, 15003, 15004, 15005]) //哆啦宝
                    ->where('merchant_id', $merchant_id)
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $jd_order_mdiscount_data = Order::where($where)
                    ->where('pay_status', 1) //成功
//                    ->where('ways_source', 'jd')
                    ->whereIn('ways_type', [15001, 15002, 15003, 15004, 15005]) //哆啦宝
                    ->where('merchant_id', $merchant_id)
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $jd_refund_obj = Order::where('merchant_id', $merchant_id)
//                    ->where('ways_source', 'jd')
                    ->whereIn('ways_type', [15001, 15002, 15003, 15004, 15005]) //哆啦宝
                    ->where($where)
                    ->where('pay_status', 6)
                    ->select('refund_amount');

                //银联刷卡
                $un_order_data = $obj4->where($where)
                    ->whereIn('pay_status', [1, 6]) //成功+退款
                    ->whereIn('ways_type', [6005, 8005, 13005, 15005]) //新大陆+京东刷卡
                    ->where('ways_source', 'unionpay')
                    ->where('merchant_id', $merchant_id)
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $un_order_mdiscount_data = Order::where($where)
                    ->where('pay_status', 1) //成功
                    ->whereIn('ways_type', [6005, 8005, 13005, 15005]) //新大陆+京东刷卡
                    ->where('ways_source', 'unionpay')
                    ->where('merchant_id', $merchant_id)
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $un_refund_obj = Order::where('merchant_id', $merchant_id)
                    ->whereIn('ways_type', [6005, 8005, 13005, 15005]) //新大陆+京东刷卡
                    ->where('ways_source', 'unionpay')
                    ->where($where)
                    ->where('pay_status', 6)
                    ->select('refund_amount');

                //银联扫码
                $unqr_order_data = $obj5->where($where)
                    ->whereIn('pay_status', [1, 6]) //成功+退款
                    ->whereNotIn('ways_type', [6004, 8004, 13004, 15004]) //新大陆+云闪付
                    ->where('ways_source', 'unionpay')
                    ->where('merchant_id', $merchant_id)
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $unqr_order_mdiscount_data = Order::where($where)
                    ->where('pay_status', 1) //成功
                    ->whereNotIn('ways_type', [6004, 8004, 13004, 15004]) //新大陆+云闪付
                    ->where('ways_source', 'unionpay')
                    ->where('merchant_id', $merchant_id)
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $unqr_refund_obj = Order::where('merchant_id', $merchant_id)
                    ->whereNotIn('ways_type', [6004, 8004, 13004, 15004]) //新大陆+京东刷卡
                    ->where('ways_source', 'unionpay')
                    ->where($where)
                    ->where('pay_status', 6)
                    ->select('refund_amount');
            } else {
                $order_data = $obj->whereIn('store_id', $store_ids)
                    ->where($where)
                    ->whereIn('pay_status', [1, 6]) //成功+退款
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $order_mdiscount_data = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 1) //成功
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $refund_obj = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 6) //退款
                    ->select('refund_amount');

                //会员卡支付
                $member_data = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 1)
                    ->where('ways_type', '18888')
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount', 'member_money');

                $member_refund_obj = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 6)
                    ->where('ways_type', '18888')
                    ->select('refund_amount');

                //支付宝
                $alipay_order_data = $obj1->whereIn('store_id', $store_ids)
                    ->where($where)
                    ->whereIn('pay_status', [1, 6]) //成功+退款
                    ->where('ways_source', 'alipay')
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                //支付宝优惠券
                $alipay_order_mdiscount_data = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 1) //成功
                    ->where('ways_source', 'alipay')
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $alipay_refund_obj = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 6) //退款
                    ->where('ways_source', 'alipay')
                    ->select('refund_amount');

                //支付宝刷脸
                $alipay_face_order_data = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->whereIn('pay_status', [1, 6]) //成功+退款
                    ->where('ways_source', 'alipay')
                    ->where('pay_method', 'alipay_face') //支付宝刷脸
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $alipay_face_order_mdiscount_data = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 1) //成功
                    ->where('ways_source', 'alipay')
                    ->where('pay_method', 'alipay_face') //支付宝刷脸
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $alipay_face_refund_obj = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 6) //退款
                    ->where('ways_source', 'alipay')
                    ->where('pay_method', 'alipay_face') //支付宝刷脸
                    ->select('refund_amount');

                //微信
                $weixin_order_data = $obj2->whereIn('store_id', $store_ids)
                    ->where($where)
                    ->whereIn('pay_status', [1, 6]) //成功+退款
                    ->where('ways_source', 'weixin')
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $weixin_order_mdiscount_data = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 1) //成功
                    ->where('ways_source', 'weixin')
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $weixin_refund_obj = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 6)
                    ->where('ways_source', 'weixin')
                    ->select('refund_amount');

                //微信刷脸
                $weixin_face_order_data = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->whereIn('pay_status', [1, 6]) //成功+退款
                    ->where('ways_source', 'weixin')
                    ->where('pay_method', 'weixin_face') //微信刷脸
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $weixin_face_order_mdiscount_data = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 1) //成功
                    ->where('ways_source', 'weixin')
                    ->where('pay_method', 'weixin_face') //微信刷脸
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $weixin_face_refund_obj = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 6) //退款
                    ->where('ways_source', 'weixin')
                    ->where('pay_method', 'weixin_face') //微信刷脸
                    ->select('refund_amount');

                //京东
                $jd_order_data = $obj3->whereIn('store_id', $store_ids)
                    ->where($where)
                    ->whereIn('pay_status', [1, 6]) //成功+退款
//                    ->where('ways_source', 'jd')
                    ->whereIn('ways_type', [15001, 15002, 15003, 15004, 15005]) //哆啦宝
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $jd_order_mdiscount_data = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 1) //成功
//                    ->where('ways_source', 'jd')
                    ->whereIn('ways_type', [15001, 15002, 15003, 15004, 15005]) //哆啦宝
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $jd_refund_obj = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 6)
//                    ->where('ways_source', 'jd')
                    ->whereIn('ways_type', [15001, 15002, 15003, 15004, 15005]) //哆啦宝
                    ->select('refund_amount');

                //银联刷卡
                $un_order_data = $obj4->whereIn('store_id', $store_ids)
                    ->where($where)
                    ->whereIn('pay_status', [1, 6]) //成功+退款
                    ->whereIn('ways_type', [6005, 8005, 13005]) //新大陆+京东刷卡
                    ->where('ways_source', 'unionpay')
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $un_order_mdiscount_data = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 1) //成功
                    ->whereIn('ways_type', [6005, 8005, 13005]) //新大陆+京东刷卡
                    ->where('ways_source', 'unionpay')
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $un_refund_obj = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 6)
                    ->where('ways_source', 'unionpay')
                    ->whereIn('ways_type', [6005, 8005, 13005]) //新大陆+京东刷卡
                    ->select('refund_amount');

                //银联二维码
                $unqr_order_data = $obj5->whereIn('store_id', $store_ids)
                    ->where($where)
                    ->whereIn('pay_status', [1, 6]) //成功+退款
                    ->whereNotIn('ways_type', [6004, 8004, 13004, 15004]) //新大陆+云闪付
                    ->where('ways_source', 'unionpay')
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $unqr_order_mdiscount_data = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 1) //成功
                    ->whereNotIn('ways_type', [6004, 8004, 13004, 15004]) //新大陆+云闪付
                    ->where('ways_source', 'unionpay')
                    ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

                $unqr_refund_obj = Order::whereIn('store_id', $store_ids)
                    ->where($where)
                    ->where('pay_status', 6)
                    ->where('ways_source', 'unionpay')
                    ->whereNotIn('ways_type', [6004, 8004, 13004, 15004]) //去除新大陆+京东刷卡
                    ->select('refund_amount');
            }

            //总的
            $total_amount = $order_data->sum('total_amount'); //交易金额
            $refund_amount = $refund_obj->sum('refund_amount'); //退款金额
            $fee_amount = $order_data->sum('fee_amount'); //结算服务费/手续费
            $mdiscount_amount = $order_mdiscount_data->sum('mdiscount_amount'); //商家优惠金额
            $total_member_amount = $member_data->sum('total_amount'); //会员卡支付金额
            $get_amount = $total_amount - $refund_amount - $mdiscount_amount - $total_member_amount; //商家实收，交易金额-退款金额-商家优惠金额-会员支付金额
            $receipt_amount = $get_amount - $fee_amount; //实际净额，实收-手续费
            $e_order = '' . $e_order . '';
            $total_count = '' . count($order_data->get()) . '';
            $refund_count = count($refund_obj->get());

            //支付宝
            $alipay_total_amount = $alipay_order_data->sum('total_amount'); //交易金额
            $alipay_refund_amount = $alipay_refund_obj->sum('refund_amount'); //退款金额
            $alipay_fee_amount = $alipay_order_data->sum('fee_amount'); //结算服务费/手续费
            $alipay_mdiscount_amount = $alipay_order_mdiscount_data->sum('mdiscount_amount'); //商家优惠金额
            $alipay_get_amount = $alipay_total_amount - $alipay_refund_amount - $alipay_mdiscount_amount; //商家实收，交易金额-退款金额
            $alipay_receipt_amount = $alipay_get_amount - $alipay_fee_amount; //实际净额，实收-手续费
            $alipay_total_count = '' . count($alipay_order_data->get()) . '';
            $alipay_refund_count = count($alipay_refund_obj->get());

            //微信
            $weixin_total_amount = $weixin_order_data->sum('total_amount'); //交易金额
            $weixin_refund_amount = $weixin_refund_obj->sum('refund_amount'); //退款金额
            $weixin_fee_amount = $weixin_order_data->sum('fee_amount'); //结算服务费/手续费
            $weixin_mdiscount_amount = $weixin_order_mdiscount_data->sum('mdiscount_amount'); //商家优惠金额
            $weixin_get_amount = $weixin_total_amount - $weixin_refund_amount - $weixin_mdiscount_amount; //商家实收，交易金额-退款金额
            $weixin_receipt_amount = $weixin_get_amount - $weixin_fee_amount; //实际净额，实收-手续费
            $weixin_total_count = '' . count($weixin_order_data->get()) . '';
            $weixin_refund_count = count($weixin_refund_obj->get());

            //京东
            $jd_total_amount = $jd_order_data->sum('total_amount'); //交易金额
            $jd_refund_amount = $jd_refund_obj->sum('refund_amount'); //退款金额
            $jd_fee_amount = $jd_order_data->sum('fee_amount'); //结算服务费/手续费
            $jd_mdiscount_amount = $jd_order_mdiscount_data->sum('mdiscount_amount'); //商家优惠金额
            $jd_get_amount = $jd_total_amount - $jd_refund_amount - $jd_mdiscount_amount; //商家实收，交易金额-退款金额
            $jd_receipt_amount = $jd_get_amount - $jd_fee_amount; //实际净额，实收-手续费
            $jd_total_count = '' . count($jd_order_data->get()) . '';
            $jd_refund_count = count($jd_refund_obj->get());

            //银联刷卡
            $un_total_amount = $un_order_data->sum('total_amount'); //交易金额
            $un_refund_amount = $un_refund_obj->sum('refund_amount'); //退款金额
            $un_fee_amount = $un_order_data->sum('fee_amount'); //结算服务费/手续费
            $un_mdiscount_amount = $un_order_mdiscount_data->sum('mdiscount_amount'); //商家优惠金额
            $un_get_amount = $un_total_amount - $un_refund_amount - $un_mdiscount_amount; //商家实收，交易金额-退款金额
            $un_receipt_amount = $un_get_amount - $un_fee_amount; //实际净额，实收-手续费
            $un_total_count = '' . count($un_order_data->get()) . '';
            $un_refund_count = count($un_refund_obj->get());

            //银联扫码
            $unqr_total_amount = $unqr_order_data->sum('total_amount'); //交易金额
            $unqr_refund_amount = $unqr_refund_obj->sum('refund_amount'); //退款金额
            $unqr_fee_amount = $unqr_order_data->sum('fee_amount'); //结算服务费/手续费
            $unqr_mdiscount_amount = $unqr_order_mdiscount_data->sum('mdiscount_amount'); //商家优惠金额
            $unqr_get_amount = $unqr_total_amount - $unqr_refund_amount - $unqr_mdiscount_amount; //商家实收，交易金额-退款金额
            $unqr_receipt_amount = $unqr_get_amount - $unqr_fee_amount; //实际净额，实收-手续费
            $unqr_total_count = '' . count($unqr_order_data->get()) . '';
            $unqr_refund_count = count($unqr_refund_obj->get());

            //会员卡支付
            $member_total_amount = $member_data->sum('total_amount'); //交易金额
            $member_deduction_sum = $member_data->sum('member_money'); //会员支付金额
            $member_refund_amount = $member_refund_obj->sum('total_amount'); //退款金额
            $member_fee_amount = $member_data->sum('fee_amount'); //结算服务费/手续费
            $member_mdiscount_amount = $member_data->sum('mdiscount_amount'); //商家优惠金额
            $member_get_amount = $member_total_amount - $member_refund_amount - $member_mdiscount_amount; //商家实收，交易金额-退款金额
            $member_receipt_amount = $member_get_amount - $member_fee_amount; //实际净额，实收-手续费
            $member_total_count = '' . count($member_data->get()) . '';
            $member_refund_count = count($member_refund_obj->get());
            $member_total_deduction = $member_total_amount - $member_deduction_sum; //

            //支付宝刷脸
            $alipay_face_total_amount = $alipay_face_order_data->sum('total_amount'); //交易金额
            $alipay_face_refund_amount = $alipay_face_refund_obj->sum('refund_amount'); //退款金额
            $alipay_face_fee_amount = $alipay_face_order_data->sum('fee_amount'); //结算服务费/手续费
            $alipay_face_mdiscount_amount = $alipay_face_order_mdiscount_data->sum('mdiscount_amount'); //商家优惠金额
            $alipay_face_get_amount = $alipay_face_total_amount - $alipay_face_refund_amount - $alipay_face_mdiscount_amount; //商家实收，交易金额-退款金额
            $alipay_face_receipt_amount = $alipay_face_get_amount - $alipay_fee_amount; //实际净额，实收-手续费
            $alipay_face_total_count = '' . count($alipay_face_order_data->get()) . ''; //交易统计
            $alipay_face_refund_count = count($alipay_face_refund_obj->get()); //退款统计

            //微信刷脸
            $weixin_face_total_amount = $weixin_face_order_data->sum('total_amount'); //交易金额
            $weixin_face_refund_amount = $weixin_face_refund_obj->sum('refund_amount'); //退款金额
            $weixin_face_fee_amount = $weixin_face_order_data->sum('fee_amount'); //结算服务费/手续费
            $weixin_face_mdiscount_amount = $weixin_face_order_mdiscount_data->sum('mdiscount_amount'); //商家优惠金额
            $weixin_face_get_amount = $weixin_face_total_amount - $weixin_face_refund_amount - $weixin_face_mdiscount_amount; //商家实收，交易金额-退款金额
            $weixin_face_receipt_amount = $weixin_face_get_amount - $weixin_face_fee_amount; //实际净额，实收-手续费
            $weixin_face_total_count = '' . count($weixin_face_order_data->get()) . '';
            $weixin_face_refund_count = count($weixin_face_refund_obj->get());

            $data = [
                'total_amount' => number_format($total_amount, 2, '.', ''),//交易金额
                'total_count' => '' . $total_count . '',//交易笔数
                'refund_count' => '' . $refund_count . '',//退款笔数
                'get_amount' => number_format($get_amount, 2, '.', ''),//商家实收，交易金额-退款金额
                'refund_amount' => number_format($refund_amount, 2, '.', ''),//退款金额
                'receipt_amount' => number_format($receipt_amount, 2, '.', ''),//实际净额，实收-手续费
                'fee_amount' => number_format($fee_amount, 2, '.', ''),//结算服务费/手续费
                'mdiscount_amount' => number_format($mdiscount_amount, 2, '.', ''), //商家优惠金额
                'member_amount' => number_format($total_member_amount, 2, '.', ''), //会员卡支付金额

                'alipay_total_amount' => number_format($alipay_total_amount, 2, '.', ''),//交易金额
                'alipay_total_count' => '' . $alipay_total_count . '',//交易笔数
                'alipay_refund_count' => '' . $alipay_refund_count . '',//退款金额
                'alipay_get_amount' => number_format($alipay_get_amount, 2, '.', ''),//商家实收，交易金额-退款金额
                'alipay_refund_amount' => number_format($alipay_refund_amount, 2, '.', ''),//退款金额
                'alipay_receipt_amount' => number_format($alipay_receipt_amount, 2, '.', ''),//实际净额，实收-手续费
                'alipay_fee_amount' => number_format($alipay_fee_amount, 2, '.', ''),//结算服务费/手续费
                'alipay_mdiscount_amount' => number_format($alipay_mdiscount_amount, 2, '.', ''), //商家优惠

                'weixin_total_amount' => number_format($weixin_total_amount, 2, '.', ''),//交易金额
                'weixin_total_count' => '' . $weixin_total_count . '',//交易笔数
                'weixin_refund_count' => '' . $weixin_refund_count . '',//退款金额
                'weixin_get_amount' => number_format($weixin_get_amount, 2, '.', ''),//商家实收，交易金额-退款金额
                'weixin_refund_amount' => number_format($weixin_refund_amount, 2, '.', ''),//退款金额
                'weixin_receipt_amount' => number_format($weixin_receipt_amount, 2, '.', ''),//实际净额，实收-手续费
                'weixin_fee_amount' => number_format($weixin_fee_amount, 2, '.', ''),//结算服务费/手续费
                'weixin_mdiscount_amount' => number_format($weixin_mdiscount_amount, 2, '.', ''), //商家优惠

                'jd_total_amount' => number_format($jd_total_amount, 2, '.', ''),//交易金额
                'jd_total_count' => '' . $jd_total_count . '',//交易笔数
                'jd_refund_count' => '' . $jd_refund_count . '',//退款金额
                'jd_get_amount' => number_format($jd_get_amount, 2, '.', ''),//商家实收，交易金额-退款金额
                'jd_refund_amount' => number_format($jd_refund_amount, 2, '.', ''),//退款金额
                'jd_receipt_amount' => number_format($jd_receipt_amount, 2, '.', ''),//实际净额，实收-手续费
                'jd_fee_amount' => number_format($jd_fee_amount, 2, '.', ''),//结算服务费/手续费
                'jd_mdiscount_amount' => number_format($jd_mdiscount_amount, 2, '.', ''), //商家优惠

                'un_total_amount' => number_format($un_total_amount, 2, '.', ''),//交易金额
                'un_total_count' => '' . $un_total_count . '',//交易笔数
                'un_refund_count' => '' . $un_refund_count . '',//退款金额
                'un_get_amount' => number_format($un_get_amount, 2, '.', ''),//商家实收，交易金额-退款金额
                'un_refund_amount' => number_format($un_refund_amount, 2, '.', ''),//退款金额
                'un_receipt_amount' => number_format($un_receipt_amount, 2, '.', ''),//实际净额，实收-手续费
                'un_fee_amount' => number_format($un_fee_amount, 2, '.', ''),//结算服务费/手续费
                'un_mdiscount_amount' => number_format($un_mdiscount_amount, 2, '.', ''), //商家优惠

                'unqr_total_amount' => number_format($unqr_total_amount, 2, '.', ''),//交易金额
                'unqr_total_count' => '' . $unqr_total_count . '',//交易笔数
                'unqr_refund_count' => '' . $unqr_refund_count . '',//退款金额
                'unqr_get_amount' => number_format($unqr_get_amount, 2, '.', ''),//商家实收，交易金额-退款金额
                'unqr_refund_amount' => number_format($unqr_refund_amount, 2, '.', ''),//退款金额
                'unqr_receipt_amount' => number_format($unqr_receipt_amount, 2, '.', ''),//实际净额，实收-手续费
                'unqr_fee_amount' => number_format($unqr_fee_amount, 2, '.', ''),//结算服务费/手续费
                'unqr_mdiscount_amount' => number_format($unqr_mdiscount_amount, 2, '.', ''), //商家优惠

                'hbfq_total_amount' => number_format(0, 2, '.', ''),//交易金额
                'hbfq_total_count' => '' . 0 . '',//交易笔数
                'hbfq_refund_count' => '' . 0 . '',//退款金额
                'hbfq_get_amount' => number_format(0, 2, '.', ''),//商家实收，交易金额-退款金额
                'hbfq_refund_amount' => number_format(0, 2, '.', ''),//退款金额
                'hbfq_receipt_amount' => number_format(0, 2, '.', ''),//实际净额，实收-手续费
                'hbfq_fee_amount' => number_format(0, 2, '.', ''),//结算服务费/手续费
                'hbfq_mdiscount_amount' => number_format(0, 2, '.', ''), //商家优惠

                'member_total_amount' => number_format($member_total_amount, 2, '.', ''),//交易金额
                'member_total_count' => '' . $member_total_count . '',//交易笔数
                'member_refund_count' => '' . $member_refund_count . '',//退款金额
                'member_get_amount' => number_format($member_get_amount, 2, '.', ''),//商家实收，交易金额-退款金额
                'member_refund_amount' => number_format($member_refund_amount, 2, '.', ''),//退款金额
                'member_receipt_amount' => number_format($member_receipt_amount, 2, '.', ''),//实际净额，实收-手续费
                'member_fee_amount' => number_format($member_fee_amount, 2, '.', ''),//结算服务费/手续费
                'member_mdiscount_amount' => number_format($member_mdiscount_amount, 2, '.', ''),
                'member_total_deduction' => number_format($member_total_deduction, 2, '.', ''), //会员积分抵扣总值

                'alipay_face_total_amount' => number_format($alipay_face_total_amount, 2, '.', ''),//交易金额
                'alipay_face_total_count' => '' . $alipay_face_total_count . '',//交易笔数
                'alipay_face_refund_count' => '' . $alipay_face_refund_count . '',//退款金额
                'alipay_face_get_amount' => number_format($alipay_face_get_amount, 2, '.', ''),//商家实收，交易金额-退款金额
                'alipay_face_refund_amount' => number_format($alipay_face_refund_amount, 2, '.', ''),//退款金额
                'alipay_face_receipt_amount' => number_format($alipay_face_receipt_amount, 2, '.', ''),//实际净额，实收-手续费
                'alipay_face_fee_amount' => number_format($alipay_face_fee_amount, 2, '.', ''),//结算服务费/手续费
                'alipay_face_mdiscount_amount' => number_format($alipay_face_mdiscount_amount, 2, '.', ''), //商家优惠

                'weixin_face_total_amount' => number_format($weixin_face_total_amount, 2, '.', ''),//交易金额
                'weixin_face_total_count' => '' . $weixin_face_total_count . '',//交易笔数
                'weixin_face_refund_count' => '' . $weixin_face_refund_count . '',//退款金额
                'weixin_face_get_amount' => number_format($weixin_face_get_amount, 2, '.', ''),//商家实收，交易金额-退款金额
                'weixin_face_refund_amount' => number_format($weixin_face_refund_amount, 2, '.', ''),//退款金额
                'weixin_face_receipt_amount' => number_format($weixin_face_receipt_amount, 2, '.', ''),//实际净额，实收-手续费
                'weixin_face_fee_amount' => number_format($weixin_face_fee_amount, 2, '.', ''),//结算服务费/手续费
                'weixin_face_mdiscount_amount' => number_format($weixin_face_mdiscount_amount, 2, '.', ''), //商家优惠
            ];

            //打印ID
            $print_data = $data;
            $print_data['store_id'] = $store_id;
            $print_data['merchant_id'] = $merchant_id;
            $print_data['time_start'] = $time_start;
            $print_data['time_end'] = $time_end;

            $data['print_id'] = $store_id . $merchant->merchant_id;
            Cache::put($data['print_id'], json_encode($print_data), 1);

            //附加流水详情
            if ($return_type == "02") {
                if ($ways_source) {
                    if (in_array($ways_source, ['alipay_face', 'weixin_face'])) {
                        $where[] = ['pay_method', '=', $ways_source];
                    } else {
                        $where[] = ['ways_source', '=', $ways_source];
                    }
                }

                if (env('DB_D1_HOST')) {
                    //有没有跨天
                    if ($is_ct_time) {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj6 = DB::connection("mysql_d1")->table('order_items');
                        } else {
                            $obj6 = DB::connection("mysql_d1")->table('orders');
                        }
                    } else {
                        if (Schema::hasTable($table)) {
                            $obj6 = DB::connection("mysql_d1")->table($table);
                        } else {
                            if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                                $obj6 = DB::connection("mysql_d1")->table('order_items');
                            } else {
                                $obj6 = DB::connection("mysql_d1")->table('orders');
                            }
                        }
                    }
                } else {
                    if ($is_ct_time) {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj6 = DB::table('order_items');
                        } else {
                            $obj6 = DB::table('orders');
                        }
                    } else {
                        if (Schema::hasTable($table)) {
                            $obj6 = DB::table($table);
                        } else {
                            if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                                $obj6 = DB::table('order_items');
                            } else {
                                $obj6 = DB::table('orders');
                            }
                        }
                    }
                }

                if ($pay_status) {
                    $where[] = ['pay_status', '=', $pay_status];
                }

                $obj6 = $obj6->where($where)
                    ->whereIn('store_id', $store_ids)
                    ->orderBy('created_at', 'desc');
                $this->t = $obj6->count();
                $data['order_list'] = $this->page($obj6)->get();
            }

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    //经营数据统计-即将舍弃
    public function data_count(Request $request)
    {
        try {
            // dd($merchant->store['store_id']);
            $merchant = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $merchant_id = $request->get('merchant_id', '');
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');
            $time_type = $request->get('time_type', '1'); //时间格式(1-月日;2-年月日)
            $store_ids = [];
            $check_data = [
                'time_start' => '开始时间',
                'time_end' => '结束时间',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            //没有传入收银员id 角色是收银员 只返回自己
            if ($merchant->merchant_type == 2) {
                $merchant_id = $merchant->merchant_id;
            }

            //如果门店为空 传登录者绑定的门店ID
            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
                $store_ids = [
                    [
                        'store_id' => $store_id,
                    ]
                ];

            } else {
                $MerchantStore = MerchantStore::where('merchant_id', $merchant->merchant_id)
                    ->select('store_id')
                    ->get();
                if (!$MerchantStore->isEmpty()) {
                    $store_ids = $MerchantStore->toArray();
                }
            }

            if ($time_type == '1') {
                $y = date("Y", time());
                $new_time_end = substr($time_end, 0, 2); //月份
                $time_end = date('Y-m-t', strtotime("$y-$new_time_end"));

                $time_start = $y . $time_start;
//                $time_end = $y . $time_end;
            } else {
                $time_start = date("Ymd", strtotime($time_start));
                $time_end = date("Ymd", strtotime($time_end));
            }

            $time_start1 = date('Y-m-d H:i:s', strtotime($time_start));
            $time_end1 = date('Y-m-d 23:59:59', strtotime($time_end));


            if ($merchant->merchant_type == 2) {
                $data_obj = MerchantStoreDayOrder::whereBetween('day', [$time_start, $time_end])
                    ->where('merchant_id', $merchant_id)
                    ->whereIn('store_id', $store_ids)
                    ->select('total_amount', 'order_sum');

                $hb_obj = MerchantStoreDayOrder::whereBetween('day', [$time_start, $time_end])
                    ->where('merchant_id', $merchant_id)
                    ->whereIn('store_id', $store_ids)
                    ->select('total_amount', 'order_sum')
                    ->whereIn('type', [1006, 1007]);


                $refund_obj = RefundOrder::where('merchant_id', $merchant_id)
                    ->where('updated_at', '<=', $time_end1)
                    ->where('updated_at', '>=', $time_start1)
                    ->whereIn('store_id', $store_ids)
                    ->select('refund_amount');

                $alipay_obj = MerchantStoreDayOrder::whereBetween('day', [$time_start, $time_end])
                    ->where('merchant_id', $merchant_id)
                    ->whereIn('store_id', $store_ids)
                    ->select('total_amount', 'order_sum')
                    ->where('source_type', 'alipay');

                $weixin_obj = MerchantStoreDayOrder::whereBetween('day', [$time_start, $time_end])
                    ->where('merchant_id', $merchant_id)
                    ->whereIn('store_id', $store_ids)
                    ->select('total_amount', 'order_sum')
                    ->where('source_type', 'weixin');

                $jd_obj = MerchantStoreDayOrder::whereBetween('day', [$time_start, $time_end])
                    ->where('merchant_id', $merchant_id)
                    ->whereIn('store_id', $store_ids)
                    ->select('total_amount', 'order_sum')
                    ->where('source_type', 'jdjr');


            } else {


                $data_obj = StoreDayOrder::whereBetween('day', [$time_start, $time_end])
                    ->whereIn('store_id', $store_ids)
                    ->select('total_amount', 'order_sum');


                $hb_obj = StoreDayOrder::whereBetween('day', [$time_start, $time_end])
                    ->whereIn('store_id', $store_ids)
                    ->select('total_amount', 'order_sum')
                    ->whereIn('type', [1006, 1007]);

                $refund_obj = RefundOrder::where('updated_at', '<=', $time_end1)
                    ->where('updated_at', '>=', $time_start1)
                    ->whereIn('store_id', $store_ids)
                    ->select('refund_amount');


                $alipay_obj = StoreDayOrder::whereBetween('day', [$time_start, $time_end])
                    ->whereIn('store_id', $store_ids)
                    ->select('total_amount', 'order_sum')
                    ->where('source_type', 'alipay');

                $weixin_obj = StoreDayOrder::whereBetween('day', [$time_start, $time_end])
                    ->whereIn('store_id', $store_ids)
                    ->select('total_amount', 'order_sum')
                    ->where('source_type', 'weixin');

                $jd_obj = StoreDayOrder::whereBetween('day', [$time_start, $time_end])
                    ->whereIn('store_id', $store_ids)
                    ->select('total_amount', 'order_sum')
                    ->where('source_type', 'jdjr');


            }

            //总金额

            $total_amount = $data_obj->sum('total_amount');
            $order_sum = $data_obj->sum('order_sum');

            $hb_fq_amount = $hb_obj->sum('total_amount');
            $hb_fq_count = $hb_obj->sum('order_sum');

            $alipay_amount = $alipay_obj->sum('total_amount');
            $alipay_count = $alipay_obj->sum('order_sum');

            $weixin_amount = $weixin_obj->sum('total_amount');
            $weixin_count = $weixin_obj->sum('order_sum');

            $jd_amount = $jd_obj->sum('total_amount');
            $jd_count = $jd_obj->sum('order_sum');


            $refund_amount = $refund_obj->sum('refund_amount');
            $refund_count = count($refund_obj->get());


            $data = [
                'total_amount' => number_format($total_amount, 2, '.', ''),
                'total_count' => "" . $order_sum . "",

                'refund_amount' => number_format($refund_amount, 2, '.', ''),
                'refund_count' => "" . $refund_count . "",

                'alipay_amount' => number_format($alipay_amount, 2, '.', ''),
                'alipay_count' => "" . $alipay_count . "",

                'weixin_amount' => number_format($weixin_amount, 2, '.', ''),
                'weixin_count' => "" . $weixin_count . "",

                'jd_amount' => number_format($jd_amount, 2, '.', ''),
                'jd_count' => '' . $jd_count . '',

                'hb_fq_amount' => number_format($hb_fq_amount, 2, '.', ''),
                'hb_fq_count' => "" . $hb_fq_count . "",

            ];
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage() . $exception->getLine();
            return $this->format();
        }
    }


    //数据走势
    public function order_data(Request $request)
    {
        try {
            // dd($merchant->store['store_id']);
            $merchant = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $merchant_id = $request->get('merchant_id', '');

            //如果门店为空 传登录者绑定的门店ID
            if ($store_id == "") {
                $MyBankStore = MerchantStore::where('merchant_id', $merchant->merchant_id)
                    ->orderBy('created_at', 'asc')
                    ->first();
                $store_id = $MyBankStore->store_id;
            }
            $store_ids = [$store_id];

            //没有传入收银员id 角色是收银员 只返回自己
            if ($merchant->merchant_type == 2) {
                $merchant_id = $merchant->merchant_id;
            } else {
                $where[] = ['store_id', '=', $store_id];
                $store = Store::where('store_id', $store_id)
                    ->select('id', 'pid')
                    ->first();

                if ($store) {
                    $store_ids = $this->getStore_id($store_id, $store->id);
                }
            }


            //今日
            $day = date('Ymd', time());
            $beginToday = date("Y-m-d 00:00:00", time());
            $endToday = date("Y-m-d H:i:s", time());


            //收银员
            if ($merchant->merchant_type == 2) {
                $day_order_data = Order::whereIn('store_id', $store_ids)
                    ->where('created_at', '>=', $beginToday)
                    ->where('merchant_id', $merchant_id)
                    ->where('created_at', '<=', $endToday)
                    ->whereIn('pay_status', [1, 3, 6])
                    ->select('total_amount');

                $refund_order_data = RefundOrder::whereBetween('created_at', [$beginToday, $endToday])
                    ->where('merchant_id', $merchant_id)
                    ->where('store_id', $store_id)
                    ->select('refund_amount');
            } else {
                $day_order_data = Order::whereIn('store_id', $store_ids)
                    ->where('created_at', '>=', $beginToday)
                    ->where('created_at', '<=', $endToday)
                    ->whereIn('pay_status', [1, 3, 6])
                    ->select('total_amount');

                $refund_order_data = RefundOrder::whereBetween('created_at', [$beginToday, $endToday])
                    ->whereIn('store_id', $store_ids)
                    ->select('refund_amount');
            }

            $day_order = $day_order_data->sum('total_amount');
            $day_order = '' . $day_order . '';
            $day_order_count = '' . count($day_order_data->get()) . '';

            $refund_day_order = $refund_order_data->sum('refund_amount');
            $refund_day_order = '' . $refund_day_order . '';
            $refund_day_order_count = '' . count($refund_order_data->get()) . '';

            //上个月
            //得到系统的年月
            $tmp_date = date("Ym", time());
            //切割出年份
            $tmp_year = substr($tmp_date, 0, 4);
            //切割出月份
            $tmp_mon = substr($tmp_date, 4, 2);
            $tmp_forwardmonth = mktime(0, 0, 0, $tmp_mon - 1, 1, $tmp_year);
            $fm_forward_month = date("Ym", $tmp_forwardmonth);

            $old_begin_time = date("Y-m-d 00:00:00", $tmp_forwardmonth);
            $old_end_time = date("Y-m-d 23:59:59", strtotime(-date('d') . 'day'));

            if ($merchant->merchant_type == 2) {
                $old_month_order_data = Order::whereIn('store_id', $store_ids)
                    ->where('created_at', '>=', $old_begin_time)
                    ->where('merchant_id', $merchant_id)
                    ->where('created_at', '<=', $old_end_time)
                    ->whereIn('pay_status', [1, 3, 6])
                    ->select('total_amount');

                $old_month_order_mdiscount_data = Order::whereIn('store_id', $store_ids)
                    ->where('created_at', '>=', $old_begin_time)
                    ->where('merchant_id', $merchant_id)
                    ->where('created_at', '<=', $old_end_time)
                    ->where('pay_status', 1)
                    ->select('mdiscount_amount');

                $refund_old_month_order_data = RefundOrder::whereBetween('created_at', [$old_begin_time, $old_end_time])
                    ->where('merchant_id', $merchant_id)
                    ->where('store_id', $store_id)
                    ->select('refund_amount');
            } else {
                $old_month_order_data = Order::whereIn('store_id', $store_ids)
                    ->where('created_at', '>=', $old_begin_time)
                    ->where('created_at', '<=', $old_end_time)
                    ->whereIn('pay_status', [1, 3, 6])
                    ->select('total_amount');

                $old_month_order_mdiscount_data = Order::whereIn('store_id', $store_ids)
                    ->where('created_at', '>=', $old_begin_time)
                    ->where('created_at', '<=', $old_end_time)
                    ->where('pay_status', 1)
                    ->select('mdiscount_amount');

                $refund_old_month_order_data = RefundOrder::whereBetween('created_at', [$old_begin_time, $old_end_time])
                    ->whereIn('store_id', $store_ids)
                    ->select('refund_amount');
            }

            $old_month_order = $old_month_order_data->sum('total_amount');
            $old_month_amount = '' . $old_month_order . '';
            $old_month_count = '' . count($old_month_order_data->get()) . '';

            $old_month_mdiscount_order = $old_month_order_data->sum('mdiscount_amount');
            $old_month_mdiscount_amount = '' . $old_month_mdiscount_order . '';
            $old_month_mdiscount_count = '' . count($old_month_order_mdiscount_data->get()) . '';

            $refund_old_month_amount = $refund_old_month_order_data->sum('refund_amount');
            $refund_old_month_amount = '' . $refund_old_month_amount . '';
            $refund_old_month_count = '' . count($refund_old_month_order_data->get()) . '';

            //本月
            $tmonth = date('Ym', time());
            $month_begin_time = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), 1, date("Y")));
            $month_end_time = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m"), date("t"), date("Y")));

            if ($merchant->merchant_type == 2) {
                $month_order_data = Order::whereIn('store_id', $store_ids)
                    ->where('created_at', '>=', $month_begin_time)
                    ->where('merchant_id', $merchant_id)
                    ->where('created_at', '<=', $month_end_time)
                    ->whereIn('pay_status', [1, 3, 6])
                    ->select('total_amount');

                $month_order_mdiscount_data = Order::whereIn('store_id', $store_ids)
                    ->where('created_at', '>=', $month_begin_time)
                    ->where('merchant_id', $merchant_id)
                    ->where('created_at', '<=', $month_end_time)
                    ->where('pay_status', 1)
                    ->select('mdiscount_amount');

                $refund_month_order_data = RefundOrder::whereBetween('created_at', [$month_begin_time, $month_end_time])
                    ->where('merchant_id', $merchant_id)
                    ->where('store_id', $store_id)
                    ->select('refund_amount');
            } else {
                $month_order_data = Order::whereIn('store_id', $store_ids)
                    ->where('created_at', '>=', $month_begin_time)
                    ->where('created_at', '<=', $month_end_time)
                    ->whereIn('pay_status', [1, 3, 6])
                    ->select('total_amount');

                $month_order_mdiscount_data = Order::whereIn('store_id', $store_ids)
                    ->where('created_at', '>=', $month_begin_time)
                    ->where('created_at', '<=', $month_end_time)
                    ->where('pay_status', 1)
                    ->select('mdiscount_amount');

                $refund_month_order_data = RefundOrder::whereBetween('created_at', [$month_begin_time, $month_end_time])
                    ->whereIn('store_id', $store_ids)
                    ->select('refund_amount');
            }

            $month_order = $month_order_data->sum('total_amount');
            $month_amount = '' . $month_order . '';
            $month_count = '' . count($month_order_data->get()) . '';

            $month_mdiscount_order = $month_order_mdiscount_data->sum('mdiscount_amount');
            $month_mdiscount_amount = '' . $month_mdiscount_order . '';
            $month_mdiscount_count = '' . count($month_order_mdiscount_data->get()) . '';

            $refund_month_amount = $refund_month_order_data->sum('refund_amount');
            $refund_month_amount = '' . $refund_month_amount . '';
            $refund_month_count = '' . count($refund_month_order_data->get()) . '';

            //7天
            $data_day = [];
            $data = [1, 2, 3, 4, 5, 6, 7];
            foreach ($data as $k => $v) {
                $day = date("Ymd", time() - 24 * $v * 60 * 60);
                $data_day[$k]['date'] = date("m/d", time() - 24 * $v * 60 * 60);

                if ($merchant_id) {
                    $day_order_data = MerchantStoreDayOrder::where('day', $day)
                        ->where('merchant_id', $merchant_id)
                        ->select('total_amount', 'order_sum');
                } else {
                    $day_order_data = StoreDayOrder::where('day', $day)
                        ->whereIn('store_id', $store_ids);
                }

                $day_order_day = $day_order_data->sum('total_amount');
                $day_order_day = '' . $day_order_day . '';
                $day_order_count_day = '' . $day_order_data->sum('order_sum') . '';
                $data_day[$k]['day_amount'] = number_format($day_order_day, 2, '.', '');
                $data_day[$k]['day_count'] = $day_order_count_day;
            }

            $data = [
                'day_amount' => number_format($day_order, 2, '.', ''),
                'day_count' => $day_order_count,

                'refund_day_amount' => number_format($refund_day_order, 2, '.', ''),
                'refund_day_count' => $refund_day_order_count,

                'month_amount' => number_format($month_amount, 2, '.', ''),
                'month_count' => $month_count,

                'month_mdiscount_amount' => number_format($month_mdiscount_amount, 2, '.', ''),
                'month_mdiscount_count' => $month_mdiscount_count,

                'refund_month_amount' => number_format($refund_month_amount, 2, '.', ''),
                'refund_month_count' => $refund_month_count,

                'old_month_amount' => number_format($old_month_amount, 2, '.', ''),
                'old_month_count' => $old_month_count,

                'old_month_mdiscount_amount' => number_format($old_month_mdiscount_amount, 2, '.', ''),
                'old_month_mdiscount_count' => $old_month_mdiscount_count,

                'refund_old_month_amount' => number_format($refund_old_month_amount, 2, '.', ''),
                'refund_old_month_count' => $refund_old_month_count,

                'data_day' => $data_day
            ];

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage() . $exception->getLine();
            return $this->format();
        }
    }


    //APP订单轮询接口
    public function order_foreach(Request $request)
    {
        try {
            $store_id = $request->get('store_id', '');
            $out_trade_no = $request->get('out_trade_no', '');
            $ways_type = $request->get('ways_type', '');
            $config_id = $request->get('config_id', '');
            $Order = Order::where('out_trade_no', $out_trade_no)->select('*')->select();
            if ($Order->pay_status == 1) {
                return json_encode([
                    'status' => '1',
                    'pay_status' => '1',
                    'message' => '支付成功',
                    'data' => [
                        'ways_source' => $Order->ways_source,
                        'pay_time' => $Order->pay_time,
                        'out_trade_no' => $Order->out_trade_no,
                        'pay_amount' => $Order->buyer_pay_amount,
                        'total_amount' => $Order->total_amount,
                        'other_no' => $Order->other_no,
                        'trade_no' => $Order->trade_no
                    ]
                ]);
            }
            if ($Order->pay_status == 1) {
                return json_encode([
                    'status' => '1',
                    'pay_status' => '1',
                    'message' => '支付成功',
                    'data' => [
                        'ways_source' => $Order->ways_source,
                        'pay_time' => $Order->pay_time,
                        'out_trade_no' => $Order->out_trade_no,
                        'pay_amount' => $Order->buyer_pay_amount,
                        'total_amount' => $Order->total_amount,
                        'other_no' => $Order->other_no,
                        'trade_no' => $Order->trade_no
                    ]
                ]);
            }
            if ($Order->pay_status == 3) {
                return json_encode([
                    'status' => '2',
                    'pay_status' => '3',
                    'message' => '支付失败',
                    'data' => [
                        'out_trade_no' => $Order->out_trade_no,
                        'pay_amount' => $Order->pay_amount,
                        'total_amount' => $Order->total_amount,
                    ]
                ]);
            }
            if ($Order->pay_status == 2) {
                return json_encode([
                    'status' => '1',
                    'pay_status' => '2',
                    'message' => '订单未支付',
                    'data' => [
                        'out_trade_no' => $Order->out_trade_no,
                        'pay_amount' => $Order->total_amount,
                        'total_amount' => $Order->total_amount
                    ]
                ]);
            }
            return json_encode([
                'status' => '2',
                'pay_status' => '3',
                'message' => $Order->pay_status->desc,
                'data' => [
                    'out_trade_no' => $Order->out_trade_no,
                    'pay_amount' => $Order->pay_amount,
                    'total_amount' => $Order->total_amount,
                ],
            ]);
//            $data = [
//                'out_trade_no' => $out_trade_no,
//                'store_id' => $store_id,
//                'ways_type' => $ways_type,
//                'config_id' => $config_id,
//
//            ];
//            return $this->order_foreach_public($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage() . $exception->getLine();
            return $this->format();
        }
    }


    //APP订单轮询-公共
    public function order_foreach_public($data)
    {
        try {
            $store_id = $data['store_id'];
            $out_trade_no = isset($data['out_trade_no']) ? $data['out_trade_no'] : "";
            $other_no = isset($data['other_no']) ? $data['other_no'] : "";
            $table = isset($data['table']) ? $data['table'] : "";

            $ways_type = $data['ways_type'];
            $config_id = $data['config_id'];
            if ($out_trade_no) {
                $c_o = 'a' . $out_trade_no;
                $where[] = ['out_trade_no', '=', $out_trade_no];
            } elseif ($other_no) {
                $c_o = 'a' . $other_no;
                $where[] = ['other_no', '=', $other_no];
            } else {
                $c_o = 'a' . $out_trade_no;
                $where[] = ['auth_code', '=', $out_trade_no];
            }

            if ($table == "") {
                $day = date('Ymd', time());
                $table = 'orders_' . $day;
            }

            $order = Cache::get($c_o);
            if (!$order) {
                if (Schema::hasTable($table)) {
                    $order = DB::table($table)->where('store_id', $store_id);
                } else {
                    $order = Order::where('store_id', $store_id);
                }
                //发起查询
                $order = $order->where($where)
                    ->select(
                        'id',
                        'ways_source',
                        'ways_type',
                        'company',
                        'ways_type_desc',
                        'pay_status',
                        'out_trade_no',
                        'trade_no',
                        'total_amount',
                        'pay_amount',
                        'qwx_no',
                        'rate',
                        'fee_amount',
                        'merchant_id',
                        'store_id',
                        'user_id',
                        'store_name',
                        'other_no',
                        'created_at'
                    )->first();

                Cache::put($c_o, $order, 1);
            }

            if (!$order) {
                return json_encode([
                    'status' => 2,
                    'message' => '订单号不存在'
                ]);
            }
            $ways_type = $order->ways_type;
            $out_trade_no = $order->out_trade_no;
            $other_no = $order->other_no;
            $created_at = $order->created_at;

            $store = Store::where('store_id', $store_id)
                ->select('config_id', 'merchant_id', 'pid', 'people_phone', 'source', 'zero_rate_type')
                ->first();

            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店ID不存在'
                ]);
            }
            $config_id = $store->config_id;
            $store_pid = $store->pid;

            //支付宝官方订单
            if (999 < $ways_type && $ways_type < 1999) {
                //配置
                $isvconfig = new AlipayIsvConfigController();
                $storeInfo = $isvconfig->alipay_auth_info($store_id, $store_pid);
                $config = $isvconfig->AlipayIsvConfig($config_id);

                $app_auth_token = $storeInfo->app_auth_token;

                $notify_url = url('/api/alipayopen/qr_pay_notify');
                $aop = new AopClient();
                $aop->apiVersion = "2.0";
                $aop->appId = $config->app_id;
                $aop->rsaPrivateKey = $config->rsa_private_key;
                $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                $aop->notify_url = $notify_url;
                $aop->signType = "RSA2"; //升级算法
                $aop->gatewayUrl = $config->alipay_gateway;
                $aop->format = "json";
                $aop->charset = "GBK";
                $aop->version = "2.0";
                $aop->method = 'alipay.trade.query';
                $requests = new AlipayTradeQueryRequest();
                $requests->setBizContent("{" .
                    "    \"out_trade_no\":\"" . $out_trade_no . "\"" .
                    "  }");
                $status = $aop->execute($requests, '', $app_auth_token);


                if ($status->alipay_trade_query_response->code == 40004) {


                    $is_dqr = substr($out_trade_no, 0, 3);
                    $pay_status = "3";
                    $re_status = 2;
                    //动态码在没有付款会报交易不存在
                    if ($is_dqr == "DQR") {
                        $pay_status = "2";
                        $re_status = 1;
                        return json_encode([
                            'status' => $re_status,
                            'pay_status' => $pay_status,
                            'message' => $status->alipay_trade_query_response->sub_msg,
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount,
                                'other_no' => $other_no,

                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => $re_status,
                            'pay_status' => $pay_status,
                            'message' => $status->alipay_trade_query_response->sub_msg,
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount,
                            ]
                        ]);
                    }

                }
                //支付成功
                if ($status->alipay_trade_query_response->trade_status == "TRADE_SUCCESS") {
                    $buyer_pay_amount = $status->alipay_trade_query_response->buyer_pay_amount;

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => 'TRADE_SUCCESS',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => $status->alipay_trade_query_response->buyer_user_id,
                            'trade_no' => $status->alipay_trade_query_response->trade_no,
                            'pay_time' => $status->alipay_trade_query_response->send_pay_date,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'receipt_amount' => $status->alipay_trade_query_response->receipt_amount,
                        ];
                        $this->update_day_order($insert_data, $order->out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '1000',//返佣来源
                            'source_desc' => '支付宝',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'receipt_amount' => $status->alipay_trade_query_response->receipt_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $status->alipay_trade_query_response->send_pay_date,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",

                        ];
                        PaySuccessAction::action($data);
                    }

                    //改变数据库状态
                    if ($order->pay_status == 1 && $order->receipt_amount == 0) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => 'TRADE_SUCCESS',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => $status->alipay_trade_query_response->buyer_user_id,
                            'trade_no' => $status->alipay_trade_query_response->trade_no,
                            'pay_time' => $status->alipay_trade_query_response->send_pay_date,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'receipt_amount' => $status->alipay_trade_query_response->receipt_amount,
                        ];
                        $this->update_day_order($insert_data, $order->out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '1000',//返佣来源
                            'source_desc' => '支付宝',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'receipt_amount' => $status->alipay_trade_query_response->receipt_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $status->alipay_trade_query_response->send_pay_date,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",

                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'out_trade_no' => $order->out_trade_no,
                            'trade_no' => $status->alipay_trade_query_response->trade_no,
                            'pay_time' => $status->alipay_trade_query_response->send_pay_date,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'receipt_amount' => $status->alipay_trade_query_response->receipt_amount,
                            'discount_amount' => 0, //todo:第三方优惠金额
                            'promotion_name' => '', //todo:优惠名称
                            'promotion_amount' => $order->total_amount - $buyer_pay_amount, //todo:优惠总额；单位元，保留两位小数
                        ]
                    ]);

                } //等待付款
                elseif ($status->alipay_trade_query_response->trade_status == "WAIT_BUYER_PAY") {

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);

                } //订单关闭
                elseif ($status->alipay_trade_query_response->trade_status == 'TRADE_CLOSED') {
                    $order->update([
                        'status' => '4',
                        'pay_status' => 4,
                        'pay_status_desc' => '订单关闭',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                    $order->save();

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单关闭',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = $status->alipay_trade_query_response->sub_msg;
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                }
            }

            //直付通
            if (16000 < $ways_type && $ways_type < 16999) {
                $config_type = '03';
                //配置
                $isvconfig = new AlipayIsvConfigController();
                $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);

                $notify_url = url('/api/alipayopen/qr_pay_notify');
                $aop = new AopClient();
                $aop->apiVersion = "2.0";
                $aop->appId = $config->app_id;
                $aop->rsaPrivateKey = $config->rsa_private_key;
                $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                $aop->notify_url = $notify_url;
                $aop->signType = "RSA2"; //升级算法
                $aop->gatewayUrl = $config->alipay_gateway;
                $aop->format = "json";
                $aop->charset = "GBK";
                $aop->version = "2.0";
                $aop->method = 'alipay.trade.query';
                $requests = new AlipayTradeQueryRequest();
                $requests->setBizContent("{" .
                    "    \"out_trade_no\":\"" . $out_trade_no . "\"" .
                    "  }");
                $status = $aop->execute($requests, '', '');

                if ($status->alipay_trade_query_response->code == 40004) {
                    $is_dqr = substr($out_trade_no, 0, 3);
                    $pay_status = "3";
                    $re_status = 2;
                    //动态码在没有付款会报交易不存在
                    if ($is_dqr == "DQR") {
                        $pay_status = "2";
                        $re_status = 1;
                        return json_encode([
                            'status' => $re_status,
                            'pay_status' => $pay_status,
                            'message' => $status->alipay_trade_query_response->sub_msg,
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount,
                                'other_no' => $other_no,
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => $re_status,
                            'pay_status' => $pay_status,
                            'message' => $status->alipay_trade_query_response->sub_msg,
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount,
                            ]
                        ]);
                    }
                }
                //支付成功
                if ($status->alipay_trade_query_response->trade_status == "TRADE_SUCCESS") {
                    $buyer_pay_amount = $status->alipay_trade_query_response->buyer_pay_amount;

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);
                        $insert_data = [
                            'status' => 'TRADE_SUCCESS',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => $status->alipay_trade_query_response->buyer_user_id,
                            'trade_no' => $status->alipay_trade_query_response->trade_no,
                            'pay_time' => $status->alipay_trade_query_response->send_pay_date,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($insert_data, $order->out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '16000',//返佣来源
                            'source_desc' => '支付宝',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $status->alipay_trade_query_response->send_pay_date,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'out_trade_no' => $order->out_trade_no,
                            'trade_no' => $status->alipay_trade_query_response->trade_no,
                            'pay_time' => $status->alipay_trade_query_response->send_pay_date,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no
                        ]
                    ]);
                } //等待付款
                elseif ($status->alipay_trade_query_response->trade_status == "WAIT_BUYER_PAY") {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                } //订单关闭
                elseif ($status->alipay_trade_query_response->trade_status == 'TRADE_CLOSED') {
                    $order->update([
                        'status' => '4',
                        'pay_status' => 4,
                        'pay_status_desc' => '订单关闭',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                    $order->save();

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单关闭',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = $status->alipay_trade_query_response->sub_msg;
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                }
            }

            //官方微信支付订单
            if (1999 < $ways_type && $ways_type < 2999) {
                $config = new WeixinConfigController();
                $options = $config->weixin_config($config_id);
                $weixin_store = $config->weixin_merchant($store_id, $store_pid);
                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;
                $config = [
                    'app_id' => $options['app_id'],
                    'mch_id' => $options['payment']['merchant_id'],
                    'key' => $options['payment']['key'],
                    'cert_path' => $options['payment']['cert_path'], // XXX: 绝对路径！！！！
                    'key_path' => $options['payment']['key_path'],     // XXX: 绝对路径！！！！
                    'sub_mch_id' => $wx_sub_merchant_id,
                    // 'device_info'     => '013467007045764',
                    // 'sub_app_id'      => '',
                    // ...
                ];

                $payment = Factory::payment($config);
                $query = $payment->order->queryByOutTradeNumber($order->out_trade_no);
                // Log::info("ly-官方微信支付订单-查询");
                // Log::info($query);

                //成功
                if ($query['trade_state'] == 'SUCCESS') {
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $order->out_trade_no; //$query['transaction_id'];
                    $re_data['time_end'] = $query['time_end'];

                    if ($order->pay_status != 1) {
                        //改变数据库状态
                        Cache::forget($c_o);
                        $insert_data = [
                            'status' => 'TRADE_SUCCESS',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => $query['openid'],
                            'trade_no' => $query['transaction_id'],
                            'pay_time' => date('Y-m-d H:i:s', strtotime($query['time_end']))
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '2000',//返佣来源
                            'source_desc' => '微信支付',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => date('Y-m-d H:i:s', strtotime($query['time_end'])),
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    // 如果用户输入密码，微信被扫没有返回优惠金额、实付金额等，所以通过查询订单来更新
                    // settlement_total_fee 当订单使用了免充值型优惠券后返回该参数
                    // 如果有 settlement_total_fee 这个参数，商家实收金额等于 settlement_total_fee
                    // 否则商家实收金额等于 total_fee，优惠金额不纳入计算
                    if (isset($query['settlement_total_fee']) && !empty($query['settlement_total_fee'])) {
                        if ($order->receipt_amount != $query['settlement_total_fee'] / 100) {
                            $data_update['receipt_amount'] = $query['settlement_total_fee'] / 100;
                            if (isset($query['coupon_fee']) && !empty($query['coupon_fee'])) {
                                $data_update['mdiscount_amount'] = $query['coupon_fee'] / 100;
                            }
                            $this->update_day_order($data_update, $out_trade_no, $table);
                        }
                    } else if (isset($query['total_fee']) && !empty($query['total_fee'])) {
                        if ($order->receipt_amount != $query['total_fee'] / 100) {
                            $data_update['receipt_amount'] = $query['total_fee'] / 100;
                            if (isset($query['coupon_fee']) && !empty($query['coupon_fee'])) {
                                $data_update['discount_amount'] = $query['coupon_fee'] / 100;
                            }
                            $this->update_day_order($data_update, $out_trade_no, $table);
                        }
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => date('Y-m-d H:i:s', strtotime($query['time_end'])),
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $query['transaction_id'],
                            'discount_amount' => 0, //todo:第三方优惠金额
                            'promotion_name' => '', //todo:优惠名称
                            'promotion_amount' => isset($query['coupon_fee']) ? $query['coupon_fee'] / 100 : 0, //todo:优惠总额；单位元，保留两位小数
                        ]
                    ]);
                } elseif ($query['trade_state'] == "USERPAYING" || $query['trade_state'] == "NOTPAY") {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no
                        ]
                    ]);
                } elseif ($query['trade_state'] == "CLOSED") {
                    $order->update([
                        'status' => '4',
                        'pay_status' => '4',
                        'pay_status_desc' => '订单关闭',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                    $order->save();

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单关闭',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } elseif ($query['trade_state'] == "REVOKED") {
                    $order->update([
                        'status' => '3',
                        'pay_status' => '3',
                        'pay_status_desc' => '订单已撤销',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                    $order->save();

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单关闭',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = $query['trade_state_desc'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                }
            }

            //官方微信a 支付订单
            if (3999 < $ways_type && $ways_type < 4999) {
                $config = new WeixinConfigController();
                $options = $config->weixina_config($config_id);
                $weixin_store = $config->weixina_merchant($store_id, $store_pid);
                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;
                $config = [
                    'app_id' => $options['app_id'],
                    'mch_id' => $options['payment']['merchant_id'],
                    'key' => $options['payment']['key'],
                    'cert_path' => $options['payment']['cert_path'], // XXX: 绝对路径！！！！
                    'key_path' => $options['payment']['key_path'],     // XXX: 绝对路径！！！！
                    'sub_mch_id' => $wx_sub_merchant_id,
                    // 'device_info'     => '013467007045764',
                    // 'sub_app_id'      => '',
                    // ...
                ];
                $payment = Factory::payment($config);
                $query = $payment->order->queryByOutTradeNumber($order->out_trade_no);
                // Log::info('ly-微信a-订单查询：');
                // Log::info($query);

                //成功
                if ($query['trade_state'] == 'SUCCESS') {
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $order->out_trade_no; //$query['transaction_id'];
                    $re_data['time_end'] = $query['time_end'];

                    if ($order->pay_status != 1) {
                        //改变数据库状态
                        Cache::forget($c_o);
                        $insert_data = [
                            'status' => 'TRADE_SUCCESS',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => $query['openid'],
                            'trade_no' => $query['transaction_id'],
                            'pay_time' => date('Y-m-d H:i:s', strtotime($query['time_end']))
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '4000', //返佣来源
                            'source_desc' => '微信支付a', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => date('Y-m-d H:i:s', strtotime($query['time_end'])),
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    // 如果用户输入密码，微信被扫没有返回优惠金额、实付金额等，所以通过查询订单来更新
                    // settlement_total_fee 当订单使用了免充值型优惠券后返回该参数
                    // 如果有 settlement_total_fee 这个参数，商家实收金额等于 settlement_total_fee
                    // 否则商家实收金额等于 total_fee，优惠金额不纳入计算
                    if (isset($query['settlement_total_fee']) && !empty($query['settlement_total_fee'])) {
                        if ($order->receipt_amount != $query['settlement_total_fee'] / 100) {
                            $data_update['receipt_amount'] = $query['settlement_total_fee'] / 100;
                            if (isset($query['coupon_fee']) && !empty($query['coupon_fee'])) {
                                $data_update['mdiscount_amount'] = $query['coupon_fee'] / 100;
                            }
                            $this->update_day_order($data_update, $out_trade_no, $table);
                        }
                    } else if (isset($query['total_fee']) && !empty($query['total_fee'])) {
                        if ($order->receipt_amount != $query['total_fee'] / 100) {
                            $data_update['receipt_amount'] = $query['total_fee'] / 100;
                            if (isset($query['coupon_fee']) && !empty($query['coupon_fee'])) {
                                $data_update['discount_amount'] = $query['coupon_fee'] / 100;
                            }
                            $this->update_day_order($data_update, $out_trade_no, $table);
                        }
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => date('Y-m-d H:i:s', strtotime($query['time_end'])),
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $query['transaction_id'],
                            'discount_amount' => 0, //todo:第三方优惠金额
                            'promotion_name' => '', //todo:优惠名称
                            'promotion_amount' => isset($query['coupon_fee']) ? $query['coupon_fee'] / 100 : 0, //todo:优惠总额；单位元，保留两位小数
                        ]
                    ]);
                } elseif ($query['trade_state'] == "USERPAYING" || $query['trade_state'] == "NOTPAY") {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no
                        ]
                    ]);
                } elseif ($query['trade_state'] == "CLOSED") {
                    $order->update([
                        'status' => '4',
                        'pay_status' => '4',
                        'pay_status_desc' => '订单关闭',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                    $order->save();

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单关闭',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } elseif ($query['trade_state'] == "REVOKED") {
                    $order->update([
                        'status' => '3',
                        'pay_status' => '3',
                        'pay_status_desc' => '订单已撤销',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                    $order->save();

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单关闭',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = $query['trade_state_desc'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                }
            }

            //京东收银支付
            if (5999 < $ways_type && $ways_type < 6999) {
                //读取配置
                $config = new JdConfigController();
                $jd_config = $config->jd_config($config_id);
                if (!$jd_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '京东配置不存在请检查配置'
                    ]);
                }

                $jd_merchant = $config->jd_merchant($store_id, $store_pid);
                if (!$jd_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '京东商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Jd\PayController();
                $data = [];
                $data['out_trade_no'] = $out_trade_no;
                $data['request_url'] = $obj->order_query_url; //请求地址;
                $data['merchant_no'] = $jd_merchant->merchant_no;
                $data['md_key'] = $jd_merchant->md_key; //
                $data['des_key'] = $jd_merchant->des_key; //
                $data['systemId'] = $jd_config->systemId; //
                $return = $obj->order_query($data);
                //支付成功
                if ($return["status"] == 1) {
                    $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['payFinishTime']));
                    $trade_no = $return['data']['tradeNo'];
                    $channelNoSeq = isset($return['data']['channelNoSeq']) ? $return['data']['channelNoSeq'] : $trade_no; //条码
                    $buyer_pay_amount = $return['data']['piAmount'] / 100;
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $channelNoSeq,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '6000',//返佣来源
                            'source_desc' => '京东金融',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : ""
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $channelNoSeq
                        ]
                    ]);

                } //等待付款
                elseif ($return["status"] == 2) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no
                        ]
                    ]);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);

                }//订单退款
                elseif ($return["status"] == 4) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单已经退款',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                }
            }

            //快钱支付
            if (2999 < $ways_type && $ways_type < 3999) {
                //读取配置
                $MyBankobj = new MyBankConfigController();
                $MyBankConfig = $MyBankobj->MyBankConfig($config_id);
                if (!$MyBankConfig) {
                    return json_encode([
                        'status' => 2,
                        'message' => '快钱配置不存在请检查配置'
                    ]);
                }

                $mybank_merchant = $MyBankobj->mybank_merchant($store_id, $store_pid);
                if (!$mybank_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '快钱商户号不存在'
                    ]);
                }

                $MerchantId = $mybank_merchant->MerchantId;
                $obj = new TradePayController();
                $return = $obj->mybankOrderQuery($MerchantId, $config_id, $out_trade_no);
                if ($return['status'] == 0) {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }
                $body = $return['data']['document']['response']['body'];
                $TradeStatus = $body['TradeStatus'];

                //成功
                if ($TradeStatus == 'succ') {
                    $OrderNo = $body['MerchantOrderNo'];
                    $GmtPayment = $body['GmtPayment'];
                    $buyer_id = '';
                    if ($ways_type == 3004) {
                        $buyer_id = $body['SubOpenId'];
                    }
                    if ($ways_type == 3003) {
                        $buyer_id = $body['BuyerUserId'];
                    }

                    $pay_time = date('Y-m-d H:i:s', strtotime($GmtPayment));
                    $payment_method = strtolower($body['Credit']);

                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);
                        $insert_data = [
                            'status' => 1,
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_id' => $buyer_id,
                            'trade_no' => $OrderNo,
                            'pay_time' => $pay_time,
                            'payment_method' => $payment_method,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '3000',//返佣来源
                            'source_desc' => '快钱支付',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : ""
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $OrderNo
                        ]
                    ]);
                } elseif ($TradeStatus == 'paying') {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = '请重新扫码';
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                }
            }

            //新大陆
            if (7999 < $ways_type && $ways_type < 8999) {
                //读取配置
                $config = new NewLandConfigController();
                $new_land_config = $config->new_land_config($config_id);
                if (!$new_land_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '新大陆配置不存在请检查配置'
                    ]);
                }

                $mybank_merchant = $config->new_land_merchant($store_id, $store_pid);
                if (!$mybank_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户新大陆通道未开通'
                    ]);
                }

                $request_data = [
                    'out_trade_no' => $out_trade_no,
                    'key' => $mybank_merchant->nl_key,
                    'org_no' => $new_land_config->org_no,
                    'merc_id' => $mybank_merchant->nl_mercId,
                    'trm_no' => $mybank_merchant->trmNo,
                    'op_sys' => '3',
                    'opr_id' => $store->merchant_id,
                    'trm_typ' => 'T'
                ];

                $obj = new PayController();
                $return = $obj->order_query($request_data);
                //支付成功
                if ($return["status"] == 1) {
                    $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['sysTime']));
                    $trade_no = $return['data']['orderNo'];
                    $buyer_pay_amount = $return['data']['amount'] / 100;
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '8000',//返佣来源
                            'source_desc' => '新大陆',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : ""
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $trade_no
                        ]
                    ]);
                } //等待付款
                elseif ($return["status"] == 2) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no
                        ]
                    ]);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                }//订单退款
                elseif ($return["status"] == 4) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单已经退款',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                }
            }

            //和融通收银支付
            if (8999 < $ways_type && $ways_type < 9999) {
                //读取配置
                $config = new HConfigController();
                $h_config = $config->h_config($config_id);
                if (!$h_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '和融通不存在请检查配置'
                    ]);
                }

                $h_merchant = $config->h_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '和融通商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Huiyuanbao\PayController();
                $data = [];
                $data['out_trade_no'] = $out_trade_no;
                $data['request_url'] = $obj->order_query_url; //请求地址;
                $data['md_key'] = $h_config->md_key; //
                $data['mid'] = $h_merchant->h_mid; //
                $data['orgNo'] = $h_config->orgNo; //

                $return = $obj->order_query($data);

                //支付成功
                if ($return["status"] == 1) {
                    // $pay_time = strtotime($return['data']['timeEnd']) ? strtotime($return['data']['timeEnd']) : time();
                    $pay_time = date('Y-m-d H:i:s', time());
                    $trade_no = '112121' . $return['data']['transactionId'];
                    $buyer_pay_amount = $return['data']['totalFee'];
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '9000',//返佣来源
                            'source_desc' => '和融通',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : ""
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $trade_no
                        ]
                    ]);
                } //等待付款
                elseif ($return["status"] == 2) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,

                        ]
                    ]);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);

                }//订单退款
                elseif ($return["status"] == 4) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单已经退款',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                }
            }

            //ltf收银支付
            if (9999 < $ways_type && $ways_type < 10999) {
                //读取配置
                $config = new LtfConfigController();
                $ltf_merchant = $config->ltf_merchant($store_id, $store_pid);
                if (!$ltf_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Ltf\PayController();
                $data = [];
                $data['out_trade_no'] = $out_trade_no;
                $data['request_url'] = $obj->order_query_url; //请求地址;
                $data['merchant_no'] = $ltf_merchant->merchantCode;
                $data['appId'] = $ltf_merchant->appId; //
                $data['key'] = $ltf_merchant->md_key; //

                $return = $obj->order_query($data);
                //支付成功
                if ($return["status"] == 1) {
                    $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['payTime']));
                    $trade_no = $return['data']['outTransactionId'];
                    $buyer_pay_amount = $return['data']['receiptAmount'];
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '10000',//返佣来源
                            'source_desc' => '联拓富',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : ""
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $trade_no
                        ]
                    ]);
                } //等待付款
                elseif ($return["status"] == 2) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'other_no' => $other_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ],
                    ]);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ],
                    ]);
                }//订单退款
                elseif ($return["status"] == 4) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单已经退款',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ],
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ],
                    ]);
                }
            }

            //随行付收银支付
            if (12999 < $ways_type && $ways_type < 13999) {
                //读取配置
                $config = new VbillConfigController();
                $vbill_config = $config->vbill_config($config_id);
                if (!$vbill_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付配置不存在请检查配置'
                    ]);
                }

                $vbill_merchant = $config->vbill_merchant($store_id, $store_pid);
                if (!$vbill_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Vbill\PayController();
                $data['notify_url'] = url('/api/vbill/pay_notify_url'); //回调地址
                $data['request_url'] = $obj->order_query_url; //请求地址;
                $data['mno'] = $vbill_merchant->mno;
                $data['privateKey'] = $vbill_config->privateKey; //
                $data['sxfpublic'] = $vbill_config->sxfpublic; //
                $data['orgId'] = $vbill_config->orgId; //
                $data['out_trade_no'] = $order->out_trade_no;
                $return = $obj->order_query($data);
                // Log::info('随行付支付查询');
                // Log::info($return);

                $message = $return['message'];

                //支付成功
                if ($return["status"] == 1) {
                    $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['respData']['tranTime']));
                    $trade_no = $return['data']['respData']['sxfUuid'];
                    $buyer_pay_amount = $return['data']['respData']['totalOffstAmt']; //消费者付款金额
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                    $promotionDetail = isset($return['data']['respData']['promotionDetail']) ? $return['data']['respData']['promotionDetail'] : ''; //优惠信息（jsonArray格式字符串）
                    $settleAmt = isset($return['data']['respData']['settleAmt']) ? $return['data']['respData']['settleAmt'] : 0; //商家入账金额,说明：包含手续费、预充值、平台补贴（优惠），不含免充值代金券（商家补贴）
                    $recFeeAmt = isset($return['data']['respData']['recFeeAmt']) ? $return['data']['respData']['recFeeAmt'] : 0; //交易手续费；单位元
                    $promotion_name = '';
                    $promotion_amount = 0;
                    if ($promotionDetail) {
                        $promotion_detail_arr = json_decode($promotionDetail, true);
                        if ($promotion_detail_arr && is_array($promotion_detail_arr)) {
//                    $promotion_name = isset($promotion_detail_arr[0]['name']) ? $promotion_detail_arr[0]['name'] : ''; //优惠名称
//                    $promotion_amount = isset($promotion_detail_arr[0]['amount']) ? $promotion_detail_arr[0]['amount']: 0.00; //优惠总额；单位元，保留两位小数
                            $promotion_name = '刷脸支付天天有优惠';
                            $promotion_amount = array_sum(array_column($promotion_detail_arr, 'amount'));
                        }
                    }

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);
//                        $fee_amount = $recFeeAmt? $recFeeAmt : ($settleAmt*$order->rate)/100;
                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'discount_amount' => $promotion_amount, //第三方平台优惠金额
//                            'fee_amount' => $fee_amount
                        ];
                        if ($settleAmt) $insert_data['receipt_amount'] = $settleAmt;
                        if ($recFeeAmt) $insert_data['fee_amount'] = $recFeeAmt;
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '13000',//返佣来源
                            'source_desc' => '随行付',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : ""
                        ];
                        PaySuccessAction::action($data);
                    }

                    // 更新实收金额
                    if (isset($return['data']['respData']['settleAmt']) && !empty($return['data']['respData']['settleAmt'])) {
                        if ($order->receipt_amount != $return['data']['respData']['settleAmt']) {
                            $data_update['receipt_amount'] = $return['data']['respData']['settleAmt'];
                            if (isset($promotion_amount) && !empty($promotion_amount)) {
                                $data_update['mdiscount_amount'] = $promotion_amount;
                            }
                            $res = $this->update_day_order($data_update, $out_trade_no, $table);
                        }
                    } else if (isset($return['data']['respData']['totalOffstAmt']) && !empty($return['data']['respData']['totalOffstAmt'])) {
                        if ($order->receipt_amount != $return['data']['respData']['totalOffstAmt']) {
                            $data_update['receipt_amount'] = $return['data']['respData']['totalOffstAmt'];
                            if (isset($promotion_amount) && !empty($promotion_amount)) {
                                $data_update['mdiscount_amount'] = $promotion_amount;
                            }
                            $this->update_day_order($data_update, $out_trade_no, $table);
                        }
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $trade_no,
                            'promotion_name' => $promotion_name, //优惠名称
                            'promotion_amount' => 0, //todo:优惠总额；单位元，保留两位小数
                            'discount_amount' => $promotion_amount, //第三方优惠金额
                        ]
                    ]);
                } //等待付款
                elseif ($return["status"] == 2) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => ($message == '成功') ? '等待支付' : $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => ($message == '成功') ? '订单支付失败' : $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ],
                    ]);
                }//订单退款
                elseif ($return["status"] == 4) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单已经退款',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ],
                    ]);
                } else {
                    //其他情况
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ],
                    ]);
                }
            }

            //随行付a收银支付
            if (18999 < $ways_type && $ways_type < 19999) {
                //读取配置
                $config = new VbillConfigController();
                $vbill_config = $config->vbilla_config($config_id);
                if (!$vbill_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付a配置不存在请检查配置'
                    ]);
                }

                $vbill_merchant = $config->vbilla_merchant($store_id, $store_pid);
                if (!$vbill_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付a商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Vbill\PayController();
                $data['notify_url'] = url('/api/vbill/pay_notify_a_url'); //回调地址
                $data['request_url'] = $obj->order_query_url; //请求地址;
                $data['mno'] = $vbill_merchant->mno;
                $data['privateKey'] = $vbill_config->privateKey; //
                $data['sxfpublic'] = $vbill_config->sxfpublic; //
                $data['orgId'] = $vbill_config->orgId; //
                $data['out_trade_no'] = $order->out_trade_no;
                $return = $obj->order_query($data);
                // Log::info('随行付a支付查询');
                // Log::info($return);

                $message = $return['message'];

                //支付成功
                if ($return["status"] == 1) {
                    $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['respData']['tranTime']));
                    $trade_no = $return['data']['respData']['sxfUuid'];
                    $buyer_pay_amount = $return['data']['respData']['totalOffstAmt'];
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                    $promotionDetail = isset($return['data']['respData']['promotionDetail']) ? $return['data']['respData']['promotionDetail'] : ''; //优惠信息（jsonArray格式字符串）
                    $settleAmt = isset($return['data']['respData']['settleAmt']) ? $return['data']['respData']['settleAmt'] : 0; //商家入账金额,说明：包含手续费、预充值、平台补贴（优惠），不含免充值代金券（商家补贴）
                    $recFeeAmt = isset($return['data']['respData']['recFeeAmt']) ? $return['data']['respData']['recFeeAmt'] : 0; //交易手续费；单位元
                    $promotion_name = '';
                    $promotion_amount = 0;
                    if ($promotionDetail) {
                        $promotion_detail_arr = json_decode($promotionDetail, true);
                        if ($promotion_detail_arr && is_array($promotion_detail_arr)) {
//                    $promotion_name = isset($promotion_detail_arr[0]['name']) ? $promotion_detail_arr[0]['name'] : ''; //优惠名称
//                    $promotion_amount = isset($promotion_detail_arr[0]['amount']) ? $promotion_detail_arr[0]['amount']: 0.00; //优惠总额；单位元，保留两位小数
                            $promotion_name = '刷脸支付天天有优惠';
                            $promotion_amount = array_sum(array_column($promotion_detail_arr, 'amount'));
                        }
                    }

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);
//                        $fee_amount = $settleAmt ? ($settleAmt*$order->rate)/100 : ($order->total_amount*$order->rate)/100;

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'discount_amount' => $promotion_amount, //第三方平台优惠金额
//                            'fee_amount' => $recFeeAmt?$recFeeAmt:$fee_amount
                        ];
                        if ($settleAmt) $insert_data['receipt_amount'] = $settleAmt;
                        if ($recFeeAmt) $insert_data['fee_amount'] = $recFeeAmt;
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '19000',//返佣来源
                            'source_desc' => '随行付A',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : ""
                        ];
                        PaySuccessAction::action($data);
                    }

                    // 更新实收金额
                    if (isset($return['data']['respData']['settleAmt']) && !empty($return['data']['respData']['settleAmt'])) {
                        if ($order->receipt_amount != $return['data']['respData']['settleAmt']) {
                            $data_update['receipt_amount'] = $return['data']['respData']['settleAmt'];
                            if (isset($promotion_amount) && !empty($promotion_amount)) {
                                $data_update['mdiscount_amount'] = $promotion_amount;
                            }
                            $res = $this->update_day_order($data_update, $out_trade_no, $table);
                        }
                    } else if (isset($return['data']['respData']['totalOffstAmt']) && !empty($return['data']['respData']['totalOffstAmt'])) {
                        if ($order->receipt_amount != $return['data']['respData']['totalOffstAmt']) {
                            $data_update['receipt_amount'] = $return['data']['respData']['totalOffstAmt'];
                            if (isset($promotion_amount) && !empty($promotion_amount)) {
                                $data_update['mdiscount_amount'] = $promotion_amount;
                            }
                            $this->update_day_order($data_update, $out_trade_no, $table);
                        }
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $trade_no,
                            'promotion_name' => $promotion_name, //优惠名称
                            'promotion_amount' => 0, //todo:优惠总额,单位元,保留两位小数
                            'discount_amount' => $promotion_amount, //第三方优惠金额
                        ]
                    ]);
                } //等待付款
                elseif ($return["status"] == 2) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => ($message == '成功') ? '等待支付' : $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => ($message == '成功') ? '订单支付失败' : $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ],
                    ]);
                }//订单退款
                elseif ($return["status"] == 4) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单已经退款',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ],
                    ]);
                } else {
                    //其他情况
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ],
                    ]);
                }
            }

            //哆啦宝
            if (14999 < $ways_type && $ways_type < 15999) {
                $manager = new ManageController();
                //读取配置
                $dlbconfig = $manager->pay_config($config_id);
                if (!$dlbconfig) {
                    return json_encode([
                        'status' => 2,
                        'message' => '哆啦宝配置配置不存在请检查配置'
                    ]);
                }

                $dlb_merchant = $manager->dlb_merchant($store_id, $store_pid);
                if (!$dlb_merchant && !empty($dlb_merchant->mch_num) && !empty($dlb_merchant->shop_num)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '哆啦宝配置商户未补充商户编号等信息!'
                    ]);
                }

                $query_data = [
                    'agentNum' => $dlbconfig->agent_num,
                    'customerNum' => $dlb_merchant->mch_num,
                    'shopNum' => $dlb_merchant->shop_num,
                    'requestNum' => $order->out_trade_no,
                    'secretKey' => $dlbconfig->secret_key,
                    'accessKey' => $dlbconfig->access_key,
                ];
                $return = $manager->query_bill($query_data);
                if ($return['status'] == 0) {
                    return json_encode($return);
                }
                if ($return['status'] == 1) {
                    $query_result = $return['data'];
                    if ($query_result['status'] == "SUCCESS") {
                        $pay_time = $query_result['completeTime'];
                        $trade_no = $query_result['orderNum'];
                        $buyer_pay_amount = $query_result['orderAmount'];
                        $buyer_id = isset($query_result['openId']) ? $query_result['openId'] : "";

                        //改变数据库状态
                        if ($order->pay_status != 1) {
                            Cache::forget($c_o);

                            $insert_data = [
                                'status' => '1',
                                'pay_status' => 1,
                                'pay_status_desc' => '支付成功',
                                'buyer_id' => $buyer_id,
                                'trade_no' => $trade_no,
                                'pay_time' => $pay_time,
                                'buyer_pay_amount' => $buyer_pay_amount
                            ];
                            $this->update_day_order($insert_data, $out_trade_no, $table);

                            //支付成功后的动作
                            $data = [
                                'ways_type' => $order->ways_type,
                                'ways_type_desc' => $order->ways_type_desc,
                                'source_type' => '15000',//返佣来源
                                'source_desc' => '哆啦宝',//返佣来源说明
                                'company' => 'dlb',//返佣来源说明
                                'total_amount' => $order->total_amount,
                                'out_trade_no' => $order->out_trade_no,
                                'rate' => $order->rate,
                                'merchant_id' => $order->merchant_id,
                                'store_id' => $order->store_id,
                                'user_id' => $order->user_id,
                                'config_id' => $config_id,
                                'store_name' => $order->store_name,
                                'ways_source' => $order->ways_source,
                                'pay_time' => $pay_time,
                                'device_id' => isset($order->device_id) ? $order->device_id : "",
                            ];
                            PaySuccessAction::action($data);
                        }

                        return json_encode([
                            'status' => 1,
                            'pay_status' => '1',
                            'message' => '支付成功',
                            'data' => [
                                'ways_source' => $order->ways_source,
                                'pay_time' => $pay_time,
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $buyer_pay_amount,
                                'total_amount' => $order->total_amount,
                                'other_no' => $other_no,
                                'trade_no' => $trade_no
                            ]
                        ]);
                    } elseif ($query_result['status'] == "INIT") {
                        return json_encode([
                            'status' => 1,
                            'pay_status' => '2',
                            'message' => '等待支付',
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->total_amount,
                                'total_amount' => $order->total_amount,
                            ]
                        ]);
                    } elseif ($query_result['status'] == "CANCEL") {
                        return json_encode([
                            'status' => 2,
                            'pay_status' => '3',
                            'message' => '订单支付失败',
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount,
                            ],
                        ]);
                    } elseif ($query_result['status'] == "REFUND") {
                        return json_encode([
                            'status' => 1,
                            'pay_status' => '3',
                            'message' => '订单已经退款',
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount,
                            ],
                        ]);
                    } elseif ($query_result['status'] == "REFUNDING") {
                        return json_encode([
                            'status' => 1,
                            'pay_status' => '3',
                            'message' => '订单退款中',
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount,
                            ],
                        ]);
                    } elseif ($query_result['status'] == "FUNDFAIL") {
                        return json_encode([
                            'status' => 1,
                            'pay_status' => '3',
                            'message' => '订单退款失败',
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount
                            ]
                        ]);
                    }
                }
            }

            //传化收银支付
            if (11999 < $ways_type && $ways_type < 12999) {
                //读取配置
                $config = new TfConfigController();

                $h_merchant = $config->tf_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '传化商户号不存在'
                    ]);
                }

                $h_config = $config->tf_config($config_id, $h_merchant->qd);
                if (!$h_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '传化配置不存在请检查配置'
                    ]);
                }

                $obj = new \App\Api\Controllers\Tfpay\PayController();
                $data['mch_id'] = $h_config->mch_id; //
                $data['pub_key'] = $h_config->pub_key; //
                $data['pri_key'] = $h_config->pri_key; //
                $data['sub_mch_id'] = $h_merchant->sub_mch_id; //
                $data['out_trade_no'] = $out_trade_no; //
                $data['date'] = date('Y-m-d', strtotime($created_at)); //

                $return = $obj->order_query($data);

                //支付成功
                if ($return["status"] == 1) {
                    $pay_time = $return['data']['paid_at'];
                    $trade_no = $return['data']['channel_no'];
                    $buyer_pay_amount = $return['data']['total_fee'];
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '12000',//返佣来源
                            'source_desc' => 'TF',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : ""
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $trade_no
                        ]
                    ]);
                } //等待付款
                elseif ($return["status"] == 2) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no
                        ]
                    ]);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                }//订单退款
                elseif ($return["status"] == 4) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单已经退款',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                }
            }

            //汇付支付
            if (17999 < $ways_type && $ways_type < 18999) {
                //交易结果查询
                $config = new HuiPayConfigController();
                $hui_pay_config = $config->hui_pay_config($config_id);
                if (!$hui_pay_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '汇付配置不存在请检查配置'
                    ]);
                }

                $hui_pay_merchant = $config->hui_pay_merchant($store_id, $store_pid);
                if (!$hui_pay_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '汇付商户不存在'
                    ]);
                }
                $hui_pay_store = $hui_pay_merchant->mer_id;
                if (!$hui_pay_store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '汇付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\HuiPay\PayController();
                $data['request_url'] = $obj->order_info; //请求地址;
                $data['mer_cust_id'] = $hui_pay_config->mer_cust_id; //商户客户号
                $data['cust_id'] = $hui_pay_merchant->user_cust_id; //用户客户号
                $data['private_key'] = $hui_pay_config->private_key;
                $data['public_key'] = $hui_pay_config->public_key;
                $data['org_id'] = $hui_pay_config->org_id;
                $data['out_trade_no'] = $order->out_trade_no; //系统订单号
                $data['device_id'] = $hui_pay_merchant->device_id ?? ''; //机具id
                $return = $obj->order_query($data); //0-系统错误 1-成功 2-处理中 3-失败 4.初始
//                Log::info('汇付交易信息查询返回111: '.str_replace("\\/", "/", json_encode($return, JSON_UNESCAPED_UNICODE)));
                //支付成功
                if ($return["status"] == 1) {
                    if (isset($return['data']) && !empty($return['data'])) {
                        if (isset($return['data']['trans_date']) && !empty($return['data']['trans_date'])) {
                            $trans_date = date('Y-m-d', strtotime($return['data']['trans_date']));
                            $trans_time = date('H:i:s', strtotime($return['data']['trans_time']));
                            $pay_time = $trans_date . ' ' . $trans_time;
                        }
                        $trade_no = $return['data']['party_order_id']; //第三方支付凭证号
                        $buyer_pay_amount = $return['data']['trans_amt']; //交易金额
                        $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                        //改变数据库状态
                        if ($order->pay_status != 1) {
                            Cache::forget($c_o);
                            $insert_data = [
                                'status' => '1',
                                'pay_status' => 1,
                                'pay_status_desc' => '支付成功',
                                'buyer_logon_id' => '',
                                'trade_no' => $trade_no,
                                'buyer_pay_amount' => $buyer_pay_amount,
                            ];
                            if (isset($return['data']['trans_date']) && !empty($return['data']['trans_date'])) {
                                $insert_data['pay_time'] = $pay_time;
                            }

                            $this->update_day_order($insert_data, $out_trade_no, $table);

                            //支付成功后的动作
                            $data = [
                                'ways_type' => $order->ways_type,
                                'company' => $order->company,
                                'ways_type_desc' => $order->ways_type_desc,
                                'source_type' => '18000',//返佣来源
                                'source_desc' => '汇付',//返佣来源说明
                                'total_amount' => $order->total_amount,
                                'out_trade_no' => $order->out_trade_no,
                                'rate' => $order->rate,
                                'fee_amount' => $order->fee_amount,
                                'merchant_id' => $order->merchant_id,
                                'store_id' => $order->store_id,
                                'user_id' => $order->user_id,
                                'config_id' => $config_id,
                                'store_name' => $order->store_name,
                                'ways_source' => $order->ways_source,
                                'device_id' => isset($order->device_id) ? $order->device_id : "",
                            ];
                            if (isset($return['data']['trans_date']) && !empty($return['data']['trans_date'])) {
                                $data['pay_time'] = $pay_time;
                            }
                            PaySuccessAction::action($data);
                        }
                        return json_encode([
                            'status' => 1,
                            'pay_status' => '1',
                            'message' => '支付成功',
                            'data' => [
                                'ways_source' => $order->ways_source,
                                'pay_time' => isset($return['data']['trans_date']) ? $pay_time : '',
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $buyer_pay_amount,
                                'total_amount' => $order->total_amount,
                                'other_no' => $other_no,
                                'trade_no' => $trade_no,
                            ]
                        ]);
                    }
                } //等待付款
                elseif ($return["status"] == 2) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);

                }//订单退款
                elseif ($return["status"] == 4) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单已经退款',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);

                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                }
            }

            //工行收银支付
            if (19999 < $ways_type && $ways_type < 20999) {
                $config = new LianfuConfigController();

                $h_merchant = $config->lianfu_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '工行商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\lianfu\PayController();
                $data['apikey'] = $h_merchant->apikey; //
                $data['signkey'] = $h_merchant->signkey; //
                $data['out_trade_no'] = $out_trade_no; //

                $return = $obj->order_query($data);

                //支付成功
                if ($return["status"] == 1) {
                    $pay_time = date('Y-m-d H:i:s', time());
                    $trade_no = isset($return['data']['tp_order_id']) ? $return['data']['tp_order_id'] : $out_trade_no;
                    $buyer_pay_amount = $order->total_amount;
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '20200',//返佣来源
                            'source_desc' => 'lianfu',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : ""
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $trade_no
                        ]
                    ]);
                } //等待付款
                elseif ($return["status"] == 2) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no
                        ]
                    ]);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);

                }//订单退款
                elseif ($return["status"] == 4) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单已经退款',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);

                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                }
            }

            //海科融通 交易查询
            if (21999 < $ways_type && $ways_type < 22999) {
                //读取配置
                $config = new HkrtConfigController();
                $hkrt_config = $config->hkrt_config($config_id);
                if (!$hkrt_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '海科融通配置不存在请检查配置'
                    ]);
                }

                $hkrt_merchant = $config->hkrt_merchant($store_id, $store_pid);
                if (!$hkrt_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '海科融通商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Hkrt\PayController();
                $hkrt_data = [
                    'access_id' => $hkrt_config->access_id,
                    'trade_no' => $order->trade_no,
                    'out_trade_no' => $order->out_trade_no,
                    'access_key' => $hkrt_config->access_key
                ];
                Log::info('海科融通-商户-交易查询');
                Log::info($hkrt_data);
                $return = $obj->order_query($hkrt_data); //0-系统错误 1-交易成功；2-交易失败；3-交易进行中；4-交易超时
                Log::info($return);

                //支付成功
                if ($return['status'] == '1') {
                    $pay_time = $return['data']['trade_end_time']; //SaaS平台交易完成时间
                    $trade_no = $return['data']['trade_no']; //SaaS平台的交易订单编号
                    $buyer_pay_amount = $return['data']['total_amount']; //订单金额,以元为单位
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '22000', //返佣来源
                            'source_desc' => '海科融通', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $trade_no
                        ]
                    ]);
                } //交易进行中
                elseif ($return['status'] == 3) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                } //交易失败
                elseif ($return['status'] == 2) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                }//交易超时
                elseif ($return['status'] == 4) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '交易超时',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                }
            }

            //易生支付 交易查询
            if (20999 < $ways_type && $ways_type < 21999) {
                //读取配置
                $easyPayStoresImages = EasypayStoresImages::where('store_id', $store_id)->select('new_config_id')->first();
                if (!$easyPayStoresImages) {
                    $easyPayStoresImages = EasypayStoresImages::where('store_id', $store_pid)->select('new_config_id')->first();
                }
                $config = new EasyPayConfigController();
                $easypay_config = $config->easypay_config($easyPayStoresImages->new_config_id);
                if (!$easypay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生支付配置不存在请检查配置'
                    ]);
                }

                $easypay_merchant = $config->easypay_merchant($store_id, $store_pid);
                if (!$easypay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生支付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\EasyPay\PayController();
                $easypay_data = [
                    'channel_id' => $easypay_config->channel_id, //渠道编号
                    'mer_id' => $easypay_merchant->term_mercode, //终端商戶编号
                    'device_id' => $easypay_merchant->term_termcode, //终端编号
                    'out_trade_no' => $order->out_trade_no, //原交易流水
                    'key' => $easypay_config->channel_key
                ];
//                Log::info('易生支付-商户-交易查询');
//                Log::info($easypay_data);
                $return = $obj->order_query($easypay_data); //-1 系统错误 0-其他 1-成功 2-下单失败 3-订单未支付
//                Log::info($return);

                //支付成功
                if ($return['status'] == '1') {
                    //1.0
//                    $pay_time = $return['data']['wxtimeend'] ? date('Y-m-d H:i:s', strtotime($return['data']['wxtimeend'])): ''; //支付完成时间，如2009年12月27日9点10分10秒表示为20091227091010
//                    $trade_no = $return['data']['wtorderid']; //系统订单号
//                    $buyer_logon_id = $return['data']['wxopenid'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
//                    $buyer_pay_amount = $return['data']['payamt'] ? ($return['data']['payamt']/100): ''; //实付金额，单位分
//                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                    //2.0
                    // $pay_time = $return['data']['timeEnd'] ? date('Y-m-d H:i:s', strtotime($return['data']['timeEnd'])) : ''; //支付完成时间，如2009年12月27日9点10分10秒表示为20091227091010
                    $pay_time = (isset($return['data']['timeEnd']) && !empty($return['data']['timeEnd'])) ? date('Y-m-d H:i:m', strtotime($return['data']['timeEnd'])) : date('Y-m-d H:i:m', time()); //支付完成时间，格式为 yyyyMMddhhmmss，如 2009年12月27日9点10分10秒表示为 20091227091010
                    $trade_no = $return['data']['outTrace']; //系统订单号
                    $buyer_logon_id = $return['data']['payerId'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
                    $buyer_pay_amount = $return['data']['payerAmt'] ? ($return['data']['payerAmt'] / 100) : ''; //实付金额，单位分
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'receipt_amount' => $buyer_pay_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '21000', //返佣来源
                            'source_desc' => '易生支付', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => '1',
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $trade_no
                        ]
                    ]);
                } elseif ($return['status'] == '3') {
                    return json_encode([
                        'status' => '2',
                        'pay_status' => '3',
                        'message' => '支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                } elseif ($return['status'] == '2') {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '订单未支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } //其他情况
                else {
                    return json_encode([
                        'status' => 2,
                        'pay_status' => 3,
                        'message' => $return['message'],
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                }
            }

            //邮驿付支付 交易查询
            if (29000 < $ways_type && $ways_type < 29010) {
                //读取配置
                $config = new PostPayConfigController();
                $post_config = $config->post_pay_config($config_id);
                if (!$post_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '邮驿付支付配置不存在请检查配置'
                    ]);
                }

                $post_merchant = $config->post_pay_merchant($store_id, $store_pid);
                if (!$post_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '邮驿付支付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\PostPay\PayController();
                $post_data = [
                    'out_trade_no' => $order->out_trade_no,//原交易流水 //渠道编号
                    'custLogin' => $store->people_phone, //商户手机号
                    'custId' => $post_merchant->cust_id, //终端编号
                    'agetId' => $post_config->org_id
                ];
                $return = $obj->order_query($post_data);

                //支付成功
                if ($return['status'] == '1') {

                    $pay_time = $return['data']['orderTime'] ? date('Y-m-d H:i:s', strtotime($return['data']['orderTime'])) : ''; //支付完成时间，如2009年12月27日9点10分10秒表示为20091227091010
                    $trade_no = $return['data']['orderNo']; //系统订单号
                    $buyer_logon_id = $return['data']['openId'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
                    $buyer_pay_amount = $return['data']['txamt'] ? ($return['data']['txamt'] / 100) : ''; //实付金额，单位分
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'receipt_amount' => $buyer_pay_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '29000', //返佣来源
                            'source_desc' => '邮驿付支付', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => '1',
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $trade_no
                        ]
                    ]);
                } elseif ($return['status'] == '3') {
                    return json_encode([
                        'status' => '2',
                        'pay_status' => '3',
                        'message' => '用户交易撤销',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                } elseif ($return['status'] == '2') {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '订单未支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } //其他情况
                else {
                    return json_encode([
                        'status' => 2,
                        'pay_status' => 3,
                        'message' => $return['message'],
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                }
            }

            //邮政收银支付
            if (26000 < $ways_type && $ways_type < 26999) {
                $config = new LianfuyoupayConfigController();
                $h_merchant = $config->lianfu_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '邮政支付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\lianfuyouzheng\PayController();
                $data['apikey'] = $h_merchant->apikey; //
                $data['signkey'] = $h_merchant->signkey; //
                $data['out_trade_no'] = $out_trade_no; //
                $data['pos_sn'] = $h_merchant->pos_sn; //

                $return = $obj->order_query($data);

                //支付成功
                if ($return["status"] == 1) {
                    $pay_time = date('Y-m-d H:i:s', time());
                    $trade_no = isset($return['data']['tp_order_id']) ? $return['data']['tp_order_id'] : $out_trade_no;
                    $buyer_pay_amount = $order->total_amount;
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '26000', //返佣来源
                            'source_desc' => 'lianfuyoupay', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : ""
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $trade_no
                        ]
                    ]);
                } //等待付款
                elseif ($return["status"] == 2) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no
                        ]
                    ]);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);

                }//订单退款
                elseif ($return["status"] == 4) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单已经退款',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                }
            }

            //苏州收银支付
            if (17000 < $ways_type && $ways_type < 17999) {
                //读取配置
                $config = new SuzhouConfigController();
                $h_merchant = $config->sz_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '苏州银行商户号不存在'
                    ]);
                }

                $h_config = $config->sz_config($config_id);
                if (!$h_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '苏州银行支付配置不存在请检查配置'
                    ]);
                }

                $obj = new \App\Api\Controllers\Suzhou\PayController();
                $data['certId'] = $h_config->certId; //
                $data['pri_key'] = $h_config->rsa_pr; //
                $data['merchantId'] = $h_merchant->MerchantId; //
                $data['out_trade_no'] = $out_trade_no; //
                $data['orderTime'] = date('YmdHis', time()); //
                $return = $obj->order_query($data);

                //支付成功
                if ($return["status"] == 1) {
                    $trade_no = $return['data']['tradeNo'];
                    $user_info = '';
                    $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['payTime']));
                    $buyer_pay_amount = $order->total_amount;
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            // 'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '17000',//返佣来源
                            'source_desc' => 'SZ',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : ""
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $trade_no
                        ]
                    ]);
                } //等待付款
                elseif ($return["status"] == 2) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no
                        ]
                    ]);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);

                }//订单退款
                elseif ($return["status"] == 4) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单已经退款',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                }
            }

            //葫芦天下
            if (22999 < $ways_type && $ways_type < 23999) {
                $manager = new \App\Api\Controllers\Hltx\ManageController();
                $hltx_merchant = $manager->pay_merchant($store_id, $store_pid);
                $qd = $hltx_merchant->qd;
                $hltx_config = $manager->pay_config($config_id, $qd);
                if (!$hltx_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => 'HL支付配置不存在请检查配置'
                    ]);
                }

                $manager->init($hltx_config);
                switch ($ways_type) {
                    case 23001:
                        $pay_channel = 'ALI';
                        break;
                    case 23002:
                        $pay_channel = 'WX';
                        break;
                    case 23004:
                        $pay_channel = 'UPAY';
                        break;
                    default:
                        $pay_channel = 'ALI';
                        break;
                }

                if (!$hltx_merchant || empty($hltx_merchant->mer_no)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户未成功开通HL通道!'
                    ]);
                }

                $hltx_data = [
                    'merNo' => $hltx_merchant->mer_no,
                    'orderNo' => $order->out_trade_no,
                ];
                $return = $manager->query_order($hltx_data);
                if ($return['status'] == 1) {
                    $return = $return['data'];
                    if ($return['tradeStatus'] == "S") {
                        if ($return['subTradeStatus'] == 'COMPLETE') {
                            $pay_time = $return['payCompleteTime'];
                            $trade_no = $return['transIndex'];
                            $buyer_pay_amount = $return['amount'] / 100;
                            //改变数据库状态
                            if ($order->pay_status != 1) {
                                Cache::forget($c_o);

                                $insert_data = [
                                    'status' => '1',
                                    'pay_status' => 1,
                                    'pay_status_desc' => '支付成功',
                                    'buyer_logon_id' => '',
                                    'trade_no' => $trade_no,
                                    'pay_time' => $pay_time,
                                    'buyer_pay_amount' => $buyer_pay_amount,
                                ];
                                $this->update_day_order($insert_data, $out_trade_no, $table);

                                //支付成功后的动作
                                $data = [
                                    'ways_type' => $order->ways_type,
                                    'ways_type_desc' => $order->ways_type_desc,
                                    'source_type' => '23000', //返佣来源
                                    'source_desc' => 'HL', //返佣来源说明
                                    'company' => 'hltx', //返佣来源说明
                                    'total_amount' => $order->total_amount,
                                    'out_trade_no' => $order->out_trade_no,
                                    'rate' => $order->rate,
                                    'merchant_id' => $order->merchant_id,
                                    'store_id' => $order->store_id,
                                    'user_id' => $order->user_id,
                                    'config_id' => $config_id,
                                    'store_name' => $order->store_name,
                                    'ways_source' => $order->ways_source,
                                    'pay_time' => $pay_time,
                                    'device_id' => isset($order->device_id) ? $order->device_id : ""
                                ];
                                PaySuccessAction::action($data);
                            }

                            $return_data = [
                                'status' => 1,
                                'pay_status' => '1',
                                'message' => '支付成功',
                                'data' => [
                                    'ways_source' => $order->ways_source,
                                    'pay_time' => $pay_time,
                                    'out_trade_no' => $order->out_trade_no,
                                    'pay_amount' => $buyer_pay_amount,
                                    'total_amount' => $order->total_amount,
                                    'other_no' => $other_no,
                                    'trade_no' => $trade_no
                                ]
                            ];
                        } elseif (in_array($return['subTradeStatus'], ['FAILED', 'SYS_FAILED', 'CANCEL', 'CLOSE'])) {
                            $return_data = [
                                'status' => 1,
                                'pay_status' => '3',
                                'message' => '订单支付失败',
                                'data' => [
                                    'out_trade_no' => $order->out_trade_no,
                                    'pay_amount' => $order->pay_amount,
                                    'total_amount' => $order->total_amount
                                ],
                            ];
                        } elseif ($return['subTradeStatus'] == 'PAYING') {
                            $return_data = [
                                'status' => 1,
                                'pay_status' => '2',
                                'message' => '等待支付',
                                'data' => [
                                    'out_trade_no' => $order->out_trade_no,
                                    'pay_amount' => $order->total_amount,
                                    'total_amount' => $order->total_amount
                                ]
                            ];
                        }
                    } elseif ($return['tradeStatus'] == "E") {
                        $return_data = [
                            'status' => 1,
                            'pay_status' => '3',
                            'message' => '订单支付失败',
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount
                            ],
                        ];
                    } else {
                        $return_data = [
                            'status' => 1,
                            'pay_status' => '2',
                            'message' => '等待支付',
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->total_amount,
                                'total_amount' => $order->total_amount
                            ]
                        ];
                    }
                } else {
                    //其他情况
                    $message = $return['message'];
                    $return_data = [
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ];
                }

                return json_encode($return_data);
            }

            //长沙收银支付
            if (25000 < $ways_type && $ways_type < 25999) {
                $config = new ChangshaConfigController();
                $h_merchant = $config->cs_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '长沙银行商户号不存在'
                    ]);
                }

                $h_config = $config->cs_config($data['config_id']);
                if (!$h_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '长沙银行支付配置不存在请检查配置'
                    ]);
                }

                $obj = new \App\Api\Controllers\Changsha\PayController();
                $data['ECustId'] = $h_merchant->ECustId; //
                $data['out_trade_no'] = $out_trade_no; //
                $return = $obj->order_query($data);

                //支付成功
                if ($return["status"] == 1) {
                    $trade_no = $return['data']['OrderId'];
                    $user_info = '';
                    $pay_time = date('Y-m-d H:i:s', time());
                    $buyer_pay_amount = $order->total_amount;

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            // 'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '25000',//返佣来源
                            'source_desc' => 'changsha',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : ""
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $trade_no
                        ]
                    ]);
                } //等待付款
                elseif ($return["status"] == 2) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no
                        ]
                    ]);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                }//订单退款
                elseif ($return["status"] == 4) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单已经退款',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                }
            }

            //联动优势 交易查询
            if (4999 < $ways_type && $ways_type < 5999) {
                $config = new LinkageConfigController();
                $linkage_config = $config->linkage_config($config_id);
                if (!$linkage_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '联动优势支付配置不存在请检查配置'
                    ]);
                }

                $linkage_merchant = $config->linkage_merchant($store_id, $store_pid);
                if (!$linkage_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '联动优势商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Linkage\PayController();
                $linkage_data = [
                    'acqSpId' => $linkage_config->mch_id, //代理商编号
                    'privateKey' => $linkage_config->privateKey, //
                    'publicKey' => $linkage_config->publicKey, //
                    'acqMerId' => $linkage_merchant->acqMerId, //商户号
                    'trade_no' => $order->trade_no, //联动优势的订单号，建议优先使用
                    'out_trade_no' => $out_trade_no //商户订单号
                ];
//                Log::info('联动优势-商户-交易查询');
//                Log::info($linkage_data);
                $return = $obj->order_query($linkage_data); //-1系统错误；0-其他；1-交易成功；2-验签失败；3-转入退款；4-交易结果未明；5-已关闭；6-已撤销；7-支付中
                Log::info($return);

                //支付成功
                if ($return['status'] == '1') {
                    $pay_time = $return['data']['payTime'] ? date('Y-m-d H:i:s', strtotime(substr($return['data']['platDate'], 0, 4) . $return['data']['payTime'])) : ''; //交易时间 格式：MMDDhhmmss
                    $trade_no = $return['data']['transactionId']; //联动优势的流水号
                    $buyer_pay_amount = $return['data']['txnAmt'] ? ($return['data']['txnAmt'] / 100) : ''; //订单金额 (打印小票使用)
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                    $paySeq = $return['data']['paySeq'] ?? ''; //支付流水号（条形码），成功返回
                    $depBankSeq = $return['data']['depBankSeq'] ?? ''; //第三方流水号（微信/支付宝/银联的交易流水号）

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'auth_code' => $paySeq,
                            'other_no' => $depBankSeq
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '5000', //返佣来源
                            'source_desc' => '联动优势', //返佣来源说明
                            'total_amount' => $buyer_pay_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => '1',
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $trade_no
                        ]
                    ]);
                } elseif ($return['status'] == '2') {
                    return json_encode([
                        'status' => '2',
                        'pay_status' => '3',
                        'message' => '支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } else {
                    return json_encode([
                        'status' => '1',
                        'pay_status' => 3,
                        'message' => $return['message'],
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                }
            }

            //威富通 交易查询
            if (26999 < $ways_type && $ways_type < 27999) {
                $config = new WftPayConfigController();
                $wftpay_config = $config->wftpay_config($config_id);
                if (!$wftpay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '威富通支付配置不存在请检查配置'
                    ]);
                }

                $wftpay_merchant = $config->wftpay_merchant($store_id, $store_pid);
                if (!$wftpay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '威富通商户号不存在'
                    ]);
                }

                //支付状态为 退款中、已退款、有退款，退款查询接口
                $obj = new WftPayPayController();
                $wftpay_data = [
                    'mch_id' => $wftpay_merchant->mch_id,
                    'out_trade_no' => $out_trade_no,
                    'private_rsa_key' => $wftpay_config->private_rsa_key,
                    'public_rsa_key' => $wftpay_config->public_rsa_key
                ];
                if ($order->pay_status == 5 || $order->pay_status == 6 || $order->pay_status == 7) {
//                    Log::info('威富通-退款查询-入参');
//                    Log::info($wftpay_data);
                    $return = $obj->refund_query($wftpay_data); //0-系统错误 1-成功 2-退款失败 3-退款处理中 4-退款到银行发现用户的卡作废或者冻结了,导致原路退款银行卡失败
                    Log::info('威富通-退款查询');
                    Log::info($return);

                    //支付成功
                    if ($return['status'] == 1) {
                        $refund_count = $return['data']['refund_count'] - 1; //退款记录数
                        $refund_no = $return['data']['out_refund_id_' . $refund_count] ?? $return['data']['out_refund_no_' . $refund_count]; //第三方退款单号or平台退款单号
                        $pay_time = isset($return['data']['refund_time_' . $refund_count]) ? date('Y-m-d H:i:s', strtotime($return['data']['refund_time_' . $refund_count])) : ''; //退款时间,yyyyMMddHHmmss
                        $refund_amount = isset($return['data']['refund_fee_' . $refund_count]) ? ($return['data']['refund_fee_' . $refund_count] / 100) : $order->total_amount; //退款总金额,单位为分,可以做部分退款
                        $refund_amount = number_format($refund_amount, 2, '.', '');
                        $trade_no = $return['data']['out_transaction_id']; //第三方订单号

                        //改变数据库状态
                        if ($order->pay_status != 6) {
                            Cache::forget($c_o);

                            $insert_data = [
                                'status' => '6',
                                'pay_status' => 6,
                                'pay_status_desc' => '退款成功',
                                'refund_no' => $refund_no,
                                'refund_amount' => $refund_amount
                            ];
                            if ($pay_time) $insert_data['pay_time'] = $pay_time;
                            $this->update_day_order($insert_data, $out_trade_no, $table);

                            //退款成功后的动作
                            $return_data = [
                                'out_trade_no' => $out_trade_no,
                                'trade_no' => $trade_no,
                                'store_id' => $order->store_id,
                                'merchant_id' => $order->merchant_id,
                                'type' => $order->ways_type,
                                'ways_source' => $order->ways_source,
                                'refund_amount' => $refund_amount,
                                'refund_no' => $refund_no
                            ];
                            RefundOrder::created($return_data);
                        }

                        return json_encode([
                            'status' => '1',
                            'pay_status' => 6,
                            'message' => '退款成功',
                            'data' => [
                                'ways_source' => $order->ways_source,
                                'pay_time' => $pay_time,
                                'out_trade_no' => $order->out_trade_no,
                                'refund_amount' => $refund_amount,
                                'total_amount' => $order->total_amount,
                                'other_no' => $other_no,
                                'trade_no' => $trade_no
                            ]
                        ]);
                    } elseif ($return['status'] == 2) {
                        return json_encode([
                            'status' => '2',
                            'pay_status' => '3',
                            'message' => '支付失败',
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'pay_status' => 3,
                            'message' => $return['message'],
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount
                            ]
                        ]);
                    }
                } else {
                    Log::info('威富通-交易查询');
                    Log::info($wftpay_data);
                    $return = $obj->order_query($wftpay_data); //0-系统错误 1-成功 2-失败 3-转入退款 4-未支付 5-已关闭 6-已冲正 7-已撤销 8-用户支付中
                    Log::info($return);

                    //支付成功
                    if ($return['status'] == 1) {
                        $pay_time = $return['data']['time_end'] ? date('Y-m-d H:i:s', strtotime($return['data']['time_end'])) : ''; //支付完成时间，格式为yyyyMMddHHmmss，如2009年12月27日9点10分10秒表示为20091227091010
                        $trade_no = $return['data']['transaction_id']; //平台交易号
                        $buyer_pay_amount = $return['data']['total_fee'] ? ($return['data']['total_fee'] / 100) : ''; //总金额，以分为单位，不允许包含任何字、符号
                        $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                        $depBankSeq = $return['data']['out_transaction_id'] ?? ''; //第三方交易号

                        //改变数据库状态
                        if ($order->pay_status != 1) {
                            Cache::forget($c_o);

                            $insert_data = [
                                'status' => '1',
                                'pay_status' => 1,
                                'pay_status_desc' => '支付成功',
                                'buyer_logon_id' => '',
                                'trade_no' => $trade_no,
                                'pay_time' => $pay_time,
                                'buyer_pay_amount' => $buyer_pay_amount,
                                'other_no' => $depBankSeq
                            ];
                            $this->update_day_order($insert_data, $out_trade_no, $table);

                            //支付成功后的动作
                            $data = [
                                'ways_type' => $order->ways_type,
                                'company' => $order->company,
                                'ways_type_desc' => $order->ways_type_desc,
                                'source_type' => '27000', //返佣来源
                                'source_desc' => '威富通', //返佣来源说明
                                'total_amount' => $buyer_pay_amount,
                                'out_trade_no' => $order->out_trade_no,
                                'rate' => $order->rate,
                                'fee_amount' => $order->fee_amount,
                                'merchant_id' => $order->merchant_id,
                                'store_id' => $order->store_id,
                                'user_id' => $order->user_id,
                                'config_id' => $config_id,
                                'store_name' => $order->store_name,
                                'ways_source' => $order->ways_source,
                                'pay_time' => $pay_time,
                                'device_id' => isset($order->device_id) ? $order->device_id : "",
                            ];
                            PaySuccessAction::action($data);
                        }

                        return json_encode([
                            'status' => '1',
                            'pay_status' => 1,
                            'message' => '支付成功',
                            'data' => [
                                'ways_source' => $order->ways_source,
                                'pay_time' => $pay_time,
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $buyer_pay_amount,
                                'total_amount' => $order->total_amount,
                                'other_no' => $other_no,
                                'trade_no' => $trade_no
                            ]
                        ]);
                    } elseif ($return['status'] == 2) {
                        return json_encode([
                            'status' => '2',
                            'pay_status' => '3',
                            'message' => '支付失败',
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount
                            ]
                        ]);
                    } elseif ($return['status'] == 8) {
                        return json_encode([
                            'status' => '2',
                            'pay_status' => '2', //1-成功 2-支付中
                            'message' => '用户支付中',
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'pay_status' => 3,
                            'message' => $return['message'],
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount
                            ]
                        ]);
                    }
                }

            }

            //汇旺财 交易查询
            if (27999 < $ways_type && $ways_type < 28999) {
                $config = new HwcPayConfigController();
                $hwcpay_config = $config->hwcpay_config($config_id);
                if (!$hwcpay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '汇旺财支付配置不存在请检查配置'
                    ]);
                }

                $hwcpay_merchant = $config->hwcpay_merchant($store_id, $store_pid);
                if (!$hwcpay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '汇旺财商户号不存在'
                    ]);
                }

                //支付状态为 退款中、已退款、有退款，退款查询接口
                $obj = new HwcPayPayController();
                $hwcpay_data = [
                    'mch_id' => $hwcpay_merchant->mch_id,
                    'out_trade_no' => $out_trade_no,
                    'private_rsa_key' => $hwcpay_config->private_rsa_key,
                    'public_rsa_key' => $hwcpay_config->public_rsa_key
                ];
                if ($order->pay_status == 5 || $order->pay_status == 6 || $order->pay_status == 7) {
//                    Log::info('汇旺财-退款查询-入参');
//                    Log::info($hwcpay_data);
                    $return = $obj->refund_query($hwcpay_data); //0-系统错误 1-成功 2-退款失败 3-退款处理中 4-退款到银行发现用户的卡作废或者冻结了,导致原路退款银行卡失败
//                    Log::info('汇旺财-退款查询');
//                    Log::info($return);

                    //支付成功
                    if ($return['status'] == 1) {
                        $refund_count = $return['data']['refund_count'] - 1; //退款记录数
                        $refund_no = $return['data']['out_refund_id_' . $refund_count] ?? $return['data']['out_refund_no_' . $refund_count]; //第三方退款单号or平台退款单号
                        $pay_time = isset($return['data']['refund_time_' . $refund_count]) ? date('Y-m-d H:i:s', strtotime($return['data']['refund_time_' . $refund_count])) : ''; //退款时间,yyyyMMddHHmmss
                        $refund_amount = $return['data']['refund_fee_' . $refund_count] ? ($return['data']['refund_fee_' . $refund_count] / 100) : ''; //退款总金额,单位为分,可以做部分退款
                        $refund_amount = number_format($refund_amount, 2, '.', '');
                        $trade_no = $return['data']['out_transaction_id'] ?? $return['data']['refund_id_' . $refund_count]; //第三方订单号or平台退款单号

                        //改变数据库状态
                        if ($order->pay_status != 6) {
                            Cache::forget($c_o);

                            $insert_data = [
                                'status' => '6',
                                'pay_status' => 6,
                                'pay_status_desc' => '退款成功',
                                'refund_no' => $refund_no,
                                'refund_amount' => $refund_amount
                            ];
                            if ($pay_time) $insert_data['pay_time'] = $pay_time;
                            $this->update_day_order($insert_data, $out_trade_no, $table);

                            //退款成功后的动作
                            $return_data = [
                                'out_trade_no' => $out_trade_no,
                                'trade_no' => $trade_no,
                                'store_id' => $order->store_id,
                                'merchant_id' => $order->merchant_id,
                                'type' => $order->ways_type,
                                'ways_source' => $order->ways_source,
                                'refund_amount' => $refund_amount,
                                'refund_no' => $refund_no
                            ];
                            RefundOrder::created($return_data);
                        }

                        return json_encode([
                            'status' => '1',
                            'pay_status' => 6,
                            'message' => '退款成功',
                            'data' => [
                                'ways_source' => $order->ways_source,
                                'pay_time' => $pay_time,
                                'out_trade_no' => $order->out_trade_no,
                                'refund_amount' => $refund_amount,
                                'total_amount' => $order->total_amount,
                                'other_no' => $other_no,
                                'trade_no' => $trade_no
                            ]
                        ]);
                    } elseif ($return['status'] == 2) {
                        return json_encode([
                            'status' => '2',
                            'pay_status' => '3',
                            'message' => '支付失败',
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'pay_status' => 3,
                            'message' => $return['message'],
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount
                            ]
                        ]);
                    }
                } else {
//                    Log::info('汇旺财-交易查询-入参');
//                    Log::info($hwcpay_data);
                    $return = $obj->order_query($hwcpay_data); //0-系统错误 1-成功 2-失败 3-转入退款 4-未支付 5-已关闭 6-已冲正 7-已撤销 8-用户支付中
//                    Log::info('汇旺财-交易查询');
//                    Log::info($return);

                    //支付成功
                    if ($return['status'] == 1) {
                        $pay_time = $return['data']['time_end'] ? date('Y-m-d H:i:s', strtotime($return['data']['time_end'])) : ''; //支付完成时间，格式为yyyyMMddHHmmss，如2009年12月27日9点10分10秒表示为20091227091010
                        $trade_no = isset($return['data']['third_order_no']) ? $return['data']['third_order_no'] : $return['data']['transaction_id']; //平台交易号
                        $buyer_pay_amount = $return['data']['total_fee'] ? ($return['data']['total_fee'] / 100) : ''; //总金额，以分为单位，不允许包含任何字、符号
                        $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                        $depBankSeq = $return['data']['out_transaction_id'] ?? ''; //第三方交易号

                        //改变数据库状态
                        if ($order->pay_status != 1) {
                            Cache::forget($c_o);

                            $insert_data = [
                                'status' => '1',
                                'pay_status' => 1,
                                'pay_status_desc' => '支付成功',
                                'buyer_logon_id' => '',
                                'trade_no' => $trade_no,
                                'pay_time' => $pay_time,
                                'buyer_pay_amount' => $buyer_pay_amount,
                                'other_no' => $depBankSeq
                            ];
                            $this->update_day_order($insert_data, $out_trade_no, $table);

                            //支付成功后的动作
                            $data = [
                                'ways_type' => $order->ways_type,
                                'company' => $order->company,
                                'ways_type_desc' => $order->ways_type_desc,
                                'source_type' => '28000', //返佣来源
                                'source_desc' => '汇旺财', //返佣来源说明
                                'total_amount' => $buyer_pay_amount,
                                'out_trade_no' => $order->out_trade_no,
                                'rate' => $order->rate,
                                'fee_amount' => $order->fee_amount,
                                'merchant_id' => $order->merchant_id,
                                'store_id' => $order->store_id,
                                'user_id' => $order->user_id,
                                'config_id' => $config_id,
                                'store_name' => $order->store_name,
                                'ways_source' => $order->ways_source,
                                'pay_time' => $pay_time,
                                'device_id' => isset($order->device_id) ? $order->device_id : "",
                            ];
                            PaySuccessAction::action($data);
                        }

                        return json_encode([
                            'status' => '1',
                            'pay_status' => 1,
                            'message' => '支付成功',
                            'data' => [
                                'ways_source' => $order->ways_source,
                                'pay_time' => $pay_time,
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $buyer_pay_amount,
                                'total_amount' => $order->total_amount,
                                'other_no' => $other_no,
                                'trade_no' => $trade_no
                            ]
                        ]);
                    } elseif ($return['status'] == 2) {
                        return json_encode([
                            'status' => '2',
                            'pay_status' => '3',
                            'message' => '支付失败',
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount
                            ]
                        ]);
                    } elseif ($return['status'] == 8) {
                        return json_encode([
                            'status' => '2',
                            'pay_status' => '2', //1-成功 2-支付中
                            'message' => '用户支付中',
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'pay_status' => 3,
                            'message' => $return['message'],
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount
                            ]
                        ]);
                    }
                }

            }

            //钱方 交易查询
            if (23999 < $ways_type && $ways_type < 24999) {
                $config = new QfPayConfigController();
                $qfpay_config = $config->qfpay_config($config_id);
                if (!$qfpay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '钱方支付配置不存在请检查配置'
                    ]);
                }

                $qfpay_merchant = $config->qfpay_merchant($store_id, $store_pid);
                if (!$qfpay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '钱方商户号不存在'
                    ]);
                }

                //支付状态为 退款中、已退款、有退款，退款查询接口
                $obj = new QfPayController();
                $qfpay_data = [
                    'mchid' => $qfpay_merchant->mchid,  //商户号
                    'key' => $qfpay_config->key,  //加签key
                    'code' => $qfpay_config->code,  //钱方唯一标识
                    'out_trade_no' => $out_trade_no,  //否,外部订单号查询,开发者平台订单号
                    'syssn' => $order->trade_no  //否,钱方订单号查询,多个以英文逗号区分开
                ];
                Log::info('钱方-交易查询-入参');
                Log::info($qfpay_data);
                $return = $obj->query($qfpay_data); //1-成功 2-请求下单成功 3-交易中
                Log::info('钱方-交易查询-结果');
                Log::info($return);

                //支付成功
                if ($return['status'] == 1) {
                    $trade_no = isset($return['data']['syssn']) ? $return['data']['syssn'] : ''; //钱方订单号
//                    $out_trade_no = isset($return['data']['out_trade_no']) ? $return['data']['out_trade_no'] : ''; //外部订单号，开发者平台订单号
                    $pay_type = isset($return['data']['pay_type']) ? $return['data']['pay_type'] : ''; //支付类型,多个以英文逗号区分开,支付宝扫码:800101；支付宝反扫:800108；支付宝服务窗：800107；微信扫码:800201；微信刷卡:800208；微信公众号支付:800207
                    $order_type = isset($return['data']['order_type']) ? $return['data']['order_type'] : ''; //订单类型:支付的订单：payment；退款的订单：refund；关闭的订单：close
                    $txdtm = isset($return['data']['txdtm']) ? $return['data']['txdtm'] : ''; //请求交易时间 格式为：YYYY-MM-DD HH:MM:SS
                    $txamt = isset($return['data']['txamt']) ? $return['data']['txamt'] : 0; //订单支付金额，单位分
                    $sysdtm = isset($return['data']['sysdtm']) ? $return['data']['sysdtm'] : $txdtm; //系统交易时间
                    $cancel = isset($return['data']['cancel']) ? $return['data']['cancel'] : ''; //撤销/退款标记 正常交易：0；已撤销：2；已退货：3
                    $respcd = isset($return['data']['respcd']) ? $return['data']['respcd'] : ''; //支付结果返回码 0000表示交易支付成功；1143、1145表示交易中，需继续查询交易结果； 其他返回码表示失败
                    $errmsg = isset($return['data']['errmsg']) ? $return['data']['errmsg'] : ''; //支付结果描述
                    $cardtp = isset($return['data']['cardtp']) ? $return['data']['cardtp'] : ''; //卡类型 未识别卡=0,借记卡=1,信用卡(贷记卡)=2,准贷记卡=3,储值卡= 4,第三方帐号=5

                    $buyer_pay_amount = number_format($txamt / 100, 2, '.', '');

                    if ($respcd == 0000) {
                        //正交易 改变数据库状态
                        if ($order->pay_status != 1 && $trade_no && $order_type == 'payment') {
                            Cache::forget($c_o);

                            $insert_data = [
                                'status' => '1',
                                'pay_status' => 1,
                                'pay_status_desc' => '支付成功',
                                'buyer_logon_id' => '',
                                'trade_no' => $trade_no,
                                'pay_time' => $sysdtm,
                                'buyer_pay_amount' => $buyer_pay_amount,
                                'receipt_amount' => $buyer_pay_amount //商家实际收款金额
                            ];
                            $this->update_day_order($insert_data, $out_trade_no, $table);

                            //支付成功后的动作
                            $data = [
                                'ways_type' => $order->ways_type,
                                'company' => $order->company,
                                'ways_type_desc' => $order->ways_type_desc,
                                'source_type' => '24000', //返佣来源
                                'source_desc' => '钱方', //返佣来源说明
                                'total_amount' => $buyer_pay_amount,
                                'out_trade_no' => $order->out_trade_no,
                                'rate' => $order->rate,
                                'fee_amount' => $order->fee_amount,
                                'merchant_id' => $order->merchant_id,
                                'store_id' => $order->store_id,
                                'user_id' => $order->user_id,
                                'config_id' => $config_id,
                                'store_name' => $order->store_name,
                                'ways_source' => $order->ways_source,
                                'pay_time' => $sysdtm,
                                'device_id' => isset($order->device_id) ? $order->device_id : "",
                            ];
                            PaySuccessAction::action($data);
                        }

                        return json_encode([
                            'status' => '1',
                            'pay_status' => 1,
                            'message' => '支付成功',
                            'data' => [
                                'ways_source' => $order->ways_source,
                                'pay_time' => $sysdtm,
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $buyer_pay_amount,
                                'total_amount' => $order->total_amount,
                                'other_no' => $other_no,
                                'trade_no' => $trade_no
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'pay_status' => 1,
                            'message' => '请求下单成功',
                            'data' => [
                                'ways_source' => $order->ways_source,
                                'pay_time' => $sysdtm,
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $buyer_pay_amount,
                                'total_amount' => $order->total_amount,
                                'other_no' => $other_no,
                                'trade_no' => $trade_no
                            ]
                        ]);
                    }
                } elseif ($return['status'] == 3) {
                    return json_encode([
                        'status' => 1, //设备显示支付状态:1-成功;2-失败
                        'pay_status' => 2,
                        'message' => '正在支付中',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } else {
                    return json_encode([
                        'status' => '3',
                        'pay_status' => 3,
                        'message' => $return['message'],
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                }

            }

            //建设银行 交易查询
            if (31000 < $ways_type && $ways_type < 31010) {
                $order = Order::where('out_trade_no', $out_trade_no)->first();

                if ($order->pay_status == "1") {
                    return json_encode([
                        'status' => '1',
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $order->pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);
                } elseif ($order->pay_status == "3") {
                    return json_encode([
                        'status' => '2',
                        'pay_status' => '3',
                        'message' => '用户交易撤销',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                } elseif ($order->pay_status == "2") {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '订单未支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } //其他情况
                else {
                    return json_encode([
                        'status' => '2',
                        'pay_status' => '3',
                        'message' => $return['message'],
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                }
            }

            //易生数科支付 交易查询
            if (32000 < $ways_type && $ways_type < 32010) {
                //读取配置
                $config = new EasySkPayConfigController();
                $easyskpay_config = $config->easyskpay_config($config_id);
                if (!$easyskpay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生数科支付配置不存在请检查配置'
                    ]);
                }

                $easyskpay_merchant = $config->easyskpay_merchant($store_id, $store_pid);
                if (!$easyskpay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生数科支付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\EasySkPay\PayController();
                $easyskpay_data = [
                    'org_id' => $easyskpay_config->org_id, //渠道编号
                    'mer_id' => $easyskpay_merchant->mer_id, //终端商戶编号
                    'orig_request_no' => $order->out_trade_no //原交易流水
                ];
//                Log::info('易生数科支付-商户-交易查询');
//                Log::info($easyskpay_data);
                $return = $obj->order_query($easyskpay_data); //-1 系统错误 0-其他 1-成功 2-下单失败 3-订单未支付
//                Log::info($return);

                //支付成功
                if ($return['status'] == '1') {
                    $trade_no = $return['data']['tradeNo']; //系统订单号
                    $pay_time = (isset($return['data']['bizData']['payTime']) && !empty($return['data']['bizData']['payTime'])) ? date('Y-m-d H:i:m', strtotime($return['data']['bizData']['payTime'])) : date('Y-m-d H:i:m', time());
                    $buyer_pay_amount = isset($return['data']['bizData']['amount']) ? ($return['data']['bizData']['amount'] / 100) : ''; //实付金额，单位分
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'receipt_amount' => $buyer_pay_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '32000', //返佣来源
                            'source_desc' => '易生数科', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => '1',
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $trade_no
                        ]
                    ]);
                } elseif ($return['status'] == '3') {
                    return json_encode([
                        'status' => '2',
                        'pay_status' => '3',
                        'message' => '支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                } elseif ($return['status'] == '2') {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '订单未支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } //其他情况
                else {
                    return json_encode([
                        'status' => 2,
                        'pay_status' => 3,
                        'message' => $return['message'],
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                }
            }

            //银盛支付 交易查询
            if (14000 < $ways_type && $ways_type < 14010) {
                //读取配置
                $config = new YinshengConfigController();
                $yinsheng_config = $config->yinsheng_config($config_id);
                if (!$yinsheng_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '银盛配置不存在请检查配置'
                    ]);
                }

                $yinsheng_merchant = $config->yinsheng_merchant($store_id, $store_pid);
                if (!$yinsheng_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '银盛商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\YinSheng\PayController();
                $ys_data = [
                    'out_trade_no' => $order->out_trade_no,
                    'partner_id' => $yinsheng_config->partner_id,
                    'trade_no' => $order->trade_no,
                    'shop_date' => $order->created_at,
                    'seller_id' => $yinsheng_merchant->mer_code
                ];
                $return = $obj->order_query($ys_data);
                //支付成功
                if ($return['status'] == '1') {

                    $pay_time = date('Y-m-d H:i:s', time()); //支付完成时间，如2009年12月27日9点10分10秒表示为20091227091010
                    $trade_no = $return['data']['trade_no']; //系统订单号
                    $buyer_logon_id = $return['data']['openid'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
                    $buyer_pay_amount = $return['data']['total_amount'] ? ($return['data']['total_amount']) : ''; //实付金额，单位分
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'receipt_amount' => $buyer_pay_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '14000', //返佣来源
                            'source_desc' => '银盛支付', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => '1',
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $trade_no
                        ]
                    ]);
                } elseif ($return['status'] == '3') {
                    return json_encode([
                        'status' => '2',
                        'pay_status' => '3',
                        'message' => '用户交易撤销',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                } elseif ($return['status'] == '2') {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '订单未支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } //其他情况
                else {
                    return json_encode([
                        'status' => 2,
                        'pay_status' => 3,
                        'message' => $return['message'],
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                }
            }

            //通联支付 交易查询
            if (33000 < $ways_type && $ways_type < 33010) {
                $order = Order::where('out_trade_no', $out_trade_no)->select('*')->first();

                if ($order->pay_status == "1") {
                    return json_encode([
                        'status' => '1',
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $order->pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);
                } elseif ($order->pay_status == "3") {
                    return json_encode([
                        'status' => '2',
                        'pay_status' => '3',
                        'message' => '用户交易撤销',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                } elseif ($order->pay_status == "2") {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '订单未支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } //其他情况
                else {
                    return json_encode([
                        'status' => '2',
                        'pay_status' => '3',
                        'message' => $return['message'],
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                }
            }

            //富友支付 交易查询
            if (11000 < $ways_type && $ways_type < 11010) {
                $config = new FuiouConfigController();
                $fuiou_config = $config->fuiou_config($config_id);
                if (!$fuiou_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '富友支付配置不存在请检查配置'
                    ]);
                }

                $fuiou_merchant = $config->fuiou_merchant($store_id, $store_pid);
                if (!$fuiou_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '富友支付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Fuiou\PayController();
                $order_data = [
                    'ins_cd' => $fuiou_config->ins_cd,//机构号
                    'mchnt_cd' => $fuiou_merchant->mchnt_cd,
                    'mchnt_order_no' => $out_trade_no
                ];
                if ($order->ways_source == 'alipay') {
                    $order_data['order_type'] = 'ALIPAY';
                }
                if ($order->ways_source == 'weixin') {
                    $order_data['order_type'] = 'WECHAT';
                }
                if ($order->ways_source == 'unionpay') {
                    $order_data['order_type'] = 'UNIONPAY';
                }
                $return = $obj->order_query($order_data); //-1 系统错误 0-其他 1-成功 2-下单失败 3-订单未支付


                //支付成功
                if ($return['status'] == '1') {
                    $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['reserved_txn_fin_ts']));
                    $trade_no = $return['data']['transaction_id'];
                    $buyer_logon_id = $return['data']['buyer_id'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
                    $buyer_pay_amount = $return['data']['order_amt'] ? ($return['data']['order_amt'] / 100) : ''; //实付金额，单位分
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'receipt_amount' => $buyer_pay_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '11000', //返佣来源
                            'source_desc' => '富友支付', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => '1',
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $trade_no
                        ]
                    ]);
                } elseif ($return['status'] == '3') {
                    return json_encode([
                        'status' => '2',
                        'pay_status' => '3',
                        'message' => '支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                } elseif ($return['status'] == '2') {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '订单未支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } //其他情况
                else {
                    return json_encode([
                        'status' => 2,
                        'pay_status' => 3,
                        'message' => $return['message'],
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                }
            }

            //拉卡拉支付 交易查询
            if (34000 < $ways_type && $ways_type < 34010) {
                //读取配置
                $config = new LklConfigController();
                $lkl_config = $config->lkl_config($config_id);
                if (!$lkl_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '拉卡拉配置不存在请检查配置'
                    ]);
                }

                $lkl_merchant = $config->lkl_merchant($store_id, $store_pid);
                if (!$lkl_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '拉卡拉商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\LklPay\PayController();
                $ys_data = [
                    'out_trade_no' => $order->out_trade_no,
                    'merchant_no' => $lkl_merchant->customer_no, //商户号
                    'term_no' => $lkl_merchant->term_no,
                ];
                $return = $obj->order_query($ys_data);
                //支付成功
                if ($return['status'] == '1') {

                    $pay_time = date('Y-m-d H:i:s', time());
                    $trade_no = $return['data']['trade_no']; //系统订单号
                    $buyer_pay_amount = $return['data']['total_amount'] ? ($return['data']['total_amount'] / 100) : ''; //实付金额，单位分
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                    $other_no = $return['data']['acc_trade_no'];
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'receipt_amount' => $buyer_pay_amount,
                            'other_no' => $other_no
                        ];
                        $this->update_day_order($insert_data, $out_trade_no, $table);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '34000', //返佣来源
                            'source_desc' => '拉卡拉支付', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => '1',
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                            'trade_no' => $trade_no
                        ]
                    ]);
                } elseif ($return['status'] == '3') {
                    return json_encode([
                        'status' => '2',
                        'pay_status' => '3',
                        'message' => '用户交易撤销',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                } elseif ($return['status'] == '2') {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '订单未支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount
                        ]
                    ]);
                } //其他情况
                else {
                    return json_encode([
                        'status' => 2,
                        'pay_status' => 3,
                        'message' => $return['message'],
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                }
            }


            return json_encode([
                'status' => 2,
                'message' => '暂无该通道-同步状态'
            ]);
        } catch
        (\Exception $e) {
            $this->status = -1;
            $this->message = $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine();
            return $this->format();
        }
    }


    //花呗订单轮询
    public function hb_order_foreach(Request $request)
    {
        try {
            Log::info('hb_order_foreach');
            Log::info($request->all());
            $store_id = $request->get('store_id', '');
            $out_trade_no = $request->get('out_trade_no', '');
            $check_data = [
                'store_id' => '门店ID',
                'out_trade_no' => '订单号',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $data = [
                'store_id' => $store_id,
                'out_trade_no' => $out_trade_no
            ];

            return $this->hb_order_foreach_public($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage() . $exception->getLine();
            return $this->format();
        }
    }


    //花呗订单轮询
    public function hb_order_foreach_public($data)
    {
        try {
            $store_id = $data['store_id'];
            $out_trade_no = $data['out_trade_no'];
            $check_data = [
                'store_id' => '门店ID',
                'out_trade_no' => '订单号',
            ];
            $check = $this->check_required($data, $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $AlipayHbOrder = AlipayHbOrder::where('out_trade_no', $out_trade_no)
                ->where('store_id', $store_id)
                ->select('ways_type', 'total_amount', 'ways_source')
                ->first();
            if (!$AlipayHbOrder) {
                return json_encode([
                    'status' => 2,
                    'message' => '订单号不存在'
                ]);
            }

            $store = Store::where('store_id', $store_id)
                ->select('config_id', 'merchant_id', 'pid', 'store_short_name')
                ->first();

            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店ID不存在'
                ]);
            }

            $config_id = $store->config_id;
            $store_pid = $store->pid;
            $store_name = $store->store_short_name;

            $ways_type = $AlipayHbOrder->ways_type;
            $total_amount = $AlipayHbOrder->total_amount;

            //官方支付宝
            if (999 < $ways_type && $ways_type < 1999) {
                //配置
                $isvconfig = new AlipayIsvConfigController();
                $storeInfo = $isvconfig->alipay_auth_info($store_id, $store_pid);
                $config = $isvconfig->AlipayIsvConfig($config_id);
                if (!$storeInfo) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店ID未授权'
                    ]);
                }

                $app_auth_token = $storeInfo->app_auth_token;

                $notify_url = url('/api/alipayopen/qr_pay_notify');
                $aop = new AopClient();
                $aop->apiVersion = "2.0";
                $aop->appId = $config->app_id;
                $aop->rsaPrivateKey = $config->rsa_private_key;
                $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                $aop->notify_url = $notify_url;
                $aop->signType = "RSA2"; //升级算法
                $aop->gatewayUrl = $config->alipay_gateway;
                $aop->format = "json";
                $aop->charset = "GBK";
                $aop->version = "2.0";
                $aop->method = 'alipay.trade.query';
                $requests = new AlipayTradeQueryRequest();
                $requests->setBizContent("{" .
                    "    \"out_trade_no\":\"" . $out_trade_no . "\"" .
                    "  }");
                $status = $aop->execute($requests, '', $app_auth_token);
                $resultCode = $status->alipay_trade_query_response->code;

                //异常
                if ($resultCode == 40004) {
                    $is_dqr = substr($out_trade_no, 0, 5);
                    //动态码在没有付款会报交易不存在
                    if ($is_dqr == "fq_qr") {
                        return json_encode([
                            'status' => 1,
                            'pay_status' => '2',
                            'message' => '等待支付'
                        ]);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $status->alipay_trade_query_response->sub_msg
                    ]);
                }

                //支付成功
                if ($status->alipay_trade_query_response->trade_status == "TRADE_SUCCESS") {
                    //改变数据库状态
                    AlipayHbOrder::where('out_trade_no', $out_trade_no)->update(
                        [
                            'trade_no' => $status->alipay_trade_query_response->trade_no,
                            'buyer_logon_id' => $status->alipay_trade_query_response->buyer_user_id,
                            'pay_status_desc' => '支付成功',
                            'pay_status' => 1,
                        ]);
                    $pay_time = $status->alipay_trade_query_response->send_pay_date;

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'out_trade_no' => $out_trade_no,
                            'ways_type' => $AlipayHbOrder->ways_type,
                            'ways_source' => $AlipayHbOrder->ways_source,
                            'total_amount' => $total_amount,
                            'pay_amount' => $total_amount,
                            'store_id' => $store_id,
                            'store_name' => $store_name,
                            'config_id' => $config_id,
                            'pay_time' => $pay_time,
                            'ways_source_desc' => '花呗分期',
                        ]
                    ]);
                } //等待付款
                elseif ($status->alipay_trade_query_response->trade_status == "WAIT_BUYER_PAY") {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付'
                    ]);

                } //订单关闭
                elseif ($status->alipay_trade_query_response->trade_status == 'TRADE_CLOSED') {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单关闭'
                    ]);
                } else {
                    //其他情况
                    $message = $status->alipay_trade_query_response->sub_msg;
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message
                    ]);
                }
            }

            //直付通
            if (16000 < $ways_type && $ways_type < 16999) {
                $config_type = '03';
                //配置
                $isvconfig = new AlipayIsvConfigController();
                $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);
                if (!$config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '直付通配置不存在'
                    ]);
                }

                //判断直付通
                $AlipayZftStore = $isvconfig->AlipayZftStore($store_id, $store_pid);
                if (!$AlipayZftStore) {
                    return json_encode([
                        'status' => 2,
                        'message' => '直付通门店不存在'
                    ]);
                }

                $aop = new AopClient();
                $aop->apiVersion = "2.0";
                $aop->appId = $config->app_id;
                $aop->rsaPrivateKey = $config->rsa_private_key;
                $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                $aop->signType = "RSA2"; //升级算法
                $aop->gatewayUrl = $config->alipay_gateway;
                $aop->format = "json";
                $aop->charset = "GBK";
                $aop->version = "2.0";
                $aop->method = 'alipay.trade.query';
                $requests = new AlipayTradeQueryRequest();
                $requests->setBizContent("{" .
                    "    \"out_trade_no\":\"" . $out_trade_no . "\"" .
                    "  }");
                $status = $aop->execute($requests, '', '');
                $resultCode = $status->alipay_trade_query_response->code;
                //异常
                if ($resultCode == 40004) {
                    $is_dqr = substr($out_trade_no, 0, 5);
                    //动态码在没有付款会报交易不存在
                    if ($is_dqr == "fq_qr") {
                        return json_encode([
                            'status' => 1,
                            'pay_status' => '2',
                            'message' => '等待支付'
                        ]);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $status->alipay_trade_query_response->sub_msg
                    ]);
                }
                //支付成功
                if ($status->alipay_trade_query_response->trade_status == "TRADE_SUCCESS") {
                    //改变数据库状态
                    AlipayHbOrder::where('out_trade_no', $out_trade_no)->update(
                        [
                            'trade_no' => $status->alipay_trade_query_response->trade_no,
                            'buyer_logon_id' => $status->alipay_trade_query_response->buyer_user_id,
                            'pay_status_desc' => '支付成功',
                            'pay_status' => 1,
                        ]);
                    $pay_time = $status->alipay_trade_query_response->send_pay_date;

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'out_trade_no' => $out_trade_no,
                            'ways_type' => $AlipayHbOrder->ways_type,
                            'ways_source' => $AlipayHbOrder->ways_source,
                            'total_amount' => $total_amount,
                            'pay_amount' => $total_amount,
                            'store_id' => $store_id,
                            'store_name' => $store_name,
                            'config_id' => $config_id,
                            'pay_time' => $pay_time,
                            'ways_source_desc' => '花呗分期',
                        ]
                    ]);

                } //等待付款
                elseif ($status->alipay_trade_query_response->trade_status == "WAIT_BUYER_PAY") {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付'
                    ]);

                } //订单关闭
                elseif ($status->alipay_trade_query_response->trade_status == 'TRADE_CLOSED') {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单关闭'
                    ]);
                } else {
                    //其他情况
                    $message = $status->alipay_trade_query_response->sub_msg;
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message
                    ]);
                }
            }

        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage() . $exception->getLine();
            return $this->format();
        }
    }


    //小程序首页的数据
    public function weixinapp_index_count(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $merchant_id = $merchant->merchant_id;
            $store_ids = [];
            $where = [];

            //收银员
            if ($merchant->merchant_type == 2) {
                $where[] = ['merchant_id', '=', $merchant_id];
            }

            $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)
                ->select('store_id')
                ->get();
            if (!$MerchantStore->isEmpty()) {
                $store_ids = $MerchantStore->toArray();
            }

            //今日
//            $day = date('Ymd', time());
//
//            //收银员
//            if ($merchant->merchant_type == 2) {
//                $day_order_data = MerchantStoreDayOrder::where('day', $day)
//                    ->whereIn('store_id', $store_ids)
//                    ->where($where)
//                    ->select('total_amount', 'order_sum');
//
//            } else {
//                $day_order_data = StoreDayOrder::where('day', $day)
//                    ->whereIn('store_id', $store_ids)
//                    ->select('total_amount', 'order_sum');
//            }
//
//
//            $day_order = $day_order_data->sum('total_amount');
//            $day_order = '' . $day_order . '';
//            $day_order_count = '' . $day_order_data->sum('order_sum') . '';

            //下面需要优化 读取动态的
            $time_start = date('Y-m-d 00:00:00', time());
            $time_end = date('Y-m-d H:i:s', time());

            $day = date('Ymd', strtotime($time_start));
            $table = 'orders_' . $day;
            if (Schema::hasTable($table)) {
                $obj = DB::table($table);
            } else {
                $obj = DB::table('orders');
            }

            $order_data = $obj->whereIn('store_id', $store_ids)
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->whereIn('pay_status', [1, 6]) //成功+退款
                ->select('total_amount', 'id');
            $day_order = $order_data->sum('total_amount'); //交易金额
            $day_order_count = $order_data->count('id');

            //列表
            if (Schema::hasTable($table)) {
                $obj1 = DB::table($table);
            } else {
                $obj1 = DB::table('orders');
            }

            $order = $obj1->where($where)
                ->whereIn('store_id', $store_ids)
                ->where('pay_status', 1)
                ->orderBy('updated_at', 'desc')
                ->select('out_trade_no', 'ways_source', 'ways_source_desc', 'updated_at', 'total_amount')
                ->first();
            if (!$order) {
                $order = '';
            }

            $data = [
                'day_order' => $day_order,
                'day_order_count' => $day_order_count,
                'order_list' => $order
            ];

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage() . $exception->getLine();
            return $this->format();
        }
    }


    //APP订单撤销
    public function order_cancel_public($data)
    {
        try {
            $store_id = $data['store_id'];
            $out_trade_no = isset($data['out_trade_no']) ? $data['out_trade_no'] : "";
            $other_no = isset($data['other_no']) ? $data['other_no'] : "";
            $ways_type = $data['ways_type'];
            $config_id = $data['config_id'];
            if ($out_trade_no) {
                $c_o = 'a' . $out_trade_no;
                $where[] = ['out_trade_no', '=', $out_trade_no];
            } else {
                $c_o = 'a' . $other_no;
                $where[] = ['other_no', '=', $other_no];
            }

            $order = Cache::get($c_o);
            if (!$order) {
                //发起查询
                $order = Order::where('store_id', $store_id)
                    ->where($where)
                    ->select(
                        'id',
                        'ways_source',
                        'ways_type',
                        'company',
                        'ways_type_desc',
                        'pay_status',
                        'out_trade_no',
                        'trade_no',
                        'total_amount',
                        'pay_amount',
                        'qwx_no',
                        'rate',
                        'fee_amount',
                        'merchant_id',
                        'store_id',
                        'user_id',
                        'store_name',
                        'other_no'
                    )->first();

                Cache::put($c_o, $order, 1);
            }

            if (!$order) {
                return json_encode([
                    'status' => 2,
                    'message' => '订单号不存在'
                ]);
            }

            $ways_type = $order->ways_type;
            $out_trade_no = $order->out_trade_no;
            $other_no = $order->other_no;

            $store = Store::where('store_id', $store_id)
                ->select('config_id', 'merchant_id', 'pid')
                ->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店ID不存在'
                ]);
            }

            $config_id = $store->config_id;
            $store_pid = $store->pid;

            //支付宝官方订单
            if (999 < $ways_type && $ways_type < 1999) {
                //配置
                $isvconfig = new AlipayIsvConfigController();
                $storeInfo = $isvconfig->alipay_auth_info($store_id, $store_pid);
                $config = $isvconfig->AlipayIsvConfig($config_id);

                $app_auth_token = $storeInfo->app_auth_token;

                $notify_url = url('/api/alipayopen/qr_pay_notify');
                $aop = new AopClient();
                $aop->apiVersion = "2.0";
                $aop->appId = $config->app_id;
                $aop->rsaPrivateKey = $config->rsa_private_key;
                $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                $aop->notify_url = $notify_url;
                $aop->signType = "RSA2"; //升级算法
                $aop->gatewayUrl = $config->alipay_gateway;
                $aop->format = "json";
                $aop->charset = "GBK";
                $aop->version = "2.0";
                $aop->method = 'alipay.trade.query';
                $requests = new AlipayTradeQueryRequest();
                $requests->setBizContent("{" .
                    "    \"out_trade_no\":\"" . $out_trade_no . "\"" .
                    "  }");
                $status = $aop->execute($requests, '', $app_auth_token);

                if ($status->alipay_trade_query_response->code == 40004) {
                    $is_dqr = substr($out_trade_no, 0, 3);
                    $pay_status = "3";
                    $re_status = 2;
                    //动态码在没有付款会报交易不存在
                    if ($is_dqr == "DQR") {
                        $pay_status = "2";
                        $re_status = 1;
                        return json_encode([
                            'status' => $re_status,
                            'pay_status' => $pay_status,
                            'message' => $status->alipay_trade_query_response->sub_msg,
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount,
                                'other_no' => $other_no
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => $re_status,
                            'pay_status' => $pay_status,
                            'message' => $status->alipay_trade_query_response->sub_msg,
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount,
                            ]
                        ]);
                    }
                }
                //支付成功
                if ($status->alipay_trade_query_response->trade_status == "TRADE_SUCCESS") {
                    $buyer_pay_amount = $status->alipay_trade_query_response->buyer_pay_amount;
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);
                        $insert_data = [
                            'status' => 'TRADE_SUCCESS',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => $status->alipay_trade_query_response->buyer_user_id,
                            'trade_no' => $status->alipay_trade_query_response->trade_no,
                            'pay_time' => $status->alipay_trade_query_response->send_pay_date,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '1000',//返佣来源
                            'source_desc' => '支付宝',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $status->alipay_trade_query_response->send_pay_date,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_time' => $status->alipay_trade_query_response->send_pay_date,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);

                } //等待付款
                elseif ($status->alipay_trade_query_response->trade_status == "WAIT_BUYER_PAY") {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);

                } //订单关闭
                elseif ($status->alipay_trade_query_response->trade_status == 'TRADE_CLOSED') {
                    $order->update([
                        'status' => '4',
                        'pay_status' => 4,
                        'pay_status_desc' => '订单关闭',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                    $order->save();

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单关闭',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);

                } else {
                    //其他情况
                    $message = $status->alipay_trade_query_response->sub_msg;
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                }
            }

            //官方微信支付订单
            if (1999 < $ways_type && $ways_type < 2999) {
                $config = new WeixinConfigController();
                $options = $config->weixin_config($config_id);
                $weixin_store = $config->weixin_merchant($store_id, $store_pid);
                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;
                $config = [
                    'app_id' => $options['app_id'],
                    'mch_id' => $options['payment']['merchant_id'],
                    'key' => $options['payment']['key'],
                    'cert_path' => $options['payment']['cert_path'], // XXX: 绝对路径！！！！
                    'key_path' => $options['payment']['key_path'],     // XXX: 绝对路径！！！！
                    'sub_mch_id' => $wx_sub_merchant_id,
                    // 'device_info'     => '013467007045764',
                    // 'sub_app_id'      => '',
                    // ...
                ];

                $payment = Factory::payment($config);
                $query = $payment->order->queryByOutTradeNumber($order->out_trade_no);

                //成功
                if ($query['trade_state'] == 'SUCCESS') {
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $order->out_trade_no; //$query['transaction_id'];
                    $re_data['time_end'] = $query['time_end'];

                    if ($order->pay_status != 1) {
                        //改变数据库状态
                        Cache::forget($c_o);
                        $insert_data = [
                            'status' => 'TRADE_SUCCESS',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => $query['openid'],
                            'trade_no' => $query['transaction_id'],
                            'pay_time' => date('Y-m-d H:i:s', strtotime($query['time_end'])),
                        ];
                        $this->update_day_order($insert_data, $order->out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '2000',//返佣来源
                            'source_desc' => '微信支付',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => date('Y-m-d H:i:s', strtotime($query['time_end'])),
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => date('Y-m-d H:i:s', strtotime($query['time_end'])),
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);

                } elseif ($query['trade_state'] == "USERPAYING" || $query['trade_state'] == "NOTPAY") {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);
                } elseif ($query['trade_state'] == "CLOSED") {
                    $order->update([
                        'status' => '4',
                        'pay_status' => '4',
                        'pay_status_desc' => '订单关闭',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                    $order->save();

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单关闭',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                } elseif ($query['trade_state'] == "REVOKED") {
                    $order->update([
                        'status' => '3',
                        'pay_status' => '3',
                        'pay_status_desc' => '订单已撤销',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                    $order->save();

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单关闭',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = $query['trade_state_desc'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                }
            }

            //官方微信a 支付订单
            if (3999 < $ways_type && $ways_type < 4999) {
                $config = new WeixinConfigController();
                $options = $config->weixina_config($config_id);
                $weixin_store = $config->weixina_merchant($store_id, $store_pid);
                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;
                $config = [
                    'app_id' => $options['app_id'],
                    'mch_id' => $options['payment']['merchant_id'],
                    'key' => $options['payment']['key'],
                    'cert_path' => $options['payment']['cert_path'], // XXX: 绝对路径！！！！
                    'key_path' => $options['payment']['key_path'],     // XXX: 绝对路径！！！！
                    'sub_mch_id' => $wx_sub_merchant_id,
                    // 'device_info'     => '013467007045764',
                    // 'sub_app_id'      => '',
                    // ...
                ];
                $payment = Factory::payment($config);
                $query = $payment->order->queryByOutTradeNumber($order->out_trade_no);

                //成功
                if ($query['trade_state'] == 'SUCCESS') {
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $order->out_trade_no; //$query['transaction_id'];
                    $re_data['time_end'] = $query['time_end'];

                    if ($order->pay_status != 1) {
                        //改变数据库状态
                        Cache::forget($c_o);
                        $insert_data = [
                            'status' => 'TRADE_SUCCESS',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => $query['openid'],
                            'trade_no' => $query['transaction_id'],
                            'pay_time' => date('Y-m-d H:i:s', strtotime($query['time_end'])),
                        ];
                        $this->update_day_order($insert_data, $order->out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '4000', //返佣来源
                            'source_desc' => '微信支付a', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => date('Y-m-d H:i:s', strtotime($query['time_end'])),
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => date('Y-m-d H:i:s', strtotime($query['time_end'])),
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);
                } elseif ($query['trade_state'] == "USERPAYING" || $query['trade_state'] == "NOTPAY") {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);
                } elseif ($query['trade_state'] == "CLOSED") {
                    $order->update([
                        'status' => '4',
                        'pay_status' => '4',
                        'pay_status_desc' => '订单关闭',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                    $order->save();

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单关闭',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                } elseif ($query['trade_state'] == "REVOKED") {
                    $order->update([
                        'status' => '3',
                        'pay_status' => '3',
                        'pay_status_desc' => '订单已撤销',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                    $order->save();

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单关闭',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = $query['trade_state_desc'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                }
            }

            //京东收银支付
            if (5999 < $ways_type && $ways_type < 6999) {
                //读取配置
                $config = new JdConfigController();
                $jd_config = $config->jd_config($config_id);
                if (!$jd_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '京东配置不存在请检查配置'
                    ]);
                }

                $jd_merchant = $config->jd_merchant($store_id, $store_pid);
                if (!$jd_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '京东商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Jd\PayController();
                $data = [];
                $data['out_trade_no'] = $out_trade_no;
                $data['request_url'] = $obj->order_query_url; //请求地址;
                $data['merchant_no'] = $jd_merchant->merchant_no;
                $data['md_key'] = $jd_merchant->md_key; //
                $data['des_key'] = $jd_merchant->des_key; //
                $data['systemId'] = $jd_config->systemId; //
                $return = $obj->order_query($data);
                //支付成功
                if ($return["status"] == 1) {
                    $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['payFinishTime']));
                    $trade_no = $return['data']['tradeNo'];
                    $channelNoSeq = isset($return['data']['channelNoSeq']) ? $return['data']['channelNoSeq'] : $trade_no; //条码
                    $buyer_pay_amount = $return['data']['piAmount'] / 100;
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $channelNoSeq,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '6000',//返佣来源
                            'source_desc' => '京东金融',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);

                } //等待付款
                elseif ($return["status"] == 2) {

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);

                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                }//订单退款
                elseif ($return["status"] == 4) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单已经退款',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                }
            }

            //快钱支付
            if (2999 < $ways_type && $ways_type < 3999) {
                //读取配置
                $MyBankobj = new MyBankConfigController();
                $MyBankConfig = $MyBankobj->MyBankConfig($config_id);
                if (!$MyBankConfig) {
                    return json_encode([
                        'status' => 2,
                        'message' => '快钱配置不存在请检查配置'
                    ]);
                }

                $mybank_merchant = $MyBankobj->mybank_merchant($store_id, $store_pid);
                if (!$mybank_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '快钱商户号不存在'
                    ]);
                }

                $MerchantId = $mybank_merchant->MerchantId;
                $obj = new TradePayController();
                $return = $obj->mybankOrderQuery($MerchantId, $config_id, $out_trade_no);
                if ($return['status'] == 0) {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message']
                    ]);
                }

                $body = $return['data']['document']['response']['body'];
                $TradeStatus = $body['TradeStatus'];

                //成功
                if ($TradeStatus == 'succ') {
                    $OrderNo = $body['MerchantOrderNo'];
                    $GmtPayment = $body['GmtPayment'];
                    $buyer_id = '';
                    if ($ways_type == 3004) {
                        $buyer_id = $body['SubOpenId'];
                    }
                    if ($ways_type == 3003) {
                        $buyer_id = $body['BuyerUserId'];
                    }

                    $pay_time = date('Y-m-d H:i:s', strtotime($GmtPayment));
                    $payment_method = strtolower($body['Credit']);

                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);
                        $insert_data = [
                            'status' => 1,
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_id' => $buyer_id,
                            'trade_no' => $OrderNo,
                            'pay_time' => $pay_time,
                            'payment_method' => $payment_method,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '3000',//返佣来源
                            'source_desc' => '快钱支付',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);

                } elseif ($TradeStatus == 'paying') {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = '请重新扫码';
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                }
            }

            //新大陆
            if (7999 < $ways_type && $ways_type < 8999) {
                //读取配置
                $config = new NewLandConfigController();
                $new_land_config = $config->new_land_config($config_id);
                if (!$new_land_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '新大陆配置不存在请检查配置'
                    ]);
                }

                $mybank_merchant = $config->new_land_merchant($store_id, $store_pid);
                if (!$mybank_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户新大陆通道未开通'
                    ]);
                }

                $request_data = [
                    'out_trade_no' => $out_trade_no,
                    'key' => $mybank_merchant->nl_key,
                    'org_no' => $new_land_config->org_no,
                    'merc_id' => $mybank_merchant->nl_mercId,
                    'trm_no' => $mybank_merchant->trmNo,
                    'op_sys' => '3',
                    'opr_id' => $store->merchant_id,
                    'trm_typ' => 'T',
                ];
                $obj = new PayController();
                $return = $obj->order_query($request_data);
                //支付成功
                if ($return["status"] == 1) {
                    $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['sysTime']));
                    $trade_no = $return['data']['orderNo'];
                    $buyer_pay_amount = $return['data']['amount'] / 100;
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '8000',//返佣来源
                            'source_desc' => '新大陆',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);

                } //等待付款
                elseif ($return["status"] == 2) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                }//订单退款
                elseif ($return["status"] == 4) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单已经退款',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                }
            }

            //和融通收银支付
            if (8999 < $ways_type && $ways_type < 9999) {
                //读取配置
                $config = new HConfigController();
                $h_config = $config->h_config($config_id);
                if (!$h_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '和融通不存在请检查配置'
                    ]);
                }

                $h_merchant = $config->h_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '和融通商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Huiyuanbao\PayController();
                $data = [];
                $data['out_trade_no'] = $out_trade_no;
                $data['request_url'] = $obj->order_query_url; //请求地址;
                $data['md_key'] = $h_config->md_key; //
                $data['mid'] = $h_merchant->h_mid; //
                $data['orgNo'] = $h_config->orgNo; //

                $return = $obj->order_query($data);

                //支付成功
                if ($return["status"] == 1) {
                    // $pay_time = strtotime($return['data']['timeEnd']) ? strtotime($return['data']['timeEnd']) : time();
                    $pay_time = date('Y-m-d H:i:s', time());
                    $trade_no = '112121' . $return['data']['transactionId'];
                    $buyer_pay_amount = $return['data']['totalFee'];
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);
                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '9000',//返佣来源
                            'source_desc' => '和融通',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);

                } //等待付款
                elseif ($return["status"] == 2) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);

                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);

                }//订单退款
                elseif ($return["status"] == 4) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单已经退款',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);

                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);
                }
            }

            //ltf收银支付
            if (9999 < $ways_type && $ways_type < 10999) {
                //读取配置
                $config = new LtfConfigController();
                $ltf_merchant = $config->ltf_merchant($store_id, $store_pid);
                if (!$ltf_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Ltf\PayController();
                $data = [];
                $data['out_trade_no'] = $out_trade_no;
                $data['request_url'] = $obj->order_query_url; //请求地址;
                $data['merchant_no'] = $ltf_merchant->merchantCode;
                $data['appId'] = $ltf_merchant->appId; //
                $data['key'] = $ltf_merchant->md_key; //

                $return = $obj->order_query($data);
                //支付成功
                if ($return["status"] == 1) {
                    $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['payTime']));
                    $trade_no = $return['data']['outTransactionId'];
                    $buyer_pay_amount = $return['data']['receiptAmount'];
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '10000',//返佣来源
                            'source_desc' => '联拓富',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);
                } //等待付款
                elseif ($return["status"] == 2) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'other_no' => $other_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                }//订单退款
                elseif ($return["status"] == 4) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单已经退款',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                }
            }

            //随行付收银支付
            if (12999 < $ways_type && $ways_type < 13999) {
                //读取配置
                $config = new VbillConfigController();
                $vbill_config = $config->vbill_config($config_id);
                if (!$vbill_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付配置不存在请检查配置'
                    ]);
                }

                $vbill_merchant = $config->vbill_merchant($store_id, $store_pid);
                if (!$vbill_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Vbill\PayController();
                $data['notify_url'] = url('/api/vbill/pay_notify_url'); //回调地址
                $data['request_url'] = $obj->order_query_url; //请求地址;
                $data['mno'] = $vbill_merchant->mno;
                $data['privateKey'] = $vbill_config->privateKey; //
                $data['sxfpublic'] = $vbill_config->sxfpublic; //
                $data['orgId'] = $vbill_config->orgId; //
                $data['out_trade_no'] = $order->out_trade_no;
                $return = $obj->order_query($data);
                //支付成功
                if ($return["status"] == 1) {
                    $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['respData']['tranTime']));
                    $trade_no = $return['data']['respData']['sxfUuid'];
                    $buyer_pay_amount = $return['data']['respData']['oriTranAmt'];
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '13000',//返佣来源
                            'source_desc' => '随行付',//返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $buyer_pay_amount,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);

                } //等待付款
                elseif ($return["status"] == 2) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '2',
                        'message' => '等待支付',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->total_amount,
                            'total_amount' => $order->total_amount,
                        ]
                    ]);

                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单支付失败',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);

                }//订单退款
                elseif ($return["status"] == 4) {
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => '订单已经退款',
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);

                } else {
                    //其他情况
                    $message = $return['message'];
                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                }
            }

            //哆啦宝
            if (14999 < $ways_type && $ways_type < 15999) {
                $manager = new ManageController();
                //读取配置
                $dlbconfig = $manager->pay_config($config_id);
                if (!$dlbconfig) {
                    return json_encode([
                        'status' => 2,
                        'message' => '哆啦宝配置配置不存在请检查配置'
                    ]);
                }

                $dlb_merchant = $manager->dlb_merchant($store_id, $store_pid);
                if (!$dlb_merchant && !empty($dlb_merchant->mch_num) && !empty($dlb_merchant->shop_num)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '哆啦宝配置商户未补充商户编号等信息!'
                    ]);
                }

                $query_data = [
                    'agentNum' => $dlbconfig->agent_num,
                    'customerNum' => $dlb_merchant->mch_num,
                    'shopNum' => $dlb_merchant->shop_num,
                    'requestNum' => $order->out_trade_no,
                    'secretKey' => $dlbconfig->secret_key,
                    'accessKey' => $dlbconfig->access_key,
                ];
                $return = $manager->query_bill($query_data);
                if ($return['status'] == 0) {
                    return json_encode($return);
                }
                if ($return['status'] == 1) {
                    $query_result = $return['data'];
                    if ($query_result['status'] == "SUCCESS") {
                        $pay_time = $query_result['completeTime'];
                        $trade_no = $query_result['orderNum'];
                        $buyer_pay_amount = $query_result['orderAmount'];
                        //改变数据库状态
                        if ($order->pay_status != 1) {
                            Cache::forget($c_o);

                            $insert_data = [
                                'status' => '1',
                                'pay_status' => 1,
                                'pay_status_desc' => '支付成功',
                                'buyer_logon_id' => '',
                                'trade_no' => $trade_no,
                                'pay_time' => $pay_time,
                                'buyer_pay_amount' => $buyer_pay_amount,
                            ];
                            $this->update_day_order($insert_data, $out_trade_no);

                            //支付成功后的动作
                            $data = [
                                'ways_type' => $order->ways_type,
                                'ways_type_desc' => $order->ways_type_desc,
                                'source_type' => '15000',//返佣来源
                                'source_desc' => '哆啦宝',//返佣来源说明
                                'company' => 'dlb',//返佣来源说明
                                'total_amount' => $order->total_amount,
                                'out_trade_no' => $order->out_trade_no,
                                'rate' => $order->rate,
                                'merchant_id' => $order->merchant_id,
                                'store_id' => $order->store_id,
                                'user_id' => $order->user_id,
                                'config_id' => $config_id,
                                'store_name' => $order->store_name,
                                'ways_source' => $order->ways_source,
                                'pay_time' => $pay_time,
                                'device_id' => isset($order->device_id) ? $order->device_id : "",
                            ];
                            PaySuccessAction::action($data);
                        }

                        return json_encode([
                            'status' => 1,
                            'pay_status' => '1',
                            'message' => '支付成功',
                            'data' => [
                                'ways_source' => $order->ways_source,
                                'pay_time' => $pay_time,
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $buyer_pay_amount,
                                'total_amount' => $order->total_amount,
                                'other_no' => $other_no,
                            ]
                        ]);
                    } elseif ($query_result['status'] == "INIT") {
                        return json_encode([
                            'status' => 1,
                            'pay_status' => '2',
                            'message' => '等待支付',
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->total_amount,
                                'total_amount' => $order->total_amount,
                            ]
                        ]);
                    } elseif ($query_result['status'] == "CANCEL") {
                        return json_encode([
                            'status' => 1,
                            'pay_status' => '3',
                            'message' => '订单支付失败',
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount,
                            ],
                        ]);
                    } elseif ($query_result['status'] == "REFUND") {
                        return json_encode([
                            'status' => 1,
                            'pay_status' => '3',
                            'message' => '订单已经退款',
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount,
                            ],
                        ]);
                    } elseif ($query_result['status'] == "REFUNDING") {
                        return json_encode([
                            'status' => 1,
                            'pay_status' => '3',
                            'message' => '订单退款中',
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount,
                            ],
                        ]);
                    } elseif ($query_result['status'] == "FUNDFAIL") {
                        return json_encode([
                            'status' => 1,
                            'pay_status' => '3',
                            'message' => '订单退款失败',
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount,
                            ],
                        ]);
                    }
                }
            }

            //海科融通 交易关闭
            if (21999 < $ways_type && $ways_type < 22999) {
                //读取配置
                $config = new HkrtConfigController();
                $hkrt_config = $config->hkrt_config($config_id);
                if (!$hkrt_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '海科融通配置不存在请检查配置'
                    ]);
                }

                $hkrt_merchant = $config->hkrt_merchant($store_id, $store_pid);
                if (!$hkrt_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '海科融通商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Hkrt\PayController();
                $hkrt_order_close_data = [
                    'access_id' => $hkrt_config->access_id,
                    'trade_no' => $order->trade_no,
                    'out_trade_no' => $order->out_trade_no,
                    'access_key' => $hkrt_config->access_key
                ];
                Log::info('海科融通-商户-交易关闭');
                Log::info($hkrt_order_close_data);
                $return = $obj->order_close($hkrt_order_close_data); //0-失败 1-成功
                Log::info($return);

                if (isset($return) && !empty($return)) {
                    if ($return["status"] == 1) {
                        //改变数据库状态
                        if ($order->pay_status != 1) {
                            Cache::forget($c_o);
                            $insert_data = [
                                'status' => '4',
                                'pay_status' => '4',
                                'pay_status_desc' => '关闭',
                            ];
                            $this->update_day_order($insert_data, $out_trade_no);
                        }

                        return json_encode([
                            'status' => '1',
                            'pay_status' => '4',
                            'message' => '关闭',
                            'data' => [
                                'ways_source' => $order->ways_source,
                                'pay_time' => $order->pay_time,
                                'out_trade_no' => $out_trade_no,
                                'total_amount' => $order->total_amount,
                                'other_no' => $other_no,
                            ]
                        ]);
                    } else {
                        //其他情况
                        $message = $return['return_msg'];
                        return json_encode([
                            'status' => 1,
                            'pay_status' => '3',
                            'message' => $message,
                            'data' => [
                                'out_trade_no' => $order->out_trade_no,
                                'pay_amount' => $order->pay_amount,
                                'total_amount' => $order->total_amount,
                            ],
                        ]);
                    }
                } else {
                    return json_encode([
                        'status' => '0',
                        'message' => '系统错误'
                    ]);
                }
            }

            //易生支付 交易关闭
            if (20999 < $ways_type && $ways_type < 21999) {
                $config = new EasyPayConfigController();
                $easypay_config = $config->easypay_config($config_id);
                if (!$easypay_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生支付配置不存在请检查配置'
                    ]);
                }

                $easypay_merchant = $config->easypay_merchant($store_id, $store_pid);
                if (!$easypay_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生支付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\EasyPay\PayController();
                $easypay_data = [
                    'channel_id' => $easypay_config->channel_id,
                    'mno' => $easypay_merchant->term_mercode, //终端商戶编号
                    'device_id' => $easypay_merchant->term_termcode, //终端编号
                    'out_trade_no' => $order->out_trade_no,
                    'trade_no' => $order->trade_no
                ];
                Log::info('易生支付-商户-交易关闭');
                Log::info($easypay_data);
                $return = $obj->order_close($easypay_data); //-1 系统错误 0-其他 1-成功
                Log::info($return);

                if ($return['status'] == '1') {
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);
                        $insert_data = [
                            'status' => '4',
                            'pay_status' => '4',
                            'pay_status_desc' => '关闭',
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);
                    }

                    return json_encode([
                        'status' => '1',
                        'pay_status' => '4',
                        'message' => '关闭',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $order->pay_time,
                            'out_trade_no' => $out_trade_no,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                }

            }

            //联动优势 交易关闭
            if (4999 < $ways_type && $ways_type < 5999) {
                $config = new LinkageConfigController();
                $linkage_config = $config->linkage_config($config_id);
                if (!$linkage_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '联动优势支付配置不存在请检查配置'
                    ]);
                }

                $linkage_merchant = $config->linkage_merchant($store_id, $store_pid);
                if (!$linkage_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '联动优势商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Linkage\PayController();
                $linkage_data = [
                    'acqSpId' => $linkage_config->mch_id, //代理商编号
                    'acqMerId' => $linkage_merchant->acqMerId, //商户号
                    'out_trade_no' => $order->out_trade_no, //订单号
                    'privateKey' => $linkage_config->privateKey, //
                    'publicKey' => $linkage_config->publicKey //
                ];
                Log::info('联动优势-商户-交易关闭');
                Log::info($linkage_data);
                $return = $obj->order_close($linkage_data); //-1系统错误；0-其他；1-交易成功；2-验签失败
                Log::info($return);

                if ($return['status'] == '1') {
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);
                        $insert_data = [
                            'status' => '4',
                            'pay_status' => '4',
                            'pay_status_desc' => '关闭',
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);
                    }

                    return json_encode([
                        'status' => '1',
                        'pay_status' => '4',
                        'message' => '关闭',
                        'data' => [
                            'ways_source' => $order->ways_source,
                            'pay_time' => $order->pay_time,
                            'out_trade_no' => $out_trade_no,
                            'total_amount' => $order->total_amount,
                            'other_no' => $other_no,
                        ]
                    ]);
                } else {
                    //其他情况
                    $message = $return['message'];

                    return json_encode([
                        'status' => 1,
                        'pay_status' => '3',
                        'message' => $message,
                        'data' => [
                            'out_trade_no' => $order->out_trade_no,
                            'pay_amount' => $order->pay_amount,
                            'total_amount' => $order->total_amount,
                        ],
                    ]);
                }

            }

        } catch (\Exception $exception) {
            Log::info('APP订单撤销-错误');
            Log::info($exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine());
            $this->status = -1;
            $this->message = $exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine();
            return $this->format();
        }
    }


    //阿尔丰盒子 c01 退款
    public function c01Refund(Request $request)
    {
        try {
            $out_trade_no = $request->get('outTradeNo', '');
            $refund_amount = $request->get('refundAmount', '');
            $store_id = $request->get('storeId', '');

            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => '2',
                    'message' => '系统门店不存在或状态异常'
                ]);
            }

            $merchant_id = $storeObj->merchant_id; //默认
            $merchantObj = Merchant::find($merchant_id);
            if (!$merchantObj) {
                return json_encode([
                    'status' => '2',
                    'message' => '未找到收银员信息'
                ]);
            }

            //收银员
            if ($merchantObj->type == 2) {
                return json_encode([
                    'status' => '2',
                    'message' => '收银员没有退款权限'
                ]);
            }

            $data = [
                'merchant_id' => $merchant_id,
                'out_trade_no' => $out_trade_no,
                'refund_amount' => $refund_amount
            ];
            $return = $this->refund_public($data);

            return $return;
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ]);
        }
    }

    //客户退款
    public function refund_org(Request $request)
    {
        try {
            $orgId = $request->get('orgId');
            $data = $request->get('data');
            $sign = $request->get('sign');
            $storeId = $request->get('storeId');
            if ($orgId == '') {
                return json_encode([
                    'status' => 2,
                    'message' => 'orgId不能为空'
                ]);
            }
            if ($storeId == '') {
                return json_encode([
                    'status' => 2,
                    'message' => 'storeId不能为空'
                ]);
            }
            if (!$data['out_trade_no']) {
                return json_encode([
                    'status' => 2,
                    'message' => 'out_trade_no不能为空'
                ]);
            }
            if (!$data['refund_amount']) {
                return json_encode([
                    'status' => 2,
                    'message' => 'refund_amount不能为空'
                ]);
            }
            if ($sign == '') {
                return json_encode([
                    'status' => 2,
                    'message' => 'sign不能为空'
                ]);
            }

            $userKeyUrl = UserKeyUrl::where('org_id', $orgId)->first();
            if (!$userKeyUrl) {
                return json_encode([
                    'status' => 2,
                    'message' => '代理商不存在',
                ]);
            }
            $payBaseController = new PayBaseController();
            $verify = $payBaseController->verifySign($data, $sign, $userKeyUrl->public_key);//验签
            if (!$verify) {
                return json_encode([
                    'status' => 2,
                    'message' => '验签失败',
                ]);
            }

            $out_trade_no = $data['out_trade_no'];

            $order = DB::table('orders')->orWhere('out_trade_no', $out_trade_no)->orWhere('trade_no', $out_trade_no)
                ->where('store_id', $storeId)->first();
            if (!$order) {
                return json_encode([
                    'status' => 2,
                    'message' => '查询失败，订单不存在',
                ]);
            }
            $refund_data = [
                'merchant_id' => $order->merchant_id,
                'out_trade_no' => $data['out_trade_no'],
                'refund_amount' => $data['refund_amount']
            ];
            $return = $this->refund_public($refund_data);

            return $return;
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ]);
        }
    }

    public function order_query_org(Request $request)
    {
        try {
            $orgId = $request->get('orgId');
            $data = $request->get('data');
            $sign = $request->get('sign');
            $storeId = $request->get('storeId');
            if ($orgId == '') {
                return json_encode([
                    'status' => 2,
                    'message' => 'orgId不能为空'
                ]);
            }
            if ($storeId == '') {
                return json_encode([
                    'status' => 2,
                    'message' => 'storeId不能为空'
                ]);
            }
            if (!$data['out_trade_no']) {
                return json_encode([
                    'status' => 2,
                    'message' => 'out_trade_no不能为空'
                ]);
            }
            if ($sign == '') {
                return json_encode([
                    'status' => 2,
                    'message' => 'sign不能为空'
                ]);
            }
            $userKeyUrl = UserKeyUrl::where('org_id', $orgId)->first();
            if (!$userKeyUrl) {
                return json_encode([
                    'status' => 2,
                    'message' => '代理商不存在',
                ]);
            }
            $payBaseController = new PayBaseController();
            $verify = $payBaseController->verifySign($data, $sign, $userKeyUrl->public_key);//验签
            if (!$verify) {
                return json_encode([
                    'status' => 2,
                    'message' => '验签失败',
                ]);
            }
            $out_trade_no = $data['out_trade_no'];
            $a = array('a', '_', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
            $b = str_ireplace($a, "", $out_trade_no);
            $day = substr($b, 0, 8);
            $table = 'orders_' . $day;

            if (env('DB_D1_HOST')) {
                if (Schema::hasTable($table)) {
                    $data = DB::connection("mysql_d1")->table($table)->orWhere('out_trade_no', $out_trade_no)
                        ->orWhere('trade_no', $out_trade_no)
                        ->where('store_id', $storeId)
                        ->select('store_id', 'out_trade_no', 'trade_no', 'ways_source', 'total_amount', 'receipt_amount', 'pay_time', 'pay_status')
                        ->first();
                } else {
                    $data = DB::connection("mysql_d1")->table("orders")->orWhere('out_trade_no', $out_trade_no)
                        ->orWhere('trade_no', $out_trade_no)
                        ->where('store_id', $storeId)
                        ->select('store_id', 'out_trade_no', 'trade_no', 'ways_source', 'total_amount', 'receipt_amount', 'pay_time', 'pay_status')
                        ->first();
                }
            } else {
                if (Schema::hasTable($table)) {
                    $data = DB::table($table)->orWhere('out_trade_no', $out_trade_no)
                        ->orWhere('trade_no', $out_trade_no)
                        ->where('store_id', $storeId)
                        ->select('store_id', 'out_trade_no', 'trade_no', 'ways_source', 'total_amount', 'receipt_amount', 'pay_time', 'pay_status')
                        ->first();
                } else {
                    $data = DB::table('orders')->orWhere('out_trade_no', $out_trade_no)
                        ->orWhere('trade_no', $out_trade_no)
                        ->where('store_id', $storeId)
                        ->select('store_id', 'out_trade_no', 'trade_no', 'ways_source', 'total_amount', 'receipt_amount', 'pay_time', 'pay_status')
                        ->first();
                    if (!$data) {
                        $data = DB::table('orders')->orWhere('out_trade_no', $out_trade_no)
                            ->orWhere('trade_no', $out_trade_no)
                            ->where('store_id', $storeId)
                            ->select('store_id', 'out_trade_no', 'trade_no', 'ways_source', 'total_amount', 'receipt_amount', 'pay_time', 'pay_status')
                            ->first();
                        if (!$data) {
                            $data = DB::table('orders')->orWhere('out_trade_no', $out_trade_no)
                                ->orWhere('trade_no', $out_trade_no)
                                ->where('store_id', $storeId)
                                ->select('store_id', 'out_trade_no', 'trade_no', 'ways_source', 'total_amount', 'receipt_amount', 'pay_time', 'pay_status')
                                ->first();
                        }
                    }
                }
            }

            // if (!$data && Schema::hasTable('orders2020')) {
            //     $data = DB::table('orders2020')
            //         ->orWhere('out_trade_no', $out_trade_no)
            //         ->where('store_id', $storeId)
            //         ->select('store_id', 'out_trade_no', 'trade_no', 'ways_source', 'total_amount', 'receipt_amount', 'pay_time', 'pay_status')
            //         ->first();
            //     if (!$data) {
            //         $data = DB::table('orders2020')
            //             ->where('trade_no', $out_trade_no)
            //             ->where('store_id', $storeId)
            //             ->select('store_id', 'out_trade_no', 'trade_no', 'ways_source', 'total_amount', 'receipt_amount', 'pay_time', 'pay_status')
            //             ->first();
            //         if (!$data) {
            //             $data = DB::table('orders2020')
            //                 ->where('auth_code', $out_trade_no)
            //                 ->where('store_id', $storeId)
            //                 ->select('store_id', 'out_trade_no', 'trade_no', 'ways_source', 'total_amount', 'receipt_amount', 'pay_time', 'pay_status')
            //                 ->first();
            //         }
            //     }
            // }

            if (!$data) {
                return json_encode([
                    'status' => 2,
                    'message' => '订单号不存在'
                ]);
            }

            try {
                if ($data->pay_status == 2) {
                    $order_foreach_public = [
                        'out_trade_no' => $data->out_trade_no,
                        'store_id' => $data->store_id,
                        'ways_type' => $data->ways_type,
                        'config_id' => $data->config_id,
                        'table' => $table,
                    ];
                    $this->order_foreach_public($order_foreach_public);
                }
            } catch (\Exception $exception) {

            }

            $new_trade_no = $data->out_trade_no;  //核销功能，返回ERP的原始单号
            $new_data = [
                "store_id" => $data->store_id,
                "out_trade_no" => $data->trade_no,
                "trade_no" => substr($new_trade_no, 0, strlen($new_trade_no) - 3),
                "ways_source" => $data->ways_source,
                "total_amount" => $data->total_amount,
                "receipt_amount" => $data->receipt_amount,
                "pay_time" => $data->pay_time,
                "pay_status" => $data->pay_status,
            ];

            $this->status = 1;
            $this->message = '数据返回成功';

            return $this->format($new_data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }

    public function orderDetails(Request $request)
    {
        try {
            $out_trade_no = $request->get('out_trade_no', '');

            $orderData = DB::table('orders')->orWhere('out_trade_no', $out_trade_no)
                ->orWhere('trade_no', $out_trade_no)->first();

            if (!$orderData) {
                $this->status = 2;
                $this->message = '订单号不存在';
                return $this->format();
            }

            $data = [
                'store_name' => $orderData->store_name,
                'total_amount' => $orderData->total_amount,
                'receipt_amount' => $orderData->receipt_amount,
                'store_id' => $orderData->store_id,
                'pay_status' => $orderData->pay_status,
                'pay_status_desc' => $orderData->pay_status_desc
            ];

            $store_id = $orderData->store_id;
            $ad_p_id = 2; //微信成功页面

            $store_key_id = '';
            $config_id = '';
            $user_id = '';
            //广告

            $store = Store::where('store_id', $store_id)
                ->select('id', 'config_id', 'user_id')
                ->first();

            if ($store) {
                $store_key_id = $store->id;
                $config_id = $store->config_id;
                $user_id = $store->user_id;
            }

            $ad_data = [
                'config_id' => $config_id,
                'user_id' => $user_id,
                'store_key_id' => $store_key_id,
                'ad_p_id' => $ad_p_id
            ];

            $obj = new AdSelectController();
            $ad = $obj->ad_select($ad_data);

            if ($ad['status'] == 1) {
                $data['adData'] = $ad['data'];
            }

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }
}
