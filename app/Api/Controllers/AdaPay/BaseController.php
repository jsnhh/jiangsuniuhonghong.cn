<?php

namespace App\Api\Controllers\AdaPay;


use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class BaseController
{
    public $public_key = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAh+0nQKyePvMO9hji8AH01VvyfHr6Xje1Yc1HjO+GdmR3oOxylrjejvZLMTCrnMvW/ObqEnday6j5moLVCbJ2XGg15V9ML8l2wOREyP8Nf2D+21yNFRB3ivgu3JPPJu6b8Q5y/98CBfUffZ9tKga4P+IKGW7EaYpCYEnDOCXQU7ds2Vd7/TLcWug5ILtzqdFFDdp6EQbrNR4/VSGy25ZmN9/FCHXwDvMRD6mb5GW5Mj8WrXK2E6hLMYqKuqWcBDBdVgqIY3GAyuWQlgduoHE2H4L7/jh4o1c6BJCczTCHu6UyQydN6oIPvZXGStULPIcVCcDrwu6KhVHXyhqigCk6xQIDAQAB';
    public $scan_url = 'http://testterpay.postar.cn/yyfsevr/order/scanByMerchant';//商户主扫接口
    public $jsapi_url = 'http://testterpay.postar.cn/yyfsevr/order/pay';//客户主扫接口
    public $getPaypalTag_url = 'http://testterpay.postar.cn/yyfsevr/order/getPaypalTag';//获取银联标识接口
    public $refund_url = 'http://testterpay.postar.cn/yyfsevr/order/refund';//退款接口
    public $tradeQuery_url = 'http://testterpay.postar.cn/yyfsevr/order/tradeQuery';//订单查询接口
    public $refundQuery_url = 'http://testterpay.postar.cn/yyfsevr/order/refundQuery';//退款查询接口
    public $getCustResult_url = 'http://testterpay.postar.cn/yyfsevr/addCust/getResult';//查询商户审核接口
    public $private_key = 'MIIEowIBAAKCAQEAnlGKzchX0aOn9xRVuRgQRp7Oc0a5ArogQA1tOcf7sSq+M2+2bTmCxQChBrwJAo7d68jiZKRzYLTfy40gEXwc+wLDPubnyPrLnoVDubVxNXOLdF6lpm28wOO5xn1Daqd+E9l+wjhcbcUvVOSU12SXcfmwCh4lsdbTyeOFB8qlqzPvT4lGSjD+nZyiAwBgoYD13Pb2ipdAhOLfwyicv5+iWTnjNY3ZQ+5C5iajXyqXheE8/u5Gc0f+xUJx4ibYtEGx3DY/Ix7+9U2p/a9DV0KMP2xb+WPdYctqtQzX1Su3VrCGGfVCHnZGwDEvQ5eBM66/2UljDCB08TO1snu16TULPQIDAQABAoIBADzVuo+Of2hYCa/gyXvhakeouE4FjAwnN0NFpYYBeY6/jOy47ZGU+tH2hunOkWvBODdlLtFbe9ZaSnCuHvd5CK3PhvUBx2ksLdgQ/1A6D3Fpn9mskNyd6Fz6ZKWH9sLh74lzFslM3P91ONXXK/aEeclbF7EbmoJ2uVUnrzOqQ/S/2lhkgycoaCj1OC8fID5dQzmZcBVx+d++B2N96as2vqCuOW2pBTAciDIVTJNomzcahvu3fiVSswJ5j08LX8ZxT5v3soET3eo40pZMzC8H03y6LDEKsig1588YcEnGizgH8622lLnrOW3IAedSCr1bwOMQ98hWvc5r2vrZ4xoPBkUCgYEAz3nJyjUhq3KgCeDNnuhTdqZ+fL1ZrOk9T7+OM3mW/WHThQXQ4ayFwOU9F30CZLImphskUk+ebZnIl8M+/8+klzhjRgty5lfzTjbZCREKoegcqczeKLLzpAzi9K8FHlpJYHkT9+E5QsLc1BFpJwAUbwjx6Hwqxz9BUcSCovDVyTMCgYEAw1iPao9loz0RQmyOb7ufWw4gdyEdvrFPgnAOh/ZSOXs382NS7nOEMh26CTKV1pZkb2d4FNgIL1rSThVnb/c3QSeOjcbSUZ9T58FvW20Pn7hTMZuIGlRjit+AqCfr2cklcyJgd5poDZstI/5Y56bTIO+NMfyyFgXbZuFuEgFJOc8CgYALWW0pa4ig+U852xtEYw8VwXvP1QfPxp58+0yLEk6pI63PbAcGIUrbx251F3iHC5vDoF53RjWzZyMq8hpDIhet0f7UORIa+Zqsn6F1toHvY2QWAcfxUAC166KKofxUsbmr1lO6To60UTaFP0KSSJXVkRaykgBBD/vBvk8rHKOFMwKBgQCxSfPHW2N8FlVOuqU/BMIo6pZOaDDNu7CUiky12rlfT9REwXUn0pE8xrvr0wwoWA/JB9f02uf6ymup0EUzWeO870Cyrap9x6Nn56fACygt5iUjFawdcU9yX0wt0SV9X8/sQVvZ0Ln090utqg5YF5DwCFlV76hU0nYAVkIGd0JVAwKBgCOkJvO1jlE3e2f3KqdtiL9rl2JoUohdSPXrDONn6NwyBOS57bSj0U/4wu1x1zZnKcJZzol+5prnSg/ijfGlvJ3sTw1XGI9m3pYlh/5/Jk/cVox5MjQZwLt0UDoUob8Ft0LtWTNn36UsvwVeDaU3HF+y5zitBYdzbgZ53XFXTREt';


    public function getSign(array $array)
    {
        ksort($array);
        $bufSignSrc = $this->ToUrlParams($array);
        $private_key = chunk_split($this->private_key, 64, "\n");
        $key = "-----BEGIN RSA PRIVATE KEY-----\n" . wordwrap($private_key) . "-----END RSA PRIVATE KEY-----";
        //   echo $key;
        if (openssl_sign($bufSignSrc, $signature, $key)) {
            //		echo 'sign success';
        } else {
            echo 'sign fail';
        }
        $sign = base64_encode($signature);//加密后的内容通常含有特殊字符，需要编码转换下，在网络间通过url传输时要注意base64编码是否是url安全的
        return $sign;

    }


    public function ToUrlParams(array $array)
    {
        $buff = "";
        foreach ($array as $k => $v) {
            if ($v != "" && !is_array($v)) {
                $buff .= $k . "=" . $v . "&";
            }
        }

        $buff = trim($buff, "&");
        return $buff;
    }

    public function request($params, $url)
    {
        $ch = curl_init();
        $this_header = array("content-type: application/x-www-form-urlencoded;charset=UTF-8");
        curl_setopt($ch,CURLOPT_HTTPHEADER,$this_header);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);//如果不加验证,就设false,商户自行处理
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        $output = curl_exec($ch);
        curl_close($ch);
        return  $output;
    }
}