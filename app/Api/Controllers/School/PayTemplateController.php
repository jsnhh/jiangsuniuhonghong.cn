<?php

namespace App\Api\Controllers\School;
use App\Logic\PrimarySchool\ChildItem;
use App\Models\Merchant;
use App\Models\StuGrade;
use App\Models\StuOrderBatch;
use App\Models\StuOrderType;
use App\Models\StuStore;
use Illuminate\Support\Facades\DB;

/*
    缴费模板
*/
class PayTemplateController extends \App\Api\Controllers\BaseController
{
    private function makeNo(){
        return str_random(12);
    }

/*
        导入缴费模板

        只要一个失败，全部撤销  

        缴费名称 不能为空
*/
    public function importExcel(){
        $have_ok_id=[];
        try{

            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $this->status=2;

            $merchant_id=$loginer->merchant_id;

            $this->status=1;
            $this->message='缴费模板已经全部导入！';
            return $this->format();


        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='导入全部失败：'.$e->getMessage().$e->getFile().$e->getLine();
        }

        try{
            if(!empty($have_ok_id))
            {
                StuOrderType::whereIn('id',$have_ok_id)->delete();
            }
        }catch(\Exception $e)
        {

        }
    
        return $this->format();

    }


    /*
        抽取公共添加的方法

    */
    private static function addEach($cin,$merchant_id){

        $validate=\Validator::make($cin, [
                'store_id'=>'required',
                'stu_order_type_no'=>'required',
                'charge_name'=>'required',
                'charge_desc'=>'required',
                'amount'=>'required',
        ], [
          'required' => ':attribute为必填项！',
          'min' => ':attribute长度不符合要求！',
          'max' => ':attribute长度不符合要求！',
          'unique' => ':attribute已经被人占用！',
          'exists' => ':attribute不存在！'
        ], [
        'store_id'=>'学校编号',
        'stu_order_type_no'=>'缴费模板编号',
        'charge_name'=>'名称',
        'charge_desc'=>'描述',
        'amount'=>'总收费金额',
        ]);

        if($validate->fails())
        {
            return ['status'=>2,'message'=>$validate->getMessageBag()->first()];
        }

        //检查是否有重复模板
        $have = StuOrderType::where('store_id',$cin['store_id'])
        ->where('charge_name',$cin['charge_name'])
        ->first();

        if(!empty($have))
        {
            return ['status'=>2,'message'=>'缴费模板已经存在！'];
        }

        $cin['status']=1;
        $cin['status_desc']='自动审核成功';

        $add=StuOrderType::create($cin);

        return ['status'=>1,'message'=>'ok','data'=>$add];

    }

/*
    子项验证并处理

*/
    private function HandleItem($str){

          $child_item=json_decode($str,true);
          if(empty($child_item))
          {
            return ['status'=>2,'message'=>'子项不能为空！'];
          }

        /*
         charge_item
        json字符串示例

        项目列表json格式包含

        item_serial_number 缴费序列号
        item_name 缴费项目名称
        item_price 缴费项目金额
        item_mandatory 缴费是否为必填
        item_number 件数

        */
          $can_use_item=[];
          $amount=0;
          $item_serial_number=1;
          foreach($child_item as $v)
          {
            if(empty($v))
            {
                return ['status'=>2,'message'=>'子项设置错误！'];
            }

            if(empty($v['item_price']))
            {
                return ['status'=>2,'message'=>'子项金额没有设置！'];
            }

            $v['item_price'] = number_format($v['item_price'],2);
            if($v['item_price']<=0)
            {
                return ['status'=>2,'message'=>'子项金额不正确！'];
            }

            if(empty($v['item_number']))
            {
                return ['status'=>2,'message'=>'子项件数没有设置！'];
            }
            $v['item_number']=(int)$v['item_number'];
            if($v['item_number']<1)
            {
                return ['status'=>2,'message'=>'子项件数不正确！'];
            }

            if(empty($v['item_name']))
            {
                return ['status'=>2,'message'=>'子项名称没有设置！'];
            }

            if(empty($v['item_mandatory']))
            {
                return ['status'=>2,'message'=>'子项未设置是否必交！'];
            }

            if(!in_array($v['item_mandatory'], ['Y','N']))
            {
                return ['status'=>2,'message'=>'子项设置必交值错误(Y/N)！'];
            }

            $amount += $v['item_price']*$v['item_number'];

            $can_use_item[]=[
                'item_serial_number'=>$item_serial_number,
                'item_name'=>$v['item_name'],
                'item_price'=>$v['item_price'],
                'item_mandatory'=>$v['item_mandatory'],
                'item_number'=>$v['item_number'],            
            ];

            $item_serial_number++;

          }

          $num=1;
          
          foreach($can_use_item as &$v)
          {
            $v['item_serial_number']=$num;
            $num++;
          }

          return ['status'=>1,'all_item'=>$can_use_item,'amount'=>$amount];

    }

    /*
        添加
    */
    public function add(){

        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $store_id= $request->get('store_id');
            $school_no = $request->get('school_no','');
            $stu_grades_no = $request->get('stu_grades_no','');
            $charge_name = $request->get('charge_name','');
            $charge_desc = $request->get('charge_desc','');
            $item_name = $request->get('item_name','');
            $item_number = $request->get('item_number', 1);
            $item_mandatory = $request->get('item_mandatory', 'Y');
            $amount = $request->get('amount','');

            $this->status=2;

            $merchant_id=$loginer->merchant_id;

            $have = StuOrderType::where('store_id',$store_id)
                ->where('school_no',$school_no)
                ->where('charge_name',$charge_name)
                ->first();
            if(!empty($have))
            {
                $this->message='缴费模板已经存在！';
                return $this->format();
            }

            $cin=[
                'store_id'=>$store_id,
                'school_no'=>$school_no,
                'stu_grades_no'=>$stu_grades_no,
                'merchant_id'=>$merchant_id,
                'stu_order_type_no'=>$this->makeNo(),
                'charge_name'=>$charge_name,
                'charge_desc'=>$charge_desc,
                'item_name'=>$item_name,
                'item_number'=>$item_number,
                'amount'=>$amount,
                'item_mandatory'=>$item_mandatory,
                'status'=>1,
                'status_desc'=>'自动审核成功',
            ];

              $validate=\Validator::make($cin, [
                        'store_id'=>'required',
                        'school_no'=>'required',
                        'stu_grades_no'=>'required',
                        'stu_order_type_no'=>'required',
                        'charge_name'=>'required',
                         'charge_desc'=>'required',
                         'item_name'=>'required',
                         'item_number'=>'required',
                         'amount'=>'required',
                         'status_desc'=>'required',
              ], [
                  'required' => ':attribute为必填项！',
                  'min' => ':attribute长度不符合要求！',
                  'max' => ':attribute长度不符合要求！',
                  'unique' => ':attribute已经被人占用！',
                  'exists' => ':attribute不存在！'
              ], [
                'school_no'=>'学校编号',
                'stu_grades_no'=>'年级',
                'stu_order_type_no'=>'缴费模板编号',
                'charge_name'=>'模版名称',
                'charge_desc'=>'模版描述',
                'item_name'=>'缴费名称',
                'item_number'=>'数量',
                'amount'=>'收费金额',
              ]);

          if($validate->fails())
          {
            $this->message=$validate->getMessageBag()->first();
            return $this->format();
          }

          // 学校信息录入支付宝
//          $parseChild = ChildItem::parse($child);
//          if($parseChild['status']!=1)
//          {
//            $this->message=$parseChild['message'];
//            return $this->format();
//          }
//
//          $cin['charge_item']=json_encode($parseChild['all_item']);
//          $cin['amount']=$parseChild['amount'];

            $grade = StuOrderType::create($cin);

            $this->status=1;
            $this->message='缴费模板添加成功。';
            return $this->format();

        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }



    /*
        修改
    */
    public function save(){

        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $store_id= $request->get('store_id');
            $school_no = $request->get('school_no','');
            $stu_grades_no = $request->get('stu_grades_no','');
            $charge_name = $request->get('charge_name','');
            $charge_desc = $request->get('charge_desc','');
            $item_name = $request->get('item_name','');
            $item_number = $request->get('item_number', 1);
            $item_mandatory = $request->get('item_mandatory', 'Y');
            $amount = $request->get('amount','');

            $this->status=2;

            $obj =  StuOrderType::where('stu_order_type_no',$request->get('stu_order_type_no'))->first();

            if(empty($obj))
            {
                $this->message='模板不存在！';
                return $this->format();
            }

//            if($obj->status==1)
//            {
//                $this->message='模板已经审核通过了，无法修改！';
//                return $this->format();
//            }

            $cin=[
                'store_id'=>$store_id,
                'school_no'=>$school_no,
                'stu_grades_no'=>$stu_grades_no,
                'charge_name'=>$charge_name,
                'charge_desc'=>$charge_desc,
                'item_name'=>$item_name,
                'item_number'=>$item_number,
                'amount'=>$amount,
                'item_mandatory'=>$item_mandatory,
            ];

//          $cin=array_filter($cin);
//
//          if(!empty($child))
//          {
//              $parseChild = ChildItem::parse($child);
//              if($parseChild['status']!=1)
//              {
//                $this->message=$parseChild['message'];
//                return $this->format();
//              }
//
//              $cin['charge_item']=json_encode($parseChild['all_item']);
//              $cin['amount']=$parseChild['amount'];
//
//          }
            if(empty($cin)) {
                $this->message='请传入要修改的参数！';return  $this->format();
            }

            $validate=\Validator::make($cin, [
                'store_id'=>'required',
                'school_no'=>'required',
                'stu_grades_no'=>'required',
                'charge_name'=>'required',
                'item_name'=>'required',
                'amount'=>'required',
            ], [
                'required' => ':attribute为必填项！',
                'min' => ':attribute长度不符合要求！',
                'max' => ':attribute长度不符合要求！',
                'unique' => ':attribute已经被人占用！',
                'exists' => ':attribute不存在！'
            ], [
                'school_no'=>'学校编号',
                'stu_grades_no'=>'年级',
                'charge_name'=>'模版名称',
                'item_name'=>'缴费名称',
                'amount'=>'收费金额',
            ]);

            if($validate->fails())
            {
                $this->message=$validate->getMessageBag()->first();
                return $this->format();
            }

            $obj = $obj->update($cin);

            $this->status=1;
            $this->message='修改缴费模板成功';
            return $this->format();

        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }

    /*
        列表
    */
    public function lst(){

        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $merchant_id=$loginer->merchant_id;
            $store_id = $request->get('store_id');
            $school_no =  $request->get('school_no','');
            $stu_grades_no = $request->get('stu_grades_no','');
            $this->status=2;

            // 学校
            $get_all_school = StuStore::where('store_id',$store_id)->get();
            $all_school=[];
            foreach($get_all_school as $v)
            {
                $all_school[$v->school_no]=$v->school_name;
            }

            // 班级
            $get_all_grade = StuGrade::where('store_id',$store_id)->get();
            if(!empty($school_no))
            {
                $get_all_grade = $get_all_grade->where('school_no', $school_no);
            }

            $all_grade=[];
            foreach($get_all_grade as $v)
            {
                $all_grade[$v->stu_grades_no]=$v->stu_grades_name;
            }

            $obj =  new StuOrderType;
            $obj=$obj->where('store_id',$store_id);

            if(!empty($request->get('status')))
            {
                $obj =$obj-> where('status',$request->get('status'));
            }
            if(!empty($school_no))
            {
                $obj =$obj-> where('school_no',$school_no);
            }
            if(!empty($stu_grades_no))
            {
                $obj =$obj-> where('stu_grades_no',$stu_grades_no);
            }

            if(!empty($request->get('start_time')))
            {
                $obj =$obj->where('created_at','>=',$request->get('start_time'));
                
            }
            if(!empty($request->get('end_time')))
            {
                $obj =$obj->where('created_at','<=',$request->get('end_time'));
                
            }

            $obj=$obj->orderBy('id','desc');

            $cout=[];

            $get_all_store = StuStore::where('store_id',$store_id)->get();
            $all_store=[];
            foreach($get_all_store as $v)
            {
                $all_store[$v->school_no] = $v;
            }

            $get_all_merchant = Merchant::where('id',$merchant_id)->get();
            $all_merchant=[];
            foreach($get_all_merchant as $v)
            {
                $all_merchant[$v->id] = $v;
            }

            $this->t=$obj->count();

            $data=$this->page($obj)->get();
 
            if(!$data->isEmpty())
            {
                $cout=array_map(function($each) use ($all_store,$all_grade,$all_merchant){
                    return array_merge($each,[
                        'school_name'=>isset($all_store[$each['school_no']]) ? $all_store[$each['school_no']]->school_name : '',
                        'grade_name'=>isset($all_grade[$each['stu_grades_no']]) ? $all_grade[$each['stu_grades_no']] :'',
                        'merchant_name'=>isset($all_merchant[$each['merchant_id']]) ? $all_merchant[$each['merchant_id']]->name : ''

                        ]);

                },$data->toArray());
            }

            $this->status=1;
            $this->message='ok';
            return $this->format($cout);

        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }


    /*
        单条
    */
    public function show(){

        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $stu_order_type_no = $request->get('stu_order_type_no');
            $this->status=2;

            $obj =  new StuOrderType;

            $obj =$obj->where('stu_order_type_no',$stu_order_type_no);

            $template =$obj->first();

            if(empty($template))
            {
                $this->message='缴费模板不存在！';
                return $this->format();
            }

            $store = StuStore::where('store_id',$template->store_id)->where('school_no',$template->school_no)->first();
            $stu_grades = StuGrade::where('store_id',$template->store_id)->where('school_no',$template->school_no)->first();

            $obj->school_name='学校';
            $obj->merchant_name='创建人';

            $template->school_name=isset($store->school_name) ? $store->school_name : '';
            $template->grades_name=isset($stu_grades->stu_grades_name) ? $stu_grades->stu_grades_name : '';

            $this->status=1;
            $this->message='ok';
            return $this->format($template);


        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }


    /*
        审核
    */
    public function check(){

        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $this->status=2;

            if($loginer->merchant_type !=1)
            {
                $this->message='您没有权限审核';
                return $this->format();
            }

            $obj =  new StuOrderType;

            $obj =$obj->where('stu_order_type_no',$request->get('stu_order_type_no'));

            $obj =$obj->first();

            if(empty($obj))
            {
                $this->message='缴费模板不存在！';
                return $this->format();
            }

            if($obj->status!=2)
            {
                $this->message='缴费模板状态已经不在审核状态！';
                return $this->format();
            }

            $status=$request->get('status','');
            $status_desc=$request->get('status_desc','') ? $request->get('status_desc','') : '';

            if(!in_array($status,[1,3]))
            {
                $this->message='缴费模板审核状态不正确！';
                return $this->format();
            }

            $obj->status=$status;
 
            if($status==1)
            {
                $obj->status_desc= empty($status_desc) ? '审核成功' : $status_desc;

            }else{
                $obj->status_desc= empty($status_desc) ? '审核失败' : $status_desc;
            }

            $obj->update();

            $this->status=1;
            $this->message='操作成功！';
            return $this->format();

        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }


    /*
        删除
    */
    public function del(){

        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $this->status=2;

            $id = $request->get('id');

            $have = StuOrderType::where('id', $id)->first();

            if (empty($have)) {

                $this->message = '您要删除的缴费模板不存在！';
                return $this->format();
            }

            $haveOrderBatch = StuOrderBatch::where('stu_order_type_no', $have->stu_order_type_no)->first();
            if ($haveOrderBatch) {
                $this->message = '存在缴费项目，不允许删除！';
                return $this->format();
            }

            $ok = $have->delete();
            if (!$ok) {

                $this->message = '删除失败，请重试！';
                return $this->format();
            }

            $this->status = 1;
            $this->message = '删除成功！';
            return $this->format();

        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }


}
