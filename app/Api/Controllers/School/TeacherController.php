<?php

namespace App\Api\Controllers\School;
use App\Models\Merchant;
use App\Models\MerchantStore;
use App\Models\StuClass;
use App\Models\StuMerchantClass;
use App\Models\StuStore;
use App\Models\StuTeacher;
use Illuminate\Support\Facades\DB;

/*
*/
class TeacherController extends \App\Api\Controllers\BaseController
{

    public function typelst(){
        $this->status=1;
        $data=[
            ['name'=>'班主任','type'=>1],
            ['name'=>'老师','type'=>2],
        ];
         return $this->format($data);
    }


    /*
        列表
    */
    public function lst(){

        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $store_id = $request->get('store_id','');
            $this->status=2;

            // 学校
            $get_all_school = StuStore::where('store_id',$store_id)->get();
            $all_school=[];
            foreach($get_all_school as $v)
            {
                $all_school[$v->school_no]=$v->school_name;
            }

            $obj = StuTeacher::where('store_id',$store_id);

            if(!empty($request->get('name')))
            {
                $obj=$obj->where('name',$request->get('name'));
            }

            if(!empty($request->get('school_no')))
            {
                $obj=$obj->where('school_no',$request->get('school_no'));
            }

            $this->t=$obj->count();

            $data=$this->page($obj)->get();
            $cout=[];

            if(!$data->isEmpty())
            {
                $cout=array_map(function($each) use($all_school){
                    return array_merge($each,[
                        'school_name'=>isset($all_school[$each['school_no']]) ? $all_school[$each['school_no']] :''
                        ] );
                },$data->toArray());
            }

            $this->status=1;
            $this->message='ok';
            return $this->format($cout);

        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }

    /*
        教师和班级关联
    */
    public function relate(){
        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $this->status=2;

            $merchant_id=$loginer->merchant_id;

            $cin=[
                'store_id'=>$request->get('store_id'),
                'merchant_id'=>$request->get('merchant_id'),
                'type'=>$request->get('type'),
                'stu_class_no'=>$request->get('stu_class_no'),
            ];

              $validate=\Validator::make($cin, [
                        'store_id'=>'required|exists:stu_stores,store_id',
                        'merchant_id'=>'required|exists:merchants,id',
                        'type'=>'required',
                        'stu_class_no'=>'required|exists:stu_class,stu_class_no',
                    // 'cate_id'=>'required|exists:goods_cate,id',
              ], [
                  'required' => ':attribute为必填项！',
                  'min' => ':attribute长度不符合要求！',
                  'max' => ':attribute长度不符合要求！',
                  'unique' => ':attribute已经被人占用！',
                  'exists' => ':attribute不存在！'
              ], [
                        'store_id'=>'学校编号',
                        'merchant_id'=>'教师编号',

                        'type'=>'教师类型',
                        'stu_class_no'=>'班级编号',
              ]);

              if($validate->fails())
              {
                $this->message=$validate->getMessageBag()->first();
                return $this->format();
              }

              if(!in_array($cin['type'], [1,2])){

                $this->message='教师类型不正确';
                return $this->format();
              }

              $cin['type']==1 ? $cin['type']=3 : '';


            $have = StuMerchantClass::where('store_id',$cin['store_id'])->where('merchant_id',$cin['merchant_id'])->where('type',$cin['type'])->first();

            if(!empty($have))
            {
                $this->message='关联项已经存在！';
                return $this->format();
            }

            StuMerchantClass::create($cin);

            $this->status=1;
            $this->message='关联项创建成功';
            return $this->format();


        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }




    /*
        教师和班级取消关联
    */
    public function unbind(){
        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $this->status=2;

            $merchant_id=$loginer->merchant_id;

            $cin=[
                'store_id'=>$request->get('store_id'),
                'merchant_id'=>$request->get('merchant_id'),
                'type'=>$request->get('type'),
                'stu_class_no'=>$request->get('stu_class_no'),
            ];



              $validate=\Validator::make($cin, [
                        'store_id'=>'required',
                        'merchant_id'=>'required',
                        'type'=>'required',
                        'stu_class_no'=>'required',
                    // 'cate_id'=>'required|exists:goods_cate,id',
              ], [
                  'required' => ':attribute为必填项！',
                  'min' => ':attribute长度不符合要求！',
                  'max' => ':attribute长度不符合要求！',
                  'unique' => ':attribute已经被人占用！',
                  'exists' => ':attribute不存在！'
              ], [
                        'store_id'=>'学校编号',
                        'merchant_id'=>'教师编号',

                        'type'=>'教师类型',
                        'stu_class_no'=>'班级编号',
              ]);

          if($validate->fails())
          {
            $this->message=$validate->getMessageBag()->first();
            return $this->format();
          }


          if(!in_array($cin['type'], [1,2])){

            $this->message='教师类型不正确';
            return $this->format();
          }


            $ok = StuMerchantClass::where('store_id',$cin['store_id'])->where('merchant_id',$cin['merchant_id'])->where('stu_class_no',$cin['stu_class_no'])->where('type',$cin['type'])->delete();

            $this->status=1;
            $this->message='已解除关联！';
            return $this->format();


        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }



    /*
        当前登录教师的资料
    */
    public function loginerInfo(){

        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));//merchant_name  姓名
            $this->status=2;


            $logo='';
            $merchant = Merchant::where('id',$loginer->merchant_id)->first();
            if(!empty($merchant))
            {
                $logo=$merchant->logo;
            }

            $store_id='';
            $get_mid = StuMerchantClass::where('merchant_id',$loginer->merchant_id)->orderBy('type','desc')->get();
            $teacher_type=2;
            $teacher_type_name='';
            $school=null;


            // 班级以及教师职称
                $all_class=[];
                foreach($get_mid as $v)
                {
                    if(!isset($all_class[$v->stu_class_no]))
                        $all_class[$v->stu_class_no]= $v->type;
                }

                $cout=[];

                if(!empty($all_class))
                {

                    $get_data = StuClass::leftJoin('stu_grades','stu_grades.stu_grades_no','stu_class.stu_grades_no')->whereIn('stu_class.stu_class_no',array_keys($all_class))->get();

                    foreach($get_data as $v)
                    {
                        $cout[]=[
                            'stu_grades_no'=>$v->stu_grades_no,
                            'stu_class_no'=>$v->stu_class_no,
                            'stu_class_name'=>$v->stu_class_name,
                            'stu_grades_name'=>$v->stu_grades_name,
                            // 在当前班级的职称
                            'type'=>isset($all_class[$v->stu_class_no]) ? $all_class[$v->stu_class_no] : '',
                            'type_name'=>(isset($all_class[$v->stu_class_no]) && $all_class[$v->stu_class_no]==3) ? '班主任' : '教师'
                        ];
                    }

              
                }


            $grade_name='';
            $class_name='';
            if(!$get_mid->isEmpty())
            {
                $mid=$get_mid[0];

                $school = StuStore::where('store_id',$mid->store_id)->first();
                $teacher_type = $mid->type;
                $store_id=$mid->store_id;
            }
            $teacher_type_name = ($teacher_type==3) ? '班主任' : '教师';


            $data=[
                'store_id'=>$store_id,
                'school_name'=>empty($school) ? '' :$school->school_name,
                
                'name'=>$loginer->merchant_name,
                'type'=>$teacher_type,
                'type_name'=>$teacher_type_name,
                'logo'=>$logo,

                'all_class'=>$cout
            ];


            $this->status=1;
            $this->message='ok';
            return $this->format($data);


        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }


    /*
    */
    public function loginerClassLst(){

        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $this->status=2;

            $mid = StuMerchantClass::where('merchant_id',$loginer->merchant_id)->get();

            if(empty($mid))
            {
                $this->message='当前教师未分配班级。';
                return $this->format();
            }
            $all_class=[];
            foreach($mid as $v)
            {
                $all_class[]=$v->stu_class_no;
            }

            $data = StuClass::whereIn('stu_class_no',$all_class)->get();
            if(($data->isEmpty()))
            {
                $this->message='当前教师未分配班级。';
                return $this->format();
            }

            $this->status=1;
            $this->message='ok';
            return $this->format($data);


        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }

    /*
        添加
    */
    public function add(){

        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $password = $request->get('password', '');
            $this->status=2;

            $merchant_id=$loginer->merchant_id;

            $have = StuTeacher::where('store_id',$request->get('store_id'))->where('name',$request->get('name'))->where('phone',$request->get('phone'))->first();

            if(!empty($have))
            {
                $this->message='教师已经存在！';
                return $this->format();
            }
            if ($password == "") {
                $password = '000000';
            }

            //验证密码
//            if (strlen($password) < 6) {
//                return json_encode([
//                    'status' => 2,
//                    'message' => '密码长度不符合要求'
//                ]);
//            }

            $cin=[
                'merchant_id'=>$merchant_id,//创建者id
                'store_id'=>$request->get('store_id',''),
                'school_no'=>$request->get('school_no',''),
                'name'=>$request->get('name',''),
                'phone'=>$request->get('phone',''),
                'status'=>$request->get('status',''),
                'password' => bcrypt($password),

            ];

              $validate=\Validator::make($cin, [
                        'store_id'=>'required',
                        'school_no'=>'required',
                        'name'=>'required',
                        'phone'=>'required',
              ], [
                  'required' => ':attribute为必填项！',
                  'min' => ':attribute长度不符合要求！',
                  'max' => ':attribute长度不符合要求！',
                  'unique' => ':attribute已经被人占用！',
                  'exists' => ':attribute不存在！'
              ], [
                        'name'=>'教师姓名',
                        'store_id'=>'学校编号',
                        'school_no'=>'学校',
                        'phone'=>'手机号',
              ]);

              if($validate->fails())
              {
                $this->message=$validate->getMessageBag()->first();
                return $this->format();
              }

              $grade = StuTeacher::create($cin);


              $this->status=1;
              $this->message='教师创建成功';
              return $this->format();

        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }



    /*
        修改
    */
    public function save(){

        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $this->status=2;

            $merchant_id=$loginer->merchant_id;

          $class = StuClass::where('stu_class_no',$request->get('stu_class_no'))->first();

          if(empty($class))
          {
            $this->message='班级不存在！';
            return  $this->format();
          }

            $cin=[
                'store_id'=>$request->get('store_id',''),
                'stu_grades_no'=>$request->get('stu_grades_no',''),
                'stu_class_name'=>$request->get('stu_class_name',''),
                'stu_class_desc'=>$request->get('stu_class_desc',''),
            ];

          $cin=array_filter($cin);

          if(empty($cin))
          {

            $this->message='请传入要修改的参数！';
            return  $this->format();
          }

          $class=$class->update($cin);


            $this->status=1;
            $this->message='修改班级成功';
            return $this->format();


        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }


    /*
        单条
    */
    public function show(){


        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $this->status=2;

            $stu_class_no=$request->get('stu_class_no','');

            $grade =  StuClass::where('stu_class_no',$stu_class_no)->first();

            if(empty($grade))
            {
                $this->message='班级不存在！';
                return $this->format();
            }

            $this->status=1;
            $this->message='ok';
            return $this->format($grade);


        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }


    /*
        删除
    */
    public function del(){

        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $this->status=2;

            $id = $request->get('id');

            $have = StuTeacher::where('id', $id)->first();

            if (empty($have)) {

                $this->message = '您要删除的教师不存在！';
                return $this->format();
            }

            $ok = $have->delete();
            if (!$ok) {

                $this->message = '删除失败，请重试！';
                return $this->format();
            }

            $this->status = 1;
            $this->message = '删除成功！';
            return $this->format();

        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }


}
