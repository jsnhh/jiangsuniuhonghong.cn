<?php

namespace App\Api\Controllers\School\Agent;
use App\Models\StuOrder;
use App\Models\StuOrderItem;
use App\Models\StuStore;
use Illuminate\Support\Facades\DB;

/*
    学校  增删查改
*/
class OrderController extends \App\Api\Controllers\BaseController
{


    /*
        列表
    */
    public function lst(){

        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $this->status=2;

            $obj =  new StuOrder;

            if(!empty($request->get('user_id')))
            {
                $rec_id=$request->get('user_id');
            }else{
                $rec_id=$loginer->user_id;
            }

            $all_user_id = $this->getSubIds($rec_id);//返回包括自己的代理商id数组

            $all_store_id=[];

            if(!empty($request->get('store_id')))
            {
                $all_store_id = $request->get('store_id');
            }else{
                // 查找学校store_id
                $get_all_store = StuStore::whereIn('user_id',$all_user_id)->get();

                foreach($get_all_store as $v)
                {
                    $all_store_id[]=$v->store_id;
                }
            }

            $obj = $obj->leftJoin('stores', 'stores.merchant_id', 'stu_orders.merchant_id')
                ->select(DB::raw('stores.store_name,stu_orders.*'))
                ->whereIn('stu_orders.store_id', $all_store_id);

            if(!empty($request->get('store_name')))
            {
                $obj=$obj->where('stores.store_name','like','%'.$request->get('store_name').'%');
            }

            if (!empty($request->get('school_no'))) {
                $obj = $obj->where('stu_orders.school_no', $request->get('school_no'));
            }
            if(!empty($request->get('stu_grades_no')))
            {
                $obj=$obj->where('stu_orders.stu_grades_no',$request->get('stu_grades_no'));
            }

            if(!empty($request->get('stu_class_no')))
            {
                $obj=$obj->where('stu_orders.stu_class_no',$request->get('stu_class_no'));
            }

            if(!empty($request->get('student_name')))
            {
                $obj=$obj->where('stu_orders.student_name','like','%'.$request->get('student_name').'%');
            }

            if(!empty($request->get('stu_order_batch_no'))){
                $obj=$obj->where('stu_orders.stu_order_batch_no',$request->get('stu_order_batch_no'));
            }

            if(!empty($request->get('student_no'))){
                $obj=$obj->where('stu_orders.student_no',$request->get('student_no'));
            }

            if(!empty($request->get('school_name'))){
                $obj=$obj->where('stu_orders.school_name','like','%'.$request->get('school_name').'%');
            }

            if(!empty($request->get('out_trade_no'))){
                $obj=$obj->where('stu_orders.out_trade_no',$request->get('out_trade_no'));
            }

            if(!empty($request->get('pay_type'))){
                $obj=$obj->where('stu_orders.pay_type',$request->get('pay_type'));//支付类型，1000-官方支付宝扫码，1005-支付宝行业缴费，2000-微信缴费，2005-微信支付缴费
            }

            if(!empty($request->get('pay_status'))){
                $obj=$obj->where('stu_orders.pay_status',$request->get('pay_status'));//1 支付成功 ，2 等待支付，3 支付失败，4 关闭，5 退款中，6 已退款 7 有退款
            }

            if(!empty($request->get('gmt_start'))){
                $obj=$obj->where('stu_orders.gmt_start','>=',$request->get('gmt_start'));//
            }

            if(!empty($request->get('gmt_end'))){
                $obj=$obj->where('stu_orders.gmt_end','<=',$request->get('gmt_end'));//
            }


            // excel导出订单及明细
            if (!empty($request->get('excel'))) {
                $file_name = '订单流水';
                return self::orderExport($obj, $file_name);
            }


            $this->t=$obj->count();

            $data=$this->page($obj)->orderBy('id','desc')->get();

            foreach($data as &$v)
            {
                switch($v->pay_status){
                    case 1:
                        $v->pay_status_desc = $v->pay_status_desc='支付成功';
                        break;
                    case 2:
                        $v->pay_status_desc = $v->pay_status_desc='等待支付';
                        break;
                    case 3:
                        $v->pay_status_desc = $v->pay_status_desc='支付失败';
                        break;
                    case 4:
                        $v->pay_status_desc = $v->pay_status_desc='关闭';
                        break;
                    case 5:
                        $v->pay_status_desc = $v->pay_status_desc='退款中';
                        break;
                    case 6:
                        $v->pay_status_desc = $v->pay_status_desc='已退款';
                        break;
                    case 7:
                        $v->pay_status_desc = $v->pay_status_desc='有退款';
                        break;
                }
            }

// 1 支付成功 ，2  等待支付 ，3  支付失败 ，4 关闭，5 退款中，6 已退款 7 有退款

            $this->status=1;
            $this->message='ok';
            return $this->format($data);


        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }


    public static function orderExport($order, $file_name)
    {

        $data = $order->get();

        $excel_title = [
            '商户名称',
            '缴费名称',
            '所属学校',
            '年级',
            '班级',
            '学号',
            '学生姓名',
            '家长姓名',
            '家长手机号',
//            '项目总金额',
            '支付金额',
            '支付类型',
            '支付状态',
            '小项情况'
        ];
        $excel_data = [];

        foreach ($data as $v) {
            $item = StuOrderItem::where('out_trade_no', $v->out_trade_no)->get();
            // 小项情况统计
            $detail_str = '';
            foreach ($item as $vv) {

                $item_name = $vv->item_name;
                $item_mandatory = $vv->item_mandatory;
                $item_price = $vv->item_price;
                $status_desc = \App\Logic\PrimarySchool\Order::status($vv->status);

                $str = '%s-%s-%s元-%s；';

                $detail_str .= sprintf($str, $item_name, $item_mandatory, $item_price, $status_desc);
            }
            $excel_data[] = [
                $v->store_name,
                $v->batch_name,
                $v->school_name,
                $v->stu_grades_name,
                $v->stu_class_name,
                $v->student_no,
                $v->student_name,
                $v->student_user_name,
                $v->student_user_mobile,
//                $v->amount,
                $v->pay_amount,
                $v->pay_type_desc,
                \App\Logic\PrimarySchool\Order::status($v->pay_status),
                $detail_str
            ];

        }

        \App\Common\Excel\Excel::downExcel($excel_title, $excel_data, $file_name);

    }

    /*
        单条
    */
    public function show(){
        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $this->status=2;

            $obj =  new \App\Models\StuOrder;
            $obj =$obj->where('out_trade_no',$request->get('out_trade_no'));

            $data =$obj->first();

            if(empty($data))
            {
                $this->message='订单不存在！';
                return $this->format();
            }

            $all_item = \App\Models\StuOrderItem::where('out_trade_no',$data->out_trade_no)->get();

            foreach($all_item as &$v)
            {
                switch($v->status){
                    case 1:
                        $v->status_desc = $v->status_desc='支付成功';
                        break;
                    case 2:
                        $v->status_desc = $v->status_desc='等待支付';
                        break;
                    case 3:
                        $v->status_desc = $v->status_desc='支付失败';
                        break;
                    case 4:
                        $v->status_desc = $v->status_desc='关闭';
                        break;
                    case 5:
                        $v->status_desc = $v->status_desc='退款中';
                        break;
                    case 6:
                        $v->status_desc = $v->status_desc='已退款';
                        break;
                    case 7:
                        $v->status_desc = $v->status_desc='有退款';
                        break;
                }
            }



            $data->all_item=$all_item;

            $this->status=1;
            $this->message='ok';
            return $this->format($data);


        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }

}
