<?php

namespace App\Api\Controllers\School\Agent;
use App\Models\StuGrade;
use App\Models\StuStore;
use Illuminate\Support\Facades\DB;

/*
*/
class GradeController extends \App\Api\Controllers\BaseController
{

    /*
        列表
    */
    public function lst(){

        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $this->status=2;

            $grade =  new StuGrade;

            if(!empty($request->get('school_no')))
            {
                $grade = $grade->where('school_no',$request->get('school_no'));
            }

            $data=$this->page($grade)->get();

            $this->status=1;
            $this->message='ok';
            return $this->format($data);

        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }

}
