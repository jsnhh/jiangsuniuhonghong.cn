<?php

namespace App\Api\Controllers\School;
use App\Models\StuOrderBatch;
use Illuminate\Support\Facades\DB;
use App\Models\StuOrderType;

/*

*/
class StatPayController extends \App\Api\Controllers\BaseController
{


    /*
        教育缴费情况统计及导出
    */
    public function pay(){

        try{
            $request=app('request');
            $loginer = $this->parseToken($request->get('token'));
            $store_id = $request->get('store_id');
            $this->status=2;

            $obj = new StuOrderType;

            $obj = $obj
                ->leftJoin('stu_orders', 'stu_orders.stu_order_type_no', 'stu_order_types.stu_order_type_no')
                ->leftJoin('stu_order_batchs', 'stu_order_batchs.stu_order_type_no', 'stu_order_types.stu_order_type_no')
                ->leftJoin('stu_stores', 'stu_stores.school_no', 'stu_order_types.school_no');

            $obj = $obj->select(DB::raw('stu_order_types.item_name as charge_name,stu_stores.school_name,stu_orders.stu_order_batch_no,
                count( stu_orders.id ) AS have_pay_rs,
                stu_orders.pay_amount,
                sum( stu_orders.pay_amount ) AS have_pay_amount, 
                stu_order_batchs.batch_amount AS tot_should_pay '))
                ->where('stu_orders.pay_status',1)
                ->groupBy('stu_order_types.stu_order_type_no' , 'stu_stores.school_no');

            if(!empty($request->get('stu_grades_no'))){
                $obj=$obj->where('stu_orders.stu_grades_no',$request->get('stu_grades_no'));
            }
            if(!empty($store_id)){
                $obj=$obj->where('stu_orders.store_id',$request->get('store_id'));
            }

            if(!empty($request->get('stu_class_no'))){
                $obj=$obj->where('stu_orders.stu_class_no',$request->get('stu_class_no'));
            }

            if(!empty($request->get('stu_order_batch_no'))){
                $obj=$obj->where('stu_orders.stu_order_batch_no',$request->get('stu_order_batch_no'));
            }

            if(!empty($request->get('start_time'))){
                $obj=$obj->where('stu_order_batchs.created_at','>=',$request->get('start_time'));
            }

            if(!empty($request->get('end_time'))){
                $obj=$obj->where('stu_order_batchs.gmt_end','<=',$request->get('end_time'));
            }

            //导出
            if(!empty($request->get('export')) && $request->get('export')==1 ){
                $data = $obj->get();

                $title=[
                    '学校名称',
                    '缴费项目名称',
                    '已缴金额',
                    '已缴数量',
                ];
                $cout=[];
                foreach($data as $v)
                {
                    $cout[]=[
                        $v->school_name,
                        $v->charge_name,
                        number_format($v->have_pay_amount, 2, '.', ''),
                        $v->have_pay_rs,
                    ];
                }
                $file_name=!empty($request->get('export_name')) ? $request->get('export_name') : '缴费情况统计';

                \App\Common\Excel\Excel::downExcel($title,$cout,$file_name);
            }

            $new_data = $obj->get();
            foreach($new_data as $v)
            {
                $v['have_pay_amount'] = number_format($v->have_pay_amount, 2, '.', '');
                $v['not_pay_rs'] = number_format( ($v->tot_should_pay / $v->pay_amount) - $v->have_pay_rs, 0, '.', '');//未缴数量
                $v['not_pay_amount'] = number_format($v->tot_should_pay - $v->have_pay_amount, 2, '.', ''); //未缴金额

            }
            $this->t=$new_data->count();

            //$data=$this->page($obj)->get();

            $this->status=1;
            $this->message='ok';
            return $this->format($new_data);


        }catch(\Exception $e){
            $this->status= -1 ;
            $this->message='系统错误'.$e->getMessage().$e->getFile().$e->getLine();
            return $this->format();
        }
    }





}
