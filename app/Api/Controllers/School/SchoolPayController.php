<?php

namespace App\Api\Controllers\School;


use App\Api\Controllers\BaseController;
use App\Api\Controllers\Config\EasyPayConfigController;
use App\Api\Controllers\Config\EasySkPayConfigController;
use App\Api\Controllers\Config\PayWaysController;
use App\Models\EasypayStoresImages;
use App\Models\Merchant;
use App\Models\Store;
use App\Models\StuClass;
use App\Models\StuGrade;
use App\Models\StuOrder;
use App\Models\StuOrderBatch;
use App\Models\StuStudent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class SchoolPayController extends BaseController
{

    //学生缴费
    public function school_pay(Request $request)
    {
        try {
            $ways_type = $request->get('ways_type');
            $store_id = $request->get('store_id');
            $total_amount = $request->get('total_amount');
            $open_id = $request->get('open_id');
            $school_name = $request->get('school_name');
            $id = $request->get('id');//学生id
            $parent_name = $request->get('parent_name');//家长姓名
            $parent_phone = $request->get('parent_phone');//家长姓名
            $stu_order_batch_no = $request->get('stu_order_batch_no');
            $remark = $request->get('remark');//备注
            $student = StuStudent::where('id', $id)
                ->select()
                ->first();
            if (!$student) {
                return json_encode([
                    'status' => 2,
                    'message' => '学生不存在'
                ]);
            }
            $student_name = $student->student_name;
            $stuGrade = StuGrade::where('stu_grades_no', $student->stu_grades_no)->select('stu_grades_name','school_no')->first();
            $stuClass = StuClass::where('stu_class_no', $student->stu_class_no)->select('stu_class_name')->first();

            $where = [];
            $where[] = ['stu_class_no', 'like', '%' . $student->stu_class_no . '%'];

            $stuOrderBatch = StuOrderBatch::where('stu_grades_no', $student->stu_grades_no)
                ->where($where)
                ->where('stu_order_batch_no', $stu_order_batch_no)
                ->select('batch_amount', 'stu_order_batch_no', 'batch_name', 'stu_order_type_no', 'gmt_end')
                ->first();
            $store = Store::where('store_id', $store_id)->first();
            if (!$store) {
                return json_encode([
                    'status' => '2',
                    'message' => '支付失败，请联系学校'
                ]);
            }
            $current_date = date("Y-m-d H:i:s", time());
            if (strtotime($current_date) >= strtotime($stuOrderBatch->gmt_end)) {
                return json_encode([
                    'status' => '2',
                    'message' => '缴费时间已过，禁止缴费'
                ]);
            }
            $store_pid = $store->pid;
            $user_id = $store->user_id;
            $merchant_id = $store->merchant_id;
            $data_insert = [
                'store_id' => $store_id,
                'user_id' => $user_id,
                'school_name' => $school_name,
                'school_no' => $stuGrade->school_no,
                'merchant_id' => $merchant_id,
                'stu_grades_no' => $student->stu_grades_no,
                'stu_grades_name' => $stuGrade->stu_grades_name,
                'stu_class_no' => $student->stu_class_no,
                'stu_class_name' => $stuClass->stu_class_name,
                'student_no' => $student->student_no,
                'student_name' => $student_name,
                'student_user_mobile' => $parent_phone,
                'student_user_name' => $parent_name,
                'stu_order_batch_no' => $stuOrderBatch->stu_order_batch_no,
                'batch_name' => $stuOrderBatch->batch_name,
                'stu_order_type_no' => $stuOrderBatch->stu_order_type_no,
                'amount' => $total_amount,
                'pay_amount' => $total_amount,
                'receipt_amount' => $total_amount,
                'buyer_id' => $open_id,
                'gmt_start' => date('Y-m-d H:i:m', time()),
                'gmt_end' => $stuOrderBatch->gmt_end,
                'remind' => 0,
                'pay_status' => '2',
                'pay_status_desc' => '等待支付',
                'order_create_type' => 3,
                'remark' => $remark,
                'ways_type' => $ways_type
            ];
            if ($ways_type == '32001' || $ways_type == '32002') {
                $config = new EasySkPayConfigController();
                $easyskpay_config = $config->easyskpay_config('1001');
                if (!$easyskpay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生数科支付配置不存在请检查配置'
                    ]);
                }

                $easyskpay_merchant = $config->easyskpay_merchant($store_id, $store_pid);
                if (!$easyskpay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生数科支付商户号不存在'
                    ]);
                }

                $location = '';
                if (!$store->lat || !$store->lng) {
                    $storeController = new \App\Api\Controllers\User\StoreController();
                    $address = $store->province_name . $store->city_name . $store->area_name . $store->store_address;//获取经纬度的地址
                    $api_res = $storeController->query_address($address, env('LBS_KEY'));
                    if ($api_res['status'] == '1') {
                        $store_lng = $api_res['result']['lng'];
                        $store_lat = $api_res['result']['lat'];
                        $store->update([
                            'lng' => $store_lng,
                            'lat' => $store_lat,
                        ]);
                        $location = "+" . $store_lat . "/-" . $store_lng;
                    } else {
//                            Log::info('添加门店-获取经纬度错误');
//                            Log::info($api_res['message']);
                    }

                } else {
                    $location = "+" . $store->lat . "/-" . $store->lng;
                }

                $out_trade_no = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                $data = [];
                if ($ways_type == '32001') {
                    $out_trade_no = 'ali_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['pay_type'] = '1000';
                    $data_insert['pay_type_source'] = 'alipay';
                    $data_insert['pay_type_desc'] = '支付宝缴费';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['company'] = 'easyskpay';
                    $data['pay_type'] = 'ALIPAY';
                }

                if ($ways_type == '32002') {
                    $out_trade_no = 'wx_qr' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $data_insert['pay_type'] = '2000';
                    $data_insert['pay_type_source'] = 'weixin';
                    $data_insert['pay_type_desc'] = '微信缴费';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['company'] = 'easyskpay';
                    $data['pay_type'] = 'WECHAT';
                    $data['subAppid'] = $easyskpay_merchant->wx_appid ? $easyskpay_merchant->wx_appid : 'wx3fc7983c75d88330';
                }

                $obj = new \App\Api\Controllers\EasySkPay\PayController();
                $easypay_data = [
                    'org_id' => $easyskpay_config->org_id,
                    'mer_id' => $easyskpay_merchant->mer_id,
                    'request_no' => $out_trade_no,
                    'pay_type' => $data['pay_type'],
                    'amount' => $total_amount,
                    'open_id' => $open_id,
                    'notify_url' => url('/api/school/pay_notify_easySk'),
                    'location' => $location,
                    'sub_app_id' => $easyskpay_config->wx_appid
                ];
                $return = $obj->codePayByCtoB($easypay_data);
                if ($return['status'] == '1') {
                    $insert_re = StuOrder::create($data_insert);//添加到学生缴费表
                    $this->addOrder($data_insert);//添加到order表
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    if ($ways_type == 32001) { //支付宝
                        return json_encode([
                            'status' => '1',
                            'trade_no' => $return['data']['bizData']['prePayId'],
                            'data' => $return['data'],
                        ]);
                    } elseif ($ways_type == 32002) { //微信
                        $weChatData = json_decode($return['data']['bizData']['prePayId'], true);
                        if ($weChatData) {
                            if (isset($weChatData['timeStamp'])) $return['data']['payTimeStamp'] = $weChatData['timeStamp'];
                            if (isset($weChatData['package'])) $return['data']['payPackage'] = $weChatData['package'];
                            if (isset($weChatData['paySign'])) $return['data']['paySign'] = $weChatData['paySign'];
                            if (isset($weChatData['signType'])) $return['data']['paySignType'] = $weChatData['signType'];
                            if (isset($weChatData['nonceStr'])) $return['data']['paynonceStr'] = $weChatData['nonceStr'];
                        }

                        $return['data']['ordNo'] = $out_trade_no;
                        return json_encode([
                            'status' => '1',
                            'trade_no' => $return['data']['tradeNo'],
                            'data' => $return['data'],
                            'wx_data' => $weChatData
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'data' => $return['data'],
                        ]);
                    }

                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => $return['message']
                    ]);
                }
            }
            if ($ways_type == '21001' || $ways_type == '21002') {
                $config = new EasyPayConfigController();

                $easypay_config = $config->easypay_config($store->config_id);
                if (!$easypay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生支付配置不存在请检查配置'
                    ]);
                }

                $easypay_merchant = $config->easypay_merchant($store_id, $store_pid);
                if (!$easypay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生支付商户号不存在'
                    ]);
                }

                $location = '';
                if (!$store->lat || !$store->lng) {
                    $storeController = new \App\Api\Controllers\User\StoreController();
                    $address = $store->province_name . $store->city_name . $store->area_name . $store->store_address;//获取经纬度的地址
                    $api_res = $storeController->query_address($address, env('LBS_KEY'));
                    if ($api_res['status'] == '1') {
                        $store_lng = $api_res['result']['lng'];
                        $store_lat = $api_res['result']['lat'];
                        $store->update([
                            'lng' => $store_lng,
                            'lat' => $store_lat,
                        ]);
                        $location = "+" . $store_lat . "/-" . $store_lng;
                    } else {
//                            Log::info('添加门店-获取经纬度错误');
//                            Log::info($api_res['message']);
                    }

                } else {
                    $location = "+" . $store->lat . "/-" . $store->lng;
                }
                //获取到账标识
                $EasypayStoresImages = EasypayStoresImages::where('store_id', $store_id)
                    ->select('real_time_entry')
                    ->first();
                $patnerSettleFlag = '0'; //秒到
                if ($EasypayStoresImages) {
                    if ($EasypayStoresImages->real_time_entry == 1) {
                        $patnerSettleFlag = '0';
                    } else {
                        $patnerSettleFlag = '1';
                    }
                }
                $out_trade_no = substr($easypay_config->channel_id, 0, 4) . substr($easypay_config->channel_id, -4) . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%02d', rand(0, 99));
                $data = [];
                if ($ways_type == '21001') {
                    $data_insert['pay_type'] = '1000';
                    $data_insert['pay_type_source'] = 'alipay';
                    $data_insert['pay_type_desc'] = '支付宝缴费';
                    $data_insert['company'] = 'easypay';
                    $data['pay_type'] = 'ALIPAY';
                    $data['subAppid'] = '';
                }

                if ($ways_type == '21002') {
                    $data_insert['pay_type'] = '2000';
                    $data_insert['pay_type_source'] = 'weixin';
                    $data_insert['pay_type_desc'] = '微信缴费';
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['company'] = 'easypay';
                    $data['pay_type'] = 'WECHAT';
                    $data['subAppid'] = $easypay_merchant->wx_channel_appid ? $easypay_merchant->wx_channel_appid : 'wx3fc7983c75d88330';
                }

                $obj = new \App\Api\Controllers\EasyPay\PayController();
                $easypay_data = [
                    'channel_id' => $easypay_config->channel_id,
                    'mno' => $easypay_merchant->term_mercode,
                    'device_id' => $easypay_merchant->term_termcode,
                    'out_trade_no' => $out_trade_no,
                    'pay_type' => $data['pay_type'],
                    'total_amount' => $total_amount,
                    'code' => $open_id,
                    'notify_url' => url('/api/school/pay_notify_easy'),
                    'location' => $location,
                    'patnerSettleFlag' => $patnerSettleFlag,
                    'subAppid' => $data['subAppid'],
                    'store_name' => $school_name
                ];
                $return = $obj->qr_submit($easypay_data);
                if ($return['status'] == '1') {
                    $insert_re = StuOrder::create($data_insert);//添加到学生缴费表
                    $this->addOrder($data_insert);//添加到order表
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    if ($ways_type == 21001) { //支付宝
                        return json_encode([
                            'status' => '1',
                            'trade_no' => $return['data']['pcTrace'],//支付请求后拉起支付宝支付订单号
                            'data' => $return['data'],
                        ]);
                    } elseif ($ways_type == 21002) { //微信
                        $weChatData = json_decode($return['data']['wxWcPayData'], true);
                        if ($weChatData) {
                            if (isset($weChatData['timeStamp'])) $return['data']['payTimeStamp'] = $weChatData['timeStamp'];
                            if (isset($weChatData['package'])) $return['data']['payPackage'] = $weChatData['package'];
                            if (isset($weChatData['paySign'])) $return['data']['paySign'] = $weChatData['paySign'];
                            if (isset($weChatData['signType'])) $return['data']['paySignType'] = $weChatData['signType'];
                            if (isset($weChatData['nonceStr'])) $return['data']['paynonceStr'] = $weChatData['nonceStr'];
                        }

                        $return['data']['ordNo'] = $out_trade_no;
                        return json_encode([
                            'status' => '1',
                            'trade_no' => $return['data']['wxPrepayId'],
                            'data' => $return['data'],
                            'wx_data' => $weChatData
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'data' => $return['data'],
                        ]);
                    }

                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => $return['message']
                    ]);
                }
            }


        } catch
        (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    //学生缴费支付回调
    public function pay_notify_easySk(Request $request)
    {
        try {
            $data = $request->all();
            Log::info('易生数科-缴费回调');
            if ($data['bizData']) {
                $BaseController = new \App\Api\Controllers\EasySkPay\BaseController();
                $bizData = $BaseController->decrypt($data['bizData']);
                $bizData = json_decode($bizData, true);
                $data['bizData'] = $bizData;
            } else {
                return [
                    'status' => '3',
                    'message' => $data['respMsg'],
                    'data' => $data,
                ];
            }
            Log::info($data);
            if (isset($data) && !empty($data)) {
                if (isset($data['requestNo'])) {
                    $out_trade_no = $data['requestNo']; //商户订单号(原交易流水)
                    $trade_no = $data['tradeNo']; //系统订单号
                    $order = StuOrder::where('out_trade_no', $out_trade_no)->first();
                    //Log::info($order);
                    //订单存在
                    if ($order) {
                        //系统订单未成功
                        if ($order->pay_status == '2') {
                            $pay_time = (isset($data['bizData']['payTime']) && !empty($data['bizData']['payTime'])) ? date('Y-m-d H:i:m', strtotime($data['bizData']['payTime'])) : '';
                            $buyer_pay_amount = isset($data['bizData']['amount']) ? ($data['bizData']['amount'] / 100) : ''; //实付金额，单位分
                            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                            $buyer_id = $data['bizData']['channelOpenId'] ? ($data['bizData']['channelOpenId']) : '';
                            $update = [
                                'pay_status' => 1,
                                'pay_status_desc' => '缴费成功',
                                'trade_no' => $trade_no,
                                'pay_time' => $pay_time,
                                'gmt_end' => $pay_time,
                            ];

                            StuOrder::where('out_trade_no', $out_trade_no)->update($update);
                        }
                    }
                }

                return json_encode([
                    'respCode' => '0000',
                    'respMsg' => '成功'
                ]);
            } else {
                Log::info('易生数科-学生缴费回调-结果');
                Log::info($data);
            }

        } catch (\Exception $ex) {
            Log::info('易生数科-学生缴费回调-异步报错');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }
    }

    //易生 支付回调 微收单2.0
    public function pay_notify_easy(Request $request)
    {
        try {
            $data = $request->all();
            Log::info('易生-缴费回调');
            Log::info($data);
            if (isset($data) && !empty($data)) {
                if (isset($data['data']['oriOrgTrace'])) {
                    $out_trade_no = $data['data']['oriOrgTrace']; //商户订单号(原交易流水)
                    $trade_no = $data['data']['outTrace']; //系统订单号
                    $order = StuOrder::where('out_trade_no', $out_trade_no)->first();

                    //订单存在
                    if ($order) {
                        //系统订单未成功
                        if ($order->pay_status == '2') {
                            $pay_time = (isset($return['data']['timeEnd']) && !empty($return['data']['timeEnd'])) ? date('Y-m-d H:i:m', strtotime($return['data']['timeEnd'])) : date('Y-m-d H:i:m'); //支付完成时间，格式为 yyyyMMddhhmmss，如 2009年12月27日9点10分10秒表示为 20091227091010
                            $buyer_pay_amount = isset($data['data']['payerAmt']) ? ($data['data']['payerAmt'] / 100) : ''; //否,实付金额,单位分
                            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                            $buyer_id = '';
                            if ($data['data']['tradeCode'] != 'WUJS1') {
                                $buyer_id = $data['data']['payerId']; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
                            }
                            $update = [
                                'pay_status' => 1,
                                'pay_status_desc' => '缴费成功',
                                'trade_no' => $trade_no,
                                'pay_time' => $pay_time,
                                'gmt_end' => $pay_time,
                            ];

                            StuOrder::where('out_trade_no', $out_trade_no)->update($update);
                        }
                    }
                }

                return json_encode([
                    'resultcode' => '00'
                ]);
            } else {
                Log::info('易生-支付回调-结果');
                Log::info($data);
            }

        } catch (\Exception $ex) {
            Log::info('易生支付回调-异步报错');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }
    }

    public function addOrder($data)
    {
        $store = Store::where('store_id', $data['store_id'])->first();
        $merchant = Merchant::where('id', $store->merchant_id)
            ->select('name')
            ->first();
        $obj_ways = new  PayWaysController();
        $ways = $obj_ways->ways_type($data['ways_type'], $store->store_id, $store->pid);
        $user_rate_obj = $obj_ways->getCostRate($ways->ways_type, $data['user_id']);
        $cost_rate = $user_rate_obj ? $user_rate_obj->rate : 0.00; //成本费率
        //插入数据库
        $data_insert = [
            'trade_no' => '',
            'store_id' => $data['store_id'],
            'user_id' => $data['user_id'],
            'store_name' => $store->store_name,
            'buyer_id' => $data['buyer_id'],
            'total_amount' => $data['amount'],
            'shop_price' => $data['amount'],
            'pay_amount' => $data['amount'],
            'payment_method' => '',
            'status' => '',
            'pay_status' => 2,
            'pay_status_desc' => '等待支付',
            'merchant_id' => $store->merchant_id,
            'merchant_name' => $merchant->name,
            'remark' => $data['remark'],
            'device_id' => '学生缴费',
            'config_id' => $store->config_id,
            'company' => $data['company'],
            'other_no' => '',
            'notify_url' => '',
            'coupon_type' => '',
            'dk_jf' => '0',
            'dk_money' => '0',
            'source' => $store->source,
            'device_name' => '学生缴费',
            'ways_source' => $data['pay_type_source'],
            'out_trade_no' => $data['out_trade_no'],
            'rate' => $ways->rate,
            'fee_amount' => $ways->rate * $data['amount'] / 100,
            'cost_rate' => $cost_rate,
            'ways_type' => $data['ways_type']
        ];
        if ($data['pay_type'] == '1000') {
            $data_insert['ways_source_desc'] = '支付宝';
            $data_insert['ways_type_desc'] = '支付宝';
        } elseif ($data['pay_type'] == '2000') {
            $data_insert['ways_source_desc'] = '微信支付';
            $data_insert['ways_type_desc'] = '微信支付';
        }

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }
    }

}
