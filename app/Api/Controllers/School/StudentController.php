<?php

namespace App\Api\Controllers\School;

use App\Models\StuClass;
use App\Models\StuGrade;
use App\Models\StuOrderBatch;
use App\Models\StuStore;
use App\Models\StuStudent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\StuOrder;
use Illuminate\Support\Facades\DB;

/*
    班级管理
*/

class StudentController extends \App\Api\Controllers\BaseController
{
    private function makeStudentNo()
    {
        return str_random(12);
    }

    private $student_lst = [];

    public function importExcel()
    {
        $all_success = true;
        $message = '表格已经全部导入成功！';

        try {
            $request = app('request');
            $loginer = $this->parseToken($request->get('token'));
            $this->status = 2;


            $stu_class_no = $request->get('stu_class_no');

            $merchant_id = $loginer->merchant_id;

            if (empty($_FILES)) {
                $this->status = 2;
                $this->message = '请上传xlsx表格！';
                return $this->format();
            }

            $file_arr = array_shift($_FILES);

            if ($file_arr['error'] !== 0) {

                $this->status = 2;
                $this->message = '请上传xlsx表格！';
                return $this->format();
            }
            // var_dump($file_arr);die;
            $file = $file_arr['tmp_name'];


            if (empty($stu_class_no)) {

                $this->status = 2;
                $this->message = '班级编号不能为空！';
                return $this->format();
            }

            $have_class = StuClass::where('stu_class_no', $stu_class_no)->first();
            if (empty($have_class)) {
                $this->status = 2;
                $this->message = '班级不存在！';
                return $this->format();
            }

            $excel_data = \App\Common\Excel\Excel::_readExcel($file);

            // $record=0;

            foreach ($excel_data as $k => $v) {

                if ($k == 0) {
                    continue;
                }

//                if (trim($v[5]) == '爸爸') {
//                    $student_user_relation = 1;
//                } else if (trim($v[5]) == '妈妈') {
//                    $student_user_relation = 2;
//                } else if (trim($v[5]) == '爷爷') {
//                    $student_user_relation = 3;
//                } else if (trim($v[5]) == '奶奶') {
//                    $student_user_relation = 4;
//                } else if (trim($v[5]) == '外公') {
//                    $student_user_relation = 5;
//                } else if (trim($v[5]) == '外婆') {
//                    $student_user_relation = 6;
//                } else if (trim($v[5]) == '家长') {
//                    $student_user_relation = 7;
//                }

                $cin = [
                    'school_no' => $have_class->school_no,
                    'stu_grades_no' => $have_class->stu_grades_no,
                    'stu_class_no' => $have_class->stu_class_no,

                    'student_no' => trim($v[0]),
                    'student_name' => trim($v[1]),
//                    'student_identify' => trim($v[1]) ? trim($v[1]) : date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999)),
//                    'student_user_name' => trim($v[3]),
//                    'student_user_mobile' => trim($v[4]),
//                    'student_user_relation' => $student_user_relation,
                    'status' => '1',
                    'status_desc' => 'excel导入',
                ];


                $hand = self::addEach($cin);
                if ($hand['status'] == 1) {
                    $this->student_lst[] = $hand['data']->id;

                } else {
                    $message = '当前表格未导入：' . $hand['message'];
                    $all_success = false;
                    break;
                }
            }


        } catch (\Exception $e) {
            $all_success = false;
            $message = '表格未导入：系统错误：' . $e->getMessage() . $e->getFile() . $e->getLine();
        }


        if ($all_success) {
            $this->status = 1;
            $this->message = $message;
            return $this->format();
        } else {
            //删除成功的记录
            if (!empty($this->student_lst)) {
                try {
                    StuStudent::whereIn('id', $this->student_lst)->delete();
                } catch (\Exception $e) {

                }
            }

            $this->status = 2;
            $this->message = $message;
            return $this->format();
        }
    }

    private static function addEach($cin)
    {
        $validate = \Validator::make($cin, [
            'school_no' => 'required',
            'student_no' => 'required',
            'stu_grades_no' => 'required',
            'stu_class_no' => 'required',
            'student_name' => 'required',
            // 'student_identify'=>'required|min:15|max:18',
            // 'student_user_mobile' => 'required|min:11|max:11',
            // 'student_user_name' => 'required',
            // 'student_user_relation' => 'required',
        ], [
            'required' => ':attribute为必填项！',
            'min' => ':attribute长度不符合要求！',
            'max' => ':attribute长度不符合要求！',
            'unique' => ':attribute已经被人占用！',
            'exists' => ':attribute不存在！'
        ], [
            'school_no' => '学校编号',
            'student_no' => '学生学号',
            'stu_grades_no' => '年级编号',
            'stu_class_no' => '班级编号',
            'student_name' => '学生姓名',
            //  'student_identify'=>'学生身份证号',
            // 'student_user_mobile' => '家长手机号',
            // 'student_user_name' => '家长姓名',
            // 'student_user_relation' => '学生与家长关系',
        ]);

        if ($validate->fails()) {
            return ['status' => 2, 'message' => $validate->getMessageBag()->first()];
        }

        $have_school = StuStore::where('school_no', $cin['school_no'])->first();
        if (empty($have_school)) {
            return ['status' => 2, 'message' => '学校不存在！'];
        }

        $have_grade = StuGrade::where('stu_grades_no', $cin['stu_grades_no'])->first();
        if (empty($have_grade)) {
            return ['status' => 2, 'message' => '年级不存在！'];
        }

        $have_class = StuClass::where('stu_class_no', $cin['stu_class_no'])->first();
        if (empty($have_class)) {
            return ['status' => 2, 'message' => '班级不存在！'];
        }

        $not_match = StuClass::where('stu_class_no', $cin['stu_class_no'])->where('stu_grades_no', $cin['stu_grades_no'])->where('school_no', $cin['school_no'])->first();
        if (empty($not_match)) {
            return ['status' => 2, 'message' => '年级或者班级不存在！'];
        }

        $have = StuStudent::where('school_no', $cin['school_no'])
            ->where('stu_grades_no', $cin['stu_grades_no'])
            ->where('stu_class_no', $cin['stu_class_no'])
            ->where('student_no', $cin['student_no'])
            ->where('student_name', $cin['student_name'])
            ->first();

        if ($have) {
            return ['status' => 2, 'message' => '学生已经存在！'];
        }

        $ok = StuStudent::create($cin);

        return ['status' => 1, 'message' => 'ok', 'data' => $ok];
    }


    /*
        添加
    */
    public function add()
    {

        try {
            $request = app('request');
            if ($request->get('token')) {
                $loginer = $this->parseToken($request->get('token'));
                $merchant_id = $loginer->merchant_id;
            }
            $this->status = 2;
            $student_identify = $request->get('student_identify', '');
            if ($student_identify == "" || $student_identify == "NULL") {
                $student_identify = date('YmdHis', time()) . substr(microtime(), 2, 4);
            }
            $cin = [
                'school_no' => $request->get('school_no', ''),
                'stu_grades_no' => $request->get('stu_grades_no', ''),
                'stu_class_no' => $request->get('stu_class_no', ''),
                'student_no' => $request->get('student_no', ''),
                'student_name' => $request->get('student_name', ''),
                'student_identify' => $student_identify,
                'student_user_mobile' => $request->get('student_user_mobile', ''),
                'student_user_name' => $request->get('student_user_name', ''),
                'student_user_relation' => $request->get('student_user_relation', ''),
                'status' => $request->get('status', '1'), //默认在校
                'status_desc' => $request->get('status_desc', ''),
                'open_id' => $request->get('open_id', ''),
            ];
            $have = StuStudent::where('school_no', $request->get('school_no'))
                ->where('stu_grades_no', $request->get('stu_grades_no'))
                ->where('stu_class_no', $request->get('stu_class_no'))
                ->where('student_no', $request->get('student_no'))
                //->where('student_name', $request->get('student_name'))
                // ->where('open_id', $request->get('open_id'))
                ->first();

            if (!empty($have)) {
                StuStudent::where('school_no', $request->get('school_no'))
                    ->where('stu_grades_no', $request->get('stu_grades_no'))
                    ->where('stu_class_no', $request->get('stu_class_no'))
                    ->where('student_no', $request->get('student_no'))
                    ->update($cin);
                StuOrder::where('stu_grades_no', $request->get('stu_grades_no'))
                    ->where('stu_class_no', $request->get('stu_class_no'))
                    ->where('student_no', $request->get('student_no'))
                    ->update(['buyer_id' => $request->get('open_id')]);
                $this->status = 1;
                $this->message = '学生添加成功';
                return $this->format();
            }

            $hand = self::addEach($cin);
            if ($hand['status'] != 1) {
                $this->status = $hand['status'];
                $this->message = $hand['message'];
                return $this->format();
            }

            $this->status = 1;
            $this->message = '学生添加成功';
            return $this->format();


        } catch (\Exception $e) {
            $this->status = -1;
            $this->message = '系统错误' . $e->getMessage() . $e->getFile() . $e->getLine();
            return $this->format();
        }
    }


    /*
        修改
    */
    public function save()
    {

        try {
            $request = app('request');
            $loginer = $this->parseToken($request->get('token'));
            $this->status = 2;
            if ($request->get('token')) {
                $loginer = $this->parseToken($request->get('token'));
            }
            $student = StuStudent::where('id', $request->get('student_id'))->first();

            if (empty($student)) {
                $this->message = '学生不存在！';
                return $this->format();
            }

            $cin = [
                'student_name' => $request->get('student_name', ''),
                'student_no' => $request->get('student_no', ''),
                'student_identify' => $request->get('student_identify', ''),
                'student_user_mobile' => $request->get('student_user_mobile', ''),
                'student_user_name' => $request->get('student_user_name', ''),
                'student_user_relation' => $request->get('student_user_relation', ''),
                'status' => $request->get('status', '1'),
                'status_desc' => $request->get('status_desc', ''),
            ];

            $student_user_mobile = $request->get('student_user_mobile');

            if (!empty($student_user_mobile)) {
                if (strlen($student_user_mobile) != 11) {
                    $this->message = '手机号码格式不正确！';
                    return $this->format();
                }
                $cin['student_user_mobile'] = $student_user_mobile;
            }

            $student_identify = $request->get('student_identify');

            if (!empty($student_identify)) {
                $cin['student_identify'] = $student_identify;

                $have = StuStudent::where('student_identify', $student_identify)->where('id', '!=', $student->id)->first();
                if (!empty($have)) {
                    $this->message = '身份证号已经被别人占用！';
                    return $this->format();
                }

            }

            $stu_grades_no = $request->get('stu_grades_no');

            if (!empty($stu_grades_no)) {
                $cin['stu_grades_no'] = $stu_grades_no;

                $not_match = StuGrade::where('stu_grades_no', $stu_grades_no)->where('school_no', $student->school_no)->first();
                if (empty($not_match)) {
                    return ['status' => 2, 'message' => '年级不存在！'];
                }

            }

            $stu_class_no = $request->get('stu_class_no');

            if (!empty($stu_class_no)) {

                $not_match = StuClass::where('stu_class_no', $stu_class_no)->where('stu_grades_no', $student->stu_grades_no)->where('school_no', $student->school_no)->first();
                if (empty($not_match)) {
                    return ['status' => 2, 'message' => '班级不存在！'];
                }

                $cin['stu_class_no'] = $stu_class_no;
            }

            $cin = array_filter($cin);

            if (empty($cin)) {

                $this->message = '请传入要修改的参数！';
                return $this->format();
            }

            $student = $student->update($cin);

            $this->status = 1;
            $this->message = '修改学生成功';
            return $this->format();


        } catch (\Exception $e) {
            $this->status = -1;
            $this->message = '系统错误' . $e->getMessage() . $e->getFile() . $e->getLine();
            return $this->format();
        }
    }


    /*
        列表
    */
    public function lst()
    {

        try {
            $request = app('request');
            $loginer = $this->parseToken($request->get('token'));
            $merchant_id = $loginer->merchant_id;
            $this->status = 2;

            $obj = new StuStudent;

            $store_id = $request->get('store_id');
            $school_no = $request->get('school_no');
            $stu_grades_no = $request->get('stu_grades_no');
            $stu_class_no = $request->get('stu_class_no');
            $student_name = $request->get('student_name');

            if (!empty($school_no)) {
                $obj = $obj->where('school_no', $school_no);
            } else {
                $get_all_school_no = StuStore::where('store_id', $store_id)->get();
                $all_school_no = [];
                foreach ($get_all_school_no as $v) {
                    $all_school_no[] = $v->school_no;
                }

                $obj = $obj->whereIn('school_no', array_unique($all_school_no));
            }

            if (!empty($stu_grades_no)) {
                $obj = $obj->where('stu_grades_no', $stu_grades_no);
            }
            if (!empty($stu_class_no)) {
                $obj = $obj->where('stu_class_no', $stu_class_no);
            }

            if (!empty($request->get('status'))) {
                $obj = $obj->where('status', $request->get('status'));
            }

            if (!empty($student_name)) {
                $obj = $obj->where('student_name', 'like', '%' . $student_name . '%');
            }
            if (!empty($student_name)) {
                $obj = $obj->where('student_name', 'like', '%' . $student_name . '%');
            }
            $school = StuStore::where('store_id', $store_id)->get();
            $grade = StuGrade::where('store_id', $store_id)->get();
            $class = StuClass::where('merchant_id', $merchant_id)->get();

            foreach ($school as $v) {
                $all_school[$v->school_no] = $v;
            }

            foreach ($grade as $v) {
                $all_grade[$v->stu_grades_no] = $v;
            }

            foreach ($class as $v) {
                $all_class[$v->stu_class_no] = $v;
            }

            $this->t = $obj->count();

            $data = $this->page($obj)->get();

            $cout = [];
            if (!$data->isEmpty()) {
                $cout = array_map(function ($each) use ($all_school, $all_grade, $all_class) {
                    return array_merge($each, [
                            'school_name' => isset($all_school[$each['school_no']]) ? $all_school[$each['school_no']]->school_name : '',
                            'grade_name' => isset($all_grade[$each['stu_grades_no']]) ? $all_grade[$each['stu_grades_no']]->stu_grades_name : '',
                            'class_name' => isset($all_class[$each['stu_class_no']]) ? $all_class[$each['stu_class_no']]->stu_class_name : '',
                        ]
                    );

                }, $data->toArray());
            }

            $this->status = 1;
            $this->message = 'ok';
            return $this->format($cout);


        } catch (\Exception $e) {
            $this->status = -1;
            $this->message = '系统错误' . $e->getMessage() . $e->getFile() . $e->getLine();
            return $this->format();
        }
    }


    /*
        单条
    */
    public function show()
    {

        try {
            $request = app('request');
            if ($request->get('token')) {
                $loginer = $this->parseToken($request->get('token'));
            }
            $this->status = 2;

            $obj = new StuStudent;

            $grade = $obj->where('id', $request->get('stu_id'))->first();

            if (empty($grade)) {
                $this->message = '学生不存在！';
                return $this->format();
            }

            $this->status = 1;
            $this->message = 'ok';
            return $this->format($grade);

        } catch (\Exception $e) {
            $this->status = -1;
            $this->message = '系统错误' . $e->getMessage() . $e->getFile() . $e->getLine();
            return $this->format();
        }
    }


    /*
        删除
    */
    public function del()
    {

        try {
            $request = app('request');
            $loginer = $this->parseToken($request->get('token'));
            $this->status = 2;

            $id = $request->get('id');

            if (empty($id)) {
                $this->message = '学生id不能为空！';
                return $this->format();
            }

            $have = StuStudent::where('id', $id)->first();
            if (empty($have)) {

                $this->message = '您要删除的学生不存在！';
                return $this->format();
            }

            $ok = $have->delete();
            if (!$ok) {

                $this->message = '删除失败，请重试！';
                return $this->format();
            }

            $this->status = 1;
            $this->message = '删除成功！';
            return $this->format();


        } catch (\Exception $e) {
            $this->status = -1;
            $this->message = '系统错误' . $e->getMessage() . $e->getFile() . $e->getLine();
            return $this->format();
        }
    }

    //获取学生信息
    public function getStudentInfo(Request $request)
    {
        try {
            $student_name = $request->get('student_name');//学生姓名
            $parent_name = $request->get('parent_name');//家长姓名
            $parent_phone = $request->get('parent_phone');//家长电话
            $student = StuStudent::where('student_user_name', $parent_name)
                ->where('student_name', $student_name)
                ->where('student_user_mobile', $parent_phone)
                ->select()
                ->first();
            if (!$student) {
                return json_encode([
                    'status' => 2,
                    'message' => '学生信息不存在，您是否需要添加学生信息'
                ]);
            }
            $stuGrade = StuGrade::where('stu_grades_no', $student->stu_grades_no)->select('stu_grades_name')->first();
            $stuClass = StuClass::where('stu_class_no', $student->stu_class_no)->select('stu_class_name')->first();
            $data = [
                'grades_name' => $stuGrade->stu_grades_name,
                'class_name' => $stuClass->stu_class_name,
                'student_no' => $student->student_no,
            ];
            return json_encode([
                'status' => 1,
                'message' => '获取学生信息成功',
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }

    }

    //获取缴费记录
    public function getPayList(Request $request)
    {
        try {
            $open_id = $request->get('open_id');
            $stu_order = StuOrder::where('buyer_id', $open_id)
                ->where('pay_status', '1')
                ->select()
                ->get();
            return json_encode([
                'status' => 1,
                'message' => '获取缴费记录成功',
                'data' => $stu_order
            ]);
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }

    }

    public function getOrderBatchList(Request $request)
    {
        try {
            $stu_class_no = $request->get('stu_class_no');//班级编号
            $where = [];
            $where[] = ['stu_class_no', 'like', '%' . $stu_class_no . '%'];

            $stuOrderBatch = StuOrderBatch::where($where)
                ->select('*')
                ->get();
            return json_encode([
                'status' => 1,
                'message' => '获取缴费项目成功',
                'data' => $stuOrderBatch
            ]);
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }

    }

    public function getOrderBatchInfo(Request $request)
    {
        try {
            $stu_order_batch_no = $request->get('stu_order_batch_no');
            $stuOrderBatch = StuOrderBatch::where('stu_order_batch_no', $stu_order_batch_no)
                ->select('amount')
                ->first();
            $amount = 0.00;
            if ($stuOrderBatch) {
                $amount = $stuOrderBatch->amount;
            }
            return json_encode([
                'status' => 1,
                'message' => '获取缴费明细成功',
                'data' => $amount
            ]);
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }

    }

    public function getStudentList(Request $request)
    {
        try {
            $open_id = $request->get('open_id');//微信openid
            $school_no = $request->get('school_no');//学校编号
            $stu_class_no = $request->get('stu_class_no');//班级编号
            $obj = new StuStudent;
            $obj = $obj
                ->leftJoin('stu_class', 'stu_class.stu_class_no', 'stu_students.stu_class_no')
                ->leftJoin('stu_grades', 'stu_grades.stu_grades_no', 'stu_students.stu_grades_no');
            $obj = $obj->select(DB::raw('stu_class.stu_class_name class_name,stu_grades.stu_grades_name grade_name,stu_students.*'))
                ->where('stu_students.open_id', $open_id)
                ->where('stu_students.school_no', $school_no);
            if (!empty($stu_class_no)) {
                $obj = $obj->where('stu_students.stu_class_no', $stu_class_no);
            }
            $obj = $obj->get();

            return json_encode([
                'status' => 1,
                'message' => '获取学生信息成功',
                'data' => $obj
            ]);
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }

    public function getStudentNoAndStuClassNo(Request $request)
    {
        try {
            $stu_class_no = $request->get('stu_class_no');//班级编号
            $student_no = $request->get('student_no');//学生编号
            $student = StuStudent::where('stu_class_no', $stu_class_no)
                ->where('student_no', $student_no)
                ->select('*')
                ->first();
            if(!$student){
                return json_encode([
                    'status' => 2,
                    'message' => '学生不存在，请输入正确学号'
                ]);
            }
            return json_encode([
                'status' => 1,
                'message' => '获取学生信息成功',
                'data' => $student
            ]);
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }

}
