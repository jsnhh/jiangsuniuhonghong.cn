<?php
namespace App\Api\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Merchant;
use App\Models\MerchantStore;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Store;
use App\Models\User;
use App\Models\WechatThirdConfig;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\Stores;
use SimpleSoftwareIO\QrCode\Generator;

class BaseController extends Controller
{

    //第三方开放平台appid
    protected $app_id;
    //第三方开放平台secret
    protected $secret;
    //第三方开放平台token
    protected $token;
    //第三方开放平台aes_key
    protected $aes_key;
    //域名
    protected $domain;
    //微信公众号的appid
    protected $weChatServerAppId;
    //微信公众号的秘钥
    protected $weChatServerSecret;
    protected $wechat_pages_url; // 微信小程序扫码点餐页面地址

    protected $min_appid='wx613f4a31c584ec97';//小程序appid(用于分享收款)

    protected $easyPay_key = '2oc0berah54frr826a9tnk8u92k862xo'; //签名秘钥
    protected $easyPay_channelId = 'D01X00000801706';    //测试渠道编号:616161622101901
    protected $easyPay_secret_key = 'wp418yna717xaosn'; //渠道秘钥

    protected $easyPay_key_two = '2oc0berah54frr826a9tnk8u92k862xo'; //签名秘钥
    protected $easyPay_channelId_two = 'D01X00000801706';    //测试渠道编号:616161622101901
    protected $easyPay_secret_key_two = 'wp418yna717xaosn'; //渠道秘钥


    public function __construct()
    {
        $model = new WechatThirdConfig();
        $data = $model->getInfo();

        if (isset($data->app_id) && !empty($data->app_id)) {
            $this->app_id = $data->app_id;
        } else {
            $this->app_id = '';
        }
        $this->secret  = isset($data->app_secret) ? $data->app_secret : '';
        $this->token   = isset($data->token) ? $data->token : '';
        $this->aes_key = isset($data->aes_key) ? $data->aes_key : '';
        $this->weChatServerAppId   = isset($data->weChatServerAppId) ? $data->weChatServerAppId : '';
        $this->weChatServerSecret  = isset($data->weChatServerSecret) ? $data->weChatServerSecret : '';
        $this->domain  = env('APP_URL');
        $this->wechat_pages_url = 'pages/payment/payment';

        // 获取易生电子协议签名秘钥
        // Cache::forget('hs_easyMP_signKey');
        $this->easyPay_key = Cache::get('hs_easyMP_signKey');
        if (!$this->easyPay_key) {

            $dataInfo = [
                'channelId' => $this->easyPay_channelId,
                'tradeTrace' => date('YmdHis') . mt_rand(1000, 9999), //请求流水，唯一，不包括特殊字符
                'version' => '3.0',
            ];

            $sign = $this->getSignEasyPay($dataInfo, $this->easyPay_secret_key); //渠道秘钥

            $dataInfo['sign'] = $sign;

            $url_key = 'https://auth.eycard.cn:8443/EasypayMPSystem/SignServet'; //获取签名key
            $result = $this->postUrl($dataInfo, $url_key);

            Log::info('==========获取签名key========');
            Log::info($result);

            //系统错误
            if (!$result) {
                return [
                    'status' => '-1',
                    'message' => '系统错误',
                ];
            }

            $res_arr = json_decode($result, true);
            //成功
            if ($res_arr['resultCode'] == '00') {
                $signKey = $res_arr['key'];
                $this->easyPay_key = $signKey;
                Log::info('==========获取签名====signKey:' . $signKey);
                Cache::forever('hs_easyMP_signKey', $signKey);
            }
        }

        // Cache::forget('hs_easyMP_signKey_two');
        $this->easyPay_key_two = Cache::get('hs_easyMP_signKey_two');
        if (!$this->easyPay_key_two) {

            $dataInfo = [
                'channelId' => $this->easyPay_channelId_two,
                'tradeTrace' => date('YmdHis') . mt_rand(1000, 9999), //请求流水，唯一，不包括特殊字符
                'version' => '3.0',
            ];

            $sign = $this->getSignEasyPay($dataInfo, $this->easyPay_secret_key_two); //渠道秘钥

            $dataInfo['sign'] = $sign;

            $url_key = 'https://auth.eycard.cn:8443/EasypayMPSystem/SignServet'; //获取签名key
            $result = $this->postUrl($dataInfo, $url_key);

            Log::info('==========获取签名key========');
            Log::info($result);

            //系统错误
            if (!$result) {
                return [
                    'status' => '-1',
                    'message' => '系统错误',
                ];
            }

            $res_arr = json_decode($result, true);
            //成功
            if ($res_arr['resultCode'] == '00') {
                $signKey = $res_arr['key'];
                Log::info('==========获取签名====signKey:' . $signKey);
                Cache::forever('hs_easyMP_signKey_two', $signKey);
            }
        }
    }

    use Helpers; //为了让所有继承这个的控制器都使用dingoApi

    use ResponseInfo; //响应信息

    public $needpage = false;//默认不要分页/

    public $l = 15;
    public $p = 1;
    public $t = 0;

    public $status = 1;
    public $message = 'ok';
    public $order_data = [];


    //交易公共进库
    public function insert_day_order($insert_data)
    {
        try {
            //如果存在base
            if (Schema::hasTable('orders_base')) {
                //后面可以不进这个数据库
                $day = date('Ymd', time());
                $table = 'orders_' . $day;
                //判断是否有表
                if (!Schema::hasTable($table)) {
                    //无表
                    DB::update('create table ' . $table . ' like 	orders_base');
                }
                $insert_data['created_at'] = date('Y-m-d H:i:s', time());
                $insert_data['updated_at'] = date('Y-m-d H:i:s', time());
                $return = DB::table($table)->insert($insert_data);
            } else {
                //订单表
                $return = Order::create($insert_data);
            }

            if (Schema::hasTable('order_items')) {
                OrderItem::create($insert_data);
            }

            return $return;
        } catch (\Exception $ex) {
            Log::info('orders_base');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            return false;
        }
    }


    //交易公共进库
    public function insert_settle_order($insert_data)
    {
        try {
            //后面可以不进这个数据库
            $day = date('Ymd', time());
            $table = 'settle_orders_' . $day;
	    
            //判断是否有表
            if (!Schema::hasTable($table)) {
                //无表
                DB::update('create table ' . $table . ' like 	settle_order_base');
            }
	    
            $insert_data['created_at'] = date('Y-m-d H:i:s', time());
            $insert_data['updated_at'] = date('Y-m-d H:i:s', time());
            $return = DB::table($table)->insert($insert_data);
	    
            return $return;
        } catch (\Exception $exception) {
            Log::info('settle_orders');
            Log::info($exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine());
        }
    }


    //当天结算交易公共更改
    public function update_settle_order($update_data, $out_trade_no, $table = "")
    {
        try {
            if ($table == "") {
                $day = date('Ymd', time());
                $table = 'settle_orders_' . $day;
            }
	    
            $order = DB::table($table)
                ->where('out_trade_no', $out_trade_no)
                ->update($update_data);
		
            return $order;
        } catch (\Exception $exception) {
            Log::info('qfpay-当天结算交易公共更改-error：');
            Log::info($exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine());
            return false;
        }
    }


    //当天交易公共更改
    public function update_day_order($update_data, $out_trade_no, $table = "")
    {
        try {

            if ($table == "") {
                $day = date('Ymd', time());
                $table = 'orders_' . $day;
            }

            if (Schema::hasTable($table)) {
                $order = DB::table($table)
                    ->where('out_trade_no', $out_trade_no)
                    ->update($update_data);
            } else {
                $order = Order::where('out_trade_no', $out_trade_no)->first();
                if (!$order) {
                    $order = Order::where('trade_no', $out_trade_no)->first();
                }

                $res = $order->update($update_data);
                if (!$res) {
                    Log::info('当天交易公共更改-失败：');
                    Log::info($out_trade_no);
                }
            }
            if (Schema::hasTable('order_items')) {
                OrderItem::where('out_trade_no', $out_trade_no)->update($update_data);
            }

            return $order;
        } catch (\Exception $exception) {
            Log::info('当天交易公共更改-error：');
            Log::info($exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine());
            return false;
        }
    }


    public function parseToken($token = '')
    {
        try {
            JWTAuth::setToken(JWTAuth::getToken());
            $data = JWTAuth::getPayload();//数组
            return (object)$data['sub'];
        } catch (\Exception $e) {
            return false;
        }
    }


    public function format($cin = [])
    {
        $data = [
            'status' => $this->status,
            'message' => $this->message,
            'order_data' => $this->order_data,
            'data' => $cin
        ];
        if ($this->needpage) {
            $data['l'] = $this->l;
            $data['p'] = $this->p;
            $data['t'] = $this->t;
        }

        return response()->json($data);
    }


    /**
     *  返回分页数据
    */
    public function page($obj, $request = '')
    {

        if (empty($request))
            $request = app('request');

        $this->p = abs(trim($request->get('p', 1)));
        $this->l = abs(trim($request->get('l', 200)));

        $this->needpage = true;

        $start = abs(($this->p - 1) * $this->l);
        return $obj->offset($start)->limit($this->l);
    }


    /**
     * 校验必填字段
     */
    public function check_required($check, $data)
    {
        $rules = [];
        $attributes = [];
        foreach ($data as $k => $v) {
            $rules[$k] = 'required';
            $attributes[$k] = $v;
        }
        $messages = [
            'required' => ':attribute不能为空',
        ];
        $validator = Validator::make($check, $rules,
            $messages, $attributes);
        $message = $validator->getMessageBag();
        return $message->first();
    }


    //循环获取下级的用户id
    function getSubIds($userID, $includeSelf = true, $t1 = "", $t2 = "")
    {
        $userIDs = [$userID];
        $where = [];

        if ($t2 == "") {
            $t2 = date('Y-m-d H:i:s', time());

        }

        if ($t1) {
            $where[] = ['created_at', '>=', $t1];
            $where[] = ['created_at', '<=', $t2];

        }
        $subIDs = User::whereIn('pid', $userIDs)
            ->where($where)
            ->select('id')->get()
            ->toArray();
        $subIDs = array_column($subIDs, 'id');
        //$userCount = count($userIDs);
        $userIDs = array_unique(array_merge($userIDs, $subIDs));
        if (!$includeSelf) {
            for ($i = 0; $i < count($userIDs); ++$i) {
                if ($userIDs[$i] == $userID) {
                    array_splice($userIDs, $i, 1);
                    break;
                }
            }
        }
        return $userIDs;
    }

    /**
     * 查询下级用户ALL
     * @param $userID
     * @param bool $includeSelf
     * @return array
     */
    function getSubIdsAll($userID, $includeSelf = true, $t1 = "", $t2 = "")
    {
        $userIDs = [$userID];
        $where = [];

        if ($t2 == "") {
            $t2 = date('Y-m-d H:i:s', time());

        }

        if ($t1) {
            $where[] = ['created_at', '>=', $t1];
            $where[] = ['created_at', '<=', $t2];

        }
        while (true) {
            //不允许设置读
            $subIDs = User::whereIn('pid', $userIDs)
                ->where($where)
                ->select('id')->get()
                ->toArray();
            $subIDs = array_column($subIDs, 'id');
            $userCount = count($userIDs);
            $userIDs = array_unique(array_merge($userIDs, $subIDs));
            if ($userCount == count($userIDs)) {
                break;
            }
        }
        if (!$includeSelf) {
            for ($i = 0; $i < count($userIDs); ++$i) {
                if ($userIDs[$i] == $userID) {
                    array_splice($userIDs, $i, 1);
                    break;
                }
            }
        }
        return $userIDs;
    }

    //获取用户下所有门店
    function getUserStoreIds($userId)
    {
        $storeIDs = [];
        $userIds = $this->getSubIdsAll($userId);
        while (true) {
            //不允许设置读
            $storeIds = Stores::whereIn('user_id', $userIds)
                ->where('is_delete', 0)
                ->where('is_close', 0)
                ->select('store_id')->get()
                ->toArray();
            $storeIds = array_column($storeIds, 'store_id');
            $count = count($storeIDs);
            $storeIDs = array_unique(array_merge($storeIDs, $storeIds));
            if ($count == count($storeIDs)) {
                break;
            }
        }
        return $storeIDs;
    }

    /**
     * 查询门店所有ID
     * @return string
     */
    public function getStore_id($store_id, $id)
    {

        $store_ids = [];

        $store = Store::orWhere('pid', $id)
            ->orWhere('store_id', $store_id)
            ->select('store_id')
            ->get();

        if (!$store->isEmpty()) {
            $store_ids = $store->toArray();
        }

        return $store_ids;
    }

    /**
     * 统一接口返回格式
     */
    public function sys_response($code = 0, $msg = '', $data = '')
    {
        // 如果$msg为空自动获取
        if (empty($msg)) {
            $key = 'CODE_' . $code;
            $reflectionClass = new \ReflectionClass('\App\Common\Enum\BaseStatusCodeEnum');
            if ($reflectionClass->hasConstant($key)) {
                $msg = $reflectionClass->getConstant($key);
            }
        }

        $returnData = [
            'code' => $code,
            'msg' => $msg,
            'data' => $data,
        ];
        if ($data === false) {
            unset($returnData['data']);
        }

        return $returnData;
    }

    /**
     * 统一接口返回格式 layui
     */
    public function sys_response_layui($status = 0, $message = '', $data = '', $count = 0)
    {
        $returnData = [
            'status' => $status,
            'msg' => $message,
            'data' => $data,
            't' => $count,
        ];
        if ($data === false) {
            unset($returnData['data']);
        }

        return $returnData;
    }

    // 拼接图片地址
    public function changeImgUrl($url, $type = 1)
    {
        if ($type == 1) {
            return env('APP_URL') . $url;
        } else if ($type == 2){
            return 'http://latest.com' . $url;
        } else {
            return 'https://test.yunsoyi.cn' . $url;
        }
    }

    /**
     * 获取商户名下所有门店
     */
    public function getMerchantStores()
    {
        $merchant = $this->parseToken();
        $storesModel = new Stores();
        $merchantStoresInput['merchant_id'] = $merchant->merchant_id;
        $MerchantStores = $storesModel->getMerchantStore($merchantStoresInput)->toArray();

        return array_column($MerchantStores, 'store_id');
    }

    /**
     * 第三方保存聚合二维码
     * @param string $url 二维码地址
     * @param string $fileName 二维码地址
     */
    public function saveQrcodeApi($url, $fileName)
    {
        // 保存路径
        $save_dir = public_path('tableAppletCommonQrCode/');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 信任任何证书
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        $file = curl_exec($ch);
        curl_close($ch);
        $resource = fopen($save_dir . $fileName , 'a');
        fwrite($resource, $file);
        fclose($resource);
    }

    /**
     * 保存聚合二维码
     * @param string $url 二维码地址
     * @param string $fileName 二维码地址
     */
    public function saveQrcode($url, $fileName)
    {
        $generator = new Generator();
        $generator->format('png')->generate($url,public_path('tableAppletCommonQrCode/' . $fileName));
    }

    /**
     * https请求地址，post请求方式
     * @param $url
     * @param $data
     * @return bool|string
     */
    public function curl_post_https($url, $data = "", $header ="")
    {
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
        if (!empty($header)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_HEADER, 0);//返回response头部信息
        }
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); // 从证书中检查SSL加密算法是否存在
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        }
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $tmpInfo = curl_exec($curl); // 执行操作
        if (curl_errno($curl)) {
            return 'Errno'.curl_error($curl);//捕抓异常
        }

        curl_close($curl); // 关闭CURL会话

        return $tmpInfo; // 返回数据，json格式
    }


    /**
     * 封装定义接口返回的数据格式
     * @param integer $status 状态码
     * @param mixed  $responseData 返回的数据
     * @param mixed  $order_data 额外返回数据
     * @return \Illuminate\Http\JsonResponse
     */
    protected function responseDataJson($status, $responseData = '', $order_data = [])
    {
        $data = [
            'status' => '',
            'message' => '',
            'order_data' => [],
            'data' => ''
        ];
        if ($this->needpage) {
            $data['l'] = $this->l;
            $data['p'] = $this->p;
            $data['t'] = $this->t;
        }

        if ( !isset($status) || empty($status) ) {
            return response()->json([]);
        }

        $data['status'] = $status;
        $data['message'] = $this->getCodeMessage($status);

        if ( !empty($responseData) ) {
            $data['data'] = $responseData;
        } else {
            $data['data'] = [];
        }

        if ( $order_data ) {
            $data['order_data'] = $order_data;
        }

        return response()->json($data);
    }

    public function postUrl($params, $url)
    {
        $data = json_encode($params,JSON_UNESCAPED_UNICODE);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); //访问超时时间
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(json_decode($data)));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded;charset=UTF-8'));
        //本地忽略校验证书
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($curl);
        return $result;
    }

    //生成签名
    public function getSignEasyPay($params,$key)
    {
        ksort($params);
        $params = array_filter($params);
        $string = "";

        foreach ($params as $name => $value) {
            $string .= $name . '=' . $value . '&';
        }

        $string .= 'key=' . $key;

        return strtoupper(md5($string));
    }

    /**
     * 生成商户编号
     * 80开头+当前日期+4位随机数
     */
    public function generateMerchantNo()
    {
        $dataNo = date('ymd', time());
        //80开头+当前日期+4位随机数

        $merchantNo = '80' . $dataNo . sprintf('%04d', rand(0, 9999));
        $checkMerchant = Merchant::where('merchant_no', $merchantNo)->select('merchant_no')->first();
        if($checkMerchant){
            $merchantNo = '80' . $dataNo . sprintf('%04d', rand(0, 9999));
        }
        return $merchantNo;
    }

}
