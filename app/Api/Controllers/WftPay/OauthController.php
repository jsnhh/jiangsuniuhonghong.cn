<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/10/21
 * Time: 10:28
 */
namespace App\Api\Controllers\WftPay;


use App\Api\Controllers\Config\MyBankConfigController;
use App\Api\Controllers\Config\VbillConfigController;
use App\Api\Controllers\Config\WftPayConfigController;
use App\Models\MemberList;
use App\Models\MemberSetJf;
use App\Models\MemberTpl;
use EasyWeChat\Factory;
use Illuminate\Http\Request;

class OauthController extends BaseController
{
    //授权
    public function oauth(Request $request)
    {
        $sub_info = $request->get('state');
        $sub_info_arr = json_decode(base64_decode((string)$sub_info), true);
        $config_id = $sub_info_arr['config_id'];
        //威富通配置
        $config = new WftPayConfigController();
        $wftpay_config = $config->wftpay_config($config_id);
        if (!$wftpay_config) {
            return json_encode([
                'status' => 2,
                'message' => '威富通配置不存在请检查配置'
            ]);
        }

        $config = [
            'app_id' => $wftpay_config->wx_appid,
            'scope' => 'snsapi_base',
            'oauth' => [
                'scopes' => ['snsapi_base'],
                'response_type' => 'code',
                'callback' => url('api/wftpay/weixin/oauth_callback?sub_info=' . $sub_info . '&wx_AppId=' . $wftpay_config->wx_appid . '&wx_Secret=' . $wftpay_config->wx_secret . ''),
            ]
        ];
        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;

        return $oauth->redirect();
    }


    //授权回调
    public function oauth_callback(Request $request)
    {
        $sub_info = $request->get('sub_info');
        $code = $request->get('code');
        $wx_AppId = $request->get('wx_AppId');
        $wx_Secret = $request->get('wx_Secret');

        $sub_info_arr = json_decode(base64_decode((string)$sub_info), true);
        $config_id = $sub_info_arr['config_id'];
        $store_id = $sub_info_arr['store_id'];
        $store_name = $sub_info_arr['store_name'];
        $merchant_id = $sub_info_arr['merchant_id'];

        $config = [
            'app_id' => $wx_AppId,
            "secret" => $wx_Secret,
            "code" => $code,
            "grant_type" => "authorization_code",
        ];

        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;
        $user = $oauth->user();
        $open_id = $user->getId();

        $data = [
            'store_id' => $store_id,
            'store_name' => $store_name,
            'store_address' => '',
            'open_id' => $open_id,
            'merchant_id' => $merchant_id,
        ];
        $data = base64_encode(json_encode((array)$data));

        return redirect('/api/wftpay/weixin/pay_view?data=' . $data);
    }


    //支付显示页面
    public function pay_view(Request $request)
    {
        $data = json_decode(base64_decode((string)$request->get('data')), true);

        $store_id = $data['store_id'];
        $open_id = $data['open_id'];

        //查询是否有开启会员
        $is_member = 0;
        $MemberTpl = MemberTpl::where('store_id', $store_id)
            ->select('tpl_status')
            ->first();
        if ($MemberTpl && $MemberTpl->tpl_status == 1) {
            $is_member = 1;
        }

        //如果是会员
        if (0) {
            //判断是否是会员
            $MemberList = MemberList::where('store_id', $store_id)
                ->where('wx_openid', $open_id)
                ->select('mb_jf', 'mb_money', 'mb_id')
                ->first();
            $data['mb_jf'] = "";
            $data['mb_id'] = "";
            $data['mb_money'] = "";
            $data['dk_money'] = "";
            $data['dk_jf'] = "0";
            $data['ways_source'] = "weixin";

            if ($MemberList) {
                $data['mb_jf'] = $MemberList->mb_jf;
                $data['mb_id'] = $MemberList->mb_id;
                $data['mb_money'] = $MemberList->mb_money;
                //判断是否有积分抵扣
                $MemberSetJf = MemberSetJf::where('store_id', $store_id)
                    ->select('dk_jf_m', 'dk_rmb')
                    ->first();

                if ($MemberSetJf) {
                    //3.用户的积分一共可以抵扣多少钱
                    $data['dk_money'] = ($MemberList->mb_jf / $MemberSetJf->dk_jf_m) * $MemberSetJf->dk_rmb;
                    $data['dk_jf'] = $MemberList->mb_jf;
                }
            }
            $data['ways_type'] = "27002";
            $data['company'] = "wftpay";
            return view('wftpay.membermweixin', compact('data'));
        } else {
            return view('wftpay.weixin', compact('data'));
        }
    }


    //微信充值页面
    public function member_cz_pay_view(Request $request)
    {
        return view('member.cz');
    }


    //授权获取openID
    public function oauth_openid(Request $request)
    {
        $sub_info = $request->get('state');

        //第三方传过来的信息
        $sub_info_arr = json_decode(base64_decode((string)$sub_info), true);

        $wftpayconfigobj = new WftPayConfigController();
        $wftpayConfig = $wftpayconfigobj->wftpay_config('1234');
        $config = [
            'app_id' => $wftpayConfig->wx_appid,
            'scope' => 'snsapi_base',
            'oauth' => [
                'scopes' => ['snsapi_base'],
                'response_type' => 'code',
                'callback' => url('api/wftpay/weixin/oauth_callback_openid?sub_info=' . $sub_info . '&wx_AppId=' . $wftpayConfig->wx_appid . '&wx_Secret=' . $wftpayConfig->wx_secret . ''),
            ]
        ];
        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;

        return $oauth->redirect();
    }


    //授权获取openID回调
    public function oauth_callback_openid(Request $request)
    {
        $sub_info = $request->get('sub_info');
        $code = $request->get('code');
        $wx_AppId = $request->get('wx_AppId');
        $wx_Secret = $request->get('wx_Secret');

        $sub_info_arr = json_decode(base64_decode((string)$sub_info), true);

        $config = [
            'app_id' => $wx_AppId,
            "secret" => $wx_Secret,
            "code" => $code,
            "grant_type" => "authorization_code",
        ];
        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;
        $user = $oauth->user();
        $open_id = $user->getId();

        $sub_info_arr['open_id'] = $open_id;
        $sub_info_arr['store_address'] = '';
        $callback_url = $sub_info_arr['callback_url'];

        $data = base64_encode(json_encode((array)$sub_info_arr));

        return redirect($callback_url . '?data=' . $data);
    }


}
