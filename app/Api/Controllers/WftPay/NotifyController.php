<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/10/21
 * Time: 10:28
 */
namespace App\Api\Controllers\WftPay;


use App\Api\Controllers\BaseController;
use App\Api\Controllers\WftPay\BaseController as WftPayBaseController;
use App\Common\PaySuccessAction;
use App\Models\MerchantWalletDetail;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\RefundOrder;
use App\Models\UserWalletDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class NotifyController extends BaseController
{
    //威富通 支付回调
    public function pay_notify_url(Request $request)
    {
        try {
            $data = $request->getContent();
//            Log::info('威富通支付-原始回调');
//            Log::info($data);

            $wftpay_obj = new WftPayBaseController();
            $data = $wftpay_obj->setContent($data);
            Log::info('威富通支付-回调-数组');
            Log::info($data);
//            $data = array (
//                'bank_type' => 'ALIPAYACCOUNT',
//                'buyer_logon_id' => '158****2202',
//                'buyer_pay_amount' => '0.01',
//                'buyer_user_id' => '2088012904744331',
//                'charset' => 'UTF-8',
//                'coupon_fee' => '0',
//                'device_info' => 'QR_ORDER',
//                'fee_type' => 'CNY',
//                'fund_bill_list' => '[{"amount":"0.01","fundChannel":"ALIPAYACCOUNT"}]',
//                'gmt_create' => '20201106144544',
//                'gmt_payment' => '20201106144548',
//                'invoice_amount' => '0.01',
//                'mch_id' => '187540042613',
//                'mdiscount' => '0',
//                'nonce_str' => '1604645149197',
//                'openid' => '2088012904744331',
//                'out_trade_no' => 'ali_qr20201106144536650811344',
//                'out_transaction_id' => '2020110622001444335717638864',
//                'pay_result' => '0',
//                'point_amount' => '0.00',
//                'receipt_amount' => '0.01',
//                'result_code' => '0',
//                'sign' => 'L3KlQJh0zl9R+5VWbZrGCiUl+FXdeBJkeg6148bMI/PaXAvYKFAVuq9zw18cb0APxtry3a1YAVP3XhsUStn/Yidv4XphTV3V+5u5HnSADd0kLo3+1YpDOAgC4hc1mO8sCYtN6Ii6p0JmmzacBDlppVn7vn+7YYqGW//yvNaFHuEG5gjePrIob7oSKi/+Un4eQ840OyaOGcetCidmjhUj1dvySzEjNYuuXB9QHFHiUsAobrqg8le1IRPvlqN4tN2QEIH1BwZcB/OTFHOhDyhgKnLybeENsk/MyPgC/hmEir4Ta8G47ARSkpTRbWki1YpZOuP/5kruarWWdIXrzXc8XA==',
//                'sign_type' => 'RSA_1_256',
//                'status' => '0',
//                'time_end' => '20201106144548',
//                'total_fee' => '1',
//                'trade_type' => 'pay.alipay.jspay',
//                'transaction_id' => '187540042613202011067204499467',
//                'version' => '2.0',
//            );

            if (isset($data['out_trade_no'])) {
                $out_trade_no = $data['out_trade_no'];

                $day = date('Ymd', time());
                $table = 'orders_' . $day;
                if (Schema::hasTable($table)) {
                    $order = DB::table($table)->where('out_trade_no', $out_trade_no)->first();
                } else {
                    $order = Order::where('out_trade_no', $out_trade_no)->first();
                }

                //订单存在
                if ($order) {
                    //系统订单未成功
                    if ($order->pay_status == 2) {
                        $trade_no = isset($data['transaction_id']) ? $data['transaction_id'] : "";
                        $pay_time = date('Y-m-d H:i:s', strtotime($data['time_end']));
                        $buyer_pay_amount = $data['total_fee'];
                        $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                        $buyer_id = isset($data['buyer_user_id']) ? $data['buyer_user_id'] : "";

                        $in_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_id' => $buyer_id,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($in_data, $out_trade_no);

                        if (strpos($out_trade_no, 'scan')) {

                        } else {
                            //支付成功后的动作
                            $data = [
                                'ways_type' => $order->ways_type,
                                'company' => $order->company,
                                'ways_type_desc' => $order->ways_type_desc,
                                'source_type' => '27000',//返佣来源
                                'source_desc' => '威富通',//返佣来源说明
                                'total_amount' => $order->total_amount,
                                'out_trade_no' => $order->out_trade_no,
                                'other_no' => $order->other_no,
                                'rate' => $order->rate,
                                'fee_amount' => $order->fee_amount,
                                'merchant_id' => $order->merchant_id,
                                'store_id' => $order->store_id,
                                'user_id' => $order->user_id,
                                'config_id' => $order->config_id,
                                'store_name' => $order->store_name,
                                'ways_source' => $order->ways_source,
                                'pay_time' => $pay_time,
                                'device_id' => $order->device_id,
                            ];
                            PaySuccessAction::action($data);
                        }
                    }
                }
            }

            return 'success';
        } catch (\Exception $ex) {
            Log::info('威富通支付异步报错');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
        }
    }


    //威富通 退款回调
    public function refund_notify_url(Request $request)
    {
        try {
            $data = $request->getContent();
            Log::info('威富通退款-回调-原始数据');
            Log::info($data);

            $wftpay_obj = new WftPayBaseController();
            $data = $wftpay_obj->setContent($data);
            Log::info('威富通退款-回调-数组');
            Log::info($data);

            if (isset($data['out_trade_no'])) {
                $out_trade_no = $data['out_trade_no'];
                $a = array('a', '_', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
                $b = str_ireplace($a, "", $out_trade_no);
                $day = substr($b, 0, 8);
                $table = 'orders_' . $day;

                $out_trade_no = substr($out_trade_no, 0, strlen($out_trade_no)-4);

                if (Schema::hasTable($table)) {
                    $order = DB::table($table)->where('out_trade_no', $out_trade_no)->first();
                } else {
                    $order = Order::where('out_trade_no', $out_trade_no)->first();
                }

                //订单存在
                if ($order && $order->pay_status == '1') {
                    if ($data['result_code'] == "0") {
                        $refund_amount = $data["refund_fee"];
                        $update_data = [
                            'status' => 6,
                            'pay_status' => 6,
                            'pay_status_desc' => '已退款',
                            'refund_amount' => $order->refund_amount + $refund_amount,
                            'fee_amount' => 0,
                        ];
                        if (Schema::hasTable($table)) {
                            DB::table($table)->where('out_trade_no', $out_trade_no)
                                ->update($update_data);
                        } else {
                           Order::where('out_trade_no', $out_trade_no)
                                ->update($update_data);
                        }
                        if (Schema::hasTable('order_items')) {
                            OrderItem::where('out_trade_no', $out_trade_no)->update($update_data);
                        }

                        RefundOrder::create([
                            'ways_source' => $order->ways_source,
                            'type' => $order->ways_type,
                            'refund_amount' => $refund_amount, //退款金额
                            'refund_no' => $data['refund_id'], //退款单号
                            'store_id' => $order->store_id,
                            'merchant_id' => $order->merchant_id,
                            'out_trade_no' => $order->out_trade_no,
                            'trade_no' => $order->other_no
                        ]);

                        //返佣去掉
                        UserWalletDetail::where('out_trade_no', $order->out_trade_no)->update([
                            'settlement' => '03',
                            'settlement_desc' => '退款订单',
                        ]);
                        MerchantWalletDetail::where('out_trade_no', $order->out_trade_no)->update([
                            'settlement' => '03',
                            'settlement_desc' => '退款订单',
                        ]);
                    }
                }
            }

            return 'success';
        } catch (\Exception $ex) {
            Log::info('威富通-退款回调异步报错');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
        }
    }


}
