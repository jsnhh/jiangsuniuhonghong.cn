<?php

namespace App\Api\Controllers\CcBankPay;

use App\Api\Controllers\Config\VbillConfigController;
use App\Models\VbillConfig;

use App\Models\CustomerAppletsUserOrders;
use App\Models\CustomerAppletsUserOrderGoods;
use function EasyWeChat\Kernel\Support\get_client_ip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PayController extends BaseController
{
    //扫一扫 0-系统错误 1-成功 2-正在支付 3-失败
    public function scan_pay($data)
    {
        try {
            $custId = $data['cust_id'];//商户号
            $posId = $data['pos_id'];//柜台号
            $branchId = $data['branch_id'];//分行号
            $orderNo = $data['out_trade_no'];//订单号
            $code = $data['code'];//支付编码
            $amount = $data['total_amount'];//支付金额
            $publicKey = $data['public_key'];//商户柜台公钥
            $termno1 = $data['termno1'];
            $termno2 = $data['termno2'];
            $data_pay = [
                'MERCHANTID' => $custId,//商户号
                'POSID' => $posId,
                'BRANCHID' => $branchId,
                'TXCODE' => 'PAY100',
                'MERFLAG' => '2',
                'ORDERID' => $orderNo,
                'QRCODE' => $code,
                'AMOUNT' => $amount,
            ];
            $sing_data = [
                'TXCODE' => 'PAY100',
                'MERFLAG' => '2',
                'ORDERID' => $orderNo,
                'QRCODE' => $code,
                'AMOUNT' => $amount,
            ];
            if ($termno1) {
                $data_pay['TERMNO1'] = $data['termno1'];
                $sing_data['TERMNO1'] = $data['termno1'];
            }
            if ($termno2) {
                $data_pay['TERMNO2'] = $data['termno2'];
                $sing_data['TERMNO2'] = $data['termno2'];
            }
            $sign = $this->calSign($this->sortParams($sing_data));
            $sing_data['SIGN'] = $sign;
            $params = http_build_query($sing_data);
            $pubKey = substr($publicKey, -30);
            $pubKey = substr($pubKey, 0, 8);
            $data_pay['ccbParam'] = $this->calCcbParam($params, $pubKey);
            $url = $this->scan_url . '?' . http_build_query($data_pay);
            Log::info('建设银行-静态二维码-params：');
            $re = $this->request($url);
            Log::info($url);
            Log::info("建设银行-支付返回-" . $re);
            $res_arr = json_decode($re, true);
            //系统错误
            if (!$res_arr) {
                return $res_arr;
            }

            //业务成功
            if ($res_arr['RESULT'] == "Y") {
                //交易成功
                return [
                    'status' => '1',
                    'message' => '支付成功',
                    'data' => $res_arr,
                ];
            } elseif ($res_arr['RESULT'] == "U") {
                return [
                    'status' => '2',
                    'message' => '待确认',
                    'data' => $res_arr,
                ];
            } elseif ($res_arr['RESULT'] == "Q") {
                return [
                    'status' => '2',
                    'message' => '待支付',
                    'data' => $res_arr,
                ];
            } elseif ($res_arr['RESULT'] == "N") {
                return [
                    'status' => 3,
                    'message' => '支付失败',
                ];
            } else {
                return [
                    'status' => 3,
                    'message' => $res_arr,
                ];
            }

        } catch (\Exception $ex) {
            return [
                'status' => '0',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine(),
            ];
        }
    }


    //查询订单
    public function order_query($data)
    {
        try {
            $custId = $data['cust_id'];//商户号
            $posId = $data['pos_id'];//柜台号
            $branchId = $data['branch_id'];//分行号
            $orderNo = $data['out_trade_no'];//订单号
            $qrCodeType = $data['qr_code_type'];//交易类型编码
            $publicKey = $data['public_key'];//商户柜台公钥
            $qrytime = $data['qrytime'];//查询次数
            $data_query = [
                'MERCHANTID' => $custId,//商户号
                'POSID' => $posId,
                'BRANCHID' => $branchId,
                'TXCODE' => 'PAY101',
                'MERFLAG' => '2',
                'ORDERID' => $orderNo,
                'QRCODETYPE' => $qrCodeType,
                'QRYTIME' => $qrytime
            ];
            $sing_data=[
                'TXCODE' => 'PAY101',
                'MERFLAG' => '2',
                'ORDERID' => $orderNo,
                'QRCODE' => $qrCodeType,
                'QRYTIME' => $qrytime,
                'QRCODETYPE' => $qrCodeType,
            ];
            if ($data['termno1']) {
                $data_query['TERMNO1'] = $data['termno1'];
                $sing_data['TERMNO1'] = $data['termno1'];
            }
            if ($data['termno2']) {
                $data_query['TERMNO2'] = $data['termno2'];
                $sing_data['TERMNO2'] = $data['termno2'];
            }

            $sign = $this->calSign($this->sortParams($sing_data));
            $sing_data['SIGN']=$sign;
            $params = http_build_query($sing_data);
            $pubKey = substr($publicKey, -30);
            $pubKey = substr($pubKey, 0, 8);
            $data_query['ccbParam'] = $this->calCcbParam($params, $pubKey);
            $url = $this->order_query_url . '?' . http_build_query($data_query);
            Log::info('建设银行订单查询-params：');
            Log::info($url);
            $re = $this->request($url);
            Log::info("建设银行订单查询结果--" . $re);
            $res_arr = json_decode($re, true);
            //系统错误
            if (!$res_arr) {
                return $res_arr;
            }
            if ($qrytime == 1 && $res_arr['RESULT'] == "N" && $res_arr['ORDERID'] == '') {
                return [
                    'status' => '2',
                    'message' => '待确认',
                    'data' => $res_arr,
                ];
            }
            //业务成功
            if ($res_arr['RESULT'] == "Y") {
                //交易成功
                return [
                    'status' => '1',
                    'message' => '支付成功',
                    'data' => $res_arr,
                ];
            } elseif ($res_arr['RESULT'] == "U") {
                return [
                    'status' => '2',
                    'message' => '待确认',
                    'data' => $res_arr,
                ];
            } elseif ($res_arr['RESULT'] == "Q") {
                return [
                    'status' => '2',
                    'message' => '待支付',
                    'data' => $res_arr,
                ];
            } elseif ($res_arr['RESULT'] == "N") {
                return [
                    'status' => 3,
                    'message' => '支付失败',
                ];
            } else {
                return [
                    'status' => 3,
                    'message' => $res_arr,
                ];
            }


        } catch (\Exception $ex) {
            return [
                'status' => '0',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine(),
            ];
        }
    }

}
