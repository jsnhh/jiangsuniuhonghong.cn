<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/9/16
 * Time: 14:01
 */
namespace App\Api\Controllers\DaDa;


use Illuminate\Support\Facades\Log;
use App\Api\Controllers\BaseController as BBaseController;

/**
 * * Class BaseController
 * @package App\Api\Controllers\DaDa
 * 1. 在线上环境注册后，账号信息会自动同步到测试环境。
 * 2. 在进行接口调试时，域名切换到测试域名。
 * 3. 所有接口对接调试完成后，可以切换到线上环境域名。
 * 4. 测试环境仅供接口对接测试。
 * 5. 测试环境的订单数据仅保留1天。
 */
class BaseController extends BBaseController
{
//    private $url = 'https://newopen.imdada.cn/'; //线上域名
    private $url = 'https://newopen.qa.imdada.cn/'; //测试域名

    protected $addOrder = 'api/order/addOrder'; //新增订单
    protected $reAddOrder = 'api/order/reAddOrder'; //重发订单
    protected $formalCancel = 'api/order/formalCancel'; //取消订单链接
    protected $status_query = 'api/order/status/query'; //订单详情查询
    protected $confirm_goods = 'api/order/confirm/goods'; //妥投异常货品返回，商家确认收货
    protected $message_confirm = 'api/message/confirm'; //消息确认

    protected $merchant_add = 'merchantApi/merchant/add'; //注册商户
    protected $shop_add = 'api/shop/add'; //新增门店
    protected $shop_update = 'api/shop/update'; //更新门店

    protected $city_code_list = 'api/cityCode/list'; //获取城市信息列表
    protected $order_cancel_reasons = 'api/order/cancel/reasons'; //获取取消原因
    protected $shop_detail = 'api/shop/detail'; //查询门店详情


    /**
     * 发起请求 1-成功 2-失败
     * @param string  $url  访问地址
     * @param string $app_key  应用Key，对应开发者账号中的app_key
     * @param  string $app_secret  应用secret，对应开发者账号中的app_secret
     * @param string $source_id  商户编号（创建商户账号分配的编号）,测试环境默认为：73753
     * @param string $body  业务参数，JSON字符串，当没有业务参数的时候, body需要赋值为空字符串,即body:""
     * @return mixed
     */
    public function http_post($url, $app_key, $app_secret, $source_id = '', $body = '')
    {
        $params_arr = [
            'app_key' => $app_key,
            'body' => json_encode($body, JSON_UNESCAPED_SLASHES),
            'format' => 'json',
            'source_id' => $source_id ?? "",
            'timestamp' => time(),
            'v' => '1.0'
        ];

        $signature = $this->counterSignature($params_arr, $app_secret); //参数加签
//        Log::info('加签后值');
//        Log::info($signature);
        $params_arr['signature'] = $signature; //签名Hash值
        $data = json_encode($params_arr, JSON_UNESCAPED_SLASHES);

        $url = $this->url . $url;
        $headers = array('Content-Type: application/json');

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_TIMEOUT, 3);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $resp = curl_exec($curl);
//        var_dump( curl_error($curl) ); //如果在执行curl的过程中出现异常，可以打开此开关查看异常内容。
        $info = curl_getinfo($curl);
        curl_close($curl);

        if (isset($info['http_code']) && $info['http_code'] == 200) {
            return [
                'status' => 1,
                'data' => $resp
            ];
        }

        return [
            'status' => '2',
            'message' => curl_error($curl)
        ];
    }


    /**
     * 签名生成signature
     * @param array $data  加签的数据
     * @param string $app_secret  应用secret，对应开发者账号中的app_secret
     * @return string
     */
    public function counterSignature($data, $app_secret)
    {
        //1.升序排序
        ksort($data);

        //2.字符串拼接
        $args = "";
        foreach ($data as $key => $value) {
            $args .= $key.$value;
        }

        $args = $app_secret . $args . $app_secret;

        //3.MD5签名,转为大写
        $sign = strtoupper(md5($args));

        return $sign;
    }


    //状态码定义
    public function getMessageByCode($code)
    {
        switch ($code) {
            case '-1':  return '系统异常';  break;
            case '0':  return '请求成功';  break;
            case '999':  return '系统维护中,暂时不能发单';  break;
            case '1000':  return '网络异常,获取数据超时,请重试';  break;
            case '1993':  return '参数不能为空';  break;
            case '1994':  return 'format值不正确,默认为json';  break;
            case '1995':  return 'body值不能为null';  break;
            case '1996':  return 'v的值不正确,默认为1.0';  break;
            case '1997':  return '原始订单order_id不能为空';  break;
            case '1998':  return '请求参数的个数不正确,请仔细核对';  break;
            case '1999':  return '接口请求的headers为application/json';  break;
            case '2000':  return '接口请求参数不能为空';  break;
            case '2001':  return 'app_key无效';  break;
            case '2002':  return '没有绑定正式商户,请检查接口中source_id值';  break;
            case '2003':  return '签名错误';  break;
            case '2004':  return '无效的门店编号';  break;
            case '2005':  return '订单不存在,请核查订单号';  break;
            case '2006':  return '订单回调URL不存在';  break;
            case '2007':  return '参数query需按要求传值';  break;
            case '2008':  return 'token不能为空';  break;
            case '2009':  return 'timestamp不能为空';  break;
            case '2010':  return 'signature不能为空';  break;
            case '2011':  return '达达订单不存在,数据异常';  break;
            case '2012':  return '订单正在处理中,请稍后再试';  break;
            case '2013':  return '请求接口参数异常,请查看开发文档参数设定';  break;
            case '2014':  return '创建商户失败';  break;
            case '2042':  return '使用保价失败,订单金额不能为空或0';  break;
            case '2043':  return '门店未审核';  break;
            case '2044':  return '城市尚未开通';  break;
            case '2045':  return '商家支付账号不存在';  break;
            case '2046':  return '商家编号不能为空';  break;
            case '2047':  return '运费服务不可用';  break;
            case '2048':  return '订单取消原因ID不能为空';  break;
            case '2049':  return '订单取消原因ID对应其他,取消原因不能为空';  break;
            case '2050':  return '订单配送中,无法取消';  break;
            case '2051':  return '订单已完成配送,无法取消';  break;
            case '2052':  return '订单已过期,无法取消';  break;
            case '2053':  return '订单取消失败';  break;
            case '2054':  return '小费不能为空或者字符,必须为数字';  break;
            case '2055':  return '小费额度不能少于1元';  break;
            case '2056':  return '城市区号不能为空';  break;
            case '2057':  return '新增小费不能小于原来订单小费的值';  break;
            case '2058':  return '添加小费失败';  break;
            case '2059':  return '只有在待接单的情况下才能加小费';  break;
            case '2060':  return '新的订单,不能重新发单,请走正常发单流程';  break;
            case '2061':  return '只有已取消、已过期、投递异常的订单才能重发';  break;
            case '2062':  return '订单价格信息已过期,请重新查询后发单';  break;
            case '2063':  return '该订单已发布,请选择新的订单';  break;
            case '2064':  return '订单已下发,且状态不是已取消、已过期、投递异常,请选择新的订单';  break;
            case '2065':  return '该平台订单编号已处理,请勿重复请求';  break;
            case '2066':  return '该平台订单编号已处理,请勿重复请求';  break;
            case '2067':  return '该订单运费已查询,请稍后再试';  break;
            case '2068':  return '接口仅测试环境可用';  break;
            case '2069':  return '追加的订单与门店不匹配';  break;
            case '2070':  return '追加的订单已被接单';  break;
            case '2071':  return '追加的配送员不符合追加要求';  break;
            case '2072':  return '订单没有追加记录';  break;
            case '2073':  return '订单状态已经改变,取消失败';  break;
            case '2074':  return '取消追加订单失败';  break;
            case '2075':  return '投诉原因不能为空';  break;
            case '2076':  return '订单已取消,无法重复取消';  break;
            case '2077':  return '小费金额不能大于订单金额';  break;
            case '2078':  return 'deliveryNo不能为空';  break;
            case '2079':  return '收货人纬度lat异常,请检查是否有问题';  break;
            case '2080':  return '收货人经度lng异常,请检查是否有问题';  break;
            case '2081':  return '【妥投异常】确认返回物失败';  break;
            case '2082':  return '确认失败,订单状态不是返回中';  break;
            case '2083':  return '模拟失败,订单必须被接单';  break;
            case '2084':  return '发货方经度lat取值异常,请检查是否有问题';  break;
            case '2085':  return '发货方纬度lat取值异常,请检查是否有问题';  break;
            case '2086':  return '运力不充足,发单失败';  break;
            case '2087':  return '发单失败';  break;
            case '2088':  return '传参不合法,请选择范围内的状态值';  break;
            case '2089':  return '订单不合要求';  break;
            case '2090':  return '网络异常,请求失败';  break;
            case '2091':  return '暂时不支持该消息类型';  break;
            case '2103':  return 'cargo_weight必须大于0';  break;
            case '2104':  return '原始订单origin_id不能为空';  break;
            case '2105':  return '订单已下发,如要重发,请使用重发接口';  break;
            case '2106':  return 'pay_for_supplier_fee不能为空';  break;
            case '2107':  return 'fetch_from_receiver_fee不能为空';  break;
            case '2108':  return 'deliver_fee不能为空';  break;
            case '2109':  return 'is_prepay不能为空';  break;
            case '2110':  return 'App密码设置不符合要求,必须包含数字和字母,长度在6~16内';  break;
            case '2111':  return 'cargo_type不能为空';  break;
            case '2112':  return 'cargo_weight不能为空';  break;
            case '2113':  return 'cargo_price不能为空';  break;
            case '2114':  return 'supplier_name不能为空';  break;
            case '2115':  return 'supplier_address不能为空';  break;
            case '2116':  return 'supplier_phone不能为空';  break;
            case '2117':  return 'supplier_tel不能为空';  break;
            case '2118':  return 'supplier_lat不能为空';  break;
            case '2119':  return 'supplier_lng不能为空';  break;
            case '2120':  return 'receiver_name不能为空';  break;
            case '2121':  return 'receiver_address不能为空';  break;
            case '2122':  return 'receiver_phone不能为空'; break;
            case '2123':  return 'callback不能为空'; break;
            case '2124':  return 'expected_fetch_time不能为空'; break;
            case '2125':  return 'expected_finish_time不能为空'; break;
            case '2126':  return 'city_code不能为空'; break;
            case '2127':  return 'invoice_title不能为空'; break;
            case '2128':  return 'receiver_lat或receiver_lng不能为空'; break;
            case '2129':  return '无效的收货地址,解析地址坐标失败'; break;
            case '2130':  return '订单已发单,不能查询运费'; break;
            case '2131':  return 'source_id值不正确,测试环境默认为73753'; break;
            case '2132':  return '门店已下线,不能发单'; break;
            case '2133':  return '投诉原因id不存在,请重新选择'; break;
            case '2134':  return '投诉失败'; break;
            case '2135':  return '订单状态为待接单,不能投诉'; break;
            case '2136':  return '门店尚未绑定商品类型'; break;
            case '2137':  return '订单异常,配送员信息不存在'; break;
            case '2138':  return 'receiver_lat或receiver_lng值不能为null'; break;
            case '2139':  return '数据异常,receiver_lat大于receiver_lng值'; break;
            case '2140':  return '订单已重发,请稍后再试'; break;
            case '2141':  return '门店已下线,请至开放平台激活门店'; break;
            case '2155':  return '余额不足，请充值'; break;
            case '2170':  return '期望完成时间不合法'; break;
            case '2200':  return 'accountId不能为空'; break;
            case '2201':  return 'password不能为空'; break;
            case '2300':  return 'accountId不能为空'; break;
            case '2301':  return 'merhcantId不能为空'; break;
            case '2302':  return '与上次结果反馈不同'; break;
            case '2304':  return '系统异常，请重试'; break;
            case '2305':  return '拒绝原因必须要填写'; break;
            case '2306':  return '订单状态不能操作'; break;
            case '2307':  return '异常上报原因映射不存在'; break;
            case '2400':  return '	该商家尚未审核上线'; break;
            case '2401':  return '	商家不存在'; break;
            case '2402':  return '	门店不存在'; break;
            case '2403':  return '	门店编号已存在'; break;
            case '2404':  return '	城市名称city_name不正确'; break;
            case '2405':  return '	区域名称area_name不正确'; break;
            case '2406':  return '	没有可以更新的参数,请重新核对'; break;
            case '2407':  return '	业务类型不存在,请重新选择'; break;
            case '2408':  return '	门店状态不存在,请重新选择'; break;
            case '2409':  return '	新的门店编号不能与现有的门店编号相同'; break;
            case '2410':  return '	参数类型不正确, Double类型不能传null与字符串'; break;
            case '2411':  return '	参数类型不正确, pay_for_supplier_fee为Double类型'; break;
            case '2412':  return '	参数类型不正确, fetch_from_receiver_fee为Double类型'; break;
            case '2413':  return '	参数类型不正确, deliver_fee为Double类型'; break;
            case '2414':  return '	参数类型不正确, cargo_type值不在展示的列表中'; break;
            case '2415':  return '	参数类型不正确, cargo_weight为Double'; break;
            case '2416':  return '	参数类型不正确, cargo_num为Integer'; break;
            case '2417':  return '	参数类型不正确, 可以不传,但是不能传null值'; break;
            case '2418':  return '	expected_fetch_time为unix时间戳,精确到秒(10位)'; break;
            case '2419':  return '	body参数json解析出错,请检查body内的参数格式是否正确'; break;
            case '2420':  return '	QA环境禁止修改11047059门店编号'; break;
            case '2421':  return '	order_mark_no格式不正确,仅包含数字(长度小于15)或为空'; break;
            case '2422':  return '	order_mark格式不正确,仅包含字母(长度小于10)或为空'; break;
            case '2423':  return '	重复的用户名'; break;
            case '2424':  return '	门店编码不能重复'; break;
            case '2425':  return '	C端用户的商家不能绑定'; break;
            case '2426':  return '	已与当前商家绑定'; break;
            case '2427':  return '	已与其他商家绑定'; break;
            case '2428':  return '	门店编码已存在'; break;
            case '2429':  return '	查询余额失败'; break;
            case '2430':  return '	账户余额为负,无法绑定'; break;
            case '2431':  return '	未找到该用户名的门店或用户名密码错误'; break;
            case '2432':  return '	城市名称不正确'; break;
            case '2433':  return '	区域名称不正确'; break;
            case '2434':  return '	没有有效的数据'; break;
            case '2435':  return '	绑定失败，扣除余额失败'; break;
            case '2436':  return '	余额为负值，无法绑定'; break;
            case '2437':  return '	绑定失败，扣款发生异常'; break;
            case '2438':  return '	系统异常,绑定失败,请联系技术人员'; break;
            case '2439':  return '	transportId和transportPhone不能全为空'; break;
            case '2440':  return '	骑士不存在'; break;
            case '2441':  return '	骑士与该门店绑定关系不存在'; break;
            case '2442':  return '	骑士id不能为空'; break;
            case '2443':  return '	startTime不能为空'; break;
            case '2444':  return '	endTime不能为空'; break;
            case '2445':  return '	schduleDate不能为空'; break;
            case '2446':  return '	schduleId不能为空'; break;
            case '2447':  return '	schduleDate格式不正确'; break;
            case '2448':  return '	排班不存在'; break;
            case '2449':  return '	排班重复'; break;
            case '2450':  return '	排班冲突'; break;
            case '2451':  return '	该骑士已绑定过站点'; break;
            case '2452':  return '	其他骑手正在扫码,请稍后再试'; break;
            case '2453':  return '	门店更新失败'; break;
            case '2454':  return '	isUseInsurance参数不能为null'; break;
            case '2455':  return '	网络异常,查询账户余额失败,请重试'; break;
            case '2456':  return '	运费账户余额不足,无法使用保价'; break;
            case '2457':  return '	取消失败[cancel_reason_id不在取消原因列表中]'; break;
            case '2458':  return '	取消失败[订单状态为9,不能取消]'; break;
            case '2459':  return '	取消失败[订单状态为10,不能取消]'; break;
            case '2460':  return '	取消失败[订单状态不为待接单和待取货]'; break;
            case '2461':  return '	门店编号格式错误,支持数字字母以及下划线和-'; break;
            case '2462':  return '	充值金额格式不正确,可以精确到分'; break;
            case '2463':  return '	获取充值Url失败,请重试'; break;
            case '2464':  return '	集合单已被接收'; break;
            case '2465':  return '	请选择合适的场景(category)'; break;
            case '2466':  return '	选择生成微信支付链接,open_id不能为空'; break;
            case '2467':  return '	请选择合适的category值'; break;
            case '2468':  return '	预约时间unix时间戳(10位),精确到分;整1分钟为间隔，并且需要至少提前5分钟预约'; break;
            case '2469':  return '	集合单下发中,请稍后再试'; break;
            case '2470':  return '	集合单下发失败'; break;
            case '2471':  return '	集合单取消失败'; break;
            case '2472':  return '	is_direct_delivery不能为null'; break;
            case '2473':  return '	access_token不存在'; break;
            case '2474':  return '	获取妥投异常商品失败'; break;
            case '2475':  return '	商家没有扫码接单权限'; break;
            case '2476':  return '	订单已被接单'; break;
            case '2477':  return '	差评一定要加评价信息哦'; break;
            case '2478':  return '	提交评分失败'; break;
            case '2479':  return '	商品名称不能为空,且长度限制为128'; break;
            case '2480':  return '	商品数量必须大于等于0,最高精确到小数点后2位'; break;
            case '2481':  return '	商品编号长度不能超过64'; break;
            case '2482':  return '	取货信息长度不能超过32'; break;
            case '2483':  return '	门店状态异常'; break;
            case '2500':  return '请选择合适的productType值'; break;
            case '2501':  return '请选择合适的productId值'; break;
            case '2502':  return 'supplierOrderId不能为空,且长度限制为100'; break;
            case '2503':  return 'senderName不能为空,且长度限制为20'; break;
            case '2504':  return 'senderPhone不能为空,且长度限制为15'; break;
            case '2505':  return 'senderAddress不能为空,且长度限制为100'; break;
            case '2506':  return 'receiverName不能为空,且长度限制为20'; break;
            case '2507':  return 'receiverPhone不能为空,且长度限制为15'; break;
            case '2508':  return 'receiverAddress不能为空,且长度限制为100'; break;
            case '2509':  return 'cargoWeight不能为空,精确到小数点后1位'; break;
            case '2510':  return 'senderLat最高精确到6位小数'; break;
            case '2511':  return 'senderLat最高精确到6位小数'; break;
            case '2512':  return 'receiverLat最高精确到6位小数'; break;
            case '2513':  return 'receiverLng最高精确到6位小数'; break;
            case '2514':  return 'orderRemark长度不能超过100'; break;
            case '2515':  return 'frameNumber长度不能超过50'; break;
            case '2516':  return 'frameQRCode长度不能超过100'; break;
            case '2517':  return 'cargoCount最小数值为1'; break;
            case '2518':  return 'callbackUrl最长长度为200'; break;
            case '2519':  return 'cityCode或者cityName不能同时为空'; break;
            case '2520':  return '经纬度范围有问题,请检查经纬度值'; break;
            case '2521':  return 'nix时间戳，精确到分钟且分钟数是5的倍数，秒必须为0，且要大于当前时间5分钟以上'; break;
            case '2522':  return '收发件人地址区域编码adCode不能为空'; break;
            case '2523':  return 'style取值错误,1=发件地址,2=收件地址'; break;
            case '2524':  return '经度精度错误,最多6位小数'; break;
            case '2525':  return '纬度错误,最多6位小数'; break;
            case '2526':  return '发件人地址不能为空,需要包含省市区详细地址'; break;
            case '2527':  return '收件人地址不能为空,需要包含省市区详细地址'; break;
            case '2528':  return '第三方订单号originId不能为空'; break;
            case '2529':  return '取消原因ID不能为空'; break;
            case '2530':  return '订单不存在,请检查订单号'; break;
            case '2531':  return '订单信息解析失败'; break;
            case '2532':  return 'cityCode不能为空'; break;
            case '2533':  return '请求失败'; break;
            case '2534':  return '确认收货失败'; break;
            case '2902':  return '无处理中的消息事'; break;
            case '2903':  return '预约单当前状态不支持指派'; break;
            case '3001':  return '请求失败'; break;
            case '3002':  return '参数错误'; break;
            case '3003':  return '商家不存在'; break;
            case '3004':  return '订单已过期'; break;
            case '3100':  return '开票前需要先同意税点提示'; break;
            case '6600':  return '名称不能为空'; break;
            case '6601':  return 'token不能为空'; break;
            case '6602':  return '商户ID不能为空'; break;
            case '6603':  return 'ID不能为空'; break;
            case '7700':  return '门店名称不能为空'; break;
            case '7701':  return '门店业务business不能为空'; break;
            case '7702':  return '门店联系人contact_name不能为空'; break;
            case '7703':  return '门店联系人电话不能为空'; break;
            case '7704':  return '门店所在省province不能为空'; break;
            case '7705':  return '门店所在市city不能为空'; break;
            case '7706':  return '门店所在区region不能为空'; break;
            case '7707':  return '门店详细地址address不能为空'; break;
            case '7708':  return '门店经度longitude不能为空'; break;
            case '7709':  return '门店纬度latitude不能为空'; break;
            case '7710':  return '门店状态status不能为空'; break;
            case '7711':  return '门店ID不能为空'; break;
            case '7712':  return '门店商户ID不能为空'; break;
            case '7713':  return '门店信息编辑人不能为空'; break;
            case '7714':  return '门店标签不能为空'; break;
            case '7715':  return '商家门店编号不能为空'; break;
            case '7716':  return '上传文件失败'; break;
            case '7717':  return '身份证不能为空,且格式要求15位或18位'; break;
            case '7718':  return '创建门店失败'; break;
            case '8800':  return '应用名称不能为空'; break;
            case '8801':  return '应用服务类型不能为空'; break;
            case '8802':  return '应用服务类型名称不能为空'; break;
            case '8803':  return 'app_key不能为空'; break;
            case '8804':  return 'supplier_id不能为空'; break;
            case '8805':  return '不是大客户门店'; break;
            case '8806':  return 'city_id不能为空'; break;
            case '8807':  return '该门店没有默认支付账号'; break;
            case '8808':  return '达达商家app账号已存在,请通过绑定处理'; break;
            case '8809':  return '余额查询失败'; break;
            case '8810':  return '请选择账户类型'; break;
            case '8811':  return 'distance不能为空'; break;
            case '8812':  return '门店联系人电话号码无效或格式不正确'; break;
            case '8813':  return '权限不足,操作失败'; break;
            case '8814':  return '门店信息重复,达达侧门店已存在'; break;
            case '8815':  return '达达transporterId不存在'; break;
            case '8816':  return '批次号不能为空'; break;
            case '8817':  return 'reasonId不能为空'; break;
            case '8818':  return '开放平台商户不能添加个人类型订单'; break;
            case '9910':  return '用户名或密码错误'; break;
            case '9901':  return '无效用户或用户不存在'; break;
            case '9902':  return '用户已存在'; break;
            case '9903':  return '参数不能为空'; break;
            case '9904':  return '编号ID不能为空'; break;
            case '9905':  return '获取失败'; break;
            case '9906':  return '删除失败'; break;
            case '9907':  return '图形验证码无效'; break;
            case '9908':  return '参数不能为空'; break;
            case '9909':  return '用户未登录'; break;
            case '9910':  return '账号不能为空'; break;
            case '9911':  return '密码不能为空'; break;
            case '99123':  return '新密码不能为空'; break;
            case '9912':  return '联系方式不能为空'; break;
            case '9913':  return '用户ID不存在'; break;
            case '9914':  return '邮箱不能为空'; break;
            case '9915':  return '手机验证码不能为空'; break;
            case '9916':  return '再次确认密码不能为空'; break;
            case '9918':  return '联系人手机号不正确'; break;
            case '9919':  return '企业地址不能为空'; break;
            case '9920':  return '企业全称不能为空'; break;
            case '9921':  return '城市不能为空'; break;
            case '9922':  return '达达订单号不存在'; break;
            case '9924':  return '手机号不正确'; break;
            case '9925':  return '联系人不能为空'; break;
            case '9930':  return '查询门店数超过100，请选择门店'; break;
            case '9931':  return '查询订单数超出限制'; break;
            case '9932':  return '订单号跟运单号不能同时为空'; break;
            case '9933':  return '骑士接单失败，请稍后重试'; break;
            case '9934':  return '获取验证码失败，请稍后重试'; break;
            case '9935':  return '查找对应的门店信息失败'; break;
            case '9936':  return '查询结果为空'; break;
            case '9937':  return '获取门店码失败，请稍后重试'; break;
            case '9938':  return '设置开关失败，请稍后重试'; break;
            case '9939':  return '催单频率太高，请稍后再试'; break;
            case '9940':  return '订单状态不支持催单'; break;
            case '9941':  return '此订单非售后单或非送回门店状态'; break;
            case '9942':  return '订单状态不支持修改'; break;
            case '10100':  return '参数不能为空'; break;
            case '45001':  return '该门店已关闭扫码到店'; break;
            default:  return '未知错误,请请联系技术人员';
        }
    }


}
