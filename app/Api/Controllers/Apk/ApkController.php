<?php


namespace App\Api\Controllers\Apk;


use App\Api\Controllers\BaseController;
use App\Models\AppUpdate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApkController extends BaseController
{
    //广告列表
    public function apkLists(Request $request)
    {
        try {

//            $public = $this->parseToken();
//            $user_id = '';
//            if ($public->type == "merchant") {
//                $user_id = $public->merchant_id;
//            }
//
//            if ($public->type == "user") {
//                $user_id = $public->user_id;
//            }


            $obj = DB::table('app_updates');


//            $obj->whereIn('created_id', $this->getSubIds($user_id))
//                ->where('model_type', $public->type)
//                ->orderBy('updated_at', 'desc');

            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    public function apkInfo(Request $request)
    {
        try {
            $public = $this->parseToken();

            $id = $request->get('id', '');
            $apk = AppUpdate::where('id', $id)->first();
            $this->message = '数据返回成功';
            return $this->format($apk);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    public function apkUpload(Request $request)
    {
        $file_name = $_FILES['apk_upload']['name'];
        $file_type = $_FILES["apk_upload"]["type"];
        $file_tmp = $_FILES["apk_upload"]["tmp_name"];
        $file_error = $_FILES["apk_upload"]["error"];
        $file_size = $_FILES["apk_upload"]["size"];
        if ($file_error > 0) { // 出错
            return json_encode([

                'status' => 2,
                'message' => $file_error,
            ]);
        }

        if ($file_size > 50485769999) { // 文件太大了
            return json_encode([
                'status' => 2,
                'message' => '上传文件过大',
            ]);
        }
        $file_name_arr = explode('.', $file_name);
        if(!in_array(strtolower($file_name_arr[count($file_name_arr)-1]),['sdk','apk'])){
            return json_encode([
                'status' => 2,
                'message' => '格式不支持',
            ]);
        }
        $path = md5(time() . rand(100000,999999)) . '.' . $file_name_arr[count($file_name_arr)-1];
        $file_path = "/apk/" . $path;
        $file_apk = public_path().'/apk/';
        if(!file_exists($file_apk)){
            mkdir($file_apk,0777,true);
        }
        $upload_result = move_uploaded_file($file_tmp, public_path() . $file_path); // 此函数只支持 HTTP POST 上传的文件
        if ($upload_result) {
            $url = url($file_path);
            $data_return['apk_url'] = $url;
            return json_encode([
                'status' => 1,
                'data' => $data_return,
            ]);
        } else {
            return json_encode([
                'status' => 2,
                'message' => '系统错误重新上传',
            ]);
        }

    }

    public function apkCreate(Request $request)
    {
        try {
//            $public = $this->parseToken();
//            $config_id = $public->config_id;
//            $user_id = '';
//            if ($public->type == "merchant") {
//                $user_id = $public->merchant_id;
//            }
//            if ($public->type == "user") {
//                $user_id = $public->user_id;
//            }

            $app_id = $request->get('app_id', '');
            $type = $request->get('type', 'face_TZH-L1');
            $version = $request->get('version', '');
            $msg = $request->get('msg', '');
            $UpdateUrl = $request->get('update_url', '');
            if ($UpdateUrl == "NULL") {
                $UpdateUrl = "";
            }
            if ($msg == "NULL") {
                $msg = "";
            }
            $check_data = [
                'type' => '类型',
//                'app_id' => 'app_id',
                'version' => '版本',
                'update_url' => 'apk包位置',
//                'msg' => '信息',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }



            $data = [
                'type' => $type,
                'app_id' => $app_id,
                'version' => $version,
                'msg' => isset($msg) ? $msg : "",
                'UpdateUrl' => isset($UpdateUrl) ? $UpdateUrl : "",
            ];
            AppUpdate::create($data);


            return json_encode([
                'status' => 1,
                'message' => '添加成功',
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    public function apkUp(Request $request)
    {
        try {
//            $public = $this->parseToken();

//            if ($public->type == "merchant") {
//                $user_id = $public->merchant_id;
//            }
//
//            if ($public->type == "user") {
//                $user_id = $public->user_id;
//            }

            $id = $request->get('id', '');
            $app_id = $request->get('app_id', '测试');
            $type = $request->get('type', '');
            $version = $request->get('version', '');
            $msg = $request->get('msg', '');
            $UpdateUrl = $request->get('update_url', '');



            if ($UpdateUrl == "NULL") {
                $UpdateUrl = "";
            }


            $data = [
                'type' => $type,
                'app_id' => $app_id,
                'version' => $version,
                'msg' => $msg,
                'UpdateUrl' => isset($UpdateUrl) ? $UpdateUrl : "",
            ];

            AppUpdate::where('id', $id)->update($data);
            return json_encode([
                'status' => 1,
                'message' => '修改成功',
            ]);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    //apk包删除
    public function apkDel(Request $request)
    {
        try {
//            $public = $this->parseToken();
//
//            if ($public->type == "merchant") {
//                $user_id = $public->merchant_id;
//            }
//
//            if ($public->type == "user") {
//                $user_id = $public->user_id;
//            }

            $id = $request->get('id', '');

            AppUpdate::where('id', $id)->delete();

            return json_encode([
                'status' => 1,
                'message' => '删除成功',
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }





}