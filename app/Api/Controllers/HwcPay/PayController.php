<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/10/21
 * Time: 10:28
 */
namespace App\Api\Controllers\HwcPay;


use function EasyWeChat\Kernel\Support\get_client_ip;
use Illuminate\Support\Facades\Log;

class PayController extends BaseController
{
    //付款码支付 b_2_c 0-系统错误 1-成功 2-失败
    public function scan_pay($data)
    {
        try {
            $mch_id = $data['mch_id'];
            $out_trade_no = $data['out_trade_no'];
            $body = $data['body'];
            $total_amount = $data['total_amount'];
            $code = $data['code'];
            $private_rsa_key = $data['private_rsa_key'];
            $public_rsa_key = $data['public_rsa_key'];

            $req_data = [
                'service' => 'unified.trade.micropay', //接口类型
                'sign_type' => 'RSA_1_256', //签名类型，取值RSA_1_256或RSA_1_1
                'mch_id' => $mch_id, //商户号，由平台分配
                'out_trade_no' => $out_trade_no, //商户系统内部的订单号,5到32个字符、只能包含字母数字或者下划线，区分大小写，每次下单请求确保在商户系统唯一
                'body' => $body, //商品描述
                'total_fee' => $total_amount*100, //总金额，以分为单位，不允许包含任何字、符号
                'mch_create_ip' => get_client_ip(), //上传商户真实的发起交易的终端出网IP
                'auth_code' => $code, //扫码支付授权码，设备读取用户展示的条码或者二维码信息
                'nonce_str' => $this->nonceStr() //随机字符串，不长于 32 位
            ];
//            Log::info('汇旺财-付款码支付入参-结果');
//            Log::info($req_data);
            $res = $this->call($req_data, $private_rsa_key, $public_rsa_key); //1-成功;2-失败
//            Log::info('汇旺财-付款码支付-结果');
//            Log::info($res);
//            ①支付请求后：status和result_code字段返回都为0时，判定订单支付成功；
//            ②支付请求后：返回的参数need_query为Y或没有该参数返回时，必须调用订单查询接口进行支付结果确认（查询接口调用建议参照第④点）；
//            ③支付请求后：返回的参数need_query为N时，可明确为失败订单；
//            ④调用订单查询接口建议：查询6次每隔5秒查询一次（具体的查询次数和时间也可自定义，建议查询时间不低于30秒）6次查询完成，接口仍未返回成功标识(即查询接口返回的trade_state不是SUCCESS)则调用撤销接口；
            //业务成功
            if ($res['status'] == 1) {
                //交易成功
                if ($res['data']['pay_result'] == 0) { //支付结果：0—成功；其它—失败
                    return [
                        'status' => 1,
                        'message' => '交易成功',
                        'data' => $res['data']
                    ];
                } else {
                    return [
                        'status' => 2,
                        'message' => $res['message'],
                        'data' => $res['data']
                    ];
                }
            } else {
                return [
                    'status' => 2,
                    'message' => isset($res['message']) ? $this->getErrorMessageByPay($res['message']) : '汇旺财付款码支付,错误未知',
                    'data' => $res['data']
                ];
            }

        } catch (\Exception $ex) {
            return [
                'status' => 0,
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine(),
            ];
        }
    }


    //扫码支付 生成二维码支付 b_2_c 0-系统错误 1-成功 2-失败
    public function send_qr($data)
    {
        try {
            $mch_id = $data['mch_id'];
            $out_trade_no = $data['out_trade_no'];
            $body = $data['body'];
            $total_amount = $data['total_amount'];
            $private_rsa_key = $data['private_rsa_key'];
            $public_rsa_key = $data['public_rsa_key'];

            $req_data = [
                'service' => 'unified.trade.native', //接口类型
                'sign_type' => 'RSA_1_256', //签名类型，取值RSA_1_256或RSA_1_1
                'mch_id' => $mch_id, //门店号，由平台分配
                'out_trade_no' => $out_trade_no, //商户系统内部的订单号,5到32个字符、只能包含字母数字或者下划线，区分大小写，每次下单请求确保在商户系统唯一
                'body' => $body, //商品描述
                'total_fee' => $total_amount*100, //总金额，以分为单位，不允许包含任何字、符号
                'mch_create_ip' => get_client_ip(), //上传商户真实的发起交易的终端出网IP
                'notify_url' => url('/api/hwcpay/pay_notify_url'), //接收平台通知的URL，需给绝对路径，255字符内格式如:http://wap.tenpay.com/tenpay.asp，确保平台能通过互联网访问该地址
                'nonce_str' => $this->nonceStr() //随机字符串，不长于 32 位
            ];
            $res = $this->call($req_data, $private_rsa_key, $public_rsa_key); //1-成功;2-失败
//            Log::info('汇旺财-扫码支付-结果');
//            Log::info($res);
            //业务成功
            if ($res['status'] == 1) {
                return [
                    'status' => 1,
                    'message' => '交易成功',
                    'data' => $res['data']
                ];
            } else {
                return [
                    'status' => 2,
                    'message' => isset($res['message']) ? $this->getErrMessageByCodePay($res['message']) : '扫码支付error',
                    'data' => $res['data']
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => 0,
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine(),
            ];
        }
    }


    //公众号&小程序支付 c_2_b 0-系统错误 1-成功 2-失败
    public function qr_submit($data)
    {
        try {
            $mch_id = $data['mch_id'];
            $out_trade_no = $data['out_trade_no'];
            $body = $data['body'];
            $total_amount = $data['total_amount'];
            $sub_openid = $data['sub_openid'] ?? '';
            $sub_appid = $data['sub_appid'] ?? '';
            $is_minipg = $data['is_minipg'] ?? '';
            $private_rsa_key = $data['private_rsa_key'];
            $public_rsa_key = $data['public_rsa_key'];

            $req_data = [
                'service' => 'pay.weixin.jspay', //接口类型
                'sign_type' => 'RSA_1_256', //签名类型，取值RSA_1_256或RSA_1_1
                'mch_id' => $mch_id, //商户号，由平台分配
                'is_raw' => '1', //原生JS,值为1
                'out_trade_no' => $out_trade_no, //商户系统内部的订单号,5到32个字符、只能包含字母数字或者下划线，区分大小写，每次下单请求确保在商户系统唯一
                'body' => $body, //商品描述
                'sub_openid' => $sub_openid, //用户openid,微信用户关注商家公众号的openid（注：使用测试商户号7551000001不能传sub_appid和sub_openid这两个参数；切换正式的商户号时则必须传,值分别为微信网页授权时使用的公众号appid和获取到的用户openid。同时在切换使用正式商户号前，必须登录银行提供的商户平台（或者渠道平台）配置好特约商户APPID和支付授权目录）
                'sub_appid' => $sub_appid, //公众账号或小程序ID,当发起公众号支付时，值是微信公众平台基本配置中的AppID(应用ID)；当发起小程序支付时，值是对应小程序的AppID
                'total_fee' => $total_amount*100, //总金额，以分为单位，不允许包含任何字、符号
                'mch_create_ip' => get_client_ip(), //上传商户真实的发起交易的终端出网IP
                'notify_url' => url('/api/hwcpay/pay_notify_url'), //接收平台通知的URL，需给绝对路径，255字符内格式如:http://wap.tenpay.com/tenpay.asp，确保平台能通过互联网访问该地址
                'nonce_str' => $this->nonceStr() //随机字符串，不长于 32 位
            ];
            if ($is_minipg) $req_data['is_minipg'] = $is_minipg; //是否小程序支付,否,String(1)值为1，表示小程序支付；不传或值不为1，表示公众账号内支付
            $res = $this->call($req_data, $private_rsa_key, $public_rsa_key); //1-成功;2-失败
//            Log::info('汇旺财-公众号&小程序支付-结果');
//            Log::info($res);
            //业务成功
            if ($res['status'] == 1) {
                return [
                    'status' => 1,
                    'message' => '交易成功',
                    'data' => $res['data']
                ];
            } else {
                return [
                    'status' => 2,
                    'message' => $res['message'],
                    'data' => $res['data']
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => 0,
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine(),
            ];
        }
    }


    /**
     * 查询订单 0-系统错误 1-成功 2-失败 3-转入退款 4-未支付 5-已关闭 6-已冲正 7-已撤销 8-用户支付中
     *
     * 调用订单查询接口建议：查询6次每隔5秒查询一次（具体的查询次数和时间也可自定义，建议查询时间不低于30秒）6次查询完成，接口仍未返回成功标识(即查询接口返回的trade_state不是SUCCESS)则调用撤销接口
     * @param $data
     * @return array
     */
    public function order_query($data)
    {
        try {
            $mch_id = $data['mch_id'];
            $out_trade_no = $data['out_trade_no'];
            $private_rsa_key = $data['private_rsa_key'];
            $public_rsa_key = $data['public_rsa_key'];

            $req_data = [
                'service' => 'unified.trade.query', //接口类型
                'sign_type' => 'RSA_1_256', //签名类型，取值RSA_1_256或RSA_1_1
                'mch_id' => $mch_id, //商户号，由平台分配
                'out_trade_no' => $out_trade_no, //商户系统内部的订单号,5到32个字符、只能包含字母数字或者下划线，区分大小写，每次下单请求确保在商户系统唯一
                'nonce_str' => $this->nonceStr() //随机字符串，不长于 32 位
            ];
            $res = $this->call($req_data, $private_rsa_key, $public_rsa_key); //1-成功;2-失败
//            Log::info('汇旺财-查询订单-结果');
//            Log::info($res);
            //业务成功
            if ($res['status'] == 1) {
                //支付成功
                if ($res['data']['trade_state'] == 'SUCCESS') {
                    return [
                        'status' => 1,
                        'message' => '支付成功',
                        'data' => $res['data']
                    ];
                } //转入退款
                elseif($res['data']['trade_state'] == 'REFUND') {
                    return [
                        'status' => 3,
                        'message' => $res['message'] ?? '转入退款',
                        'data' => $res['data']
                    ];
                } //未支付
                elseif($res['data']['trade_state'] == 'NOTPAY') {
                    return [
                        'status' => 4,
                        'message' => $res['message'] ?? '未支付',
                        'data' => $res['data']
                    ];
                } //已关闭
                elseif($res['data']['trade_state'] == 'CLOSED') {
                    return [
                        'status' => 5,
                        'message' => $res['message'] ?? '已关闭',
                        'data' => $res['data']
                    ];
                } //已冲正
                elseif($res['data']['trade_state'] == 'REVERSE') {
                    return [
                        'status' => 6,
                        'message' => $res['message'] ?? '已冲正',
                        'data' => $res['data']
                    ];
                } //已撤销
                elseif($res['data']['trade_state'] == 'REVOKED') {
                    return [
                        'status' => 7,
                        'message' => $res['message'] ?? '已撤销',
                        'data' => $res['data']
                    ];
                } //用户支付中
                elseif($res['data']['trade_state'] == 'USERPAYING') {
                    return [
                        'status' => 8,
                        'message' => $res['message'] ?? '用户支付中',
                        'data' => $res['data']
                    ];
                } //支付失败(其他原因，如银行返回失败)
                elseif($res['data']['trade_state'] == 'PAYERROR') {
                    return [
                        'status' => 2,
                        'message' => $res['message'] ?? '支付失败(其他原因，如银行返回失败)',
                        'data' => $res['data']
                    ];
                } else {
                    return [
                        'status' => 2,
                        'message' => $res['data']['trade_state_desc'] ?? '微商银行查询订单结果未知',
                        'data' => $res['data']
                    ];
                }
            } else {
                return [
                    'status' => 2,
                    'message' => $res['message'] ?? '微商银行订单查询错误',
                    'data' => $res['data']
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => 0,
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine(),
            ];
        }
    }


    /**
     * 撤销订单(仅付款码支付) 0-系统错误 1-成功 2-失败
     *
     * 当支付返回失败，或收银系统超时需要取消交易，可以调用该接口。接口逻辑：支付失败的关单，支付成功的撤销支付（资金会退还给用户）
     * 注意：5分钟内的成功订单仍可以撤销，其他正常支付的单如需实现相同功能请调用退款接口
     * 调用支付接口后请勿立即调用撤销订单接口，建议支付后至少15s后再调用撤销订单接口
     * @param $data
     * @return array
     */
    public function reverse($data)
    {
        try {
            $mch_id = $data['mch_id'];
            $out_trade_no = $data['out_trade_no'];
            $private_rsa_key = $data['private_rsa_key'];
            $public_rsa_key = $data['public_rsa_key'];

            $req_data = [
                'service' => 'unified.micropay.reverse', //接口类型
                'sign_type' => 'RSA_1_256', //签名类型，取值RSA_1_256或RSA_1_1
                'mch_id' => $mch_id, //商户号，由平台分配
                'out_trade_no' => $out_trade_no, //商户系统内部的订单号,5到32个字符、只能包含字母数字或者下划线，区分大小写，每次下单请求确保在商户系统唯一
                'nonce_str' => $this->nonceStr() //随机字符串，不长于 32 位
            ];
            $res = $this->call($req_data, $private_rsa_key, $public_rsa_key); //1-成功;2-失败
            Log::info('汇旺财-撤销订单-结果');
            Log::info($res);
            //业务成功
            if ($res['status'] == 1) {
                if (isset($res['data']['retry_flag']) && $res['data']['retry_flag'] == 'Y') { //N表示不用再重试，Y表示需要重试
                    return [
                        'status' => 2,
                        'message' => $res['data']['err_msg'] ?? '撤销结果未知，请重试',
                        'data' => $res['data']
                    ];
                }

                return [
                    'status' => 1,
                    'message' => $res['data']['err_msg'] ?? '撤销成功',
                    'data' => $res['data']
                ];
            } else {
                return [
                    'status' => 2,
                    'message' => $res['message'] ?? '订单撤销失败',
                    'data' => $res['data']
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => 0,
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine(),
            ];
        }
    }


    /**
     * 申请退款 0-系统错误 1-成功 2-退款失败 3-退款处理中 4-退款到银行发现用户的卡作废或者冻结了,导致原路退款银行卡失败
     *
     * 1.在平台系统中，只要退款累计金额不超过交易单支付总额，一笔交易单可以多次退款，退款申请单号（退款接口中有此参数）唯一确定一次退款，而不是交易单号确定一次退款。退款申请单号由商户生成，所以商户一定要保证退款申请单的唯一性。商家在退款过程中要特别注意，只有在能确定退款失败的情况下，才能重新发起另一笔退款。一笔退款失败后重新提交，请不要更换退款单号，请使用原商户退款单号
     * 2.5分钟正常的申请退款请求次数不超过300次
     * 3.错误或无效请求频率限制：6qps，即每秒钟异常或错误的退款申请请求不超过6次
     * 4.每个支付订单的部分退款次数不能超过50次
     * @param $data
     * @return array
     */
    public function refund($data)
    {
        try {
            $mch_id = $data['mch_id'];
            $out_trade_no = $data['out_trade_no'];
            $total_fee = $data['total_amount'];
            $refund_fee = $data['refund_fee'];
            $merchant_num = $data['merchant_num']; //商户号
            $private_rsa_key = $data['private_rsa_key'];
            $public_rsa_key = $data['public_rsa_key'];
            $out_refund_no = $out_trade_no;

            $req_data = [
                'service' => 'unified.trade.refund', //接口类型
                'sign_type' => 'RSA_1_256', //签名类型，取值RSA_1_256或RSA_1_1
                'mch_id' => $mch_id, //门店编号，由平台分配
                'out_trade_no' => $out_trade_no, //商户系统内部的订单号
                'out_refund_no' => $out_trade_no, //商户退款单号，32个字符内、可包含字母,确保在商户系统唯一。同个退款单号多次请求，平台当一个单处理，只会退一次款。如果出现退款不成功，请采用原退款单号重新发起，避免出现重复退款
                'total_fee' => $total_fee*100, //订单总金额，单位为分
                'refund_fee' => $refund_fee*100, //退款总金额,单位为分,可以做部分退款
                'op_user_id' => $merchant_num, //操作员帐号,默认为商户号
                'nonce_str' => $this->nonceStr() //随机字符串，不长于 32 位
            ];
            $res = $this->call($req_data, $private_rsa_key, $public_rsa_key); //1-成功;2-失败
//            Log::info('汇旺财-申请退款-结果');
//            Log::info($res);
            //业务成功
            if ($res['status'] == 1) {
                //此处表示退款申请接收成功，实际的退款结果根据退款查询接口查询
                $return_query_data = [
                    'mch_id' => $mch_id,
                    'out_trade_no' => $out_trade_no,
                    'private_rsa_key' => $private_rsa_key,
                    'public_rsa_key' => $public_rsa_key
                ];
                $return_query_res = $this->refund_query($return_query_data); //0-系统错误 1-成功 2-退款失败 3-退款处理中 4-退款到银行发现用户的卡作废或者冻结了,导致原路退款银行卡失败
                if ($return_query_res['status'] == 1) {
                    return [
                        'status' => 1,
                        'message' => $return_query_res['message'] ?? '退款成功',
                        'data' => $return_query_res['data']
                    ];
                } elseif($return_query_res['status'] == 2) {
                    return [
                        'status' => 2,
                        'message' => $return_query_res['message'] ?? '退款失败',
                        'data' => $return_query_res['data']
                    ];
                } elseif($return_query_res['status'] == 3) {
                    return [
                        'status' => 3,
                        'message' => $return_query_res['message'] ?? '退款处理中',
                        'data' => $return_query_res['data']
                    ];
                } elseif($return_query_res['status'] == 4) {
                    return [
                        'status' => 4,
                        'message' => $return_query_res['message'] ?? '退款到银行发现用户的卡作废或者冻结了,导致原路退款银行卡失败',
                        'data' => $return_query_res['data']
                    ];
                } else {
                    return [
                        'status' => 2,
                        'message' => $return_query_res['message'] ?? '退款失败,原因未知',
                        'data' => $return_query_res['data']
                    ];
                }

            } else {
                return [
                    'status' => 2,
                    'message' => $res['message'] ?? '退款失败',
                    'data' => $res['data']
                ];
            }

        } catch (\Exception $ex) {
            return [
                'status' => 0,
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine(),
            ];
        }
    }


    //退款查询 0-系统错误 1-成功 2-退款失败 3-退款处理中 4-退款到银行发现用户的卡作废或者冻结了,导致原路退款银行卡失败
    public function refund_query($data)
    {
        try {
            $mch_id = $data['mch_id'];
            $out_trade_no = $data['out_trade_no'];
            $private_rsa_key = $data['private_rsa_key'];
            $public_rsa_key = $data['public_rsa_key'];

            $req_data = [
                'service' => 'unified.trade.refundquery', //接口类型
                'sign_type' => 'RSA_1_256', //签名类型，取值RSA_1_256或RSA_1_1
                'mch_id' => $mch_id, //商户号，由平台分配
                'out_trade_no' => $out_trade_no, //商户系统内部的订单号
                'out_refund_no' => $out_trade_no, //如果是支付宝，refund_id、out_refund_no必填其中一个
                'nonce_str' => $this->nonceStr() //随机字符串，不长于 32 位
            ];
            $res = $this->call($req_data, $private_rsa_key, $public_rsa_key); //1-成功;2-失败
//            Log::info('汇旺财-退款查询-结果');
//            Log::info($res);
            //业务成功
            if ($res['status'] == 1) {
                $count = (int)$res['data']['refund_count']-1;
                $refund_status = $res['data']['refund_status_'.$count];
                $refund_status_info = (isset($res['data']['refund_status_info_'.$count]) && !empty($res['data']['refund_status_info_'.$count])) ? $res['data']['refund_status_info_'.$count]: '';
                //退款成功
                if ($refund_status == 'SUCCESS') {
                    return [
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $res['data']
                    ];
                } //退款失败
                elseif ($refund_status == 'FAIL') {
                    return [
                        'status' => 2,
                        'message' => $refund_status_info ? $refund_status_info: '退款失败',
                        'data' => $res['data']
                    ];
                } //退款处理中
                elseif ($refund_status == 'PROCESSING') {
                    return [
                        'status' => 3,
                        'message' => $refund_status_info ? $refund_status_info : '退款处理中',
                        'data' => $res['data']
                    ];
                } //转入代发，退款到银行发现用户的卡作废或者冻结了，导致原路退款银行卡失败，资金回流到商户的现金帐号，需要商户人工干预，通过线下或者平台转账的方式进行退款
                elseif ($refund_status == 'CHANGE') {
                    return [
                        'status' => 4,
                        'message' => $refund_status_info ? $refund_status_info : '转入代发，退款到银行发现用户的卡作废或者冻结了，导致原路退款银行卡失败，资金回流到商户的现金帐号，需要商户人工干预，通过线下或者平台转账的方式进行退款',
                        'data' => $res['data']
                    ];
                }
                else {
                    return [
                        'status' => 2,
                        'message' => $refund_status_info ? $refund_status_info: '退款结果未知',
                        'data' => $res['data']
                    ];
                }
            } else {
                return [
                    'status' => 2,
                    'message' => $res['message'] ?? '退款查询错误',
                    'data' => $res['data']
                ];
            }

        } catch (\Exception $ex) {
            return [
                'status' => 0,
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine(),
            ];
        }
    }


    /**
     * 关闭订单(扫码支付、公众号&小程序支付)
     *
     * 商户订单支付失败需要生成新单号重新发起支付，要对原订单号调用关单，避免重复支付；系统下单后，用户支付超时，系统退出不再受理，避免用户继续，请调用关单接口
     * 注：关单接口仅限于微信支付宝使用，银联不支持关单，且微信支付宝必须是扫码后才支持关单，若用户没有扫码，也无法支持关单
     * @param $data
     * @return array
     */
    public function close($data)
    {
        try {
            $mch_id = $data['mch_id'];
            $out_trade_no = $data['out_trade_no'];
            $private_rsa_key = $data['private_rsa_key'];
            $public_rsa_key = $data['public_rsa_key'];

            $req_data = [
                'service' => 'unified.trade.close', //接口类型
                'sign_type' => 'RSA_1_256', //签名类型，取值RSA_1_256或RSA_1_1
                'mch_id' => $mch_id, //商户号，由平台分配
                'out_trade_no' => $out_trade_no, //商户系统内部的订单号
                'nonce_str' => $this->nonceStr() //随机字符串，不长于 32 位
            ];
            $res = $this->call($req_data, $private_rsa_key, $public_rsa_key); //1-成功;2-失败
            Log::info('汇旺财-关闭订单-结果');
            Log::info($res);
            //业务成功
            if ($res['status'] == 1) {
                return [
                    'status' => 1,
                    'message' => '关闭订单成功',
                    'data' => $res['data']
                ];
            } else {
                return [
                    'status' => 2,
                    'message' => $res['message'] ?? '关闭订单错误',
                    'data' => $res['data']
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => 0,
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine(),
            ];
        }
    }


    /**
     * 微信刷脸SDK调用凭证 0-系统错误 1-成功 2-失败
     * @param $data
     * @return array
     */
    public function weChatFacePayAuth($data)
    {
        try {
            $mch_id = $data['mch_id'];
            $device_id = $data['device_id'];
            $out_trade_no = $data['out_trade_no'];
            $store_id = $data['store_id'];
            $store_name = $data['store_name'];
            $rawdata = $data['rawdata'];
            $private_rsa_key = $data['private_rsa_key'];
            $public_rsa_key = $data['public_rsa_key'];

            $req_data = [
                'service' => 'pay.weixin.facepay.authinfo', //接口类型
                'sign_type' => 'RSA_1_256', //签名类型，取值RSA_1_256或RSA_1_1
                'mch_id' => $mch_id, //门店号，由平台分配
                'device_id' => $device_id, //终端设备编号，由商户自定义
                'out_trade_no' => $out_trade_no, //商户系统内部的订单号
                'store_id' => $mch_id, //门店编号，与mch_id一致
                'store_name' => $store_name, //门店名称，由商户定义（可用于展示）
                'rawdata' => $rawdata, //初始化数据，由微信人脸SDK接口返回
                'nonce_str' => $this->nonceStr() //随机字符串，不长于 32 位
            ];
            $res = $this->call($req_data, $private_rsa_key, $public_rsa_key); //1-成功;2-失败
            Log::info('汇旺财-微信刷脸凭证-结果');
            Log::info($res);
            //业务成功
            if ($res['status'] == 1) {
                return [
                    'status' => 1,
                    'message' => '成功',
                    'data' => $res['data']
                ];
            } else {
                return [
                    'status' => 2,
                    'message' => $res['message'] ?? '微信刷脸凭证获取error',
                    'data' => $res['data']
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => 0,
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine(),
            ];
        }
    }


}
