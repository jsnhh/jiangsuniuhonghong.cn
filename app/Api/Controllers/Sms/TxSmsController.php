<?php


namespace App\Api\Controllers\Sms;


use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Log;
use TencentCloud\Common\Credential;
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Common\Profile\HttpProfile;
use TencentCloud\Sms\V20210111\SmsClient;
use TencentCloud\Sms\V20210111\Models\SendSmsRequest;

class TxSmsController extends Controller
{
    public $pwd = '8bd5d67f7107b8eeccbfc80ca9834d6c';
    public $SmsSdkAppId = '1400837655';
    public $templateID = '1858928';
    public $post_url = 'https://sms.tencentcloudapi.com/';
    public $SecretId = 'AKIDzqW0KRQFdqumQr7El6TfeR5Zu7Tn12qP';
    public $SecretKey = 'gBTXR9XkjEh0FKbeS3VlncIFDNaigXJw';

    public function sendSms($mobile, $paramSrc)
    {
        try {
            // 实例化一个认证对象，入参需要传入腾讯云账户 SecretId 和 SecretKey，此处还需注意密钥对的保密
            // 代码泄露可能会导致 SecretId 和 SecretKey 泄露，并威胁账号下所有资源的安全性。以下代码示例仅供参考，建议采用更安全的方式来使用密钥，请参见：https://cloud.tencent.com/document/product/1278/85305
            // 密钥可前往官网控制台 https://console.cloud.tencent.com/cam/capi 进行获取
            $cred = new Credential($this->SecretId, $this->SecretKey);
            // 实例化一个http选项，可选的，没有特殊需求可以跳过
            $httpProfile = new HttpProfile();
            $httpProfile->setEndpoint("sms.tencentcloudapi.com");

            // 实例化一个client选项，可选的，没有特殊需求可以跳过
            $clientProfile = new ClientProfile();
            $clientProfile->setHttpProfile($httpProfile);
            // 实例化要请求产品的client对象,clientProfile是可选的
            $client = new SmsClient($cred, "ap-beijing", $clientProfile);

            // 实例化一个请求对象,每个接口都会对应一个request对象
            $req = new SendSmsRequest();
            $PhoneNumberSet = [
                '+86' . $mobile
            ];
            $TemplateParamSet = [
                $paramSrc
            ];
            $params = array(
                "PhoneNumberSet" => $PhoneNumberSet,
                "SmsSdkAppId" => $this->SmsSdkAppId,
                "TemplateId" => $this->templateID,
                "TemplateParamSet" => $TemplateParamSet,
            );
            $req->fromJsonString(json_encode($params));

            // 返回的resp是一个SendSmsResponse的实例，与请求对象对应
            $resp = $client->SendSms($req);
            Log::info($resp->toJsonString());
            // 输出json格式的字符串回包
        } catch (\Exception $ex) {
            Log::info('腾讯云-短信发送-错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getLine());
        }
    }
}