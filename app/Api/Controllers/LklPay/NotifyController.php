<?php

namespace App\Api\Controllers\LklPay;

use App\Common\PaySuccessAction;
use App\Models\LklStore;
use App\Models\Order;
use App\Models\StorePayWay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Symfony\Component\HttpFoundation\Response;

class NotifyController extends BaseController
{
    //商户入网审核推送
    public function merchant_examine_push(Request $request)
    {
        try {
            $data = $request->all();
            Log::info('拉卡拉-商户入网推送' . json_encode($data));
            $res_data = $this->publicKeyDecrypt($data['data']);
            $res_data = json_decode($res_data, true);
            Log::info(json_encode($res_data));
            $lklStore = LklStore::where('merchant_no', $res_data['customerNo'])->select('merchant_no', 'store_id', 'audit_status')->first();
            if ($lklStore && $lklStore->audit_status != '04') {
                if ($res_data['status'] == 'SUCCESS' || $res_data['status'] == 'REVIEW_PASS') {
                    $updata_lkl = [
                        'audit_status' => '04',
                        'audit_msg' => '审核通过',
                        'term_no' => $res_data['termNos'],
                        'customer_no' => $res_data['externalCustomerNo'],
                    ];
                    $status = 1;
                    $status_msg = '审核通过';
                } else {
                    $updata_lkl = [
                        'audit_status' => '03',
                        'audit_msg' => $res_data['remark'],
                    ];
                    $status = 3;
                    $status_msg = $res_data['remark'];
                }
                LklStore::where('merchant_no', $res_data['customerNo'])->update($updata_lkl);
                StorePayWay::where('store_id', $lklStore->store_id)->where('company', 'lklpay')->update(['status' => $status, 'status_desc' => $status_msg]);
                return response()->json(Response::HTTP_OK);
            }
            //Log::info($res_data);


        } catch (\Exception $ex) {
            Log::info('拉卡拉商户入网推送-报错');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

    }

    //商户入网审核推送
    public function update_merchant_push(Request $request)
    {
        try {
            $data = $request->all();
            Log::info('拉卡拉-商户改件推送' . json_encode($data));
            $res_data = $this->publicKeyDecrypt($data['data']);
            $res_data = json_decode($res_data, true);
            Log::info(json_encode($res_data));
            $lklStore = LklStore::where('merchant_no', $res_data['customerNo'])->select('merchant_no', 'store_id')->first();
            if ($lklStore) {
                if ($res_data['reviewPass'] == 'PASS') {
                    $updata_lkl = [
                        'audit_status' => '04',
                        'audit_msg' => '审核通过',
                        'term_no' => $res_data['termNo'],
                        'customer_no' => $res_data['externalCustomerNo'],
                    ];
                    $status = 1;
                    $status_msg = '审核通过';
                } else if ($res_data['reviewPass'] == 'UNPASS') {
                    $updata_lkl = [
                        'audit_status' => '03',
                        'audit_msg' => $res_data['reviewResult'],
                    ];
                    $status = 3;
                    $status_msg = $res_data['reviewResult'];
                } else {
                    $updata_lkl = [
                        'audit_status' => '02',
                        'audit_msg' => '待审核',
                    ];
                    $status = 2;
                    $status_msg = '待审核';
                }
                LklStore::where('merchant_no', $res_data['customerNo'])->update($updata_lkl);
                StorePayWay::where('store_id', $lklStore->store_id)->where('company', 'lklpay')->update(['status' => $status, 'status_desc' => $status_msg]);
                return response()->json(Response::HTTP_OK);
            }
            //return response()->json(['status' => 200]);
        } catch (\Exception $ex) {
            Log::info('拉卡拉商户改件推送-报错');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

    }

    public function pay_notify_url(Request $request)
    {
        try {
            $data = $request->all();
            Log::info('拉卡拉-支付回调');
            Log::info($data);

            if (isset($data) && !empty($data)) {
                if (isset($data['out_trade_no'])) {
                    $out_trade_no = $data['out_trade_no']; //商户订单号(原交易流水)
                    $trade_no = $data['trade_no']; //系统订单号
                    $day = date('Ymd', time());
                    $table = 'orders_' . $day;
                    if (Schema::hasTable($table)) {
                        $order = DB::table($table)->where('out_trade_no', $out_trade_no)->first();
                    } else {
                        $order = Order::where('out_trade_no', $out_trade_no)->first();
                    }

                    //订单存在
                    if ($order) {
                        //系统订单未成功
                        if ($order->pay_status == '2') {
                            $pay_time = (isset($data['trade_time']) && !empty($data['trade_time'])) ? date('Y-m-d H:i:m', strtotime($data['trade_time'])) : date('Y-m-d H:i:m'); //支付完成时间，格式为 yyyyMMddhhmmss，如 2009年12月27日9点10分10秒表示为 20091227091010
                            $buyer_pay_amount = isset($data['total_amount']) ? ($data['total_amount'] / 100) : ''; //否,实付金额,单位分
                            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                            $buyer_id = '';
                            $in_data = [
                                'status' => '1',
                                'pay_status' => 1,
                                'pay_status_desc' => '支付成功',
                                'trade_no' => $trade_no,
                                'pay_time' => $pay_time,
                                'buyer_id' => $buyer_id,
                                'buyer_pay_amount' => $buyer_pay_amount,
                                'receipt_amount' => $buyer_pay_amount,
                            ];

                            $this->update_day_order($in_data, $out_trade_no);

                            if (strpos($out_trade_no, 'scan')) {

                            } else {
                                //支付成功后的动作
                                $data = [
                                    'ways_type' => $order->ways_type,
                                    'company' => $order->company,
                                    'ways_type_desc' => $order->ways_type_desc,
                                    'source_type' => '34000', //返佣来源
                                    'source_desc' => '拉卡拉支付', //返佣来源说明
                                    'total_amount' => $order->total_amount,
                                    'out_trade_no' => $order->out_trade_no,
                                    'other_no' => $order->other_no,
                                    'rate' => $order->rate,
                                    'fee_amount' => $order->fee_amount,
                                    'merchant_id' => $order->merchant_id,
                                    'store_id' => $order->store_id,
                                    'user_id' => $order->user_id,
                                    'config_id' => $order->config_id,
                                    'store_name' => $order->store_name,
                                    'ways_source' => $order->ways_source,
                                    'pay_time' => $pay_time,
                                    'device_id' => $order->device_id,
                                ];
                                if (Cache::has('console_out_trade_no_' . $out_trade_no)) {
                                    Log::info('--VibillOrderSuccess--lkl--检查是否操作---Cache.  ---console_out_trade_no_:' . $out_trade_no);
                                    return;
                                }
                                Cache::add('console_out_trade_no_' . $out_trade_no, '1', 1);
                                PaySuccessAction::action($data);
                            }


                            // $notifyData = [
                            //     'userId' => $order->user_id,
                            //     'outTradeNo' => $data['data']['oriOrgTrace'], //商户订单号(原交易流水)
                            //     'tradeNo' => $data['data']['outTrace'], //系统订单号
                            //     'payTime' => date('Y-m-d H:i:s', strtotime($data['data']['timeEnd'])),//支付成功时间
                            //     'payerAmt' => isset($data['data']['payerAmt']) ? ($data['data']['payerAmt'] / 100) : '', //否,实付金额,单位分
                            //     'tradeCode' => $data['data']['tradeCode'],//支付渠道
                            //     'payerId' => isset($data['data']['payerId']) ? ($data['data']['payerId']) : ''//用户标识  支付宝 微信返回
                            // ];
                            // $this->userNotify($notifyData);//代理支付回调
                        }
                    }
                }

                return json_encode(['code' => "SUCCESS", 'message' => '执行成功']);
            } else {
                Log::info('易生-支付回调-结果');
                Log::info($data);
            }

        } catch (\Exception $ex) {
            Log::info('易生支付回调-异步报错');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }
    }


}
