<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/07/27
 * Time: 下午5:13
 */

namespace App\Api\Controllers\LklPay;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PayController extends BaseController
{
    public function scan_pay($data)
    {
        try {
            $merchant_no = $data['merchant_no'] ?? '';
            $term_no = $data['term_no'] ?? '';
            $out_trade_no = $data['out_trade_no'] ?? '';
            $total_amount = $data['total_amount'] ?? 0;
            $total_amount = $total_amount ? $total_amount * 100 : 0;
            $location_info = [
                'request_ip' => '1.116.144.7'
            ];
            $auth_code = $data['auth_code'] ?? '';
            $lkl_data = [
                'merchant_no' => $merchant_no,
                'term_no' => $term_no,
                'out_trade_no' => $out_trade_no,
                'total_amount' => $total_amount . "",
                'location_info' => $location_info,
                'auth_code' => $auth_code
            ];
            Log::info('拉卡拉-被扫入参');
            Log::info($lkl_data);
            $result = $this->pay_post($this->scan_url, $lkl_data);
            Log::info('拉卡拉-被扫-返回');
            Log::info($result);
            if (!$result) {
                return [
                    'status' => '-1',
                    'message' => '请求失败',
                ];
            }
            $result = json_decode($result, true);
            if ($result['code'] == 'BBS00000') {
                return [
                    'status' => '1',
                    'message' => '支付成功',
                    'data' => $result['resp_data']
                ];
            }
            if ($result['code'] == 'BBS10000') {
                return [
                    'status' => '2',
                    'message' => '等待支付',
                    'data' => $result['resp_data']
                ];
            }
            if ($result['code'] == 'BBS11105') {
                return [
                    'status' => '2',
                    'message' => '等待支付',
                    'data' => $result['resp_data']
                ];
            }
            return [
                'status' => '0',
                'message' => $result['msg'],
            ];

        } catch (\Exception $ex) {
            Log::info('拉卡拉-被扫支付-错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    public function qr_submit($data)
    {
        try {
            $merchant_no = $data['merchant_no'] ?? '';
            $term_no = $data['term_no'] ?? '';
            $out_trade_no = $data['out_trade_no'] ?? '';
            $total_amount = $data['total_amount'] ?? 0;
            $total_amount = $total_amount ? $total_amount * 100 : 0;
            $pay_type = $data['pay_type'] ?? '';
            $code = $data['code'] ?? '';
            $notify_url = $data['notify_url'] ?? '';
            $trans_type = '51';
            $account_type = '';
            if ($pay_type == 'ALIPAY') {
                $account_type = 'ALIPAY';
            }
            if ($pay_type == 'WECHAT') {
                $account_type = 'WECHAT';
            }
            if ($pay_type == 'UQRCODEPAY') {
                $account_type = 'UQRCODEPAY';
            }
            $location_info = [
                'request_ip' => '1.116.144.7'
            ];
            $acc_busi_fields = [
                'user_id' => $code
            ];
            $lkl_data = [
                'merchant_no' => $merchant_no,
                'term_no' => $term_no,
                'out_trade_no' => $out_trade_no,
                'total_amount' => $total_amount . "",
                'location_info' => $location_info,
                'account_type' => $account_type,
                'trans_type' => $trans_type,
                'notify_url' => $notify_url,
                'acc_busi_fields' => $acc_busi_fields
            ];
            Log::info('拉卡拉-主扫-入参');
            Log::info($lkl_data);
            $result = $this->pay_post($this->qr_url, $lkl_data);
            Log::info('拉卡拉-主扫-返回');
            Log::info($result);
            if (!$result) {
                return [
                    'status' => '-1',
                    'message' => '请求失败',
                ];
            }
            $result = json_decode($result, true);
            if ($result['code'] == 'BBS00000') {
                return [
                    'status' => '1',
                    'message' => '支付成功',
                    'data' => $result['resp_data']
                ];
            }
            if ($result['code'] == 'BBS10000') {
                return [
                    'status' => '2',
                    'message' => '等待支付',
                    'data' => $result['resp_data']
                ];
            }
            if ($result['code'] == 'BBS11105') {
                return [
                    'status' => '2',
                    'message' => '等待支付',
                    'data' => $result['resp_data']
                ];
            }
            return [
                'status' => '0',
                'message' => $result['msg'],
            ];
        } catch (\Exception $ex) {
            Log::info('拉卡拉-主扫-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine(),
            ];
        }
    }

    public function order_query($data)
    {
        try {
            $merchant_no = $data['merchant_no'] ?? '';
            $term_no = $data['term_no'] ?? '';
            $out_trade_no = $data['out_trade_no'] ?? '';

            $lkl_data = [
                'merchant_no' => $merchant_no,
                'term_no' => $term_no,
                'out_trade_no' => $out_trade_no //商户订单号
            ];
            Log::info('拉卡拉-交易查询-入参');
            Log::info($lkl_data);
            $result = $this->pay_post($this->query_url, $lkl_data);
            Log::info('拉卡拉-交易查询-返回');
            Log::info($result);
            if (!$result) {
                return [
                    'status' => '-1',
                    'message' => '请求失败',
                ];
            }
            $result = json_decode($result, true);
            if ($result['code'] == 'BBS00000') {
                if($result['resp_data']['trade_state']=='SUCCESS'){
                    return [
                        'status' => '1',
                        'message' => '支付成功',
                        'data' => $result['resp_data']
                    ];
                }
                if($result['resp_data']['trade_state']=='DEAL'){
                    return [
                        'status' => '2',
                        'message' => '等待支付',
                        'data' => $result['resp_data']
                    ];
                }
                if($result['resp_data']['trade_state']=='FAIL'){
                    return [
                        'status' => '0',
                        'message' => '交易失败',
                    ];
                }

            }

            return [
                'status' => '0',
                'message' => $result['msg'],
            ];
        } catch (\Exception $ex) {
            Log::info('拉卡拉-交易查询错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    public function refund($data)
    {
        try {
            $merchant_no = $data['merchant_no'] ?? '';
            $term_no = $data['term_no'] ?? '';
            $origin_out_trade_no = $data['origin_out_trade_no'] ?? '';
            $refund_amount = $data['refund_amount'] ?? '';
            $refund_amount = $refund_amount * 100;
            $out_trade_no = $data['OutRefundNo'] ?? '';
            $location_info = [
                'request_ip' => '1.116.144.7'
            ];
            $lkl_data = [
                'merchant_no' => $merchant_no,
                'term_no' => $term_no,
                'origin_out_trade_no' => $origin_out_trade_no,
                'refund_amount' => $refund_amount,
                'out_trade_no' => $out_trade_no,
                'location_info' => $location_info
            ];
            Log::info('拉卡拉-退款-入参');
            Log::info($lkl_data);
            $res = $this->pay_post($this->refund_url, $lkl_data);
            Log::info('拉卡拉-退款-返回');
            Log::info($res);
            if (!$res) {
                return [
                    'status' => '-1',
                    'message' => '请求失败',
                ];
            }
            $result = json_decode($res, true);
            if ($result['code'] == 'BBS00000') {
                return [
                    'status' => '1',
                    'message' => '退款成功',
                    'data' => $result['resp_data'],
                ];
            }
            return [
                'status' => '0',
                'message' => $result['msg'],
            ];

        } catch (\Exception $ex) {
            Log::info('拉卡拉-退款-错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    public function refund_query($data)
    {
        try {
            $merchant_no = $data['merchant_no'];
            $term_no = $data['term_no'] ?? '';
            $out_refund_order_no = $data['refund_no'] ?? '';

            $lkl_data = [
                'merchant_no' => $merchant_no,
                'term_no' => $term_no,
                'out_refund_order_no' => $out_refund_order_no
            ];
            Log::info('拉卡拉-退款查询-入参');
            Log::info($lkl_data);
            $res = $this->pay_post($this->refund_query_url, $lkl_data);
            Log::info('拉卡拉-退款查询-返回');
            Log::info($res);
            if ($res) {
                return [
                    'status' => '-1',
                    'message' => '请求失败',
                ];
            }
            $result = json_decode($res, true);
            if ($result['code'] == 'BBS00000') {
                return [
                    'status' => '1',
                    'message' => '退款成功',
                    'data' => $result['resp_data'],
                ];
            }
            return [
                'status' => '0',
                'message' => $result['msg'],
            ];
        } catch (\Exception $ex) {
            Log::info('拉卡拉-退款查询-错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }


}
