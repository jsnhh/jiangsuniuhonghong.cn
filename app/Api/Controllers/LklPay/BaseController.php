<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/07/27
 * Time: 下午5:13
 */

namespace App\Api\Controllers\LklPay;

use App\Api\Controllers\BaseController as BBaseController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;

class BaseController extends BBaseController
{
    //入网参数
    public $userNo = '22251196';
    public $activityId = '13';
    public $getToken = 'https://tkapi.lakala.com/auth/oauth/token?';
    public $clientId = 'huisao';
    public $clientSecret = 'mDswo4fZylp1OR45';
    public $userName = '13793127833';
    public $password = 'Aa123456';
    public $lkl_url = 'https://htkactvi.lakala.com/registration/';
    public $lkl_up_url = 'https://tkapi.lakala.com/htkmerchants/channel/customer/update/';
    public $privateKey = 'MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBANiPrFFw0iHAZIEzntAttOe6EgU/C3XOtvKPz7c6qltPwGPhn6cPSCwpzfMPT7iSnONW515I+WdqXtKn86oucjlfQiY470NwoDGIYuGZrLl+DWXJj37kKabW2P911R9p9JXeIhMlLSPcYoaVq9j3OceoS508BJ9dNHkW6MhdYkDLAgMBAAECgYEAj73tdrEYCOcWzttgknWEGjtbMrhB9xQaQq5KBSifYrRw0tsziUOPeCDXXnnPRIesMwm/0MlHIelsvw5ToaXqkgi1ODqcsS+YfcyXOSG/VeH1XO5g9POrbnMy/IWaXRvaEcnjvLRNtLKcVaTeH1cz1mLAX6QoZ5RCDHQqJcOcumECQQD+SLFiMylfBx3pT51NwzdXHknh+1yAGW1MzWYiwh0zxf01/gzyyUaT0Nz5YfUdhIYaR/Wf7YjsNlAqUyER3pSXAkEA2gXPO/nTVCLlf+zu4wmkm3xzecyoiv8lR3cPzIxjv9Oaik5npzTBJqo8rsrprjvrnh+DJI15GDaI1mr8vFP37QJBAPWooMXwC13k8D7ATYYcvNALUPFi92BCMFfsb2nRAolME24f+75qOScAtxquSjG0ICIvttpEju01I4lgOiibUg0CQAhNsFbYbPQHYDY/D9oVw+8oDj3VTZX9oa3XGwLvVd04cepSkiAMIk8WrLj7gWrhXVfoW1ZJDROfABcqe/WwPQECQQCRecElS8+9k+rmVjRikdcP7tEJfZM2GDeHmB7VVIeTcTlzottY01zvO6mg0z8EfTb3ZtC76FQ0ctMS1sneAD0s';
    public $pubKey = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDYj6xRcNIhwGSBM57QLbTnuhIFPwt1zrbyj8+3OqpbT8Bj4Z+nD0gsKc3zD0+4kpzjVudeSPlnal7Sp/OqLnI5X0ImOO9DcKAxiGLhmay5fg1lyY9+5Cmm1tj/ddUfafSV3iITJS0j3GKGlavY9znHqEudPASfXTR5FujIXWJAywIDAQAB';
    public $lkl_mer_url='https://tkapi.lakala.com/htkmerchants/open/merchant/submer';
    //支付参数
    public $scan_url = 'https://s2.lakala.com/api/v3/labs/trans/micropay';
    public $qr_url = 'https://s2.lakala.com/api/v3/labs/trans/preorder';
    public $query_url = 'https://s2.lakala.com/api/v3/labs/query/tradequery';
    public $refund_url = 'https://s2.lakala.com/api/v3/labs/relation/refund';
    public $refund_query_url = 'https://test.wsmsd.cn/sit/api/v3/labs/query/idmrefundquery';
    public $appid = 'OP00000003';
    public $serial_no = '00dfba8194c41b84cf';
    public $smKey = 'dRzPaYd7z6vYn9sL/JTZ3A==';
    public $private_pay_url = '/cert/api_private_key.pem';
    public $schema = "LKLAPI-SHA256withRSA";

    //获取拉卡拉token
    public function get_token_post($url, $type = 'add')
    {
        if ($type == 'add') {
            $token = Cache::get('lklTokenAdd');
            $request = [
                'grant_type' => 'client_credentials',
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret
            ];
        } else {
            $token = Cache::get('lklTokenUpdate');
            $request = [
                'grant_type' => 'password',
                'username' => $this->userName,
                'password' => $this->password
            ];
        }
        if ($token) {
            return $token;
        }

        $basic = base64_encode($this->clientId . ':' . $this->clientSecret);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); //访问超时时间
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($request));
        if ($type == 'update') {
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Authorization: Basic ' . $basic,
            ]);
        }
        //本地忽略校验证书
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $tmpInfo = curl_exec($curl);
//        Log::info($tmpInfo);
        $data = json_decode($tmpInfo, true);
        $s = $data['expires_in'] / 60;
        if ($type == 'add') {//入网
            Cache::put('lklTokenAdd', $data['access_token'], $s);
        } else {//变更
            Cache::put('lklTokenUpdate', $data['access_token'], $s);
        }

        return $data['access_token'];
    }

    //获取拉卡拉门店类别、地址
    public function get_htkregistration_post($url, $token)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); //访问超时时间
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Authorization: bearer ' . $token,
        ]);
        //本地忽略校验证书
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $tmpInfo = curl_exec($curl);
//        Log::info($tmpInfo);
        return $tmpInfo;
    }

    //上传图片

    /**
     * array (
     * 'batchNo' => NULL,
     * 'status' => '00',
     * 'url' => 'merchant/null/20230829114927343003ID_CARD_FRONT.jpg',
     * 'showUrl' => 'https://lkl-zf-public-read-test2.oss-cn-shanghai.aliyuncs.com/pd/huituoke/merchant/null/20230829114927343003ID_CARD_FRONT.jpg?Expires=1693281568&OSSAccessKeyId=LTAI4FgiQ23LUrAw9N2aTjDy&Signature=Fa5toEIAHR1Yrvrd3IUGkG%2Fqus4%3D',
     * 'result' => NULL,
     * )
     */
    public function upload_post($url, $token, $data)
    {
        Log::info('拉卡拉上传图片-入参：');
        Log::info($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        // 禁用证书验证
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'charset=UTF-8',
            'Content-Type=multipart/form-data',
            'Authorization: bearer ' . $token,
        ]);

        $file_contents = curl_exec($ch);
        $info = json_decode($file_contents, JSON_UNESCAPED_UNICODE);
        Log::info('拉卡拉上传图片-原始res：');
        Log::info($info);
        curl_close($ch);
        return $info;
    }

    //进件
    public function post($url, $post_data = '', $token)
    {
        //Log::info($token);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        $post_data = stripslashes(json_encode($post_data, JSON_UNESCAPED_UNICODE));
        if ($post_data != '') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json; charset=UTF-8',
            'Authorization: bearer ' . $token
        ]);
        // 禁用证书验证
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        $file_contents = curl_exec($ch);
        curl_close($ch);
        return $file_contents;
    }

    public function getSign($data)
    {
        $private_content = file_get_contents(dirname(__FILE__) . $this->private_pay_url);
        if (!$private_key = openssl_get_privatekey($private_content)) {
            die('error');
            Log::info('error');
        }
        Log::info($data);
        $res = openssl_sign($data, $sign, $private_key, OPENSSL_ALGO_SHA256);
        openssl_free_key($private_key);
        if ($res) {
            return base64_encode($sign);
        } else {
            Log::info('加密失败');
        }
    }

    //支付
    public function pay_post($url, $post_data = '', $isen = false)
    {
        $req_time = date('YmdHis', time());
        $version = '3.0';
        $data = [
            'req_time' => $req_time,
            'version' => $version,
            'req_data' => $post_data
        ];
        $timeStamp = time();
        $nonceStr = $this->get_nonce_str();
        $body = stripslashes(json_encode($data, JSON_UNESCAPED_UNICODE));
        if ($isen) {
            $sm4 = new SM4();
            $body = $sm4->encrypt(base64_decode($this->smKey), $body);
        }

        $dataStr = $this->appid . "\n" . $this->serial_no . "\n" . $timeStamp . "\n" . $nonceStr . "\n" . $body . "\n";
        $signature = $this->getSign($dataStr);
        $Authorization = $this->schema . " appid=\"" . $this->appid . "\"," . "serial_no=\"" . $this->serial_no . "\"," . "timestamp=\""
            . $timeStamp . "\"," . "nonce_str=\"" . $nonceStr . "\"," . "signature=\"" . $signature . "\"";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        if ($data != '') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Authorization: ' . $Authorization
        ]);
        // 禁用证书验证
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        $file_contents = curl_exec($ch);
        curl_close($ch);
        return $file_contents;
    }

    /**
     *解密
     * @param $data
     * @return string
     */
    public function publicKeyDecrypt($data)
    {
        $pubKey = "-----BEGIN PUBLIC KEY-----\n" .
            wordwrap($this->pubKey, 64, "\n", true)
            . "\n-----END PUBLIC KEY-----";
        $crypto = '';
        foreach (str_split(base64_decode($data), 128) as $chunk) {
            openssl_public_decrypt($chunk, $decryptData, $pubKey);
            $crypto .= $decryptData;
        }
        return $crypto;
    }
    public function privateKeyDecrypt($data){
        $privateKey = "-----BEGIN RSA PRIVATE KEY-----\n" .
            wordwrap($this->privateKey, 64, "\n", true) .
            "\n-----END RSA PRIVATE KEY-----";
        $crypto = '';
        foreach (str_split(json_encode($data), 117) as $chunk) {
            openssl_private_encrypt($chunk, $encryptData, $privateKey);
            $crypto .= $encryptData;
        }
        $encrypted = base64_encode($crypto);
        return $encrypted;
    }

    public function images_get($img_url)
    {
        $img_url = explode('/', $img_url);
        $img_url = end($img_url);
        $img = public_path() . '/upload/images/' . $img_url;
        return $img;
    }

    /**
     * 生成12位随机字符串
     */
    public function get_nonce_str()
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $res = "";

        for ($i = 0; $i < 12; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars) - 1)];
        }

        return $res;
    }
}
