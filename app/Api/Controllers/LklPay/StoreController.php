<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/07/27
 * Time: 下午5:13
 */

namespace App\Api\Controllers\LklPay;


use App\Models\LklPayMcc;
use App\Models\LklStore;
use App\Models\ProvinceCity;
use App\Models\Store;
use App\Models\StoreBank;
use App\Models\StoreImg;
use App\Models\StorePayWay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StoreController extends BaseController
{
    public function lklPayMerchantReplenish(Request $request)
    {
        try {
            $type = $request->get('type', '1'); //2-查询数据
            $store_id = $request->get('storeId', ''); //门店id
            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                $this->status = -1;
                $this->message = '门店不存在或状态异常';
                return $this->format();
            }
            $lklStore = LklStore::where('store_id', $store_id)->select('*')->first();
            if ($type == '2') {
                $this->status = 1;
                $this->message = '返回数据成功';
                return $this->format($lklStore);
            }
            $email = $request->get('email', '');
            $mcc_type = $request->get('mcc_type', '');
            $mcc = $request->get('mcc', '');
            $mcc_name = $request->get('mcc_name', '');
            $settle_type = $request->get('settle_type', '');
            $agree_ment_url = $request->get('agree_ment_url', '');
            $others_url = $request->get('others_url', '');
            $letter_of_authorization_url = $request->get('letter_of_authorization_url', '');
            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');
            $province_name = $request->get('province_name', '');
            $city_name = $request->get('city_name', '');
            $area_name = $request->get('area_name', '');
            $settle_province_code = $request->get('settle_province_code', '');
            $settle_city_code = $request->get('settle_city_code', '');
            $settle_province_name = $request->get('city_code', '');
            $settle_city_name = $request->get('settle_province_name', '');
            $openning_bank_code = $request->get('openning_bank_code', '');
            $openning_bank_name = $request->get('openning_bank_name', '');
            $clearing_bank_code = $request->get('clearing_bank_code', '');
            $lkl_data = [
                'email' => $email,
                'mcc_type' => $mcc_type,
                'mcc' => $mcc,
                'mcc_name' => $mcc_name,
                'settle_type' => $settle_type,
                'agree_ment_url' => $agree_ment_url,
                'others_url' => $others_url,
                'letter_of_authorization_url' => $letter_of_authorization_url,
                'province_code' => $province_code,
                'city_code' => $city_code,
                'area_code' => $area_code,
                'province_name' => $province_name,
                'city_name' => $city_name,
                'area_name' => $area_name,
                'settle_province_code' => $settle_province_code,
                'settle_city_code' => $settle_city_code,
                'settle_province_name' => $settle_province_name,
                'settle_city_name' => $settle_city_name,
                'openning_bank_code' => $openning_bank_code,
                'openning_bank_name' => $openning_bank_name,
                'clearing_bank_code' => $clearing_bank_code,
            ];
            StoreImg::where('store_id', $store_id)->update(['store_auth_bank_img' => $letter_of_authorization_url]);
            $lklStore = LklStore::where('store_id', $store_id)->select('id')->first();
            if ($lklStore) {
                $result = LklStore::where('store_id', $store_id)->update($lkl_data);
                if (!$result) {
                    $this->status = 2;
                    $this->message = '更新失败';
                    return $this->format();
                }
            } else {
                $lkl_data['store_id'] = $store_id;
                $result = LklStore::create($lkl_data);
                if (!$result) {
                    $this->status = 2;
                    $this->message = '添加失败';
                    return $this->format();
                }
            }
            $this->message = '添加成功';
            $this->status = 1;
            return $this->format($result);

        } catch (\Exception $ex) {
            Log::info('拉卡拉补充材料--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    //商户入网
    public function addMerchant($StoreInfo, $StoreBank, $StoreImg)
    {
        try {
            $store_id = $StoreInfo->store_id;
            $storePayWay = StorePayWay::where('store_id', $store_id)
                ->where('company', 'lklpay')
                ->where('ways_source', 'weixin')
                ->first();

            if (!$storePayWay) {
                return json_encode([
                    'status' => 2,
                    'message' => "未设置费率"
                ]);
            }

            $rate = $storePayWay->rate;
            $lklStore = LklStore::where('store_id', $store_id)->select('*')->first();
            if ($lklStore->audit_status == '01' && $lklStore->merchant_no) {
                return json_encode([
                    'status' => 2,
                    'message' => "商户审核中，禁止提交"
                ]);
            }
            $merType = '';
            if ($StoreInfo->store_type == '1') { //个体工商户
                $merType = 'TP_MERCHANT';
            } else if ($StoreInfo->store_type == '2') { //企业
                $merType = 'TP_MERCHANT';
            } else if ($StoreInfo->store_type == '3') { //小微商户
                $merType = 'TP_PERSONAL';
            }
            if (!$lklStore) {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'门店列表-通道管理'中补充资料"
                ]);
            }
            $licenseDtEnd = $StoreInfo->store_license_time;
            if ($StoreInfo->store_license_time == '长期') {
                $licenseDtEnd = '9999-12-31';
            }
            $larIdCardEnd = $StoreInfo->head_sfz_time;
            if ($StoreInfo->head_sfz_time == '长期') {
                $larIdCardEnd = '9999-12-31';
            }
            $accountIdDtEnd = $StoreBank->bank_sfz_time;
            if ($StoreBank->bank_sfz_time == '长期') {
                $accountIdDtEnd = '9999-12-31';
            }
            //图片公共部分
            if (!$lklStore->agree_ment_url) {
                return json_encode([
                    'status' => -1,
                    'message' => '商户协议-未上传'
                ]);
            }
            $agree_ment = $lklStore->agree_ment_url;
            $agree_ment_res = $this->img_upload($agree_ment, '0', 'AGREE_MENT');
            if (!$agree_ment_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '商户协议-上传失败'
                ]);
            }
            LklStore::where('store_id', $store_id)->update(['agree_ment' => $agree_ment_res['url']]);

            if (!$StoreImg->head_sfz_img_a) {
                return json_encode([
                    'status' => -1,
                    'message' => '法人身份证正面-未上传'
                ]);
            }
            $id_card_front = $StoreImg->head_sfz_img_a;
            $id_card_front_res = $this->img_upload($id_card_front, '0', 'ID_CARD_FRONT');
            if (!$id_card_front_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '法人身份证正面-上传失败'
                ]);
            }
            LklStore::where('store_id', $store_id)->update(['id_card_front' => $id_card_front_res['url']]);

            if (!$StoreImg->head_sfz_img_b) {
                return json_encode([
                    'status' => 1,
                    'message' => '法人身份证反面-未上传'
                ]);
            }
            $id_card_behind = $StoreImg->head_sfz_img_b;
            $id_card_behind_res = $this->img_upload($id_card_behind, '0', 'ID_CARD_BEHIND');
            if (!$id_card_behind_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '法人身份证反面-上传失败'
                ]);
            }
            LklStore::where('store_id', $store_id)->update(['id_card_behind' => $id_card_behind_res['url']]);

            if (!$StoreImg->store_logo_img) {
                return json_encode([
                    'status' => 1,
                    'message' => '营业场所门头照-未上传'
                ]);
            }
            $shop_outside_img = $StoreImg->store_logo_img;
            $shop_outside_img_res = $this->img_upload($shop_outside_img, '0', 'SHOP_OUTSIDE_IMG');
            if (!$shop_outside_img_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '营业场所门头照-上传失败'
                ]);
            }
            LklStore::where('store_id', $store_id)->update(['shop_outside_img' => $shop_outside_img_res['url']]);

            if (!$StoreImg->store_img_a) {
                return json_encode([
                    'status' => 2,
                    'message' => '收银台照片-未上传'
                ]);
            }
            $checkstand_img = $StoreImg->store_img_a;
            $checkstand_img_res = $this->img_upload($checkstand_img, '0', 'CHECKSTAND_IMG');
            if (!$checkstand_img_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '收银台照片-上传失败'
                ]);
            }
            LklStore::where('store_id', $store_id)->update(['checkstand_img' => $checkstand_img_res['url']]);

            if (!$StoreImg->store_img_b) {
                return json_encode([
                    'status' => 2,
                    'message' => '经营场所照片-未上传'
                ]);
            }
            $shop_inside_img = $StoreImg->store_img_b;
            $shop_inside_img_res = $this->img_upload($shop_inside_img, '0', 'SHOP_INSIDE_IMG');
            if (!$shop_inside_img_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '经营场所照片-上传失败'
                ]);
            }
            LklStore::where('store_id', $store_id)->update(['shop_inside_img' => $shop_inside_img_res['url']]);

            if ($StoreInfo->store_type == 1 || $StoreInfo->store_type == 2) {//1个体商户、2企业商户
                if (!$StoreImg->store_license_img) {
                    return json_encode([
                        'status' => 2,
                        'message' => '营业执照-未上传'
                    ]);
                }
                $business_licence = $StoreImg->store_license_img;
                $business_licence_res = $this->img_upload($business_licence, '0', 'BUSINESS_LICENCE');
                if (!$business_licence_res) {
                    return json_encode([
                        'status' => -1,
                        'message' => '营业执照-上传失败'
                    ]);
                }
                LklStore::where('store_id', $store_id)->update(['business_licence' => $business_licence_res['url']]);

                if ($StoreBank->store_bank_type == '02') {// 02对公结算
                    //A011-开户许可证
                    if (!$StoreImg->store_industrylicense_img) {
                        return json_encode([
                            'status' => 2,
                            'message' => '开户许可证-未上传'
                        ]);
                    }
                    $opening_permit = $StoreImg->store_industrylicense_img;
                    $opening_permit_res = $this->img_upload($opening_permit, '0', 'OPENING_PERMIT');
                    if (!$opening_permit_res) {
                        return json_encode([
                            'status' => -1,
                            'message' => '开户许可证-上传失败'
                        ]);
                    }
                    LklStore::where('store_id', $store_id)->update(['opening_permit' => $opening_permit_res['url']]);

                } else if ($StoreBank->store_bank_type == '01') {// 对私结算
                    if (!$StoreImg->bank_img_a) {
                        return json_encode([
                            'status' => 2,
                            'message' => '结算银行卡正面-未上传'
                        ]);
                    }
                    $bank_card = $StoreImg->bank_img_a;
                    $bank_card_res = $this->img_upload($bank_card, '0', 'BANK_CARD');
                    if (!$bank_card_res) {
                        return json_encode([
                            'status' => -1,
                            'message' => '结算银行卡正面-上传失败'
                        ]);
                    }
                    LklStore::where('store_id', $store_id)->update(['bank_card' => $bank_card_res['url']]);

                    if ($StoreInfo->head_name != $StoreBank->store_bank_name) { //非法人结算
                        if (!$StoreImg->bank_sfz_img_a) {
                            return json_encode([
                                'status' => 2,
                                'message' => '结算卡身份证正面-未上传'
                            ]);
                        }
                        $settle_id_card_front = $StoreImg->bank_sfz_img_a;
                        $settle_id_card_front_res = $this->img_upload($settle_id_card_front, '0', 'SETTLE_ID_CARD_FRONT');
                        if (!$settle_id_card_front_res) {
                            return json_encode([
                                'status' => -1,
                                'message' => '结算卡身份证正面-上传失败'
                            ]);
                        }
                        LklStore::where('store_id', $store_id)->update(['settle_id_card_front' => $settle_id_card_front_res['url']]);

                        if (!$StoreImg->bank_sfz_img_b) {
                            return json_encode([
                                'status' => 2,
                                'message' => '结算卡身份证反面-未上传'
                            ]);
                        }
                        $settle_id_card_behind = $StoreImg->bank_sfz_img_b;
                        $settle_id_card_behind_res = $this->img_upload($settle_id_card_behind, '0', 'SETTLE_ID_CARD_BEHIND');
                        if (!$settle_id_card_behind_res) {
                            return json_encode([
                                'status' => -1,
                                'message' => '结算卡身份证反面-上传失败'
                            ]);
                        }
                        LklStore::where('store_id', $store_id)->update(['settle_id_card_behind' => $settle_id_card_behind_res['url']]);

                        if (!$StoreImg->store_auth_bank_img) {
                            return json_encode([
                                'status' => 2,
                                'message' => '结算授权书-未上传'
                            ]);
                        }
                        $letter_of_authorization = $StoreImg->store_auth_bank_img;
                        $letter_of_authorization_res = $this->img_upload($letter_of_authorization, '0', 'LETTER_OF_AUTHORIZATION');
                        if (!$letter_of_authorization_res) {
                            return json_encode([
                                'status' => -1,
                                'message' => '结算授权书-上传失败'
                            ]);
                        }
                        LklStore::where('store_id', $store_id)->update(['letter_of_authorization' => $letter_of_authorization_res['url']]);
                    }
                }
            } else { //小微
                if (!$StoreImg->bank_img_a) {
                    return json_encode([
                        'status' => 2,
                        'message' => '结算银行卡正面-未上传'
                    ]);
                }
                $bank_card = $StoreImg->bank_img_a;
                $bank_card_res = $this->img_upload($bank_card, '0', 'BANK_CARD');
                if (!$bank_card_res) {
                    return json_encode([
                        'status' => -1,
                        'message' => '结算银行卡正面-上传失败'
                    ]);
                }
                LklStore::where('store_id', $store_id)->update(['bank_card' => $bank_card_res['url']]);
            }
            if ($lklStore->others_url) {
                $others_res = $this->img_upload($lklStore->others_url, '0', 'OTHERS');
                if (!$others_res) {
                    return json_encode([
                        'status' => -1,
                        'message' => '其它图片-上传失败'
                    ]);
                }
                LklStore::where('store_id', $store_id)->update(['others' => $others_res['url']]);
            }
            $latitude = '';
            $longtude = '';
            if (!$StoreInfo->lat || !$StoreInfo->lng) {
                $storeController = new \App\Api\Controllers\User\StoreController();
                $address = $StoreInfo->province_name . $StoreInfo->city_name . $StoreInfo->area_name . $StoreInfo->store_address;//获取经纬度的地址
                $api_res = $storeController->query_address($address, env('LBS_KEY'));
                if ($api_res['status'] == '1') {
                    $store_lng = $api_res['result']['lng'];
                    $store_lat = $api_res['result']['lat'];
                    Store::where('store_id', $store_id)->update([
                        'lng' => $store_lng,
                        'lat' => $store_lat,
                    ]);
                    $latitude = $store_lat;
                    $longtude = $store_lng;
                }

            } else {
                $latitude = $StoreInfo->lat;
                $longtude = $StoreInfo->lng;
            }
            $accountType = '58';
            if ($lklStore->settle_type == 'D0') {
                $rate = 0.36;
            }
            $wei_rate = [
                'feeCode' => 'WECHAT',
                'feeValue' => $rate
            ];
            $ali_rate = [
                'feeCode' => 'ALIPAY',
                'feeValue' => $rate
            ];
            $scan_pay_second = [
                'feeCode' => 'SCAN_PAY_SECOND',
                'feeValue' => 0.02
            ];
            $card_second = [
                'feeCode' => 'CARD_SECOND',
                'feeValue' => 0.02
            ];

            if ($lklStore->settle_type == 'D0') {
                $fees = [
                    $wei_rate,
                    $ali_rate,
                    $scan_pay_second,
                    $card_second
                ];
            } else {
                $fees = [
                    $wei_rate,
                    $ali_rate
                ];
            }
            $bizContent = [
                'termNum' => 1,
                'activityId' => $this->activityId,
                'fees' => $fees,
                'mcc' => $lklStore->mcc,
            ];
            $attchments = [
                ['id' => $id_card_front_res['url'], 'type' => 'ID_CARD_FRONT'],
                ['id' => $id_card_behind_res['url'], 'type' => 'ID_CARD_BEHIND'],
                ['id' => $agree_ment_res['url'], 'type' => 'AGREE_MENT'],
                ['id' => $checkstand_img_res['url'], 'type' => 'CHECKSTAND_IMG'],
                ['id' => $shop_outside_img_res['url'], 'type' => 'SHOP_OUTSIDE_IMG'],
                ['id' => $shop_inside_img_res['url'], 'type' => 'SHOP_INSIDE_IMG']
            ];
            if ($StoreBank->store_bank_type == '02') {// 02对公结算
                $attchments[] = ['id' => $opening_permit_res['url'], 'type' => 'OPENING_PERMIT'];
//                $attchments[] = ['id' => $checkstand_img_res['url'], 'type' => 'CHECKSTAND_IMG'];
//                $attchments[] = ['id' => $shop_outside_img_res['url'], 'type' => 'SHOP_OUTSIDE_IMG'];
//                $attchments[] = ['id' => $shop_inside_img_res['url'], 'type' => 'SHOP_INSIDE_IMG'];
                $accountType = '57';
            } else {
                if ($StoreInfo->head_name != $StoreBank->store_bank_name) {// 非法人结算
                    $attchments[] = ['id' => $letter_of_authorization_res['url'], 'type' => 'LETTER_OF_AUTHORIZATION'];
                    $attchments[] = ['id' => $settle_id_card_front_res['url'], 'type' => 'SETTLE_ID_CARD_FRONT'];
                    $attchments[] = ['id' => $settle_id_card_behind_res['url'], 'type' => 'SETTLE_ID_CARD_BEHIND'];
                }
                $attchments[] = ['id' => $bank_card_res['url'], 'type' => 'BANK_CARD'];
            }
            if ($merType == 'TP_MERCHANT') {
                $attchments[] = ['id' => $business_licence_res['url'], 'type' => 'BUSINESS_LICENCE'];
            }
            $data = [
                'userNo' => $this->userNo,
                'email' => $lklStore->email,
                'busiCode' => 'WECHAT_PAY',
                'merRegName' => $StoreInfo->store_name,
                'merType' => $merType,
                'merName' => $StoreInfo->store_short_name,
                'merAddr' => $lklStore->city_name . $lklStore->area_name . $StoreInfo->store_address,
                'provinceCode' => $lklStore->province_code,
                'cityCode' => $lklStore->city_code,
                'countyCode' => $lklStore->area_code,
                'latitude' => $latitude,
                'longtude' => $longtude,
                'source' => 'H5',
                'businessContent' => $StoreInfo->category_name,
                'larName' => $StoreInfo->head_name,
                'larIdType' => '01',
                'larIdCard' => $StoreInfo->head_sfz_no,
                'larIdCardStart' => $StoreInfo->head_sfz_stime,
                'larIdCardEnd' => $larIdCardEnd,
                'contactMobile' => $StoreInfo->people_phone,
                'contactName' => $StoreInfo->people,
                'openningBankCode' => $lklStore->openning_bank_code,
                'openningBankName' => $lklStore->openning_bank_name,
                'clearingBankCode' => $lklStore->clearing_bank_code,
                'settleProvinceCode' => $lklStore->settle_province_code,
                'settleProvinceName' => $lklStore->settle_province_name,
                'settleCityCode' => $lklStore->settle_city_code,
                'settleCityName' => $lklStore->settle_city_name,
                'accountNo' => $StoreBank->store_bank_no,
                'accountName' => $StoreBank->store_bank_name,
                'accountType' => $accountType,
                'accountIdType' => '01',
                'accountIdCard' => $StoreBank->bank_sfz_no,
                'accountIdDtStart' => $StoreBank->bank_sfz_stime,
                'accountIdDtEnd' => $accountIdDtEnd,
                'bizContent' => $bizContent,
                'attchments' => $attchments,
                'settleType' => $lklStore->settle_type,
                'settlementType' => 'AUTOMATIC'
            ];
            if ($merType == 'TP_MERCHANT') {
                $data['licenseName'] = $StoreInfo->store_name;
                $data['licenseNo'] = $StoreInfo->store_license_no;
                $data['licenseDtStart'] = $StoreInfo->store_license_stime;
                $data['licenseDtEnd'] = $licenseDtEnd;
                $data['merRegName'] = $StoreInfo->store_short_name;
            }
            Log::info('拉卡拉商户入网--入参');
            Log::info(stripslashes(json_encode($data, JSON_UNESCAPED_UNICODE)));
            $url = $this->lkl_url . 'merchant';
            $token = $this->get_token_post($this->getToken);
            $res = $this->post($url, $data, $token);
            Log::info('拉卡拉商户入网--res');
            Log::info($res);
            $res_data = json_decode($res, true);
            if (isset($res_data['status'])) {
                $storeUp = [
                    'merchant_no' => $res_data['merchantNo'],
                    'audit_status' => '01',
                    'audit_msg' => '待审核',
                ];
                LklStore::where('store_id', $store_id)->update($storeUp);
                return json_encode([
                    'status' => 1,
                    'data' => $res_data
                ]);
            } else {
                return json_encode([
                    'status' => 2,
                    'data' => $res_data
                ]);
            }

        } catch (\Exception $ex) {
            Log::info('拉卡拉入网--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }

    }

    //商户费率变更
    public function updateMerchantFee(Request $request)
    {
        try {
            $store_id = $request->get('store_id', '1');
            $storePayWay = StorePayWay::where('store_id', $store_id)
                ->where('company', 'lklpay')
                ->where('ways_source', 'weixin')
                ->first();

            if (!$storePayWay) {
                return json_encode([
                    'status' => 2,
                    'message' => "未设置费率"
                ]);
            }
            $lklStore = LklStore::where('store_id', $store_id)->select('settle_type', 'merchant_no')->first();
            $rate = $storePayWay->rate;
            if ($lklStore->settle_type == 'D0') {
                $rate = 0.36;
            }
            $wei_rate = [
                'fee' => $rate,
                'feeType' => 'WECHAT'
            ];
            $ali_rate = [
                'fee' => $rate,
                'feeType' => 'ALIPAY'
            ];
            $scan_pay_second = [
                'fee' => 0.02,
                'feeType' => 'SCAN_PAY_SECOND'
            ];
            $card_second = [
                'fee' => 0.02,
                'feeType' => 'CARD_SECOND'
            ];

            if ($lklStore->settle_type == 'D0') {
                $fees = [
                    $wei_rate,
                    $ali_rate,
                    $scan_pay_second,
                    $card_second
                ];
            } else {
                $fees = [
                    $wei_rate,
                    $ali_rate
                ];
            }
            $fee_data = [
                'fees' => $fees,
                'settleType' => $lklStore->settle_type,
                'productCode' => 'WECHAT_PAY',
                'settlementType' => 'AUTOMATIC'
            ];
            $url = $this->lkl_up_url . 'fee/' . $lklStore->merchant_no;
            Log::info($url);
            Log::info('拉卡拉商户费率信息变更--入参');
            Log::info(stripslashes(json_encode($fee_data, JSON_UNESCAPED_UNICODE)));
            $token = $this->get_token_post($this->getToken, 'update');
            $res = $this->post($url, $fee_data, $token);
            Log::info('拉卡拉商户费率信息变更--res');
            Log::info($res);
            if (!$res) {
                return json_encode([
                    'status' => '-1',
                    'message' => '请求失败',
                ]);
            }
            $res_data = json_decode($res, true);
            if (isset($res_data['message']) && $res_data['message'] == 'SUCCESS') {
                $storeUp = [
                    'audit_status' => '01',
                    'audit_msg' => '待审核',
                ];
                LklStore::where('store_id', $store_id)->update($storeUp);
                StorePayWay::where('store_id', $store_id)
                    ->where('company', 'lklpay')
                    ->update(['status' => 2, 'status_desc' => '商户费率信息变更中']);
                return json_encode([
                    'status' => 1,
                    'message' => '请求成功',
                ]);
            } else {
                return json_encode([
                    'status' => '-1',
                    'message' => $res_data['message'],
                ]);
            }

        } catch (\Exception $ex) {
            Log::info('拉卡拉商户费率变更--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    //商户结算信息修改
    public function updateMerchantSettle(Request $request)
    {
        try {
            $store_id = $request->get('store_id', '1');
            $storeInfo = Store::where('store_id', $store_id)->select('*')->first();
            $storeImg = StoreImg::where('store_id', $store_id)
                ->select('*')
                ->first();
            if (!$storeImg) {
                return json_encode([
                    'status' => 2,
                    'message' => "请完善图片信息"
                ]);
            }
            $storeBank = StoreBank::where('store_id', $store_id)
                ->select('*')
                ->first();
            if (!$storeBank) {
                return json_encode([
                    'status' => 2,
                    'message' => "请完善账户信息"
                ]);
            }
            $lklStore = LklStore::where('store_id', $store_id)->select('*')->first();
            if ($storeInfo->store_type == 1 || $storeInfo->store_type == 2) {//1个体商户、2企业商户
                if ($storeBank->store_bank_type == '02') {// 02对公结算
                    //A011-开户许可证
                    if (!$storeBank->store_industrylicense_img) {
                        return json_encode([
                            'status' => 2,
                            'message' => '开户许可证-未上传'
                        ]);
                    }
                    $opening_permit = $storeImg->store_industrylicense_img;
                    $opening_permit_res = $this->img_upload($opening_permit, '0', 'OPENING_PERMIT');
                    if (!$opening_permit_res) {
                        return json_encode([
                            'status' => -1,
                            'message' => '开户许可证-上传失败'
                        ]);
                    }
                    LklStore::where('store_id', $store_id)->update(['opening_permit' => $opening_permit_res['url']]);

                } else if ($storeBank->store_bank_type == '01') {// 对私结算
                    if (!$storeImg->bank_img_a) {
                        return json_encode([
                            'status' => 2,
                            'message' => '结算银行卡正面-未上传'
                        ]);
                    }
                    $bank_card = $storeImg->bank_img_a;
                    $bank_card_res = $this->img_upload($bank_card, '0', 'BANK_CARD');
                    if (!$bank_card_res) {
                        return json_encode([
                            'status' => -1,
                            'message' => '结算银行卡正面-上传失败'
                        ]);
                    }
                    LklStore::where('store_id', $store_id)->update(['bank_card' => $bank_card_res['url']]);

                    if ($storeInfo->head_name != $storeBank->store_bank_name) { //非法人结算
                        if (!$storeImg->bank_sfz_img_a) {
                            return json_encode([
                                'status' => 2,
                                'message' => '结算卡身份证正面-未上传'
                            ]);
                        }
                        $settle_id_card_front = $storeImg->bank_sfz_img_a;
                        $settle_id_card_front_res = $this->img_upload($settle_id_card_front, '0', 'SETTLE_ID_CARD_FRONT');
                        if (!$settle_id_card_front_res) {
                            return json_encode([
                                'status' => -1,
                                'message' => '结算卡身份证正面-上传失败'
                            ]);
                        }
                        LklStore::where('store_id', $store_id)->update(['settle_id_card_front' => $settle_id_card_front_res['url']]);

                        if (!$storeImg->bank_sfz_img_b) {
                            return json_encode([
                                'status' => 2,
                                'message' => '结算卡身份证反面-未上传'
                            ]);
                        }
                        $settle_id_card_behind = $storeImg->bank_sfz_img_b;
                        $settle_id_card_behind_res = $this->img_upload($settle_id_card_behind, '0', 'SETTLE_ID_CARD_BEHIND');
                        if (!$settle_id_card_behind_res) {
                            return json_encode([
                                'status' => -1,
                                'message' => '结算卡身份证反面-上传失败'
                            ]);
                        }
                        LklStore::where('store_id', $store_id)->update(['settle_id_card_behind' => $settle_id_card_behind_res['url']]);

                        if (!$storeImg->store_auth_bank_img) {
                            return json_encode([
                                'status' => 2,
                                'message' => '结算授权书-未上传'
                            ]);
                        }
                        $letter_of_authorization = $storeImg->store_auth_bank_img;
                        $letter_of_authorization_res = $this->img_upload($letter_of_authorization, '0', 'LETTER_OF_AUTHORIZATION');
                        if (!$letter_of_authorization_res) {
                            return json_encode([
                                'status' => -1,
                                'message' => '结算授权书-上传失败'
                            ]);
                        }
                        LklStore::where('store_id', $store_id)->update(['letter_of_authorization' => $letter_of_authorization_res['url']]);
                    }
                }
            } else { //小微
                if (!$storeImg->bank_img_a) {
                    return json_encode([
                        'status' => 2,
                        'message' => '结算银行卡正面-未上传'
                    ]);
                }
                $bank_card = $storeImg->bank_img_a;
                $bank_card_res = $this->img_upload($bank_card, '0', 'BANK_CARD');
                if (!$bank_card_res) {
                    return json_encode([
                        'status' => -1,
                        'message' => '结算银行卡正面-上传失败'
                    ]);
                }
                LklStore::where('store_id', $store_id)->update(['bank_card' => $bank_card_res['url']]);
            }
            $attachments = [];
            $accountKind = '58';
            if ($storeBank->store_bank_type == '02') {// 02对公结算
                $attachments[] = ['id' => $lklStore->opening_permit, 'type' => 'OPENING_PERMIT'];
                $accountKind = '57';
            } else {
                if ($storeInfo->head_name != $storeBank->store_bank_name) {// 非法人结算
                    $attachments[] = ['id' => $lklStore->letter_of_authorization, 'type' => 'LETTER_OF_AUTHORIZATION'];
                    $attachments[] = ['id' => $lklStore->settle_id_card_front, 'type' => 'SETTLE_ID_CARD_FRONT'];
                    $attachments[] = ['id' => $lklStore->settle_id_card_behind, 'type' => 'SETTLE_ID_CARD_BEHIND'];
                }
                $attachments[] = ['id' => $lklStore->bank_card, 'type' => 'BANK_CARD'];
            }
            $url = $this->lkl_up_url . 'settle/' . $lklStore->merchant_no;

            $settle_data = [
                'accountKind' => $accountKind,
                'accountName' => $storeBank->store_bank_name,
                'accountNo' => $storeBank->store_bank_no,
                'attachments' => $attachments,
                'bankNo' => $lklStore->openning_bank_code,
                'bankName' => $lklStore->openning_bank_name,
                'clearingBankNo' => $lklStore->clearing_bank_code,
                'identityNo' => $storeBank->bank_sfz_no,
                'settleProvinceCode' => $lklStore->settle_province_code,
                'settleProvinceName' => $lklStore->settle_province_name,
                'settleCityCode' => $lklStore->settle_city_code,
                'settleCityName' => $lklStore->settle_city_name,
            ];
            Log::info('拉卡拉商户结算信息变更--入参');
            Log::info(stripslashes(json_encode($settle_data, JSON_UNESCAPED_UNICODE)));
            $token = $this->get_token_post($this->getToken, 'update');
            $res = $this->post($url, $settle_data, $token);
            Log::info('拉卡拉商户结算信息变更--res');
            Log::info($res);
            if (!$res) {
                return json_encode([
                    'status' => '-1',
                    'message' => '请求失败',
                ]);
            }
            $res_data = json_decode($res, true);
            if (isset($res_data['message']) && $res_data['message'] == 'SUCCESS') {
                $storeUp = [
                    'audit_status' => '01',
                    'audit_msg' => '待审核',
                ];
                LklStore::where('store_id', $store_id)->update($storeUp);
                StorePayWay::where('store_id', $store_id)
                    ->where('company', 'lklpay')
                    ->update(['status' => 2, 'status_desc' => '商户账户信息变更中']);
                return json_encode([
                    'status' => 1,
                    'message' => '请求成功',
                ]);
            } else {
                return json_encode([
                    'status' => '-1',
                    'message' => $res_data['message'],
                ]);
            }

        } catch (\Exception $ex) {
            Log::info('拉卡拉商户结算信息变更--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    //商户基础信息修改
    public function updateMerchant(Request $request)
    {
        try {
            $store_id = $request->get('store_id', '1');
            $storeInfo = Store::where('store_id', $store_id)->select('*')->first();
            $storeImg = StoreImg::where('store_id', $store_id)
                ->select('*')
                ->first();
            if (!$storeImg) {
                return json_encode([
                    'status' => 2,
                    'message' => "请完善图片信息"
                ]);
            }
            $storeBank = StoreBank::where('store_id', $store_id)
                ->select('*')
                ->first();
            if (!$storeBank) {
                return json_encode([
                    'status' => 2,
                    'message' => "请完善账户信息"
                ]);
            }
            $licenseDtEnd = $storeInfo->store_license_time;
            if ($storeInfo->store_license_time == '长期') {
                $licenseDtEnd = '9999-12-31';
            }
            $larIdCardEnd = $storeInfo->head_sfz_time;
            if ($storeInfo->head_sfz_time == '长期') {
                $larIdCardEnd = '9999-12-31';
            }
            $lklStore = LklStore::where('store_id', $store_id)->select('*')->first();
            if (!$storeImg->head_sfz_img_a) {
                return json_encode([
                    'status' => -1,
                    'message' => '法人身份证正面-未上传'
                ]);
            }
            $id_card_front = $storeImg->head_sfz_img_a;
            $id_card_front_res = $this->img_upload($id_card_front, '0', 'ID_CARD_FRONT');
            if (!$id_card_front_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '法人身份证正面-上传失败'
                ]);
            }
            LklStore::where('store_id', $store_id)->update(['id_card_front' => $id_card_front_res['url']]);

            if (!$storeImg->head_sfz_img_b) {
                return json_encode([
                    'status' => 1,
                    'message' => '法人身份证反面-未上传'
                ]);
            }
            $id_card_behind = $storeImg->head_sfz_img_b;
            $id_card_behind_res = $this->img_upload($id_card_behind, '0', 'ID_CARD_BEHIND');
            if (!$id_card_behind_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '法人身份证反面-上传失败'
                ]);
            }
            LklStore::where('store_id', $store_id)->update(['id_card_behind' => $id_card_behind_res['url']]);


            if (!$storeImg->store_logo_img) {
                return json_encode([
                    'status' => 1,
                    'message' => '营业场所门头照-未上传'
                ]);
            }
            $shop_outside_img = $storeImg->store_logo_img;
            $shop_outside_img_res = $this->img_upload($shop_outside_img, '0', 'SHOP_OUTSIDE_IMG');
            if (!$shop_outside_img_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '营业场所门头照-上传失败'
                ]);
            }
            LklStore::where('store_id', $store_id)->update(['shop_outside_img' => $shop_outside_img_res['url']]);

            if (!$storeImg->store_img_a) {
                return json_encode([
                    'status' => 2,
                    'message' => '收银台照片-未上传'
                ]);
            }
            $checkstand_img = $storeImg->store_img_a;
            $checkstand_img_res = $this->img_upload($checkstand_img, '0', 'CHECKSTAND_IMG');
            if (!$checkstand_img_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '收银台照片-上传失败'
                ]);
            }
            LklStore::where('store_id', $store_id)->update(['checkstand_img' => $checkstand_img_res['url']]);

            if (!$storeImg->store_img_b) {
                return json_encode([
                    'status' => 2,
                    'message' => '经营场所照片-未上传'
                ]);
            }
            $shop_inside_img = $storeImg->store_img_b;
            $shop_inside_img_res = $this->img_upload($shop_inside_img, '0', 'SHOP_INSIDE_IMG');
            if (!$shop_inside_img_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '经营场所照片-上传失败'
                ]);
            }
            LklStore::where('store_id', $store_id)->update(['shop_inside_img' => $shop_inside_img_res['url']]);

            if ($storeInfo->store_type == 1 || $storeInfo->store_type == 2) {
                if (!$storeImg->store_license_img) {
                    return json_encode([
                        'status' => 2,
                        'message' => '营业执照-未上传'
                    ]);
                }
                $business_licence = $storeImg->store_license_img;
                $business_licence_res = $this->img_upload($business_licence, '0', 'BUSINESS_LICENCE');
                if (!$business_licence_res) {
                    return json_encode([
                        'status' => -1,
                        'message' => '营业执照-上传失败'
                    ]);
                }
                LklStore::where('store_id', $store_id)->update(['business_licence' => $business_licence_res['url']]);
            }
            if ($storeInfo->store_type == '1') { //个体工商户
                $merType = 'TP_MERCHANT';
            } else if ($storeInfo->store_type == '2') { //企业
                $merType = 'TP_MERCHANT';
            } else if ($storeInfo->store_type == '3') { //小微商户
                $merType = 'TP_PERSONAL';
            }
            $fileData = [
                ['imgPath' => $lklStore->id_card_front, 'imgType' => 'ID_CARD_FRONT'],
                ['imgPath' => $lklStore->id_card_behind, 'imgType' => 'ID_CARD_BEHIND'],
            ];
            $store_data = [
                'merBizName' => $storeInfo->store_short_name,
                'provinceCode' => $lklStore->province_code,
                'cityCode' => $lklStore->city_code,
                'countyCode' => $lklStore->area_code,
                'provinceName' => $lklStore->province_name,
                'cityName' => $lklStore->city_name,
                'countyName' => $lklStore->area_name,
                'merRegAddr' => $storeInfo->store_address,
                'merContactName' => $storeInfo->people,
                'merContactMobile' => $storeInfo->people_phone,
                'larName' => $storeInfo->head_name,
                'larIdcard' => $storeInfo->head_sfz_no,
                'customerNo' => $lklStore->merchant_no,
                'larIdcardStDt' => $storeInfo->head_sfz_stime,
                'larIdcardExpDt' => $larIdCardEnd,
                'fileData' => $fileData
            ];

            $fileData[] = ['imgPath' => $lklStore->checkstand_img, 'imgType' => 'CHECKSTAND_IMG'];
            $fileData[] = ['imgPath' => $lklStore->shop_outside_img, 'imgType' => 'SHOP_OUTSIDE_IMG'];
            $fileData[] = ['imgPath' => $lklStore->shop_inside_img, 'imgType' => 'SHOP_INSIDE_IMG'];

            if ($storeInfo->store_type == 1 || $storeInfo->store_type == 2) {
                $fileData[] = ['imgPath' => $lklStore->business_licence, 'imgType' => 'BUSINESS_LICENCE'];
                $store_data['merBlisExpDt'] = $licenseDtEnd;
                $store_data['merBlis'] = $storeInfo->store_license_no;
                $store_data['merBlisName'] = $storeInfo->store_name;
            }
            Log::info('拉卡拉商户基础信息变更--入参');
            Log::info(stripslashes(json_encode($store_data, JSON_UNESCAPED_UNICODE)));
            $url = $this->lkl_up_url . 'basic';
            $token = $this->get_token_post($this->getToken, 'update');
            $res = $this->post($url, $store_data, $token);
            Log::info('拉卡拉商户基础信息变更--res');
            Log::info($res);
            if (!$res) {
                return json_encode([
                    'status' => '-1',
                    'message' => '请求失败',
                ]);
            }
            $res_data = json_decode($res, true);
            if (isset($res_data['message']) && $res_data['message'] == 'SUCCESS') {
                $storeUp = [
                    'audit_status' => '01',
                    'audit_msg' => '待审核',
                ];
                LklStore::where('store_id', $store_id)->update($storeUp);
                StorePayWay::where('store_id', $store_id)
                    ->where('company', 'lklpay')
                    ->update(['status' => 2, 'status_desc' => '商户信息变更中']);
                return json_encode([
                    'status' => 1,
                    'message' => '请求成功',
                ]);
            } else {
                return json_encode([
                    'status' => '-1',
                    'message' => $res_data['message'],
                ]);
            }


        } catch
        (\Exception $ex) {
            Log::info('拉卡拉商户结算信息变更--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }


    //查询门店类别
    public function queryMccList(Request $request)
    {
        try {
            $parent_code = $request->get('parent_code', '1');
            $data = LklPayMcc::where('parent_code', $parent_code)->where('business_scene', '2')->select('*')->get();
            return json_encode([
                'status' => 1,
                'data' => $data
            ]);
        } catch (\Exception $ex) {
            Log::info('拉卡拉获取门店类别--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }

    }

    //查询地区信息
    public function queryAreaList(Request $request)
    {
        try {
            $parent_code = $request->get('parent_code', '1');
            $token = $this->get_token_post($this->getToken);
            $url = $this->lkl_url . 'organization/' . $parent_code;
            $data = $this->get_htkregistration_post($url, $token);
            $list = json_decode($data, true);
            return json_encode([
                'status' => 1,
                'data' => $list
            ]);
        } catch (\Exception $ex) {
            Log::info('拉卡拉获取地区信息--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }

    }

    //查询银行地区信息
    public function queryBankAreaList(Request $request)
    {
        try {
            $parent_code = $request->get('parent_code', '1');
            $token = $this->get_token_post($this->getToken);
            $url = $this->lkl_url . 'organization/bank/' . $parent_code;
            $data = $this->get_htkregistration_post($url, $token);
            $list = json_decode($data, true);
            return json_encode([
                'status' => 1,
                'data' => $list
            ]);
        } catch (\Exception $ex) {
            Log::info('拉卡拉获取银行地址信息--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }

    }

    //查询银行信息
    public function queryBankList(Request $request)
    {
        try {
            $area_code = $request->get('area_code', '1');
            $bank_name = $request->get('bank_name', '');
            if ($bank_name == '中国银行') {
                $bank_name = $bank_name . '股份';
            }
            $token = $this->get_token_post($this->getToken);
            $url = $this->lkl_url . 'bank?' . http_build_query(['areaCode' => $area_code, 'bankName' => $bank_name]);
            Log::info($url);
            $data = $this->get_htkregistration_post($url, $token);
            $list = json_decode($data, true);
            return json_encode([
                'status' => 1,
                'data' => $list
            ]);
        } catch (\Exception $ex) {
            Log::info('拉卡拉获取银行地址信息--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }

    }

    public function getLklPayMccApp(Request $request)
    {
        try {
            $mccLit = DB::table('lkl_mcc')
                ->where('parent_code', '1')
                ->where('business_scene', '2')
                ->select('name as label', 'code as value')
                ->get();

            foreach ($mccLit as $mcc) {

                $subMccLit = DB::table('lkl_mcc')
                    ->where('parent_code', $mcc->value)
                    ->where('business_scene', '2')
                    ->select('name as label', 'code as value')
                    ->get();
                $mcc->children = $subMccLit;
            }

            return response()->json([
                'status' => 1,
                'data' => $mccLit
            ]);
        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }
    }

    public function add_lkl_img(Request $request)
    {
        try {
            $store_id = $request->get('storeId', ''); //门店id
            $storeImg = StoreImg::where('store_id', $store_id)->select('*')->first();
            if (!$storeImg) {
                return [
                    'status' => '-1',
                    'message' => '请完善商户信息'
                ];
            }

            $head_sfz_img_a = $storeImg->head_sfz_img_a;
            $this->img_upload($head_sfz_img_a, '0', 'ID_CARD_FRONT');

        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    public function getSubMerchant(Request $request)
    {
        try {
            $store_id = $request->get('store_id', '');
            $LklStore = LklStore::where('store_id', $store_id)->select('*')->first();
            if ($LklStore->wei_mer && $LklStore->ali_mer) {
                return json_encode([
                    'status' => '1',
                    'message' => '查询成功',
                    'data' => $LklStore
                ]);
            }
            if ($LklStore) {
                $sign_data = [
                    'externalCustomerNo' => $LklStore->customer_no
                ];
                $sign = $this->privateKeyDecrypt($sign_data);
                $data = [
                    'data' => $sign
                ];
                Log::info('拉卡拉子商户号查询--入参');
                Log::info(stripslashes(json_encode($data, JSON_UNESCAPED_UNICODE)));
                $token = $this->get_token_post($this->getToken);
                $res = $this->post($this->lkl_mer_url, $data, $token);
                Log::info('拉卡拉子商户号查询--res');
                Log::info($res);
                $res_data = json_decode($res, true);
                if (isset($res_data['data'])) {
                    $res_data = $this->publicKeyDecrypt($res_data['data']);
                    $res_data = json_decode($res_data, true);
                    Log::info('拉卡拉子商户号查询--res--明文' . json_encode($res_data));
                    $zFBList = $res_data['zFBList'];
                    foreach ($zFBList as $zfb) {
                        LklStore::where('store_id', $store_id)->update(['ali_mer' => $zfb['subMerchantNo']]);
                    }
                    $wXList = $res_data['wXList'];
                    foreach ($wXList as $wx) {
                        if ($wx['channelId'] == '183399713') {
                            LklStore::where('store_id', $store_id)->update(['wei_mer' => $wx['subMerchantNo']]);
                        }
                    }
                }

            }
            $LklStore = LklStore::where('store_id', $store_id)->select('*')->first();
            return json_encode([
                'status' => '1',
                'message' => '查询成功',
                'data' => $LklStore
            ]);

        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    /**
     * @param $img_url
     * @param $sourcechnl 0:PC, 1:安卓, 2:IOS
     * @param $imgType
     */
    public function img_upload($img_url, $sourcechnl, $imgType)
    {
        $filePath = $this->images_get($img_url);
        $file = new \CURLFile(realpath($filePath));

        $url = $this->lkl_url . 'file/upload';
        $token = $this->get_token_post($this->getToken);

        $reqData = [
            'file' => $file,
            'imgType' => $imgType, //'AGREE_MENT'
            'sourcechnl' => $sourcechnl,
            'isOcr' => 'false', // 字符串格式
        ];

        return $this->upload_post($url, $token, $reqData);

    }


}
