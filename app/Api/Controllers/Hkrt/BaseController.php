<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/05/19
 * Time: 下午5:13
 */
namespace App\Api\Controllers\Hkrt;


use Illuminate\Support\Facades\Log;

class BaseController
{
    public $postCharset = "UTF-8"; //
    public $fileCharset = "UTF-8"; //

//    public $url = 'http://47.95.131.62:8080'; //测试地址
    public $url = 'http://39.96.106.181:8080'; //生产地址

    public $merchant_apply = '/api/v1/merchant/apply'; //商户入网（本接口用于商户入网接口，入网成功后默认T1交易）
    public $merchant_modify = '/api/v1/merchant/modify'; //商户入网信息修改
    public $merchant_withdrawal_apply = '/api/v1/merchant-withdrawal/apply'; //商户结算方式设置及变更，若已设置则为变更操作，D0,D1,Ts
    public $merchant_bank_info_modify = '/api/v1/merchant-bank-info/modify'; //商户结算卡变更
    public $merchant_image_upload = '/api/v1/merchant-image/upload'; //图片上传
    public $merchant_biz_query = '/api/v1/merchant-biz/query'; //商户业务查询

    //聚合支付
    public $polymeric_passivepay = '/api/v1/pay/polymeric/passivepay'; //被扫(微信、支付宝、银联二维码)
    public $polymeric_jsapipay = '/api/v1/pay/polymeric/jsapipay'; //聚合公众号支付API
    public $polymeric_query = '/api/v1/pay/polymeric/query'; //查询订单
    public $polymeric_closeorder = '/api/v1/pay/polymeric/closeorder'; //关闭订单
    public $polymeric_refund = '/api/v1/pay/polymeric/refund'; //申请退款
    public $polymeric_refundquery = '/api/v1/pay/polymeric/refundquery'; //退款查询
    public $wx_appid_conf_add = '/api/v1/wx-appid-conf/add'; //报备微信公众号


    /**
     * curl post java对接  传输数据流
     * @param $data
     * @param $Url
     * @return mixed
     */
    public function execute($data, $Url)
    {
        $ch = curl_init($Url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data)); //JSON类型字符串
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type=application/json;charset=UTF-8'));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }


    /**
     * 参数拼接
     * @param $params array 要拼接的参数
     * @param $access_key string sSaaS平台应用密钥
     * @return string
     */
    public function getSignContent($params, $access_key)
    {
        ksort($params);
        $string_2_sign = "";
        $i = 0;

        foreach ($params as $key => $values) {
            if (($this->checkEmpty($values) === false)  && ("@" != substr($values, 0, 1))) {
                // 转换成目标字符集
                $values = $this->character($values, $this->postCharset);

                if ($i == 0) {
                    $string_2_sign .= "$key" . "=" . "$values";
                } else {
                    $string_2_sign .= "&" . "$key" . "=" . "$values";
                }

                $i++;
            }
        }

        unset ($key, $values);
        return strtoupper(md5($string_2_sign.$access_key));
    }


    /**
     * 校验$value是否非空
     * if not set ,return true;
     * if is null , return true;
     * @param $value mixed 检查的字段
     * @return bool
     */
    protected function checkEmpty($value)
    {
        if (!isset($value))
            return true;
        if ($value === null)
            return true;
        if (trim($value) === "")
            return true;

        return false;
    }


    /**
     * 转换字符集编码
     * @param $data
     * @param $targetCharset
     * @return string
     */
    function character($data, $targetCharset)
    {
        if (!empty($data)) {
            $fileType = $this->fileCharset;
            if (strcasecmp($fileType, $targetCharset) != 0) {
                $data = mb_convert_encoding($data, $targetCharset);
            }
        }

        return $data;
    }


    /**
     * 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
     * @param $para
     * @return string
     */
    function createLinkString($para)
    {
        $params = [];
        foreach ($para as $key => $value) {
            if (is_array($value)) {
                $value = stripslashes(json_encode($value, JSON_UNESCAPED_UNICODE));
            }
            $params[] = $key . '=' . $value;
        }
        $data = implode("&", $params);

        return $data;
    }


    /**
     * 对数组排序
     * @param $para
     * @return mixed
     */
    function argSort($para)
    {
        ksort($para);

        return $para;
    }


}