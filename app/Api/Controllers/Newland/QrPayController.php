<?php
/**
 * Created by PhpStorm.
 * User: daimingkang
 * Date: 2018/10/20
 * Time: 11:01 AM
 */

namespace App\Api\Controllers\Newland;


use App\Common\PaySuccessAction;
use App\Models\MemberCzList;
use App\Models\MemberList;
use App\Models\MerchantWalletDetail;
use App\Models\Order;
use App\Models\RefundOrder;
use App\Models\Store;
use App\Models\StorePayWay;
use App\Models\UserWalletDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class QrPayController extends \App\Api\Controllers\BaseController
{


    //支付后跳转回调地址
    public function pay_action(Request $request)
    {


        $return = $request->all();
        $tradeno = $request->get('tradeno');
        $total_amount = $request->get('amount');
        $message = $request->get('message', '支付成功');
        $ad_p_id = '';
        $store_id = '';
        $ways_source = "";
        $device_id = "";
        $user_id = "";
        $out_trade_no = $tradeno;


        $day = date('Ymd', time());
        $table = 'orders_' . $day;
        if (Schema::hasTable($table)) {
            $order = DB::table($table)->where('out_trade_no', $out_trade_no)
                ->select('store_id', 'ways_source', 'device_id')
                ->first();
        } else {
            $order = Order::where('out_trade_no', $out_trade_no)
                ->select('store_id', 'ways_source', 'device_id')
                ->first();
        }

        if ($order) {
            $store_id = $order->store_id;
            $ways_source = $order->ways_source;
            $device_id = $order->device_id;

        }

        if ($ways_source == "alipay") {
            //支付宝失败
            $ad_p_id = '3';
        } else {
            //微信失败
            $ad_p_id = '4';

        }
        //支付成功
        if ($return['returncode'] == '000000') {
            if ($ways_source == "alipay") {
                $ad_p_id = '1';
            } else {
                $ad_p_id = '2';

            }
            $url = "&store_id=" . $store_id . '&total_amount=' . $total_amount . "&ad_p_id=" . $ad_p_id;
            $message = '支付成功';


            if ($device_id == "member_cz") {
                $cz = "";
                $cz_s = "";
                $mb_money = "";

                $MemberCzList = MemberCzList::where('store_id', $store_id)
                    ->where('out_trade_no', $out_trade_no)
                    ->select('cz_money', 'cz_s_money', 'mb_id')
                    ->first();
                if ($MemberCzList) {
                    $cz = $MemberCzList->cz_money;
                    $cz_s = $MemberCzList->cz_s_money;
                    $MemberList = MemberList::where('store_id', $store_id)
                        ->where('mb_id', $MemberCzList->mb_id)
                        ->first();
                    if ($MemberList) {
                        $mb_money = $MemberList->mb_money;
                    }

                }

                $data_url = "&store_id=" . $store_id . '&total_amount=' . $total_amount . "&ad_p_id=" . $ad_p_id . '&cz=' . $cz . "&cz_s=" . $cz_s . "&mb_money=" . $mb_money;
                $url = url('page/cz_success?message=支付成功') . $data_url;
            } else {
                $url = url('/page/pay_success?message=') . $message . $url;
            }

        } //用户取消
        elseif ($return['returncode'] == '000098') {
            $url = "&store_id=" . $store_id . '&total_amount=' . $total_amount . "&ad_p_id=" . $ad_p_id;
            $message = '用户取消';
            $url = url('/page/pay_errors?message=') . $message . $url;

        } //失败
        else {
            $message = '支付失败';
            $url = "&store_id=" . $store_id . '&total_amount=' . $total_amount . "&ad_p_id=" . $ad_p_id;
            $url = url('/page/pay_errors?message=') . $message . $url;
        }

        return redirect($url);


    }


    //新大陆刷卡订单号入库
    public function PayInOrder(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $merchant_id = $merchant->merchant_id;
            $merchant_name = $merchant->merchant_name;
            $config_id = $merchant->config_id;
            $ways_type = $request->get('ways_type', '8005');
            $company = $request->get('company', 'newland');
            $device_id = $request->get('device_id', '');
            $pay_status = $request->get('pay_status');
            $total_amount = $request->get('total_amount');
            $translocaltime = $request->get('translocaltime');
            $translocaldate = $request->get('translocaldate');
            $out_trade_no = $request->get('out_trade_no', '');//外部订单号
            $trade_no = $request->get('trade_no', '');//新大陆订单号
            $refund_no = $request->get('refund_no', '');
            $other_no = $request->get('other_no', '');

            $status_desc = '支付失败';
            $store_id = $request->get('store_id');
            $remark = $request->get('remark', '');
            if ($pay_status == '1') {
                $status_desc = '支付成功';
            }

            if ($pay_status == '6') {
                $status_desc = '退款成功';
            }

            $store = Store::where('store_id', $store_id)
                ->select('user_id', 'store_name')
                ->first();
            if (!$store) {
                return json_encode(['status' => 2, 'message' => '门店ID不存在']);

            }

            $tg_user_id = $store->user_id;
            $store_name = $store->store_name;


            $day = date('Ymd', time());
            $table = 'orders_' . $day;
            if (Schema::hasTable($table)) {
                $order = DB::table($table)->where('out_trade_no', $out_trade_no)->first();
            } else {
                $order = Order::where('out_trade_no', $out_trade_no)->first();
            }


            if ($order) {
                $insertW = [
                    'trade_no' => $trade_no,
                    "out_trade_no" => $out_trade_no,
                    "other_no" => $other_no,
                    'status' => $pay_status,
                    'pay_status_desc' => $status_desc,
                    'pay_status' => $pay_status,
                ];

                $order->update($insertW);
                $order->save();

            } else {
                $rate = '0.6';

                //插入数据库
                $StorePayWay = StorePayWay::where('ways_type', '8005')
                    ->where('store_id', $store_id)
                    ->select('rate_e')
                    ->first();

                if ($StorePayWay) {

                    $rate = $StorePayWay->rate_e;
                }

                $fee_amount = ($rate * $total_amount) / 100;

                $data_insert = [
                    'trade_no' => $trade_no,
                    "other_no" => $other_no,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'buyer_id' => '',
                    'ways_type' => $ways_type,
                    'ways_type_desc' => '银联刷卡',
                    'ways_source' => 'unionpay',
                    'ways_source_desc' => '银联刷卡',
                    'rate' => $rate,
                    'fee_amount' => $fee_amount,
                    'total_amount' => $total_amount,
                    'out_trade_no' => $out_trade_no,
                    'shop_price' => $total_amount,
                    'payment_method' => '',
                    'company' => $company,
                    'status' => $pay_status,
                    'pay_status' => $pay_status,
                    'pay_status_desc' => $status_desc,
                    'merchant_id' => $merchant_id,
                    'merchant_name' => $merchant_name,
                    'remark' => $remark,
                    'device_id' => $device_id,
                    'config_id' => $config_id,
                    'user_id' => $tg_user_id,
                ];

                if ($ways_type == "8005") {
                    $data_insert['created_at'] = $translocaldate . $translocaltime;
                    $data_insert['updated_at'] = $translocaldate . $translocaltime;
                }

                $this->insert_day_order($data_insert);

                //支付成功
                if ((int)$pay_status == 1) {
                    //支付成功后的动作
                    $data = [
                        'ways_type' => '8005',
                        'ways_type_desc' => '银联刷卡',
                        'source_type' => '8000',//返佣来源
                        'source_desc' => '银联刷卡',//返佣来源说明
                        'total_amount' => $total_amount,
                        'out_trade_no' => $out_trade_no,
                        'rate' => $data_insert['rate'],
                        'company' => 'newland',
                        'fee_amount' => $fee_amount,
                        'merchant_id' => $merchant_id,
                        'store_id' => $store_id,
                        'user_id' => $tg_user_id,
                        'config_id' => $config_id,
                        'store_name' => $store_name,
                        'ways_source' => 'unionpay',
                        'trade_no' => $trade_no,
                        'buyer_id' => '',
                        'ways_source_desc' => '银联刷卡',
                        'shop_price' => $total_amount,
                        'payment_method' => '',
                        'status' => $pay_status,
                        'pay_status' => $pay_status,
                        'pay_status_desc' => $status_desc,
                        'merchant_name' => $merchant_name,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'created_at' => $translocaldate . $translocaltime,
                        'updated_at' => $translocaldate . $translocaltime,
                        'no_print' => '1',//不打印
                        'no_push' => '1',//不推送
                    ];


                    PaySuccessAction::action($data);


                }

                $this->update_day_order($data_insert, $out_trade_no);

            }


            //

            if ($pay_status == '6') {
                $status_desc = '退款成功';

                $update_data = [
                    'pay_status_desc' => '已退款',
                    'pay_status' => 6,
                    'fee_amount' => 0,
                    'refund_amount' => $order->refund_amount + $total_amount,
                    'status' => 6
                ];
                $this->update_day_order($update_data, $out_trade_no);

                RefundOrder::create([
                    'ways_source' => $order->ways_source,
                    'type' => '8005',
                    'refund_amount' => $order->total_amount,//退款金额
                    'refund_no' => $refund_no ? $refund_no : $order->out_trade_no . rand(1000, 9999),//退款单号
                    'store_id' => $store_id,
                    'merchant_id' => $merchant_id,
                    'out_trade_no' => $order->out_trade_no,
                    'trade_no' => $order->trade_no
                ]);



                //返佣去掉
                UserWalletDetail::where('out_trade_no', $out_trade_no)->update([
                    'settlement' => '03',
                    'settlement_desc' => '退款订单',
                ]);
                MerchantWalletDetail::where('out_trade_no', $out_trade_no)->update([
                    'settlement' => '03',
                    'settlement_desc' => '退款订单',
                ]);


            }

            return json_encode([
                    'status' => 1,
                    'message' => '刷卡' . $status_desc,
                    'data' => [
                        "out_trade_no" => $out_trade_no,
                        "trade_no" => $trade_no,
                        'total_amount' => $request->get('total_amount'),
                    ]
                ]
            );
        } catch (\Exception $exception) {
            Log::info('报错');

            Log::info($exception);
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }


}