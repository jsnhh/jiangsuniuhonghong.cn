<?php
namespace App\Api\Controllers\Newland;


use App\Common\XingPOS\Request\XingStoreHuQuWeiXinShuaLianPingZheng;
use function EasyWeChat\Kernel\Support\get_client_ip;
use function EasyWeChat\Kernel\Support\get_server_ip;
use Illuminate\Support\Facades\Log;

class PayController extends BaseController
{
    //扫一扫 0-系统错误 1-成功 2-正在支付 3-失败
    public function scan_pay($data)
    {
        try {
            $out_trade_no = $data['out_trade_no'];
            $code = $data['code'];
            $total_amount = $data['total_amount'];
            $remark = $data['remark'];
            $device_id = $data['device_id'];
            $shop_name = $data['shop_name'];
            $key = $data['key'];
            $org_no = $data['org_no'];
            $merc_id = $data['merc_id'];
            $trm_no = $data['trm_no'];
            $op_sys = $data['op_sys'];
            $opr_id = $data['opr_id'];
            $trm_typ = $data['trm_typ'];
            $payChannel = $data['payChannel'];

            $aop = new \App\Common\XingPOS\Aop();
            $aop->key = $key;
            $aop->op_sys = $op_sys;//操作系统
            // $aop->character_set = '01';
            //  $aop->latitude = '0';//纬度
            //  $aop->longitude = '0';//精度
            $aop->org_no = $org_no;//机构号
            $aop->merc_id = $merc_id;//商户号
            $aop->trm_no = $trm_no;//设备号
            $aop->opr_id = $opr_id;//操作员
            $aop->trm_typ = $trm_typ;//设备类型，P-智能 POS A- app 扫码 C-PC端  T-台牌扫码
            $aop->trade_no = $out_trade_no;//商户单号
            $aop->txn_time = date('Ymdhis', time());//设备交易时间
            $aop->add_field = 'V1.0.1';
            $aop->version = 'V1.0.1';

            $data = [
                'amount' => number_format($total_amount * 100, 0, '.', ''),
                'total_amount' => number_format($total_amount * 100, 0, '.', ''),
                'payChannel' => $payChannel, //支付渠道
                'authCode' => $code,
            ];

            $request_obj_pay = new  \App\Common\XingPOS\Request\XingPaySaoMaZhiFuShangHuZhuSao();
            $request_obj_pay->setBizContent($data);
            $return = $aop->execute($request_obj_pay);
//            Log::info('新大陆-商户主扫结果');
//            Log::info($return);

            //不成功 系统报错
            if ($return['returnCode'] != "000000") {
                return [
                    'status' => 0,
                    'message' => $return['message'],
                ];
            }

            //交易成功
            if ($return['result'] == "S") {
                return [
                    'status' => 1,
                    'data' => $return,
                    'message' => '交易成功',
                ];
            }

            //用户输入密码
            if ($return['result'] == "A") {
                return [
                    'status' => 2,
                    'message' => '等待用户输入密码',
                ];
            }

            //交易失败
            if ($return['result'] == "F") {
                return [
                    'status' => 3,
                    'message' => '交易失败',
                ];
            }

            //交易失败
            if ($return['result'] == "Z") {
                return [
                    'status' => 3,
                    'message' => '交易失败',
                ];
            }

        } catch (\Exception $exception) {
            return [
                'status' => 0,
                'message' => $exception->getMessage(),
            ];
        }
    }


    //查询订单 0-系统错误 1-成功 2-正在支付 3-失败 4.已经退款
    public function order_query($data)
    {
        try {
            $out_trade_no = $data['out_trade_no'];
            $key = $data['key'];
            $org_no = $data['org_no'];
            $merc_id = $data['merc_id'];
            $trm_no = $data['trm_no'];
            $op_sys = $data['op_sys'];
            $opr_id = $data['opr_id'];
            $trm_typ = $data['trm_typ'];

            $aop = new \App\Common\XingPOS\Aop();
            $aop->key = $key;
            $aop->op_sys = $op_sys;//操作系统
            // $aop->character_set = '01';
            //  $aop->latitude = '0';//纬度
            //  $aop->longitude = '0';//精度
            $aop->org_no = $org_no;//机构号
            $aop->merc_id = $merc_id;//商户号
            $aop->trm_no = $trm_no;//设备号
            $aop->opr_id = $opr_id;//操作员
            $aop->trm_typ = $trm_typ;//设备类型，P-智能 POS A- app 扫码 C-PC端  T-台牌扫码
            $aop->trade_no = $out_trade_no;//商户单号
            $aop->txn_time = date('Ymdhis', time());//设备交易时间
            $aop->add_field = 'V1.0.1';
            $aop->version = 'V1.0.0';


            $data = [
                'qryNo' => $out_trade_no,
            ];

            $request_obj_pay = new  \App\Common\XingPOS\Request\XingPayDingDanChaXun();
            $request_obj_pay->setBizContent($data);
            $return = $aop->execute($request_obj_pay);


            //不成功 系统报错
            if ($return['returnCode'] != "000000") {
                return [
                    'status' => 0,
                    'message' => $return['message'],
                ];
            }
            //交易成功
            if ($return['result'] == "S") {
                return [
                    'status' => 1,
                    'data' => $return,
                    'message' => '交易成功',
                ];
            }

            //用户输入密码
            if ($return['result'] == "A") {
                return [
                    'status' => 2,
                    'message' => '等待用户输入密码',
                ];
            }
            //交易失败
            if ($return['result'] == "F") {
                return [
                    'status' => 3,
                    'message' => '交易失败',
                ];
            }
            //交易失败
            if ($return['result'] == "Z") {
                return [
                    'status' => 3,
                    'message' => '交易失败',
                ];
            }

            //交易撤销
            if ($return['result'] == "D") {
                return [
                    'status' => 3,
                    'message' => '用户交易撤销',
                ];
            }

            return [
                'status' => 0,
                'message' => '其他错误',

            ];


        } catch (\Exception $exception) {
            return [
                'status' => 0,
                'message' => $exception->getMessage(),
            ];
        }
    }


    //退款 0-系统错误 1-成功
    public function refund($data)
    {

        try {
            $out_trade_no = $data['out_trade_no'];
            $trade_no = $data['trade_no'];
            $key = $data['key'];
            $org_no = $data['org_no'];
            $merc_id = $data['merc_id'];
            $trm_no = $data['trm_no'];
            $op_sys = $data['op_sys'];
            $opr_id = $data['opr_id'];
            $trm_typ = $data['trm_typ'];
            $txnAmt = isset($data['txnAmt']) ? $data['txnAmt'] : "";

            $aop = new \App\Common\XingPOS\Aop();
            $aop->key = $key;
            $aop->op_sys = $op_sys;//操作系统
            // $aop->character_set = '01';
            //  $aop->latitude = '0';//纬度
            //  $aop->longitude = '0';//精度
            $aop->org_no = $org_no;//机构号
            $aop->merc_id = $merc_id;//商户号
            $aop->trm_no = $trm_no;//设备号
            $aop->opr_id = $opr_id;//操作员
            $aop->trm_typ = $trm_typ;//设备类型，P-智能 POS A- app 扫码 C-PC端  T-台牌扫码
            $aop->trade_no = date('Ymdhis', time());//商户单号 不重复
            $aop->txn_time = date('Ymdhis', time());//设备交易时间
            $aop->add_field = 'V1.0.1';
            $aop->version = 'V1.0.0';


            $data = [
                'orderNo' => $trade_no,
            ];

            if ($txnAmt) {
                $data['txnAmt'] = $txnAmt;
            }

            $request_obj_pay = new  \App\Common\XingPOS\Request\XingPayTuiKuan();
            $request_obj_pay->setBizContent($data);
            $return = $aop->execute($request_obj_pay);

            //不成功 系统报错
            if ($return['returnCode'] != "000000") {
                return [
                    'status' => 0,
                    'message' => $return['message'],
                ];
            }
            //退款成功
            if ($return['result'] == "S") {
                return [
                    'status' => 1,
                    'data' => $return,
                    'message' => '退款成功',
                ];
            }

            //退款失败
            if ($return['result'] == "F") {
                return [
                    'status' => 0,
                    'message' => '退款失败',
                ];
            }
            //退款失败
            if ($return['result'] == "Z") {
                return [
                    'status' => 0,
                    'message' => '退款失败',
                ];
            }


            return [
                'status' => 0,
                'message' => '其他错误',

            ];

        } catch (\Exception $exception) {
            return [
                'status' => 0,
                'message' => $exception->getMessage(),
            ];
        }
    }


    //退款查询 0-系统错误 1-成功 2-正在退款 3-失败
    public function refund_query($data)
    {


    }


    //生成动态二维码-公共
    public function send_qr($data)
    {
        try {
            $out_trade_no = $data['out_trade_no'];
            $total_amount = $data['total_amount'];
            $remark = $data['remark'];
            $device_id = $data['device_id'];
            $shop_name = $data['shop_name'];
            $key = $data['key'];
            $org_no = $data['org_no'];
            $merc_id = $data['merc_id'];
            $trm_no = $data['trm_no'];
            $op_sys = $data['op_sys'];
            $opr_id = $data['opr_id'];
            $trm_typ = $data['trm_typ'];
            $payChannel = $data['payChannel'];

            $aop = new \App\Common\XingPOS\Aop();
            $aop->key = $key;
            $aop->op_sys = $op_sys;//操作系统
            // $aop->character_set = '01';
            //  $aop->latitude = '0';//纬度
            //  $aop->longitude = '0';//精度
            $aop->org_no = $org_no;//机构号
            $aop->merc_id = $merc_id;//商户号
            $aop->trm_no = $trm_no;//设备号
            $aop->opr_id = $opr_id;//操作员
            $aop->trm_typ = $trm_typ;//设备类型，P-智能 POS A- app 扫码 C-PC端  T-台牌扫码
            $aop->trade_no = $out_trade_no;//商户单号
            $aop->txn_time = date('Ymdhis', time());//设备交易时间
            $aop->add_field = 'V1.0.1';
            $aop->version = 'V1.0.0';

            $data = [
                'amount' => number_format($total_amount * 100, 0, '.', ''),
                'total_amount' => number_format($total_amount * 100, 0, '.', ''),
                'payChannel' => $payChannel,
            ];
            $request_obj_pay = new  \App\Common\XingPOS\Request\XingPaySaoMaZhiFuKeHuZhuSao();
            $request_obj_pay->setBizContent($data);
            $return = $aop->execute($request_obj_pay);
//            Log::info('新大陆-客户主扫结果');
//            Log::info($return);

            //不成功 系统报错
            if ($return['returnCode'] != "000000") {
                return [
                    'status' => 0,
                    'message' => $return['message'],
                ];
            }

            //生成成功
            if ($return['result'] == "S") {
                return [
                    'status' => 1,
                    'message' => '返回成功',
                    'code_url' => $return['payCode'],
                    'data' => $return
                ];
            }

            //生成失败
            if ($return['result'] == "F") {
                return [
                    'status' => 0,
                    'data' => $return,
                    'message' => '生成失败',
                ];
            }

            //未知失败
            if ($return['result'] == "Z") {
                return [
                    'status' => 0,
                    'data' => $return,
                    'message' => '生成失败',
                ];
            }

        } catch (\Exception $exception) {
            return [
                'status' => 0,
                'message' => $exception->getMessage(),
            ];
        }

    }


    //静态码提交-公共 0  1 他们的通道
    public function qr_submit($data)
    {
        try {
            $out_trade_no = $data['out_trade_no'];
            $total_amount = $data['total_amount'];
            $remark = $data['remark'];
            $device_id = $data['device_id'];
            $shop_name = $data['shop_name'];
            $key = $data['key'];
            $org_no = $data['org_no'];
            $merc_id = $data['merc_id'];
            $trm_no = $data['trm_no'];
            $op_sys = $data['op_sys'];
            $opr_id = $data['opr_id'];
            $trm_typ = $data['trm_typ'];
            $payChannel = $data['payChannel'];
            $paysuccurl = $data['paysuccurl'];
            $data = [
                'opsys' => $op_sys,
                'characterset' => '00',
                'orgno' => $org_no,
                'mercid' => $merc_id,
                'trmno' => $trm_no,
                'tradeno' => $out_trade_no,
                'trmtyp' => $trm_typ,
                'txntime' => date('Ymdhis', time()),//设备交易时间
                'signtype' => 'MD5',
                'version' => 'V1.0.0',
                'amount' => number_format($total_amount * 100, 0, '.', ''),
                'total_amount' => number_format($total_amount * 100, 0, '.', ''),
                'paychannel' => $payChannel,
                'paysuccurl' => $paysuccurl

            ];
            ksort($data);
            $stringToBeSigned = "";
            $i = 0;
            foreach ($data as $k => $v) {
                $stringToBeSigned .= $v;
                $i++;
            }
            $url = 'https://gateway.starpos.com.cn/sysmng/bhpspos4/5533020.do';
            unset ($k, $v);
            $stringToBeSigned = $stringToBeSigned . $key;
            $data['signvalue'] = md5($stringToBeSigned);
            $i1 = 0;
            $stringToBeSigned1 = '';
            foreach ($data as $k => $v) {
                // 转换成目标字符集
                $v = $this->characet($v, $this->postCharset);

                if ($i1 == 0) {
                    $stringToBeSigned1 .= "$k" . "=" . "$v";
                } else {
                    $stringToBeSigned1 .= "&" . "$k" . "=" . "$v";
                }
                $i1++;
            }

            unset ($k, $v);
            $url = $url . '?' . $stringToBeSigned1;

            return [
                'status' => 1,
                'message' => '返回成功',
                'data' => [
                    'url' => $url,
                ]
            ];
        } catch (\Exception $exception) {
            return [
                'status' => 0,
                'message' => $exception->getMessage(),
            ];
        }
    }


    //静态码提交-微信自己通道
    public function qr_submit_appid($data)
    {
        try {
            $out_trade_no = $data['out_trade_no'];
            $total_amount = $data['total_amount'];
            $remark = $data['remark'];
            $device_id = $data['device_id'];
            $shop_name = $data['shop_name'];
            $key = $data['key'];
            $org_no = $data['org_no'];
            $merc_id = $data['merc_id'];
            $trm_no = $data['trm_no'];
            $op_sys = $data['op_sys'];
            $opr_id = $data['opr_id'];
            $trm_typ = $data['trm_typ'];
            $payChannel = $data['payChannel'];
            $open_id = $data['open_id'];
            $wx_appid = $data['wx_appid'];

            $aop = new \App\Common\XingPOS\Aop();
            $aop->key = $key;
            $aop->op_sys = $op_sys;//操作系统
            $aop->org_no = $org_no;//机构号
            $aop->merc_id = $merc_id;//商户号
            $aop->trm_no = $trm_no;//设备号
            $aop->opr_id = $opr_id;//操作员
            $aop->trm_typ = $trm_typ;//设备类型，P-智能 POS A- app 扫码 C-PC端  T-台牌扫码
            $aop->trade_no = $out_trade_no;//商户单号
            $aop->txn_time = date('Ymdhis', time());//设备交易时间
            $aop->add_field = 'V1.0.1';
            $aop->version = 'V1.0.0';

            $data = [
                'amount' => number_format($total_amount * 100, 0, '.', ''),
                'total_amount' => number_format($total_amount * 100, 0, '.', ''),
                'openid' => $open_id,
                'txnappid' => $wx_appid,
                'selOrderNo' => $out_trade_no,
            ];

            $request_obj_pay = new  \App\Common\XingPOS\Request\XingPayWeiXinGongZhongHaoZhiFu();
            $request_obj_pay->setBizContent($data);
            $return = $aop->execute($request_obj_pay);
//            Log::info('新大陆-微信公众号支付结果');
//            Log::info($return);

            //不成功 系统报错
            if ($return['returnCode'] != "000000") {
                return [
                    'status' => 0,
                    'message' => $return['message'],
                ];
            }

            return [
                'status' => 1,
                'data' => $return,
                'message' => '返回成功',
            ];
        } catch (\Exception $exception) {
            return [
                'status' => 0,
                'message' => $exception->getMessage(),
            ];
        }
    }


    //静态码提交-支付宝自己通道
    public function qr_submit_fwc($data)
    {
        try {
            $out_trade_no = $data['out_trade_no'];
            $total_amount = $data['total_amount'];
            $remark = $data['remark'];
            $device_id = $data['device_id'];
            $shop_name = $data['shop_name'];
            $key = $data['key'];
            $org_no = $data['org_no'];
            $merc_id = $data['merc_id'];
            $trm_no = $data['trm_no'];
            $op_sys = $data['op_sys'];
            $opr_id = $data['opr_id'];
            $trm_typ = $data['trm_typ'];
            $payChannel = $data['payChannel'];
            $open_id = $data['open_id'];
            $wx_appid = $data['wx_appid'];

            $aop = new \App\Common\XingPOS\Aop();
            $aop->key = $key;
            $aop->op_sys = $op_sys;//操作系统
            $aop->org_no = $org_no;//机构号
            $aop->merc_id = $merc_id;//商户号
            $aop->trm_no = $trm_no;//设备号
            $aop->opr_id = $opr_id;//操作员
            $aop->trm_typ = $trm_typ;//设备类型，P-智能 POS A- app 扫码 C-PC端  T-台牌扫码
            $aop->trade_no = $out_trade_no;//商户单号
            $aop->txn_time = date('Ymdhis', time());//设备交易时间
            $aop->add_field = 'V1.0.1';
            $aop->version = 'V1.0.0';

            $data = [
                'amount' => number_format($total_amount * 100, 0, '.', ''),
                'total_amount' => number_format($total_amount * 100, 0, '.', ''),
                'ali_user_id' => $open_id,
            ];

            $request_obj_pay = new  \App\Common\XingPOS\Request\XingPayFwcZhiFu();
            $request_obj_pay->setBizContent($data);
            $return = $aop->executePay($request_obj_pay);

            //不成功 系统报错
            if ($return['returnCode'] != "000000") {
                return [
                    'status' => 0,
                    'message' => $return['message'],
                ];
            }

            return [
                'status' => 1,
                'data' => $return,
                'message' => '返回成功',
            ];


        } catch (\Exception $exception) {
            return [
                'status' => 0,
                'message' => $exception->getMessage(),
            ];
        }
    }

    
    //获取微信刷脸凭证
    public function get_sdk_face_auth($data)
    {
        try {
            $out_trade_no = $data['out_trade_no'];
            $device_id = $data['device_id'];
            $key = $data['key'];
            $org_no = $data['org_no'];
            $merc_id = $data['merc_id'];
            $trm_no = $data['trm_no'];
            $op_sys = $data['op_sys'];
            $opr_id = $data['opr_id'];
            $trm_typ = $data['trm_typ'];
            $wx_sub_appid = $data['wx_sub_appid'];
            $raw_data = $data['raw_data'];
            $store_id = $data['store_id'] ?? '';
            $store_name = $data['store_name'] ?? '';

            $aop = new \App\Common\XingPOS\Aop();
            $aop->key = $key; //机构密钥
            $aop->op_sys = $op_sys; //操作系统
            $aop->org_no = $org_no; //机构号
            $aop->merc_id = $merc_id; //商户号
            $aop->trm_no = $trm_no; //设备号
            $aop->opr_id = $opr_id; //操作员
            $aop->trm_typ = $trm_typ; //设备类型，P-智能POS A-app扫码  C-PC端  T-台牌扫码
            $aop->trade_no = $out_trade_no; //商户单号
            $aop->txn_time = date('Ymdhis', time()); //设备交易时间
            $aop->add_field = 'V1.0.0';
            $aop->version = 'V1.0.0';

            $face_data = [
                'wxSubAppid' => $wx_sub_appid, //微信公众号appid
                'deviceid' => $device_id, //终端设备编号
                'rawData' => $raw_data, //初始化数据
                'stoeId' => $store_id, //商店编号
                'stoeNm' => $store_name, //商店名称
            ];
//            Log::info('新大陆-获取微信刷脸调用凭证-入参');
//            Log::info($face_data);
            $request_obj_pay = new XingStoreHuQuWeiXinShuaLianPingZheng();
            $request_obj_pay->setBizContent($face_data);
            $return = $aop->execute($request_obj_pay);
//            Log::info($return);

            //不成功 系统报错
            if ($return['returnCode'] != "000000") {
                return [
                    'status' => '0',
                    'message' => $return['message'],
                ];
            }

            //交易成功
            if ($return['authInfo']) { //SDK调用凭证
                return [
                    'status' => '1',
                    'message' => '成功',
                    'data' => $return,
                ];
            } else {
                return [
                    'status' => '0',
                    'message' => '失败',
                    'data' => $return,
                ];
            }
        } catch (\Exception $exception) {
            return [
                'status' => '-1',
                'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine(),
            ];
        }
    }

}
