<?php
/**
 * Created by PhpStorm.
 * User: daimingkang
 * Date: 2019/2/21
 * Time: 3:47 PM
 */

namespace App\Api\Controllers\Weixin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MiniController extends BaseController
{
    public $appid = 'wx04f64c0daf5c080c ';//appid
    public $secret = '5001fe365e1417a48e8da21c9843b715';//秘钥

    public function getOpenid(Request $request)
    {
        try {
            $code = $request->get('code');
            $url = 'https://api.weixin.qq.com/sns/jscode2session?appid=' .
                $this->appid . '&secret=' .
                $this->secret . '&js_code=' . $code .
                '&grant_type=authorization_code';
            $info = $this->request_get($url);
            return [
                'status' => 1,
                'openid' => $info['openid']
            ];
        } catch
        (\Exception $exception) {
            return [
                'status' => 0,
                'message' => $exception->getMessage(),
            ];
        }

    }

    public function request_get($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        //本地忽略校验证书
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $tmpInfo = curl_exec($curl);
        //Log::info('获取用户授权返回--' . $tmpInfo);
        $arr = json_decode($tmpInfo, true);
        return $arr;
    }
}