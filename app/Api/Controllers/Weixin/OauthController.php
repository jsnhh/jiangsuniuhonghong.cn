<?php

namespace App\Api\Controllers\Weixin;


use App\Api\Controllers\Config\PayWaysController;
use App\Models\MemberList;
use App\Models\MemberSetJf;
use App\Models\MemberTpl;
use App\Models\ShoppingOpenId;
use App\Models\Store;
use EasyWeChat\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OauthController extends BaseController
{

    //
    public function oauth(Request $request)
    {
        $state = $this->decode($request->get('state'));
        $options = $this->Options($state['config_id']);
        $scope_type = $state['scope_type'];//snsapi_base snsapi_userinfo获取的信息不一样
        $sub_info = $request->get('state');
        $config = [
            'app_id' => $options['app_id'],
            'scope' => $scope_type,
            'oauth' => [
                'scopes' => [$scope_type],
                'response_type' => 'code',
                'callback' => url('/api/weixin/callback?sub_info=' . $sub_info),
            ],

        ];

        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;

        return $oauth->redirect();
    }


    //微信官方支付a
    public function oautha(Request $request)
    {
        $state = $this->decode($request->get('state'));
        $options = $this->OptionsA($state['config_id']);
        $scope_type = $state['scope_type']; //snsapi_base snsapi_userinfo获取的信息不一样
        $sub_info = $request->get('state');
        $config = [
            'app_id' => $options['app_id'],
            'scope' => $scope_type,
            'oauth' => [
                'scopes' => [$scope_type],
                'response_type' => 'code',
                'callback' => url('/api/weixin/callbacka?sub_info=' . $sub_info),
            ],
        ];

        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;

        return $oauth->redirect();
    }


    public function callback(Request $request)
    {
        $sub_info = $request->get('sub_info');
        $sub_info = $this->decode($sub_info);
        $options = $this->Options($sub_info['config_id']);
        $sub_info['code'] = $request->get('code');
        $users = $this->users($sub_info, $options);

        //微信支付
        if ($sub_info['bank_type'] == "weixin") {
            $sub_info['open_id'] = $users->getId();
            $sub_info = $this->encode($sub_info);

            return redirect('/api/weixin/qr_pay_view?sub_info=' . $sub_info);
        }

        //教育缴费 //微信支付
        if ($sub_info['bank_type'] == "school_weixin") {
            $open_id = $users->getId();
            $store_id = $sub_info['store_id'];
            $school_no = $sub_info['school_no'];
            $school_name = $sub_info['school_name'];
            $merchant_id = $sub_info['merchant_id'];
            $obj_ways = new PayWaysController();
            $pay_type = 'weixin';
            $store = Store::where('store_id', $store_id)
                ->select('config_id', 'user_id', 'pid', 'merchant_id', 'store_short_name', 'store_address')
                ->first();
            $StorePayWay = $obj_ways->ways_source($pay_type, $store_id, $store->pid);
            if (!$StorePayWay) {
                $message = "没有找到合适的支付方式";

                return view('errors.page_errors', compact('message'));
            }
            $ways_type = $StorePayWay->ways_type;
            if ($ways_type == 32002) {
                $url = url('/api/easyskpay/weixin/index?open_id=' . $open_id . '&store_id=' . $store_id . '&school_name=' . $school_name . '&school_no=' . $school_no . '&merchant_id=' . $merchant_id);
            }
            if ($ways_type == 21002) {
                $url = url('/api/easypay/weixin/index?open_id=' . $open_id . '&store_id=' . $store_id . '&school_name=' . $school_name . '&school_no=' . $school_no . '&merchant_id=' . $merchant_id);
            }
            return redirect($url);
        }
        //获取商户openid
        if ($sub_info['bank_type'] == "getOpenid") {
            $open_id = $users->getId();
            $store_id = $sub_info['store_id'];
            $store = Store::where('store_id', $store_id)->first();
            if ($store) {
                Store::where('store_id', $store_id)->update(['openid' => $open_id]);
                $message = "获取openid成功";
                return view('success.success', compact('message'));
            } else {
                $message = "获取openid失败";
                return view('errors.page_errors', compact('message'));
            }
        }
        //获取商户openid
        if ($sub_info['bank_type'] == "getShoppingOpenid") {
            $open_id = $users->getId();
            $res = ShoppingOpenId::create(['open_id' => $open_id]);
            if ($res) {
                $message = "获取openid成功";
                return view('success.success', compact('message'));
            } else {
                $message = "获取openid失败";
                return view('errors.page_errors', compact('message'));
            }
        }

    }


    //微信官方a 回调
    public function callbacka(Request $request)
    {
        $sub_info = $request->get('sub_info');
        $sub_info = $this->decode($sub_info);
        $options = $this->OptionsA($sub_info['config_id']);
        $sub_info['code'] = $request->get('code');
        $users = $this->users($sub_info, $options);

        //微信支付
        if ($sub_info['bank_type'] == "weixin") {
            $sub_info['open_id'] = $users->getId();
            $sub_info = $this->encode($sub_info);

            return redirect('/api/weixin/qr_pay_view_a?sub_info=' . $sub_info);
        }

        //教育缴费 //微信支付
        if ($sub_info['bank_type'] == "school_weixin") {
            $open_id = $users->getId();
            $store_id = $sub_info['store_id'];
            $stu_grades_no = $sub_info['stu_grades_no'];
            $stu_class_no = $sub_info['stu_class_no'];
            $school_name = $sub_info['school_name'];

            $url = url('/school/payeducationa?open_id=' . $open_id . '&store_id=' . $store_id . '&school_name=' . $school_name . '&stu_grades_no=' . $stu_grades_no . '&stu_class_no=' . $stu_class_no);

            return redirect($url);
        }

    }


    //微信支付视图页面
    public function qr_pay_view(Request $request)
    {
        $sub_info = $request->get('sub_info');
        $sub_info = $this->decode($sub_info);
        $store_id = $sub_info['store_id'];//门店id
        $merchant_id = $sub_info['merchant_id'];//收银员id
        $store_name = $sub_info['store_name'];
        $store_address = $sub_info['store_address'];
        $open_id = $sub_info['open_id'];
        $type = $sub_info['bank_type'];
        $is_profit_sharing = $sub_info['is_profit_sharing'] ?? false;
        $data = [
            //'oem_name' => $AppOem->name,
            'store_id' => $store_id,
            'store_name' => $store_name,
            'store_address' => $store_address,
            'open_id' => $open_id,
            'merchant_id' => $merchant_id,
        ];
        if ($is_profit_sharing) $data['is_profit_sharing'] = $is_profit_sharing; //是否开启分账

        //查询是否有开启会员
        $is_member = 0;
        $MemberTpl = MemberTpl::where('store_id', $store_id)
            ->select('tpl_status')
            ->first();
        if ($MemberTpl && $MemberTpl->tpl_status == 1) {
            $is_member = 1;
        }

        //如果是会员
        if ($is_member) {
            //判断是否是会员
            $MemberList = MemberList::where('store_id', $store_id)
                ->where('wx_openid', $open_id)
                ->select('mb_jf', 'mb_money', 'mb_id', 'mb_phone')
                ->first();
            $data['mb_jf'] = "";
            $data['mb_id'] = "";
            $data['mb_money'] = "";
            $data['dk_money'] = "";
            $data['dk_jf'] = "0";
            $data['ways_source'] = "weixin";
            $data['is_info'] = "0";

            if ($MemberList) {
                if ($MemberList->mb_phone) {
                    $data['is_info'] = "1";
                }

                $data['mb_jf'] = $MemberList->mb_jf;
                $data['mb_id'] = $MemberList->mb_id;
                $data['mb_money'] = $MemberList->mb_money;

                //判断是否有积分抵扣
                $MemberSetJf = MemberSetJf::where('store_id', $store_id)
                    ->select('dk_jf_m', 'dk_rmb')
                    ->first();
                if ($MemberSetJf) {
                    //3.用户的积分一共可以抵扣多少钱
                    $data['dk_money'] = ($MemberList->mb_jf / $MemberSetJf->dk_jf_m) * $MemberSetJf->dk_rmb;
                    $data['dk_jf'] = $MemberList->mb_jf;
                }
            }
        }

        if ($type == 'weixin') {
            if ($is_member) {
                $data['ways_type'] = "2000";
                $data['company'] = "weixin";

                return view('member.weixin', compact('data'));
            } else {
                return view('weixin.create_weixin_order', compact('data'));
            }
        }
    }


    //微信官方a 支付页面
    public function qr_pay_view_a(Request $request)
    {
        $sub_info = $request->get('sub_info');
        $sub_info = $this->decode($sub_info);
        $store_id = $sub_info['store_id']; //门店id
        $merchant_id = $sub_info['merchant_id']; //收银员id
        $store_name = $sub_info['store_name'];
        $store_address = $sub_info['store_address'];
        $open_id = $sub_info['open_id'];
        $type = $sub_info['bank_type'];
        $is_profit_sharing = $sub_info['is_profit_sharing'] ?? false;
        $data = [
            //'oem_name' => $AppOem->name,
            'store_id' => $store_id,
            'store_name' => $store_name,
            'store_address' => $store_address,
            'open_id' => $open_id,
            'merchant_id' => $merchant_id,
        ];
        if ($is_profit_sharing) $data['is_profit_sharing'] = $is_profit_sharing; //是否开启分账

        //查询是否有开启会员
        $is_member = 0;
        $MemberTpl = MemberTpl::where('store_id', $store_id)
            ->select('tpl_status')
            ->first();
        if ($MemberTpl && $MemberTpl->tpl_status == 1) {
            $is_member = 1;
        }

        //如果是会员
        if ($is_member) {
            //判断是否是会员
            $MemberList = MemberList::where('store_id', $store_id)
                ->where('wx_openid', $open_id)
                ->select('mb_jf', 'mb_money', 'mb_id', 'mb_phone')
                ->first();
            $data['mb_jf'] = "";
            $data['mb_id'] = "";
            $data['mb_money'] = "";
            $data['dk_money'] = "";
            $data['dk_jf'] = "0";
            $data['ways_source'] = "weixin";
            $data['is_info'] = "0";

            if ($MemberList) {
                if ($MemberList->mb_phone) {
                    $data['is_info'] = "1";
                }

                $data['mb_jf'] = $MemberList->mb_jf;
                $data['mb_id'] = $MemberList->mb_id;
                $data['mb_money'] = $MemberList->mb_money;

                //判断是否有积分抵扣
                $MemberSetJf = MemberSetJf::where('store_id', $store_id)
                    ->select('dk_jf_m', 'dk_rmb')
                    ->first();
                if ($MemberSetJf) {
                    //3.用户的积分一共可以抵扣多少钱
                    $data['dk_money'] = ($MemberList->mb_jf / $MemberSetJf->dk_jf_m) * $MemberSetJf->dk_rmb;
                    $data['dk_jf'] = $MemberList->mb_jf;
                }
            }
        }

        if ($type == 'weixin') {
            if ($is_member) {
                $data['ways_type'] = "4000";
                $data['company'] = "weixina";

                return view('member.weixina', compact('data'));
            } else {
                return view('weixina.create_weixin_order', compact('data'));
            }
        }
    }


    //微信充值
    public function member_cz_pay_view(Request $request)
    {
        return view('member.cz');
    }


    //学校发起支付页面
    public function paydetails()
    {
        return view('school.paydetails');
    }


    //获取users
    public function users($sub_info, $options)
    {
        $code = $sub_info['code'];
        $config = [
            'app_id' => $options['app_id'],
            "secret" => $options['app_secret'],
            "code" => $code,
            "grant_type" => "authorization_code",
        ];
        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;

        // 获取 OAuth 授权结果用户信息
        return $user = $oauth->user();
    }


}
