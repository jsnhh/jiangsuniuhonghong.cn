<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/8/17
 * Time: 11:11
 */
namespace App\Api\Controllers\Weixin;


use App\Api\Controllers\Config\WeixinConfigController;
use EasyWeChat\Factory;
use Illuminate\Http\Request;

class WxShopController extends \App\Api\Controllers\BaseController
{

    //curl请求
    function https_request($url, $data = '', $type = 'get', $res = 'json')
    {
        //1.初始化curl
        $curl = curl_init();
        //2.设置curl的参数
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SAFE_UPLOAD, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        if ($type == 'post'){
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        }
        //3.采集
        $output = curl_exec($curl);
        //var_dump(curl_getinfo($curl));
        //4.关闭
        curl_close($curl);
        if ($res == 'json') {
            return json_decode($output, true);
        }
    }


    //获取微信access_token
    public function get_access_token($config_id = '1234')
    {
        $config = new WeixinConfigController();
        $config_obj = $config->weixin_config_obj($config_id);
        $config = [
            'app_id' => $config_obj->app_id,
            'secret' => $config_obj->app_secret
        ];
        $app = Factory::officialAccount($config);
        $access_token = $app->access_token->getToken();

        return $access_token;
    }


    //获取微信access_token
//    function get_access_token($data)
//    {
//        $appid = $data['appid'];
//        $secret = $data['secret'];
//        $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$appid."&secret=".$secret;
//        $res = $this->https_request($url, 'get', 'json');
//        $access_token = $res['access_token'];
//
//        return $access_token;
//    }


    //上传图片到微信（该方法只能获取到图片的URL。上传的图片限制文件大小限制1MB，仅支持JPG、PNG格式）
    public function mediaUploadImg(Request $request)
    {
        $config_id = $request->get('config_id', '');
        $filename = $request->get('filename', ''); //文件名同目录下 其他目录自己指定

        try {
            $access_token = $this->get_access_token($config_id);
            $url = "https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=".$access_token;
            $requestData = array('media' => '@'. $filename);
            $res = $this->https_request($url, $requestData, 'post');
//        print_r($res);

            if (isset($res['errcode']) && $res['errcode'] != 0) {
                return json_encode([
                    'status' => '2',
                    'message' => $res['errmsg'],
                    'errcode' => $res['errcode']
                ]);
            } else {
                return json_encode([
                    'status' => '1',
                    'message' => '请求成功',
                    'data' => $res['url']
                ]);
            }
        } catch (\Exception $ex) {
            return [
                'status' => '-1',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ];
        }

    }





}
