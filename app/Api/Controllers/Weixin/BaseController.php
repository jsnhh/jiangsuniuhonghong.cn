<?php
namespace App\Api\Controllers\Weixin;

use App\Api\Controllers\Config\WeixinConfigController;
use App\Models\WeixinConfig;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use WechatPay\GuzzleMiddleware\Util\PemUtil;
use WechatPay\GuzzleMiddleware\Util\MediaUtil;
use WechatPay\GuzzleMiddleware\WechatPayMiddleware;

class BaseController extends \App\Api\Controllers\BaseController
{
    // 表单提交字符集编码
    public $postCharset = "UTF-8";
    private $fileCharset = "UTF-8";

    //商户api私钥证书路径
    protected $privateKeyPath;
    //商户号
    protected $merchantId;
    //商户api证书序列号
    protected $serial_no;
    //APIv3密钥，需要登录微信支付平台管理进行设置
    protected $key_v3;

    public function __construct()
    {
        $this->privateKeyPath       = config('api.privateKeyPath');
        $this->merchantId           = config('api.merchantId');
        $this->serial_no            = config('api.serial_no');
        $this->key_v3               = config('api.key_v3');
    }


    /**
     * 生成签名
     * @param $data
     * @param $key
     * @param string $sign_type 是否需要补signtype
     * @return string
     */
    public function MakeSign($data, $key, $sign_type = 'MD5')
    {
        //签名步骤一：按字典序排序参数
        $string = $this->getSignContent($data);
        //签名步骤二：在string后加入KEY
        $string = $string . "&key=" . $key;
        //签名步骤三：MD5加密
        if ($sign_type == "MD5") {
            $string = md5($string);
        } else if ($sign_type == "HMAC-SHA256") {
            $string = hash_hmac("sha256", $string, $key);
        } else {
            die('类型不支持');
        }
        //签名步骤四：所有字符转为大写
        $result = strtoupper($string);

        return $result;
    }


    /**
     * 以post方式提交xml到对应的接口url
     * @param $config
     * @param string    $xml  需要post的xml数据
     * @param string    $url  url
     * @param bool $useCert 是否需要证书，默认不需要
     * @param int $second   url执行超时时间，默认30s
     * @return mixed
     */
    public static function postXmlCurl($config, $xml, $url, $useCert = false, $second = 30)
    {
        $ch = curl_init();
        $curlVersion = curl_version();
        $ua = "WXPaySDK/3.0.9 (" . PHP_OS . ") PHP/" . PHP_VERSION . " CURL/" . $curlVersion['version'] . " "
            . $config['mch_id'];

        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);

        $proxyHost = "0.0.0.0";
        $proxyPort = 0;

        //如果有配置代理这里就设置代理
        if ($proxyHost != "0.0.0.0" && $proxyPort != 0) {
            curl_setopt($ch, CURLOPT_PROXY, $proxyHost);
            curl_setopt($ch, CURLOPT_PROXYPORT, $proxyPort);
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);//严格校验
        curl_setopt($ch, CURLOPT_USERAGENT, $ua);
        //设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        if ($useCert == true) {
            //设置证书
            //使用证书：cert 与 key 分别属于两个.pem文件
            //证书文件请放入服务器的非web目录下
            $sslCertPath = $config['sslCertPath'];
            $sslKeyPath = $config['sslKeyPath'];
            curl_setopt($ch, CURLOPT_SSLCERTTYPE, 'PEM');
            curl_setopt($ch, CURLOPT_SSLCERT, $sslCertPath);
            curl_setopt($ch, CURLOPT_SSLKEYTYPE, 'PEM');
            curl_setopt($ch, CURLOPT_SSLKEY, $sslKeyPath);
        }
        //post提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        //运行curl
        $data = curl_exec($ch);
        //返回结果
        if ($data) {
            curl_close($ch);
            return $data;
        } else {
            $error = curl_errno($ch);
            curl_close($ch);
            die($error);
        }
    }


    /**
     * 输出xml字符
     * @throws
     **/
    public function ToXml($data)
    {
        $xml = "<xml>";
        foreach ($data as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";
        return $xml;
    }


    //参数拼接
    public function getSignContent($params)
    {
        ksort($params);
        $stringToBeSigned = "";
        $i = 0;
        foreach ($params as $k => $v) {
            if (false === $this->checkEmpty($v) && "@" != substr($v, 0, 1)) {

                // 转换成目标字符集
                $v = $this->characet($v, $this->postCharset);

                if ($i == 0) {
                    $stringToBeSigned .= "$k" . "=" . "$v";
                } else {
                    $stringToBeSigned .= "&" . "$k" . "=" . "$v";
                }
                $i++;
            }
        }

        unset ($k, $v);
        return $stringToBeSigned;
    }


    /**
     * 转换字符集编码
     * @param $data
     * @param $targetCharset
     * @return string
     */
    function characet($data, $targetCharset)
    {
        if (!empty($data)) {
            $fileType = $this->fileCharset;
            if (strcasecmp($fileType, $targetCharset) != 0) {
                $data = mb_convert_encoding($data, $targetCharset);
                //$data = iconv($fileType, $targetCharset.'//IGNORE', $data);
            }
        }

        return $data;
    }


    /**
     * 校验$value是否非空
     *  if not set ,return true;
     *    if is null , return true;
     **/
    protected function checkEmpty($value)
    {
        if (!isset($value))
            return true;
        if ($value === null)
            return true;
        if (trim($value) === "")
            return true;

        return false;
    }


    static function xml_to_array($xml)
    {
        if (!$xml) {
            die("xml数据异常！");
        }
        //将XML转为array
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $values = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);

        return $values;
    }


    public function Options($config_id = '1234')
    {
        $config = new WeixinConfigController();
        $WeixinConfig = $config->weixin_config_obj($config_id);

        $options = [
            'app_id' => $WeixinConfig->app_id,
            'app_secret' => $WeixinConfig->app_secret,
            'payment' => [
                'merchant_id' => $WeixinConfig->wx_merchant_id,
                'key' => $WeixinConfig->key,
                'cert_path' => public_path() . $WeixinConfig->cert_path, // XXX: 绝对路径！！！！
                'key_path' => public_path() . $WeixinConfig->key_path,      // XXX: 绝对路径！！！！
                'notify_url' => $WeixinConfig->notify_url,       // 你也可以在下单时单独设置来想覆盖它
            ],
        ];

        return $options;
    }


    public function OptionsA($config_id = '1234')
    {
        $config = new WeixinConfigController();
        $WeixinConfig = $config->weixina_config_obj($config_id);

        $options = [
            'app_id' => $WeixinConfig->app_id,
            'app_secret' => $WeixinConfig->app_secret,
            'payment' => [
                'merchant_id' => $WeixinConfig->wx_merchant_id,
                'key' => $WeixinConfig->key,
                'cert_path' => public_path() . $WeixinConfig->cert_path, // XXX: 绝对路径！！！！
                'key_path' => public_path() . $WeixinConfig->key_path,      // XXX: 绝对路径！！！！
                'notify_url' => $WeixinConfig->notify_url,       // 你也可以在下单时单独设置来想覆盖它
            ],
        ];

        return $options;
    }


    /**
     * 通讯数据加密
     */
    public static function encode($data)
    {
        return $data = base64_encode(json_encode((array)$data));
    }


    /**
     * 通讯数据解密
     */
    public static function decode($data)
    {
        return json_decode(base64_decode((string)$data), true);
    }


    //上传图片请求
    public function httpsRequest($url, $data, array $headers = [], $userCert = false, $timeout = 30)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }

        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout); //设置超时
        if ($userCert) {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);//严格校验
            curl_setopt($curl, CURLOPT_SSLCERTTYPE, 'PEM');
            curl_setopt($curl, CURLOPT_SSLKEYTYPE, 'PEM');
            list($sslCertPath, $sslKeyPath) = [$data['sslCertPath'], $data['sslKeyPath']];
            curl_setopt($curl, CURLOPT_SSLCERT, $sslCertPath);
            curl_setopt($curl, CURLOPT_SSLKEY, $sslKeyPath);
        } else {
            if (substr($url, 0, 5) == 'https') {
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 信任任何证书
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); // 检查证书中是否设置域名
            }
        }

        if (!empty($headers)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            // curl_setopt($curl, CURLINFO_HEADER_OUT, true); //TRUE 时追踪句柄的请求字符串，从 PHP 5.1.3 开始可用。这个很关键，就是允许你查看请求header
            // $headers = curl_getinfo($curl, CURLINFO_HEADER_OUT); //官方文档描述是“发送请求的字符串”，其实就是请求的header。这个就是直接查看请求header，因为上面允许查看
        }

        curl_setopt($curl, CURLOPT_HEADER, true); // 是否需要响应 header
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE); // 获得响应结果里的：头大小
        $response_header = substr($output, 0, $header_size); // 根据头大小去获取头信息内容
        $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE); // 获取响应状态码
        $response_body = substr($output, $header_size);
        $error = curl_error($curl);
        curl_close($curl);

        return $response_body;
    }


    //region 敏感字段加密start
    public function getEncrypt($str, $public_key)
    {
        //$str是待加密字符串
        $encrypted = '';
        openssl_public_encrypt($str, $encrypted, $public_key);

        $sign = base64_encode($encrypted); //base64编码

        return $sign;
    }


    //微信支付-服务商分账-错误码列表
    public function wx_sharing_error($err_code)
    {
        switch ($err_code)
        {
            case 'SYSTEMERROR': return '接口返回错误,系统超时,请使用相同参数再次调用API'; break;
            case 'PARAM_ERROR': return '参数错误,请求参数未按指引进行填写,请求参数错误，请重新检查再调用API'; break;
            case 'INVALID_REQUEST': return '请求不合法,参数中APPID或 MCHID不存在等,请检查请求参数'; break;
            case 'OPENID_MISMATCH': return 'Openid错误,Openid与Appid不匹配,请检查Openid是否正确'; break;
            case 'FREQUENCY_LIMITED': return '频率限制,请求过多被频率限制,该笔请求未受理，请降低频率后原单重试'; break;
            case 'NOAUTH': return '无分账权限,未开通分账权限,请先开通分账'; break;
            case 'USER_NOT_EXIST': return '分账接收方不存在,分账接收方不存在,请确认分账接收方类型或者账号无误后重试'; break;
            case 'ACCOUNTERROR': return '分账接收方账户不存在,账户未开通,账户未开通，请接收方商户在商户平台点击“充值”创建账户'; break;
            default: return 'unknown sharing err code'; break;
        }
    }

    /**
     * 加密敏感字符串
     * @param $str string 需要加密的字符串
     * @param $public_key_path string 微信支付平台证书
     * @return string
     */
    public function getEncrypts($str, $public_key_path){
        //$str是待加密字符串
        $public_key = file_get_contents($public_key_path);
        $encrypted = '';
        if (openssl_public_encrypt($str,$encrypted,$public_key,OPENSSL_PKCS1_OAEP_PADDING)) {
            //base64编码
            $sign = base64_encode($encrypted);
        } else {
            throw new Exception('encrypt failed');
        }
        return $sign;
    }

    public function getEncryptsCommon($str, $public_key){
        //$str是待加密字符串
        // $public_key = file_get_contents($public_key_path);
        $encrypted = '';
        if (openssl_public_encrypt($str,$encrypted,$public_key,OPENSSL_PKCS1_OAEP_PADDING)) {
            //base64编码
            $sign = base64_encode($encrypted);
        } else {
            throw new Exception('encrypt failed');
        }
        return $sign;
    }

    //生成随机字符串
    public function nonce_str($length = 32){
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $str ="";
        for ( $i = 0; $i < $length; $i++ )  {
            $str.= substr($chars, mt_rand(0, strlen($chars)-1), 1);
        }
        return $str;
    }

    /**
     * 读取商户api私钥
     * @param $apiclient_key | 商户api证书私钥文件
     * @return false|resource
     */
    public function getPrivateKey($apiclient_key){
        return openssl_get_privatekey(file_get_contents($apiclient_key));
    }

    //签名
    public function sign($url,$http_method,$timestamp,$nonce,$body,$mch_private_key,$merchant_id,$serial_no){
        $url_parts=parse_url($url);
        $canonical_url=($url_parts['path'].(!empty($url_parts['query'])?"?${url_parts['query']}":""));
        $message=
            $http_method . "\n".
            $canonical_url . "\n".
            $timestamp . "\n".
            $nonce . "\n".
            $body . "\n";
        openssl_sign($message,$raw_sign,$mch_private_key,'sha256WithRSAEncryption');
        $sign=base64_encode($raw_sign);
        $schema='WECHATPAY2-SHA256-RSA2048';
        $token=sprintf(
            'mchid="%s",nonce_str="%s",signature="%s",timestamp="%d",serial_no="%s"',
            $merchant_id,
            $nonce,
            $sign,
            $timestamp,
            $serial_no
        );
        return $token;
    }

    //curl提交
    public function curl_post($url,$data=[],$header,$method='POST'){
        $curl=curl_init();
        curl_setopt($curl,CURLOPT_URL,$url);
        curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
        curl_setopt($curl,CURLOPT_HEADER,false);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,false);
        if($method=="POST"){
            curl_setopt($curl,CURLOPT_POST,TRUE);
            curl_setopt($curl,CURLOPT_POSTFIELDS,$data);
        }
        $result=curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    // 获取微信支付平台证书
    public function gethengshu($input){
        $url = "https://api.mch.weixin.qq.com/v3/certificates"; //证书获取地址

        $body               = "";
        $timestamp          = time();                   //时间戳
        $nonce              = $this->nonce_str();       //获取一个随机字符串
        $merchant_id        = $input['mch_id'];         //商户号
        $serial_no          = $input['serial_no'];      //商户证书序列号
        $mch_private_key    = $this->getPrivateKey($input['apiclient_key']); //读取商户api证书私钥

        $sign = $this->sign($url,'GET',$timestamp,$nonce,$body,$mch_private_key,$merchant_id,$serial_no);

        $header=[
            'Authorization:WECHATPAY2-SHA256-RSA2048 '.$sign,
            'Accept:application/json',
            'User-Agent:'.$merchant_id
        ];
        $result=$this->curl($url,'',$header,'GET');
        $result=json_decode($result,true);
        $serial_no=$result['data'][0]['serial_no'];//获取的平台证书序列号
        $encrypt_certificate=$result['data'][0]['encrypt_certificate'];
        $sign_key="eiodlkiomj34ol309fj9oipd89654xcfth37898hui";  //APIv3密钥，商户平台API安全中获取
        $result=$this->decryptToString($encrypt_certificate['associated_data'],$encrypt_certificate['nonce'],$encrypt_certificate['ciphertext'],$sign_key);
        file_put_contents('E:\项目\ycert\cert.pem',$result);//获取的文件临时保存到服务器
        return $serial_no;//返回平台证书序列号
    }

    //解密返回的信息
    public function decryptToString($associatedData,$nonceStr,$ciphertext,$aesKey){
        $ciphertext=\base64_decode($ciphertext);
        if(function_exists('\sodium_crypto_aead_aes256gcm_is_available')&& \sodium_crypto_aead_aes256gcm_is_available()){
            return \sodium_crypto_aead_aes256gcm_decrypt($ciphertext,$associatedData,$nonceStr,$aesKey);
        }
        if(PHP_VERSION_ID >= 70100 && in_array('aes-256-gcm', \openssl_get_cipher_methods())){
            $ctext=substr($ciphertext,0,-16);
            $authTag=substr($ciphertext,-16);
            return \openssl_decrypt(
                $ctext,
                'aes-256-gcm',
                $aesKey,
                \OPENSSL_RAW_DATA,
                $nonceStr,
                $authTag,
                $associatedData
            );
        }
        throw new \RuntimeException('php7.1');
    }

    //上传证件图片
    public function uploadMedia($path, $merchant_id, $serial_no, $apiclient_key){
        $url = "https://api.mch.weixin.qq.com/v3/merchant/media/upload";//微信的上传地址

        $mch_private_key = $this->getPrivateKey($apiclient_key);//读取商户api证书公钥 getPublicKey()获取方法
        $timestamp = time();//时间戳
        $nonce = $this->nonce_str();//随机字符串
        // $fi = new \finfo(FILEINFO_MIME_TYPE);
        // $mime_type=$fi->file($path);

        $meta=[
            'filename'=>pathinfo($path,2),
            'sha256'=>hash_file('sha256',$path)
        ];
        $sign = $this->sign($url,'POST',$timestamp,$nonce,json_encode($meta),$mch_private_key,$merchant_id,$serial_no);
        // var_dump($sign);die;
        $boundary = uniqid();
        $header=[
            'Content-Type:multipart/form-data;boundary=' . $boundary,
            'Authorization:WECHATPAY2-SHA256-RSA2048 ' . $sign,
            'Accept:application/json',
            'User-Agent:' . $merchant_id
        ];
        $body='--' . $boundary . "\r\n";
        $body.='Content-Disposition:form-data; name="meta"' . "\r\n";
        $body.='Content-Type:application/json' . "\r\n\r\n";
        $body.=json_encode($meta)."\r\n";
        $body.='--' . $boundary . "\r\n";
        $body.='Content-Disposition:form-data;name="file";filename="' . $meta['filename'] . '"' . "\r\n";
        // $body.='Content-Type:' .$mime_type."\r\n\r\n";
        $body.='Content-Type:image/jpg' ."\r\n\r\n";
        $body.=file_get_contents($path)."\r\n";
        $body.='--'.$boundary .'--'."\r\n";
        $result=$this->curl_post($url,$body,$header);
        $result=json_decode($result,true);
        var_dump($result);die;

        return $result['media_id'];//返回微信返回来的图片标识
    }

    /**
     * 构造微信所需client
     * @param $merchant_id string 商户号
     * @param $serial_no string 商户API证书序列号
     * @param $apiclient_key string 商户私钥
     * @param $wechat_key_path string 微信支付平台证书
     * @return Client
     */
    public function get_wechat_client_common($merchant_id, $serial_no, $apiclient_key, $wechat_key_path)
    {
        // 商户私钥
        $apiclient_key     = PemUtil::loadPrivateKey($apiclient_key);
        // 微信支付平台证书
        $wechat_key_path   = PemUtil::loadCertificate($wechat_key_path);
        // 构造WechatPayMiddleware
        $wechatpayMiddleware = WechatPayMiddleware::builder()
            ->withMerchant($merchant_id, $serial_no, $apiclient_key) // 传入商户相关配置
            ->withWechatPay([ $wechat_key_path ]) // 可传入多个微信支付平台证书，参数类型为array
            ->build();

        // 将WechatPayMiddleware添加到Guzzle的HandlerStack中
        $stack = HandlerStack::create();
        $stack->push($wechatpayMiddleware, 'wechatpay');

        // 创建Guzzle HTTP Client时，将HandlerStack传入
        $client = new Client(['handler' => $stack]);

        return $client;
    }

    /**
     * v3上传图片
     */
    public function wechat_upload_media($path, $merchant_id, $serial_no, $apiclient_key, $wechat_key_path)
    {
        // 实例化一个媒体文件流，注意文件后缀名需符合接口要求
        // $media = new MediaUtil('/your/file/path/with.extension');
        $media = new MediaUtil($path);

        $client = $this->get_wechat_client_common($merchant_id, $serial_no, $apiclient_key, $wechat_key_path);

        // 正常使用Guzzle发起API请求
        try {
            // $resp = $client->request('POST', 'https://api.mch.weixin.qq.com/v3/[merchant/media/video_upload|marketing/favor/media/image-upload]', [
            //     'body'    => $media->getStream(),
            //     'headers' => [
            //         'Accept'       => 'application/json',
            //         'content-type' => $media->getContentType(),
            //     ]
            // ]);
            // POST 语法糖
            $resp = $client->post('merchant/media/upload', [
                'body'    => $media->getStream(),
                'headers' => [
                    'Accept'       => 'application/json',
                    'content-type' => $media->getContentType(),
                ]
            ]);
            echo $resp->getStatusCode().' '.$resp->getReasonPhrase()."\n";
            echo $resp->getBody()."\n";
        } catch (Exception $e) {
            echo $e->getMessage()."\n";
            if ($e->hasResponse()) {
                echo $e->getResponse()->getStatusCode().' '.$e->getResponse()->getReasonPhrase()."\n";
                echo $e->getResponse()->getBody();
            }
            return;
        }
    }

}
