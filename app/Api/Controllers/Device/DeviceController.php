<?php
namespace App\Api\Controllers\Device;


use App\Api\Controllers\AliRuyi\RuyiPayController;
use App\Api\Controllers\BaseController;
use App\Api\Controllers\Merchant\StoreController;
use App\Models\AlipayAntStores;
use App\Models\AlipayOperation;
use App\Models\Device;
use App\Models\Merchant;
use App\Models\MerchantDevice;
use App\Models\MerchantStore;
use App\Models\Store;
use App\Models\VConfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use MyBank\Tools;
use TencentCloud\Common\Credential;
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Common\Profile\HttpProfile;
use TencentCloud\Common\Exception\TencentCloudSDKException;
use TencentCloud\Iotcloud\V20180614\IotcloudClient;
use TencentCloud\Iotcloud\V20180614\Models\PublishMessageRequest;

class DeviceController extends BaseController
{
    //添加设备
    public function add(Request $request)
    {
        try {
            $user_merchant = $this->parseToken();
            $store_id = $request->get('store_id');
            $merchant_id = $request->input('merchant_id', "");
            $device_type = $request->get('device_type', '');
            $device_name = $request->get('device_name', '');
            $device_no = $request->get('device_no', '');
            $device_key = $request->get('device_key', '');
            $type = $request->get('type', '');
            $cj = $request->get('cj', '');
            $cj = isset($cj) ? $cj : "";
            $config_id = $user_merchant->config_id;
            $data = $request->except(['token']);

            if (in_array($type, ['p'])) {
                $check_data = [
                    'store_id' => '门店id',
                    //'merchant_id' => '收银员',
                    'device_type' => '设备类型',
                    'device_no' => '设备编号',
                    'device_key' => '设备秘钥',
                    'device_name' => '设备名称'
                ];
            } else{
                $check_data = [
                    'store_id' => '门店id',
//                    'merchant_id' => '收银员',
                    'device_type' => '设备类型',
                    'device_no' => '设备编号',
                    'device_key' => '设备秘钥',
                    'device_name' => '设备名称'
                ];
            }
            $check = $this->check_required($data, $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $merchant_name = "";

            if ($merchant_id == "") {
                $merchant_id = "";
            }

            $store = Store::where('store_id', $store_id)
                ->select('store_name')
                ->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在'
                ]);
            }

            $Device = Device::where('device_no', $device_no)
                ->where('device_type', $device_type)
                // ->where('store_id', $store_id)
                ->first();
//            if ($Device && in_array($type, ['s', 'f'])) {
            if ($Device) {
                return json_encode([
                    'status' => 2,
                    'message' => '设备已经被' . $Device->store_name . '绑定',
                ]);
            }

            $store_name = $store->store_name;

            $VConfig = VConfig::where('config_id', $config_id)
                ->select(
                    'zw_token',
                    'old_zw_token',
                    'zlbz_token',
                    'yly_user_name',
                    'yly_user_id',
                    'yly_api_key'
                )->first();
            if (!$VConfig) {
                $VConfig = VConfig::where('config_id', '1234')
                    ->select(
                        'zw_token',
                        'old_zw_token',
                        'zlbz_token',
                        'yly_user_name',
                        'yly_user_id',
                        'yly_api_key'
                    )->first();
            }
            if (!$VConfig) {
                return json_encode([
                    'status' => 2,
                    'message' => '系统配置-设备配置，参数未设置'
                ]);
            }

            //云喇叭添加-智联
            if ($device_type == 'v_zlbz_1') {
                //强制解绑
                $datadel = [
                    'id' => $device_no,
                    'm' => '4',
                    'token' => $VConfig->zlbz_token
                ];
                $curl = Tools::curl($datadel, 'http://39.106.131.149/bind.php');
                $curl = json_decode($curl, true);
                //绑定
                $datap = [
                    'id' => $device_no,
                    'm' => '1',
                    'uid' => $store_id,
                    'token' => $VConfig->zlbz_token
                ];

                $curl = Tools::curl($datap, 'http://39.106.131.149/bind.php');
                $curl = json_decode($curl, true);
                if ($curl['errcode'] != 0) {
                    return json_encode([
                        'status' => 2,
                        'message' => $curl['errcode'] . '-' . $curl['errmsg']
                    ]);
                }
            }

            //v_zw_1 智网云喇叭
            if ($device_type == 'v_zw_1') {
                if ($VConfig->zw_token) {
                    //1.强制解绑
                    $url = "http://cloudspeaker.smartlinkall.com/bind.php?id=" . $device_no . "&m=4&uid=" . $store_id . $device_no . "&token=" . $VConfig->zw_token . "&seq=" . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    Tools::curl_get($url);

                    //2.绑定
                    $url = "http://cloudspeaker.smartlinkall.com/bind.php?id=" . $device_no . "&m=1&uid=" . $store_id . $device_no . "&token=" . $VConfig->zw_token . "&seq=" . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $zw_token_data = Tools::curl_get($url);
                    $zw_token_data = json_decode($zw_token_data, true);
                    if ($zw_token_data['errcode'] != 0) {
//                        return json_encode([
//                            'status' => 2,
//                            'message' => $data['errmsg']
//                        ]);
                    }
                }

                if ($VConfig->old_zw_token) {
                    //1.强制解绑
                    $url = "http://cloudspeaker.smartlinkall.com/bind.php?id=" . $device_no . "&m=4&uid=" . $store_id . $device_no . "&token=" . $VConfig->old_zw_token . "&seq=" . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    Tools::curl_get($url);

                    //2.绑定
                    $url = "http://cloudspeaker.smartlinkall.com/bind.php?id=" . $device_no . "&m=1&uid=" . $store_id . $device_no . "&token=" . $VConfig->old_zw_token . "&seq=" . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                    $old_zw_token_data = Tools::curl_get($url);
                    $old_zw_token_data = json_decode($old_zw_token_data, true);
                    if ($old_zw_token_data['errcode'] != 0) {
//                        return json_encode([
//                            'status' => 2,
//                            'message' => $data['errmsg']
//                        ]);
                    }
                }

                if (isset($zw_token_data) && isset($old_zw_token_data) && ($zw_token_data['errcode'] != 0) && ($old_zw_token_data['errcode'] != 0)) {
                    return json_encode([
                        'status' => 2,
                        'message' => $zw_token_data['errmsg'].' | '.$old_zw_token_data['errmsg']
                    ]);
                }

            }

            //易联云打印机 k4
            if ($device_type == 'p_yly_k4') {
                $push = new YlianyunAopClient();
                $push_id = $VConfig->yly_user_id; //用户id
                $push_key = $VConfig->yly_api_key; //api密钥
                $push_user_name = $VConfig->yly_user_name; //用户名
                $print_key = $device_key;
                $print_name = $device_no;

                $add = $push->action_addprint($push_id, $print_name, $push_user_name, $print_name, '1', $push_key, $print_key);
                if ($add != '1') {
                    $msg = '添加失败';
                    if ($add == '2') {
                        $msg = '不要重复添加打印机';
                    }
                    return json_encode([
                        'status' => 2,
                        'message' => $msg
                    ]);
                }
            }
            //如意绑定
            if($device_type=='ruyi_lite'){
                $alipayAntStores= AlipayAntStores::where('store_id', $store_id)->first();
                $alipayOperation=AlipayOperation::where('store_id', $store_id)
                    ->where('merchant_no',$alipayAntStores->ip_role_id)
                    ->first();
                $deviceData=[
                    'store_id' => $store_id,
                    'device_sn' => $device_no,
                    'type' => 'scanBox',
                    'device_type' => $device_type,
                    'device_name' => $device_name,
                    'token'=>$this->token,
                    'bind_user_id'=>$alipayOperation->bind_user_id
                ];

                $resp=$this->bind_device($deviceData);
                $resp=json_decode($resp,true);

                if($resp['status']!='1'){
                    return json_encode([
                        'status' => 2,
                        'message' => $resp['message']
                    ]);
                }

            }
            if ($merchant_id) {
                $merchant_ids = explode(',', $merchant_id);
                foreach ($merchant_ids as $k => $v) {
                    $merchant_s = Merchant::where('id', $v)
                        ->select('name')
                        ->first();
                    if ($merchant_s) {
                        $merchant_name = $merchant_s->name;
                    } else {
                        continue;
                    }

                    $indata = [
                        'store_id' => $store_id,
                        'merchant_id' => $v,
                        'merchant_name' => $merchant_name,
                        'store_name' => $store_name,
                        'device_type' => $device_type,
                        'device_name' => $device_name,
                        'device_no' => $device_no,
                        'device_key' => $device_key,
                        'type' => $type,
                        'cj' => $cj,
                        'config_id' => $config_id
                    ];
                    Device::create($indata);
                }
            } else {
//                $merchant_s = Merchant::where('id', $merchant_id)
//                    ->select('name')
//                    ->first();
//                if ($merchant_s) {
//                    $merchant_name = $merchant_s->name;
//                }

                //商户设备绑定收银员默认为服务商下店长（设备播报问题）
                $obj = DB::table('merchants');
                $obj->join('merchant_stores','merchant_stores.merchant_id','=','merchants.id')
                    ->where('merchants.pid','=',0)
                    ->where('merchant_stores.store_id','=',$store_id)
                    ->select('merchants.name', 'merchants.id')->get();
                $merchant_s = $this->page($obj)->get(0);

                if ($merchant_s) {
                    $merchant_name = $merchant_s[0]->name;
                    $merchant_id = $merchant_s[0]->id;
                }

                $indata = [
                    'store_id' => $store_id,
                    'merchant_id' => $merchant_id,
                    'merchant_name' => $merchant_name,
                    'store_name' => $store_name,
                    'device_type' => $device_type,
                    'device_name' => $device_name,
                    'device_no' => $device_no,
                    'device_key' => $device_key,
                    'type' => $type,
                    'config_id' => $config_id,
                    'cj' => $cj,
                ];
                Device::create($indata);
            }

            $this->status = 1;
            $this->message = "添加成功";
            return $this->format($data);
        } catch (\Exception $e) {
            Log::info('添加设备error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage().' | '.$e->getLine()
            ]);
        }
    }


    public function up(Request $request)
    {
        try {
            $user_merchant = $this->parseToken();
            $id = $request->get('id');
            $merchant_id = $request->get('merchant_id', '');
            $store_id = $request->get('store_id', '');
            $config_id = $user_merchant->config_id;
            $data = $request->except(['token']);
            $cj = isset($data['cj']) ? $data['cj'] : "";
            $data['cj'] = $cj;

            $check_data = [
                'id' => '设备id',
            ];
            $check = $this->check_required($data, $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            if ($merchant_id == "" || $merchant_id == "NULL") {
                $merchant_id = "";
            }

            $merchant_name = "";
            if ($merchant_id) {
                $merchant_s = Merchant::where('id', $merchant_id)->first();
                if ($merchant_s) {
                    $merchant_name = $merchant_s->name;
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '收银员不存在'
                    ]);
                }
            }

            $store = Store::where('store_id', $store_id)
                ->select('store_name')
                ->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在'
                ]);
            }

            $merchant_ids = explode(',', $merchant_id);
            if (count($merchant_ids) > 1) {
                return json_encode([
                    'status' => 2,
                    'message' => '修改不支持添加多个'
                ]);
            }
            //如意设备修改
            if($request->get('device_type')=='ruyi_lite'){
                $device=Device::where('id',$id)->first();
                if($request->get('device_no')!=$device->device_no&&$request->get('store_id')!=$device->store_id){
                    $unbindUpData=[
                        'device_sn'=>$device->device_no,
                        'store_id'=>$device->store_id
                    ];
                    $unbind=$this->unbind_device($unbindUpData);
                    if($unbind['status']!='1'){
                        return json_encode([
                            'status'=>'2',
                            'message'=>'修改失败,如意解绑返回信息'.$unbind['message']
                        ]);
                    }
                    $bindUpData=[
                        'store_id'=>$store_id,
                        'type'=>'scanBox',
                        'device_type'=>$request->get('device_type'),
                        'device_name'=>$request->get('device_name'),
                        'device_sn'=>$request->get('device_no')
                    ];
                    $bind=$this->bind_device($bindUpData);
                    $bind=json_decode($bind,true);
                    if($bind['status']!='1'){
                        return json_encode([
                            'status'=>'2',
                            'message'=>'修改失败,如意绑定返回信息'.$bind['message']
                        ]);
                    }
                }
            }
            $store_name = $store->store_name;
            $data['merchant_name'] = $merchant_name;
            $data['merchant_id'] = $merchant_id;
            $data['store_id'] = $store_id;
            $data['store_name'] = $store_name;
            $data['config_id'] = $config_id;
            Device::where('id', $id)->update($data);

            $this->status = 1;
            $this->message = "修改成功";
            return $this->format($data);
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage().' | '.$e->getLine()
            ]);
        }
    }


    //删除设备
    public function del(Request $request)
    {
        try {
            $user_merchant = $this->parseToken();
            $id = $request->get('id');

            $data = $request->except(['token']);
            $check_data = [
                'id' => '设备id',
            ];
            $check = $this->check_required($data, $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $device = Device::where('id', $id)->first();
            if (!$device) {
                return json_encode([
                    'status' => 2,
                    'message' => '设备不存在'
                ]);
            }

            if ($user_merchant->type == "merchant") {
                return json_encode([
                    'status' => 2,
                    'message' => '暂无法解绑！请联系业务员'
                ]);
            }

            $VConfig = VConfig::where('config_id', $device->config_id)
                ->select(
                    'zw_token',
                    'zlbz_token',
                    'yly_user_id',
                    'yly_api_key'
                )->first();
            if (!$VConfig) {
                $VConfig = VConfig::where('config_id', '1234')
                    ->select(
                        'zw_token',
                        'zlbz_token',
                        'yly_user_id',
                        'yly_api_key'
                    )->first();
            }

            //云喇叭删除
            if ($device->device_type == 'v_zlbz_1') {
                $data = [
                    'id' => $device->device_no,
                    'm' => '0',
                    'uid' => $device->store_id,
                    'token' => $VConfig->zlbz_token
                ];
                $curl = Tools::curl($data, 'http://39.106.131.149/bind.php');
                $curl = json_decode($curl, true);
                if ($curl['errcode'] != 0) {
                    return json_encode([
                        'status' => 2,
                        'message' => $curl['errcode'] . '-' . $curl['errmsg']
                    ]);
                }
            }

            //易联云删除
            if ($device->device_type == "p_yly_k4") {
                $push = new YlianyunAopClient();
                $push_id = $VConfig->yly_user_id; //用户id
                $push_key = $VConfig->yly_api_key; //api密钥
                $print_key = $device->device_key;
                $print_name = $device->device_no;
                $delete = $push->action_removeprinter($push_id, $print_name, $push_key, $print_key);
                if ($delete != '1') {
                    $msg = '删除设备失败';
                    if ($delete == '2') {
                        $msg = '没这个设备';
                    }

                    return json_encode([
                        'status' => 2,
                        'message' => $msg
                    ]);
                }
            }
            //如意解绑
            if($device->device_type=='ruyi_lite'){
                $delData=[
                    'device_sn'=>$device->device_no,
                    'store_id'=>$device->store_id
                ];
                $resp=$this->unbind_device($delData);
                if($resp['status']!='1'){
                    return json_encode([
                        'status' => 2,
                        'message' => $resp['message']
                    ]);
                }
            }
            Device::where('id', $id)->delete();

            $this->status = 1;
            $this->message = "删除成功";
            return $this->format($data);
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage().' | '.$e->getLine()
            ]);
        }
    }


    public function select(Request $request)
    {
        try {
            $user_merchant = $this->parseToken();
            $id = $request->get('id');

            $data = $request->except(['token']);
            $check_data = [
                'id' => '设备id'
            ];
            $check = $this->check_required($data, $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $re = Device::where('id', $id)->first();
            if (!$re) {
                return json_encode([
                    'status' => 2,
                    'message' => 'id有误'
                ]);
            }

            $this->status = 1;
            $this->message = "查询成功";
            return $this->format($re);
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage().' | '.$e->getLine()
            ]);
        }
    }


    public function lists(Request $request)
    {
        try {
            $user_merchant = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $merchant_id = $request->get('merchant_id', '');
            $device_type = $request->get('device_type', '');
            $type = $request->get('type', '');

            $where = [];
            $whereIn = [];
            $store_ids = [];
            if ($merchant_id) {
                $where[] = ['merchant_id', '=', $merchant_id];
            }

            if ($store_id) {
                $store_ids = [
                    [
                        'store_id' => $store_id
                    ]
                ];
            } else {
//                $MerchantStore = MerchantStore::where('merchant_id', $user_merchant->merchant_id)
//                    ->select('store_id')
//                    ->get();
//                if (!$MerchantStore->isEmpty()) {
//                    $store_ids = $MerchantStore->toArray();
//                }
                $store = Store::where('merchant_id', $user_merchant->merchant_id)->select('id', 'store_id')->first();
                $stores = Store::where('pid', $store->id)->select('store_id')->get();
                if (!$stores->isEmpty()) {
                    $store_ids = $stores->toArray();
                }
                array_push($store_ids, ['store_id' => $store->store_id]);
            }

            if ($device_type) {
                $where[] = ['device_type', '=', $device_type];
            }

            if ($type) {
                $where[] = ['type', '=', $type];
            }

            $obj = DB::table('devices');

            $data = $obj->where($where)
                ->whereIn('store_id', $store_ids)
                ->orderBy('updated_at', 'desc');
            $this->t = $data->count();
            $data = $this->page($data)->get();

            $this->status = 1;
            $this->message = "数据返回成功";
            return $this->format($data);
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage().' | '.$e->getLine()
            ]);
        }
    }

    public function device_type(Request $request)
    {
        try {
            $user_merchant = $this->parseToken();
            $type = $request->get('type', 'p');

            $data = [];
            if ($type == "p") {
                $data = [
                    [
                        'device_type' => 'p_zlbz_1',
                        'device_name' => '打印机(智联博众)'
                    ],
                    [
                        'device_type' => 'p_yly_k4',
                        'device_name' => '打印机(易联云)'
                    ]
                ];
            }

            if ($type == "s") {
                $data = [
                    [
                        'device_type' => 's_bp_sl51',
                        'device_name' => '扫码盒子(sl51)'
                    ],
                    [
                        'device_type' => 's_nl_me50h',
                        'device_name' => '扫码盒子(ME50H)'
                    ],
                    [
                        'device_type' => 's_nl_me50c',
                        'device_name' => '扫码盒子(ME50C)'
                    ],
                    [
                        'device_type' => 's_bp_sl58',
                        'device_name' => '扫码设备(sl58)'
                    ],
                    [
                        'device_type' => 's_bp_sl56',
                        'device_name' => '扫码设备(sl56)'
                    ],
                    [
                        'device_type' => 's_ps_ys',
                        'device_name' => '扫码盒子(品生)'
                    ],
//                    [
//                        'device_type' => 'face_cm01',
//                        'device_name' => '刷脸设备(织点cm01)'
//                    ],
                    [
                        'device_type' => 's_bf_qr65',
                        'device_name' => '百富扫码王(qr65)'
                    ],
                    [
                        'device_type' => 's_bf_qr68',
                        'device_name' => '百富扫码王(qr68)'
                    ],
                    [
                        'device_type' => 's_bf_qr68_4g',
                        'device_name' => '百富扫码王(qr68-4G)'
                    ],
                    [
                        'device_type' => 's_ky_mp10',
                        'device_name' => '扫码盒子(mp10)'
                    ],
                    [
                        'device_type' => 's_s6',
                        'device_name' => '扫码盒子(s6)'
                    ],
                    [
                        'device_type' => 'c_01',
                        'device_name' => '阿尔丰盒子(c01)'
                    ],
                    [
                        'device_type' => 'ruyi_lite',
                        'device_name' => '如意Lite'
                    ],
                    [
                        'device_type' => 'cls_box',
                        'device_name' => '收款音箱'
                    ]
                ];
            }

            if ($type == "f") {
                $data = [
                    [
                        'device_type' => 'face_f4',
                        'device_name' => '支付宝蜻蜓'
                    ],
                    [
                        'device_type' => 'face_pro',
                        'device_name' => '微信刷脸青蛙PRO'
                    ],
                    [
                        'device_type' => 'face_TZH-L1',
                        'device_name' => '微信青蛙'
                    ],
                    [
                        'device_type' => 'AECR F8',
                        'device_name' => '联迪F8'
                    ],
//		            [
//                        'device_type' => 'face_cm01',
//                        'device_name' => '微信刷脸(织点cm01)'
//                    ],
//                    [
//                        'device_type' => 'face_jsd101',
//                        'device_name' => '微信刷脸(君时达Z2)'
//                    ]
                ];
            }

            if ($type == "v") {
                $data = [
                    [
                        'device_type' => 'v_zlbz_1',
                        'device_name' => '播报设备(智联博众)'
                    ],
                    [
                        'device_type' => 'v_zw_1',
                        'device_name' => '播报设备(智网云)'
                    ],
                    [
                        'device_type' => 'v_bp_1',
                        'device_name' => '播报设备(波普SLX1)'
                    ],
                    [
                        'device_type' => 'v_kd_58',
                        'device_name' => '播报设备(新国都KD58)'
                    ],
                    [
                        'device_type' => 'v_jbp_d30',
                        'device_name' => '播报设备(聚宝盆D30)'
                    ],
                    [
                        'device_type' => 'v_ps_sp08',
                        'device_name' => '播报设备(品生sp08)'
                    ],
                    [
                        'device_type' => 'v_cls_q038',
                        'device_name' => '播报设备(易企推Q038)'
                    ],
                    [
                        'device_type' => 'v_cls_wifi',
                        'device_name' => '播报设备(易企推Wifi)'
                    ]
                ];
            }

            $this->status = 1;
            $this->message = "数据返回成功";

            return $this->format($data);
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage().' | '.$e->getLine()
            ]);
        }
    }


    public function get_device(Request $request)
    {
        try {
            $user_merchant = $this->parseToken();

            $data = $request->except(['token']);
            $check_data = [
                'type' => '设备类型',
                'name' => '收件人名字',
                'phone' => '收件人电话',
                'address' => '详细地址',
                'province_code' => '省',
                'city_code' => '市',
                'area_code' => '区'
            ];
            $check = $this->check_required($data, $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $this->status = 1;
            $this->message = "申请成功";
            return $this->format($data);
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage().' | '.$e->getLine()
            ]);
        }
    }


    //配置
    public function v_config(Request $request)
    {
        try {
            $user_merchant = $this->parseToken();
            $config_id = $user_merchant->config_id;
            $zlbz_token = $request->get('zlbz_token', "");
            $old_zw_token = $request->get('old_zw_token', "");
            $zw_token = $request->input('zw_token', "");
            $yly_user_name = $request->post('yly_user_name', "");
            $yly_user_id = $request->post('yly_user_id', "");
            $yly_api_key = $request->post('yly_api_key', "");
            $type = $request->post('type', ""); //有值为更新
            $tencent_cloud_secretid = $request->post('tencent_cloud_secretid', "");
            $tencent_cloud_secretkey = $request->post('tencent_cloud_secretkey', "");
            $cls_prefix = $request->post('cls_prefix', "");
            $cls_product_id = $request->post('cls_product_id', "");

            $VConfig = VConfig::where('config_id', $config_id)->first();

            if (!$type) {
                $this->status = 1;
                $this->message = "查询成功";
                return $this->format($VConfig);
            }

            $data = [
                'config_id' => $config_id,
                'old_zw_token' => $old_zw_token ?? '',
                'zw_token' => $zw_token ?? '',
                'yly_user_name' => $yly_user_name ?? '',
                'yly_user_id' => $yly_user_id ?? '',
                'yly_api_key' => $yly_api_key ?? '',
                'zlbz_token' => $zlbz_token ?? '',
                'tencent_cloud_secretid' => $tencent_cloud_secretid ?? '',
                'tencent_cloud_secretkey' => $tencent_cloud_secretkey ?? '',
                'cls_prefix' => $cls_prefix ?? '',
                'cls_product_id' => $cls_product_id ?? '',
            ];
            // if ($zlbz_token) $data['zlbz_token'] = $zlbz_token;
            if ($VConfig) {
                // $VConfig->update($data);
                // $VConfig->save();
                VConfig::where('config_id', $config_id)->update($data);
            } else {
                VConfig::create($data);
            }

            $this->status = 1;
            $this->message = "添加成功";
            return $this->format($data);
        } catch (\Exception $ex) {
            Log::info('添加设备-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            return json_encode([
                'status' => -1,
                'message' => $ex->getMessage().' | '.$ex->getLine()
            ]);
        }
    }

    /**如意设备绑定
     * @param $data
     * @return false|string
     */
    public function bind_device($data)
    {
        try {
            //$merchant = $this->parseToken();
            $store_id = $data['store_id'];
            $type = $data['type'];  //scanBox
            $device_type = $data['device_type']; //ruyi_lite
            $device_name = $data['device_name'];    //如意Lite
            $device_sn = $data['device_sn'];
            $bind_user_id=$data['bind_user_id'];
            $MerchantDevice = MerchantDevice::where('store_id', $store_id)
                ->where('device_sn', $device_sn)
                ->first();
            if ($MerchantDevice) {
                return json_encode([
                    'status' => 2,
                    'message' => '设备已经绑定无需重复绑定'
                ]);
            }
            $alipayAntStores= AlipayAntStores::where('store_id', $store_id)->first();

            if(!$alipayAntStores){
                return json_encode([
                    'status' => 2,
                    'message' => '门店暂未绑定，请绑定门店'
                ]);
            }

            $alipayOperation=AlipayOperation::where('store_id', $store_id)
                ->where('merchant_no',$alipayAntStores->ip_role_id)
                ->first();


            if($alipayOperation->store_type==1){
                $merchant_id_type='direct';
            }else{
                $merchant_id_type='indirect';
            }
            $deviceData=[
                'storeId'=>$store_id,
                'device_sn'=>$device_sn,//设备sn
                'source'=>'2088121668676708',//设备来源ISV在支付宝的pid
                'external_id'=> $store_id,//商户编号，由ISV定义，需要保证在ISV下唯一
                'merchant_id_type'=>$merchant_id_type,//区分商户ID类型，直连商户填写direct，间连商户填写indirect
                'merchant_id'=>$alipayOperation->merchant_no,//商户角色id。对于直连开店场景，填写商户pid；对于间连开店场景，填写商户smid
                'shop_id'=>$alipayAntStores->shop_id,//店铺ID
                'bind_user_id'=>$bind_user_id
            ];
            $ruyiPay=new RuyiPayController();
            $deviceResp=$ruyiPay->deviceBind($deviceData);
            if($deviceResp['status']==1){
                MerchantDevice::create([
                    'store_id' => $store_id,//系统商户id
                    'source'=>'2088121668676708',//设备来源ISV在支付宝的pid
                    'device_type' => $device_type,//设备类型
                    'device_sn' => $device_sn,  //sn
                    'external_id'=>$store_id,//商户编号，由ISV定义，需要保证在ISV下唯一
                    'type' => $type,   //scanBox
                    'device_name' => $device_name,  //设备名称
                    'merchant_id_type'=>$merchant_id_type, //区分商户ID类型，直连商户填写direct，间连商户填写indirec
                    'merchant_id'=> $alipayOperation->merchant_no,//商户角色id。对于直连开店场景，填写商户pid；对于间连开店场景，填写商户smid
                    'shop_id'=>$alipayAntStores->shop_id,//店铺ID
                    'external_shop_id'=>$store_id        //外部门店id
                ]);

                return json_encode([
                    'status' => 1,
                    'message' => '绑定成功',
                ]);
            }else{
                return json_encode([
                    'status' => 2,
                    'message' => $deviceResp['message'],
                ]);
            }


        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()
            ]);
        }

    }

    /**如意设备解绑
     * @param $data
     * @return array|string[]
     */
    public function unbind_device($data)
    {
        try {
            //$merchant = $this->parseToken();
            $device_sn = $data['device_sn'];
            $store_id = $data['store_id'];
            if ($device_sn == "") {
                return [
                    'status'=>'2',
                    'message'=>'请选择设备'
                ];
            }
            $MerchantDevice = MerchantDevice::where('device_sn', $device_sn)->where('store_id', $store_id)->first();
            if (!$MerchantDevice) {
                return [
                    'status'=>'2',
                    'message'=>'设备不存在'
                ];
            }

            try{
                DB::beginTransaction();  //开启事务

                $deviceData=[
                    'storeId'=>$store_id,
                    'device_sn'=>$device_sn,//设备sn
                    'source'=>'2088121668676708',//设备来源ISV在支付宝的pid
                    'external_id'=>$MerchantDevice->external_shop_id,//商户编号，由ISV定义，需要保证在ISV下唯一
                    'merchant_id_type'=>$MerchantDevice->merchant_id_type,//区分商户ID类型，直连商户填写direct，间连商户填写indirec
                    'merchant_id'=>$MerchantDevice->merchant_id,//商户角色id。对于直连开店场景，填写商户pid；对于间连开店场景，填写商户smid
                    'shop_id'=>$MerchantDevice->shop_id,//店铺ID
                ];
                $ruyiPay=new RuyiPayController();
                $deviceResp=$ruyiPay->deviceUnbind($deviceData);
                if($deviceResp['status']==1){
                    $MerchantDevice->delete();
                    DB::commit(); //提交事务
                }
                return $deviceResp;
            } catch(\Exception $ex) {
                DB::rollBack();  //事务回滚
                return [
                    'status' => '-1',
                    'message' => $ex->getCode() . '|' . $ex->getMessage() . '|' . $ex->getLine()
                ];
            }

        } catch (\Exception $exception) {
            return [
                'status' => '-1',
                'message' => $exception->getCode() . '|' . $exception->getMessage() . '|' . $exception->getLine()
            ];
        }

    }

}
