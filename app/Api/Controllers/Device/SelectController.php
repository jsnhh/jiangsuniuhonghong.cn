<?php
namespace App\Api\Controllers\Device;


use App\Models\DeviceOem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SelectController extends \App\Api\Controllers\BaseController
{

    //获得设备的接口地址
    public function device_oem_lists(Request $request)
    {
        try {
            $user_merchant = $this->parseToken();
            $device_id = $request->get('device_id', '');
            $user_id = $user_merchant->user_id;
            $where = [];

            if ($device_id) {
                $where[] = ['device_id', '=', $device_id];
            }

            $user_ids = $this->getSubIds($user_id);

            $obj = DB::table('device_oems');
            $data = $obj->where($where)->whereIn('user_id', $user_ids);
            $data->leftjoin('devices', 'devices.device_no', '=', 'device_oems.device_id')
                ->select('devices.store_name', 'devices.merchant_name','device_oems.*');

            $this->t = $data->count();
            $data = $this->page($data)->orderBy('created_at','desc')->get();
            $this->status = 1;
            $this->message = "数据返回成功";
	    
            return $this->format($data);
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    //删除
    public function device_oem_del(Request $request)
    {
        try {
            $user_merchant = $this->parseToken();
            $device_id = $request->get('device_id', '');
            $device_type = $request->get('device_type', '');
            $where = [];

            $check_data = [
                'device_id' => '设备ID',
                'device_type' => '设备类型'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            if ($device_id) {
                $where[] = ['device_id', '=', $device_id];
            }

            if ($device_type) {
                $where[] = ['device_type', '=', $device_type];
            }

            $data = DeviceOem::where($where)->delete();
            if ($data) {
                $this->status = 1;
                $this->message = "删除成功";
                return $this->format($request->except('token'));
            } else {
                $this->status = 2;
                $this->message = "删除失败";
                return $this->format($request->except('token'));
            }

        } catch (\Exception $ex) {
            return json_encode([
                'status' => -1,
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    //查看
    public function device_oem_info(Request $request)
    {
        try {
            $user_merchant = $this->parseToken();
            $device_id = $request->get('device_id', '');
            $device_type = $request->get('device_type', '');
            $where = [];

            $check_data = [
                'device_id' => '设备ID',
                'device_type' => '设备类型',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            if ($device_id) {
                $where[] = ['device_id', '=', $device_id];
            }

            if ($device_type) {
                $where[] = ['device_type', '=', $device_type];
            }

            $data = DeviceOem::where($where)->first();

            return json_encode([
                'status' => '1',
                'message' => '数据返回成功',
                'data' => $data
            ]);
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    //修改
    public function device_oem_up(Request $request)
    {
        try {
            $user_merchant = $this->parseToken();
            $device_id = $request->get('device_id', ''); //设备号
            $device_type = $request->get('device_type', ''); //设备类型
            $where = [];

            $check_data = [
                'device_id' => '设备ID',
                'device_type' => '设备类型'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            if ($device_id) {
                $where[] = ['device_id', '=', $device_id];
            }

            if ($device_type) {
                $where[] = ['device_type', '=', $device_type];
            }

            $insert = $request->except(['token']);
//            $data = DeviceOem::where($where)->update($insert);
            $device_oem_obj = DeviceOem::where('device_id', $device_id)
                ->orderBy('updated_at', 'desc')
                ->first();
            if ($device_oem_obj) {
                $res = $device_oem_obj->update($insert);
                if ($res) {
                    return json_encode([
                        'status' => 1,
                        'message' => '数据保存成功',
                        'data' => $insert
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '修改失败',
                        'data' => $insert
                    ]);
                }
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '设备号不存在',
                    'data' => $device_id
                ]);
            }

        } catch (\Exception $ex) {
            return json_encode([
                'status' => -1,
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    //添加
    public function device_oem_add(Request $request)
    {
        try {
            $user_merchant = $this->parseToken();
            $device_id = $request->get('device_id', '');
            $device_type = $request->get('device_type', '');
            $Request = $request->get('Request', '');

            $check_data = [
                'device_id' => '设备ID',
                'device_type' => '设备类型'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $device_oem_obj = DeviceOem::where('device_id', $device_id)
                ->where('device_type', $device_type)
                ->first();
            if ($device_oem_obj) {
                return json_encode([
                    'status' => 2,
                    'message' => '设备号已存在',
                    'data' => [
                        'device_id' => $device_id,
                        'device_type' => $device_type
                    ]
                ]);
            }

            $insert = [
                'user_id'=>$user_merchant->user_id,
                'device_id'=>$device_id,
                'device_type'=>$device_type,
                'Request'=>$Request,
                'ScanPay' => '/api/devicepay/scan_pay',
                'QrPay' => '/api/devicepay/qr_pay',
                'QrAuthPay' => '/api/devicepay/qr_auth_pay',
                'PayWays' => '/api/devicepay/store_pay_ways',
                'OrderQuery' => '/api/devicepay/order_query',
                'Order' => '/api/devicepay/order',
                'OrderList' => '/api/devicepay/order_list',
                'Refund' => '/api/devicepay/refund'
            ];
            $data = DeviceOem::create($insert);
            if ($data) {
                return json_encode([
                    'status' => '1',
                    'message' => '添加成功',
                    'data' => $insert
                ]);
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '添加失败',
                    'data' => $insert
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => -1,
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    //导入
    public function device_oem_import(Request $request)
    {
        try {
            $user_merchant = $this->parseToken();
            if (empty($_FILES)) {
                $this->status = 2;
                $this->message = '请上传xlsx表格！';
                return $this->format();
            }

            $file_arr = array_shift($_FILES);
            if ($file_arr['error'] !== 0) {
                $this->status = 2;
                $this->message = '请上传xlsx表格！';
                return $this->format();
            }
            // var_dump($file_arr);die;
            $file = $file_arr['tmp_name'];
            $excel_data = \App\Common\Excel\Excel::_readExcel($file);
            foreach ($excel_data as $k => $v) {
                if ($k == 0) {
                    continue;
                }

                if (trim($v[0]) == "") {
                    continue;
                }

                if (trim($v[1]) == "") {
                    continue;
                }

                $ym = trim($v[2]);

                $insert = [
                    'user_id' => $user_merchant->user_id,
                    'device_id' => trim($v[0]),
                    'device_type' => trim($v[1]),
                    'Request' => $ym,
                    'ScanPay' => '/api/devicepay/scan_pay',
                    'QrPay' => '/api/devicepay/qr_pay',
                    'QrAuthPay' => '/api/devicepay/qr_auth_pay',
                    'PayWays' => '/api/devicepay/store_pay_ways',
                    'OrderQuery' => '/api/devicepay/order_query',
                    'Order' => '/api/devicepay/order',
                    'OrderList' => '/api/devicepay/order_list',
                    'Refund' => '/api/devicepay/refund'
                ];

                $newland_merchant = DeviceOem::where('device_id', trim($v[0]))
                    ->where('device_type', trim($v[1]))
                    ->first();
                if ($newland_merchant) {
                    //如果是同一个人导入的 会更新
                    if ($user_merchant->user_id == $newland_merchant->user_id){
                        $newland_merchant->update($insert);
                        $newland_merchant->save();
                    }else{
                        continue;
                    }
                } else {
                    DeviceOem::create($insert);
                }
            }

            $this->status = 1;
            $this->message = '导入成功';
            return $this->format();
        } catch (\Exception $ex) {
            return json_encode([
                'status' => -1,
                'message' => $ex->getMessage().' | '.$ex->getLine()
            ]);
        }
    }


}
