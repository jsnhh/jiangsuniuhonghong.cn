<?php
/**
 * Created by PhpStorm.
 * User: daimingkang
 * Date: 2019/8/29
 * Time: 9:06 PM
 */

namespace App\Api\Controllers\Tfpay;


use App\Api\Controllers\Config\TfConfigController;
use App\Models\Store;
use Illuminate\Http\Request;

class SelectController
{


    public function select_bank(Request $request)
    {

        try {
            $store_id = $request->get('store_id');

            $Store = Store::where('store_id', $store_id)
                ->select('config_id', 'pid')
                ->first();

            if (!$Store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在'
                ]);
            }
            $store_pid = $Store->pid;
            $config = new TfConfigController();


            $tf_merchant = $config->tf_merchant($store_id, $store_pid);

            if (!$tf_merchant) {
                return json_encode([
                    'status' => 2,
                    'message' => '传化商户不存在'
                ]);
            }

            $tf_config = $config->tf_config($Store->config_id,$tf_merchant->qd);

            if (!$tf_config) {
                return json_encode([
                    'status' => 2,
                    'message' => '传化配置不存在请检查配置'
                ]);
            }

            //传化报备

            $obj = new \App\Api\Controllers\Tfpay\BaseController();
            $obj->mch_id = $tf_config->mch_id;
            $obj->pub_key = $tf_config->pub_key;
            $obj->pri_key = $tf_config->pri_key;


            //费率同步
            $post_data = [
                'sub_mch_id' => $tf_merchant->sub_mch_id,
            ];

            $method = '/openapi/merchant/settle-bankcard-query';
            $re = $obj->api($post_data, $method, false);
            if ($re['code'] != 0) {
                return json_encode([
                    'status' => 2,
                    'message' => $re['msg']
                ]);
            }

            return json_encode([
                'status' => 1,
                'message' => '数据返回成功',
                'data' => $re
            ]);

        } catch (\Exception $exception) {

        }


    }

}