<?php
/**
 * Created by PhpStorm.
 * User: daimingkang
 * Date: 2019/4/27
 * Time: 9:36 PM
 */

namespace App\Api\Controllers\Tfpay;


use App\Api\Controllers\Config\TfConfigController;
use App\Models\MyBankCategory;
use App\Models\TfStore;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StoreController
{
    //进件
    public function open_store($store, $store_bank, $store_img, $qd = 1)
    {
        try {

            $config = new TfConfigController();
            $h_config = $config->tf_config($store->config_id,$qd);

            if (!$h_config) {

                return [
                    'status' => 2,
                    'message' => '传化配置不存在请检查配置',
                ];
            }

            $wechat_channel_no = $h_config->wechat_channel_no;
            $alipay_pid = $h_config->alipay_pid;


            $obj = new BaseController();
            $obj->mch_id = $h_config->mch_id;
            $obj->pub_key = $h_config->pub_key;
            $obj->pri_key = $h_config->pri_key;

            //上传图片

            //1.身份证正面

            $file = $this->img_content($store_img->head_sfz_img_a, '身份证正面', $obj); //文件本地路径

            if ($file['status'] == 2) {
                return $file;
            }
            $idcard_front_pic = $file['img'];


            //2.身份证反面
            $file = $this->img_content($store_img->head_sfz_img_b, '身份证反面', $obj); //文件本地路径
            if ($file['status'] == 2) {
                return $file;
            }
            $idcard_back_pic = $file['img'];

            //3.营业执照
            $file = $this->img_content($store_img->store_license_img ? $store_img->store_license_img : $store_img->head_sfz_img_a, '营业执照', $obj); //文件本地路径
            if ($file['status'] == 2) {
                return $file;
            }
            $license_pic = $file['img'];


            //3.银行卡照片
            $img_url = $store_bank->store_bank_type != "02" ? $store_img->bank_img_a : $store_img->store_industrylicense_img;
            $file = $this->img_content($img_url, '银行卡照片', $obj); //文件本地路径
            if ($file['status'] == 2) {
                return $file;
            }
            $bankcard_pic = $file['img'];

            //4.门头
            $file = $this->img_content($store_img->store_logo_img, '门头', $obj); //文件本地路径
            if ($file['status'] == 2) {
                return $file;
            }

            $door_pic = $file['img'];


            $file_pics = [
                'idcard_front_pic' => $idcard_front_pic,
                'idcard_back_pic' => $idcard_back_pic,
                'license_pic' => $license_pic,
                'bankcard_pic' => $bankcard_pic,
                'door_pic' => $door_pic,
            ];


            //银行卡

            $tfpay_banks = DB::table('tfpay_banks')
                ->where('bank_name', $store_bank->bank_name)
                ->first();

            if (!$tfpay_banks) {
                return [
                    'status' => 2,
                    'message' => '暂不支持该银行卡进件',
                ];
            }

            $bankcard = [
                'type' => $store_bank->store_bank_type != "01" ? '2' : '1',//1 个人 2 企业
                'bank_code' => $tfpay_banks->bank_no,
                'account_name' => $store_bank->store_bank_name,
                'account_number' => $store_bank->store_bank_no,
                'bank_province_code' => $store_bank->bank_province_code ? $store_bank->bank_province_code : $store_bank->province_code,
                'bank_city_code' => $store_bank->bank_city_code ? $store_bank->bank_city_code : $store->city_code,
                'branch_name' => $store_bank->sub_bank_name,
            ];

            //商户信息
            $store_type = $store->store_type;//经营性质 1-个体，2-企业，3-个人

            if ($store_type == 1) {
                $organization_type = "2";
            } elseif ($store_type == 2) {
                $organization_type = "3";
            } else {
                $organization_type = "1";

            }


            $MyBankCategory = MyBankCategory::where('category_id', $store->category_id)
                ->first();
            if (!$MyBankCategory) {
                return json_encode(['status' => 2, 'message' => '请选择正确的门店类目']);
            }

            $post_data = [
                'organization_type' => $organization_type,//1、小微商户 2、个体工商户 3、企业
                'name' => $store->store_name,
                'shortname' => $store->store_short_name,
                'mcc_code' => '1005',//$MyBankCategory->mcc
                'sub_mcc_code' => '5399',
                'contact' => $store->people,
                'contact_type' => 'LEGAL_PERSON',
                'contact_business_type' => '02',
                'service_phone' => $store->people_phone,
                'id_card_number' => $store->head_sfz_no,
                'license_type' => 'NATIONAL_LEGAL',
                'license_number' => $store->store_license_no ? $store->store_license_no : $store->head_sfz_no,
                'province_code' => $store->province_code,
                'city_code' => $store->city_code,
                'district_code' => $store->area_code,
                'address' => $store->store_address,
                'bankcard' => $bankcard,
                'file_pics' => $file_pics,
                'wechat_channel_no' => $wechat_channel_no,
                'alipay_pid' => $alipay_pid,
                'notify_url' => url('/api/tfpay/store_notify')
            ];

            $TfStore = TfStore::where('store_id', $store->store_id)
                ->select('sub_mch_id')
                ->first();
            if ($TfStore) {
                $method = '/openapi/merchant/update';
                $post_data['sub_mch_id'] = $TfStore->sub_mch_id;

            } else {
                $method = '/openapi/merchant/register';
            }


            $re = $obj->api($post_data, $method, false);


            //成功
            if ($re['code'] == 0) {
                return [
                    'status' => 1,
                    'message' => '进件成功',
                    'data' => $re['data'],
                ];
            } else {
                return [
                    'status' => 2,
                    'message' => $re['msg'],
                ];
            }

        } catch (\Exception $exception) {
            return [
                'status' => 2,
                'message' => $exception->getMessage(),
            ];
        }


    }


    public function img_content($img_url, $msg, $obj)
    {
        try {
            $img_url = explode('/', $img_url);
            $img_url = end($img_url);
            $img = public_path() . '/upload/images/' . $img_url;
            if ($img) {
                try {
                    //压缩图片
                    $img_obj = \Intervention\Image\Facades\Image::make($img);
                    $img_obj->resize(800, 700);
                    $img = public_path() . '/upload/s_images/' . $img_url;
                    $img_obj->save($img);

                } catch (\Exception $exception) {

                }
            }

            $method = '/openapi/picture/upload'; //方法名
            $post_data['file'] = curl_file_create($img, 'image/png', 'file');

            $result = $obj->api($post_data, $method, true);
            if (!isset($result['code']) && isset($result['info'])) {
                return [
                    'status' => 2,
                    'message' => $result['info'],
                ];
            }
	    
            if ($result['code'] !== 0) {
                return [
                    'status' => 2,
                    'message' => $msg . $result['msg'],
                ];
            } else {
                return [
                    'status' => 1,
                    'img' => $result['data']['path'],
                ];
            }
        } catch (\Exception $exception) {
            return [
                'status' => 2,
                'message' => $exception->getMessage(),
            ];
        }
    }


}