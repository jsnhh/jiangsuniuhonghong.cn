<?php

namespace App\Api\Controllers\Tfpay;

use App\Api\Controllers\Config\MyBankConfigController;
use App\Api\Controllers\Config\TfConfigController;
use App\Models\MemberList;
use App\Models\MemberSetJf;
use App\Models\MemberTpl;
use App\Models\TfConfig;
use EasyWeChat\Factory;
use Illuminate\Http\Request;

class OauthOpenidController extends \App\Api\Controllers\DevicePay\BaseController
{

    //第三方非想用平台来获取快钱的openid
    public function oauth_sign_openid(Request $request)
    {
        //第三方传过来的信息
        //获取请求参数
        $data = $request->all();
        //验证签名
        $check = $this->check_md5($data);
        if ($check['return_code'] == 'FALL') {
            return $this->return_data($check);
        }
        $config_id = $data['config_id'];
        $callback_url = $data['callback_url'];


        $config = new TfConfigController();
        $h_config = $config->tf_config($config_id);

        if (!$h_config) {
            return json_encode([
                'status' => 2,
                'message' => '传化配置不存在请检查配置'
            ]);
        }
        $config = [
            'app_id' => $h_config->wx_appid,
            'scope' => 'snsapi_base',
            'oauth' => [
                'scopes' => ['snsapi_base'],
                'response_type' => 'code',
                'callback' => url('api/tfpay/weixin/oauth_sign_callback_openid?wx_AppId=' . $h_config->wx_appid . '&wx_Secret=' . $h_config->wx_secret . '&callback_url=' . $callback_url),
            ],

        ];
        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;
        return $oauth->redirect();

    }

    //第三方非想用平台来获取快钱的openid
    public function oauth_sign_callback_openid(Request $request)
    {
        $callback_url = $request->get('callback_url');
        $code = $request->get('code');
        $wx_AppId = $request->get('wx_AppId');
        $wx_Secret = $request->get('wx_Secret');
        $config = [
            'app_id' => $wx_AppId,
            "secret" => $wx_Secret,
            "code" => $code,
            "grant_type" => "authorization_code",
        ];

        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;
        $user = $oauth->user();
        $open_id = $user->getId();


        return redirect($callback_url . '?open_id=' . $open_id);


    }


    public function Options()
    {
        $options = [
            'app_id' => '',
            'payment' => [
                'merchant_id' => '',
                'key' => '',
                'cert_path' => '', // XXX: 绝对路径！！！！
                'key_path' => '',      // XXX: 绝对路径！！！！
                'notify_url' => '',       // 你也可以在下单时单独设置来想覆盖它
            ],
        ];

        return $options;
    }
}
