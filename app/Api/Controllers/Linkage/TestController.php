<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/07/27
 * Time: 下午5:13
 */
namespace App\Api\Controllers\Linkage;


class TestController extends BaseController
{
    public function test()
    {
        //报备微信公众号
        $wx_appid_conf_add_data = [
            'accessid' => 'cpostest',
            'agent_no' => '11010200097',
            'merch_no' => '88888888',
            'conf_key' => 'auth_path',
            'conf_value' => env('APP_URL') . '/api/hkrt/weixin/'
        ];
        $wx_appid_conf_add_data['sign'] = $this->getSignContent($wx_appid_conf_add_data, '48e70b51cab84019a2dddf2f03f13a38');
        $wx_appid_conf_add_res = $this->execute($wx_appid_conf_add_data, $this->url.$this->wx_appid_conf_add);
        print_r($wx_appid_conf_add_res);die;
        //{"return_code":10010,"return_msg":"服务商编号不存在","sign":"449E61FA5F63C91E3505C8181A0650B8"}
        //{"result":"0","msg":"成功","return_code":10000,"sign":"25C1FB4FF07EA67D8F333FE346739B10"}

        //商户业务查询
//        $merchant_biz_query_data = [
//           'accessid' => 'cpostest',
//           'agent_no' => '11010200097',
//           'agent_apply_no' => '356771679469',
//           'apply_type' => '12'
//        ];
//        $merchant_biz_query_data['sign'] = $this->getSignContent($merchant_biz_query_data, '48e70b51cab84019a2dddf2f03f13a38');
//        $merchant_biz_query_res = $this->execute($merchant_biz_query_data, $this->url.$this->merchant_biz_query);
//        print_r($merchant_biz_query_res);die;
        //HTTP Status 500 – Internal Server Error


        //商户结算卡变更
//        $merchant_bank_info_modify_data = [
//            'agent_no' => '11010200097',
//            'agent_apply_no' => '998972558691',
//            'merch_no' => '833226358120003',
//            'notify_url' => url('api/hkrt/merchant_bank_info_modify_notify_url'),
//            'bankcard_data' => json_encode([
//                'acc_name' => '艾飞',
//                'bank_code' => '105100000017',
//                'acc_type' => '10B',
//                'phone' => '18645178181',
//                'acc_no' => '6217000010044116492',
//                'bank_name' => '西直门支行',
//                'bank_city_code' => '110100',
//                'idcard_no' => '21010119900324007X',
//                'bank_province_code' => '110000'
//            ]),
//            'image_data' => json_encode([
//                'A28' => '',
//                'A9999' => 'c96650cbfd914754ac6cff24be4b89b3',
//                'A4' => ''
//            ]),
//            'accessid' => 'cpostest'
//        ];
//        $merchant_bank_info_modify_data['sign'] = $this->getSignContent($merchant_bank_info_modify_data, '48e70b51cab84019a2dddf2f03f13a38');
//        $merchant_bank_info_modify_res = $this->execute($merchant_bank_info_modify_data, $this->url.$this->merchant_bank_info_modify);
//        print_r($merchant_bank_info_modify_res);die;
        //{"return_code":"24008","return_msg":"服务商申请单号已存在","sign":"C1567FEC7A7A52E6B96B44BF8BC6ED13"}
        //{"apply_no":"200521120158412514975772","return_code":10000,"sign":"597C22FAA829D159DE38EFBA50BB0BF3"}


        //结算方式申请及变更
//        $merchant_withdrawal_apply_data = [
//            'accessid' => 'cpostest',
//            'agent_no' => '11010200097',
//            'merch_no' => '88888888',
//            'settlement_cycle' => 'D0',
//            'withdrawal_rate' => '0.6',
//            'withdrawal_feemin' => '1'
//        ];
//        $merchant_withdrawal_apply_data['sign'] = $this->getSignContent($merchant_withdrawal_apply_data, '48e70b51cab84019a2dddf2f03f13a38');
//        $merchant_withdrawal_apply_res = $this->execute($merchant_withdrawal_apply_data, $this->url.$this->merchant_withdrawal_apply);
//        print_r($merchant_withdrawal_apply_res);die;
        //{"return_code":23026,"return_msg":"对公商户仅支持T1结算方式","sign":"FDE2D227208291644E8F4116038A416E"}


        //商户入网信息修改
//        $merchant_modify_data = [
//            'agent_no' => '11010200097',
//            'merch_no' => '833581758120002',
//            'agent_apply_no' => '553697699204',
//            'notify_url' => url('api/hkrt/merchant_apply_notify_url'),
//            'merchant_data' => json_encode([
//                'big_merch_no' => '',
//                'contact_name' => '贾宝玉',
//                'address' => '北京市海淀区西直门金运大厦A座11楼',
//                'legal_cert_type' => '1',
//                'contact_phone' => '15822226795',
//                'area_code' => '110101',
//                'merch_type' => '10A',
//                'city_code' => '110100',
//                'province_code' => '110000',
//                'shop_name' => '测试部境内大商户',
//                'bus_area_code' => '110101',
//                'contact_email' => 'saas@hkrt.cn',
//                'merch_name' => '测试部境内大商户',
//                'ledger_receive_flag' => '0',
//                'legal_cert_no' => '231182199208198303',
//                'bus_address' => '北京市海淀区西直门金运大厦A座11楼',
//                'merch_short_name' => '人民币大商户',
//                'bus_license_no' => '91440300MA5FF5XEXC22',
//                'legal_name' => '张三',
//                'bus_city_code' => '110100',
//                'bus_province_code' => '110000',
//                'bus_scope_code' => '00010007'
//            ]),
//            'bankcard_data' => json_encode([
//                'acc_name' => '张三',
//                'bank_code' => '105100000017',
//                'acc_type' => '10A',
//                'phone' => '18645178182',
//                'acc_no' => '111101',
//                'bank_name' => '西直门支行',
//                'bank_city_code' => '110100',
//                'idcard_no' => '231182199208198303',
//                'bank_province_code' => '110000'
//            ]),
//            'image_data' => json_encode([
//                'A32' => '6ea605328e93413190cac49dd7965e59',
//                'A34' => '6ea605328e93413190cac49dd7965e59',
//                'A35' => '6ea605328e93413190cac49dd7965e59',
//                'A36' => '6ea605328e93413190cac49dd7965e59',
//                'A28' => '6ea605328e93413190cac49dd7965e59',
//                'A9998' => '',
//                'A9999' => '6ea605328e93413190cac49dd7965e59',
//                'A1' => '6ea605328e93413190cac49dd7965e59',
//                'A2' => '6ea605328e93413190cac49dd7965e59',
//                'A3' => '6ea605328e93413190cac49dd7965e59',
//                'A4' => '6ea605328e93413190cac49dd7965e59',
//                'A5' => '6ea605328e93413190cac49dd7965e59',
//                'A6' => '6ea605328e93413190cac49dd7965e59',
//                'A7' => '6ea605328e93413190cac49dd7965e59',
//                'A8' => '6ea605328e93413190cac49dd7965e59',
//                'A9' => '6ea605328e93413190cac49dd7965e59',
//                'A31' => '6ea605328e93413190cac49dd7965e59'
//            ]),
//            "accessid" => "cpostest"
//        ];
//        $merchant_modify_data['sign'] = $this->getSignContent($merchant_modify_data, '48e70b51cab84019a2dddf2f03f13a38');
//        $merchant_modify_res = $this->execute($merchant_modify_data, $this->url.$this->merchant_modify);
//        print_r($merchant_modify_res);die;
        //{"return_code":20122,"return_msg":"商户入网审核仅审核失败状态时支持修改"}
        //{"merch_no":"833581758120002","return_code":10000,"sign":"F07B3E86B0502C96C5393F8C0B97322A"}


        //商户入网申请
//        $merchant_apply_data = [
//            'agent_no' => '11010200097',
//            'agent_apply_no' => '990559944960',
//            'notify_url' => url('api/hkrt/merchant_apply_notify_url'),
//            'merchant_data' => json_encode([
//                'big_merch_no' => '',
//                'contact_name' => '测试联系人姓名',
//                'address' => '北京市海淀区西直门金运大厦A座11楼',
//                'legal_cert_type' => '1',
//                'contact_phone' => '18645178182',
//                'area_code' => '110101',
//                'merch_type' => '10A',
//                'city_code' => '110100',
//                'province_code' => '110000',
//                'shop_name' => '门店名称1',
//                'bus_area_code' => '110101',
//                'contact_email' => 'saas@hkrt.cn',
//                'merch_name' => '商户名称1',
//                'ledger_receive_flag' => '0',
//                'legal_cert_no' => '231182199208197717',
//                'bus_address' => '北京市海淀区西直门金运大厦A座11楼',
//                'merch_short_name' => '商户简称1',
//                'bus_license_no' => '111222333444555',
//                'legal_name' => '测试法人姓名',
//                'bus_city_code' => '110100',
//                'bus_province_code' => '110000',
//                'bus_scope_code' => '00010007'
//            ]),
//            'bankcard_data' => json_encode([
//                'acc_name' => '艾飞',
//                'bank_code' => '105100000017',
//                'acc_type' => '10A',
//                'phone' => '18645178182',
//                'acc_no' => '6217000010044116492',
//                'bank_name' => '西直门支行',
//                'bank_city_code' => '110100',
//                'idcard_no' => '231182199208197717',
//                'bank_province_code' => '110000'
//            ]),
//            'image_data' => json_encode([
//                'A32' => '',
//                'A34' => '',
//                'A35' => '',
//                'A36' => '',
//                'A28' => 'c0941d8a969b4e20a890be8f6edc2951',
//                'A9998' => '',
//                'A9999' => 'c0941d8a969b4e20a890be8f6edc2951',
//                'A1' => 'c0941d8a969b4e20a890be8f6edc2951',
//                'A2' => 'c0941d8a969b4e20a890be8f6edc2951',
//                'A3' => 'c0941d8a969b4e20a890be8f6edc2951',
//                'A4' => 'c0941d8a969b4e20a890be8f6edc2951',
//                'A5' => 'c0941d8a969b4e20a890be8f6edc2951',
//                'A6' => 'c0941d8a969b4e20a890be8f6edc2951',
//                'A7' => 'c0941d8a969b4e20a890be8f6edc2951',
//                'A8' => 'c0941d8a969b4e20a890be8f6edc2951',
//                'A9' => 'c0941d8a969b4e20a890be8f6edc2951',
//                'A31' => 'c0941d8a969b4e20a890be8f6edc2951'
//            ]),
//            'accessid' => 'cpostest'
//        ];
//        $merchant_apply_data['sign'] = $this->getSignContent($merchant_apply_data, '48e70b51cab84019a2dddf2f03f13a38');
//        $merchant_apply_res = $this->execute($merchant_apply_data, $this->url.$this->merchant_apply);
//        print_r($merchant_apply_res);die;
        //{"return_code":24008,"return_msg":"服务商申请单号已存在","sign":"C1567FEC7A7A52E6B96B44BF8BC6ED13"}
        //{"return_code":22047,"return_msg":"营业场所室内照2不能为空"}
        //{"return_code":20127,"return_msg":"营业执照编号已存在，不能重复入网"}


        //图片上传
//        $merchant_image_upload_data = [
//            'accessid' => 'cpostest',
//            'image' => 'imgToBase64data...'
//        ];
//        $merchant_image_upload_data['sign'] = $this->getSignContent($merchant_image_upload_data, '48e70b51cab84019a2dddf2f03f13a38');
//        $merchant_image_upload_res = $this->execute($merchant_image_upload_data, $this->url.$this->merchant_image_upload);
//        print_r($merchant_image_upload_res);die;
        //{"image_id":"4d1623047a054b4888acc4f3a5085810","return_code":10000,"sign":"D41317F8122123DB4C162E03F0579667"}


        //退款查询API
//        $polymeric_refundquery_data = [
//            'accessid' => 'cpostest',
//            'refund_no' => 'AL200521110347933669576047'
//        ];
//        $polymeric_refundquery_data['sign'] = $this->getSignContent($polymeric_refundquery_data, '48e70b51cab84019a2dddf2f03f13a38');
//        $polymeric_refundquery_res = $this->execute($polymeric_refundquery_data, $this->url.$this->polymeric_refundquery);
//        print_r($polymeric_refundquery_res);die;
        //{"total_amount":"0.01","refund_no":"AL200521110347933669576047","out_refund_no":"201905060000001","refund_amount":"-0.01","refund_status":1,"trade_begin_time":"2020-05-21 11:03:47","trade_end_time":"2020-05-21 11:03:50","channel_trade_no":"010520052111031308167MC","trade_type":"ALI","remanent_amount":"0.00","refunded_amount":"0.01","return_code":10000,"sign":"68623F7581471E8F2A9BAE508FA9C066"}


        //申请退款API
//        $polymeric_refund_data = [
//            'accessid' => 'cpostest',
//            'out_refund_no' => '201905060000001',
//            'refund_amount' => '0.01',
//            'trade_no' => 'AL200521101925278743626569',
//            'out_trade_no' => '20200325141703576',
//            'notify_url' => url('api/hkrt/return_pay_notify_url')
//        ];
//        $polymeric_refund_data['sign'] = $this->getSignContent($polymeric_refund_data, '48e70b51cab84019a2dddf2f03f13a38');
//        $polymeric_refund_res = $this->execute($polymeric_refund_data, $this->url.$this->polymeric_refund);
//        print_r($polymeric_refund_res);die;
        //{"refund_no":"AL200521110347933669576047","channel_trade_no":"010520052111031308167MC","refund_status":1,"trade_end_time":"2020-05-21 11:03:50","trade_type":"ALI","total_amount":"0.01","remanent_amount":"0.00","refunded_amount":"0.01","return_code":10000,"sign":"8E77C539E7287F2074A22C35AFE56DF9"}
        //{"return_code":30012,"return_msg":"交易订单号重复","sign":"386D75C2655FDE1D897A17BE6032404F"}


        //关闭订单API
//        $polymeric_closeorder_data = [
//            'accessid' => 'cpostest',
//            'out_trade_no' => '20200325141703576',
//            'trade_no' => 'AL200521101925278743626569'
//        ];
//        $polymeric_closeorder_data['sign'] = $this->getSignContent($polymeric_closeorder_data, '48e70b51cab84019a2dddf2f03f13a38');
//        $polymeric_closeorder_res = $this->execute($polymeric_closeorder_data, $this->url.$this->polymeric_closeorder);
//        print_r($polymeric_closeorder_res);die;
        //{"return_code":30017,"return_msg":"交易已经完成，无法关闭","sign":"6A7B709307717BBADCD48CC7495CA541"}


        //查询订单API
//        $polymeric_query_data = [
//            'accessid' => 'cpostest',
//            'out_trade_no' => '20200325141703576',
//            'trade_no' => 'AL200521101925278743626569'
//        ];
//        $polymeric_query_data['sign'] = $this->getSignContent($polymeric_query_data, '48e70b51cab84019a2dddf2f03f13a38');
//        $polymeric_query_res = $this->execute($polymeric_query_data, $this->url.$this->polymeric_query);
//        print_r($polymeric_query_res);die;
        //{"trade_no":"AL200325141943092770216077","out_trade_no":"20200325141703575","total_amount":"26.00","trade_status":"2","trade_begin_time":"2020-03-25 14:19:43","trade_end_time":"2020-03-25 14:22:53","error_code":"30001","error_msg":"交易已关闭","userid":"2088212026361584","trade_type":"ALI","alipay_no":"2020032522001461581430594633","channel_trade_no":"010920032514191297612MC","remanent_amount":"26.00","refunded_amount":"0.00","return_code":10000,"sign":"5FB22E875D27053F0CCF95A1EA886753"}
        //{"trade_no":"AL200521101925278743626569","out_trade_no":"20200325141703576","total_amount":"0.01","trade_status":"1","trade_begin_time":"2020-05-21 10:19:25","trade_end_time":"2020-05-21 10:19:25","userid":"2088902194581584","trade_type":"ALI","alipay_no":"2020051822001481581447791185","channel_trade_no":"010520052110191308129MC","remanent_amount":"0.01","refunded_amount":"0.00","return_code":10000,"sign":"4F8311E3DB6023D92F94CCAD72DEE610"}


        //公众号支付
//        $polymeric_jsapipay_data = [
//            'accessid' => 'cpostest',
//            'trade_type' => 'WX',
//            'merch_no' => '88888888',
//            'out_trade_no' => '2020032513292797',
//            'total_amount' => '0.01',
//            'appid' => 'WX88888888888',
//            'openid' => 'azasadrfdhgfffdderty',
//            'sn' => 'WSY196',
//            'pn' => 'S0000053',
//            'remark' => '公众号支付备注test',
//            'limit_pay' => '0',
////            'ledger_relation' => json_encode([
////                'receive_no' => '833584358120001',
////                'amt' => '1'
////            ]),
//            'goods_name' => '矿泉水'
//        ];
//        $polymeric_jsapipay_data['sign'] = $this->getSignContent($polymeric_jsapipay_data, '48e70b51cab84019a2dddf2f03f13a38');
////        print_r($polymeric_jsapipay_data);die;
//        $polymeric_jsapipay_res = $this->execute($polymeric_jsapipay_data, $this->url.$this->polymeric_jsapipay);
//        print_r($polymeric_jsapipay_res);die;
        //{"return_code":10002,"return_msg":"参数[openid]为空","sign":"C98455C7EEABE2F136E09D241E05DBAC"}


        //被扫
        $passive_pay_data = [
            'accessid' => 'cpostest',
            'auth_code' => '282906393717230652',
            'merch_no' => '88888888',
            'notify_url' => 'https://test.yunsoyi.cn/api/hkrt/pay_notify_url',
            'out_trade_no' => 'aliscan20200525104056178396355',
//            'sign' => '6B4FCA883B319C259A1799064ADA88D9',
            'total_amount' => '0.01',
        ];
//        $passive_pay_data = [
//            'accessid' => 'cpostest',
//            'merch_no' => '88888888', //商户编号
//            'out_trade_no' => '20200325141703576', //服务商交易订单号
//            'total_amount' => '0.01', //订单总金额，以元为单位
//            'auth_code' => '287667597892433290', //支付授权码
//            'notify_url' => url('api/hkrt/pay_notify_url'), //支付成功后的通知地址
//            'sn' => 'WSY196', //厂商终端号
//            'pn' => 'S0000053', //SAAS终端号,标准服务商必填
//            'remark' => '备注', //交易备注
//            'limit_pay' => '0', //限制贷记卡支付,0-不限制贷记卡支付;1-禁止使用贷记卡支付;默认为0
////            'ledger_relation' => [ //分账交易关系组
////                'receive_no' => '833584358120001', //收账方(海科商户号)ledger_relation组内参数
////                'amt' => '1' //分账金额（单位：元）
////            ],
////            'goods_name' => '矿泉水', //商品名称(仅限支付宝微信被扫，银联二维码上送字段也不会被使用)
//        ];
//        print_r($passive_pay_data);
        $passive_pay_data['sign'] = $this->getSignContent($passive_pay_data, '48e70b51cab84019a2dddf2f03f13a38');
//        print_r($passive_pay_data['sign']);die; //506F42C37A0F72F8D0917179C49706BA
        $passive_pay_res = $this->execute($passive_pay_data, $this->url.$this->polymeric_passivepay);
        print_r($passive_pay_res);die;
        //{"out_trade_no":"20200325141703576","channel_trade_no":"010520052110191308129MC","trade_no":"AL200521101925278743626569","trade_status":1,"trade_type":"ALI","trade_end_time":"2020-05-21 10:19:25","return_code":10000,"sign":"2FB181C820556368ECBCE8B636707176"}
        //{"return_code":30012,"return_msg":"交易订单号重复","sign":"386D75C2655FDE1D897A17BE6032404F"}

    }


}
