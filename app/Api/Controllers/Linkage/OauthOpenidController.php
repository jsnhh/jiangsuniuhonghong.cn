<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/07/27
 * Time: 下午5:13
 */
namespace App\Api\Controllers\Linkage;


use App\Api\Controllers\Config\LinkageConfigController;
use App\Api\Controllers\Config\TfConfigController;
use EasyWeChat\Factory;
use Illuminate\Http\Request;

class OauthOpenidController extends \App\Api\Controllers\DevicePay\BaseController
{

    //
    public function oauth_sign_openid(Request $request)
    {
        //第三方传过来的信息
        $data = $request->all(); //获取请求参数

        //验证签名
        $check = $this->check_md5($data);
        if ($check['return_code'] == 'FALL') {
            return $this->return_data($check);
        }

        $config_id = $data['config_id'];
        $callback_url = $data['callback_url'];

        $config = new LinkageConfigController();
        $linkage_config = $config->linkage_config($config_id);
        if (!$linkage_config) {
            return json_encode([
                'status' => '2',
                'message' => '联动优势支付配置不存在，请检查配置'
            ]);
        }

        $config = [
            'app_id' => $linkage_config->wxAppid,
            'scope' => 'snsapi_base',
            'oauth' => [
                'scopes' => ['snsapi_base'],
                'response_type' => 'code',
                'callback' => url('api/linkage/weixin/oauth_sign_callback_openid?wx_AppId=' . $linkage_config->wxAppid . '&wx_Secret=' . $linkage_config->wxSecret . '&callback_url=' . $callback_url)
            ]
        ];
        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;

        return $oauth->redirect();
    }


    //获取openid
    public function oauth_sign_callback_openid(Request $request)
    {
        $callback_url = $request->get('callback_url');
        $code = $request->get('code');
        $wx_AppId = $request->get('wx_AppId');
        $wx_Secret = $request->get('wx_Secret');
        $config = [
            'app_id' => $wx_AppId,
            "secret" => $wx_Secret,
            "code" => $code,
            "grant_type" => "authorization_code"
        ];

        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;
        $user = $oauth->user();
        $open_id = $user->getId();

        return redirect($callback_url . '?open_id=' . $open_id);
    }


}
