<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2021/01/22
 * Time: 17:49
 */

namespace App\Api\Controllers\YinSheng;


use App\Api\Controllers\BaseController;
use App\Api\Controllers\Merchant\TemplateMessageController;
use App\Api\Controllers\WftPay\BaseController as WftPayBaseController;
use App\Common\PaySuccessAction;
use App\Models\MerchantWalletDetail;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\RefundOrder;
use App\Models\StorePayWay;
use App\Models\UserWalletDetail;
use App\Models\YinshengContractInfos;
use App\Models\YinshengStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class NotifyController extends BaseController
{
    //银盛支付 支付回调
    public function pay_notify_url(Request $request)
    {
        try {
            $original_data = $request->getContent();
            if ($original_data) {
                $payData = explode('&', $original_data);
                $data = [];
                foreach ($payData as $val) {
                    $valData = explode('=', $val);
                    $key = $valData[0];
                    $val = $valData[1];
                    $data[$key] = $val;
                }
                Log::info('银盛支付支付-回调转换');
                Log::info(json_encode($data));
            }
            if (isset($data['out_trade_no'])) {
                $out_trade_no = $data['out_trade_no']; //银盛支付合作商户网站唯一订单号

                $day = date('Ymd', time());
                $table = 'orders_' . $day;
                if (Schema::hasTable($table)) {
                    $order = DB::table($table)->where('out_trade_no', $out_trade_no)->first();
                } else {
                    $order = Order::where('out_trade_no', $out_trade_no)->first();
                    if (!$order) {
                        $order = Order::where('trade_no', $out_trade_no)->first();
                    }
                }

                //订单存在
                if ($order) {
                    //系统订单未成功
                    if ($order->pay_status == 2) {
                        $trade_no = isset($data['trade_no']) ? $data['trade_no'] : ""; //可空,该交易在银盛支付系统中的交易流水号
                        $other_no = isset($return['data']['channel_send_sn']) ? $return['data']['channel_send_sn'] : ''; //可空,该交易在银盛支付系统中的交易流水号
                        $pay_time = date('Y-m-d H:i:s', time());
                        $buyer_pay_amount = isset($data['total_amount']) ? $data['total_amount'] : $order->total_amount; //可空,该笔订单的资金总额，单位为RMB-Yuan。取值范围为[0.01，100000000.00]，精确到小数点后两位
                        $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                        if ($data['trade_status'] == 'TRADE_SUCCESS') {
                            $pay_status = 1;
                            $pay_status_desc = '支付成功';
                        } elseif ($data['trade_status'] == 'TRADE_CLOSED') {
                            $pay_status = 4;
                            $pay_status_desc = '客户主动关闭订单';
                        } elseif ($data['trade_status'] == 'WAIT_BUYER_PAY') {
                            $pay_status = 2;
                            $pay_status_desc = '等待买家付款';
                        } elseif ($data['trade_status'] == 'TRADE_PROCESS') {
                            $pay_status = 2;
                            $pay_status_desc = '交易正在处理中';
                        } elseif ($data['trade_status'] == 'TRADE_PART_REFUND') {
                            $pay_status = 6;
                            $pay_status_desc = '部分退款成功';
                        } elseif ($data['trade_status'] == 'TRADE_ALL_REFUND') {
                            $pay_status = 6;
                            $pay_status_desc = '全部退款成功';
                        } elseif ($data['trade_status'] == 'TRADE_FAILD') {
                            $pay_status = 3;
                            $pay_status_desc = '交易失败';
                        } else {
                            $pay_status = 2;
                            $pay_status_desc = '等待支付';
                        }

                        $in_data = [
                            'status' => '1',
                            'pay_status' => $pay_status,
                            'pay_status_desc' => $pay_status_desc,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'receipt_amount' => $buyer_pay_amount,
                            'other_no' => $other_no,

                        ];
                        if ($trade_no) $in_data['trade_no'] = $trade_no;
                        $this->update_day_order($in_data, $out_trade_no);

                        if (strpos($out_trade_no, 'scan')) {

                        } else {
                            //支付成功后的动作
                            $data = [
                                'ways_type' => $order->ways_type,
                                'company' => $order->company,
                                'ways_type_desc' => $order->ways_type_desc,
                                'source_type' => '14000',//返佣来源
                                'source_desc' => '银盛支付',//返佣来源说明
                                'total_amount' => $order->total_amount,
                                'out_trade_no' => $order->out_trade_no,
                                'other_no' => $order->other_no,
                                'rate' => $order->rate,
                                'fee_amount' => $order->fee_amount,
                                'merchant_id' => $order->merchant_id,
                                'store_id' => $order->store_id,
                                'user_id' => $order->user_id,
                                'config_id' => $order->config_id,
                                'store_name' => $order->store_name,
                                'ways_source' => $order->ways_source,
                                'pay_time' => $pay_time,
                                'device_id' => $order->device_id,
                            ];
                            if (Cache::has('console_out_trade_no_' . $out_trade_no)) {
                                Log::info('--ysepay--检查是否操作---Cache.  ---console_out_trade_no_:' . $out_trade_no);
                                return;
                            }

                            Cache::add('console_out_trade_no_' . $out_trade_no, '1', 1);
                            PaySuccessAction::action($data);

                        }
                    }
                }
            }

            return 'success';
        } catch (\Exception $ex) {
            Log::info('银盛支付支付异步报错');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }
    }


    //todo:银盛支付 退款回调
    public function refund_notify_url(Request $request)
    {
        try {
            $data = $request->getContent();
            Log::info('银盛支付退款-回调-原始数据');
            Log::info($data);

            if (isset($data['out_trade_no'])) {
                $out_trade_no = $data['out_trade_no'];
                $a = array('a', '_', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
                $b = str_ireplace($a, "", $out_trade_no);
                $day = substr($b, 0, 8);
                $table = 'orders_' . $day;

                $out_trade_no = substr($out_trade_no, 0, strlen($out_trade_no) - 4);

                if (Schema::hasTable($table)) {
                    $order = DB::table($table)->where('out_trade_no', $out_trade_no)->first();
                } else {
                    $order = Order::where('out_trade_no', $out_trade_no)->first();
                }

                //订单存在
                if ($order && $order->pay_status == '1') {
                    if ($data['result_code'] == "0") {
                        $refund_amount = $data["refund_fee"];
                        $update_data = [
                            'status' => 6,
                            'pay_status' => 6,
                            'pay_status_desc' => '已退款',
                            'refund_amount' => $order->refund_amount + $refund_amount,
                            'fee_amount' => 0,
                        ];
                        if (Schema::hasTable($table)) {
                            DB::table($table)->where('out_trade_no', $out_trade_no)
                                ->update($update_data);
                        } else {
                            Order::where('out_trade_no', $out_trade_no)
                                ->update($update_data);
                        }
                        if (Schema::hasTable('order_items')) {
                            OrderItem::where('out_trade_no', $out_trade_no)->update($update_data);
                        }

                        RefundOrder::create([
                            'ways_source' => $order->ways_source,
                            'type' => $order->ways_type,
                            'refund_amount' => $refund_amount, //退款金额
                            'refund_no' => $data['refund_id'], //退款单号
                            'store_id' => $order->store_id,
                            'merchant_id' => $order->merchant_id,
                            'out_trade_no' => $order->out_trade_no,
                            'trade_no' => $order->other_no
                        ]);

                        //返佣去掉
                        UserWalletDetail::where('out_trade_no', $order->out_trade_no)->update([
                            'settlement' => '03',
                            'settlement_desc' => '退款订单',
                        ]);
                        MerchantWalletDetail::where('out_trade_no', $order->out_trade_no)->update([
                            'settlement' => '03',
                            'settlement_desc' => '退款订单',
                        ]);
                    }
                }
            }

            return 'success';
        } catch (\Exception $ex) {
            Log::info('银盛支付-退款回调异步报错');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }
    }

    /**
     * 客户进件异步通知（notifyType = 1）
     * 合同签约异步通知（notifyType = 2）
     * 入网资料变更通知（notifyType = 3）
     * 费率变更异步通知（notifyType = 5）
     * 报备异步通知（notifyType = 6）
     */
    public function merchant_notify_url(Request $request)
    {
        try {
            $data = $request->all();
            Log::info('银盛-异步通知-回调');
            Log::info($data);

            $bizContent = json_decode($data['bizContent'], true);

            //状态00-成功、10-转人工审核、90-审核拒绝、99-失败 ，非00建议解析note节点并保存，以便与银盛核对并处理
            $status = $bizContent['status'];

            $notifyType = $bizContent['notifyType'];
            if ($notifyType == '1') {
                $cust = $bizContent['cust']; //客户信息
                if ($cust) {
                    $sysFlowId = isset($cust['sysFlowId']) ? $cust['sysFlowId'] : '';

                    $YinshengStore = YinshengStore::where('sys_flowId', $sysFlowId)->first();
                    if ($YinshengStore) {
                        $YinshengStore->update([
                            'aduit_status' => $status,
                            'audit_msg' => $cust['note'],
                            'notify_type' => $notifyType,
                            'cust_id' => $cust['custId'] ?? ''
                        ]);
                        $storePayWay = StorePayWay::where('store_id', $YinshengStore->store_id)
                            ->where('company', 'ysepay')
                            ->get();

                        foreach ($storePayWay as $k => $v) {
                            $ways = StorePayWay::where('store_id', $YinshengStore->store_id)
                                ->where('ways_type', $v->ways_type)
                                ->first();
                            if ($ways) {
                                $ways->update([
                                    'status' => 2,
                                    'status_desc' => $cust['note']
                                ]);
                                $ways->save();
                            }
                        }
                    }
                }
            } else if ($notifyType == '2') { //合同签约
                $auth = $bizContent['auth']; //客户信息
                Log::info('-----合同签约---auth----' . json_encode($auth));
                $contractInfos = YinshengContractInfos::where('auth_id', $auth['authId'])->first();
                if ($contractInfos) {
                    $YinshengStore = YinshengStore::where('store_id', $contractInfos->store_id)->first();
                    $storePayWay = StorePayWay::where('store_id', $YinshengStore->store_id)
                        ->where('company', 'ysepay')
                        ->get();
                    if ($status == '00') {
                        $contractInfos->update([
                            'sign_status' => $status,
                            'sign_msg' => $auth['note'],
                            'mer_code' => $auth['mercId']
                        ]);


                        if ($YinshengStore) {
                            $YinshengStore->update([
                                'mer_code' => $auth['mercId'],
                                'notify_type' => $notifyType
                            ]);

                            $YinshengStore->save();


                            foreach ($storePayWay as $k => $v) {
                                $ways = StorePayWay::where('store_id', $YinshengStore->store_id)
                                    ->where('ways_type', $v->ways_type)
                                    ->first();
                                if ($ways) {
                                    $ways->update([
                                        'status' => 1,
                                        'status_desc' => $auth['note']
                                    ]);
                                    $ways->save();
                                }
                            }
                        }

                    } else {
                        $contractInfos->update([
                            'sign_status' => $status,
                            'sign_msg' => $auth['note']
                        ]);
                        if ($YinshengStore) {
                            $YinshengStore->update([
                                'status' => $status,
                                'aduit_status' => $status,
                                'audit_msg' => $auth['note'],
                                'notify_type' => $notifyType
                            ]);
                        }

                        foreach ($storePayWay as $k => $v) {
                            $ways = StorePayWay::where('store_id', $YinshengStore->store_id)
                                ->where('ways_type', $v->ways_type)
                                ->first();
                            if ($ways) {
                                $ways->update([
                                    'status_desc' => $auth['note']
                                ]);
                                $ways->save();
                            }
                        }
                    }
                }
            } else if ($notifyType == '3') { //入网资料变更、结算信息变更 通知（notifyType = 3）
                $change = $bizContent['change'];
                Log::info('-----入网资料变更、结算信息变更---notifyType--->' . json_encode($change));

                $changeSysFlowId = $change['changeSysFlowId'];

                $YinshengStore = YinshengStore::where('change_sys_flowId', $changeSysFlowId)->first();
                if ($YinshengStore) {
                    $storePayWay = StorePayWay::where('store_id', $YinshengStore->store_id)
                        ->where('company', 'ysepay')
                        ->get();

                    foreach ($storePayWay as $k => $v) {
                        $ways = StorePayWay::where('store_id', $YinshengStore->store_id)
                            ->where('ways_type', $v->ways_type)
                            ->first();
                        if ($status == '00') {
                            if ($ways) {
                                $ways->update([
                                    'status' => 1,
                                    'status_desc' => '审核通过'
                                ]);
                                $ways->save();
                            }
                        } else {
                            if ($ways) {
                                $ways->update([
                                    'status' => 2,
                                    'status_desc' => $change['note']
                                ]);
                                $ways->save();
                            }
                        }

                    }
                }

            } else if ($notifyType == '5') { //费率变更异步通知（notifyType = 5）
                $change = $bizContent['change'];
                Log::info('-----费率变更异步通知---notifyType--->' . json_encode($change));

                $changeSysFlowId = $change['changeSysFlowId'];

                $YinshengStore = YinshengStore::where('change_sys_flowId', $changeSysFlowId)->first();
                if ($YinshengStore) {
                    $storePayWay = StorePayWay::where('store_id', $YinshengStore->store_id)
                        ->where('company', 'ysepay')
                        ->get();

                    foreach ($storePayWay as $k => $v) {
                        $ways = StorePayWay::where('store_id', $YinshengStore->store_id)
                            ->where('ways_type', $v->ways_type)
                            ->first();
                        if ($status == '00') {
                            if ($ways) {
                                $ways->update([
                                    'status' => 1,
                                    'status_desc' => '审核通过'
                                ]);
                                $ways->save();
                            }
                        } else {
                            if ($ways) {
                                $ways->update([
                                    'status' => 2,
                                    'status_desc' => $change['note']
                                ]);
                                $ways->save();
                            }
                        }

                    }
                }

            } else if ($notifyType == '6') { //报备异步通知（notifyType = 6）
                $report = $bizContent['report'];
                Log::info('-----报备异步通知---notifyType--->' . json_encode($report));
                $mer_code = $report['mercId'];
                $YinshengStore = YinshengStore::where('mer_code', $mer_code)->select('*')->first();
                if ($YinshengStore) {
                    $thridMercList = $report['thridMercList'];
                    foreach ($thridMercList as $thridMerc) {
                        if ($thridMerc['apprSts'] == '00') {
                            if ($thridMerc['reportChannel'] == 'NUCC_WECHAT') {
                                YinshengStore::where('mer_code', $mer_code)->update(['nucc_wechat_mer' => $thridMerc['thridMercId']]);
                            }
                            if ($thridMerc['reportChannel'] == 'CUPS_WECHAT') {
                                YinshengStore::where('mer_code', $mer_code)->update(['cups_wechat_mer' => $thridMerc['thridMercId']]);
                            }
                            if ($thridMerc['reportChannel'] == 'NUCC_ALIPAY') {
                                YinshengStore::where('mer_code', $mer_code)->update(['nucc_alipay_mer' => $thridMerc['thridMercId']]);
                            }
                            if ($thridMerc['reportChannel'] == 'CUPS_ALIPAY') {
                                YinshengStore::where('mer_code', $mer_code)->update(['cups_alipay_mer' => $thridMerc['thridMercId']]);
                            }
                        }
                    }
                }
            }

            return 'success';

        } catch (\Exception $ex) {
            Log::info('银盛-异步通知-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }
    }

}
