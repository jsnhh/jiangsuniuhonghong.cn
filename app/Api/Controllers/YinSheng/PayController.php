<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2021/01/22
 * Time: 17:49
 */

namespace App\Api\Controllers\YinSheng;


use function EasyWeChat\Kernel\Support\get_client_ip;
use function EasyWeChat\Kernel\Support\get_server_ip;
use Illuminate\Support\Facades\Log;

class PayController extends BaseController
{

    //付款码支付 b_2_c -1系统错误 1-交易成功 2-失败 3-等待买家付款 4-客户主动关闭订单 5-交易正在处理中 6-部分退款成功 7-全部退款成功
    public function scan_pay($data)
    {
        try {
            $partner_id = $data['partner_id']; //商户号
            $out_trade_no = $data['out_trade_no']; //商户生成的订单号,生成规则前8位必须为交易日期,如20180525,范围跨度支持包含当天在内的前后一天,且只能由大小写英文字母,数字,下划线及横杠组成
            $ways_source = $data['ways_source']; //
            $body = $data['body']; //商品的标题,交易标题,订单标题,订单关键字等,最长为250个汉字
            $total_amount = $data['total_amount']; //该笔订单的资金总额,单位为RMB-元.取值范围为[0.01,100000000.00],精确到小数点后两位
            $store_name = $data['store_name']; //收款方银盛支付客户名(注册时公司名称)
            $business_code = $data['business_code']; //业务代码
            $code = $data['code']; //扫码支付授权码,设备读取用户展示的条码或者二维码信息
            $device_id = $data['device_id'] ?? ''; //终端设备号.注:当bank_type域为中国银联-9001002时,为必填
            $seller_id = $data['seller_id'] ?? ''; //商户号
            $req_data = [
                'method' => 'ysepay.online.barcodepay', //接口类型
                'partner_id' => $partner_id, //
                'timestamp' => date('Y-m-d H:i:s', time()), //32,发送请求的时间,格式"yyyy-MM-dd HH:mm:ss"
                'charset' => 'utf-8', //商户网站使用的编码格式
                'sign_type' => 'RSA',
                'notify_url' => url('/api/ysepay/pay_notify_url'), //银盛支付服务器主动通知商户网站里指定的页面 http 路径，支持多个 url 进行异步通知，多个 url用分隔符“,”分开，格式如：url1,url2,url3
                'version' => '3.8', //接口版本
                'tran_type' => '1'
            ];

            $biz_content = [
                'out_trade_no' => $out_trade_no,
                'shopdate' => date('Ymd'), //商户日期(该参数做交易与查询时需要一致) 该日期需在当日的前后一天时间范围之内
                'subject' => $body,
                'total_amount' => $total_amount,
                'seller_id' => $seller_id, //收款方银盛支付用户号(商户号)
                //'seller_name' => $store_name,
                'timeout_express' => '15d', //设置未付款交易的超时时间，一旦超时，该笔交易就会自动被关闭。(需申请业务权限，权限未开通情况下该参数不生效，默认未付款交易的超时时间为 7d).取值范围：1m～15d。m-分钟，h-小时，d-天。该参数数值不接受小数点，如1.5h，可转换为 90m。注意：设置了未付款交易超时时间的情况下，若我司在限定时间内没有收到成功支付通知，则会关闭交易，关闭后该笔交易若付款方支付成功的情况下，会自动原路退款至付款方
                'business_code' => $business_code,
                'auth_code' => $code,
            ];

            if ($ways_source == 'alipay') {
                $biz_content['bank_type'] = '1903000'; //二维码行别，微信-1902000,支付宝-1903000,中国银联-9001002,苏宁-1905000
                $biz_content['scene'] = 'bar_code'; //支付场景,条码支付取值:bar_code;声波支付取值:wave_code.注:当bank_type域为支付宝-1903000时,为必填
            } elseif ($ways_source == 'weixin') {
                $biz_content['bank_type'] = '1902000';
            } elseif ($ways_source == 'unionpay') {
                $biz_content['bank_type'] = '9001002';
                $biz_content['device_info'] = $device_id;
            }

            $req_data['biz_content'] = json_encode($biz_content, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            $sing = $this->sign($req_data);
            $req_data['sign'] = $sing;
            Log::info('银盛支付-付款码支付-入参');
            Log::info($req_data);
            $res = $this->pay_post($this->payUrl, $req_data); //1-成功;2-失败
            Log::info('银盛支付-付款码支付-结果');
            Log::info($res);
            $return_data = json_decode($res, true);
            $resDate = $return_data['ysepay_online_barcodepay_response'];
            //业务成功
            if ($resDate['code'] == '10000') {
                //交易成功，且可对该交易做操作，如：多级分润、退款等
                if ($resDate['trade_status'] == 'TRADE_SUCCESS') {
                    return [
                        'status' => 1,
                        'message' => '交易成功',
                        'data' => $resDate
                    ];
                }
                elseif ($resDate['trade_status'] == 'TRADE_PROCESS') {
                    return [
                        'status' => 2,
                        'message' => '交易正在处理中',
                        'data' => $resDate
                    ];
                } else {
                    return [
                        'status' => 0,
                        'message' => '交易失败',
                        'data' => $resDate
                    ];
                }

            } else {
                return [
                    'status' => 0,
                    'message' => isset($resDate['sub_msg']) ? $resDate['sub_msg'] : '银盛支付付款码支付,未知错误'
                ];
            }


        } catch (\Exception $ex) {
            Log::info('银盛支付-被扫-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => -1,
                'message' => '系统繁忙，请稍候再试'
            ];
        }
    }


    //二维码支付 -1系统错误 1-交易成功 2-失败 3-等待买家付款 4-客户主动关闭订单 5-交易正在处理中 6-部分退款成功 7-全部退款成功
    public function send_qr($data)
    {
        try {
            $partner_id = $data['partner_id']; //商户号
            $return_url = $data['return_url'] ?? ''; //可空,同步通知地址
            $out_trade_no = $data['out_trade_no']; //商户生成的订单号,生成规则前8位必须为交易日期,如20180525,范围跨度支持包含当天在内的前后一天,且只能由大小写英文字母、数字、下划线及横杠组成
            $body = $data['body']; //商品的标题/交易标题/订单标题/订单关键字等。该参数最长为 250 个汉字
            $total_amount = $data['total_amount']; //订单的资金总额，单位为 RMB-Yuan。取值范围为[0.01，100000000.00]，精确到小数点后两位。Number(10,2)指10位长度，2位精度
            $store_name = $data['store_name']; //收款方银盛支付客户名
            $business_code = $data['business_code']; //业务代码
            $ways_source = $data['ways_source']; //
            $public_key = isset($data['public_key']) ? public_path() . $data['public_key'] : ''; //验签公钥
            $private_key = isset($data['private_key']) ? public_path() . $data['private_key'] : ''; //加签私钥
            $pfx_password = $data['pfx_password']; //商户私钥证书密码

            $req_data = array();
            $req_data['method'] = 'ysepay.online.qrcodepay'; //接口名称
            $req_data['partner_id'] = $partner_id;
            $req_data['timestamp'] = date('Y-m-d H:i:s', time());
            $req_data['charset'] = $this->_charset;
            $req_data['sign_type'] = $this->_sign_type;
            $req_data['notify_url'] = url('/api/yinsheng/pay_notify_url');
            $req_data['version'] = $this->_version;
            if ($return_url) $req_data['return_url'] = $return_url;

            $biz_content_arr = array(
                "out_trade_no" => $out_trade_no,
                "shopdate" => $this->datetime2string(date('Ymd')),
                "subject" => $body,
                "total_amount" => $total_amount,
                "seller_id" => $partner_id,
                "seller_name" => $store_name,
                "timeout_express" => "24h", //设置未付款交易的超时时间，一旦超时，该笔交易就会自动被关闭。(需申请业务权限，权限未开通情况下该参数不生效，默认未付款交易的超时时间为7d)取值范围：1m～15d。m-分钟，h-小时，d-            天。该参数数值不接受小数点，如 1.5h，可转换为 90m。注意：设置了未付款交易超时时间的情况下，若我司在限定时间内没有收到成功支付通知，则会关闭交易，关闭后该笔交易若付款方支付成功的情况下，会自动原路退款至付款方
                "business_code" => $business_code
            );
            if ($ways_source == 'alipay') {
                $biz_content_arr['bank_type'] = '1903000'; //二维码行别，微信-1902000,支付宝-1903000,QQ扫码-1904000,银联扫码-9001002,招商银行-3085840
            } elseif ($ways_source == 'weixin') {
                $biz_content_arr['bank_type'] = '1902000';
            } elseif ($ways_source == 'unionpay') {
                $biz_content_arr['bank_type'] = '9001002';
            }
            $req_data['biz_content'] = json_encode($biz_content_arr, JSON_UNESCAPED_UNICODE); //构造字符串
            $req_data['sign'] = $this->sign($req_data);

            Log::info('银盛支付-二维码支付-入参');
            Log::info($req_data);
            $res = $this->sendQrCurlRequest($req_data, $public_key); //1-成功 2-失败
            Log::info('银盛支付-二维码支付-结果');
            Log::info($res);


            //业务成功
            if ($res['status'] == 1) {
                $return_data = json_decode($res['data'], true);
                if (isset($return_data['trade_status'])) {
                    //交易成功，且可对该交易做操作，如：多级分润、退款等
                    if ($return_data['trade_status'] == 'TRADE_SUCCESS') {
                        return [
                            'status' => 1,
                            'message' => '交易成功',
                            'data' => $return_data
                        ];
                    } //交易创建,等待买家付款
                    elseif ($return_data['trade_status'] == 'TRADE_PROCESS') {
                        return [
                            'status' => 2,
                            'message' => '等待买家付款',
                            'data' => $return_data
                        ];
                    }else{
                        return [
                            'status' => 0,
                            'message' => '支付失败',
                            'data' => $return_data
                        ];
                    }
                } else {
                    return [
                        'status' => 2,
                        'message' => isset($return_data['sub_msg']) ? $return_data['sub_msg'] : '业务异常',
                        'data' => $return_data
                    ];
                }
            } else {
                return [
                    'status' => 2,
                    'message' => isset($res['message']) ? $res['message'] : '二维码支付 unknown error'
                ];
            }
        } catch (\Exception $ex) {
            Log::info('银盛支付-二维码支付-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => -1,
                'message' => '系统繁忙，请稍候再试'
            ];
        }
    }


    //JSAPI支付  -1系统错误 1-成功 2-失败
    public function qr_submit($data)
    {
        try {
            $partner_id = $data['partner_id']; //商户号
            $sub_openid = $data['sub_openid']; //
            $out_trade_no = $data['out_trade_no']; //商户生成的订单号,生成规则前8位必须为交易日期,如20180525,范围跨度支持包含当天在内的前后一天,且只能由大小写英文字母、数字、下划线及横杠组成
            $body = $data['body']; //商品的标题/交易标题/订单标题/订单关键字等。该参数最长为 250 个汉字
            $total_amount = $data['total_amount']; //订单的资金总额，单位为 RMB-Yuan。取值范围为[0.01，100000000.00]，精确到小数点后两位。Number(10,2)指10位长度，2位精度
            $seller_id = $data['seller_id']; //收款方银盛支付客户名
            $business_code = $data['business_code']; //业务代码
            $ways_source = $data['ways_source']; //支付方式
            $appid = $data['appid'];
            $req_data = [
                'partner_id' => $partner_id, //商户号
                'timestamp' => date('Y-m-d H:i:s'), //交易开始时间
                'charset' => 'utf-8', //商户网站使用的编码格式
                'sign_type' => 'RSA',
                'notify_url' => url('/api/ysepay/pay_notify_url'), //回调地址
                'version' => '3.5', //接口版本号
            ];

            $biz_content = [
                'out_trade_no' => $out_trade_no,
                'subject' => $body,
                'total_amount' => $total_amount, //金额 单位 元
                'seller_id' => $seller_id, // 收款方银盛支付用户号（商户号）
                'timeout_express' => '30m', //超时关闭  30分钟
                'business_code' => $business_code,
                //'appid' => $appid
            ];
            if ($ways_source == 'alipay') {
                $req_data['method'] = 'ysepay.online.alijsapi.pay';
                $biz_content['buyer_id'] = $sub_openid;
            } elseif ($ways_source == 'weixin') {
                $req_data['method'] = 'ysepay.online.weixin.pay';
                $biz_content['appid'] = $appid;
                $biz_content['sub_openid'] = $sub_openid;
            } elseif ($ways_source == 'unionpay') {
                $req_data['method'] = 'ysepay.online.cupmulapp.qrcodepay';
                $biz_content['spbill_create_ip'] = '154.8.143.104';
                $biz_content['bank_type'] = '9001002';
                $biz_content['userId'] = $sub_openid;
                $biz_content['allow_repeat_pay'] = 'Y';
                $biz_content['shopdate'] = date('Ymd');
            }
            $req_data['biz_content'] = json_encode($biz_content, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            $sing = $this->sign($req_data);
            $req_data['sign'] = $sing;
            Log::info('银盛支付-jsapiPay-入参');
            Log::info($req_data);
            $res = $this->pay_post($this->payUrl, $req_data);
            Log::info('银盛支付-jsapiPay-结果');
            Log::info($res);
            //业务成功
            $return_data = json_decode($res, true);
            if ($ways_source == 'alipay') {
                $resDate = $return_data['ysepay_online_alijsapi_pay_response'];
            }
            if ($ways_source == 'weixin') {
                $resDate = $return_data['ysepay_online_weixin_pay_response'];
            }
            if ($ways_source == 'unionpay') {
                $resDate = $return_data['ysepay_online_cupmulapp_qrcodepay_response'];
            }
            if ($resDate['code'] == '10000') {
                if ($resDate['trade_status'] == 'TRADE_SUCCESS') {
                    return [
                        'status' => 1,
                        'message' => '交易成功',
                        'data' => $resDate
                    ];
                } elseif ($resDate['trade_status'] == 'TRADE_SUCCESS') {
                    return [
                        'status' => 1,
                        'message' => '交易创建，等待买家付款',
                        'data' => $resDate
                    ];
                } elseif ($resDate['trade_status'] == 'WAIT_BUYER_PAY') {
                    return [
                        'status' => 1,
                        'message' => '交易创建，等待买家付款',
                        'data' => $resDate
                    ];
                } elseif ($resDate['trade_status'] == 'TRADE_FAILD') {
                    return [
                        'status' => 0,
                        'message' => '支付失败',
                        'data' => $resDate
                    ];
                }

            } else {
                return [
                    'status' => 0,
                    'message' => $resDate['sub_msg']
                ];
            }
        } catch (\Exception $ex) {
            Log::info('银盛支付-jsapiPay-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return $this->responseDataJson(-1);
        }
    }


    /**
     * 查询订单 -1系统错误 1-成功 2-失败
     *
     * @param $data
     * @return array
     */
    public function order_query($data)
    {
        try {
            $partner_id = $data['partner_id'];
            $out_trade_no = $data['out_trade_no'];
            $trade_no = $data['trade_no'];
            $shopDate = $data['shop_date'];
            $seller_id = $data['seller_id'];
            $req_data = [
                'method' => 'ysepay.online.trade.order.query', //接口类型
                'partner_id' => $partner_id, //
                'timestamp' => date('Y-m-d H:i:s', time()), //32,发送请求的时间,格式"yyyy-MM-dd HH:mm:ss"
                'charset' => 'utf-8', //商户网站使用的编码格式
                'sign_type' => 'RSA',
                'version' => '3.4' //接口版本
            ];
            $biz_content = [
                'out_trade_no' => $out_trade_no,
                'shopdate' => date('Y-m-d H:i:m', strtotime($shopDate)), //商户日期(该参数做交易与查询时需要一致) 该日期需在当日的前后一天时间范围之内
                'trade_no' => $trade_no,
                'seller_id' => $seller_id, //收款方银盛支付用户号(商户号)
            ];
            $req_data['biz_content'] = json_encode($biz_content, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            $sign = $this->sign($req_data);
            $req_data['sign'] = $sign;
            Log::info('银盛支付-查询订单-入参');
            Log::info($req_data);
            $res = $this->pay_post($this->payUrl, $req_data);
            Log::info('银盛支付-查询订单-结果');
            Log::info($res);
            $return_data = json_decode($res, true);
            $resData = $return_data['ysepay_online_trade_order_query_response'];
            //业务成功
            if ($resData['code'] == '10000') {
                //支付成功
                if ($resData['trade_status'] == 'TRADE_SUCCESS') {
                    return [
                        'status' => 1,
                        'message' => '支付成功',
                        'data' => $resData
                    ];
                } else {
                    return [
                        'status' => 2,
                        'message' => $resData['sub_msg'] ?? '微商银行查询订单结果未知',
                        'data' => $resData
                    ];
                }
            } else {
                return [
                    'status' => 3,
                    'message' => $res['sub_msg'] ?? '微商银行订单查询错误'
                ];
            }
        } catch (\Exception $ex) {

        }
    }


    /**
     * 撤销订单(仅付款码支付) -1系统错误 1-成功 2-失败
     *
     * @param $data
     * @return array
     */
    public function reverse($data)
    {
        try {
            $partner_id = $data['partner_id'];
            $out_trade_no = $data['out_trade_no'];
            $shopDate = $data['shopdate'];
            $trade_no = $data['trade_no'];
            $req_data = [
                'method' => 'ysepay.online.trade.close', //接口类型
                'partner_id' => $partner_id, //商户号，由平台分配
                'timestamp' => date('Y-m-d H:i:s'),
                'charset' => 'utf-8',
                'sign_type' => 'RSA', //签名类型，取值RSA_1_256或RSA_1_1
                'version' => '3.0',

            ];
            $biz_content = [
                'out_trade_no' => $out_trade_no,
                'shopdate' => $this->datetime2string(date('Y-m-d H:i:m', strtotime($shopDate))), //商户日期(该参数做交易与查询时需要一致) 该日期需在当日的前后一天时间范围之内
                'trade_no' => $trade_no,
            ];
            $req_data['biz_content'] = json_encode($biz_content, JSON_UNESCAPED_UNICODE);
            $req_data['sign'] = $this->sign($req_data);
            Log::info('银盛支付-撤销订单-入参');
            Log::info($req_data);
            $res = $this->post($this->payUrl, $req_data); //1-成功;2-失败
            Log::info('银盛支付-撤销订单-结果');
            Log::info($res);
            $return_data = json_decode($res, true);
            //业务成功
            if ($res['code'] == 1) {
                return [
                    'status' => 1,
                    'message' => $return_data['err_msg'] ?? '撤销成功',
                    'data' => $return_data
                ];
            } else {
                return [
                    'status' => 2,
                    'message' => $res['message'] ?? '订单撤销失败'
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => 0,
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine(),
            ];
        }
    }


    /**
     * 申请退款 0-系统错误 1-成功 2-退款失败
     *
     * @param $data
     * @return array
     */
    public function refund($data)
    {
        try {
            $partner_id = $data['partner_id'];
            $out_trade_no = $data['out_trade_no'];
            $shopDate = $data['shop_date'];
            $tran_type = $data['tran_type'];
            $refund_amount = $data['refund_amount'];
            $trade_no = $data['trade_no'];
            $out_request_no = $data['orderNo'];
            $req_data = [
                'method' => 'ysepay.online.trade.refund', //接口类型
                'partner_id' => $partner_id, //
                'timestamp' => date('Y-m-d H:i:s', time()), //32,发送请求的时间,格式"yyyy-MM-dd HH:mm:ss"
                'charset' => 'utf-8', //商户网站使用的编码格式
                'sign_type' => 'RSA',
                'version' => '3.4',//接口版本
                'tran_type' => $tran_type
            ];
            $biz_content = [
                'out_trade_no' => $out_trade_no,
                'shopdate' => date('Y-m-d H:i:m', strtotime($shopDate)), //商户日期(该参数做交易与查询时需要一致) 该日期需在当日的前后一天时间范围之内
                'trade_no' => $trade_no,
                'refund_amount' => $refund_amount,
                'refund_reason' => '退款',
                'out_request_no' => $out_request_no,
            ];
            $req_data['biz_content'] = json_encode($biz_content, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            $sign = $this->sign($req_data);
            $req_data['sign'] = $sign;
            Log::info('银盛支付-退款-入参');
            Log::info($req_data);
            $res = $this->pay_post($this->payUrl, $req_data);
            Log::info('银盛支付-退款-结果');
            Log::info($res);
            $return_data = json_decode($res, true);
            $resData = $return_data['ysepay_online_trade_refund_response'];
            //业务成功
            if ($resData['code'] == '10000') {
                return [
                    'status' => '1',
                    'message' => '退款成功',
                    'data' => $resData,
                ];
            } else {
                return [
                    'status' => 2,
                    'message' => $resData['sub_msg'] ?? '退款失败'
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => 0,
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine(),
            ];
        }
    }


    //退款查询 -1系统错误 1-成功 2-退款失败 3-退款处理中
    public function refund_query($data)
    {
        try {
            $partner_id = $data['partner_id'];
            $out_trade_no = $data['out_trade_no'];
            $trade_no = $data['trade_no'];
            $out_request_no = $data['out_request_no'];

            $req_data = [
                'method' => 'ysepay.online.trade.refund.query', //接口类型
                'partner_id' => $partner_id, //
                'timestamp' => date('Y-m-d H:i:s', time()), //32,发送请求的时间,格式"yyyy-MM-dd HH:mm:ss"
                'charset' => 'utf-8', //商户网站使用的编码格式
                'sign_type' => 'RSA',
                'notify_url' => url('/api/yinsheng/pay_notify_url'), //银盛支付服务器主动通知商户网站里指定的页面 http 路径，支持多个 url 进行异步通知，多个 url用分隔符“,”分开，格式如：url1,url2,url3
                'version' => '3.4',//接口版本
            ];
            $biz_content = [
                'out_trade_no' => $out_trade_no,
                'trade_no' => $trade_no,
                'out_request_no' => $out_request_no,
                'refund_reason' =>'退款查询',
                'refund_amount' =>'0.02'
            ];
            $req_data['biz_content'] = json_encode($biz_content, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            $sing = $this->sign($req_data);
            $req_data['sign'] = $sing;
            Log::info('银盛支付-退款查询-入参');
            Log::info($req_data);
            $res = $this->pay_post($this->payUrl, $req_data);
            Log::info('银盛支付-退款查询-结果');
            Log::info($res);
            $res = json_decode($res, true);

            $resData = $res['ysepay_online_trade_refund_query_response'];
            //业务成功
            if ($resData['code'] == '10000') {

                //退款成功
                if ($resData['refund_state'] == 'success') {
                    return [
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $resData
                    ];
                } //退款失败
                elseif ($resData['refund_state'] == 'fail') {
                    return [
                        'status' => 3,
                        'message' => '退款失败',
                        'data' => $resData
                    ];
                }
            } else {
                return [
                    'status' => 2,
                    'message' => $resData['sub_msg'] ?? '退款查询错误'
                ];
            }

        } catch (\Exception $ex) {
            return [
                'status' => 0,
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine(),
            ];
        }
    }

    //获取银联标识
    public function get_user_id($data)
    {
        try {
            $partner_id = $data['partner_id'];
            $userAuthCode = $data['userAuthCode'];
            $appUpIdentifier = $data['appUpIdentifier'];

            $req_data = [
                'method' => 'ysepay.online.cupgetmulapp.userid', //接口类型
                'partner_id' => $partner_id, //
                'timestamp' => date('Y-m-d H:i:s', time()), //32,发送请求的时间,格式"yyyy-MM-dd HH:mm:ss"
                'charset' => 'utf-8', //商户网站使用的编码格式
                'sign_type' => 'RSA',
                'version' => '3.4',//接口版本
            ];
            $biz_content = [
                'userAuthCode' => $userAuthCode,
                'appUpIdentifier' => $appUpIdentifier
            ];
            $req_data['biz_content'] = json_encode($biz_content, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            $sign = $this->sign($req_data);
            $req_data['sign'] = $sign;
            Log::info('银盛支付-获取银联标识-入参');
            Log::info($req_data);
            $res = $this->pay_post($this->payUrl, $req_data);
            Log::info('银盛支付-获取银联标识-结果');
            Log::info($res);
            $res = json_decode($res, true);
            $resData = $res['ysepay_online_cupgetmulapp_userid_response'];
            //业务成功
            if ($resData['code'] == '10000') {
                if ($resData['trade_status'] == 'SUCCESS') {
                    return [
                        'status' => 1,
                        'message' => '获取成功',
                        'data' => $resData
                    ];
                } elseif ($resData['trade_status'] == 'FAIL') {
                    return [
                        'status' => 2,
                        'message' => '获取失败',
                        'data' => $resData
                    ];
                }
            } else {
                return [
                    'status' => 2,
                    'message' => $resData['sub_msg'] ?? '获取失败'
                ];
            }

        } catch (\Exception $ex) {
            return [
                'status' => 0,
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine(),
            ];
        }
    }


}
