<?php

namespace App\Api\Controllers\YinSheng;

use App\Models\Store;
use App\Models\StoreBank;
use App\Models\StoreImg;
use App\Models\StorePayWay;
use App\Models\YinshengStore;
use App\Models\YinshengContractInfos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class YsepayStoreController extends BaseController
{

    //商户入网补充资料
    public function ysepayMerchantAccess(Request $request)
    {
        try {
            $type = $request->get('type', '1'); //2-查询数据
            $store_id = $request->get('storeId', ''); //门店id
            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                $this->status = -1;
                $this->message = '门店不存在或状态异常';
                return $this->format();
            }

            $YinshengStore = YinshengStore::where('store_id', $store_id)->first();

            if ($type == 2) {
                $this->status = 1;
                $this->message = '返回数据成功';
                return $this->format($YinshengStore);
            }

            $mch_type = $request->get('mch_type', '');
            $mcc_cd = $request->get('mcc_cd', '');
            $mcc_name = $request->get('mcc_name', '');
            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');
            $province_name = $request->get('province_name', '');
            $city_name = $request->get('city_name', '');
            $area_name = $request->get('area_name', '');
            $is_settInPlatAcc = $request->get('is_settInPlatAcc', 'N'); //是否平台内账户,Y是 N否，默认否
            $bus_open_type = $request->get('bus_open_type', '00'); //到账方式( 00-扫码工作日到账01-扫码实时到账10-刷卡工作日到账11-刷卡实时到账20-D1到账)允许多选用“|”分隔 T1工作日到账必选
            $d_calc_type = $request->get('d_calc_type', '0'); //借记卡计费类型(1-按笔数(单位:分);0-按比例(单位:%)),实时入账必传
            $c_calc_type = $request->get('c_calc_type', '0'); //信用卡计费类型(1-按笔数(单位:分);0-按比例(单位:%)),实时入账必传
            $d_calc_val = $request->get('d_calc_val', ''); //借记卡扣率
            $d_stlm_max_amt = $request->get('d_stlm_max_amt', ''); //借记卡封顶金额单位:分
            $d_fee_low_limit = $request->get('d_fee_low_limit', ''); //借记卡手续费最低值单位:分
            $c_calc_val = $request->get('c_calc_val', ''); //信用卡扣率
            $c_fee_low_limit = $request->get('c_fee_low_limit', ''); //信用卡手续费最低值单位:分
            $business_application_img = $request->get('business_application_img', '');

            $except_token_data = $request->except(['token']);

            $validate = Validator::make($except_token_data, [
                'storeId' => 'required|max:50',
                'mch_type' => 'required|max:5',
                'mcc_cd' => 'required|max:5',
                'province_code' => 'required',
                'city_code' => 'required',
                'area_code' => 'required',
                'bus_open_type' => 'required',
                'd_calc_type' => 'required',
                'c_calc_type' => 'required',
                'd_calc_val' => 'required|max:12',
                'd_stlm_max_amt' => 'required|max:8',
                'd_fee_low_limit' => 'required|max:8',
                'c_calc_val' => 'required|max:12',
                'c_fee_low_limit' => 'required|max:8',
            ], [
                'required' => ':attribute参数为必填项',
                'min' => ':attribute参数长度太短',
                'max' => ':attribute参数长度太长',
                'unique' => ':attribute参数已经被人占用',
                'exists' => ':attribute参数不存在',
                'integer' => ':attribute参数必须是数字',
                'required_if' => ':attribute参数满足条件时必传',
            ], [
                'storeId' => '门店id',
                'mch_type' => '主营业务',
                'mcc_cd' => '主营业务',
                'province_code' => '商户区域',
                'city_code' => '商户区域',
                'area_code' => '商户区域',
                'bus_open_type' => '到账方式',
                'd_calc_val' => '借记卡扣率',
                'd_stlm_max_amt' => '借记卡封顶金额',
                'd_fee_low_limit' => '借记卡手续费最低值',
                'c_calc_val' => '信用卡扣率',
                'c_fee_low_limit' => '信用卡手续费最低值',
                'd_calc_type' => '借记卡计费类型',
                'c_calc_type' => '信用卡计费类型',
            ]);
            if ($validate->fails()) {
                $this->status = 2;
                $this->message = $validate->getMessageBag()->first();
                return $this->format();
            }

            $insert_data = [
                'config_id' => $storeObj->config_id,
                'store_id' => $store_id,
                'mch_type' => $mch_type,
                'mcc_cd' => $mcc_cd,
                'mcc_name' => $mcc_name,
                'province_code' => $province_code,
                'city_code' => $city_code,
                'area_code' => $area_code,
                'province_name' => $province_name,
                'city_name' => $city_name,
                'area_name' => $area_name,
                'bus_open_type' => $bus_open_type,
                'd_calc_type' => $d_calc_type,
                'c_calc_type' => $c_calc_type,
                'd_calc_val' => $d_calc_val,
                'd_stlm_max_amt' => $d_stlm_max_amt,
                'd_fee_low_limit' => $d_fee_low_limit,
                'c_calc_val' => $c_calc_val,
                'c_fee_low_limit' => $c_fee_low_limit,
                'd_calc_val' => $d_calc_val,
                'd_stlm_max_amt' => $d_stlm_max_amt,
                'd_fee_low_limit' => $d_fee_low_limit,
                'c_calc_val' => $c_calc_val,
                'c_fee_low_limit' => $c_fee_low_limit,
                'business_application_img' => $business_application_img
            ];

            if ($YinshengStore) {
                $result = $YinshengStore->update($insert_data);
                if (!$result) {
                    $this->status = 2;
                    $this->message = '更新失败';
                    return $this->format();
                }
            } else {
                $result = YinshengStore::create($insert_data);
                if (!$result) {
                    $this->status = 2;
                    $this->message = '添加失败';
                    return $this->format();
                }
            }

            $this->message = '添加成功';
            $this->status = 1;
            return $this->format($result);

        } catch (\Exception $ex) {
            Log::info('银盛商户入网 材料补充-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    /**
     * 入网申请
     * @param $reqId string 流水号  要求32个字符内（最少14个字符），只能是数字、大小写字母_-且在同一个商户号下唯一
     * @param $custInfo array 基本信息
     * @param $crpInfo array 法人信息
     * @param $stlAccInfo array 结算信息
     * @param $busInfo array 营业信息
     * @return array
     */
    public function addCustInfoApply($StoreInfo, $StoreBank, $StoreImg)
    {
        $store_id = $StoreInfo->store_id;

        // 1.2 上传图片信息
        // 图片上传 imageUpload
        // 小微商户 （A002--法人身份证人像面、A003--法人身份证国徽面、A006--商户门头照、A007--商户经营场所照、A008--商户收银台照、A004--结算银行卡正面（有卡号一面）、A005--结算银行卡反面）
        // 个体商户、企业商户（A002--法人身份证人像面、A003--法人身份证国徽面、A006--商户门头照、A007--商户经营场所照、A008--商户收银台照）
        // 结算账户类型 11 对私 21 对公
        // 对私结算 (A004--结算银行卡正面、A005--结算银行卡反面)
        // 对公结算（A011--开户许可证）
        // 非法人结算（A013--被授权人身份证人像面、A014--被授权人身份证国徽面、B005--结算授权书、B004--法人手持结算授权书）

        //图片公共部分
        if (!$StoreImg->head_sfz_img_a) {
            return json_encode([
                'status' => 1,
                'message' => '法人身份证正面-未上传'
            ]);
        }
        if (!$StoreImg->head_sfz_img_b) {
            return json_encode([
                'status' => 1,
                'message' => '法人身份证反面-未上传'
            ]);
        }
        if (!$StoreImg->store_logo_img) {
            return json_encode([
                'status' => 1,
                'message' => '营业场所门头照-未上传'
            ]);
        }
        if (!$StoreImg->store_img_a) {
            return json_encode([
                'status' => 2,
                'message' => '收银台照片-未上传'
            ]);
        }
        if (!$StoreImg->store_img_b) {
            return json_encode([
                'status' => 2,
                'message' => '经营场所照片-未上传'
            ]);
        }
        if ($StoreInfo->store_type == 1 || $StoreInfo->store_type == 2) {//1个体商户、2企业商户
            if (!$StoreImg->store_license_img) {
                return json_encode([
                    'status' => 2,
                    'message' => '营业执照-未上传'
                ]);
            }

            if ($StoreBank->store_bank_type == '02') {// 02对公结算
                //A011-开户许可证
                if (!$StoreImg->store_industrylicense_img) {
                    return json_encode([
                        'status' => 2,
                        'message' => '结算银行卡正面-未上传'
                    ]);
                }
            } else if ($StoreBank->store_bank_type == '01') {// 对私结算
                if (!$StoreImg->bank_img_a) {
                    return json_encode([
                        'status' => 2,
                        'message' => '结算银行卡正面-未上传'
                    ]);
                }
                if (!$StoreImg->bank_img_b) {
                    return json_encode([
                        'status' => 2,
                        'message' => '结算银行卡反面-未上传'
                    ]);
                }
                if ($StoreInfo->head_name != $StoreBank->store_bank_name) { //非法人结算
                    if (!$StoreImg->bank_sfz_img_a) {
                        return json_encode([
                            'status' => 2,
                            'message' => '结算卡身份证正面-未上传'
                        ]);
                    }
                    if (!$StoreImg->bank_sfz_img_b) {
                        return json_encode([
                            'status' => 2,
                            'message' => '结算卡身份证反面-未上传'
                        ]);
                    }
                    if (!$StoreImg->store_auth_bank_img) {
                        return json_encode([
                            'status' => 2,
                            'message' => '结算授权书-未上传'
                        ]);
                    }
                }
            }
        } else { //小微
            if (!$StoreImg->bank_img_a) {
                return json_encode([
                    'status' => 2,
                    'message' => '结算银行卡正面-未上传'
                ]);
            }
            if (!$StoreImg->bank_img_b) {
                return json_encode([
                    'status' => 2,
                    'message' => '结算银行卡反面-未上传'
                ]);
            }
        }

        $YinshengStore = YinshengStore::where('store_id', $store_id)->first();
        if (!$YinshengStore) {
            return json_encode([
                'status' => 1,
                'message' => "请在'门店列表-通道管理'中补充资料"
            ]);
        }

        $mercType = '2';
        if ($StoreInfo->store_type == '1') { //个体工商户
            $mercType = '3';
        } else if ($StoreInfo->store_type == '2') { //企业
            $mercType = '4';
        } else if ($StoreInfo->store_type == '3') { //小微商户
            $mercType = '2';
        }

        $custInfo = [
            'agtMercId' => $this->agtMercId, // 代理商编号(子商户)
            'mercName' => $StoreInfo->store_short_name, // 商户名称
            'mercShortName' => $StoreInfo->store_short_name,  // 商户简称
            'mercType' => $mercType,  //商户类型2 小微 3个体 4企业
            'mccCd' => $YinshengStore->mcc_cd, //mcc码 通过查询mcc码接口可以获取
            'contactMail' => ($StoreInfo->store_email != '') ? $StoreInfo->store_email : ($StoreInfo->people_phone . '@139.com'),  //联系人邮箱
            'contactMan' => $StoreInfo->people, //联系人
            'contactPhone' => $StoreInfo->people_phone, //联系人电话
            'cusMgrNm' => ($StoreInfo->store_email != '') ? $StoreInfo->store_email : ($StoreInfo->people_phone . '@139.com'), //客户经理
            'isOpenMarket' => 'N', // 是否开通营销,Y开通N不开通,默认不开通
            'notifyUrl' => url('api/ysepay/merchant_notify_url'), // 异步通知地址
            //'remark' => ''
        ];
        $crpInfo = [
            'crpCertNo' => $StoreInfo->head_sfz_no, //法人证件号
            'crpCertType' => '00', // 法人证件类型 00 身份证
            'certBgn' => $this->YsepayTimeFormat($StoreInfo->head_sfz_stime), // 证件开始日期 '20211007'
            'certExpire' => $this->YsepayTimeFormat($StoreInfo->head_sfz_time), // 证件有效期 '29991231'
            'crpNati' => '249', //法人国籍
            'crpNm' => $StoreInfo->head_name,  //法人姓名
            'crpPhone' => $StoreInfo->people_phone, //法人手机号
            'crpAddr' => $StoreInfo->province_name . $StoreInfo->city_name . $StoreInfo->area_name . $StoreInfo->store_address, //注册地址,//法人地址
            //实际控制人（默认同法人信息）,若传值，则姓名，证件类型，证件号，证件开始日期、结束日期必填
            //'actContrInfo' => [],
            //被授权人信息（法人结算同法人信息,非法人结算同开户人信息）,若传值，则姓名，证件类型，证件号，证件开始日期、结束日期必填
            //'authInfo' => [],
            //受益人(默认同法人)，最多三个受益人
            //'bnfList' => [],
        ];

        $isUncrpSett = 'N';
        if ($StoreInfo->head_name != $StoreBank->store_bank_name) {// 非法人结算
            $isUncrpSett = 'Y';
        }

        $stlAccInfo = [
            'isSettInPlatAcc' => 'N',//是否平台内账户,Y是 N否，默认否
            'isUncrpSett' => $isUncrpSett,//是否非法人结算,Y是（对私） N否（对公），默认否(卡类型 02对公-01对私)
            'stlAccNm' => $StoreBank->store_bank_name,//结算户名
            'stlAccNo' => $StoreBank->store_bank_no,//结算账号
            'bankSubCode' => $StoreBank->bank_no,//开户支行联行号
            'stlAccType' => ($StoreBank->store_bank_type == '02') ? '21' : '11',//结算账户类型 11 对私 21 对公 23 对公存折 24 单位结算卡
            'bankMobile' => ($StoreBank->store_bank_type == '01') ? $StoreInfo->people_phone : '',//银行预留手机号 对私时必填
            'openCertNo' => ($StoreBank->store_bank_type == '01') ? $StoreBank->bank_sfz_no : '',//开开户证件号 对私时必填
            'bankProince' => $YinshengStore->province_code,    //银行开户行所属省代码
            'bankCity' => $YinshengStore->city_code    //银行开户行所属市代码
        ];

        $busInfo = [
            'busNo' => ($StoreInfo->store_type == '1' || $StoreInfo->store_type == '2') ? $StoreInfo->store_license_no : '',//营业执照号 非小微必填
            'busNm' => ($StoreInfo->store_type == '1' || $StoreInfo->store_type == '2') ? $StoreInfo->store_name : '',//营业执照名称 非小微必填
            'busCertBgn' => $this->YsepayTimeFormat($StoreInfo->store_license_stime),//营业执照有效开始日期
            'busCertExpire' => $this->YsepayTimeFormat($StoreInfo->store_license_time),//营业执照有效期
            'busProviceCode' => $YinshengStore->province_code,//营业归属省代码
            'busCityCode' => $YinshengStore->city_code,//营业归属市代码
            'busAreaCode' => $YinshengStore->area_code,//营业归属区(县)代码
            'busCertType' => '19',//营业执照证件类型
            'busAddr' => $StoreInfo->province_name . $StoreInfo->city_name . $StoreInfo->area_name . $StoreInfo->store_address,//营业详细地址
            'regAddr' => $StoreInfo->province_name . $StoreInfo->city_name . $StoreInfo->area_name . $StoreInfo->store_address,//注册地址
        ];

        Log::info('---ysepay-商户入网---request params----->' . json_encode([
                'custInfo' => $custInfo,
                'crpInfo' => $crpInfo,
                'stlAccInfo' => $stlAccInfo,
                'busInfo' => $busInfo,
            ]));

        /** 生成对业务参数加密的随机密钥 */
        $key = $this->randomStr();
        $reqId = 'ys' . time() . mt_rand(10000, 99999);

        $sysFlowId = $YinshengStore->sys_flowId; //入网申请流水号
        $sysStatus = $YinshengStore->status; //00:入网成功 01:待上传图片 02:待审核 90:审核拒绝 10:转人工审核
        $aduitStatus = $YinshengStore->aduit_status;//审核状态(00：成功 02：待审核 90：审核拒绝)

        if ($sysStatus == '00' && ($aduitStatus == '01' || $aduitStatus == '02' || $aduitStatus == '00')) {
            Log::info("=======入网成功，进行资料确认=======");
            //资料确认
            return $this->auditCustInfoApply($sysFlowId, $YinshengStore);
        }

        if (empty($sysFlowId)) {
            $reqMap = $this->genCommonReqParam('smsc.addCustInfoApply', $key, $reqId);
            $reqMap['bizContent'] = $this->encrypt($this->jsonEncode([
                // 基本信息
                'custInfo' => $custInfo,
                // 法人信息
                'crpInfo' => $crpInfo,
                // 结算信息
                'stlAccInfo' => $stlAccInfo,
                // 营业信息
                'busInfo' => $busInfo,
            ]), $key);

            // 签名
            $this->doSign($reqMap);
            $resMap = $this->post($this->ysBaseUrl . $this->addCustInfoUrl, $reqMap);
            $resMap = json_decode(base64_decode($resMap), true);
        }

        //扫码服务商调用“商户审核”接口返回状态为审核拒绝或转人工审核时，调用此接口修改商户信息重新进件
        //90:审核拒绝 10:转人工审核
        if ($sysStatus == '90' || $sysStatus == '10' || $aduitStatus == '10' || $aduitStatus == '90') {
            //商户资料修改
            $reqMap = $this->genCommonReqParam('smsc.modifyCustInfoApply', $key, $reqId);
            $reqMap['bizContent'] = $this->encrypt($this->jsonEncode([
                // 入网申请流水
                'sysFlowId' => $sysFlowId,
                // 基本信息
                'custInfo' => $custInfo,
                // 法人信息
                'crpInfo' => $crpInfo,
                // 结算信息
                'stlAccInfo' => $stlAccInfo,
                // 营业信息
                'busInfo' => $busInfo,
            ]), $key);
            // 签名
            $this->doSign($reqMap);
            $resMap = $this->post($this->ysBaseUrl . $this->modifyCustInfoUrl, $reqMap);
            $resMap = json_decode(base64_decode($resMap), true);
        }

        $new_sysFlowId = '';
        if (isset($resMap) && !empty($resMap)) {
            $this->checkSign($resMap);
            $this->decryptBizData($resMap, $key);
            Log::info('---ysepay---商户入网---response params-->' . json_encode($resMap));

            if ($resMap['code'] == '00000' && $resMap['subCode'] == '0000') {

                $new_sysFlowId = $resMap['businessData']['sysFlowId'];

                if ($new_sysFlowId != $sysFlowId) {
                    $YinshengStore->update([
                        'sys_flowId' => $new_sysFlowId, //入网申请流水号
                        'status' => '00' //
                    ]);
                }

                //公共图片（A002--法人身份证人像面、A003--法人身份证国徽面、A006--商户门头照、A007--商户经营场所照、A008--商户收银台照）

                $headSfzImgA = $this->images_get($StoreImg->head_sfz_img_a); //A002--法人身份证人像面
                $headSfzImgARes = $this->imageUpload($new_sysFlowId, 'A002', $headSfzImgA); //-1 系统错误 1-成功 2-失败
                if ($headSfzImgARes['code'] == '00000' && $headSfzImgARes['subCode'] == '0000') {

                } else {
                    if ($headSfzImgARes['code'] != '00000') {
                        return json_encode([
                            'status' => 2,
                            'message' => $headSfzImgARes['msg']
                        ]);
                    }
                    return json_encode([
                        'status' => 2,
                        'message' => $headSfzImgARes['subCode'] . '-' . $headSfzImgARes['subMsg']
                    ]);
                }

                $headSfzImgB = $this->images_get($StoreImg->head_sfz_img_b); //A003--法人身份证国徽面
                $headSfzImgBRes = $this->imageUpload($new_sysFlowId, 'A003', $headSfzImgB);
                if ($headSfzImgBRes['code'] == '00000' && $headSfzImgBRes['subCode'] == '0000') {

                } else {
                    if ($headSfzImgBRes['code'] != '00000') {
                        return json_encode([
                            'status' => 2,
                            'message' => $headSfzImgBRes['msg']
                        ]);
                    }
                    return json_encode([
                        'status' => 2,
                        'message' => $headSfzImgBRes['subCode'] . '-' . $headSfzImgBRes['subMsg']
                    ]);
                }

                $storeLogoImg = $this->images_get($StoreImg->store_logo_img); //A006--商户门头照
                $storeLogoImgRes = $this->imageUpload($new_sysFlowId, 'A006', $storeLogoImg);
                if ($storeLogoImgRes['code'] == '00000' && $storeLogoImgRes['subCode'] == '0000') {

                } else {
                    if ($storeLogoImgRes['code'] != '00000') {
                        return json_encode([
                            'status' => 2,
                            'message' => $storeLogoImgRes['msg']
                        ]);
                    }
                    return json_encode([
                        'status' => 2,
                        'message' => $storeLogoImgRes['subCode'] . '-' . $storeLogoImgRes['subMsg']
                    ]);
                }

                $storeImgA = $this->images_get($StoreImg->store_img_a); //A008--商户收银台照
                $storeImgARes = $this->imageUpload($new_sysFlowId, 'A008', $storeImgA);
                if ($storeImgARes['code'] == '00000' && $storeImgARes['subCode'] == '0000') {

                } else {
                    if ($storeImgARes['code'] != '00000') {
                        return json_encode([
                            'status' => 2,
                            'message' => $storeImgARes['msg']
                        ]);
                    }
                    return json_encode([
                        'status' => 2,
                        'message' => $storeImgARes['subCode'] . '-' . $storeImgARes['subMsg']
                    ]);
                }
                $storeImgB = $this->images_get($StoreImg->store_img_b); //A007--商户经营场所照
                $storeImgBRes = $this->imageUpload($new_sysFlowId, 'A007', $storeImgB);
                if ($storeImgBRes['code'] == '00000' && $storeImgBRes['subCode'] == '0000') {

                } else {
                    if ($storeImgBRes['code'] != '00000') {
                        return json_encode([
                            'status' => 2,
                            'message' => $storeImgBRes['msg']
                        ]);
                    }
                    return json_encode([
                        'status' => 2,
                        'message' => $storeImgBRes['subCode'] . '-' . $storeImgBRes['subMsg']
                    ]);
                }

                if ($StoreInfo->store_type == 1 || $StoreInfo->store_type == 2) {//1个体商户、2企业商户
                    //A001营业执照
                    $storeLicenseImg = $this->images_get($StoreImg->store_license_img);
                    $storeLicenseImgRes = $this->imageUpload($new_sysFlowId, 'A001', $storeLicenseImg);
                    if ($storeLicenseImgRes['code'] == '00000' && $storeLicenseImgRes['subCode'] == '0000') {

                    } else {
                        if ($storeLicenseImgRes['code'] != '00000') {
                            return json_encode([
                                'status' => 2,
                                'message' => $storeLicenseImgRes['msg']
                            ]);
                        }
                        return json_encode([
                            'status' => 2,
                            'message' => $storeLicenseImgRes['subCode'] . '-' . $storeLicenseImgRes['subMsg']
                        ]);
                    }
                    if ($StoreBank->store_bank_type == '02') {// 02对公结算

                        $storeIndustryLicenseImg = $this->images_get($StoreImg->store_industrylicense_img);
                        $storeIndustryLicenseImgRes = $this->imageUpload($new_sysFlowId, 'A011', $storeIndustryLicenseImg);
                        if ($storeIndustryLicenseImgRes['code'] == '00000' && $storeIndustryLicenseImgRes['subCode'] == '0000') {

                        } else {
                            if ($storeIndustryLicenseImgRes['code'] != '00000') {
                                return json_encode([
                                    'status' => 2,
                                    'message' => $storeIndustryLicenseImgRes['msg']
                                ]);
                            }
                            return json_encode([
                                'status' => 2,
                                'message' => $storeIndustryLicenseImgRes['subCode'] . '-' . $storeIndustryLicenseImgRes['subMsg']
                            ]);
                        }

                    } else if ($StoreBank->store_bank_type == '01') {// 对私结算

                        $storeBankA = $this->images_get($StoreImg->bank_img_a); //A004--结算银行卡正面（有卡号一面）
                        $storeBankARes = $this->imageUpload($new_sysFlowId, 'A004', $storeBankA);
                        if ($storeBankARes['code'] == '00000' && $storeBankARes['subCode'] == '0000') {

                        } else {
                            if ($storeBankARes['code'] != '00000') {
                                return json_encode([
                                    'status' => 2,
                                    'message' => $storeBankARes['msg']
                                ]);
                            }
                            return json_encode([
                                'status' => 2,
                                'message' => $storeBankARes['subCode'] . '-' . $storeBankARes['subMsg']
                            ]);
                        }
                        $storeBankB = $this->images_get($StoreImg->bank_img_b); //A005--结算银行卡反面
                        $storeBankBRes = $this->imageUpload($new_sysFlowId, 'A005', $storeBankB);
                        if ($storeBankBRes['code'] == '00000' && $storeBankBRes['subCode'] == '0000') {

                        } else {
                            if ($storeBankBRes['code'] != '00000') {
                                return json_encode([
                                    'status' => 2,
                                    'message' => $storeBankBRes['msg']
                                ]);
                            }
                            return json_encode([
                                'status' => 2,
                                'message' => $storeBankBRes['subCode'] . '-' . $storeBankBRes['subMsg']
                            ]);
                        }

                        //01对私 && 非法人结算
                        if ($StoreInfo->head_name != $StoreBank->store_bank_name) { //非法人结算
                            $storeBankSfzA = $this->images_get($StoreImg->bank_sfz_img_a); //A013--被授权人身份证人像面
                            $storeBankSfzARes = $this->imageUpload($new_sysFlowId, 'A013', $storeBankSfzA);
                            if ($storeBankSfzARes['code'] == '00000' && $storeBankSfzARes['subCode'] == '0000') {

                            } else {
                                if ($storeBankSfzARes['code'] != '00000') {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => $storeBankSfzARes['msg']
                                    ]);
                                }
                                return json_encode([
                                    'status' => 2,
                                    'message' => $storeBankSfzARes['subCode'] . '-' . $storeBankSfzARes['subMsg']
                                ]);
                            }
                            $storeBankSfzB = $this->images_get($StoreImg->bank_sfz_img_b); //A014--被授权人身份证国徽面
                            $storeBankSfzBRes = $this->imageUpload($new_sysFlowId, 'A014', $storeBankSfzB);
                            if ($storeBankSfzBRes['code'] == '00000' && $storeBankSfzBRes['subCode'] == '0000') {

                            } else {
                                if ($storeBankSfzBRes['code'] != '00000') {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => $storeBankSfzBRes['msg']
                                    ]);
                                }
                                return json_encode([
                                    'status' => 2,
                                    'message' => $storeBankSfzBRes['subCode'] . '-' . $storeBankSfzBRes['subMsg']
                                ]);
                            }
                            $storeAuthBank = $this->images_get($StoreImg->store_auth_bank_img); //B005--结算授权书
                            $storeAuthBankRes = $this->imageUpload($new_sysFlowId, 'B005', $storeAuthBank);
                            if ($storeAuthBankRes['code'] == '00000' && $storeAuthBankRes['subCode'] == '0000') {

                            } else {
                                if ($storeAuthBankRes['code'] != '00000') {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => $storeAuthBankRes['msg']
                                    ]);
                                }
                                return json_encode([
                                    'status' => 2,
                                    'message' => $storeAuthBankRes['subCode'] . '-' . $storeAuthBankRes['subMsg']
                                ]);
                            }
                            $storeAuthBankFrRes = $this->imageUpload($new_sysFlowId, 'B004', $storeAuthBank); //B004--法人手持结算授权书
                            if ($storeAuthBankFrRes['code'] == '00000' && $storeAuthBankFrRes['subCode'] == '0000') {

                            } else {
                                if ($storeAuthBankFrRes['code'] != '00000') {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => $storeAuthBankFrRes['msg']
                                    ]);
                                }
                                return json_encode([
                                    'status' => 2,
                                    'message' => $storeAuthBankFrRes['subCode'] . '-' . $storeAuthBankFrRes['subMsg']
                                ]);
                            }
                        }
                    }

                } else { //小微商户

                    $storeBankA = $this->images_get($StoreImg->bank_img_a); //A004--结算银行卡正面（有卡号一面）
                    $storeBankARes = $this->imageUpload($new_sysFlowId, 'A004', $storeBankA);
                    if ($storeBankARes['code'] == '00000' && $storeBankARes['subCode'] == '0000') {

                    } else {
                        if ($storeBankARes['code'] != '00000') {
                            return json_encode([
                                'status' => 2,
                                'message' => $storeBankARes['msg']
                            ]);
                        }
                        return json_encode([
                            'status' => 2,
                            'message' => $storeBankARes['subCode'] . '-' . $storeBankARes['subMsg']
                        ]);
                    }
                    $storeBankB = $this->images_get($StoreImg->bank_img_b); //A005--结算银行卡反面
                    $storeBankBRes = $this->imageUpload($new_sysFlowId, 'A005', $storeBankB);
                    if ($storeBankBRes['code'] == '00000' && $storeBankBRes['subCode'] == '0000') {

                    } else {
                        if ($storeBankBRes['code'] != '00000') {
                            return json_encode([
                                'status' => 2,
                                'message' => $storeBankBRes['msg']
                            ]);
                        }
                        return json_encode([
                            'status' => 2,
                            'message' => $storeBankBRes['subCode'] . '-' . $storeBankBRes['subMsg']
                        ]);
                    }
                }

                //1.3 资料确认

                return $this->auditCustInfoApply($new_sysFlowId, $YinshengStore);

            } else {
                if ($resMap['code'] != '00000') {
                    return json_encode([
                        'status' => 2,
                        'message' => $resMap['msg']
                    ]);
                }
                return json_encode([
                    'status' => 2,
                    'message' => $resMap['subMsg']
                ]);
            }
        } else {
            return json_encode([
                'status' => 2,
                'message' => "三方系统返回错误"
            ]);
        }

    }

    /**
     * 商户入网审核
     * @param $reqId string 流水号  要求32个字符内（最少14个字符），只能是数字、大小写字母_-且在同一个商户号下唯一
     * @param $sysFlowId string 入网申请流水号,调用商户入网申请接口成功会返回入网申请流水号
     * @param $auditFlag string 审核标志 Y通过,N拒绝
     */
    public function auditCustInfoApply($sysFlowId, $YinshengStore)
    {
        $reqId = 'ys' . time() . mt_rand(10000, 99999);
        $auditFlag = 'Y';

        /** 生成对业务参数加密的随机密钥 */
        $key = $this->randomStr();
        $reqMap = $this->genCommonReqParam('smsc.auditCustInfoApply', $key, $reqId);
        /** 封装业务参数,具体参数见文档*/
        /** 使用生成的密钥key对业务参数进行加密，并将加密后的业务参数放入请求参数bizContent中*/
        $reqMap['bizContent'] = $this->encrypt($this->jsonEncode([
            // 入网申请流水号,调用商户入网申请接口成功会返回入网申请流水号。
            'sysFlowId' => $sysFlowId,
            // 审核标志 Y通过,N拒绝
            'auditFlag' => $auditFlag,
        ]), $key);
        // 签名
        $this->doSign($reqMap);
        $resMap = $this->post($this->ysBaseUrl . $this->auditCustInfoUrl, $reqMap);
        $resMap = json_decode(base64_decode($resMap), true);
        if (isset($resMap) && !empty($resMap)) {
            $this->checkSign($resMap);
            $this->decryptBizData($resMap, $key);
            Log::info('---ysepay-入网审核---auditCustInfoApply----resMap-->' . json_encode($resMap));

            if ($resMap['code'] == '00000') {
                $sysFlowId = $resMap['businessData']['sysFlowId'];
                $status = $resMap['businessData']['status']; //状态,00：成功 02：待审核 90：审核拒绝
                $custId = ($status == '00') ? $resMap['businessData']['custId'] : '';//客户号,入网成功则返回

                $audit_msg = '';
                if ($status == '00') {
                    $audit_msg = '成功';
                } else if ($status == '02') {
                    $audit_msg = '待审核';
                } else if ($status == '90') {
                    $audit_msg = '审核拒绝';
                } else {
                    $audit_msg = $resMap['subMsg'];
                }

                $YinshengStore->update([
                    'cust_id' => $custId, //客户号
                    'aduit_status' => $status,
                    'audit_msg' => $audit_msg
                ]);

                return json_encode([
                    'status' => $status,
                    'sysFlowId' => $sysFlowId,
                    'custId' => $custId,
                    'message' => $resMap['subMsg']
                ]);
            } else {
                return json_encode([
                    'status' => $resMap['businessData']['status'],
                    'message' => $resMap['subMsg']
                ]);
            }
        } else {
            return json_encode([
                'status' => 2,
                'message' => "资料确认第三方系统返回错误！"
            ]);
        }
    }

    /**
     * 查询商户状态
     * @param $reqId string 流水号  要求32个字符内（最少14个字符），只能是数字、大小写字母_-且在同一个商户号下唯一
     * @param $sysFlowId string 入网申请流水号,调用商户入网申请接口成功会返回入网申请流水号
     */
    public function queryCustApply(Request $request)
    {
        $sysFlowId = $request->get('sysFlowId', '');

        $reqId = 'ys' . time() . mt_rand(10000, 99999);

        /** 生成对业务参数加密的随机密钥 */
        $key = $this->randomStr();
        $reqMap = $this->genCommonReqParam('smsc.queryCustApply', $key, $reqId);
        /** 封装业务参数,具体参数见文档*/
        /** 使用生成的密钥key对业务参数进行加密，并将加密后的业务参数放入请求参数bizContent中*/
        $reqMap['bizContent'] = $this->encrypt($this->jsonEncode([
            // 入网申请流水号,调用商户入网申请接口成功会返回入网申请流水号。
            'sysFlowId' => $sysFlowId,
        ]), $key);
        // 签名
        $this->doSign($reqMap);
        $resMap = $this->post($this->ysBaseUrl . $this->queryCustApplyUrl, $reqMap);
        $resMap = json_decode(base64_decode($resMap), true);

        if (isset($resMap) && !empty($resMap)) {
            $this->checkSign($resMap);
            $this->decryptBizData($resMap, $key);
            Log::info('---ysepay---查询商户状态---response  params-->' . json_encode($resMap));
            if ($resMap['code'] == '00000' && $resMap['subCode'] == '0000') {
                $sysFlowId = $resMap['businessData']['sysFlowId'];
                $status = $resMap['businessData']['status']; //状态,00：成功 02：待审核 90：审核拒绝
                $custId = ($status == '00') ? $resMap['businessData']['custId'] : '';//客户号,入网成功则返回

                return json_encode([
                    'status' => $status,
                    'sysFlowId' => $sysFlowId,
                    'custId' => $custId,
                    'message' => $resMap['businessData']['note']
                ]);

            } else {
                if ($resMap['code'] != '00000') {
                    return json_encode([
                        'status' => 2,
                        'message' => $resMap['msg']
                    ]);
                }
                return json_encode([
                    'status' => 2,
                    'message' => $resMap['subMsg']
                ]);
            }
        }

        return json_encode([
            'status' => 2,
            'message' => '三方系统返回错误'
        ]);
    }

    /**
     * 商户基本信息变更申请
     * @param $reqId string 流水号  要求32个字符内（最少14个字符），只能是数字、大小写字母_-且在同一个商户号下唯一
     * @param $baseInfo array 基本信息
     * @return array
     */
    public function changeMercBaseInfo(Request $request)
    {
        $store_id = $request->get('store_id');

        $StoreInfo = Store::where('store_id', $store_id)->first();
        if (!$StoreInfo) {
            return json_encode([
                'status' => 2,
                'message' => '门店未认证请认证门店'
            ]);
        }
        $StoreImg = StoreImg::where('store_id', $store_id)->first();
        if (!$StoreImg) {
            return json_encode([
                'status' => 2,
                'message' => '没有商户资料'
            ]);
        }

        $YinshengStore = YinshengStore::where('store_id', $store_id)->first();
        if (!$YinshengStore) {
            return json_encode([
                'status' => 1,
                'message' => "请在'门店列表-通道管理'中补充资料"
            ]);
        }
        $custId = $YinshengStore->cust_id; //客户号
        $mercId = $YinshengStore->mer_code; //商户号

        $reqId = 'ys' . time() . mt_rand(10000, 99999);

        /** 生成对业务参数加密的随机密钥 */
        $key = $this->randomStr();
        $reqMap = $this->genCommonReqParam('smsc.changeMercBaseInfo', $key, $reqId);

        $custInfo = [
            'mercName' => $StoreInfo->store_short_name, // 商户名称
            'mercShortName' => $StoreInfo->store_short_name,  // 商户简称
            'contactMail' => ($StoreInfo->store_email != '') ? $StoreInfo->store_email : ($StoreInfo->people_phone . '@139.com'),  //联系人邮箱
            'contactMan' => $StoreInfo->people, //联系人
            'contactPhone' => $StoreInfo->people_phone, //联系人电话
        ];
        $crpInfo = [
            'certExpire' => $this->YsepayTimeFormat($StoreInfo->head_sfz_time), // 证件有效期 '29991231'
            'crpNm' => $StoreInfo->head_name,  //法人姓名
            'crpPhone' => $StoreInfo->people_phone, //法人手机号
        ];

        if ($StoreInfo->store_type == 1 || $StoreInfo->store_type == 2) {//1个体商户、2企业商户
            $busInfo = [
                'busNm' => ($StoreInfo->store_type == '1' || $StoreInfo->store_type == '2') ? $StoreInfo->store_name : '',//营业执照名称 非小微必填
                'busCertExpire' => $this->YsepayTimeFormat($StoreInfo->store_license_time),//营业执照有效期
                'busAddr' => $StoreInfo->province_name . $StoreInfo->city_name . $StoreInfo->area_name . $StoreInfo->store_address,//营业详细地址
                'regAddr' => $StoreInfo->province_name . $StoreInfo->city_name . $StoreInfo->area_name . $StoreInfo->store_address,//注册地址
            ];

            $reqMap['bizContent'] = $this->encrypt($this->jsonEncode([
                // 客户号id
                'custId' => $custId,
                'mercId' => $mercId,
                'notifyUrl' => url('api/ysepay/merchant_notify_url'), // 异步通知地址
                // 基本信息
                'custInfo' => $custInfo,
                // 法人信息
                'crpInfo' => $crpInfo,
                // 营业信息
                'busInfo' => $busInfo,
            ]), $key);
        } else {
            //小微商户
            $reqMap['bizContent'] = $this->encrypt($this->jsonEncode([
                // 客户号id
                'custId' => $custId,
                'mercId' => $mercId,
                'notifyUrl' => url('api/ysepay/merchant_notify_url'), // 异步通知地址
                // 基本信息
                'custInfo' => $custInfo,
                // 法人信息
                'crpInfo' => $crpInfo
            ]), $key);

        }

        Log::info('---ysepay--商户基本信息变更申请---request params----->' . json_encode([
                'custInfo' => $custInfo,
                'crpInfo' => $crpInfo
            ]));

        // 签名
        $this->doSign($reqMap);
        $resMap = $this->post($this->ysBaseUrl . $this->changeMercBaseInfoUrl, $reqMap);
        $resMap = json_decode(base64_decode($resMap), true);
        if (isset($resMap) && !empty($resMap)) {

            $this->checkSign($resMap);
            $this->decryptBizData($resMap, $key);
            Log::info('---ysepay---商户基本信息变更申请---changeMercBaseInfo----reponse params：' . json_encode($resMap));
            if ($resMap['code'] == '00000' && $resMap['subCode'] == '0000') {
                $changeSysFlowId = $resMap['businessData']['changeSysFlowId']; //变更申请流水号

                $YinshengStore->update([
                    'change_sys_flowId' => $changeSysFlowId
                ]);

                $headSfzImgA = $this->images_get($StoreImg->head_sfz_img_a); //A002--法人身份证人像面
                $headSfzImgARes = $this->uploadChangePic($changeSysFlowId, 'A002', $headSfzImgA); //-1 系统错误 1-成功 2-失败
                if ($headSfzImgARes['code'] == '00000' && $headSfzImgARes['subCode'] == '0000') {

                } else {
                    if ($headSfzImgARes['code'] != '00000') {
                        return json_encode([
                            'status' => 2,
                            'message' => $headSfzImgARes['msg']
                        ]);
                    }
                    return json_encode([
                        'status' => 2,
                        'message' => $headSfzImgARes['subCode'] . '-' . $headSfzImgARes['subMsg']
                    ]);
                }

                $headSfzImgB = $this->images_get($StoreImg->head_sfz_img_b); //A003--法人身份证国徽面
                $headSfzImgBRes = $this->uploadChangePic($changeSysFlowId, 'A003', $headSfzImgB);
                if ($headSfzImgBRes['code'] == '00000' && $headSfzImgBRes['subCode'] == '0000') {

                } else {
                    if ($headSfzImgBRes['code'] != '00000') {
                        return json_encode([
                            'status' => 2,
                            'message' => $headSfzImgBRes['msg']
                        ]);
                    }
                    return json_encode([
                        'status' => 2,
                        'message' => $headSfzImgBRes['subCode'] . '-' . $headSfzImgBRes['subMsg']
                    ]);
                }

                if ($StoreInfo->store_type == 1 || $StoreInfo->store_type == 2) {//1个体商户、2企业商户
                    //A001营业执照
                    $storeLicenseImg = $this->images_get($StoreImg->store_license_img);
                    $storeLicenseImgRes = $this->uploadChangePic($changeSysFlowId, 'A001', $storeLicenseImg);
                    if ($storeLicenseImgRes['code'] == '00000' && $storeLicenseImgRes['subCode'] == '0000') {

                    } else {
                        if ($storeLicenseImgRes['code'] != '00000') {
                            return json_encode([
                                'status' => 2,
                                'message' => $storeLicenseImgRes['msg']
                            ]);
                        }
                        return json_encode([
                            'status' => 2,
                            'message' => $storeLicenseImgRes['subCode'] . '-' . $storeLicenseImgRes['subMsg']
                        ]);
                    }
                }

                $Images = $this->images_get($YinshengStore->business_application_img); //业务变更申请表  B008-其他1 B009-其他2
                $ImagesRes = $this->uploadChangePic($changeSysFlowId, 'B008', $Images);
                if ($ImagesRes['code'] == '00000' && $ImagesRes['subCode'] == '0000') {

                } else {
                    if ($ImagesRes['code'] != '00000') {
                        return json_encode([
                            'status' => 2,
                            'message' => $ImagesRes['msg']
                        ]);
                    }
                    return json_encode([
                        'status' => 2,
                        'message' => $ImagesRes['subCode'] . '-' . $ImagesRes['subMsg']
                    ]);
                }

                $storePayWay = StorePayWay::where('store_id', $store_id)
                    ->where('company', 'ysepay')
                    ->get();

                foreach ($storePayWay as $k => $v) {
                    $ways = StorePayWay::where('store_id', $store_id)
                        ->where('ways_source', $v->ways_source)
                        ->first();
                    if ($ways) {
                        $ways->update([
                            'status' => 1,
                            'status_desc' => '商户基本信息变更申请中'
                        ]);
                        $ways->save();
                    }
                }

                return json_encode([
                    'status' => 1,
                    'message' => '申请成功'
                ]);

            } else {
                if ($resMap['code'] != '00000') {
                    return json_encode([
                        'status' => 2,
                        'message' => $resMap['msg']
                    ]);
                }
                return json_encode([
                    'status' => 2,
                    'message' => $resMap['subMsg']
                ]);
            }
        } else {
            return json_encode([
                'status' => 2,
                'message' => "商户基本信息变更申请——第三方系统返回错误！"
            ]);
        }
    }

    /**
     * 结算信息变更申请
     * @param $reqId string 流水号  要求32个字符内（最少14个字符），只能是数字、大小写字母_-且在同一个商户号下唯一
     * @param array $accInfo 结算信息
     * @return array
     */
    public function changeMercStlAccInfo(Request $request)
    {
        $store_id = $request->get('store_id', '');

        $StoreInfo = Store::where('store_id', $store_id)->first();
        if (!$StoreInfo) {
            return json_encode([
                'status' => 2,
                'message' => '门店未认证请认证门店'
            ]);
        }
        $StoreBank = StoreBank::where('store_id', $store_id)->first();
        if (!$StoreBank) {
            return json_encode([
                'status' => 2,
                'message' => '没有绑定银行卡,请先绑定银行卡'
            ]);
        }

        $StoreImg = StoreImg::where('store_id', $store_id)->first();
        if (!$StoreImg) {
            return json_encode([
                'status' => 2,
                'message' => '没有商户资料'
            ]);
        }

        $isUncrpSett = 'N';
        if ($StoreInfo->head_name != $StoreBank->store_bank_name) {// 非法人结算
            $isUncrpSett = 'Y';
        }

        $YinshengStore = YinshengStore::where('store_id', $store_id)->first();
        if (!$YinshengStore) {
            return json_encode([
                'status' => 1,
                'message' => "请在'门店列表-通道管理'中补充资料"
            ]);
        }
        $custId = $YinshengStore->cust_id; //客户号
        $mercId = $YinshengStore->mer_code; //商户号

        $stlAccInfo = [
            'isSettInPlatAcc' => 'N',//是否平台内账户,Y是 N否，默认否
            'isUncrpSett' => $isUncrpSett,//是否非法人结算,Y是（对私） N否（对公），默认否(卡类型 02对公-01对私)
            'stlAccNm' => $StoreBank->store_bank_name,//结算户名
            'stlAccNo' => $StoreBank->store_bank_no,//结算账号
            'bankSubCode' => $StoreBank->bank_no,//开户支行联行号
            'stlAccType' => ($StoreBank->store_bank_type == '02') ? '21' : '11',//结算账户类型 11 对私 21 对公 23 对公存折 24 单位结算卡
            'bankMobile' => ($StoreBank->store_bank_type == '01') ? $StoreInfo->people_phone : '',//银行预留手机号 对私时必填
            'openCertNo' => ($StoreBank->store_bank_type == '01') ? $StoreBank->bank_sfz_no : '',//开开户证件号 对私时必填
            'bankProince' => $YinshengStore->province_code,    //银行开户行所属省代码
            'bankCity' => $YinshengStore->city_code    //银行开户行所属市代码
        ];

        $reqId = 'ys' . time() . mt_rand(10000, 99999);
        /** 生成对业务参数加密的随机密钥 */
        $key = $this->randomStr();
        $reqMap = $this->genCommonReqParam('smsc.changeMercStlAccInfo', $key, $reqId);
        /** 封装业务参数,具体参数见文档*/
        /** 使用生成的密钥key对业务参数进行加密，并将加密后的业务参数放入请求参数bizContent中*/

        $reqMap['bizContent'] = $this->encrypt($this->jsonEncode([
            'custId' => $custId,
            'mercId' => $mercId,
            'notifyUrl' => url('api/ysepay/merchant_notify_url'), // 异步通知地址
            'stlAccInfo' => $stlAccInfo
        ]), $key);

        // 签名
        $this->doSign($reqMap);
        $resMap = $this->post($this->ysBaseUrl . '/openapi/t1/smsc/changeMercStlAccInfo', $reqMap);
        $resMap = json_decode(base64_decode($resMap), true);
        if (isset($resMap) && !empty($resMap)) {
            $this->checkSign($resMap);
            $this->decryptBizData($resMap, $key);
            Log::info('---ysepay-结算信息变更申请---changeMercStlAccInfo----response  params:' . json_encode($resMap));
            if ($resMap['code'] == '00000' && $resMap['subCode'] == '0000') {
                $changeSysFlowId = $resMap['businessData']['changeSysFlowId']; //变更申请流水号

                $YinshengStore->update([
                    'change_sys_flowId' => $changeSysFlowId
                ]);

                //图片更新
                if ($StoreInfo->store_type == 1 || $StoreInfo->store_type == 2) {//1个体商户、2企业商户
                    if ($StoreBank->store_bank_type == '02') {// 02对公结算
                        $storeIndustryLicenseImg = $this->images_get($StoreImg->store_industrylicense_img);
                        $storeIndustryLicenseImgRes = $this->uploadChangePic($changeSysFlowId, 'A011', $storeIndustryLicenseImg);
                        if ($storeIndustryLicenseImgRes['code'] == '00000' && $storeIndustryLicenseImgRes['subCode'] == '0000') {

                        } else {
                            if ($storeIndustryLicenseImgRes['code'] != '00000') {
                                return json_encode([
                                    'status' => 2,
                                    'message' => $storeIndustryLicenseImgRes['msg']
                                ]);
                            }
                            return json_encode([
                                'status' => 2,
                                'message' => $storeIndustryLicenseImgRes['subCode'] . '-' . $storeIndustryLicenseImgRes['subMsg']
                            ]);
                        }

                    } else if ($StoreBank->store_bank_type == '01') {// 对私结算

                        $storeBankA = $this->images_get($StoreImg->bank_img_a); //A004--结算银行卡正面（有卡号一面）
                        $storeBankARes = $this->uploadChangePic($changeSysFlowId, 'A004', $storeBankA);
                        if ($storeBankARes['code'] == '00000' && $storeBankARes['subCode'] == '0000') {

                        } else {
                            if ($storeBankARes['code'] != '00000') {
                                return json_encode([
                                    'status' => 2,
                                    'message' => $storeBankARes['msg']
                                ]);
                            }
                            return json_encode([
                                'status' => 2,
                                'message' => $storeBankARes['subCode'] . '-' . $storeBankARes['subMsg']
                            ]);
                        }
                        $storeBankB = $this->images_get($StoreImg->bank_img_b); //A005--结算银行卡反面
                        $storeBankBRes = $this->uploadChangePic($changeSysFlowId, 'A005', $storeBankB);
                        if ($storeBankBRes['code'] == '00000' && $storeBankBRes['subCode'] == '0000') {

                        } else {
                            if ($storeBankBRes['code'] != '00000') {
                                return json_encode([
                                    'status' => 2,
                                    'message' => $storeBankBRes['msg']
                                ]);
                            }
                            return json_encode([
                                'status' => 2,
                                'message' => $storeBankBRes['subCode'] . '-' . $storeBankBRes['subMsg']
                            ]);
                        }

                        //01对私 && 非法人结算
                        if ($StoreInfo->head_name != $StoreBank->store_bank_name) { //非法人结算
                            $storeBankSfzA = $this->images_get($StoreImg->bank_sfz_img_a); //A013--被授权人身份证人像面
                            $storeBankSfzARes = $this->uploadChangePic($changeSysFlowId, 'A013', $storeBankSfzA);
                            if ($storeBankSfzARes['code'] == '00000' && $storeBankSfzARes['subCode'] == '0000') {

                            } else {
                                if ($storeBankSfzARes['code'] != '00000') {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => $storeBankSfzARes['msg']
                                    ]);
                                }
                                return json_encode([
                                    'status' => 2,
                                    'message' => $storeBankSfzARes['subCode'] . '-' . $storeBankSfzARes['subMsg']
                                ]);
                            }
                            $storeBankSfzB = $this->images_get($StoreImg->bank_sfz_img_b); //A014--被授权人身份证国徽面
                            $storeBankSfzBRes = $this->uploadChangePic($changeSysFlowId, 'A014', $storeBankSfzB);
                            if ($storeBankSfzBRes['code'] == '00000' && $storeBankSfzBRes['subCode'] == '0000') {

                            } else {
                                if ($storeBankSfzBRes['code'] != '00000') {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => $storeBankSfzBRes['msg']
                                    ]);
                                }
                                return json_encode([
                                    'status' => 2,
                                    'message' => $storeBankSfzBRes['subCode'] . '-' . $storeBankSfzBRes['subMsg']
                                ]);
                            }
                            $storeAuthBank = $this->images_get($StoreImg->store_auth_bank_img); //B005--结算授权书
                            $storeAuthBankRes = $this->uploadChangePic($changeSysFlowId, 'B005', $storeAuthBank);
                            if ($storeAuthBankRes['code'] == '00000' && $storeAuthBankRes['subCode'] == '0000') {

                            } else {
                                if ($storeAuthBankRes['code'] != '00000') {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => $storeAuthBankRes['msg']
                                    ]);
                                }
                                return json_encode([
                                    'status' => 2,
                                    'message' => $storeAuthBankRes['subCode'] . '-' . $storeAuthBankRes['subMsg']
                                ]);
                            }
                            $storeAuthBankFrRes = $this->uploadChangePic($changeSysFlowId, 'B004', $storeAuthBank); //B004--法人手持结算授权书
                            if ($storeAuthBankFrRes['code'] == '00000' && $storeAuthBankFrRes['subCode'] == '0000') {

                            } else {
                                if ($storeAuthBankFrRes['code'] != '00000') {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => $storeAuthBankFrRes['msg']
                                    ]);
                                }
                                return json_encode([
                                    'status' => 2,
                                    'message' => $storeAuthBankFrRes['subCode'] . '-' . $storeAuthBankFrRes['subMsg']
                                ]);
                            }
                        }
                    }

                } else { //小微商户

                    $storeBankA = $this->images_get($StoreImg->bank_img_a); //A004--结算银行卡正面（有卡号一面）
                    $storeBankARes = $this->uploadChangePic($changeSysFlowId, 'A004', $storeBankA);
                    if ($storeBankARes['code'] == '00000' && $storeBankARes['subCode'] == '0000') {

                    } else {
                        if ($storeBankARes['code'] != '00000') {
                            return json_encode([
                                'status' => 2,
                                'message' => $storeBankARes['msg']
                            ]);
                        }
                        return json_encode([
                            'status' => 2,
                            'message' => $storeBankARes['subCode'] . '-' . $storeBankARes['subMsg']
                        ]);
                    }
                    $storeBankB = $this->images_get($StoreImg->bank_img_b); //A005--结算银行卡反面
                    $storeBankBRes = $this->uploadChangePic($changeSysFlowId, 'A005', $storeBankB);
                    if ($storeBankBRes['code'] == '00000' && $storeBankBRes['subCode'] == '0000') {

                    } else {
                        if ($storeBankBRes['code'] != '00000') {
                            return json_encode([
                                'status' => 2,
                                'message' => $storeBankBRes['msg']
                            ]);
                        }
                        return json_encode([
                            'status' => 2,
                            'message' => $storeBankBRes['subCode'] . '-' . $storeBankBRes['subMsg']
                        ]);
                    }
                }

                $Images = $this->images_get($YinshengStore->business_application_img); //业务变更申请表  B008-其他1 B009-其他2
                $ImagesRes = $this->uploadChangePic($changeSysFlowId, 'B008', $Images);
                if ($ImagesRes['code'] == '00000' && $ImagesRes['subCode'] == '0000') {

                } else {
                    if ($ImagesRes['code'] != '00000') {
                        return json_encode([
                            'status' => 2,
                            'message' => $ImagesRes['msg']
                        ]);
                    }
                    return json_encode([
                        'status' => 2,
                        'message' => $ImagesRes['subCode'] . '-' . $ImagesRes['subMsg']
                    ]);
                }

                $storePayWay = StorePayWay::where('store_id', $store_id)
                    ->where('company', 'ysepay')
                    ->get();

                foreach ($storePayWay as $k => $v) {
                    $ways = StorePayWay::where('store_id', $store_id)
                        ->where('ways_source', $v->ways_source)
                        ->first();
                    if ($ways) {
                        $ways->update([
                            'status' => 1,
                            'status_desc' => '结算信息变更申请中'
                        ]);
                        $ways->save();
                    }
                }

                return json_encode([
                    'status' => 1,
                    'message' => '申请成功'
                ]);

            } else {
                if ($resMap['code'] != '00000') {
                    return json_encode([
                        'status' => 2,
                        'message' => $resMap['msg']
                    ]);
                }
                return json_encode([
                    'status' => 2,
                    'message' => $resMap['subMsg']
                ]);
            }
        } else {
            return json_encode([
                'status' => 2,
                'message' => "三方系统返回错误"
            ]);
        }
    }


    /**
     * 费率信息变更申请
     * @param $reqId string 流水号  要求32个字符内（最少14个字符），只能是数字、大小写字母_-且在同一个商户号下唯一
     * @param array $rateInfo 费率信息
     * @return array
     */
    public function changeRate(Request $request)
    {
        $store_id = $request->get('store_id', '');

        $YinshengStore = YinshengStore::where('store_id', $store_id)->first();
        if (!$YinshengStore) {
            return json_encode([
                'status' => 2,
                'message' => "请在'门店列表-通道管理'中补充资料"
            ]);
        }
        if (!$YinshengStore->business_application_img) {
            return json_encode([
                'status' => 2,
                'message' => '业务变更申请表-未上传'
            ]);
        }

        $storePayWay = StorePayWay::where('store_id', $store_id)
            ->where('company', 'ysepay')
            ->where('ways_source', 'weixin')
            ->first();

        if (!$storePayWay) {
            $this->status = 2;
            $this->message = '未设置费率';
            return $this->format();
        }
        $rate = $storePayWay->rate;

        $custId = $YinshengStore->cust_id;
        $mercId = $YinshengStore->mer_code;
        $bus_open_type = $YinshengStore->bus_open_type;

        $rateInfo = [
            'custId' => $custId,
            // 商户号
            'mercId' => $mercId,
            //异步通知url
            'notifyUrl' => url('api/ysepay/merchant_notify_url'), // 异步通知地址
        ];


        $openTypeArr = explode('|', $bus_open_type);
        foreach ($openTypeArr as $openType) {
            if ($openType == '00') { //00-扫码工作日到账
                $getCodeScanT1Fee = [
                    // 微信扫码费率
                    'wxPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $rate, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => "1", //最低收费 单位为分
                    ],
                    //支付宝扫码费率
                    'aliPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $rate, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => "1", //最低收费 单位为分
                    ],
                    //银联一档借记卡扫码费率(>1000)
                    'bank1debitPayFee' => [
                        'rateType' => '0',//收费方式 这里只支持0按百分比
                        'rateFee' => $YinshengStore->d_calc_val,//费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $YinshengStore->d_fee_low_limit,//最低收费 单位为分
                        'rateTop' => $YinshengStore->d_stlm_max_amt,//最高收费 单位为分
                    ],
                    //银联二档借记卡扫码费率(<=1000)
                    'bank2debitPayFee' => [
                        'rateType' => '0',//收费方式 这里只支持0按百分比
                        'rateFee' => $YinshengStore->d_calc_val,//费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $YinshengStore->d_fee_low_limit,//最低收费 单位为分
                        'rateTop' => $YinshengStore->d_stlm_max_amt,//最高收费 单位为分
                    ],
                    //银联一档贷记卡扫码费率(>1000)
                    'bank1creditPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $YinshengStore->c_calc_val, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $YinshengStore->c_fee_low_limit, //最低收费 单位为分
                    ],
                    //银联二档贷记卡扫码费率(<=1000)
                    'bank2creditPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $YinshengStore->c_calc_val, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $YinshengStore->c_fee_low_limit, //最低收费 单位为分
                    ]
                ];
                //扫码工作日到账费率 busOpenType=00时必填
                $rateInfo['codeScanT1Fee'] = $getCodeScanT1Fee;

            }
            if ($openType == '01') { //01-扫码实时到账
                $getCodeScanD0Fee = [
                    'rateType' => "0", //收费方式 这里只支持0按百分比
                    'rateFee' => 0.02, //费率 单位为% 填0.53代表0.53%
                    'rateBottom' => "1", //最低收费 单位为分
                ];

                //扫码实时到账垫资费 busOpenType=01时必填
                $rateInfo['codeScanD0Fee'] = $getCodeScanD0Fee;
                $getCodeScanT1Fee = [
                    // 微信扫码费率
                    'wxPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $rate - 0.02, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => "1", //最低收费 单位为分
                    ],
                    //支付宝扫码费率
                    'aliPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $rate - 0.02, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => "1", //最低收费 单位为分
                    ],
                    //银联一档借记卡扫码费率(>1000)
                    'bank1debitPayFee' => [
                        'rateType' => '0',//收费方式 这里只支持0按百分比
                        'rateFee' => $YinshengStore->d_calc_val,//费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $YinshengStore->d_fee_low_limit,//最低收费 单位为分
                        'rateTop' => $YinshengStore->d_stlm_max_amt,//最高收费 单位为分
                    ],
                    //银联二档借记卡扫码费率(<=1000)
                    'bank2debitPayFee' => [
                        'rateType' => '0',//收费方式 这里只支持0按百分比
                        'rateFee' => $YinshengStore->d_calc_val,//费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $YinshengStore->d_fee_low_limit,//最低收费 单位为分
                        'rateTop' => $YinshengStore->d_stlm_max_amt,//最高收费 单位为分
                    ],
                    //银联一档贷记卡扫码费率(>1000)
                    'bank1creditPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $YinshengStore->c_calc_val, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $YinshengStore->c_fee_low_limit, //最低收费 单位为分
                    ],
                    //银联二档贷记卡扫码费率(<=1000)
                    'bank2creditPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $YinshengStore->c_calc_val, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $YinshengStore->c_fee_low_limit, //最低收费 单位为分
                    ]
                ];
                //扫码工作日到账费率 busOpenType=00时必填
                $rateInfo['codeScanT1Fee'] = $getCodeScanT1Fee;

            }
            if ($openType == '20') { //20-D1到账
                $getD1Fee = [
                    'rateType' => '0',//收费方式 0 按百分比 1按固定金额
                    'rateFee' => 0.00,// 费率 按百分比时单位为% 按固定金额时 单位为分
                    'rateBottom' => "1",//最低收费 单位为分 按百分比时生效
                ];
                $getCodeScanT1Fee = [
                    // 微信扫码费率
                    'wxPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $rate - 0.01, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => "1", //最低收费 单位为分
                    ],
                    //支付宝扫码费率
                    'aliPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $rate - 0.01, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => "1", //最低收费 单位为分
                    ],
                    //银联一档借记卡扫码费率(>1000)
                    'bank1debitPayFee' => [
                        'rateType' => '0',//收费方式 这里只支持0按百分比
                        'rateFee' => $YinshengStore->d_calc_val,//费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $YinshengStore->d_fee_low_limit,//最低收费 单位为分
                        'rateTop' => $YinshengStore->d_stlm_max_amt,//最高收费 单位为分
                    ],
                    //银联二档借记卡扫码费率(<=1000)
                    'bank2debitPayFee' => [
                        'rateType' => '0',//收费方式 这里只支持0按百分比
                        'rateFee' => $YinshengStore->d_calc_val,//费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $YinshengStore->d_fee_low_limit,//最低收费 单位为分
                        'rateTop' => $YinshengStore->d_stlm_max_amt,//最高收费 单位为分
                    ],
                    //银联一档贷记卡扫码费率(>1000)
                    'bank1creditPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $YinshengStore->c_calc_val, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $YinshengStore->c_fee_low_limit, //最低收费 单位为分
                    ],
                    //银联二档贷记卡扫码费率(<=1000)
                    'bank2creditPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $YinshengStore->c_calc_val, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $YinshengStore->c_fee_low_limit, //最低收费 单位为分
                    ]
                ];
                $signData['codeScanT1Fee'] = $getCodeScanT1Fee;
                //天天到账垫资费 busOpenType=20时必填
                $rateInfo['d1Fee'] = $getD1Fee;
            }
        }

        $reqId = 'ys' . time() . mt_rand(10000, 99999);
        /** 生成对业务参数加密的随机密钥 */
        $key = $this->randomStr();
        $reqMap = $this->genCommonReqParam('smsc.changeRate', $key, $reqId);
        /** 封装业务参数,具体参数见文档*/
        /** 使用生成的密钥key对业务参数进行加密，并将加密后的业务参数放入请求参数bizContent中*/
        $reqMap['bizContent'] = $this->encrypt($this->jsonEncode($rateInfo), $key);
        // 签名
        $this->doSign($reqMap);
        $resMap = $this->post($this->ysBaseUrl . '/openapi/smsc/changeRate', $reqMap);
        $resMap = json_decode(base64_decode($resMap), true);
        if (isset($resMap) && !empty($resMap)) {
            $this->checkSign($resMap);
            $this->decryptBizData($resMap, $key);
            Log::info('---ysepay---费率信息变更申请---changeRate--response  params:' . json_encode($resMap));
            if ($resMap['code'] == '00000' && $resMap['subCode'] == '0000') {
                $changeSysFlowId = $resMap['businessData']['changeSysFlowId']; //变更申请流水号

                $YinshengStore->update([
                    'change_sys_flowId' => $changeSysFlowId
                ]);

                $Images = $this->images_get($YinshengStore->business_application_img); //业务变更申请表  B008-其他1 B009-其他2
                $ImagesRes = $this->uploadChangePic($changeSysFlowId, 'B008', $Images);
                if ($ImagesRes['code'] == '00000' && $ImagesRes['subCode'] == '0000') {

                } else {
                    if ($ImagesRes['code'] != '00000') {
                        return json_encode([
                            'status' => 2,
                            'message' => $ImagesRes['msg']
                        ]);
                    }
                    return json_encode([
                        'status' => 2,
                        'message' => $ImagesRes['subCode'] . '-' . $ImagesRes['subMsg']
                    ]);
                }

                $storePayWay = StorePayWay::where('store_id', $store_id)
                    ->where('company', 'ysepay')
                    ->get();

                foreach ($storePayWay as $k => $v) {
                    $ways = StorePayWay::where('store_id', $store_id)
                        ->where('ways_source', $v->ways_source)
                        ->first();
                    if ($ways) {
                        $ways->update([
                            'status' => 2,
                            'status_desc' => '费率信息变更申请中'
                        ]);
                        $ways->save();
                    }
                }

                return json_encode([
                    'status' => 1,
                    'message' => '申请成功'
                ]);
            } else {
                if ($resMap['code'] != '00000') {
                    return json_encode([
                        'status' => 2,
                        'message' => $resMap['msg']
                    ]);
                }
                return json_encode([
                    'status' => 2,
                    'message' => $resMap['subMsg']
                ]);
            }
        } else {
            return json_encode([
                'status' => 2,
                'message' => "三方系统返回错误"
            ]);
        }

    }

    /**
     * 上传变更图片
     * @param $reqId string 流水号  要求32个字符内（最少14个字符），只能是数字、大小写字母_-且在同一个商户号下唯一
     * @param $changeFlowId string 变更申请流水号 变更申请成功后返回
     * @param $picType string 图片类型， A001-营业执照 A002-法人身份证正面(头像面) A003-法人身份证反面(国徽面) A004-结算账户正面(卡号面) A005-结算账户反面 A006-商户门头照片 A007-内景照片 A008-收银台照片 A009-手持身份证合影照片 A010-收单协议盖章页 A011-开户许可证 A012-收单协议首页 A013-非法人身份证头像面 A014-非法人身份证国徽面 B001-租赁合同 第一页 B002-租赁合同 第二页 B003-租赁合同 第三页 B004-法人/非法人手持授权书 B005-法人/非法人结算授权书 B006-租赁面积图片 B007-经营业务图片 B008-其他1 B009-其他2
     * @param $filePath string 文件路径
     * @return array
     */
    public function uploadChangePic($changeFlowId, $picType, $filePath)
    {
        $reqId = 'ys' . time() . mt_rand(10000, 99999);
        /** 生成对业务参数加密的随机密钥 */
        $key = $this->randomStr();
        $reqMap = $this->genCommonReqParam('file.smsc.uploadChangePic', $key, $reqId);
        $sha256 = hash_file('sha256', $filePath);
        $picNm = basename($filePath);

        Log::info('---ysepay---图片上传---uploadChangePic----request params-->' . json_encode([
                'meta' => [
                    // 入网申请流水号,调用商户入网申请接口成功会返回入网申请流水号。
                    'changeFlowId' => $changeFlowId,
                    //图片类型， A001-营业执照 A002-法人身份证正面(头像面) A003-法人身份证反面(国徽面) A004-结算账户正面(卡号面) A005-结算账户反面 A006-商户门头照片 A007-内景照片 A008-收银台照片 A009-手持身份证合影照片 A010-收单协议盖章页 A011-开户许可证 A012-收单协议首页 A013-非法人身份证头像面 A014-非法人身份证国徽面 B001-租赁合同 第一页 B002-租赁合同 第二页 B003-租赁合同 第三页 B004-法人/非法人手持授权书 B005-法人/非法人结算授权书 B006-租赁面积图片 B007-经营业务图片 B008-其他1 B009-其他2
                    'picType' => $picType,
                    // 图片名称
                    'picNm' => $picNm,
                    // sha256
                    'sha256' => $sha256,
                ]
            ]));

        /** 封装业务参数,具体参数见文档*/
        /** 使用生成的密钥key对业务参数进行加密，并将加密后的业务参数放入请求参数bizContent中*/
        $reqMap['bizContent'] = $this->encrypt($this->jsonEncode([
            'meta' => [
                //变更申请流水号 变更申请成功后返回
                'changeFlowId' => $changeFlowId,
                //图片类型， A001-营业执照 A002-法人身份证正面(头像面) A003-法人身份证反面(国徽面) A004-结算账户正面(卡号面) A005-结算账户反面 A006-商户门头照片 A007-内景照片 A008-收银台照片 A009-手持身份证合影照片 A010-收单协议盖章页 A011-开户许可证 A012-收单协议首页 A013-非法人身份证头像面 A014-非法人身份证国徽面 B001-租赁合同 第一页 B002-租赁合同 第二页 B003-租赁合同 第三页 B004-法人/非法人手持授权书 B005-法人/非法人结算授权书 B006-租赁面积图片 B007-经营业务图片 B008-其他1 B009-其他2
                'picType' => $picType,
                // 图片名称
                'picNm' => $picNm,
                // sha256
                'sha256' => $sha256,
            ]
        ]), $key);
        // 签名
        $this->doSign($reqMap);
        $reqMap['file'] = new \CURLFile(realpath($filePath));
        $resMap = $this->post($this->ysBaseUrl . '/openapi/file/smsc/uploadChangePic', $reqMap);
        $resMap = json_decode(base64_decode($resMap), true);
        $this->checkSign($resMap);
        $this->decryptBizData($resMap, $key);
        Log::info('---ysepay-上传变更图片---uploadChangePic----resMap-->' . json_encode($resMap));
        return $resMap;
    }

    /**
     * 查询变更状态
     * @param $reqId string 流水号  要求32个字符内（最少14个字符），只能是数字、大小写字母_-且在同一个商户号下唯一
     * @param $changeSysFlowId string 入网申请流水号,调用商户入网申请接口成功会返回入网申请流水号
     * @return array
     */
    public function queryCustChange($reqId, $changeSysFlowId)
    {
        /** 生成对业务参数加密的随机密钥 */
        $key = $this->randomStr();
        $reqMap = $this->genCommonReqParam('smsc.queryCustChange', $key, $reqId);
        /** 封装业务参数,具体参数见文档*/
        /** 使用生成的密钥key对业务参数进行加密，并将加密后的业务参数放入请求参数bizContent中*/
        $reqMap['bizContent'] = $this->encrypt($this->jsonEncode([
            // 入网申请流水号,调用商户入网申请接口成功会返回入网申请流水号。
            'changeSysFlowId' => $changeSysFlowId,
        ]), $key);
        // 签名
        $this->doSign($reqMap);
        $resMap = $this->post($this->ysBaseUrl . '/openapi/smsc/queryCustChange', $reqMap);
        $resMap = json_decode(base64_decode($resMap), true);
        $this->checkSign($resMap);
        $this->decryptBizData($resMap, $key);
        Log::info('---ysepay-查询变更状态---queryCustChange----resMap-->' . json_encode($resMap));
        return $resMap;
    }

    /**
     * 图片上传
     * @param $reqId string 流水号  要求32个字符内（最少14个字符），只能是数字、大小写字母_-且在同一个商户号下唯一
     * @param $sysFlowId string 入网申请流水号,调用商户入网申请接口成功会返回入网申请流水号。
     * @param $picType string 图片类型， A001-营业执照 A002-法人身份证正面(头像面) A003-法人身份证反面(国徽面) A004-结算账户正面(卡号面) A005-结算账户反面 A006-商户门头照片 A007-内景照片 A008-收银台照片 A009-手持身份证合影照片 A010-收单协议盖章页 A011-开户许可证 A012-收单协议首页 A013-非法人身份证头像面 A014-非法人身份证国徽面 B001-租赁合同 第一页 B002-租赁合同 第二页 B003-租赁合同 第三页 B004-法人/非法人手持授权书 B005-法人/非法人结算授权书 B006-租赁面积图片 B007-经营业务图片 B008-其他1 B009-其他2
     * @param $filePath string 文件路径
     * @return array
     */
    public function imageUpload($sysFlowId, $picType, $filePath)
    {
        $reqId = 'ys' . time() . mt_rand(10000, 99999);
        /** 生成对业务参数加密的随机密钥 */
        $key = $this->randomStr();
        $reqMap = $this->genCommonReqParam('file.smsc.upload', $key, $reqId);
        $sha256 = hash_file('sha256', $filePath);
        $picNm = basename($filePath);

        Log::info('---ysepay---图片上传---imageUpload----request params-->' . json_encode([
                'meta' => [
                    // 入网申请流水号,调用商户入网申请接口成功会返回入网申请流水号。
                    'sysFlowId' => $sysFlowId,
                    //图片类型， A001-营业执照 A002-法人身份证正面(头像面) A003-法人身份证反面(国徽面) A004-结算账户正面(卡号面) A005-结算账户反面 A006-商户门头照片 A007-内景照片 A008-收银台照片 A009-手持身份证合影照片 A010-收单协议盖章页 A011-开户许可证 A012-收单协议首页 A013-非法人身份证头像面 A014-非法人身份证国徽面 B001-租赁合同 第一页 B002-租赁合同 第二页 B003-租赁合同 第三页 B004-法人/非法人手持授权书 B005-法人/非法人结算授权书 B006-租赁面积图片 B007-经营业务图片 B008-其他1 B009-其他2
                    'picType' => $picType,
                    // 图片名称
                    'picNm' => $picNm,
                    // sha256
                    'sha256' => $sha256,
                ]
            ]));


        /** 封装业务参数,具体参数见文档*/
        /** 使用生成的密钥key对业务参数进行加密，并将加密后的业务参数放入请求参数bizContent中*/
        $reqMap['bizContent'] = $this->encrypt($this->jsonEncode([
            'meta' => [
                // 入网申请流水号,调用商户入网申请接口成功会返回入网申请流水号。
                'sysFlowId' => $sysFlowId,
                //图片类型， A001-营业执照 A002-法人身份证正面(头像面) A003-法人身份证反面(国徽面) A004-结算账户正面(卡号面) A005-结算账户反面 A006-商户门头照片 A007-内景照片 A008-收银台照片 A009-手持身份证合影照片 A010-收单协议盖章页 A011-开户许可证 A012-收单协议首页 A013-非法人身份证头像面 A014-非法人身份证国徽面 B001-租赁合同 第一页 B002-租赁合同 第二页 B003-租赁合同 第三页 B004-法人/非法人手持授权书 B005-法人/非法人结算授权书 B006-租赁面积图片 B007-经营业务图片 B008-其他1 B009-其他2
                'picType' => $picType,
                // 图片名称
                'picNm' => $picNm,
                // sha256
                'sha256' => $sha256,
            ]
        ]), $key);
        // 签名
        $this->doSign($reqMap);
        //图片文件,将媒体图片进行二进制转换，得到的媒体图片二进制内容，在请求body中上传此二进制内容。媒体图片只支持JPG、JPEG、BMP、PNG格式，文件大小不能超过2M
        $reqMap['file'] = new \CURLFile(realpath($filePath));

        $resMap = $this->post($this->ysBaseUrl . $this->uploadUrl, $reqMap);
        $resMap = json_decode(base64_decode($resMap), true);
        $this->checkSign($resMap);
        $this->decryptBizData($resMap, $key);
        Log::info('---ysepay-图片上传---imageUpload----response params-->' . json_encode($resMap));

        return $resMap;
    }

    /**
     * 开通/关闭线上D0权限
     */
    public function onlineOpen(Request $request)
    {
        $store_id = $request->get('store_id', '');
        $option = $request->get('option', 'ON'); //ON(开通);OFF(关闭)
        $contractInfos = YinshengContractInfos::where('store_id', $store_id)->first();

        if ($contractInfos) {
            $mercId = $contractInfos->mer_code;
        } else {
            return json_encode([
                'status' => 2,
                'message' => '商户号不能为空'
            ]);
        }

        Log::info('---ysepay---开通/关闭线上D0权限-----request  params-->' . json_encode([
                'mercId' => $mercId,
                'option' => $option,
                'rateFee' => 0.02,
                'rateBottom' => 1,
            ]));

        $reqId = 'ys' . time() . mt_rand(10000, 99999);
        /** 生成对业务参数加密的随机密钥 */
        $key = $this->randomStr();
        $reqMap = $this->genCommonReqParam('smsc.saas.authority.online', $key, $reqId);
        /** 封装业务参数,具体参数见文档*/
        /** 使用生成的密钥key对业务参数进行加密，并将加密后的业务参数放入请求参数bizContent中*/
        $reqMap['bizContent'] = $this->encrypt($this->jsonEncode([
            'mercId' => $mercId,
            'option' => $option,
            'rateFee' => 0.02,
            'rateBottom' => 1,
        ]), $key);
        // 签名
        $this->doSign($reqMap);
        $resMap = $this->post($this->ysBaseUrl . '/openapi/smsc/saas/authority/online', $reqMap);
        $resMap = json_decode(base64_decode($resMap), true);
        if (isset($resMap) && !empty($resMap)) {
            $this->checkSign($resMap);
            $this->decryptBizData($resMap, $key);
            Log::info('---ysepay---开通/关闭线上D0权限-----response  params-->' . json_encode($resMap));
            if ($resMap['code'] == '00000' && $resMap['subCode'] == '0000') {

                $contractInfos->update([
                    'd0_is_open' => $option == 'ON' ? '1' : '0',
                ]);

                return json_encode([
                    'status' => 1,
                    'message' => $resMap['subMsg']
                ]);
            } else {
                if ($resMap['code'] != '00000') {
                    return json_encode([
                        'status' => 2,
                        'message' => $resMap['msg']
                    ]);
                }
                return json_encode([
                    'status' => 2,
                    'message' => $resMap['subMsg']
                ]);
            }
        } else {
            return json_encode([
                'status' => 2,
                'message' => "三方系统返回错误"
            ]);
        }
    }


    /**
     * 合同签约
     * @param $reqId string 流水号  要求32个字符内（最少14个字符），只能是数字、大小写字母_-且在同一个商户号下唯一
     * @param $signData array 签约数据
     */
    public function smscSign(Request $request)
    {
        $store_id = $request->get('store_id', ''); //门店id
        $bus_open_type = $request->get('bus_open_type', '00'); //到账方式( 00-扫码工作日到账01-扫码实时到账10-刷卡工作日到账11-刷卡实时到账20-D1到账)允许多选用“|”分隔 T1工作日到账必选
        $d_calc_type = $request->get('d_calc_type', '0'); //借记卡计费类型(1-按笔数(单位:分);0-按比例(单位:%)),实时入账必传
        $c_calc_type = $request->get('c_calc_type', '0'); //信用卡计费类型(1-按笔数(单位:分);0-按比例(单位:%)),实时入账必传
        $d_calc_val = $request->get('d_calc_val', ''); //借记卡扣率
        $d_stlm_max_amt = $request->get('d_stlm_max_amt', ''); //借记卡封顶金额单位:分
        $d_fee_low_limit = $request->get('d_fee_low_limit', ''); //借记卡手续费最低值单位:分
        $c_calc_val = $request->get('c_calc_val', ''); //信用卡扣率
        $c_fee_low_limit = $request->get('c_fee_low_limit', ''); //信用卡手续费最低值单位:分

        $except_token_data = $request->except(['token']);

        $validate = Validator::make($except_token_data, [
            'store_id' => 'required|max:50',
            'bus_open_type' => 'required',
            'd_calc_type' => 'required',
            'c_calc_type' => 'required',
            'd_calc_val' => 'required|max:12',
            'd_stlm_max_amt' => 'required|max:8',
            'd_fee_low_limit' => 'required|max:8',
            'c_calc_val' => 'required|max:12',
            'c_fee_low_limit' => 'required|max:8',
        ], [
            'required' => ':attribute参数为必填项',
            'min' => ':attribute参数长度太短',
            'max' => ':attribute参数长度太长',
            'unique' => ':attribute参数已经被人占用',
            'exists' => ':attribute参数不存在',
            'integer' => ':attribute参数必须是数字',
            'required_if' => ':attribute参数满足条件时必传',
        ], [
            'store_id' => '门店id',
            'bus_open_type' => '到账方式',
            'd_calc_val' => '借记卡扣率',
            'd_stlm_max_amt' => '借记卡封顶金额',
            'd_fee_low_limit' => '借记卡手续费最低值',
            'c_calc_val' => '信用卡扣率',
            'c_fee_low_limit' => '信用卡手续费最低值',
            'd_calc_type' => '借记卡计费类型',
            'c_calc_type' => '信用卡计费类型',
        ]);
        if ($validate->fails()) {
            $this->status = 2;
            $this->message = $validate->getMessageBag()->first();
            return $this->format();
        }

        $insert_data = [
            'store_id' => $store_id,
            'contract_type' => 2, //合同类型,1-纸质合同 2-电子合同
            'bus_open_type' => $bus_open_type,
            'd_calc_type' => $d_calc_type,
            'c_calc_type' => $c_calc_type,
            'd_calc_val' => $d_calc_val,
            'd_stlm_max_amt' => $d_stlm_max_amt,
            'd_fee_low_limit' => $d_fee_low_limit,
            'c_calc_val' => $c_calc_val,
            'c_fee_low_limit' => $c_fee_low_limit,
            'd_calc_val' => $d_calc_val,
            'd_stlm_max_amt' => $d_stlm_max_amt,
            'd_fee_low_limit' => $d_fee_low_limit,
            'c_calc_val' => $c_calc_val,
            'c_fee_low_limit' => $c_fee_low_limit,
        ];

        $contractInfos = YinshengContractInfos::where('store_id', $store_id)->first();

        if ($contractInfos) {
            $result = $contractInfos->update($insert_data);
            if (!$result) {
                $this->status = 2;
                $this->message = '更新失败';
                return $this->format();
            }
        } else {
            $result = YinshengContractInfos::create($insert_data);
            if (!$result) {
                $this->status = 2;
                $this->message = '添加失败';
                return $this->format();
            }
            $contractInfos = YinshengContractInfos::where('store_id', $store_id)->first();
        }

        $storePayWay = StorePayWay::where('store_id', $store_id)
            ->where('company', 'ysepay')
            ->where('ways_source', 'weixin')
            ->first();

        if (!$storePayWay) {
            $this->status = 2;
            $this->message = '未设置费率';
            return $this->format();
        }
        $rate = $storePayWay->rate;

        $YinshengStore = YinshengStore::where('store_id', $store_id)->first();
        if (!$YinshengStore) {
            return json_encode([
                'status' => 2,
                'message' => "请在'门店列表-通道管理'中补充资料"
            ]);
        }
        if (empty($YinshengStore->cust_id)) {
            return json_encode([
                'status' => 2,
                'message' => "请先完成资料确认"
            ]);
        }
        $custId = $YinshengStore->cust_id;

        $signData = [
            // 客户号
            'custId' => $custId,
            // 到账方式 00-扫码工作日到账01-扫码实时到账10-刷卡工作日到账11-刷卡实时到账20-D1到账 允许多选用“|”分隔 T1工作日到账必选
            'busOpenType' => $bus_open_type,
            //合同类型 1-纸质合同 2-电子合同
            'contractType' => '2',
            //签约通知标识 contractType=2时必填 0(短信+邮件) 1(短信) 2(邮件) 3(不通知)
            'isSendConMsg' => '1',
            //异步通知url
            'notifyUrl' => url('api/ysepay/merchant_notify_url'), // 异步通知地址

        ];


        $openTypeArr = explode('|', $bus_open_type);
        foreach ($openTypeArr as $openType) {
            if ($openType == '00') { //00-扫码工作日到账
                $getCodeScanT1Fee = [
                    // 微信扫码费率
                    'wxPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $rate, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => "1", //最低收费 单位为分
                    ],
                    //支付宝扫码费率
                    'aliPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $rate, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => "1", //最低收费 单位为分
                    ],
                    //银联一档借记卡扫码费率(>1000)
                    'bank1debitPayFee' => [
                        'rateType' => '0',//收费方式 这里只支持0按百分比
                        'rateFee' => $d_calc_val,//费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $d_fee_low_limit,//最低收费 单位为分
                        'rateTop' => $d_stlm_max_amt,//最高收费 单位为分
                    ],
                    //银联二档借记卡扫码费率(<=1000)
                    'bank2debitPayFee' => [
                        'rateType' => '0',//收费方式 这里只支持0按百分比
                        'rateFee' => $d_calc_val,//费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $d_fee_low_limit,//最低收费 单位为分
                        'rateTop' => $d_stlm_max_amt,//最高收费 单位为分
                    ],
                    //银联一档贷记卡扫码费率(>1000)
                    'bank1creditPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $c_calc_val, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $c_fee_low_limit, //最低收费 单位为分
                    ],
                    //银联二档贷记卡扫码费率(<=1000)
                    'bank2creditPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $c_calc_val, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $c_fee_low_limit, //最低收费 单位为分
                    ]
                ];

                //扫码工作日到账费率 busOpenType=00时必填
                $signData['codeScanT1Fee'] = $getCodeScanT1Fee;
            }
            if ($openType == '01') { //00-扫码实时入账 签约费率

            }
            if ($openType == '10') { //10-刷卡工作日到账
                $getSwCardT1Fee = [
                    'debitPayFee' => [
                        'rateType' => '0',//收费方式 这里只支持0按百分比
                        'rateFee' => $d_calc_val,//费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $d_fee_low_limit,//最低收费 单位为分
                        'rateTop' => $d_stlm_max_amt,//最高收费 单位为分
                    ],
                    'creditPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $c_calc_val, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $c_fee_low_limit, //最低收费 单位为分
                    ]
                ];

                $signData['swCardT1Fee'] = $getSwCardT1Fee;
            }
            if ($openType == '01') { //01-扫码实时到账
                $getCodeScanD0Fee = [
                    'rateType' => "0", //收费方式 这里只支持0按百分比
                    'rateFee' => 0.02, //费率 单位为% 填0.53代表0.53%
                    'rateBottom' => "1", //最低收费 单位为分
                ];
                $signData['codeScanD0Fee'] = $getCodeScanD0Fee;
                $getCodeScanT1Fee = [
                    // 微信扫码费率
                    'wxPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $rate - 0.02, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => "1", //最低收费 单位为分
                    ],
                    //支付宝扫码费率
                    'aliPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $rate - 0.02, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => "1", //最低收费 单位为分
                    ],
                    //银联一档借记卡扫码费率(>1000)
                    'bank1debitPayFee' => [
                        'rateType' => '0',//收费方式 这里只支持0按百分比
                        'rateFee' => $d_calc_val,//费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $d_fee_low_limit,//最低收费 单位为分
                        'rateTop' => $d_stlm_max_amt,//最高收费 单位为分
                    ],
                    //银联二档借记卡扫码费率(<=1000)
                    'bank2debitPayFee' => [
                        'rateType' => '0',//收费方式 这里只支持0按百分比
                        'rateFee' => $d_calc_val,//费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $d_fee_low_limit,//最低收费 单位为分
                        'rateTop' => $d_stlm_max_amt,//最高收费 单位为分
                    ],
                    //银联一档贷记卡扫码费率(>1000)
                    'bank1creditPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $c_calc_val, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $c_fee_low_limit, //最低收费 单位为分
                    ],
                    //银联二档贷记卡扫码费率(<=1000)
                    'bank2creditPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $c_calc_val, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $c_fee_low_limit, //最低收费 单位为分
                    ]
                ];

                //扫码工作日到账费率 busOpenType=00时必填
                $signData['codeScanT1Fee'] = $getCodeScanT1Fee;
            }
            if ($openType == '11') { //11-刷卡实时到账
                $getSwCardD0Fee = [
                    'debitPayFee' => [
                        'rateType' => $d_calc_type,//收费方式 0 按百分比 1按固定金额
                        'rateFee' => $d_calc_val,// 费率 按百分比时单位为% 按固定金额时 单位为分
                        'rateBottom' => $d_fee_low_limit,//最低收费 单位为分 按百分比时生效
                        'rateTop' => $d_stlm_max_amt,//最高收费 单位为分 按百分比时生效
                    ],
                    'creditPayFee' => [
                        'rateType' => $c_calc_type,//收费方式 0 按百分比 1按固定金额
                        'rateFee' => $c_calc_val,// 费率 按百分比时单位为% 按固定金额时 单位为分
                        'rateBottom' => $c_fee_low_limit,//最低收费 单位为分 按百分比时生效
                        'rateTop' => $d_stlm_max_amt,//最高收费 单位为分 按百分比时生效
                    ]
                ];

                $signData['swCardD0Fee'] = $getSwCardD0Fee;
            }
            if ($openType == '20') { //20-D1到账
                $getD1Fee = [
                    'rateType' => '0',//收费方式 0 按百分比 1按固定金额
                    'rateFee' => 0.00,// 费率 按百分比时单位为% 按固定金额时 单位为分
                    'rateBottom' => "1",//最低收费 单位为分 按百分比时生效
                ];
                $getCodeScanT1Fee = [
                    // 微信扫码费率
                    'wxPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $rate - 0.01, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => "1", //最低收费 单位为分
                    ],
                    //支付宝扫码费率
                    'aliPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $rate - 0.01, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => "1", //最低收费 单位为分
                    ],
                    //银联一档借记卡扫码费率(>1000)
                    'bank1debitPayFee' => [
                        'rateType' => '0',//收费方式 这里只支持0按百分比
                        'rateFee' => $d_calc_val,//费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $d_fee_low_limit,//最低收费 单位为分
                        'rateTop' => $d_stlm_max_amt,//最高收费 单位为分
                    ],
                    //银联二档借记卡扫码费率(<=1000)
                    'bank2debitPayFee' => [
                        'rateType' => '0',//收费方式 这里只支持0按百分比
                        'rateFee' => $d_calc_val,//费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $d_fee_low_limit,//最低收费 单位为分
                        'rateTop' => $d_stlm_max_amt,//最高收费 单位为分
                    ],
                    //银联一档贷记卡扫码费率(>1000)
                    'bank1creditPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $c_calc_val, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $c_fee_low_limit, //最低收费 单位为分
                    ],
                    //银联二档贷记卡扫码费率(<=1000)
                    'bank2creditPayFee' => [
                        'rateType' => "0", //收费方式 这里只支持0按百分比
                        'rateFee' => $c_calc_val, //费率 单位为% 填0.53代表0.53%
                        'rateBottom' => $c_fee_low_limit, //最低收费 单位为分
                    ]
                ];
                $signData['codeScanT1Fee'] = $getCodeScanT1Fee;
                $signData['d1Fee'] = $getD1Fee;
            }
        }

        $reqId = 'ys' . time() . mt_rand(10000, 99999);

        /** 生成对业务参数加密的随机密钥 */
        $key = $this->randomStr();
        $reqMap = $this->genCommonReqParam('smsc.sign', $key, $reqId);
        /** 封装业务参数,具体参数见文档*/
        /** 使用生成的密钥key对业务参数进行加密，并将加密后的业务参数放入请求参数bizContent中*/
        $reqMap['bizContent'] = $this->encrypt($this->jsonEncode($signData), $key);
        // 签名
        $this->doSign($reqMap);
        $resMap = $this->post($this->ysBaseUrl . '/openapi/t1/smsc/sign', $reqMap);
        $resMap = json_decode(base64_decode($resMap), true);
        if (isset($resMap) && !empty($resMap)) {
            $this->checkSign($resMap);
            $this->decryptBizData($resMap, $key);
            Log::info('---ysepay---合同签约---smscSign----response  params-->' . json_encode($resMap));
            if ($resMap['code'] == '00000' && $resMap['subCode'] == '0000') {
                $signUrl = $resMap['businessData']['signUrl']; //签约地址
                $signId = $resMap['businessData']['signId']; //签约id
                $authId = $resMap['businessData']['authId'];  //权限id

                $contractInfos->update([
                    'sign_id' => $signId,
                    'sign_url' => $signUrl,
                    'auth_id' => $authId
                ]);

                return json_encode([
                    'status' => 1,
                    'message' => $resMap['subMsg']
                ]);

            } else {
                if ($resMap['code'] != '00000') {
                    return json_encode([
                        'status' => 2,
                        'message' => $resMap['msg']
                    ]);
                }
                return json_encode([
                    'status' => 2,
                    'message' => $resMap['subMsg']
                ]);
            }

        } else {
            return json_encode([
                'status' => 2,
                'message' => "三方系统返回错误"
            ]);
        }
    }

    /**
     * 重发签约短信或邮件
     * @param $reqId string 流水号  要求32个字符内（最少14个字符），只能是数字、大小写字母_-且在同一个商户号下唯一
     * @param $signId string 签约ID,发起签约时候返回
     * @param $isSendConMsg string 签约通知标识 contractType=2时必填 0(短信+邮件) 1(短信) 2(邮件) 3(不通知)
     */
    public function sendSmsOrEmailMsg(Request $request)
    {
        $store_id = $request->get('store_id', '');

        $contractInfos = YinshengContractInfos::where('store_id', $store_id)->first();

        if ($contractInfos) {
            $signId = $contractInfos->sign_id;
        } else {
            $this->status = 2;
            $this->message = '请先申请签署合同';
            return $this->format();
        }

        $reqId = 'ys' . time() . mt_rand(10000, 99999);
        /** 生成对业务参数加密的随机密钥 */
        $key = $this->randomStr();
        $reqMap = $this->genCommonReqParam('smsc.sign.sendSmsOrEmailMsg', $key, $reqId);
        /** 封装业务参数,具体参数见文档*/
        /** 使用生成的密钥key对业务参数进行加密，并将加密后的业务参数放入请求参数bizContent中*/
        $reqMap['bizContent'] = $this->encrypt($this->jsonEncode([
            'signId' => $signId,
            'isSendConMsg' => 1, //通知方式,0 短信+邮件 1 短信 2邮件 3不通知
        ]), $key);
        // 签名
        $this->doSign($reqMap);
        $resMap = $this->post($this->ysBaseUrl . '/openapi/smsc/sign/sendSmsOrEmailMsg', $reqMap);
        $resMap = json_decode(base64_decode($resMap), true);
        if (isset($resMap) && !empty($resMap)) {
            $this->checkSign($resMap);
            $this->decryptBizData($resMap, $key);
            Log::info('---ysepay---重发签约短信或邮件-----response  params-->' . json_encode($resMap));
            if ($resMap['code'] == '00000' && $resMap['subCode'] == '0000') {

                $signUrl = $resMap['businessData']['signUrl']; //签约地址
                $signId = $resMap['businessData']['signId']; //签约id

                $contractInfos->update([
                    'sign_id' => $signId,
                    'sign_url' => $signUrl
                ]);

                return json_encode([
                    'status' => 1,
                    'message' => $resMap['subMsg']
                ]);
            } else {
                if ($resMap['code'] != '00000') {
                    return json_encode([
                        'status' => 2,
                        'message' => $resMap['msg']
                    ]);
                }
                return json_encode([
                    'status' => 2,
                    'message' => $resMap['subMsg']
                ]);
            }
        } else {
            return json_encode([
                'status' => 2,
                'message' => "三方系统返回错误"
            ]);
        }
    }

    /**
     * 电子合同查询签约状态
     * @param $reqId string 流水号  要求32个字符内（最少14个字符），只能是数字、大小写字母_-且在同一个商户号下唯一
     * @param $signId string 客户签约流水号
     */
    public function queryContract(Request $request)
    {
        $store_id = $request->get('store_id', '');
        $type = $request->get('type', '1'); //2查询信息

        $contractInfos = YinshengContractInfos::where('store_id', $store_id)->first();

        if ($type == 2) {
            $this->status = 1;
            $this->message = '返回数据成功';
            return $this->format($contractInfos);
        }

        if ($contractInfos) {
            $signId = $contractInfos->sign_id;
            $authId = $contractInfos->auth_id;
            $sign_status = $contractInfos->sign_status;
            if ($sign_status == '00') { //签署成功
                return json_encode([
                    'status' => 1,
                    'message' => '签署成功'
                ]);
            }
        } else {
            $this->status = 2;
            $this->message = '请先申请签署合同';
            return $this->format();
        }

        $reqId = 'ys' . time() . mt_rand(10000, 99999);
        /** 生成对业务参数加密的随机密钥 */
        $key = $this->randomStr();
        $reqMap = $this->genCommonReqParam('smsc.saas.constract.queryAuthInfo', $key, $reqId);

        /** 封装业务参数,具体参数见文档*/
        /** 使用生成的密钥key对业务参数进行加密，并将加密后的业务参数放入请求参数bizContent中*/
        $reqMap['bizContent'] = $this->encrypt($this->jsonEncode([
            'authId' => $authId,
        ]), $key);
        // 签名
        $this->doSign($reqMap);
        $resMap = $this->post($this->ysBaseUrl . '/openapi/smsc/saas/constract/queryAuthInfo', $reqMap);
        $resMap = json_decode(base64_decode($resMap), true);
        if (isset($resMap) && !empty($resMap)) {
            $this->checkSign($resMap);
            $this->decryptBizData($resMap, $key);
            Log::info('---ysepay---获取签约结果-----response  params-->' . json_encode($resMap));
            if ($resMap['code'] == '00000' && $resMap['subCode'] == '0000') {

                $status = $resMap['businessData']['status']; //状态,00-成功、01-初始化、02-签约中(电子合同才会出现此状态)、03-待审核(纸质合同或发起方不支持自动审核会出现此状态)、04-审核拒绝。
                $note = ''; //备注,审核拒绝才有值
                $mercId = '';
                if ($status == '00') {
                    $mercId = $resMap['businessData']['mercId']; //商户号,成功时才有值
                    $note = '成功';
                } else if ($status == '01') {
                    $note = '初始化';
                } else if ($status == '02') {
                    $note = '签约中';
                } else if ($status == '03') {
                    $note = '待审核';
                } else if ($status == '04') {
                    $note = $resMap['businessData']['note'];
                }
                $contractInfos->update([
                    'mer_code' => $mercId,
                    'sign_status' => $status,
                    'sign_msg' => $note
                ]);

                return json_encode([
                    'status' => 1,
                    'message' => $note
                ]);

            } else {
                if ($resMap['code'] != '00000') {
                    return json_encode([
                        'status' => 2,
                        'message' => $resMap['msg']
                    ]);
                }
                return json_encode([
                    'status' => 2,
                    'message' => $resMap['subMsg']
                ]);
            }
        } else {
            return json_encode([
                'status' => 2,
                'message' => "三方系统返回错误"
            ]);
        }
    }

    /**
     * 电子合同下载
     * @param $reqId string 流水号  要求32个字符内（最少14个字符），只能是数字、大小写字母_-且在同一个商户号下唯一
     * @param $signId string 签约ID,发起签约时候返回
     * @param $savePath string 保存文件路径,为空就不保存
     * @return array
     */
    public function downloadContract(Request $request)
    {
        $store_id = $request->get('store_id', '');

        $contractInfos = YinshengContractInfos::where('store_id', $store_id)->first();

        if ($contractInfos) {
            $signId = $contractInfos->sign_id;
            $merCode = $contractInfos->mer_code;
        } else {
            $this->status = 2;
            $this->message = '请先申请签署合同';
            return $this->format();
        }

        $reqId = 'ys' . time() . mt_rand(10000, 99999);
        /** 生成对业务参数加密的随机密钥 */
        $key = $this->randomStr();
        $reqMap = $this->genCommonReqParam('smsc.sign.downloadContract', $key, $reqId);
        /** 封装业务参数,具体参数见文档*/
        /** 使用生成的密钥key对业务参数进行加密，并将加密后的业务参数放入请求参数bizContent中*/
        $reqMap['bizContent'] = $this->encrypt($this->jsonEncode([
            'signId' => $signId,
        ]), $key);
        // 签名
        $this->doSign($reqMap);
        $resMap = $this->post($this->ysBaseUrl . '/openapi/smsc/sign/downloadContract', $reqMap);
        $resMap = json_decode(base64_decode($resMap), true);
        if (isset($resMap) && !empty($resMap)) {
            $this->checkSign($resMap);
            $this->decryptBizData($resMap, $key);
            Log::info('---ysepay---下载电子合同-----response  params-->' . json_encode($resMap));
            if ($resMap['code'] == '00000' && !empty($savePath) && $resMap['subCode'] == '0000') {
                $businessData = $resMap['businessData'];

                $this->status = 1;
                $this->message = $merCode;
                return $this->format($businessData['contractFileString']);

                // if (!empty($businessData['contractFileString'])) {
                //     FileUtil::saveFile($savePath, base64_decode($businessData['contractFileString']));
                // }

            } else {
                if ($resMap['code'] != '00000') {
                    return json_encode([
                        'status' => 2,
                        'message' => $resMap['msg']
                    ]);
                }
                return json_encode([
                    'status' => 2,
                    'message' => $resMap['subMsg']
                ]);
            }
        } else {
            return json_encode([
                'status' => 2,
                'message' => "三方系统返回错误"
            ]);
        }

    }


    /**
     * MCC码查询
     * @return array
     */
    public function queryMccList(Request $request)
    {
        $type = $request->get('type', 'parent'); //parent child
        $mch_type = $request->get('mch_type', ''); //分类名称

        $where = [];
        if ($mch_type) {
            $where[] = ['mch_type', '=', $mch_type];
        }

        if ($type == 'parent') {
            $mccLit = DB::table('ysepay_mcc')
                ->select('mch_type', 'mch_type_name')
                ->distinct()
                ->orderBy('mch_type')
                ->get();
        } else {
            $mccLit = DB::table('ysepay_mcc')
                ->select('mcc_id', 'mcc_name')
                ->where('mch_type', $mch_type)
                ->distinct()
                ->get();
        }

        return response()->json([
            'status' => 1,
            'data' => $mccLit
        ]);
    }


    private function jsonEncode($data)
    {
        return json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    //时间格式
    protected function YsepayTimeFormat($time)
    {
        if ($time) {
            if ($time == '长期') {
                $return_time = '29991231';
            } else {
                $return_time = str_replace('-', '', $time);
            }
        } else {
            $return_time = '';
        }

        return $return_time;
    }

    public function images_get($img_url)
    {
        $img_url = explode('/', $img_url);
        $img_url = end($img_url);
        $img = public_path() . '/upload/images/' . $img_url;
        return $img;
    }

}
