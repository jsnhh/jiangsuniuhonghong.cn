<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2021/01/22
 * Time: 17:49
 */
namespace App\Api\Controllers\YinSheng;

use Illuminate\Support\Facades\Log;
use App\Api\Controllers\BaseController as BBaseController;
use RuntimeException;

class BaseController extends BBaseController
{

//    public $ysBaseUrl = 'https://appdev.ysepay.com'; // 测试环境
//    public $privateCertPath = "/cert/client.pfx"; //测试环境  商户端的私钥证书 (RSA加密)
//    public $publicCertPath =  "/cert/service.cer"; //测试环境  银盛端公钥证书 (RSA加密)
//    public $privateCertPwd = "123456"; //测试环境 商户端私钥证书密码

    public $ysBaseUrl = 'https://ysgate.ysepay.com'; // 正式环境
    public $payUrl = 'https://qrcode.ysepay.com/gateway.do';//支付接口
    public $privateCertPath = "/cert/826451973720034.pfx"; //商户端的私钥证书 (RSA加密)
    public $publicCertPath =  "/cert/businessgate.cer"; //银盛端公钥证书 (RSA加密)

    public $privateCertPwd = "Pp016099"; //商户端私钥证书密码

    public $areaInfoUrl = '/openapi/pregate/trade/findCmmtAreaInfoList';//地区信息查询
    public $mccInfoUrl = '/openapi/aggregation/scan/mccQuery';//MCC码
    public $bankInfoUrl = '/openapi/pregate/trade/findBankNameAndBankCode';//行名行号查询
    public $bankBinUrl = '/openapi/trade/findBankBinByBankCardNo';//卡bin查询-根据银行卡号查询卡bin
    public $addCustInfoUrl = '/openapi/t1/smsc/addCustInfoApply';//资料上送
    public $modifyCustInfoUrl = '/openapi/smsc/modifyCustInfoApply';//资料修改
    public $auditCustInfoUrl = '/openapi/t1/smsc/auditCustInfoApply';//资料确认
    public $queryCustApplyUrl = '/openapi/smsc/queryCustApply';//商户状态查询
    public $changeMercBaseInfoUrl = '/openapi/smsc/changeMercBaseInfo';//商户基本信息变更申请
    public $changeBaseAuditUrl = '/openapi/smsc/changeBaseAudit';//基本信息变更审核

    public $uploadUrl = '/openapi/file/smsc/upload';//图片上传
    public $signUrl = '/openapi/t1/smsc/sign';//合同签约申请

//    public $certId = '826440345119153'; //测试环境
    public $certId = '826451973720034'; //正式环境

    public $agtMercId = '801451000002YG8'; //正式环境 代理商编号(子商户)：

    private $cacheMap = [];


    public function genCommonReqParam($method, $key, $reqId)
    {

        return [
            // 接口名称
            'method' => $method,
            // 发送请求的时间，格式"yyyy-MM-dd HH:mm:ss"
            'timeStamp' => date('Y-m-d H:i:s'),
            // 请求使用的编码格式，固定为utf-8
            'charset' => 'utf-8',
            // 请求唯一流水号,商户系统唯一，要求32个字符内（最少14个字符），只能是数字、大小写字母_-且在同一个商户号下唯一
            'reqId' => $reqId,
            //发起方商户号，服务商在银盛给自己开设的商户号，即可当作发起方商户号，由银盛生成并下发。 注意：不同于子商户号，服务商发展的商户即为子商户号
            'certId' => $this->certId,
            // 版本号，默认1.0，按接口文档来
            'version' => "1.0",
            // 银盛公钥加密随机生成的字符串（key）得到的加密值
//            'check' => $this->encryptByPub( $_SERVER['DOCUMENT_ROOT'] . $this->publicCertPath, $key)
            'check' => $this->encryptByPub( dirname(__FILE__) . $this->publicCertPath, $key)
        ];
    }

    /**
     * post发送请求
     *
     * @param string $url 请求地址
     * @param array $myParams 请求数据
     * @param string $public_key 验签公钥地址
     * @param string $response_name 返回数据key值
     * @return array 1-成功;2-失败
     */

    public function post($url, $post_data = '', $timeout = 20)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        if ($post_data != '') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        }
        // 禁用证书验证
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $file_contents = curl_exec($ch);
        curl_close($ch);
        return $file_contents;
    }

    public function doSign(array &$reqMap)
    {
        // 使用接入方私钥对排序的请求参数加签，并存放到请求参数里面.privateCertPath:私钥地址，privateCertPwd:私钥密钥
        $reqMap['sign'] = $this->signWithPfx($this->map2SortedSignStr($reqMap), dirname(__FILE__) . $this->privateCertPath, $this->privateCertPwd);
    }


    public function getCacheValue($key, $func)
    {
        if (array_key_exists($key, $this->cacheMap)) {
            return $this->cacheMap[$key];
        }
        if (is_callable($func)) {
            return $this->cacheMap[$key] = call_user_func($func);
        }
        return null;
    }

    /**
     * @param $certPath string
     * @return string
     */
    public function parseCertFileToPem($certPath)
    {
        return $this->getCacheValue($certPath, function () use ($certPath) {
            $certificateCAcerContent = file_get_contents($certPath);
            return '-----BEGIN CERTIFICATE-----' . PHP_EOL . chunk_split(base64_encode($certificateCAcerContent), 64, PHP_EOL) . '-----END CERTIFICATE-----' . PHP_EOL;
        });
    }

    /**
     * 公钥加密
     */
    public function encryptByPub($pubCertPath, $data)
    {
        //获取公钥
        $pu_key = openssl_pkey_get_public($this->parseCertFileToPem($pubCertPath));
        if (!$pu_key) {
            throw new RuntimeException("公钥不正确");
        }
        //公钥加密
        openssl_public_encrypt($data, $encrypted, $pu_key);
        openssl_free_key($pu_key);
        return base64_encode($encrypted);
    }

    public function verifySign($pubCertPath, $sign, $srcData)
    {
        //获取公钥
        $pu_key = openssl_pkey_get_public($this->parseCertFileToPem($pubCertPath));
        if (!$pu_key) {
            throw new RuntimeException("公钥不正确");
        }
        $success = openssl_verify($srcData, base64_decode($sign), $pu_key, OPENSSL_ALGO_SHA256);
        // Free the key from memory
        openssl_free_key($pu_key);
        if ($success === 1) {
            return true;
        }
        return false;
    }


    /**
     * @param $pfxPath string
     * @param $pfxPwd string
     * @return array
     */
    private function readPfxCertInfo($pfxPath, $pfxPwd)
    {
        return $this->getCacheValue($pfxPath, function () use ($pfxPwd, $pfxPath) {
            $cert_store = file_get_contents($pfxPath);
            $status = openssl_pkcs12_read($cert_store, $cert_info, $pfxPwd);
            if (!$status) {
                throw new RuntimeException('Invalid pfxPwd');
            }
            return $cert_info;
        });
    }

    /**
     * pfx私钥文件签名
     */
    public function signWithPfx($data, $pfxPath, $pfxPwd)
    {
        $cert_info = $this->readPfxCertInfo($pfxPath, $pfxPwd);
        $private_key = $cert_info['pkey'];
        $pri_key = openssl_get_privatekey($private_key);
        if (!$pri_key) {
            throw new RuntimeException('Invalid private key from Pfx file ' . $pfxPath);
        }
        $status = openssl_sign($data, $signature, $pri_key, OPENSSL_ALGO_SHA256);
        // Free the key from memory
        openssl_free_key($pri_key);
        if (!$status) {
            throw new RuntimeException('Computing of the signature failed');
        }
        return base64_encode($signature);
    }

    /**
     * map 转换成签名字符串，map的key做字典升序
     * @param $map array 签名参数
     * @return string
     */
    public function map2SortedSignStr($map)
    {
        $array_keys = array_keys($map);
        sort($array_keys);
        $result = '';
        foreach ($array_keys as $key) {
            if ($key == 'sign') {
                continue;
            }
            if (!empty($result)) {
                $result .= '&';
            }
            $result .= $key . '=' . $map[$key];
        }
        return $result;
    }

    /**
     * 随机字符串
     */
    public function randomStr($length = 16)
    {
        //字符组合
        $str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $len = strlen($str) - 1;
        $randStr = '';
        for ($i = 0; $i < $length; $i++) {
            $randStr .= $str[mt_rand(0, $len)];
        }
        return $randStr;
    }

    public function checkSign($resMap)
    {
        /** 对结果进行解密，并使用银盛公钥验签*/
        if (empty($resMap['sign'])) {
            throw new RuntimeException('验签失败,未返回加签信息,可能是银盛未配置发起方或者发起方证书类型配置有误,返回结果提示为:' . $resMap['msg']);
        } else {
            if (!$this->verifySign(dirname(__FILE__) . $this->publicCertPath, $resMap['sign'], $this->map2SortedSignStr($resMap))) {
                throw new RuntimeException("返回结果验签失败");
            }
        }
    }

    public function decryptBizData(&$resMap, $key)
    {
        /** 使用上面生成的加密密钥key，解密返回的业务参数*/
        if (!empty($resMap['businessData'])) {
            $resMap['businessData'] = json_decode($this->decrypt(base64_decode($resMap['businessData']), $key), true);
        }
    }

    public function encrypt($data, $key)
    {
        $encData = openssl_encrypt($data, 'aes-128-ecb', $key, OPENSSL_PKCS1_PADDING);
        return base64_encode($encData);
    }

    public function decrypt($data, $key)
    {
        return openssl_decrypt($data, 'aes-128-ecb', $key, OPENSSL_PKCS1_PADDING);
    }
    public function sign($reqMap)
    {
        // 使用接入方私钥对排序的请求参数加签，并存放到请求参数里面.privateCertPath:私钥地址，privateCertPwd:私钥密钥
        //$reqMap['sign'] = $this->signWithPfx($this->map2SortedSignStr($reqMap), dirname(__FILE__) . $this->privateCertPath, $this->privateCertPwd);
        ksort($reqMap);
        $signStr = "";
        foreach ($reqMap as $key => $val) {
            $signStr .= $key . '=' . $val . '&';
        }
        $signStr = rtrim($signStr, '&');
        $sign=$this->sign_encrypt($signStr, dirname(__FILE__) . $this->privateCertPath, $this->privateCertPwd);
        return $sign;
    }

    public function sign_encrypt($data, $pfxPath, $pfxPwd)
    {
        $cert_info = $this->readPfxCertInfo($pfxPath, $pfxPwd);
        $private_key = $cert_info['pkey'];
        $pri_key = openssl_get_privatekey($private_key);
        if (!$pri_key) {
            throw new RuntimeException('Invalid private key from Pfx file ' . $pfxPath);
        }
        $status = openssl_sign($data, $signature, $pri_key, OPENSSL_ALGO_SHA1);
        // Free the key from memory
        openssl_free_key($pri_key);
        if (!$status) {
            throw new RuntimeException('Computing of the signature failed');
        }
        return base64_encode($signature);
    }
    public function pay_post($url, $post_data = '', $timeout = 20){
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, FALSE);//不抓取头部信息。只返回数据
        curl_setopt($curl, CURLOPT_TIMEOUT,$timeout);//超时设置
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);//1表示不返回bool值
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));//重点
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post_data));

        $response = curl_exec($curl);

        if (curl_errno($curl)) {

            return curl_error($curl);

        }

        curl_close($curl);

        return $response;
    }
}
