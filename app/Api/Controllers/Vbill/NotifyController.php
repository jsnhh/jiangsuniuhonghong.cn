<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2019/11/12
 * Time: 11:38AM
 */
namespace App\Api\Controllers\Vbill;


use App\Api\Controllers\BaseController;
use App\Common\PaySuccessAction;
use App\Models\MerchantWalletDetail;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\RefundOrder;
use App\Models\Store;
use App\Models\StorePayWay;
use App\Models\UserRate;
use App\Models\UserWalletDetail;
use App\Models\VbillaStore;
use App\Models\VbillStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class NotifyController extends BaseController
{
    //随行付 支付回调
    public function pay_notify_url(Request $request)
    {
        try {
            $data = $request->all();
            // Log::info('ly-随行付-支付回调');
            // Log::info($data);

            if (isset($data['ordNo'])) {
                $out_trade_no = $data['ordNo'];

                $day = date('Ymd', time());
                $table = 'orders_' . $day;
                if (Schema::hasTable($table)) {
                    $order = DB::table($table)->where('out_trade_no', $out_trade_no)->first();
                } else {
                    $order = Order::where('out_trade_no', $out_trade_no)->first();
                }

                //订单存在
                if ($order) {
                    //系统订单未成功
                    if ($order->pay_status == 2) {
                        $trade_no = isset($data['transactionId']) ? $data['transactionId'] : "";
                        $pay_time = date('Y-m-d H:i:s', strtotime($data['payTime']));
                        $buyer_pay_amount = $data['amt']; //发起交易的订单金额
                        $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                        $buyer_id = isset($data['buyerId']) ? $data['buyerId'] : "";
                        $pointAmount = isset($data['pointAmount']) ? $data['pointAmount'] : 0; //代金券金额
                        $totalOffstAmt = isset($data['totalOffstAmt']) ? $data['totalOffstAmt'] : $buyer_pay_amount; //消费者付款金额
                        $settleAmt = isset($data['settleAmt']) ? $data['settleAmt'] : $order->total_amount - $pointAmount; //商家入账金额,说明：包含手续费、预充值、平台补贴（优惠），不含免充值代金券（商家补贴）
                        $recFeeAmt = isset($data['recFeeAmt']) ? $data['recFeeAmt'] : 0; //交易手续费

                        $fee_amount = $recFeeAmt ? $recFeeAmt : round(($order->rate * $settleAmt)/100, 2);

                        $in_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_id' => $buyer_id,
                            'buyer_pay_amount' => $totalOffstAmt,
                            'receipt_amount' => $settleAmt,
                            'fee_amount' => $fee_amount
                        ];
                        $this->update_day_order($in_data, $out_trade_no);

                        if (strpos($out_trade_no, 'scan')) {

                        } else {
                            //支付成功后的动作
                            $data = [
                                'ways_type' => $order->ways_type,
                                'company' => $order->company,
                                'ways_type_desc' => $order->ways_type_desc,
                                'source_type' => '13000',//返佣来源
                                'source_desc' => '随行付',//返佣来源说明
                                'total_amount' => $order->total_amount,
                                'out_trade_no' => $order->out_trade_no,
                                'other_no' => $order->other_no,
                                'rate' => $order->rate,
                                'fee_amount' => $fee_amount,
                                'merchant_id' => $order->merchant_id,
                                'store_id' => $order->store_id,
                                'user_id' => $order->user_id,
                                'config_id' => $order->config_id,
                                'store_name' => $order->store_name,
                                'ways_source' => $order->ways_source,
                                'pay_time' => $pay_time,
                                'device_id' => $order->device_id,
                            ];
                            PaySuccessAction::action($data);
                        }
                    }

                    //判断是否是有异步地址
                    if ($order->notify_url) {
                        try {
                            $notify_url = $order->notify_url;
                            $in_data = $data;
                            $key = '88888888';
                            $string = $this->getSignContent($in_data) . '&key=' . $key;
                            $in_data['sign'] = md5($string);
                            if (strpos($notify_url, '?') !== false) {
                                $notify_url = $notify_url . '&' . $this->getSignContent($in_data);
                                $return = $this->curlPost_java(json_encode($in_data), $notify_url);
                            } else {
                                $notify_url = $notify_url . '?' . $this->getSignContent($in_data);
                                $return = $this->curlPost_java(json_encode($in_data), $notify_url);
                            }
                            // return $return;
                        } catch (\Exception $exception) {

                        }
                    }
                }
            }

            return json_encode([
                'code' => 'success',
                'msg' => "成功",
            ]);

        } catch (\Exception $ex) {
            Log::info('随行付支付异步报错');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
        }
    }


    //随行付A 支付回调
    public function pay_notify_a_url(Request $request)
    {
        try {
            $data = $request->all();
            // Log::info('随行付A-支付回调');
            // Log::info($data);

            if (isset($data['ordNo'])) {
                $out_trade_no = $data['ordNo'];

                $day = date('Ymd', time());
                $table = 'orders_' . $day;
                if (Schema::hasTable($table)) {
                    $order = DB::table($table)->where('out_trade_no', $out_trade_no)->first();
                } else {
                    $order = Order::where('out_trade_no', $out_trade_no)->first();
                }

                //订单存在
                if ($order) {
                    //系统订单未成功
                    if ($order->pay_status == 2) {
                        $trade_no = isset($data['transactionId']) ? $data['transactionId'] : "";
                        $pay_time = date('Y-m-d H:i:s', strtotime($data['payTime']));
                        $buyer_pay_amount = $data['amt']; //发起交易的订单金额
                        $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                        $buyer_id = isset($data['buyerId']) ? $data['buyerId'] : "";
                        $pointAmount = isset($data['pointAmount']) ? $data['pointAmount'] : 0; //代金券金额
                        $totalOffstAmt = isset($data['totalOffstAmt']) ? $data['totalOffstAmt'] : $buyer_pay_amount; //消费者付款金额
                        $settleAmt = isset($data['settleAmt']) ? $data['settleAmt'] : $order->total_amount - $pointAmount; //商家入账金额,说明：包含手续费、预充值、平台补贴（优惠），不含免充值代金券（商家补贴）
                        $recFeeAmt = isset($data['recFeeAmt']) ? $data['recFeeAmt'] : 0; //交易手续费

                        $fee_amount = $recFeeAmt ? $recFeeAmt : round(($order->rate * $settleAmt)/100, 2);

                        $in_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_id' => $buyer_id,
                            'buyer_pay_amount' => $totalOffstAmt,
                            'receipt_amount' => $settleAmt,
                            'fee_amount' => $fee_amount
                        ];
                        $this->update_day_order($in_data, $out_trade_no);

                        if (strpos($out_trade_no, 'scan')) {

                        } else {
                            //支付成功后的动作
                            $data = [
                                'ways_type' => $order->ways_type,
                                'company' => $order->company,
                                'ways_type_desc' => $order->ways_type_desc,
                                'source_type' => '19000',//返佣来源
                                'source_desc' => '随行付A',//返佣来源说明
                                'total_amount' => $order->total_amount,
                                'out_trade_no' => $order->out_trade_no,
                                'other_no' => $order->other_no,
                                'rate' => $order->rate,
                                'fee_amount' => $fee_amount,
                                'merchant_id' => $order->merchant_id,
                                'store_id' => $order->store_id,
                                'user_id' => $order->user_id,
                                'config_id' => $order->config_id,
                                'store_name' => $order->store_name,
                                'ways_source' => $order->ways_source,
                                'pay_time' => $pay_time,
                                'device_id' => $order->device_id,
                            ];
                            PaySuccessAction::action($data);
                        }
                    } else {
                        //系统订单已经成功了
                    }

                    //判断是否是有异步地址
                    if ($order->notify_url) {
                        try {
                            $notify_url = $order->notify_url;
                            $in_data = $data;
                            $key = '88888888';
                            $string = $this->getSignContent($in_data) . '&key=' . $key;
                            $in_data['sign'] = md5($string);
                            if (strpos($notify_url, '?') !== false) {
                                $notify_url = $notify_url . '&' . $this->getSignContent($in_data);
                                $return = $this->curlPost_java(json_encode($in_data), $notify_url);
                            } else {
                                $notify_url = $notify_url . '?' . $this->getSignContent($in_data);
                                $return = $this->curlPost_java(json_encode($in_data), $notify_url);
                            }
                            // return $return;
                        } catch (\Exception $exception) {

                        }
                    }
                }
            }

            return json_encode([
                'code' => 'success',
                'msg' => "成功",
            ]);
        } catch (\Exception $ex) {
            Log::info('随行付A支付回调异步报错');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
        }
    }


    //随行付 退款回调
    public function refund_notify_url(Request $request)
    {
        try {
            $data = $request->all();
//            Log::info('随行付退款回调原始数据');
//            Log::info($data);

            if (isset($data['ordNo'])) {
                $out_trade_no = $data['ordNo'];
                $a = array('a', '_', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
                $b = str_ireplace($a, "", $out_trade_no);
                $day = substr($b, 0, 8);
                $table = 'orders_' . $day;

                $out_trade_no = substr($out_trade_no, 0, strlen($out_trade_no)-4);

                if (Schema::hasTable($table)) {
                    $order = DB::table($table)->where('out_trade_no', $out_trade_no)->first();
                } else {
                    $order = Order::where('out_trade_no', $out_trade_no)->first();
                }

                //订单存在
                if ($order && $order->pay_status == '1') {
                    if ($data['bizCode'] == "0000") {
                        $refund_amount = $data["totalOffstAmt"];
                        $realRefundAmount = isset($data["realRefundAmount"])? $data["realRefundAmount"]: 0; //商家出款金额
                        $update_data = [
                            'status' => 6,
                            'pay_status' => 6,
                            'pay_status_desc' => '已退款',
                            'refund_amount' => $order->refund_amount + $refund_amount,
                            'fee_amount' => 0,
                        ];
                        if (Schema::hasTable($table)) {
                            DB::table($table)
                                ->where('out_trade_no', $out_trade_no)
                                ->update($update_data);
                        } else {
                           Order::where('out_trade_no', $out_trade_no)
                                ->update($update_data);
                        }
                        if (Schema::hasTable('order_items')) {
                            OrderItem::where('out_trade_no', $out_trade_no)->update($update_data);
                        }

                        RefundOrder::create([
                            'ways_source' => $order->ways_source,
                            'type' => $order->ways_type,
                            'refund_amount' => $refund_amount, //退款金额
                            'refund_no' => $data['ordNo'], //退款单号
                            'store_id' => $order->store_id,
                            'merchant_id' => $order->merchant_id,
                            'out_trade_no' => $order->out_trade_no,
                            'trade_no' => $order->other_no
                        ]);

                        //返佣去掉
                        UserWalletDetail::where('out_trade_no', $order->out_trade_no)->update([
                            'settlement' => '03',
                            'settlement_desc' => '退款订单',
                        ]);
                        MerchantWalletDetail::where('out_trade_no', $order->out_trade_no)->update([
                            'settlement' => '03',
                            'settlement_desc' => '退款订单',
                        ]);
                    }
                }
            }

            return json_encode([
                'code' => 'success',
                'msg' => "成功",
            ]);
        } catch (\Exception $ex) {
            Log::info('随行付-退款回调异步报错');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
        }
    }


    //随行付 微信提交实名认证 回调
    public function weChatApplyNotify(Request $request)
    {
        try {
            $data = $request->all();
            Log::info('随行付-微信提交实名认证-回调原始数据');
            Log::info($data);

            if ( isset($data['mno']) ) {
                $mno = $data['mno']; //商编
                if ( isset($data['wxApplyNo']) ) {
                    $wxApplyNo = $data['wxApplyNo']; //微信申请单编号
                    $idenStatus = isset($data['idenStatus']) ? $data['idenStatus'] : ''; //认证状态
                    $infoQrcode = isset($data['infoQrcode']) ? $data['infoQrcode'] : ''; //确认二维码链接地址
                    $rejectCode = isset($data['rejectCode']) ? $data['rejectCode'] : ''; //驳回参数
                    $rejectInfo = isset($data['rejectInfo']) ? $data['rejectInfo'] : ''; //驳回原因

                    $vbill_store_obj = VbillStore::where('mno', $mno)
                        ->where('wxApplyNo', $wxApplyNo)
                        ->orderBy('updated_at', 'desc')
                        ->first();
                    if ($vbill_store_obj) {
                        if ( ($vbill_store_obj->idenStatus != 1) && $idenStatus ) {
                            switch ($idenStatus)
                            {
                                case 'APPLYMENT_STATE_WAITTING_FOR_AUDIT':   $iden_status = 4;
                                    break;
                                case 'APPLYMENT_STATE_WAITTING_FOR_CONFIRM_CONTACT':   $iden_status = 2;
                                    break;
                                case 'APPLYMENT_STATE_WAITTING_FOR_CONFIRM_LEGALPERSON':   $iden_status = 3;
                                    break;
                                case 'APPLYMENT_STATE_PASSED':   $iden_status = 1;
                                    break;
                                case 'APPLYMENT_STATE_REJECTED':   $iden_status = 5;
                                    break;
                                case 'APPLYMENT_STATE_FREEZED':   $iden_status = 6;
                                    break;
                                case 'APPLYMENT_STATE_CANCELED':   $iden_status = 7;
                                    break;
                                case 'APPLYMENT_STATE_OPENACCOUNT':   $iden_status = 8;
                                    break;
                                default:
                                    $iden_status = 9;
                            }

                            $update_data = [
                                'idenStatus' => $iden_status, //0-未认证;1-审核通过;2-待确认联系信息;3-待账户验证;4-审核中5-审核驳回;6-已冻结;7-已作废;8-重复认证;9-其他
                            ];
                            if($infoQrcode) $update_data['infoQrcode'] = $infoQrcode;
                            if($rejectCode) $update_data['rejectCode'] = $rejectCode;
                            if($rejectInfo) $update_data['rejectInfo'] = $rejectInfo;
                            $update_res = $vbill_store_obj->update($update_data);
                            if (!$update_res) {
                                Log::info('随行付-微信认证回调-更新认证状态失败');
                                Log::info($vbill_store_obj->id);
                            }
                        }
                    } else {
                        Log::info('随行付-微信认证回调-未找到');
                        Log::info('商户号：'.$mno.'；微信申请单号：'.$wxApplyNo);
                    }

                    return json_encode([
                        'code' => 'success',
                        'msg' => '成功'
                    ]);
                }
            }
        } catch (\Exception $ex) {
            Log::info('随行付-微信提交实名认证-回调error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
        }
    }


    //随行付a 微信提交实名认证 回调
    public function weChatApplyNotifyA(Request $request)
    {
        try {
            $data = $request->all();
            Log::info('随行付a-微信提交实名认证-回调原始数据');
            Log::info($data);

            if ( isset($data['mno']) ) {
                $mno = $data['mno']; //商编
                if ( isset($data['wxApplyNo']) ) {
                    $wxApplyNo = $data['wxApplyNo']; //微信申请单编号
                    $idenStatus = isset($data['idenStatus']) ? $data['idenStatus'] : ''; //认证状态
                    $infoQrcode = isset($data['infoQrcode']) ? $data['infoQrcode'] : ''; //确认二维码链接地址
                    $rejectCode = isset($data['rejectCode']) ? $data['rejectCode'] : ''; //驳回参数
                    $rejectInfo = isset($data['rejectInfo']) ? $data['rejectInfo'] : ''; //驳回原因

                    $vbill_store_obj = VbillaStore::where('mno', $mno)
                        ->where('wxApplyNo', $wxApplyNo)
                        ->orderBy('updated_at', 'desc')
                        ->first();
                    if ($vbill_store_obj) {
                        if ( ($vbill_store_obj->idenStatus != 1) && $idenStatus ) {
                            switch ($idenStatus)
                            {
                                case 'APPLYMENT_STATE_WAITTING_FOR_AUDIT':   $iden_status = 4;
                                    break;
                                case 'APPLYMENT_STATE_WAITTING_FOR_CONFIRM_CONTACT':   $iden_status = 2;
                                    break;
                                case 'APPLYMENT_STATE_WAITTING_FOR_CONFIRM_LEGALPERSON':   $iden_status = 3;
                                    break;
                                case 'APPLYMENT_STATE_PASSED':   $iden_status = 1;
                                    break;
                                case 'APPLYMENT_STATE_REJECTED':   $iden_status = 5;
                                    break;
                                case 'APPLYMENT_STATE_FREEZED':   $iden_status = 6;
                                    break;
                                case 'APPLYMENT_STATE_CANCELED':   $iden_status = 7;
                                    break;
                                case 'APPLYMENT_STATE_OPENACCOUNT':   $iden_status = 8;
                                    break;
                                default:
                                    $iden_status = 9;
                            }

                            $update_data = [
                                'idenStatus' => $iden_status, //0-未认证;1-审核通过;2-待确认联系信息;3-待账户验证;4-审核中5-审核驳回;6-已冻结;7-已作废;8-重复认证;9-其他
                            ];
                            if($infoQrcode) $update_data['infoQrcode'] = $infoQrcode;
                            if($rejectCode) $update_data['rejectCode'] = $rejectCode;
                            if($rejectInfo) $update_data['rejectInfo'] = $rejectInfo;
                            $update_res = $vbill_store_obj->update($update_data);
                            if (!$update_res) {
                                Log::info('随行付a-微信认证回调-更新认证状态失败');
                                Log::info($vbill_store_obj->id);
                            }
                        }
                    } else {
                        Log::info('随行付a-微信认证回调-未找到');
                        Log::info('商户号：'.$mno.'；微信申请单号：'.$wxApplyNo);
                    }

                    return json_encode([
                        'code' => 'success',
                        'msg' => '成功'
                    ]);
                }
            }
        } catch (\Exception $ex) {
            Log::info('随行付a-微信提交实名认证-回调error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
        }
    }


    /**
     * curl post java对接  传输数据流
     * */
    public function curlPost_java($data, $Url)
    {
        $ch = curl_init($Url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);//$data JSON类型字符串
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data)));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }


    //参数拼接
    public function getSignContent($params)
    {
        ksort($params);

        $stringToBeSigned = "";
        $i = 0;
        foreach ($params as $k => $v) {
            if (false === $this->checkEmpty($v) && "@" != substr($v, 0, 1)) {

                // 转换成目标字符集
                $v = $this->characet($v, $this->postCharset);

                if ($i == 0) {
                    $stringToBeSigned .= "$k" . "=" . "$v";
                } else {
                    $stringToBeSigned .= "&" . "$k" . "=" . "$v";
                }
                $i++;
            }
        }

        unset ($k, $v);
        return $stringToBeSigned;
    }


    /**
     * 校验$value是否非空
     *  if not set ,return true;
     *    if is null , return true;
     **/
    protected function checkEmpty($value)
    {
        if (!isset($value))
            return true;
        if ($value === null)
            return true;
        if (trim($value) === "")
            return true;

        return false;
    }


    /**
     * 转换字符集编码
     * @param $data
     * @param $targetCharset
     * @return string
     */
    function characet($data, $targetCharset)
    {
        if (!empty($data)) {
            $fileType = $this->fileCharset;
            if (strcasecmp($fileType, $targetCharset) != 0) {

                $data = mb_convert_encoding($data, $targetCharset);
                //              $data = iconv($fileType, $targetCharset.'//IGNORE', $data);
            }
        }

        return $data;
    }


}