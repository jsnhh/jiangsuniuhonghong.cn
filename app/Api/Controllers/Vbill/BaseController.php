<?php
namespace App\Api\Controllers\Vbill;


use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class BaseController
{
    public $scan_url = "https://openapi.tianquetech.com/order/reverseScan"; //被扫接口（B扫C）
    public $order_query_url = "https://openapi.tianquetech.com/query/tradeQuery";
    public $refund_url = 'https://openapi.tianquetech.com/order/refund';
    public $refund_query_url = 'https://openapi.tianquetech.com/query/refundQuery';
    public $qr_pay_url = 'https://openapi.tianquetech.com/order/jsapiScan'; //聚合支付

    public $upload_url = "https://openapi.suixingpay.com/merchant/uploadPicture"; //图片上传
    public $open_store_url = 'https://openapi.suixingpay.com/merchant/income'; //进件接口
    public $open_update_store_url = 'https://openapi.suixingpay.com/merchant/updateMerchantInfo'; //进件失败修改接口
    public $open_query_store_url = 'https://openapi.suixingpay.com/merchant/queryMerchantInfo'; //入驻查询
    public $weixin_config = "https://openapi.suixingpay.com/merchant/weChatPaySet/addConf"; //微信子商户号配置
    public $store_query_info="https://openapi.suixingpay.com/merchant/merchantInfoQuery"; //商户信息查询接口
    public $update_rate="https://openapi.suixingpay.com/merchant/merchantSetup"; //修改费率信息接口
    public $wechat_commit_apply = "https://openapi.suixingpay.com/merchant/realName/commitApply"; //提交实名认证申请接口
    public $wechat_lose_face = "https://openapi.tianquetech.com/query/wechatLoseFace"; //获取微信刷脸调用凭证
    public $getUnionInfo = "https://openapi.tianquetech.com/query/getUnionInfo"; //银联获取信息
    public $weChatPaySetAddConf = "https://openapi.tianquetech.com/merchant/weChatPaySet/addConf"; //配置微信子商户的授权目录、支付 APPID、默认关注 APPID

    //实名认证
    public $realNameCommitApply = "https://openapi.tianquetech.com/merchant/realName/commitApply"; //提交实名认证申请
    public $realNameQueryApplyInfo = "https://openapi.tianquetech.com/merchant/realName/queryApplyInfo"; //认证申请结果查询
    public $realNameBackApplyBill = "https://openapi.tianquetech.com/merchant/realName/backApplyBill"; //认证申请撤销
    public $realNameQueryGrantStatus = "https://openapi.tianquetech.com/merchant/realName/queryGrantStatus"; //微信子商户授权状态查询

    public $wechatOrderUrl = "https://openapi.tianquetech.com/query/weChatOrder"; // 微信扫码点餐-上送餐品信息接口


    public function execute($data, $url, $privateKey, $sxfpublic)
    {
        $mygoods = $this->argSort($data);
        $mystr = $this->createLinkstring($mygoods);
        $sxfpublic = "-----BEGIN PUBLIC KEY-----\n" .
            wordwrap($sxfpublic, 64, "\n", true) .
            "\n-----END PUBLIC KEY-----";

        $privateKey = "-----BEGIN RSA PRIVATE KEY-----\n" .
            wordwrap($privateKey, 64, "\n", true) .
            "\n-----END RSA PRIVATE KEY-----";

        $sign = $this->rsaSign($mystr, $privateKey);
        $mygoods['sign'] = $sign;
        $datajsonstr = json_encode($mygoods, JSON_UNESCAPED_SLASHES);
        $curl = curl_init();
        $this_header = array("content-type: application/x-www-form-urlencoded; charset=UTF-8");
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this_header);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $datajsonstr);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($datajsonstr)
        ));
        //  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, true);
        $tmpInfo = curl_exec($curl);     //返回api的json对象
        $info = json_decode($tmpInfo, JSON_UNESCAPED_UNICODE);
//        Log::info('随行付执行结果返回');
//        Log::info($info);

        return [
            'status' => 1,
            'data' => $info
        ];

        $newsign = $info['sign'];
        unset($info['sign']);
        ksort($info);
        $mystrs2 = $this->createLinkstring($info);

        if ($this->verify($mystrs2, $newsign, $sxfpublic)) {
            return [
                'status' => 1,
                'data' => $info
            ];

        } else {

            return [
                'status' => 0,
                'message' => '平台请求返回异常',
                'data' => $info
            ];
        }



    }

    /**
     * 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
     * @param array $para 需要拼接的数组
     * @return string
     */
    function createLinkstring($para)
    {
        $params = array();
        foreach ($para as $key => $value) {
            if (is_array($value)) {
                $value = stripslashes(json_encode($value, JSON_UNESCAPED_UNICODE));
            }
            $params[] = $key . '=' . $value;
        }
        $data = implode("&", $params);

        return $data;
    }

    /**
     * * 对数组排序
     * @param array $para 排序前的数组
     * @return mixed
     */
    function argSort($para)
    {
        ksort($para);
        // reset($para);
        return $para;
    }

    /**
     *  * RSA签名
     * @param string $data 待签名数据
     * @param string $private_key_path 商户私钥文件路径
     * @return string
     */
    function rsaSign($data, $private_key_path)
    {
        //$priKey = file_get_contents($private_key_path);
        //$priKey=chunk_split($priKey, 64, "\n");
        $res = openssl_get_privatekey($private_key_path);
        openssl_sign($data, $sign, $res);
        openssl_free_key($res);
        //base64编码
        $sign = base64_encode($sign);
        return $sign;
    }


    /**RSA验签
     * $data待签名数据
     * $sign需要验签的签名
     * 验签用支付宝公钥
     * return 验签是否通过 bool值
     */
    function verify($data, $sign, $pubKey)
    {
        // $pubKey = file_get_contents('key/alipay_public_key.pem');
        //转换为openssl格式密钥
        $res = openssl_get_publickey($pubKey);
        //调用openssl内置方法验签，返回bool值
        $result = (bool)openssl_verify($data, base64_decode($sign), $res);
        //var_dump($result);
        //释放资源
        openssl_free_key($res);
        //返回资源是否成功
        return $result;
    }

    /**
     * 成普通日期格式转换rfc3339标准格式
     * rfc3339标准格式为YYYY-MM-DDTHH:mm:ss+TIMEZONE，YYYY-MM-DD表示年月日，
     * T出现在字符串中，表示time元素的开头，HH:mm:ss表示时分秒，TIMEZONE表示时区（+08:00表示东八区时间，领先UTC 8小时，即北京时间）
     */
    public function dateTransformationTimezone($date){
        date_default_timezone_set("UTC");
        $gmDate = strtotime($date);
        $gmDate = gmdate("Y-m-dTH:i:s+08:00",$gmDate);
        $gmDate = str_replace("GM","",$gmDate);
        return $gmDate;
    }

    /**
     * 统一接口返回格式
     */
    public function sys_response($code = 0, $msg = '', $data = '')
    {
        // 如果$msg为空自动获取
        if (empty($msg)) {
            $key = 'CODE_' . $code;
            $reflectionClass = new \ReflectionClass('\App\Common\Enum\BaseStatusCodeEnum');
            if ($reflectionClass->hasConstant($key)) {
                $msg = $reflectionClass->getConstant($key);
            }
        }

        $returnData = [
            'code' => $code,
            'msg' => $msg,
            'data' => $data,
        ];
        if ($data === false) {
            unset($returnData['data']);
        }
        return $returnData;
    }

    /**
     * 校验必填字段
     */
    public function check_required($check, $data)
    {
        $rules = [];
        $attributes = [];
        foreach ($data as $k => $v) {
            $rules[$k] = 'required';
            $attributes[$k] = $v;
        }
        $messages = [
            'required' => ':attribute不能为空',
        ];
        $validator = Validator::make($check, $rules,
            $messages, $attributes);
        $message = $validator->getMessageBag();
        return $message->first();
    }

}