<?php
namespace App\Api\Controllers\Vbill;

use App\Api\Controllers\Config\VbillConfigController;
use App\Models\VbillConfig;
use App\Models\WechatCashCouponConfig;
use App\Models\CustomerAppletsUserOrders;
use App\Models\CustomerAppletsUserOrderGoods;
use function EasyWeChat\Kernel\Support\get_client_ip;
use function EasyWeChat\Kernel\Support\get_server_ip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PayController extends BaseController
{
    //扫一扫 0-系统错误 1-成功 2-正在支付 3-失败
    public function scan_pay($data)
    {
        try {
            $out_trade_no = $data['out_trade_no'];
            $code = $data['code'];
            $total_amount = $data['total_amount'];
            $remark = $data['remark'];
            $device_id = $data['device_id'];
            $shop_name = $data['shop_name'];
            $notify_url = $data['notify_url'];
            $url = $data['request_url'];
            $payType = $data['pay_type'];
            $mno = $data['mno'];
            $orgId = $data['orgId'];
            $privateKey = $data['privateKey'];
            $sxfpublic = $data['sxfpublic'];
            $total_amount = number_format($total_amount, 2, '.', '');

            $obj = new BaseController();
            $data_re = [
                'orgId' => $orgId,
                'reqId' => time(),
                'version' => '1.0',
                'timestamp' => date('Ymdhis', time()),
                'signType' => 'RSA',
                'reqData' => [
                    'mno' => $mno,//商户编号
                    'ordNo' => $out_trade_no,//商户订单号
                    'scene' => '1',
                    'authCode' => $code,
                    'amt' => $total_amount,//订单总金额元
                    'payType' => $payType,//
                    'subject' => $shop_name,//订单标题
                    'tradeSource' => '01',
                    'trmIp' => get_client_ip(),
                    'notifyUrl' => $notify_url
                ],
            ];

            if ($payType=="WECHAT"&&isset($data['subAppid'])) {
                $data_re['reqData']['subAppid'] = $data['subAppid'];
            }

            Log::info('随行付---scan_pay--静态码提交-params：');
            Log::info($data_re);

            $re = $obj->execute($data_re, $url, $privateKey, $sxfpublic);

            Log::info('随行付---scan_pay--静态码提交-res：');
            Log::info($re);

            //系统错误
            if ($re['status'] == 0) {
                return $re;
            }

            //业务成功
            if ($re['data']['code'] == "0000") {
                //交易成功
                if ($re['data']['respData']['bizCode'] == "0000") {
                    return [
                        'status' => 1,
                        'message' => '交易成功',
                        'data' => $re['data'],
                    ];
                } //用户输入密码
                elseif ($re['data']['respData']['bizCode'] == "2002") {
                    return [
                        'status' => 2,
                        'message' => '请用户输入密码',
                        'data' => $re['data'],
                    ];
                } else {
                    return [
                        'status' => 3,
                        'message' => $re['data']['respData']['bizMsg'],
                        'data' => $re['data'],
                    ];
                }
            } else {
                return [
                    'status' => 0,
                    'message' => $re['data']['msg']
                ];
            }

        } catch (\Exception $ex) {
            return [
                'status' => '0',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine(),
            ];
        }
    }


    //查询订单 0-系统错误 1-成功 2-正在支付 3-失败 4.已经退款
    public function order_query($data)
    {
        try {
            $out_trade_no = $data['out_trade_no'];
            $url = $data['request_url'];
            $mno = $data['mno'];
            $orgId = $data['orgId'];
            $privateKey = $data['privateKey'];
            $sxfpublic = $data['sxfpublic'];

            $obj = new BaseController();
            $data = [
                'orgId' => $orgId,
                'reqId' => time(),
                'version' => '1.0',
                'timestamp' => date('Ymdhis', time()),
                'signType' => 'RSA',
                'reqData' => [
                    'mno' => $mno,//商户编号
                    'ordNo' => $out_trade_no,//商户订单号
                ],
            ];
            $re = $obj->execute($data, $url, $privateKey, $sxfpublic);

            //系统错误
            if ($re['status'] == 0) {
                return $re;
            }
            //业务成功
            if ($re['data']['code'] == "0000") {
                //交易成功
                if ($re['data']['respData']['tranSts'] == "SUCCESS") {
                    return [
                        'status' => 1,
                        'message' => '交易成功',
                        'data' => $re['data'],
                    ];
                } //用户输入密码
                elseif ($re['data']['respData']['tranSts'] == "PAYING") {
                    return [
                        'status' => 2,
                        'message' => '请用户输入密码',
                        'data' => $re['data'],
                    ];
                    //用户取消
                } elseif ($re['data']['respData']['tranSts'] == "NOTPAY") {
                    return [
                        'status' => 3,
                        'message' => $re['data']['respData']['bizMsg'],
                        'data' => $re['data'],
                    ];
                } else {
                    return [
                        'status' => 3,
                        'message' => $re['data']['respData']['bizMsg'],
                        'data' => $re['data'],
                    ];
                }


            } else {
                return [
                    'status' => 0,
                    'message' => $re['data']['msg']
                ];
            }

        } catch (\Exception $ex) {
            return [
                'status' => '0',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine(),
            ];
        }
    }


    //退款 0-系统错误 1-成功
    public function refund($data)
    {
        try {
            $out_trade_no = $data['out_trade_no'];
            $url = $data['request_url'];
            $mno = $data['mno'];
            $orgId = $data['orgId'];
            $privateKey = $data['privateKey'];
            $sxfpublic = $data['sxfpublic'];
            $refund_amount = $data['refund_amount'];
            $refund_notify_url = $data['notify_url'];
            $ordNo = $out_trade_no . rand(1000, 9999);
//            $ordNo = $out_trade_no . '123';

            $obj = new BaseController();
            $data = [
                'orgId' => $orgId,
                'reqId' => time(),
                'version' => '1.0',
                'timestamp' => date('Ymdhis', time()),
                'signType' => 'RSA',
                'reqData' => [
                    'ordNo' => $ordNo,
                    'mno' => $mno, //商户编号
                    'origOrderNo' => $out_trade_no, //商户订单号
                    'amt' => $refund_amount,
                    'notifyUrl' => $refund_notify_url
                ],
            ];
            Log::info('随行付-退款');
            Log::info($data);
            $re = $obj->execute($data, $url, $privateKey, $sxfpublic);
            Log::info($re);

            //系统错误
            if ($re['status'] == 0) {
                return $re;
            }
            //业务成功
            if ($re['data']['code'] == "0000") {
                //退款成功
                if ($re['data']['respData']['bizCode'] == "0000") {
                    return [
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $re['data'],
                    ];
                } else {
			        sleep(3); //5秒后在发起查询
                    //退款失败，查询退款结果
                    $url = $this->refund_query_url;
                    $data = [
                        'orgId' => $orgId,
                        'reqId' => time(),
                        'version' => '1.0',
                        'timestamp' => date('Ymdhis', time()),
                        'signType' => 'RSA',
                        'reqData' => [
                            'ordNo' => $ordNo,
                            'mno' => $mno, //商户编号
                        ],
                    ];
                    $re = $obj->execute($data, $url, $privateKey, $sxfpublic);

                    if ($re['data']['code'] == "0000") {
                        if ($re['data']['respData']['bizCode'] == "0000") {
                            if ($re['data']['respData']['tranSts']=="REFUNDSUC") {
                                return [
                                    'status' => 1,
                                    'message' => '退款成功',
                                    'data' => $re['data'],
                                ];
                            }

                            if ($re['data']['respData']['tranSts'] == "REFUNDING") {
                                return [
                                    'status' => 1,
                                    'message' => '退款中',
                                    'data' => $re['data'],
                                ];
                            }
                        }


                        if ($re['data']['respData']['bizCode'] == "2002") {
                            return [
                                'status' => 1,
                                'message' => '退款成功',
                                'data' => $re['data'],
                            ];
                        }

                    }

                    return [
                        'status' => 0,
                        'message' => $re['data']['respData']['bizMsg'],
                        'data' => $re['data'],
                    ];
                }
            } else {
                return [
                    'status' => 0,
                    'message' => $re['data']['msg']
                ];
            }

        } catch (\Exception $ex) {
            return [
                'status' => 0,
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ];
        }
    }


    //退款查询 0-系统错误 1-成功 2-正在退款 3-失败
    public function refund_query($data)
    {
        try {
            $out_trade_no = $data['out_trade_no']; //系统订单号
            $url = $data['request_url']; //退款查询结果地址
            $mno = $data['mno']; //商户编号
            $orgId = $data['orgId']; //随行付机构号
            $privateKey = $data['privateKey']; //平台私钥
            $sxfpublic = $data['sxfpublic']; //随行付公钥

            $obj = new BaseController();
            $url = $this->refund_query_url;
            $data = [
                'orgId' => $orgId,
                'reqId' => time(),
                'version' => '1.0',
                'timestamp' => date('Ymdhis', time()),
                'signType' => 'RSA',
                'reqData' => [
                    'ordNo' => $out_trade_no, //商户退款订单号
//                    'ordNo' => $out_trade_no . '123', //商户退款订单号
                    'mno' => $mno, //商户入驻返回的商户编号
                ],
            ];
//            Log::info('随行付-退款查询');
//            Log::info($data);
            $re = $obj->execute($data, $url, $privateKey, $sxfpublic);
//            Log::info('随行付退款查询请求原始结果');
//            Log::info($re);

            //系统错误
            if ($re['status'] == 0) {
                return $re;
            }
            //业务成功
            if ($re['data']['code'] == "0000") {
                //退款成功
                if ($re['data']['respData']['tranSts'] == "REFUNDSUC") {
                    return [
                        'status' => 1,
                        'message' => '退款成功',
                        'data' => $re['data'],
                    ];
                } elseif ($re['data']['respData']['tranSts'] == 'REFUNDING') {
                    return [
                        'status' => 2,
                        'message' => '正在退款中',
                        'data' => $re['data'],
                    ];
                } elseif ($re['data']['respData']['tranSts'] == 'REFUNDFAIL') {
                    return [
                        'status' => 2,
                        'message' => '退款失败',
                        'data' => $re['data'],
                    ];
                } else {
                    return [
                        'status' => 0,
                        'message' => $re['data']['respData']['bizMsg'],
                        'data' => $re['data'],
                    ];
                }

            } else {
                return [
                    'status' => 0,
                    'message' => $re['data']['msg']
                ];
            }


        } catch (\Exception $ex) {
            return [
                'status' => '0',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ];
        }
    }


    //生成动态二维码-公共
    public function send_qr($data)
    {
        try {
            $out_trade_no = $data['out_trade_no'];
            $total_amount = $data['total_amount'];
            $remark = $data['remark'];
            $notify_url = $data['notify_url'];
            $url = $data['request_url'];
            $merchant_no = $data['merchant_no'];
            $returnParams = $data['return_params'];//原样返回
            $des_key = $data['des_key'];
            $md_key = $data['md_key'];
            $systemId = $data['systemId'];
            //请求数据
            $data = [
                'merchantNo' => $merchant_no,
                'businessCode' => 'AGGRE',
                'version' => '3.0',
                'outTradeNo' => $out_trade_no,
                'amount' => $total_amount * 100,
                'remark' => $remark,//
                'returnParams' => $returnParams,
                'businessType' => '00',//返回二维码
                'successNotifyUrl' => $notify_url,
            ];

            $obj = new BaseController();
            $obj->des_key = $des_key;
            $obj->md_key = $md_key;
            $obj->systemId = $systemId;
//            Log::info('随行付-生成动态二维码');
//            Log::info($data);
            $re = $obj->execute($data, $url);
//            Log::info($re);

            //系统错误
            if ($re['status'] == 0) {
                return $re;
            }

            //业务成功
            if ($re['data']['resultCode'] == "SUCCESS") {
                return [
                    'status' => 1,
                    'message' => '返回成功',
                    'code_url' => $re['data']['scanUrl'],
                    'data' => $re['data']
                ];
            } else {
                return [
                    'status' => 0,
                    'message' => $re['data']['errCodeDes']
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => '0',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ];
        }
    }


    //静态码提交-公共
    public function qr_submit($data)
    {
        try {
            $out_trade_no = $data['out_trade_no'];
            $total_amount = $data['total_amount'];
            $remark = $data['remark'];
            $device_id = $data['device_id'];
            $shop_name = $data['shop_name'];
            $notify_url = $data['notify_url'];
            $url = $data['request_url'];
            $payType = $data['pay_type'];
            $payWay = $data['pay_way'];
            $mno = $data['mno'];
            $orgId = $data['orgId'];
            $privateKey = $data['privateKey'];
            $sxfpublic = $data['sxfpublic'];
            $open_id = $data['open_id'];

            $reqData = [
                'mno' => $mno,//商户编号
                'ordNo' => $out_trade_no,//商户订单号
                'amt' => $total_amount,//订单总金额元
                'payType' => $payType,//
                'payWay' => $payWay,
                'subject' => $shop_name,//订单标题
                'tradeSource' => '01',
                'trmIp' => get_client_ip(),
                'notifyUrl' => $notify_url,
                'userId'=>$open_id
            ];

            if (isset($data['subAppid'])) {
                $reqData['subAppid'] = $data['subAppid'];
            }

            if ($payType == "UNIONPAY") {
                $reqData['customerIp'] = get_client_ip();
            }

            $obj = new BaseController();
            $data = [
                'orgId' => $orgId,
                'reqId' => time(),
                'version' => '1.0',
                'timestamp' => date('Ymdhis', time()),
                'signType' => 'RSA',
                'reqData' => $reqData,
            ];

            Log::info('随行付-静态码提交-params：');
            Log::info($data);
            $re = $obj->execute($data, $url, $privateKey, $sxfpublic);
            Log::info('随行付-静态码提交-res：');
            Log::info($re);

            //系统错误
            if ($re['status'] == 0) {
                return $re;
            }

            //业务成功
            if ($re['data']['code'] == "0000") {
                //交易成功
                if ($re['data']['respData']['bizCode'] == "0000") {
                    return [
                        'status' => 1,
                        'message' => '交易成功',
                        'data' => $re['data'],
                    ];
                } //用户输入密码
                elseif ($re['data']['respData']['bizCode'] == "2002") {
                    return [
                        'status' => 2,
                        'message' => '请用户输入密码',
                        'data' => $re['data'],
                    ];
                } else {
                    return [
                        'status' => 3,
                        'message' => $re['data']['respData']['bizMsg'],
                        'data' => $re['data'],
                    ];
                }


            } else {
                return [
                    'status' => 0,
                    'message' => $re['data']['msg']
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => '0',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ];
        }
    }
    
    
     //获取银联标志
    public function get_un_openid($data)
    {
        try {
            $url = 'https://openapi.tianquetech.com/query/getUnionInfo';
            $userAuthCode = $data['userAuthCode'];
            $paymentApp = $data['paymentApp'];
            $orgId = $data['orgId'];
            $privateKey = $data['privateKey'];
            $sxfpublic = $data['sxfpublic'];

            $reqData = [
                'userAuthCode' => $userAuthCode,//授权码
                'paymentApp' => $paymentApp,//订单总金额元
            ];

            $obj = new BaseController();
            $data = [
                'orgId' => $orgId,
                'reqId' => time(),
                'version' => '1.0',
                'timestamp' => date('Ymdhis', time()),
                'signType' => 'RSA',
                'reqData' => $reqData,
            ];

            $re = $obj->execute($data, $url, $privateKey, $sxfpublic);
            if ($re['status'] == 0) {
                return $re;
            }

            //业务成功
            if ($re['data']['code'] == "0000") {
                //交易成功
                if ($re['data']['respData']['bizCode'] == "0000") {
                    return [
                        'status' => 1,
                        'message' => '交易成功',
                        'data' => $re['data'],
                    ];
                } //用户输入密码
                elseif ($re['data']['respData']['bizCode'] == "2002") {
                    return [
                        'status' => 2,
                        'message' => '请用户输入密码',
                        'data' => $re['data'],
                    ];
                } else {
                    return [
                        'status' => 3,
                        'message' => $re['data']['respData']['bizMsg'],
                        'data' => $re['data'],
                    ];
                }
            } else {
                return [
                    'status' => 0,
                    'message' => $re['data']['msg']
                ];
            }
        } catch
        (\Exception $exception) {
            return [
                'status' => 0,
                'message' => $exception->getMessage(),
            ];
        }
    }


    //获取微信刷脸调用凭证
    public function lose_face($data)
    {
        try {
            $out_trade_no = $data['out_trade_no'];
            $url = $data['url'];
            $mno = $data['mno'];
            $orgId = $data['orgId'];
            $privateKey = $data['privateKey'];
            $sxfpublic = $data['sxfpublic'];
            $raw_data = $data['raw_data'];
            $device_id = $data['device_id'];
            $store_no = $data['store_no'];

            $obj = new BaseController();
            $data = [
                'orgId' => $orgId,
                'reqId' => time(),
                'version' => '1.0',
                'timestamp' => date('Ymdhis', time()),
                'signType' => 'RSA',
                'reqData' => [
                    'mno' => $mno, //商户编号
                    'storeNo' => $store_no, //门店编号
                    'orderNo' => $out_trade_no, //商户订单号
                    'rawdata' => $raw_data, //初始数据
                    'deviceId' => $device_id, //微信终端编号（WXtrmno）
                ],
            ];

//            Log::info('随行付-获取微信刷脸调用凭证：');
//            Log::info($data);
            $re = $obj->execute($data, $url, $privateKey, $sxfpublic);
//            Log::info($re);
            //系统错误
            if ($re['status'] == 0) {
                return $re;
            }

            //业务成功
            if ($re['data']['code'] == "0000") {
                //成功
                if ($re['data']['respData']['bizCode'] == "0000") {
                    return [
                        'status' => 1,
                        'message' => '交易成功',
                        'data' => $re['data']
                    ];
                } else {
                    return [
                        'status' => 0,
                        'message' => $re['data']['respData']['bizMsg']
                    ];
                }
            } else {
                return [
                    'status' => 0,
                    'message' => $re['data']['msg']
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => '0',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ];
        }
    }


    //微信提交实名认证申请
    public function weChatRealNameCommitApply($data)
    {
        try {
            $mno = $data['mno'];
            $backUrl = $data['backUrl'] ?? '';
            $orgId = $data['orgId'];
            $privateKey = $data['privateKey'];
            $sxfpublic = $data['sxfpublic'];

            $obj = new BaseController();
            $data = [
                'orgId' => $orgId,
                'reqId' => time(),
                'version' => '1.0',
                'timestamp' => date('Ymdhis', time()),
                'signType' => 'RSA',
                'reqData' => [
                    'mno' => $mno
                ],
            ];
            if ($backUrl) $data['reqData']['backUrl'] = $backUrl;

//            Log::info('随行付-微信提交实名认证-入参');
//            Log::info($data);
            $re = $obj->execute($data, $this->realNameCommitApply, $privateKey, $sxfpublic);
            Log::info('随行付-微信提交实名认证-结果');
            Log::info($re);
            //系统错误
            if ($re['status'] == 0) {
                return $re;
            }

            //业务成功
            if ($re['data']['code'] == "0000") {
                //成功
                if ($re['data']['respData']['bizCode'] == "0000") {
                    return [
                        'status' => 1,
                        'message' => '成功',
                        'data' => $re['data']
                    ];
                } else {
                    return [
                        'status' => 0,
                        'message' => $re['data']['respData']['bizMsg']
                    ];
                }
            } else {
                return [
                    'status' => 0,
                    'message' => $re['data']['msg']??'失败'
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => -1,
                'message' => $ex->getMessage().' | '.$ex->getLine()
            ];
        }
    }


    //微信提交实名认证申请 查询
    public function weChatRealNameQuery($data)
    {
        try {
            $mno = $data['mno'];
            $wxApplyNo = $data['wxApplyNo'];
            $orgId = $data['orgId'];
            $privateKey = $data['privateKey'];
            $sxfpublic = $data['sxfpublic'];

            $obj = new BaseController();
            $data = [
                'orgId' => $orgId,
                'reqId' => time(),
                'version' => '1.0',
                'timestamp' => date('Ymdhis', time()),
                'signType' => 'RSA',
                'reqData' => [
                    'mno' => $mno,
                    'wxApplyNo' => $wxApplyNo
                ],
            ];

            Log::info('随行付-微信实名认证-结果查询-入参');
            Log::info($data);
            $re = $obj->execute($data, $this->realNameQueryApplyInfo, $privateKey, $sxfpublic);
            Log::info('随行付-微信实名认证-结果查询-结果');
            Log::info($re);
            //系统错误
            if ($re['status'] == 0) {
                return $re;
            }

            //业务成功
            if ($re['data']['code'] == "0000") {
                //成功
                if ($re['data']['respData']['bizCode'] == "0000") {
                    return [
                        'status' => 1,
                        'message' => '成功',
                        'data' => $re['data']
                    ];
                } else {
                    return [
                        'status' => 0,
                        'message' => $re['data']['respData']['bizMsg']
                    ];
                }
            } else {
                return [
                    'status' => 0,
                    'message' => $re['data']['msg']??'失败'
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => -1,
                'message' => $ex->getMessage().' | '.$ex->getLine()
            ];
        }
    }


    //微信提交实名认证 撤销
    public function weChatRealNameBack($data)
    {
        try {
            $mno = $data['mno'];
            $wxApplyNo = $data['wxApplyNo'];
            $orgId = $data['orgId'];
            $privateKey = $data['privateKey'];
            $sxfpublic = $data['sxfpublic'];

            $obj = new BaseController();
            $data = [
                'orgId' => $orgId,
                'reqId' => time(),
                'version' => '1.0',
                'timestamp' => date('Ymdhis', time()),
                'signType' => 'RSA',
                'reqData' => [
                    'mno' => $mno,
                    'wxApplyNo' => $wxApplyNo
                ],
            ];

            Log::info('随行付-微信实名认证-撤销-入参');
            Log::info($data);
            $re = $obj->execute($data, $this->realNameBackApplyBill, $privateKey, $sxfpublic);
            Log::info('随行付-微信实名认证-撤销-结果');
            Log::info($re);
            //系统错误
            if ($re['status'] == 0) {
                return $re;
            }

            //业务成功
            if ($re['data']['code'] == "0000") {
                //成功
                if ($re['data']['respData']['bizCode'] == "0000") {
                    return [
                        'status' => 1,
                        'message' => '成功',
                        'data' => $re['data']
                    ];
                } else {
                    return [
                        'status' => 0,
                        'message' => $re['data']['respData']['bizMsg']
                    ];
                }
            } else {
                return [
                    'status' => 0,
                    'message' => $re['data']['msg']??'失败'
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => -1,
                'message' => $ex->getMessage().' | '.$ex->getLine()
            ];
        }
    }


    //微信子商户授权状态 查询
    public function weChatRealNameQueryGrantStatus($data)
    {
        try {
            $subMchId = $data['childNo'];
            $orgId = $data['orgId'];
            $privateKey = $data['privateKey'];
            $sxfpublic = $data['sxfpublic'];

            $obj = new BaseController();
            $data = [
                'orgId' => $orgId,
                'reqId' => time(),
                'version' => '1.0',
                'timestamp' => date('Ymdhis', time()),
                'signType' => 'RSA',
                'reqData' => [
                    'subMchId' => $subMchId
                ],
            ];

            Log::info('随行付-微信子商户授权状态查询-入参');
            Log::info($data);
            $re = $obj->execute($data, $this->realNameQueryGrantStatus, $privateKey, $sxfpublic);
            Log::info('随行付-微信子商户授权状态查询-结果');
            Log::info($re);
            //系统错误
            if ($re['status'] == 0) {
                return $re;
            }

            //业务成功
            if ($re['data']['code'] == "0000") {
                //成功
                if ($re['data']['respData']['bizCode'] == "0000") {
                    return [
                        'status' => 1,
                        'message' => '成功',
                        'data' => $re['data']
                    ];
                } else {
                    return [
                        'status' => 0,
                        'message' => $re['data']['respData']['bizMsg']
                    ];
                }
            } else {
                return [
                    'status' => 0,
                    'message' => $re['data']['msg']??'失败'
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => -1,
                'message' => $ex->getMessage().' | '.$ex->getLine()
            ];
        }
    }

    // 微信扫码点餐-上送餐品信息接口（随行付）
    public function wechatOrder(Request $request)
    {
        $requestData = $request->all();

        // 参数校验
        $check_data = [
            'applet_appid'  => '小程序appid',
            'order_id'      => '小程序订单号',
            'user_openid'   => '用户openid',
            'session_key'   => '用户session_key',
            'status'        => 'status',
            'store_id'      => 'store_id',
        ];
        $check = $this->check_required($requestData, $check_data);
        if ($check) {
            return $this->sys_response(400001, $check);
        }

        $appletOrderModel       = new CustomerAppletsUserOrders();
        $appletOrderGoodsModel  = new CustomerAppletsUserOrderGoods();

        // 小程序点餐路径
        $order_entry = "pages/ordermenu/ordermenu";

        // 查询随行付商户号
        $config_obj = new VbillConfigController();
        $vbill_merchant_obj = $config_obj->vbill_merchant($requestData['store_id'], 0);
        if (!$vbill_merchant_obj) {
            return $this->sys_response(40002, "随行付-商户未入网");
        }
        if (!$vbill_merchant_obj->mno) {
            return $this->sys_response(40002, "随行付-商户号不存在");
        }

        // 根据 order_id 查询 小程序订单信息，
        $orderInfo = $appletOrderModel->getOrderInfo($requestData);
        if (empty($orderInfo)) {
            return $this->sys_response(40002, "暂无订单数据");
        }

        $total_amount       = $orderInfo['order_total_money'] * 100;
        $discount_amount    = $orderInfo['use_coupon_money'] * 100;
        $user_amount        = $orderInfo['order_pay_money'] * 100;

        // 组合用户菜品信息
        $orderGoodsInfo = $appletOrderGoodsModel->getOrderGoodsInfo($requestData);
        if (empty($orderGoodsInfo)) {
            return $this->sys_response(40002, "暂无订单商品数据");
        }

        // 用户点餐菜品信息
        $dish_list = [];
        foreach ($orderGoodsInfo as $key => $value) {
            $dish_list[] = [
                'outDishNo'     => (string)$orderGoodsInfo[$key]['good_id'],    // 商户菜品ID
                'name'          => $orderGoodsInfo[$key]['name'],               // 菜品名称
                'orderPrice'    => $orderGoodsInfo[$key]['good_price'] * 100,   // 菜品单价，单位为分
                'unit'          => 'BY_SHARE',                                  // 菜品单位，BY_SHARE-按份 BY_WEIGHT-按重量
                'orderCount'    => $orderGoodsInfo[$key]['good_num'],           // 菜品数量，保留小数点后2位有效数字
            ];
        }

        $date = date("Y-m-d H:i:s", time());
        $date_now = $this->dateTransformationTimezone($date);

        // 获取随行付参数
        $vbillConfigModel = new VbillConfig();
        $vbillConfigInfo = $vbillConfigModel->getVbillConfigInfo();
        if (empty($vbillConfigInfo)) {
            return $this->sys_response(40002, "暂无随行付配置信息");
        }

        try {
            $orgId          = $vbillConfigInfo['orgId'];
            $privateKey     = $vbillConfigInfo['privateKey'];
            $sxfpublic      = $vbillConfigInfo['sxfpublic'];

            $obj = new BaseController();
            // $data = [
            //     'reqId' => time(),
            //     'mno'                   => $wx_merchant_id,                 // 商户编号
            //     'loginToken'            => $requestData['session_key'],     // 授权码
            //     'outTradeNo'            => $requestData['out_order_no'],    // 正交易天阙订单号
            //     'orderEntry'            => $order_entry,                    // 点餐入口
            //     'orderTotalAmount'      => $total_amount,                   // 订单总额
            //     'orderDiscountAmount'   => $discount_amount,                // 优惠金额
            //     'orderUserAmount'       => $user_amount,                    // 实际支付金额
            //     'orderStatus'           => $requestData['status'],          // 订单状态
            //     'dishList'              => $dish_list,                      // 餐品信息
            // ];

            $data_re = [
                'orgId'     => $orgId,
                'reqId'     => time(),
                'version'   => '1.0',
                'timestamp' => date('Ymdhis', time()),
                'signType'  => 'RSA',
                'reqData'   => [
                    'mno'                   => $vbill_merchant_obj->mno,        // 随行付商户编号
                    'loginToken'            => $requestData['session_key'],     // 授权码
                    'outTradeNo'            => $requestData['out_order_no'],    // 正交易天阙订单号
                    'orderEntry'            => $order_entry,                    // 点餐入口
                    'orderTotalAmount'      => $total_amount,                   // 订单总额
                    'orderDiscountAmount'   => $discount_amount,                // 优惠金额
                    'orderUserAmount'       => $user_amount,                    // 实际支付金额
                    'orderStatus'           => $requestData['status'],          // 订单状态
                    'dishList'              => $dish_list,                      // 餐品信息
                ],
            ];

            $re = $obj->execute($data_re, $this->wechatOrderUrl, $privateKey, $sxfpublic);
            Log::info('随行付-微信扫码点餐-结果');
            Log::info($re);
            //系统错误
            if ($re['status'] == 0) {
                return $re;
            }

            //业务成功
            if ($re['data']['code'] == "0000") {
                //成功
                if ($re['data']['respData']['bizCode'] == "0000") {
                    return [
                        'status' => 1,
                        'message' => '成功',
                        'data' => $re['data']
                    ];
                } else {
                    return [
                        'status' => 0,
                        'message' => $re['data']['respData']['bizMsg']
                    ];
                }
            } else {
                return [
                    'status' => 0,
                    'message' => $re['data']['msg']??'失败'
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => -1,
                'message' => $ex->getMessage().' | '.$ex->getLine()
            ];
        }
    }

}
