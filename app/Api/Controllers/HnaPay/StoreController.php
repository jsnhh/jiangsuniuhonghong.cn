<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/07/27
 * Time: 下午5:13
 */

namespace App\Api\Controllers\HnaPay;


use App\Models\HnaPayStore;
use App\Models\LklPayMcc;
use App\Models\LklStore;
use App\Models\ProvinceCity;
use App\Models\Store;
use App\Models\StoreBank;
use App\Models\StoreImg;
use App\Models\StorePayWay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StoreController extends BaseController
{
    public function lklPayMerchantReplenish(Request $request)
    {
        try {
            $type = $request->get('type', '1'); //2-查询数据
            $store_id = $request->get('storeId', ''); //门店id
            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                $this->status = -1;
                $this->message = '门店不存在或状态异常';
                return $this->format();
            }
            $lklStore = LklStore::where('store_id', $store_id)->select('*')->first();
            if ($type == '2') {
                $this->status = 1;
                $this->message = '返回数据成功';
                return $this->format($lklStore);
            }
            $email = $request->get('email', '');
            $mcc_type = $request->get('mcc_type', '');
            $mcc = $request->get('mcc', '');
            $mcc_name = $request->get('mcc_name', '');
            $settle_type = $request->get('settle_type', '');
            $agree_ment_url = $request->get('agree_ment_url', '');
            $others_url = $request->get('others_url', '');
            $letter_of_authorization_url = $request->get('letter_of_authorization_url', '');
            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');
            $province_name = $request->get('province_name', '');
            $city_name = $request->get('city_name', '');
            $area_name = $request->get('area_name', '');
            $settle_province_code = $request->get('settle_province_code', '');
            $settle_city_code = $request->get('settle_city_code', '');
            $settle_province_name = $request->get('city_code', '');
            $settle_city_name = $request->get('settle_province_name', '');
            $openning_bank_code = $request->get('openning_bank_code', '');
            $openning_bank_name = $request->get('openning_bank_name', '');
            $clearing_bank_code = $request->get('clearing_bank_code', '');
            $lkl_data = [
                'email' => $email,
                'mcc_type' => $mcc_type,
                'mcc' => $mcc,
                'mcc_name' => $mcc_name,
                'settle_type' => $settle_type,
                'agree_ment_url' => $agree_ment_url,
                'others_url' => $others_url,
                'letter_of_authorization_url' => $letter_of_authorization_url,
                'province_code' => $province_code,
                'city_code' => $city_code,
                'area_code' => $area_code,
                'province_name' => $province_name,
                'city_name' => $city_name,
                'area_name' => $area_name,
                'settle_province_code' => $settle_province_code,
                'settle_city_code' => $settle_city_code,
                'settle_province_name' => $settle_province_name,
                'settle_city_name' => $settle_city_name,
                'openning_bank_code' => $openning_bank_code,
                'openning_bank_name' => $openning_bank_name,
                'clearing_bank_code' => $clearing_bank_code,
            ];
            StoreImg::where('store_id', $store_id)->update(['store_auth_bank_img' => $letter_of_authorization_url]);
            $lklStore = LklStore::where('store_id', $store_id)->select('id')->first();
            if ($lklStore) {
                $result = LklStore::where('store_id', $store_id)->update($lkl_data);
                if (!$result) {
                    $this->status = 2;
                    $this->message = '更新失败';
                    return $this->format();
                }
            } else {
                $lkl_data['store_id'] = $store_id;
                $result = LklStore::create($lkl_data);
                if (!$result) {
                    $this->status = 2;
                    $this->message = '添加失败';
                    return $this->format();
                }
            }
            $this->message = '添加成功';
            $this->status = 1;
            return $this->format($result);

        } catch (\Exception $ex) {
            Log::info('拉卡拉补充材料--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    //商户入网
    public function addMerchant($StoreInfo, $StoreBank, $StoreImg)
    {
        try {
            $store_id = $StoreInfo->store_id;
            $storePayWay = StorePayWay::where('store_id', $store_id)
                ->where('company', 'hnapay')
                ->where('ways_source', 'weixin')
                ->first();

            if (!$storePayWay) {
                return json_encode([
                    'status' => 2,
                    'message' => "未设置费率"
                ]);
            }
            $hanPayStore = HnaPayStore::where('store_id', $store_id)->select('*')->first();
            $merchantType = '';
            if ($StoreInfo->store_type == '1') { //个体工商户
                $merchantType = '02';
            } else if ($StoreInfo->store_type == '2') { //企业
                $merchantType = '03';
            } else if ($StoreInfo->store_type == '3') { //小微商户
                $merchantType = '01';
            }
            if (!$hanPayStore) {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'门店列表-通道管理'中补充资料"
                ]);
            }
            $businessLicEnt = $StoreInfo->store_license_time;
            if ($StoreInfo->store_license_time == '长期') {
                $businessLicEnt = '9999-99-99';
            }
            $legalPersonCertificateEnt = $StoreInfo->head_sfz_time;
            if ($StoreInfo->head_sfz_time == '长期') {
                $legalPersonCertificateEnt = '9999-99-99';
            }
            $validateDateExpired = $StoreBank->bank_sfz_time;
            if ($StoreBank->bank_sfz_time == '长期') {
                $validateDateExpired = '9999-99-99';
            }
            //图片公共部分开始-------
            if (!$hanPayStore->protocol_photo) {
                return json_encode([
                    'status' => -1,
                    'message' => '商户协议-未上传'
                ]);
            }
            $protocol_photo_res = $this->img_upload($hanPayStore->protocol_photo, '14');//商户协议 14
            if (!$protocol_photo_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '商户协议-上传失败'
                ]);
            }
            HnaPayStore::where('store_id', $store_id)->update(['protocol_photo_id' => $protocol_photo_res['pictureName']]);

            if (!$StoreImg->head_sfz_img_a) {
                return json_encode([
                    'status' => -1,
                    'message' => '法人身份证正面-未上传'
                ]);
            }
            $lawyer_cert_front_photo_res = $this->img_upload($StoreImg->head_sfz_img_a, '1');//法人身份证正面 1
            if (!$lawyer_cert_front_photo_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '法人身份证正面-上传失败'
                ]);
            }
            HnaPayStore::where('store_id', $store_id)->update(['lawyer_cert_front_photo_id' => $lawyer_cert_front_photo_res['pictureName']]);

            if (!$StoreImg->head_sfz_img_b) {
                return json_encode([
                    'status' => -1,
                    'message' => '法人身份证反面-未上传'
                ]);
            }
            $lawyer_cert_back_photo_res = $this->img_upload($StoreImg->head_sfz_img_b, '2');//法人身份证反面 2
            if (!$lawyer_cert_back_photo_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '法人身份证反面-上传失败'
                ]);
            }
            HnaPayStore::where('store_id', $store_id)->update(['lawyer_cert_back_photo_id' => $lawyer_cert_back_photo_res['pictureName']]);

            if (!$StoreImg->store_logo_img) {
                return json_encode([
                    'status' => 1,
                    'message' => '营业场所门头照-未上传'
                ]);
            }
            $main_photo_res = $this->img_upload($StoreImg->store_logo_img, '8');//营业场所门头照 8
            if (!$main_photo_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '营业场所门头照-上传失败'
                ]);
            }
            HnaPayStore::where('store_id', $store_id)->update(['main_photo_id' => $main_photo_res['pictureName']]);

            if (!$StoreImg->store_img_a) {
                return json_encode([
                    'status' => 2,
                    'message' => '收银台照片-未上传'
                ]);
            }
            $store_cashier_photo_res = $this->img_upload($StoreImg->store_img_a, '10');//收银台照片 10
            if (!$store_cashier_photo_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '收银台照片-上传失败'
                ]);
            }
            HnaPayStore::where('store_id', $store_id)->update(['store_cashier_photo_id' => $store_cashier_photo_res['pictureName']]);

            if (!$StoreImg->store_img_b) {
                return json_encode([
                    'status' => 2,
                    'message' => '经营场所照片-未上传'
                ]);
            }
            $store_hall_photo_res = $this->img_upload($StoreImg->store_img_b, '9'); //门店内景 9
            if (!$store_hall_photo_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '经营场所照片-上传失败'
                ]);
            }
            HnaPayStore::where('store_id', $store_id)->update(['store_hall_photo_id' => $store_hall_photo_res['pictureName']]);
            //图片公共部分结束-------

            if ($StoreInfo->store_type == 1 || $StoreInfo->store_type == 2) {//1个体商户、2企业商户
                if (!$StoreImg->store_license_img) {
                    return json_encode([
                        'status' => 2,
                        'message' => '营业执照-未上传'
                    ]);
                }
                $license_photo_res = $this->img_upload($StoreImg->store_license_img, '4');//营业执照 4
                if (!$license_photo_res) {
                    return json_encode([
                        'status' => -1,
                        'message' => '营业执照-上传失败'
                    ]);
                }
                HnaPayStore::where('store_id', $store_id)->update(['license_photo_id' => $license_photo_res['pictureName']]);

                if ($StoreBank->store_bank_type == '02') {// 02对公结算
                    //A011-开户许可证
                    if (!$StoreImg->store_industrylicense_img) {
                        return json_encode([
                            'status' => 2,
                            'message' => '开户许可证-未上传'
                        ]);
                    }
                    $opening_license_account_photo_res = $this->img_upload($StoreImg->store_industrylicense_img, '3');//开户许可证 3
                    if (!$opening_license_account_photo_res) {
                        return json_encode([
                            'status' => -1,
                            'message' => '开户许可证-上传失败'
                        ]);
                    }
                    HnaPayStore::where('store_id', $store_id)->update(['opening_license_account_photo_id' => $opening_license_account_photo_res['pictureName']]);

                }
                if ($StoreBank->store_bank_type == '01') {// 对私结算
                    if (!$StoreImg->bank_img_a) {
                        return json_encode([
                            'status' => 2,
                            'message' => '结算银行卡正面-未上传'
                        ]);
                    }
                    $bank_card_front_photo_res = $this->img_upload($StoreImg->bank_img_a, '16'); //结算银行卡正面 16
                    if (!$bank_card_front_photo_res) {
                        return json_encode([
                            'status' => -1,
                            'message' => '结算银行卡正面-上传失败'
                        ]);
                    }
                    HnaPayStore::where('store_id', $store_id)->update(['bank_card_front_photo_id' => $bank_card_front_photo_res['pictureName']]);

                    if (!$StoreImg->bank_sfz_img_a) {
                        return json_encode([
                            'status' => 2,
                            'message' => '结算卡身份证正面-未上传'
                        ]);
                    }
                    $authorized_cert_front_photo_res = $this->img_upload($StoreImg->bank_sfz_img_a, '21'); //结算卡身份证正面 21
                    if (!$authorized_cert_front_photo_res) {
                        return json_encode([
                            'status' => -1,
                            'message' => '结算卡身份证正面-上传失败'
                        ]);
                    }
                    HnaPayStore::where('store_id', $store_id)->update(['authorized_cert_front_photo_id' => $authorized_cert_front_photo_res['pictureName']]);

                    if (!$StoreImg->bank_sfz_img_b) {
                        return json_encode([
                            'status' => 2,
                            'message' => '结算卡身份证反面-未上传'
                        ]);
                    }
                    $authorized_cert_back_photo_res = $this->img_upload($StoreImg->bank_sfz_img_b, '22');//结算卡身份证反面 22
                    if (!$authorized_cert_back_photo_res) {
                        return json_encode([
                            'status' => -1,
                            'message' => '结算卡身份证反面-上传失败'
                        ]);
                    }
                    HnaPayStore::where('store_id', $store_id)->update(['authorized_cert_back_photo_id' => $authorized_cert_back_photo_res['pictureName']]);
                    if ($StoreInfo->head_name != $StoreBank->store_bank_name) { //非法人结算

                        if (!$StoreImg->store_auth_bank_img) {
                            return json_encode([
                                'status' => 2,
                                'message' => '结算授权书-未上传'
                            ]);
                        }
                        $settle_auth_letter_photo_res = $this->img_upload($StoreImg->store_auth_bank_img, '20');// 结算授权书 20
                        if (!$settle_auth_letter_photo_res) {
                            return json_encode([
                                'status' => -1,
                                'message' => '结算授权书-上传失败'
                            ]);
                        }
                        HnaPayStore::where('store_id', $store_id)->update(['settle_auth_letter_photo_id' => $settle_auth_letter_photo_res['pictureName']]);
                        if (!$StoreImg->bank_sc_img) {
                            return json_encode([
                                'status' => 2,
                                'message' => '手持身份证-未上传'
                            ]);
                        }
                        $hold_identity_pic_res = $this->img_upload($StoreImg->bank_sc_img, '20');// 手持身份证 20
                        if (!$hold_identity_pic_res) {
                            return json_encode([
                                'status' => -1,
                                'message' => '手持身份证-上传失败'
                            ]);
                        }
                        HnaPayStore::where('store_id', $store_id)->update(['hold_identity_pic_id' => $hold_identity_pic_res['pictureName']]);
                    }
                }
            } else { //小微
                if (!$StoreImg->bank_img_a) {
                    return json_encode([
                        'status' => 2,
                        'message' => '结算银行卡正面-未上传'
                    ]);
                }
                $bank_card_front_photo_res = $this->img_upload($StoreImg->bank_img_a, '16'); //结算银行卡正面 16
                if (!$bank_card_front_photo_res) {
                    return json_encode([
                        'status' => -1,
                        'message' => '结算银行卡正面-上传失败'
                    ]);
                }
                HnaPayStore::where('store_id', $store_id)->update(['bank_card_front_photo_id' => $bank_card_front_photo_res['pictureName']]);

                if (!$StoreImg->bank_sfz_img_a) {
                    return json_encode([
                        'status' => 2,
                        'message' => '结算卡身份证正面-未上传'
                    ]);
                }
                $authorized_cert_front_photo_res = $this->img_upload($StoreImg->bank_sfz_img_a, '21'); //结算卡身份证正面 21
                if (!$authorized_cert_front_photo_res) {
                    return json_encode([
                        'status' => -1,
                        'message' => '结算卡身份证正面-上传失败'
                    ]);
                }
                HnaPayStore::where('store_id', $store_id)->update(['authorized_cert_front_photo_id' => $authorized_cert_front_photo_res['pictureName']]);

                if (!$StoreImg->bank_sfz_img_b) {
                    return json_encode([
                        'status' => 2,
                        'message' => '结算卡身份证反面-未上传'
                    ]);
                }
                $authorized_cert_back_photo_res = $this->img_upload($StoreImg->bank_sfz_img_b, '22');//结算卡身份证反面 22
                if (!$authorized_cert_back_photo_res) {
                    return json_encode([
                        'status' => -1,
                        'message' => '结算卡身份证反面-上传失败'
                    ]);
                }
                HnaPayStore::where('store_id', $store_id)->update(['authorized_cert_back_photo_id' => $authorized_cert_back_photo_res['pictureName']]);
                if ($StoreInfo->head_name != $StoreBank->store_bank_name) { //非法人结算

                    if (!$StoreImg->store_auth_bank_img) {
                        return json_encode([
                            'status' => 2,
                            'message' => '结算授权书-未上传'
                        ]);
                    }
                    $settle_auth_letter_photo_res = $this->img_upload($StoreImg->store_auth_bank_img, '20');// 结算授权书 20
                    if (!$settle_auth_letter_photo_res) {
                        return json_encode([
                            'status' => -1,
                            'message' => '结算授权书-上传失败'
                        ]);
                    }
                    HnaPayStore::where('store_id', $store_id)->update(['settle_auth_letter_photo_id' => $settle_auth_letter_photo_res['pictureName']]);
                    if (!$StoreImg->bank_sc_img) {
                        return json_encode([
                            'status' => 2,
                            'message' => '手持身份证-未上传'
                        ]);
                    }
                    $hold_identity_pic_res = $this->img_upload($StoreImg->bank_sc_img, '20');// 手持身份证 20
                    if (!$hold_identity_pic_res) {
                        return json_encode([
                            'status' => -1,
                            'message' => '手持身份证-上传失败'
                        ]);
                    }
                    HnaPayStore::where('store_id', $store_id)->update(['hold_identity_pic_id' => $hold_identity_pic_res['pictureName']]);
                }
            }
            $profitConf = [
                [
                    'rateTypeId' => '0101',
                    'channel' => '01',
                    'openFlag' => '1',
                    'feeRate' => '0.0038',

                ], [
                    'rateTypeId' => '0201',
                    'channel' => '02',
                    'openFlag' => '1',
                    'feeRate' => '0.0038',
                ]
            ];
            $requestId = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
            $data = [
                'requestId' => $requestId,
                'merchantNm' => $StoreInfo->store_name,
                'merchantShortNm' => $StoreInfo->store_short_name,
                'merchantType' => $merchantType,
                'mcc' => $hanPayStore->mcc,
                'province' => $hanPayStore->province_code,
                'city' => $hanPayStore->city_code,
                'district' => $hanPayStore->area_code,
                'address' => $StoreInfo->store_address,
                'linkman' => $StoreInfo->people,
                'phone' => $StoreInfo->people_phone,
                'email' => $hanPayStore->email,
                'customerPhone' => $StoreInfo->people_phone,
                'principal' => $StoreInfo->head_name,
                'principalIdcodeType' => 0,
                'principalIdcode' => $StoreInfo->head_sfz_no,
                'legalPersonCertificateStt' => $StoreInfo->head_sfz_stime,
                'legalPersonCertificateEnt' => $legalPersonCertificateEnt,
                'protocolPhoto' => $hanPayStore->protocol_photo_id,
                'lawyerCertFrontPhoto' => $hanPayStore->lawyer_cert_front_photo_id,
                'lawyerCertBackPhoto' => $hanPayStore->lawyer_cert_back_photo_id,
                'mainPhoto' => $hanPayStore->main_photo_id,
                'storeHallPhoto' => $hanPayStore->store_hall_photo_id,
                'storeCashierPhoto' => $hanPayStore->store_cashier_photo_id,
                'accountNo' => $StoreBank->store_bank_no,
                'bankId' => $hanPayStore->bank_id,
                'accountNm' => $StoreBank->store_bank_name,
                'cnapsCode' => $StoreBank->bank_no,
                'bankName' => $StoreBank->sub_bank_name,
                'bankProvince' => $hanPayStore->bank_province_code,
                'bankCity' => $hanPayStore->bank_city_code,
                'settleWay' => $hanPayStore->settle_way,
                'profitConf' => $profitConf,
            ];
            if ($merchantType != '01') {
                $data['licenseMatch'] = '00';
                $data['businessLicenseName'] = $StoreInfo->store_name;
                $data['businesslicense'] = $StoreInfo->store_license_no;
                $data['businessLicStt'] = $StoreInfo->store_license_stime;
                $data['businessLicEnt'] = $businessLicEnt;
                $data['licensePhoto'] = $hanPayStore->license_photo_id;
                $data['businessLicEnt'] = $businessLicEnt;
                $data['businessLicEnt'] = $businessLicEnt;
            }
            if ($StoreBank->store_bank_type == '02') {//对公
                $data['accountType'] = '1';
                $data['openingLicenseAccountPhoto'] = $hanPayStore->opening_license_account_photo_id;
            }
            if ($StoreBank->store_bank_type == '01') {//对私
                $data['accountType'] = '2';
                if ($StoreInfo->head_name != $StoreBank->store_bank_name) { //非法人结算
                    $data['accountNature'] = '2';
                    $data['settleAuthLetterPhoto'] = $hanPayStore->settle_auth_letter_photo_id;
                } else {
                    $data['accountNature'] = '1';
                }
                $data['idcardNo'] = $StoreBank->bank_sfz_no;
                $data['validateDateStart'] = $StoreBank->bank_sfz_stime;
                $data['validateDateExpired'] = $validateDateExpired;
                $data['identityPhone'] = $hanPayStore->identity_phone;
                $data['bankCardFrontPhoto'] = $hanPayStore->bank_card_front_photo_id;
                $data['authorizedCertFrontPhoto'] = $hanPayStore->authorized_cert_front_photo_id;
                $data['authorizedCertBackPhoto'] = $hanPayStore->authorized_cert_back_photo_id;
                $data['holdIdentityPic'] = $hanPayStore->hold_identity_pic_id;
            }
            Log::info('新生易商户入网--入参');
            Log::info(stripslashes(json_encode($data, JSON_UNESCAPED_UNICODE)));
            $url = $this->mer_url . '/merchant/apply';
            $sign = $this->getSign($data);
            $data['sign'] = $sign;
            $res = $this->post($url, $data);
            Log::info('新生商户入网--res');
            Log::info($res);
            $res_data = json_decode($res, true);
            if (isset($res_data['status'])) {
                $storeUp = [
                    'merchant_no' => $res_data['merchantNo'],
                    'audit_status' => '01',
                    'audit_msg' => '待审核',
                ];
                LklStore::where('store_id', $store_id)->update($storeUp);
                return json_encode([
                    'status' => 1,
                    'data' => $res_data
                ]);
            } else {
                return json_encode([
                    'status' => 2,
                    'data' => $res_data
                ]);
            }

        } catch (\Exception $ex) {
            Log::info('新生入网--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }

    }

    //商户费率变更
    public function updateMerchantFee(Request $request)
    {
        try {
            $store_id = $request->get('store_id', '1');
            $storePayWay = StorePayWay::where('store_id', $store_id)
                ->where('company', 'hnapay')
                ->where('ways_source', 'weixin')
                ->first();

            if (!$storePayWay) {
                return json_encode([
                    'status' => 2,
                    'message' => "未设置费率"
                ]);
            }
            $lklStore = LklStore::where('store_id', $store_id)->select('settle_type', 'merchant_no')->first();
            $rate = $storePayWay->rate;
            $wei_rate = [
                'fee' => $rate,
                'feeType' => 'WECHAT'
            ];
            $ali_rate = [
                'fee' => $rate,
                'feeType' => 'ALIPAY'
            ];
            $fees = [
                $wei_rate,
                $ali_rate
            ];
            $fee_data = [
                'fees' => $fees,
                'settleType' => $lklStore->settle_type,
                'productCode' => 'WECHAT_PAY'
            ];
            $url = $this->lkl_up_url . 'fee/' . $lklStore->merchant_no;
            Log::info($url);
            Log::info('拉卡拉商户费率信息变更--入参');
            Log::info(stripslashes(json_encode($fee_data, JSON_UNESCAPED_UNICODE)));
            $token = $this->get_token_post($this->getToken, 'update');
            $res = $this->post($url, $fee_data, $token);
            Log::info('拉卡拉商户费率信息变更--res');
            Log::info($res);
            if (!$res) {
                return json_encode([
                    'status' => '-1',
                    'message' => '请求失败',
                ]);
            }
            $res_data = json_decode($res, true);
            if (isset($res_data['message']) && $res_data['message'] == 'SUCCESS') {
                $storeUp = [
                    'audit_status' => '01',
                    'audit_msg' => '待审核',
                ];
                LklStore::where('store_id', $store_id)->update($storeUp);
                StorePayWay::where('store_id', $store_id)
                    ->where('company', 'lklpay')
                    ->update(['status' => 2, 'status_desc' => '商户费率信息变更中']);
                return json_encode([
                    'status' => 1,
                    'message' => '请求成功',
                ]);
            } else {
                return json_encode([
                    'status' => '-1',
                    'message' => $res_data['message'],
                ]);
            }

        } catch (\Exception $ex) {
            Log::info('拉卡拉商户费率变更--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    //商户结算信息修改
    public function updateMerchantSettle(Request $request)
    {
        try {
            $store_id = $request->get('store_id', '1');
            $storeInfo = Store::where('store_id', $store_id)->select('*')->first();
            $storeImg = StoreImg::where('store_id', $store_id)
                ->select('*')
                ->first();
            if (!$storeImg) {
                return json_encode([
                    'status' => 2,
                    'message' => "请完善图片信息"
                ]);
            }
            $storeBank = StoreBank::where('store_id', $store_id)
                ->select('*')
                ->first();
            if (!$storeBank) {
                return json_encode([
                    'status' => 2,
                    'message' => "请完善账户信息"
                ]);
            }
            $hnaPayStore = HnaPayStore::where('store_id', $store_id)->select('*')->first();
            if ($storeInfo->store_type == 1 || $storeInfo->store_type == 2) {//1个体商户、2企业商户
                if ($storeBank->store_bank_type == '02') {// 02对公结算
                    //A011-开户许可证
                    if (!$storeBank->store_industrylicense_img) {
                        return json_encode([
                            'status' => 2,
                            'message' => '开户许可证-未上传'
                        ]);
                    }
                    $opening_permit = $storeImg->store_industrylicense_img;
                    $opening_permit_res = $this->img_upload($opening_permit, '0', 'OPENING_PERMIT');
                    if (!$opening_permit_res) {
                        return json_encode([
                            'status' => -1,
                            'message' => '开户许可证-上传失败'
                        ]);
                    }
                    LklStore::where('store_id', $store_id)->update(['opening_permit' => $opening_permit_res['url']]);

                } else if ($storeBank->store_bank_type == '01') {// 对私结算
                    if (!$storeImg->bank_img_a) {
                        return json_encode([
                            'status' => 2,
                            'message' => '结算银行卡正面-未上传'
                        ]);
                    }
                    $bank_card = $storeImg->bank_img_a;
                    $bank_card_res = $this->img_upload($bank_card, '0', 'BANK_CARD');
                    if (!$bank_card_res) {
                        return json_encode([
                            'status' => -1,
                            'message' => '结算银行卡正面-上传失败'
                        ]);
                    }
                    LklStore::where('store_id', $store_id)->update(['bank_card' => $bank_card_res['url']]);

                    if ($storeInfo->head_name != $storeBank->store_bank_name) { //非法人结算
                        if (!$storeImg->bank_sfz_img_a) {
                            return json_encode([
                                'status' => 2,
                                'message' => '结算卡身份证正面-未上传'
                            ]);
                        }
                        $settle_id_card_front = $storeImg->bank_sfz_img_a;
                        $settle_id_card_front_res = $this->img_upload($settle_id_card_front, '0', 'SETTLE_ID_CARD_FRONT');
                        if (!$settle_id_card_front_res) {
                            return json_encode([
                                'status' => -1,
                                'message' => '结算卡身份证正面-上传失败'
                            ]);
                        }
                        LklStore::where('store_id', $store_id)->update(['settle_id_card_front' => $settle_id_card_front_res['url']]);

                        if (!$storeImg->bank_sfz_img_b) {
                            return json_encode([
                                'status' => 2,
                                'message' => '结算卡身份证反面-未上传'
                            ]);
                        }
                        $settle_id_card_behind = $storeImg->bank_sfz_img_b;
                        $settle_id_card_behind_res = $this->img_upload($settle_id_card_behind, '0', 'SETTLE_ID_CARD_BEHIND');
                        if (!$settle_id_card_behind_res) {
                            return json_encode([
                                'status' => -1,
                                'message' => '结算卡身份证反面-上传失败'
                            ]);
                        }
                        LklStore::where('store_id', $store_id)->update(['settle_id_card_behind' => $settle_id_card_behind_res['url']]);

                        if (!$storeImg->store_auth_bank_img) {
                            return json_encode([
                                'status' => 2,
                                'message' => '结算授权书-未上传'
                            ]);
                        }
                        $letter_of_authorization = $storeImg->store_auth_bank_img;
                        $letter_of_authorization_res = $this->img_upload($letter_of_authorization, '0', 'LETTER_OF_AUTHORIZATION');
                        if (!$letter_of_authorization_res) {
                            return json_encode([
                                'status' => -1,
                                'message' => '结算授权书-上传失败'
                            ]);
                        }
                        LklStore::where('store_id', $store_id)->update(['letter_of_authorization' => $letter_of_authorization_res['url']]);
                    }
                }
            } else { //小微
                if (!$storeImg->bank_img_a) {
                    return json_encode([
                        'status' => 2,
                        'message' => '结算银行卡正面-未上传'
                    ]);
                }
                $bank_card = $storeImg->bank_img_a;
                $bank_card_res = $this->img_upload($bank_card, '0', 'BANK_CARD');
                if (!$bank_card_res) {
                    return json_encode([
                        'status' => -1,
                        'message' => '结算银行卡正面-上传失败'
                    ]);
                }
                LklStore::where('store_id', $store_id)->update(['bank_card' => $bank_card_res['url']]);
            }
            $attachments = [];
            $accountKind = '58';
            if ($storeBank->store_bank_type == '02') {// 02对公结算
                $attachments[] = ['id' => $lklStore->opening_permit, 'type' => 'OPENING_PERMIT'];
                $accountKind = '57';
            } else {
                if ($storeInfo->head_name != $storeBank->store_bank_name) {// 非法人结算
                    $attachments[] = ['id' => $lklStore->letter_of_authorization, 'type' => 'LETTER_OF_AUTHORIZATION'];
                    $attachments[] = ['id' => $lklStore->settle_id_card_front, 'type' => 'SETTLE_ID_CARD_FRONT'];
                    $attachments[] = ['id' => $lklStore->settle_id_card_behind, 'type' => 'SETTLE_ID_CARD_BEHIND'];
                }
                $attachments[] = ['id' => $lklStore->bank_card, 'type' => 'BANK_CARD'];
            }
            $url = $this->lkl_up_url . 'settle/' . $lklStore->merchant_no;

            $requestId = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
            $settle_data = [
                'requestId' => $requestId,
                'merchantNo' => $hnaPayStore->merchant_no,
                'modifyType' => '1',
                'accountNo' => $storeBank->store_bank_no,
                'bankId' => $hnaPayStore->bank_id,
                'accountNo' => $storeBank->store_bank_no,
                'attachments' => $attachments,
                'bankNo' => $lklStore->openning_bank_code,
                'bankName' => $lklStore->openning_bank_name,
                'clearingBankNo' => $lklStore->clearing_bank_code,
                'identityNo' => $storeBank->bank_sfz_no,
                'settleProvinceCode' => $lklStore->settle_province_code,
                'settleProvinceName' => $lklStore->settle_province_name,
                'settleCityCode' => $lklStore->settle_city_code,
                'settleCityName' => $lklStore->settle_city_name,
            ];
            if ($storeBank->store_bank_type == '02') {//对公
                $settle_data['accountType'] = '1';
                //$data['openingLicenseAccountPhoto'] = $hnaPayStore->opening_license_account_photo_id;
            }
            if ($storeBank->store_bank_type == '01') {//对私
                $settle_data['accountType'] = '2';
                if ($storeInfo->head_name != $storeBank->store_bank_name) { //非法人结算
                    $settle_data['accountNature'] = '2';
                } else {
                    $settle_data['accountNature'] = '1';
                }
                $validateDateExpiredcnapsCode = $storeBank->bank_sfz_time;
                if ($storeBank->bank_sfz_time == '长期') {
                    $validateDateExpiredcnapsCode = '9999-99-99';
                }
                $settle_data['validateDateStart'] = $storeBank->bank_sfz_stime;
                $settle_data['validateDateExpiredcnapsCode'] = $validateDateExpiredcnapsCode;
            }
            Log::info('拉卡拉商户结算信息变更--入参');
            Log::info(stripslashes(json_encode($settle_data, JSON_UNESCAPED_UNICODE)));
            $token = $this->get_token_post($this->getToken, 'update');
            $res = $this->post($url, $settle_data, $token);
            Log::info('拉卡拉商户结算信息变更--res');
            Log::info($res);
            if (!$res) {
                return json_encode([
                    'status' => '-1',
                    'message' => '请求失败',
                ]);
            }
            $res_data = json_decode($res, true);
            if (isset($res_data['message']) && $res_data['message'] == 'SUCCESS') {
                $storeUp = [
                    'audit_status' => '01',
                    'audit_msg' => '待审核',
                ];
                LklStore::where('store_id', $store_id)->update($storeUp);
                StorePayWay::where('store_id', $store_id)
                    ->where('company', 'lklpay')
                    ->update(['status' => 2, 'status_desc' => '商户账户信息变更中']);
                return json_encode([
                    'status' => 1,
                    'message' => '请求成功',
                ]);
            } else {
                return json_encode([
                    'status' => '-1',
                    'message' => $res_data['message'],
                ]);
            }

        } catch (\Exception $ex) {
            Log::info('拉卡拉商户结算信息变更--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    //商户基础信息修改
    public function updateMerchant(Request $request)
    {
        try {
            $store_id = $request->get('store_id', '1');
            $storeInfo = Store::where('store_id', $store_id)->select('*')->first();
            $storeImg = StoreImg::where('store_id', $store_id)
                ->select('*')
                ->first();
            if (!$storeImg) {
                return json_encode([
                    'status' => 2,
                    'message' => "请完善图片信息"
                ]);
            }
            $storeBank = StoreBank::where('store_id', $store_id)
                ->select('*')
                ->first();
            if (!$storeBank) {
                return json_encode([
                    'status' => 2,
                    'message' => "请完善账户信息"
                ]);
            }
            $hnaPayStore = HnaPayStore::where('store_id', $store_id)->select('*')->first();
            if (!$storeImg->head_sfz_img_a) {
                return json_encode([
                    'status' => -1,
                    'message' => '法人身份证正面-未上传'
                ]);
            }
            $lawyer_cert_front_photo_res = $this->img_upload($storeImg->head_sfz_img_a, '1');//法人身份证正面 1
            if (!$lawyer_cert_front_photo_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '法人身份证正面-上传失败'
                ]);
            }
            HnaPayStore::where('store_id', $store_id)->update(['lawyer_cert_front_photo_id' => $lawyer_cert_front_photo_res['pictureName']]);

            if (!$storeImg->head_sfz_img_b) {
                return json_encode([
                    'status' => -1,
                    'message' => '法人身份证反面-未上传'
                ]);
            }
            $lawyer_cert_back_photo_res = $this->img_upload($storeImg->head_sfz_img_b, '2');//法人身份证反面 2
            if (!$lawyer_cert_back_photo_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '法人身份证反面-上传失败'
                ]);
            }
            HnaPayStore::where('store_id', $store_id)->update(['lawyer_cert_back_photo_id' => $lawyer_cert_back_photo_res['pictureName']]);

            if (!$hnaPayStore->protocol_photo) {
                return json_encode([
                    'status' => -1,
                    'message' => '商户协议-未上传'
                ]);
            }
            $protocol_photo_res = $this->img_upload($hnaPayStore->protocol_photo, '14');//商户协议 14
            if (!$protocol_photo_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '商户协议-上传失败'
                ]);
            }
            HnaPayStore::where('store_id', $store_id)->update(['protocol_photo_id' => $protocol_photo_res['pictureName']]);

            if (!$storeImg->store_logo_img) {
                return json_encode([
                    'status' => 1,
                    'message' => '营业场所门头照-未上传'
                ]);
            }
            $main_photo_res = $this->img_upload($storeImg->store_logo_img, '8');//营业场所门头照 8
            if (!$main_photo_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '营业场所门头照-上传失败'
                ]);
            }
            HnaPayStore::where('store_id', $store_id)->update(['main_photo_id' => $main_photo_res['pictureName']]);
            if (!$storeImg->store_img_a) {
                return json_encode([
                    'status' => 2,
                    'message' => '收银台照片-未上传'
                ]);
            }
            $store_cashier_photo_res = $this->img_upload($storeImg->store_img_a, '10');//收银台照片 10
            if (!$store_cashier_photo_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '收银台照片-上传失败'
                ]);
            }
            HnaPayStore::where('store_id', $store_id)->update(['store_cashier_photo_id' => $store_cashier_photo_res['pictureName']]);

            if (!$storeImg->store_img_b) {
                return json_encode([
                    'status' => 2,
                    'message' => '经营场所照片-未上传'
                ]);
            }
            $store_hall_photo_res = $this->img_upload($storeImg->store_img_b, '9'); //门店内景 9
            if (!$store_hall_photo_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '经营场所照片-上传失败'
                ]);
            }
            HnaPayStore::where('store_id', $store_id)->update(['store_hall_photo_id' => $store_hall_photo_res['pictureName']]);

            if (!$hnaPayStore->merchant_info_modify_photo) {
                return json_encode([
                    'status' => 2,
                    'message' => '商户信息变更表-未上传'
                ]);
            }
            $merchant_info_modify_photo_res = $this->img_upload($hnaPayStore->merchant_info_modify_photo, '18'); //门店内景 9
            if (!$merchant_info_modify_photo_res) {
                return json_encode([
                    'status' => -1,
                    'message' => '商户信息变更表-上传失败'
                ]);
            }
            HnaPayStore::where('store_id', $store_id)->update(['merchant_info_modify_photo_id' => $store_hall_photo_res['pictureName']]);

            if ($storeBank->store_bank_type == '02') {// 02对公结算
                //A011-开户许可证
                if (!$storeImg->store_industrylicense_img) {
                    return json_encode([
                        'status' => 2,
                        'message' => '开户许可证-未上传'
                    ]);
                }
                $opening_license_account_photo_res = $this->img_upload($storeImg->store_industrylicense_img, '3');//开户许可证 3
                if (!$opening_license_account_photo_res) {
                    return json_encode([
                        'status' => -1,
                        'message' => '开户许可证-上传失败'
                    ]);
                }
                HnaPayStore::where('store_id', $store_id)->update(['opening_license_account_photo_id' => $opening_license_account_photo_res['pictureName']]);
            }
            if ($storeInfo->store_type == 1 || $storeInfo->store_type == 2) {
                if (!$storeImg->store_license_img) {
                    return json_encode([
                        'status' => 2,
                        'message' => '营业执照-未上传'
                    ]);
                }
                $license_photo_res = $this->img_upload($storeImg->store_license_img, '4');//营业执照 4
                if (!$license_photo_res) {
                    return json_encode([
                        'status' => -1,
                        'message' => '营业执照-上传失败'
                    ]);
                }
                HnaPayStore::where('store_id', $store_id)->update(['license_photo_id' => $license_photo_res['pictureName']]);
            }
            $requestId = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
            $store_data = [
                'requestId' => $requestId,
                'merchantNo' => $hnaPayStore->merchant_no,
                'modifyType' => '1',
                'merchantShortNm' => $storeInfo->store_short_name,
                'province' => $hnaPayStore->province_code,
                'city' => $hnaPayStore->city_code,
                'district' => $hnaPayStore->area_code,
                'address' => $storeInfo->store_address,
                'phone' => $storeInfo->people_phone,
                'customerPhone' => $storeInfo->people_phone,
                'lawyerCertFrontPhoto' => $hnaPayStore->lawyer_cert_front_photo_id,
                'lawyerCertBackPhoto' => $hnaPayStore->lawyer_cert_back_photo_id,
                'protocolPhoto' => $hnaPayStore->protocol_photo_id,
                'mainPhoto' => $hnaPayStore->main_photo_id,
                'storeHallPhoto' => $hnaPayStore->store_hall_photo_id,
                'storeCashierPhoto' => $hnaPayStore->store_cashier_photo_id,
                'merchantInfoModifyPhoto' => $hnaPayStore->merchant_info_modify_photo_id,
            ];

            if ($storeInfo->store_type == 1 || $storeInfo->store_type == 2) {
                $store_data['licensePhoto'] = $hnaPayStore->license_photo_id;
            }
            Log::info('拉卡拉商户基础信息变更--入参');
            Log::info(stripslashes(json_encode($store_data, JSON_UNESCAPED_UNICODE)));
            $url = $this->mer_url . '/merchant/modify';
            $sign = $this->getSign($store_data);
            $data['sign'] = $sign;
            $res = $this->post($url, $data);
            Log::info('新生商户基础信息变更--res');
            Log::info($res);
            if (!$res) {
                return json_encode([
                    'status' => '-1',
                    'message' => '请求失败',
                ]);
            }
            $res_data = json_decode($res, true);
            if (isset($res_data['message']) && $res_data['message'] == 'SUCCESS') {
                $storeUp = [
                    'audit_status' => '01',
                    'audit_msg' => '待审核',
                ];
                LklStore::where('store_id', $store_id)->update($storeUp);
                StorePayWay::where('store_id', $store_id)
                    ->where('company', 'hnapay')
                    ->update(['status' => 2, 'status_desc' => '商户信息变更中']);
                return json_encode([
                    'status' => 1,
                    'message' => '请求成功',
                ]);
            } else {
                return json_encode([
                    'status' => '-1',
                    'message' => $res_data['message'],
                ]);
            }


        } catch (\Exception $ex) {
            Log::info('新生商户结算信息变更--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }


    //查询门店类别
    public function queryMccList(Request $request)
    {
        try {
            $parent_code = $request->get('parent_code', '1');
            $data = LklPayMcc::where('parent_code', $parent_code)->where('business_scene', '2')->select('*')->get();
            return json_encode([
                'status' => 1,
                'data' => $data
            ]);
        } catch (\Exception $ex) {
            Log::info('拉卡拉获取门店类别--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }

    }

    //查询地区信息
    public function queryAreaList(Request $request)
    {
        try {
            $parent_code = $request->get('parent_code', '1');
            $token = $this->get_token_post($this->getToken);
            $url = $this->lkl_url . 'organization/' . $parent_code;
            $data = $this->get_htkregistration_post($url, $token);
            $list = json_decode($data, true);
            return json_encode([
                'status' => 1,
                'data' => $list
            ]);
        } catch (\Exception $ex) {
            Log::info('拉卡拉获取地区信息--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }

    }

    //查询银行地区信息
    public function queryBankAreaList(Request $request)
    {
        try {
            $parent_code = $request->get('parent_code', '1');
            $token = $this->get_token_post($this->getToken);
            $url = $this->lkl_url . 'organization/bank/' . $parent_code;
            $data = $this->get_htkregistration_post($url, $token);
            $list = json_decode($data, true);
            return json_encode([
                'status' => 1,
                'data' => $list
            ]);
        } catch (\Exception $ex) {
            Log::info('拉卡拉获取银行地址信息--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }

    }

    //查询银行信息
    public function queryBankList(Request $request)
    {
        try {
            $area_code = $request->get('area_code', '1');
            $bank_name = $request->get('bank_name', '');
            if ($bank_name == '中国银行') {
                $bank_name = $bank_name . '股份';
            }
            $token = $this->get_token_post($this->getToken);
            $url = $this->lkl_url . 'bank?' . http_build_query(['areaCode' => $area_code, 'bankName' => $bank_name]);
            Log::info($url);
            $data = $this->get_htkregistration_post($url, $token);
            $list = json_decode($data, true);
            return json_encode([
                'status' => 1,
                'data' => $list
            ]);
        } catch (\Exception $ex) {
            Log::info('拉卡拉获取银行地址信息--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }

    }

    public function getLklPayMccApp(Request $request)
    {
        try {
            $mccLit = DB::table('lkl_mcc')
                ->where('parent_code', '1')
                ->where('business_scene', '2')
                ->select('name as label', 'code as value')
                ->get();

            foreach ($mccLit as $mcc) {

                $subMccLit = DB::table('lkl_mcc')
                    ->where('parent_code', $mcc->value)
                    ->where('business_scene', '2')
                    ->select('name as label', 'code as value')
                    ->get();
                $mcc->children = $subMccLit;
            }

            return response()->json([
                'status' => 1,
                'data' => $mccLit
            ]);
        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }
    }

    public function add_lkl_img(Request $request)
    {
        try {
            $store_id = $request->get('storeId', ''); //门店id
            $storeImg = StoreImg::where('store_id', $store_id)->select('*')->first();
            if (!$storeImg) {
                return [
                    'status' => '-1',
                    'message' => '请完善商户信息'
                ];
            }

            $head_sfz_img_a = $storeImg->head_sfz_img_a;
            $this->img_upload($head_sfz_img_a, '0', 'ID_CARD_FRONT');

        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    public function getSubMerchant(Request $request)
    {
        try {
            $store_id = $request->get('store_id', '');
            $LklStore = LklStore::where('store_id', $store_id)->select('*')->first();
            if ($LklStore->wei_mer && $LklStore->ali_mer) {
                return json_encode([
                    'status' => '1',
                    'message' => '查询成功',
                    'data' => $LklStore
                ]);
            }
            if ($LklStore) {
                $sign_data = [
                    'externalCustomerNo' => $LklStore->customer_no
                ];
                $sign = $this->privateKeyDecrypt($sign_data);
                $data = [
                    'data' => $sign
                ];
                Log::info('拉卡拉子商户号查询--入参');
                Log::info(stripslashes(json_encode($data, JSON_UNESCAPED_UNICODE)));
                $token = $this->get_token_post($this->getToken);
                $res = $this->post($this->lkl_mer_url, $data, $token);
                Log::info('拉卡拉子商户号查询--res');
                Log::info($res);
                $res_data = json_decode($res, true);
                if (isset($res_data['data'])) {
                    $res_data = $this->publicKeyDecrypt($res_data['data']);
                    $res_data = json_decode($res_data, true);
                    Log::info('拉卡拉子商户号查询--res--明文' . json_encode($res_data));
                    $zFBList = $res_data['zFBList'];
                    foreach ($zFBList as $zfb) {
                        LklStore::where('store_id', $store_id)->update(['ali_mer' => $zfb['subMerchantNo']]);
                    }
                    $wXList = $res_data['wXList'];
                    foreach ($wXList as $wx) {
                        if ($wx['channelId'] == '183399713') {
                            LklStore::where('store_id', $store_id)->update(['wei_mer' => $wx['subMerchantNo']]);
                        }
                    }
                }

            }
            $LklStore = LklStore::where('store_id', $store_id)->select('*')->first();
            return json_encode([
                'status' => '1',
                'message' => '查询成功',
                'data' => $LklStore
            ]);

        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    /**
     * @param $img_url
     * @param $sourcechnl 0:PC, 1:安卓, 2:IOS
     * @param $imgType
     */
    public function img_upload($img_url, $pictureType)
    {
        $filePath = $this->images_get($img_url);
        $file = new \CURLFile(realpath($filePath));

        $url = $this->lkl_url . 'file/upload';
        $reqData = [
            'orgNo' => $this->orgNo,
            'pictureType' => $pictureType,
            'file' => $file,
        ];

        return $this->upload_post($url, $reqData);

    }


}
