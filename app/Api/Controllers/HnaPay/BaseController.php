<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/07/27
 * Time: 下午5:13
 */

namespace App\Api\Controllers\HnaPay;

use App\Api\Controllers\BaseController as BBaseController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;

class BaseController extends BBaseController
{
    //入网参数
    public $orgNo = '22251196';
    public $mer_url = 'https://gateway-hpxtest1.hnapay.com/merchant';
    public $privateKey = '';

    //上传图片

    public function upload_post($url, $data)
    {
        Log::info('新生易上传图片-入参：');
        Log::info($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        // 禁用证书验证
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'charset=UTF-8',
            'Content-Type=multipart/form-data',
        ]);

        $file_contents = curl_exec($ch);
        $info = json_decode($file_contents, JSON_UNESCAPED_UNICODE);
        Log::info('拉卡拉上传图片-原始res：');
        Log::info($info);
        curl_close($ch);
        return $info;
    }

    //进件
    public function post($url, $post_data = '', $token)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        $post_data = stripslashes(json_encode($post_data, JSON_UNESCAPED_UNICODE));
        if ($post_data != '') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json; charset=UTF-8',
        ]);
        // 禁用证书验证
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        $file_contents = curl_exec($ch);
        curl_close($ch);
        return $file_contents;
    }


    //支付
    public function pay_post($url, $post_data = '', $isen = false)
    {
        $req_time = date('YmdHis', time());
        $version = '3.0';
        $data = [
            'req_time' => $req_time,
            'version' => $version,
            'req_data' => $post_data
        ];
        $timeStamp = time();
        $nonceStr = $this->get_nonce_str();
        $body = stripslashes(json_encode($data, JSON_UNESCAPED_UNICODE));
        if ($isen) {
            $sm4 = new SM4();
            $body = $sm4->encrypt(base64_decode($this->smKey), $body);
        }

        $dataStr = $this->appid . "\n" . $this->serial_no . "\n" . $timeStamp . "\n" . $nonceStr . "\n" . $body . "\n";
        $signature = $this->getSign($dataStr);
        $Authorization = $this->schema . " appid=\"" . $this->appid . "\"," . "serial_no=\"" . $this->serial_no . "\"," . "timestamp=\""
            . $timeStamp . "\"," . "nonce_str=\"" . $nonceStr . "\"," . "signature=\"" . $signature . "\"";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        if ($data != '') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Authorization: ' . $Authorization
        ]);
        // 禁用证书验证
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        $file_contents = curl_exec($ch);
        curl_close($ch);
        return $file_contents;
    }

    /**
     *解密
     * @param $data
     * @return string
     */
    public function publicKeyDecrypt($data)
    {
        $pubKey = "-----BEGIN PUBLIC KEY-----\n" .
            wordwrap($this->pubKey, 64, "\n", true)
            . "\n-----END PUBLIC KEY-----";
        $crypto = '';
        foreach (str_split(base64_decode($data), 128) as $chunk) {
            openssl_public_decrypt($chunk, $decryptData, $pubKey);
            $crypto .= $decryptData;
        }
        return $crypto;
    }

    public function privateKeyDecrypt($data)
    {
        $privateKey = "-----BEGIN RSA PRIVATE KEY-----\n" .
            wordwrap($this->privateKey, 64, "\n", true) .
            "\n-----END RSA PRIVATE KEY-----";
        $crypto = '';
        foreach (str_split(json_encode($data), 117) as $chunk) {
            openssl_private_encrypt($chunk, $encryptData, $privateKey);
            $crypto .= $encryptData;
        }
        $encrypted = base64_encode($crypto);
        return $encrypted;
    }

    public function images_get($img_url)
    {
        $img_url = explode('/', $img_url);
        $img_url = end($img_url);
        $img = public_path() . '/upload/images/' . $img_url;
        return $img;
    }

    /**
     * 生成12位随机字符串
     */
    public function get_nonce_str()
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $res = "";

        for ($i = 0; $i < 12; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars) - 1)];
        }

        return $res;
    }

    //生成签名
    public function getSign($content)
    {
        $data = $this->formatBizQueryParaMap($content, false);

        $privateKey = "-----BEGIN RSA PRIVATE KEY-----\n" .
            wordwrap($this->privateKey, 64, "\n", true) .
            "\n-----END RSA PRIVATE KEY-----";

        $key = openssl_get_privatekey($privateKey);

        openssl_sign($data, $signature, $key);
        openssl_free_key($key);
        $sign = base64_encode($signature);
        return $sign;
    }

    //组装签名函数
    function formatBizQueryParaMap($paraMap, $urlencode)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if ($urlencode) {
                $v = urlencode($v);
            }
            $buff .= $k . "=" . $v . "&";
        }
        $reqPar = '';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }
}
