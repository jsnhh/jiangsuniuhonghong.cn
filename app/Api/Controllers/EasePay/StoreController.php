<?php


namespace App\Api\Controllers\EasePay;


use App\Api\Controllers\Config\AllinPayConfigController;
use App\Api\Controllers\Config\EasePayConfigController;
use App\Models\AllinPayConfig;
use App\Models\AllinPayStore;
use App\Models\EasePayOccupation;
use App\Models\EasePayStore;
use App\Models\LklStore;
use App\Models\Store;
use App\Models\StoreBank;
use App\Models\StorePayWay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class StoreController extends BaseController
{
    //商户入网
    public function addMerchant($StoreInfo, $StoreBank, $StoreImg)
    {
        try {
            $store_id = $StoreInfo->store_id;
            $storePayWay = StorePayWay::where('store_id', $store_id)
                ->where('company', 'easepay')
                ->where('ways_source', 'weixin')
                ->first();

            if (!$storePayWay) {
                return json_encode([
                    'status' => 2,
                    'message' => "未设置费率"
                ]);
            }
            $easePayConfigController = new EasePayConfigController();
            $easepay_config = $easePayConfigController->easepay_config('1234');
            if (!$easepay_config) {
                return json_encode([
                    'status' => 2,
                    'message' => "首信易配置不存在"
                ]);
            }
            $rate = $storePayWay->rate;
            $easePayStore = EasePayStore::where('store_id', $store_id)->select('*')->first();
            if (!$easePayStore) {
                return json_encode([
                    'status' => 2,
                    'message' => "首信易商户未入网"
                ]);
            }


            //图片公共部分
            if (!$StoreImg->head_sfz_img_a) {
                return json_encode([
                    'status' => -1,
                    'message' => '法人身份证正面-未上传'
                ]);

            } else {
                $img_data = $this->getImgPath($StoreImg->head_sfz_img_a);
                $uploadRes = $this->imageUpload($img_data);
                if ($uploadRes['status'] == '2') {
                    return json_encode([
                        'status' => -1,
                        'message' => '上传图片到首信易-' . $uploadRes['message']
                    ]);
                }
                $legalIdCardProsPath = $uploadRes['url'];
            }
            if (!$StoreImg->head_sfz_img_b) {
                return json_encode([
                    'status' => 1,
                    'message' => '法人身份证反面-未上传'
                ]);
            } else {
                $img_data = $this->getImgPath($StoreImg->head_sfz_img_b);
                $uploadRes = $this->imageUpload($img_data);
                if ($uploadRes['status'] == '2') {
                    return json_encode([
                        'status' => -1,
                        'message' => '上传图片到首信易-' . $uploadRes['message']
                    ]);
                }
                $legalIdCardConsPath = $uploadRes['url'];
            }

            if (!$StoreImg->store_logo_img) {
                return json_encode([
                    'status' => 1,
                    'message' => '门头照-未上传'
                ]);
            } else {
                $img_data = $this->getImgPath($StoreImg->store_logo_img);
                $uploadRes = $this->imageUpload($img_data);
                if ($uploadRes['status'] == '2') {
                    return json_encode([
                        'status' => -1,
                        'message' => '上传图片到首信易-' . $uploadRes['message']
                    ]);
                }
                $storeHeaderPath = $uploadRes['url'];
            }

            if (!$StoreImg->store_img_b) {
                return json_encode([
                    'status' => 2,
                    'message' => '经营场所照片-未上传'
                ]);
            } else {
                $img_data = $this->getImgPath($StoreImg->store_img_b);
                $uploadRes = $this->imageUpload($img_data);
                if ($uploadRes['status'] == '2') {
                    return json_encode([
                        'status' => -1,
                        'message' => '上传图片到首信易-' . $uploadRes['message']
                    ]);
                }
                $storeIndoorPath = $uploadRes['url'];
            }
            if (!$StoreImg->bank_sc_img) {
                return json_encode([
                    'status' => 2,
                    'message' => '请上传手持身份证'
                ]);
            } else {
                $img_data = $this->getImgPath($StoreImg->bank_sc_img);
                $uploadRes = $this->imageUpload($img_data);
                if ($uploadRes['status'] == '2') {
                    return json_encode([
                        'status' => -1,
                        'message' => '上传图片到首信易-' . $uploadRes['message']
                    ]);
                }
                $holdingIdCardPath = $uploadRes['url'];
            }
            if (!$easePayStore->wechat_applet) {
                return json_encode([
                    'status' => 2,
                    'message' => '请上传微信公众号流程图'
                ]);
            } else {
                $img_data = $this->getImgPath($easePayStore->wechat_applet);
                $uploadRes = $this->imageUpload($img_data);
                if ($uploadRes['status'] == '2') {
                    return json_encode([
                        'status' => -1,
                        'message' => '上传图片到首信易-' . $uploadRes['message']
                    ]);
                }
                $wechatAppletPath = $uploadRes['url'];
            }

            if ($StoreInfo->store_type == 1 || $StoreInfo->store_type == 2) {//1个体商户、2企业商户
                if (!$StoreImg->store_license_img) {
                    return json_encode([
                        'status' => 2,
                        'message' => '营业执照-未上传'
                    ]);
                } else {
                    $img_data = $this->getImgPath($StoreImg->store_license_img);
                    $uploadRes = $this->imageUpload($img_data);
                    if ($uploadRes['status'] == '2') {
                        return json_encode([
                            'status' => -1,
                            'message' => '上传图片到首信易-' . $uploadRes['message']
                        ]);
                    }
                    $businessLicensePath = $uploadRes['url'];
                }

                if ($StoreBank->store_bank_type == '02') {// 02对公结算
                    //A011-开户许可证
                    if (!$StoreImg->store_industrylicense_img) {
                        return json_encode([
                            'status' => 2,
                            'message' => '开户许可证-未上传'
                        ]);
                    } else {
                        $img_data = $this->getImgPath($StoreBank->store_industrylicense_img);
                        $uploadRes = $this->imageUpload($img_data);
                        if ($uploadRes['status'] == '2') {
                            return json_encode([
                                'status' => -1,
                                'message' => '上传图片到首信易-' . $uploadRes['message']
                            ]);
                        }
                        $openAccountPath = $uploadRes['url'];
                    }
                } else if ($StoreBank->store_bank_type == '01') {// 对私结算
                    if (!$StoreImg->bank_img_a) {
                        return json_encode([
                            'status' => 2,
                            'message' => '结算银行卡正面-未上传'
                        ]);
                    } else {
                        $img_data = $this->getImgPath($StoreImg->bank_img_a);
                        $uploadRes = $this->imageUpload($img_data);
                        if ($uploadRes['status'] == '2') {
                            return json_encode([
                                'status' => -1,
                                'message' => '上传图片到首信易-' . $uploadRes['message']
                            ]);
                        }
                        $legalPersonBankCardPath = $uploadRes['url'];
                    }

                }
            }

            if ($easePayStore->request_id) {
                $requestId = $easePayStore->request_id;
                $operationType = 'MODIFY';
            } else {
                $requestId = $this->randomStr();
                $operationType = 'CREATE';
            }
            $registerRole = '';
            $signedName = '';
            if ($StoreInfo->store_type == '1') { //个体工商户
                $registerRole = 'INDIVIDUAL_BUSINESS';
                $signedName = $StoreInfo->head_name;
            } else if ($StoreInfo->store_type == '2') { //企业
                if ($StoreInfo->head_name != $StoreInfo->people) {
                    $registerRole = 'UNINCORPORATED_ENTERPRISE';
                    $signedName = $StoreInfo->people;
                } else {
                    $registerRole = 'ENTERPRISE_LEGAL_PERSON';
                    $signedName = $StoreInfo->head_name;
                }
            } else if ($StoreInfo->store_type == '3') { //小微商户
                $registerRole = 'NATURAL_PERSON';
                $signedName = $StoreInfo->head_name;
            }

            $baseInfo = [
                'signedType' => 'GENERAL_SUB_MERCHANT', //商户入网类型
                'signedName' => $signedName,  //签约名称
                'registerRole' => $registerRole, //签约类型
                'cerType' => 'ID_CARD', // 证书类型
                'signedShorthand' => $StoreInfo->store_short_name, //商户简称
                'businessAddressProvince' => $StoreInfo->province_code,
                'businessAddressCity' => $StoreInfo->city_code,
                'businessAddressArea' => $StoreInfo->area_code,
                'businessAddress' => $StoreInfo->province_name . $StoreInfo->city_name . $StoreInfo->area_name . $StoreInfo->store_address,
                'contactName' => $StoreInfo->people,
                'contactEmail' => $easePayStore->contact_email,
                'contactPhone' => $StoreInfo->people_phone,
                'businessClassification' => $easePayStore->mcc_id,
                'desireAuth' => 'DESIRE_MOBILEINFO',
            ];
            $bankCardInfo = [
                'bankName' => $StoreBank->bank_name,
                'bankBranchName' => $StoreBank->sub_bank_name,
                'accountName' => $StoreBank->store_bank_name,
                'bankCardNo' => $StoreBank->store_bank_no,
                'accountType' => ($StoreBank->store_bank_type == '02') ? 'PUBLIC' : 'PRIVATE',//账号类型：PRIVATE对私 PUBLIC对公
                'liquidationType' => 'SETTLE',
                'reservedPhoneNo' => $StoreBank->reserved_mobile,
            ];
            $desireAuthInfo = [
                'legalPersonName' => $StoreInfo->head_name,
                'legalPersonIdNo' => $StoreInfo->head_sfz_no,
                'legalPersonPhoneNo' => $StoreInfo->people_phone
            ];
            $certificateInfo = [
                'legalPersonName' => $StoreInfo->head_name,
                'profession' => $easePayStore->profession,
                'legalPersonPhone' => $StoreInfo->people_phone,
                'legalPersonIdType' => 'IDCARD',
                'legalPersonIdNo' => $StoreInfo->head_sfz_no,
                'idEffectiveDateStart' => $StoreInfo->head_sfz_stime,
                'idEffectiveDateEnd' => ($StoreInfo->head_sfz_time == '长期') ? '2099-12-31' : $StoreInfo->head_sfz_time,
                'testAccountInfo' => '',
                'legalIdCardProsPath' => $legalIdCardProsPath,//法人证件人像面路径
                'legalIdCardConsPath' => $legalIdCardConsPath,//法人证件国徽面路径
                'storeHeaderPath' => $storeHeaderPath,//门店门头照片路径
                'storeIndoorPath' => $storeIndoorPath,//店内场景照片路径
                'holdingIdCardPath' => $holdingIdCardPath,//法人手持证件影印件路径
            ];
            if ($StoreInfo->store_type != '3') {
                $certificateInfo['cerNo'] = $StoreInfo->store_license_no;
                $certificateInfo['sellingArea'] = 'C';
                $certificateInfo['staffSize'] = 'C';
                $certificateInfo['tradingScenarios'] = 'WECHAT_APPLET';
                $certificateInfo['wechatAppletName'] = '江苏牛付电子商务有限公司';
                $certificateInfo['businessLicensePath'] = $businessLicensePath;//营业执照照片路径
                $certificateInfo['wechatAppletPath'] = $wechatAppletPath;//公众号/小程序/生活号业务流程截图影印件路径
            }
            if ($StoreInfo->store_type == '2') {
                $certificateInfo['openAccountPath'] = $openAccountPath;// 	开户许可证照片路径
            } else {
                $certificateInfo['legalPersonBankCardPath'] = $legalPersonBankCardPath;//法人银行卡图影印件路径
            }
            if ($StoreInfo->head_name != $StoreBank->store_bank_name) { //非法人结算
                $certificateInfo['legalPersonBankCardPath'] = $legalPersonBankCardPath;//法人银行卡图影印件路径
            }
            $contractInfo = [
                'receiverName' => $StoreInfo->people,
                'receiverPhone' => $StoreInfo->people_phone,
                'receiverAddress' => $StoreInfo->province_name . $StoreInfo->city_name . $StoreInfo->area_name . $StoreInfo->store_address,
                'contractType' => 'ELECTRON',
            ];
            $paymentProfiles = [
                ['paymentMode' => 'WEIXIN_OFFICIAL', 'feeType' => 'RATE', 'feeAmount' => $rate],
                ['paymentMode' => 'ALIPAY_OFFICIAL', 'feeType' => 'RATE', 'feeAmount' => $rate]
            ];
            $settlementProfile = [
                'settleMode' => 'REGULAR_SETTLE',
                'minAmount' => '0',
                'cycleType' => 'BYWEEK',
                'cycleData' => '1,2,3,4,5,6,7',
                'holidaySettleIdentify' => 'TRUE'
            ];
            $data = [
                'merchantId' => $easepay_config->merchant_id,//首信易服务商商户号
                'requestId' => $requestId,//请求订单号
                'operationType' => $operationType,
                'notifyUrl' => url('/api/easePay/merchant_apply_notify_url'),//商户入网成功回调地址
                'extendedParameters' => 'sendActiveEmail:FALSE,sendExpressPayMsg:FALSE',
                'baseInfo' => $baseInfo,
                'bankCardInfo' => $bankCardInfo,
                'desireAuthInfo' => $desireAuthInfo,
                'certificateInfo' => $certificateInfo,
                'contractInfo' => $contractInfo,
                'paymentProfiles' => $paymentProfiles,
                'settlementProfile' => $settlementProfile,
            ];
            if ($StoreInfo->store_type == '2') {
                $data['certificateContacts'] = [[
                    'name' => $StoreInfo->head_name,
                    'idType' => 'IDCARD',
                    'idNo' => $StoreInfo->head_sfz_no,
                    'effectiveDateStart' => $StoreInfo->head_sfz_stime,
                    'effectiveDateEnd' => ($StoreInfo->head_sfz_time == '长期') ? '2099-12-31' : $StoreInfo->head_sfz_time,
                    'address' => $StoreInfo->province_name . $StoreInfo->city_name . $StoreInfo->area_name . $StoreInfo->store_address,
                    'equityRatio' => '90'
                ]];
            }

            $signData = $this->getSign($data);
            $data['hmac'] = $signData['hmac'];
            Log::info('首信易商户入网--入参');
            Log::info(json_encode($data));

            $res = $this->request($this->add_mer_url, $signData['encryptKey'], $easepay_config->merchant_id, $requestId, $easepay_config->partner_id, $signData['date']);
            Log::info('首信易商户入网--res');
            Log::info($res);
            $res_data = json_decode($res, true);
            if ($res_data['status'] == 'SUCCESS') {
                $audit_status = '2';
                $audit_msg = '审核中';
                $storeUp = [
                    'audit_status' => $audit_status,
                    'audit_msg' => $audit_msg,
                    'sub_merchant_id' => $res_data['subMerchantId']
                ];
                EasePayStore::where('request_id', $res_data['requestId'])->update($storeUp);
                return json_encode([
                    'status' => 1,
                    'data' => $res_data
                ]);
            } else {
                return json_encode([
                    'status' => 2,
                    'message'=>isset($res_data['data']['errorMessage']) ? $res_data['data']['errorMessage'] : $res_data['data']['error'],
                    'data' => $res_data
                ]);
            }

        } catch (\Exception $ex) {
            Log::info('首信易入网--错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }

    }

    public function getEasePayStore(Request $request)
    {
        try {
            $store_id = $request->get('store_id');
            $easePayStore = EasePayStore::where('store_id', $store_id)->select('*')->first();
            return json_encode([
                'status' => 1,
                'data' => $easePayStore
            ]);
        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }

    }

    //查询商户入网状态
    public function queryMerchantReviewStatus($store_id)
    {
        try {
            $easePayConfigController = new EasePayConfigController();
            $easepay_config = $easePayConfigController->easepay_config('1234');
            if (!$easepay_config) {
                return json_encode([
                    'status' => 2,
                    'message' => "首信易配置不存在"
                ]);
            }
            $easePayStore = EasePayStore::where('store_id', $store_id)->select('*')->first();
            if (!$easePayStore) {
                return json_encode([
                    'status' => 2,
                    'message' => "首信易商户未入网"
                ]);
            }
            $data = [
                'merchantId' => $easepay_config->merchant_id,
                'subMerchantId' => $easePayStore->sub_merchant_id
            ];
            $signData = $this->getSign($data);
            $data['hmac'] = $signData['hmac'];
            Log::info('首信易商户状态查询--入参');
            Log::info(json_encode($data));

            $res = $this->request($this->query_mer_url, $signData['encryptKey'], $easepay_config->merchant_id, $easePayStore->request_id, $easepay_config->partner_id, $signData['date']);
            Log::info('首信易商户状态查询--res');
            Log::info($res);
            $res_data = json_decode($res, true);
            if ($res_data['status'] == 'SUCCESS') {
                $audit_status = '2';
                $audit_msg = '审核中';
                if ($res_data['subMerchantReviewStatus'] == 'PROCESSING') {//审核中

                }
                if ($res_data['subMerchantReviewStatus'] == 'PASS') {//审核通过
                    $audit_status = '2';
                    $audit_msg = '审核通过，等待电子签约';
                }
                if ($res_data['subMerchantReviewStatus'] == 'NO_PASS') {//审核不通过
                    $audit_status = '3';
                    $audit_msg = '审核失败';
                }
                if ($res_data['subMerchantReviewStatus'] == 'REFUSE') {//审核拒绝
                    $audit_status = '3';
                    $audit_msg = '审核失败';
                }
                if ($res_data['subMerchantReviewStatus'] == 'ESIGN_PROCESS') {//待签约
                    $audit_status = '2';
                    $audit_msg = '审核通过，等待电子签约';
                }
                if ($res_data['subMerchantReviewStatus'] == 'ESIGN_PROCESSING') {//签约中
                    $audit_status = '2';
                    $audit_msg = '审核通过，签约中';
                }
                if ($res_data['subMerchantReviewStatus'] == 'ESIGN_FAIL') {//签约失败
                    $audit_status = '2';
                    $audit_msg = '审核通过，签约失败';
                }
                if ($res_data['subMerchantReviewStatus'] == 'ESIGN_SUCCESS') {//签约成功
                    $audit_status = '2';
                    $audit_msg = '审核通过，签约成功';
                }
                if ($res_data['subMerchantReviewStatus'] == 'SUCCESS') {//入网成功
                    $audit_status = '1';
                    $audit_msg = '入网成功';
                }
                $storeUp = [
                    'audit_status' => $audit_status,
                    'audit_msg' => $audit_msg
                ];
                if ($res_data['postReviewStatus'] == 'INIT') {//初始化

                }
                if ($res_data['postReviewStatus'] == 'SUCCESS') {//核查通过

                }
                if ($res_data['postReviewStatus'] == 'FAIL') {//核查不通过
                    $audit_status = '3';
                    $audit_msg = '核查不通过-' . $res_data['postReviewRemark'];
                }
                if ($res_data['postReviewStatus'] == 'COMPLEMENT') {//核查补充
                    $audit_status = '3';
                    $audit_msg = '核查补充-' . $res_data['postReviewRemark'];
                    $storeUp['certificate_supplement_url'] = $res_data['certificateSupplementUrl'];
                }
                $storeUp['audit_status'] = $audit_status;
                $storeUp['audit_msg'] = $audit_msg;
                EasePayStore::where('sub_merchant_id', $res_data['subMerchantId'])->update($storeUp);
                StorePayWay::where('store_id', $store_id)
                    ->where('company', 'easepay')
                    ->update([
                        'status' => $audit_status,
                        'status_desc' => $audit_msg
                    ]);
                return json_encode([
                    'status' => 1,
                    'data' => $res_data
                ]);
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => $res_data['errorMessage']
                ]);
            }
        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    //微信报备
    public function weChatApiReport($store_id)
    {
        try {
            $easePayConfigController = new EasePayConfigController();
            $easepay_config = $easePayConfigController->easepay_config('1234');
            if (!$easepay_config) {
                return json_encode([
                    'status' => 2,
                    'message' => "首信易配置不存在"
                ]);
            }
            $easePayStore = EasePayStore::where('store_id', $store_id)->select('*')->first();
            if (!$easePayStore) {
                return json_encode([
                    'status' => 2,
                    'message' => "首信易商户未入网"
                ]);
            }
            $store = Store::where('store_id', $store_id)->select('store_short_name', 'people_phone')->frist();
            $requestId = $this->randomStr();
            $data = [
                'partnerId' => $easepay_config->partner_id,
                'merchantId' => $easePayStore->sub_merchant_id,
                'requestId' => $requestId,
                'distributorNo' => $easepay_config->wx_distributor_no,
                'merchantShortName' => $store->store_short_name,
                'servicePhone' => $store->people_phone,
                'merchantRemark' => $store->store_short_name . '报备'
            ];
            $signData = $this->getSign($data);
            $data['hmac'] = $signData['hmac'];
            Log::info('首信易商户微信报备--入参');
            Log::info(json_encode($data));

            $res = $this->request($this->weChatApiReportUrl, $signData['encryptKey'], $easepay_config->merchant_id, $requestId, $easepay_config->partner_id, $signData['date']);
            Log::info('首信易商户微信报备--res');
            Log::info($res);
            $res_data = json_decode($res, true);
            if ($res_data['status'] == 'SUCCESS') {
                $upData = [
                    'wechat_status' => 'SUCCESS',
                    'wechat_status_desc' => '成功',
                    'wechat_id' => $res_data['mchId'],
                    'wx_request_id' => $requestId,
                    'wx_report_serial_no' => $res_data['reportSerialNo']
                ];
            } else {
                $upData = ['wechat_status' => 'FAILED', 'wechat_status_desc' => $res_data['errorMessage']];
            }
            EasePayStore::where('sub_merchant_id', $res_data['merchantId'])->update($upData);
        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    //支付宝报备
    public function aliPayApiReport($store_id)
    {
        try {
            $easePayConfigController = new EasePayConfigController();
            $easepay_config = $easePayConfigController->easepay_config('1234');
            if (!$easepay_config) {
                return json_encode([
                    'status' => 2,
                    'message' => "首信易配置不存在"
                ]);
            }
            $easePayStore = EasePayStore::where('store_id', $store_id)->select('*')->first();
            if (!$easePayStore) {
                return json_encode([
                    'status' => 2,
                    'message' => "首信易商户未入网"
                ]);
            }
            $store = Store::where('store_id', $store_id)->select('store_short_name', 'people_phone')->frist();
            $requestId = $this->randomStr();
            $data = [
                'partnerId' => $easepay_config->partner_id,
                'merchantId' => $easePayStore->sub_merchant_id,
                'requestId' => $requestId,
                'distributorNo' => $easepay_config->ali_distributor_no,
                'merchantShortName' => $store->store_short_name,
                'servicePhone' => $store->people_phone,
                'tag' => '06',
                'type' => 'LEGAL_PERSON',
            ];
            $signData = $this->getSign($data);
            $data['hmac'] = $signData['hmac'];
            Log::info('首信易商户支付宝报备--入参');
            Log::info(json_encode($data));

            $res = $this->request($this->aliPayApiReportUrl, $signData['encryptKey'], $easepay_config->merchant_id, $requestId, $easepay_config->partner_id, $signData['date']);
            Log::info('首信易商户支付宝报备--res');
            Log::info($res);
            $res_data = json_decode($res, true);
            if ($res_data['status'] == 'SUCCESS') {
                $upData = [
                    'alipay_status' => 'SUCCESS',
                    'alipay_status_desc' => '成功',
                    'alipay_id' => $res_data['mchId'],
                    'ali_report_serial_no' => $res_data['reportSerialNo']
                ];
            } else {
                $upData = ['alipay_status' => 'FAILED', 'alipay_status_desc' => $res_data['errorMessage']];
            }
            EasePayStore::where('sub_merchant_id', $res_data['merchantId'])->update($upData);
        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    //微信支付报备
    public function weChatConfigReport($store_id)
    {
        try {
            $easePayConfigController = new EasePayConfigController();
            $easepay_config = $easePayConfigController->easepay_config('1234');
            if (!$easepay_config) {
                return json_encode([
                    'status' => 2,
                    'message' => "首信易配置不存在"
                ]);
            }
            $easePayStore = EasePayStore::where('store_id', $store_id)->select('*')->first();
            if (!$easePayStore) {
                return json_encode([
                    'status' => 2,
                    'message' => "首信易商户未入网"
                ]);
            }
            $requestId = $this->randomStr();
            $data = [
                'partnerId' => $easepay_config->partner_id,
                'merchantId' => $easePayStore->sub_merchant_id,
                'requestId' => $requestId,
                'reportRequestId' => $easePayStore->wx_request_id,
                'configType' => 'APIPATH',
                'configInfo' => $easepay_config->authorization_directory,
                'appType' => 'OFFICIAL',
            ];
            $signData = $this->getSign($data);
            $data['hmac'] = $signData['hmac'];
            Log::info('首信易商户微信支付报备--入参');
            Log::info(json_encode($data));

            $res = $this->request($this->weChatConfigReportUrl, $signData['encryptKey'], $easepay_config->merchant_id, $requestId, $easepay_config->partner_id, $signData['date']);
            Log::info('首信易商户微信支付报备--res');
            Log::info($res);
            $res_data = json_decode($res, true);
            if ($res_data['status'] == 'SUCCESS') {
                $upData = ['wx_pay_status' => 'SUCCESS', 'wx_pay_status_desc' => '成功'];
            } else {
                $upData = ['wx_pay_status' => 'FAILED', 'wx_pay_status_desc' => $res_data['errorMessage']];
            }
            EasePayStore::where('sub_merchant_id', $res_data['merchantId'])->update($upData);
        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    //终端报备
    public function terminalReport($store_id)
    {
        try {
            $easePayConfigController = new EasePayConfigController();
            $easepay_config = $easePayConfigController->easepay_config('1234');
            if (!$easepay_config) {
                return json_encode([
                    'status' => 2,
                    'message' => "首信易配置不存在"
                ]);
            }
            $easePayStore = EasePayStore::where('store_id', $store_id)->select('*')->first();
            if (!$easePayStore) {
                return json_encode([
                    'status' => 2,
                    'message' => "首信易商户未入网"
                ]);
            }
            $store = Store::where('store_id', $store_id)->select('*')->first();
            $requestId = $this->randomStr();
            $data = [
                'partnerId' => $easepay_config->partner_id,
                'merchantId' => $easePayStore->sub_merchant_id,
                'requestId' => $requestId,
                'reportSerialNo' => $easePayStore->wx_report_serial_no,
                'reportType' => 'WECHAT',
                'operationId' => '00',
                'deviceType' => '11',
                'deviceAddressInfo' => [
                    'deviceAddressProvince' => $store->province_code,
                    'deviceAddressCity' => $store->city_code,
                    'deviceAddressArea' => $store->area_code,
                    'businessAddress' => $store->store_address,
                ]
            ];
            $signData = $this->getSign($data);
            $data['hmac'] = $signData['hmac'];
            Log::info('首信易商户微信终端报备--入参');
            Log::info(json_encode($data));

            $res = $this->request($this->terminalReportUrl, $signData['encryptKey'], $easepay_config->merchant_id, $requestId, $easepay_config->partner_id, $signData['date']);
            Log::info('首信易商户微信终端报备--res');
            Log::info($res);
            $res_data = json_decode($res, true);
            if ($res_data['status'] == 'SUCCESS') {
                $upData = ['wx_report_terminal_no' => $res_data['reportTerminalNo']];
                EasePayStore::where('sub_merchant_id', $res_data['merchantId'])->update($upData);
            } else {
                Log::info("商户===" . $easePayStore->sub_merchant_id . "===微信终端报备失败===" . $res_data['errorMessage']);
            }

            $data['reportType'] = 'ALIPAY';
            $data['reportSerialNo'] = $easePayStore->ali_report_serial_no;
            $data['hmac'] = '';
            $signData = $this->getSign($data);
            $data['hmac'] = $signData['hmac'];
            Log::info('首信易商户支付宝终端报备--入参');
            Log::info(json_encode($data));

            $res = $this->request($this->terminalReportUrl, $signData['encryptKey'], $easepay_config->merchant_id, $requestId, $easepay_config->partner_id, $signData['date']);
            Log::info('首信易商户支付宝终端报备--res');
            Log::info($res);
            $res_data = json_decode($res, true);
            if ($res_data['status'] == 'SUCCESS') {
                $upData = ['ali_report_terminal_no' => $res_data['reportTerminalNo']];
                EasePayStore::where('sub_merchant_id', $res_data['merchantId'])->update($upData);
            } else {
                Log::info("商户===" . $easePayStore->sub_merchant_id . "===支付宝终端报备失败===" . $res_data['errorMessage']);
            }

        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    //重发电子协议
    public function queryElectUrl(Request $request)
    {
        try {
            $store_id = $request->get('store_id');
            $easePayStore = EasePayStore::where('store_id', $store_id)->select('*')->first();
            $EasePayConfigController = new EasePayConfigController();
            $easepay_config = $EasePayConfigController->easepay_config('1234');
            if ($easePayStore->audit_status == '3') {
                return json_encode([
                    'status' => 2,
                    'message' => "审核状态异常，禁止重发"
                ]);
            }
            if (!$easepay_config) {
                return json_encode([
                    'status' => 2,
                    'message' => "首信易配置不存在"
                ]);
            }
            $requestId = $this->randomStr();
            $data = [
                'merchantId' => $easepay_config->merchant_id,
                'requestId' => $requestId,
                'subMerchantId' => $easePayStore->sub_merchant_id,
                'eSignNotifyUrl' => url('/api/easePay/eSignNotifyUrl'),
            ];
            $signData = $this->getSign($data);
            $data['hmac'] = $signData['hmac'];
            Log::info('首信易重发电子签约--入参');
            Log::info(json_encode($data));

            $res = $this->request($this->electronicReSignUrl, $signData['encryptKey'], $easepay_config->merchant_id, $requestId, $easepay_config->partner_id, $signData['date']);
            Log::info('首信易重发电子签约--res');
            Log::info($res);
            $res_data = json_decode($res, true);
            if ($res_data['status'] == 'SUCCESS') {
                EasePayStore::where('sub_merchant_id', $res_data['subMerchantId'])->update(['electronic_contracting_url' => $res_data['electronicContractingUrl']]);
                return json_encode([
                    'status' => 1,
                    'message' => '重发成功',
                    'data' => $res_data
                ]);
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '重发失败-' . $res_data['errorMessage'],
                    'data' => $res_data
                ]);
            }
        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    //重发短信
    public function electSign(Request $request)
    {
        try {
            $store_id = $request->get('store_id');
            $allinPayStore = AllinPayStore::where('store_id', $store_id)->select('*')->first();
            $allinPayConfigController = new AllinPayConfigController();
            $allin_config = $allinPayConfigController->allin_pay_config('1234');
            if ($allinPayStore->audit_status == '3') {
                return json_encode([
                    'status' => 2,
                    'message' => "审核状态异常，禁止查询"
                ]);
            }
            if (!$allin_config) {
                return json_encode([
                    'status' => 2,
                    'message' => "通联配置不存在"
                ]);
            }
            $data = [
                'orgid' => $allin_config->org_id,
                'cusid' => $allinPayStore->cus_id,
                'appid' => $allin_config->appid,
                'version' => '11',
                'randomstr' => $this->randomStr(),
                'signtype' => 'RSA'
            ];
            $sign = $this->getSign($data);
            $data['sign'] = $sign;
            Log::info('查询电子签约地址 --入参');
            Log::info($data);
            $res = $this->request($this->query_elect_url, $data);
            Log::info('查询电子签约地址--res');
            Log::info($res);
            $res_data = json_decode($res, true);
            if ($res_data['retcode'] == 'SUCCESS') {
                $elect_sign_url = $res_data['sybsignurl'];
                AllinPayStore::where('store_id', $allinPayStore->store_id)->update(['elect_sign_url' => $elect_sign_url]);
                return json_encode([
                    'status' => 1,
                    'message' => '查询成功',
                    'data' => $res_data
                ]);
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => $res_data['retmsg'],
                    'data' => $res_data
                ]);
            }
        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    //查询商户微信和支付认证结果
    public function querySubMer(Request $request)
    {
        try {
            $store_id = $request->get('store_id');
            $allinPayStore = AllinPayStore::where('store_id', $store_id)->select('*')->first();
            $allinPayConfigController = new AllinPayConfigController();
            $allin_config = $allinPayConfigController->allin_pay_config('1234');
            if (!$allin_config) {
                return json_encode([
                    'status' => 2,
                    'message' => "通联配置不存在"
                ]);
            }
            if (!$allinPayStore) {
                return json_encode([
                    'status' => 1,
                    'message' => "通联商户未入网"
                ]);
            }
            if ($allinPayStore->win_mer && $allinPayStore->ali_mer) {
                return json_encode([
                    'status' => 1,
                    'message' => "获取成功",
                    'data' => $allinPayStore
                ]);
            }
            if (!$allinPayStore->win_mer) {
                $wx_data = [
                    'orgid' => $allin_config->org_id,
                    'cusid' => $allinPayStore->cus_id,
                    'appid' => $allin_config->appid,
                    'version' => '11',
                    'randomstr' => $this->randomStr(),
                    'signtype' => 'RSA',
                    'chnltype' => 'WXP'
                ];
                $sign = $this->getSign($wx_data);
                $wx_data['sign'] = $sign;
                Log::info('查询商户微信子商户号--入参');
                Log::info($wx_data);
                $wx_res = $this->request($this->query_sub_mer_url, $wx_data);
                Log::info('查询商户微信子商户号--res');
                Log::info($wx_res);
                $wx_res_data = json_decode($wx_res, true);
                if ($wx_res_data['retcode'] == 'SUCCESS') {
                    $win_mer = $wx_res_data['cmid'];
                    AllinPayStore::where('store_id', $allinPayStore->store_id)->update(['win_mer' => $win_mer]);
                } else {
                    Log::info("商户号--" . $store_id . "--获取微信商户号--" . $wx_res_data['retmsg']);
                }
            }
            if (!$allinPayStore->ali_mer) {
                $ali_data = [
                    'orgid' => $allin_config->org_id,
                    'cusid' => $allinPayStore->cus_id,
                    'appid' => $allin_config->appid,
                    'version' => '11',
                    'randomstr' => $this->randomStr(),
                    'signtype' => 'RSA',
                    'chnltype' => 'ALP'
                ];
                $sign = $this->getSign($ali_data);
                $ali_data['sign'] = $sign;
                Log::info('查询商户支付宝子商户号--入参');
                Log::info($ali_data);
                $ali_res = $this->request($this->query_sub_mer_url, $ali_data);
                Log::info('查询商户支付宝子商户号--res');
                Log::info($ali_res);
                $ali_res_data = json_decode($ali_res, true);
                if ($ali_res_data['retcode'] == 'SUCCESS') {
                    $ali_mer = $ali_res_data['cmid'];
                    AllinPayStore::where('store_id', $allinPayStore->store_id)->update(['ali_mer' => $ali_mer]);
                } else {
                    Log::info("商户号--" . $store_id . "--获取支付宝商户号--" . $ali_res_data['retmsg']);
                }
            }
            $allinPayStore = AllinPayStore::where('store_id', $store_id)->select('*')->first();
            return json_encode([
                'status' => 1,
                'message' => "获取成功",
                'data' => $allinPayStore
            ]);

        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    //商户费率变更
    public function editAcct(Request $request)
    {
        try {
            $store_id = $request->get('store_id');
            $storePayWay = StorePayWay::where('store_id', $store_id)
                ->where('company', 'allinpay')
                ->where('ways_source', 'weixin')
                ->first();

            if (!$storePayWay) {
                return json_encode([
                    'status' => 2,
                    'message' => "未设置费率"
                ]);
            }
            if ($storePayWay->status != 1) {
                return json_encode([
                    'status' => 2,
                    'message' => "商户入网不是成功状态，禁止变更"
                ]);
            }
            $rate = $storePayWay->rate;
            $allinPayConfigController = new AllinPayConfigController();
            $allin_config = $allinPayConfigController->allin_pay_config('1234');
            if (!$allin_config) {
                return json_encode([
                    'status' => 2,
                    'message' => "通联配置不存在"
                ]);
            }
            $allinPayStore = AllinPayStore::where('store_id', $store_id)->select('*')->first();
            if (!$allinPayStore) {
                return json_encode([
                    'status' => 2,
                    'message' => "通联商户未入网"
                ]);
            }

            $randomstr = $this->randomStr();
            $StoreBank = StoreBank::where('store_id', $store_id)->select('*')->first();
            $data = [
                'orgid' => $allin_config->org_id,//机构号
                'cusid' => $allinPayStore->cus_id,//商户号
                'appid' => $allin_config->appid,//通联方appid
                'version' => '11',//版本号
                'randomstr' => $randomstr,//随机字符串
                'signtype' => 'RSA',
                'notifyurl' => url('/api/allinPay/merchant_apply_notify_url'),
                'clearmode' => '1',
                'acctid' => $StoreBank->store_bank_no,//结算卡号
                'acctname' => $StoreBank->store_bank_name,//结算户名
                'accttype' => ($StoreBank->store_bank_type == '02') ? '1' : '0',//账号类型：0对私 1对公
                'accttp' => '00',
                'bankcode' => $allinPayStore->bank_code,//所属银行
                'cnapsno' => $StoreBank->bank_no,//支付银行号
            ];
            if ($StoreBank->store_bank_type == '02') {// 02对公结算
                $data['pubacctinfo'] = $StoreBank->store_bank_no . '#' . '';
            }
            $signData = $this->getSign($data);
            $data['sign'] = $signData;
            Log::info('通联商户结算信息变更--入参');
            Log::info($data);
            $res = $this->request($this->edit_acct_url, $data);
            Log::info('通联商户结算信息变更--res');
            Log::info($res);
            $res_data = json_decode($res, true);
            if ($res_data['retcode'] == 'SUCCESS') {
                if ($res_data['auditstatus'] == '00') {
                    $audit_status = '1';
                    $audit_msg = '结算信息变更审核中';
                    $storeUp = [
                        'audit_status' => $audit_status,
                        'audit_msg' => $audit_msg,
                    ];

                    AllinPayStore::where('store_id', $store_id)->update($storeUp);
                    StorePayWay::where('store_id', $store_id)
                        ->where('company', 'allinpay')
                        ->update(['status' => 2, 'status_desc' => '结算信息变更审核中']);
                    return json_encode([
                        'status' => 1,
                        'message' => '申请变更成功',
                        'data' => $res_data
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $res_data['errmsg'],
                        'data' => $res_data
                    ]);
                }


            } else {
                return json_encode([
                    'status' => 2,
                    'message' => $res_data['retmsg'],
                    'data' => $res_data
                ]);
            }
        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    //商户费率变更
    public function editProduct(Request $request)
    {
        try {
            $store_id = $request->get('store_id');
            $storePayWay = StorePayWay::where('store_id', $store_id)
                ->where('company', 'allinpay')
                ->where('ways_source', 'weixin')
                ->first();

            if (!$storePayWay) {
                return json_encode([
                    'status' => 2,
                    'message' => "未设置费率"
                ]);
            }
            if ($storePayWay->status != 1) {
                return json_encode([
                    'status' => 2,
                    'message' => "商户入网不是成功状态，禁止变更"
                ]);
            }
            $rate = $storePayWay->rate;
            $allinPayConfigController = new AllinPayConfigController();
            $allin_config = $allinPayConfigController->allin_pay_config('1234');
            if (!$allin_config) {
                return json_encode([
                    'status' => 2,
                    'message' => "通联配置不存在"
                ]);
            }
            $allinPayStore = AllinPayStore::where('store_id', $store_id)->select('*')->first();
            if (!$allinPayStore) {
                return json_encode([
                    'status' => 2,
                    'message' => "通联商户未入网"
                ]);
            }
            $randomstr = $this->randomStr();
            $prodlist = [
                [
                    'pid' => 'P0002',//产品id P0002-当面付
                    'mtrxcode' => 'VSP501',//微信
                    'feerate' => $rate * 10
                ],
                [
                    'pid' => 'P0002',//产品id P0002-当面付
                    'mtrxcode' => 'VSP511',//支付宝
                    'feerate' => $rate * 10
                ],
                [
                    'pid' => 'P0003',//产品id P0003-网上收银
                    'mtrxcode' => 'VSP501',//微信
                    'feerate' => $rate * 10
                ],
                [
                    'pid' => 'P0003',//产品id P0003-网上收银
                    'mtrxcode' => 'VSP511',//支付宝
                    'feerate' => $rate * 10
                ],


            ];
            $data = [
                'orgid' => $allin_config->org_id,//机构号
                'cusid' => $allinPayStore->cus_id,//商户号
                'appid' => $allin_config->appid,//通联方appid
                'version' => '11',//版本号
                'randomstr' => $randomstr,//随机字符串
                'reqtype' => 'edit',//修改费率
                'signtype' => 'RSA',
                'prodlist' => json_encode($prodlist),
                'notifyurl' => url('/api/allinPay/merchant_apply_notify_url'),
            ];
            $signData = $this->getSign($data);
            $data['sign'] = $signData;
            Log::info('通联商户费率变更--入参');
            Log::info($data);
            $res = $this->request($this->edit_product_url, $data);
            Log::info('通联商户费率变更--res');
            Log::info($res);
            $res_data = json_decode($res, true);
            if ($res_data['retcode'] == 'SUCCESS') {
                if ($res_data['auditstatus'] == '00') {
                    $audit_status = '1';
                    $audit_msg = '费率变更审核中';
                    $storeUp = [
                        'audit_status' => $audit_status,
                        'audit_msg' => $audit_msg,
                    ];

                    AllinPayStore::where('store_id', $store_id)->update($storeUp);
                    StorePayWay::where('store_id', $store_id)
                        ->where('company', 'allinpay')
                        ->update(['status' => 2, 'status_desc' => '费率变更审核中']);
                    return json_encode([
                        'status' => 1,
                        'message' => '申请变更成功',
                        'data' => $res_data
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $res_data['errmsg'],
                        'data' => $res_data
                    ]);
                }


            } else {
                return json_encode([
                    'status' => 2,
                    'message' => $res_data['retmsg'],
                    'data' => $res_data
                ]);
            }
        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    public function repaircusrgc($store_id)
    {
        try {
            $allinPayConfig = AllinPayConfig::where('config_id', '1234')->select('*')->first();
            $allinPayStore = AllinPayStore::where('store_id', $store_id)->select('*')->first();
            $randomstr = $this->randomStr();
            $data = [
                'orgid' => $allinPayConfig->org_id,//机构号
                'cusid' => $allinPayStore->cus_id,//商户号
                'appid' => $allinPayConfig->appid,//通联方appid
                'version' => '11',//版本号
                'randomstr' => $randomstr,//随机字符串
                'signtype' => 'RSA',
            ];
            $signData = $this->getSign($data);
            $data['sign'] = urlencode($signData);
            $url = $this->repaircusrgc_url . '?' . $this->toUrlParams($data);
            AllinPayStore::where('store_id', $store_id)->update(['repaircusrgc_url' => $url]);
        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    //
    public function getOccupation()
    {
        $easePayOccupation = EasePayOccupation::select('code as id', 'name as label')->get();
        return json_encode([
            'status' => 1,
            'message' => '获取成功',
            'data' => $easePayOccupation
        ]);
    }

    function getSex($cid)
    {
        //根据身份证号，自动返回性别
        if (!$this->isIdCard($cid)) return '';
        $sexint = (int)substr($cid, 16, 1);
        return $sexint % 2 === 0 ? '2' : '1';
    }

    public function isIdCard($number)
    {
        //检查是否是身份证号
        // 转化为大写，如出现x
        $number = strtoupper($number);
        //加权因子
        $wi = array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
        //校验码串
        $ai = array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');
        //按顺序循环处理前17位
        $sigma = 0;
        for ($i = 0; $i < 17; $i++) {
            //提取前17位的其中一位，并将变量类型转为实数
            $b = (int)$number{$i};      //提取相应的加权因子
            $w = $wi[$i];     //把从身份证号码中提取的一位数字和加权因子相乘，并累加
            $sigma += $b * $w;
        }
        //计算序号
        $snumber = $sigma % 11;
        //按照序号从校验码串中提取相应的字符。
        $check_number = $ai[$snumber];
        if ($number{17} == $check_number) {
            return true;
        } else {
            return false;
        }
    }


}