<?php

namespace App\Api\Controllers\EasePay;


use App\Models\AllinPayStore;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;

class BaseController
{
    public $private_key = '';
    public $public_key = '';
    public $add_mer_url = 'https://apis.5upay.com/serviceprovider/declaration/declare';//商户入网
    public $weChatApiReportUrl = 'https://apis.5upay.com/serviceprovider/report/weChatApiReport';//微信报备
    public $aliPayApiReportUrl = 'https://apis.5upay.com/serviceprovider/report/aliPayApiReport';//支付宝报备
    public $weChatConfigReportUrl = 'https://apis.5upay.com/serviceprovider/report/weChatConfigReport';//微信支付报备
    public $terminalReportUrl = 'https://apis.5upay.com/serviceprovider/report/terminalReport';//终端报备
    public $query_mer_url = 'https://apis.5upay.com/serviceprovider/declaration/query';//商户状态查询
    public $electronicReSignUrl = 'https://apis.5upay.com/serviceprovider/declaration/electronicReSign';//重发电子签约
    public $reSendMsgUrl = 'https://apis.5upay.com/serviceprovider/declaration/reSendMsg';//重发验证短信
    public $scan_url = 'https://syb-test.allinpay.com/apiweb/unitorder/scanqrpay';//商户主扫接口
    public $jsapi_url = 'https://syb-test.allinpay.com/apiweb/unitorder/pay';//客户主扫接口
    public $cancel_url = 'https://syb-test.allinpay.com/apiweb/tranx/cancel';//交易撤销接口 当天交易退款接口
    public $refund_url = 'https://syb-test.allinpay.com/apiweb/tranx/refund';//退款接口
    public $query_url = 'https://syb-test.allinpay.com/apiweb/tranx/query';//订单查询接口
    public $get_userid_url = 'https://syb-test.allinpay.com/apiweb/unitorder/authcodetouserid';//获取银联标识
    public $private_key_url = '/cert/896462014.pfx';//CFCA私钥证书地址
    public $client_paw = 'chf535446@';
    public $public_key_url = '/cert/payease_publickey.cer';//CFCA私钥证书地址

    //RSA签名
    public function getSign(array $array)
    {

        $bufSignSrc = $this->ToUrlParams($array);
        $sha1mac = sha1($bufSignSrc, true);//进行SHA1 签名
        $pubKey = file_get_contents(dirname(__FILE__) . $this->private_key_url);
        $results = array();
        $worked = openssl_pkcs12_read($pubKey, $results, $this->client_paw);
        $rs = openssl_sign($sha1mac, $hmac, $results['pkey'], "md5");
        $hmac = base64_encode($hmac);
        $array['hmac'] = $hmac;
        $json_str = json_encode($array, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        /*
         * 生成16位随机数（AES秘钥）
         */
        $str1 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
        $randStr = str_shuffle($str1);//打乱字符串
        $rands = substr($randStr, 0, 16);
        $str = trim($json_str);
        $str = $this->addPKCS7Padding($str);
        $encrypt_str = openssl_encrypt($str, 'AES-128-ECB', $rands, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING);
        $date = base64_encode($encrypt_str);

        $verifyKey4Server = file_get_contents(dirname(__FILE__) . $this->public_key_url);
        $pem = chunk_split(base64_encode($verifyKey4Server), 64, "\n");//转换为pem格式的公钥
        $public_key = "-----BEGIN CERTIFICATE-----\n" . $pem . "-----END CERTIFICATE-----\n";
        $pu_key = openssl_pkey_get_public($public_key);//这个函数可用来判断公钥是否是可用的
        openssl_public_encrypt($rands, $encryptKey, $pu_key);//公钥加密
        $encryptKey = base64_encode($encryptKey);
        return [
            'date' => $date, //提交的数据
            'hmac' => $hmac,//sign
            'encryptKey' => $encryptKey//encryptKey
        ];

    }


    public function ToUrlParams(array $array)
    {
        ksort($array);
        $hmacSource = '';
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                ksort($value);
                foreach ($value as $key2 => $value2) {

                    if (is_object($value2)) {
                        $value2 = array_filter((array)$value2);
                        ksort($value2);
                        foreach ($value2 as $oKey => $oValue) {
                            $oValue .= '#';
                            $hmacSource .= trim($oValue);

                        }
                    } else if (is_array($value2)) {
                        ksort($value2);
                        foreach ($value2 as $key3 => $value3) {
                            if (is_object($value3)) {
                                $value3 = array_filter((array)$value3);
                                ksort($value3);
                                foreach ($value3 as $oKey => $oValue) {
                                    $oValue .= '#';
                                    $hmacSource .= trim($oValue);
                                }
                            } else {
                                $value3 .= '#';
                                $hmacSource .= trim($value3);
                            }
                        }
                    } else {
                        $value2 .= '#';
                        $hmacSource .= trim($value2);
                    }
                }
            } else {
                $value .= '#';
                $hmacSource .= trim($value);
            }
        }
    }

    public function request($url, $encryptKey, $merchantId, $requestId, $partnerId, $params)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, 1); // 过滤HTTP头
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/vnd.5upay-v3.0+json',
            'encryptKey: ' . $encryptKey,
            'merchantId: ' . $merchantId,
            'requestId: ' . $requestId,
            'partnerId: ' . $partnerId
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);// 显示输出结果
        curl_setopt($curl, CURLOPT_POST, true); // post传输数据
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);// post传输数据
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);//SSL证书认证
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);//严格认证

        $responseText = curl_exec($curl);
//        curl_close($curl);
//        preg_match_all('/(encryptKey|merchantId|data"):(\s+|")([^"\s]+)/si',$responseText,$m);
//        list($encryptKey, $merchantId, $data) = $m[3];
//        $responsedata = array("data" =>$data,"encryptKey"=>$encryptKey,"merchantId"=>$merchantId);
        //return $responseText;
        preg_match_all('/(encryptKey|merchantId|data"):(\s+|")([^"\s]+)/si', $responseText, $m);
        list($encryptKey, $merchantId, $data) = $m[3];
        $responsedata = array("data" => $data, "encryptKey" => $encryptKey, "merchantId" => $merchantId);
        $encryptKey = $responsedata['encryptKey'];
        $pubKey = file_get_contents(dirname(__FILE__) . $this->private_key_url);
        $results = array();
        $worked = openssl_pkcs12_read($pubKey, $results, $this->client_paw);
        $private_key = $results['pkey'];
        $pi_key = openssl_pkey_get_private($private_key);//这个函数可用来判断私钥是否是可用的，可用返回资源id Resource id
        openssl_private_decrypt(base64_decode($encryptKey), $decrypted, $pi_key);//私钥解密
        $responsedatadata = $responsedata['data'];

        $date = base64_decode($responsedatadata);
        $screct_key = $decrypted;
        $encrypt_str = openssl_decrypt($responsedatadata, "AES-128-ECB", $screct_key);
        $encrypt_str = preg_replace('/[\x00-\x1F]/', '', $encrypt_str);
        $encrypt_str = json_decode($encrypt_str, true);
        $encrypt_str = $this->clearBlank($encrypt_str);
        $hmac = $encrypt_str['hmac'];
        unset($encrypt_str['hmac']); //去除数组的一个hmac元素
        ksort($encrypt_str);
        Log::info(json_encode($encrypt_str, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
        //Log::info($date);

    }

    public function ValidSign(array $array)
    {

    }

    /**
     * 填充算法
     * @param string $source
     * @return string
     */
    function addPKCS7Padding($string, $blocksize = 16)
    {
        $len = strlen($string);
        $pad = $blocksize - ($len % $blocksize);
        $string .= str_repeat(chr($pad), $pad);
        return $string;

    }

    /**
     * 随机字符串
     */
    public function randomStr($length = 20)
    {
        //字符组合
        $str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $len = strlen($str) - 1;
        $randStr = '';
        for ($i = 0; $i < $length; $i++) {
            $randStr .= $str[mt_rand(0, $len)];
        }
        return $randStr;
    }

    public function imageUpload($img)
    {
        $server = 'merchant-sftp.5upay.com';
        $port = 2822;
        $username = '896462014';
        $keyFilePath = dirname(__FILE__) . '/cert/896462014'; // 密钥文件的路径

        $keyFilePathPub = dirname(__FILE__) . '/cert/896462014.pub';
        $fileContent = file_get_contents($keyFilePathPub);
        //Log::info($fileContent);
        $keyPassphrase = ''; // 如果密钥有密码的话
        // 本地文件路径和文件名
        $localFilePath = $img['img_url'];

        // 远程SFTP服务器上的文件路径和文件名
        $remoteFilePath = '/serviceprovider/' . $img['img_name'];

        // 建立SFTP连接
        $connection = ssh2_connect($server, $port);
        $status = '1';
        $message = '上传成功';
        if (!$connection) {
            Log::info('无法连接到SFTP服务器');
            $status = '2';
            $message = '无法连接到SFTP服务器';
        }

        // 进行密钥身份验证
        if (ssh2_auth_pubkey_file($connection, $username, $keyFilePathPub, $keyFilePath, $keyPassphrase)) {
            // 打开SFTP会话
            $sftp = ssh2_sftp($connection);

            if (!$sftp) {
                Log::info('无法打开SFTP会话');
                $status = '2';
                $message = '无法打开SFTP会话';
            }
            Log::info("ssh2.sftp://$sftp$remoteFilePath");
            if (copy($localFilePath, "ssh2.sftp://$sftp$remoteFilePath")) {
                Log::info('文件上传成功！');
            } else {
                Log::info('文件上传失败！');
                $status = '2';
                $message = '文件上传失败';
            }

        } else {
            Log::info('密钥身份验证失败');
            $status = '2';
            $message = '密钥身份验证失败';
        }

        // 关闭连接
        ssh2_disconnect($connection);
        return [
            'status' => $status,
            'message' => $message,
            'url' => $remoteFilePath
        ];
    }

    public function getImgPath($img_url)
    {
        $img_url = explode('/', $img_url);
        $img_name = end($img_url);
        $img_url = public_path() . '/upload/images/' . $img_name;
        return [
            'img_name' => $img_name,
            'img_url' => $img_url
        ];
    }

    public function clearBlank($arr)
    {
        function odd($var)
        {
            return ($var <> '');//return true or false
        }

        return (array_filter($arr, $this->odd));
    }

    function array_remove_empty(&$arr, $trim = true)
    {
        foreach ($arr as $key => $value) {
            if (is_array($value)) {
                array_remove_empty($arr[$key]);
            } else {
                $value = trim($value);
                if ($value == '') {
                    unset($arr[$key]);
                } elseif ($trim) {
                    $arr[$key] = $value;
                }
            }
        }
    }

}