<?php

namespace App\Api\Controllers\AllinPay;

use Illuminate\Support\Facades\Log;

class PayController extends BaseController
{
    //扫一扫 0-系统错误 1-成功 2-正在支付 3-失败
    public function scan_pay($data)
    {
        try {
            $orderNo = $data['out_trade_no'];
            $code = $data['code'];
            $total_amount = $data['total_amount'];
            $randomstr = 'scan' . date('YmdHis') . mt_rand(1000, 9999);
            $orgId = $data['orgId'];
            $txamt = isset($total_amount) ? intval($total_amount * 100) : '';
            $cusId = $data['cusId'];
            $body = $data['body'];
            $appid = $data['appid'];
            $sub_appid = $data['sub_appid'];
            $termno = $data['termno'];
            $notifyURL = $data['notify_url'];
            $terminfo = [
                'termno' => $termno,
                'devicetype' => '10'
            ];
            $reqData = [
                'orgid' => '',
                'cusid' => $cusId,
                'appid' => $appid,
                'randomstr' => $randomstr,
                'trxamt' => $txamt,
                'authcode' => $code,
                'reqsn' => $orderNo,
                'body' => $body,
                'notify_url' => $notifyURL,
                'version' => '11',
                'signtype' => 'RSA',
                'terminfo' => $terminfo

            ];
            $signData = $this->getSign($reqData);
            if($signData['status']=='1'){
                $reqData['sign'] = $signData['sign'];
            }else{
                return [
                    'status' => '-1',
                    'message' => '请填写商户私钥',
                ];
            }

            Log::info('通联-静态二维码-params：');
            Log::info($reqData);
            $re = $this->request($this->scan_url, $reqData);
            Log::info("通联统一扫码支付--" . $re);
            $res_arr = json_decode($re, true);
            //系统错误
            if (!$res_arr) {
                return [
                    'status' => '-1',
                    'message' => '系统错误',
                ];
            }
            if ($res_arr['retcode'] != 'SUCCESS') {
                return [
                    'status' => '-1',
                    'message' => $res_arr['retmsg'],
                ];
            }
            //业务成功
            if ($res_arr['trxstatus'] == "0000") {
                //交易成功
                return [
                    'status' => 1,
                    'message' => '请求成功',
                    'data' => $res_arr,
                ];

            } elseif ($res_arr['trxstatus'] == "2008" || $res_arr['trxstatus'] == "2000") {
                return [
                    'status' => '2',
                    'message' => '等待支付',
                    'data' => $res_arr
                ];
            } else {
                return [
                    'status' => '0',
                    'message' => $res_arr['errmsg'],
                    'data' => $res_arr
                ];
            }

        } catch (\Exception $ex) {
            return [
                'status' => '0',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine(),
            ];
        }
    }

    //静态码提交-公共
    public function qr_submit($data)
    {
        try {
            $orderNo = $data['out_trade_no'];
            $total_amount = $data['total_amount'];
            $payWay = $data['pay_type'];
            $orgId = $data['orgId'];
            $open_id = $data['code'];
            $cusId = $data['cusId'];
            $txamt = isset($total_amount) ? intval($total_amount * 100) : '';
            $appid = $data['appid'];
            $randomstr = date('YmdHis') . mt_rand(1000, 99999);
            $body = $data['body'];
            $notifyURL = $data['notify_url'];
            $sub_appid = $data['wxAppid'];
            $reqData = [
                'reqsn' => $orderNo,//商户订单号
                //'orgid' => $orgId,//机构号
                'cusid' => $cusId,//商户号
                'appid' => $appid,
                'trxamt' => $txamt,//订单总金额分
                'randomstr' => $randomstr,//
                'body' => $body,
                'acct' => $open_id,
                'notify_url' => $notifyURL,
                'version' => '11',
                'signtype' => 'RSA'
            ];
            if ($payWay == 'ALIPAY') {
                $reqData['paytype'] = 'A02';
            }
            if ($payWay == 'WECHAT') {
                $reqData['sub_appid'] = $sub_appid;
                $reqData['paytype'] = 'W02';
            }
            if ($payWay == 'UNIONPAY') {
                $reqData['paytype'] = 'U02';
            }

            $signData = $this->getSign($reqData);
            if($signData['status']=='1'){
                $reqData['sign'] = $signData['sign'];
            }else{
                return [
                    'status' => '-1',
                    'message' => '请填写商户私钥',
                ];
            }
            Log::info('通联主扫支付--params：');
            Log::info($reqData);
            $res = $this->request($this->jsapi_url, $reqData);
            Log::info("通联主扫支付--" . $res);
            $res_arr = json_decode($res, true);
            //系统错误
            if (!$res_arr) {
                return [
                    'status' => '-1',
                    'message' => '系统错误',
                ];
            }
            if ($res_arr['retcode'] != 'SUCCESS') {
                return [
                    'status' => '-1',
                    'message' => $res_arr['retmsg'],
                ];
            }
            //业务成功
            if ($res_arr['trxstatus'] == "0000") {
                //交易成功
                return [
                    'status' => 1,
                    'message' => '请求成功',
                    'data' => $res_arr,
                ];
            } elseif ($res_arr['trxstatus'] == "2008" || $res_arr['trxstatus'] == "2000") {
                return [
                    'status' => '2',
                    'message' => '等待支付',
                    'data' => $res_arr
                ];
            } else {
                return [
                    'status' => '0',
                    'message' => $res_arr['errmsg'],
                    'data' => $res_arr
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => '0',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    //查询订单 0-系统错误 1-成功 2-正在支付 3-失败 4.已经退款
    public function order_query($data)
    {
        try {
            $orderNo = $data['out_trade_no'];
            $cusId = $data['cusId'];
            $ageId = $data['ageId'];
            $trxid = $data['trxid'];
            $randomstr = date('YmdHis') . mt_rand(1000, 99999);
            $appid = $data['appid'];
            $reqData = [
                'reqsn' => $orderNo,
                //'orgid' => $ageId,
                'cusid' => $cusId,
                'appid' => $appid,
                'reqsn' => $orderNo,
                'trxid' => $trxid,
                'randomstr' => $randomstr,
                'version' => '11',
                'signtype' => 'RSA',

            ];
            $signData = $this->getSign($reqData);
            if($signData['status']=='1'){
                $reqData['sign'] = $signData['sign'];
            }else{
                return [
                    'status' => '-1',
                    'message' => '请填写商户私钥',
                ];
            }
            Log::info('通联订单查询-params：');
            Log::info($reqData);
            $re = $this->request($this->query_url, $reqData);
            Log::info("通联订单查询结果--" . $re);
            $res_arr = json_decode($re, true);
            //系统错误
            if (!$res_arr) {
                return [
                    'status' => '-1',
                    'message' => '系统错误',
                ];
            }
            if ($res_arr['retcode'] != 'SUCCESS') {
                return [
                    'status' => '-1',
                    'message' => $res_arr['retmsg'],
                ];
            }
            //业务成功
            if ($res_arr['trxstatus'] == "0000") {
                //交易成功
                return [
                    'status' => 1,
                    'message' => '交易成功',
                    'data' => $res_arr,
                ];
            } elseif ($res_arr['trxstatus'] == "2008" || $res_arr['trxstatus'] == "2000") {
                return [
                    'status' => '2',
                    'message' => '等待支付',
                    'data' => $res_arr
                ];
            } else {
                return [
                    'status' => '3',
                    'message' => $res_arr['errmsg'],
                    'data' => $res_arr
                ];
            }


        } catch (\Exception $ex) {
            return [
                'status' => '0',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine(),
            ];
        }
    }

    //交易撤销 当天退款 0-系统错误 1-成功
    public function cancel($data)
    {
        try {
            $ageId = $data['ageId'];
            $cusId = $data['cusId'];
            $refund_amount = $data['refund_amount'];
            $orderNo = $data['orderNo'];
            $refundAmount = isset($refund_amount) ? intval($refund_amount * 100) : '';
            $randomstr = date('YmdHis') . mt_rand(1000, 99999);
            $appid = $data['appid'];
            $oldreqsn = $data['oldreqsn'];
            $oldtrxid = $data['oldtrxid'];
            $reqData = [
                'reqsn' => $orderNo,
                'randomstr' => $randomstr,
                //'orgid' => $ageId,
                'cusid' => $cusId,
                'appid' => $appid,
                'trxamt' => $refundAmount,
                'oldreqsn' => $oldreqsn,
                'oldtrxid' => $oldtrxid,
                'version' => '11',
                'signtype' => 'RSA'

            ];
            $signData = $this->getSign($reqData);
            if($signData['status']=='1'){
                $reqData['sign'] = $signData['sign'];
            }else{
                return [
                    'status' => '-1',
                    'message' => '请填写商户私钥',
                ];
            }
            Log::info('通联-交易撤销-params:');
            Log::info($reqData);
            $re = $this->request($this->cancel_url, $reqData);
            Log::info("通联撤销--" . $re);
            $res_arr = json_decode($re, true);
            //系统错误
            if (!$res_arr) {
                return [
                    'status' => '-1',
                    'message' => '系统错误',
                ];
            }
            if ($res_arr['retcode'] != 'SUCCESS') {
                return [
                    'status' => '-1',
                    'message' => $res_arr['retmsg'],
                ];
            }
            //业务成功
            if ($res_arr['trxstatus'] == "0000") {
                //交易成功
                return [
                    'status' => 1,
                    'message' => '退款成功',
                    'data' => $res_arr,
                ];
            } else {
                return [
                    'status' => '3',
                    'message' => $res_arr['errmsg'],
                    'data' => $res_arr
                ];
            }

        } catch (\Exception $ex) {
            return [
                'status' => 0,
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    //退款 隔天退款 0-系统错误 1-成功
    public function refund($data)
    {
        try {
            $ageId = $data['ageId'];
            $cusId = $data['cusId'];
            $refund_amount = $data['refund_amount'];
            $orderNo = $data['orderNo'];
            $refundAmount = isset($refund_amount) ? intval($refund_amount * 100) : '';
            $randomstr = date('YmdHis') . mt_rand(1000, 99999);
            $appid = $data['appid'];
            $oldreqsn = $data['oldreqsn'];
            $oldtrxid = $data['oldtrxid'];
            $reqData = [
                'reqsn' => $orderNo,
                'randomstr' => $randomstr,
                //'orgid' => $ageId,
                'cusid' => $cusId,
                'appid' => $appid,
                'trxamt' => $refundAmount,
                'oldreqsn' => $oldreqsn,
                'oldtrxid' => $oldtrxid,
                'version' => '11',
                'signtype' => 'RSA'

            ];
            $signData = $this->getSign($reqData);
            if($signData['status']=='1'){
                $reqData['sign'] = $signData['sign'];
            }else{
                return [
                    'status' => '-1',
                    'message' => '请填写商户私钥',
                ];
            }
            Log::info('通联-交易退款-params:');
            Log::info($reqData);
            $re = $this->request($this->refund_url, $reqData);
            Log::info("通联退款--" . $re);
            $res_arr = json_decode($re, true);
            //系统错误
            if (!$res_arr) {
                return [
                    'status' => '-1',
                    'message' => '系统错误',
                ];
            }
            if ($res_arr['retcode'] != 'SUCCESS') {
                return [
                    'status' => '-1',
                    'message' => $res_arr['retmsg'],
                ];
            }
            //业务成功
            if ($res_arr['trxstatus'] == "0000") {
                //交易成功
                return [
                    'status' => 1,
                    'message' => '退款成功',
                    'data' => $res_arr,
                ];
            } else {
                return [
                    'status' => '3',
                    'message' => $res_arr['errmsg'],
                    'data' => $res_arr
                ];
            }

        } catch (\Exception $ex) {
            return [
                'status' => 0,
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

    public function qrGetUserId($data)
    {
        try {
            $ageId = $data['ageId'];
            $cusId = $data['cusId'];
            $appid = $data['appid'];
            $authcode = $data['authcode'];
            $authtype = $data['authtype'];
            $identify = $data['identify'];
            $randomstr = date('YmdHis') . mt_rand(1000, 99999);
            $reqData = [
                //'orgid' => $ageId,
                'cusid' => $cusId,
                'appid' => $appid,
                'authcode' => $authcode,
                'authtype' => $authtype,
                'identify' => $identify,
                'randomstr' => $randomstr,
                'sub_appid' =>'wxa72b1b67413a8f6e',
                'version' => '11',
                'signtype' => 'RSA'

            ];
            $signData = $this->getSign($reqData);
            if($signData['status']=='1'){
                $reqData['sign'] = $signData['sign'];
            }else{
                return [
                    'status' => '-1',
                    'message' => '请填写商户私钥',
                ];
            }
            Log::info('通联-获取银联支付标识-params:');
            Log::info($reqData);
            $re = $this->request($this->get_userid_url, $reqData);
            Log::info("通联-获取银联支付标识--res" . $re);
            $res_arr = json_decode($re, true);
            //系统错误
            if (!$res_arr) {
                return [
                    'status' => '-1',
                    'message' => '系统错误',
                ];
            }
            if ($res_arr['retcode'] != 'SUCCESS') {
                return [
                    'status' => '-1',
                    'message' => $res_arr['retmsg'],
                ];
            }
            //业务成功
            if ($res_arr['trxstatus'] == "0000") {
                //交易成功
                return [
                    'status' => 1,
                    'message' => '获取成功',
                    'data' => $res_arr,
                ];
            } else {
                return [
                    'status' => '3',
                    'message' => $res_arr['errmsg'],
                    'data' => $res_arr
                ];
            }
        } catch (\Exception $ex) {
            return [
                'status' => 0,
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ];
        }
    }

}
