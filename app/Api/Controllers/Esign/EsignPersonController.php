<?php

namespace App\Api\Controllers\Esign;

use App\Api\Controllers\Esign\comm\EsignHttpHelper;
use App\Api\Controllers\Esign\emun\HttpEmun;
use App\Api\Controllers\Fuiou\BaseController;
use App\Models\EsignAuths;
use App\Models\UserAuths;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

/**
 * 认证和授权服务-个人API
 */
class EsignPersonController extends BaseController
{

    //查询签署信息
    public function getUserEsign(Request $request)
    {
        try {
            $user_id = $request->get('user_id', "");

            $esignAuths = EsignAuths::where('user_id', $user_id)->where('user_type', '1')->first();

            if($esignAuths){
                return json_encode([
                    'status' => '1',
                    'message' => '返回成功',
                    'data' => $esignAuths
                ]);
            }
            return json_encode([
                'status' => '1',
                'message' => '返回成功',
                'data' => null
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => '-1',
                'message' => $exception->getMessage()
            ]);
        }
    }


    /**
     * 参考：https://open.esign.cn/doc/opendoc/apiv3-guide/dtsgz1
     * 1、查询认证授权状态
     * 2、如果未认证，则获取认证授权链接
     *
     */
    public function personAuth(Request $request){

        $data = $request->except(['token']);

        $user_id = $request->get('user_id', '');

        $user_name = $request->get('user_name', '');
        $id_card = $request->get('id_card', '');
        $id_card_front_url = $request->get('id_card_front_url', '');
        $id_card_back_url = $request->get('id_card_back_url', '');
        $user_address = $request->get('user_address', '');

        $scence = $request->get('scence', '1'); //场景：1代表认证；2代表授权
        $psnAccount = $request->get('psn_account', ''); //个人账户（手机号或邮箱）

        $validate = Validator::make($data, [
            'user_id' => 'required',
            'user_name' => 'required',
            'id_card' => 'required',
            'id_card_front_url' => 'required',
            'id_card_back_url' => 'required',
            'psn_account' => 'required',
        ], [
            'required' => ':attribute参数为必填项'
        ], [
            'user_id' => '代理ID',
            'user_name' => '姓名',
            'id_card' => '证件号',
            'id_card_front_url' => '证件照正面',
            'id_card_back_url' => '证件照反面',
            'psn_account' => '手机号',
        ]);
        if ($validate->fails()) {
            $this->status = -1;
            $this->message = $validate->getMessageBag()->first();
            return $this->format();
        }
        //检查是否已实名
        $exist_userAuths = UserAuths::where('user_id', $user_id)->where('user_type', '1')->first();
        if(!$exist_userAuths){
            return json_encode([
                'status' => '-1',
                'message' => '尚未实名，请先进行实名认证'
            ]);
        }

        $userAuth_name = $exist_userAuths->user_name;
        $userAuth_id_card = $exist_userAuths->id_card;

        if($user_name != $userAuth_name || $id_card != $userAuth_id_card){
            return json_encode([
                'status' => '-1',
                'message' => '证件信息与实名认证信息不符，请重新上传'
            ]);
        }

        if($scence == '1'){
            //1、认证
            Log::info("--psnAuthUrl--个人实名。");
            $realnameStatus = $this->personsIdentityInfo($psnAccount,$scence);
            if ($realnameStatus == 1){
                $insertData = [
                    'user_id' => $user_id,
                    'user_name' => $user_name,
                    'id_card' => $id_card,
                    'user_address' => $user_address,
                    'user_type' => '1',
                    'psn_account' => $psnAccount,
                    'id_card_front_url' => $id_card_front_url,
                    'id_card_back_url' => $id_card_back_url,
                    // 'auth_flow_id' => $data->authFlowId,
                    // 'auth_url' => $data->authUrl,
                    // 'auth_short_url' => $data->authShortUrl,
                    'realname_status' => $realnameStatus
                ];

                $exist_auths = EsignAuths::where('user_name', $user_name)->where('id_card', $id_card)->where('user_type', '1')->first();
                if($exist_auths){
                    $exist_auths->update($insertData);
                }else{
                    EsignAuths::create($insertData);
                }

                return json_encode([
                    'status' => '1',
                    'message' => '操作成功'
                ]);
            }else {
                Log::info("--psnAuthUrl--客户未实名，请实名。");
                //客户未实名，请实名
                $res = $this->psnAuthUrl($psnAccount, $user_name, $id_card);
                Log::info("--psnAuthUrl--客户未实名，请实名。 code:" . $res->code);
                if($res->code == '0'){
                    $data = $res->data;

                    $insertData = [
                        'user_id' => $user_id,
                        'user_name' => $user_name,
                        'id_card' => $id_card,
                        'user_address' => $user_address,
                        'user_type' => '1',
                        'psn_account' => $psnAccount,
                        'id_card_front_url' => $id_card_front_url,
                        'id_card_back_url' => $id_card_back_url,
                        'auth_flow_id' => $data->authFlowId,
                        'auth_url' => $data->authUrl,
                        'auth_short_url' => $data->authShortUrl,
                    ];

                    $exist_auths = EsignAuths::where('user_name', $user_name)->where('id_card', $id_card)->where('user_type', '1')->first();
                    if($exist_auths){
                        $exist_auths->update($insertData);
                    }else{
                        EsignAuths::create($insertData);
                    }

                    return json_encode([
                        'status' => '1',
                        'message' => '操作成功'
                    ]);

                }else{
                    return json_encode([
                        'status' => '-1',
                        'message' => $res->message
                    ]);
                }
            }

        }else if($scence == '2'){
            //个人授权
            $authorizeUserInfo = $this->personsIdentityInfo($psnAccount,$scence);
            if ($authorizeUserInfo){
                //$psnId查询个人实名状态获取
                $esignAuth = EsignAuths::where('psn_account', $psnAccount)->where('user_type', '1')->first();
                $psnId = $esignAuth->psn_id;
                $resetFlag = false;
                if($psnId){
                    $response = $this->personsAuthorizedInfo($psnId);
                    $expireTime = json_decode($response->getBody())->data->authorizedInfo[0]->expireTime;
                    //计算是否授权过期
                    $num = $this->expireTime($expireTime);
                    if($num > 0){
                        return json_encode([
                            'status' => '1',
                            'message' => '操作成功'
                        ]);
                    }else{
                        $resetFlag = true;
                    }
                }else{
                    $resetFlag = true;
                }
                if($resetFlag){
                    // 请重新授权
                    $res = $this-> psnAuthorizehUrl($psnAccount, $user_name, $id_card);

                    if($res->code == '0'){
                        $data = $res->data;

                        $insertData = [
                            'user_id' => $user_id,
                            'user_name' => $user_name,
                            'id_card' => $id_card,
                            'user_address' => $user_address,
                            'user_type' => '1',
                            'psn_account' => $psnAccount,
                            'id_card_front_url' => $id_card_front_url,
                            'id_card_back_url' => $id_card_back_url,
                            'auth2_flow_id' => $data->authFlowId,
                            'auth2_url' => $data->authUrl,
                            'auth2_short_url' => $data->authShortUrl,
                        ];

                        $exist_auths = EsignAuths::where('user_name', $user_name)->where('id_card', $id_card)->where('user_type', '1')->first();
                        if($exist_auths){
                            $exist_auths->update($insertData);
                        }else{
                            EsignAuths::create($insertData);
                        }

                        return json_encode([
                            'status' => '1',
                            'message' => '操作成功'
                        ]);

                    }else{
                        return json_encode([
                            'status' => '-1',
                            'message' => $res->message
                        ]);
                    }
                }

            }else{
                // 请授权
                $res = $this->psnAuthorizehUrl($psnAccount, $user_name, $id_card);

                if($res->code == '0'){
                    $data = $res->data;

                    $insertData = [
                        'user_id' => $user_id,
                        'user_name' => $user_name,
                        'id_card' => $id_card,
                        'user_address' => $user_address,
                        'user_type' => '1',
                        'psn_account' => $psnAccount,
                        'id_card_front_url' => $id_card_front_url,
                        'id_card_back_url' => $id_card_back_url,
                        'auth2_flow_id' => $data->authFlowId,
                        'auth2_url' => $data->authUrl,
                        'auth2_short_url' => $data->authShortUrl,
                    ];

                    $exist_auths = EsignAuths::where('user_name', $user_name)->where('id_card', $id_card)->where('user_type', '1')->first();
                    if($exist_auths){
                        $exist_auths->update($insertData);
                    }else{
                        EsignAuths::create($insertData);
                    }

                    return json_encode([
                        'status' => '1',
                        'message' => '操作成功'
                    ]);
                }else{
                    return json_encode([
                        'status' => '-1',
                        'message' => $res->message
                    ]);
                }
            }

        }else{
            return json_encode([
                'status' => '-1',
                'message' => '场景选择错误'
            ]);
        }

    }

    /**
     * 发起个人实名
     * @param $psnAccount
     */
    public function psnAuthUrl($psnAccount, $user_name, $id_card){
        $config = Config::$config;
        $apiaddr = "/v3/psn-auth-url";
        $requestType = HttpEmun::POST;
        $data=[
            "psnAuthConfig"=>[
                "psnAccount"=>$psnAccount,
                "psnInfo"=>[
                    "psnName"=> $user_name,
                    "psnIDCardNum"=> $id_card,
                    "psnIDCardType"=> "CRED_PSN_CH_IDCARD"
                ]
            ],
            "notifyUrl"=> url('/api/esign/notify_url'),
            "clientType"=>"ALL",
            "appScheme"=>""
        ];
        $paramStr = json_encode($data);
        //生成签名验签+json体的header

        $signAndBuildSignAndJsonHeader = EsignHttpHelper::signAndBuildSignAndJsonHeader($config['eSignAppId'], $config['eSignAppSecret'], $paramStr, $requestType, $apiaddr);
        //发起接口请求
        $response = EsignHttpHelper::doCommHttp($config['eSignHost'], $apiaddr, $requestType, $signAndBuildSignAndJsonHeader, $paramStr);
        Log::info("--psnAuthUrl--Status:" . $response->getStatus());
        Log::info("--psnAuthUrl--Body:" . $response->getBody());

        return json_decode($response->getBody());
    }

    /**
     * 发起个人授权
     * @param $psnAccount
     */
    public function psnAuthorizehUrl($psnAccount, $user_name, $id_card){
        $config = Config::$config;
        $apiaddr = "/v3/psn-auth-url";
        $requestType = HttpEmun::POST;
        $data=[
            "psnAuthConfig"=>[
                "psnAccount"=>$psnAccount,
                "psnInfo"=>[
                    "psnName"=> $user_name,
                    "psnIDCardNum"=> $id_card,
                    "psnIDCardType"=> "CRED_PSN_CH_IDCARD"
                ]
            ],
            "authorizeConfig"=>[
                "authorizedScopes"=>[
                    "get_psn_identity_info","psn_initiate_sign","manage_psn_resource"
                ]
            ],
            "notifyUrl"=> url('/api/esign/notify_url'),
            "clientType"=>"ALL",
            "appScheme"=>""
        ];
        $paramStr = json_encode($data);
        //生成签名验签+json体的header

        $signAndBuildSignAndJsonHeader = EsignHttpHelper::signAndBuildSignAndJsonHeader($config['eSignAppId'], $config['eSignAppSecret'], $paramStr, $requestType, $apiaddr);
        //发起接口请求
        $response = EsignHttpHelper::doCommHttp($config['eSignHost'], $apiaddr, $requestType, $signAndBuildSignAndJsonHeader, $paramStr);
        Log::info("--psnAuthorizehUrl--Status:" . $response->getStatus());
        Log::info("--psnAuthorizehUrl--Body:" . $response->getBody());

        return json_decode($response->getBody());
    }

    /**
     * 查询个人认证信息
     * @param $psnId
     * @param $scence
     * @return mixed
     */
    public function personsIdentityInfo($psnId,$scence){
        $config = Config::$config;
        $apiaddr = "/v3/persons/identity-info?psnAccount=".$psnId;
        $requestType = HttpEmun::GET;
        $signAndBuildSignAndJsonHeader = EsignHttpHelper::signAndBuildSignAndJsonHeader($config['eSignAppId'], $config['eSignAppSecret'], "", $requestType, $apiaddr);
        //发起接口请求
        $response = EsignHttpHelper::doCommHttp($config['eSignHost'], $apiaddr, $requestType, $signAndBuildSignAndJsonHeader, "");
        Log::info("--personsIdentityInfo--Status:" . $response->getStatus());
        Log::info("--personsIdentityInfo--Body:" . $response->getBody());
        if(json_decode($response->getBody())->code == '0'){
            $realnameStatus = json_decode($response->getBody())->data->realnameStatus;

            $authorizeUserInfo = json_decode($response->getBody())->data->authorizeUserInfo;

            if ($scence==1){
                return $realnameStatus;
            }else{
                return $authorizeUserInfo;
            }
        }else{
            return "-1";
        }

    }

    /**
     * 查询个人授权信息
     * @param $psnId
     * @return \esign\comm\EsignResponse
     */
    public function personsAuthorizedInfo($psnId){
        $config = Config::$config;
        $apiaddr = "/v3/persons/".$psnId."/authorized-info";
        $requestType = HttpEmun::GET;
        $signAndBuildSignAndJsonHeader = EsignHttpHelper::signAndBuildSignAndJsonHeader($config['eSignAppId'], $config['eSignAppSecret'], "", $requestType, $apiaddr);
        //发起接口请求
        $response = EsignHttpHelper::doCommHttp($config['eSignHost'], $apiaddr, $requestType, $signAndBuildSignAndJsonHeader, "");
        Log::info("--personsAuthorizedInfo--Status:" .$response->getStatus());
        Log::info("--personsAuthorizedInfo--Body:" .$response->getBody());
        return $response;
    }

    function expireTime($expireTime){
        list($t1, $t2) = explode(' ', microtime());
        $nowTime=(float)sprintf('%.0f',(floatval($t1)+floatval($t2))*1000);
        $num=$expireTime-$nowTime;
        return $num;
    }

}