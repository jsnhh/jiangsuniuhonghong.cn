<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2021/01/22
 * Time: 17:49
 */

namespace App\Api\Controllers\Esign;


use App\Models\EsignAuths;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class NotifyController
{

    /**
     * 实名认证 AUTH_PASS
     * 授权完成 AUTHORIZE_FINISH
     * 签署结果通知 SIGN_MISSON_COMPLETE
     * 流程结束通知 SIGN_FLOW_COMPLETE
     */
    public function notify_url(Request $request)
    {
        try {
            $data = $request->all();
            Log::info('Esign-异步通知-回调');
            Log::info($data);

            Log::info('action：' . $data['action']);

            if ($data['action'] == 'AUTH_PASS') { //实名认证
                $authFlowId = $data['authFlowId'];
                $authType = $data['authType']; //PSN - 个人认证，ORG - 机构认证
                $psnId = '';
                $orgId = '';
                if($authType == 'PSN'){
                    $psnId = $data['psnInfo']['psnId'];
                }elseif ($authType == 'ORG'){
                    $orgId = $data['organization']['orgId'];
                }

                Log::info('=====authType：' . $authType);
                Log::info('=====psnId：' . $psnId);

                $esignAuth = EsignAuths::where('auth_flow_id', $authFlowId)->first();
                if ($esignAuth) {
                    $esignAuth->update([
                        'realname_status' => '1', //认证状态:0 - 未实名，1 - 已实名
                        'psn_id' => $psnId,
                        'org_id' => $orgId,
                    ]);
                }

            } else if ($data['action'] == 'AUTHORIZE_FINISH') { //授权完成
                $authFlowId = $data['authFlowId'];

                $esignAuth = EsignAuths::where('auth2_flow_id', $authFlowId)->first();
                if ($esignAuth) {
                    $esignAuth->update([
                        'authorize_status' => '1', //授权状态:0 - 未授权，1 - 已授权
                    ]);
                }
            } else if ($data['action'] == 'SIGN_MISSON_COMPLETE') { //签署结果通知
                $signFlowId = $data['signFlowId'];

                $esignAuth = EsignAuths::where('sign_flow_id', $signFlowId)->first();
                if ($esignAuth) {
                    $esignAuth->update([
                        'sign_result' => $data['signResult'], //签署结果:2 - 签署完成，3 - 失败，4 - 拒签
                        'result_description' => $data['resultDescription'],
                    ]);
                }
            } else if ($data['action'] == 'SIGN_FLOW_COMPLETE') { //流程结束通知
                $signFlowId = $data['signFlowId'];

                $esignAuth = EsignAuths::where('sign_flow_id', $signFlowId)->first();
                if ($esignAuth) {
                    $esignAuth->update([
                        'sign_flow_status' => $data['signFlowStatus'], //签署流程最终状态:2 - 已完成 3 - 已撤销 5 - 已过期 7 - 已拒签
                        'status_description' => $data['statusDescription'],
                    ]);
                }
            }

            return 'success';

        } catch (\Exception $ex) {
            Log::info('Esign-异步通知-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }
    }

}
