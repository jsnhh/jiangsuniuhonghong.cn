<?php


namespace App\Api\Controllers\Esign;

use App\Api\Controllers\Esign\comm\EsignHttpHelper;
use App\Api\Controllers\Esign\comm\EsignUtilHelper;
use App\Api\Controllers\Esign\emun\HttpEmun;
use App\Models\EsignAuths;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * 合同签署服务API
 */
class SignFlowController
{

    public $filePath =  "/file/易企推APP电子签约条款.pdf";
    // public $docTemplateId = "2977bcb44d1645a08b35bac7d48253ab"; //测试环境-模板ID
    public $docTemplateId = "ede04644230c455ab36cbe1d9bfde511"; //正式环境-模板ID

    /**
     * 基于文件发起签署
     */
    public function createByFile(Request $request){
        Log::info('**********基于文件发起签署调用开始**********');
        $user_id = $request->get('user_id', '');
        if(!$user_id){
            return json_encode([
                'status' => '2',
                'message' => '签署失败,获取参数失败'
            ]);
        }

        //检查用户是否签署完成
        $esign = EsignAuths::where('user_id', $user_id)->where('realname_status', '1')
            ->where('authorize_status', '1')->first();

        if($esign){
            $sign_result = $esign->sign_result; //签署结果:2 - 签署完成，3 - 失败，4 - 拒签
            $sign_flow_status = $esign->sign_flow_status; //签署流程最终状态:2 - 已完成 3 - 已撤销 5 - 已过期 7 - 已拒签
            if($sign_result == '2' && $sign_flow_status == '2'){
                return json_encode([
                    'status' => '1',
                    'message' => '操作成功',
                    'data' => [
                        'sign_flow_id' => $esign->sign_flow_id,
                        'sign_flow_status' => $sign_flow_status,
                        "sign_result" => $sign_result
                    ],
                ]);
            }
        }else{
            return json_encode([
                'status' => '2',
                'message' => '获取签署信息失败'
            ]);
        }

        //填写pdf模板生成文件
        $signFlowTitle = '（'. $esign->user_name .'）'. '易企推APP电子签约条款';

        $fileId = $esign->file_id;
        if(!$fileId){
            // 选择官网制作模板方式
            // $fileId = $this->fileUploadUrl();

            $fileId = $this->createByDocTemplate($user_id, $signFlowTitle);
        }
        if($esign->sign_flow_id){
            Log::info('---createByFile--------esign->sign_flow_id:' + $esign->sign_flow_id);
            $shortUrl = $this->getSignUrl($esign->psn_id, $esign->sign_flow_id);

            return json_encode([
                'status' => '1',
                'message' => '签署成功',
                'data' => [
                    'shortUrl' => $shortUrl
                ],
            ]);
        }

        if($fileId == 'error'){
            return json_encode([
                'status' => '2',
                'message' => '签署失败'
            ]);
        }else{
            // 查询已实名、已授权的用户
            $esignAuths = EsignAuths::where('user_id', $user_id)->where('realname_status', '1')
                ->where('authorize_status', '1')->first();
            if($esignAuths){
                $userType = $esignAuths->user_type; //认证类型：1 个人，2 企业
            }else{
                return json_encode([
                    'status' => '2',
                    'message' => '获取签署信息失败'
                ]);
            }
            if($userType == '1'){
                $data = [
                    "docs" =>[
                        [
                            "fileId" =>$fileId,
                            "fileName" =>"易企推APP电子签约条款.pdf"
                        ]
                    ],
                    "signFlowConfig" => [
                        "signFlowTitle" => $signFlowTitle,
                        "autoFinish"=> true,
                        "notifyUrl"=>  url('/api/esign/notify_url'),
                        "signConfig" => [
                            "availableSignClientTypes" => "1" //"" 1 - 网页（自适配H5/PC样式），2 - 支付宝
                        ],
                    ],
                    "signers" =>[
                        [
                            "psnSignerInfo" => [
                                "psnAccount" => $esignAuths->psn_account,
                            ],
                            "noticeConfig" => [
                                "noticeTypes" => "1" //"" - 不进行任何通知，1 - 短信通知，2 - 邮件通知
                            ],
                            "signerType" =>0,
                            "signFields" =>[
                                [
                                    "fileId" =>$fileId,
                                    "normalSignFieldConfig" => [
                                        "autoSign" =>false,
                                        "psnSealStyles" => '0', //页面可选个人印章样式，默认值0和1, 0 - 手写签名,1 - 姓名印章
                                        "signFieldStyle" =>1, //1 - 单页签章，2 - 骑缝签章
                                        "signFieldPosition" => [
                                            "positionPage" =>"3",
                                            "positionX" =>398.06,
                                            "positionY" =>309.78
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "psnSignerInfo" => [
                                "psnAccount" => $esignAuths->psn_account,
                            ],
                            "noticeConfig" => [
                                "noticeTypes" => "1" //"" - 不进行任何通知，1 - 短信通知，2 - 邮件通知
                            ],
                            "signerType" =>0,
                            "signFields" =>[
                                [
                                    "fileId" =>$fileId,
                                    "normalSignFieldConfig" => [
                                        "autoSign" =>false,
                                        "psnSealStyles" => '0', //页面可选个人印章样式，默认值0和1, 0 - 手写签名,1 - 姓名印章
                                        "signFieldStyle" =>2, //1 - 单页签章，2 - 骑缝签章
                                        "signFieldPosition" => [
                                            "acrossPageMode" => 'ALL',
                                            "positionY" =>428.21
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "signerType" =>1,
                            "signFields" =>[
                                [
                                    "fileId" =>$fileId,
                                    "customBizNum" => date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999)), //自定义业务编号
                                    "normalSignFieldConfig" => [
                                        "autoSign" =>true,
                                        "signFieldStyle" =>1, //1 - 单页签章，2 - 骑缝签章
                                        "signFieldPosition" => [
                                            "positionPage" =>"3",
                                            "positionX" =>185.7,
                                            "positionY" =>301.04
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            "signerType" =>1,
                            "signFields" =>[
                                [
                                    "fileId" =>$fileId,
                                    "customBizNum" => date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999)), //自定义业务编号
                                    "normalSignFieldConfig" => [
                                        "autoSign" =>true,
                                        "signFieldStyle" =>2, //1 - 单页签章，2 - 骑缝签章
                                        "signFieldPosition" => [
                                            "positionY" =>494.17
                                        ]
                                    ]
                                ]
                            ]
                        ],
                    ]
                ];
            }elseif($userType == '2'){

            }

            $paramStr = json_encode($data);
            Log::info( "基于文件发起签署====paramStr:". $paramStr);

            $config = Config::$config;
            $apiaddr = "/v3/sign-flow/create-by-file";
            $requestType = HttpEmun::POST;

            $signAndBuildSignAndJsonHeader = EsignHttpHelper::signAndBuildSignAndJsonHeader($config['eSignAppId'], $config['eSignAppSecret'], $paramStr, $requestType, $apiaddr);
            $response = EsignHttpHelper::doCommHttp($config['eSignHost'], $apiaddr, $requestType, $signAndBuildSignAndJsonHeader, $paramStr);

            Log::info( "基于文件发起签署====body:". $response->getBody());

            if($response->getStatus() == 200){
                $result = json_decode($response->getBody());
                if($result->code==0){
                    $signFlowId = $result->data->signFlowId;
                    Log::info("基于文件发起签署接口调用成功，signFlowId: " . $signFlowId);

                    $esignAuths->update([
                        'file_id' => $fileId,
                        'sign_flow_id' => $signFlowId
                    ]);

                    return json_encode([
                        'status' => '1',
                        'message' => '签署成功'
                    ]);

                }else{
                     Log::info("基于文件发起签署接口调用失败，错误信息: " . $result->message);
                    return json_encode([
                        'status' => '2',
                        'message' => $result->message
                    ]);
                }

            }else{
                 Log::info("基于文件发起签署接口调用失败，HTTP错误码" . $response->getStatus());
                return json_encode([
                    'status' => '2',
                    'message' => $response->getStatus()
                ]);
            }
        }
    }


    /**
     * 查询合同模板中控件详情
     */

    public function templatesTComponents(Request $request){

        $templateId = $this->docTemplateId;

        $config = Config::$config;
        $apiaddr="/v3/doc-templates/".$templateId;
        $requestType = HttpEmun::GET;
        //生成签名验签+json体的header

        $signAndBuildSignAndJsonHeader = EsignHttpHelper::signAndBuildSignAndJsonHeader($config['eSignAppId'], $config['eSignAppSecret'], "", $requestType, $apiaddr);
        $response = EsignHttpHelper::doCommHttp($config['eSignHost'], $apiaddr, $requestType, $signAndBuildSignAndJsonHeader, "");

        Log::info("-----templatesTComponents----Body:" . $response->getBody());

        return json_encode($response->getBody());

    }

    /**
     * 填写pdf模板生成文件
     */
    public function createByDocTemplate($user_id, $templateFileName){

        $esignAuths = EsignAuths::where('user_id', $user_id)->where('user_type', '1')->first();

        $templateId = $this->docTemplateId;
//        $templateFileName = '（'. $esignAuths->user_name .'）'. '易企推APP电子签约条款';

        $year = date('Y', time()); //年
        $month = date('m', time()); //月
        $day = date('d', time()); //日

        $Date = $year . '年' .$month. '月' .$day. '日';

        $config = Config::$config;
        $apiaddr="/v3/files/create-by-doc-template";
        $requestType = HttpEmun::POST;
        $data=[
            "docTemplateId"=>$templateId,
            "fileName"=> $templateFileName,
            "components"=>[
                [
                    "componentKey"=> "userName",
                    "componentValue"=> $esignAuths->user_name
                ],
                [
                    "componentKey"=> "address",
                    "componentValue"=> $esignAuths->user_address
                ],
                [
                    "componentKey"=> "idNumber",
                    "componentValue"=>$esignAuths->id_card
                ],
                [
                    "componentKey"=> "signB",
                    "componentValue"=> $esignAuths->user_name
                ],
                [
                    "componentKey"=> "dateA",
                    "componentValue"=> $Date
                ],
                [
                    "componentKey"=> "dateB",
                    "componentValue"=> $Date
                ],
            ]
        ];
        $paramStr = json_encode($data);
        //生成签名验签+json体的header

        $signAndBuildSignAndJsonHeader = EsignHttpHelper::signAndBuildSignAndJsonHeader($config['eSignAppId'], $config['eSignAppSecret'], $paramStr, $requestType, $apiaddr);
        $response = EsignHttpHelper::doCommHttp($config['eSignHost'], $apiaddr, $requestType, $signAndBuildSignAndJsonHeader, $paramStr);

        Log::info("-----createByDocTemplate----Body:" . $response->getBody());
        $fileId = 'error';

        if(json_decode($response->getBody())->code == '0'){
            $fileId=json_decode($response->getBody())->data->fileId;
        }

        return $fileId;

    }

    /**
     * 下载已签署文件及附属材料
     */
    public function downloadFile(Request $request){

        $user_id = $request->get('user_id', '');
        $sign_flow_id = $request->get('sign_flow_id', '');
        if($user_id == '' || $sign_flow_id == ''){
            return json_encode([
                'status' => '2',
                'message' => '签署失败,获取参数失败'
            ]);
        }

        //检查用户是否签署完成
        $esign = EsignAuths::where('user_id', $user_id)->where('sign_flow_id', $sign_flow_id)->first();
        if($esign){
            $sign_result = $esign->sign_result; //签署结果:2 - 签署完成，3 - 失败，4 - 拒签
            $sign_flow_status = $esign->sign_flow_status; //签署流程最终状态:2 - 已完成 3 - 已撤销 5 - 已过期 7 - 已拒签
            if($sign_result == '2' && $sign_flow_status == '2'){

                $config = Config::$config;
                $apiaddr="/v3/sign-flow/%s/file-download-url";
                $apiaddr = sprintf($apiaddr,$sign_flow_id);
                $requestType = HttpEmun::GET;
                $paramStr = null;

                $signAndBuildSignAndJsonHeader = EsignHttpHelper::signAndBuildSignAndJsonHeader($config['eSignAppId'], $config['eSignAppSecret'], $paramStr, $requestType, $apiaddr);
                $response = EsignHttpHelper::doCommHttp($config['eSignHost'], $apiaddr, $requestType, $signAndBuildSignAndJsonHeader, $paramStr);
                //生成签名验签+json体的header
                if($response->getStatus() == 200){
                    Log::info("下载已签署文件及附属材料调用成功: ".$response->getBody());
                    if(json_decode($response->getBody())->code == '0'){
                        $data = json_decode($response->getBody())->data;

                        $files = $data->files;

                        $fileDownloadUrl = $files[0]->downloadUrl;
                        Log::info("-=======fileDownloadUrl =======: ".$fileDownloadUrl);
                        return json_encode([
                            'status' => '1',
                            'message' => '操作成功',
                            'data' => $fileDownloadUrl
                        ]);
                    }
                }

            }

        }

        return json_encode([
            'status' => '1',
            'message' => '返回成功',
            'data' => null
        ]);

    }



    /**
     * 上传文件即为待签署文件
     */
    public function fileUploadUrl(){

        $filePath= dirname(__FILE__) . $this->filePath;
        Log::info("-----fileUploadUrl----filePath:" . $filePath);
        $contentMd5 = $this->contentMd5($filePath);

        $config = Config::$config;
        $apiaddr = "/v3/files/file-upload-url";
        $requestType = HttpEmun::POST;
        $data=[
            "contentMd5" =>$contentMd5,
            "contentType" =>"application/pdf",
            "convertToPDF" =>false,
            "fileName" =>"易企推APP电子签约条款.pdf",
            "fileSize" =>filesize($filePath)
        ];
        $paramStr = json_encode($data);
        //生成签名验签+json体的header

        $signAndBuildSignAndJsonHeader = EsignHttpHelper::signAndBuildSignAndJsonHeader($config['eSignAppId'], $config['eSignAppSecret'], $paramStr, $requestType, $apiaddr);
        //获取文件上传地址
        $response = EsignHttpHelper::doCommHttp($config['eSignHost'], $apiaddr, $requestType, $signAndBuildSignAndJsonHeader, $paramStr);
        Log::info("-----fileUploadUrl----Body:" . $response->getBody());

        if(json_decode($response->getBody())->code == '0'){
            $fileUploadUrl = json_decode($response->getBody())->data->fileUploadUrl;
            $fileId = json_decode($response->getBody())->data->fileId;
            //文件流put上传
            $response = EsignHttpHelper::upLoadFileHttp($fileUploadUrl,$filePath,"application/pdf");

            return $fileId;
        }else{
            return 'error';
        }
    }

    public function contentMd5($filePath){
        if(!file_exists($filePath)){
            Log::info($filePath."文件不存在");exit;
        }
        return EsignUtilHelper::getContentBase64Md5($filePath);
    }

    /**
     * 获取制作合同模板页面
     */
    public function docTemplateCreateUrl($fileId,$docTemplateType){

        $config = Config::$config;
        $apiaddr="/v3/doc-templates/doc-template-create-url";
        $requestType = HttpEmun::POST;
        $data=[
            "docTemplateName"=>"易企推APP电子签约条款模板",
            "docTemplateType"=>$docTemplateType,
            "fileId"=>$fileId,
            "redirectUrl"=>""
        ];
        $paramStr = json_encode($data);
        //生成签名验签+json体的header
        $signAndBuildSignAndJsonHeader = EsignHttpHelper::signAndBuildSignAndJsonHeader($config['eSignAppId'], $config['eSignAppSecret'], $paramStr, $requestType, $apiaddr);
        $response = EsignHttpHelper::doCommHttp($config['eSignHost'], $apiaddr, $requestType, $signAndBuildSignAndJsonHeader, $paramStr);
        Log::info("-----docTemplateCreateUrl----Body:" . $response->getBody());

        return json_decode($response->getBody());

//        $docTemplateId = "";
//
//        if($response->getStatus() == 200) {
//            $result = json_decode($response->getBody());
//            if ($result->code == 0) {
//                $docTemplateId = $result->data->docTemplateId;
//                $docTemplateCreateUrl = $result->data->docTemplateCreateUrl; //合同模板的页面短链接
//            }
//        }
//
//        return $docTemplateId;

    }

    /**
     * 获取合同文件签署链接
     * @param
     */
    public function getSignUrl($psnId, $signFlowId){

        $config = Config::$config;

        $apiaddr="/v3/sign-flow/%s/sign-url";
        $apiaddr = sprintf($apiaddr,$signFlowId);
        $requestType = HttpEmun::POST;
        $data = [
            "clientType" =>"ALL",
            "needLogin" =>false,
            "operator" => [
                "psnId" => $psnId
            ],
            "urlType" =>2
        ];
        $paramStr = json_encode($data);
        $signAndBuildSignAndJsonHeader = EsignHttpHelper::signAndBuildSignAndJsonHeader($config['eSignAppId'], $config['eSignAppSecret'], $paramStr, $requestType, $apiaddr);
        $response = EsignHttpHelper::doCommHttp($config['eSignHost'], $apiaddr, $requestType, $signAndBuildSignAndJsonHeader, $paramStr);
        Log::info("-----getSignUrl----Body:" . $response->getBody());

        $shortUrl= null;
        if($response->getStatus() == 200){
            $shortUrl =  json_decode($response->getBody())->data->shortUrl;
            Log::info("-----getSignUrl----shortUrl:" . $shortUrl);

        }else{
            Log::info("获取合同文件签署链接接口调用失败，HTTP错误码:".$response->getStatus());
        }

        return $shortUrl;

    }


}