<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/10/20
 * Time: 10:39
 */
namespace App\Api\Controllers\WinCode;


use App\Models\Merchant;
use App\Models\MerchantStore;
use App\Models\WincodeStore;
use App\Models\Store;
use Illuminate\Http\Request;

class StoreController extends BaseController
{
    //新增激活码
    public function add_code(Request $request)
    {
        try {
            $data = $request->all();
            $merchant_id = $request->post('merchant_id', '');
            $store_id = $request->post('store_id', '');

            $check_data = [
                'merchant_id' => '收银员',
                'store_id' => '门店',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $store = Store::where('store_id', $store_id)
                ->select('store_name', 'id')
                ->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在'
                ]);
            }

            $MerchantStore = MerchantStore::where('store_id', $store_id)
                ->where('merchant_id', $merchant_id)
                ->select('id')
                ->first();
            if (!$MerchantStore) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店&收银员不存在'
                ]);
            }

            $merchant_name = Merchant::where('id', $merchant_id)
                ->select('name')
                ->first()
                ->name;

            $insert = [
                'store_id' => $store_id,
                'store_name' => $store->store_name,
                'merchant_id' => $merchant_id,
                'merchant_name' => $merchant_name,
                'device_id' => $this->createDeviceId(6),
                'activation_code' => $this->createDeviceToken(7),
                'appid' => $this->appid,
                'secret' => $this->winsecret
            ];
            $res = WincodeStore::create($insert);
            if ($res) {
                return json_encode([
                    'status' => 1,
                    'message' => '添加成功',
                    'data' => $insert
                ]);
            } else {
                return json_encode([
                    'status' => 1,
                    'message' => '添加成功',
                    'data' => $insert
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => -1,
                'message' => $ex->getMessage().' | '.$ex->getLine()
            ]);
        }
    }


    //激活码列表
    public function code_list(Request $request)
    {
        try {
            //获取请求参数
            $data = $request->all();

            $check_data = [
                'store_id' => '门店',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $store = Store::where('store_id', $data['store_id'])
                ->select('store_name')
                ->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在'
                ]);
            }

            $obj = WincodeStore::where('store_id', $data['store_id'])
                ->orderBy('created_at', 'desc');
            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $ex) {
            return json_encode([
                'status' => -1,
                'message' => $ex->getMessage().' | '.$ex->getLine()
            ]);
        }
    }


    //删除激活码
    public function del_code(Request $request)
    {
        try {
            //获取请求参数
            $public = $this->parseToken();
            $data = $request->all();
            $device_id = $request->get('device_id');
            $store_id = $request->get('store_id');
            $merchant_id = $request->get('merchant_id');

            $check_data = [
                'merchant_id' => '收银员',
                'store_id' => '门店',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

//            if (1) {
//                return json_encode([
//                    'status' => 2,
//                    'message' => '没有权限删除'
//                ]);
//            }

            $store = Store::where('store_id', $data['store_id'])
                ->select('id')
                ->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在'
                ]);
            }

            $res = WincodeStore::where('device_id', $device_id)
                ->where('store_id', $store_id)
                ->where('merchant_id', $merchant_id)
                ->delete();
            if ($res) {
                return json_encode([
                    'status' => 1,
                    'message' => '删除成功',
                ]);
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '删除失败',
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => -1,
                'message' => $ex->getMessage().' | '.$ex->getLine()
            ]);
        }
    }


}
