<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/10/20
 * Time: 10:39
 */

namespace App\Api\Controllers\WinCode;


use Alipayopen\Sdk\AopClient;
use Alipayopen\Sdk\Request\AlipayTradePayRequest;
use App\Api\Controllers\Config\AlipayIsvConfigController;
use App\Api\Controllers\Config\AllinPayConfigController;
use App\Api\Controllers\Config\CcBankPayConfigController;
use App\Api\Controllers\Config\EasyPayConfigController;
use App\Api\Controllers\Config\EasySkPayConfigController;
use App\Api\Controllers\Config\FuiouConfigController;
use App\Api\Controllers\Config\NewLandConfigController;
use App\Api\Controllers\Config\PayWaysController;
use App\Api\Controllers\Config\PostPayConfigController;
use App\Api\Controllers\Config\WeixinConfigController;
use App\Api\Controllers\Config\YinshengConfigController;
use App\Common\PaymentChannelsPlugInAction;
use App\Common\PaySuccessAction;
use App\Models\AlipayAccount;
use App\Models\Store;
use EasyWeChat\Factory;
use Illuminate\Support\Facades\Log;

class TradepayTwoController extends \App\Api\Controllers\BaseController
{

    //收钱啦 付款码支付
    public function scan_pay($data)
    {
        try {
            $store_id = $data['store_id'];
            $code = $data['code'];
            $total_amount = $data['total_amount'];
            $shop_price = $data['shop_price'];
            $remark = $data['remark'];
            //$device_id = $data['device_id'];
            $device_id = '收钱啦内插';
            $shop_name = $data['shop_name'];
            $shop_desc = $data['shop_desc'];
            $qwx_no = $data['qwx_no'];
            $pay_method = isset($data['pay_method']) ? $data['pay_method'] : "qr_code";
            $out_trade_no = isset($data['out_trade_no']) ? $data['out_trade_no'] : "";
            $merchant_id = isset($data['merchant_id']) ? $data['merchant_id'] : "";
            $merchant_name = isset($data['merchant_name']) ? $data['merchant_name'] : "";

            $store = Store::where('store_id', $store_id)
                ->select('config_id', 'store_name', 'pid', 'user_id', 'people_phone', 'source')
                ->first();

            if ($device_id != 'app') {
                if (empty($merchant_id)) {
                    $merchant_id = "1";
                    $merchant_name = "收银员";
                }
            }

            $config_id = $store->config_id;
            $store_name = $store->store_name;
            $store_pid = $store->pid;
            $tg_user_id = $store->user_id;
            $source = $store->source;//来源,01:畅立收，02:河南畅立收

            //插入数据库
            $data_insert = [
                'trade_no' => '',
                'qwx_no' => $qwx_no,
                'user_id' => $tg_user_id,
                'store_id' => $store_id,
                'store_name' => $store_name,
                'buyer_id' => '',
                'total_amount' => $total_amount,
                'pay_amount' => $total_amount,
                'shop_price' => $shop_price,
                'payment_method' => '',
                'status' => '',
                'pay_status' => 2,
                'pay_status_desc' => '等待支付',
                'merchant_id' => $merchant_id,
                'merchant_name' => $merchant_name,
                'remark' => $remark,
                'device_id' => $device_id,
                'config_id' => $config_id,
                'pay_method' => $pay_method,
                'auth_code' => $code,
                'source' => $source,
                'device_name'=>$device_id
            ];

            $paymentPlugIn = new PaymentChannelsPlugInAction();

            $str = substr($code, 0, 2);

            /**支付宝渠道开始**/
            if ($str == "28") {
                //读取优先为高级的通道
                $obj_ways = new PayWaysController();
                $ways = $obj_ways->ways_source('alipay', $store_id, $store_pid);
                if (!$ways) {
                    $msg = '此类型通道没有开通';
                    return [
                        'status' => 2,
                        'message' => $msg
                    ];
                }

                if (empty($out_trade_no)) {
                    $out_trade_no = 'aliWft' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                }

                $user_rate_obj = $obj_ways->getCostRate($ways->ways_type, $tg_user_id);
                $cost_rate = $user_rate_obj ? $user_rate_obj->rate : 0.00; //成本费率

                if ($cost_rate) $data_insert ['cost_rate'] = $cost_rate;

                $data_insert['rate'] = $ways->rate;
                $data_insert['fee_amount'] = $ways->rate * $total_amount / 100;

                //官方支付宝扫一扫
                if ($ways && $ways->ways_type == 1000) {
                    $data['out_trade_no'] = $out_trade_no;
                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = 'alipay';
                    $data_insert['ways_source_desc'] = '支付宝';

                    $config_type = '01';

                    //配置
                    $isvconfig = new AlipayIsvConfigController();
                    $storeInfo = $isvconfig->alipay_auth_info($store_id, $store_pid);
                    $out_user_id = $storeInfo->user_id;//商户的id
                    $alipay_store_id = $storeInfo->alipay_store_id;

                    //分成模式 服务商
                    if ($storeInfo->settlement_type == "set_a") {
                        if ($storeInfo->config_type == '02') {
                            $config_type = '02';
                        }
                        $storeInfo = AlipayAccount::where('config_id', $config_id)
                            ->where('config_type', $config_type)
                            ->first();//服务商的
                    }
                    if (!$storeInfo) {
                        $msg = '支付宝授权信息不存在';
                        return [
                            'status' => 2,
                            'message' => $msg
                        ];

                    }
                    $app_auth_token = $storeInfo->app_auth_token;
                    $out_store_id = $storeInfo->out_store_id;

                    $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);

                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    $notify_url = url('/api/alipayopen/qr_pay_notify');
                    $disable_pay_channels = ''; //仅用方式
                    $aop = new AopClient();
                    $aop->apiVersion = "2.0";
                    $aop->appId = $config->app_id;
                    $aop->rsaPrivateKey = $config->rsa_private_key;
                    $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                    $aop->method = 'alipay.trade.pay';
                    // $aop->notify_url = $notify_url;
                    $aop->signType = "RSA2";//升级算法
                    $aop->gatewayUrl = $config->alipay_gateway;
                    $aop->format = "json";
                    $aop->charset = "GBK";
                    $aop->version = "2.0";

                    $requests = new AlipayTradePayRequest();
                    // $requests->setNotifyUrl($notify_url);
                    //提交到支付宝
                    if ($alipay_store_id) {
                        $data_re = "{" .
                            "\"out_trade_no\":\"" . $out_trade_no . "\"," .
                            "\"seller_id\":\"" . $out_user_id . "\"," .//商户收款账号
                            "\"disable_pay_channels\":\"" . $disable_pay_channels . "\"," .
                            "    \"scene\":\"bar_code\"," .
                            "    \"auth_code\":\"" . $code . "\"," .
                            "    \"subject\":\"" . $shop_name . "\"," .
                            "    \"total_amount\":" . $total_amount . "," .
                            "    \"timeout_express\":\"90m\"," .
                            "    \"body\":\"" . $shop_desc . "\"," .
                            "      \"goods_detail\":[{" .
                            "        \"goods_id\":\"" . $store_id . "\"," .
                            "        \"goods_name\":\"" . $shop_name . "\"," .
                            "        \"quantity\":1," .
                            "        \"price\":" . $total_amount . "," .
                            "        \"body\":\"" . $shop_name . "\"" .
                            "        }]," .
                            "    \"store_id\":\"" . $out_store_id . "\"," .
                            "    \"shop_id\":\"" . $alipay_store_id . "\"," .
                            "    \"terminal_id\":\"" . $device_id . "\"," .
                            "    \"operator_id\":\"D_001_" . $merchant_id . "\"," .
                            "    \"extend_params\":{" .
                            "      \"sys_service_provider_id\":\"" . $config->alipay_pid . "\"" .
                            "}" .
                            "  }";
                    } else {
                        $data_re = "{" .
                            "\"out_trade_no\":\"" . $out_trade_no . "\"," .
                            "\"seller_id\":\"" . $out_user_id . "\"," .//商户收款账号
                            "\"disable_pay_channels\":\"" . $disable_pay_channels . "\"," .
                            "    \"scene\":\"bar_code\"," .
                            "    \"auth_code\":\"" . $code . "\"," .
                            "    \"subject\":\"" . $shop_name . "\"," .
                            "    \"total_amount\":" . $total_amount . "," .
                            "    \"timeout_express\":\"90m\"," .
                            "    \"body\":\"" . $shop_desc . "\"," .
                            "      \"goods_detail\":[{" .
                            "        \"goods_id\":\"" . $store_id . "\"," .
                            "        \"goods_name\":\"" . $shop_name . "\"," .
                            "        \"quantity\":1," .
                            "        \"price\":" . $total_amount . "," .
                            "        \"body\":\"" . $shop_name . "\"" .
                            "        }]," .
                            "    \"store_id\":\"" . $store_id . "\"," .
                            "    \"terminal_id\":\"" . $device_id . "\"," .
                            "    \"operator_id\":\"D_001_" . $merchant_id . "\"," .
                            "    \"extend_params\":{" .
                            "      \"sys_service_provider_id\":\"" . $config->alipay_pid . "\"" .
                            "}" .
                            "  }";
                    }
                    $requests->setBizContent($data_re);
                    $result = $aop->execute($requests, null, $app_auth_token);

                    $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
                    $resultCode = $result->$responseNode->code;

                    //异常
                    if ($resultCode == 40004) {
                        return json_encode([
                            'status' => 2,
                            'message' => $result->$responseNode->msg . $result->$responseNode->sub_code,
                            'result_code' => $resultCode,
                        ]);
                    }

                    //支付成功
                    if (!empty($resultCode) && $resultCode == 10000) {
                        $buyer_id = $result->$responseNode->buyer_user_id;
                        $buyer_logon_id = $result->$responseNode->buyer_logon_id;
                        $payment_method = $result->$responseNode->fund_bill_list[0]->fund_channel;
                        $trade_no = $result->$responseNode->trade_no;
                        $gmt_payment = $result->$responseNode->gmt_payment;
                        $buy_pay_amount = $result->$responseNode->buyer_pay_amount;

                        $up_data = [
                            'trade_no' => $trade_no,
                            'buyer_id' => $buyer_id,
                            'buyer_logon_id' => $buyer_logon_id,
                            'status' => 'TRADE_SUCCESS',
                            'pay_status_desc' => '支付成功',
                            'pay_status' => 1,
                            'payment_method' => $payment_method,
                            'pay_time' => $gmt_payment,
                            'buyer_pay_amount' => $buy_pay_amount,
                        ];
                        $this->update_day_order($up_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $ways->ways_type,
                            'ways_type_desc' => $ways->ways_desc,
                            'company' => $ways->company,
                            'source_type' => '1000',//返佣来源
                            'source_desc' => '支付宝',//返佣来源说明
                            'total_amount' => $total_amount,
                            'out_trade_no' => $out_trade_no,
                            'rate' => $data_insert['rate'],
                            'merchant_id' => $merchant_id,
                            'store_id' => $store_id,
                            'user_id' => $tg_user_id,
                            'config_id' => $config_id,
                            'store_name' => $store_name,
                            'ways_source' => $ways->ways_source,
                            'fee_amount' => $data_insert['fee_amount'],
                            'pay_time' => $gmt_payment,
                            'no_push' => '1',//不推送
                            'no_fuwu' => '1',//不服务消息
//                            'no_print' => '1',//不打印
                            //'no_v' => '1',//不小盒子播报

                        ];
                        PaySuccessAction::action($data);

                        return json_encode([
                            'status' => 1,
                            'message' => '支付成功',
                            'data' => [
                                'out_trade_no' => $out_trade_no,
                                'out_transaction_id' => $out_trade_no,
                                'total_amount' => $total_amount,
                                'store_name' => $store_name,
                                'trade_no' => $trade_no,
                                'pay_time' => $gmt_payment,
                            ]
                        ]);
                    }

                    //正在支付
                    if (!empty($resultCode) && $resultCode == 10003) {
                        return json_encode([
                            'status' => 9,
                            'message' => '等待用户支付',
                            'data' => [
                                'out_trade_no' => $out_trade_no,
                                'total_amount' => $total_amount,
                                'store_name' => $store_name,
                            ]
                        ]);
                    }

                    $msg = $result->$responseNode->sub_msg; //错误信息
                    return json_encode([
                        'status' => 2,
                        'message' => $msg
                    ]);
                }

                //京东收银支付宝扫一扫
                if ($ways && $ways->ways_type == 6001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $store->user_id,
                    ];
                    return $paymentPlugIn->jd_pay_public($data_insert, $data_public);
                }

                //联拓富收银支付宝扫一扫
                if ($ways && $ways->ways_type == 10001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $store->user_id,
                    ];
                    return $paymentPlugIn->ltf_pay_public($data_insert, $data_public);
                }

                //和融通收银支付宝
                if ($ways && $ways->ways_type == 9001) {
                    $data_public = [
                        'pay_type' => 'ali',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $store->user_id,
                    ];
                    return $paymentPlugIn->h_pay_public($data_insert, $data_public);
                }

                //快钱支付宝支付宝扫一扫
                if ($ways && $ways->ways_type == 3001) {
                    $data_public = [
                        'pay_type' => 'alipay',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->mybank_pay_public($data_insert, $data_public);
                }

                //新大陆支付宝
                if ($ways && $ways->ways_type == 8001) {
                    $config = new NewLandConfigController();
                    $new_land_config = $config->new_land_config($config_id);
                    if (!$new_land_config) {
                        return json_encode([
                            'status' => 2,
                            'message' => '新大陆配置不存在请检查配置'
                        ]);
                    }

                    $new_land_merchant = $config->new_land_merchant($store_id, $store_pid);
                    if (!$new_land_merchant) {
                        return json_encode([
                            'status' => 2,
                            'message' => '商户新大陆通道未开通'
                        ]);
                    }
                    $request_data = [
                        'out_trade_no' => $out_trade_no,
                        'code' => $code,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'key' => $new_land_merchant->nl_key,
                        'org_no' => $new_land_config->org_no,
                        'merc_id' => $new_land_merchant->nl_mercId,
                        'trm_no' => $new_land_merchant->trmNo,
                        'op_sys' => '3',
                        'opr_id' => $merchant_id,
                        'trm_typ' => 'T',
                        'payChannel' => 'ALIPAY',
                    ];

                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '支付宝';
                    $data_insert['ways_source'] = $ways->ways_source;
                    $data_insert['ways_source_desc'] = '支付宝';
                    $data_insert['company'] = $ways->company;

                    return $paymentPlugIn->newland_pay_public($data_insert, $request_data);
                }

                //随行付支付宝
                if ($ways && $ways->ways_type == 13001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->vbill_pay_public($data_insert, $data_public);
                }

                //随行付A支付宝
                if ($ways && $ways->ways_type == 19001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->vbilla_pay_public($data_insert, $data_public);
                }

                //哆啦宝支付宝
                if ($ways && $ways->ways_type == 15001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->dlb_pay_public($data_insert, $data_public);
                }

                //传化支付宝
                if ($ways && $ways->ways_type == 12001) {
                    $data_public = [
                        'channel' => 'ALIPAY_POS',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->tf_pay_public($data_insert, $data_public);
                }

                //汇付支付宝
                if ($ways && $ways->ways_type == 18001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->huipay_pay_public($data_insert, $data_public);
                }

                //工行支付宝
                if ($ways && $ways->ways_type == 20001) {
                    $data_public = [
                        'payment' => '2',
                        'goodsId' => time(),
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->lianfu_pay_public($data_insert, $data_public);
                }

                //海科融通 支付宝 被扫
                if ($ways && $ways->ways_type == 22001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->hkrt_pay_public($data_insert, $data_public);
                }

                //易生支付 支付宝 被扫
                if ($ways && $ways->ways_type == 21001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->easypay_pay_public($data_insert, $data_public);
                }

                //联动优势 支付宝 被扫
                if ($ways && $ways->ways_type == 5001) {
                    $data_public = [
                        'orderType' => 'alipay',
                        'goodsId' => time(),
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id
                    ];
                    return $paymentPlugIn->linkage_pay_public($data_insert, $data_public);
                }

                //威富通 支付宝
                if ($ways && $ways->ways_type == 27001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->wft_pay_public($data_insert, $data_public);
                }

                //汇旺财 支付宝
                if ($ways && $ways->ways_type == 28001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->hwc_pay_public($data_insert, $data_public);
                }

                //邮驿付支付 支付宝 被扫
                if ($ways && $ways->ways_type == 29001) {
                    if (strlen($out_trade_no) > 20) {
                        $out_trade_no = substr($out_trade_no, 0, 15) . substr(microtime(), 2, 5);
                    }
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                        'phone' => $store->people_phone
                    ];
                    return $paymentPlugIn->post_pay_public($data_insert, $data_public);
                }

                //建设银行 支付宝
                if ($ways && $ways->ways_type == 31001) {
                    if (strlen($out_trade_no) > 30) {
                        $out_trade_no = substr($out_trade_no, 0, 25) . substr(microtime(), 2, 5);
                    }
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->ccBank_pay_public($data_insert, $data_public);
                }

                //河南畅立收
                if ($source == '02') {
                    //易生数科支付 支付宝 被扫
                    if ($ways && $ways->ways_type == 32001) {
                        $data_public = [
                            'pay_type' => 'ALIPAY',
                            'return_params' => '原样返回',
                            'title' => '支付宝收款',
                            'ways_source_desc' => '支付宝',
                            'ways_type_desc' => '支付宝',
                            'code' => $code,
                            'out_trade_no' => $out_trade_no,
                            'config_id' => $config_id,
                            'store_id' => $store_id,
                            'store_pid' => $store_pid,
                            'ways_type' => '' . $ways->ways_type . '',
                            'ways_source' => $ways->ways_source,
                            'company' => $ways->company,
                            'total_amount' => $total_amount,
                            'remark' => $remark,
                            'device_id' => $device_id,
                            'shop_name' => $shop_name,
                            'merchant_id' => $merchant_id,
                            'store_name' => $store_name,
                            'tg_user_id' => $tg_user_id,
                        ];
                        return $paymentPlugIn->easyskpay_pay_public($data_insert, $data_public);
                    }
                }

                //通联支付 支付宝 被扫
                if ($ways && $ways->ways_type == 33001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->allin_pay_public($data_insert, $data_public);
                }

                //银盛 支付宝 被扫
                if ($ways && $ways->ways_type == 14001) {
                    $data_public = [
                        'pay_type' => 'ALIPAY',
                        'return_params' => '原样返回',
                        'title' => '支付宝收款',
                        'ways_source_desc' => '支付宝',
                        'ways_type_desc' => '支付宝',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                        'phone' => $store->people_phone
                    ];
                    return $paymentPlugIn->yinsheng_pay_public($data_insert, $data_public);
                }

            }
            /**支付宝渠道结束**/

            //微信渠道
            if ($str == "13" || $str == "14") {
                //读取优先为高级的通道
                $obj_ways = new PayWaysController();
                $ways = $obj_ways->ways_source('weixin', $store_id, $store_pid);
                if (!$ways) {
                    $msg = '此类型通道没有开通';
                    return [
                        'status' => 2,
                        'message' => $msg
                    ];
                }

                $user_rate_obj = $obj_ways->getCostRate($ways->ways_type, $tg_user_id);
                $cost_rate = $user_rate_obj ? $user_rate_obj->rate : 0.00; //成本费率

                if ($cost_rate) $data_insert ['cost_rate'] = $cost_rate;
                $data_insert['rate'] = $ways->rate;
                $data_insert['fee_amount'] = $ways->rate * $total_amount / 100;

                if (empty($out_trade_no)) {
                    $out_trade_no = 'wxWft' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                }

                //官方微信扫一扫
                if ($ways && $ways->ways_type == 2000) {
                    $config = new WeixinConfigController();
                    $options = $config->weixin_config($config_id);
                    $weixin_store = $config->weixin_merchant($store_id, $store_pid);
                    if (!$weixin_store) {
                        return json_encode([
                            'status' => 2,
                            'message' => '微信商户号不存在'
                        ]);
                    }
                    $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;

                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = 'weixin';
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_source_desc'] = '微信支付';

                    $type = 2001;
                    $attach = $store_id . ',' . $config_id; //附加信息原样返回
                    $goods_detail = [];

                    //入库参数
                    $insert_re = $this->insert_day_order($data_insert);
                    if (!$insert_re) {
                        return json_encode([
                            'status' => 2,
                            'message' => '订单未入库'
                        ]);
                    }

                    $config = [
                        'app_id' => $options['app_id'],
                        'mch_id' => $options['payment']['merchant_id'],
                        'key' => $options['payment']['key'],
                        'cert_path' => $options['payment']['cert_path'], // XXX: 绝对路径！！！！
                        'key_path' => $options['payment']['key_path'],     // XXX: 绝对路径！！！！
                        'sub_mch_id' => $wx_sub_merchant_id,
                    ];
                    $payment = Factory::payment($config);

                    if ($weixin_store->wx_shop_id && $weixin_store->wx_shop_id) {
//                        $goods_detail = [
//                            'goods_detail' => [
//                                [
//                                    'goods_id' => $weixin_store->wx_shop_id,
//                                    'quantity' => 1,
//                                    'price' => $total_amount * 100
//                                ]
//                            ]
//                        ];

                        $wx_shop_id = $weixin_store->wx_shop_id;
                        $wx_shop_id = explode(',', $wx_shop_id);
                        $goods_detail = [];
                        foreach ($wx_shop_id as $value) {
                            $goods_detail[] = [
                                'goods_id' => $value,
                                'quantity' => 1,
                                'price' => $total_amount * 100
                            ];
                        }

                        $goods_detail = [
                            'goods_detail' => $goods_detail
                        ];

                        $attributes = [
                            'version' => '1.0',
                            'body' => $shop_name,
                            'detail' => json_encode($goods_detail),
                            'out_trade_no' => $out_trade_no,
                            'total_fee' => $total_amount * 100,
                            'auth_code' => $code,
                            'attach' => $attach,//原样返回
                            'device_info' => $device_id,
                        ];
                    } else {
                        $attributes = [
                            'body' => $shop_name,
                            'detail' => $shop_desc,
                            'out_trade_no' => $out_trade_no,
                            'total_fee' => $total_amount * 100,
                            'auth_code' => $code,
                            'attach' => $attach,//原样返回
                            'device_info' => $device_id,
                            'goods_detail' => $goods_detail,//交易详细数据
                        ];
                    }

                    $result = $payment->pay($attributes);

                    //请求状态
                    if ($result['return_code'] == 'SUCCESS') {
                        //支付成功
                        if ($result['result_code'] == 'SUCCESS') {
                            $data_update = [
                                'receipt_amount' => 0,//商家实际收到的款项
                                'status' => $result['result_code'],
                                'pay_status' => 1,//系统状态
                                'pay_status_desc' => '支付成功',
                                'payment_method' => $result['bank_type'],
                                'buyer_id' => $result['openid'],
                                'trade_no' => $result['transaction_id'],
                            ];
                            $this->update_day_order($data_update, $out_trade_no);

                            if (!$insert_re) {
                                return json_encode([
                                    'status' => 2,
                                    'message' => '订单未入库'
                                ]);
                            }

                            //支付成功后的动作
                            $data = [
                                'ways_type' => $ways->ways_type,
                                'ways_type_desc' => $ways->ways_desc,
                                'company' => $ways->company,
                                'source_type' => '2000',//返佣来源
                                'source_desc' => '微信支付',//返佣来源说明
                                'total_amount' => $total_amount,
                                'out_trade_no' => $out_trade_no,
                                'rate' => $data_insert['rate'],
                                'fee_amount' => $data_insert['fee_amount'],
                                'merchant_id' => $merchant_id,
                                'store_id' => $store_id,
                                'user_id' => $tg_user_id,
                                'config_id' => $config_id,
                                'store_name' => $store_name,
                                'ways_source' => $ways->ways_source,
                                'pay_time' => $result['time_end'],
                                'no_push' => '1',//不推送
                                'no_fuwu' => '1',//不服务消息
//                                'no_print' => '1',//不打印
                                //'no_v' => '1',//不小盒子播报

                            ];
                            PaySuccessAction::action($data);

                            return json_encode([
                                'status' => 1,
                                'data' => [
                                    'out_trade_no' => $out_trade_no,
                                    'store_name' => $store_name,
                                    'trade_no' => $result['transaction_id'],
                                    'total_amount' => $total_amount,
                                    'pay_time' => $result['time_end'],
                                    'out_transaction_id' => $out_trade_no,
                                ]
                            ]);
                        } else {
                            if ($result['err_code'] == "USERPAYING") {
                                return json_encode([
                                    'status' => 9,
                                    'message' => '等待用户支付',
                                    'data' => [
                                        'out_trade_no' => $out_trade_no,
                                        'total_amount' => $total_amount,
                                        'store_name' => $store_name,
                                    ]
                                ]);
                            } else {
                                $msg = $result['err_code_des'];//错误信息
                                return json_encode([
                                    'status' => 2,
                                    'message' => $msg,
                                ]);
                            }
                        }
                    } else {
                        $data = [
                            'status' => 2,
                            "message" => $result['return_msg'],
                        ];
                    }

                    return json_encode($data);
                }

                //京东收银-微信
                if ($ways && $ways->ways_type == 6002) {
                    $data_public = [
                        'pay_type' => 'WX',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $store->user_id,
                    ];
                    return $paymentPlugIn->jd_pay_public($data_insert, $data_public);
                }

                //联拓富收银-微信
                if ($ways && $ways->ways_type == 10002) {
                    $data_public = [
                        'pay_type' => 'WX',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $store->user_id,
                    ];
                    return $paymentPlugIn->ltf_pay_public($data_insert, $data_public);
                }

                //快钱微信支付扫一扫
                if ($ways && $ways->ways_type == 3002) {
                    $data_public = [
                        'pay_type' => 'weixin',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->mybank_pay_public($data_insert, $data_public);
                }

                //和融通-微信
                if ($ways && $ways->ways_type == 9002) {
                    $data_public = [
                        'pay_type' => 'wx',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $store->user_id,
                    ];
                    return $paymentPlugIn->h_pay_public($data_insert, $data_public);
                }

                //新大陆微信
                if ($ways && $ways->ways_type == 8002) {
                    $config = new NewLandConfigController();
                    $new_land_config = $config->new_land_config($config_id);
                    if (!$new_land_config) {
                        return json_encode([
                            'status' => 2,
                            'message' => '新大陆配置不存在请检查配置'
                        ]);
                    }

                    $mybank_merchant = $config->new_land_merchant($store_id, $store_pid);
                    if (!$mybank_merchant) {
                        return json_encode([
                            'status' => 2,
                            'message' => '商户新大陆通道未开通'
                        ]);
                    }
                    $request_data = [
                        'out_trade_no' => $out_trade_no,
                        'code' => $code,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'key' => $mybank_merchant->nl_key,
                        'org_no' => $new_land_config->org_no,
                        'merc_id' => $mybank_merchant->nl_mercId,
                        'trm_no' => $mybank_merchant->trmNo,
                        'op_sys' => '3',
                        'opr_id' => $merchant_id,
                        'trm_typ' => 'T',
                        'payChannel' => 'WXPAY',
                    ];

                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_type_desc'] = '微信支付';
                    $data_insert['ways_source'] = $ways->ways_source;
                    $data_insert['ways_source_desc'] = '微信支付';

                    return $paymentPlugIn->newland_pay_public($data_insert, $request_data);
                }

                //随行付-微信
                if ($ways && $ways->ways_type == 13002) {
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->vbill_pay_public($data_insert, $data_public);
                }

                //随行付a-微信
                if ($ways && $ways->ways_type == 19002) {
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->vbilla_pay_public($data_insert, $data_public);
                }

                //哆啦宝-微信
                if ($ways && $ways->ways_type == 15002) {
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->dlb_pay_public($data_insert, $data_public);
                }

                //传化微信
                if ($ways && $ways->ways_type == 12002) {
                    $data_public = [
                        'channel' => 'WECHAT_POS',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->tf_pay_public($data_insert, $data_public);
                }

                //汇付-微信
                if ($ways && $ways->ways_type == 18002) {
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->huipay_pay_public($data_insert, $data_public);
                }

                //工行微信
                if ($ways && $ways->ways_type == 20002) {
                    $data_public = [
                        'payment' => '1',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'other_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id
                    ];
                    return $paymentPlugIn->lianfu_pay_public($data_insert, $data_public);
                }

                //海科融通 微信 被扫
                if ($ways && $ways->ways_type == 22002) {
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->hkrt_pay_public($data_insert, $data_public);
                }

                //易生支付 微信 被扫
                if ($ways && $ways->ways_type == 21002) {
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->easypay_pay_public($data_insert, $data_public);
                }

                //联动优势 微信 被扫
                if ($ways && $ways->ways_type == 5002) {
                    $data_public = [
                        'orderType' => 'wechat',
                        'goodsId' => time(),
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id
                    ];
                    return $paymentPlugIn->linkage_pay_public($data_insert, $data_public);
                }

                //威富通 收钱啦 微信 被扫
                if ($ways && $ways->ways_type == 27002) {
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->wft_pay_public($data_insert, $data_public);
                }

                //汇旺财 收钱啦 微信 被扫
                if ($ways && $ways->ways_type == 28002) {
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->hwc_pay_public($data_insert, $data_public);
                }

                //邮驿付支付 微信 被扫
                if ($ways && $ways->ways_type == 29002) {
                    if (strlen($out_trade_no) > 20) {
                        $out_trade_no = substr($out_trade_no, 0, 15) . substr(microtime(), 2, 5);
                    }

                    $data_public = [
                        'pay_type' => 'WXPAY',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                        'phone' => $store->people_phone
                    ];

                    return $paymentPlugIn->post_pay_public($data_insert, $data_public);
                }

                //建设银行 微信
                if ($ways && $ways->ways_type == 31002) {
                    if (strlen($out_trade_no) > 30) {
                        $out_trade_no = substr($out_trade_no, 0, 25) . substr(microtime(), 2, 5);
                    }
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->ccBank_pay_public($data_insert, $data_public);
                }

                //河南畅立收
                if ($source == '02') {
                    //易生支付 微信 被扫
                    if ($ways && $ways->ways_type == 32002) {
                        $data_public = [
                            'pay_type' => 'WECHAT',
                            'return_params' => '原样返回',
                            'title' => '微信收款',
                            'ways_source_desc' => '微信支付',
                            'ways_type_desc' => '微信支付',
                            'code' => $code,
                            'out_trade_no' => $out_trade_no,
                            'config_id' => $config_id,
                            'store_id' => $store_id,
                            'store_pid' => $store_pid,
                            'ways_type' => '' . $ways->ways_type . '',
                            'ways_source' => $ways->ways_source,
                            'company' => $ways->company,
                            'total_amount' => $total_amount,
                            'remark' => $remark,
                            'device_id' => $device_id,
                            'shop_name' => $shop_name,
                            'merchant_id' => $merchant_id,
                            'store_name' => $store_name,
                            'tg_user_id' => $tg_user_id,
                        ];
                        return $paymentPlugIn->easyskpay_pay_public($data_insert, $data_public);
                    }
                }

                //通联支付 微信 被扫
                if ($ways && $ways->ways_type == 33002) {
                    $data_public = [
                        'pay_type' => 'WECHAT',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];
                    return $paymentPlugIn->allin_pay_public($data_insert, $data_public);
                }

                //银盛 微信 被扫
                if ($ways && $ways->ways_type == 14002) {

                    $data_public = [
                        'pay_type' => 'WXPAY',
                        'return_params' => '原样返回',
                        'title' => '微信收款',
                        'ways_source_desc' => '微信支付',
                        'ways_type_desc' => '微信支付',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                        'phone' => $store->people_phone
                    ];

                    return $paymentPlugIn->yinsheng_pay_public($data_insert, $data_public);
                }



            }
            //银联渠道
            if ($str == "62") {
                $obj_ways = new PayWaysController();
                $ways = $obj_ways->ways_source_un_qr('unionpay', $store_id, $store_pid);
                if (!$ways) {
                    return json_encode([
                        'status' => 2,
                        'message' => '没有开通此类型通道'
                    ]);
                }

                if ($ways->is_close) {
                    return json_encode([
                        'status' => 2,
                        'message' => '此通道已关闭'
                    ]);
                }
                $user_rate_obj = $obj_ways->getCostRate($ways->ways_type, $tg_user_id);
                $cost_rate = $user_rate_obj ? $user_rate_obj->rate : 0.00; //成本费率

                if ($cost_rate) $data_insert ['cost_rate'] = $cost_rate;
                $data_insert['rate'] = $ways->rate;
                $data_insert['fee_amount'] = $ways->rate * $total_amount / 100;

                if (empty($out_trade_no)) {
                    $out_trade_no = 'unWft' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                }
                //易生支付 银联扫码
                if ($ways && $ways->ways_type == 21004) {
                    $config = new EasyPayConfigController();
                    $easypay_config = $config->easypay_config($config_id);
                    if (!$easypay_config) {
                        return json_encode([
                            'status' => '2',
                            'message' => '易生支付配置不存在请检查配置'
                        ]);
                    }

                    $easypay_merchant = $config->easypay_merchant($store_id, $store_pid);
                    if (!$easypay_merchant) {
                        return json_encode([
                            'status' => '2',
                            'message' => '商户易生支付通道未开通'
                        ]);
                    }

                    if ($total_amount < 1000) {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_a;
                        $data_insert['fee_amount'] = $ways->rate_a * $total_amount / 100;
                    } else {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_c;
                        $data_insert['fee_amount'] = $ways->rate_c * $total_amount / 100;
                    }

                    $request_data = [
                        'pay_type' => 'UNIONPAY',
                        'return_params' => '原样返回',
                        'title' => '银联扫码收款',
                        'ways_source_desc' => '银联扫码',
                        'ways_type_desc' => '银联扫码',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_source'] = $ways->ways_source;
                    $data_insert['ways_source_desc'] = '银联扫码';

                    return $paymentPlugIn->easypay_pay_public($data_insert, $request_data);
                }

                //邮驿付支付 银联扫码
                if ($ways && $ways->ways_type == 29003) {
                    $config = new PostPayConfigController();
                    $post_config = $config->post_pay_config($config_id);
                    if (!$post_config) {
                        return json_encode([
                            'status' => '2',
                            'message' => '邮驿付支付配置不存在请检查配置'
                        ]);
                    }

                    $post_merchant = $config->post_pay_merchant($store_id, $store_pid);
                    if (!$post_merchant) {
                        return json_encode([
                            'status' => '2',
                            'message' => '商户邮驿付支付通道未开通'
                        ]);
                    }

                    if ($total_amount < 1000) {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_a;
                        $data_insert['fee_amount'] = $ways->rate_a * $total_amount / 100;
                    } else {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_c;
                        $data_insert['fee_amount'] = $ways->rate_c * $total_amount / 100;
                    }
                    if (strlen($out_trade_no) > 20) {
                        $out_trade_no = substr($out_trade_no, 0, 15) . substr(microtime(), 2, 5);
                    }
                    $request_data = [
                        'pay_type' => 'YLPAY',
                        'return_params' => '原样返回',
                        'title' => '银联扫码收款',
                        'ways_source_desc' => '银联扫码',
                        'ways_type_desc' => '银联扫码',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                        'phone' => $store->people_phone
                    ];

                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_source'] = $ways->ways_source;
                    $data_insert['ways_source_desc'] = '银联扫码';

                    return $paymentPlugIn->post_pay_public($data_insert, $request_data);
                }

                //随行付银行银联
                if ($ways && $ways->ways_type == 13004) {
                    $data_public = [
                        'pay_type' => 'UNIONPAY',
                        'service' => 'UPOPCardPay',
                        'return_params' => '原样返回',
                        'title' => '银联收款',
                        'ways_source_desc' => '银联收款',
                        'ways_type_desc' => '银联收款',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $paymentPlugIn->vbill_pay_public($data_insert, $data_public);
                }

                //随行付A银行银联
                if ($ways && $ways->ways_type == 19004) {
                    $data_public = [
                        'pay_type' => 'UNIONPAY',
                        'service' => 'UPOPCardPay',
                        'return_params' => '原样返回',
                        'title' => '银联收款',
                        'ways_source_desc' => '银联收款',
                        'ways_type_desc' => '银联收款',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    return $paymentPlugIn->vbilla_pay_public($data_insert, $data_public);
                }

                //建设银行 银联扫码
                if ($ways && $ways->ways_type == 31003) {
                    $config = new CcBankPayConfigController();

                    $construct_merchant = $config->ccBank_pay_merchant($store_id, $store_pid);
                    if (!$construct_merchant) {
                        return json_encode([
                            'status' => '2',
                            'message' => '建设银行通道未开通'
                        ]);
                    }

                    if ($total_amount < 1000) {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_a;
                        $data_insert['fee_amount'] = $ways->rate_a * $total_amount / 100;
                    } else {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_c;
                        $data_insert['fee_amount'] = $ways->rate_c * $total_amount / 100;
                    }
                    if (strlen($out_trade_no) > 30) {
                        $out_trade_no = substr($out_trade_no, 0, 25) . substr(microtime(), 2, 5);
                    }
                    $request_data = [
                        'pay_type' => 'YLPAY',
                        'return_params' => '原样返回',
                        'title' => '银联扫码收款',
                        'ways_source_desc' => '银联扫码',
                        'ways_type_desc' => '银联扫码',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_source'] = $ways->ways_source;
                    $data_insert['ways_source_desc'] = '银联扫码';

                    return $paymentPlugIn->ccBank_pay_public($data_insert, $request_data);
                }

                //河南畅立收
                if ($source == '02') {
                    //易生支付 银联扫码
                    if ($ways && $ways->ways_type == 32003) {
                        $config = new EasySkPayConfigController();
                        $easyskpay_config = $config->easyskpay_config($config_id);
                        if (!$easyskpay_config) {
                            return json_encode([
                                'status' => '2',
                                'message' => '易生数科支付配置不存在请检查配置'
                            ]);
                        }

                        $easyskpay_merchant = $config->easyskpay_merchant($store_id, $store_pid);
                        if (!$easyskpay_merchant) {
                            return json_encode([
                                'status' => '2',
                                'message' => '商户易生数科支付通道未开通'
                            ]);
                        }

                        if ($total_amount < 1000) {
                            //扫码费率入库
                            $data_insert['rate'] = $ways->rate_a;
                            $data_insert['fee_amount'] = $ways->rate_a * $total_amount / 100;
                        } else {
                            //扫码费率入库
                            $data_insert['rate'] = $ways->rate_c;
                            $data_insert['fee_amount'] = $ways->rate_c * $total_amount / 100;
                        }

                        $request_data = [
                            'pay_type' => 'UNIONPAY',
                            'return_params' => '原样返回',
                            'title' => '银联扫码收款',
                            'ways_source_desc' => '银联扫码',
                            'ways_type_desc' => '银联扫码',
                            'code' => $code,
                            'out_trade_no' => $out_trade_no,
                            'config_id' => $config_id,
                            'store_id' => $store_id,
                            'store_pid' => $store_pid,
                            'ways_type' => '' . $ways->ways_type . '',
                            'ways_source' => $ways->ways_source,
                            'company' => $ways->company,
                            'total_amount' => $total_amount,
                            'remark' => $remark,
                            'device_id' => $device_id,
                            'shop_name' => $shop_name,
                            'merchant_id' => $merchant_id,
                            'store_name' => $store_name,
                            'tg_user_id' => $tg_user_id,
                        ];

                        //入库参数
                        $data_insert['out_trade_no'] = $out_trade_no;
                        $data_insert['ways_type'] = $ways->ways_type;
                        $data_insert['ways_type_desc'] = '银联扫码';
                        $data_insert['company'] = $ways->company;
                        $data_insert['ways_source'] = $ways->ways_source;
                        $data_insert['ways_source_desc'] = '银联扫码';

                        return $paymentPlugIn->easyskpay_pay_public($data_insert, $request_data);
                    }
                }

                //易生支付 银联扫码
                if ($ways && $ways->ways_type == 33003) {
                    $config = new AllinPayConfigController();
                    $allin_config = $config->allin_pay_config($config_id);
                    if (!$allin_config) {
                        return json_encode([
                            'status' => '2',
                            'message' => '通联支付配置不存在请检查配置'
                        ]);
                    }

                    $allin_merchant = $config->allin_pay_merchant($store_id, $store_pid);
                    if (!$allin_merchant) {
                        return json_encode([
                            'status' => '2',
                            'message' => '商户通联支付通道未开通'
                        ]);
                    }

                    if ($total_amount < 1000) {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_a;
                        $data_insert['fee_amount'] = $ways->rate_a * $total_amount / 100;
                    } else {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_c;
                        $data_insert['fee_amount'] = $ways->rate_c * $total_amount / 100;
                    }

                    $request_data = [
                        'pay_type' => 'UNIONPAY',
                        'return_params' => '原样返回',
                        'title' => '银联扫码收款',
                        'ways_source_desc' => '银联扫码',
                        'ways_type_desc' => '银联扫码',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_source'] = $ways->ways_source;
                    $data_insert['ways_source_desc'] = '银联扫码';

                    return $paymentPlugIn->allin_pay_public($data_insert, $request_data);
                }

                //富友支付 银联扫码
                if ($ways && $ways->ways_type == 11003) {
                    $config = new FuiouConfigController();
                    $fuiou_config = $config->fuiou_config($config_id);
                    if (!$fuiou_config) {
                        return json_encode([
                            'status' => '2',
                            'message' => '富友支付配置不存在请检查配置'
                        ]);
                    }

                    $fuiou_merchant = $config->fuiou_merchant($store_id, $store_pid);
                    if (!$fuiou_merchant) {
                        return json_encode([
                            'status' => '2',
                            'message' => '商户富友支付通道未开通'
                        ]);
                    }

                    if ($total_amount < 1000) {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_a;
                        $data_insert['fee_amount'] = $ways->rate_a * $total_amount / 100;
                    } else {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_c;
                        $data_insert['fee_amount'] = $ways->rate_c * $total_amount / 100;
                    }

                    $request_data = [
                        'pay_type' => 'UNIONPAY',
                        'return_params' => '原样返回',
                        'title' => '银联扫码收款',
                        'ways_source_desc' => '银联扫码',
                        'ways_type_desc' => '银联扫码',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                    ];

                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_source'] = $ways->ways_source;
                    $data_insert['ways_source_desc'] = '银联扫码';

                    return $paymentPlugIn->fuiou_pay_public($data_insert, $request_data);
                }

                //银盛支付 银联扫码
                if ($ways && $ways->ways_type == 14004) {
                    $config = new YinshengConfigController();
                    $yinsheng_config = $config->yinsheng_config($config_id);
                    if (!$yinsheng_config) {
                        return json_encode([
                            'status' => '2',
                            'message' => '银盛配置不存在请检查配置'
                        ]);
                    }

                    $yinsheng_merchant = $config->yinsheng_merchant($store_id, $store_pid);
                    if (!$yinsheng_merchant) {
                        return json_encode([
                            'status' => '2',
                            'message' => '银盛通道未开通'
                        ]);
                    }

                    if ($total_amount < 1000) {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_a;
                        $data_insert['fee_amount'] = $ways->rate_a * $total_amount / 100;
                    } else {
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate_c;
                        $data_insert['fee_amount'] = $ways->rate_c * $total_amount / 100;
                    }
                    $request_data = [
                        'pay_type' => 'YLPAY',
                        'return_params' => '原样返回',
                        'title' => '银联扫码收款',
                        'ways_source_desc' => '银联扫码',
                        'ways_type_desc' => '银联扫码',
                        'code' => $code,
                        'out_trade_no' => $out_trade_no,
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'store_pid' => $store_pid,
                        'ways_type' => '' . $ways->ways_type . '',
                        'ways_source' => $ways->ways_source,
                        'company' => $ways->company,
                        'total_amount' => $total_amount,
                        'remark' => $remark,
                        'device_id' => $device_id,
                        'shop_name' => $shop_name,
                        'merchant_id' => $merchant_id,
                        'store_name' => $store_name,
                        'tg_user_id' => $tg_user_id,
                        'phone' => $store->people_phone
                    ];

                    //入库参数
                    $data_insert['out_trade_no'] = $out_trade_no;
                    $data_insert['ways_type'] = $ways->ways_type;
                    $data_insert['ways_type_desc'] = '银联扫码';
                    $data_insert['company'] = $ways->company;
                    $data_insert['ways_source'] = $ways->ways_source;
                    $data_insert['ways_source_desc'] = '银联扫码';

                    return $paymentPlugIn->yinsheng_pay_public($data_insert, $request_data);
                }
            }

            return json_encode([
                'status' => 2,
                'message' => '暂不支持此二维码'
            ]);
        } catch (\Exception $exception) {
            Log::info('收钱啦-付款码支付-error');
            Log::info($exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine());
            return json_encode([
                'status' => '-1',
                'message' => $exception->getMessage() . ' | ' . $exception->getLine()
            ]);
        }
    }

}
