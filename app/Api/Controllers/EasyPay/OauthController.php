<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/6/8
 * Time: 下午5:36
 */

namespace App\Api\Controllers\EasyPay;

use App\Api\Controllers\Config\EasyPayConfigController;
use App\Models\MemberList;
use App\Models\MemberSetJf;
use App\Models\MemberTpl;
use EasyWeChat\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class OauthController extends BaseController
{
    //授权
    public function oauth(Request $request)
    {
        $sub_info = $request->get('state');
        $sub_info_arr = json_decode(base64_decode((string)$sub_info), true);
        $config_id = $sub_info_arr['config_id'];

        //易生配置
        $config = new EasyPayConfigController();
        $easypay_config = $config->easypay_config($config_id);
        if (!$easypay_config) {
            return json_encode([
                'status' => 2,
                'message' => '易生配置不存在请检查配置'
            ]);
        }

        $config = [
            'app_id' => $easypay_config->wx_appid,
            'scope' => 'snsapi_base',
            'oauth' => [
                'scopes' => ['snsapi_base'],
                'response_type' => 'code',
                'callback' => url('api/easypay/weixin/oauth_callback?sub_info=' . $sub_info . '&wx_AppId=' . $easypay_config->wx_appid . '&wx_Secret=' . $easypay_config->wx_secret . ''),
            ],
        ];
        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;

        return $oauth->redirect();
    }

    //回调
    public function oauth_callback(Request $request)
    {
        try {
            $sub_info = $request->get('sub_info');
            $code = $request->get('code');
            $wx_AppId = $request->get('wx_AppId');
            $wx_Secret = $request->get('wx_Secret');

            $sub_info_arr = json_decode(base64_decode((string)$sub_info), true);
            $config_id = $sub_info_arr['config_id'];
            $store_id = $sub_info_arr['store_id'];
            $store_name = $sub_info_arr['store_name'];
            $merchant_id = $sub_info_arr['merchant_id'];

            $config = [
                'app_id' => $wx_AppId,
                "secret" => $wx_Secret,
                "code" => $code,
                "grant_type" => "authorization_code",
            ];

            $app = Factory::officialAccount($config);
            $oauth = $app->oauth;
            $user = $oauth->user();
            $open_id = $user->getId();

            $data = [
                'store_id' => $store_id,
                'store_name' => $store_name,
                'store_address' => '',
                'open_id' => $open_id,
                'merchant_id' => $merchant_id,
            ];

            $data = base64_encode(json_encode((array)$data));

            return redirect('/api/easypay/weixin/pay_view?data=' . $data);
        } catch (\Exception $ex) {
            Log::info('易生微信授权-报错');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

    }


    //支付显示页面
    public function pay_view(Request $request)
    {
        $data = json_decode(base64_decode((string)$request->get('data')), true);

        $store_id = $data['store_id'];
        $open_id = $data['open_id'];

        //查询是否有开启会员
        $is_member = 0;
        $MemberTpl = MemberTpl::where('store_id', $store_id)
            ->select('tpl_status')
            ->first();
        if ($MemberTpl && $MemberTpl->tpl_status == 1) {
            $is_member = 1;
        }

        //如果是会员
        if (0) {
            //判断是否是会员
            $MemberList = MemberList::where('store_id', $store_id)
                ->where('wx_openid', $open_id)
                ->select('mb_jf', 'mb_money', 'mb_id')
                ->first();
            $data['mb_jf'] = "";
            $data['mb_id'] = "";
            $data['mb_money'] = "";
            $data['dk_money'] = "";
            $data['dk_jf'] = "0";
            $data['ways_source'] = "weixin";

            if ($MemberList) {
                $data['mb_jf'] = $MemberList->mb_jf;
                $data['mb_id'] = $MemberList->mb_id;
                $data['mb_money'] = $MemberList->mb_money;
                //判断是否有积分抵扣
                $MemberSetJf = MemberSetJf::where('store_id', $store_id)
                    ->select('dk_jf_m', 'dk_rmb')
                    ->first();
                if ($MemberSetJf) {
                    //3.用户的积分一共可以抵扣多少钱
                    $data['dk_money'] = ($MemberList->mb_jf / $MemberSetJf->dk_jf_m) * $MemberSetJf->dk_rmb;
                    $data['dk_jf'] = $MemberList->mb_jf;
                }
            }
            $data['ways_type'] = "21002";
            $data['company'] = "easypay";
            return view('easypay.membermweixin', compact('data'));
        } else {
            return view('easypay.weixin', compact('data'));
        }
    }


    //微信充值页面
    public function member_cz_pay_view(Request $request)
    {
        return view('member.cz');
    }


    //获取openid
    public function oauth_openid(Request $request)
    {
        $sub_info = $request->get('state');

        //微信配置
        $easypay_config = new EasyPayConfigController();
        $EasyPayConfig = $easypay_config->easypay_config('1234');
        $config = [
            'app_id' => $EasyPayConfig->wx_appid,
            'scope' => 'snsapi_base',
            'oauth' => [
                'scopes' => ['snsapi_base'],
                'response_type' => 'code',
                'callback' => url('api/easypay/weixin/oauth_callback_openid?sub_info=' . $sub_info . '&wx_AppId=' . $EasyPayConfig->wx_appid . '&wx_Secret=' . $EasyPayConfig->wx_secret . ''),
            ],
        ];
        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;

        return $oauth->redirect();
    }

//    public function oauth_openid(Request $request)
//    {
//        $sub_info = $request->get('state');
//        //第三方传过来的信息
//        $sub_info_arr = json_decode(base64_decode((string)$sub_info), true);
//        //快钱配置
//        $mbconfig = new MyBankConfigController();
//        $MyBankConfig = $mbconfig->MyBankConfig('1234');
//        $config = [
//            'app_id' => $MyBankConfig->wx_AppId,
//            'scope' => 'snsapi_base',
//            'oauth' => [
//                'scopes' => ['snsapi_base'],
//                'response_type' => 'code',
//                'callback' => url('api/easypay/weixin/oauth_callback_openid?sub_info=' . $sub_info . '&wx_AppId=' . $MyBankConfig->wx_AppId . '&wx_Secret=' . $MyBankConfig->wx_Secret . ''),
//            ],
//        ];
//        $app = Factory::officialAccount($config);
//        $oauth = $app->oauth;
//
//        return $oauth->redirect();
//    }


    //获取回调
    public function oauth_callback_openid(Request $request)
    {
        $sub_info = $request->get('sub_info');
        $code = $request->get('code');
        $wx_AppId = $request->get('wx_AppId');
        $wx_Secret = $request->get('wx_Secret');

        $sub_info_arr = json_decode(base64_decode((string)$sub_info), true);

        $config = [
            'app_id' => $wx_AppId,
            "secret" => $wx_Secret,
            "code" => $code,
            "grant_type" => "authorization_code",
        ];

        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;
        $user = $oauth->user();
        $open_id = $user->getId();

        $sub_info_arr['open_id'] = $open_id;
        $sub_info_arr['store_address'] = '';
        $callback_url = $sub_info_arr['callback_url'];

        $data = base64_encode(json_encode((array)$sub_info_arr));

        return redirect($callback_url . '?data=' . $data);
    }
    //收银机获取signKey值 易生1.0所需
    function getSignKey(Request $request)
    {
        $config_id = $request->get('config_id');
        $channel_id = $request->get('channel_id');
        if (!$config_id) {
            return json_encode([
                    'status' => -1,
                    'msg' => 'config_id不能为空'
                ]
            );
        }
        if (!$channel_id) {
            return json_encode([
                    'status' => -1,
                    'msg' => 'channel_id不能为空'
                ]
            );
        }
        $EasyPayConfigController = new EasyPayConfigController();
        $easy_config = $EasyPayConfigController->easypay_config($config_id);
        if ($easy_config) {
            return json_encode([
                    'status' => 1,
                    'msg' => '获取成功',
                    'signKey' => $easy_config->getSignKey
                ]
            );
        }
        return json_encode([
                'status' => -1,
                'msg' => '获取失败',
            ]
        );

    }

}
