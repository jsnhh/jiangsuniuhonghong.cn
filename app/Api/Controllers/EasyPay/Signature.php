<?php
namespace App\Api\Controllers\EasyPay;

final class Signature
{
    private $key;


    function __construct($key)
    {
        $this->key = $key;
    }


    function sign($params)
    {
        ksort($params);
        $params = array_filter($params);
        $string = "";

        foreach ($params as $name => $value) {
            $string .= $name . '=' . $value . '&';
        }

        $string .= 'key=' . $this->key;

        return strtoupper(md5($string));
    }


}
