<?php
namespace App\Api\Controllers\EasyPay;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

final class Payment
{
    // 支付1.0 begin
//    function __construct($url)
//    {
//        $this->url = $url;
//        $this->http = new Client();
//    }
//
//
//    public function request($params, $key)
//    {
//        $params = array_filter($params);
//        $signature = new Signature($key);
//        $params['sign'] = $signature->sign($params);
//
//        $response = $this->http->request('POST', $this->url, [
//            'Content-Type' => 'application/x-www-form-urlencoded;charset=UTF-8',
//            'form_params' => $params
//        ]);
//
//        return (string) $response->getBody();
//    }
    // 支付1.0 end

    // 支付2.0 begin
     function __construct()
     {
         $this->http = new Client();
     }

     //微收单2.0
     public function request($params, $url)
     {
         $data = stripslashes(json_encode($params,JSON_UNESCAPED_UNICODE));//JSON_UNESCAPED_UNICODE 发送数据中文Unicode乱码处理
         $curl = curl_init();
         curl_setopt($curl, CURLOPT_URL, $url);
         curl_setopt($curl, CURLOPT_HEADER, 0);
         curl_setopt($curl, CURLOPT_POST, 1);
         curl_setopt($curl, CURLOPT_TIMEOUT, 30); //访问超时时间
         curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
         curl_setopt($curl, CURLOPT_HTTPHEADER, [
             'Content-Type: application/json; charset=UTF-8',
         ]);
         //本地忽略校验证书
         curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
         $tmpInfo = curl_exec($curl);
         return $tmpInfo;
     }
    // 支付2.0 end


}
