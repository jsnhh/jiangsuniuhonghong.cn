<?php
namespace App\Api\Controllers\EasyPay;


use App\Api\Controllers\Config\WeixinConfigController;
use App\Models\WeixinNotify;
use Dingo\Api\Http\Request;
use Faker\Factory;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

final class TestController extends BaseController
{
//    private $payment;

    private $src;
    private $image;
    private $imageinfo;
    private $percent = 0.5;


//    public function __construct()
//    {
//        $this->url = 'https://notify-test.eycard.cn:7443/WorthTech_Access_AppPaySystemV2/apppayacc';
//        $this->payment = new Payment($this->url);
//    }


    //更新并获取签名密钥
//    public function test11()
//    {
//        $params = [
//            "channelid" => "D01000000089888", //渠道编号
//            "opt" => "getSign", //操作类型
//        ];
//
//        $key = "d5riddkald4k4did"; //渠道密钥
//
//        $result = $this->payment->request($params, $key);
//        print_r($result);die;
////        {"opt":"getSign","sign":"AEFFB0BA1D2C68C99FBD140469C72A76","resultcode":"00","key":"8od1d4o2i0dkj7lk212dkild7adi275l","channelid":"D01000000089888"}
//    }


    //用户被扫，商户扫用户码
//    public function testMechantScanPayerCodePayment()
//    {
//        $params = [
//            "channelid" => "D01000000089888",
//            "merid" => "531000012966395",
//            "termid" => "W1486800",
//            "opt" => "scanPay",
//            "tradetrace" => "123343334",
//            "tradeamt" => "1",
//            "authcode" => "134782163690476394",
//            "body" => "sss",
//            "version" => "", // 版本号-非必填
//            "handingfee" => "" // 手续费-非必填
//        ];
//
//        $key = "8od1d4o2i0dkj7lk212dkild7adi275l";
////        $key = "7dlddsvod8la50jls3js3klda31ddr8d";
//
//        $result = $this->payment->request($params, $key);
//        print_r($result);die;
////        {"termid":"W1486800","opt":"scanPay","tradetrace":"123343334","returnmsg":"101付款码无效，请重新扫码","wtorderid":"11420200703094243115143","sign":"A42BBDBD8920FBC1CBE2BDB829E1A1A7","merid":"531000012966395","resultcode":"06","channelid":"D01000000089888"}
////        $this->assertNotEmpty($result);
////        $this->assertContains("123343334", $result);
//    }


    //用户主扫，用户扫商户码
//    public function testPayerScanMechantCodePayment()
//    {
//        $params = [
//            "channelid" => "D01000000089888",
//            "merid" => "531000012966395",
//            "termid" => "W1486800",
//            "opt" => "apPreOrder",
//            "tradetype" => "NATIVE",
//            "tradetrace" => "alipay_scan2020070356789",
//            "tradeamt" => "1",
//            "body" => "sss",
//            "notifyurl" => url('api/easypay/pay_notify_url'),
//            "version" => "",// 版本号-非必填
//            "handingfee" => ""// 手续费-非必填
//        ];
//
//        $key = "8od1d4o2i0dkj7lk212dkild7adi275l";
//
//        $result = $this->payment->request($params, $key);
//        print_r($result);die;
////        {"codeurl":"https://qr.alipay.com/bax01591e1t2xjq4wuld2073","tradetype":"NATIVE","termid":"W1486800","opt":"apPreOrder","tradetrace":"alipay_scan2020070356789","returnmsg":"Success","wtorderid":"21520200703095003115147","sign":"8928AE93A4435F7373700273C0938695","merid":"531000012966395","prepayid":"","resultcode":"00","channelid":"D01000000089888"}
////        $this->assertNotEmpty($result);
////        $this->assertContains("codeurl", $result);
////        $this->assertContains("apPreOrder", $result);
////        $this->assertContains("1233433345", $result);
//    }


    //订单查询
//    public function testPaymentQuery()
//    {
//        $params = [
//            "channelid" => "D01000000089888",
//            "merid" => "531000012966395",
//            "termid" => "W1486800",
//            "opt" => "tradeQuery",
//            "tradetrace" => "alipay_scan2020070356789"
//        ];
//
//        $key = "8od1d4o2i0dkj7lk212dkild7adi275l";
//
//        $result = $this->payment->request($params, $key);
//        print_r($result);die;
////        {"termid":"W1486800","opt":"tradeQuery","tradetrace":"alipay_scan2020070356789","returnmsg":"订单未支付","wtorderid":"21520200703095003115147","sign":"B035D513BCDB78D43B45C17849304D4C","merid":"531000012966395","resultcode":"AA","channelid":"D01000000089888"}
////        $this->assertNotEmpty($result);
////        $this->assertContains("123343334", $result);
//    }


    //退款
//    public function returnOrder()
//    {
//        $params = [
//            "channelid" => "D01000000089888", //渠道编号
//            "merid" => "531000012966395", //商戶编号
//            "termid" => "W1486800", //终端编号
//            "tradetrace" => "alipay_scan2020070356789", //商户订单号
//            "opt" => "zwrefund", //操作类型（zwrefund）
////            "oritradetrace" => "", //原交易流水； 与 oriwtorderid 字段可以二选一上送，如果都上送，以本节点为准
//            "oriwtorderid" => "21520200703095003115147", //原交易的系统订单号；与 oritradetrace 字段可以二选一上送，如果都上送，以 oritradetrace 节点为准
//            "tradeamt" => 1, //退货金额,以分为单位，不大于原交易金额与已成功退货金额之差
//        ];
//
////        $key = "8od1d4o2i0dkj7lk212dkild7adi275l";
//        $key = "i15d5k4k7jovod2dl20af1das5k2dsli";
//
//        $result = $this->payment->request($params, $key);
//        print_r($result);die;
////        {"termid":"W1486800","opt":"tradeQuery","tradetrace":"alipay_scan2020070356789","returnmsg":"验证签名失败","merid":"531000012966395","resultcode":"A0","channelid":"D01000000089888"}
////        {"termid":"W1486800","opt":"zwrefund","tradetrace":"alipay_scan2020070356789","returnmsg":"请求参数不合法@ oritradetrace or oriwtorderid","merid":"531000012966395","resultcode":"01","channelid":"D01000000089888"}
////        {"termid":"W1486800","opt":"zwrefund","tradetrace":"alipay_scan2020070356789","returnmsg":"原交易结果未确定交易,请先对原交易进行查询操作","sign":"939977D052F281396BB2989DEAC6C732","merid":"531000012966395","resultcode":"08","channelid":"D01000000089888"}
//    }


//    public function tearDown() {
//        unset($this->url);
//        unset($this->payment);
//    }


    //进件 上传图片
//    public function test(\Illuminate\Http\Request $request)
//    {
//        try {
//            $file_name = $request->get('file_name', ''); //
//            $image_type = $request->get('image_type', ''); //
//            $client_code = $request->get('client_code', ''); //
//            $key = $request->get('key', ''); //
//            var_dump($request->all());die;

//            $img_url = explode('/', $file_name);
//            $img_url = end($img_url);
//            $img = public_path() . '/upload/images/' . $img_url;
//            if ($img) {
//                try {
                    //压缩图片
//                    $img_obj = \Intervention\Image\Facades\Image::make($img);
//                    $img_obj->resize(800, 700);
//                    $img = public_path() . '/upload/s_images/' . $img_url;
//                    $img_obj->save($img);
//                } catch (\Exception $e) {
//                    Log::info('易生上传图片-压缩图片-error');
//                    Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
//                }
//            }

//            $obj = new \App\Api\Controllers\EasyPay\MerAccessController();
//            $reqData = [
//                'version' => '1.0',
//                'fileName' => $img,
//                'clientCode' => $client_code,
//                'picMode' => $image_type //图片类型,01-法人身份证正面 ；02-法人身份证反面；03-商户联系人身份证正面；04-商户联系人身份证反面；05-营业执照；06-收银台；07-内部环境照；08-营业场所门头照；09-门牌号；10-协议；11-商户登记表正面；12-商户登记表反面；13-开户许可证；14-银行卡；15-清算授权书；16-定位照（选填）；17-手持身份证照片（选填）
//            ];
//            $result = $obj->imageCurl($reqData, $key); //1-成功 2-失败
////            $result = [
////                "status" => 1,
////                "data" => [
////                    "clientCode" => "48310009",
////                    "retCode" => "0000",
////                    "retMsg" => "操作成功",
////                    "version" => "1.0",
////                    "MAC" => "450411CC81CC57C669C702838327BDA4",
////                    "fileId" => "605967e33e10af59bef26e24"
////                ],
////                "message" => "操作成功"
////            ];
//            if ($result['status'] == 1) {
//                if (isset($result['data']) && isset($result['data']['retCode']) && $result['data']['retCode'] == '0000') {
//                    return [
//                        'status' => 1,
//                        'data' => $result['data'],
//                        'message' => $result['data']['retMsg'] ?? $result['message']
//                    ];
//                } else {
//                    return [
//                        'status' => 2,
//                        'data' => $result['data'] ?? $result,
//                        'message' => $result['data']['retMsg'] ?? $result['message']
//                    ];
//                }
//            }

//            return [
//                'status' => 2,
//                'data' => $result['data'] ?? $result,
//                'message' => $result['message']
//            ];
//        } catch (\Exception $ex) {
//            Log::info('易生支付test-图片上传-error');
//            Log::info($ex->getMessage() .'|'. $ex->getFile() .'|'. $ex->getLine());
//            return [
//                'status' => -1,
//                'message' => $ex->getMessage() .'|'. $ex->getFile() .'|'. $ex->getLine()
//            ];
//        }
//    }

    //商户审核结果查询
    public function test()
    {
//        $params = [
//            "version" => "1.0",
//            "clientCode" => "64358413",
//            "messageType" => "QUERYAUDMER",
//            "MAC" => "D01000000089888",
//            "merTrace" => "627e27fe3e10af0519f30d9d",
//            "operaTrace" => "28165243494040115200"
//        ];
//
//        $key = 'EDferdssserrtXDe';
//
//        $easyPayMerAccessObj = new MerAccessController();
//        $result = $easyPayMerAccessObj->execute($params, $key);
//
//        return json_encode($result);
//        print_r($result); die;

        $thisKey = 'MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAJXJo/EewLmDuTChmfUNtfdSosZnlUESPRyGtpvzEw8nwFrShU2dCckcHiEB5n3yejUArjMtnZhLvwVM6dPoWzczScY8mt8I5IY19Mnf1nzOHUocqFZPLAXHdrRs/tVh77UMQIL810t7czIQ8XQ6MarXborprhM85xSJvHujGXNjAgMBAAECgYAa6rZUQSN+8uXXkGK7P7CAdlAb+Uc//0gOFUNBeQfmxEzzxTsBPIQiqLPdWJsT7Pv7BFlJhxv33zy/dhfa5blmRDd/V7ZxMKidobVnwvnEtIIXawjny6+LuXdHywZKlZhKTkvSumIFjpftE/JQ6jv+lwNLR4HMx6Zk1spVvjwu6QJBAP6RR91W2FRgXLX7+Jl15ePa4J/0yS1C/YvGoGo0CggYkSxjraGHZCQYG46/81ld9i84WbukBIU8GMXgmWTrN48CQQCWoWsQmmmK2aB2A/VfTGcLJwDBDt+Rsp1k5s6Y3VKwkyt4Qoqps8QdC8EEwru1hzzc3KZer4P1ZeRI6LgJ1rztAkEAys0OhWSUPtPtyAUOTaNBG77ZSFUImAhQWTRJw5HllAGOVeCrCI4F10NlYNnGkiFq6L9prTTDbSktSybczSsePQJARjm3SqRStFNFxf+OdAGQwNhMN7WRccHOM26PvyDgHkZeq9CKCRgwREJomKmZDcahD28neVlmsg/w4DTvKFce+QJBAMX5si40GHA+Oabb/YxYuAOv9tNbY4wq14UyjaoCEkWP4EfFR60gN0quv4BIryV7hiCTiGFgJSlJfbCu4vVki1U=';

        $data = [
            'tradeCode' => 'WTJS1',
            'tradeAmt' => 102,
            'orderInfo' => 'jsapiPay',
            'orgBackUrl' => 'https://pay.jiangsuniuhonghong.cn/api/easypay/pay_notify_url',
            'wxSubAppid' => 'wxb96586c4fb1d67eb',
            'payerId' => 'osSGo51-w-iSEdadLxcYaTFvM_tI'
        ];

        $test = new BaseController();
        $sign = $test->getSign($data, $thisKey);

        $terminalinfo = ['location' => '+35.78548/-116.079643','terminalip' => '154.8.143.104'];

        $appendData = ['terminalinfo' => $terminalinfo];

        $easypay_data = [
            'orgId' => 'ORG000000001027',
            'orgMercode' => '831462157340005',
            'orgTermno' => '39047634',
            'orgTrace' => 'wx_qr20220609000042328103935',
            'sign' => $sign,
            'signType' => 'RSA2',
            'appendData' => $appendData,
            'data' => $data
        ];

        Log::info('易生支付-静态码提交-params：');
        Log::info($easypay_data);
        $res_obj = $this->payment->request($easypay_data, $this->jsapiUrl);
        Log::info('易生支付-静态码提交-res：');
        Log::info($res_obj);

//        $res_obj = $this->payment->request($easypay_data, $this->jsapiUrl);
//        $signver = $test->verify($data, $sign);

        return json_encode($res_obj);

    }

    //电子协议 Test
    public function testEasyPayMPS(Request $data)
//     public function testEasyPayMPS()
    {
        $txCode = $data->get('txCode');

        $url = 'https://api-test.eycard.cn:9443/EasypayMPSystem/ContractServet';
//        $download_url = 'https://notify-test.eycard.cn:7443/EasypayMPSystem/DownloadContractServet.do'; //合同下载
        $download_url = 'https://api-test.eycard.cn:9443/EasypayMPSystem/DownloadContractServet.do'; //合同下载


        $channelId = '616161622101901';    //渠道编号:616161622101901
        $merId = 'MER021322101901';   //商户编号:MER021322101901
        $termId = 'TE101901';            //终端编号:TE101901
        $key = '5q0067j7o0cqez9zkk8qrd7zb0e849c9i';

        $sign_key = Cache::get('easyMP_signKey');

//        if (!$sign_key) {
//            //获取签名key
//            $url_key = 'https://api-test.eycard.cn:9443/EasypayMPSystem/SignServet';
//
//            $dataInfo = [
//                'channelId' => $channelId,
//                'tradeTrace' => date('YmdHis') . mt_rand(1000, 9999), //请求流水，唯一，不包括特殊字符
//                'version' => '3.0',
//                'sign' => $key
//            ];
//
//            $result = $this->postUrl(json_encode($dataInfo), $url_key);
//
//            Log::info('==========获取签名key========');
//            Log::info($result);
//
//            //系统错误
//            if (!$result) {
//                return [
//                    'status' => '-1',
//                    'message' => '系统错误',
//                ];
//            }
//
//            $res_arr = json_decode($result, true);
//            $sign = '';
//            //成功
//            if ($res_arr['resultCode'] == '00') {
//                $sign = $res_arr['sign'];
//                Log::info('==========获取签名key====sign:' . $sign_key);
//                Cache::forever('easyMP_signKey', $sign);
//            }
//        }

//        $sign_key = '5q0067j7o0cqez9zkk8qrd7zb0e849c9i';
//        Log::info('==========获取签名key====sign_key:' . $sign_key);


        // $txCode = 3001; // 个人开户 3001; 企业开户 3002; 商户开户 3003; 增加印章 3011

        $headInfo = [
            // 'txTime' => strtotime(time())
            'txTime' =>date('YmdHis', time()) // 交易时间：yyyyMMddHHmmss
        ];
        $contractBody = [];

        if($txCode == 3001){ //个人开户
            $personName = $data->get('personName');
            $identTypeCode = $data->get('identTypeCode');
            $identNo = $data->get('identNo');
            $mobilePhone = $data->get('mobilePhone');
            $address = $data->get('address');
            $authenticationMode = $data->get('authenticationMode');

            $person = [
                "personName" => $personName,
                "identTypeCode" => $identTypeCode,
                "identNo" => $identNo,
                "mobilePhone" => $mobilePhone,
                "address" => $address,
                "authenticationMode" => $authenticationMode
            ];

            $contractBody = [
                'head' => $headInfo,
                'person' => $person,
                'notSendPwd' => "1"
            ];

            $data = [
                'channelId' => $channelId,
                'merId' => $merId,
                'termId' => $termId,
                'tradeTrace' => date('YmdHis') . mt_rand(1000, 9999), //请求流水，唯一，不包括特殊字符
                'version' => '1.0',
                'txCode' => strval($txCode), // 个人开户 3001; 企业开户 3002; 商户开户 3003
                'contractBody' => json_encode($contractBody,JSON_UNESCAPED_UNICODE),
            ];

            $sign = $this->getSign($data, $key);

            $data['sign'] = $sign;

            Log::info('==========请求参数:'.json_encode($data,JSON_UNESCAPED_UNICODE));

            $result = $this->postUrl($data, $url);

            Log::info('==========个人开户========');
            Log::info($result);

            $res_arr = json_decode($result, true);

            //成功
            if ($res_arr['resultCode'] == '00' ) {

                $contractBody = json_decode($res_arr['contractBody'],true);

                //用户ID
                $userId = $contractBody['person']['userId'];

                Log::info('==========个人开户=====userId:' . $userId);

                return json_encode([
                    'status' => 1,
                    'userId' => $userId,
                    'message' => 'success'
                ]);
            }else{

                return json_encode([
                    'status' => -1,
                    'data' => $res_arr['resultCode'],
                    'message' => 'error'
                ]);

            }

        }else if ($txCode == 3002){ //企业开户

            $enterprise = [
                "enterpriseName" => "某公司", //企业名称
                "identTypeCode" => "8", //企业营业执照
                "identNo" => "110101111111117",
                "mobilePhone" => "18800008888",
                "landlinePhone" => "01083526655", //企业联系电话
                "authenticationMode" => "公安"
            ];
            $enterpriseTransactor = [
                "transactorName" => "王某",
                "identTypeCode" => "0",
                "identNo" => "110101200101010018",
                "address" => "北京"
            ];

            $contractBody = [
                'head' => $headInfo,
                'enterprise' => $enterprise,
                'enterpriseTransactor' => $enterpriseTransactor,
                'notSendPwd' => 1 //0：发送；1：不发送；建议取值1
            ];


            $data = [
                'channelId' => $channelId,
                'merId' => $merId,
                'termId' => $termId,
                'tradeTrace' => date('YmdHis') . mt_rand(1000, 9999), //请求流水，唯一，不包括特殊字符
                'version' => '1.0',
                'txCode' => strval($txCode), // 个人开户 3001; 企业开户 3002; 商户开户 3003
                'contractBody' => json_encode($contractBody,JSON_UNESCAPED_UNICODE),
            ];

            $sign = $this->getSign($data, $key);

            $data['sign'] = $sign;

            Log::info('==========请求参数:'.json_encode($data,JSON_UNESCAPED_UNICODE));

            $result = $this->postUrl($data, $url);


            Log::info('==========企业开户========');
            Log::info($result);

            $res_arr = json_decode($result, true);

            //成功
            if ($res_arr['resultCode'] == '00' ) {

                $contractBody = json_decode($res_arr['contractBody'],true);

                //用户ID
                $userId = $contractBody['enterprise']['userId'];

                Log::info('==========企业开户=====userId:' . $userId);

                return json_encode([
                    'status' => 1,
                    'userId' => $userId,
                    'message' => 'success'
                ]);
            }else{

                return json_encode([
                    'status' => -1,
                    'data' => $res_arr['resultCode'],
                    'message' => 'error'
                ]);

            }

        }else if ($txCode == 3003){

            $contractBody = [
                'head' => $headInfo,
                'merCode' => '821245470' //交易系统中的内部商户编号
            ];

            $data = [
                'channelId' => $channelId,
                'merId' => $merId,
                'termId' => $termId,
                'tradeTrace' => date('YmdHis') . mt_rand(1000, 9999), //请求流水，唯一，不包括特殊字符
                'version' => '1.0',
                'txCode' => $txCode, // 个人开户 3001; 企业开户 3002; 商户开户 3003
                'contractBody' => $contractBody,
                'sign' => $sign_key
            ];

            $result = $this->postUrl(json_encode($data), $url);

            Log::info('==========商户开户========');
            Log::info($result);

            $res_arr = json_decode($result, true);

            //成功
            if ($res_arr['head']['retMessage'] == 'OK') {
                //用户ID
                $userId = $res_arr['person']['userId'];

                Log::info('==========商户开户=====userId:' . $userId);

            }


        }else if ($txCode == 3011){ //增加印章 3011
            $sealAdd = [
                'userId' => '1234D2BF390B4DCF8AE175A97112EDDD',
                'seal' => ['imageData' => 'iVBORw0KGgoAAAANSUhEUgAAATkAAAB6CDmAADmAADmAADmAADmAADmAADmAADmAADmAADmAADmAADmAAAAAADzbgg/AAAAEHRSTlMAj7+fYBBAr1Ag388w739w08reHsBnlb09gM8qe3sAn1X29gA+q38tvNrsgRqulgAAAABJRU5ErkJggg==' ]
            ];

            $contractBody = [
                'head' => $headInfo,
                'sealAdd' => $sealAdd
            ];

        }else if ($txCode == 3012) { //修改印章 3012

            $sealUpdate = [
                'userId' => '1234D2BF390B4DCF8AE175A97112EDDD',
                'seal' => [
                    'sealId' => '123427E2B8F04795907712D847EE913C', //添加印章时返回的 ID
                    'imageData' => 'iVBORw0KGgoAAAANSUhEUgAAATkAAAB6CDmAADmAADmAADmAADmAADmAADmAADmAADmAADmAADmAADmAAAAAADzbgg/AAAAEHRSTlMAj7+fYBBAr1Ag388w739w08reHsBnlb09gM8qe3sAn1X29gA+q38tvNrsgRqulgAAAABJRU5ErkJggg==' ]
            ];

            $contractBody = [
                'head' => $headInfo,
                'sealUpdate' => $sealUpdate
            ];

        }else if ($txCode == 3013) { // 删除印章 3013
            $sealDelete = [
                'userId' => '1234D2BF390B4DCF8AE175A97112EDDD',
                'seal' => [
                    'sealId' => '123427E2B8F04795907712D847EE913C' //添加印章时返回的 ID
                ]
            ];

            $contractBody = [
                'head' => $headInfo,
                'sealDelete' => $sealDelete
            ];

        }else if ($txCode == 3014) { // 查询印章 3014
            $sealQuery = [
                'userId' => '1234D2BF390B4DCF8AE175A97112EDDD'
            ];

            $contractBody = [
                'head' => $headInfo,
                'sealQuery' => $sealQuery
            ];
        }else if ($txCode == 3101) { // 发送验证码 3101
            $proxySign = [
                'userId' => '8D4CFF34AB1D289EE05311016B0A9A8A',
                'randomCode' => strval(rand(10, 99))
            ];

            $contractBody = [
                'head' => $headInfo,
                'proxySign' => $proxySign
            ];


            $data = [
                'channelId' => $channelId,
                'merId' => $merId,
                'termId' => $termId,
                'tradeTrace' => date('YmdHis') . mt_rand(1000, 9999), //请求流水，唯一，不包括特殊字符
                'version' => '1.0',
                'txCode' => strval($txCode), // 个人开户 3001; 企业开户 3002; 商户开户 3003
                'contractBody' => json_encode($contractBody,JSON_UNESCAPED_UNICODE),
            ];

            $sign = $this->getSign($data, $key);

            $data['sign'] = $sign;

            Log::info('==========请求参数:'.json_encode($data,JSON_UNESCAPED_UNICODE));

            $result = $this->postUrl($data, $url);


            Log::info('==========发送验证码========');
            Log::info($result);

            $res_arr = json_decode($result, true);

            //成功
            if ($res_arr['resultCode'] == '00' ) {

                $contractBody = json_decode($res_arr['contractBody'],true);

                //用户ID
                $userId = $contractBody['proxySign']['userId'];

                Log::info('==========发送验证码=====userId:' . $userId);

                //创建合同
                $this->createContract($headInfo,$channelId,$merId,$termId,$key,$url);

                return json_encode([
                    'status' => 1,
                    'userId' => $userId,
                    'message' => 'success'
                ]);
            }else{

                return json_encode([
                    'status' => -1,
                    'data' => $res_arr['resultCode'],
                    'message' => 'error'
                ]);

            }










        }else if ($txCode == 3201) { // 创建合同 3201;




        }else if ($txCode == 3206) { // 签署合同 3206

            $signInfos = [
                'userId' => '6327987D3B854A3A9F4A198426084F02',
                'authorizationTime' => 20190214171200,
                'location' => '154.8.143.104',
                'signLocation' => 'Signature1',
                'projectCode' => '003',
                'imageData' => 'iVBORw0'
            ];

            $signContract = [
                'contractNo' => 'ZL20190826000000123',
                'signInfos' => $signInfos
            ];

            $contractBody = [
                'head' => $headInfo,
                'signContract'  => $signContract
            ];

            //签署合同
            $this->signContract($headInfo,$channelId,$merId,$termId,$key,$url);


        }else if ($txCode == 3210) { // 合同查询 3210

            //创建合同
            $this->queryP($headInfo,$channelId,$merId,$termId,$key,$url);

        }else if($txCode == 3333) { // 合同下载 3210


            $data = [
                'channelId' => $channelId,
                'merId' => $merId,
                'termId' => $termId,
                'tradeTrace' => date('YmdHis') . mt_rand(1000, 9999), //请求流水，唯一，不包括特殊字符
                'version' => '1.0',
                'contractNo' => 'QT20221026000007656' //合同编号
            ];

            $sign = $this->getSign($data, $key);

            $data['sign'] = $sign;

            Log::info('==========请求参数:'.json_encode($data,JSON_UNESCAPED_UNICODE));

            $result = $this->postUrl($data, $download_url);

            Log::info('==========合同下载========');
            Log::info($result);
            if($result){
                $res_arr = json_decode($result, true);

                //成功
                if ($res_arr['resultCode'] == '00' ) {
                    //合同信息
                    $pdfFileData = $res_arr['pdfFileData'];

                    $img = base64_decode($pdfFileData);

                    $a = file_put_contents('./test.pdf', $img);//返回的是字节数  //放置路径的文件名后缀可跟需求改变而改变
                    print_r($a);

                }else{

                    return json_encode([
                        'status' => -1,
                        'data' => $res_arr['resultCode'],
                        'message' => $res_arr['resultMsg']
                    ]);

                }
            }else{
                Log::info('==========合同下载=== is null =====');
            }



        }


//        return json_encode($res_obj);
    }

    //创建合同 3201
    public function createContract($headInfo,$channelId,$merId,$termId,$key,$url){
        $note = [
            'checkCode' => '000000',
            'randomCode' => strval(rand(10, 99))
        ];

        $signInfos = [
            'userId' => '8D4CFF34AB1D289EE05311016B0A9A8A',
            'authorizationTime' => date('YmdHis', time()),// 授权时间：yyyyMMddHHmmss
//                'location' => '154.8.143.104',
            'location' => '127.0.0.1',
            'signLocation' => 's1;s2', // 签名域的标签值
            'projectCode' => '3787697707651', //项目编号
//                'isCopy' => 1, //是否抄送 0：不抄送；1：抄送；默认为0  如果抄送则isProxySign项无效
            'isProxySign' => 1, //是否代签  0：不代签；1：代签；默认为0。 取1时，可不用再调用 “签署合同”
        ];

        $textValueInfo = [
            "merName" => "某公司",  // 商户名/注册名
            "merLegal"  => "王某", //法人
            "merAddr"  => "30000",  //商户地址/注册地址
            "contractDateYear"  => "2022",    //合同日期 YYYY
            "contractDateMonth"  => "10",
            "contractDateDay"  => "01",
            "bizName"  => "10000",  //经营名称
            "businLic"  => "2019",  //统一 社会信用代码
            "bizAddr"  => "10000",  //经营地址
            "legalCode"  => "110101200101010018", //法人身份证号
            "legalPhone"  => "18800008888",  //法人电话
            "linkMan"  => "壹萬元",   //联系人
            "linkmanPhone"  => "01083526655", //联系人电话
            "accName"  => "成都", //账号名称
            "bankName"  => "10000", //开户行
            "account" => "壹萬元整",  //账号
            "chargeTypeDebit"  => "10000", //卡支付借记卡
            "chargeTypeDebitMax"  => "001", //卡支付借记卡封顶值
            "chargeTypeCredit"  => "壹萬元整", //卡支付贷记卡
            "chargeTypeUnionpayDebit"  => "10000", //银联二维码借记卡
            "chargeTypeUnionpayDebitMax"  => "10000", //银联二维码借记卡封顶值
            "chargeTypeUnionpayCredit"  => "10000", //银联二维码贷记卡
            "chargeTypeUnionpayDisDebit"  => "10000", //银联营销借记卡
            "chargeTypeUnionpayDisDebitMax"  => "10000", //银联营销借记卡封顶值
            "chargeTypeUnionpayDisCredit"  => "10000", //银联营销贷记卡
            "chargeTypeWxpay"  => "10000", //微信
            "chargeTypeAlipay"  => "10000", //支付宝
            "d1SettlementRatio"  => "10000", //D1结算按比例
            "d1SettlementFee"  => "10000", //D1结算按笔
            "d0SettlementRatio"  => "10000", //D0结算按比例
            "manager"  => "10000",  //客户经理
            "managerPhone"  => "10000", //客户经理电话
            "contractDate" => "10000", //合同日期 YYYY年MM月DD日
            "checkSameCard"  => "10000", //同卡支付
            "checkSettleT1"  => "成都支行", //T1结算
            "checkSettleD1"  => "成都支行", //D1结算
            "checkSettleD1Ratio"  => "成都支行", //D1结算按比例
            "checkSettleD1Fee"  => "成都支行", //D1结算按笔
            "checkSettleD0"  => "成都支行", //D0结算
        ];

        $createContract = [
            'templateId' => 'QT_18432',
            'isSign' => "1",  //签署类型 易生平台自身是否要签署该协议。0：抄送；1：签署；2：暂不签署；默认为0
            'signInfos' => array($signInfos),
            'textValueInfo' => $textValueInfo,
            'signLocation' => 's0'  // 易生签名域的标签值

        ];

        $contractBody = [
            'head' => $headInfo,
            'note'  => $note,
            'createContract' => $createContract
        ];

        $data = [
            'channelId' => $channelId,
            'merId' => $merId,
            'termId' => $termId,
            'tradeTrace' => date('YmdHis') . mt_rand(1000, 9999), //请求流水，唯一，不包括特殊字符
            'version' => '1.0',
//            'txCode' => strval($txCode),
            'txCode' => '3201',
            'contractBody' => json_encode($contractBody,JSON_UNESCAPED_UNICODE),
        ];

        $sign = $this->getSign($data, $key);

        $data['sign'] = $sign;

        Log::info('==========请求参数:'.json_encode($data,JSON_UNESCAPED_UNICODE));

        $result = $this->postUrl($data, $url);

        Log::info('==========创建合同========');
        Log::info($result);

        $res_arr = json_decode($result, true);

        //成功
        if ($res_arr['resultCode'] == '00' ) {

            $contractBody = json_decode($res_arr['contractBody'],true);

            //合同编号
            $contractNo = $contractBody['contract']['contractNo'];
            $contractState = $contractBody['contract']['contractState'];
            if($contractState == 1){ //0未完成；1已完成； 2已拒绝； 3正在签署； 4锁定待签 ；9已过期
                Log::info('==========创建合同==合同编号===contractNo:' . $contractNo);
            }else{

            }

            Log::info('==========创建合同==合同编号===contractInfo:' . json_encode($contractBody['contract']));


            return json_encode([
                'status' => 1,
                'contractNo' => $contractNo,
                'message' => 'success'
            ]);
        }else{

            return json_encode([
                'status' => -1,
                'data' => $res_arr['resultCode'],
                'message' => $res_arr['resultMsg']
            ]);

        }
    }


    //签署合同 3206
    public function signContract($headInfo,$channelId,$merId,$termId,$key,$url){

        $signInfo = [
            'userId' => '8D4CFF34AB1D289EE05311016B0A9A8A',
            'authorizationTime' => date('YmdHis', time()),// 授权时间：yyyyMMddHHmmss
//                'location' => '154.8.143.104',
            'location' => '127.0.0.1',
            'signLocation' => 's1;s2', // 签名域的标签值
            'projectCode' => '3787697707651', //项目编号
        ];

        $signContract = [
            'contractNo' => 'QT20221026000007656',
            'signInfo' => $signInfo,
        ];

        $contractBody = [
            'head' => $headInfo,
            'signContract' => $signContract
        ];


        $data = [
            'channelId' => $channelId,
            'merId' => $merId,
            'termId' => $termId,
            'tradeTrace' => date('YmdHis') . mt_rand(1000, 9999), //请求流水，唯一，不包括特殊字符
            'version' => '1.0',
//            'txCode' => strval($txCode),
            'txCode' => '3206',
            'contractBody' => json_encode($contractBody,JSON_UNESCAPED_UNICODE),
        ];

        $sign = $this->getSign($data, $key);

        $data['sign'] = $sign;

        Log::info('==========请求参数:'.json_encode($data,JSON_UNESCAPED_UNICODE));

        $result = $this->postUrl($data, $url);

        Log::info('==========签署合同========');
        Log::info($result);

        $res_arr = json_decode($result, true);

        //成功
        if ($res_arr['resultCode'] == '00' ) {

            $contractBody = json_decode($res_arr['contractBody'],true);

            //合同信息
            $signContract = $contractBody['signContract'];

            Log::info('==========签署合同==signContract:' . json_encode($signContract));

            return json_encode([
                'status' => 1,
                'contractNo' => $signContract,
                'message' => 'success'
            ]);
        }else{

            return json_encode([
                'status' => -1,
                'data' => $res_arr['resultCode'],
                'message' => $res_arr['resultMsg']
            ]);

        }

    }


    public function queryP($headInfo,$channelId,$merId,$termId,$key,$url){

        $contractNo = [
            'contractNo' => 'QT20221026000007656' //合同编号
        ];

        $contractBody = [
            'head' => $headInfo,
            'contract'  => $contractNo
        ];

        $data = [
            'channelId' => $channelId,
            'merId' => $merId,
            'termId' => $termId,
            'tradeTrace' => date('YmdHis') . mt_rand(1000, 9999), //请求流水，唯一，不包括特殊字符
            'version' => '1.0',
//            'txCode' => strval($txCode),
            'txCode' => '3210',
            'contractBody' => json_encode($contractBody,JSON_UNESCAPED_UNICODE),
        ];

        $sign = $this->getSign($data, $key);

        $data['sign'] = $sign;

        Log::info('==========请求参数:'.json_encode($data,JSON_UNESCAPED_UNICODE));

        $result = $this->postUrl($data, $url);

        Log::info('==========合同查询========');
        Log::info($result);

        $res_arr = json_decode($result, true);

        //成功
        if ($res_arr['resultCode'] == '00' ) {

            $contractBody = json_decode($res_arr['contractBody'],true);

            //合同信息
            $contractInfo = $contractBody['contract'];


            Log::info('==========合同查询==contractInfo:' . json_encode($contractInfo));


            return json_encode([
                'status' => 1,
                'contractNo' => $contractNo,
                'message' => 'success'
            ]);
        }else{

            return json_encode([
                'status' => -1,
                'data' => $res_arr['resultCode'],
                'message' => $res_arr['resultMsg']
            ]);

        }


    }

        //
    public function postUrl($params, $url)
    {

        $data = json_encode($params,JSON_UNESCAPED_UNICODE);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); //访问超时时间
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(json_decode($data)));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded;charset=UTF-8'));
        //本地忽略校验证书
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($curl);
        return $result;
    }
    /**
     * curl post java对接  传输数据流
     * */
    public function curlPost_java($data, $Url)
    {
        $ch = curl_init($Url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);//$data JSON类型字符串
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data)));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    //生成签名
    function getSign($params,$key)
    {
        ksort($params);
        $params = array_filter($params);
        $string = "";

        foreach ($params as $name => $value) {
            $string .= $name . '=' . $value . '&';
        }

        $string .= 'key=' . $key;

        Log::info('==========$string========' .$string);

        return strtoupper(md5($string));
    }


}
