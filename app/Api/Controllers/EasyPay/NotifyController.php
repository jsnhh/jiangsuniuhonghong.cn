<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/6/8
 * Time: 下午5:36
 */

namespace App\Api\Controllers\EasyPay;

use App\Api\Controllers\BaseController;
use App\Common\PaySuccessAction;
use App\Models\EasypayConfig;
use App\Models\EasypayStore;
use App\Models\MerchantWalletDetail;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\RefundOrder;
use App\Models\StorePayWay;
use App\Models\UserKeyUrl;
use App\Models\UserOrg;
use App\Models\UserWalletDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class NotifyController extends BaseController
{
    //易生 支付回调 微收单2.0
    public function pay_notify_url(Request $request)
    {
        try {
            $data = $request->all();
            Log::info('易生-支付回调');
            Log::info($data);
            if (isset($data) && !empty($data)) {
                if (isset($data['data']['oriOrgTrace'])) {
                    $out_trade_no = $data['data']['oriOrgTrace']; //商户订单号(原交易流水)
                    $trade_no = $data['data']['outTrace']; //系统订单号
                    $day = date('Ymd', time());
                    $table = 'orders_' . $day;
                    if (Schema::hasTable($table)) {
                        $order = DB::table($table)->where('out_trade_no', $out_trade_no)->first();
                    } else {
                        $order = Order::where('out_trade_no', $out_trade_no)->first();
//                        if (!$order) {
//                            $order = Order::where('trade_no', $trade_no)->first();
//                        }
                    }

                    //订单存在
                    if ($order) {
                        //系统订单未成功
                        if ($order->pay_status == '2') {
                            $pay_time = (isset($return['data']['timeEnd']) && !empty($return['data']['timeEnd'])) ? date('Y-m-d H:i:m', strtotime($return['data']['timeEnd'])) : date('Y-m-d H:i:m'); //支付完成时间，格式为 yyyyMMddhhmmss，如 2009年12月27日9点10分10秒表示为 20091227091010
                            $buyer_pay_amount = isset($data['data']['payerAmt']) ? ($data['data']['payerAmt'] / 100) : ''; //否,实付金额,单位分
                            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                            $buyer_id = '';
                            if ($data['data']['tradeCode'] != 'WUJS1') {
                                $buyer_id = $data['data']['payerId']; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
                            }
                            $in_data = [
                                'status' => '1',
                                'pay_status' => 1,
                                'pay_status_desc' => '支付成功',
                                'trade_no' => $trade_no,
                                'pay_time' => $pay_time,
                                'buyer_id' => $buyer_id,
                                'buyer_pay_amount' => $buyer_pay_amount,
                                'receipt_amount' => $buyer_pay_amount,
                            ];

                            $this->update_day_order($in_data, $out_trade_no);

                            if (strpos($out_trade_no, 'scan')) {

                            } else {
                                //支付成功后的动作
                                $data = [
                                    'ways_type' => $order->ways_type,
                                    'company' => $order->company,
                                    'ways_type_desc' => $order->ways_type_desc,
                                    'source_type' => '21000', //返佣来源
                                    'source_desc' => '易生支付', //返佣来源说明
                                    'total_amount' => $order->total_amount,
                                    'out_trade_no' => $order->out_trade_no,
                                    'other_no' => $order->other_no,
                                    'rate' => $order->rate,
                                    'fee_amount' => $order->fee_amount,
                                    'merchant_id' => $order->merchant_id,
                                    'store_id' => $order->store_id,
                                    'user_id' => $order->user_id,
                                    'config_id' => $order->config_id,
                                    'store_name' => $order->store_name,
                                    'ways_source' => $order->ways_source,
                                    'pay_time' => $pay_time,
                                    'device_id' => $order->device_id,
                                ];
                                if (Cache::has('console_out_trade_no_' . $out_trade_no)) {
                                    Log::info('--VibillOrderSuccess--easypay--检查是否操作---Cache.  ---console_out_trade_no_:' . $out_trade_no);
                                    return;
                                }
                                Cache::add('console_out_trade_no_' . $out_trade_no, '1', 1);
                                PaySuccessAction::action($data);
                            }


                            // $notifyData = [
                            //     'userId' => $order->user_id,
                            //     'outTradeNo' => $data['data']['oriOrgTrace'], //商户订单号(原交易流水)
                            //     'tradeNo' => $data['data']['outTrace'], //系统订单号
                            //     'payTime' => date('Y-m-d H:i:s', strtotime($data['data']['timeEnd'])),//支付成功时间
                            //     'payerAmt' => isset($data['data']['payerAmt']) ? ($data['data']['payerAmt'] / 100) : '', //否,实付金额,单位分
                            //     'tradeCode' => $data['data']['tradeCode'],//支付渠道
                            //     'payerId' => isset($data['data']['payerId']) ? ($data['data']['payerId']) : ''//用户标识  支付宝 微信返回
                            // ];
                            // $this->userNotify($notifyData);//代理支付回调
                        }
                    }
                }

                return 'ok';
            } else {
                Log::info('易生-支付回调-结果');
                Log::info($data);
            }

        } catch (\Exception $ex) {
            Log::info('易生支付回调-异步报错');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }
    }

    function pay_notify_url_u(Request $request)
    {
        $message = "支付成功";
        return view('success.success', compact('message'));
    }

    /**
     * 易生 商户异步通知 回调
     * 场景说明 所有异步接口,包括商户入件、补件、变更、终端新增、变更等,都可通过该接口获取商户处理结果；
     *          该接口会回一些主要的字段:funcInfo信息是所有的功能字段;sysInfo:包括内部商户号(merCode),终端名称(username),终端商户号(termMercode),终端终端号(termTermcode),渠道商户号(bangdingcommercode),渠道终端号(bangdingtermno)
     * 接口说明: 该接口在没有收到代理商应答结果时,会重新发送回调结果,最多发送3次;回调地址如果不送,则不会触发该回调接口,需要代理商主动调取查询接口获取结果; 回调地址上送有两种途径：
     *          (1)上送每个接口的回调地址(backUrl)字段;
     *          (2)由易生将回调地址配置到对应的机构参数中(优先取接口里送的回调地址,如果接口中没有送,则取机构参数中配置的)
     * @param Request $request
     * @return string
     */
    public function merchant_apply_notify_url(Request $request)
    {
        try {
            $data = $request->all();
            Log::info('易生-商户异步通知-回调');
            Log::info($data);
            $merTrace = isset($data['merTrace']) ? $data['merTrace'] : ''; //商户唯一标识
            if ($merTrace) {
                $recentMessType = isset($data['recentMessType']) ? $data['recentMessType'] : ''; //原报文类型,和操作的接口报文类型一致
                $operaTrace = isset($data['operaTrace']) ? $data['operaTrace'] : ''; //操作流水 原操作流水号
                $sysInfo = isset($data['sysInfo']) ? $data['sysInfo'] : ''; //终端信息 C,json,格式字符串
                $clientCode = isset($data['clientCode']) ? $data['clientCode'] : ''; //机构号
                $auditStatus = isset($data['auditStatus']) ? $data['auditStatus'] : ''; //审核状态 9-待处理；3-待审核；0-审核通过；X-审核失败；1-审核驳回
                $funcInfo = isset($data['funcInfo']) ? $data['funcInfo'] : ''; //功能信息 C,json,格式字符串
                $auditMsg = isset($data['auditMsg']) ? $data['auditMsg'] : ''; //审核说明

                if ($sysInfo) {
                    $sysInfo = json_decode($sysInfo, true);
                    $merCode = isset($sysInfo[0]['merCode']) ? $sysInfo[0]['merCode'] : ''; //内部商户号
                    $termMode = isset($sysInfo[0]['termMode']) ? $sysInfo[0]['termMode'] : ''; //企业（0）或者个体户（1）or个人（2）
                    $username = isset($sysInfo[0]['username']) ? $sysInfo[0]['username'] : ''; //终端名称
                    $termMercode = isset($sysInfo[0]['termMercode']) ? $sysInfo[0]['termMercode'] : ''; //终端商户号
                    $termTermcode = isset($sysInfo[0]['termTermcode']) ? $sysInfo[0]['termTermcode'] : ''; //终端终端号
                    $bangdingcommercode = isset($sysInfo[0]['bangdingcommercode']) ? $sysInfo[0]['bangdingcommercode'] : ''; //渠道商户号
                    $bangdingtermno = isset($sysInfo[0]['bangdingtermno']) ? $sysInfo[0]['bangdingtermno'] : ''; //渠道终端号
                }
                $alipayId = '';
                $wechatId = '';
                if ($funcInfo) {
                    $funcInfoArr = json_decode($funcInfo, true);
                    if (is_array($funcInfoArr)) {
                        foreach ($funcInfoArr as $values) {
                            if ($values['funcId'] == 2) { //支付宝
                                $alipayId = isset($values['alipayId']) ? $values['alipayId'] : '';
                            }
                            if ($values['funcId'] == 3) { //微信
                                $wechatId = isset($values['wechatId']) ? $values['wechatId'] : '';
                            }
                        }
                    }
                }

                $easypay_store = EasypayStore::where('mer_trace', $data['merTrace'])->first();
                if ($easypay_store) {
                    $update_data = [];
                    if ($auditStatus || $auditStatus === '0') $update_data['audit_status'] = $auditStatus;
                    if ($auditMsg) $update_data['audit_msg'] = $auditMsg;
                    if ($operaTrace) $update_data['opera_trace'] = $operaTrace;
                    if (isset($merCode)) $update_data['mer_code'] = $merCode;
                    if (isset($username)) $update_data['username'] = $username;
                    if (isset($termMercode)) $update_data['term_mercode'] = $termMercode;
                    if (isset($termTermcode)) $update_data['term_termcode'] = $termTermcode;
                    if (isset($bangdingcommercode)) $update_data['bang_ding_commer_code'] = $bangdingcommercode;
                    if (isset($bangdingtermno)) $update_data['bang_ding_ter_mno'] = $bangdingtermno;
                    if (isset($alipayId)) $update_data['alipay_id'] = $alipayId;
                    if (isset($wechatId)) $update_data['wechat_id'] = $wechatId;

                    if ($auditStatus === '0') {//审核通过
                        $status = 1;
                    } elseif ($auditStatus == 3 || $auditStatus == 9) {
                        $status = 2;
                    } elseif ($auditStatus == 1) { //1-审核驳回
                        $status = 3;
                    } else {
                        $status = 3;
                    }
                    $store_id = $easypay_store->store_id;
                    $res = StorePayWay::where('store_id', $store_id)
                        ->where('company', 'easypay')
                        ->update([
                            'status' => $status,
                            'status_desc' => $auditMsg
                        ]);
                    if (!$res) {
                        Log::info('易生商户异步通知回调-更新门店进件状态-失败');
                        Log::info($store_id);
                    }
                    if ($update_data) {
                        $res = $easypay_store->update($update_data);
                        if (!$res) {
                            Log::info('易生商户异步通知回调-更新失败');
                            Log::info($merTrace);
                        }
                    }

                    $config_id = $easypay_store->config_id;
                    $easypay_configs_obj = EasypayConfig::where('config_id', $config_id)->first();
                    if (!$easypay_configs_obj) {
                        $easypay_configs_obj = EasypayConfig::where('config_id', '1234')->first();
                    }
                    if (!$easypay_configs_obj && !$easypay_configs_obj->client_code && !$easypay_configs_obj->key) {
                        Log::info('商户入网-回调应答-易生支付未配置：');
                        Log::info($config_id);
                    }

                    $client_code = $easypay_configs_obj->client_code;
                    $key = $easypay_configs_obj->key;

                    $response_data = [
                        'version' => $data['version'],
                        'clientCode' => $client_code,
                        'retCode' => '0000',
                        'retMsg' => '成功'
                    ];
                    $response_data['MAC'] = $this->createSign($response_data, $key);
                    Log::info('易生商户入网-回调应答：');
                    Log::info($response_data);
                    return json_encode($response_data);
                } else {
                    Log::info('易生商户入网-回调-未匹配数据表');
                    Log::info($merTrace);
                }
            }
        } catch (\Exception $ex) {
            Log::info('易生-商户异步通知-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }
    }

    public function createSign($params, $importKey)
    {
        if (isset($params['fileName'])) {
            unset($params['fileName']);
        }
        if (isset($params['MAC'])) {
            unset($params['MAC']);
        }
        ksort($params);
        $params = array_filter($params);
        $string = "";

        foreach ($params as $name => $value) {
            $string .= $name . '=' . $value . '&';
        }

        $string .= 'key=' . $importKey;
//        Log::info('易生进件-加签参数');
//        Log::info($string);

        return strtoupper(md5($string));
    }

    //代理支付回调
    function userNotify($data)
    {
        $userKeyUrl = UserKeyUrl::where('user_id', $data['userId'])->first();
        $userKeyUrl = json_decode($userKeyUrl, true);
        if ($userKeyUrl) {
            $payment = new Payment();
            $payment->request($data, $userKeyUrl->notify_url);
        }
    }

}
