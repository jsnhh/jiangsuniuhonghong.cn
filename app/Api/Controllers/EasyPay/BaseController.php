<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/6/8
 * Time: 下午5:36
 */

namespace App\Api\Controllers\EasyPay;

use App\Api\Controllers\Config\EasyPayConfigController;
use App\Models\EasypayConfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class BaseController
{

//    public $url = 'https://notify-test.eycard.cn:7443/WorthTech_Access_AppPaySystemV2/apppayacc'; //测试线
    //public $url = 'https://open.eycard.cn:8443/WorthTech_Access_AppPaySystemV2/apppayacc'; //生产线 1.0

    //微收单2.0
//    public $scanPayUrl = 'https://t-wapi.bhecard.com:8443/standard/scanPay';//被扫支付地址 测试
//    public $jsapiUrl = 'https://t-wapi.bhecard.com:8443/standard/jsapi';//jsapi支付地址 测试
//    public $tradeQueryUrl = 'https://t-wapi.bhecard.com:8443/standard/tradeQuery';//订单查询地址 测试
//    public $closeUrl = 'https://t-wapi.bhecard.com:8443/standard/close';//关闭交易地址 测试
//    public $refundUrl = 'https://t-wapi.bhecard.com:8443/ledger/mposrefund';//退款地址 测试
//    public $appPayUrl = 'https://t-wapi.bhecard.com:8443/standard/app';//app支付 测试
//    public $getOpenidUrl = 'https://t-wapi.bhecard.com:8443//userId/qrGetUserId';//获取用户标识 测试

//易生  私钥  测试
//    public $key = 'MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAOSxfpYzsR5pKg0l
//pDb8rwhmJuaN06l+o9wf5FvKFoGCGt0yMmv/OUqsiFzuatZ7VV2hx/WavOVVubMr
//6YXD8/GINStC/6Hj7ih30XDjQLM1TRmBfhux+FO3LEYUULKbp/LZz+tXDU6QN6Hv
//w3CGNRhwdAV5Sq1d0REg7tbPuCZpAgMBAAECgYBigmpqvN2AogAxJZO08+dZkNcU
//3ObdzeU7fqlZOJW1N5vmyCacCpZdKjCbB0ASH5JIANYnX34as3CIU1QGAMBk63Rm
//7LYF7mPu3r+qYUhwt60FTynxktkKgvtr2slDNc+oF4noAKvzd3W752RWn/lp9rGv
//hkk8gljaBVa0A+4A+QJBAP6R2b7dDdmICLJFJV8EdKJjThndSQymS4MLT2p3ZdT2
//ULhMtLjbOy0FeQaBFEkF6cy8TquIvO76qwlNCPlO5qcCQQDl+mz7s7Pd6SaMcuAM
//PDnuCNqVZNspseP3WmnB3q/Bt3Ad+dTtoopVYgvrIAb+VxVkZ804k3c192wl9Bil
//VTxvAkEA4I4jdztjRim7EhXwezpg8AWNFT+femsOXRATA6VCzHJijdAL8qxgLLyR
//H22pSSjQLetFsgYyMtQnH1M2wUQaLQJBAKNDNa02NKF8C+a3AoENHF70oCBgegnO
//hLSr1dpQuVr/W7OcEWIl+qiEs0tW8EANGF2wJwtb/Mwt+vOypvBwYzcCQQCff7Cv
//+JHXx+voggV+lJqkBpFGjIV2axQ97n4JrBxwok7y42KEhMrKqVRAzXp7iGSxXfq7
//AkL0JSU7gSz+dg/3';

    //微收单2.0
    public $scanPayUrl = 'https://platform.eycard.cn:8443/standard/scanPay';//被扫支付地址 生产线
    public $jsapiUrl = 'https://platform.eycard.cn:8443/standard/jsapi';//jsapi支付地址 生产线
    public $tradeQueryUrl = 'https://platform.eycard.cn:8443/standard/tradeQuery';//订单查询地址 生产线
    public $closeUrl = 'https://platform.eycard.cn:8443/standard/close';//关闭交易地址 生产线
    public $refundUrl = 'https://platform.eycard.cn:6111/ledger/mposrefund';//退款地址 生产线
    public $appPayUrl = 'https://platform.eycard.cn:8443/standard/app';//app支付 生产线
    public $getOpenidUrl = 'https://platform.eycard.cn:8443/userId/qrGetUserId';//获取用户标识 生产线
    public $refundQueryUrl = 'https://platform.eycard.cn:6111/ledger/mposfindrefund';//退款查询地址 生产线
    public $key = 'MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKlqzImZ2ZlsbUhBCUFssQOsB4CazzClzF6vyLGek0MKMnxdaAAkmYRqQJyCGoApAL7G4wlY2MP1AfFi8YIkTQANWpypsnIIyqifjG3C/xTGTTTQuYksPGqDWciLG1/SXsMFgdYxYfQS1+DG2iV0FINFcPvZQL3AGMJAwKC5SMqnAgMBAAECgYA/llxK0ToBAddKpU5qBfeynyD5viFB6xj3+RZkxFYVdYBZeV2NoC8WqiKyG4sCy2ECOfgBDB3f/tfJ2A85f5J+c8emjUdazuSvlfpz0uM4+bKBoEOmHRpX+kPXLUbFAL/m4Xpem95un2Lw1ZnPgfe1oW+otuSHe4wBvex23fecwQJBAN/Rl4xW3E/aIZ7+CVrHpTlgg54ucTXlgYnhyLPQqB5tjS6cL6UOIeJY8O3RRHRpZEiAfI8fwSpaha+toQUjtNcCQQDBxsYT/frxFQKycDEwYXD00O6yZQ+rkX4AXD0GSsGdnpRtinXhgttu3HnSo0qoC/SI8k55LY4JimUCqGtaUw6xAkEAgbs374WNb8+M+15eR7s+/tH9mFttDYN5IVW5AtkoCXMQiXrIdxCI/Wz2QbKxzhQkp42qJT+PtKABq0Gl+pemwwJAYyYO4dnZlZNXju/C62tgPZPOHq+BJhtKqmqHuvlzOfATKD1pdZrcJkxfdPyiTXvYgl3g8zXw8EbEymhcjZnj8QJACw0tczQraXhlLSmjIJHpDND7fjkugv6FuG3qxYmYBcEfHFR8E/eUkk5Xl6cQWsNpHaPVl3D1F0Hmn7+YXMkAiA==';

    public $publicKey = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCVyaPxHsC5g7kwoZn1DbX3UqLGZ5VBEj0chrab8xMPJ8Ba0oVNnQnJHB4hAeZ98no1AK4zLZ2YS78FTOnT6Fs3M0nGPJrfCOSGNfTJ39Z8zh1KHKhWTywFx3a0bP7VYe+1DECC/NdLe3MyEPF0OjGq126K6a4TPOcUibx7oxlzYwIDAQAB';

    public $payment;
    public $signKey;
    public $wx_appid;

    function __construct()
    {
        $this->payment = new Payment();
        $easypay_config_obj = EasypayConfig::where('config_id', '1234')->first();
        if (!$easypay_config_obj) {
            return [
                'status' => '2',
                'message' => '易生支付参数未配置'
            ];
        }

        $this->wx_appid = $easypay_config_obj->wx_appid;
    }
    // 支付1.0
//    function __construct()
//    {
//        $this->payment = new Payment();
//
//        $sign_key = Cache::get('easypay_key');
//        if (!$sign_key) {
//            $easypay_config_obj = EasypayConfig::where('config_id', '1234')->first();
//            if (!$easypay_config_obj) {
//                return [
//                    'status' => '2',
//                    'message' => '易生支付参数未配置'
//                ];
//            }
//
//            if ($easypay_config_obj->getSignKey) {
//                $sign_key = $easypay_config_obj->getSignKey;
//            } else {
//                $params = [
//                    'channelid' => $easypay_config_obj->channel_id, //渠道编号
//                    'opt' => 'getSign', //操作类型
//                ];
//
//                $key = $easypay_config_obj->sign; //渠道密钥
//
//                $result = $this->payment->request($params, $key);
//                Log::info('易生支付-更新并获取签名密钥');
//                Log::info($result);
//                if (!$result) {
//                    return [
//                        'status' => '0',
//                        'message' => '易生,获取支付签名密钥失败'
//                    ];
//                }
//
//                $result_arr = json_decode($result, true);
//                if ($result_arr['resultcode'] == '00') {
//                    $sign_key = $result_arr['key'];
//                    $easypay_config_obj->update([
//                        'getSignKey' => $sign_key
//                    ]);
//                    $res = $easypay_config_obj->save();
//                    if (!$res) {
//                        Log::info('易生支付-更新签名密钥-写表失败');
//                    }
//                } else {
//                    return [
//                        'status' => '0',
//                        'message' => $result_arr['returnmsg']
//                    ];
//                }
//
//                Cache::forever('easypay_key', $sign_key);
//            }
//        }
//
//        $this->signKey = $sign_key;
    //   }
    public function getMessageByCode($code)
    {
        switch ($code) {
            case 'W5':
                return '非当日交易,不能撤销';
                break;
            case 'W0':
                return '上级渠道已关闭,请启用/切换其他可用渠道';
                break;
            case 'A0':
                return '签名错误;检查签名数据、签名密钥是否正确';
                break;
            case 'AA':
                return '待支付';
                break;
            case '98':
                return '系统超时或异常; 状态未确定,请根据通知/查询确定';
                break;
            case '96':
                return '系统异常';
                break;
            case '94':
                return '订单号重复';
                break;
            case '93':
                return '失败; 原交易是失败 交易,不能退货,如有争议,请进入人工退货流程';
                break;
            case '92':
                return '系统异常,暂时作为成功处理,请根据对账文件进行处理';
                break;
            case '64':
                return '超限额;累计的退货金额大于原笔交易金额';
                break;
            case '61':
                return '交易限额;商户收款限额';
                break;
            case '51':
                return '当日商户寸头不足(当日累计正向交易金额小于退货金额)';
                break;
            case '30':
                return '字段级别的错误,确定上送报文字段是否正确';
                break;
            case '25':
                return '找不到相关记录';
                break;
            case '08':
                return '原交易结果未确定; 需要先查询原交易确定交易结果后,再调用本接口';
                break;
            case '06':
                return '错误/失败';
                break;
            case '03':
                return '商户级别的错误;请确定请求信息（渠道编号/商户编号/终端编号）是否有效';
                break;
            case '01':
                return '系统错误或异常';
                break;
            case '00':
                return '成功';
                break;
            default:
                return 'Unknown error';
        }
    }

    //生成签名
    public function getSign($content, $privateKey)
    {
        $data = $this->formatBizQueryParaMap($content, false);

        $privateKey = "-----BEGIN RSA PRIVATE KEY-----\n" .
            wordwrap($privateKey, 64, "\n", true) .
            "\n-----END RSA PRIVATE KEY-----";

        $key = openssl_get_privatekey($privateKey);

        openssl_sign($data, $signature, $key, OPENSSL_ALGO_SHA256);
        openssl_free_key($key);
        $sign = base64_encode($signature);
        return $sign;
    }

    //验签
    public function verify($content, $sign)
    {
        $data = $this->formatBizQueryParaMap($content, false);

        $publicKey = "-----BEGIN PUBLIC KEY-----\n" .
            wordwrap($this->publicKey, 64, "\n", true) .
            "\n-----END PUBLIC KEY-----";

        $res = openssl_pkey_get_public($publicKey);

        $result = (bool)openssl_verify($data, base64_decode($sign), $res, OPENSSL_ALGO_SHA256);
        // openssl_free_key($res); //释放密钥资源
        // $sign = base64_encode($sign);
        return $result;
    }


    //组装签名函数
    function formatBizQueryParaMap($paraMap, $urlencode)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if ($urlencode) {
                $v = urlencode($v);
            }
            $buff .= $k . "=" . $v . "&";
        }
        $reqPar = '';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }
}
