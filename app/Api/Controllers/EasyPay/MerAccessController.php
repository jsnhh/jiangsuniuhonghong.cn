<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2021/3/22
 * Time: 17:43
 */
namespace App\Api\Controllers\EasyPay;

use App\Models\EasypayConfig;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MerAccessController
{
//    public $host_url = 'https://mtest.eycard.cn:4443/AG_MerchantManagementSystem_Core/agent/api/gen'; //进件,测试地址
//    public $host_url = 'https://180.168.215.67:4443/AG_MerchantManagementSystem_Core/agent/api/gen'; //进件,测试地址(开发文档)
    public $host_url = 'https://mms.eycard.cn/AG_MerchantManagementSystem_Core/agent/api/gen'; //生产线

//    public $upload_image = 'https://mtest.eycard.cn:4443/AG_MerchantManagementSystem_Core/agent/api/imgUpl'; //进件,图片上传,测试
//    public $upload_image = 'https://180.168.215.67:4443/AG_MerchantManagementSystem_Core/agent/api/imgUpl'; //进件,图片上传,测试(开发文档)
    public $upload_image = 'https://mms.eycard.cn/AG_MerchantManagementSystem_Core/agent/api/imgUpl'; //图片上传,生产线


    /**
     *  进件请求 1-成功 2-失败
     * @param $params
     * @param $key
     * @return array
     */
    public function execute($params, $key)
    {
        $sign = $this->createSign($params, $key);
        $params['MAC'] = $sign;
//        Log::info('易生进件-原始入参-params：');
//        Log::info($params);
//        $dataJsonStr = json_encode($params, JSON_UNESCAPED_SLASHES);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->host_url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
//            'Content-Type: application/json',
//            'Content-Type: application/x-www-form-urlencoded',
//            'Content-Length: ' . strlen($dataJsonStr),
            'charset=UTF-8'
        ]);
        $tmpInfo = curl_exec($curl); //返回json
        $responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($tmpInfo == NULL) {
            $errInfo = "call http err :" . curl_errno($curl) . " - " . curl_error($curl) ;
            curl_close($curl);
            return [
                'status' => 2,
                'message' => $errInfo,
                'code' => $responseCode
            ];
        } elseif($responseCode  != "200") {
            $errInfo = "call http err httpcode=" . $responseCode;
            curl_close($curl);
            return [
                'status' => 2,
                'message' => $errInfo,
                'code' => $responseCode
            ];
        }

        $info = json_decode($tmpInfo, JSON_UNESCAPED_UNICODE);
//        Log::info('易生进件-原始res：');
//        Log::info($info);

        return [
            'status' => 1,
            'data' => $info
        ];

    }


    /**
     * 上传图片 1-成功 2-失败
     * @param $params
     * @param $key
     * @return array
     */
    public function imageCurl($params, $key)
    {
        Log::info('上传图片-入参：');
        Log::info($params);
        $filePath = $params['fileName']; //绝对地址
        $sign = $this->createSign($params, $key);
        $params['MAC'] = $sign;

        $curl = curl_init();
        $imageArr = getimagesize($filePath);
        $fileType = $imageArr['mime'];
        $fileName = basename($filePath);
        $body = curl_file_create($filePath, $fileType, $fileName);
        $params['fileName'] = $body;
        curl_setopt($curl, CURLOPT_URL, $this->upload_image);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        Log::info('上传图片原始-params:');
        Log::info($params);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'charset=UTF-8',
            'Content-Type=multipart/form-data'
        ]);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); //本地忽略校验证书
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $tmpInfo = curl_exec($curl); //返回json
        $responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($tmpInfo == NULL) {
            $errInfo = "call http err :" . curl_errno($curl) . " - " . curl_error($curl) ;
            curl_close($curl);
            return [
                'status' => 2,
                'message' => $errInfo,
                'code' => $responseCode
            ];
        } elseif($responseCode  != "200") {
            $errInfo = "call http err httpcode=" . $responseCode;
            curl_close($curl);
            return [
                'status' => 2,
                'message' => $errInfo,
                'code' => $responseCode
            ];
        }

        $info = json_decode($tmpInfo, JSON_UNESCAPED_UNICODE);
        Log::info('易生上传图片-原始res：');
        Log::info($info);

        return [
            'status' => 1,
            'data' => $info
        ];
    }


    /**
     * 商户审核结果查询  0-审核通过 1-审核驳回 2-失败 3-待审核 9-待处理 X-审核失败
     *
     * @param string $data['clientCode']  机构号
     * @param string $data['merTrace']  O,商户唯一标识
     * @param string $data['operaTrace']  原操作流水号
     * @param string $data['key']  进件加密key
     * @return array
     */
    public function merchant_query($data)
    {
        try {
            $clientCode = $data['clientCode'] ?? '';
            $merTrace = $data['merTrace'] ?? '';
            $operaTrace = $data['operaTrace'] ?? '';
            $key = $data['key'] ?? '';

            $easypay_data = [
                'version' => '1.0',
                'clientCode' => $clientCode,
                'messageType' => 'QUERYAUDMER',
                'operaTrace' => $operaTrace
            ];
            if ($merTrace) $easypay_data['merTrace'] = $merTrace;
//            Log::info('易生支付-商户审核结果查询-params：');
//            Log::info($easypay_data);
            $res_arr = $this->execute($easypay_data, $key); //1-成功 2-失败
//            Log::info('易生支付-商户审核结果查询-result：');
//            Log::info($res_arr);
            //成功
            if ($res_arr['data']['retCode'] == '0000') {
                if (isset($res_arr['data']['auditStatus'])) {
                    $auditStatus = $res_arr['data']['auditStatus']; //9-待处理；3-待审核；0-审核通过；X-审核失败；1-审核驳回
                    $auditMsg = $res_arr['data']['auditMsg'] ?? '';

                    return [
                        'status' => $auditStatus,
                        'message' => $auditMsg ?? '审核未知',
                        'data' => $res_arr['data'],
                    ];
                }

                return [
                    'status' => 2,
                    'message' => $res_arr['data']['retMsg'] ?? (isset($res_arr['data']['retCode']) ? $this->getMessageByCode($res_arr['data']['retCode']) : '商户审核结果查询unknown'),
                    'data' => $res_arr['data'],
                ];
            } else {
                return [
                    'status' => 2,
                    'message' => $res_arr['data']['retMsg'] ?? (isset($res_arr['data']['retCode']) ? $this->getMessageByCode($res_arr['data']['retCode']) : '商户审核结果查询unknown'),
                    'data' => $res_arr['data']
                ];
            }
        } catch (\Exception $ex) {
            Log::info('易生支付-商户审核结果查询-error');
            Log::info($ex->getMessage() .'|'. $ex->getFile() .'|'. $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() .'|'. $ex->getFile() .'|'. $ex->getLine(),
            ];
        }
    }


    /**
     * 签名域(MAC)之外的所有数据元按照key的ascii顺序排序,并以&作为连接符拼接成待签名串(即key1=value1&key2=value2…),最后将自己的私钥拼接在报文待签名字符串最后(key=md5).然后对待签名串使用MD5做签名操作(签名字符串都是大写)
     * @param array $params 需要拼接的数组
     * @param string $importKey 进件私钥
     * @return string
     */
    public function createSign($params, $importKey)
    {
        if (isset($params['fileName'])) {
            unset($params['fileName']);
        }
        if (isset($params['MAC'])) {
            unset($params['MAC']);
        }
        ksort($params);
        $params = array_filter($params);
        $string = "";

        foreach ($params as $name => $value) {
            $string .= $name . '=' . $value . '&';
        }

        $string .= 'key=' . $importKey;
//        Log::info('易生进件-加签参数');
//        Log::info($string);

        return strtoupper(md5($string));
    }


    public function getMessageByCode($code)
    {
        switch ($code) {
            case '0000':
                return '操作成功'; break;
            case '1001':
                return '机构参数未加载'; break;
            case '1002':
                return '验签失败'; break;
            case '1003':
                return '参数缺失,信息不完整'; break;
            case '1004':
                return '参数非法'; break;
            case '1005':
                return '请求的功能不支持'; break;
            case '1006':
                return '系统异常'; break;
            case '1008':
                return '数据库操作失败'; break;
            case '1009':
                return '23：00~05：00 不允许访问'; break;
            case '1010':
                return 'IP地址非法,访问接口的 IP 没添加到白名单'; break;
            case '1011':
                return '版本号错误,版本号默认为 1.0'; break;
            case '1012':
                return '操作流水不能为空'; break;
            case '1013':
                return '商户唯一ID不能为空,merTrace 不能为空'; break;
            case '1014':
                return '未上送需要变更的信息,变更接口上送的信息为空'; break;
            case '1015':
                return '商户功能不能为空,入件或补件没有送需要开通的功能'; break;
            case '1016':
                return '解包错误'; break;
            case '6001':
                return '基本信息校验,主要是上传的内容的校验,包括 MCC、行号、地区码、所属项目等,具体错误会在应答描述中返回'; break;
            case '6002':
                return '鉴权错误;具体错误会在应答描述中返回'; break;
            case '6003':
                return '数据长度校验,具体错误会在应答描述中返回'; break;
            case '6004':
                return '非空校验,具体错误会在应答描述中返回'; break;
            case '6005':
                return '图片类型错误'; break;
            case '6006':
                return '图片不能为空'; break;
            case '6007':
                return '图片后缀错误'; break;
            case '6008':
                return '扣率校验错误,指扣率的值低于易生设置的最低值,具体见接口返回的描述'; break;
            case '6009':
                return '功能信息校验错误,指不满足功能里的特殊需求,具体见接口返回的描述'; break;
            case '6010':
                return '校验所开通的功能是否互斥,指所开通的功能里存在不能同时开通的,具体见接口返回的描述'; break;
            case '6012':
                return '该商户已审核;重复审核时出现'; break;
            case '6013':
                return '图片唯一ID错误'; break;
            case '6014':
                return '营业执照黑名单校验失败'; break;
            case '6015':
                return '开通的功能不符合经营类别,指该经营类别下不允许开通某些功能,具体见接口返回的描述'; break;
            case '6016':
                return '判断扣率值是否合法,校验扣率的格式是否正确,具体见接口返回的描述'; break;
            case '6018':
                return '商户唯一标识错误'; break;
            case '6019':
                return '该商户已注销'; break;
            case '6020':
                return '该商户处于商户变更中或注销中'; break;
            case '6021':
                return '操作流水已存在'; break;
            case '6022':
                return '终端号错误或终端已注销'; break;
            case '6023':
                return '内部终端号不能为空'; break;
            case '6024':
                return '银行卡收单功能未开通或已关闭'; break;
            case '6025':
                return '该商户未审核或商户唯一标识错误'; break;
            case '6026':
                return '该商户已开通三方分润功能'; break;
            case '6027':
                return '某些商户功能处于变更中,处于变更中的功能未审核前,不允许再变更'; break;
            case '6028':
                return '操作流水错误'; break;
            case '6029':
                return '参数类型错误'; break;
            case '6030':
                return '参数名称错误'; break;
            case '6031':
                return '功能必须上送实体终端;某些功能必须上送实体终端信息,如：银行卡收单'; break;
            case '6032':
                return '操作类型错误'; break;
            case '6033':
                return '是否小程序字段错误'; break;
            case '6034':
                return '渠道类型错误'; break;
            case '6035':
                return '上级渠道错误'; break;
            case '6036':
                return '该终端处于变更中'; break;
            case '6037':
                return '结算账户已注销'; break;
            case '6038':
                return '有多个结算账户的商户不允许变更'; break;
            case '6039':
                return '该结算账户处于变更中'; break;
            case '6040':
                return '该商户的功能变更信息已审核'; break;
            case '6041':
                return '未上传必须的图片'; break;
            case '6042':
                return '该商户所开通的功能不需要实体终端'; break;
            case '6100':
                return '商户审核驳回'; break;
            default: return '';
        }
    }


    //易生 上传图片  -1 系统错误 1-成功 2-失败
    public function upload_image($file_name, $image_type, $client_code, $key)
    {
        try {
            $img_url = explode('/', $file_name);
            $img_url = end($img_url);
            $img = public_path() . '/upload/images/' . $img_url;
//            if ($img) {
//                try {
//                    //压缩图片
//                    $img_obj = \Intervention\Image\Facades\Image::make($img);
//                    $img_obj->resize(800, 700);
//                    $img = public_path() . '/upload/s_images/' . $img_url;
//                    $img_obj->save($img);
//                } catch (\Exception $e) {
//                    Log::info('易生上传图片-压缩图片-error');
//                    Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
//                }
//            }

            $reqData = [
                'version' => '1.0', //版本号
                'fileName' => $img, //图片文件,该字段不参与签名
                'clientCode' => $client_code, //机构号,该字段由易生配置
                'picMode' => $image_type, //图片类型,01-法人身份证正面 ；02-法人身份证反面；03-商户联系人身份证正面；04-商户联系人身份证反面；05-营业执照；06-收银台；07-内部环境照；08-营业场所门头照；09-门牌号；10-协议；11-商户登记表正面；12-商户登记表反面；13-开户许可证；14-银行卡；15-清算授权书；16-定位照（选填）；17-手持身份证照片（选填）
            ];
            $result = $this->imageCurl($reqData, $key); //1-成功 2-失败
            if ($result['status'] == 1) {
                if (isset($result['data']) && isset($result['data']['retCode']) && $result['data']['retCode'] == '0000') {
                    return [
                        'status' => 1,
                        'data' => $result['data'],
                        'message' => $result['data']['retMsg'] ?? $result['message']
                    ];
                } else {
                    return [
                        'status' => 2,
                        'data' => $result['data'] ?? '',
                        'message' => $result['data']['retMsg'] ?? $result['message']
                    ];
                }
            }

            return [
                'status' => 2,
                'data' => $result['data'] ?? '',
                'message' => $result['message']
            ];
        } catch (\Exception $ex) {
            Log::info('易生支付-图片上传');
            Log::info($ex->getMessage() .'|'. $ex->getFile() .'|'. $ex->getLine());
            return [
                'status' => -1,
                'message' => $ex->getMessage() .'|'. $ex->getFile() .'|'. $ex->getLine()
            ];
        }
    }
    

}
