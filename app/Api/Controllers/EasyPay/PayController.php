<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/6/8
 * Time: 下午5:36
 */

namespace App\Api\Controllers\EasyPay;


use App\Api\Controllers\Config\EasyPayConfigController;
use function EasyWeChat\Kernel\Support\get_client_ip;
use Illuminate\Support\Facades\Log;

class PayController extends BaseController
{
    //统一下单支付1.0  -1 系统错误 0-其他 1-成功
//    public function scan_pay22($data)
//    {
//        try {
//            $channel_id = $data['channel_id'] ?? '';
//            $out_trade_no = $data['out_trade_no'] ?? '';
//            $code = $data['code'] ?? '';
//            $total_amount = $data['total_amount'] ?? '';
//            $trade_amt = isset($total_amount) ? ($total_amount * 100) : '';
//            $term_id = $data['device_id'] ?? '';
//            $shop_name = $data['shop_name'] ?? '';
//            $notify_url = $data['notify_url'] ?? '';
//            $pay_type = $data['pay_type'] ?? '';
//            $mer_id = $data['mno'] ?? '';
//
//            if ($pay_type == 'alipay') {
//                $opt = 'apPreOrder';
//            } elseif ($pay_type == 'weixin') {
//                $opt = 'wxPreOrder';
//            } elseif ($pay_type == 'unionpay') {
//                $opt = 'upPreOrder';
//            } else {
//                $opt = '';
//            }
//            $arr = ['location' => $data['location'],
//                'terminalip' => '154.8.143.104'];
//            $json = json_encode($arr); //转换成JSON数组
//            $terminalinfo = stripslashes($json);
//            $easypay_data = [
//                'channelid' => $channel_id, //是,渠道编号
//                'merid' => $mer_id, //是,商户编号
//                'termid' => $term_id, //是,终端编号
//                'tradetrace' => $out_trade_no, //是,商户订单号（控制在 50 位长度以内，确保在请求接口的系统中全局唯一；可以包含数字与字母；不能包含其他特殊字符）
//                'opt' => $opt, //是,操作类型（“wxPreOrder”：微信；“apPreOrder”：支付宝；“upPreOrder”：银联），根据实际情况选其一
//                'tradetype' => 'JSAPI', //是,支付方式（“NATIVE”：线下扫码/原生支付[微信不支持]；“JSAPI”：网页内支付）
//                'tradeamt' => $trade_amt, //是,交易金额（单位：分）
//                'body' => $shop_name, //是,商品或支付单简要描述（给用户看）
//                'openid' => $code, //tradetype=JSAPI，此参数必传,支付宝（买家支付宝用户 ID，参考文档 ）；微信（用户在商户sub_appid[支付的公众号] 下的唯一标识，参考文档 ）
//                'notifyurl' => $notify_url, //是,客户端接收微信支付成功通知的地址，通知报文请见“后台通知接口”
//                'returnurl' => ($pay_type == 'unionpay') ? '' : '', //否,交易成功后的前台接收地址。当提交的是银联行业码预下单交易（opt 为upPreOrder,为 tradetype 是 JSAPI），本节点必填???
//                'customerip' => get_client_ip(), //否,持卡人确认付款时的 IP 地址,用于防钓鱼。需注意:无代理情况通过‘REMOTE_ADDR’获取 IP 地址,有代理情况通过‘REAL_IP’获取真实 IP地址。当提交的是银联行业码预下单交易（opt 为 upPreOrder,为tradetype 是 JSAPI），本节点必填。
////                'handingfee' => $handingfee, //否,手续费（以分为单位,第一位是 D 或 C，表示借、贷）
//                'version' => '2', //否,填写的值为 2 时，交易成功后返回优惠信息
//                'terminalinfo' => $terminalinfo
//            ];
//            Log::info('易生支付-静态二维码-params：');
//            Log::info($easypay_data);
//            $res_obj = $this->payment->request($easypay_data, $this->signKey);
//            Log::info('易生支付-静态二维码-res：');
//            Log::info($res_obj);
//
//            //系统错误
//            if (!$res_obj) {
//                return [
//                    'status' => '-1',
//                    'message' => '系统错误'
//                ];
//            }
//
//            $res_arr = json_decode($res_obj, true);
//
//            //成功
//            if ($res_arr['resultcode'] == '00') {
//                return [
//                    'status' => '1',
//                    'message' => '返回成功',
//                    'code_url' => $res_arr['codeurl'],
//                    'data' => $res_arr['data']
//                ];
//            } //TODO：其他码暂不清楚
//            else {
//                return [
//                    'status' => '0',
//                    'message' => $res_arr['returnmsg'],
//                    'data' => $res_arr
//                ];
//            }
//        } catch (\Exception $ex) {
//            Log::info('易生支付-静态二维码');
//            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
//            return [
//                'status' => '-1',
//                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine(),
//            ];
//        }
//    }

    //统一下单支付2.0  -1 系统错误 0-其他 1-成功
    public function scan_pay22($data)
    {
        try {
            $channel_id = $data['channel_id'] ?? '';
            $out_trade_no = $data['out_trade_no'] ?? '';
            $code = $data['code'] ?? '';
            $total_amount = $data['total_amount'] ?? '';
            $trade_amt = isset($total_amount) ? ($total_amount * 100) : '';
            $term_id = $data['device_id'] ?? '';
            $notify_url = $data['notify_url'] ?? '';
            $pay_type = $data['pay_type'] ?? '';
            $mer_id = $data['mno'] ?? '';

            if ($pay_type == 'alipay') {
                $opt = 'WAJS2';
            } elseif ($pay_type == 'weixin') {
                $opt = 'WTJS1';
            } elseif ($pay_type == 'unionpay') {
                $opt = 'WUJS1';
            } else {
                $opt = '';
            }
            $terminalinfo = ['location' => $data['location'],
                'terminalip' => '154.8.143.104'];
            $appendData = [
                'terminalinfo' => $terminalinfo
            ];
            $data = [
                'tradeCode' => $opt,
                'tradeAmt' => $trade_amt,
                'orderInfo' => 'scanPay',
                'orgBackUrl' => $notify_url,
                'wxSubAppid' => $this->wx_appid,
                'payerId' => $code
            ];
            $sign = $this->getSign($data, $this->key);
            $easypay_data = [
                'orgId' => $channel_id,
                'orgMercode' => $mer_id,
                'orgTermno' => $term_id,
                'orgTrace' => $out_trade_no,
                'sign' => $sign,
                'signType' => 'RSA2',
                'appendData' => $appendData,
                'data' => $data
            ];
            Log::info('易生支付-静态二维码-params：');
            Log::info($easypay_data);
            $res_obj = $this->payment->request($easypay_data, $this->jsapiUrl);
            Log::info('易生支付-静态二维码-res：');
            Log::info($res_obj);

            //系统错误
            if (!$res_obj) {
                return [
                    'status' => '-1',
                    'message' => '系统错误'
                ];
            }

            $res_arr = json_decode($res_obj, true);
            //成功
            if ($res_arr['sysRetcode'] != '000000') {
                return [
                    'status' => '0',
                    'message' => $res_arr['sysRetmsg']
                ];
            } else {
                if ($res_arr['data']['finRetcode'] == '00') {
                    return [
                        'status' => '1',
                        'message' => '交易成功',
                        'data' => $res_arr['data'],
                    ];
                } elseif ($res_arr['data']['finRetcode'] == '99') {
                    return [
                        'status' => '2',
                        'message' => '待支付',
                        'data' => $res_arr['data'],
                    ];
                } else {
                    return [
                        'status' => '0',
                        'message' => '支付失败',
                        'data' => $res_arr['data'],
                    ];
                }

            }
        } catch (\Exception $ex) {
            Log::info('易生支付-静态二维码');
            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine(),
            ];
        }
    }

//    //被扫(付款码支付1.0) -1 系统错误 0-其他 1-成功 2-待支付
//    public function scan_pay($data)
//    {
//        try {
//            $channel_id = $data['channel_id'] ?? '';
//            $mer_id = $data['mno'] ?? '';
//            $term_id = $data['device_id'] ?? '';
//            $out_trade_no = $data['out_trade_no'] ?? '';
//            $total_amount = $data['total_amount'] ?? '';
//            $trade_amt = isset($total_amount) ? intval($total_amount * 100) : '';
//            $code = $data['code'] ?? '';
//            $shop_name = $data['shop_name'] ?? '';
//            $arr = ['location' => $data['location'],
//                'terminalip' => '154.8.143.104'];
//            $json = json_encode($arr); //转换成JSON数组
//
//            $terminalinfo = stripslashes($json);
//            $easypay_data = [
//                'channelid' => $channel_id, //是,渠道编号
//                'merid' => $mer_id, //是,商户编号
//                'termid' => $term_id, //是,终端编号
//                'tradetrace' => $out_trade_no, //是,商户订单号（控制在 50 位长度以内，确保在请求接口的系统中全局唯一；可以包含数字与字母；不能包含其他特殊字符）
//                'opt' => 'scanPay', //是,操作类型（scanPay）
//                'tradeamt' => $trade_amt, //是,交易金额（单位：分）
//                'authcode' => $code, //是,扫码支付授权码，设备读取用户微信、支付宝、银联钱包中的条码或者二维码信息
//                'body' => $shop_name, //是,商品或支付单简要描述（给用户看）
//                'version' => '2', //否,填写的值为 2 时，交易成功后返回优惠信息
////                'handingfee' => $handingfee, //否,手续费（以分为单位,第一位是 D 或 C，表示借、贷）
//                'terminalinfo' => $terminalinfo
//            ];
//            Log::info('易生支付-被扫2-params：');
//            Log::info($easypay_data);
//            $res_obj = $this->payment->request($easypay_data, $this->signKey);
//            Log::info('易生支付-被扫2-res：');
//            Log::info($res_obj);
//
//            //系统错误
//            if (!$res_obj) {
//                return [
//                    'status' => '-1',
//                    'message' => '系统错误',
//                ];
//            }
//
//            $res_arr = json_decode($res_obj, true);
//
//            //成功
//            if ($res_arr['resultcode'] == '00') {
//                return [
//                    'status' => '1',
//                    'message' => $res_arr['returnmsg'] ?? '交易成功',
//                    'data' => $res_arr,
//                ];
//            } //待支付
//            elseif ($res_arr['resultcode'] == 'AA') {
//                return [
//                    'status' => '2',
//                    'message' => '待支付',
//                    'data' => $res_arr,
//                ];
//            } //TODO：其他码暂不清楚
//            else {
//                return [
//                    'status' => '0',
//                    'message' => $res_arr['returnmsg'] ?? (isset($res_arr['resultcode']) ? $this->getMessageByCode($res_arr['resultcode']) : 'unknown'),
//                    'data' => $res_arr
//                ];
//            }
//        } catch (\Exception $ex) {
//            Log::info('易生-付款码支付-被扫-error');
//            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
//            return [
//                'status' => '-1',
//                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine(),
//            ];
//        }
//    }
    //被扫(付款码支付2.0) -1 系统错误 0-其他 1-成功 2-待支付
    public function scan_pay($data)
    {
        try {
            $channel_id = $data['channel_id'] ?? '';
            $mer_id = $data['mno'] ?? '';
            $term_id = $data['device_id'] ?? '';
            $out_trade_no = $data['out_trade_no'] ?? '';
            $total_amount = $data['total_amount'] ?? '';
            $trade_amt = isset($total_amount) ? intval($total_amount * 100) : '';
            $code = $data['code'] ?? '';
            $patnerSettleFlag = $data['patnerSettleFlag'] ?? '';
            $store_name = $data['store_name'] ?? '';
            $terminalinfo = [
                'location' => $data['location'],
                'terminalip' => '154.8.143.104'
            ];
            if ($store_name == '') {
                $orderInfo = 'qrPay';
            } else {
                $orderInfo = $store_name;
            }
            $wxSubAppid = $data['subAppid'] ?? '';
            $key = $data['key'] ?? '';
            $data = [
                'authCode' => $code,
                'tradeAmt' => $trade_amt,
                'wxSubAppid' => $wxSubAppid,
                'orderInfo' => $orderInfo,
                'patnerSettleFlag' => $patnerSettleFlag,
                'delaySettleFlag' => $patnerSettleFlag,
                'splitSettleFlag' => '0'//分账使用0=不分账1=分账
            ];
            $appendData = [
                'terminalinfo' => $terminalinfo,
            ];
            $sign = $this->getSign($data, $key);
            $easypay_data = [
                'orgId' => $channel_id,
                'orgMercode' => $mer_id,
                'orgTermno' => $term_id,
                'orgTrace' => $out_trade_no,
                'sign' => $sign,
                'signType' => 'RSA2',
                'appendData' => $appendData,
                'data' => $data
            ];
            Log::info('易生支付-被扫2-params：');
            Log::info($easypay_data);
            $res_obj = $this->payment->request($easypay_data, $this->scanPayUrl);
            Log::info('易生支付-被扫2-res：');
            Log::info($res_obj);

            //系统错误
            if (!$res_obj) {
                return [
                    'status' => '-1',
                    'message' => '系统错误',
                ];
            }

            $res_arr = json_decode($res_obj, true);

            //成功
            if ($res_arr['sysRetcode'] != '000000') {
                return [
                    'status' => '0',
                    'message' => $res_arr['sysRetmsg']
                ];
            } else {
                if ($res_arr['data']['finRetcode'] == '00') {
                    return [
                        'status' => '1',
                        'message' => '交易成功',
                        'data' => $res_arr['data'],
                    ];
                } elseif ($res_arr['data']['finRetcode'] == '99') {
                    return [
                        'status' => '2',
                        'message' => '待支付',
                        'data' => $res_arr['data'],
                    ];
                } else {
                    return [
                        'status' => '0',
                        'message' => '支付失败',
                        'data' => $res_arr['data'],
                    ];
                }

            }
        } catch (\Exception $ex) {
            Log::info('易生-付款码支付-被扫-error');
            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine(),
            ];
        }
    }


//    //查询订单1.0接口 -1 系统错误 0-其他 1-成功 2-下单失败 3-订单未支付
//    public function order_query($data)
//    {
//        try {
//            $channel_id = $data['channel_id'] ?? '';
//            $mer_id = $data['mer_id'] ?? '';
//            $device_id = $data['device_id'] ?? '';
//            $out_trade_no = $data['out_trade_no'] ?? '';
//
//            $select_data = [
//                'channelid' => $channel_id, //渠道编号
//                'merid' => $mer_id, //商戶编号
//                'termid' => $device_id, //终端编号
//                'tradetrace' => $out_trade_no, //原交易流水
//                'opt' => 'tradeQuery', //操作类型（tradeQuery）
//                //'tradetype' => '', //否,交易类型（“samecardQuery”：同名卡转出查询；其他查询为空）
//            ];
//            Log::info('易生支付-查询订单-params:');
//            Log::info($select_data);
//            $res_obj = $this->payment->request($select_data, $this->signKey);
//            Log::info('易生支付-查询订单-res:');
//            Log::info($res_obj);
//
//            //系统错误
//            if (!$res_obj) {
//                return [
//                    'status' => '-1',
//                    'message' => '系统错误'
//                ];
//            }
//
//            $res_arr = json_decode($res_obj, true);
//
//            //交易成功
//            if ($res_arr['resultcode'] == '00') {
//                return [
//                    'status' => '1',
//                    'message' => $res_arr['returnmsg'] ?? '查询成功',
//                    'data' => $res_arr,
//                ];
//            } elseif ($res_arr['resultcode'] == '06') {
//                return [
//                    'status' => '3',
//                    'message' => '下单失败',
//                    'data' => $res_arr,
//                ];
//            } elseif ($res_arr['resultcode'] == 'AA') {
//                return [
//                    'status' => '2',
//                    'message' => '待支付',
//                    'data' => $res_arr
//                ];
//            } else {
//                return [
//                    'status' => '0',
//                    'message' => $res_arr['returnmsg'] ?? (isset($res_arr['resultcode']) ? $this->getMessageByCode($res_arr['resultcode']) : 'unknown')
//                ];
//            }
//        } catch (\Exception $ex) {
//            Log::info('易生支付-交易查询-错误');
//            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
//            return [
//                'status' => '-1',
//                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine(),
//            ];
//        }
//    }
    //查询订单2.0接口 -1 系统错误 0-其他 1-成功 2-下单失败 3-订单未支付
    public function order_query($data)
    {
        try {
            $channel_id = $data['channel_id'] ?? '';
            $mer_id = $data['mer_id'] ?? '';
            $device_id = $data['device_id'] ?? '';
            $out_trade_no = $data['out_trade_no'] ?? '';
            $key = $data['key'] ?? '';
            $data = [
                'oriOrgTrace' => $out_trade_no
            ];
            $sign = $this->getSign($data, $key);
            $select_data = [
                'orgId' => $channel_id, //渠道编号
                'orgMercode' => $mer_id, //商戶编号
                'orgTermno' => $device_id, //终端编号
                'orgTrace' => date('YmdHis') . mt_rand(1000, 9999),
                'sign' => $sign,
                'data' => $data,
                'signType' => 'RSA2'
            ];
            Log::info('易生支付-查询订单-params:');
            Log::info($select_data);
            $res_obj = $this->payment->request($select_data, $this->tradeQueryUrl);
            Log::info('易生支付-查询订单-res:');
            Log::info($res_obj);

            //系统错误
            if (!$res_obj) {
                return [
                    'status' => '-1',
                    'message' => '系统错误'
                ];
            }

            $res_arr = json_decode($res_obj, true);

            //成功
            if ($res_arr['sysRetcode'] != '000000') {
                return [
                    'status' => '0',
                    'message' => $res_arr['sysRetmsg']
                ];
            } else {
                if ($res_arr['data']['finRetcode'] == '00') {
                    return [
                        'status' => '1',
                        'message' => '支付成功',
                        'data' => $res_arr['data'],
                    ];
                } elseif ($res_arr['data']['finRetcode'] == '99') {
                    return [
                        'status' => '2',
                        'message' => '订单未支付',
                        'data' => $res_arr['data'],
                    ];
                } else {
                    return [
                        'status' => '3',
                        'message' => '支付失败',
                        'data' => $res_arr['data'],
                    ];
                }

            }
        } catch (\Exception $ex) {
            Log::info('易生支付-交易查询-错误');
            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine(),
            ];
        }
    }

    //退款（支持对原交易的部分金额退货）1.0接口 -1 系统错误 0-其他 1-成功 2-失败
//    public function refund($data)
//    {
//        try {
//            $channel_id = $data['channel_id'] ?? '';
//            $mer_id = $data['mer_id'] ?? '';
//            $term_id = $data['term_id'] ?? '';
//            $out_trade_no = $data['out_trade_no'] ?? '';
//            $trade_no = $data['trade_no'] ?? '';
//            $refund_amount = isset($data['refund_amount']) ? intval($data['refund_amount'] * 100) : '';
//            $return_no = $trade_no . rand(1000, 9999);
//
//            $return_data = [
//                'channelid' => $channel_id, //渠道编号
//                'merid' => $mer_id, //商戶编号
//                'termid' => $term_id, //终端编号
//                'tradetrace' => $return_no, //商户订单号（控制在 50 位长度以内，确保在请求接口的系统中全局唯一；可以包含数字与字母；不能包含其他特殊字符）
//                'opt' => 'zwrefund', //操作类型（zwrefund）
//                'oriwtorderid' => $trade_no, //原交易的系统订单号
//                'tradeamt' => $refund_amount, //退货金额,以分为单位，不大于原交易金额与已成功退货金额之差
////                'notifymobileno' => '', //否,商户的通知手机 ,用于接收退货进展的通知短信（待完善）
////                'notifyusername' => '', //否,用于接收退货进展的通知短信（待完善）
//            ];
//            Log::info('易生支付-退款-params:');
//            Log::info($return_data);
////            Log::info($this->signKey);
//            $res_obj = $this->payment->request($return_data, $this->signKey);
//            Log::info('易生支付-退款-res:');
//            Log::info($res_obj);
//
//            //系统错误
//            if (!$res_obj) {
//                return [
//                    'status' => '-1',
//                    'message' => '系统错误'
//                ];
//            }
//
//            $res_arr = json_decode($res_obj, true);
//
//            //业务成功
//            if ($res_arr['resultcode'] == '00' || $res_arr['resultcode'] == 'W6') {
//                return [
//                    'status' => '1',
//                    'message' => $res_arr['returnmsg'] ?? '受理成功',
//                    'data' => $res_arr,
//                ];
//            } elseif ($res_arr['resultcode'] == '93') {
//                return [
//                    'status' => '2',
//                    'message' => $res_arr['returnmsg'] ?? '退款失败',
//                    'data' => $res_arr,
//                ];
//            } elseif ($res_arr['resultcode'] == 'W5') {
//                return [
//                    'status' => '2',
//                    'message' => $res_arr['returnmsg'] ?? '交易日期不符合要求,不能进行该操作',
//                    'data' => $res_arr,
//                ];
//            } else {
//                //退款失败，查询退款结果
////                $return_select_data = [
////                    'channel_id' => $channel_id,
////                    'out_trade_no' => $out_trade_no,
////                    'mer_id' => $mer_id,
////                    'device_id' => $term_id
////                ];
////                $return_select_arr = $this->order_query($return_select_data); //-1 系统错误 0-其他 1-成功 2-下单失败
////
////                if ($return_select_arr['status'] == '1') {
////                    return [
////                        'status' => '0',
////                        'message' => $return_select_arr['returnmsg'] ?? '查询成功',
////                        'data' => $return_select_arr
////                    ];
////                }
//
//                return [
//                    'status' => '0',
//                    'message' => $res_arr['returnmsg'] ?? '退款失败',
//                    'data' => $res_arr
//                ];
//            }
//        } catch (\Exception $ex) {
//            Log::info('易生支付-退款-错误');
//            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
//            return [
//                'status' => '-1',
//                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine(),
//            ];
//        }
//    }
    //退款（支持对原交易的部分金额退货）2.0接口  -1 系统错误 0-其他 1-成功 2-失败
    public function refund($data)
    {
        try {
            $channel_id = $data['channel_id'] ?? '';
            $mer_id = $data['mer_id'] ?? '';
            $term_id = $data['term_id'] ?? '';
            $out_trade_no = $data['out_trade_no'] ?? '';
            $key = $data['key'] ?? '';
            $refund_amount = isset($data['refund_amount']) ? intval($data['refund_amount'] * 100) : '';
            $data = [
                'oriOrgTrace' => $out_trade_no,
                'transAmt' => strval($refund_amount)
            ];
            $sign = $this->getSign($data, $key);
            $return_data = [
                'orgId' => $channel_id, //渠道编号
                'orgMerCode' => $mer_id, //商戶编号
                'orgTermNo' => $term_id, //终端编号
                'orgTrace' => substr($channel_id, 0, 4) . substr($channel_id, -4) . date('YmdHis', time()) . sprintf('%03d', rand(0, 999)),
                'sign' => $sign,
                'signType' => 'RSA2',
                'bizData' => $data
            ];
            Log::info('易生支付-退款-params:');
            Log::info($return_data);
//            Log::info($this->signKey);
            $res_obj = $this->payment->request($return_data, $this->refundUrl);
            Log::info('易生支付-退款-res:');
            Log::info($res_obj);

            //系统错误
            if (!$res_obj) {
                return [
                    'status' => '-1',
                    'message' => '系统错误'
                ];
            }

            $res_arr = json_decode($res_obj, true);

            //成功
            if ($res_arr['sysRetCode'] != '000000') {
                return [
                    'status' => '0',
                    'message' => $res_arr['sysRetMsg']
                ];
            } else {
                if ($res_arr['bizData']['tradeRetCode'] == '00') {
                    return [
                        'status' => '1',
                        'message' => '退款成功',
                        'data' => $res_arr,
                    ];
                } else {
                    return [
                        'status' => '0',
                        'message' => '退款失败',
                        'data' => $res_arr,
                    ];
                }

            }
        } catch (\Exception $ex) {
            Log::info('易生支付-退款-错误');
            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine(),
            ];
        }
    }


    //交易撤销1.0接口(仅对当日交易可以撤销，日切时间为渠道服务器的 00:00) 中 -1 系统错误 0-其他 1-成功
//    public function order_close($data)
//    {
//        try {
//            $channel_id = $data['channel_id'] ?? '';
//            $mer_id = $data['mno'] ?? '';
//            $term_id = $data['device_id'] ?? '';
//            $out_trade_no = $data['out_trade_no'] ?? '';
//            $trade_no = $data['trade_no'] ?? '';
//            $return_no = $out_trade_no . rand(1000, 9999);
//
//            $easypay_data = [
//                'channelid' => $channel_id, //是,渠道编号
//                'merid' => $mer_id, //是,商户编号
//                'termid' => $term_id, //是,终端编号
//                'tradetrace' => $return_no, //是,商户订单号（控制在 50 位长度以内，确保在请求接口的系统中全局唯一；可以包含数字与字母；不能包含其他特殊字符）
//                'opt' => 'cancel', //是,操作类型（cancel）
//                'oriwtorderid' => $trade_no, //是,原交易的系统订单号；与 oritradetrace 字段可以二选一上送，如果都上送，以 oritradetrace 节点为准
//                'version' => '2', //否,填写的值为 2 时，交易成功后返回优惠信息
//            ];
//            Log::info('易生支付-交易撤销-params:');
//            Log::info($easypay_data);
//            $res_obj = $this->payment->request($easypay_data, $this->signKey);
//            Log::info('易生支付-交易撤销-res:');
//            Log::info($res_obj);
//
//            //系统错误
//            if (!$res_obj) {
//                return [
//                    'status' => '-1',
//                    'message' => '系统错误',
//                ];
//            }
//
//            $res_arr = json_decode($res_obj, true);
//
//            //成功
//            if ($res_arr['resultcode'] == '00') {
//                return [
//                    'status' => '1',
//                    'message' => $res_arr['returnmsg'] ?? '撤销成功',
//                    'data' => $res_arr,
//                ];
//            } //TODO：其他码暂不清楚
//            else {
//                return [
//                    'status' => '0',
//                    'message' => $res_arr['returnmsg'] ?? (isset($res_arr['resultcode']) ? $this->getMessageByCode($res_arr['resultcode']) : 'unknown'),
//                    'data' => $res_arr
//                ];
//            }
//        } catch (\Exception $ex) {
//            Log::info('易生支付-交易撤销-错误');
//            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
//            return [
//                'status' => '-1',
//                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine(),
//            ];
//        }
//    }

    //微收单2.0接口
    public function order_close($data)
    {
        try {
            $channel_id = $data['channel_id'] ?? '';
            $mer_id = $data['mno'] ?? '';
            $term_id = $data['device_id'] ?? '';
            $out_trade_no = $data['out_trade_no'] ?? '';
            $trade_no = $data['trade_no'] ?? '';
            $return_no = $out_trade_no . rand(1000, 9999);
            $data = [
                'oriOrgTrace' => $out_trade_no
            ];
            $sign = $this->getSign($data, $this->key);
            $easypay_data = [
                'orgId' => $channel_id, //渠道编号
                'orgMercode' => $mer_id, //商戶编号
                'orgTermno' => $term_id, //终端编号
                'orgTrace' => substr($channel_id, 0, 4) . substr($channel_id, -4) . date('YmdHis', time()) . sprintf('%03d', rand(0, 999)),//原交易流水
                'data' => $data,
                'sign' => $sign,
                'signType' => 'RSA2'
            ];
            Log::info('易生支付-交易撤销-params:');
            Log::info($easypay_data);
            $res_obj = $this->payment->request($easypay_data, $this->closeUrl);
            Log::info('易生支付-交易撤销-res:');
            Log::info($res_obj);

            //系统错误
            if (!$res_obj) {
                return [
                    'status' => '-1',
                    'message' => '系统错误',
                ];
            }

            $res_arr = json_decode($res_obj, true);

            if ($res_arr['sysRetcode'] != '000000') {
                return [
                    'status' => '0',
                    'message' => $res_arr['sysRetmsg']
                ];
            } else {
                if ($res_arr['data']['finRetcode'] == '00') {
                    return [
                        'status' => '1',
                        'message' => '撤销成功',
                        'data' => $res_arr['data'],
                    ];
                } elseif ($res_arr['data']['finRetcode'] == '99') {
                    return [
                        'status' => '2',
                        'message' => '订单未支付',
                        'data' => $res_arr['data'],
                    ];
                } else {
                    return [
                        'status' => '0',
                        'message' => '撤销失败',
                        'data' => $res_arr['data'],
                    ];
                }

            }
        } catch (\Exception $ex) {
            Log::info('易生支付-交易撤销-错误');
            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine(),
            ];
        }
    }


    //生成动态二维码(主扫支付) -1 系统错误 0-其他 1-成功 2-待支付
    public function send_qr($data)
    {
        try {
//            $channel_id = $data['channel_id'] ?? '';
//            $mer_id = $data['mno'] ?? '';
//            $term_id = $data['device_id'] ?? '';
//            $out_trade_no = $data['out_trade_no'] ?? '';
//            $total_amount = $data['total_amount'] ?? '';
//            $trade_amt = isset($total_amount) ? intval($total_amount * 100) : '';
//            $shop_name = $data['shop_name'] ?? '';
//            $code = $data['code'] ?? '';
//            $notifyurl = $data['notifyurl'] ?? '';
//            $pay_type = $data['pay_type'] ?? '';
//
//            if ($pay_type == 'alipay') {
//                $opt = 'apPreOrder';
//            } elseif ($pay_type == 'weixin') {
//                $opt = 'wxPreOrder';
//            } elseif ($pay_type == 'unionpay') {
//                $opt = 'upPreOrder';
//            }
//            $arr = ['location' => $data['location'],
//                'terminalip' => '154.8.143.104'];
//            $json = json_encode($arr); //转换成JSON数组
//
//            $terminalinfo = stripslashes($json);
//            $easypay_data = [
//                'channelid' => $channel_id, //是,渠道编号
//                'merid' => $mer_id, //是,商户编号
//                'termid' => $term_id, //是,终端编号
//                'tradetrace' => $out_trade_no, //是,商户订单号（控制在 50 位长度以内，确保在请求接口的系统中全局唯一；可以包含数字与字母；不能包含其他特殊字符）
//                'opt' => $opt, //是,操作类型（“apPreOrder”：支付宝；“upPreOrder”：银联），根据实际情况选其一
//                'tradetype' => 'NATIVE', //支付方式（“NATIVE”）
//                'tradeamt' => $trade_amt, //是,交易金额（单位：分）
////                'handingfee' => $handingfee, //否,手续费（以分为单位,第一位是 D 或 C，表示借、贷）
//                'body' => $shop_name, //是,商品或支付单简要描述（给用户看）
//                'openid' => $code, //支付宝（买家支付宝用户 ID，参考文档 ），上送支付宝用户 ID 时，当前订单仅当前支付宝用户支付
//                'notifyurl' => $notifyurl, //客户端接收微信支付成功通知的地址，通知报文请见“后台通知接口”
//                'version' => '2', //否,填写的值为 2 时，交易成功后返回优惠信息
//                'terminalinfo' => $terminalinfo
//            ];
//            Log::info('易生支付-主扫支付-params:');
//            Log::info($easypay_data);
//            $res_obj = $this->payment->request($easypay_data, $this->signKey);
//            Log::info('易生支付-主扫支付-res:');
//            Log::info($res_obj);
//
//            //系统错误
//            if (!$res_obj) {
//                return [
//                    'status' => '-1',
//                    'message' => '系统错误',
//                ];
//            }
//
//            $res_arr = json_decode($res_obj, true);
//
//            //成功
//            if ($res_arr['resultcode'] == '00') {
//                return [
//                    'status' => '1',
//                    'message' => $res_arr['returnmsg'] ?? '交易成功',
//                    'code_url' => $res_arr['codeurl'] ?? '', //tradetype 为 NATIVE 时有返回，此参数可直接生成二维码展示出来进行扫码支付；tradetype 为 H5 时有返回：拉起微信支付收银台的中间页面，可通过访问该 url 来拉起微信客户端，完成支付
//                    'data' => $res_arr,
//                ];
//            } //待支付
//            elseif ($res_arr['resultcode'] == 'AA') {
//                return [
//                    'status' => '2',
//                    'message' => '待支付',
//                    'data' => $res_arr,
//                ];
//            } //TODO：其他码暂不清楚
//            else {
//                return [
//                    'status' => '0',
//                    'message' => $res_arr['returnmsg'] ?? (isset($res_arr['resultcode']) ? $this->getMessageByCode($res_arr['resultcode']) : 'unknown'),
//                    'data' => $res_arr
//                ];
//            }
            return $this->scan_pay22($data);
        } catch (\Exception $ex) {
            Log::info('易生支付-主扫支付-错误');
            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine(),
            ];
        }
    }

//
//    //静态码提交-公共(公众号/服务窗支付) 1.0接口 -1 系统错误 0-其他 1-成功
//    public function qr_submit($data)
//    {
//        try {
//            $channel_id = $data['channel_id'] ?? '';
//            $mer_id = $data['mno'] ?? '';
//            $term_id = $data['device_id'] ?? '';
//            $out_trade_no = $data['out_trade_no'] ?? '';
//            $pay_type = $data['pay_type'] ?? '';
//            $total_amount = $data['total_amount'] ?? '';
//            $trade_amt = isset($total_amount) ? intval($total_amount * 100) : '';
//            $shop_name = $data['shop_name'] ?? '';
//            $code = $data['code'] ?? '';
//            $notify_url = $data['notify_url'] ?? '';
//
//            if ($pay_type == 'ALIPAY') {
//                $opt = 'apPreOrder';
//            } else {
//                $opt = 'wxPreOrder';
//            }
//            $arr = ['location' => $data['location'],
//                'terminalip' => '154.8.143.104'];
//            $json = json_encode($arr); //转换成JSON数组
//
//            $terminalinfo = stripslashes($json);
//            $easypay_data = [
//                'channelid' => $channel_id, //是,渠道编号
//                'merid' => $mer_id, //是,商户编号
//                'termid' => $term_id, //是,终端编号
//                'tradetrace' => $out_trade_no, //是,商户订单号（控制在 50 位长度以内，确保在请求接口的系统中全局唯一；可以包含数字与字母；不能包含其他特殊字符）
//                'opt' => $opt, //是,操作类型（“wxPreOrder”：微信；“apPreOrder”：支付宝），根据实际情况选其一
//                'tradetype' => 'JSAPI', //是,支付方式（“JSAPI”）
//                'tradeamt' => $trade_amt, //是,交易金额（单位：分）
//                'body' => $shop_name, //是,商品或支付单简要描述（给用户看）
//                'openid' => $code, //是,支付宝（买家支付宝用户 ID，参考文档 ）；微信（用户在商户sub_appid[支付的公众号] 下的唯一标识，参考文档 ）
//                'notifyurl' => $notify_url, //是,客户端接收微信支付成功通知的地址，通知报文请见“后台通知接口”
////                'handingfee' => $handingfee, //否,手续费（以分为单位,第一位是 D 或 C，表示借、贷）
//                'version' => '2', //否,填写的值为 2 时，交易成功后返回优惠信息
//                'terminalinfo' => $terminalinfo
//            ];
//            Log::info('易生支付-静态码提交-params：');
//            Log::info($easypay_data);
//            $res_obj = $this->payment->request($easypay_data, $this->signKey);
//            Log::info('易生支付-静态码提交-res：');
//            Log::info($res_obj);
//
//
//            //系统错误
//            if (!$res_obj) {
//                return [
//                    'status' => '-1',
//                    'message' => '系统错误',
//                ];
//            }
//
//            $res_arr = json_decode($res_obj, true);
//
//            //成功
//            if ($res_arr['resultcode'] == '00') {
//                return [
//                    'status' => '1',
//                    'message' => $res_arr['returnmsg'] ?? '交易成功',
//                    'data' => $res_arr,
//                ];
//            } //待支付
//            elseif ($res_arr['resultcode'] == 'AA') {
//                return [
//                    'status' => '2',
//                    'message' => $res_arr['returnmsg'] ?? '待支付',
//                    'data' => $res_arr,
//                ];
//            }//TODO：其他码暂不清楚
//            else {
//                return [
//                    'status' => '0',
//                    'message' => $res_arr['returnmsg'] ?? (isset($res_arr['resultcode']) ? $this->getMessageByCode($res_arr['resultcode']) : 'unknown'),
//                    'data' => $res_arr
//                ];
//            }
//        } catch (\Exception $ex) {
//            Log::info('易生支付-静态码提交-错误');
//            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
//            return [
//                'status' => '-1',
//                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine(),
//            ];
//        }
//    }
    //静态码提交-公共(公众号/服务窗支付) 微收单2.0接口 -1 系统错误 0-其他 1-成功
    public function qr_submit($data)
    {
        try {
            $channel_id = $data['channel_id'] ?? '';
            $mer_id = $data['mno'] ?? '';
            $term_id = $data['device_id'] ?? '';
            $out_trade_no = $data['out_trade_no'] ?? '';
            $pay_type = $data['pay_type'] ?? '';
            $total_amount = $data['total_amount'] ?? '';
            $trade_amt = isset($total_amount) ? intval($total_amount * 100) : '';
            $code = $data['code'] ?? '';
            $notify_url = $data['notify_url'] ?? '';
            $patnerSettleFlag = $data['patnerSettleFlag'] ?? '';
            $wxSubAppid = $data['subAppid'] ?? '';
            $store_name = $data['store_name'] ?? '';
            $key = $data['key'] ?? '';
            if ($pay_type == 'ALIPAY') {
                $opt = 'WAJS1';
                $this->wx_appid = $wxSubAppid;
            } else if ($pay_type == 'WECHAT') {
                $opt = 'WTJS1';
                $this->wx_appid = $wxSubAppid;
            } else if ($pay_type == 'UNIONPAY') {
                $opt = 'WUJS1';
                $this->wx_appid = $wxSubAppid;
            } else if ($pay_type == 'wx_applet') {
                $opt = 'WTJS2';
                $this->wx_appid = $wxSubAppid;//小程序支付使用小程序appid wx0a6d4511ac3f8890
            }
            $terminalinfo = [
                'location' => $data['location'],
                'terminalip' => '154.8.143.104'
            ];
            $appendData = [
                'terminalinfo' => $terminalinfo
            ];
            if ($store_name == '') {
                $orderInfo = 'qrPay';
            } else {
                $orderInfo = $store_name;
            }

            $data = [
                'tradeCode' => $opt,
                'tradeAmt' => $trade_amt,
                'orderInfo' => $orderInfo,
                'orgBackUrl' => $notify_url,
                'wxSubAppid' => $this->wx_appid,
                'payerId' => $code,
                'patnerSettleFlag' => $patnerSettleFlag,
                'delaySettleFlag' => $patnerSettleFlag,
                'splitSettleFlag' => '0'//分账使用0=不分账1=分账
            ];
            if ($opt == 'WUJS1') {
                $data['orgFrontUrl'] = url('/api/easypay/pay_notify_url_u');
            }
            $sign = $this->getSign($data, $key);
            $easypay_data = [
                'orgId' => $channel_id,
                'orgMercode' => $mer_id,
                'orgTermno' => $term_id,
                'orgTrace' => $out_trade_no,
                'sign' => $sign,
                'signType' => 'RSA2',
                'appendData' => $appendData,
                'data' => $data
            ];
            Log::info('易生支付-静态码提交-params：');
            Log::info($easypay_data);
            $res_obj = $this->payment->request($easypay_data, $this->jsapiUrl);
            Log::info('易生支付-静态码提交-res：');
            Log::info($res_obj);


            //系统错误
            if (!$res_obj) {
                return [
                    'status' => '-1',
                    'message' => '系统错误',
                ];
            }

            $res_arr = json_decode($res_obj, true);

            //成功
            if ($res_arr['sysRetcode'] != '000000') {
                return [
                    'status' => '0',
                    'message' => $res_arr['sysRetmsg']
                ];
            } else {
                if ($res_arr['data']['finRetcode'] == '00') {
                    return [
                        'status' => '1',
                        'message' => '交易成功',
                        'data' => $res_arr['data'],
                    ];
                } elseif ($res_arr['data']['finRetcode'] == '99') {
                    return [
                        'status' => '1',
                        'message' => '待支付',
                        'data' => $res_arr['data'],
                    ];
                } else {
                    return [
                        'status' => '0',
                        'message' => '支付失败',
                        'data' => $res_arr['data'],
                    ];
                }

            }
        } catch (\Exception $ex) {
            Log::info('易生支付-静态码提交-错误');
            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine(),
            ];
        }
    }

    function unionPayApp($data)
    {
        try {
            $channel_id = $data['channel_id'] ?? '';
            $mer_id = $data['mno'] ?? '';
            $term_id = $data['device_id'] ?? '';
            $out_trade_no = $data['out_trade_no'] ?? '';
            $total_amount = $data['total_amount'] ?? '';
            $trade_amt = isset($total_amount) ? intval($total_amount * 100) : '';
            $appendData = [];
            $data = [
                'tradeAmt' => $trade_amt,
                'orderInfo' => 'unionPay',
                'wxSubAppid' => $this->wx_appid,
            ];
            $sign = $this->getSign($data, $this->key);
            $easypay_data = [
                'orgId' => $channel_id,
                'orgMercode' => $mer_id,
                'orgTermno' => $term_id,
                'orgTrace' => $out_trade_no,
                'sign' => $sign,
                'signType' => 'RSA2',
                'appendData' => $appendData,
                'data' => $data
            ];
            Log::info('易生支付-app提交-params：');
            Log::info($easypay_data);
            $res_obj = $this->payment->request($easypay_data, $this->appPayUrl);
            Log::info('易生支付-app提交-res：');
            Log::info($res_obj);


            //系统错误
            if (!$res_obj) {
                return [
                    'status' => '-1',
                    'message' => '系统错误',
                ];
            }

            $res_arr = json_decode($res_obj, true);

            //成功
            if ($res_arr['sysRetcode'] != '000000') {
                return [
                    'status' => '0',
                    'message' => $res_arr['sysRetmsg']
                ];
            } else {
                if ($res_arr['data']['finRetcode'] == '00') {
                    return [
                        'status' => '1',
                        'message' => '交易成功',
                        'data' => $res_arr['data'],
                    ];
                } elseif ($res_arr['data']['finRetcode'] == '99') {
                    return [
                        'status' => '2',
                        'message' => '待支付',
                        'data' => $res_arr['data'],
                    ];
                } else {
                    return [
                        'status' => '0',
                        'message' => '支付失败',
                        'data' => $res_arr['data'],
                    ];
                }

            }
        } catch (\Exception $ex) {
            Log::info('易生支付-app提交-错误');
            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine(),
            ];
        }
    }

    function qrGetUserId($data)
    {
        $appendData = ['orgDeviceIp' => '154.8.143.104'];
        $unData = [
            'qrAppUpIdentifier' => $data['qrAppUpIdentifier'],
            'qrUserAuthCode' => $data['userAuthCode'],
        ];
        $sign = $this->getSign($unData, $this->key);

        $easy_data = [
            'orgId' => $data['orgId'],
            'orgMercode' => $data['orgMercode'],
            'orgTermno' => $data['orgTermno'],
            'orgTrace' => substr($data['orgId'], 0, 4) . substr($data['orgId'], -4) . date('YmdHis', time()) . sprintf('%03d', rand(0, 999)),
            'signType' => 'RSA2',
            'sign' => $sign,
            'appendData' => $appendData,
            'data' => $unData
        ];
        Log::info('获取易生-云闪付用户标识：');
        Log::info($easy_data);
        $res_obj = $this->payment->request($easy_data, $this->getOpenidUrl);
        Log::info('易生-云闪付用户标识-res：');
        Log::info($res_obj);
        $res_arr = json_decode($res_obj, true);

        //成功
        if ($res_arr['sysRetcode'] != '000000') {
            return [
                'status' => '0',
                'message' => $res_arr['sysRetmsg']
            ];
        } else {
            if ($res_arr['data']['finRetcode'] == '00') {
                return [
                    'status' => '1',
                    'message' => '获取成功',
                    'data' => $res_arr['data'],
                ];
            } else {
                return [
                    'status' => '0',
                    'message' => '获取失败',
                    'data' => $res_arr['data'],
                ];
            }

        }
    }

    //退款查询2.0接口
    public function refund_query($data)
    {
        try {
            $channel_id = $data['channel_id'] ?? '';
            $mer_id = $data['mer_id'] ?? '';
            $device_id = $data['device_id'] ?? '';
            $refund_no = $data['refund_no'] ?? '';
            $data = [
                'oriOrgTrace' => $refund_no
            ];
            $sign = $this->getSign($data, $this->key);
            $select_data = [
                'orgId' => $channel_id, //渠道编号
                'orgMercode' => $mer_id, //商戶编号
                'orgTermno' => $device_id, //终端编号
                'orgTrace' => date('YmdHis') . mt_rand(1000, 9999),
                'sign' => $sign,
                'bizData' => $data,
                'signType' => 'RSA2'
            ];
            Log::info('易生支付-退货查询-params:');
            Log::info($select_data);
            $res_obj = $this->payment->request($select_data, $this->refundQueryUrl);
            Log::info('易生支付-退货查询-res:');
            Log::info($res_obj);

            //系统错误
            if (!$res_obj) {
                return [
                    'status' => '-1',
                    'message' => '系统错误'
                ];
            }

            $res_arr = json_decode($res_obj, true);

            //成功
            if ($res_arr['sysRetCode'] != '000000') {
                return [
                    'status' => '0',
                    'message' => $res_arr['sysRetMsg']
                ];
            } else {
                if ($res_arr['bizData']['tradeRetCode'] == '00') {
                    return [
                        'status' => '1',
                        'message' => '退货成功',
                        'data' => $res_arr['bizData'],
                    ];
                } else {
                    return [
                        'status' => '3',
                        'message' => '退货失败',
                        'data' => $res_arr['bizData'],
                    ];
                }

            }
        } catch (\Exception $ex) {
            Log::info('易生支付-退款查询-错误');
            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine(),
            ];
        }
    }
}
