<?php
namespace App\Api\Controllers\HuiPay;


use App\Api\Controllers\Config\HuiPayConfigController;
use App\Models\HuiPayStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends BaseController
{

    //公众号入驻配置
    public function merAddUserConf($data)
    {
        try {
            $user_cust_id = $data['user_cust_id'];
            $store_id = $data['store_id'];

            $hui_pay_user = HuiPayStore::where('user_cust_id', $user_cust_id)
                ->where('store_id', $store_id)
                ->first();
            if (!$hui_pay_user) {
                return json_encode([
                    'status' => '2',
                    'message' => '改商户尚未进件',
                ]);
            }

            $config_id = $hui_pay_user->config_id;

            $hui_pay_config_obj = new HuiPayConfigController();
            $hui_pay_config = $hui_pay_config_obj->hui_pay_config($config_id);
            $private_key = $hui_pay_config->private_key;
            $public_key = $hui_pay_config->public_key;
            $org_id = $hui_pay_config->org_id;

            $data = ['user_cust_id' => $user_cust_id];

            $post_data = [
                'data' => $data,
                'sign' => $this->rsa_sign($data, $private_key),
                'sign_type' => 'RSA2',
                'mer_cust_id' => $this->mer_cust_id,
                'source_num' => $org_id,
            ];

            $result = $this->post_func($this->weixingzh, $post_data);
            if ($result['status'] == 1) {
                $res_data = $result['data'];
                if ($res_data['resp_code'] == '000000') {
                    $return_json_data = json_decode($res_data['data'], true);
                    $verify_res = $this->verify($data, $return_json_data['sign'], $public_key);
                    if (!$verify_res) {
                        return json_encode([
                            'status' => '3',
                            'message' => '入驻状态查询验签失败',
                        ]);
                    }

                    return $return_json_data;
                } else {
                    return json_encode([
                        'status' => $res_data['resp_code'],
                        'message' => $res_data['resp_desc'],
                    ]);
                }
            } else {
                return json_encode([
                    'status' => '0',
                    'message' => $result['message'],
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().'-'.$ex->getLine(),
            ]);
        }
    }


    //设备绑定
    public function device_bind($data)
    {
        try {
            $user_cust_id = $data['user_cust_id'];
            $store_id = $data['store_id'];

            $hui_pay_user = HuiPayStore::where('user_cust_id', $user_cust_id)
                ->where('store_id', $store_id)
                ->first();
            if (!$hui_pay_user) {
                return json_encode([
                    'status' => '2',
                    'message' => '改商户尚未进件',
                ]);
            }

            $config_id = $hui_pay_user->config_id;

            $hui_pay_config_obj = new HuiPayConfigController();
            $hui_pay_config = $hui_pay_config_obj->hui_pay_config($config_id);
            $private_key = $hui_pay_config->private_key;
            $public_key = $hui_pay_config->public_key;
            $org_id = $hui_pay_config->org_id;

            $data = [
                'device_id' => $device_id,  //必须，设备号
                'device_name' => $device_name,	 //必须，设备名称
                'shop_id' => $shop_id,	 //必须，绑定门店id
            ];

            $post_data = [
                'data' => $data,
                'sign' => $this->rsa_sign($data, $private_key),
                'sign_type' => 'RSA2',
                'mer_cust_id' => $this->mer_cust_id,
                'source_num' => $org_id,
            ];

            $result = $this->post_func($this->device_bind, $post_data);
            if ($result['status'] == 1) {
                $res_data = $result['data'];
                if ($res_data['resp_code'] == '000000') {
                    $return_json_data = json_decode($res_data['data'], true);
                    $verify_res = $this->verify($data, $return_json_data['sign'], $public_key);
                    if (!$verify_res) {
                        return json_encode([
                            'status' => '3',
                            'message' => '入驻状态查询验签失败',
                        ]);
                    }
                    return $return_json_data;
                } else {
                    return json_encode([
                        'status' => $res_data['resp_code'],
                        'message' => $res_data['resp_desc'],
                    ]);
                }
            } else {
                return json_encode([
                    'status' => '0',
                    'message' => $result['message'],
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().'-'.$ex->getLine(),
            ]);
        }
    }


    //设备解除绑定
    public function device_unbind($data)
    {
        try {
            $user_cust_id = $data['user_cust_id'];
            $store_id = $data['store_id'];

            $hui_pay_user = HuiPayStore::where('user_cust_id', $user_cust_id)
                ->where('store_id', $store_id)
                ->first();
            if (!$hui_pay_user) {
                return json_encode([
                    'status' => '2',
                    'message' => '改商户尚未进件',
                ]);
            }

            $config_id = $hui_pay_user->config_id;

            $hui_pay_config_obj = new HuiPayConfigController();
            $hui_pay_config = $hui_pay_config_obj->hui_pay_config($config_id);
            $private_key = $hui_pay_config->private_key;
            $public_key = $hui_pay_config->public_key;
            $org_id = $hui_pay_config->org_id;

            $data = [
                'device_id' => $device_id,  //必须，设备号
                'shop_id' => $shop_id,	 //必须，绑定门店id
            ];

            $post_data = [
                'data' => $data,
                'sign' => $this->rsa_sign($data, $private_key),
                'sign_type' => 'RSA2',
                'mer_cust_id' => $this->mer_cust_id,
                'source_num' => $org_id,
            ];

            $result = $this->post_func($this->device_unbind, $post_data);
            if ($result['status'] == 1) {
                $res_data = $result['data'];
                if ($res_data['resp_code'] == '000000') {
                    $return_json_data = json_decode($res_data['data'], true);
                    $verify_res = $this->verify($data, $return_json_data['sign'], $public_key);
                    if (!$verify_res) {
                        return json_encode([
                            'status' => '3',
                            'message' => '入驻状态查询验签失败',
                        ]);
                    }
                    return $return_json_data;
                } else {
                    return json_encode([
                        'status' => $res_data['resp_code'],
                        'message' => $res_data['resp_desc'],
                    ]);
                }
            } else {
                return json_encode([
                    'status' => '0',
                    'message' => $result['message'],
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().'-'.$ex->getLine(),
            ]);
        }
    }


    //正扫支付
    public function user_sweep(Request $request)
    {
        try {
            $user_cust_id = $request->get('user_cust_id', '');
            $store_id = $request->get('store_id', '');

            $hui_pay_user = HuiPayStore::where('user_cust_id', $user_cust_id)
                ->where('store_id', $store_id)
                ->first();
            if (!$hui_pay_user) {
                return json_encode([
                    'status' => '2',
                    'message' => '改商户尚未进件',
                ]);
            }

            $config_id = $hui_pay_user->config_id;

            $hui_pay_config_obj = new HuiPayConfigController();
            $hui_pay_config = $hui_pay_config_obj->hui_pay_config($config_id);
            $private_key = $hui_pay_config->private_key;
            $public_key = $hui_pay_config->public_key;
            $org_id = $hui_pay_config->org_id;

            $data = [
                'device_id' => '',  //必须，设备号
                'cust_id' => '',  //必须,定长16位,客户号
                'pay_type' => '',  //定长2位,必须.支付类型	A1：支付宝正扫;W1：微信正扫;U1：银联正扫
                'trans_amt' => '',  //必须，交易金额	格式必须是###.00，比如2.00，2.01
                'order_id' => '',  //必须，订单号
                'ip_addr' => '',  //必须，ip地址
                'buyer_id' => '',  //必须，买家的支付宝用户ID，支付宝支付时必须
                'buyer_logon_id' => '',  //必须，买家支付宝账号，支付宝支付时必须，
                'open_id' => '',  //必须，微信用户关注商家公众号的openid，微信支付时必须，
                'union_user_id' => '',  //必须，银联用户标识.银联支付时必须，
                'ret_url' => '',  //必须，页面回调地址.交易完成后渠道跳转的地址
                'bg_ret_url' => '',	//可选，交易结果通知地址。不传则不通知;接口同步返回不通知
                'mer_priv' => '',  //可选，商户的私有数据项	原样返回
                'extension' => '',  //可选，扩展域，原样返回
                'hb_fq_num' => '',	//可选，用户花呗分期数，支付宝花呗分期分期数，暂时只支持3、6、12
                'is_raw' => '',	//定长1位,可选,微信公众号是否原生态。是否原生态。1-是；0-否。不填默认1
                'api_version' => '20',	//可选,版本号,目前固定为20
                'oper_user_id' => '',	//可选，操作员
                'goods_desc' => '',	//可选，商品描述
                'os_version' => '',	//可选，操作系统版本
                'soft_version' => '',	//可选，软件版本
                'longitude' => '',	//可选，经度
                'latitude' => '',	//可选，纬度
                'imei' => '',	//可选，
                'mac_ip' => '',	//可选，Mac地址
            ];

            $post_data = [
                'data' => $data,
                'sign' => $this->rsa_sign($data, $private_key),
                'sign_type' => 'RSA2',
                'mer_cust_id' => $this->mer_cust_id,
                'source_num' => $org_id,
            ];

            $result = $this->post_func($this->front_scan, $post_data);
            if ($result['status'] == 1) {
                $res_data = $result['data'];
                if ($res_data['resp_code'] == '000000') {
                    $return_json_data = json_decode($res_data['data'], true);
                    $verify_res = $this->verify($data, $return_json_data['sign'], $public_key);
                    if (!$verify_res) {
                        return json_encode([
                            'status' => '3',
                            'message' => '入驻状态查询验签失败',
                        ]);
                    }

                    return $return_json_data;
                } else {
                    return json_encode([
                        'status' => $res_data['resp_code'],
                        'message' => $res_data['resp_desc'],
                    ]);
                }
            } else {
                return json_encode([
                    'status' => '0',
                    'message' => $result['message'],
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().'-'.$ex->getLine(),
            ]);
        }
    }


    //反扫支付
    public function bizscan(Request $request)
    {
        try {
            $user_cust_id = $request->get('user_cust_id', '');
            $store_id = $request->get('store_id', '');

            $hui_pay_user = HuiPayStore::where('user_cust_id', $user_cust_id)
                ->where('store_id', $store_id)
                ->first();
            if (!$hui_pay_user) {
                return json_encode([
                    'status' => '2',
                    'message' => '改商户尚未进件',
                ]);
            }

            $config_id = $hui_pay_user->config_id;

            $hui_pay_config_obj = new HuiPayConfigController();
            $hui_pay_config = $hui_pay_config_obj->hui_pay_config($config_id);
            $private_key = $hui_pay_config->private_key;
            $public_key = $hui_pay_config->public_key;
            $org_id = $hui_pay_config->org_id;

            $data = [
                'device_id' => '',  //必须，设备号
                'cust_id' => '',  //必须,定长16位,客户号
                'order_id' => '', //必须，订单号，由机具或商户生成，必须保证唯一，50位内的字母或数字组合
                'pay_type' => '',  //定长2位,必须.支付类型	A1：支付宝正扫;W1：微信正扫;U1：银联正扫
                'trans_amt' => '',  //必须，交易金额	格式必须是###.00，比如2.00，2.01
                'auth_code' => '',  //必须，二维码
                'ip_addr' => '',  //必须，ip地址
                'buyer_id' => '',  //必须，买家的支付宝用户ID，支付宝支付时必须
                'buyer_logon_id' => '',  //必须，买家支付宝账号，支付宝支付时必须，
                'open_id' => '',  //必须，微信用户关注商家公众号的openid，微信支付时必须，
                'union_user_id' => '',  //必须，银联用户标识.银联支付时必须，
                'ret_url' => '',  //必须，页面回调地址.交易完成后渠道跳转的地址
                'bg_ret_url' => '',	//可选，交易结果通知地址。不传则不通知;接口同步返回不通知
                'mer_priv' => '',  //可选，商户的私有数据项	原样返回
                'extension' => '',  //可选，扩展域，原样返回
                'hb_fq_num' => '',	//可选，用户花呗分期数，支付宝花呗分期分期数，暂时只支持3、6、12
                'is_raw' => '',	//定长1位,可选,微信公众号是否原生态。是否原生态。1-是；0-否。不填默认1
                'api_version' => '20',	//可选,版本号,目前固定为20
                'oper_user_id' => '',	//可选，操作员
                'goods_desc' => '',	//可选，商品描述
                'os_version' => '',	//可选，操作系统版本
                'soft_version' => '',	//可选，软件版本
                'longitude' => '',	//可选，经度
                'latitude' => '',	//可选，纬度
                'imei' => '',	//可选，
                'mac_ip' => '',	//可选，Mac地址
            ];

            $post_data = [
                'data' => $data,
                'sign' => $this->rsa_sign($data, $private_key),
                'sign_type' => 'RSA2',
                'mer_cust_id' => $this->mer_cust_id,
                'source_num' => $org_id,
            ];

            $result = $this->post_func($this->back_scan, $post_data);
            if ($result['status'] == 1) {
                $res_data = $result['data'];
                if ($res_data['resp_code'] == '000000') {
                    $return_json_data = json_decode($res_data['data'], true);
                    $verify_res = $this->verify($data, $return_json_data['sign'], $public_key);
                    if (!$verify_res) {
                        return json_encode([
                            'status' => '3',
                            'message' => '入驻状态查询验签失败',
                        ]);
                    }

                    return $return_json_data;
                } else {
                    return json_encode([
                        'status' => $res_data['resp_code'],
                        'message' => $res_data['resp_desc'],
                    ]);
                }
            } else {
                return json_encode([
                    'status' => '0',
                    'message' => $result['message'],
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().'-'.$ex->getLine(),
            ]);
        }
    }


}
