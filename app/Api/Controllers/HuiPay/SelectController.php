<?php

namespace App\Api\Controllers\HuiPay;


use App\Api\Controllers\Config\HuiPayConfigController;
use App\Models\HuiPayStore;
use App\Models\Store;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SelectController extends BaseController
{

    //入驻状态查询--查询绑卡、微信入驻、支付宝入驻等的状态
    public function mer_apply_result($data)
    {
        try {
            $apply_id = $data['apply_id'];
            $store_id = $data['store_id'];

            $hui_pay_user = HuiPayStore::where('store_id', $store_id)
                ->where('apply_id', $apply_id)
                ->first();
            if (!$hui_pay_user) {
                return json_encode([
                    'status' => '2',
                    'message' => '改商户尚未进件',
                ]);
            }

            $config_id = $hui_pay_user->config_id;

            $hui_pay_config_obj = new HuiPayConfigController();
            $hui_pay_config = $hui_pay_config_obj->hui_pay_config($config_id);
            $private_key = $hui_pay_config->private_key;
            $public_key = $hui_pay_config->public_key;
            $mer_cust_id = $hui_pay_config->mer_cust_id;
            $org_id = $hui_pay_config->org_id;

            $data = ['apply_id' => $apply_id];

            $post_data = [
                'data' => $data,
                'sign_type' => 'RSA2',
                'mer_cust_id' => $mer_cust_id,
                'source_num' => $org_id,
            ];

            $result = $this->execute($this->store_result, $post_data, $data, $private_key, $public_key);
            if ($result['status'] === '0') {
                return json_encode([
                    'status' => '0',
                    'message' => '入驻状态查询验签失败',
                ]);
            }
            if ($result['status'] == '00000') {
                if (isset($res_data['data']) && !empty($res_data['data'])) {
                    $return_json_data = json_decode($res_data['data'], true);
                    return json_encode([
                        'status' => $result['status'],
                        'message' => $result['message'],
                        'data' => $return_json_data
                    ]);
                } else {
                    return json_encode([
                        'status' => $result['status'],
                        'message' => $result['message'],
                    ]);
                }

            } else {
                return json_encode([
                    'status' => $result['status'],
                    'message' => $result['message'],
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().'-'.$ex->getLine(),
            ]);
        }
    }


    //商户信息查询--通过小微客户号查询小微的详细信息
    public function mer_detail($data)
    {
        try {
            $user_cust_id = $data['user_cust_id'];
            $store_id = $data['store_id'];

            $hui_pay_user = HuiPayStore::where('user_cust_id', $user_cust_id)
                ->where('store_id', $store_id)
                ->first();
            if (!$hui_pay_user) {
                return json_encode([
                    'status' => '2',
                    'message' => '改商户尚未进件',
                ]);
            }

            $config_id = $hui_pay_user->config_id;

            $hui_pay_config_obj = new HuiPayConfigController();
            $hui_pay_config = $hui_pay_config_obj->hui_pay_config($config_id);
            $private_key = $hui_pay_config->private_key;
            $public_key = $hui_pay_config->public_key;
            $mer_cust_id = $hui_pay_config->mer_cust_id;
            $org_id = $hui_pay_config->org_id;

            $data = ['user_cust_id' => $user_cust_id];

            $post_data = [
                'data' => $data,
                'sign_type' => 'RSA2',
                'mer_cust_id' => $mer_cust_id,
                'source_num' => $org_id,
            ];

            $result = $this->execute($this->store_detail, $post_data, $data, $private_key, $public_key);
            if ($result['status'] === '0') {
                return json_encode([
                    'status' => '0',
                    'message' => '入驻状态查询验签失败',
                ]);
            }
            if ($result['status'] == '00000') {
                if (isset($res_data['data']) && !empty($res_data['data'])) {
                    $return_json_data = json_decode($res_data['data'], true);
                    return json_encode([
                        'status' => $result['status'],
                        'message' => $result['message'],
                        'data' => $return_json_data
                    ]);
                } else {
                    return json_encode([
                        'status' => $result['status'],
                        'message' => $result['message'],
                    ]);
                }

            } else {
                return json_encode([
                    'status' => $result['status'],
                    'message' => $result['message'],
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().'-'.$ex->getLine(),
            ]);
        }
    }


    //交易订单查询--供商户查询其用户在本平台进行过的交易的交易状态
    public function order_query($data)
    {
        try {
            $user_cust_id = $data['user_cust_id'];
            $store_id = $data['store_id'];

            $hui_pay_user = HuiPayStore::where('user_cust_id', $user_cust_id)
                ->where('store_id', $store_id)
                ->first();
            if (!$hui_pay_user) {
                return json_encode([
                    'status' => '2',
                    'message' => '该商户尚未汇付进件',
                ]);
            }



            $config_id = $hui_pay_user->config_id;

            $hui_pay_config_obj = new HuiPayConfigController();
            $hui_pay_config = $hui_pay_config_obj->hui_pay_config($config_id);
            $private_key = $hui_pay_config->private_key;
            $public_key = $hui_pay_config->public_key;
            $mer_cust_id = $hui_pay_config->mer_cust_id;
            $org_id = $hui_pay_config->org_id;

            $data = [
                'cust_id' => $user_cust_id,  //必须，小微客户号
                'device_id' => $device_id,  //必须，终端设备号
                'order_type' => $pay_type,  //必须，订单类型。P-支付;R-退款
                'order_id' => $pay_type=='R'?$order_id:'',  //可选，订单号。退款时必填
                'party_order_id' => '',  //可选，第三方支付凭证号。支付时和订单号二选一必填
            ];

            $post_data = [
                'data' => $data,
                'sign_type' => 'RSA2',
                'mer_cust_id' => $mer_cust_id,
                'source_num' => $org_id,
            ];

            $result = $this->execute($this->order_info, $post_data, $data, $private_key, $public_key);
            if ($result['status'] === '0') {
                return json_encode([
                    'status' => '0',
                    'message' => '入驻状态查询验签失败',
                ]);
            }
            if ($result['status'] == '00000') {
                if (isset($res_data['data']) && !empty($res_data['data'])) {
                    $return_json_data = json_decode($res_data['data'], true);
                    return json_encode([
                        'status' => $result['status'],
                        'message' => $result['message'],
                        'data' => $return_json_data
                    ]);
                } else {
                    return json_encode([
                        'status' => $result['status'],
                        'message' => $result['message'],
                    ]);
                }

            } else {
                return json_encode([
                    'status' => $result['status'],
                    'message' => $result['message'],
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().'-'.$ex->getLine(),
            ]);
        }
    }

    //支付源id获取--用于获取微信、支付宝、银联正扫支付时的用户id
    public function channel_user_id_query($data)
    {
        try {
            $user_cust_id = $data['user_cust_id'];
            $store_id = $data['store_id'];

            $hui_pay_user = HuiPayStore::where('user_cust_id', $user_cust_id)
                ->where('store_id', $store_id)
                ->first();
            if (!$hui_pay_user) {
                return json_encode([
                    'status' => '2',
                    'message' => '改商户尚未进件',
                ]);
            }

            $config_id = $hui_pay_user->config_id;

            $hui_pay_config_obj = new HuiPayConfigController();
            $hui_pay_config = $hui_pay_config_obj->hui_pay_config($config_id);
            $private_key = $hui_pay_config->private_key;
            $public_key = $hui_pay_config->public_key;
            $org_id = $hui_pay_config->org_id;

            $data = [
                'api_version' => '20',  //可选，版本号,目前固定为20
                'device_id' => $device_id,  //必须，终端设备号
                'cust_id' => $user_cust_id,  //必须，客户号
                'pay_type' => $pay_type,  //必填，支付类型，A1-支付宝正扫；W1-微信正扫；U1-银联正扫
                'auth_code' => $auth_code,  //必填，授权码
                'mer_priv' => '',  //可选，商户的私有数据项，原样返回
                'extension' => '',  //可选，扩张域，原样返回
            ];

            $post_data = [
                'data' => $data,
                'sign_type' => 'RSA2',
                'mer_cust_id' => $mer_cust_id,
                'source_num' => $org_id,
            ];

            $result = $this->execute($this->user_info, $post_data, $data, $private_key, $public_key);
            if ($result['status'] === '0') {
                return json_encode([
                    'status' => '0',
                    'message' => '入驻状态查询验签失败',
                ]);
            }
            if ($result['status'] == '00000') {
                if (isset($res_data['data']) && !empty($res_data['data'])) {
                    $return_json_data = json_decode($res_data['data'], true);
                    return json_encode([
                        'status' => $result['status'],
                        'message' => $result['message'],
                        'data' => $return_json_data
                    ]);
                } else {
                    return json_encode([
                        'status' => $result['status'],
                        'message' => $result['message'],
                    ]);
                }

            } else {
                return json_encode([
                    'status' => $result['status'],
                    'message' => $result['message'],
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().'-'.$ex->getLine(),
            ]);
        }
    }


}
