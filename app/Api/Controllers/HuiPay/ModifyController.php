<?php
namespace App\Api\Controllers\HuiPay;


use App\Api\Controllers\Config\HuiPayConfigController;
use App\Models\HuiPayStore;
use Illuminate\Http\Request;

class ModifyController extends BaseController
{

    //进件驳回后商户信息修改
    public function mer_apply_modify(Request $request)
    {
        try {
            $user_cust_id = $request->get('user_cust_id', '');
            $store_id = $request->get('store_id', '');
            $apply_id = $request->get('apply_id', '');

            $hui_pay_user = HuiPayStore::where('user_cust_id', $user_cust_id)
                ->where('store_id', $store_id)
                ->first();
            if (!$hui_pay_user) {
                return json_encode([
                    'status' => '2',
                    'message' => '改商户尚未进件',
                ]);
            }

            $config_id = $hui_pay_user->config_id;

            $hui_pay_config_obj = new HuiPayConfigController();
            $hui_pay_config = $hui_pay_config_obj->hui_pay_config($config_id);
            $private_key = $hui_pay_config->private_key;
            $public_key = $hui_pay_config->public_key;
            $org_id = $hui_pay_config->org_id;

            $data = [
                'apply_id' => $apply_id, //必须,企业开户申请号
//                'corp_short_name' =>,  //可选，企业名称简称
//                'corp_business_address' =>,	//可选，企业经营地址
//                'corp_fixed_telephone' =>,	//可选，企业固定电话
//                'contact_name' =>,	//可选，联系人姓名
//                'contact_mobile' =>,	//可选，联系人手机
//                'contact_email' =>,	//可选，联系人邮箱
//                'legal_name' =>,	//可选，法人姓名
//                'legal_id_card' =>,	//可选，法人证件号
//                'legal_id_card_type' =>,	//可选，法人证件类型
//                'legal_cert_start_date' =>,	//可选，法人证件起始日期
//                'legal_cert_end_date' =>,	//可选，法人证件起始日期
//                'legal_mobile' =>,	//可选，法人手机号
//                'prov_code' =>,	//可选，省份
//                'city_code' =>,	//可选，地区
//                'district_code' =>,	//可选，区县
            ];

            $images1 = [
//                'file_id' => ,	//必须，文件token
//                'attach_name' => ,	//必须，文件名称
//                'attach_type' => '',  //必须，文件类型01 ,  营业执照图片；02-税务登记证号；03-组织机构代码证；04-开户许可证；05-法人身份证正面；06-法人身份证反面；07-结算人身份证正面；08-结算人身份证反面；09-商务协议；10-公司照片一；11-公司照片二；12-公司照片三；13-联系人身份证正面；14-联系人身份证反面；15-店铺门头照片；16-店铺收银台照片；17-店内照片；18-结算卡正面；19-结算卡反面；20-主流餐饮平台入驻照片；21-D+1协议照片；22-结算变更说明表；23-授权委托书；99-其他
//                'attach_desc' => ,	//必须，文件描述
            ];
            $data['file_tokens'][] = $images1;  //可选，图片

            $post_data = [
                'data' => $data,
                'sign' => $this->rsa_sign($data, $private_key),
                'sign_type' => 'RSA2',
                'mer_cust_id' => $this->mer_cust_id,
                'source_num' => $org_id,
            ];

            $result = $this->post_func($this->modify_mer_apply, $post_data);
            if ($result['status'] == 1) {
                $res_data = $result['data'];
                if ($res_data['resp_code'] == '000000') {
                    return json_encode([
                        'status' => '1',
                        'message' => '修改成功'
                    ]);
                } else {
                    return json_encode([
                        'status' => $res_data['resp_code'],
                        'message' => $res_data['resp_desc'],
                    ]);
                }
            } else {
                return json_encode([
                    'status' => '0',
                    'message' => $result['message'],
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().'-'.$ex->getLine(),
            ]);
        }
    }


    //修改商户扫码费率
    public function qrcp_fee_update($data)
    {
        try {
            $user_cust_id = $data['user_cust_id'];
            $store_id = $data['store_id'];

            $hui_pay_user = HuiPayStore::where('user_cust_id', $user_cust_id)
                ->where('store_id', $store_id)
                ->first();
            if (!$hui_pay_user) {
                return json_encode([
                    'status' => '2',
                    'message' => '改商户尚未进件',
                ]);
            }

            $config_id = $hui_pay_user->config_id;

            $hui_pay_config_obj = new HuiPayConfigController();
            $hui_pay_config = $hui_pay_config_obj->hui_pay_config($config_id);
            $private_key = $hui_pay_config->private_key;
            $public_key = $hui_pay_config->public_key;
            $org_id = $hui_pay_config->org_id;

            $data = [
                'user_cust_id' => $user_cust_id, //用户客户号,必须
                'agent_id' => $this->mer_cust_id, //操作代理商号,必须
                'role_flag' => '1', //角色,必须，代理商传1
                'qrcp_fee_conf_dtos' => [
                    'pay_type' => '', //必须,支付类型 A1"-"A 支付宝统一下单";W1"-"W 微信公众号"; U1"-"U 银联js支付"; A0"-"A 支付宝反扫"; W0"- "W 微信反扫";U0"-"U 银联反扫";
                    'service_calc_mode' => '',	//必须，服务费率公式
                    'stat_flag' => '',	//必须，状态
                    'calc_mode' => '',	//可选，费率公式默认为0
                    'fee_acct_id' => '',	//可选，外扣必填
                    'fee_cust_id' => '',	//可选，外扣必填
                ],
            ];

            $post_data = [
                'data' => $data,
                'sign' => $this->rsa_sign($data, $private_key),
                'sign_type' => 'RSA2',
                'mer_cust_id' => $this->mer_cust_id,
                'source_num' => $org_id,
            ];

            $result = $this->post_func($this->update_rate, $post_data);
            if ($result['status'] == 1) {
                $res_data = $result['data'];
                if ($res_data['resp_code'] == '000000') {
                    return json_encode([
                        'status' => '1',
                        'message' => '修改成功'
                    ]);
                } else {
                    return json_encode([
                        'status' => $res_data['resp_code'],
                        'message' => $res_data['resp_desc'],
                    ]);
                }
            } else {
                return json_encode([
                    'status' => '0',
                    'message' => $result['message'],
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().'-'.$ex->getLine(),
            ]);
        }
    }


}
