<?php
/**
 * Created by PhpStorm.
 * User: daimingkang
 * Date: 2018/9/6
 * Time: 下午7:36
 */

namespace App\Api\Controllers\Suzhou;


use Illuminate\Support\Facades\Validator;

class BaseController extends \App\Api\Controllers\BaseController
{
    // 表单提交字符集编码
    public $postCharset = "UTF-8";

    private $fileCharset = "UTF-8";
    public $rsaPrivateKeyFilePath = "";
    public $pri_key = "MIIEogIBAAKCAQEAiwr8eh82t/38791es44d7nEMRw95ABzSAhTcT1ufuZUsB8KgsSlRPWSTAmvxuGFFtyaGghUVnYwz0M8FtcIlUH7PIfbN+NAfyy1l1GeuJwc7O5aYqVhseLr2S6zIYpV0DMItK0Yf5bS/fw+vdakUmQyFmPlDn+AE+WIvAwOY009yRvQ30ybLvnWCYsD/mI/4LNKPmsfQBBFtwg3IrpuzVbqekLJsyESFxQcrOEBbhaxZDvxC/MFM0eZghcu0jnS9gEm8xldr/5CVP30mChAvS+/Fmy0Vz+oGQjtHrbEE84eiO1GQ8vaim/kDdGOQuPZh9gWAzi6n25JgFZTNhY5tQQIDAQABAoIBAG8Y5yV4/VLEaa8K1Vloy7eyslmxHkCdxpuNKX9rOVOiwpyswZLs7ZG9Y82CjJCDgJ3X9Y/I4w2QcfJ9CBXzu7b2q/B6cJdHTLAeZQfzk+5xU73IFdDx6zIISTWsKCzFOAXdvROfo2r6IiyXTt4XrHKh07moKYV8DN1qux+QZzMnToPwvpFVg/gSqMIfquey5w5HuCUl0JRwY9aNEOBwj2EN4EsbNWTT/j+Z1GLESsWPCDYgWYO//xqMF0YU+GxhVN5p6+HX8mjLfNfYOi0Tf9+/BscdRKDQIFPEtlo5R8HPYR/4eVPA+Un+CedYgjdWidwIwvXsGidRl6nTmDHsAAECgYEA12r6RKXqUlZjJDvAoQt+GWPJA9GJn6uc8yj8XjV1qjF/tLEpXMJAgJQtSkBWC5Ir3NuBWB1V9x9FA5qM+F5wB1Wq6LZZ8Rqboy8zdaocxNpM1jt7myGVAx2+Pxt802sZuhSbaG/YYHTWDorrzp/IRKJxkJfgW74HMJq2/f0UOUECgYEApTymfcrR9hiuGaCs/cp3/OdxDH+BhKknZ/3Ybi+sWbXPskm9xVFyiQXtaNG9kpcyucO15bHuhn/nUS9fp0+TmV43qg3yjqzxH9Ng4FKCIjdUJSyG+4Odgw13j/c2XnwR4tDcqmLKz9qScgX2XpafP9Lb0BvzQijqi7WTBvWZNAECgYBhqmXXU+Xd+L6wQcr++Oft+Pi9G3YrBzJ8aDurNs+nHxQIRz46iPOXmBbEkFCypE3wrzCAU6STCwFONfbt8FsYaDW5lltG3cpsfMz27u5mLZcilgtfdSVLKWQE1qCMdgVPAWLIQdU3PiGCrGdm4bh5rbCRXtpgndJCrhKDKwfNQQKBgFwxaNuKzHc48frOkXbQ/v2WzJailctE7f2xvo5L/yfWr1nUlxN/DB6ztfDkBZfC0MShE95i1qkouBGuEvqxBbNuxDQXsBw3FhohwE2o+37HzCUpoO64UqwKWYlz71vscbZRvq06jdpHI6qG9R11gMkFhyuoTKO/XI4y8yPQ6HgBAoGAdy3Y+D/FJ7v0MUMbuVcV4QutK1kvH+2Ts/OkC150QUtZtOTEbg8SEoVriMNALyXaNE6MWq56k2ernvTHVlvvc4Yk6okBUQkFjFj8IlGdK+11j0aGH8CWNspMWOty+KFQs+ap+npqH0IsogzJA6ZogyF2wFheeHQNzSIMVxyJwHc=";

    //参数拼接
    public function getSignContent($params)
    {
        ksort($params);

        $stringToBeSigned = "";
        $i = 0;
        foreach ($params as $k => $v) {

            if (false === $this->checkEmpty($v) && "@" != substr($v, 0, 1)) {
                // 转换成目标字符集
                $v = $this->characet($v, $this->postCharset);

                if ($i == 0) {
                    $stringToBeSigned .= "$k" . "=" . "$v";
                } else {
                    $stringToBeSigned .= "&" . "$k" . "=" . "$v";
                }
                $i++;
            }
        }

        unset ($k, $v);
        return $stringToBeSigned;
    }


    /**
     * 转换字符集编码
     * @param $data
     * @param $targetCharset
     * @return string
     */
    function characet($data, $targetCharset)
    {


        if (!empty($data)) {
            $fileType = $this->fileCharset;
            if (strcasecmp($fileType, $targetCharset) != 0) {

                $data = mb_convert_encoding($data, $targetCharset);
                //				$data = iconv($fileType, $targetCharset.'//IGNORE', $data);
            }
        }


        return $data;
    }

    /**
     * 校验$value是否非空
     *  if not set ,return true;
     *    if is null , return true;
     **/
    protected function checkEmpty($value)
    {
        if (!isset($value))
            return true;
        if ($value === null)
            return true;
        if (trim($value) === "")
            return true;

        return false;
    }


    /*
        curl发送数据
    */
    static function curl($data, $url)
    {
        //启动一个CURL会话
        $ch = curl_init();
        // 设置curl允许执行的最长秒数
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        // 获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //发送一个常规的POST请求。
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        //要传送的所有数据
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        // 执行操作
        $res = curl_exec($ch);
        $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($res == NULL) {
            curl_close($ch);
            return false;
        } else if ($response != "200") {
            curl_close($ch);
            return false;
        }
        curl_close($ch);
        return $res;
    }

    function curl_get($url)
    {

        $ch = curl_init();

        //设置选项，包括URL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);//绕过ssl验证
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        //执行并获取HTML文档内容
        $output = curl_exec($ch);

        //释放curl句柄
        curl_close($ch);
        return $output;
    }

    /**
     * 校验必填字段
     */
    public function check_required($check, $data)
    {
        $rules = [];
        $attributes = [];
        foreach ($data as $k => $v) {
            $rules[$k] = 'required';
            $attributes[$k] = $v;
        }
        $messages = [
            'required' => ':attribute不能为空',
        ];
        $validator = Validator::make($check, $rules,
            $messages, $attributes);
        $message = $validator->getMessageBag();
        return $message->first();
    }

    public function curl_form($url, $data)
    {
        $headers = array('Content-Type: application/x-www-form-urlencoded');
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0); // 从证书中检查SSL加密算法是否存在
      //  curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data)); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($curl); // 执行操作
        if (curl_errno($curl)) {
            echo 'Errno' . curl_error($curl);//捕抓异常
        }
        curl_close($curl); // 关闭CURL会话
        $result = mb_convert_encoding($result, 'utf-8', 'GB2312');
        parse_str($result, $arr);


        return $arr;
    }

    /**
     * http的curl 方法post请求接口
     * @param string $url
     * @param string $post_data
     * @return string
     */
    function http_curl($url, $post_data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // post数据
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 信任任何证书
        // post的变量
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($post_data))
        );
        $output = curl_exec($ch);
        curl_close($ch);
        //返回数据
        return $output;
    }


    public function rsaSign($content)
    {
        $pri_key = $this->pri_key;

        $pri_key_f = $this->formatPriKey($pri_key);
        $privKeyId = openssl_pkey_get_private($pri_key_f);
        $signature = '';
        openssl_sign($content, $signature, $privKeyId, OPENSSL_ALGO_SHA256);
        openssl_free_key($privKeyId);
        //base64编码
        return base64_encode($signature);
    }

    public function sign($data, $signType = "RSA")
    {
        if ($this->checkEmpty($this->rsaPrivateKeyFilePath)) {
            $priKey = $this->pri_key;
            $res = "-----BEGIN RSA PRIVATE KEY-----\n" .
                wordwrap($priKey, 64, "\n", true) .
                "\n-----END RSA PRIVATE KEY-----";
        } else {
            $priKey = file_get_contents($this->rsaPrivateKeyFilePath);
            $res = openssl_get_privatekey($priKey);
        }

        ($res) or die('您使用的私钥格式错误，请检查RSA私钥配置');

        if ("RSA2" == $signType) {
            openssl_sign($data, $sign, $res, OPENSSL_ALGO_SHA256);
        } else {
            openssl_sign($data, $sign, $res);
        }

        if (!$this->checkEmpty($this->rsaPrivateKeyFilePath)) {
            openssl_free_key($res);
        }
        $sign = base64_encode($sign);
        $sign = $this->strToHex($sign);//16进制
        return $sign;
    }


    public function strToHex($string)
    {
        $hex = "";
        for ($i = 0; $i < strlen($string); $i++)
            $hex .= dechex(ord($string[$i]));
        $hex = strtoupper($hex);
        return $hex;
    }

    function rsaSign1($content)
    {
        $pri_key = $this->pri_key;
        $pri_key_f = $this->formatPriKey($pri_key);
        $res = openssl_get_privatekey($pri_key_f);
        openssl_sign($content, $sign, $res);
        openssl_free_key($res);
        //base64编码
        $sign = base64_encode($sign);
        return $sign;
    }


    /**格式化rsa私钥
     * @param $priKey
     * @return string
     * User: WangMingxue
     * Email cumt_wangmingxue@126.com
     */
    public function formatPriKey($priKey)
    {
        $fKey = "-----BEGIN PRIVATE KEY-----\n";
        $len = strlen($priKey);
        for ($i = 0; $i < $len;) {
            $fKey = $fKey . substr($priKey, $i, 64) . "\n";
            $i += 64;
        }
        $fKey .= "-----END PRIVATE KEY-----";
        return $fKey;
    }

}