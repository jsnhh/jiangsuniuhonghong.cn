<?php
namespace App\Api\Controllers\RolePermission;


use App\Api\Controllers\BaseController;
use App\Models\Merchant;
use App\Models\RoleHasPermissions;
use App\Models\User;
use App\Models\UserRate;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
//use Spatie\Permission\Models\Role;
//use Spatie\Permission\Models\Permission;
use App\Models\Role;
use App\Models\Permission;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use function MongoDB\BSON\toJSON;
use function Sodium\add;

class RoleController extends BaseController
{

    //角色列表
    public function role_list(Request $request)
    {
        try {
            $public = $this->parseToken();
            $role_id = $request->get('role_id'); //角色id

            $created_id = '';
            $guard_name = '';
            if ($public->type == "merchant") {
                $guard_name = 'merchant.api';
                $created_id = $public->merchant_id;
            }

            if ($public->type == "user") {
                $guard_name = 'user.api';
                $created_id = $public->user_id;
            }

            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table('roles');
            } else {
                $obj = DB::table('roles');
            }

            $where = [];
            $where[] = ['guard_name', '=', $guard_name];
            if($role_id){
                $where[] = ['id', '=', $role_id];
            }
            $roles = $obj->where('created_id', $created_id)
                ->where($where)
                ->select('id as role_id', 'display_name','name','guard_name','created_at')
                ->get();

            $this->status = 1;
            $this->message = '数据返回成功';

            return $this->format($roles);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    //权限列表
    public function permission_list(Request $request)
    {
        try {
            $public = $this->parseToken();
            $permission_id = $request->get('permission_id');
            $display_name = $request->get('display_name');

            $obj = DB::table('permissions as p');

            $where = [];
            if($display_name){
                $where[] = ['p.display_name', 'like', '%' . $display_name . '%'];
            }
            if($permission_id){
                $where[] = ['p.id', '=', $permission_id];
            }

            if($public->level == 0){
                $list = $obj->select('p.id as permission_id', 'p.name', 'p.pid', 'p.display_name','p.guard_name','p.created_at','p.path','p.menu_type','p.perms','p.icon','p.component','p.order_num')
                    ->leftJoin('role_has_permissions', 'role_has_permissions.permission_id','=','p.id')
                    ->leftJoin('roles', 'roles.id','=','role_has_permissions.role_id')
                    ->leftJoin('model_has_roles', 'model_has_roles.role_id','=','roles.id')
                    ->leftJoin('users', 'users.id','=','model_has_roles.model_id')
                    ->where($where)
                    ->distinct()
                    ->orderBy('p.pid')
                    ->orderBy('p.order_num')
                    ->get()->toArray();

            }else{
                $list = $obj->select('p.id as permission_id', 'p.name', 'p.pid', 'p.display_name','p.guard_name','p.created_at','p.path','p.menu_type','p.perms','p.icon','p.component','p.order_num')
                    ->leftJoin('role_has_permissions', 'role_has_permissions.permission_id','=','p.id')
                    ->leftJoin('roles', 'roles.id','=','role_has_permissions.role_id')
                    ->leftJoin('model_has_roles', 'model_has_roles.role_id','=','roles.id')
                    ->leftJoin('users', 'users.id','=','model_has_roles.model_id')
                    ->where('users.is_delete', '0')
                    ->where('users.id', $public->user_id)
                    ->where($where)
                    ->distinct()
                    ->orderBy('p.pid')
                    ->orderBy('p.order_num')
                    ->get()->toArray();
            }

            if ($list) {
                foreach ($list as  $val) {
                    $val-> children = [];
                }
            }

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($list);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }

    //添加角色
    public function add_role(Request $request)
    {
        try {
            $public = $this->parseToken();
            $display_name = $request->get('display_name'); //角色名字
            $pid = $request->get('pid', 0); //角色父级id
            $name = $request->get('name'); //角色权限字符串
            $PermissionIds = $request->get('permissionIds'); //角色权限字符串

            $guard_name = '';
            $created_id = 0;
            if ($public->type == "merchant") {
                $guard_name = 'merchant.api';
                $created_id = $public->merchant_id;
            }

            if ($public->type == "user") {
                $guard_name = 'user.api';
                $created_id = $public->user_id;
            }

            $check_data = [
                'display_name' => '角色名字',
                'name' => '角色权限字符串'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $role = Role::create([
                'name' => $name,
                'pid' => $pid,
                'display_name' => $display_name,
                'guard_name' => $guard_name,
                'created_id' => $created_id
            ]);

            if(count($PermissionIds) > 0){
                foreach ($PermissionIds as $val){
                    DB::beginTransaction();
                    RoleHasPermissions::create([
                        'permission_id' => $val,
                        'role_id' => $role->id
                    ]);
                    DB::commit();
                }
            }
            if ($role) {
                return json_encode([
                    "status" => 1,
                    "message" => "添加角色成功"
                ]);
            } else {
                return json_encode([
                    "status" => 2,
                    "message" => "添加角色失败"
                ]);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            return json_encode([
                "status" => 2,
                "message" => $e->getMessage().' | '.$e->getLine()
            ]);
        }
    }

    //更新角色
    public function update_role(Request $request)
    {
        try {
            $public = $this->parseToken();
            $role_id = $request->get('role_id'); //角色Id
            $display_name = $request->get('display_name'); //角色名字
            $name = $request->get('name'); //角色权限字符串
            $PermissionIds = $request->get('permissionIds'); //角色权限字符串

            $check_data = [
                'role_id' => '角色ID',
                'display_name' => '角色名字',
                'name' => '角色权限字符串'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $role = Role::where('id', $role_id)->first();
            if($role){
                $data = [
                    'name' => $name,
                    'display_name' => $display_name
                ];
                $role->update($data);
                $role->save();
            }

            if(count($PermissionIds) > 0){
                DB::table('role_has_permissions')
                    ->where('role_id', $role_id)
                    ->delete();

                foreach ($PermissionIds as $val){
                    DB::beginTransaction();
                    RoleHasPermissions::create([
                        'permission_id' => $val,
                        'role_id' => $role->id
                    ]);
                    DB::commit();
                }

            }
            if ($role) {
                return json_encode([
                    "status" => 1,
                    "message" => "更新角色成功"
                ]);
            } else {
                return json_encode([
                    "status" => 2,
                    "message" => "更新角色失败"
                ]);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            return json_encode([
                "status" => 2,
                "message" => $e->getMessage().' | '.$e->getLine()
            ]);
        }
    }


    //添加权限
    public function add_permission(Request $request)
    {
        try {
            $public = $this->parseToken();
            $display_name = $request->get('display_name'); //权限名称
            $pid = $request->get('pid', 0); //
            $path = $request->get('path','#');    //请求地址
            $menu_type = $request->get('menu_type');    //菜单类型（M目录 C菜单 F按钮）
            $perms = $request->get('perms');    //权限标识
            $icon = $request->get('icon', '#');
            $component = $request->get('component'); //组件路径
            $order_num = $request->get('order_num', 1); //排序


            $check_data = [
                'display_name' => '权限名称',
                'menu_type' => '菜单类型',
                'order_num' => '显示排序',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $guard_name = '';
            $level = 1;
            if ($public->type == "merchant") {
                $guard_name = 'merchant.api';
                $level = $public->pid;
            }

            if ($public->type == "user") {
                $guard_name = 'user.api';
                $level = $public->level;
            }

            //非平台(or默认店长) 暂不可以添加权限
            if ($level > 0) {
                return $this->responseDataJson(180005);
            }

//            $permission = Permission::create([
//                'name' => $display_name,
//                'display_name' => $display_name,
//                'guard_name' => $guard_name,
//                'pid' => $pid
//            ]);
            //新增权限
            $permission =  Permission::create([
                'name' => $display_name,
                'display_name' => $display_name,
                'guard_name' => $guard_name,
                'component' => $component,
                'pid' => $pid,
                'path' => $path,
                'menu_type' => $menu_type,
                'perms' => $perms,
                'icon' => $icon,
                'order_num' => $order_num
            ]);

            if ($permission) {
                return json_encode([
                    "status" => 1,
                    "message" => "添加权限成功"
                ]);
            } else {
                return json_encode([
                    "status" => 2,
                    "message" => "添加权限失败"
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                "status" => '-1',
                "message" => $ex->getMessage().' | '.$ex->getLine()
            ]);
        }
    }

    //修改权限
    public function update_permission(Request $request)
    {
        try {
            $public = $this->parseToken();
            $permission_id = $request->get('permission_id'); //Id
            $display_name = $request->get('display_name'); //权限名称
            $pid = $request->get('pid', 0); //
            $path = $request->get('path','#');    //请求地址
            $menu_type = $request->get('menu_type');    //菜单类型（M目录 C菜单 F按钮）
            $perms = $request->get('perms');    //权限标识
            $icon = $request->get('icon', '#');
            $component = $request->get('component'); //组件路径
            $order_num = $request->get('order_num', 1); //排序


            $check_data = [
                'permission_id' => '权限ID',
                'display_name' => '权限名称',
                'menu_type' => '菜单类型',
                'order_num' => '显示排序',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $guard_name = '';
            $level = 1;
            if ($public->type == "merchant") {
                $guard_name = 'merchant.api';
                $level = $public->pid;
            }

            if ($public->type == "user") {
                $guard_name = 'user.api';
                $level = $public->level;
            }

            //非平台(or默认店长) 暂不可以修改权限
            if ($level > 0) {
                return $this->responseDataJson(180005);
            }

            //修改权限

            $permission = Permission::where('id', $permission_id)->first();
            if($permission){
                $data = [
                    'name' => $display_name,
                    'display_name' => $display_name,
                    'guard_name' => $guard_name,
                    'component' => $component,
                    'pid' => $pid,
                    'path' => $path,
                    'menu_type' => $menu_type,
                    'perms' => $perms,
                    'icon' => $icon,
                    'order_num' => $order_num
                ];
                $permission->update($data);
                $permission->save();
            }

            if ($permission) {
                return json_encode([
                    "status" => 1,
                    "message" => "修改权限成功"
                ]);
            } else {
                return json_encode([
                    "status" => 2,
                    "message" => "修改权限失败"
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                "status" => '-1',
                "message" => $ex->getMessage().' | '.$ex->getLine()
            ]);
        }
    }

    //给用户分配角色
    public function assign_role(Request $request)
    {
        try {
            $public = $this->parseToken();
            $role_id = $request->get('role_id'); //角色id集合
            $user_id = $request->get('customer_id'); //用户id
            $role_id = trim($role_id);

            $model_type = "";
            $user = "";
            if ($public->type == "merchant") {
                $model_type = 'App\Models\Merchant';
                $user = Merchant::where('id', $user_id)->first();
                $pid = $user->pid;
            }

            if ($public->type == "user") {
                $model_type = 'App\Models\User';
                $user = User::where('id', $user_id)->first();
                $pid = $user->pid;
            }

            if ($user) {
                if ($pid > 0) {
                    if ($user_id == $public->user_id){
                        return json_encode([
                            'status' => 2,
                            'message' => '用户无法给自己分配角色'
                        ]);
                    }
                }
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '用户不存在'
                ]);
            }

            $check_data = [
                'customer_id' => '用户id',
                'role_id' => '角色id',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $del_user_rolse = DB::table('model_has_roles')
                ->where('model_id', $user_id)
                ->where('model_type', $model_type)
                ->delete();
            if (!$del_user_rolse) {
                Log::info('分配角色-删除失败');
                Log::info('用户id：'.$user_id);
                Log::info('用户类型：'.$model_type);
            }

            $role_id_arr = explode(',', $role_id);
            if (count($role_id_arr) <= 1) {
                $res = $user->assignRole($role_id);
                if ($res) {
                    return json_encode([
                        "status" => 1,
                        "message" => "分配角色成功"
                    ]);
                } else {
                    return json_encode([
                        "status" => 2,
                        "message" => "分配角色失败"
                    ]);
                }
            } else {
                foreach ($role_id_arr as $k => $v) {
                    $user->assignRole($v);
                }

                return json_encode([
                    "status" => 1,
                    "message" => "分配成功"
                ]);
            }
        } catch (\Exception $e) {
            return json_encode([
                "status" => 2,
                "message" => $e->getMessage().' | '.$e->getLine()
            ]);
        }
    }

    //给用户分配角色
    public function assign_role_users($request)
    {
        try {

            $role_id = $request['role_id']; //角色id集合
            $user_id = $request['customer_id']; //用户id
            $type = $request['type'];
            $role_id = trim($role_id);

            $model_type = "";
            $user = "";
            if ($type == "merchant") {
                $model_type = 'App\Models\Merchant';
                $user = Merchant::where('id', $user_id)->first();
            }

            if ($type == "user") {
                $model_type = 'App\Models\User';
                $user = User::where('id', $user_id)->first();
            }

            $del_user_rolse = DB::table('model_has_roles')
                ->where('model_id', $user_id)
                ->where('model_type', $model_type)
                ->delete();
            if (!$del_user_rolse) {
                Log::info('分配角色-删除失败');
                Log::info('用户id：'.$user_id);
                Log::info('用户类型：'.$model_type);
            }

            $role_id_arr = explode(',', $role_id);
            if (count($role_id_arr) <= 1) {
                $res = $user->assignRole($role_id);
                if ($res) {
                    return json_encode([
                        "status" => 1,
                        "message" => "分配角色成功"
                    ]);
                } else {
                    return json_encode([
                        "status" => 2,
                        "message" => "分配角色失败"
                    ]);
                }
            } else {
                foreach ($role_id_arr as $k => $v) {
                    $user->assignRole($v);
                }

                return json_encode([
                    "status" => 1,
                    "message" => "分配成功"
                ]);
            }
        } catch (\Exception $e) {
            return json_encode([
                "status" => 2,
                "message" => $e->getMessage().' | '.$e->getLine()
            ]);
        }
    }

    //给角色分配权限
    public function assign_permission(Request $request)
    {
        try {
            $public = $this->parseToken();
            $permission_id = $request->get('permission_id'); //权限id集合
            $role_id = $request->get('role_id'); //角色id
            $permission_id = trim($permission_id);
            $permission_id_arr = explode(',', $permission_id);

            $role = Role::where('id', $role_id)->first();
            if (!$role) {
                return json_encode([
                    'status' => 2,
                    'message' => '角色不存在'
                ]);
            }

            $assign_permission = Cache::get('assign_permission');
            if ($assign_permission) {
                return json_encode([
                    'status' => 2,
                    'message' => '操作频繁请稍后再试！'
                ]);
            } else {
                Cache::put('assign_permission', '1', 1);
            }

            $check_data = [
                'permission_id' => '权限id',
                'role_id' => '角色id',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            //先删除角色的所有权限
            $del_role_has_permissions_res = DB::table('role_has_permissions')
                ->where('role_id', $role_id)
                ->delete();
            if (!$del_role_has_permissions_res) {
                return json_encode([
                    'status' => 2,
                    'message' => '删除角色原有权限失败'
                ]);
            }

            if (count($permission_id_arr) <= 1) {
                $permission = Permission::where('id', $permission_id)
                    ->select('id')
                    ->first();
                if (!$permission) {
                    return json_encode([
                        'status' => 2,
                        'message' => '权限不存在'
                    ]);
                }

                $res = $role->givePermissionTo($permission_id);
                if ($res) {
                    return json_encode([
                        "status" => 1,
                        "message" => "角色分配权限成功"
                    ]);
                } else {
                    return json_encode([
                        "status" => 2,
                        "message" => "角色分配权限失败"
                    ]);
                }
            } else {
                foreach ($permission_id_arr as $k => $v) {
                    $permission = Permission::where('id', $v)
                        ->select('id')
                        ->first();
                    if (!$permission) {
                        continue;
                    }
                    $role->givePermissionTo($v);
                }

                return json_encode([
                    "status" => 1,
                    "message" => "分配成功"
                ]);
            }
        } catch (\Exception $e) {
            return json_encode([
                "status" => 2,
                "message" => $e->getMessage().' | '.$e->getLine()
            ]);
        }
    }


    //查询 用户已分配角色
    public function user_role_list(Request $request)
    {
        try {
            $public = $this->parseToken();
            $user_id = $request->get('customer_id'); //用户ID

            $check_data = [
                'customer_id' => '用户ID',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $model_type = "";
            $user = "";
            if ($public->type == "merchant") {
                $model_type = 'App\Models\Merchant';
                $user = Merchant::where('id', $user_id)->first();
            }

            if ($public->type == "user") {
                $model_type = 'App\Models\User';
                $user = User::where('id', $user_id)->first();
            }

            if (!$user) {
                $this->status = 2;
                $this->message = '用户不存在';
                return $this->format();
            }

            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table('model_has_roles');
            } else {
                $obj = DB::table('model_has_roles');
            }

            $roles = $obj
                ->leftJoin('roles', 'roles.id', '=', 'model_has_roles.role_id')
                ->where('model_type', $model_type)
                ->where('model_id', $user_id)
                ->select('model_has_roles.role_id','roles.display_name')
                ->get();

            $this->message = '数据返回成功';
            return $this->format($roles);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage().' | '.$exception->getLine();
            return $this->format();
        }
    }


    //查询 角色已分配权限
    public function role_permission_list(Request $request)
    {
        try {
            $public = $this->parseToken();
            $role_id = $request->get('role_id'); //角色id

            $check_data = [
                'role_id' => '角色id',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table('role_has_permissions');
            } else {
                $obj = DB::table('role_has_permissions');
            }

            $roles = $obj
                ->where('role_id', $role_id)
                ->select('permission_id')
                ->get();

            $this->message = '数据返回成功';
            return $this->format($roles);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage().' | '.$exception->getLine();
            return $this->format();
        }
    }


    //查询 用户已分配权限
    public function user_permission_list(Request $request)
    {
        try {
            $public = $this->parseToken();
            $user_id = $request->get('customer_id'); //用户id

            $check_data = [
                'customer_id' => '用户ID'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $model_type = "";
            $user_obj = "";
            if ($public->type == "merchant") {
                $model_type = 'App\Models\Merchant';
                $user_obj = Merchant::where('id', $user_id)
                    ->where('is_close', 2)
                    ->first();
            }

            if ($public->type == "user") {
                $model_type = 'App\Models\User';
                $user_obj = User::where('id', $user_id)
                    ->where('is_delete', 0)
                    ->first();
            }

            if (!$user_obj) {
                return $this->responseDataJson(190001);
            }

            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table('model_has_permissions');
            } else {
                $obj = DB::table('model_has_permissions');
            }

            $roles = $obj
                ->where('model_type', $model_type)
                ->where('model_id', $user_id)
                ->get();

            $this->message = '数据返回成功';
            return $this->format($roles);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage().' | '.$exception->getLine();
            return $this->format();
        }
    }


    //给用户分配权限
    public function user_assign_permission(Request $request)
    {
        try {
            $public = $this->parseToken();
            $permission_id = $request->get('permission_id'); //权限id集合(多个用,连接)
            $permission_id = trim($permission_id);
            $permission_id_arr = explode(',', $permission_id);
            $customer_id = $request->get('customer_id'); //用户id

            $check_data = [
                'permission_id' => '权限',
                'customer_id' => '用户ID'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $assign_permission = Cache::get('user_assign_permission');
            if ($assign_permission) {
                return json_encode([
                    'status' => 2,
                    'message' => '操作频繁请稍后再试！'
                ]);
            } else {
                Cache::put('user_assign_permission', '1', 1);
            }

            $model_type = "";
            $user = "";
            if ($public->type == "merchant") {
                $model_type = 'App\Models\Merchant';
                $user = Merchant::where('id', $customer_id)->first();
            }
            if ($public->type == "user") {
                $model_type = 'App\Models\User';
                $user = User::where('id', $customer_id)->first();
            }

            //先删除
            $del_res = DB::table('model_has_permissions')
                ->where('model_type', $model_type)
                ->where('model_id', $customer_id)
                ->delete();
            if (!$del_res) {
                return json_encode([
                    'status' => 2,
                    'message' => '给用户分配权限-刷新权限失败'
                ]);
            }

            if (count($permission_id_arr) <= 1) {
                $permission = Permission::where('id', $permission_id)
                    ->select('id')
                    ->first();
                if (!$permission) {
                    return json_encode([
                        'status' => 2,
                        'message' => '权限不存在'
                    ]);
                }
                $add_user_permission_res = $user->givePermissionTo($permission_id);
                if ($add_user_permission_res) {
                    return $this->responseDataJson(180001);
                } else {
                    return $this->responseDataJson(180002);
                }
            } else {
                foreach ($permission_id_arr as $k => $v) {
                    $permission = Permission::where('id', $v)
                        ->select('id')
                        ->first();
                    if (!$permission) {
                        continue;
                    }
                    $user->givePermissionTo($v);
                }

                return json_encode([
                    "status" => 1,
                    "message" => "分配成功"
                ]);
            }
        } catch (\Exception $e) {
            return json_encode([
                "status" => 2,
                "message" => $e->getMessage().' | '.$e->getLine()
            ]);
        }
    }


    //删除角色
    public function del_role(Request $request)
    {
        try {
            $public = $this->parseToken();
            $role_id = $request->get('role_id'); //角色id

            $check_data = [
                'role_id' => '角色id'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $level = $public->level;
            //非平台(or默认店长) 不能删除
            if ($level > 0) {
                return $this->responseDataJson(180005);
            }

            $res = Role::where('id', $role_id)->delete();
            //删除关联数据
            DB::table('role_has_permissions')
                ->where('role_id', $role_id)
                ->delete();

            DB::table('model_has_roles')
                ->where('role_id', $role_id)
                ->delete();

            $this->status = 1;
            $this->message = '角色删除成功';
            return $this->format();

        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage().' | '.$exception->getFile();
            return $this->format();
        }
    }


    // 删除 权限
    public function del_permission(Request $request)
    {
        try {
            $public = $this->parseToken();
            $permission_id = $request->get('permission_id'); //权限id

            $check_data = [
                'permission_id' => '权限id'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $guard_name = '';
            $level = 1;
            if ($public->type == "merchant") {
                $guard_name = 'merchant.api';
                $level = $public->pid;
            }

            if ($public->type == "user") {
                $guard_name = 'user.api';
                $level = $public->level;
            }

            //非平台（or默认店长）不能删除权限
            if ($level > 0) {
                return $this->responseDataJson(180005);
            }

            try {
                DB::beginTransaction(); //开启事务

                $res = Permission::find($permission_id)
                    ->delete();

                DB::commit();
            } catch (\Exception $ex) {
                DB::rollBack();
            }

            if ($res) {
                $this->message = '删除权限成功';
                return $this->format();
            } else {
                return $this->responseDataJson(180004);
            }
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage().' | '.$exception->getFile();
            return $this->format();
        }
    }

    // 查询权限下拉树结构
    public function treeselect(Request $request)
    {
        try {

            $list = Permission::where('pid','=', 0)
                ->with('children')
                ->select('id', 'display_name')
                ->get()->toArray();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($list);

        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage().' | '.$exception->getFile();
            return $this->format();
        }
    }

    // 根据角色ID查询权限下拉树结构
    public function roleMenuTreeselect(Request $request)
    {
        try {
            $public = $this->parseToken();

            $role_id = $request->get('role_id'); //角色id
            $check_data = [
                'role_id' => '角色id',
            ];
            $check = $this->check_required($request->all(), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            // 管理员显示所有菜单信息
            if($public->level == 0){
                $list = Permission::where('pid','=', 0)
                    ->with('children')
                    ->select('id', 'display_name')
                    ->get()->toArray();
            }else{
                $list = Permission::where('pid','=', 0)
                    ->with('children')
                    ->select('id', 'display_name')
                    ->leftJoin('role_has_permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
                    ->where('role_has_permissions.role_id','=', $role_id)
                    ->get()->toArray();
            }
            $checkedKeys = [];
            if($role_id){ //查询是否勾选
                $obj = DB::table('permissions as p');
                $menuCheck = $obj->select('p.id')
                    ->leftJoin('role_has_permissions','role_has_permissions.permission_id', '=', 'p.id')
                    ->where('role_has_permissions.role_id','=', $role_id)
                    ->orderBy('p.pid')
                    ->get()
                    ->toArray();
                if($menuCheck){
                    $checkIds = array_column($menuCheck, 'id');
                    $checkedKeys = $checkIds;
                }
            }

            return json_encode(['status' => 1, 'message' => '数据返回成功', 'checkedKeys' => $checkedKeys, 'data' => $list]);

        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage().' | '.$exception->getFile();
            return $this->format();
        }
    }

    //获取用户菜单
    public function getRouters(Request $request)
    {
        try {
            $public = $this->parseToken();

            $menu_type = ['M', 'C'];

            if($public->level == 0){
                $list = Permission::whereIn('menu_type',$menu_type)
                    ->select( 'id','pid','display_name','menu_type','guard_name','path','component','icon','order_num')
                    ->orderBy('pid')
                    ->orderBy('order_num')
                    ->get()->toArray();

            }else{
//                $obj = DB::table('permissions as p');
//                $list = $obj->select( 'p.id','p.pid','p.display_name','p.menu_type','p.guard_name','p.path','p.component','p.icon')
                $list = Permission::whereIn('menu_type',$menu_type)->select( 'permissions.id','permissions.pid','permissions.display_name','permissions.menu_type','permissions.guard_name','permissions.path','permissions.component','permissions.icon','permissions.order_num')
                    ->leftJoin('role_has_permissions', 'role_has_permissions.permission_id','=','permissions.id')
                    ->leftJoin('roles', 'roles.id','=','role_has_permissions.role_id')
                    ->leftJoin('model_has_roles', 'model_has_roles.role_id','=','roles.id')
                    ->leftJoin('users', 'users.id','=','model_has_roles.model_id')
                    ->where('users.is_delete', '0')
                    ->where('users.id', $public->user_id)
//                    ->whereIn('p.menu_type',$menu_type)
                    ->distinct()
                    ->orderBy('permissions.pid')
                    ->orderBy('permissions.order_num')
                    ->get()->toArray();
            }
            $result = [];
            if ($list) {
                foreach ($list as &$per) {
                    $per['children'] = [];
                }
                unset($per);

                $newList = $this->getChildPerms($list, 0);
                $result = $this->buildMenus($newList);
            }

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($result);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }

    function getChildPerms($list, $pid){
        $newList = [];
        foreach ($list as  $val) {
            if($val['pid'] == $pid){
                $result = $this->recursionFn($list,$val);
                array_push($newList,$result);
            }
        }
        return $newList;

    }
    function recursionFn($list, $menu){
        // 得到子节点列表
        $childList = $this->getChildList($list, $menu);
        $menu['children'] = $childList;
        foreach ($childList as  $val) {
            if($this->hasChild($list,$val)){
                $this->recursionFn($list,$val);
            }
        }
        return $menu;
    }

    function getChildList($list, $menu){
        $list_new = [];
        foreach ($list as  $val) {
            if($val['pid'] == $menu['id']){
                array_push($list_new,$val);
            }
        }
        return $list_new;
    }

    /**
     * 判断是否有子节点
     */
    function hasChild($list, $menu){

        if(count($this->getChildList($list,$menu)) > 0){
            return true;
        }
        return false;
    }

    function buildMenus($list){
        $new_list = [];
        foreach ($list as  $val) {
            $router = [];
            $router['name'] = ucfirst($val['path']);
            $router['hidden'] = false;
            $router['component'] = 'Layout';
            $meta = [
                'title' => $val['display_name'],
                'icon' => $val['icon'],
                'noCache' => false,
                'link' => null
            ];
            $router['meta'] = $meta;

            $cMenu = $val['children'];
            if (count($cMenu) > 0 && $val['menu_type'] == 'M') {
                $router['path'] = '/'.$val['path'];
                $router['alwaysShow'] = true;
                $router['redirect'] = 'noRedirect';
                $router['children'] = $this->buildMenus($cMenu);

            } else if ($val['menu_type'] == 'C') { //是否为菜单内部跳转
                $chil_meta = [
                    'title' => $val['display_name'],
                    'icon' => $val['icon'],
                    'noCache' => false,
                    'link' => null
                ];
                $router = [
                    'path' => $val['path'],
                    'component' => $val['component'],
                    'name' => ucfirst($val['path']),
                    'meta' => $chil_meta,
                    'hidden'=> false
                ];
            }
            array_push($new_list, $router);
        }

        return $new_list;

    }

}
