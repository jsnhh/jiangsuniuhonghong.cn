<?php

namespace App\Api\Controllers\AdLinks;

use App\Api\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Models\AdLinks;
use Illuminate\Support\Facades\DB;

class AdLinksController extends BaseController
{
    //新增广告链接
    public function adlinksCreate(Request $request)
    {
        try {
            $public = $this->parseToken();
            $config_id = $public->config_id;
            $store_id = $request->get('store_id', '');
            $store_name = $request->get('store_name', '');
            $links = $request->get('links', '');
            $new_links = json_decode($links);
            $total_url = '';


            foreach($new_links as $key => $val){
                $total_url =  $total_url.$val->url_details;
            }

            $user_id = '';
//            if ($public->type == "merchant") {
//                $user_id = $public->merchant_id;
//            }
//            if ($public->type == "user") {
//                $user_id = $public->user_id;
//            }
            if (!$total_url) {
                return json_encode([
                    'status' => 2,
                    'message' => '请至少填写一个链接',
                ]);
            }


            $data = [
                'config_id' => $config_id,
                'store_id' => $store_id,
                'store_name' => $store_name,
                'links' => $links,
                'total_url' => $total_url,
            ];
            $obj = AdLinks::create($data);
            if ($obj) {
                return $this->responseDataJson(170001, $data);
            } else {
                return $this->responseDataJson(170002, $data);
            }
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    //广告链接
    public function adlinksLists(Request $request)
    {
        try {
            $public = $this->parseToken();
            $config_id = $public->config_id;
            $store_id = $request->get('store_id', '');
            $where = [];
            if ($config_id) {
                $where[] = ['config_id', '=', $config_id];
            }
            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
            }
            $obj = DB::table('ad_links')->where($where)
                ->orderBy('updated_at', 'desc');
            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $data = $this->page($obj)->orderBy('updated_at', 'desc')->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }

    public function adlinksDel(Request $request)
    {
        try {
            $public = $this->parseToken();
            $id = $request->get('id', '');
            $obj = AdLinks::where('id', $id)->delete();

            if ($obj) {
                return $this->responseDataJson(170005);
            } else {
                return $this->responseDataJson(170006);
            }
//            return json_encode([
//                'status' => 1,
//                'message' => '删除成功',
//            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    public function adlinksInfo(Request $request)
    {
        try {
            $public = $this->parseToken();

            $id = $request->get('id', '');
            $data = AdLinks::where('id', $id)->first();
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    public function adlinksUp(Request $request)
    {
        try {
//            $public = $this->parseToken();
//            $config_id = $public->config_id;


//            if ($public->type == "merchant") {
//                $user_id = $public->merchant_id;
//            }
//
//            if ($public->type == "user") {
//                $user_id = $public->user_id;
//            }

            $id = $request->get('id', '');
            $store_id = $request->get('store_id', '');
            $store_name = $request->get('store_name', '');
            $links = $request->get('links', '');

            $new_links = json_decode($links);
            $total_url = '';


            foreach($new_links as $key => $val){
                $total_url =  $total_url.$val->url_details;
            }
            if (!$total_url) {
                return json_encode([
                    'status' => 2,
                    'message' => '请至少填写一个链接',
                ]);
            }


            $data = [
                'store_id' => $store_id,
                'store_name' => $store_name,
                'links' => $links,
                'total_url' => $total_url,
            ];

            $obj = AdLinks::where('id', $id)->update($data);
            if ($obj) {
                return $this->responseDataJson(170003, $data);
            } else {
                return $this->responseDataJson(170004, $data);
            }
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    public function adlinksTotal(Request $request)
    {
        $token = $this->parseToken();
        $store_id = $request->post('storeId', ''); //门店id
        $check_data = [
            'storeId' => '门店id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = 2;
            $this->message = $check;
            return $this->format();
        }

        try {
            $data = AdLinks::where('store_id', $store_id)
                ->orderBy('updated_at', 'desc')
                ->first();
            if ($data) {
                $this->status = 1;
                $this->message = '查询成功';
                return $this->format($data);
            } else {
                $this->status = 2;
                $this->message = '查询失败';
                return $this->format($store_id);
            }
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


}
