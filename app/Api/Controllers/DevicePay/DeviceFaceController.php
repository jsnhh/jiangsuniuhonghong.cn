<?php
namespace App\Api\Controllers\DevicePay;


use Alipayopen\Sdk\AopClient;
use Alipayopen\Sdk\Request\AlipayTradeCancelRequest;
use App\Api\Controllers\Config\AlipayIsvConfigController;
use App\Api\Controllers\Config\HwcPayConfigController;
use App\Api\Controllers\Config\LinkageConfigController;
use App\Api\Controllers\Config\NewLandConfigController;
use App\Api\Controllers\Config\VbillConfigController;
use App\Api\Controllers\Config\WeixinConfigController;
use App\Api\Controllers\Config\WftPayConfigController;
use App\Api\Controllers\Deposit\AliDepositController;
use App\Api\Controllers\Deposit\WxDepositController;
use App\Api\Controllers\Linkage\PayController as LinkagePayController;
use App\Api\Controllers\HwcPay\PayController as HwcPayController;
use App\Api\Controllers\WftPay\PayController as WftPayController;
use App\Api\Controllers\Merchant\OrderController;
use App\Api\Controllers\Merchant\PayBaseController;
use App\Api\Controllers\Newland\PayController;
use App\Common\PaySuccessAction;
use App\Api\Controllers\Config\PayWaysController;
use App\Models\AppLogo;
use App\Models\AppOem;
use App\Models\AppUpdate;
use App\Models\DepositOrder;
use App\Models\Device;
use App\Models\DeviceOem;
use App\Models\MemberTpl;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Store;
use App\Models\StorePayWay;
use function EasyWeChat\Kernel\Support\get_client_ip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use SimpleSoftwareIO\QrCode\Generator;

class DeviceFaceController extends BaseController
{

    //设备初始化接口
    public function face_device_start(Request $request)
    {
        try {
            //获取请求参数
            $data = $request->all();
//            Log::info('设备初始化接口');
//            Log::info($data);

            //验证签名
            $check = $this->check_md5($data);
            if ($check['return_code'] == 'FALL') {
                return $this->return_data($check);
            }

            $device_id = $data['device_id'];
            if (!$device_id) {
                $err = [
                    'return_code' => 'FALL',
                    'return_msg' => '设备device_id不能为空'
                ];
                return $this->return_data($err);
            }

            $device_type = $data['device_type'];
            if (!$device_type) {
                $err = [
                    'return_code' => 'FALL',
                    'return_msg' => '设备device_type不能为空'
                ];
                return $this->return_data($err);
            }

            $DeviceOem = DeviceOem::where('device_id', $device_id)
                ->where('device_type', $device_type)
                ->select('Request')
                ->first();
            if (!$DeviceOem) {
                $err = [
                    'return_code' => 'FALL',
                    'return_msg' => '设备不存在'
                ];
                return $this->return_data($err);
            }

            if ($device_type == 'c_01') {
                $where = [];
                if (isset($data['device_key'])) {
                    $where[] = ['device_key', '=', $data['device_key']];
                }
                $DeviceObj = Device::where('device_no', $device_id)
                    ->where('device_type', $device_type)
                    ->where($where)
                    ->select('store_id')
                    ->first();
                if (!$DeviceObj) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '设备尚未绑定门店'
                    ];
                    return $this->return_data($err);
                }
                $store_id = $DeviceObj->store_id;
            }

            $AppUpdate = AppUpdate::where('type', $device_type)->first();
            $AppUrl = "";
            $Version = "2.1";
            if ($device_type == 'c_01') {
                $Version = "1.0";
            }
            if ($AppUpdate) {
                $AppUrl = $AppUpdate->UpdateUrl;
                $Version = $AppUpdate->version;
            }

            $SdkUrl = "";
            $SdkVersion = "";
            $sdk_obj = AppUpdate::where('type', 'face_sdk')->first();
            if ($sdk_obj) {
                $SdkUrl = $sdk_obj->UpdateUrl;
                $SdkVersion = $sdk_obj->version;
            }

            //调用系统前参数
            $data = [
                'return_code' => "SUCCESS",
                'return_msg' => "数据返回成功",
                'Request' => $DeviceOem->Request,
                'PayInit' => '/api/devicepay/face_pay_start',
                'WXInit' => '/api/devicepay/wxfacepay_initialize',
                'ScanPay' => '/api/devicepay/all_pay',
                'OrderQuery' => '/api/devicepay/all_pay_query',
                'OrderCancel' => '/api/devicepay/order_pay_cancel',
                'Login' => '/api/merchant/login',
                'MerchantOrder' => '/api/merchant/order',
                'MerchantOrderInfo' => '/api/merchant/order_info',
                'MerchantRefund' => '/api/merchant/refund',
                'MerchantUpdateOrder' => '/api/basequery/update_order',
                'MerchantOrderCount' => '/api/merchant/order_count',
                'MerchantStoreLists' => '/api/merchant/store_lists',
                'MerchantMerchantLists' => '/api/merchant/merchant_lists',
                'MerchantFqPay' => '/api/merchant/fq/fq_pay',
                'MerchantFqOrder' => '/api/merchant/fq/order',
                'MerchantFqOrderQuery' => '/api/merchant/hb_order_foreach',
                'MerchantFqOrderInfo' => '/api/merchant/fq/order_info',
                'MerchantFqRefund' => '/api/merchant/fq/refund',
                'MerchantHbQueryRate' => '/api/merchant/fq/hb_query_rate',
                'MerchantFqOrderCancel' => '/api/merchant/fq/hb_order_cancel',
                'Depositmicropay' => '/api/deposit/micropay',
                'Depositfacepay' => '/api/deposit/facepay',
                'Depositfund_order_query' => '/api/deposit/fund_order_query',
                'Depositfund_cancel' => '/api/deposit/fund_cancel',
                'Depositfund_pay' => '/api/deposit/fund_pay',
                'Depositpay_order_query' => '/api/deposit/pay_order_query',
                'Depositpay_order_list' => '/api/deposit/pay_order_list',
                'Depositpay_order_info' => '/api/deposit/pay_order_info',
                'Depositrefund' => '/api/deposit/refund',
                'Depositprint_tpl' => '/api/deposit/print_tpl',
                'Depositpay_order_count' => '/api/deposit/pay_order_count',
                'CheckPayPassword' => '/api/merchant/check_pay_password',
                'Version' => $Version,
                'AppUrl' => $AppUrl,
                'SdkVersion' => $SdkVersion,
                'SdkUrl' => $SdkUrl,
                'member_pay_submit' => '/api/member/member_pay_submit',
                'member_info_save' => '/api/member/member_info_save',
                'cz_b' => '/api/member/cz_b',
                'cz_query' => '/api/member/cz_query',
                'mb_infos' => '/api/member/mb_infos',
                'add_shops' => '/api/merchant/add_shops',
                'wxcode_getopenid' => '/api/merchant/wx_get_openid'
            ];
            if ($device_type == 'c_01') {
                $data['StoreId'] = $store_id;
                $data['StorePayQr'] = url('/qr?store_id=' . $store_id);
//                $qr_code_name = 'C01_qr_' . $store_id . '.png';
//                $generator = new Generator();
//                $generator->format('png')->size(300)->generate($store_pay_qr, public_path('tableAppletCommonQrCode/' . $qr_code_name));
//                $data['StorePayQr'] = env('APP_URL') . '/tableAppletCommonQrCode/' . $qr_code_name;
            }

            return $this->return_data($data);
        } catch (\Exception $ex) {
            $err = [
                'return_code' => 'FALL',
                'return_msg' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ];
            return $this->return_data($err);
        }
    }


    /**
     * 刷脸交易初始化接口
     */
    public function face_pay_start(Request $request)
    {
        try {
            //获取请求参数
            $data = $request->all();
            $request->getContent();
//            Log::info('刷脸交易初始化接口');
//            Log::info($data);

            //验证签名
            $check = $this->check_md5($data);
            if ($check['return_code'] == 'FALL') {
                return $this->return_data($check);
            }

            $device_id = $data['device_id'];
            if (!$device_id) {
                $err = [
                    'return_code' => 'FALL',
                    'return_msg' => '设备device_id不能为空'
                ];
                return $this->return_data($err);
            }

            $device_type = $data['device_type'];
            if (!$device_type) {
                $err = [
                    'return_code' => 'FALL',
                    'return_msg' => '设备device_type不能为空'
                ];
                return $this->return_data($err);
            }

            //找到设备ID
            $Device = Device::where('device_no', $device_id)
                ->where('device_type', $device_type)
                ->first();
            if (!$Device) {
                $err = [
                    'return_code' => 'FALL',
                    'return_msg' => '设备不存在'
                ];
                return $this->return_data($err);
            }

            $cj = isset($Device->cj) ? $Device->cj : "";
            $store_id = $Device->store_id;
            $store_name = $Device->store_name;
            $config_id = $Device->config_id;
            $merchant_id = $Device->merchant_id;
            $merchant_name = $Device->merchant_name;

            $face_code_end = $cj; //
            //微收银就不要在刷脸后面加东西 因为现在微收银限制了
            if ($cj && $cj == "1") {
                $face_code_end = '';
            }

            $store = Store::where('store_id', $store_id)
                ->select('is_admin_close', 'id', 'is_delete', 'pid', 'is_close', 'user_id')
                ->first();
            if (!$store) {
                $err = [
                    'return_code' => 'FALL',
                    'return_msg' => '平台门店ID不正确'
                ];

                return $this->return_data($err);
            }

            //关闭的商户禁止交易
            if ($store->is_close || $store->is_admin_close || $store->is_delete) {
                $err = [
                    'return_code' => 'FALL',
                    'return_msg' => '商户已经被服务商关闭'
                ];

                return $this->return_data($err);
            }

            $store_pid = $store->pid;

            //支付宝刷脸交易信息
            $alipay_obj = new AlipayIsvConfigController();
            $storeInfo = $alipay_obj->alipay_auth_info($store_id, $store_pid);
            $alipay_store_id = "";
            $out_store_id = "";
            $app_auth_token = "";
            $alipay_user_id = "";
            if ($storeInfo) {
                $alipay_store_id = $storeInfo->alipay_store_id;
                $out_store_id = $storeInfo->out_store_id;
                $app_auth_token = $storeInfo->app_auth_token;
                $alipay_user_id = $storeInfo->alipay_user_id;
            }

            $alipay_config = $alipay_obj->AlipayIsvConfig($config_id, '01');
            $partnerId = "";
            $ali_appid = "";
            if ($alipay_config) {
                $partnerId = $alipay_config->alipay_pid;
                $ali_appid = $alipay_config->app_id;
            }

            //微信刷脸交易信息
            $weixin_obj = new WeixinConfigController();
            $weixin_config = $weixin_obj->weixin_config_obj($config_id);
            $weixin_store = $weixin_obj->weixin_merchant($store_id, $store_pid);

            $wx_sub_merchant_id = "";
            if ($weixin_store) {
                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;
            }

            $wx_app_id = "";
            $wx_mch_id = "";

            if ($weixin_config) {
                $wx_app_id = $weixin_config->app_id;
                $wx_mch_id = $weixin_config->wx_merchant_id;
            }

            //平台信息
            $AppConfigMsg = AppOem::where('config_id', $config_id)->first();
            if (!$AppConfigMsg) {
                $AppConfigMsg = AppOem::where('config_id', '1234')->first();
            }

            //判断设备类型
            $face_type = "weixin";
            $pay_action_desc = "信刷脸支付";
            $pay_voice = "请扫码支付或微信刷脸支付";

            $ad_p_id = "";
            if ($device_type == "face_cm01") {
                $ad_p_id = "7";
            }

            if ($device_type == "face_TZH-L1") {
                $ad_p_id = "5";
            }

            if ($device_type == "face_jsd101") {
                $ad_p_id = "6";
            }

            if ($device_type == "face_f4") {
                $ad_p_id = "8";
            }

            if ($device_type == "face_pro") {
                $ad_p_id = "9";
            }

            //广告部分
            //配置ID-广告
            $user_id = $store->user_id; //商户直属的业务员
            $user_id_abc = $store->user_id;
            $store_key_id = $store->id;

            $ad_pub_data = [
                'user_id' => $user_id,
                'user_id_abc' => $user_id_abc,
                'store_key_id' => $store_key_id,
                'ad_p_id' => $ad_p_id
            ];

            $ad_data = [
                [
                    'ad_file' => url('/ad4.png'),
                    'ad_url' => url('/mb/login')
                ]
            ];

            $ad_data = $this->ad_data($ad_pub_data);

            $ad_data = json_encode($ad_data);

            $isv_logo = url('/f_logo.png');
            $login_logo = url('/login_logo.png');
            $start_bk_img = url('/qidongye.png');
            $ht_img = url('/qidongye.png');

            $AppLogo = AppLogo::where('config_id', $config_id)
                ->first();
            if (!$AppLogo) {
                $AppLogo = AppLogo::where('config_id', '1234')
                    ->first();
            }
            if ($AppLogo) {
                if ($AppLogo->face_logo) {
                    $isv_logo = $AppLogo->face_logo;
                }

                if ($AppLogo->login_logo) {
                    $login_logo = $AppLogo->login_logo;
                }

                if ($AppLogo->start_bk_img) {
                    $start_bk_img = $AppLogo->start_bk_img;
                }

                if ($AppLogo->ht_img) {
                    $ht_img = $AppLogo->ht_img;
                }
            }

            $store_member = "0";
            $MemberTpl = MemberTpl::where('store_id', $store_id)->where('tpl_status', 1)->first();
            if ($MemberTpl) {
                $store_member = "1";
            }

            //调用系统前参数
            $data = [
                'return_code' => "SUCCESS",
                'return_msg' => "数据返回成功",
                'store_id' => $store_id,
                'store_name' => $store_name,
                'merchant_id' => $merchant_id,
                'merchant_name' => $merchant_name,
                'config_id' => $config_id,
                'face_type' => $face_type,
                'pay_action' => 'pay', //pay -普通支付  deposit-押金支付
                'pay_action_desc' => $pay_action_desc, //pay -普通支付  deposit-押金支付
                'pay_voice' => $pay_voice,
                'ali_app_id' => $ali_appid,
                'ali_user_id' => $alipay_user_id, //签约商户的pid。来自蚂蚁开放平台，示例："2088011211821038"
                'ali_pid' => $partnerId, //ISV的pid。对于自用型商户，填写签约商户的pid，和merchantId保持一致
                'ali_store_id' => $out_store_id, //可选，商户门店编号，和当面付请求的store_id保持一致。
                'ali_store_code' => $alipay_store_id, //可选，口碑内部门店编号，如2017100900077000000045877777，和当面付请求中的alipay_store_id保持一致。
                'al_brand_code' => '',
                'wx_app_id' => $wx_app_id, //wx94b87b679e8677aa'
                "wx_mch_id" => $wx_mch_id, //'1494729062'
                "wx_sub_app_id" => "",
                "wx_sub_mch_id" => $wx_sub_merchant_id, //'1518150321'
                "wx_telephone" => $AppConfigMsg->phone,
                "wx_ask_face_permit" => '1',
                "ad_action_time" => '5', //切换时间 5s
                'ad_data' => $ad_data,
                'face_code_end' => isset($face_code_end) ? $face_code_end : "", //刷脸code 后面加
                'qr_code_end' => '', //扫码code 后面加
                'isv_name' => $AppConfigMsg->name,
                'isv_logo' => $isv_logo,
                'isv_phone' => $AppConfigMsg->phone,
                'login_logo' => $login_logo,
                'pay_qr_url' => url('/qr?store_id=') . $store_id,
                'lcd_data_type' => '0C1B', //银豹
                'start_bk_img' => $start_bk_img,
                'pay_bk_img' => $start_bk_img,
                'plat_logo' => $ht_img,
                'store_member' => $store_member //0 代表没有开启门店会员  1 代表开启门店会员
            ];

            return $this->return_json_data($data);
        } catch (\Exception $exception) {
            $err = [
                'return_code' => 'FALL',
                'return_msg' => $exception->getMessage().' | '.$exception->getLine()
            ];
            return $this->return_data($err);
        }
    }


    //微信通过该接口获取微信刷脸服务的初始化信息
    public function wxfacepay_initialize(Request $request)
    {
        try {
            $data = $request->all();
//            Log::info('微信通过该接口获取微信刷脸服务的初始化信息');
//            Log::info($data);

            //验证签名
            $check = $this->check_md5($data);
            if ($check['return_code'] == 'FALL') {
                return $this->return_data($check);
            }

            $device_id = $data['device_id'];
            $device_type = $data['device_type'];
            $store_id = $data['store_id'];
            $data_store_name = $data['store_name'] ?? '';
            $rawdata = $data['rawdata'];
            $out_trade_no = $data['$out_trade_no'] ?? '';

            if (!$device_id) {
                $err = [
                    'return_code' => 'FALL',
                    'return_msg' => '设备device_id不能为空'
                ];
                return $this->return_data($err);
            }

            if (!$device_type) {
                $err = [
                    'return_code' => 'FALL',
                    'return_msg' => '设备device_type不能为空'
                ];
                return $this->return_data($err);
            }

            //找到设备ID
            $Device = Device::where('device_no', $device_id)
                ->where('store_id', $store_id)
                ->where('device_type', $device_type)
                ->first();
            if (!$Device) {
                $err = [
                    'return_code' => 'FALL',
                    'return_msg' => '设备不存在'
                ];
                return $this->return_data($err);
            }

            $store = Store::where('store_id', $store_id)
                ->select('pid', 'config_id', 'store_name', 'merchant_id')
                ->first();
            if (!$store) {
                $err = [
                    'return_code' => 'FALL',
                    'return_msg' => '门店不存在'
                ];
                return $this->return_data($err);
            }
            $store_pid = $store->pid;
            $store_name = $store->store_name;
            $config_id = $store->config_id;

            $obj_ways = new PayWaysController();
            $ways = $obj_ways->ways_source('weixin', $store_id, $store_pid);
//            Log::info('微信支付方式-首选');
//            Log::info($ways->company);
            if (!$ways) {
                $err = [
                    'return_code' => 'FALL',
                    'return_msg' => '微信支付方式-首选-配置不存在或状态异常'
                ];
                return $this->return_data($err);
            }

            if ($ways->company == 'vbill') {
                $vbill_config_obj = new VbillConfigController();
                $vbill_config = $vbill_config_obj->vbill_config($config_id);
                if (!$vbill_config) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '随行付配置不存在请检查配置'
                    ];
                    return $this->return_data($err);
                }

                $vbill_merchant = $vbill_config_obj->vbill_merchant($store_id, $store_pid);
                if (!$vbill_merchant) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '随行付商户号不存在'
                    ];
                    return $this->return_data($err);
                }

                //获取随行付微信刷脸调用凭证
                $out_trade_no = 'wx_scan' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));

                $vbill_obj = new \App\Api\Controllers\Vbill\PayController();
                $wechat_face_data = [
                    'out_trade_no' => $out_trade_no,
                    'url' => $vbill_obj->wechat_lose_face,
                    'mno' => $vbill_merchant->mno,
                    'orgId' => $vbill_config->orgId,
                    'privateKey' => $vbill_config->privateKey,
                    'sxfpublic' => $vbill_config->sxfpublic,
                    'raw_data' => $rawdata, //初始数据
                    'device_id' => $device_id,
                    'store_no' => $store_id //门店编号
                ];

//                Log::info('随行付获取微信刷脸调用凭证');
//                Log::info($wechat_face_data);
                $wechat_face_return = $vbill_obj->lose_face($wechat_face_data);
//                Log::info($wechat_face_return);
                if ($wechat_face_return['status'] == 1) {
                    $re_data = [
                        'return_code' => 'SUCCESS', //SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                        'return_msg' => null,
                        'result_code' => 'SUCCESS',
                        'result_msg' => '数据返回成功',
                        'device_id' => $data['device_id'],
                        'device_type' => $data['device_type'],
                        'store_id' => $data['store_id'],
                        'authinfo' => $wechat_face_return['data']['respData']['authInfo'],
                        'expires_in' => $wechat_face_return['data']['respData']['expireTime'],
                        'wx_appid' => '',
                        'wx_mchid' => '',
                        'type' => 'vbill' //标识
                    ];
                } else {
                    $re_data = [
                        'return_code' => 'FALL', //SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                        'return_msg' => $wechat_face_return['message'],
                        'device_id' => $data['device_id'],
                        'device_type' => $data['device_type'],
                        'store_id' => $data['store_id']
                    ];
                }

            }
            elseif($ways->company == 'vbilla'){
                $vbill_config_obj = new VbillConfigController();
                $vbill_config = $vbill_config_obj->vbilla_config($config_id);
                if (!$vbill_config) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '随行付A配置不存在请检查配置'
                    ];
                    return $this->return_data($err);
                }

                $vbill_merchant = $vbill_config_obj->vbilla_merchant($store_id, $store_pid);
                if (!$vbill_merchant) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '随行付A商户号不存在'
                    ];
                    return $this->return_data($err);
                }

                //获取随心付微信刷脸调用凭证
                $out_trade_no = 'wx_scan' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));

                $vbill_obj = new \App\Api\Controllers\Vbill\PayController();
                $wechat_face_data = [
                    'out_trade_no' => $out_trade_no,
                    'url' => $vbill_obj->wechat_lose_face,
                    'mno' => $vbill_merchant->mno,
                    'orgId' => $vbill_config->orgId,
                    'privateKey' => $vbill_config->privateKey,
                    'sxfpublic' => $vbill_config->sxfpublic,
                    'raw_data' => $rawdata, //初始数据
                    'device_id' => $device_id,
                    'store_no' => $store_id //门店编号
                ];

//                Log::info('随行付A获取微信刷脸调用凭证');
//                Log::info($wechat_face_data);
                $wechat_face_return = $vbill_obj->lose_face($wechat_face_data);
//                Log::info($wechat_face_return);
                if ($wechat_face_return['status'] == 1) {
                    $re_data = [
                        'return_code' => 'SUCCESS', //SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                        'return_msg' => null,
                        'result_code' => 'SUCCESS',
                        'result_msg' => '数据返回成功',
                        'device_id' => $data['device_id'],
                        'device_type' => $data['device_type'],
                        'store_id' => $data['store_id'],
                        'authinfo' => $wechat_face_return['data']['respData']['authInfo'],
                        'expires_in' => $wechat_face_return['data']['respData']['expireTime'],
                        'wx_appid' => '',
                        'wx_mchid' => '',
                        'type' => 'vbilla'
                    ];
                } else {
                    $re_data = [
                        'return_code' => 'FALL', //SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                        'return_msg' => $wechat_face_return['message'],
                        'device_id' => $data['device_id'],
                        'device_type' => $data['device_type'],
                        'store_id' => $data['store_id']
                    ];
                }
            }
            elseif($ways->company == 'newland'){
                $new_land_config_obj = new NewLandConfigController();
                $new_land_config = $new_land_config_obj->new_land_config($config_id);
                if (!$new_land_config) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '新大陆配置不存在请检查配置'
                    ];
                    return $this->return_data($err);
                }

                $new_land_merchant = $new_land_config_obj->new_land_merchant($store_id, $store_pid);
                if (!$new_land_merchant) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '新大陆商户号不存在'
                    ];
                    return $this->return_data($err);
                }

                if (!$new_land_merchant->nl_mercId) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '新大陆商户号不存在'
                    ];
                    return $this->return_data($err);
                }

                if (!$new_land_merchant->trmNo) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '新大陆设备号不存在'
                    ];
                    return $this->return_data($err);
                }

                if (!$new_land_merchant->wx_sub_appid) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '新大陆微信子公众号id不存在'
                    ];
                    return $this->return_data($err);
                }

                //新大陆-获取微信刷脸调用凭证
                $out_trade_no = 'wx_scan' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));

                $new_land_obj = new PayController();
                $wechat_face_data = [
                    'out_trade_no' => $out_trade_no, //商户单号
                    'device_id' => $device_id, //终端设备编号
                    'key' => $new_land_merchant->nl_key, //机构密钥
                    'org_no' => $new_land_config->org_no, //机构号
                    'merc_id' => $new_land_merchant->nl_mercId, //商户号
                    'trm_no' => $new_land_merchant->trmNo, //设备号
                    'op_sys' => '3', //操作系统
                    'opr_id' => $store->merchant_id, //操作员
                    'trm_typ' => 'T', //设备类型，P-智能POS A-app扫码  C-PC端  T-台牌扫码
//                    'wx_sub_appid' => $new_land_merchant->wx_sub_appid, //新大陆入网-微信子公众号id
                    'wx_sub_appid' => 'wx5b32bce922c2ac7c', //新大陆-微信公众号appid
                    'raw_data' => $rawdata, //初始数据
                    'store_id' => $store_id, //门店id
                    'store_name' => $data_store_name //门店名称
                ];

//                Log::info('新大陆获取微信刷脸调用凭证');
//                Log::info($wechat_face_data);
                $wechat_face_return = $new_land_obj->get_sdk_face_auth($wechat_face_data);
//                Log::info($wechat_face_return);
                if ($wechat_face_return['status'] == '1') {
                    $authInfo = str_replace(' ', '+', $wechat_face_return['data']['authInfo']);
//                    Log::info('上传authinfo');
//                    Log::info($authInfo);
                    $re_data = [
                        'return_code' => 'SUCCESS', //SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                        'return_msg' => null,
                        'result_code' => 'SUCCESS',
                        'result_msg' => '数据返回成功',
                        'device_id' => $data['device_id'],
                        'device_type' => $data['device_type'],
                        'store_id' => $data['store_id'],
                        'authinfo' => $authInfo,
//                        'authinfo' => $wechat_face_return['data']['authInfo'],
                        'expires_in' => $wechat_face_return['data']['expiresIn'],
                        'wx_appid' => $wechat_face_return['data']['wxAppid'],
                        'wx_mchid' => $wechat_face_return['data']['wxMchid'],
                        'type' => 'newland'
                    ];
                } else {
                    $re_data = [
                        'return_code' => 'FALL', //SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                        'return_msg' => $wechat_face_return['message'],
                        'device_id' => $data['device_id'],
                        'device_type' => $data['device_type'],
                        'store_id' => $data['store_id']
                    ];
                }
            }
            elseif($ways->company == 'linkage'){
                $linkage_config_obj = new LinkageConfigController();
                $linkage_config = $linkage_config_obj->linkage_config($config_id);
                if (!$linkage_config) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '联动优势支付配置不存在,请检查配置'
                    ];
                    return $this->return_data($err);
                }

                if (!$linkage_config->mch_id) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '联动优势代理商编号不存在'
                    ];
                    return $this->return_data($err);
                }

                $linkage_merchant = $linkage_config_obj->linkage_merchant($store_id, $store_pid);
                if (!$linkage_merchant) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '联动优势商户入网信息不存在'
                    ];
                    return $this->return_data($err);
                }

                if (!$linkage_merchant->acqMerId) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '联动优势商户号不存在'
                    ];
                    return $this->return_data($err);
                }

                $linkage_obj = new LinkagePayController();
                $wechat_face_data = [
                    'acqSpId' => $linkage_config->mch_id, //代理商编号
                    'acqMerId' => $linkage_merchant->acqMerId, //商户号
                    'raw_data' => $rawdata, //初始数据
                    'appid' => $linkage_config->wxAppid ?? '', //否，商户号绑定的公众号/小程序appid
                    'mchId' => $mchId ?? '1502469151', //否，微信商户号
                    'subMchId' => $linkage_merchant->subMchId ?? '', //否，微信子商户号(服务商模式)
                    'privateKey' => $linkage_config->privateKey,
                    'publicKey' => $linkage_config->publicKey
                ];
//                Log::info('联动优势-获取微信刷脸调用凭证');
//                Log::info($wechat_face_data);
                $wechat_face_return = $linkage_obj->wx_face_voucher($wechat_face_data); //-1系统错误 0-其他 1-成功 2-验签失败
//                Log::info($wechat_face_return);
                if ($wechat_face_return['status'] == '1') {
                    $re_data = [
                        'return_code' => 'SUCCESS', //SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                        'return_msg' => null,
                        'result_code' => 'SUCCESS',
                        'result_msg' => '数据返回成功',
                        'device_id' => $data['device_id'],
                        'device_type' => $data['device_type'],
                        'store_id' => $data['store_id'],
                        'authinfo' => $wechat_face_return['data']['authinfo'] ?? '',
                        'expires_in' => $wechat_face_return['data']['expiresIn'] ?? '',
                        'wx_appid' => $wechat_face_return['data']['appid'] ?? '',
                        'wx_mchid' => $wechat_face_return['data']['mchId'] ?? '',
                        'type' => 'linkage'
                    ];
                } else {
                    $re_data = [
                        'return_code' => 'FALL', //SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                        'return_msg' => $wechat_face_return['message'],
                        'device_id' => $data['device_id'],
                        'device_type' => $data['device_type'],
                        'store_id' => $data['store_id']
                    ];
                }
            }
            elseif($ways->company == 'wftpay'){
                $wftpay_config_obj = new WftPayConfigController();
                $wftpay_config = $wftpay_config_obj->wftpay_config($config_id);
                if (!$wftpay_config) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '威富通支付配置不存在,请检查配置'
                    ];
                    return $this->return_data($err);
                }

                $wftpay_merchant = $wftpay_config_obj->wftpay_merchant($store_id, $store_pid);
                if (!$wftpay_merchant) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '威富通商户入网信息不存在'
                    ];
                    return $this->return_data($err);
                }

                if (!$out_trade_no) {
                    $out_trade_no = 'wx_scan' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                }

                $wftpay_obj = new WftPayController();
                $wechat_face_data = [
                    'mch_id' => $wftpay_merchant->mch_id,
                    'device_id' => $device_id,
                    'out_trade_no' => $out_trade_no,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'rawdata' => $rawdata,
                    'private_rsa_key' => $wftpay_config->private_rsa_key,
                    'public_rsa_key' => $wftpay_config->public_rsa_key
                ];
//                Log::info('威富通-获取微信刷脸调用凭证');
//                Log::info($wechat_face_data);
                $wechat_face_return = $wftpay_obj->weChatFacePayAuth($wechat_face_data); //0-系统错误 1-成功 2-退款失败
//                Log::info($wechat_face_return);
                if ($wechat_face_return['status'] == '1') {
                    $re_data = [
                        'return_code' => 'SUCCESS', //SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                        'return_msg' => null,
                        'result_code' => 'SUCCESS',
                        'result_msg' => '数据返回成功',
                        'device_id' => $data['device_id'],
                        'device_type' => $data['device_type'],
                        'store_id' => $data['store_id'],
                        'authinfo' => $wechat_face_return['data']['authinfo'] ?? '',
                        'expires_in' => $wechat_face_return['data']['expires_in'] ?? '',
                        'wx_appid' => $wechat_face_return['data']['appid'] ?? '', //服务商的APPID
                        'wx_mchid' => $wechat_face_return['data']['wx_mch_id'] ?? '', //微信服务商机构号
                        'type' => 'wftpay'
                    ];
                }
                else {
                    $re_data = [
                        'return_code' => 'FALL', //SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                        'return_msg' => $wechat_face_return['message'],
                        'device_id' => $data['device_id'],
                        'device_type' => $data['device_type'],
                        'store_id' => $data['store_id']
                    ];
                }
            }
            elseif ($ways->company == 'hwcpay') {
                $hwcpay_config_obj = new HwcPayConfigController();
                $hwcpay_config = $hwcpay_config_obj->hwcpay_config($config_id);
                if (!$hwcpay_config) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '汇旺财支付配置不存在,请检查配置'
                    ];
                    return $this->return_data($err);
                }

                $hwcpay_merchant = $hwcpay_config_obj->hwcpay_merchant($store_id, $store_pid);
                if (!$hwcpay_merchant) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '汇旺财商户入网信息不存在'
                    ];
                    return $this->return_data($err);
                }

                if (!$out_trade_no) {
                    $out_trade_no = 'wx_scan' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                }

                $hwcpay_obj = new HwcPayController();
                $wechat_face_data = [
                    'mch_id' => $hwcpay_merchant->mch_id,
                    'device_id' => $device_id,
                    'out_trade_no' => $out_trade_no,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'rawdata' => $rawdata,
                    'private_rsa_key' => $hwcpay_config->private_rsa_key,
                    'public_rsa_key' => $hwcpay_config->public_rsa_key
                ];
//                Log::info('汇旺财-获取微信刷脸调用凭证');
//                Log::info($wechat_face_data);
                $wechat_face_return = $hwcpay_obj->weChatFacePayAuth($wechat_face_data); //0-系统错误 1-成功 2-退款失败
//                Log::info($wechat_face_return);
                if ($wechat_face_return['status'] == '1') {
                    $re_data = [
                        'return_code' => 'SUCCESS', //SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                        'return_msg' => null,
                        'result_code' => 'SUCCESS',
                        'result_msg' => '数据返回成功',
                        'device_id' => $data['device_id'],
                        'device_type' => $data['device_type'],
                        'store_id' => $data['store_id'],
                        'authinfo' => $wechat_face_return['data']['authinfo'] ?? '',
                        'expires_in' => $wechat_face_return['data']['expires_in'] ?? '',
                        'wx_appid' => $wechat_face_return['data']['appid'] ?? '', //服务商的APPID
                        'wx_mchid' => $wechat_face_return['data']['wx_mch_id'] ?? '', //微信服务商机构号
                        'type' => 'hwcpay'
                    ];
                }
                else {
                    $re_data = [
                        'return_code' => 'FALL', //SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                        'return_msg' => $wechat_face_return['message'],
                        'device_id' => $data['device_id'],
                        'device_type' => $data['device_type'],
                        'store_id' => $data['store_id']
                    ];
                }
            }
            elseif($ways->company == 'weixina') {
                //微信a 刷脸
                $weixin_obj = new WeixinConfigController();
                $weixin_config = $weixin_obj->weixina_config_obj($config_id);
                $weixin_store = $weixin_obj->weixina_merchant($store_id, $store_pid);
                if (!$weixin_store) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '微信a商户号不存在'
                    ];
                    return $this->return_data($err);
                }

                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;

                //公共配置
                $config = [
                    'appid' => $weixin_config->app_id,
                    "mch_id" => $weixin_config->wx_merchant_id,
                    "version" => "1",
                    "sign_type" => 'MD5',
                    "now" => '' . time() . '',
                    "nonce_str" => '' . time() . '',
                    "store_id" => $store_id,
                    "store_name" => $store_name,
                    "device_id" => $device_id,
                    'rawdata' => $rawdata
                ];

                //子商户
                if ($wx_sub_merchant_id) {
                    $config['sub_mch_id'] = $wx_sub_merchant_id;
                }
                $obj = new WxBaseController();
                $url = $obj->get_wxpayface_authinfo_url;
                $key = $weixin_config->key;
                $config['sign'] = $obj->MakeSign($config, $key);
                $xml = $obj->ToXml($config);
                $re_data = $obj::postXmlCurl($config, $xml, $url, $useCert = false, $second = 30);
                $re_data = $obj::xml_to_array($re_data);
//                Log::info('微信a-获取微信刷脸调用凭证');
//                Log::info($re_data);
                if ($re_data['return_code'] == 'SUCCESS') {
                    $re_data = [
                        'return_code' => 'SUCCESS', //SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                        'return_msg' => null,
                        'result_code' => 'SUCCESS',
                        'result_msg' => '数据返回成功',
                        'device_id' => $data['device_id'],
                        'device_type' => $data['device_type'],
                        'store_id' => $data['store_id'],
                        'authinfo' => $re_data['authinfo'],
                        'expires_in' => $re_data['expires_in'],
                        'wx_appid' => '',
                        'wx_mchid' => '',
                        'type' => 'wechata'
                    ];
                } else {
                    $re_data = [
                        'return_code' => 'FALL', //SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                        'return_msg' => $re_data['return_msg'],
                        'device_id' => $data['device_id'],
                        'device_type' => $data['device_type'],
                        'store_id' => $data['store_id']
                    ];
                }
            } else {
                //微信刷脸交易信息
                $weixin_obj = new WeixinConfigController();
                $weixin_config = $weixin_obj->weixin_config_obj($config_id);
                $weixin_store = $weixin_obj->weixin_merchant($store_id, $store_pid);
                if (!$weixin_store) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '微信商户号不存在'
                    ];
                    return $this->return_data($err);
                }

                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;

                //公共配置
                $config = [
                    'appid' => $weixin_config->app_id,//$options['app_id'],
                    "mch_id" => $weixin_config->wx_merchant_id,//$options['payment']['merchant_id'],
                    "version" => "1",
                    "sign_type" => 'MD5',
                    "now" => '' . time() . '',
                    "nonce_str" => '' . time() . '',
                    "store_id" => $store_id,
                    "store_name" => $store_name,
                    "device_id" => $device_id,
                    'rawdata' => $rawdata
                ];

                //子商户
                if ($wx_sub_merchant_id) {
                    $config['sub_mch_id'] = $wx_sub_merchant_id;
                }
                $obj = new WxBaseController();
                $url = $obj->get_wxpayface_authinfo_url;
                $key = $weixin_config->key;
                $config['sign'] = $obj->MakeSign($config, $key);
                $xml = $obj->ToXml($config);
                $re_data = $obj::postXmlCurl($config, $xml, $url, $useCert = false, $second = 30);
                $re_data = $obj::xml_to_array($re_data);
                if ($re_data['return_code'] == 'SUCCESS') {
                    $re_data = [
                        'return_code' => 'SUCCESS',//SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                        'return_msg' => null,
                        'result_code' => 'SUCCESS',
                        'result_msg' => '数据返回成功',
                        'device_id' => $data['device_id'],
                        'device_type' => $data['device_type'],
                        'store_id' => $data['store_id'],
                        'authinfo' => $re_data['authinfo'],
                        'expires_in' => $re_data['expires_in'],
                        'wx_appid' => '',
                        'wx_mchid' => '',
                        'type' => 'wechat'
                    ];
                } else {
                    $re_data = [
                        'return_code' => 'FALL',//SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                        'return_msg' => $re_data['return_msg'],
                        'device_id' => $data['device_id'],
                        'device_type' => $data['device_type'],
                        'store_id' => $data['store_id']
                    ];
                }
            }

            return $this->return_data($re_data);
        } catch (\Exception $exception) {
            $err = [
                'return_code' => 'FALL',
                'return_msg' => $exception->getMessage() . $exception->getLine()
            ];
            return $this->return_data($err);
        }
    }


    //统一支付
    public function all_pay(Request $request)
    {
        try {
            $request_data = $request->all();

            //验证签名
            $check = $this->check_md5($request_data);
            if ($check['return_code'] == 'FALL') {
                return $this->return_data($check);
            }

            $check_data = [
                'total_amount' => '付款金额',
                'device_id' => '设备编号',
                'device_type' => '设备类型',
                'store_id' => '门店ID',
                'config_id' => '服务商ID'
            ];
            $check = $this->check_required($request_data, $check_data);
            if ($check) {
                $err = [
                    'return_code' => 'FALL',
                    'return_msg' => $check
                ];
                return $this->return_data($err);
            }
            $device_id = $request_data['device_id'];
            $store_id = $request_data['store_id'];
            $config_id = $request_data['config_id'];
            $merchant_id = isset($request_data['merchant_id']) ? $request_data['merchant_id'] : "0";
            $merchant_name = isset($request_data['merchant_name']) ? $request_data['merchant_name'] : "";
            $device_type = $request_data['device_type'];
            $other_no = isset($request_data['other_no']) ? $request_data['other_no'] : "";
            $out_trade_no = isset($request_data['out_trade_no']) ? $request_data['out_trade_no'] : "";
            $total_amount = isset($request_data['total_amount']) ? $request_data['total_amount'] : "";
            $remark = isset($request_data['remark']) ? $request_data['remark'] : "";
            $shop_name = isset($request_data['shop_name']) ? $request_data['shop_name'] : "交易";
            $shop_desc = isset($request_data['shop_desc']) ? $request_data['shop_desc'] : "交易";
            $pay_action = isset($request_data['pay_action']) ? $request_data['pay_action'] : "";
            $ftoken = isset($request_data['ftoken']) ? $request_data['ftoken'] : "";
            $face_code = isset($request_data['face_code']) ? $request_data['face_code'] : "";
            $auth_code = isset($request_data['auth_code']) ? $request_data['auth_code'] : "";
            $pay_method = isset($request_data['pay_method']) ? $request_data['pay_method'] : "scan";

            if ($auth_code == "" && $face_code) {
                $str = substr($face_code, 0, 2);
                if (in_array($str, ['13', '14'])) {
                    $auth_code = $face_code;
                }
            }
//            //找到设备ID
//            $Device = Device::where('device_no', $device_id)
//                ->where('store_id', $store_id)
//                ->where('device_type', $device_type)
//                ->select('')
//                ->first();
//
//            if (!$Device) {
//                $err = [
//                    'return_code' => 'FALL',
//                    'return_msg' => '设备不存在',
//                ];
//                return $this->return_data($err);
//            }
//
//            $merchant_id = $Device->merchant_id;
//            $merchant_name = $Device->merchant_name;
//            $store_id = $Device->store_id;
//            $config_id = $Device->config_id;
//
            //公共返回参数
            $re_data = [
                'return_code' => 'SUCCESS',
                'return_msg' => '返回成功',
                'result_code' => '',
                'result_msg' => '',
                'other_no' => $other_no,
                'out_trade_no' => $out_trade_no,
                'pay_time' => '',
                'total_amount' => $total_amount,
                'pay_amount' => '',
                'store_id' => $store_id,
                'payer_user_id' => '',
                'payer_logon_id' => '',
                'ways_source' => '',
                'ways_source_desc' => '',
                'remark' => $remark
            ];

            //请求参数
            $data = [
                'config_id' => $config_id,
                'merchant_id' => $merchant_id,
                'merchant_name' => $merchant_name,
                'code' => $auth_code,
                'total_amount' => $total_amount,
                'shop_price' => $total_amount,
                'remark' => $remark,
                'device_id' => $device_id,
                'device_type' => $device_type,
                'shop_name' => $shop_name,
                'shop_desc' => $shop_desc,
                'store_id' => $store_id,
                'other_no' => $other_no,
                'out_trade_no' => $out_trade_no,
                'pay_method' => $pay_method
            ];

            /**
             * 1.纯独立收银支付开始
             */
            //纯独立聚合收银支付开始-扫码/刷脸
            if ($pay_action == "pay") {
                //统一扫码支付扫码支付+微信刷脸
                if ($auth_code) {
                    $pay_obj = new PayBaseController();
                    $scan_pay_public = $pay_obj->scan_pay_public($data);
                    $tra_data_arr = json_decode($scan_pay_public, true);
                    if ($tra_data_arr['status'] != 1) {
                        $err = [
                            'return_code' => 'FALL',
                            'return_msg' => $tra_data_arr['message']
                        ];
                        return $this->return_data($err);
                    }

                    //用户支付成功
                    if ($tra_data_arr['pay_status'] == '1') {
                        //微信，支付宝支付凭证
                        $re_data['result_code'] = 'SUCCESS';
                        $re_data['result_msg'] = '支付成功';
                        $re_data['out_trade_no'] = $tra_data_arr['data']['out_trade_no'];
                        $re_data['pay_time'] = date('Y-m-d H:i:s', strtotime(isset($tra_data_arr['data']['pay_time']) ? $tra_data_arr['data']['pay_time'] : time()));
                        $re_data['ways_source'] = $tra_data_arr['data']['ways_source'];
                        $re_data['total_amount'] = isset($tra_data_arr['data']['total_amount']) ? $tra_data_arr['data']['total_amount'] : "";
                        $re_data['pay_amount'] = isset($tra_data_arr['data']['total_amount']) ? $tra_data_arr['data']['total_amount'] : "";
                        $re_data['discount_amount'] = isset($tra_data_arr['data']['discount_amount']) ? $tra_data_arr['data']['discount_amount'] : 0; //第三方优惠金额
                        $re_data['promotion_name'] = isset($tra_data_arr['data']['promotion_name']) ? $tra_data_arr['data']['promotion_name'] : ''; //优惠名称
                        $re_data['promotion_amount'] = isset($tra_data_arr['data']['promotion_amount']) ? $tra_data_arr['data']['promotion_amount'] : 0; //优惠总额；单位元，保留两位小数

                        $pay_voice = '';
                        $receipt_amount = $re_data['pay_amount'] - $re_data['promotion_amount']; //实收金额
                        if ($re_data['ways_source'] == 'alipay') {
                            $re_data['ways_source_desc'] = "支付宝";
                            $pay_voice = '支付宝支付' . $receipt_amount . '元';
                        }
                        if ($re_data['ways_source'] == 'weixin') {
                            $re_data['ways_source_desc'] = "微信支付";
                            $pay_voice = '微信支付' . $receipt_amount . '元';
                        }
                        if ($re_data['ways_source'] == 'jd') {
                            $re_data['ways_source_desc'] = "京东支付";
                            $pay_voice = '京东支付' . $receipt_amount . '元';
                        }
                        if ($re_data['ways_source'] == 'unionpay') {
                            $re_data['ways_source_desc'] = "银联支付";
                            $pay_voice = '银联支付' . $receipt_amount . '元';
                        }
                        //if (in_array($tra_data_arr['data']['ways_type'], [6003, 15003])) {
                        //    $re_data['ways_source_desc'] = "京东支付";
                        //    $pay_voice = '京东支付' . $receipt_amount . '元';
                        //}
                        $re_data['pay_voice'] = $pay_voice;
                    } elseif ($tra_data_arr['pay_status'] == '2') {
                        //正在支付
                        $re_data['result_code'] = 'USERPAYING';
                        $re_data['result_msg'] = '用户正在支付';
                        $re_data['out_trade_no'] = $tra_data_arr['data']['out_trade_no'];
                    } else {
                        //其他错误
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $tra_data_arr['message'];
                    }
                } else {
                    //微信原生刷脸支付-基本上不走了,
                    if ($face_code) {
                        $store = Store::where('store_id', $store_id)
                            ->select('user_id', 'config_id', 'merchant_id', 'store_name', 'user_id')
                            ->first();
                        $config_id = $store->config_id;
                        $merchant_id = $store->merchant_id;
                        $merchant_name = '';
                        $store_name = $store->store_name;
                        $store_pid = $store->pid;
                        $tg_user_id = $store->user_id;
                        $remark = "";
                        $total_amount = $data['total_amount'];
                        $pay_amount = $total_amount;//单位 分
                        $device_id = $data['device_id'];

                        $config = new WeixinConfigController();
                        $weixin_config = $config->weixin_config_obj($config_id);
                        $weixin_store = $config->weixin_merchant($store_id, $store_pid);
                        if (!$weixin_store) {
                            $err = [
                                'return_code' => 'FALL',
                                'return_msg' => '微信商户号不存在'
                            ];
                            return $this->return_data($err);
                        }
                        $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;

                        //公共配置
                        $config = [
                            'appid' => $weixin_config->app_id,
                            "mch_id" => $weixin_config->wx_merchant_id,
                            "version" => "1",
                            "sign_type" => 'MD5',
                            "nonce_str" => '' . time() . '',
                            "body" => '刷脸支付-' . $shop_name,
                            "out_trade_no" => $out_trade_no,
                            "total_fee" => $request_data['total_amount'] * 100,
                            "spbill_create_ip" => get_client_ip(),
                            "openid" => $request_data['open_id'],
                            "face_code" => $request_data['face_code']
                        ];

                        $useCert = false;
                        //子商户
                        if ($wx_sub_merchant_id) {
                            $config['sub_mch_id'] = $wx_sub_merchant_id;
                        }

                        $obj = new WxBaseController();
                        $key = $weixin_config->key;

                        $config['sign'] = $obj->MakeSign($config, $key);
                        $xml = $obj->ToXml($config);
                        $url = $obj->facepay_url;
                        $return_data = $obj::postXmlCurl($config, $xml, $url, $useCert, $second = 30);
                        $return_data = $obj::xml_to_array($return_data);

                        //插入数据库
                        $data_insert = [
                            'out_trade_no' => $out_trade_no,
                            'trade_no' => '',
                            'user_id' => $tg_user_id,
                            'store_id' => $store_id,
                            'store_name' => $store_name,
                            'buyer_id' => '',
                            'total_amount' => $total_amount,
                            'pay_amount' => $pay_amount,
                            'shop_price' => $pay_amount,
                            'payment_method' => '',
                            'status' => '2',
                            'pay_status' => 2,
                            'pay_status_desc' => '等待支付',
                            'merchant_id' => $merchant_id,
                            'merchant_name' => $merchant_name,
                            'remark' => $remark,
                            'device_id' => $device_id,
                            'config_id' => $config_id,
                            'ways_type' => 2008,
                            'ways_type_desc' => '微信刷脸支付',
                            'ways_source' => 'weixin',
                            'ways_source_desc' => '微信支付',
                            'company' => 'weixin',
                            'rate' => '0.00', //费率
                            'fee_amount' => '0.00' //费率
                        ];

                        //查询微信支付费率
                        $ways = StorePayWay::where('ways_type', '2000')
                            ->where('store_id', $store_id)
                            ->where('status', 1)
                            ->orderBy('sort', 'asc')
                            ->first();
                        if (!$ways) {
                            return json_encode([
                                'status' => 2,
                                'message' => '没有开通此类型通道'
                            ]);
                        }
                        //扫码费率入库
                        $data_insert['rate'] = $ways->rate;
                        $data_insert['fee_amount'] = $ways->rate * $total_amount / 100;

                        //入库
                        $insert_re = $this->insert_day_order($data_insert);
                        if (!$insert_re) {
                            $err = [
                                'return_code' => 'FALL',
                                'return_msg' => '订单未入库'
                            ];
                            return $this->return_data($err);
                        }

                        //请求状态
                        if ($return_data['return_code'] == 'SUCCESS') {
                            //支付成功
                            if ($return_data['result_code'] == 'SUCCESS') {
                                Order::where('out_trade_no', $out_trade_no)->update([
                                    'trade_no' => $return_data['transaction_id'],
                                    'buyer_id' => $return_data['openid'],
                                    'buyer_logon_id' => $return_data['openid'],
                                    'status' => 1,
                                    'pay_status_desc' => '支付成功',
                                    'pay_status' => 1,
                                    'payment_method' => $return_data['bank_type'],
                                    'buyer_pay_amount' => $total_amount / 100
                                ]);

                                //支付成功后的动作
                                $data = [
                                    'ways_type' => $data_insert['ways_type'],
                                    'company' => $data_insert['company'],
                                    'ways_type_desc' => $data_insert['ways_type_desc'],
                                    'source_type' => '2000', //返佣来源
                                    'source_desc' => '微信支付', //返佣来源说明
                                    'total_amount' => $total_amount,
                                    'out_trade_no' => $out_trade_no,
                                    'rate' => $data_insert['rate'],
                                    'fee_amount' => $data_insert['fee_amount'],
                                    'merchant_id' => $merchant_id,
                                    'store_id' => $store_id,
                                    'user_id' => $tg_user_id,
                                    'config_id' => $config_id,
                                    'store_name' => $store_name,
                                    'ways_source' => $data_insert['ways_source']
                                ];
                                PaySuccessAction::action($data);

                                $re_data['result_code'] = 'SUCCESS';
                                $re_data['result_msg'] = '支付成功';
                                $re_data['out_trade_no'] = $out_trade_no;
                                $re_data['pay_time'] = date('Y-m-d H:i:s', strtotime(isset($return_data['time_end'])));
                                $re_data['ways_source'] = $data_insert['ways_source'];
                                $re_data['ways_source_desc'] = $data_insert['ways_source_desc'];
                                $re_data['pay_voice'] = '微信刷脸支付' . $total_amount . '元';
                                $re_data['total_amount'] = $total_amount;
                                $re_data['pay_amount'] = $total_amount;
                            } else {
                                if ($return_data['err_code'] == "USERPAYING") {
                                    //正在支付
                                    $re_data['result_code'] = 'USERPAYING';
                                    $re_data['result_msg'] = '用户正在支付';
                                    $re_data['out_trade_no'] = $out_trade_no;
                                } else {
                                    $msg = $return_data['err_code_des'];//错误信息
                                    //其他错误
                                    $re_data['result_code'] = 'FALL';
                                    $re_data['result_msg'] = $msg;
                                }
                            }
                        } else {
                            //其他错误
                            $re_data['result_code'] = 'FALL';
                            $re_data['result_msg'] = $return_data['return_msg'];
                        }

                        return $this->return_data($re_data);
                    }

                    //支付宝原生刷脸支付
                    if ($ftoken) {

                    }
                }
            }

            /**
             * 2.酒店押金支付开始
             */
            //酒店押金支付始
            if ($pay_action == "deposit") {
                //支付宝刷脸支付押金
                if ($ftoken) {

                } //微信刷脸支付押金
                elseif ($face_code) {

                } //扫码押金
                else {

                }
            }


            /**
             * 公共返回
             */
            return $this->return_data($re_data);
        } catch (\Exception $exception) {
            $err = [
                'return_code' => 'FALL',
                'return_msg' => $exception->getMessage() . $exception->getLine(),
            ];
            return $this->return_data($err);
        }
    }


    //查询订单号状态
    public function all_pay_query(Request $request)
    {
        try {
            //获取请求参数
            $data = $request->all();

            //验证签名
            $check = $this->check_md5($data);
            if ($check['return_code'] == 'FALL') {
                return $this->return_data($check);
            }

            $other_no = isset($data['other_no']) ? $data['other_no'] : '';
            $out_trade_no = isset($data['out_trade_no']) ? $data['out_trade_no'] : "";
            $device_id = $data['device_id'];
            $device_type = $data['device_type'];
            $pay_action = isset($data['pay_action']) ? $data['pay_action'] : "";
            $store_id = isset($data['store_id']) ? $data['store_id'] : "";
            $config_id = isset($data['config_id']) ? $data['config_id'] : "";

            $pay_action = "pay";

            $check_data = [
                'device_id' => '设备编号',
                'device_type' => '设备类型',
                'store_id' => '门店ID',
                'config_id' => '服务商ID'
            ];
            $check = $this->check_required($data, $check_data);
            if ($check) {
                $err = [
                    'return_code' => 'FALL',
                    'return_msg' => $check
                ];
                return $this->return_data($err);
            }

            //公共返回参数
            $re_data = [
                'return_code' => 'SUCCESS',
                'return_msg' => '返回成功',
                'result_code' => '',
                'result_msg' => '',
                'other_no' => $other_no,
                'out_trade_no' => '',
                'pay_time' => '',
                'store_id' => $store_id,
                'total_amount' => '',
                'pay_amount' => '',
                'payer_user_id' => '',
                'payer_logon_id' => '',
                'ways_source' => '',
                'ways_source_desc' => '',
                'remark' => ''
            ];

            $data = [
                'out_trade_no' => $out_trade_no,
                'other_no' => $other_no,
                'store_id' => $store_id,
                'ways_type' => '',
                'config_id' => $config_id
            ];

            //押金支付查询
            if ($pay_action == "deposit") {
                $where = [];
                if (isset($out_order_no)) {
                    $where[] = ['out_order_no', '=', $out_order_no];
                }

                if (isset($out_trade_no)) {
                    $where[] = ['out_trade_no', '=', $out_trade_no];
                }

                $DepositOrder = DepositOrder::where($where)
                    ->select('out_order_no', 'out_trade_no', 'pay_amount', 'amount', 'ways_source', 'ways_source_desc', 'ways_company', 'operation_id', 'auth_no', 'out_request_no', 'deposit_status')
                    ->first();
                if (!$DepositOrder) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '订单号不存在'
                    ];
                    return $this->return_data($err);
                }

                $store = Store::where('store_id', $store_id)
                    ->select('id', 'config_id', 'user_id', 'pid')
                    ->first();
                if (!$store) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => '门店不存在'
                    ];
                    return $this->return_data($err);
                }

                $store_pid = $store->pid;
                $config_id = $store->config_id;

                //官方支付宝
                if ($DepositOrder->ways_company == "alipay") {
                    //支付宝预授权
                    $obj = new AliDepositController();

                    $isvconfig = new AlipayIsvConfigController();
                    $config = $isvconfig->AlipayIsvConfig($config_id);
                    $merchanr_info = $isvconfig->alipay_auth_info($store_id, $store_pid);

                    //查询
                    $data = [
                        'app_id' => $config->app_id,
                        'rsa_private_key' => $config->rsa_private_key,
                        'alipay_rsa_public_key' => $config->alipay_rsa_public_key,
                        'alipay_gateway' => $config->alipay_gateway,
                        'notify_url' => '',
                        'app_auth_token' => $merchanr_info->app_auth_token,
                        'out_order_no' => $DepositOrder->out_order_no,
                        'operation_id' => $DepositOrder->operation_id,
                        'auth_no' => $DepositOrder->auth_no,
                        'out_request_no' => $DepositOrder->out_request_no
                    ];
                    $re = $obj->base_fund_order_query($data);

                    //支付宝扫码预授权查询  0 系统错 1成功 2 等待用户确认 3失败
                    //0 系统错  3失败
                    if ($re['status'] == 0 || $re['status'] == 3) {
                        $err = [
                            'return_code' => 'FALL',
                            'return_msg' => $re['message']
                        ];
                        return $this->return_data($err);
                    }

                    $re['data']['store_id'] = $store_id;
                    $re['data']['ways_source'] = $DepositOrder->ways_source;
                    $re['data']['ways_source_desc'] = $DepositOrder->ways_source_desc;
                    $re['data']['out_trade_no'] = $DepositOrder->out_trade_no;

                    //冻结成功
                    if ($re['status'] == 1) {
                        //如果数据库是冻结中改变状态
                        if ($DepositOrder->deposit_status == 2) {
                            DepositOrder::where('out_order_no', $DepositOrder->out_order_no)->update([
                                'deposit_time' => $re['data']['gmt_trans'],
                                'deposit_status_desc' => '押金冻结成功',
                                'deposit_status' => 1,
                                'payer_user_id' => $re['data']['payer_user_id'],
                                'payer_logon_id' => $re['data']['payer_logon_id']
                            ]);
                        }

                        //返回参数
                        $re_data = [
                            'return_code' => 'SUCCESS',
                            'return_msg' => '返回成功',
                            'result_code' => 'SUCCESS',
                            'result_msg' => '支付成功',
                            'total_amount' => $DepositOrder->amount,
                            'pay_amount' => $DepositOrder->pay_amount,
                            'other_no' => $other_no,
                            'out_trade_no' => $DepositOrder->out_trade_no,
                            'pay_time' => $re['data']['gmt_trans'],
                            'store_id' => $store_id,
                            'payer_user_id' => $re['data']['payer_user_id'],
                            'payer_logon_id' => $re['data']['payer_logon_id'],
                            'ways_source' => $re['data']['ways_source'],
                            'ways_source_desc' => $re['data']['ways_source_desc']
                        ];
                        return $this->return_data($re_data);
                    } elseif ($re['status'] == 2) {
                        //返回参数
                        $re_data = [
                            'return_code' => 'SUCCESS',
                            'return_msg' => '返回成功',
                            'result_code' => 'USERPAYING',
                            'result_msg' => '等待成功',
                            'total_amount' => $DepositOrder->amount,
                            'pay_amount' => $DepositOrder->pay_amount,
                            'other_no' => $other_no,
                            'out_trade_no' => $DepositOrder->out_trade_no,
                            'pay_time' => '',
                            'store_id' => $store_id,
                            'payer_user_id' => '',
                            'payer_logon_id' => '',
                            'ways_source' => $re['data']['ways_source'],
                            'ways_source_desc' => $re['data']['ways_source_desc']
                        ];
                        return $this->return_data($re_data);
                    } else {
                        //其他错误
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $re['message'];
                    }
                }

                //官方微信
                if ($DepositOrder->ways_company == "weixin") {
                    //支付宝预授权
                    $obj = new WxDepositController();
                    $config = new WeixinConfigController();
                    $options = $config->weixin_config($config_id);
                    $weixin_store = $config->weixin_merchant($store_id, $store_pid);
                    $sub_merchant_id = $weixin_store->wx_sub_merchant_id;
                    $wx_sub_app_id = $weixin_store->wx_sub_app_id;
                    //查询
                    $data = [
                        'app_id' => $options['app_id'],
                        'key' => $options['payment']['key'],
                        'sub_app_id' => $wx_sub_app_id,
                        'mch_id' => $options['payment']['merchant_id'],
                        'sub_mch_id' => $sub_merchant_id,
                        'out_order_no' => $DepositOrder->out_order_no,
                    ];

                    $re = $obj->base_fund_order_query($data); //支付宝扫码预授权查询  0 系统错 1成功 2 等待用户确认 3失败
                    //0 系统错  3失败
                    if ($re['status'] == 0 || $re['status'] == 3) {
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $re['message'];
                        return $this->return_data($re_data);
//                        return json_encode([
//                            'status' => 2,
//                            'deposit_status' => '3',
//                            'message' => $re['message']
//                        ]);
                    }

                    $re['data']['store_id'] = $store_id;
                    $re['data']['ways_source'] = $DepositOrder->ways_source;
                    $re['data']['ways_source_desc'] = $DepositOrder->ways_source_desc;
                    $re['data']['out_trade_no'] = $DepositOrder->out_trade_no;

                    //冻结成功
                    if ($re['status'] == 1) {
                        //如果数据库是冻结中改变状态
                        if ($DepositOrder->deposit_status == 2) {
                            $insert_data = [
                                'deposit_time' => '' . "" . $re['data']['gmt_trans'] . "" . '',
                                'deposit_status_desc' => '押金冻结成功',
                                'deposit_status' => 1,
                                'payer_user_id' => $re['data']['openid'],
                                'payer_logon_id' => $re['data']['sub_openid'],
                                'trade_no' => $re['data']['transaction_id']
                            ];

                            DepositOrder::where('out_order_no', $DepositOrder->out_order_no)->update($insert_data);
                        }

                        //返回参数
                        $re_data = [
                            'return_code' => 'SUCCESS',
                            'return_msg' => '返回成功',
                            'result_code' => 'SUCCESS',
                            'result_msg' => '支付成功',
                            'other_no' => $other_no,
                            'total_amount' => $DepositOrder->amount,
                            'pay_amount' => $DepositOrder->pay_amount,
                            'out_trade_no' => $DepositOrder->out_trade_no,
                            'pay_time' => $re['data']['gmt_trans'],
                            'store_id' => $store_id,
                            'payer_user_id' => $re['data']['openid'],
                            'payer_logon_id' => $re['data']['sub_openid'],
                            'ways_source' => $DepositOrder->ways_source,
                            'ways_source_desc' => $DepositOrder->ways_source_desc
                        ];
                        return $this->return_data($re_data);
                    } elseif ($re['status'] == 2) {
                        //返回参数
                        $re_data = [
                            'return_code' => 'SUCCESS',
                            'return_msg' => '返回成功',
                            'result_code' => 'USERPAYING',
                            'result_msg' => '等待成功',
                            'other_no' => $other_no,
                            'total_amount' => $DepositOrder->amount,
                            'pay_amount' => $DepositOrder->pay_amount,
                            'out_trade_no' => $DepositOrder->out_trade_no,
                            'pay_time' => '',
                            'store_id' => $store_id,
                            'payer_user_id' => '',
                            'payer_logon_id' => '',
                            'ways_source' => $DepositOrder->ways_source,
                            'ways_source_desc' => $DepositOrder->ways_source_desc
                        ];
                        return $this->return_data($re_data);
                    } else {
                        //其他错误
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $re['message'];
                    }
                }
            }

            //聚合支付查询
            if ($pay_action == "pay") {
                $order_obj = new OrderController();
                $return = $order_obj->order_foreach_public($data);
                $tra_data_arr = json_decode($return, true);
                if ($tra_data_arr['status'] != 1) {
                    $err = [
                        'return_code' => 'FALL',
                        'return_msg' => $tra_data_arr['message']
                    ];
                    return $this->return_data($err);
                }

                $re_data['total_amount'] = isset($tra_data_arr['data']['total_amount']) ? $tra_data_arr['data']['total_amount'] : "";
                $re_data['pay_amount'] = isset($tra_data_arr['data']['pay_amount']) ? $tra_data_arr['data']['pay_amount'] : $tra_data_arr['data']['total_amount'];

                //用户支付成功
                if ($tra_data_arr['pay_status'] == '1') {
                    //微信，支付宝支付凭证
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['out_trade_no'] = $tra_data_arr['data']['out_trade_no'];
                    $re_data['pay_time'] = date('Y-m-d H:i:s', strtotime($tra_data_arr['data']['pay_time']));
                    $re_data['ways_source'] = $tra_data_arr['data']['ways_source'];
                    $re_data['amount'] = isset($tra_data_arr['data']['pay_amount']) ? $tra_data_arr['data']['pay_amount'] : $tra_data_arr['data']['total_amount'];
                    $re_data['discount_amount'] = isset($tra_data_arr['data']['discount_amount']) ? $tra_data_arr['data']['discount_amount'] : 0; //第三方优惠金额
                    $re_data['promotion_name'] = isset($tra_data_arr['data']['promotion_name']) ? $tra_data_arr['data']['promotion_name'] : ''; //优惠名称
                    $re_data['promotion_amount'] = isset($tra_data_arr['data']['promotion_amount']) ? $tra_data_arr['data']['promotion_amount'] : 0; //优惠总额；单位元，保留两位小数

                    $pay_voice = '';
                    $receipt_amount = $re_data['pay_amount'] - $re_data['promotion_amount']; //实收金额
                    if ($re_data['ways_source'] == 'alipay') {
                        $re_data['ways_source_desc'] = "支付宝";
                        $pay_voice = '支付宝支付' . $receipt_amount . '元';
                    }

                    if ($re_data['ways_source'] == 'weixin') {
                        $re_data['ways_source_desc'] = "微信支付";
                        $pay_voice = '微信支付' . $receipt_amount . '元';
                    }

                    if ($re_data['ways_source'] == 'jd') {
                        $re_data['ways_source_desc'] = "京东支付";
                        $pay_voice = '京东支付' . $receipt_amount . '元';
                    }

                    if ($re_data['ways_source'] == 'unionpay') {
                        $re_data['ways_source_desc'] = "银联支付";
                        $pay_voice = '银联支付' . $receipt_amount . '元';
                    }

                    $re_data['pay_voice'] = $pay_voice;
                } elseif ($tra_data_arr['pay_status'] == '2') {
                    //正在支付
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '用户正在支付';
                    $re_data['out_trade_no'] = $tra_data_arr['data']['out_trade_no'];
                } else {
                    //其他错误
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $tra_data_arr['message'];
                }
            }

            return $this->return_data($re_data);
        } catch (\Exception $exception) {
            $err = [
                'return_code' => 'FALL',
                'return_msg' => $exception->getMessage() . $exception->getLine()
            ];
            return $this->return_data($err);
        }
    }


    //订单号撤销接口
    public function order_pay_cancel(Request $request)
    {
        try {
            //获取请求参数
            $data = $request->all();

            //验证签名
            $check = $this->check_md5($data);
            if ($check['return_code'] == 'FALL') {
                return $this->return_data($check);
            }

            $other_no = isset($data['other_no']) ? $data['other_no'] : '';
            $out_trade_no = isset($data['out_trade_no']) ? $data['out_trade_no'] : "";
            $device_id = $data['device_id'];
            $device_type = $data['device_type'];
            $pay_action = isset($data['pay_action']) ? $data['pay_action'] : "";
            $store_id = isset($data['store_id']) ? $data['store_id'] : "";
            $config_id = isset($data['config_id']) ? $data['config_id'] : "";

            $pay_action = "pay";

            $check_data = [
                'device_id' => '设备编号',
                'device_type' => '设备类型',
                'store_id' => '门店ID',
                'config_id' => '服务商ID'
            ];
            $check = $this->check_required($data, $check_data);
            if ($check) {
                $err = [
                    'return_code' => 'FALL',
                    'return_msg' => $check,
                ];
                return $this->return_data($err);
            }

            $a = array('a', '_', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
            $b = str_ireplace($a, "", $out_trade_no);
            $day = substr($b, 0, 8);
            $table = 'orders_' . $day;
            if (Schema::hasTable($table)) {
                $obj = DB::table($table);
            } else {
                if (Schema::hasTable('order_items')) {
                    $obj = DB::table('order_items');
                } else {
                    $obj = DB::table('orders');
                }
            }

            //发起查询
            $order = $obj->where('store_id', $store_id)
                ->where('out_trade_no', $out_trade_no)
                ->select(
                    'id',
                    'ways_source',
                    'ways_type',
                    'config_id',
                    'out_trade_no'
                )->first();
            if (!$order) {
                $err = [
                    'return_code' => 'FALL',
                    'return_msg' => '订单号不存在'
                ];
                return $this->return_data($err);
            }

            $store = Store::where('store_id', $store_id)
                ->select('config_id', 'merchant_id', 'pid')
                ->first();
            if (!$store) {
                $err = [
                    'return_code' => 'FALL',
                    'return_msg' => '门店ID不存在'
                ];
                return $this->return_data($err);
            }

            $config_id = $store->config_id;
            $store_pid = $store->pid;
            $ways_type = $order->ways_type;
            $out_trade_no = $order->out_trade_no;

            try {
                //支付宝官方订单
                if (0) {
                    //配置
                    $isvconfig = new AlipayIsvConfigController();
                    $storeInfo = $isvconfig->alipay_auth_info($store_id, $store_pid);
                    $config = $isvconfig->AlipayIsvConfig($config_id);
                    $app_auth_token = $storeInfo->app_auth_token;
                    $aop = new AopClient();
                    $aop->rsaPrivateKey = $config->rsa_private_key;
                    $aop->appId = $config->app_id;
                    $aop->method = 'alipay.trade.cancel';
                    $aop->signType = "RSA2";//升级算法
                    $aop->gatewayUrl = $config->alipay_gateway;
                    $aop->format = "json";
                    $aop->charset = "GBK";
                    $aop->version = "2.0";
                    $requests = new AlipayTradeCancelRequest();
                    $requests->setBizContent("{" .
                        "    \"out_trade_no\":\"" . $out_trade_no . "\"" .
                        "  }");
                    $result = $aop->execute($requests, '', $app_auth_token);
                    $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
                    $resultCode = $result->$responseNode->code;
                    if (!empty($resultCode) && $resultCode == 10000) {
//                        Log::info('成功');
                    } else {
//                        Log::info('失败');
                    }
                }
            } catch (\Exception $exception) {

            }

            //公共返回参数
            $re_data = [
                'return_code' => 'SUCCESS',
                'return_msg' => '返回成功',
                'result_code' => 'SUCCESS',
                'result_msg' => '订单撤销成功',
                'store_id' => $store_id
            ];

            return $this->return_data($re_data);
        } catch (\Exception $exception) {
            $err = [
                'return_code' => 'FALL',
                'return_msg' => $exception->getMessage() . $exception->getLine()
            ];
            return $this->return_data($err);
        }
    }


}
