<?php
namespace App\Api\Controllers\Basequery;


use App\Api\Controllers\BaseController;
use App\Api\Controllers\Config\VbillConfigController;
use App\Api\Controllers\Merchant\OrderController;
use App\Common\PaySuccessAction;
use App\Models\AlipayIsvConfig;
use App\Models\AppConfigMsg;
use App\Models\AppLogo;
use App\Models\AppOem;
use App\Models\AppUpdate;
use App\Models\Bank;
use App\Models\JpushConfig;
use App\Models\Merchant;
use App\Models\MerchantStore;
use App\Models\MerchantWalletDetail;
use App\Models\MyBankCategory;
use App\Models\MyBankStore;
use App\Models\Order;
use App\Models\RefundOrder;
use App\Models\SmsConfig;
use App\Models\Store;
use App\Models\TfConfig;
use App\Models\TfStore;
use App\Models\User;
use App\Models\UserWalletDetail;
use App\Models\VbillStoreCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class SelectController extends BaseController
{

    /**
     * 收银员列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function merchant_lists(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $store_id = $request->get('store_id', '00000');

            $check_data = [
                'store_id' => '门店id',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $obj = DB::table('merchant_stores');
            $obj->join('merchants', 'merchant_stores.merchant_id', '=', 'merchants.id')
                ->where('merchant_stores.store_id', $store_id)
                ->select('merchants.id as merchant_id', 'merchants.logo', 'merchants.name', 'merchants.phone', 'merchants.type')
                ->get();

            $this->t = $obj->count();
            $data = $this->page($obj)->get();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);


        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }


    /**
     * 门店类型
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store_type(Request $request)
    {
        $data = [
            [
                'store_type' => 1,
                'store_type_desc' => '个体工商户',
            ],
            [
                'store_type' => 2,
                'store_type_desc' => '企业',
            ],
            [
                'store_type' => 3,
                'store_type_desc' => '小微商户',
            ],
            [
                'store_type' => 4,
                'store_type_desc' => '学校-教育行业',
            ],
        ];
        $this->status = '1';
        $this->message = '返回数据成功';

        return $this->format($data);
    }


    /**
     * 支付宝isv信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function alipay_isv_info(Request $request)
    {

        $store_id = $request->store_id;
        $config_id = '1234';
        $store = Store::where('store_id', $store_id)->select('config_id')->first();
        if ($store) {
            $config_id = $store->config_id;
        }
        $data = AlipayIsvConfig::where('config_id', $config_id)
            ->select('isv_name', 'isv_phone')
            ->first();
        $this->status = 1;
        $this->message = '返回数据成功';
        return $this->format($data);

    }


    /**
     * 查询门店经营类目
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store_category(Request $request)
    {
        $data = MyBankCategory::select('category_id', 'category_name')->get();

        $this->status = 1;
        $this->message = '返回数据成功';
        return $this->format($data);
    }


    public function bank(Request $request)
    {

        try {
            $keyword = $request->get('bankname');
            $company = $request->get('company', '');

            $where = [];

            if ($company == "dlb") {
                if ($keyword) {
                    $where[] = ['bank_name', 'like', '%' . $keyword . '%'];
                }
		
                $data = DB::table('dlb_bank')
                    ->select('bank_name as bankname')
                    ->where($where)
                    ->get();
            } else {
                if ($keyword) {
                    $where[] = ['bankname', 'like', '%' . $keyword . '%'];
                }
                $data = Bank::select('id','bankname')
                    ->where($where)
                    ->get();
            }

            return json_encode(['status' => 1, 'data' => $data]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }


    //查询银行信息
    public function sub_bank(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $bank_name = $request->get('bank_name', '');
            $cityCodeName = $request->get('cityCodeName', '');

            $where = [];
            if (!empty($bankCode)) {
                $where[] = ['bankCodeName',$bank_name];
            }
            if (!empty($cityCodeName)) {
                $where[] = ['cityCodeName', 'like', '%' . $cityCodeName . '%'];
            }
            $data = DB::table('bank_info_vbills')
                ->select('instOutCode as bank_no','bankName as sub_bank_name')
                ->where($where)
                ->get();

            return response()->json([
                'status' => 1,
                'data' => $data
            ]);

        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    //订单汇总查询
    public function order_query_a(Request $request)
    {

        $public = $this->parseToken();
        $store_id = $request->get('store_id', '');
        $ways_source = $request->get('ways_source', '');
        $ways_type = $request->get('ways_type', '');
        $time_start = $request->get('time_start', '');
        $time_end = $request->get('time_end', '');

        $return_type = $request->get('return_type', '');


    }


    //订单汇总查询
    public function order_query_b(Request $request)
    {

        $public = $this->parseToken();
        $store_id = $request->get('store_id', '');
        $ways_source = $request->get('merchant_id', '');
        $ways_type = $request->get('ways_type', '');
        $time_start = $request->get('time_start', '');
        $time_end = $request->get('time_end', '');

        $total_amount = 12.00;
        $get_amount = 10.00;
        $refund_amount = 11.00;
        $receipt_amount = 120.00;
        $fee_amount = 142.00;
        $mdiscount_amount = 129.90;
        $total_count = 160;
        $refund_count = rand(1, 20);

        $data = [
            [
                'type' => 'all',
                'desc' => '总订单统计',
                'total_amount' => number_format($total_amount, 2, '.', ''),
                'get_amount' => number_format($get_amount, 2, '.', ''),
                'refund_amount' => number_format($refund_amount, 2, '.', ''),
                'receipt_amount' => number_format($receipt_amount, 2, '.', ''),
                'fee_amount' => number_format($fee_amount, 2, '.', ''),
                'mdiscount_amount' => number_format($mdiscount_amount, 2, '.', ''),
                'total_count' => '' . $total_count . '',
                'refund_count' => '' . $refund_count . '',
            ],
            [
                'type' => 'alipay',
                'desc' => '支付宝订单统计',
                'total_amount' => number_format($total_amount, 2, '.', ''),
                'get_amount' => number_format($get_amount, 2, '.', ''),
                'refund_amount' => number_format($refund_amount, 2, '.', ''),
                'receipt_amount' => number_format($receipt_amount, 2, '.', ''),
                'fee_amount' => number_format($fee_amount, 2, '.', ''),
                'mdiscount_amount' => number_format($mdiscount_amount, 2, '.', ''),
                'total_count' => '' . $total_count . '',
                'refund_count' => '' . $refund_count . '',


            ], [
                'type' => 'weixin',
                'desc' => '微信订单统计',
                'total_amount' => number_format($total_amount, 2, '.', ''),
                'get_amount' => number_format($get_amount, 2, '.', ''),
                'refund_amount' => number_format($refund_amount, 2, '.', ''),
                'receipt_amount' => number_format($receipt_amount, 2, '.', ''),
                'fee_amount' => number_format($fee_amount, 2, '.', ''),
                'mdiscount_amount' => number_format($mdiscount_amount, 2, '.', ''),
                'total_count' => '' . $total_count . '',
                'refund_count' => '' . $refund_count . '',


            ],

        ];
        $this->status = 1;
        $this->message = '数据返回成功';
        return $this->format($data);

    }


    //删除这个手机号注册账号和门店信息
    public function del_store(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $code = $request->get('code');
            $phone = $request->get('phone');


        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }


    //更新app
    public function appUpdate(Request $request)
    {
        $type = $request->get('type', '');
        $app_id = $request->get('app_id');
        $action = $request->get('action', '1');
        $version = $request->get('version', '');

        $where = [];
        $check_data = [
            'app_id' => '包名ID',
            'type' => '类型',
            'version' => '当前版本号',
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            return json_encode([
                'status' => 2,
                'message' => $check
            ]);
        }


        //
        if ($type) {
            $where[] = ['type', '=', $type];
        }

        if ($app_id) {
            $where[] = ['app_id', '=', $app_id];
        }

        if ($version) {
            $where[] = ['version', '>', $version];
        }

        $app = AppUpdate::where($where)->first();
        $update_info = AppUpdate::where($where)->select('msg')->get();
        if ($app) {
            return json_encode([
                'status' => 1,
                'data' => [
                    'version' => $app->version,
                    'app_url' => $app->UpdateUrl,
                    'update_info' => $update_info,
                ],]);
        } else {
            return json_encode([
                'status' => 2,
                'message' => '没有记录'
            ]);
        }
    }


    //结算方式查询
    public function settle_mode_type(Request $request)
    {

        try {

            $type = $request->get('ways_type', '3001');
            $data = [
                [
                    'settle_mode_type' => '01',
                    'settle_mode_type_desc' => '结算到银行卡',
                    'agreement' => '银行收单结算协议',
                    'url' => url(''),
                ]
            ];
            if ($type == '1000') {
                $data = [
                    [
                        'settle_mode_type' => '01',
                        'settle_mode_type_desc' => '结算到支付宝',
                        'agreement' => '支付宝协议',
                        'url' => url(''),

                    ]
                ];
            }

            if ($type == '16001'||$type == '16002') {
                $data = [
                    [
                        'settle_mode_type' => '02',
                        'settle_mode_type_desc' => '结算到支付宝',
                        'agreement' => '支付宝协议',
                        'url' => url('')
                    ], 
		    [
                        'settle_mode_type' => '01',
                        'settle_mode_type_desc' => '结算到银行卡',
                        'agreement' => '支付宝协议',
                        'url' => url('')
                    ],
                ];
            }

            if ($type == '2000') {
                $data = [
                    [
                        'settle_mode_type' => '01',
                        'settle_mode_type_desc' => '结算到微信商户号',
                        'agreement' => '微信支付协议',
                        'url' => url(''),
                    ],
                ];
            }

            if ($type == '3001' || $type == '3002') {
                $data = [
                    [
                        'settle_mode_type' => '01',
                        'settle_mode_type_desc' => '结算到银行卡',
                        'agreement' => '快钱支付支付协议',
                        'url' => url(''),
                    ]
                ];
            }


            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);


        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }


    //app oem平台信息
    public function app_oem_info(Request $request)
    {
        try {
            $public = $this->parseToken();
            $config_id = $public->config_id;

            $app_id = $request->get('app_id', '');
            $app_oem = AppOem::where('config_id', $config_id)->first();
            $beianhao = $request->get('beianhao', '');
            $phone = $request->get('phone', '');
            $merchant_app_url = $request->get('merchant_app_url', '');
            $name = $request->get('name', '');
            $app_icon = $request->get('app_icon', '');
            if ($app_id == "") {
                $data = [
                    'status' => 1,
                    'data' => $app_oem,
                ];
                return json_encode($data);
            } else {
                //更新
                $data = [
                    'config_id' => $config_id,
                    'app_id' => $app_id,
                    'name' => $name,
                    'ym' => url('/'),
                    'beianhao' => $beianhao,
                    'phone' => $phone,
                    'merchant_app_url' => $merchant_app_url,
                    'keyword' => $name,
                    'title' => $name,
                    'body' => $name,

                ];

                if ($app_oem) {
                    $app_oem->update($data);
                    $app_oem->save();
                } else {
                    AppOem::create($data);
                }
                $AppConfigMsg = AppConfigMsg::where('config_id', $config_id)
                    ->first();
                $app_config = [
                    'config_id' => $config_id,
                    'app_id' => $app_id,
                    'ym' => url('/'),
                    'app_name' => $name,
                    'app_icon' => $app_icon,
                    'app_phone' => $phone,
                    'company' => $name,
                    'about_title' => $name,
                    'about_body' => $name,
                ];
                if ($AppConfigMsg) {
                    $AppConfigMsg->update($app_config);
                    $AppConfigMsg->save();
                } else {
                    AppConfigMsg::create($app_config);
                }

                return json_encode([
                    'status' => 1,
                    'message' => '保存成功'
                ]);

            }

        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }


    }


    //app 极光配置
    public function j_push_info(Request $request)
    {
        try {
            $public = $this->parseToken();
            $config_id = $public->config_id;
            if ($public->level > 1) {
                return json_encode([
                    'status' => 2,
                    'message' => '你没有权限设置'
                ]);
            }
            $DevKey = $request->get('DevKey', '');
            $API_DevSecret = $request->get('API_DevSecret', '');
            $JpushConfig = JpushConfig::where('config_id', $config_id)->first();
            if ($DevKey == "") {
                $data = [
                    'status' => 1,
                    'data' => $JpushConfig,
                ];
                return json_encode($data);
            } else {
                $data = [
                    'config_id' => $config_id,
                    'DevKey' => $DevKey,
                    'API_DevSecret' => $API_DevSecret,

                ];
                //更新
                if ($JpushConfig) {
                    $JpushConfig->update($data);
                    $JpushConfig->save();
                } else {
                    JpushConfig::create($data);
                }


                return json_encode([
                    'status' => 1,
                    'message' => '保存成功'
                ]);

            }

        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }


    }


    //app 短信信息配置
    public function sms_type(Request $request)
    {
        try {
            $public = $this->parseToken();
            $config_id = $public->config_id;
            if ($public->level > 1) {
                return json_encode([
                    'status' => 2,
                    'message' => '你没有权限设置'
                ]);
            }
            $SmsConfig = SmsConfig::where('config_id', '1234')
                ->select('type', 'type_desc')
                ->get();

            return json_encode([
                'status' => 1,
                'message' => '数据返回成功',
                'data' => $SmsConfig
            ]);


        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }


    }


    //app 短信信息配置
    public function sms_info(Request $request)
    {
        try {
            $public = $this->parseToken();
            $config_id = $public->config_id;
            if ($public->level > 1) {
                return json_encode([
                    'status' => 2,
                    'message' => '你没有权限设置'
                ]);
            }
            $app_key = $request->get('app_key', '');
            $app_secret = $request->get('app_secret', '');
            $type = $request->get('type', '');
            $TemplateCode = $request->get('TemplateCode', '');
            $SignName = $request->get('SignName', '');
            $SmsConfig = SmsConfig::where('config_id', $config_id)
                ->where('type', $type)
                ->first();

            if ($app_key == "") {
                $data = [
                    'status' => 1,
                    'data' => $SmsConfig,
                ];
                return json_encode($data);
            } else {
                $SmsConfig_pub = SmsConfig::where('config_id', '1234')
                    ->where('type', $type)
                    ->select('type_desc')
                    ->first();
                $data = [
                    'config_id' => $config_id,
                    'app_key' => $app_key,
                    'app_secret' => $app_secret,
                    'SignName' => $SignName,
                    'TemplateCode' => $TemplateCode,
                    'type' => $type,
                    'type_desc' => $SmsConfig_pub->type_desc,
                ];

                //更新
                if ($SmsConfig) {
                    $SmsConfig->update($data);
                    $SmsConfig->save();
                } else {
                    SmsConfig::create($data);
                }


                return json_encode([
                    'status' => 1,
                    'message' => '保存成功'
                ]);

            }

        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }


    }


    //同步订单状态状态
    public function update_order(Request $request)
    {
        try {
            $public = $this->parseToken();
            $store_id = $request->get('store_id');
            $out_trade_no = $request->get('out_trade_no');

            $a = array('a', '_', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
            $b = str_ireplace($a, "", $out_trade_no);
            $day = substr($b, 0, 8);
            $table = 'orders_' . $day;

            if (Schema::hasTable($table)) {
                $order = DB::table($table)->where('out_trade_no', $out_trade_no)
                    ->where('store_id', $store_id)
                    ->first();
            } else {
                $order = Order::where('out_trade_no', $out_trade_no)
                    ->where('store_id', $store_id)
                    ->first();
            }

            if (!$order) {
                return json_encode(['status' => 2, 'message' => "订单号不存在"]);
            }
            if ($order->pay_status == 6) {
                return json_encode(['status' => 2, 'message' => "退款订单无法同步状态"]);
            }


            $data = [
                'out_trade_no' => $order->out_trade_no,
                'store_id' => $order->store_id,
                'ways_type' => $order->ways_type,
                'config_id' => $order->config_id,
                'table' => $table,
            ];

            $obj = new OrderController();
            $return = $obj->order_foreach_public($data);
            return $return;
        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getLine()]);
        }
    }


    //app 位置logo
    public function app_logos_info(Request $request)
    {
        try {
            $public = $this->parseToken();
            $config_id = $public->config_id;

            $user = User::where('id', $public->user_id)
                ->first();

            $face_logo = $request->get('face_logo', ''); //刷脸logo
            $login_logo = $request->get('login_logo', ''); //刷脸登录logo
            $start_bk_img = $request->get('start_bk_img', ''); //启动背景图
            $qr_img = $request->get('qr_img', ''); //小程序二维码
            $ht_img = $request->get('ht_img', ''); //平台logo
            $ali_ad_default = $request->get('aliAdDefault', ''); //支付宝默认广告图
            $wechat_ad_default = $request->get('weChatAdDefault', ''); //微信默认广告图
            $favicon_url = $request->get('faviconUrl', ''); //网站图标

            $type = $request->get('type', '2'); //不传或'2'为获取数据，传其他数字为新增/修改

            $AppLogo = AppLogo::where('config_id', $config_id)->first();
            if (!$AppLogo) {
                $AppLogo = AppLogo::where('config_id', '1234')->first();
            }

            if ($type == "2") { //查询
                if ($AppLogo) {
                    $data = [
                        'status' => '1',
                        'data' => $AppLogo,
                    ];
                } else {
                    $data = [
                        'status' => '1',
                        'data' => [],
                    ];
                }

                return json_encode($data);
            } else { //更新
//                $hasPermission = $user->hasPermissionTo('logo配置');
//                if (!$hasPermission) {
//                    return json_encode([
//                        'status' => '2',
//                        'message' => '没有配置-logo配置-权限'
//                    ]);
//                }

                $data = [
                    'config_id' => $config_id,
                    'face_logo' => $face_logo,
                    'login_logo' => $login_logo,
                    'start_bk_img' => $start_bk_img
                ];
                if ($qr_img) $data['qr_img'] = $qr_img;
                if ($ht_img) $data['ht_img'] = $ht_img;
                if ($ali_ad_default) $data['ali_ad_default'] = $ali_ad_default;
                if ($wechat_ad_default) $data['wechat_ad_default'] = $wechat_ad_default;
                if ($favicon_url) $data['favicon_url'] = $favicon_url;

                if ($AppLogo) {
                    $AppLogo->update($data);
                    $res = $AppLogo->save();
                } else {
                    $res = AppLogo::create($data);
                }

                if ($res) {
                    return json_encode([
                        'status' => '1',
                        'message' => '保存成功'
                    ]);
                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => '保存失败'
                    ]);
                }
            }
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //计算平台的数据
    public function count_pt_data(Request $request)
    {
        $user = $this->parseToken();
        $store_id = $request->get('store_id', '');
        $user_id = $request->get('user_id', '');
        $company = $request->get('company', '');
        $company_name = $request->get('company_name', $company);
        $time_start_s = date('Y-m-d 00:00:00', time());
        $time_start_e = date('Y-m-d 23:59:59', time());
        $time_start = $request->get('time_start', '');
        $time_end = $request->get('time_end', '');
        $rate = $request->get('rate', '');
        $kp = $request->get('kp', '6%');
        $zd_rate =

            //限制时间
        $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
        $day = 31;
        //限制时间
        if ($date > $day) {

            return json_encode([
                'status' => 2,
                'message' => '时间跨度不能超过' . $day . '天'
            ]);

        }


        $MerchantOrderExcelDown = Cache::get('count_pt_data_excel');
        if ($MerchantOrderExcelDown) {
            dd('数据导出量比较大,为了不影响后台查询、关闭窗口后1分钟在操作');
        } else {
            Cache::put('count_pt_data_excel', '1', 1);
        }


        if (in_array($time_start, [null, ''])) {
            $time_start = $time_start_s;
        }

        if (in_array($time_end, [null, ''])) {
            $time_end = $time_start_e;
        }

        $amount_start = $request->get('amount_start', '');
        $amount_end = $request->get('amount_end', '');

        $user_name = User::where('id', $user_id)->select('name')->first()->name;
        //限制时间
        $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
        $day = 31;


        //限制时间
        if ($date > $day) {

            return json_encode([
                'status' => 2,
                'message' => '时间跨度不能超过' . $day . '天'
            ]);

        }

        //跨天操作
        $time_start_db = date('Ymd', strtotime($time_start));
        $time_end_db = date('Ymd', strtotime($time_end));
        $is_ct_time = 0;
        if ($time_start_db != $time_end_db) {
            $is_ct_time = 1;
        }


        $check_data = [
            'time_start' => '开始时间',
            'time_end' => '结束时间',
        ];
        $where = [];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            return json_encode([
                'status' => 2,
                'message' => $check
            ]);
        }
        //条件查询
        if ($time_start) {
            $time_start = date('Y-m-d H:i:s', strtotime($time_start));
            $where[] = ['created_at', '>=', $time_start];
        }
        if ($time_end) {
            $time_end = date('Y-m-d H:i:s', strtotime($time_end));
            $where[] = ['created_at', '<=', $time_end];
        }


        if ($amount_start) {
            $where[] = ['total_amount', '>=', $amount_start];
        }

        if ($amount_end) {
            $where[] = ['total_amount', '<=', $amount_end];
        }


        if ($company) {
            $where[] = ['company', $company];
        }

        if ($store_id) {
            $where[] = ['store_id', $store_id];
        }
        if ($user_id != "1") {
            if ($user_id) {

                $user_ids = $this->getSubIds($user_id);


            } else {
                $user_ids = $this->getSubIds($user->user_id);
            }
        }


        $day = date('Ymd', strtotime($time_start));
        $table = 'orders_' . $day;

        if (env('DB_D1_HOST')) {
            //有没有跨天
            if ($is_ct_time) {
                if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                    $obj = DB::connection("mysql_d1")->table('order_items');

                } else {
                    $obj = DB::connection("mysql_d1")->table('orders');
                }
            } else {

                if (Schema::hasTable($table)) {
                    $obj = DB::connection("mysql_d1")->table($table);

                } else {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj = DB::connection("mysql_d1")->table('order_items');

                    } else {
                        $obj = DB::connection("mysql_d1")->table('orders');
                    }
                }
            }
        } else {
            if ($is_ct_time) {
                if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                    $obj = DB::table('order_items');
                } else {
                    $obj = DB::table('orders');
                }
            } else {

                if (Schema::hasTable($table)) {
                    $obj = DB::table($table);

                } else {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj = DB::table('order_items');

                    } else {
                        $obj = DB::table('orders');
                    }
                }
            }
        }


        if (env('DB_D1_HOST')) {
            //有没有跨天
            if ($is_ct_time) {
                if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                    $obj1 = DB::connection("mysql_d1")->table('order_items');

                } else {
                    $obj1 = DB::connection("mysql_d1")->table('orders');
                }
            } else {

                if (Schema::hasTable($table)) {
                    $obj1 = DB::connection("mysql_d1")->table($table);

                } else {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj1 = DB::connection("mysql_d1")->table('order_items');

                    } else {
                        $obj1 = DB::connection("mysql_d1")->table('orders');
                    }
                }
            }
        } else {
            if ($is_ct_time) {
                if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                    $obj1 = DB::table('order_items');
                } else {
                    $obj1 = DB::table('orders');
                }
            } else {

                if (Schema::hasTable($table)) {
                    $obj1 = DB::table($table);

                } else {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj1 = DB::table('order_items');

                    } else {
                        $obj1 = DB::table('orders');
                    }
                }
            }
        }


        if ($user_id != "1") {
            $order_data = $obj->whereIn('user_id', $user_ids)
                ->where($where)
                ->whereIn('pay_status', [1, 6, 3])//成功+退款
                ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');


            $refund_obj = $obj1->whereIn('user_id', $user_ids)
                ->where($where)
                ->whereIn('pay_status', [6, 3])//退款
                ->select('total_amount');
        } else {
            $order_data = $obj->where($where)
                ->whereIn('pay_status', [1, 6, 3])//成功+退款
                ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');


            $refund_obj = $obj1->where($where)
                ->whereIn('pay_status', [6, 3])//退款
                ->select('total_amount');
        }


        //总的
        $total_amount = $order_data->sum('total_amount');//交易金额
        $refund_amount = $refund_obj->sum('total_amount');//退款金额
        $fee_amount = $order_data->sum('fee_amount');//结算服务费/手续费
        $mdiscount_amount = '0';// $order_data->sum('mdiscount_amount');//商家优惠金额
        $get_amount = $total_amount - $refund_amount - $mdiscount_amount;//商家实收，交易金额-退款金额
        $receipt_amount = $get_amount - $fee_amount;//实际净额，实收-手续费
        $total_count = '' . count($order_data->get()) . '';
        $refund_count = count($refund_obj->get());


    }


    //计算平台的数据导出
    public function count_pt_data_excel(Request $request)
    {
        $user = $this->parseToken();
        $store_id = $request->get('store_id', '');
        $user_id = $request->get('user_id', '');
        $company = $request->get('company', '');
        $company_name = $request->get('company_name', $company);
        $time_start_s = date('Y-m-d 00:00:00', time());
        $time_start_e = date('Y-m-d 23:59:59', time());
        $time_start = $request->get('time_start', '');
        $time_end = $request->get('time_end', '');
        $rate = $request->get('rate', '');
        $kp = $request->get('kp', '6%');
        $zd_rate = $request->get('zd', '');
        $zd_rate = isset($zd_rate) ? $zd_rate : "";
        $ways_source = $request->get('ways_source', '');
        //限制时间
        $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
        $day = 31;
        //限制时间
        if ($date > $day) {

            return json_encode([
                'status' => 2,
                'message' => '时间跨度不能超过' . $day . '天'
            ]);

        }


        $MerchantOrderExcelDown = Cache::get('count_pt_data_excel');
        if ($MerchantOrderExcelDown) {
            dd('数据导出量比较大,为了不影响后台查询、关闭窗口后1分钟在操作');
        } else {
            Cache::put('count_pt_data_excel', '1', 1);
        }


        if (in_array($time_start, [null, ''])) {
            $time_start = $time_start_s;
        }

        if (in_array($time_end, [null, ''])) {
            $time_end = $time_start_e;
        }

        $amount_start = $request->get('amount_start', '');
        $amount_end = $request->get('amount_end', '');

        $user_name = User::where('id', $user_id)->select('name')->first()->name;
        //限制时间
        $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
        $day = 31;


        //限制时间
        if ($date > $day) {

            return json_encode([
                'status' => 2,
                'message' => '时间跨度不能超过' . $day . '天'
            ]);

        }

        //跨天操作
        $time_start_db = date('Ymd', strtotime($time_start));
        $time_end_db = date('Ymd', strtotime($time_end));
        $is_ct_time = 0;
        if ($time_start_db != $time_end_db) {
            $is_ct_time = 1;
        }


        $check_data = [
            'time_start' => '开始时间',
            'time_end' => '结束时间',
        ];
        $where = [];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            return json_encode([
                'status' => 2,
                'message' => $check
            ]);
        }
        //条件查询
        if ($time_start) {
            $time_start = date('Y-m-d H:i:s', strtotime($time_start));
            $where[] = ['created_at', '>=', $time_start];
        }
        if ($time_end) {
            $time_end = date('Y-m-d H:i:s', strtotime($time_end));
            $where[] = ['created_at', '<=', $time_end];
        }


        if ($amount_start) {
            $where[] = ['total_amount', '>=', $amount_start];
        }

        if ($amount_end) {
            $where[] = ['total_amount', '<=', $amount_end];
        }


        if ($company) {
            $where[] = ['company', $company];
        }

        if ($ways_source) {
            $where[] = ['ways_source', $ways_source];
        }

        if ($store_id) {
            $where[] = ['store_id', $store_id];
        }

        if ($zd_rate) {
            $where[] = ['rate', $zd_rate];
        }

        if ($user_id != "1") {
            if ($user_id) {

                $user_ids = $this->getSubIds($user_id);


            } else {
                $user_ids = $this->getSubIds($user->user_id);
            }
        }


        $day = date('Ymd', strtotime($time_start));
        $table = 'orders_' . $day;

        if (env('DB_D1_HOST')) {
            //有没有跨天
            if ($is_ct_time) {
                if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                    $obj = DB::connection("mysql_d1")->table('order_items');

                } else {
                    $obj = DB::connection("mysql_d1")->table('orders');
                }
            } else {

                if (Schema::hasTable($table)) {
                    $obj = DB::connection("mysql_d1")->table($table);

                } else {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj = DB::connection("mysql_d1")->table('order_items');

                    } else {
                        $obj = DB::connection("mysql_d1")->table('orders');
                    }
                }
            }
        } else {
            if ($is_ct_time) {
                if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                    $obj = DB::table('order_items');
                } else {
                    $obj = DB::table('orders');
                }
            } else {

                if (Schema::hasTable($table)) {
                    $obj = DB::table($table);

                } else {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj = DB::table('order_items');

                    } else {
                        $obj = DB::table('orders');
                    }
                }
            }
        }


        if (env('DB_D1_HOST')) {
            //有没有跨天
            if ($is_ct_time) {
                if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                    $obj1 = DB::connection("mysql_d1")->table('order_items');

                } else {
                    $obj1 = DB::connection("mysql_d1")->table('orders');
                }
            } else {

                if (Schema::hasTable($table)) {
                    $obj1 = DB::connection("mysql_d1")->table($table);

                } else {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj1 = DB::connection("mysql_d1")->table('order_items');

                    } else {
                        $obj1 = DB::connection("mysql_d1")->table('orders');
                    }
                }
            }
        } else {
            if ($is_ct_time) {
                if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                    $obj1 = DB::table('order_items');
                } else {
                    $obj1 = DB::table('orders');
                }
            } else {

                if (Schema::hasTable($table)) {
                    $obj1 = DB::table($table);

                } else {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj1 = DB::table('order_items');

                    } else {
                        $obj1 = DB::table('orders');
                    }
                }
            }
        }


        if ($user_id != "1") {
            $order_data = $obj->whereIn('user_id', $user_ids)
                ->where($where)
                ->whereIn('pay_status', [1, 6, 3])//成功+退款
                ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');


            $refund_obj = $obj1->whereIn('user_id', $user_ids)
                ->where($where)
                ->whereIn('pay_status', [6, 3])//退款
                ->select('total_amount');
        } else {
            $order_data = $obj->where($where)
                ->whereIn('pay_status', [1, 6, 3])//成功+退款
                ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');


            $refund_obj = $obj1->where($where)
                ->whereIn('pay_status', [6, 3])//退款
                ->select('total_amount');
        }


        //总的
        $total_amount = $order_data->sum('total_amount');//交易金额
        $refund_amount = $refund_obj->sum('total_amount');//退款金额
        $fee_amount = $order_data->sum('fee_amount');//结算服务费/手续费
        $mdiscount_amount = '0';// $order_data->sum('mdiscount_amount');//商家优惠金额
        $get_amount = $total_amount - $refund_amount - $mdiscount_amount;//商家实收，交易金额-退款金额
        $receipt_amount = $get_amount - $fee_amount;//实际净额，实收-手续费
        $total_count = '' . count($order_data->get()) . '';
        $refund_count = count($refund_obj->get());


        $data = [
            0 => [
                'company' => $company_name,
                'ways_source'=>$ways_source,
                'zd'=>$zd_rate,
                'time_start' => $time_start,
                'time_end' => $time_end,
                'get_amount' => number_format($get_amount, 2, '.', ''),//商家实收，交易金额-退款金额
                'total_count' => '' . $total_count . '',//交易笔数
                'fee_amount' => number_format($fee_amount, 2, '.', ''),//结算服务费/手续费
                'rate' => $rate,
                'cbsxf' => $rate * $get_amount,
                'kp' => $kp,
                'kpfy' => number_format($fee_amount, 2, '.', '') - ($rate * $get_amount),
                'bkpfy' => (number_format($fee_amount, 2, '.', '') - ($rate * $get_amount)) * (1 - $kp)
            ]
        ];

        $y = date('Ym', strtotime($time_start));

        $filename = $user_name . '-' . $y . $company_name . '.csv';
        $s_array = ['筛选条件：服务商：' . $user_name];
        $tileArray = ['通道','类型','指定费率', '开始时间', '结束时间', '实收金额', '交易笔数', '交易手续费', '成本', '成本手续费', '开票税点', '开票返佣', '不开票返佣'];

        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 0);
        ob_end_clean();
        ob_start();
        header("Content-Type: text/csv");
        header("Content-Disposition:filename=" . $filename);
        $fp = fopen('php://output', 'w');
        fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));//转码 防止乱码(比如微信昵称(乱七八糟的))

        fputcsv($fp, $s_array);
        fputcsv($fp, []);
        fputcsv($fp, $tileArray);
        $index = 0;
        foreach ($data as $item) {
            $index++;
            fputcsv($fp, $item);
        }
        ob_flush();
        flush();
        ob_end_clean();

    }


    //tf报备
    public function tf_bb(Request $request)
    {


        $url = $request->url();
        $is_https = substr($url, 0, 5);
        $qd = $request->get('qd', 1);
        if ($is_https != "https") {
            $url = $request->server();
            return redirect('https://' . $url['SERVER_NAME'] . $url['REQUEST_URI']);
        }


        $TfConfig = TfConfig::where('config_id', '1234')
            ->where('qd', $qd)
            ->first();
        $obj = new \App\Api\Controllers\Tfpay\BaseController();
        $obj->mch_id = $TfConfig->mch_id;
        $obj->pub_key = $TfConfig->pub_key;
        $obj->pri_key = $TfConfig->pri_key;
        $method = '/openapi/merchant/open';


        $TfStore = TfStore::where('alipay_pid', '!=', $TfConfig->alipay_pid)
            ->where('qd', $qd)
            ->where('config_id', '!=', '18888880001')
            ->first();
        if (!$TfStore) {
            dd('全部报备结束');
        }

        //传化支付宝报备
        $post_data = [
            'sub_mch_id' => $TfStore->sub_mch_id,
            'channel' => '0',
            'alipay_pid' => $TfConfig->alipay_pid
        ];

        $re1 = $obj->api($post_data, $method, false);
        $TfStore->alipay_pid = $TfConfig->alipay_pid;
        $TfStore->save();


        //传化微信报备


        //新增app ID
        $post_data = [
            'sub_mch_id' => $TfStore->sub_mch_id,
            'sub_appid' => $TfConfig->wx_appid,
        ];
        $method = '/openapi/merchant/wechat/appid';
        $appid = $obj->api($post_data, $method, false);


        //报备支付授权目录
        $post_data = [
            'sub_mch_id' => $TfStore->sub_mch_id,
            'jsapi_path' => env('APP_URL') . '/api/tfpay/weixin/',
        ];
        $method = '/openapi/merchant/wechat/jsapi-path';
        $jsapipath = $obj->api($post_data, $method, false);


        dd($jsapipath);
    }


    //tf报备
    public function tf_store_bb(Request $request)
    {


        $url = $request->url();
        $is_https = substr($url, 0, 5);
        $store_id = $request->get('store_id', '');

        if ($is_https != "https") {
            $url = $request->server();
            return redirect('https://' . $url['SERVER_NAME'] . $url['REQUEST_URI']);
        }


        $TfConfig = TfConfig::where('config_id', '1234')
            ->first();
        $obj = new \App\Api\Controllers\Tfpay\BaseController();
        $obj->mch_id = $TfConfig->mch_id;
        $obj->pub_key = $TfConfig->pub_key;
        $obj->pri_key = $TfConfig->pri_key;
        $method = '/openapi/merchant/open';


        $TfStore = TfStore::where('store_id', '=', $store_id)
            ->first();
        if (!$TfStore) {
            dd('门店ID不正确');
        }

        //传化支付宝报备
        $post_data = [
            'sub_mch_id' => $TfStore->sub_mch_id,
            'channel' => '0',
            'alipay_pid' => '2088821716112265‬'
        ];

        $re1 = $obj->api($post_data, $method, false);
        $TfStore->alipay_pid = $TfConfig->alipay_pid;
        $TfStore->save();

        dd($re1);
    }


    //备案号查询
    public function beian(Request $request)
    {
        $server = $request->server();
        $SERVER_NAME = $server['SERVER_NAME'];
        $AppOem = AppOem::where('ym', $SERVER_NAME)->select('beianhao')->first();
        if (!$AppOem) {
            $AppOem = AppOem::where('config_id', '1234')->select('beianhao')->first();
        }

        if ($AppOem) {
            return json_encode([
                'status' => 1,
                'message' => '数据返回成功',
                'data' => [
                    'beianhao' => $AppOem->beianhao
                ]
            ]);
        } else {
            return json_encode([
                'status' => '2',
                'message' => '数据返回成功',
                'data' => [
                    'beianhao' => []
                ]
            ]);
        }
    }


    //小程序二维码读取/配置
    public function xcx_logos_info(Request $request)
    {
        try {
            $public = $this->parseToken();
            $config_id = $public->config_id;

            $qr_img = $request->get('qr_img', ''); //小程序二维码
            $type = $request->get('type', '2');

            $user = User::where('id', $public->user_id)
                ->where('is_delete', '0')
                ->first();
            if (!$user) {
                return json_encode([
                    'status' => '2',
                    'message' => '用户不存在或状态异常，请联系管理员',
                ]);
            }
//            $hasPermission = $user->hasPermissionTo('小程序二维码配置');
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '没有配置-小程序二维码配置-权限']);
//            }

            $AppLogo = AppLogo::where('config_id', $config_id)
                ->select(['id', 'config_id', 'qr_img', 'created_at', 'updated_at'])
                ->first();

            //查询
            if ($type == "2") {
                if (!$AppLogo || empty($AppLogo->qr_img)) {
                    //没有拿上级，上级没有拿平台
                    $p_user = User::where('id', $user->p_id)
                        ->where('is_delete', '0')
                        ->first();
                    if ($p_user) {
                        $AppLogo = AppLogo::where('config_id', $p_user->config_id)
                            ->select(['id', 'config_id', 'qr_img', 'created_at', 'updated_at'])
                            ->first();
                        if (!$AppLogo) {
                            $AppLogo = AppLogo::where('config_id', '1234')
                                ->select(['id', 'config_id', 'qr_img', 'created_at', 'updated_at'])
                                ->first();
                        }
                    } else {
                        $AppLogo = AppLogo::where('config_id', '1234')
                            ->select(['id', 'config_id', 'qr_img', 'created_at', 'updated_at'])
                            ->first();
                    }
                }

                if ($AppLogo) {
                    $data = [
                        'status' => '1',
                        'data' => $AppLogo,
                    ];
                } else {
                    $data = [
                        'status' => '2',
                        'data' => [],
                    ];
                }

                return json_encode($data);
            } else {
                //更新
                $data = [
                    'config_id' => $config_id,
                    'qr_img' => $qr_img,
                ];

                if ($AppLogo) {
                    $AppLogo->update($data);
                    $res = $AppLogo->save();
                } else {
                    $res = AppLogo::create($data);
                }

                if ($res) {
                    return json_encode([
                        'status' => '1',
                        'message' => '保存成功'
                    ]);
                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => '保存失败'
                    ]);
                }
            }
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    //获取平台logo
    public function get_logo(Request $request)
    {
        $appLogo = AppLogo::where('config_id', '1234')
            ->select('ht_img', 'favicon_url')
            ->first();
        if (!$appLogo) {
            $appLogo = AppLogo::select('ht_img', 'favicon_url')
                ->orderBy('updated_at', 'desc')
                ->first();
        }

        if ($appLogo) {
            return json_encode([
                'status' => '1',
                'message' => '数据返回成功',
                'data' => [
                    'indexLogo' => $appLogo->ht_img,
                    'faviconUrl' => $appLogo->favicon_url,
                ]
            ]);
        } else {
            return json_encode([
                'status' => '2',
                'message' => '数据返回成功',
                'data' => [
                    'indexLogo' => '',
                    'faviconUrl' => '',
                ]
            ]);
        }
    }


    /**
     * 随行付 门店经营类型
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function vbill_store_category(Request $request)
    {
        $level = $request->get('level', '3');
        $category_name = $request->get('categoryName', '');

        $where = [];
        if ($level) {
            $where[] = ['level', '=', $level];
        }
        if ($category_name) {
            $where[] = ['category_name', 'like', '%'. $category_name .'%'];
        }

//        $is_exist = Cache::has('vbill_store_category_'.$level);
//        if ($is_exist) {
//            $data = Cache::get('vbill_store_category_'.$level);
//        } else {
            $data = VbillStoreCategory::where($where)
                ->where('business_type', '线下')
                ->get();
//            Cache::put('vbill_store_category_'.$level, $data, 10000);
//        }

        $this->status = 1;
        $this->message = '返回数据成功';
        return $this->format($data);
    }


    public function selectDictionaries(Request $request)
    {
        try {
            $p_code = $request->get('p_code', ''); //app 查询

            $where = [];

            if($p_code){
                $where[] = ['name', '=', $p_code];
            }else{
                $public = $this->parseToken();

                $name = $request->get('name', '');
                $code = $request->get('code', '');

                if ($public->type == "merchant") {
                    $user_id = $public->merchant_id;
                    $user = Merchant::where('id', $user_id)->first();
                }

                if ($public->type == "user") {
                    $user_id = $public->user_id;
                    $user = User::where('id', $user_id)->first();
                }

//                $source = $user->source;
//
//                if($source){
//                    $where[] = ['remark', '=', $source];
//                }
                if($name){
                    $where[] = ['name', '=', $name];
                }
                if($code){
                    $where[] = ['code', '=', $code];
                }
            }

            $dataDictionaries = DB::table('data_dictionaries')->where($where)->select('name','code','code_value','remark')->get();
            if($dataDictionaries){
                $this->status = 1;
                $this->message = '数据返回成功';
            }else{
                $this->status = -1;
                $this->message = '查询数据失败';
            }

            return $this->format($dataDictionaries);

        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }

}
