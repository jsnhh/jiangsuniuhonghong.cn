<?php
namespace App\Api\Controllers\Basequery;


use App\Api\Controllers\BaseController;
use App\Models\MerchantStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Image;
use TencentYoutuyun\Conf;
use TencentYoutuyun\YouTu;

class UploadController extends BaseController
{

    public function upload(Request $request, $file_key = 'attach_name', $type = 'img')
    {
        try {
            $token = $request->get('token');
            $merchant = $this->parseToken($token);
            $height = $request->get('height', 100); // 图片高
            $width = $request->get('width', 100);   // 图片宽
            $is_img_compress = $request->get('is_img_compress', 1); // 图片是否压缩：默认1不压缩，2压缩

            if ($merchant == false) {
                return response()->json([
                    'status' => 2,
                    'message' => '账户过期请重新登录！'
                ]);
            }
            $img_type = $request->get('img_type', '');
            $file_key = trim($request->get('attach_name')) ? trim($request->get('attach_name')) : $file_key;
            $type = $request->get('type', 'img');

            $up = Input::hasFile($file_key); //检测是否有上传文件  返回布尔值
            if (!$up) {
                return response()->json([
                    'status' => 2,
                    'message' => '未识别到文件！'
                ]);
            }

            $public_dir = public_path();
            $disk_dir = $public_dir;
            $web_dir = 'upload';
            if (!is_dir($public_dir . '/' . $web_dir)) {
                return response()->json([
                    'status' => 2,
                    'message' => '服务器初始目录未设置！'
                ]);
            }

            // 上传图片
            if ($type == 'img') {
                if ($is_img_compress == 2) { // 缩略图
                    $web_dir .= '/s_images';
                    $disk_dir = $public_dir . '/' . $web_dir;
                } else {
                    $web_dir .= '/images';
                    $disk_dir = $public_dir . '/' . $web_dir;
                }
            }

            // 上传商品图片
            if ($type == 'shop') {
                if ($is_img_compress == 2) { // 缩略图
                    $web_dir .= '/s_images/shop/' . $merchant->id;
                    $disk_dir = $public_dir . '/' . $web_dir;
                } else {
                    $web_dir .= '/images/shop/' . $merchant->id;
                    $disk_dir = $public_dir . '/' . $web_dir;
                }
            }

            // 上传附件
            if ($type == 'file') {
                $web_dir .= '/attach/' . date('Y/m/d');
                $disk_dir = $public_dir . '/' . $web_dir;
            }

            $ok = true;
            !is_dir($disk_dir) && $ok = mkdir($disk_dir, 0777, true);
            if ($ok === false) {
                return response()->json([
                    'status' => 2,
                    'message' => '服务器权限不够'
                ]);
            }

            $file = Input::file($file_key);
            $extension = $file->getClientOriginalExtension(); //上传文件的后缀   png

            if(!in_array(strtolower($extension),['jpg','png','gif','jpeg','pem','mp4','key'])){
                return json_encode([
                    'status' => 2,
                    'message' => '格式不支持'
                ]);
            }

            $new_name = date('His') . '_' . mt_rand(100, 999) . '.' . $extension;
            $web_pic_url = url($web_dir . '/' . $new_name);

            // 压缩图片
            if ($is_img_compress == 2) {
                // 没有文件夹就创建
                if (!is_dir(public_path($web_dir))) {
                    mkdir(public_path($web_dir), 0777);
                }
                $img_obj = \Intervention\Image\Facades\Image::make($file);
                $img_obj->resize($width, $height);
                $img = $disk_dir . '/' . $new_name;
                $img_obj->save($img);
            } else {
                $return = $file->move($disk_dir, $new_name);
            }

            if (file_exists($disk_dir . '/' . $new_name)) {
                //上传到阿里云oss
                if (env('ALIOSS_AccessKeyId')) {
                    //阿里云oss
                    $AccessKeyId = env('ALIOSS_AccessKeyId');
                    $AccessKeySecret = env('ALIOSS_AccessKeySecret');
                    $endpoint = env('ALIOSS_endpoint');
                    $bucket = env('ALIOSS_bucket');
                    $object = $new_name;
                    try {
                        $content = file_get_contents($disk_dir . '/' . $new_name);
                        $ossClient = new \OSS\OssClient($AccessKeyId, $AccessKeySecret, $endpoint);
                        $data = $ossClient->putObject($bucket, $object, $content);
                        $web_pic_url = $data['oss-request-url'];
                        //删除本地图片
                    } catch (\OSS\Core\OssException $e) {
                        Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
                    }
                }

                //得到返回图片
                $data_return = [
                    'img_url' => $web_pic_url,
                ];

                //识别营业执照
                $is_yyzz = 1;
                if ($is_yyzz && $img_type == '1') {
                    $data_return['store_license_no'] = '';
                    $data_return['store_license_stime'] = '';
                    $data_return['store_license_time'] = '';
                    $data_return['is_long_time'] = '0';

                    try {
                        $aiObject = new AiController();
                        $uploadAiRet = $aiObject->businessLicenseUrl($web_pic_url);

                        if(isset($uploadAiRet)){
                            $data_return['store_license_no'] = $uploadAiRet['business_code'];
                            $data_return['store_license_stime'] = $uploadAiRet['busEffDt'];
                            $data_return['store_license_time'] =$uploadAiRet['busExpDt'] == '--' ? '长期' : $uploadAiRet['busExpDt'];
                            $data_return['store_name'] = $uploadAiRet['store_name'];
                            $data_return['store_address'] = $uploadAiRet['mercAdds'];
                        }
                    } catch (\Exception $e) {
                        Log::info('upload-error: ');
                        Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
                    }
                }

                //识别身份证正面
                $is_card_a = 1;
                if ($is_card_a && $img_type == '2') {
                    $data_return['sfz_name'] = '';
                    $data_return['sfz_no'] = '';
                    try {
                        $aiObject = new AiController();
                        $uploadAiRet = $aiObject->idcardFrontUrl($web_pic_url);

                        if(isset($uploadAiRet)){
                            $data_return['sfz_name'] = $uploadAiRet['username'];
                            $data_return['sfz_no'] = $uploadAiRet['id_no'];
                            $data_return['sfz_address'] = $uploadAiRet['address'];
                        }
                    } catch (\Exception $e) {
                        Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
                    }
                }

                //识别身份证反面
                $is_card_b = 1;
                if ($is_card_b && $img_type == '3') {
                    $data_return['sfz_stime'] = '';
                    $data_return['sfz_time'] = '';
                    try {
                        $aiObject = new AiController();
                        $uploadAiRet = $aiObject->idcardBackUrl($web_pic_url);

                        if(isset($uploadAiRet)){
                            $data_return['sfz_stime'] = $uploadAiRet['head_sfz_stime'];
                            $data_return['sfz_time'] = $uploadAiRet['head_sfz_time'] == '9999-12-31' ? '长期' : $uploadAiRet['head_sfz_time'];

                        }
                    } catch (\Exception $e) {
                        Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
                    }
                }

                //识别银行卡正面
                $is_bank_a = 1;
                if ($is_bank_a && $img_type == '4') {
                    $data_return['store_bank_no'] = '';
                    $data_return['bank_name'] = '';

                    try {
                        $aiObject = new AiController();
                        $uploadAiRet = $aiObject->bankcardBackUrl($web_pic_url);

                        if(isset($uploadAiRet)){
                            $data_return['store_bank_no'] = $uploadAiRet['bank_card_number'];
                            $data_return['bank_name'] = $uploadAiRet['bank_name'];
                        }
                    } catch (\Exception $e) {
                        Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
                    }
                }

                return response()->json([
                    'status' => 1,
                    'message' => '上传成功！',
                    'data' => $data_return,
                ]);
            }

            return response()->json([
                'status' => 2,
                'message' => '上传失败！'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => -1,
                'message' => $e->getMessage().' | '.$e->getFile().' | '.$e->getLine()
            ]);
        }
    }


    //服务端上传图片
    public function webupload(Request $request)
    {
        try {
            $action = $request->get('act'); // 获取GET参数
            $token = $request->get('token');
            $merchant = $this->parseToken($token);

            if ($merchant == false) {
                return response()->json([
                    'status' => 2,
                    'message' => '账户过期请重新登录！'
                ]);
            }

            $type = $request->get('type', 'store');
            $img_type = $request->get('img_type', '');
            $file_type = $request->get('file_type', 'img'); //img file

            if ($file_type == "img") {
                //上传图片具体操作
                $file_name = $_FILES['img_upload']['name'];
                $file_type = $_FILES["img_upload"]["type"];
                $file_tmp = $_FILES["img_upload"]["tmp_name"];
                $file_error = $_FILES["img_upload"]["error"];
                $file_size = $_FILES["img_upload"]["size"];
            } else {
                //上传文件具体操作
                $file_name = $_FILES['file']['name'];
                $file_type = $_FILES["file"]["type"];
                $file_tmp = $_FILES["file"]["tmp_name"];
                $file_error = $_FILES["file"]["error"];
                $file_size = $_FILES["file"]["size"];
            }

            if ($file_error > 0) { // 出错
                return json_encode([
                    'status' => 2,
                    'message' => $file_error,
                ]);
            }

            if ($file_size > 1048576  ) { // 文件太大了 1Mb， 0.5 -524288
                return json_encode([
                    'status' => 2,
                    'message' => '上传文件过大',
                ]);
            }

            $file_name_arr = explode('.', $file_name);
            //域名授权目录提交
            $file_first = $merchant->phone.time();
            if ($type == 'wxauth') {
                $file_first = $file_name_arr[0];
            }
            $new_file_name = $file_first . '.' . $file_name_arr[1];

            if(!in_array(strtolower($file_name_arr[1]),['jpg','png','gif','jpeg','pem','mp4','key','txt', 'pfx','cer'])){
                return json_encode([
                    'status' => 2,
                    'message' => '格式不支持',
                ]);
            }

            $file_path = "/upload/images/" . $new_file_name;
            if ($type == 'wxfile') {
                $file_path = "/upload/images/" . $new_file_name;
            }

            //域名授权目录提交
            if ($type == 'wxauth') {
                $file_path = "/" . $new_file_name;
            }

            if ($type == "store") {
                $file_path = "/upload/images/" . $new_file_name;
            }

            if (file_exists($file_path)) {
                return json_encode([
                    'status' => 2,
                    'message' => '文件已存在',
                ]);
            } else {
                //上传到腾讯云cos
//                $cosClient = new \Qcloud\Cos\Client(
//                    array(
//                        'region' => env('TENCENT_Region'), //设置一个默认的存储桶地域
//                        'schema' => 'http', //协议头部，默认为http
//                        'credentials'=> array(
//                            'secretId'  => env('TENCENT_SecretId'),
//                            'secretKey' => env('TENCENT_SecretKey')
//                        )
//                    )
//                );
//
//                try {
//                    $key = $new_file_name;
//                    $data = $cosClient->putObject(array(
//                        'Bucket' => env('TENCENT_Bucket'),
//                        'Key' => $key,
//                        'Body' => fopen($file_tmp, 'rb')
//                    ));
//                    // Log::info('腾讯云cos结果：');
//                    // Log::info($data);
//                    if($data['Location']){
//                        $url = $data['Location'];
//                        $url = 'http://'. $url;
//                    }else{
//                        return json_encode([
//                            'status' => 2,
//                            'message' => '系统错误重新上传',
//                        ]);
//                    }
//
//                    // Log::info('===========腾讯云cos结果：' .$url);
//
//                }catch (\Exception $e){
//                    Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
//                }

                $upload_result = move_uploaded_file($file_tmp, public_path() . $file_path); // 此函数只支持 HTTP POST 上传的文件
                if ($upload_result) {
                    $url = url($file_path);
                    if ($type == 'wxfile') {
                        $url = $file_path;
                    }

//                    //上传到阿里云oss
//                    $oss = 0;
////                    $oss = env('ALIOSS_Open');
//                    if ($file_type == "video/mp4") {
//                        $oss = 1;
//                    }
////                    Log::info('上传到阿里云oss：');
////                    Log::info($oss);
////                    Log::info(env('ALIOSS_AccessKeyId'));
//
//                    if ($oss && env('ALIOSS_AccessKeyId')) {
//                        //阿里云oss
//                        $AccessKeyId = env('ALIOSS_AccessKeyId');
//                        $AccessKeySecret = env('ALIOSS_AccessKeySecret');
//                        $endpoint = env('ALIOSS_endpoint');
//                        $bucket = env('ALIOSS_bucket');
//
//                        $object = $new_file_name;
//                        try {
//                            $content = file_get_contents(public_path() . $file_path);
//                            $ossClient = new \OSS\OssClient($AccessKeyId, $AccessKeySecret, $endpoint);
//                            $data = $ossClient->putObject($bucket, $object, $content);
////                            Log::info('阿里云OSS结果：');
////                            Log::info($data);
//
//                            $url = $data['oss-request-url'];
//                            //删除本地图片
//                        } catch (\OSS\Core\OssException $e) {
//                            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
//                        }
//                    }

                    //识别营业执照
                    $is_yyzz = 1;
                    if ($is_yyzz && $img_type == '1') {
                        $data_return['store_license_no'] = '';
                        $data_return['store_license_time'] = '';
                        $data_return['store_license_stime'] = '';
                        $data_return['is_long_time'] = '0';

                        try {

                            $aiObject = new AiController();
                            $uploadAiRet = $aiObject->businessLicenseUrl($url);

                            if(isset($uploadAiRet)){
                                $data_return['store_license_no'] = $uploadAiRet['business_code'];
                                $data_return['store_license_stime'] = $uploadAiRet['busEffDt'];
                                $data_return['store_license_time'] =$uploadAiRet['busExpDt'] == '--' ? '长期' : $uploadAiRet['busExpDt'];

                                $data_return['store_name'] = $uploadAiRet['store_name'];
                                $data_return['store_address'] = $uploadAiRet['mercAdds'];
                            }

                        } catch (\Exception $e) {
                            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
                        }
                    }

                    //识别身份证正面
                    $is_card_a = 1;
                    if ($is_card_a && $img_type == '2') {
                        $data_return['sfz_name'] = '';
                        $data_return['sfz_no'] = '';
                        try {
                            $aiObject = new AiController();
                            $uploadAiRet = $aiObject->idcardFrontUrl($url);

                            if(isset($uploadAiRet)){
                                $data_return['sfz_name'] = $uploadAiRet['username'];
                                $data_return['sfz_no'] = $uploadAiRet['id_no'];
                                $data_return['sfz_address'] = $uploadAiRet['address'];
                            }
                        } catch (\Exception $e) {
                            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
                        }
                    }

                    //识别身份证反面
                    $is_card_b = 1;
                    if ($is_card_b && $img_type == '3') {
                        $data_return['sfz_time'] = '';
                        $data_return['sfz_stime'] = '';

                        try {
                            $aiObject = new AiController();
                            $uploadAiRet = $aiObject->idcardBackUrl($url);

                            if(isset($uploadAiRet)){
                                $data_return['sfz_stime'] = $uploadAiRet['head_sfz_stime'];
                                $data_return['sfz_time'] = $uploadAiRet['head_sfz_time'] == '9999-12-31' ? '长期' : $uploadAiRet['head_sfz_time'];

                            }
                        } catch (\Exception $e) {
                            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
                        }
                    }

                    //识别银行卡正面
                    $is_bank_a = 1;
                    if ($is_bank_a && $img_type == '4') {
                        $data_return['store_bank_no'] = '';
                        $data_return['bank_name'] = '';

                        try {
                            $aiObject = new AiController();
                            $uploadAiRet = $aiObject->bankcardBackUrl($url);

                            if(isset($uploadAiRet)){
                                $data_return['store_bank_no'] = $uploadAiRet['bank_card_number'];
                                $data_return['bank_name'] = $uploadAiRet['bank_name'];
                            }
                        } catch (\Exception $e) {
                            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
                        }
                    }

                    $data_return['img_url'] = $url;

                    return json_encode([
                        'status' => 1,
                        'data' => $data_return,
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '系统错误重新上传',
                    ]);
                }
            }
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getLine(),
            ]);
        }
    }


    public function file_content(Request $request)
    {
        try {
            $token = $request->get('token');
            $this->parseToken($token);
            $store_id = $request->get('store_id');
            $merchant_id = $request->get('merchant_id');
            $http_curl = $request->get('http_curl', '');
            $content = $request->get('content', '');
            $gs = $request->get('gs', '.mp3');
            $type = $request->get('type', $store_id);
            $price = $request->get('price', $merchant_id . rand(1, 100));

            if ($http_curl) {
                $obj = new \App\Api\Controllers\Suzhou\BaseController();
                $content = $obj->http_curl($http_curl, json_encode(''));
            }

            $file = '/upload/' . $type . $price . $gs;
            $file1 = public_path() . $file;

            if (!file_exists($file1)) {
                $fp = fopen($file1, 'w');
                fwrite($fp, $content);
                fclose($fp);
            }

            $url = url($file);

            return json_encode([
                'status' => 1,
                'message' => '返回成功',
                'data' => [
                    'url' => $url
                ]
            ]);
        } catch (\Exception $e) {
            return json_encode([
                'status' => 2,
                'message' => $e->getMessage().' | '.$e->getFile().' | '.$e->getLine()
            ]);
        }
    }


}
