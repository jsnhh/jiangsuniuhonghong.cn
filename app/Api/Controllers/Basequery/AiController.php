<?php

namespace App\Api\Controllers\Basequery;


use App\Api\Controllers\BaseController;
use App\Models\MerchantStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Image;
use AipOcr;

class AiController extends BaseController
{
    private $APP_ID = '48281367';
    private $API_KEY = 'mQ3Hd9cMUwRFHZLGmUUdPBG6';
    private $SECRET_KEY = 'pkcQZVI6AYA3DGPTHsYiCrGKWQQzBLxZ';
    private $apiOcr;
    private $uploadpath;

    public function init()
    {
        $this->apiOcr = new AipOcr($this->APP_ID, $this->API_KEY, $this->SECRET_KEY);
        $this->uploadpath = public_path();
    }


    /**
     * 营业执照识别
     * @param Request $request
     */
    public function businessLicense(Request $request)
    {
        try {
            $obj = [];
            $this->init();
            $fileName = $request->get('fileName');
            if (empty($fileName)) {
                return response()->json([
                    'status' => 2,
                    'message' => '文件不存在'
                ]);
            }
            $img_base64 = file_get_contents($fileName);
            $result = $this->apiOcr->businessLicense($img_base64);
            if (!isset($result['error_code'])) {
                $obj['business_code'] = $result['words_result']['社会信用代码']['words'];
                if (empty($obj['business_code']) || $obj['business_code'] == '无' ||
                    strlen($obj['business_code']) < 10) {
                    $obj['business_code'] = '';
                }
                $obj['actnam'] = $result['words_result']['法人']['words'];
                if (empty($obj['actnam']) || $obj['actnam'] == '无') {
                    $obj['actnam'] = '';
                }
                $obj['store_name'] = $result['words_result']['单位名称']['words'];
                if (empty($obj['store_name']) || $obj['store_name'] == '无') {
                    $obj['store_name'] = '';
                }
                $busEffDt = $result['words_result']['成立日期']['words'];
                if (!empty($busEffDt) && $busEffDt != '无' &&
                    strlen($busEffDt) >= 10) {
                    $busEffDt = str_replace("年", "-", $busEffDt);
                    $busEffDt = str_replace("月", "-", $busEffDt);
                    $busEffDt = str_replace("日", "", $busEffDt);
                    $obj['busEffDt'] = $busEffDt;
                } else {
                    $obj['busEffDt'] = '';
                }
                $busExpDt = $result['words_result']['有效期']['words'];
                if (!empty($busExpDt) && $busExpDt != '无' && $busExpDt != '长期' &&
                    strlen($busEffDt) >= 10) {
                    $busExpDt = str_replace("年", "-", $busExpDt);
                    $busExpDt = str_replace("月", "-", $busExpDt);
                    $busExpDt = str_replace("日", "", $busExpDt);
                    $obj['busExpDt'] = $busExpDt;
                } else {
                    $obj['busExpDt'] = '9999-12-31';
                }
                $obj['mercAdds'] = $result['words_result']['地址']['words'];
                if (empty($obj['mercAdds']) || $obj['mercAdds'] == '无') {
                    $obj['mercAdds'] = '';
                }
                return response()->json([
                    'status' => 1,
                    'data' => $obj
                ]);
            }else{
                return response()->json([
                    'status' => 2,
                    'data' => '识别错误'
                ]);
            }

        } catch (\Exception $e) {
            return response()->json([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }


    public function businessLicenseUrl($fileName)
    {
        try {
            $obj = [];
            $this->init();
            if (empty($fileName)) {
                return null;
            }
            $img_base64 = file_get_contents($fileName);
            $result = $this->apiOcr->businessLicense($img_base64);
            if (!isset($result['error_code'])) {
                $obj['business_code'] = $result['words_result']['社会信用代码']['words'];
                if (empty($obj['business_code']) || $obj['business_code'] == '无' ||
                    strlen($obj['business_code']) < 10) {
                    $obj['business_code'] = '';
                }
                $obj['actnam'] = $result['words_result']['法人']['words'];
                if (empty($obj['actnam']) || $obj['actnam'] == '无') {
                    $obj['actnam'] = '';
                }
                $obj['store_name'] = $result['words_result']['单位名称']['words'];
                if (empty($obj['store_name']) || $obj['store_name'] == '无') {
                    $obj['store_name'] = '';
                }
                $busEffDt = $result['words_result']['成立日期']['words'];
                if (!empty($busEffDt) && $busEffDt != '无' &&
                    strlen($busEffDt) >= 10) {
                    $busEffDt = str_replace("年", "-", $busEffDt);
                    $busEffDt = str_replace("月", "-", $busEffDt);
                    $busEffDt = str_replace("日", "", $busEffDt);
                    $obj['busEffDt'] = $busEffDt;
                } else {
                    $obj['busEffDt'] = '';
                }
                $busExpDt = $result['words_result']['有效期']['words'];
                if (!empty($busExpDt) && $busExpDt != '无' && $busExpDt != '长期' &&
                    strlen($busEffDt) >= 10) {
                    $busExpDt = str_replace("年", "-", $busExpDt);
                    $busExpDt = str_replace("月", "-", $busExpDt);
                    $busExpDt = str_replace("日", "", $busExpDt);
                    $obj['busExpDt'] = $busExpDt;
                } else {
                    $obj['busExpDt'] = '9999-12-31';
                }
                $obj['mercAdds'] = $result['words_result']['地址']['words'];
                if (empty($obj['mercAdds']) || $obj['mercAdds'] == '无') {
                    $obj['mercAdds'] = '';
                }
                return $obj;
            }else{
                return null;
            }

        } catch (\Exception $e) {
            return response()->json([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }

    /**
     * 身份证正面
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function idcardFrontUrl ($fileName)
    {
        try {
            $obj = [];
            $this->init();
            if (empty($fileName)) {
                return null;
            }
            $img_base64 = file_get_contents($fileName);

            $result = $this->apiOcr->idcard($img_base64, "front");

            if (!isset($result['error_code'])) {
                $obj['username'] = $result['words_result']['姓名']['words'];
                $obj['id_no'] = $result['words_result']['公民身份号码']['words'];
                $obj['address'] = $result['words_result']['住址']['words'];
                return $obj;
            }else{
                return null;
            }

        } catch (\Exception $e) {
            return response()->json([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }

    public function idcardFront (Request $request)
    {
        try {
            $obj = [];
            $this->init();
            $fileName = $request->get('fileName');
            if (empty($fileName)) {
                return response()->json([
                    'status' => 2,
                    'message' => '文件不存在'
                ]);
            }
            $img_base64 = file_get_contents($fileName);
            $result = $this->apiOcr->idcard($img_base64, "front");
            if (!isset($result['error_code'])) {
                $obj['username'] = $result['words_result']['姓名']['words'];
                $obj['id_no'] = $result['words_result']['公民身份号码']['words'];
                return response()->json([
                    'status' => 1,
                    'data' => $obj
                ]);
            }else{
                return response()->json([
                    'status' => 2,
                    'data' => '识别错误'
                ]);
            }

        } catch (\Exception $e) {
            return response()->json([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }


    /**
     * 身份证反面
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function idcardBackUrl ($fileName)
    {
        try {
            $obj = [];
            $this->init();
            if (empty($fileName)) {
                return null;
            }
            $img_base64 = file_get_contents($fileName);
            $result = $this->apiOcr->idcard($img_base64, "back");

            if (!isset($result['error_code'])) {
                $crpStartDt = $result['words_result']['签发日期']['words'];
                if (! empty($crpStartDt)) {
                    $obj['head_sfz_stime'] = $this->xbtDate($crpStartDt);
                }
                $crpEndDt = $result['words_result']['失效日期']['words'];
                if (! empty($crpEndDt) && is_numeric($crpEndDt)) {
                    $obj['head_sfz_time'] = $this->xbtDate($crpEndDt);
                } else {
                    $obj['head_sfz_time'] = '9999-12-31';
                }
                return $obj;
            }else{
                return null;
            }

        } catch (\Exception $e) {
            return response()->json([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }

    public function idcardBack (Request $request)
    {
        try {
            $obj = [];
            $this->init();
            $fileName = $request->get('fileName');
            if (empty($fileName)) {
                return response()->json([
                    'status' => 2,
                    'message' => '文件不存在'
                ]);
            }
            $img_base64 = file_get_contents($fileName);
            $result = $this->apiOcr->idcard($img_base64, "back");
            if (!isset($result['error_code'])) {
                $crpStartDt = $result['words_result']['签发日期']['words'];
                if (! empty($crpStartDt)) {
                    $obj['head_sfz_stime'] = $this->xbtDate($crpStartDt);
                }
                $crpEndDt = $result['words_result']['失效日期']['words'];
                if (! empty($crpEndDt) && is_numeric($crpEndDt)) {
                    $obj['head_sfz_time'] = $this->xbtDate($crpEndDt);
                } else {
                    $obj['head_sfz_time'] = '9999-12-31';
                }
                return response()->json([
                    'status' => 1,
                    'data' => $obj
                ]);
            }else{
                return response()->json([
                    'status' => 2,
                    'data' => '识别错误'
                ]);
            }

        } catch (\Exception $e) {
            return response()->json([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }

    /**
     * 银行卡识别
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bankcardBackUrl ($fileName)
    {
        try {
            $obj = [];
            $this->init();
            if (empty($fileName)) {
                return null;
            }

            $img_base64 = file_get_contents($fileName);
            $result = $this->apiOcr->bankcard($img_base64);

            if (!isset($result['error_code'])) {

                $obj['bank_card_number'] = str_replace(" ", "", $result['result']['bank_card_number']);
                $obj['bank_name'] = $result['result']['bank_name'];
                return $obj;

            }else{
                return null;
            }

        } catch (\Exception $e) {
            return response()->json([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }

    public function bankcardBack (Request $request)
    {
        try {
            $obj = [];
            $this->init();
            $fileName = $request->get('fileName');
            if (empty($fileName)) {
                return response()->json([
                    'status' => 2,
                    'message' => '文件不存在'
                ]);
            }
            $img_base64 = file_get_contents($fileName);
            $result = $this->apiOcr->bankcard($img_base64);
            if (!isset($result['error_code'])) {
                $obj['bank_card_number'] = str_replace(" ", "", $result['result']['bank_card_number']);
                $obj['bank_name'] = $result['result']['bank_name'];
                return response()->json([
                    'status' => 1,
                    'data' => $obj
                ]);

            }else{
                return response()->json([
                    'status' => 2,
                    'data' => '识别错误'
                ]);
            }

        } catch (\Exception $e) {
            return response()->json([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }

    function xbtDate ($date)
    {
        $year = substr($date, 0, 4);
        $month = substr($date, 4, 2);
        $day = substr($date, 6, 8);
        return $year . '-' . $month . '-' . $day;
    }

    /**
     * 银行类别
     * @param Request $request
     */
    public  function  getBankCat(Request $request){
        try {
            $data = DB::table('bank_info_vbills')
                ->select('id','bankCode as code','bankCodeName as name')
                ->groupBy('bankCode')
                ->get();
            return response()->json([
                'status' => 1,
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }

    public function getSubBank(Request $request){
        try {
            $bankCode = $request->get('bankCode','');
            $cityCodeName = $request->get('cityCodeName', '');

            $where = [];
            if (!empty($bankCode)) {
                $where[] = ['bankCode',$bankCode];
            }
            if (!empty($cityCodeName)) {
                $where[] = ['cityCodeName', 'like', '%' . $cityCodeName . '%'];
            }
            $data = DB::table('bank_info_vbills')
                ->select('id','instOutCode as code','bankName as name')
                ->where($where)
                ->get();
            return response()->json([
                'status' => 1,
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }

    /**
     * 易生MCC
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public  function  getMccId(Request $request){
        try {
            $id = $request->get('id','');
            $data = DB::table('vbill_store_category')
                ->select('mcc')
                ->where('id',$id)
                ->value('mcc');
            return response()->json([
                'status' => 1,
                'mcc' => $data
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }

    /**
     * 易生MCC
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public  function  getMccName(Request $request){
        try {
            $id = $request->get('mcc_id','');
            $data = DB::table('eayspay_mcc')
                ->select('mcc_name')
                ->where('mcc_id',$id)
                ->value('mcc_name');
            return response()->json([
                'status' => 1,
                'mcc_name' => $data
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }

    public  function  getAreaId(Request $request){
        try {
            $name = $request->get('name','');
            $where[] = ['name', 'like', '%' . $name . '%'];
            $data = DB::table('easypay_area_no')
                ->select('code')
                ->where($where)
                ->value('code');
            if (empty($data)){
                $name = $request->get('province_name','');
                $where[] = ['yl_pr_name', 'like', '%' . $name . '%'];
                $data = DB::table('easypay_area_no')
                    ->select('code')
                    ->where($where)
                    ->value('code');
            }
            return response()->json([
                'status' => 1,
                'code' => $data
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ]);
        }
    }

}