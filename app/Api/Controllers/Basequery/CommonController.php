<?php

namespace App\Api\Controllers\Basequery;

use App\Api\Controllers\BaseController;
use App\Models\AreaCodes;
use App\Models\BankCodes;
use App\Models\EasePayMcc;
use App\Models\FhcPayMcc;
use App\Models\LklPayMcc;
use App\Models\LklStore;
use App\Models\StoreMccs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class CommonController extends BaseController
{
    //获取商户地区码-通用
    public function getAreaCodeList(Request $request)
    {
        $list = Cache::get('commonAreaCodeList');
        if (!$list) {
            $list = AreaCodes::where('area_parent_id', '1')
                ->with('children')
                ->select('area_code', 'area_name', 'area_parent_id', 'code', 'area_type')
                ->get();
            Cache::forever('commonAreaCodeList', $list);
        }

        $this->status = 1;
        $this->meassage = '数据请求成功';
        return $this->format($list);
    }

    //获取商户Mcc-通用
    public function getStoreMccPList(Request $request)
    {
        $list = StoreMccs::where('pid', 0)
            ->select('mcc_name as label', 'mcc_code as value')
            ->get();

        $this->status = 1;
        $this->meassage = '数据请求成功';
        return $this->format($list);
    }

    //获取商户Mcc-通用
    public function getStoreMccList(Request $request)
    {
        $mcc_name = $request->get('mcc_name', '');
        $pid = $request->get('pid', '');
        $where = [];
        if ($pid) {
            $where[] = ['pid', '=', $pid];
        }
        if ($mcc_name) {
            if (is_numeric($mcc_name)) {
                $where[] = ['mcc_code', '=', $mcc_name];
            } else {
                $where[] = ['mcc_name', 'like', '%' . $mcc_name . '%'];
            }
        }
        $list = StoreMccs::where($where)
            ->where('pid', '!=', 0)
            ->select('pid', 'mcc_name', 'mcc_code')
            ->get();

        $this->status = 1;
        $this->meassage = '数据请求成功';
        return $this->format($list);
    }

    //获取银行大类-通用
    public function getBankTypeList(Request $request)
    {
        $list = BankCodes::select('bank_type', 'bank_type_name')
            ->groupBy('bank_type')
            ->get();
        $this->status = 1;
        $this->meassage = '数据请求成功';
        return $this->format($list);
    }

    //获取银行编码-通用
    public function getBankCodeList(Request $request)
    {
        $cityCode = $request->get('city_code', '');
        $bankType = $request->get('bank_type', '');

        $list = BankCodes::where('city_code', $cityCode)->where('bank_type', $bankType)
            ->select('bank_code', 'bank_name', 'city_code', 'bank_type', 'bank_type_name')
            ->get();
        $this->status = 1;
        $this->meassage = '数据请求成功';
        return $this->format($list);
    }

    //获取商户Mcc-通用
    public function getLklStoreMccPList(Request $request)
    {
        $list = LklPayMcc::where('parent_code', '1')
            ->where('business_scene', '2')
            ->select('name as label', 'code as value')
            ->get();

        $this->status = 1;
        $this->meassage = '数据请求成功';
        return $this->format($list);
    }

    //获取商户Mcc-通用
    public function getLklStoreMccList(Request $request)
    {
        $mcc_name = $request->get('mcc_name', '');
        $pid = $request->get('pid', '');
        $where = [];
        if ($pid) {
            $where[] = ['parent_code', '=', $pid];
        } else {
            $where[] = ['parent_code', '!=', '1'];
        }
        if ($mcc_name) {
            if (is_numeric($mcc_name)) {
                $where[] = ['code', '=', $mcc_name];
            } else {
                $where[] = ['name', 'like', '%' . $mcc_name . '%'];
            }
        }
        $where[] = ['business_scene', '=', '2'];
        $list = LklPayMcc::where($where)
            ->select('parent_code as pid', 'name as mcc_name', 'code as mcc_code')
            ->get();

        $this->status = 1;
        $this->meassage = '数据请求成功';
        return $this->format($list);
    }

    //获取金控商户Mcc-通用
    public function getFhcMccPList(Request $request)
    {
        $list = FhcPayMcc::select('p_name as label', 'p_name as value')
            ->distinct()
            ->orderBy('p_name')
            ->get();

        $this->status = 1;
        $this->meassage = '数据请求成功';
        return $this->format($list);
    }

    //获取金控商户Mcc-通用
    public function getFhcMccList(Request $request)
    {
        $mcc_name = $request->get('mcc_name', '');
        $pid = $request->get('pid', '');
        $where = [];
        if ($pid) {
            $where[] = ['p_name', '=', $pid];
        }
        if ($mcc_name) {
            if (is_numeric($mcc_name)) {
                $where[] = ['mcc_code', '=', $mcc_name];
            } else {
                $where[] = ['mcc_name', 'like', '%' . $mcc_name . '%'];
            }
        }
        $list = FhcPayMcc::where($where)
            ->select('p_name as pid', 'mcc_name', 'mcc_id as mcc_code')
            ->get();

        $this->status = 1;
        $this->meassage = '数据请求成功';
        return $this->format($list);
    }

    //获取首信易商户Mcc-通用
    public function getEaseStoreMccPList(Request $request)
    {
        $list = EasePayMcc::where('p_code', '0')
            ->select('name as label', 'code as value')
            ->get();

        $this->status = 1;
        $this->meassage = '数据请求成功';
        return $this->format($list);
    }

    //获取首信易商户Mcc-通用
    public function getEaseStoreMccList(Request $request)
    {
        $mcc_name = $request->get('mcc_name', '');
        $pid = $request->get('pid', '');
        $where = [];
        if ($pid) {
            $where[] = ['p_code', '=', $pid];
        } else {
            $where[] = ['p_code', '!=', '0'];
        }
        if ($mcc_name) {
            if (is_numeric($mcc_name)) {
                $where[] = ['code', '=', $mcc_name];
            } else {
                $where[] = ['name', 'like', '%' . $mcc_name . '%'];
            }
        }
        $list = EasePayMcc::where($where)
            ->select('p_code as pid', 'name as mcc_name', 'code as mcc_code')
            ->get();

        $this->status = 1;
        $this->meassage = '数据请求成功';
        return $this->format($list);
    }
}
