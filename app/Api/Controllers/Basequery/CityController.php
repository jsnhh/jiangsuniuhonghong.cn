<?php
namespace App\Api\Controllers\Basequery;


use App\Api\Controllers\BaseController;
use App\Models\ProvinceCity;
use App\Models\ProvinceCityArea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CityController extends BaseController
{
    public function city(Request $request)
    {
        $areaCode = $request->get('area_code', 1); //编号
        $type = $request->get('type', 2); //类型,2-随行付省市区

        if ($type == 2) {
            $city = Cache::get('vbill_city_'.$areaCode);
            if (!$city) {
                $city = ProvinceCityArea::where('area_parent_id', $areaCode)
                    ->select('area_code', 'area_name', 'area_parent_id', 'area_type', 'dada_city_code')
                    ->get();
                Cache::put('vbill_city_'.$areaCode, $city, 1000000);
            }
        } else {
            $city = Cache::get('city_' . $areaCode);
            if (!$city) {
                $city = ProvinceCity::where('area_parent_id', $areaCode)
                    ->select('area_code', 'area_name', 'area_parent_id', 'area_type')
                    ->get();
                Cache::put('city_' . $areaCode, $city, 1000000);
            }
        }

        $this->status = 1;
        $this->meassage = '数据请求成功';
        return $this->format($city);
    }


}
