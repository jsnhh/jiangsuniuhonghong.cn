<?php

namespace App\Api\Controllers\Basequery;

use Alipayopen\Sdk\AopClient;
use Alipayopen\Sdk\AopUploadClient;
use Alipayopen\Sdk\Request\AntMerchantExpandIndirectImageUploadRequest;
use Alipayopen\Sdk\Request\AntMerchantExpandIndirectZftCreateRequest;
use Alipayopen\Sdk\Request\AntMerchantExpandIndirectZftModifyRequest;
use App\Api\Controllers\BaseController;
use App\Api\Controllers\Config\AlipayIsvConfigController;
use App\Api\Controllers\Config\EasyPayConfigController;
use App\Api\Controllers\Config\FuiouConfigController;
use App\Api\Controllers\Config\HConfigController;
use App\Api\Controllers\Config\HkrtConfigController;
use App\Api\Controllers\Config\HuiPayConfigController;
use App\Api\Controllers\Config\JdConfigController;
use App\Api\Controllers\Config\LinkageConfigController;
use App\Api\Controllers\Config\MyBankConfigController;
use App\Api\Controllers\Config\NewLandConfigController;
use App\Api\Controllers\Config\TfConfigController;
use App\Api\Controllers\Config\VbillConfigController;
use App\Api\Controllers\Config\WeixinConfigController;
use App\Api\Controllers\DuoLaBao\ManageController;
use App\Api\Controllers\EasyPay\MerAccessController;
use App\Api\Controllers\Hkrt\PayController;
use App\Api\Controllers\Jd\StoreController;
use App\Api\Controllers\User\EasyPayStoreController;
use App\Api\Controllers\YinSheng\YsepayStoreController;
use App\Models\AlipayZftStore;
use App\Models\BankInfo;
use App\Models\BankInfoHuipay;
use App\Models\DlbStore;
use App\Models\DlbStoreUpload;
use App\Models\EasypayConfig;
use App\Models\EasypayStore;
use App\Models\EasypayStoresImages;
use App\Models\HasOpenWays;
use App\Models\HltxStore;
use App\Models\HkrtStore;
use App\Models\HStore;
use App\Models\HuiPayStore;
use App\Models\JdStore;
use App\Models\JdStoreItem;
use App\Models\McscTManageCategory;
use App\Models\Merchant;
use App\Models\MyBankCategory;
use App\Models\MyBankStore;
use App\Models\MyBankStoreTem;
use App\Models\NewLandStore;
use App\Models\NewLandStoreItem;
use App\Models\Store;
use App\Models\StoreBank;
use App\Models\StoreImg;
use App\Models\StoreLinkage;
use App\Models\StorePayWay;
use App\Models\TfConfig;
use App\Models\TfStore;
use App\Models\User;
use App\Models\UserRate;
use App\Models\UserStoreSet;
use App\Models\VbillaStore;
use App\Models\VbillStore;
use App\Models\VbillStoreCategory;
use App\Models\WeixinStoreItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Mybank\Mybank;
use MyBank\Sdk\ParamUtil;
use MyBank\Sdk\RSACert;
use MyBank\Tools;
use TencentYoutuyun\Conf;
use TencentYoutuyun\YouTu;

class StorePayWaysController extends BaseController
{
    //申请通道
    public function openways(Request $request)
    {
        try {
            //$user = $this->parseToken();
            $phone = '';
            $email = '';
            $type = $request->get('ways_type');
            $code = $request->get('code', '888888');
            $Store_id = $request->get('store_id');
            $qd = $request->get('qd', '');

            $SettleModeType = '';
//            $SettleModeType = $request->get('settle_mode_type', ''); //
//            if (!$SettleModeType) {
//                return json_encode(['status' => 2, 'message' => '请选择结算类型/方式']);
//            }

            $qd = isset($qd) ? $qd : "1";

            //  $user = User::where('id', $user->user_id)->first();
            // $hasPermission = $user->hasPermissionTo('申请通道');
            //  if (!$hasPermission) {
            //      return json_encode(['status' => 2, 'message' => '没有权限申请通道']);
            //  }

            $store = Store::where('store_id', $Store_id)
                ->select('merchant_id', 'people_phone')
                ->first();
            if ($store) {
                if ($store->people_phone) {
                    $phone = $store->people_phone;
                } else {
                    $merchant = Merchant::where('id', $store->merchant_id)
                        ->select('phone')
                        ->first();
                    if ($merchant) {
                        $phone = $merchant->phone;
                    }
                }
            }

            //$SettleModeType = $request->get('SettleModeType', '01'); //结算方式

            return $this->base_open_ways($type, $code, $Store_id, $SettleModeType, $phone, $email, '', $qd);
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '2',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine(),
            ]);
        }
    }


    /**
     * 商户入网 公共
     * @param $type 'ways_type'
     * @param $code  快钱通道code
     * @param $store_id '门店id'
     * @param $SettleModeType '结算方式'
     * @param string $phone
     * @param string $email
     * @param array $other '这个参数来报名参加各种活动时,开通第二通道使用的'
     * @param int $qd
     * @return string
     */
    public function base_open_ways($type, $code, $store_id, $SettleModeType, $phone = '', $email = '', $other = [], $qd = 1)
    {
        try {
            $Store = Store::where('store_id', $store_id)->first();
            if (!$Store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店未认证请认证门店'
                ]);
            }

            $MyBankCategory = MyBankCategory::where('category_id', $Store->category_id)
                ->first();
            if (!$MyBankCategory) {
                $MyBankCategory = VbillStoreCategory::where('id', $Store->category_id)
                    ->first();
            }
            if (!$MyBankCategory) {
                return json_encode([
                    'status' => 2,
                    'message' => '请选择正确的门店类目'
                ]);
            }

            $StoreBank = StoreBank::where('store_id', $store_id)->first();
            if (!$StoreBank) {
                return json_encode([
                    'status' => 2,
                    'message' => '没有绑定银行卡,请先绑定银行卡'
                ]);
            }

            $StoreImg = StoreImg::where('store_id', $store_id)->first();
            if (!$StoreImg) {
                return json_encode([
                    'status' => 2,
                    'message' => '没有商户资料'
                ]);
            }

            $user = User::where('id', $Store->user_id)
                ->select('is_delete')
                ->first();
            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '业务员被删除无法添加门店1'
                ]);
            }
            if ($user->is_delete) {
                return json_encode([
                    'status' => 2,
                    'message' => '业务员被删除无法添加门店2'
                ]);
            }

            //检测必要参数
            $store_banks = [
                'store_bank_no' => $StoreBank->store_bank_no,
                'store_bank_name' => $StoreBank->store_bank_name,
                'bank_no' => $StoreBank->bank_no,
            ];
            $check_data = [
                'store_bank_no' => '银行卡号',
                'store_bank_name' => '持卡人名称',
                'bank_no' => '联行号',
            ];
            $check = $this->check_required($store_banks, $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            //公共判断
            $source_type = $Store->source_type;  //1：河南-中原项目
            $store_type = $Store->store_type; //经营性质 1-个体工商户，2-企业，3-小微商户
            $config_id = $Store->config_id;
            $store_name = $Store->store_name;
            $store_short_name = $Store->store_short_name;
            $store_bank_type = $StoreBank->store_bank_type ? $StoreBank->store_bank_type : '01'; //01 对私人 02 对公
            $store_pid = $Store->pid;
            $license_no = $Store->store_license_no; //营业执照号
            $store_email = $Store->store_email ? $Store->store_email : $email;
            $phone = $Store->people_phone ? $Store->people_phone : $phone;

            //费率 默认商户的费率为代理商的费率
            $UserRate = UserRate::where('user_id', $Store->user_id)
                ->where('ways_type', $type) //目前是一样的直接读取支付宝就行
                ->first();
            if ($UserRate) {
                $rate = $UserRate->store_all_rate;
                $rate_a = $UserRate->store_all_rate_a; //银联扫码小于1000 贷记卡费率
                $rate_b = $UserRate->store_all_rate_b;
                $rate_b_top = $UserRate->store_all_rate_b_top;
                $rate_c = $UserRate->store_all_rate_c; //银联扫码大于1000 贷记卡费率
                $rate_d = $UserRate->store_all_rate_d;
                $rate_d_top = $UserRate->store_all_rate_d_top;
                $rate_e = $UserRate->store_all_rate_e;
                $rate_f = $UserRate->store_all_rate_f;
                $rate_f_top = $UserRate->store_all_rate_f_top;

                $status = $UserRate->status;
                if ($status == 2) {
                    return json_encode([
                        'status' => 2,
                        'message' => '通道维护中,暂停该通道进件、请更换其他通道'
                    ]);
                }
            }

            //查找是否有此通道
            $ways1 = StorePayWay::where('store_id', $store_id)
                ->where('ways_type', $type)
                ->first();
            if ($ways1) {
                $rate = $ways1->rate; //如果门店设置走门店扫码费率
                $rate_a = $ways1->rate_a;
                $rate_b = $ways1->rate_b;
                $rate_b_top = $ways1->rate_b_top;
                $rate_c = $ways1->rate_c;
                $rate_d = $ways1->rate_d;
                $rate_d_top = $ways1->rate_d_top;
                $rate_e = $ways1->rate_e;
                $rate_f = $ways1->rate_f;
                $rate_f_top = $ways1->rate_f_top;
                if (in_array($ways1->status, [1])) {
                    return json_encode([
                        'status' => -1,
                        'message' => '通道已经申请,不需要重复申请'
                    ]);
                }
            }

            //没有邮箱 随机一个
            if ($store_email == "") {
                $store_email = '' . $phone . '@139.com';
            }

            //新大陆
            if (7999 < $type && $type < 8999) {
                $config = new NewLandConfigController();
                $new_land_config = $config->new_land_config($config_id);
                if (!$new_land_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '新大陆配置不存在请检查配置'
                    ]);
                }

                $aop = new \App\Common\XingPOS\Aop();
                $aop->key = $new_land_config->nl_key;
                $aop->version = 'V1.0.1';//这个一定要和文档一致
                $aop->org_no = $new_land_config->org_no;
                $is_qd = $new_land_config->is_qd;
                //整理提交资料

                //银行卡类型
                $stl_sign = '1';
                if ($store_bank_type == '02') {
                    $stl_sign = '0';
                }

                $store_license_time = $Store->store_license_time; //营业执照到期时间
                if (($store_license_time == '') || ($store_license_time == '长期')) {
                    $store_license_time = '9999-12-31'; //长期
                }

                $store_head_sfz_time = $Store->head_sfz_time; //法人身份证到期时间
                if (($store_head_sfz_time == '') || ($store_head_sfz_time == '长期')) {
                    $store_head_sfz_time = '9999-12-31'; //长期
                }

                //对私判断结算人和法人是否是一个人
                //结算身份证信息默认都是法人
                $bank_sfz_no = $Store->head_sfz_no;
                $bank_sfz_time = $Store->head_sfz_time;

                //法人名字和结算人名字不一样读取结算人的身份证
                if ($Store->head_name != $StoreBank->store_bank_name) {
                    //读取识别图片信息
                    //法人身份证有效期
                    //识别结算卡身份证正面
                    $is_card_a = 1;
                    if ($is_card_a) {
                        try {
                            // 设置APP 鉴权信息 请在http://open.youtu.qq.com 创建应用
                            $appid = env('YOUTU_appid');
                            $secretId = env('YOUTU_secretId');
                            $secretKey = env('YOUTU_secretKey');
                            $userid = env('YOUTU_userid');
                            Conf::setAppInfo($appid, $secretId, $secretKey, $userid, conf::API_YOUTU_END_POINT);
                            $uploadRet = YouTu::idcardocrurl($StoreImg->bank_sfz_img_a, 0);
                            if (isset($uploadRet['errorcode']) && $uploadRet['errorcode'] == 0) {
                                if (isset($uploadRet['name'])) {
                                    //身份证名字
                                    //$uploadRet['name'];
                                }
                                if (isset($uploadRet['id'])) {
                                    //身份证号码
                                    $StoreBank->bank_sfz_no = $uploadRet['id'];
                                    $StoreBank->save();
                                }

                            }
                        } catch (\Exception $exception) {

                        }
                    }

                    //识别身份证反面
                    $is_card_b = 1;
                    if ($is_card_b) {
                        $data_return['sfz_time'] = '';
                        try {
                            // 设置APP 鉴权信息 请在http://open.youtu.qq.com 创建应用
                            $appid = env('YOUTU_appid');
                            $secretId = env('YOUTU_secretId');
                            $secretKey = env('YOUTU_secretKey');
                            $userid = env('YOUTU_userid');
                            Conf::setAppInfo($appid, $secretId, $secretKey, $userid, conf::API_YOUTU_END_POINT);
                            $uploadRet = YouTu::idcardocrurl($StoreImg->bank_sfz_img_b, 1);

                            if (isset($uploadRet['errorcode']) && $uploadRet['errorcode'] == 0) {
                                if (isset($uploadRet['valid_date'])) {
                                    $valid_date = explode("-", $uploadRet['valid_date']);
                                    if (isset($valid_date[1])) {
                                        $StoreBank->bank_sfz_time = str_replace(".", "-", $valid_date[1]); //2019.01.01换成2019-01-01
                                        $StoreBank->save();
                                    }
                                }
                            }
                        } catch (\Exception $exception) {

                        }
                    }

                    $bank_sfz_no = $StoreBank->bank_sfz_no != "" && $StoreBank->bank_sfz_no != "undefined" ? $StoreBank->bank_sfz_no : $Store->head_sfz_no;
                    $bank_sfz_time = $StoreBank->bank_sfz_time;
                }

                if ($store_type == '3') { //小微商户
                    $incom_type = '1';
                } elseif ($store_type == '2') { //企业
                    $incom_type = '2';
                } elseif ($store_type == '1') { //个体工商户
                    $incom_type = '4';
                } else {
                    $incom_type = '2';
                }

                $stoe_nm = $Store->province_name . $Store->city_name . $Store->store_name;
                if (mb_strlen($stoe_nm, 'UTF8') > 20) {
                    $stoe_nm = $Store->province_name . $Store->store_name;
                }
                $sign_data = [
                    "incom_type" => $incom_type, //进件类型,1小微;2企业;3 快速;4 个体工商户;5 特殊商户
                    "stl_typ" => '1', //1 T+1 2 D+1（对公账户不能选择D+1）
                    'scanStoeCnm' => $Store->store_short_name, //扫码小票商户名称
                    "stl_sign" => $stl_sign, //结算标志 1-对私 0-对 公
                    "stl_oac" => $StoreBank->store_bank_no, //结算账号
                    "bnk_acnm" => $StoreBank->store_bank_name, //户名
                    "wc_lbnk_no" => $StoreBank->bank_no, //开户行，联行行号
                    "stoe_nm" => $stoe_nm, //省市区+门店名
                    "stoe_cnt_nm" => $Store->head_name, //门店联系人
                    'stoeCntCardid' => $Store->head_sfz_no, //联系人身份证号
                    "stoe_cnt_tel" => $phone, //联系人手机号
                    "mcc_cd" => $MyBankCategory->mcc, //mcc
                    "stoe_area_cod" => $Store->area_code, //区域码
                    "stoe_adds" => $Store->store_address, //门店详细地址
                    "mailbox" => $store_email,
                    "alipay_flg" => "Y",
                    "yhkpay_flg" => "Y",
                ];

                $no_sign_date = [
                    "icrp_id_no" => $bank_sfz_no, //结算人身份证号
                    "crp_exp_dt_tmp" => $this->time_action($bank_sfz_time, '2020-02-02'), //结算人身份证有效期
                    "fee_rat_scan" => $rate, //微信扫码费率
                    "fee_rat3_scan" => $rate, //支付宝扫码费率
                    "fee_rat1_scan" => $rate_a, //银联二维码费率-1000以下
                    "fee_rat2_scan" => $rate_c, //银联二维码费率-1000以上
                    "fee_rat" => $rate_f, //借记卡费率
                    "max_fee_amt" => $rate_f_top, //借记卡封顶
                    "fee_rat1" => $rate_e, //贷记卡费率
                    "ysfcreditfee" => $rate_a,
                    "ysfdebitfee" => $rate_a,
                    "tranTyps" => "C1,C2,C3,C4,C5,C6,C7",
                    "suptDbfreeFlg" => "1",
                    "cardTyp" => "00",
                    "trm_rec" => "1", //pos终端数量
                    "trm_scan" => "1", //扫码终端数量
                    "bus_lic_no" => $Store->store_license_no ? $Store->store_license_no : $Store->head_sfz_no, //营业执照号为空传身份证号
                    "bse_lice_nm" => $Store->store_name, //营业执照名
                    "crp_nm" => $Store->head_name, //法人
                    "mercAdds" => $Store->province_name . $Store->city_name . $Store->area_name . $Store->store_address, //商户地址
                    "bus_exp_dt" => $this->time_action($store_license_time, '9999-12-31'), //营业执照到期日
                    'busEffDt' => $this->time_action($Store->store_license_stime, '2020-02-02'), //营业执照起始日期
                    "crp_id_no" => $Store->head_sfz_no, //法人身份证号
                    "crp_exp_dt" => $this->time_action($store_head_sfz_time, '9999-12-31'), //法人身份证到期日
                    'crpEffDt' => $this->time_action($Store->head_sfz_stime, '2020-05-20'), //法人起始日期,企业小微必输
                    'serFeeTyp' => '1', //手续费收取方式  1、内扣 2、外口 默认内扣
                ];
                if ($stl_sign == '1') {
                    $no_sign_date['crpStartDt'] = $this->time_action($Store->head_sfz_stime, '2020-05-20'); //结算人身份证起始日期,结算标志为1-对私必输：输入格式 1999-12-31对公：默认法人起始日期
                }

                $NewLandStore = NewLandStore::where('store_id', $store_id)->first();

                //如果已经进件了-修改
                if ($NewLandStore && $NewLandStore->nl_mercId) {
                    $jj_status = $NewLandStore->jj_status;
                    $img_status = $NewLandStore->img_status;
                    $tj_status = $NewLandStore->tj_status;
                    $check_flag = $NewLandStore->check_flag;
                    $check_qm = $NewLandStore->check_qm;

                    //如果审核通过了 要修改 需要调审核修改申请
                    if ($check_flag == '1') {
                        $nl_mercId = $NewLandStore->nl_mercId;
                        $sign_data_ShenQing = [
                            'mercId' => $nl_mercId,
                        ];
                        $request_obj = new  \App\Common\XingPOS\Request\XingStoreShangHuXiuGaiShenQing();
                        $aop->version = 'V1.0.1'; //这个一定要和文档一致
                        $request_obj->setBizContent($sign_data_ShenQing, []);
                        $return = $aop->executeStore($request_obj);
                        //不成功
                        if ($return['msg_cd'] != '000000') {
                            return json_encode([
                                'status' => 2,
                                'message' => $return['msg_dat'],
                            ]);
                        }
                    }

                    //全部改为0
                    $jj_status = 0;
                    $img_status = 0;
                    $tj_status = 0;
                    $check_flag = 0;
                    $check_qm = 0;

                    $NewLandStore->jj_status = $jj_status;
                    $NewLandStore->img_status = $img_status;
                    $NewLandStore->tj_status = $tj_status;
                    $NewLandStore->check_flag = $check_flag; //1-通过 2-驳回 3.转人工
                    $NewLandStore->check_qm = $check_qm;
                    $NewLandStore->save();
                }

                //1.进件请求
                //已经成功进件过了
                if ($NewLandStore && $NewLandStore->jj_status == 1) {
                    $mercId = $NewLandStore->nl_mercId;
                    $stoe_id = $NewLandStore->nl_stoe_id;
                    $log_no = $NewLandStore->log_no;
                } else {
                    $request_obj = new  \App\Common\XingPOS\Request\XingStoreShangHuJinJian();
                    $aop->version = 'V1.0.5'; //这个一定要和文档一致
                    $request_obj->setBizContent($sign_data, $no_sign_date);
                    $return = $aop->executeStore($request_obj);

                    //不成功
                    if ($return['msg_cd'] != '000000') {
                        return json_encode([
                            'status' => 2,
                            'message' => $return['msg_dat'],
                        ]);
                    }
                    $jj_status = 1;

                    //成功以后
                    $mercId = $return['mercId'];
                    $stoe_id = $return['stoe_id'];
                    $log_no = $return['log_no'];
                    $data_insert = [
                        'store_id' => $store_id,
                        'store_name' => $store_name,
                        'config_id' => $config_id,
                        'jj_status' => $jj_status,
                        'nl_mercId' => $mercId,
                        'nl_stoe_id' => $stoe_id,
                        'log_no' => $log_no,
                        'is_qd' => $is_qd
                    ];
                    $NewLandStore = NewLandStore::where('store_id', $store_id)->first();
                    if ($NewLandStore) {
                        $NewLandStore->update($data_insert);
                        $NewLandStore->save();
                    } else {
                        NewLandStore::create($data_insert);
                    }
                }

                //2.上传图片
                $NewLandStore = NewLandStore::where('store_id', $store_id)->first();

                //未提交商户图片
                if ($NewLandStore && $NewLandStore->img_status != 1) {
                    $data = [
                        "mercId" => $mercId,
                        'log_no' => $log_no,
                        'stoe_id' => $stoe_id,
                    ];

                    $store_type = 2;
                    $img_data = [];
                    //对公类型的商户
                    if ((int)$store_bank_type == '02') {
                        $img_data = [
                            [
                                'imgTyp' => '1', //营业执照
                                'imgNm' => 'yyzz.jpg',
                                'imgFile' => $this->new_land_img($StoreImg->store_license_img ? $StoreImg->store_license_img : $StoreImg->head_sfz_img_a, '营业执照')
                            ],
                            [
                                'imgTyp' => '14', //商户协议
                                'imgNm' => 'xyzp.jpg',
                                'imgFile' => $this->new_land_img($StoreImg->store_license_img ? $StoreImg->store_license_img : $StoreImg->head_sfz_img_a, '商户协议')
                            ],
                            [
                                'imgTyp' => '12', //开户许可证
                                'imgNm' => 'khxk.jpg',
                                'imgFile' => $this->new_land_img($StoreImg->store_industrylicense_img, '开户许可证')
                            ],
                            [
                                'imgTyp' => '4', //法人正面
                                'imgNm' => 'frz.jpg',
                                'imgFile' => $this->new_land_img($StoreImg->head_sfz_img_a, '法人正面')
                            ],
                            [
                                'imgTyp' => '5', //法人反面
                                'imgNm' => 'frf.jpg',
                                'imgFile' => $this->new_land_img($StoreImg->head_sfz_img_b, '法人反面')
                            ],
                            [
                                'imgTyp' => '9', //结算法人正面
                                'imgNm' => 'jsfra.jpg',
                                'imgFile' => $this->new_land_img($StoreImg->head_sfz_img_a, '结算法人正面')
                            ],
                            [
                                'imgTyp' => '10', //结算法人反面
                                'imgNm' => 'jsfrb.jpg',
                                'imgFile' => $this->new_land_img($StoreImg->head_sfz_img_b, '结算法人反面')
                            ],
                            [
                                'imgTyp' => '6', //门头照片
                                'imgNm' => 'mtz.jpg',
                                'imgFile' => $this->new_land_img($StoreImg->store_logo_img, '门头照片')
                            ],
                            [
                                'imgTyp' => '7', //场景照
                                'imgNm' => 'cjz.jpg',
                                'imgFile' => $this->new_land_img($StoreImg->store_img_b, '场景照')
                            ],
                            [
                                'imgTyp' => '8', //收银台
                                'imgNm' => 'syt.jpg',
                                'imgFile' => $this->new_land_img($StoreImg->store_img_a, '收银台')
                            ]
                        ];
                    } else {
                        //对私结算
                        //1.同法人
                        //2.非法人
                        //3.//个人
                        //$store_type 经营性质 1-个体，2-企业，3-个人

                        // 3-个人
                        if ($store_type == 3) {
                            $img_data = [
                                [
                                    'imgTyp' => '14', //商户协议
                                    'imgNm' => 'xyzp.jpg',
                                    'imgFile' => $this->new_land_img($StoreImg->store_license_img ? $StoreImg->store_license_img : $StoreImg->head_sfz_img_a, '商户协议')
                                ],
                                [
                                    'imgTyp' => '4', //法人正面
                                    'imgNm' => 'frz.jpg',
                                    'imgFile' => $this->new_land_img($StoreImg->head_sfz_img_a, '法人正面')
                                ],
                                [
                                    'imgTyp' => '5', //法人反面
                                    'imgNm' => 'frf.jpg',
                                    'imgFile' => $this->new_land_img($StoreImg->head_sfz_img_b, '法人反面')
                                ],
                                [
                                    'imgTyp' => '13', //结算人法人手持身份证
                                    'imgNm' => 'jsrscsfz.jpg',
                                    'imgFile' => $this->new_land_img($StoreImg->bank_sc_img ? $StoreImg->bank_sc_img : $StoreImg->head_sc_img, '结算人法人手持身份证')
                                ],
                                [
                                    'imgTyp' => '6', //结算人站在门口
                                    'imgNm' => 'jsrzzmk.jpg',
                                    'imgFile' => $this->new_land_img($StoreImg->head_store_img, '结算人站在门口')
                                ],
                                [
                                    'imgTyp' => '7', //场景照
                                    'imgNm' => 'cjz.jpg',
                                    'imgFile' => $this->new_land_img($StoreImg->store_img_b, '场景照')
                                ],
                                [
                                    'imgTyp' => '8', //收银台
                                    'imgNm' => 'syt.jpg',
                                    'imgFile' => $this->new_land_img($StoreImg->store_img_a, '收银台')
                                ],
                                [
                                    'imgTyp' => '9', //结算法人正面
                                    'imgNm' => 'jsfra.jpg',
                                    'imgFile' => $this->new_land_img($StoreImg->head_sfz_img_a, '结算法人正面')
                                ],
                                [
                                    'imgTyp' => '10', //结算法人反面
                                    'imgNm' => 'jsfrb.jpg',
                                    'imgFile' => $this->new_land_img($StoreImg->head_sfz_img_b, '结算法人反面')
                                ],
                                [
                                    'imgTyp' => '11', //银行卡正面
                                    'imgNm' => 'yhkzm.jpg',
                                    'imgFile' => $this->new_land_img($StoreImg->bank_img_a, '银行卡正面')
                                ]
                            ];
                        } else {
                            //同法人
                            if ($Store->head_name == $StoreBank->store_bank_name) {
                                $img_data = [
                                    [
                                        'imgTyp' => '1', //营业执照
                                        'imgNm' => 'yyzz.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->store_license_img ? $StoreImg->store_license_img : $StoreImg->head_sfz_img_a, '营业执照')
                                    ],
                                    [
                                        'imgTyp' => '14', //商户协议
                                        'imgNm' => 'xyzp.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->store_license_img ? $StoreImg->store_license_img : $StoreImg->head_sfz_img_a, '商户协议')
                                    ],
                                    [
                                        'imgTyp' => '4', //法人正面
                                        'imgNm' => 'frz.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->head_sfz_img_a, '法人正面')
                                    ], [
                                        'imgTyp' => '5', //法人反面
                                        'imgNm' => 'frf.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->head_sfz_img_b, '法人反面')
                                    ], [
                                        'imgTyp' => '6', //门头照片
                                        'imgNm' => 'mtz.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->store_logo_img, '门头照片')
                                    ],
                                    [
                                        'imgTyp' => '7', //场景照
                                        'imgNm' => 'cjz.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->store_img_b, '场景照')
                                    ],
                                    [
                                        'imgTyp' => '8', //收银台
                                        'imgNm' => 'syt.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->store_img_a, '收银台')
                                    ], [
                                        'imgTyp' => '9', //结算法人正面
                                        'imgNm' => 'jsfra.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->head_sfz_img_a, '结算法人正面')
                                    ], [
                                        'imgTyp' => '10', //结算法人反面
                                        'imgNm' => 'jsfrb.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->head_sfz_img_b, '结算法人反面')
                                    ],
                                    [
                                        'imgTyp' => '11', //银行卡正面
                                        'imgNm' => 'yhkzm.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->bank_img_a, '银行卡正面')
                                    ],
                                ];
                            } else {
                                //非法人
                                $img_data = [
                                    [
                                        'imgTyp' => '14', //商户协议
                                        'imgNm' => 'xyzp.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->store_license_img ? $StoreImg->store_license_img : $StoreImg->head_sfz_img_a, '商户协议')
                                    ],
                                    [
                                        'imgTyp' => '1', //营业执照
                                        'imgNm' => 'yyzz.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->store_license_img ? $StoreImg->store_license_img : $StoreImg->head_sfz_img_a, '营业执照')
                                    ],
                                    [
                                        'imgTyp' => '4', //法人正面
                                        'imgNm' => 'frz.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->head_sfz_img_a, '法人正面')
                                    ], [
                                        'imgTyp' => '5', //法人反面
                                        'imgNm' => 'frf.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->head_sfz_img_b, '法人反面')
                                    ], [
                                        'imgTyp' => '6', //门头照片
                                        'imgNm' => 'mtz.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->store_logo_img, '门头照片')
                                    ],
                                    [
                                        'imgTyp' => '7', //场景照
                                        'imgNm' => 'cjz.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->store_img_b, '场景照')
                                    ],
                                    [
                                        'imgTyp' => '8', //收银台
                                        'imgNm' => 'syt.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->store_img_a, '收银台')
                                    ], [
                                        'imgTyp' => '9', //结算法人正面
                                        'imgNm' => 'jsfra.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->bank_sfz_img_a ? $StoreImg->bank_sfz_img_a : $StoreImg->head_sfz_img_a, '结算法人正面')
                                    ], [
                                        'imgTyp' => '10', //结算法人反面
                                        'imgNm' => 'jsfrb.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->bank_sfz_img_b ? $StoreImg->bank_sfz_img_b : $StoreImg->head_sfz_img_b, '结算法人反面')
                                    ],
                                    [
                                        'imgTyp' => '11', //银行卡正面
                                        'imgNm' => 'yhkzm.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->bank_img_a, '银行卡正面')
                                    ],
                                    [
                                        'imgTyp' => '16', //授权结算书
                                        'imgNm' => 'sqjss.jpg',
                                        'imgFile' => $this->new_land_img($StoreImg->store_auth_bank_img ? $StoreImg->store_auth_bank_img : $StoreImg->head_sfz_img_a, '授权结算书')
                                    ],
                                ];
                            }
                        }
                    }

                    foreach ($img_data as $k => $v) {
                        $data['imgTyp'] = $v['imgTyp'];
                        $data['imgNm'] = $v['imgNm'];
                        $data_no_sign['imgFile'] = $v['imgFile'];

                        $request_obj = new  \App\Common\XingPOS\Request\XingStoreTuPianShangChuan();
                        $aop->version = 'V1.0.1'; //这个一定要和文档一致
                        $request_obj->setBizContent($data, $data_no_sign);
                        $return = $aop->executeStore($request_obj);

                        //不成功
                        if ($return['msg_cd'] != '000000') {
                            return $return_err = json_encode([
                                'status' => 2,
                                'message' => $return['msg_dat'] . '-' . $v['imgTyp'],
                            ]);
                        }
                    }

                    //图片确认成功
                    $img_status = 1;
                    $NewLandStore->img_status = $img_status;
                    $NewLandStore->save();
                }

                //3.商户提交
                $status = 2;
                $status_desc = '审核中';

                // if ($NewLandStore && $NewLandStore->tj_status != 1) {
                $sign_data = [
                    'mercId' => $mercId,
                    'log_no' => $log_no,
                ];
                $request_obj = new  \App\Common\XingPOS\Request\XingStoreShangHuTiJiao();
                $aop->version = 'V1.0.1'; //这个一定要和文档一致
                $request_obj->setBizContent($sign_data, []);
                $return = $aop->executeStore($request_obj);

                //不成功
                if ($return['msg_cd'] != '000000' && $return['msg_cd'] != 'SCM60003') {
                    return $return_err = json_encode([
                        'status' => 2,
                        'message' => $return['msg_dat'],
                    ]);
                }
                //商户确认提交成功
                $tj_status = 1;
                $check_flag = $return['check_flag'];

                $NewLandStore->tj_status = $tj_status;
                $NewLandStore->check_flag = $check_flag;

                //直接通过
                if ($return['check_flag'] == 1) {
                    $XA = substr($return['REC'][0]['trmNo'], 0, 2);
                    if ($XA == "XA") {
                        $trmNo = $return['REC'][0]['trmNo'];
                    } else {
                        $trmNo = $return['REC'][1]['trmNo'];
                    }
                    $status = 1;
                    $status_desc = '开通成功';
                    $NewLandStore->nl_key = $return['key'];
                    $NewLandStore->trmNo = $trmNo;
                }

                //驳回
                if ($return['check_flag'] == 2) {
                    $status = 3;
                    $status_desc = '开通失败';
                }

                //转人工审核
                if ($return['check_flag'] == 2) {
                    $status = 2;
                    $status_desc = '审核中';
                }

                $NewLandStore->save();

                // }

                //4. 安心签名
                if ($NewLandStore && $NewLandStore->check_qm != 1) {
                    $sign_data = [
                        'mercId' => $mercId,
                    ];

                    $request_obj = new  \App\Common\XingPOS\Request\XingStoreShangHuFaSongQianShuAnXinQianDiZhi();
                    $aop->version = 'V1.0.1'; //这个一定要和文档一致
                    $request_obj->setBizContent($sign_data);
                    $return = $aop->executeStore($request_obj);

                    //不成功
                    if ($return['msg_cd'] != '000000') {
                        return $return_err = json_encode([
                            'status' => 2,
                            'message' => $return['msg_dat'],
                        ]);
                    }
                    $check_qm = 1;
                    $NewLandStore->check_qm = $check_qm;
                    $NewLandStore->save();
                }

                //人工审核
                if ($status == 2) {
                    //顺带入临时库等待查询状态
                    $NewLandStoreItem = NewLandStoreItem::where('store_id', $store_id)->first();
                    $item_insert = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'jj_status' => $jj_status,
                        'img_status' => $img_status,
                        'tj_status' => $tj_status,
                        'nl_mercId' => $mercId,
                        'nl_stoe_id' => $stoe_id,
                        'log_no' => $log_no,
                        'check_flag' => $check_flag,
                        'check_qm' => $check_qm,
                        'nl_key' => '',
                        'trmNo' => '',
                    ];
                    if ($NewLandStoreItem) {
                        $NewLandStoreItem->update($item_insert);
                        $NewLandStoreItem->save();
                    } else {
                        NewLandStoreItem::create($item_insert);
                    }
                }

                $send_ways_data['config_id'] = $config_id;
                $send_ways_data['store_id'] = $store_id;
                $send_ways_data['rate'] = $rate;
                $send_ways_data['status'] = $status;
                $send_ways_data['status_desc'] = $status_desc;
                $send_ways_data['company'] = 'newland';
                $return = $this->send_ways_data($send_ways_data);
                return $return;
            }

            //京东聚合
            if (5999 < $type && $type < 6999) {
                $config = new JdConfigController();
                $jd_config = $config->jd_config($config_id);
                if (!$jd_config) {
                    return json_encode(['status' => 2, 'message' => '没有配置相关通道']);
                }

                //京东聚合必须是5个字
                if (mb_strlen($Store->store_name, 'UTF8') < 5) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店名称必须大于5个字',
                    ]);
                }

                $jd_store = JdStoreItem::where('store_id', $store_id)->first();
                $buildOrRepair = '0';
                if ($jd_store && $jd_store->store_true) {
                    $buildOrRepair = '1';
                }
                $data['store_id'] = $store_id;
                $data['phone'] = $phone;
                $data['email'] = $store_email;
                $data['buildOrRepair'] = $buildOrRepair; //入驻标识0：新入驻，1：修改
                $data['store_md_key'] = $jd_config->store_md_key; //'8data998mnwepxugnk03-2zirb'; //入驻标识0：新入驻，1：修改
                $data['store_des_key'] = $jd_config->store_des_key; // 'XdD4NyO2LG03DThegNm/bhmP6jR6zvg3'; //入驻标识0：新入驻，1：修改
                $data['agentNo'] = $jd_config->agentNo; //110770481
                $data['serialNo'] = time() . rand(1000, 9000);
                $data['request_url'] = 'https://psi.jd.com/merchant/enterSingle'; //入驻标识0：新入驻，1：修改
                $OBJ = new StoreController();
                $open_store = $OBJ->open_store($data);

                //进件成功
                if ($open_store['code'] === "0000") {
                    //已经入驻过了
                    if ($jd_store && $jd_store->store_true) {
                        //更新门店
                        $jd_store->serialNo = $open_store['serialNo'];
                        $jd_store->merchant_no = $open_store['merchantNo'];
                        $jd_store->save();

                        if ($jd_store->pay_true == 0) {

                            $data1 = [
                                'request_url' => 'https://psi.jd.com/merchant/applySingle',
                                'agentNo' => $jd_config->agentNo,
                                'serialNo' => "" . time() . $phone . "1",
                                'merchantNo' => $open_store['merchantNo'],
                                'store_md_key' => $jd_config->store_md_key,
                                'store_des_key' => $jd_config->store_des_key,
                                'productId' => '35',
                                'payToolId' => '1060',
                                'mfeeType' => '2',
                                'mfee' => $rate,
                            ];

                            //提交申请产品-白条
                            $data1['productId'] = '405';
                            $re = $OBJ->store_open_ways($data1);

                            //提交申请产品-聚合支付
                            $data1['productId'] = '35';
                            $re = $OBJ->store_open_ways($data1);

                            if ($re['code'] == "0000") {
                                JdStoreItem::where('store_id', $store_id)
                                    ->update(
                                        [
                                            'pay_true' => 1
                                        ]);
                            } else {
                                $data['rate'] = $rate;
                                $data['status'] = 3;
                                $data['status_desc'] = $re['message'];
                                $data['company'] = 'jdjr';
                                $this->send_ways_data($data);

                                return json_encode([
                                    'status' => 2,
                                    'message' => $re['message'],
                                ]);
                            }
                        }

                        //默认支付通道未注册成功
                        $data['rate'] = $rate;
                        $data['status'] = 2;
                        $data['status_desc'] = '审核中';
                        $data['company'] = 'jdjr';
                        $return = $this->send_ways_data($data);
                        return json_encode($return);
                    } else {
                        //插入数据库
                        $insert_data = [
                            'pid' => $Store->pid,
                            'store_id' => $Store->store_id,
                            'config_id' => $Store->config_id,
                            'merchant_no' => $open_store['merchantNo'],
                            'store_true' => 1,
                            'pay_true' => 0,
                            'serialNo' => $open_store['serialNo']
                        ];
                        $insert = JdStoreItem::create($insert_data);
                        //提交申请产品
                        $data1 = [
                            'request_url' => 'https://psi.jd.com/merchant/applySingle',
                            'agentNo' => $jd_config->agentNo,
                            'serialNo' => "" . time() . $phone . "2",
                            'merchantNo' => $open_store['merchantNo'],
                            'store_md_key' => $jd_config->store_md_key,
                            'store_des_key' => $jd_config->store_des_key,
                            'productId' => '35',
                            'payToolId' => '1060',
                            'mfeeType' => '2',
                            'mfee' => $rate,
                        ];
                        $re = $OBJ->store_open_ways($data1);
                        if ($re['code'] == "0000") {
                            JdStoreItem::where('store_id', $store_id)
                                ->update(
                                    [
                                        'pay_true' => 1
                                    ]);

                            $data['rate'] = $rate;
                            $data['status'] = 2;
                            $data['status_desc'] = '审核中';
                            $data['company'] = 'jdjr';
                            $return = $this->send_ways_data($data);
                            return json_encode($return);
                        } else {
                            return json_encode([
                                'status' => 2,
                                'message' => $re['message'],
                            ]);
                        }
                    }
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $open_store['message'],
                    ]);
                }
            }

            //快钱支付
            if (2999 < $type && $type < 3999) {
                try {
                    if (!$store_id || !$code) {
                        return json_encode(['status' => 2, 'message' => '门第ID验证码参数不正确']);
                    }
                    $category_id = $Store->category_id;
                    if ($Store->category_id == "") {
                        if ($Store->category_name) {
                            $ca = MyBankCategory::where('category_name', $Store->category_name)
                                ->select('category_id')
                                ->first();
                            if ($ca) {
                                $category_id = $ca->category_id;
                            } else {
                                return json_encode(['status' => 2, 'message' => '门店分类不正确请重新选择分类']);
                            }

                        } else {
                            return json_encode(['status' => 2, 'message' => '门店分类不正确请重新选择分类']);
                        }
                    }

                    //企业账户不支持结算到余利宝
                    if ($Store->store_type == 2 && $SettleModeType == "02") {
                        return json_encode(['status' => 2, 'message' => '企业账户不支持结算到余利宝']);
                    }

                    //判断是公司还是个人
                    if (!$StoreBank) {
                        return json_encode(['status' => 2, 'message' => '没有绑定银行卡,请先绑定银行卡']);
                    }

                    $bankcard_type = $StoreBank->store_bank_type; //01 对私人 02 对公
                    $aop = new \App\Api\Controllers\MyBank\BaseController();
                    $ao = $aop->aop($config_id);
                    $ao->url = $aop->MY_BANK_request2;

                    $store_ta_rate = $rate + 0.02; //快钱支付不支持t0 默认暂时设置
                    //手续费列表
                    $FeeParamList = [
                        [
                            "channelType" => "01",//支付宝
                            "feeType" => "01",//t0
                            "feeValue" => number_format($store_ta_rate / 100, 4, '.', '')
                        ],
                        [
                            "channelType" => "01",//微信支付
                            "feeType" => "02",//t1
                            "feeValue" => number_format($rate / 100, 4, '.', '')
                        ],
                        [
                            "channelType" => "02",
                            "feeType" => "01",
                            "feeValue" => number_format($store_ta_rate / 100, 4, '.', '')
                        ],
                        [
                            "channelType" => "02",
                            "feeType" => "02",
                            "feeValue" => number_format($rate / 100, 4, '.', '')
                        ]
                    ];

                    //活动
                    if (!empty($other)) {
                        //蓝海行 将支付宝费率设置为0
                        if ($other['activity_type'] == 'RBLUE') {
                            //手续费列表
                            $FeeParamList = [
                                [
                                    "channelType" => "01",//支付宝
                                    "feeType" => "01",//t0
                                    "feeValue" => 0
                                ],
                                [
                                    "channelType" => "01",//支付宝
                                    "feeType" => "02",//t1
                                    "feeValue" => 0
                                ],
                                [
                                    "channelType" => "02",
                                    "feeType" => "01",
                                    "feeValue" => $store_ta_rate / 100
                                ],
                                [
                                    "channelType" => "02",
                                    "feeType" => "02",
                                    "feeValue" => $rate / 100
                                ]
                            ];
                        }


                        $store_id = $store_id . '456';
                    }

                    //结算到他行卡
                    if ($SettleModeType == '01') {
                        if ($Store->store_type == 1) {
                            $ao->Function = "ant.mybank.merchantprod.merchant.register";
                            $MerchantType = '02';
                            $SupportPrepayment = 'N';
                            $SettleMode = '01';
                            $principalPerson = $Store->store_name;
                            $PrincipalMobile = $Store->people_phone;
                        }
                        //企业
                        if ($Store->store_type == 2) {
                            $ao->Function = "ant.mybank.merchantprod.merchant.register";
                            $MerchantType = '03'; //企业
                            $SupportPrepayment = 'N';
                            $SettleMode = '01';
                            $principalPerson = $Store->store_name;
                            $PrincipalMobile = $Store->people_phone;
                            $CertOrgCode = $Store->store_license_no; //组织机构代码
                        }

                        //个人
                        if ($Store->store_type == 3) {
                            $ao->Function = "ant.mybank.merchantprod.merchant.register";
                            $MerchantType = '01';
                            $SupportPrepayment = 'N';
                            $SettleMode = '01';
                            $principalPerson = $Store->store_name;
                            $PrincipalMobile = $Store->people_phone;
                        }
                    } //结算到余利宝
                    else {
                        //个体 开通余利宝
                        if ($Store->store_type == 1) {
                            $ao->Function = "ant.mybank.merchantprod.merchant.registerWithAccount";
                            $MerchantType = '02';
                            $SupportPrepayment = 'N';
                            $SettleMode = '02';
                            $principalPerson = $Store->store_name;
                            $PrincipalMobile = $Store->people_phone;
                        }
                        //企业
                        if ($Store->store_type == 2) {
                            $ao->Function = "ant.mybank.merchantprod.merchant.register";
                            $MerchantType = '03'; //企业
                            $SupportPrepayment = 'N';
                            $SettleMode = '01';
                            $principalPerson = $Store->store_name;
                            $PrincipalMobile = $Store->people_phone;
                            $CertOrgCode = $Store->store_license_no; //组织机构代码
                        }

                        //个人
                        if ($Store->store_type == 3) {
                            $ao->Function = "ant.mybank.merchantprod.merchant.registerWithAccount";
                            $MerchantType = '01';
                            $SupportPrepayment = 'N';
                            $SettleMode = '02';
                            $principalPerson = $Store->store_name;
                            $PrincipalMobile = $Store->people_phone;
                        }
                    }
                    //默认失败
                    $send_ways_data['rate'] = $rate;
                    $send_ways_data['store_id'] = $store_id;
                    $send_ways_data['status'] = 3;
                    $send_ways_data['company'] = 'mybank';

                    //商户资料
                    $CertPhotoA = $this->images_get($StoreImg->head_sfz_img_a);
                    $CertPhotoB = $this->images_get($StoreImg->head_sfz_img_b);
                    $ShopPhoto = $this->images_get($StoreImg->store_logo_img);
                    $LicensePhoto = $this->images_get($StoreImg->store_license_img);
                    $CheckstandPhoto = $this->images_get($StoreImg->store_img_a); //收银台
                    $ShopEntrancePhoto = $this->images_get($StoreImg->store_img_b); //门店内景

                    $CertPhotoA = $this->uploadIMG('01', $CertPhotoA, $config_id, '法人身份证正面');

                    if ($CertPhotoA['status'] == 0) {
                        $send_ways_data['status_desc'] = $CertPhotoA['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }
                    sleep(2);
                    $CertPhotoB = $this->uploadIMG('02', $CertPhotoB, $config_id, '法人身份证反面');
                    if ($CertPhotoB['status'] == 0) {
                        $send_ways_data['status_desc'] = $CertPhotoB['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }
                    sleep(2);
                    $ShopPhoto = $this->uploadIMG('06', $ShopPhoto, $config_id, '门头照片');
                    if ($ShopPhoto['status'] == 0) {
                        $send_ways_data['status_desc'] = $ShopPhoto['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }
                    sleep(2);

                    $CheckstandPhoto = $this->uploadIMG('08', $CheckstandPhoto, $config_id, '收银台');
                    if ($CheckstandPhoto['status'] == 0) {
                        $send_ways_data['status_desc'] = $CheckstandPhoto['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }
                    sleep(2);
                    $ShopEntrancePhoto = $this->uploadIMG('09', $ShopEntrancePhoto, $config_id, '内景照片');
                    if ($ShopEntrancePhoto['status'] == 0) {
                        $send_ways_data['status_desc'] = $ShopEntrancePhoto['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }
                    sleep(2);
                    //个人没有营业执照不需要上传
                    if ($Store->store_type != 3) {
                        $LicensePhoto = $this->uploadIMG('03', $LicensePhoto, $config_id, '营业执照');
                        if ($LicensePhoto['status'] == 0) {
                            $send_ways_data['status_desc'] = $LicensePhoto['message'];
                            $return = $this->send_ways_data($send_ways_data);
                            return json_encode($return);
                        }
                        sleep(2);
                    } else {
                        $LicensePhoto = [];
                        $LicensePhoto['url'] = "";
                    }

                    $MerchantDetail = [
                        "alias" => $Store->store_short_name,
                        "contactMobile" => $Store->people_phone,
                        "contactName" => $Store->people,
                        "province" => $Store->province_code,
                        "city" => $Store->city_code,
                        "district" => $Store->area_code,
                        "address" => $Store->store_address,
                        "servicePhoneNo" => $Store->people_phone,
                        // "email" => $Store->people_phone . '@139.com',
                        "principalPerson" => $principalPerson,
                        "principalCertType" => "01",
                        "PrincipalMobile" => $PrincipalMobile,
                        "principalCertNo" => $Store->head_sfz_no,
                        'PrincipalPerson' => $Store->head_name,
                        'CertPhotoA' => $CertPhotoA['url'],//法人身份证
                        'CertPhotoB' => $CertPhotoB['url'],
                        'LicensePhoto' => $LicensePhoto['url'],
                        'ShopPhoto' => $ShopPhoto['url'],//门头
                        'CheckstandPhoto' => $CheckstandPhoto['url'],//门头
                        'ShopEntrancePhoto' => $ShopEntrancePhoto['url'],//门头
                    ];
                    //企业
                    if ($Store->store_type == 2) {
                        $MerchantDetail['LegalPerson'] = $Store->store_name;
                        $MerchantDetail['CertOrgCode'] = $CertOrgCode;

                        //开户许可证
                        $IndustryLicensePhoto = $this->images_get($StoreImg->store_industrylicense_img);
                        $IndustryLicensePhoto = $this->uploadIMG('05', $IndustryLicensePhoto, $config_id, '开户许可证');
                        if ($IndustryLicensePhoto['status'] == 0) {
                            $send_ways_data['status_desc'] = $IndustryLicensePhoto['message'];
                            $return = $this->send_ways_data($send_ways_data);
                            return json_encode($return);
                        }
                        $MerchantDetail['IndustryLicensePhoto'] = $IndustryLicensePhoto['url']; //开户许可证
                    }


                    //结算到余利宝
                    if ($SettleModeType == '02') {
                        //个体
                        if ($Store->store_type == 1) {
                            $MerchantDetail['OtherBankCardNo'] = $StoreBank->bankcard_no;
                        }

                        //个人
                        if ($Store->store_type == 3) {
                            $MerchantDetail['OtherBankCardNo'] = $StoreBank->bankcard_no;
                        }
                    }

                    //个人
                    if ($Store->store_type != 3) {
                        $MerchantDetail['BussAuthNum'] = $Store->store_license_no; //营业执照工商注册号。若商户类型为“自然人”无需上送。
                    }

                    /****关注服务商公众号的类型开始****/
                    $PartnerType = '03'; //默认是关注有梦想的公众号服务商的关注
                    //快钱配置
                    $mbconfig = new MyBankConfigController();

                    $MyBankConfig = $mbconfig->MyBankConfig($config_id);

                    if ($MyBankConfig) {
                        $Path = $MyBankConfig->Path;
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '快钱支付配置信息不存在,请联系服务商',
                        ]);
                    }
//                    //服务商存在
//                    if ($config_id != '123' && $MyBankConfig && $MyBankConfig->SubscribeMerchId && $MyBankConfig->SubscribeAppId && $Store->wx_AppId) {
//                        $PartnerType = '02'; //关注服务商的公众号
//                        $MerchantDetail['AppId'] = $MyBankConfig->wx_AppId; //服务商的公众号id
//                        $MerchantDetail['SubscribeMerchId'] = $MyBankConfig->SubscribeMerchId; //服务商在快钱支付的商户号
//                        $MerchantDetail['SubscribeAppId'] = $MyBankConfig->SubscribeAppId; //需要关注的服务商的公众号id
//                        $MerchantDetail['Path'] = $MyBankConfig->Path;
//                        $Path = $MerchantDetail['Path'];
//                    }
                    //商户配置就关注商户的
                    if ($Store->wx_SubscribeAppId && $Store->wx_AppId) {
                        $PartnerType = '01'; //关注服务商的公众号
                        //  $MerchantDetail['AppId'] = $Store->wx_AppId; //服务商的公众号id
                        $MerchantDetail['SubscribeAppId'] = $Store->wx_SubscribeAppId; //需要关注的服务商的公众号id
                        //  $MerchantDetail['Path'] = $Path;
                    }
                    /****关注服务商公众号的类型结束****/

                    $data = [
                        'MerchantName' => $Store->store_name,
                        'OutMerchantId' => $store_id,
                        'MerchantType' => $MerchantType,
                        'DealType' => '01',//实体
                        'SupportPrepayment' => $SupportPrepayment,//商户清算资金是否支持T+0到账
                        'SettleMode' => $SettleMode,//结算到余利宝
                        'Mcc' => $category_id,
                        'MerchantDetail' => base64_encode(json_encode($MerchantDetail)),
                        'TradeTypeList' => '01,02,06,08',
                        'PayChannelList' => '01,02',
                        'FeeParamList' => base64_encode(json_encode($FeeParamList)),
                        'AuthCode' => $code,
                        //'DeniedPayToolList' => '01',//01：不禁用 02：信用卡 03：花呗（仅支付宝）
                        'SupportStage' => 'Y',
                        'PartnerType' => $PartnerType,
                        //'AlipaySource' => $MyBankConfig->ali_pid
                    ];

                    //指定渠道进件
                    if ($MyBankConfig->WechatChannel) {
                        // $data['WechatChannel']=$MyBankConfig->WechatChannel;
                    }

                    //蓝海行动
                    if ($category_id == "000000") {
                        $data['Mcc'] = '2015050700000000';
                    }

                    //活动
                    if (!empty($other)) {
                        //蓝海行动
                        if ($other['activity_type'] == 'RBLUE') {
                            $data['RateVersion'] = 'RBLUE';
                        }
                    }


                    //结算到他行卡
                    if ($SettleModeType == '01') {
                        //企业
                        if ($Store->store_type == 2) {
                            //银行卡
                            $BankCardParam = [
                                "BankCertName" => $StoreBank->store_bank_name,//名称
                                "BankCardNo" => $StoreBank->store_bank_no,//银行卡号
                                "AccountType" => '02',//账户类型。可选值：01：对私账 02对公账户
                                "ContactLine" => $StoreBank->bank_no,//联航号
                            ];
                            $data['BankCardParam'] = base64_encode(json_encode($BankCardParam));
                        } //个体个人
                        else {
                            //银行卡
                            $BankCardParam = [
                                "BankCertName" => $StoreBank->store_bank_name,//名称
                                "BankCardNo" => $StoreBank->store_bank_no,//银行卡号
                                "AccountType" => '01',//账户类型。可选值：01：对私账 02对公账户
                                //"ContactLine" => $StoreBank->banknum,
                                "BranchName" => $StoreBank->sub_bank_name,
                                "BranchProvince" => $StoreBank->bank_province_code,//省编号
                                "BranchCity" => $StoreBank->bank_city_code,//市编号
                                "CertType" => "01",//持卡人证件类型。可选值： 01：身份证
                                "CertNo" => $StoreBank->bank_sfz_no != "" && $StoreBank->bank_sfz_no != "undefined" ? $StoreBank->bank_sfz_no : $Store->head_sfz_no,//持卡人证件号码
                                "CardHolderAddress" => $Store->store_address,//持卡人地址
                            ];
                            $data['BankCardParam'] = base64_encode(json_encode($BankCardParam));
                        }
                    }

                    $insert = $data;

                    $data['OutTradeNo'] = date('YmdHis') . time() . rand(10000, 99999);
                    $ao->Version = '1.0.0';
                    $this->url = $aop->MY_BANK_request2;
                    $re = $ao->Request($data);


                    if ($re['status'] == 2) {
                        $status = 3;
                        $status_desc = $re['message'];
                        $send_ways_data['status_desc'] = $status_desc;
                        //$return = $this->send_ways_data($send_ways_data);
                        return json_encode($re);
                    }
                    $Result = $re['data']['document']['response']['body']['RespInfo'];
                    $body = $re['data']['document']['response']['body'];
                    //失败
                    if ($Result['ResultStatus'] == 'F') {
                        $status = 3;
                        $status_desc = $Result['ResultMsg'];
                        $send_ways_data['status_desc'] = $status_desc;
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    } elseif ($Result['ResultStatus'] == 'S') {
                        $status = 2;
                        $status_desc = "审核中";
                        $send_ways_data['status'] = $status;
                        $send_ways_data['status_desc'] = $status_desc;
                        $return = $this->send_ways_data($send_ways_data);
                        //成功
                        $insert['OrderNo'] = $body['OrderNo'];
                        $insert['config_id'] = $config_id;

                        $MyBankStoreTem = MyBankStoreTem::where('OutMerchantId', $store_id)->first();
                        if ($MyBankStoreTem) {
                            $MyBankStoreTem->update($insert);
                            $MyBankStoreTem->save();
                        } else {
                            MyBankStoreTem::create($insert);
                        }
                        return json_encode([
                            'status' => 1,
                            'message' => '提交成功等待审核',
                        ]);

                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '未知状态,请重新提交',
                        ]);
                    }

                } catch (\Exception $exception) {
                    Log::info($exception);
                    return json_encode([
                        'status' => 2,
                        'message' => $exception->getMessage(),
                    ]);
                }


            }

            //和融通
            if (8999 < $type && $type < 9999) {
                $config = new HConfigController();
                $h_config = $config->h_config($config_id);
                if (!$h_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '没有配置相关通道'
                    ]);
                }

                //进件
                $url = "https://merch.hybunion.cn/JHAdminConsole/phone/phoneMicroMerchantInfo_addAggPayMerchantInfo.action";
                $jh_mid = "";

                //修改
                if ($ways1 && $ways1->status == 3) {
                    $url = "https://merch.hybunion.cn/JHAdminConsole/updateMerReportInfo.action";
                    $h_merchant = $config->h_merchant($store_id, $store_pid);
                    if ($h_merchant) {
                        $jh_mid = $h_merchant->h_mid;
                    }
                }

                $data = [
                    'store_id' => $store_id,
                    'phone' => $phone,
                    'email' => $store_email,
                    'request_url' => $url,
                    'isHighQualityMer' => '1', //是否是优质商户
                    'scanRate' => $rate / 100,
                    'settleType' => 'D',
                    'saleId' => $h_config->saleId,
                    'jh_mid' => $jh_mid
                ];
                $store_obj = new \App\Api\Controllers\Huiyuanbao\StoreController();
                $return = $store_obj->open_store($data);

                //不成功
                if ($return['status'] == 2) {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message'],
                    ]);
                }

                //第一次进件
                if ($jh_mid == "") {
                    //成功
                    $reportInfo = $return['reportInfo'];
                    $mid = $reportInfo['mid'];
                } else {
                    //修改
                    $mid = $jh_mid;
                }

                $HStore = HStore::where('store_id', $store_id)->first();
                $data_insert = [
                    'store_id' => $store_id,
                    'config_id' => $config_id,
                    'h_mid' => $mid,
                    'h_status' => 2,
                    'h_status_desc' => '审核中',
                ];
                if ($HStore) {
                    $HStore->update($data_insert);
                    $HStore->save();
                } else {
                    HStore::create($data_insert);
                }

                $data['rate'] = $rate;
                $data['status'] = 2;
                $data['status_desc'] = '审核中';
                $data['company'] = 'herongtong';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //微信官方
            if ($type == '2000') {
                $store_type = $Store->store_type; //经营性质 1-个体，2-企业，3-个人

                //暂不支持 企业
                if ($store_type == 2) {
                    return json_encode([
                        'status' => 2,
                        'message' => '暂不支持企业进件',
                    ]);
                }

                $config = new WeixinConfigController();
                $weixin_config = $config->weixin_config_obj($config_id);
                $obj = new \App\Api\Controllers\Weixin\StoreController();
                $product_desc = "其他";

                //超市便利店
                if ($Store->category_id == "2015091000052157") {
                    $product_desc = "线下零售";
                }

                //美容
                if ($Store->category_id == "2015062600004525") {
                    $product_desc = "居民生活服务";
                }

                //餐饮
                if ($Store->category_id == "2015050700000000") {
                    $product_desc = "餐饮";
                }

                //休闲娱乐
                if ($Store->category_id == "2015063000020189") {
                    $product_desc = "休闲娱乐";
                }

                //交通出行
                if ($Store->category_id == "2016062900190124") {
                    $product_desc = "交通出行";
                }

                $data_info = [
                    //参数信息
                    'mch_id' => $weixin_config->wx_merchant_id,
                    'key' => $weixin_config->key,
                    'getcertficates_request_url' => 'https://api.mch.weixin.qq.com/risk/getcertficates',
                    'upload_img_request_url' => 'https://api.mch.weixin.qq.com/secapi/mch/uploadmedia',
                    //'submit_request_url' => 'https://api.mch.weixin.qq.com/applyment/micro/submit',
                    'submit_request_url' => 'https://api.mch.weixin.qq.com/v3/applyment4sub/applyment/',
                    'sslCertPath' => public_path() . $weixin_config->cert_path,
                    'sslKeyPath' => public_path() . $weixin_config->key_path,
                    'public_key_path' => public_path() . $weixin_config->key_path,
                    "product_desc" => $product_desc, //售卖商品/提供服务描述
                    "rate" => "" . $rate . "", //费率

                    //门店信息
                    "head_name" => $Store->head_name,//身份证姓名
                    "head_sfz_no" => $Store->head_sfz_no,//身份证号码
                    "head_sfz_stime" => $Store->head_sfz_stime,//身份证有效期
                    "head_sfz_time" => $Store->head_sfz_time,//身份证有效期
                    "store_name" => $Store->head_name,//小微 先走法人名称
                    "city_code" => $Store->city_code,//门店省市编码
                    "store_address" => $Store->store_address,//门店街道名称
                    "store_short_name" => $Store->store_short_name,//商户简称
                    "people_phone" => $Store->people_phone,//客服电话
                    "people" => $Store->people,//联系人姓名

                    //银行信息
                    "store_bank_name" => $StoreBank->store_bank_name,//银行卡户名
                    "bank_name" => $StoreBank->bank_name,//开户银行
                    "bank_city_code" => $StoreBank->bank_city_code,//开户银行省市编码
                    "sub_bank_name" => $StoreBank->sub_bank_name,//开户银行全称（含支行）
                    "store_bank_no" => $StoreBank->store_bank_no,//银行账号
                ];

                //图片信息
                $img_data = [
                    'head_sfz_img_a' => $StoreImg->head_sfz_img_a,
                    'head_sfz_img_b' => $StoreImg->head_sfz_img_b,
                    'store_logo_img' => $StoreImg->store_logo_img,
                    'store_img_b' => $StoreImg->store_img_b,
                ];

                $return = $obj->xw_store($data_info, $img_data);

                //报错
                if ($return['status'] == 2) {
                    return $return;
                }

                //成功
                $applyment_id = $return['data']['applyment_id']; //微信支付分配的申请单号
                $insert = [
                    'config_id' => $config_id,
                    'applyment_id' => $applyment_id,
                    'store_id' => $store_id,
                    'store_type' => $Store->store_type,
                    'xw_status' => 2
                ];
                $MyBankStoreTem = WeixinStoreItem::where('store_id', $store_id)->first();
                if ($MyBankStoreTem) {
                    $MyBankStoreTem->update($insert);
                    $MyBankStoreTem->save();
                } else {
                    WeixinStoreItem::create($insert);
                }

                //入临时库
                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = 2;
                $data['status_desc'] = '审核中';
                $data['company'] = 'weixin';
                $return = $this->send_ways_data($data);

                return json_encode($return);
            }

            //支付宝官方
            if ($type == '1000') {
                return json_encode([
                    'status' => 2,
                    'message' => '请在->我的->认证中心->支付宝授权',
                ]);
            }

            //富友聚合
            if (10999 < $type && $type < 11999) {
                $config = new FuiouConfigController();
                $fuiou_config = $config->fuiou_config($config_id);
                if (!$fuiou_config) {
                    return json_encode(['status' => 2, 'message' => '没有配置相关通道']);
                }

                if (mb_strlen($Store->store_name, 'UTF8') < 5) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店名称必须大于5个字',
                    ]);
                }

                //经营性质 1-个体，2-企业，3-个人
                //如果是个人
                $license_type = "0";
                $license_no = $Store->store_license_no;
                $license_expire_dt = date('Ymd', strtotime($Store->store_license_time));

                //个人
                if ($store_type == 3) {
                    $license_type = 'A';
                    $license_no = $Store->head_sfz_no;
                    $license_expire_dt = date('Ymd', strtotime($Store->head_sfz_time));
                }

                if ($store_type == 1) {
                    $license_type = 'B';
                }

                //需要处理
                $business = "207"; //类目
                $bank_type = "0102";
                $inter_bank_no = $StoreBank->bank_no; //入账卡开户行联行号,
                $iss_bank_nm = $StoreBank->sub_bank_name; //入账卡开户行名称
                $settle_tp = "1"; ///清算类型
                $acnt_certif_id = $StoreBank->bank_sfz_no != "" && $StoreBank->bank_sfz_no != "undefined" ? $StoreBank->bank_sfz_no : $Store->head_sfz_no; //入账证件号,
                $acnt_certif_expire_dt = $Store->bank_sfz_time ? $Store->bank_sfz_time : $Store->head_sfz_time; //入账证件到期日,
                $acnt_certif_expire_dt = date('Ymd', strtotime($acnt_certif_expire_dt));

                $acnt_nm = $StoreBank->store_bank_name; //入账卡户名
                $acnt_no = $StoreBank->store_bank_no; //入账卡号（不带长度位）,

                $head_sfz_no = $Store->head_sfz_no;
                $head_sfz_time = $Store->head_sfz_time;
                $head_sfz_time = date('Ymd', strtotime($head_sfz_time));

                $acnt_type = $store_bank_type == "01" ? '2' : "1";

                $request = [
                    'trace_no' => time(),//唯一流水号
                    'ins_cd' => $fuiou_config->ins_cd,//机构号
                    'mchnt_name' => $store_name,//商户名称
                    'mchnt_shortname' => $Store->store_short_name,//商户简称
                    'real_name' => $Store->store_name,//营业执照名称
                    'license_type' => $license_type,//证件类型：0 营业执照，1 三证合一，A 身份证（一证下机）B 个体户
                    'license_no' => $license_no,//证件号码，填写方法：1.license_type=0 或1，此处填写营业执照号码。2.license_type=A，此处填写身份证号码3.license_type=B，此处填写个体工商户营业执照号码
                    'license_expire_dt' => $license_expire_dt,//证件到期日（格式yyyyMMdd）格式 长期请填20991231 无有效期请填 19000101 1.license_type=0 或1，此处填写 营业执照到期日。 2.license_type=A 此处填写身份证 的到期日 3.license_type=B，此处填写个体 工商户营业执照号的到期日
                    'certif_id' => $head_sfz_no,//法人身份证号码
                    'certif_id_expire_dt' => $head_sfz_time,//法人身份证到期日（格式YYYYMMDD）
                    'contact_person' => $Store->people ? $Store->people : $Store->head_name,//联系人姓名,
                    'contact_phone' => $Store->people_phone ? $Store->people_phone : '',//客服电话,
                    'contact_addr' => $Store->store_address,//联系人地址,
                    'contact_mobile' => $Store->people_phone,//联系人电话,
                    'contact_email' => $Store->store_email,//联系人邮箱,
                    'business' => $business,//经营范围代码（新开户则必填）,
                    'city_cd' => $Store->city_code,//市代码,
                    'county_cd' => $Store->area_code,//区代码,
                    'acnt_type' => $acnt_type,//入账卡类型：1：对公；2：对私; 入账卡类型为1 时，对公户户名需 与营业执照名称保持一致（进件若 为双账户时，此处必填2，即对私 结算）,
                    'bank_type' => $bank_type,//行别,（acnt_type=1 必填）(参考 行别对照表) 见附件7 行别对照表,
                    'inter_bank_no' => $inter_bank_no,//入账卡开户行联行号,
                    'iss_bank_nm' => $iss_bank_nm,//入账卡开户行名称,
                    'acnt_nm' => $acnt_nm,//入账卡户名,
                    'acnt_no' => $acnt_no,//入账卡号（不带长度位）,
                    'settle_tp' => $settle_tp,//清算类型,
                    'artif_nm' => $Store->head_name,//法人姓名,
                    'acnt_artif_flag' => $Store->head_name == $StoreBank->store_bank_name ? '1' : '0',//法人入账标识(0:非法人入账,1:法人入账,
                    'acnt_certif_tp' => '0',//入账证件类型("0":"身份证,
                    'acnt_certif_id' => $acnt_certif_id,//入账证件号,
                    'acnt_certif_expire_dt' => $acnt_certif_expire_dt,//入账证件到期日,
                ];

                $request['key'] = $fuiou_config->md_key;
                $open_store = new \App\Api\Controllers\Fuiou\StoreController();
                $fuiou_res = $open_store->open_store($request);
                //Log::info('富友支付-进件结果');
                //Log::info($fuiou_res);
            }

            //传化支付
            if (11999 < $type && $type < 12999) {
                return json_encode([
                    'status' => 2,
                    'message' => '暂时不支持申请此通道！请更换换其他通道',
                ]);

                $store_obj = new \App\Api\Controllers\Tfpay\StoreController();

                $return = $store_obj->open_store($Store, $StoreBank, $StoreImg, $qd);
                //不成功
                if ($return['status'] == 2) {
                    return json_encode([
                        'status' => 2,
                        'message' => $return['message'],
                    ]);
                }

                $TfStore = TfStore::where('store_id', $store_id)->first();

                $config = new TfConfigController();
                $tf_config = $config->tf_config($config_id, $qd);
                if (!$tf_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '传化配置不存在请检查配置'
                    ]);
                }

                $data_insert = [
                    'store_id' => $store_id,
                    'config_id' => $config_id,
                    'agent_id' => $return['data']['agent_id'],
                    'sub_mch_id' => $return['data']['sub_mch_id'],
                    'status' => $return['data']['status'],
                    'qd' => $qd,
                    'alipay_pid' => $tf_config->alipay_pid,
                    'wechat_channel_no' => $tf_config->wechat_channel_no,
                ];
                if ($TfStore) {
                    $TfStore->update($data_insert);
                    $TfStore->save();
                } else {
                    TfStore::create($data_insert);
                }

                $status_desc = "审核中";
                $status = 2;
                if ($return['data']['status'] == 1) {
                    $status_desc = "开通成功";
                    $status = 1;
                }

                if ($return['data']['status'] == 4) {
                    $status_desc = '进件失败请修改资料重新进件';
                    $status = 3;
                }

                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $status;
                $data['status_desc'] = $status_desc;
                $data['company'] = 'tfpay';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //支付宝直付通
            if (15999 < $type && $type < 16999) {
                $isvconfig = new AlipayIsvConfigController();
                $config_type = '03';//直付通
                $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);
                if (!$config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '直付通未配置'
                    ]);
                }

                //1.接入参数初始化
                $aop = new AopClient();
                $aop->apiVersion = "2.0";
                $aop->appId = $config->app_id;
                $aop->rsaPrivateKey = $config->rsa_private_key;
                $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                $aop->signType = "RSA2";//升级算法
                $aop->gatewayUrl = $config->alipay_gateway;
                $aop->format = "json";
                $aop->charset = "GBK";
                $aop->gatewayUrl = $config->alipay_gateway; //


                // $store_type; //经营性质 1-个体，2-企业，3-个人
                //商家类型：01：企业；02：事业单位；03：民办非企业组织；04：社会团体；05：党政及国家机关；06：个人商户；07：个体工商户

                $business_address = [
                    'province_code' => $Store->province_code,
                    'city_code' => $Store->city_code,
                    'district_code' => $Store->area_code,
                    'address' => $Store->store_address,
                ];
                $contact_infos = [
                    0 => [
                        'name' => $Store->people,
                        'phone' => $Store->people_phone,
                        'mobile' => $Store->people_phone,
                        'type' => 'LEGAL_PERSON',
                        [
                            0 => "06",
                        ]
                    ],
                ];

                // $SettleModeType;//01-到卡 02-到支付宝

                if ($SettleModeType == "01") {

                    $usage_type = "01";
                    if ($store_bank_type == "01") {
                        $usage_type = "02";
                    }


                    if (!Schema::hasTable('alipay_zft_banks')) {
                        return json_encode(['status' => 2, 'message' => '直付通银行卡信息基础库未配置']);
                    }


                    if (env('DB_D1_HOST')) {
                        $instName = DB::connection("mysql_d1")->table("alipay_zft_banks")
                            ->where('instName', $StoreBank->bank_name)
                            ->select('instId')
                            ->first();

                    } else {
                        $instName = DB::table('alipay_zft_banks')
                            ->where('out_trade_no', $StoreBank->bank_name)
                            ->select('instId')
                            ->first();
                    }

                    if (!$instName) {
                        return json_encode(['status' => 2, 'message' => '直付通开户行简称缩写没有找到']);
                    }

                    $biz_cards = [
                        0 => [
                            'account_holder_name' => $StoreBank->store_bank_name,//卡户名
                            'account_no' => $StoreBank->store_bank_no,//银行卡号
                            'account_inst_province' => $Store->province_name,//
                            'account_inst_city' => $Store->city_name,//
                            'account_branch_name' => $Store->area_name,//
                            'usage_type' => $usage_type,//对公-01 对私-02
                            'account_type' => 'DC',//借记卡-DC
                            'account_inst_name' => $StoreBank->bank_name,//银行名称
                            'account_inst_id' => $instName->instId,//开户行简称缩写
                        ]
                    ];

                    $data_re = [
                        'external_id' => $store_id,//商户编号，由机构定义，需要保证在机构下唯一
                        'name' => $store_name,
                        'alias_name' => $store_short_name,
                        'mcc' => $MyBankCategory->mcc,
                        'legal_name' => $Store->head_name,
                        'legal_cert_no' => $Store->head_sfz_no,
                        'business_address' => $business_address,
                        'service_phone' => $Store->people_phone,
                        'contact_infos' => $contact_infos,
                        'biz_cards' => $biz_cards,
                        'service' => [
                            0 => "当面付",
                        ],
                        'sign_time_with_isv' => date('Y-m-d', time()),
                        'binding_alipay_logon_id' => $Store->alipay_account,//支付宝账户
                        'cert_name' => $Store->store_name,
                    ];

                } else {
                    $data_re = [
                        'external_id' => $store_id,//商户编号，由机构定义，需要保证在机构下唯一
                        'name' => $store_name,
                        'alias_name' => $store_short_name,
                        'mcc' => $MyBankCategory->mcc,
                        'legal_name' => $Store->head_name,
                        'legal_cert_no' => $Store->head_sfz_no,
                        'business_address' => $business_address,
                        'service_phone' => $Store->people_phone,
                        'contact_infos' => $contact_infos,
                        'service' => [
                            0 => "当面付",
                        ],
                        'sign_time_with_isv' => date('Y-m-d', time()),
                        'alipay_logon_id' => $Store->alipay_account,//支付宝账户
                        'binding_alipay_logon_id' => $Store->alipay_account,//支付宝账户
                        'cert_name' => $Store->store_name,
                    ];
                }


                //个人
                if ($store_type == '3') {
                    $merchant_type = '06';
                    $cert_no = $Store->head_sfz_no;
                    $cert_type = '100';//100：个人商户身份证

                } else {
                    $cert_no = $license_no;
                    $cert_type = '2011'; //201：营业执照；2011:营业执照(统一社会信用代码)

                    if ($store_type == '2') {
                        $merchant_type = '01'; //企业
                    } else {
                        $merchant_type = '07'; //个体
                    }

                    //图片
                    //营业执照
                    $img_url = $StoreImg->store_license_img;
                    $license = $this->alipay_img_public($img_url, $config, $aop);

                    $data_re['cert_image'] = $license;
                }

                //图片
                //法人身份证正面
                $img_url = $StoreImg->head_sfz_img_a;
                $head_sfz_img_a = $this->alipay_img_public($img_url, $config, $aop);
                $data_re['legal_cert_front_image'] = $head_sfz_img_a;

                //图片
                //法人身份证反面
                $img_url = $StoreImg->head_sfz_img_b;
                $head_sfz_img_b = $this->alipay_img_public($img_url, $config, $aop);
                $data_re['legal_cert_back_image'] = $head_sfz_img_b;

                //门头
                $img_url = $StoreImg->store_logo_img;
                $store_logo_img = $this->alipay_img_public($img_url, $config, $aop);
                $data_re['out_door_images'] = [
                    0 => $store_logo_img,
                ];
                $data_re['merchant_type'] = $merchant_type;
                $data_re['cert_no'] = $cert_no;
                $data_re['cert_type'] = $cert_type;

                $AlipayZftStore = AlipayZftStore::where('store_id', $store_id)->first();
                if ($AlipayZftStore && $AlipayZftStore->status == "99") {
                    //修改门店
                    $aop->method = "ant.merchant.expand.indirect.zft.modify";
                    $requests = new AntMerchantExpandIndirectZftModifyRequest();
                } else {
                    //创建门店
                    $aop->method = "ant.merchant.expand.indirect.zft.create";
                    $requests = new AntMerchantExpandIndirectZftCreateRequest();
                }

                $data_re = json_encode($data_re);
                $requests->setBizContent($data_re);
                $result = $aop->execute($requests);

                $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
                $resultCode = $result->$responseNode->code;
                if (!empty($resultCode) && $resultCode == 10000) {
                    $order_id = $result->$responseNode->order_id; //订单号
                    $insert = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'order_id' => $order_id,
                        'ip_role_id' => '',
                        'store_name' => $store_name,
                        'ext_info' => '',
                        'alipay_account' => $Store->alipay_account,
                        'SettleModeType' => $SettleModeType,
                    ];

                    if ($AlipayZftStore) {
                        $AlipayZftStore->update($insert);
                        $AlipayZftStore->save();
                    } else {
                        AlipayZftStore::create($insert);
                    }

                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['status'] = 2;
                    $data['status_desc'] = '等待商户支付宝确认';
                    $data['company'] = 'zft';
                    $return = $this->send_ways_data($data);

                    return $return;
                } else {
                    $data = [
                        'status' => 2,
                        "message" => $result->$responseNode->sub_msg . '-' . $result->$responseNode->msg,
                    ];

                    return json_encode($data);
                }
            }

            //随行付
            if (12999 < $type && $type < 13999) {
                $obj = new \App\Api\Controllers\Vbill\BaseController();
                $config = new VbillConfigController();
                $vbill_config = $config->vbill_config($config_id);
                if (!$vbill_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '随行付配置不存在请检查配置'
                    ]);
                }

                $privateKey = $vbill_config->privateKey;
                $sxfpublic = $vbill_config->sxfpublic;
                $orgId = $vbill_config->orgId;
                $upload_request_url = $obj->upload_url;

                //默认失败
                $send_ways_data['rate'] = $rate;
                $send_ways_data['store_id'] = $store_id;
                $send_ways_data['status'] = 3;
                $send_ways_data['company'] = 'vbill';

                //商户资料
                $CertPhotoA = $this->images_get($StoreImg->head_sfz_img_a); //法人身份证正面
                $CertPhotoB = $this->images_get($StoreImg->head_sfz_img_b); //法人身份证反面
                $ShopPhoto = $this->images_get($StoreImg->store_logo_img); //门头照片
                $LicensePhoto = $this->images_get($StoreImg->store_license_img); //营业执照
                $CheckstandPhoto = $this->images_get($StoreImg->store_img_a); //收银台
                $ShopEntrancePhoto = $this->images_get($StoreImg->store_img_b); //门店内景

                $CertPhotoA = $this->vbill_uploadIMG($upload_request_url, '02', $CertPhotoA, $config_id, '法人身份证正面');
                if ($CertPhotoA['status'] == 0) {
                    $send_ways_data['status_desc'] = $CertPhotoA['message'];
                    $return = $this->send_ways_data($send_ways_data);
                    return json_encode($return);
                }

                $CertPhotoB = $this->vbill_uploadIMG($upload_request_url, '03', $CertPhotoB, $config_id, '法人身份证反面');
                if ($CertPhotoB['status'] == 0) {
                    $send_ways_data['status_desc'] = $CertPhotoB['message'];
                    $return = $this->send_ways_data($send_ways_data);
                    return json_encode($return);
                }

                $ShopPhoto = $this->vbill_uploadIMG($upload_request_url, '10', $ShopPhoto, $config_id, '门头照片');
                if ($ShopPhoto['status'] == 0) {
                    $send_ways_data['status_desc'] = $ShopPhoto['message'];
                    $return = $this->send_ways_data($send_ways_data);
                    return json_encode($return);
                }

                $CheckstandPhoto = $this->vbill_uploadIMG($upload_request_url, '12', $CheckstandPhoto, $config_id, '收银台');
                if ($CheckstandPhoto['status'] == 0) {
                    $send_ways_data['status_desc'] = $CheckstandPhoto['message'];
                    $return = $this->send_ways_data($send_ways_data);
                    return json_encode($return);
                }

                $ShopEntrancePhoto = $this->vbill_uploadIMG($upload_request_url, '11', $ShopEntrancePhoto, $config_id, '内景照片');
                if ($ShopEntrancePhoto['status'] == 0) {
                    $send_ways_data['status_desc'] = $ShopEntrancePhoto['message'];
                    $return = $this->send_ways_data($send_ways_data);
                    return json_encode($return);
                }

                $reqData = [
                    'mecDisNm' => $store_short_name,//
                    'mblNo' => $Store->people_phone,
                    'operationalType' => '01',//取值范围：01 线下 02 线上 03 非盈利类 04缴费类 05 保险类 06 私立院校
                    'mecTypeFlag' => '00',
                    'qrcodeList' => [
                        [
                            'rateType' => '01',
                            'rate' => $rate,
                        ],
                        [
                            'rateType' => '02',
                            'rate' => $rate,
                        ],
                        [
                            'rateType' => '06', //银联单笔小于等于 1000
                            'rate' => $rate_a,
                        ],
                        [
                            'rateType' => '07', //银联单笔大于 1000
                            'rate' => $rate_c,
                        ]
                    ],
                    'settleType' => '04',
                    'cprRegAddr' => $Store->store_address,
                    'regProvCd' => $Store->province_code . '000000',
                    'regCityCd' => $Store->city_code . '000000',
                    'regDistCd' => $Store->area_code . '000000',
                    'mccCd' => $MyBankCategory->mcc,//类目
                    'csTelNo' => $Store->people_phone,
                    'identityName' => $Store->head_name,
                    'identityTyp' => '00',
                    'identityNo' => $Store->head_sfz_no,
                    'legalPersonLicStt' => $Store->head_sfz_stime ? date('Ymd', strtotime($Store->head_sfz_stime)) : "",
                    'legalPersonLicEnt' => ($Store->head_sfz_time == '长期') ? '29991231' : date('Ymd', strtotime($Store->head_sfz_time)),
                    'actNm' => $StoreBank->store_bank_name,
                    'actTyp' => $StoreBank->store_bank_type == "02" ? "00" : "01",//00 对公 01 对私
                    'actNo' => $StoreBank->store_bank_no,
                    'lbnkNo' => $StoreBank->bank_no,
                    'lbnkNm' => $StoreBank->sub_bank_name,
                    'legalPersonidPositivePic' => $CertPhotoA['PhotoUrl'],
                    'legalPersonidOppositePic' => $CertPhotoB['PhotoUrl'],
                    'storePic' => $ShopPhoto['PhotoUrl'],
                    'insideScenePic' => $ShopEntrancePhoto['PhotoUrl'],
                ];

                if (strpos($Store->area_name, '镇')) {
//                    $reqData['regDistCd']= $Store->area_code . '000';
                    $reqData['regDistCd'] = $Store->city_code . $Store->area_code;
                }

                $reqData_img = [
                    'legalPersonidPositivePic' => $CertPhotoA['PhotoUrl'],
                    'legalPersonidOppositePic' => $CertPhotoB['PhotoUrl'],
                    'storePic' => $ShopPhoto['PhotoUrl'],
                    'insideScenePic' => $ShopEntrancePhoto['PhotoUrl'],
                ];

                //非法人结算
                if ($StoreBank->store_bank_type == "01" && $Store->head_name != $StoreBank->store_bank_name) {
                    $settlePersonIdcardPositive = $this->images_get($StoreImg->bank_sfz_img_a);
                    $settlePersonIdcardOpposite = $this->images_get($StoreImg->bank_sfz_img_b);
                    $letterOfAuthPic = $this->images_get($StoreImg->store_auth_bank_img);

                    $settlePersonIdcardPositive = $this->vbill_uploadIMG($upload_request_url, '08', $settlePersonIdcardPositive, $config_id, '结算人身份证正面（人像面）');
                    if ($settlePersonIdcardPositive['status'] == 0) {
                        $send_ways_data['status_desc'] = $settlePersonIdcardPositive['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }

                    $settlePersonIdcardOpposite = $this->vbill_uploadIMG($upload_request_url, '07', $settlePersonIdcardOpposite, $config_id, '结算人身份证反面（国徽面）');
                    if ($settlePersonIdcardOpposite['status'] == 0) {
                        $send_ways_data['status_desc'] = $settlePersonIdcardOpposite['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }

                    $letterOfAuthPic = $this->vbill_uploadIMG($upload_request_url, '26', $letterOfAuthPic, $config_id, '非法人结算授权函');
                    if ($letterOfAuthPic['status'] == 0) {
                        $send_ways_data['status_desc'] = $letterOfAuthPic['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }

                    $reqData['settlePersonIdcardPositive'] = $settlePersonIdcardPositive['PhotoUrl'];
                    $reqData['settlePersonIdcardOpposite'] = $settlePersonIdcardOpposite['PhotoUrl'];
                    $reqData['letterOfAuthPic'] = $letterOfAuthPic['PhotoUrl'];

                    $reqData_img['settlePersonIdcardPositive'] = $settlePersonIdcardPositive['PhotoUrl'];
                    $reqData_img['settlePersonIdcardOpposite'] = $settlePersonIdcardOpposite['PhotoUrl'];
                    $reqData_img['letterOfAuthPic'] = $letterOfAuthPic['PhotoUrl'];
                }

                //对私结算
                if ($StoreBank->store_bank_type == "01") {
                    if ($store_type == 3) {
                        $reqData['stmManIdNo'] = $Store->head_sfz_no;
                    } else {
                        $reqData['stmManIdNo'] = $StoreBank->bank_sfz_no != "" && $StoreBank->bank_sfz_no != "undefined" ? $StoreBank->bank_sfz_no : $Store->head_sfz_no;
                    }

                    $reqData['accountLicStt'] = $Store->bank_sfz_stime ? date('Ymd', strtotime($Store->bank_sfz_stime)) : date('Ymd', strtotime($Store->head_sfz_stime));
                    $reqData['accountLicEnt'] = $Store->head_sfz_time == '长期' ? '29991231' : date('Ymd', strtotime($Store->head_sfz_time));

                    $bankCardPositivePic = $this->images_get($StoreImg->bank_img_a); //银行卡正面
                    $bankCardOppositePic = $this->images_get($StoreImg->bank_img_b); //银行卡反面

                    $bankCardPositivePic = $this->vbill_uploadIMG($upload_request_url, '05', $bankCardPositivePic, $config_id, '银行卡正面');
                    if ($bankCardPositivePic['status'] == 0) {
                        $send_ways_data['status_desc'] = $bankCardPositivePic['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }

                    $bankCardOppositePic = $this->vbill_uploadIMG($upload_request_url, '06', $bankCardOppositePic, $config_id, '银行卡反面');
                    if ($bankCardOppositePic['status'] == 0) {
                        $send_ways_data['status_desc'] = $bankCardOppositePic['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }

                    $reqData['bankCardPositivePic'] = $bankCardPositivePic['PhotoUrl'];
                    $reqData['bankCardOppositePic'] = $bankCardOppositePic['PhotoUrl'];
                    $reqData_img['bankCardPositivePic'] = $bankCardPositivePic['PhotoUrl'];
                    $reqData_img['bankCardOppositePic'] = $bankCardOppositePic['PhotoUrl'];
                } else {
                    //对公结算

                    //开户许可证
                    $IndustryLicensePhoto = $this->images_get($StoreImg->store_industrylicense_img);
                    $IndustryLicensePhoto = $this->vbill_uploadIMG($upload_request_url, '04', $IndustryLicensePhoto, $config_id, '开户许可证');
                    if ($IndustryLicensePhoto['status'] == 0) {
                        $send_ways_data['status_desc'] = $IndustryLicensePhoto['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }
                    //开户许可证
                    $reqData['openingAccountLicensePic'] = $IndustryLicensePhoto['PhotoUrl'];
                    $reqData_img['openingAccountLicensePic'] = $IndustryLicensePhoto['PhotoUrl'];
                }

                //个人
                $haveLicenseNo = "01";
                $reqData['haveLicenseNo'] = $haveLicenseNo; //01 自然人 02 个体户 03 企业

                //企业
                if ($store_type == 2) {
                    $reqData['haveLicenseNo'] = "03";
                    $reqData['cprRegNmCn'] = $store_name;
                    $reqData['registCode'] = $Store->store_license_no;
                    $reqData['licenseMatch'] = '00';
                    $reqData['businessLicStt'] = $Store->store_license_stime ? date('Ymd', strtotime($Store->store_license_stime)) : "";
                    $reqData['businessLicEnt'] = ($Store->store_license_time == '长期') ? '29991231' : date('Ymd', strtotime($Store->store_license_time));

                    //资料
                    $LicensePhoto = $this->vbill_uploadIMG($upload_request_url, '13', $LicensePhoto, $config_id, '营业执照照片');
                    if ($LicensePhoto['status'] == 0) {
                        $send_ways_data['status_desc'] = $LicensePhoto['message'];
                        $return = $this->send_ways_data($send_ways_data);

                        return json_encode($return);
                    }
                    //营业执照
                    $reqData['licensePic'] = $LicensePhoto['PhotoUrl'];
                    $reqData_img['licensePic'] = $LicensePhoto['PhotoUrl'];
                }

                //个体
                if ($store_type == 1) {
                    $reqData['haveLicenseNo'] = "02";
                    $reqData['cprRegNmCn'] = $store_name;
                    $reqData['registCode'] = $Store->store_license_no;
                    $reqData['businessLicStt'] = $Store->store_license_stime ? date('Ymd', strtotime($Store->store_license_stime)) : "";
                    $reqData['businessLicEnt'] = ($Store->store_license_time == '长期') ? '29991231' : date('Ymd', strtotime($Store->store_license_time));

                    //资料
                    $LicensePhoto = $this->vbill_uploadIMG($upload_request_url, '13', $LicensePhoto, $config_id, '营业执照照片');
                    if ($LicensePhoto['status'] == 0) {
                        $send_ways_data['status_desc'] = $LicensePhoto['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }

                    //营业执照
                    $reqData['licensePic'] = $LicensePhoto['PhotoUrl'];
                    $reqData_img['licensePic'] = $LicensePhoto['PhotoUrl'];
                }

                $url = $obj->open_store_url;
                $VbillStore = VbillStore::where('store_id', $store_id)
                    ->first();
                if ($VbillStore && $VbillStore->taskStatus > 1) {
                    $reqData['mno'] = $VbillStore->mno;
                    $url = $obj->open_update_store_url;
                    //入驻图片驳回
                    if ($VbillStore->taskStatus == 3) {
                        $reqData = $reqData_img;
                        $reqData['mno'] = $VbillStore->mno;
                    }
                }
                $data = [
                    'orgId' => $orgId,
                    'reqId' => time(),
                    'version' => '1.0',
                    'timestamp' => date('Ymdhis', time()),
                    'signType' => 'RSA',
                    'reqData' => $reqData,
                ];

                $data = array_filter($data, function ($v) {
                    if ($v == "") {
                        return false;
                    } else {
                        return true;
                    }
                });
                $re = $obj->execute($data, $url, $privateKey, $sxfpublic);
                if ($re['status'] == 0) {
                    return json_encode([
                        'status' => 2,
                        'message' => $re['message'],
                    ]);
                }
                if ($re['data']['code'] == "0000" && $re['data']['respData']['bizCode'] == "0000") {

                    $status_desc = "审核中";
                    $status = 2;

                    $data_in = [
                        'config_id' => $config_id,
                        'mno' => $re['data']['respData']['mno'],
                        'store_id' => $store_id,
                        'applicationId' => $re['data']['respData']['applicationId'],
                    ];
                    if ($VbillStore) {
                        $VbillStore->update($data_in);
                        $VbillStore->save();
                    } else {
                        VbillStore::create($data_in);
                    }
                } else {
                    $status_desc = $re['data']['respData']['bizMsg'] ?? '审核失败';
                    $status = 3;
                }

                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $status;
                $data['status_desc'] = $status_desc;
                $data['company'] = 'vbill';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //汇付
            if (17999 < $type && $type < 18999) {
                if ($Store->store_type == '3') {
                    return json_encode([
                        'status' => '2',
                        'messsage' => '汇付支付暂不支持个人进件'
                    ]);
                }

                $hui_pay_base_obj = new \App\Api\Controllers\HuiPay\BaseController();
                $config = new HuiPayConfigController();
                $hui_pay_config = $config->hui_pay_config($config_id);
                if (!$hui_pay_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '汇付支付配置不存在请检查配置'
                    ]);
                }

                $hui_pay_merchant = $config->hui_pay_merchant($store_id, '');

                $mer_cust_id = $hui_pay_config->mer_cust_id; //商户客户号
                $source_num = $hui_pay_config->org_id; //渠道来源,机构号
                $private_key = $hui_pay_config->private_key;  //私钥
                $public_key = $hui_pay_config->public_key;  //公钥

                //默认失败
                $send_ways_data['rate'] = $rate;
                $send_ways_data['store_id'] = $store_id;
                $send_ways_data['status'] = 3;
                $send_ways_data['company'] = 'huipay';

                //商户资料
                $CertPhotoImgB = $StoreImg->head_sfz_img_b;  //法人身份证反面
                $ShopPhotoImg = $StoreImg->store_logo_img;  //门头照片
                $LicensePhotoImg = $StoreImg->store_img_b;  //内景照
                $CashierPhotoImg = $StoreImg->store_img_a; //收银台
                $ShopEntrancePhotoImg = $StoreImg->store_img_c; //门店全景照

                $file_tokens = [];

                $CertPhotoImgA = $StoreImg->head_sfz_img_a;  //法人身份证正面
                if ($CertPhotoImgA) {
                    $CertPhotoA = $this->hui_pay_upload_img($CertPhotoImgA, $config_id, '法人身份证正面');
//                var_dump($CertPhotoA);die;
                    if ($CertPhotoA['status'] != 1) {
                        $send_ways_data['status_desc'] = $CertPhotoA['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    } else {
                        $cert_photo_a = [
                            'attach_desc' => '法人身份证正面',
                            'attach_name' => '法人身份证正面',
                            'attach_type' => '05',  //文件类型，必须，'01-营业执照图片；02-税务登记证号；03-组织机构代码证；04-开户许可证；05-法人身份证正面；06-法人身份证反面；07-结算人身份证正面；08-结算人身份证反面；09-商务协议；10-公司照片一；11-公司照片二；12-公司照片三；13-联系人身份证正面；14-联系人身份证反面；15-店铺门头照片；16-店铺收银台照片；17-店内照片；18-结算卡正面；19-结算卡反面；20-主流餐饮平台入驻照片；21-D+1协议照片；22-结算变更说明表；23-授权委托书；99-其他'
                            'file_id' => $CertPhotoA['file_id']
                        ];
                        $file_tokens[] = $cert_photo_a;
                    }
                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => '法人身份证正面照必传'
                    ]);
                }

                if ($CertPhotoImgB) {
                    $CertPhotoB = $this->hui_pay_upload_img($CertPhotoImgB, $config_id, '法人身份证反面');
                    if ($CertPhotoB['status'] != 1) {
                        $send_ways_data['status_desc'] = $CertPhotoB['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    } else {
                        $cert_photo_b = [
                            'attach_desc' => '法人身份证反面',
                            'attach_name' => '法人身份证反面',
                            'attach_type' => '06',
                            'file_id' => $CertPhotoB['file_id']
                        ];
                        $file_tokens[] = $cert_photo_b;
                    }
                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => '法人身份证反面照必传'
                    ]);
                }

                if ($ShopPhotoImg) {
                    $ShopPhoto = $this->hui_pay_upload_img($ShopPhotoImg, $config_id, '门头照片');
                    if ($ShopPhoto['status'] != 1) {
                        $send_ways_data['status_desc'] = $ShopPhoto['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    } else {
                        $shop_shoto = [
                            'attach_desc' => '门头照片',
                            'attach_name' => '门头照片',
                            'attach_type' => '15',
                            'file_id' => $ShopPhoto['file_id']
                        ];
                        $file_tokens[] = $shop_shoto;
                    }
                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => '门头照片必传'
                    ]);
                }

                if ($CashierPhotoImg) {
                    $CashierPhoto = $this->hui_pay_upload_img($CashierPhotoImg, $config_id, '收银台');
                    if ($CashierPhoto['status'] != 1) {
                        $send_ways_data['status_desc'] = $CashierPhoto['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    } else {
                        $cashier_photo = [
                            'attach_desc' => '收银台',
                            'attach_name' => '收银台',
                            'attach_type' => '16',
                            'file_id' => $CashierPhoto['file_id']
                        ];
                        $file_tokens[] = $cashier_photo;
                    }
                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => '收银台照必传'
                    ]);
                }

                if ($ShopEntrancePhotoImg) {
                    $ShopEntrancePhoto = $this->hui_pay_upload_img($ShopEntrancePhotoImg, $config_id, '门店内全景照片');
                    if ($ShopEntrancePhoto['status'] != 1) {
                        $send_ways_data['status_desc'] = $ShopEntrancePhoto['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    } else {
                        $shop_entrance_photo = [
                            'attach_desc' => '门店内全景照片',
                            'attach_name' => '门店内全景照片',
                            'attach_type' => '17',
                            'file_id' => $ShopEntrancePhoto['file_id']
                        ];
                        $file_tokens[] = $shop_entrance_photo;
                    }
                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => '门店内全景照片必传'
                    ]);
                }

                //商户是否第一次进件
                if ($hui_pay_merchant) {
                    $is_apply_id = $hui_pay_merchant->apply_id;
                } else {
                    $is_apply_id = '';
                }
                if (!empty($is_apply_id) && isset($is_apply_id)) {
                    $sign_data = [
                        'apply_id' => $is_apply_id,  //企业开户申请号
                        'corp_short_name' => $Store->store_short_name,  //企业名称简称
                        'corp_business_address' => $Store->province_name . $Store->city_name . $Store->area_name . $Store->store_address,  //企业经营地址
                        'corp_fixed_telephone' => $Store->people_phone,  //企业固定电话
                        'contact_name' => $Store->people,  //联系人姓名
                        'contact_mobile' => $Store->people_phone,  //联系人手机
                        'contact_email' => $Store->store_email,  //联系人邮箱
                        'legal_name' => $Store->head_name, //法人姓名
                        'legal_id_card' => ($StoreBank->bank_sfz_no != "" && $StoreBank->bank_sfz_no != "undefined") ? $StoreBank->bank_sfz_no : $Store->head_sfz_no, //法人证件号
                        'legal_id_card_type' => '10', //法人证件类型
                        'legal_cert_start_date' => $this->huiPayDate($Store->head_sfz_stime), //法人证件起始日期
                        'legal_cert_end_date' => $this->huiPayDate($Store->head_sfz_time), //法人证件起始日期
                        'legal_mobile' => $Store->people_phone, //法人手机号
                        'prov_code' => $Store->province_code, //省份
                        'city_code' => $Store->city_code, //地区
                        'district_code' => $Store->area_code, //区县
                    ];
                } else {
                    $apply_id = $this->createApplyId(20);  //记录到库中，后续查询用的到

                    $second_level_code = $MyBankCategory->second_level_code;  //通过平台二级分类名称找到汇付相关信息
                    $hui_pay_category = McscTManageCategory::where('second_level_code', $second_level_code)->first();
                    if (!$hui_pay_category) {
                        return json_encode([
                            'status' => '2',
                            'message' => '汇付行业类目不匹配'
                        ]);
                    }

                    $bank_info_hui_pay = BankInfoHuipay::where('bank_name', $StoreBank->bank_name)->first();
                    if (!$bank_info_hui_pay) {
                        return json_encode([
                            'status' => '2',
                            'message' => '汇付银行信息不匹配'
                        ]);
                    }

                    $sign_data = [
                        'login_name' => $Store->people, //登陆人姓名,必须
                        'login_mobile' => $Store->people_phone, //登陆人手机号,必须,以1开头的11位数字
                        'social_credit_code' => $Store->store_license_no, //统一社会信用代码,企业证照类型 为三证合一时必填
                        'license_start_date' => $Store->store_license_stime ? $this->huiPayDate($Store->store_license_stime) : '', //证照起始日期,必填
                        'license_end_date' => $Store->store_license_time ? $this->huiPayDate($Store->store_license_time) : '', //证照截止日期,必填,永久填'99991231'
                        'legal_name' => $Store->head_name, //法人姓名,必须
                        'legal_id_card_type' => '10', //法人证件类型,必须,'10:身份证；11-护照；12-军官证；13-士兵证；14-回乡证；15-户口本；16-警官证；17-台胞证；18-组织机构代码；19-营业执照；20-税务登记证；21-统一社会信用代码证；22-港澳通行证；23-法人代表证；24-外国人永久居住证；25-其他'
                        'legal_id_card' => ($StoreBank->bank_sfz_no != "" && $StoreBank->bank_sfz_no != "undefined") ? $StoreBank->bank_sfz_no : $Store->head_sfz_no, //法人证件号明文,必须,身份证时必传掩码密文和哈希，其他场合必传明文
                        'legal_mobile' => $Store->people_phone, //法人手机号,必须
                        'legal_cert_start_date' => $this->huiPayDate($Store->head_sfz_stime), //法人证件开始日期,必须
                        'legal_cert_end_date' => $this->huiPayDate($Store->head_sfz_time), //法人证件结束日期,必须
                        'settlement_bank_id' => $bank_info_hui_pay->bank_id,  //结算卡开户银行id,必须????
                        'settlement_bank_branch' => $StoreBank->bank_name,  //开户银行简称,可选
                        'settlement_card_no' => $StoreBank->store_bank_no,  //结算卡卡号,必须
                        'settlement_name' => $StoreBank->store_bank_name,  //结算卡户名,必须
                        'settlement_card_type' => $StoreBank->store_bank_type == '02' ? '10' : '11',  //结算卡账户类型,必须,'10-对公；11-对私'
                        'settlement_card_province' => $Store->province_code,  //结算卡省份，必须
                        'settlement_card_city' => $Store->city_code,  //结算卡地区，必须
                        'prov_code' => $Store->province_code,  //省份，必须
                        'city_code' => $Store->city_code,  //地区，必须
                        'service_phone' => $Store->people_phone,  //客服电话，必须?????
                        'district_code' => $Store->area_code,  //县，必须11111
                        'apply_id' => $apply_id, //企业开户申请号,必须,自己生成，唯一就行,长度<40
                        'corp_name' => $Store->store_name, //企业名称,必须11111
                        'corp_license_type' => '1', //企业证照类型,必须,1:三证合一 2:普通证照
                        'corp_business_address' => $Store->province_name . $Store->city_name . $Store->area_name . $Store->store_address, //企业经营地址,必须
                        'corp_reg_address' => $Store->province_name . $Store->city_name . $Store->area_name . $Store->store_address, //企业注册地址,必须
                        'corp_fixed_telephone' => $Store->people_phone, //企业固定电话,必须
                        'business_scope' => $Store->category_name, //经营范围,必须
                        'contact_name' => $Store->people, //企业联系人姓名,必须
                        'contact_email' => $Store->store_email, //企业联系人邮箱,必须
                        'contact_mobile' => $Store->people_phone, //联系人手机号,必须,以1开头的11位数字
                        'controlling_shareholder_list' => [  //实际控股人,必须??????
                            'cust_name' => $Store->people,
                            'id_card_type' => '10',
                            'id_card' => ($StoreBank->bank_sfz_no != "" && $StoreBank->bank_sfz_no != "undefined") ? $StoreBank->bank_sfz_no : $Store->head_sfz_no,
                            'ratio' => '100',
                            'shareholder_addr' => $Store->province_name . $Store->city_name . $Store->area_name . $Store->store_address
                        ],
                        'corp_deal_type' => '1',  //商户经营类型,必须,默认1
                        'corp_short_name' => $Store->store_short_name,  //小B商户简称,必须
                        'business_type' => $Store->store_type == '2' ? '3' : '5',  //商户种类,必填,'1-政府机构2-国营企业3-私营企业4-外资企业5-个体工商户7-事业单位'
                        'cup_mcc_code' => $hui_pay_category->unionpay_mcc,  //银联mcc码,必须
                        'agent_stat' => '0',  //是否为代理商，必须，'0-小微商户；1-代理商'
                        'business_time' => '08:00-18:00:00',  //经营时间，必须?????
                        'trade_route' => '',  //路由编号(通道编号),可选????
                        'mcs_lm_type' => $hui_pay_category->first_level_code . $hui_pay_category->second_level_code,  //汇来米的类目code(一级二级类目拼接),可选
                        'settlement_bank_code' => $bank_info_hui_pay->bank_id,  //结算卡银行编号，可选
                        'settlement_card_address' => '',  //结算卡开户地址,可选11111
                        'settlement_credential_phone' => $StoreBank->store_bank_phone,  //结算卡联系人手机号,可选
                        'settlement_card_bank_number' => $StoreBank->bank_no,  //结算卡联行号,可选
                        'settlement_card_bank' => $StoreBank->bank_name,  //结算卡开卡银行,可选
                        'settlement_bank_channel_no' => '',  //银行渠道号,可选？？
                        'industry' => '', //行业,可选,'01-酒店、餐饮类；02-旅游类；03-艺术品交易/收藏类；04-娱乐类；05-建筑业/房地产类；06-拍卖和典当类；07-废品收购类；08-居民服务、快递和其他服务类；09-批发和零售类；10-信息传输、计算机服务业和软件业类；11-预售卡及虚拟物品销售类；12-租赁和商务服务业类；13-慈善、基金会等非盈利组织；14-金融理财类；15-外贸类；16-货币服务；17-林、渔、农、牧业类；18-交通运输，仓储类；19-水利环境和公共设施管理类；20-政府类；21-卫生、社会保障和社会福利类；22-教育业；23-采矿业类；24-制造业类；25-科学研究、技术服务和地质勘查业；26-其他'
                        'reg_channel' => '01',  //入驻渠道，必须(代理商开户可不填)，'01:四方公司'
                        'pay_way' => '0',  //支付通道，必须(代理商开户可不填)，'1-支付宝；2-微信；0-所有'1111
                        'rate_type' => '02',  //费率类型,必须(代理商开户可不填),'00-标准费率全部；01-标准费率线上；02-标准费率线下；03-非盈利费率；04-缴费费率；05-保险费率；06-行业活动费率；07-校园餐饮费率'
                        'ali_pay_category' => $hui_pay_category->alipay_manage_category,  //支付宝经营类目,必须(代理商开户可不填)
                        'we_chat_category' => $hui_pay_category->wechat_code,  //微信经营类目,必须(代理商开户可不填)
                        'cls_id' => $hui_pay_category->alipay_clsld,  //支付宝行业分类编号，必须(代理商开户可不填)
                        'model_type' => '1',  //入驻模式,必须(代理商开户可不填),'0-大商户模式；1-服务商模式'
                        'contact_phone' => $Store->people_phone,  //联系人电话，可选
//                        'agent_id' => '',  //代理商号，可选(代理商开户可不填)
//                        'agent_name' => '',  //代理商名称，可选(代理商开户可不填)
                        'shop_name' => $Store->store_name,  //门店名称，可选(代理商开户可不填)
                        'shop_address' => $Store->province_name . $Store->city_name . $Store->area_name,  //门店地址，可选(代理商开户可不填)
                        'shop_prov_code' => $Store->province_code,  //门店所在省，可选(代理商开户可不填)
                        'shop_city_code' => $Store->city_code,  //门店城市,可选(代理商开户可不填)
                        'shop_district_code' => $Store->area_code,  //门店区县,可选(代理商开户可不填)
                        'shop_contact' => $Store->people,  //门店联系人,可选(代理商开户可不填)
                        'shop_phone' => $Store->people_phone,  //门店电话号，可选(代理商开户可不填)
//                        'contract_sign_date' => '',  //合同签署日期,必须(代理商开户可不填),商户的邀请人必填
//                        'contract_sign_manager' => '',  //合同签署经理,必须(代理商开户可不填),商户的邀请人必填
//                        'contract_effect_date' => '',  //合同有效期,必须(代理商开户可不填),商户的邀请人必填
                    ];

                    //不是三证合一（暂时平台没有这种情况）
//                    if ($sign_data['corp_license_type'] != '1') {
//                        $sign_data['business_code'] = ''; //营业执照注册号,'企业证照类型'为普通证照时必填
//                        $sign_data['institution_code'] = ''; //组织机构代码,'企业证照类型'为普通证照时必填
//                        $sign_data['tax_code'] = ''; //税务登记证,'企业证照类型'为普通证照时必填
//                    }

                    //是否法人（对私必填），可选，'1-非法人；2-法人'
                    if ($StoreBank->store_bank_type == '01') {
                        if ($Store->head_name != $StoreBank->store_bank_name) {
                            $sign_data['un_legal_type'] = '1';
                        } else {
                            $sign_data['un_legal_type'] = '2';
                        }
                    }

                    //是否法人
                    if ($Store->head_name != $StoreBank->store_bank_name) {
                        $sign_data['contact_type'] = '00';  //联系人类型,必须,'01-法人；02-实际控制人；03-代理人；00-其他'
                        $sign_data['legal_true_flag'] = '1'; //是否为非法人,必须,非法人作为法人 填写1;如果是法人，可不传
                        $sign_data['settlement_id_card'] = $StoreBank->bank_sfz_no;  //结算卡人证件号，可选，只支持身份证号(非法人时有)
                    } else {
                        $sign_data['contact_type'] = '01';  //联系人类型,必须,'01-法人；02-实际控制人；03-代理人；00-其他'
                    }

                    //进件渠道，必须，'01-商户APP；02-渠道APP；03-控台进件'
                    $sign_data['corp_channel'] = '03';
                }

                //对私非法人结算-图片
                if ($StoreBank->store_bank_type == "01" && $Store->head_name != $StoreBank->store_bank_name) {
                    $settlePersonIdcardPositiveImg = $StoreImg->bank_sfz_img_a;
                    if ($settlePersonIdcardPositiveImg) {
                        $settlePersonIdcardPositive = $this->hui_pay_upload_img($settlePersonIdcardPositiveImg, $config_id, '结算人身份证正面（人像面）');
                        if ($settlePersonIdcardPositive['status'] != 1) {
                            $send_ways_data['status_desc'] = $settlePersonIdcardPositive['message'];
                            $return = $this->send_ways_data($send_ways_data);
                            return json_encode($return);
                        } else {
                            $settle_person_idcard_positive = [
                                'attach_desc' => '结算人身份证正面（人像面）',
                                'attach_name' => '结算人身份证正面（人像面）',
                                'attach_type' => '07',  //文件类型，必须，'01-营业执照图片；02-税务登记证号；03-组织机构代码证；04-开户许可证；05-法人身份证正面；06-法人身份证反面；07-结算人身份证正面；08-结算人身份证反面；09-商务协议；10-公司照片一；11-公司照片二；12-公司照片三；13-联系人身份证正面；14-联系人身份证反面；15-店铺门头照片；16-店铺收银台照片；17-店内照片；18-结算卡正面；19-结算卡反面；20-主流餐饮平台入驻照片；21-D+1协议照片；22-结算变更说明表；23-授权委托书；99-其他'
                                'file_id' => $settlePersonIdcardPositive['file_id']
                            ];
                            $file_tokens[] = $settle_person_idcard_positive;
                        }
                    } else {
                        return json_encode([
                            'status' => '2',
                            'message' => '结算人身份证正面（人像面）照片必传'
                        ]);
                    }

                    $settlePersonIdcardOppositeImg = $StoreImg->bank_sfz_img_b;
                    if ($settlePersonIdcardOppositeImg) {
                        $settlePersonIdcardOpposite = $this->hui_pay_upload_img($settlePersonIdcardOppositeImg, $config_id, '结算人身份证反面（国徽面）');
                        if ($settlePersonIdcardOpposite['status'] != 1) {
                            $send_ways_data['status_desc'] = $settlePersonIdcardOpposite['message'];
                            $return = $this->send_ways_data($send_ways_data);
                            return json_encode($return);
                        } else {
                            $settle_person_idcard_opposite = [
                                'attach_desc' => '结算人身份证反面（国徽面）',
                                'attach_name' => '结算人身份证反面（国徽面）',
                                'attach_type' => '08',
                                'file_id' => $settlePersonIdcardOpposite['file_id']
                            ];
                            $file_tokens[] = $settle_person_idcard_opposite;
                        }
                    } else {
                        return json_encode([
                            'status' => '2',
                            'message' => '结算人身份证反面（人像面）照片必传'
                        ]);
                    }

                    $letterOfAuthPicImg = $StoreImg->store_auth_bank_img;
                    if ($letterOfAuthPicImg) {
                        $letterOfAuthPic = $this->hui_pay_upload_img($letterOfAuthPicImg, $config_id, '非法人结算授权函');
                        if ($letterOfAuthPic['status'] != 1) {
                            $send_ways_data['status_desc'] = $letterOfAuthPic['message'];
                            $return = $this->send_ways_data($send_ways_data);
                            return json_encode($return);
                        } else {
                            $letter_ofAuth_pic = [
                                'attach_desc' => '非法人结算授权函',
                                'attach_name' => '非法人结算授权函',
                                'attach_type' => '23',
                                'file_id' => $letterOfAuthPic['file_id']
                            ];
                            $file_tokens[] = $letter_ofAuth_pic;
                        }
                    } else {
                        return json_encode([
                            'status' => '2',
                            'message' => '非法人结算授权函照片必传'
                        ]);
                    }
                }

                //对私结算-图片
                if ($StoreBank->store_bank_type == "01") {
                    $bankCardPositivePicImg = $StoreImg->bank_img_a;
                    if ($bankCardPositivePicImg) {
                        $bankCardPositivePic = $this->hui_pay_upload_img($bankCardPositivePicImg, $config_id, '银行卡正面');
                        if ($bankCardPositivePic['status'] != 1) {
                            $send_ways_data['status_desc'] = $bankCardPositivePic['message'];
                            $return = $this->send_ways_data($send_ways_data);
                            return json_encode($return);
                        } else {
                            $bank_card_positive_pic = [
                                'attach_desc' => '银行卡正面',
                                'attach_name' => '银行卡正面',
                                'attach_type' => '18',
                                'file_id' => $bankCardPositivePic['file_id']
                            ];
                            $file_tokens[] = $bank_card_positive_pic;
                        }
                    } else {
                        return json_encode([
                            'status' => '2',
                            'message' => '银行卡正面照片必传'
                        ]);
                    }

                    $bankCardOppositePicImg = $StoreImg->bank_img_b;
                    if ($bankCardOppositePicImg) {
                        $bankCardOppositePic = $this->hui_pay_upload_img($bankCardOppositePicImg, $config_id, '银行卡反面');
                        if ($bankCardOppositePic['status'] != 1) {
                            $send_ways_data['status_desc'] = $bankCardOppositePic['message'];
                            $return = $this->send_ways_data($send_ways_data);
                            return json_encode($return);
                        } else {
                            $bank_card_opposite_pic = [
                                'attach_desc' => '银行卡反面',
                                'attach_name' => '银行卡反面',
                                'attach_type' => '19',
                                'file_id' => $bankCardOppositePic['file_id']
                            ];
                            $file_tokens[] = $bank_card_opposite_pic;
                        }
                    } else {
                        return json_encode([
                            'status' => '2',
                            'message' => '银行卡正面照片必传'
                        ]);
                    }
                } else {
                    //对公结算
                    //开户许可证
                    $IndustryLicensePhotoImg = $StoreImg->store_industrylicense_img;
                    if ($IndustryLicensePhotoImg) {
                        $IndustryLicensePhoto = $this->hui_pay_upload_img($IndustryLicensePhotoImg, $config_id, '开户许可证');
                        if ($IndustryLicensePhoto['status'] != 1) {
                            $send_ways_data['status_desc'] = $IndustryLicensePhoto['message'];
                            $return = $this->send_ways_data($send_ways_data);
                            return json_encode($return);
                        } else {
                            $industry_license_photo = [
                                'attach_desc' => '开户许可证',
                                'attach_name' => '开户许可证',
                                'attach_type' => '04',
                                'file_id' => $IndustryLicensePhoto['file_id']
                            ];
                            $file_tokens[] = $industry_license_photo;
                        }
                    } else {
                        return json_encode([
                            'status' => '2',
                            'message' => '开户许可证必传'
                        ]);
                    }
                }

                $sign_data['file_tokens'] = $file_tokens;  //图片上传信息

                $post_data = [
                    'data' => $sign_data,
                    'sign_type' => 'RSA2',
                    'mer_cust_id' => $mer_cust_id,
                    'source_num' => $source_num
                ];

//                $data = $hui_pay_base_obj->unset_arr($sign_data);
//                ksort($data);
//                $new_data = str_replace("\\/", "/", json_encode($data, JSON_UNESCAPED_UNICODE));
//                var_dump($new_data);die;
//                $post_data['sign'] = $hui_pay_base_obj->rsa_sign($sign_data, $hui_pay_config->private_key, 'RSA2');
//                $post_data['data'] = str_replace('\\', '', json_encode($post_data['data'], JSON_UNESCAPED_UNICODE));
//                $datajsonstr = str_replace("\\/", "/", json_encode($post_data, JSON_UNESCAPED_UNICODE));
//                print_r($datajsonstr);die;

                //商户是否第一次进件
                if (!empty($is_apply_id) && isset($is_apply_id)) {
                    //进件驳回修改
                    $re = $hui_pay_base_obj->execute($hui_pay_base_obj->modify_mer_apply, $post_data, $sign_data, $private_key, $public_key);
//                    var_dump($re);die;
                    if ($re['status'] === '0') {
                        return json_encode([
                            'status' => '3',
                            'status_desc' => '进件驳回验签错误'
                        ]);
                    }

                    if ($re['status'] != '000000') {
                        $status = 3;
                        $status_desc = $re['message'];
                    } else {
                        $status = 2;
                        $status_desc = "审核中";
                    }
                } else {
                    $re = $hui_pay_base_obj->execute($hui_pay_base_obj->mer_apply_submit, $post_data, $sign_data, $private_key, $public_key);
//                    var_dump($re);die;
                    if ($re['status'] === '0') {
                        return json_encode([
                            'status' => '3',
                            'status_desc' => '进件验签错误'
                        ]);
                    }
                    if ($re['status'] != '000000') {
                        $status = 3;
                        $status_desc = $re['message'];
                    } else {
                        //进件成功
//                        $result_data = json_decode($re['data'], true);
//                        $user_cust_id = $result_data['userCustId']; //用户客户号

                        $status = 2;
                        $status_desc = "审核中";
                        $huiPayStoreData = [
                            'apply_id' => $apply_id,
//                            'user_cust_id' => $user_cust_id,  //暂时进件不返回
                            'status' => '2',  //状态，1-开通成功；2-审核中；3-驳回
                            'config_id' => $config_id,
                            'store_id' => $store_id
                        ];

                        try {
                            //成功后，门店表存入申请id
                            DB::beginTransaction();  //开启事务
                            $HuiPayStore = HuiPayStore::where('store_id', $store_id)->first();
                            if ($HuiPayStore) {
                                $HuiPayStore->update($huiPayStoreData);
                                $HuiPayStore->save();
                            } else {
                                HuiPayStore::create($huiPayStoreData);
                            }
                            $Store->update(['apply_id' => $apply_id]);
                            DB::commit(); //提交事务
                        } catch (\Exception $ex) {
                            Log::info('汇付商户进件');
                            Log::info($ex);
                            DB::rollBack();  //事务回滚
                        }

                    }
                }

                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $status;
                $data['status_desc'] = $status_desc;
                $data['company'] = 'huipay';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //随行付a
            if (18999 < $type && $type < 19999) {
                $obj = new \App\Api\Controllers\Vbill\BaseController();
                $config = new VbillConfigController();
                $vbill_config = $config->vbilla_config($config_id);
                if (!$vbill_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付配置不存在请检查配置'
                    ]);
                }

                $privateKey = $vbill_config->privateKey; //
                $sxfpublic = $vbill_config->sxfpublic; //
                $orgId = $vbill_config->orgId; //
                $upload_request_url = $obj->upload_url;
                //默认失败
                $send_ways_data['rate'] = $rate;
                $send_ways_data['store_id'] = $store_id;
                $send_ways_data['status'] = 3;
                $send_ways_data['company'] = 'vbilla';

                //商户资料
                $CertPhotoA = $this->images_get($StoreImg->head_sfz_img_a);
                $CertPhotoB = $this->images_get($StoreImg->head_sfz_img_b);
                $ShopPhoto = $this->images_get($StoreImg->store_logo_img);
                $LicensePhoto = $this->images_get($StoreImg->store_license_img);
                $CheckstandPhoto = $this->images_get($StoreImg->store_img_a); //收银台
                $ShopEntrancePhoto = $this->images_get($StoreImg->store_img_b); //门店内景

                $CertPhotoA = $this->vbill_uploadIMG($upload_request_url, '02', $CertPhotoA, $config_id, '法人身份证正面');
                if ($CertPhotoA['status'] == 0) {
                    $send_ways_data['status_desc'] = $CertPhotoA['message'];
                    $return = $this->send_ways_data($send_ways_data);
                    return json_encode($return);
                }

                $CertPhotoB = $this->vbill_uploadIMG($upload_request_url, '03', $CertPhotoB, $config_id, '法人身份证反面');
                if ($CertPhotoB['status'] == 0) {
                    $send_ways_data['status_desc'] = $CertPhotoB['message'];
                    $return = $this->send_ways_data($send_ways_data);
                    return json_encode($return);
                }
                $ShopPhoto = $this->vbill_uploadIMG($upload_request_url, '10', $ShopPhoto, $config_id, '门头照片');
                if ($ShopPhoto['status'] == 0) {
                    $send_ways_data['status_desc'] = $ShopPhoto['message'];
                    $return = $this->send_ways_data($send_ways_data);
                    return json_encode($return);
                }

                $CheckstandPhoto = $this->vbill_uploadIMG($upload_request_url, '12', $CheckstandPhoto, $config_id, '收银台');
                if ($CheckstandPhoto['status'] == 0) {
                    $send_ways_data['status_desc'] = $CheckstandPhoto['message'];
                    $return = $this->send_ways_data($send_ways_data);
                    return json_encode($return);
                }
                $ShopEntrancePhoto = $this->vbill_uploadIMG($upload_request_url, '11', $ShopEntrancePhoto, $config_id, '内景照片');
                if ($ShopEntrancePhoto['status'] == 0) {
                    $send_ways_data['status_desc'] = $ShopEntrancePhoto['message'];
                    $return = $this->send_ways_data($send_ways_data);
                    return json_encode($return);
                }

                $reqData = [
                    'mecDisNm' => $store_short_name,
                    'mblNo' => $Store->people_phone,
                    'operationalType' => '01', //取值范围：01 线下 02 线上 03 非盈利类 04缴费类 05 保险类 06 私立院校
                    'mecTypeFlag' => '00',
                    'qrcodeList' => [
                        [
                            'rateType' => '01',
                            'rate' => $rate,
                        ],
                        [
                            'rateType' => '02',
                            'rate' => $rate,
                        ],
                        [
                            'rateType' => '06',
                            'rate' => $rate_a,
                        ],
                        [
                            'rateType' => '07',
                            'rate' => $rate_c,
                        ]
                    ],
                    'settleType' => '04',
                    'cprRegAddr' => $Store->store_address,
                    'regProvCd' => $Store->province_code . '000000',
                    'regCityCd' => $Store->city_code . '000000',
                    'regDistCd' => $Store->area_code . '000000',
                    'mccCd' => $MyBankCategory->mcc, //类目
                    'csTelNo' => $Store->people_phone,
                    'identityName' => $Store->head_name,
                    'identityTyp' => '00',
                    'identityNo' => $Store->head_sfz_no,
                    'legalPersonLicStt' => $Store->head_sfz_stime ? date('Ymd', strtotime($Store->head_sfz_stime)) : "",
                    'legalPersonLicEnt' => ($Store->head_sfz_time == '长期') ? '29991231' : date('Ymd', strtotime($Store->head_sfz_time)),
                    'actNm' => $StoreBank->store_bank_name,
                    'actTyp' => $StoreBank->store_bank_type == "02" ? "00" : "01", //00 对公 01 对私
                    'actNo' => $StoreBank->store_bank_no,
                    'lbnkNo' => $StoreBank->bank_no,
                    'lbnkNm' => $StoreBank->sub_bank_name,
                    'legalPersonidPositivePic' => $CertPhotoA['PhotoUrl'],
                    'legalPersonidOppositePic' => $CertPhotoB['PhotoUrl'],
                    'storePic' => $ShopPhoto['PhotoUrl'],
                    'insideScenePic' => $ShopEntrancePhoto['PhotoUrl']
                ];

                $reqData_img = [
                    'legalPersonidPositivePic' => $CertPhotoA['PhotoUrl'],
                    'legalPersonidOppositePic' => $CertPhotoB['PhotoUrl'],
                    'storePic' => $ShopPhoto['PhotoUrl'],
                    'insideScenePic' => $ShopEntrancePhoto['PhotoUrl'],
                ];

                //非法人结算
                if ($StoreBank->store_bank_type == "01" && $Store->head_name != $StoreBank->store_bank_name) {
                    $settlePersonIdcardPositive = $this->images_get($StoreImg->bank_sfz_img_a);
                    $settlePersonIdcardOpposite = $this->images_get($StoreImg->bank_sfz_img_b);
                    $letterOfAuthPic = $this->images_get($StoreImg->store_auth_bank_img);

                    $settlePersonIdcardPositive = $this->vbill_uploadIMG($upload_request_url, '08', $settlePersonIdcardPositive, $config_id, '结算人身份证正面（人像面）');
                    if ($settlePersonIdcardPositive['status'] == 0) {
                        $send_ways_data['status_desc'] = $settlePersonIdcardPositive['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }

                    $settlePersonIdcardOpposite = $this->vbill_uploadIMG($upload_request_url, '07', $settlePersonIdcardOpposite, $config_id, '结算人身份证反面（国徽面）');
                    if ($settlePersonIdcardOpposite['status'] == 0) {
                        $send_ways_data['status_desc'] = $settlePersonIdcardOpposite['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }

                    $letterOfAuthPic = $this->vbill_uploadIMG($upload_request_url, '26', $letterOfAuthPic, $config_id, '非法人结算授权函');
                    if ($letterOfAuthPic['status'] == 0) {
                        $send_ways_data['status_desc'] = $letterOfAuthPic['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }

                    $reqData['settlePersonIdcardPositive'] = $settlePersonIdcardPositive['PhotoUrl'];
                    $reqData['settlePersonIdcardOpposite'] = $settlePersonIdcardOpposite['PhotoUrl'];
                    $reqData['letterOfAuthPic'] = $letterOfAuthPic['PhotoUrl'];

                    $reqData_img['settlePersonIdcardPositive'] = $settlePersonIdcardPositive['PhotoUrl'];
                    $reqData_img['settlePersonIdcardOpposite'] = $settlePersonIdcardOpposite['PhotoUrl'];
                    $reqData_img['letterOfAuthPic'] = $letterOfAuthPic['PhotoUrl'];
                }

                //对私结算
                if ($StoreBank->store_bank_type == "01") {
                    if ($store_type == 3) {
                        $reqData['stmManIdNo'] = $Store->head_sfz_no;
                    } else {
                        $reqData['stmManIdNo'] = $StoreBank->bank_sfz_no != "" && $StoreBank->bank_sfz_no != "undefined" ? $StoreBank->bank_sfz_no : $Store->head_sfz_no;
                    }

                    $reqData['accountLicStt'] = $Store->bank_sfz_stime ? date('Ymd', strtotime($Store->bank_sfz_stime)) : date('Ymd', strtotime($Store->head_sfz_stime));
                    $reqData['accountLicEnt'] = $Store->head_sfz_time == '长期' ? '29991231' : date('Ymd', strtotime($Store->head_sfz_time));

                    $bankCardPositivePic = $this->images_get($StoreImg->bank_img_a);//收银台
                    $bankCardOppositePic = $this->images_get($StoreImg->bank_img_b);//门店内景

                    $bankCardPositivePic = $this->vbill_uploadIMG($upload_request_url, '05', $bankCardPositivePic, $config_id, '银行卡正面');
                    if ($bankCardPositivePic['status'] == 0) {
                        $send_ways_data['status_desc'] = $bankCardPositivePic['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }

                    $bankCardOppositePic = $this->vbill_uploadIMG($upload_request_url, '06', $bankCardOppositePic, $config_id, '银行卡反面');
                    if ($bankCardOppositePic['status'] == 0) {
                        $send_ways_data['status_desc'] = $bankCardOppositePic['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }

                    $reqData['bankCardPositivePic'] = $bankCardPositivePic['PhotoUrl'];
                    $reqData['bankCardOppositePic'] = $bankCardOppositePic['PhotoUrl'];
                    $reqData_img['bankCardPositivePic'] = $bankCardPositivePic['PhotoUrl'];
                    $reqData_img['bankCardOppositePic'] = $bankCardOppositePic['PhotoUrl'];
                } else {
                    //对公结算

                    //开户许可证
                    $IndustryLicensePhoto = $this->images_get($StoreImg->store_industrylicense_img);
                    $IndustryLicensePhoto = $this->vbill_uploadIMG($upload_request_url, '04', $IndustryLicensePhoto, $config_id, '开户许可证');
                    if ($IndustryLicensePhoto['status'] == 0) {
                        $send_ways_data['status_desc'] = $IndustryLicensePhoto['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }
                    //开户许可证
                    $reqData['openingAccountLicensePic'] = $IndustryLicensePhoto['PhotoUrl'];
                    $reqData_img['openingAccountLicensePic'] = $IndustryLicensePhoto['PhotoUrl'];
                }

                //个人
                $haveLicenseNo = "01";
                $reqData['haveLicenseNo'] = $haveLicenseNo; //01 自然人 02 个体户 03 企业

                //企业
                if ($store_type == 2) {
                    $reqData['haveLicenseNo'] = "03";
                    $reqData['cprRegNmCn'] = $store_name;
                    $reqData['registCode'] = $Store->store_license_no;
                    $reqData['licenseMatch'] = '00';
                    $reqData['businessLicStt'] = $Store->store_license_stime ? date('Ymd', strtotime($Store->store_license_stime)) : "";
                    $reqData['businessLicEnt'] = ($Store->store_license_time == '长期') ? '29991231' : date('Ymd', strtotime($Store->store_license_time));

                    //资料
                    $LicensePhoto = $this->vbill_uploadIMG($upload_request_url, '13', $LicensePhoto, $config_id, '营业执照照片');
                    if ($LicensePhoto['status'] == 0) {
                        $send_ways_data['status_desc'] = $LicensePhoto['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }

                    //营业执照
                    $reqData['licensePic'] = $LicensePhoto['PhotoUrl'];
                    $reqData_img['licensePic'] = $LicensePhoto['PhotoUrl'];
                }

                //个体
                if ($store_type == 1) {
                    $reqData['haveLicenseNo'] = "02";
                    $reqData['cprRegNmCn'] = $store_name;
                    $reqData['registCode'] = $Store->store_license_no;
                    $reqData['businessLicStt'] = $Store->store_license_stime ? date('Ymd', strtotime($Store->store_license_stime)) : "";
                    $reqData['businessLicEnt'] = ($Store->store_license_time == '长期') ? '29991231' : date('Ymd', strtotime($Store->store_license_time));

                    //资料
                    $LicensePhoto = $this->vbill_uploadIMG($upload_request_url, '13', $LicensePhoto, $config_id, '营业执照照片');
                    if ($LicensePhoto['status'] == 0) {
                        $send_ways_data['status_desc'] = $LicensePhoto['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }

                    //营业执照
                    $reqData['licensePic'] = $LicensePhoto['PhotoUrl'];
                    $reqData_img['licensePic'] = $LicensePhoto['PhotoUrl'];
                }

                $url = $obj->open_store_url;
                $VbillStore = VbillaStore::where('store_id', $store_id)
                    ->first();
                if ($VbillStore && $VbillStore->taskStatus > 1) {
                    $reqData['mno'] = $VbillStore->mno;
                    $url = $obj->open_update_store_url;
                    //入驻图片驳回
                    if ($VbillStore->taskStatus == 3) {
                        $reqData = $reqData_img;
                        $reqData['mno'] = $VbillStore->mno;
                    }
                }

                $data = [
                    'orgId' => $orgId,
                    'reqId' => time(),
                    'version' => '1.0',
                    'timestamp' => date('Ymdhis', time()),
                    'signType' => 'RSA',
                    'reqData' => $reqData,
                ];

                $data = array_filter($data, function ($v) {
                    if ($v == "") {
                        return false;
                    } else {
                        return true;
                    }
                });
                $re = $obj->execute($data, $url, $privateKey, $sxfpublic);
                if ($re['status'] == 0) {
                    return json_encode([
                        'status' => 2,
                        'message' => $re['message'],
                    ]);
                }
                if ($re['data']['code'] == "0000" && $re['data']['respData']['bizCode'] == "0000") {
                    $status_desc = "审核中";
                    $status = 2;

                    $data_in = [
                        'config_id' => $config_id,
                        'mno' => $re['data']['respData']['mno'],
                        'store_id' => $store_id,
                        'applicationId' => $re['data']['respData']['applicationId'],
                    ];
                    if ($VbillStore) {
                        $VbillStore->update($data_in);
                        $VbillStore->save();
                    } else {
                        VbillaStore::create($data_in);
                    }
                } else {
                    $status_desc = $re['data']['respData']['bizMsg'] ?? '审核失败';
                    $status = 3;
                }

                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $status;
                $data['status_desc'] = $status_desc;
                $data['company'] = 'vbilla';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //哆啦宝
            if (14999 < $type && $type < 15999) {
                $manager = new ManageController();
                $dlb_config = $manager->pay_config($config_id);
                if (!$dlb_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '哆啦宝配置配置不存在请检查配置'
                    ]);
                }

                //默认失败
                $send_ways_data['rate'] = $rate;
                $send_ways_data['store_id'] = $store_id;
                $send_ways_data['status'] = 3;
                $send_ways_data['company'] = 'dlb';

                $timestamp = time();
                $access_key = $dlb_config->access_key;
                $secret_key = $dlb_config->secret_key;
                unset($dlb_config->access_key);
                unset($dlb_config->secret_key);

                //经营性质 1-个体，2-企业，3-个人
                $customerType = "NONE";
                if ($store_type == 1) {
                    $customerType = "INDIVIDUALBISS";
                }
                if ($store_type == 2) {
                    $customerType = "COMPANY";
                }
                if ($store_type == 3) {
                    $customerType = "PERSON";
                }

                $add_store_data = [
                    "agentNum" => $dlb_config->agent_num, //代理商编号
                    "fullName" => $store_name, //哆啦宝商户全称
                    "shortName" => $store_short_name, //哆啦宝商户简称
                    "industry" => $Store->dlb_industry, //商户所属行业,(一级行业)???
                    "province" => $Store->dlb_province, //商户所属省份???
                    "city" => $Store->dlb_city, //商户所属城市???
                    "linkMan" => $Store->people, //商户联系人
                    "linkPhone" => $Store->people_phone, //商户联系电话
                    "customerType" => $customerType, //商户类型,COMPANY(公司企业)/PERSON(个人)/INDIVIDUALBISS(个体工商户)/INSTITUTION(事业单位)
                    'certificateType' => 'IDENTIFICATION', //证件类型,IDENTIFICATION(身份证)/PASSPORT(护照)
                    'certificateCode' => $Store->head_sfz_no, //证件编号
                    "district" => $Store->area_name, //地区
                    "certificateName" => $Store->head_name, //证件人姓名
                    "certificateStartDate" => date('Y-m-d', strtotime($Store->head_sfz_stime)), //法人证件开始日期
                    "certificateEndDate" => date('Y-m-d', strtotime($Store->head_sfz_time)), //法人证件结束日期
                    "contactPhoneNum" => $Store->people_phone, //联系人手机号-认证使用
                    "linkManId" => $Store->head_sfz_no, //联系人身份证号-认证使用
                ];

                //营业执照注册地址（个人可不传）
                if ($store_type != 3) {
                    $add_store_data['postalAddress'] = $Store->store_address;
                }

                $mch_num = '';
                $dlb_store = DlbStore::where('store_id', $store_pid)
                    ->first();
                if (!$dlb_store) {
                    //新增商户信息
                    $add_store_token = $manager->dlb_sign($timestamp, $manager->dlb_open_store_path, $add_store_data, $secret_key);
                    $add_store_header = array(
                        "Content-Type" => "application/json",
                        "accessKey" => $access_key,
                        "timestamp" => $timestamp,
                        "token" => $add_store_token,
                    );

                    $add_store_result = $manager->post_func($manager->dlb_host_url . $manager->dlb_open_store_path, $add_store_data, $add_store_header);
//                    Log::info('哆啦宝进件，新增商户结果：');
//                    Log::info($add_store_result);

                    //新增商户成功，新增哆啦宝商户表
                    if ($add_store_result['status'] == '1') {
                        $add_store_return = json_decode($add_store_result['data'], true);
                        if ($add_store_return['result'] == 'success') {
                            $mch_num = $add_store_return['data']['customerNum'];
                            $dlb_store_insert = [
                                'config_id' => $Store->config_id,
                                'store_id' => $Store->store_id,
                                'mch_num' => $mch_num, //商户编号
                                'status' => 1,
                            ];
                            DlbStore::create($dlb_store_insert);
                        } else {
//                            Log::info('哆啦宝进件，新增商户结果22：');
//                            Log::info($add_store_return);
                            return json_encode([
                                'status' => 2,
                                'message' => $add_store_return['error']['errorMsg'],
                            ]);
                        }
                    } else {
//                        Log::info('哆啦宝进件，新增商户结果33：');
//                        Log::info($add_store_result['message']);
                        return json_encode([
                            'status' => 2,
                            'message' => $add_store_result['error']['errorMsg'],
                        ]);
                    }
                } else {
                    //修改商户信息
                    $modify_store_token = $manager->dlb_sign($timestamp, $manager->dlb_moddify_customerinfo, $add_store_data, $secret_key);
                    $modify_store_header = array(
                        "Content-Type" => "application/json",
                        "accessKey" => $access_key,
                        "timestamp" => $timestamp,
                        "token" => $modify_store_token,
                    );
                    $modify_store_result = $manager->post_func($manager->dlb_host_url . $manager->dlb_moddify_customerinfo, $add_store_data, $modify_store_header);
//                    Log::info('哆啦宝进件，修改商户结果：');
//                    Log::info($modify_store_result);

                    //新增商户成功，新增哆啦宝商户表
                    if ($modify_store_result['status'] == '1') {
                        $modify_store_return = json_decode($modify_store_result['data'], true);
                        if ($modify_store_return['result'] == 'success') {
                            $mch_num = $modify_store_return['data']['customerNum'];
                            $dlb_store->update(['mch_num' => $mch_num]);
                            $dlb_store->save();
                        } else {
//                            Log::info('哆啦宝进件，修改商户结果22：');
//                            Log::info($modify_store_return);
                            return json_encode([
                                'status' => 2,
                                'message' => $modify_store_return,
                            ]);
                        }
                    } else {
//                        Log::info('哆啦宝进件，修改商户结果33：');
//                        Log::info($modify_store_result['message']);
                        return json_encode([
                            'status' => 2,
                            'message' => $modify_store_result['message'],
                        ]);
                    }
                }

                $dlb_pay_bank_list = $Store->dlb_pay_bank_list; //支付类型+费率
                if ($dlb_pay_bank_list && $dlb_pay_bank_list != '[]') {
                    $dlb_pay_bank_list = json_decode($dlb_pay_bank_list, true);
                    $new_dlb_pay_bank_list = [];
                    foreach ($dlb_pay_bank_list as $key => $value) {
                        $new_dlb_pay_bank_list[$key]['num'] = $value['num']; //支付类型编号,注意：京东支付只需传递银联编号，默认会开通银联和京东支付。无需单独传递京东编号???
                        $new_dlb_pay_bank_list[$key]['rate'] = $value['rate']; //费率???
                    }
                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => '哆啦宝进件-支付类型编号和费率未设置'
                    ]);
                }
                //结算信息
                if ($mch_num) {
                    $settle_data = [
                        'customerNum' => $mch_num, //商户编号
                        'bankAccountName' => $StoreBank->bank_namev, //银行账户名称
                        'bankAccountNum' => $StoreBank->bank_no, //银行账户编号
                        'province' => $Store->dlb_province, //商户所属省份???
                        'city' => $Store->dlb_city, //商户所属城市???
                        'bankBranchName' => $Store->dlb_sub_bank, //银行分行名称???
                        'bankName' => $Store->dlb_bank, //银行名称???
                        'settleAmount' => '1', //结算金额，起结金额必须大于等于1
                        'payBankList' => $new_dlb_pay_bank_list,
                        'accountType' => ($StoreBank->store_bank_type == "01") ? 'PRIVATE' : 'PUBLIC', //账户类型,PUBLIC(对公)/PRIVATE(对私)，如果商户身份证和结算人身份证号一致为对私法人结算，如果不一致为对私非法人结算。对私法人结算和对私非法人结算所要求传递的附件资料不一致
                        'phone' => $Store->people_phone, //银行预留手机号
                        'settlerCertificateCode' => $StoreBank->bank_sfz_no, //结算人身份证号
                        'settlerCertificateStartDate' => $StoreBank->bank_sfz_stime, //结算人身份证开始时间
                        'settlerCertificateEndDate' => $StoreBank->bank_sfz_time, //结算人身份证结束时间
                    ];

                    if ($dlb_store->settle_num) {
                        //修改结算信息
                        $modify_settle_token = $manager->dlb_sign($timestamp, $manager->dlb_modify_settleinfo, $settle_data, $secret_key);
                        $modify_settle_header = array(
                            "Content-Type" => "application/json",
                            "accessKey" => $access_key,
                            "timestamp" => $timestamp,
                            "token" => $modify_settle_token,
                        );
                        $modify_settle_result = $manager->post_func($manager->dlb_host_url . $manager->dlb_modify_settleinfo, $settle_data, $modify_settle_header);
//                        Log::info('哆啦宝进件，修改结算信息结果：');
//                        Log::info($modify_settle_result);

                        if ($modify_settle_result['status'] == '1') {
                            //更新结算编码
                            $modify_settle_return = json_decode($modify_settle_result['data'], true);
                            if ($modify_settle_return['result'] == 'success') {
                                $settle_num = $modify_settle_return['data']['settleNum'];
                                $dlb_store->update(['settle_num' => $settle_num]);
                                $dlb_store->save();
                            } else {
//                                Log::info('哆啦宝进件，修改结算信息22：');
//                                Log::info($modify_settle_return);
                                //TODO:返回错误码信息
                            }
                        } else {
//                            Log::info('哆啦宝进件，修改结算信息33：');
//                            Log::info($modify_settle_result);
                        }
                    } else {
                        //设置结算信息
                        $settle_token = $manager->dlb_sign($timestamp, $manager->dlb_settleinfo, $settle_data, $secret_key);
                        $settle_header = array(
                            "Content-Type" => "application/json",
                            "accessKey" => $access_key,
                            "timestamp" => $timestamp,
                            "token" => $settle_token,
                        );
                        $settle_result = $manager->post_func($manager->dlb_host_url . $manager->dlb_settleinfo, $settle_data, $settle_header);
//                        Log::info('哆啦宝进件，设置结算信息结果：');
//                        Log::info($settle_result);

                        if ($settle_result['status'] == '1') {
                            //更新结算编码
                            $settle_return = json_decode($settle_result['data'], true);
                            if ($settle_return['result'] == 'success') {
                                $settle_num = $settle_return['data']['settleNum'];
                                $dlb_store->update(['settle_num' => $settle_num]);
                                $dlb_store->save();
                            } else {
//                                Log::info('哆啦宝进件，设置结算信息22：');
//                                Log::info($settle_return);
                                //TODO:返回错误码信息
                            }
                        } else {
//                            Log::info('哆啦宝进件，设置结算信息33：');
//                            Log::info($settle_result);
                        }
                    }

                    //店铺信息
                    $shopinfo_data = [
                        'agentNum' => $dlb_config->agent_num, //代理商编号
                        'customerNum' => $mch_num, //商户编号
                        'shopName' => $Store->store_short_name, //店铺名称
                        'address' => $Store->store_address, //店铺地址
                        'oneIndustry' => $Store->dlb_industry, //店铺一级行业???
                        'twoIndustry' => $Store->dlb_second_industry, //店铺二级行业???
                        'mobilePhone' => $Store->people_phone, //店铺联系人手机号
                        'mapLng' => $Store->lng, //经度
                        'mapLat' => $Store->lat, //纬度
                    ];
                    //个人
                    if ($Store->store_type == '3') {
                        $shopinfo_data['microBizType'] = $Store->dlb_micro_biz_type; //经营类型 (个人商户必填)??? MICRO_TYPE_STORE -门店场所, MICRO_TYPE_MOBILE -流动经营/便民服务, MICRO_TYPE_ONLINE -线上商品/服务交易
                    }
                    if ($dlb_store->shop_num) {
                        //修改店铺信息
                        $modify_shopinfo_token = $manager->dlb_sign($timestamp, $manager->dlb_modify_shopinfo, $shopinfo_data, $secret_key);
                        $modify_shopinfo_header = array(
                            "Content-Type" => "application/json",
                            "accessKey" => $access_key,
                            "timestamp" => $timestamp,
                            "token" => $modify_shopinfo_token,
                        );
                        $modify_shopinfo_result = $manager->post_func($manager->dlb_host_url . $manager->dlb_modify_shopinfo, $shopinfo_data, $modify_shopinfo_header);
//                        Log::info('哆啦宝进件，修改店铺信息结果：');
//                        Log::info($modify_shopinfo_result);

                        if ($modify_shopinfo_result['status'] == '1') {
                            //更新结算编码
                            $modify_shopinfo_return = json_decode($modify_shopinfo_result['data'], true);
                            if ($modify_shopinfo_return['result'] == 'success') {
                                $shop_num = $modify_shopinfo_return['data']['shopNum'];
                                $dlb_store->update(['shop_num' => $shop_num]);
                                $dlb_store->save();
                            } else {
//                                Log::info('哆啦宝进件，修改店铺信息22：');
//                                Log::info($modify_shopinfo_return);
                                //TODO:返回错误码信息
                            }
                        } else {
//                            Log::info('哆啦宝进件，修改店铺信息33：');
//                            Log::info($modify_shopinfo_result);
                        }
                    } else {
                        //设置店铺信息
                        $modify_token = $manager->dlb_sign($timestamp, $manager->dlb_shopinfo, $shopinfo_data, $secret_key);
                        $modify_header = array(
                            "Content-Type" => "application/json",
                            "accessKey" => $access_key,
                            "timestamp" => $timestamp,
                            "token" => $modify_token,
                        );
                        $shopinfo_result = $manager->post_func($manager->dlb_host_url . $manager->dlb_shopinfo, $shopinfo_data, $modify_header);
//                        Log::info('哆啦宝进件，修改店铺信息结果：');
//                        Log::info($shopinfo_result);

                        if ($shopinfo_result['status'] == '1') {
                            //更新结算编码
                            $modify_shopinfo_return = json_decode($shopinfo_result['data'], true);
                            if ($modify_shopinfo_return['result'] == 'success') {
                                $shop_num = $modify_shopinfo_return['data']['shopNum'];
                                $dlb_store->update(['shop_num' => $shop_num]);
                                $dlb_store->save();
                            } else {
//                                Log::info('哆啦宝进件，修改店铺信息22：');
//                                Log::info($modify_shopinfo_return);
                                //TODO:返回错误码信息
                            }
                        } else {
//                            Log::info('哆啦宝进件，修改店铺信息33：');
//                            Log::info($shopinfo_result);
                        }
                    }

                    //公共必传图片
                    $cashier_photo = $StoreImg->store_img_a; //收银台 CASHIERDESK
                    $shop_inside_photo = $StoreImg->store_img_b; //门店内景 SHOP
                    $shop_door_photo = $StoreImg->store_logo_img; //门头照片 IDENTITYDOOR
                    $cert_photo_a = $StoreImg->head_sfz_img_a; //法人身份证正面 IDENTITYFRONT
                    $cert_photo_b = $StoreImg->head_sfz_img_b; //法人身份证反面 IDENTITYOPPOSITE

                    $license_photo = $StoreImg->store_license_img; //营业执照 LICENCE

                    //收银台
                    if ($cashier_photo) {
                        $attach_type = 'CASHIERDESK';
                        $upload_file_re = $this->dlb_upload_img($cashier_photo, $attach_type, $mch_num, $config_id);
                        if ($upload_file_re['status'] == '1') {
                            //附件信息存库备用
                            $cashierdesk_data = [
                                'config_id' => $config_id,
                                'store_id' => $store_id,
                                'mch_num' => $mch_num,
                                'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                'attach_name' => $upload_file_re['data']['fileName'],
                                'attach_type' => $attach_type,
                                'attach_url' => $upload_file_re['data']['url'],
                            ];
                            DlbStoreUpload::created($cashierdesk_data);
                        } else {
                            $send_ways_data['status_desc'] = $upload_file_re['message'];
                            $return = $this->send_ways_data($send_ways_data);
                            return json_encode($return);
                        }
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '哆啦宝进件-收银台照片必须上传，请重试'
                        ]);
                    }

                    //门店内景
                    if ($shop_inside_photo) {
                        $attach_type = 'SHOP';
                        $upload_file_re = $this->dlb_upload_img($shop_inside_photo, $attach_type, $mch_num, $config_id);
                        if ($upload_file_re['status'] == '1') {
                            //附件信息存库备用
                            $shop_data = [
                                'config_id' => $config_id,
                                'store_id' => $store_id,
                                'mch_num' => $mch_num,
                                'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                'attach_name' => $upload_file_re['data']['fileName'],
                                'attach_type' => $attach_type,
                                'attach_url' => $upload_file_re['data']['url'],
                            ];
                            DlbStoreUpload::created($shop_data);
                        } else {
                            $send_ways_data['status_desc'] = $upload_file_re['message'];
                            $return = $this->send_ways_data($send_ways_data);
                            return json_encode($return);
                        }
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '哆啦宝进件-门店内景照片必须上传，请重试'
                        ]);
                    }

                    //门头照片
                    if ($shop_door_photo) {
                        $attach_type = 'IDENTITYDOOR';
                        $upload_file_re = $this->dlb_upload_img($shop_door_photo, $attach_type, $mch_num, $config_id);
                        if ($upload_file_re['status'] == '1') {
                            //附件信息存库备用
                            $identitydoor_data = [
                                'config_id' => $config_id,
                                'store_id' => $store_id,
                                'mch_num' => $mch_num,
                                'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                'attach_name' => $upload_file_re['data']['fileName'],
                                'attach_type' => $attach_type,
                                'attach_url' => $upload_file_re['data']['url'],
                            ];
                            DlbStoreUpload::created($identitydoor_data);
                        } else {
                            $send_ways_data['status_desc'] = $upload_file_re['message'];
                            $return = $this->send_ways_data($send_ways_data);
                            return json_encode($return);
                        }
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '哆啦宝进件-门店门头照片必须上传，请重试'
                        ]);
                    }

                    //法人身份证正面
                    if ($cert_photo_a) {
                        $attach_type = 'IDENTITYFRONT';
                        $upload_file_re = $this->dlb_upload_img($cert_photo_a, $attach_type, $mch_num, $config_id);
                        if ($upload_file_re['status'] == '1') {
                            //附件信息存库备用
                            $identityfront_data = [
                                'config_id' => $config_id,
                                'store_id' => $store_id,
                                'mch_num' => $mch_num,
                                'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                'attach_name' => $upload_file_re['data']['fileName'],
                                'attach_type' => $attach_type,
                                'attach_url' => $upload_file_re['data']['url'],
                            ];
                            DlbStoreUpload::created($identityfront_data);
                        } else {
                            $send_ways_data['status_desc'] = $upload_file_re['message'];
                            $return = $this->send_ways_data($send_ways_data);
                            return json_encode($return);
                        }
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '哆啦宝进件-法人身份证正面照片必须上传，请重试'
                        ]);
                    }

                    //法人身份证反面
                    if ($cert_photo_b) {
                        $attach_type = 'IDENTITYOPPOSITE';
                        $upload_file_re = $this->dlb_upload_img($cert_photo_b, $attach_type, $mch_num, $config_id);
                        if ($upload_file_re['status'] == '1') {
                            //附件信息存库备用
                            $identity_opposite_data = [
                                'config_id' => $config_id,
                                'store_id' => $store_id,
                                'mch_num' => $mch_num,
                                'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                'attach_name' => $upload_file_re['data']['fileName'],
                                'attach_type' => $attach_type,
                                'attach_url' => $upload_file_re['data']['url'],
                            ];
                            DlbStoreUpload::created($identity_opposite_data);
                        } else {
                            $send_ways_data['status_desc'] = $upload_file_re['message'];
                            $return = $this->send_ways_data($send_ways_data);
                            return json_encode($return);
                        }
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '哆啦宝进件-法人身份证反面照片必须上传，请重试'
                        ]);
                    }

                    //营业执照-非个人都要传
                    if ($Store->store_type != '3') {
                        if ($license_photo) {
                            $attach_type = 'LICENCE';
                            $upload_file_re = $this->dlb_upload_img($license_photo, $attach_type, $mch_num, $config_id);
                            if ($upload_file_re['status'] == '1') {
                                //附件信息存库备用
                                $licence_data = [
                                    'config_id' => $config_id,
                                    'store_id' => $store_id,
                                    'mch_num' => $mch_num,
                                    'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                    'attach_name' => $upload_file_re['data']['fileName'],
                                    'attach_type' => $attach_type,
                                    'attach_url' => $upload_file_re['data']['url'],
                                ];
                                DlbStoreUpload::created($licence_data);
                            } else {
                                $send_ways_data['status_desc'] = $upload_file_re['message'];
                                $return = $this->send_ways_data($send_ways_data);
                                return json_encode($return);
                            }
                        } else {
                            return json_encode([
                                'status' => 2,
                                'message' => '哆啦宝进件-营业执照照片必须上传，请重试'
                            ]);
                        }
                    }

                    $bank_front_photo = $StoreImg->bank_img_a; //结算银行卡（正面）
                    $authorization_for_settlement_photo = $StoreImg->store_auth_bank_img; //结算账户指定书
                    $bank_front_hold_photo = $StoreImg->dlb_settler_hold_settlecard; //结算人手持结算卡
                    $handheld_of_id_card_photo = $StoreImg->bank_sc_img; //结算人手持身份证
                    $settle_identity_front_photo = $StoreImg->bank_sfz_img_a; //结算人身份证（正）
                    $settle_identity_opposite_photo = $StoreImg->bank_sfz_img_b; //结算人身份证（反）
                    $permit_photo = $StoreImg->store_industrylicense_img; //开户许可证
                    $identify_handhold_photo = $StoreImg->bank_sc_img; //手持身份证

                    //经营性质 1-个体
                    if ($Store->store_type = '1') {
                        //对私01
                        if ($StoreBank->store_bank_type == "01") {
                            //结算银行卡（正面） BANKFRONT
                            if ($bank_front_photo) {
                                $attach_type = 'BANKFRONT';
                                $upload_file_re = $this->dlb_upload_img($bank_front_photo, $attach_type, $mch_num, $config_id);
                                if ($upload_file_re['status'] == '1') {
                                    //附件信息存库备用
                                    $cashierdesk_data = [
                                        'config_id' => $config_id,
                                        'store_id' => $store_id,
                                        'mch_num' => $mch_num,
                                        'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                        'attach_name' => $upload_file_re['data']['fileName'],
                                        'attach_type' => $attach_type,
                                        'attach_url' => $upload_file_re['data']['url'],
                                    ];
                                    DlbStoreUpload::created($cashierdesk_data);
                                } else {
                                    $send_ways_data['status_desc'] = $upload_file_re['message'];
                                    $return = $this->send_ways_data($send_ways_data);
                                    return json_encode($return);
                                }
                            } else {
                                return json_encode([
                                    'status' => 2,
                                    'message' => '哆啦宝进件-结算银行卡（正面）照片必须上传，请重试'
                                ]);
                            }

                            //非法人结算
                            if ($Store->head_name == $StoreBank->store_bank_name) {
                                //结算账户指定书 AUTHORIZATIONFORSETTLEMENT
                                if ($authorization_for_settlement_photo) {
                                    $attach_type = 'AUTHORIZATIONFORSETTLEMENT';
                                    $upload_file_re = $this->dlb_upload_img($authorization_for_settlement_photo, $attach_type, $mch_num, $config_id);
                                    if ($upload_file_re['status'] == '1') {
                                        //附件信息存库备用
                                        $cashierdesk_data = [
                                            'config_id' => $config_id,
                                            'store_id' => $store_id,
                                            'mch_num' => $mch_num,
                                            'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                            'attach_name' => $upload_file_re['data']['fileName'],
                                            'attach_type' => $attach_type,
                                            'attach_url' => $upload_file_re['data']['url'],
                                        ];
                                        DlbStoreUpload::created($cashierdesk_data);
                                    } else {
                                        $send_ways_data['status_desc'] = $upload_file_re['message'];
                                        $return = $this->send_ways_data($send_ways_data);
                                        return json_encode($return);
                                    }
                                } else {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => '哆啦宝进件-结算账户指定书照片必须上传，请重试'
                                    ]);
                                }

                                //结算人手持结算卡 BANKFRONTHOLD  ???
                                if ($bank_front_hold_photo) {
                                    $attach_type = 'BANKFRONTHOLD';
                                    $upload_file_re = $this->dlb_upload_img($bank_front_hold_photo, $attach_type, $mch_num, $config_id);
                                    if ($upload_file_re['status'] == '1') {
                                        //附件信息存库备用
                                        $cashierdesk_data = [
                                            'config_id' => $config_id,
                                            'store_id' => $store_id,
                                            'mch_num' => $mch_num,
                                            'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                            'attach_name' => $upload_file_re['data']['fileName'],
                                            'attach_type' => $attach_type,
                                            'attach_url' => $upload_file_re['data']['url'],
                                        ];
                                        DlbStoreUpload::created($cashierdesk_data);
                                    } else {
                                        $send_ways_data['status_desc'] = $upload_file_re['message'];
                                        $return = $this->send_ways_data($send_ways_data);
                                        return json_encode($return);
                                    }
                                } else {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => '哆啦宝进件-结算人手持结算卡照片必须上传，请重试'
                                    ]);
                                }

                                //结算人手持身份证 HANDHELD_OF_ID_CARD
                                if ($handheld_of_id_card_photo) {
                                    $attach_type = 'HANDHELD_OF_ID_CARD';
                                    $upload_file_re = $this->dlb_upload_img($handheld_of_id_card_photo, $attach_type, $mch_num, $config_id);
                                    if ($upload_file_re['status'] == '1') {
                                        //附件信息存库备用
                                        $cashierdesk_data = [
                                            'config_id' => $config_id,
                                            'store_id' => $store_id,
                                            'mch_num' => $mch_num,
                                            'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                            'attach_name' => $upload_file_re['data']['fileName'],
                                            'attach_type' => $attach_type,
                                            'attach_url' => $upload_file_re['data']['url'],
                                        ];
                                        DlbStoreUpload::created($cashierdesk_data);
                                    } else {
                                        $send_ways_data['status_desc'] = $upload_file_re['message'];
                                        $return = $this->send_ways_data($send_ways_data);
                                        return json_encode($return);
                                    }
                                } else {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => '哆啦宝进件-结算人手持身份证照片必须上传，请重试'
                                    ]);
                                }

                                //结算人身份证（正） SETTLEIDENTITYFRONT
                                if ($settle_identity_front_photo) {
                                    $attach_type = 'SETTLEIDENTITYFRONT';
                                    $upload_file_re = $this->dlb_upload_img($settle_identity_front_photo, $attach_type, $mch_num, $config_id);
                                    if ($upload_file_re['status'] == '1') {
                                        //附件信息存库备用
                                        $cashierdesk_data = [
                                            'config_id' => $config_id,
                                            'store_id' => $store_id,
                                            'mch_num' => $mch_num,
                                            'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                            'attach_name' => $upload_file_re['data']['fileName'],
                                            'attach_type' => $attach_type,
                                            'attach_url' => $upload_file_re['data']['url'],
                                        ];
                                        DlbStoreUpload::created($cashierdesk_data);
                                    } else {
                                        $send_ways_data['status_desc'] = $upload_file_re['message'];
                                        $return = $this->send_ways_data($send_ways_data);
                                        return json_encode($return);
                                    }
                                } else {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => '哆啦宝进件-结算人身份证（正）照片必须上传，请重试'
                                    ]);
                                }

                                //结算人身份证（反） SETTLEIDENTITYOPPOSITE
                                if ($settle_identity_opposite_photo) {
                                    $attach_type = 'SETTLEIDENTITYOPPOSITE';
                                    $upload_file_re = $this->dlb_upload_img($settle_identity_opposite_photo, $attach_type, $mch_num, $config_id);
                                    if ($upload_file_re['status'] == '1') {
                                        //附件信息存库备用
                                        $cashierdesk_data = [
                                            'config_id' => $config_id,
                                            'store_id' => $store_id,
                                            'mch_num' => $mch_num,
                                            'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                            'attach_name' => $upload_file_re['data']['fileName'],
                                            'attach_type' => $attach_type,
                                            'attach_url' => $upload_file_re['data']['url'],
                                        ];
                                        DlbStoreUpload::created($cashierdesk_data);
                                    } else {
                                        $send_ways_data['status_desc'] = $upload_file_re['message'];
                                        $return = $this->send_ways_data($send_ways_data);
                                        return json_encode($return);
                                    }
                                } else {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => '哆啦宝进件-结算人身份证（反）照片必须上传，请重试'
                                    ]);
                                }
                            }
                        } else {
                            //对公 默认法人
                            if ($Store->head_name == $StoreBank->store_bank_name) {
                                //开户许可证 PERMIT
                                if ($permit_photo) {
                                    $attach_type = 'PERMIT';
                                    $upload_file_re = $this->dlb_upload_img($permit_photo, $attach_type, $mch_num, $config_id);
                                    if ($upload_file_re['status'] == '1') {
                                        //附件信息存库备用
                                        $cashierdesk_data = [
                                            'config_id' => $config_id,
                                            'store_id' => $store_id,
                                            'mch_num' => $mch_num,
                                            'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                            'attach_name' => $upload_file_re['data']['fileName'],
                                            'attach_type' => $attach_type,
                                            'attach_url' => $upload_file_re['data']['url'],
                                        ];
                                        DlbStoreUpload::created($cashierdesk_data);
                                    } else {
                                        $send_ways_data['status_desc'] = $upload_file_re['message'];
                                        $return = $this->send_ways_data($send_ways_data);
                                        return json_encode($return);
                                    }
                                } else {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => '哆啦宝进件-开户许可证照片必须上传，请重试'
                                    ]);
                                }
                            }
                        }
                    }

                    //经营性质 2-企业
                    if ($Store->store_type = '2') {
                        //开户许可证 PERMIT
                        if ($permit_photo) {
                            $attach_type = 'PERMIT';
                            $upload_file_re = $this->dlb_upload_img($permit_photo, $attach_type, $mch_num, $config_id);
                            if ($upload_file_re['status'] == '1') {
                                //附件信息存库备用
                                $cashierdesk_data = [
                                    'config_id' => $config_id,
                                    'store_id' => $store_id,
                                    'mch_num' => $mch_num,
                                    'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                    'attach_name' => $upload_file_re['data']['fileName'],
                                    'attach_type' => $attach_type,
                                    'attach_url' => $upload_file_re['data']['url'],
                                ];
                                DlbStoreUpload::created($cashierdesk_data);
                            } else {
                                $send_ways_data['status_desc'] = $upload_file_re['message'];
                                $return = $this->send_ways_data($send_ways_data);
                                return json_encode($return);
                            }
                        } else {
                            return json_encode([
                                'status' => 2,
                                'message' => '哆啦宝进件-开户许可证照片必须上传，请重试'
                            ]);
                        }

                        //对私01
                        if ($StoreBank->store_bank_type == "01") {
                            //结算银行卡（正面） BANKFRONT
                            if ($bank_front_photo) {
                                $attach_type = 'BANKFRONT';

                                $upload_file_re = $this->dlb_upload_img($bank_front_photo, $attach_type, $mch_num, $config_id);
                                if ($upload_file_re['status'] == '1') {
                                    //附件信息存库备用
                                    $cashierdesk_data = [
                                        'config_id' => $config_id,
                                        'store_id' => $store_id,
                                        'mch_num' => $mch_num,
                                        'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                        'attach_name' => $upload_file_re['data']['fileName'],
                                        'attach_type' => $attach_type,
                                        'attach_url' => $upload_file_re['data']['url'],
                                    ];
                                    DlbStoreUpload::created($cashierdesk_data);
                                } else {
                                    $send_ways_data['status_desc'] = $upload_file_re['message'];
                                    $return = $this->send_ways_data($send_ways_data);
                                    return json_encode($return);
                                }
                            } else {
                                return json_encode([
                                    'status' => 2,
                                    'message' => '哆啦宝进件-结算银行卡（正面）照片必须上传，请重试'
                                ]);
                            }

                            //非法人结算
                            if ($Store->head_name != $StoreBank->store_bank_name) {
                                //结算账户指定书 AUTHORIZATIONFORSETTLEMENT
                                if ($authorization_for_settlement_photo) {
                                    $attach_type = 'AUTHORIZATIONFORSETTLEMENT';
                                    $upload_file_re = $this->dlb_upload_img($authorization_for_settlement_photo, $attach_type, $mch_num, $config_id);
                                    if ($upload_file_re['status'] == '1') {
                                        //附件信息存库备用
                                        $cashierdesk_data = [
                                            'config_id' => $config_id,
                                            'store_id' => $store_id,
                                            'mch_num' => $mch_num,
                                            'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                            'attach_name' => $upload_file_re['data']['fileName'],
                                            'attach_type' => $attach_type,
                                            'attach_url' => $upload_file_re['data']['url'],
                                        ];
                                        DlbStoreUpload::created($cashierdesk_data);
                                    } else {
                                        $send_ways_data['status_desc'] = $upload_file_re['message'];
                                        $return = $this->send_ways_data($send_ways_data);
                                        return json_encode($return);
                                    }
                                } else {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => '哆啦宝进件-结算账户指定书照片必须上传，请重试'
                                    ]);
                                }

                                //结算人手持结算卡 BANKFRONTHOLD ???
                                if ($bank_front_hold_photo) {
                                    $attach_type = 'BANKFRONTHOLD';
                                    $upload_file_re = $this->dlb_upload_img($bank_front_hold_photo, $attach_type, $mch_num, $config_id);
                                    if ($upload_file_re['status'] == '1') {
                                        //附件信息存库备用
                                        $cashierdesk_data = [
                                            'config_id' => $config_id,
                                            'store_id' => $store_id,
                                            'mch_num' => $mch_num,
                                            'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                            'attach_name' => $upload_file_re['data']['fileName'],
                                            'attach_type' => $attach_type,
                                            'attach_url' => $upload_file_re['data']['url'],
                                        ];
                                        DlbStoreUpload::created($cashierdesk_data);
                                    } else {
                                        $send_ways_data['status_desc'] = $upload_file_re['message'];
                                        $return = $this->send_ways_data($send_ways_data);
                                        return json_encode($return);
                                    }
                                } else {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => '哆啦宝进件-结算人手持结算卡照片必须上传，请重试'
                                    ]);
                                }

                                //结算人手持身份证 HANDHELD_OF_ID_CARD
                                if ($handheld_of_id_card_photo) {
                                    $attach_type = 'HANDHELD_OF_ID_CARD';
                                    $upload_file_re = $this->dlb_upload_img($handheld_of_id_card_photo, $attach_type, $mch_num, $config_id);
                                    if ($upload_file_re['status'] == '1') {
                                        //附件信息存库备用
                                        $cashierdesk_data = [
                                            'config_id' => $config_id,
                                            'store_id' => $store_id,
                                            'mch_num' => $mch_num,
                                            'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                            'attach_name' => $upload_file_re['data']['fileName'],
                                            'attach_type' => $attach_type,
                                            'attach_url' => $upload_file_re['data']['url'],
                                        ];
                                        DlbStoreUpload::created($cashierdesk_data);
                                    } else {
                                        $send_ways_data['status_desc'] = $upload_file_re['message'];
                                        $return = $this->send_ways_data($send_ways_data);
                                        return json_encode($return);
                                    }
                                } else {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => '哆啦宝进件-结算人手持身份证照片必须上传，请重试'
                                    ]);
                                }

                                //结算人身份证（正） SETTLEIDENTITYFRONT
                                if ($settle_identity_front_photo) {
                                    $attach_type = 'SETTLEIDENTITYFRONT';
                                    $upload_file_re = $this->dlb_upload_img($settle_identity_front_photo, $attach_type, $mch_num, $config_id);
                                    if ($upload_file_re['status'] == '1') {
                                        //附件信息存库备用
                                        $cashierdesk_data = [
                                            'config_id' => $config_id,
                                            'store_id' => $store_id,
                                            'mch_num' => $mch_num,
                                            'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                            'attach_name' => $upload_file_re['data']['fileName'],
                                            'attach_type' => $attach_type,
                                            'attach_url' => $upload_file_re['data']['url'],
                                        ];
                                        DlbStoreUpload::created($cashierdesk_data);
                                    } else {
                                        $send_ways_data['status_desc'] = $upload_file_re['message'];
                                        $return = $this->send_ways_data($send_ways_data);
                                        return json_encode($return);
                                    }
                                } else {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => '哆啦宝进件-结算人身份证（正）照片必须上传，请重试'
                                    ]);
                                }

                                //结算人身份证（反） SETTLEIDENTITYOPPOSITE
                                if ($settle_identity_opposite_photo) {
                                    $attach_type = 'SETTLEIDENTITYOPPOSITE';
                                    $upload_file_re = $this->dlb_upload_img($settle_identity_opposite_photo, $attach_type, $mch_num, $config_id);
                                    if ($upload_file_re['status'] == '1') {
                                        //附件信息存库备用
                                        $cashierdesk_data = [
                                            'config_id' => $config_id,
                                            'store_id' => $store_id,
                                            'mch_num' => $mch_num,
                                            'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                            'attach_name' => $upload_file_re['data']['fileName'],
                                            'attach_type' => $attach_type,
                                            'attach_url' => $upload_file_re['data']['url'],
                                        ];
                                        DlbStoreUpload::created($cashierdesk_data);
                                    } else {
                                        $send_ways_data['status_desc'] = $upload_file_re['message'];
                                        $return = $this->send_ways_data($send_ways_data);
                                        return json_encode($return);
                                    }
                                } else {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => '哆啦宝进件-结算人身份证（反）照片必须上传，请重试'
                                    ]);
                                }
                            }
                        }

                    }

                    //经营性质 3-个人
                    if ($Store->store_type = '3') {
                        //对私
                        if ($StoreBank->store_bank_type == "01") {
                            //法人结算
                            if ($Store->head_name == $StoreBank->store_bank_name) {
                                //手持身份证 IDENTIFYHANDHOLD
                                if ($identify_handhold_photo) {
                                    $attach_type = 'IDENTIFYHANDHOLD';
                                    $upload_file_re = $this->dlb_upload_img($identify_handhold_photo, $attach_type, $mch_num, $config_id);
                                    if ($upload_file_re['status'] == '1') {
                                        //附件信息存库备用
                                        $cashierdesk_data = [
                                            'config_id' => $config_id,
                                            'store_id' => $store_id,
                                            'mch_num' => $mch_num,
                                            'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                            'attach_name' => $upload_file_re['data']['fileName'],
                                            'attach_type' => $attach_type,
                                            'attach_url' => $upload_file_re['data']['url'],
                                        ];
                                        DlbStoreUpload::created($cashierdesk_data);
                                    } else {
                                        $send_ways_data['status_desc'] = $upload_file_re['message'];
                                        $return = $this->send_ways_data($send_ways_data);
                                        return json_encode($return);
                                    }
                                } else {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => '哆啦宝进件-手持身份证照片必须上传，请重试'
                                    ]);
                                }

                                //结算银行卡（正面） BANKFRONT
                                if ($bank_front_photo) {
                                    $attach_type = 'BANKFRONT';

                                    $upload_file_re = $this->dlb_upload_img($bank_front_photo, $attach_type, $mch_num, $config_id);
                                    if ($upload_file_re['status'] == '1') {
                                        //附件信息存库备用
                                        $cashierdesk_data = [
                                            'config_id' => $config_id,
                                            'store_id' => $store_id,
                                            'mch_num' => $mch_num,
                                            'attach_num' => $upload_file_re['data']['attachNum'], //附件编号
                                            'attach_name' => $upload_file_re['data']['fileName'],
                                            'attach_type' => $attach_type,
                                            'attach_url' => $upload_file_re['data']['url'],
                                        ];
                                        DlbStoreUpload::created($cashierdesk_data);
                                    } else {
                                        $send_ways_data['status_desc'] = $upload_file_re['message'];
                                        $return = $this->send_ways_data($send_ways_data);
                                        return json_encode($return);
                                    }
                                } else {
                                    return json_encode([
                                        'status' => 2,
                                        'message' => '哆啦宝进件-结算银行卡（正面）照片必须上传，请重试'
                                    ]);
                                }
                            }
                        }
                    }

                    //提交报单
                    $declare_data = [
                        'customerNum' => $mch_num, //商户编号
                        'callbackUrl' => 'api/dlb/audit_notify', //运营人员审核完成会请求该回调地址
                    ];
                    if ($StoreBank->store_bank_type != "01") {
                        $declare_data['licenseId'] = $Store->store_license_no; //账户对公类型，营业执照必填。
                        $declare_data['licenseStartTime'] = $Store->store_license_stime; //账户对公类型，营业执照起始日期。
                        $declare_data['licenseEndTime'] = ($Store->store_license_time == '长期') ? '2099-01-01' : date('Y-m-d', $Store->store_license_time); //账户对公类型，营业执照结束日期，需比起始日期大于一天。
                    }

                    if (!$dlb_store->status) {
                        //无报单状态，提交报单
                        $declare_form_token = $manager->dlb_sign($timestamp, $manager->dlb_declare_complete, $declare_data, $secret_key);
                        $declare_form_header = array(
                            "Content-Type" => "application/json",
                            "accessKey" => $access_key,
                            "timestamp" => $timestamp,
                            "token" => $declare_form_token,
                        );
                        $declare_form_result = $manager->post_func($manager->dlb_host_url . $manager->dlb_declare_complete, $declare_data, $declare_form_header);
//                        Log::info('哆啦宝进件，提交报单结果：');
//                        Log::info($declare_form_result);

                        if ($declare_form_result['status'] == '1') {
                            //更新报单结果
                            $declare_form_return = json_decode($declare_form_result['data'], true);
                            if ($declare_form_return['result'] == 'success') {
                                //状态
                                $dlb_status = $declare_form_return['data']['declareStatus'];
                                switch ($dlb_status) { //报单状态(1-已通过;2-未审核;3-未通过;4-通道审核中;5-未完成)
                                    case 'INIT':
                                        $dlb_new_status = '5';
                                        $status = '3';
                                        $status_desc = '未完成';
                                        break;
                                    case 'NOTPASS':
                                        $dlb_new_status = '3';
                                        $status = '3';
                                        $status_desc = '未通过';
                                        break;
                                    case 'NOTAUDIT':
                                        $dlb_new_status = '2';
                                        $status = '3';
                                        $status_desc = '未审核';
                                        break;
                                    case 'PASS':
                                        $dlb_new_status = '1';
                                        $status = '1';
                                        $status_desc = '审核通过';
                                        break;
                                    case 'WAITCHANNELAUDIT':
                                        $dlb_new_status = '4';
                                        $status = '2';
                                        $status_desc = '通道审核中';
                                        break;
                                    default:
                                        $dlb_new_status = '0';
                                        $status = '3';
                                        $status_desc = '报单错误,后续补上';
                                }

                                $dlb_store->update(['status' => $dlb_new_status]);
                                $dlb_store->save();
                            } else {
//                                Log::info('哆啦宝进件，提交报单22：');
//                                Log::info($declare_form_return);
                                //TODO:返回错误码信息
                            }
                        } else {
//                            Log::info('哆啦宝进件，提交报单33：');
//                            Log::info($declare_form_result);
                        }
                    }

                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '哆啦宝结算编号未录入'
                    ]);
                }

                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $status;
                $data['status_desc'] = $status_desc;
                $data['company'] = 'dlb';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //海科融通 商户入网
            if (21999 < $type && $type < 22999) {
                $hkrt_obj = new \App\Api\Controllers\Hkrt\BaseController();
                $hkrt_config_obj = new HkrtConfigController();
                $hkrt_config = $hkrt_config_obj->hkrt_config($config_id);
                if (!$hkrt_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '海科融通配置不存在请检查配置'
                    ]);
                }

                $access_id = $hkrt_config->access_id;
                $access_key = $hkrt_config->access_key;

                //默认失败
                $send_ways_data['rate'] = $rate;
                $send_ways_data['store_id'] = $store_id;
                $send_ways_data['status'] = 3;
                $send_ways_data['company'] = 'hkrt';

                //公共必传图片
                $CertPhotoA = $this->images_get($StoreImg->head_sfz_img_a); //法人身份证正面
                $CertPhotoB = $this->images_get($StoreImg->head_sfz_img_b); //法人身份证反面
                $ShopPhoto = $this->images_get($StoreImg->store_logo_img); //门头照片
                $CheckstandPhoto = $this->images_get($StoreImg->store_img_a); //收银台
                $ShopEntrancePhoto = $this->images_get($StoreImg->store_img_b); //门店内景
                $StoreImgC = $this->images_get($StoreImg->store_img_c); //门店内全景

                $CertPhotoA = $this->hkrtUploadImage($CertPhotoA, $access_id, $access_key);
                if ($CertPhotoA['status'] == '0') {
                    $send_ways_data['status_desc'] = $CertPhotoA['message'];
                    $return = $this->send_ways_data($send_ways_data);
                    return json_encode($return);
                }

                $CertPhotoB = $this->hkrtUploadImage($CertPhotoB, $access_id, $access_key);
                if ($CertPhotoB['status'] == '0') {
                    $send_ways_data['status_desc'] = $CertPhotoB['message'];
                    $return = $this->send_ways_data($send_ways_data);
                    return json_encode($return);
                }

                $ShopPhoto = $this->hkrtUploadImage($ShopPhoto, $access_id, $access_key);
                if ($ShopPhoto['status'] == '0') {
                    $send_ways_data['status_desc'] = $ShopPhoto['message'];
                    $return = $this->send_ways_data($send_ways_data);
                    return json_encode($return);
                }

                $CheckstandPhoto = $this->hkrtUploadImage($CheckstandPhoto, $access_id, $access_key);
                if ($CheckstandPhoto['status'] == '0') {
                    $send_ways_data['status_desc'] = $CheckstandPhoto['message'];
                    $return = $this->send_ways_data($send_ways_data);
                    return json_encode($return);
                }

                $ShopEntrancePhoto = $this->hkrtUploadImage($ShopEntrancePhoto, $access_id, $access_key);
                if ($ShopEntrancePhoto['status'] == '0') {
                    $send_ways_data['status_desc'] = $ShopEntrancePhoto['message'];
                    $return = $this->send_ways_data($send_ways_data);
                    return json_encode($return);
                }

                $StoreImgC = $this->hkrtUploadImage($StoreImgC, $access_id, $access_key);
                if ($StoreImgC['status'] == '0') {
                    $send_ways_data['status_desc'] = $StoreImgC['message'];
                    $return = $this->send_ways_data($send_ways_data);
                    return json_encode($return);
                }

                if ($Store->store_type == '1') { //1-个体
                    $merch_type = '10A';
                } elseif ($Store->store_type == '2') { //2-企业
                    $merch_type = '10B';
                } else { //3-个人
                    $merch_type = '10C';
                }

                if ($Store->store_type != '3') {
                    $LicensePhoto = $this->images_get($StoreImg->store_license_img); //营业执照
                    $LicensePhoto = $this->hkrtUploadImage($LicensePhoto, $access_id, $access_key);
                    if ($LicensePhoto['status'] == '0') {
                        $send_ways_data['status_desc'] = $LicensePhoto['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }
                }

                //非法人对私结算
                if ($StoreBank->store_bank_type == '01' && $Store->head_name != $StoreBank->store_bank_name) {
                    $settlePersonIdcardPositive = $this->images_get($StoreImg->bank_sfz_img_a); //结算人身份证正面（人像面）
                    $settlePersonIdcardOpposite = $this->images_get($StoreImg->bank_sfz_img_b); //结算人身份证反面（国徽面）
                    $letterOfAuthPic = $this->images_get($StoreImg->store_auth_bank_img); //非法人结算授权函

                    $settlePersonIdcardPositive = $this->hkrtUploadImage($settlePersonIdcardPositive, $access_id, $access_key);
                    if ($settlePersonIdcardPositive['status'] == '0') {
                        $send_ways_data['status_desc'] = $settlePersonIdcardPositive['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }

                    $settlePersonIdcardOpposite = $this->hkrtUploadImage($settlePersonIdcardOpposite, $access_id, $access_key);
                    if ($settlePersonIdcardOpposite['status'] == '0') {
                        $send_ways_data['status_desc'] = $settlePersonIdcardOpposite['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }

                    $letterOfAuthPic = $this->hkrtUploadImage($letterOfAuthPic, $access_id, $access_key);
                    if ($letterOfAuthPic['status'] == '0') {
                        $send_ways_data['status_desc'] = $letterOfAuthPic['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }

                    $A35 = $settlePersonIdcardPositive['image_id'];
                    $A36 = $settlePersonIdcardOpposite['image_id'];
                    $A32 = $letterOfAuthPic['image_id'];
                }

                //对私结算
                if ($StoreBank->store_bank_type == '01') {
                    $bankCardPositivePic = $this->images_get($StoreImg->bank_img_a); //银行卡正面
                    $bankCardOppositePic = $this->images_get($StoreImg->bank_img_b); //银行卡反面

                    $bankCardPositivePic = $this->hkrtUploadImage($bankCardPositivePic, $access_id, $access_key);
                    if ($bankCardPositivePic['status'] == 0) {
                        $send_ways_data['status_desc'] = $bankCardPositivePic['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }

                    $bankCardOppositePic = $this->hkrtUploadImage($bankCardOppositePic, $access_id, $access_key);
                    if ($bankCardOppositePic['status'] == 0) {
                        $send_ways_data['status_desc'] = $bankCardOppositePic['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }

                    $A4 = $bankCardPositivePic['image_id'];
                    $BankCardOppositePic = $bankCardOppositePic['image_id'];
                } else {
                    //对公结算

                    //开户许可证
                    $IndustryLicensePhoto = $this->images_get($StoreImg->store_industrylicense_img);
                    $IndustryLicensePhoto = $this->hkrtUploadImage($IndustryLicensePhoto, $access_id, $access_key);
                    if ($IndustryLicensePhoto['status'] == '0') {
                        $send_ways_data['status_desc'] = $IndustryLicensePhoto['message'];
                        $return = $this->send_ways_data($send_ways_data);
                        return json_encode($return);
                    }
                    //开户许可证
                    $A4 = $IndustryLicensePhoto['image_id'];
                }

                $agent_apply_no = $this->createAgentApplyNo(12); //服务商申请单号
                $reqData = [
                    'agent_no' => $hkrt_config->agent_no, //服务商的编号，由SAAS平台分配，表示当前商户属于该服务商名下
                    'agent_apply_no' => $agent_apply_no,
                    'notify_url' => url('api/hkrt/merchant_apply_notify_url'), //商户审核状态变更后的通知地址
                    'merchant_data' => json_encode([
                        'contact_name' => $Store->people, //联系人姓名
                        'address' => $Store->province_name . $Store->city_name . $Store->area_name . $Store->store_address, //实际经营地址：详细地址
                        'legal_cert_type' => '1', //法人证件类型,1:身份证 小微商户上送个人证件类型
                        'contact_phone' => $Store->people_phone, //联系人手机号
                        'area_code' => $Store->area_code, //实际经营地址：区代码
                        'merch_type' => $merch_type, //商户类型,10A--个体工商户;10B--企业;10C--小微商户(个人)
                        'city_code' => $Store->city_code, //实际经营地址：市代码
                        'province_code' => $Store->province_code, //实际经营地址：省代码
                        'shop_name' => $Store->store_name, //门店名称
                        'bus_area_code' => $Store->area_code, //营业执照经营地址：区代码
                        'contact_email' => $Store->store_email, //联系人邮箱
                        'merch_name' => $Store->store_name, //商户全称,企业及个体工商户时上送，最大长度40
                        'legal_cert_no' => $Store->head_sfz_no, //法人证件号
                        'bus_address' => $Store->province_name . $Store->city_name . $Store->area_name . $Store->store_address, //营业执照经营地址：详细地址,企业及个体工商户时上送
                        'merch_short_name' => $Store->store_short_name, //商户简称
                        'bus_license_no' => $Store->store_license_no, //营业执照号
                        'legal_name' => $Store->head_name, //法人姓名
                        'bus_city_code' => $Store->city_code, //营业执照经营地址：市代码
                        'bus_province_code' => $Store->province_code, //营业执照经营地址：省代码
                        'bus_scope_code' => $Store->hkrt_bus_scope_code //营业范围代码
                    ]),
                    'bankcard_data' => json_encode([
                        'acc_name' => $StoreBank->store_bank_name, //结算账户名称
                        'bank_code' => $StoreBank->bank_no, //结算账户清算行号
                        'acc_type' => ($StoreBank->store_bank_type == '01') ? '10B' : '10A', //结算类型,10A-对公;10B-对私
                        'phone' => ($StoreBank->store_bank_type == '01') ? $StoreBank->reserved_mobile : '', //预留手机号,acc_type=10B时必传
                        'acc_no' => $StoreBank->store_bank_no, //结算账户号
                        'bank_name' => $StoreBank->bank_name, //结算卡开户银行网点名称,acc_type=10A时必传
                        'bank_city_code' => $StoreBank->bank_city_code, //开户行网点 对应的市代码
                        'idcard_no' => ($StoreBank->store_bank_type == '01' && $Store->head_name != $StoreBank->store_bank_name) ? $StoreBank->bank_sfz_no : $Store->head_sfz_no, //持卡人身份证号,acc_type=10B时必传
                        'bank_province_code' => $StoreBank->bank_province_code //开户行网点 对应的省代码
                    ]),
                    'image_data' => json_encode([
                        'A32' => $A32 ?? '', //法人手持结算授权函合影,对私非法人结算时必传
                        'A34' => ($Store->store_type == '3') ? '' : $LicensePhoto['image_id'], //营业执照合影（法人/经营者）,小微商户(个人)非必传
                        'A35' => $A35 ?? '', //结算人身份证正面,对私非法人结算时必传
                        'A36' => $A36 ?? '', //结算人身份证反面,对私非法人结算时必传
                        'A28' => $A28 ?? '', //O,开户许可证（基本户）,对公、企业必传
                        'A9998' => $A9998 ?? '', //O,其他
                        'A9999' => $A32 ?? '', //非法人结算授权函,对私非法人结算时必传
                        'A1' => $LicensePhoto['image_id'] ?? '', //营业执照,小微商户(个人)非必传
                        'A2' => $CertPhotoA['image_id'] ?? '', //法人身份证正面
                        'A3' => $CertPhotoB['image_id'] ?? '', //法人身份证反面
                        'A4' => $A4, //结算卡正面/开户许可证（基本户）。结算卡对私：上传结算卡正面照片,结算卡对公：上传开户申请表/开户许可证照片
                        'A5' => $ShopPhoto['image_id'] ?? '', //商户门头照
                        'A6' => $ShopEntrancePhoto['image_id'] ?? '', //营业场所室内照
                        'A31' => $StoreImgC['image_id'] ?? '' //O,营业场所室内照2
                    ]),
                    'accessid' => $hkrt_config->access_id
                ];

                $hkrt_store = HkrtStore::where('store_id', $store_id)
                    ->select('merch_no', 'status')
                    ->first();
                //是否已经进件
                if ($hkrt_store) {
                    Log::info('海科融通-商户入网修改');
                    $reqData['sign'] = $hkrt_obj->getSignContent($reqData, $access_key);
                    Log::info($reqData);
                    $re = $hkrt_obj->execute($reqData, $hkrt_obj->url . $hkrt_obj->merchant_modify);
                    Log::info($re);
                } else {
                    Log::info('海科融通-商户入网');
                    $reqData['sign'] = $hkrt_obj->getSignContent($reqData, $access_key);
                    Log::info($reqData);
                    $re = $hkrt_obj->execute($reqData, $hkrt_obj->url . $hkrt_obj->merchant_apply);
                    Log::info($re);
                }

                if (isset($re) && !empty($re)) {
                    $return_data_arr = json_decode($re, true);

                    if ($return_data_arr['return_code'] == '10000') {
                        //商户结算方式设置及变更，若已设置则为变更操作
                        $withdrawal_apply_data = [
                            'accessid' => $hkrt_config->access_id,
                            'agent_no' => $hkrt_config->agent_no,
                            'merch_no' => $return_data_arr['merch_no'],
                            'settlement_cycle' => 'D1', //结算周期,D1(每日);Ts(笔笔到账);D0(当日到账)
                            'withdrawal_rate' => $rate, //提现费率,单位为：%，比如费率为 0.6%, 需要传 0.6
                            'withdrawal_feemin' => '0' //单笔最低提现手续费,单位为：元，比如费率为 1, 需要传 1
                        ];
                        $withdrawal_apply_data['sign'] = $hkrt_obj->getSignContent($withdrawal_apply_data, $access_key);
                        Log::info('海科融通-商户结算方式设置及变更');
                        Log::info($withdrawal_apply_data);
                        $withdrawal_apply_res = $hkrt_obj->execute($withdrawal_apply_data, $hkrt_obj->url . $hkrt_obj->merchant_withdrawal_apply);
                        Log::info($withdrawal_apply_res);
                        if (isset($withdrawal_apply_res) && !empty($withdrawal_apply_res)) {
                            $withdrawal_apply_res_arr = json_decode($withdrawal_apply_res, true);
                            if ($withdrawal_apply_res_arr['return_code'] == '10000') {
                                //修改成功后更改状态
                                $storePayWays = StorePayWay::where('store_id', $store_id)
                                    ->where('company', 'hkrt')
                                    ->update([
                                        'settlement_type' => 'D1'
                                    ]);
                            }
                        } else {
                            $status = '3';
                            $status_desc = '第三方系统返回错误';
                        }

                        $status_desc = "审核中";
                        $status = '2';

                        if ($hkrt_store) {
                            $hkrt_store->update([
                                'merch_no' => $return_data_arr['merch_no'], //商户号
                                'agent_apply_no' => $agent_apply_no
                            ]);
                            $hkrt_store->save();
                        } else {
                            $data_in = [
                                'config_id' => $config_id,
                                'merch_no' => $return_data_arr['merch_no'], //商户号
                                'agent_apply_no' => $agent_apply_no,
                                'store_id' => $store_id
                            ];
                            HkrtStore::create($data_in);
                        }
                    } else {
                        $status = '3';
                        $status_desc = $return_data_arr['return_msg'];
                    }
                } else {
                    $status = '3';
                    $status_desc = "三方系统返回错误";
                }

                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $status;
                $data['status_desc'] = $status_desc;
                $data['company'] = 'hkrt';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //易生支付 商户入网
            if (20999 < $type && $type < 21999) {
                //1.1 图片上传（同步）
                //1.2 返回图片id
                //2.1 商户信息录入（异步）
                //2.2 商户信息接收成功
                //2.3 商户待审核
                //2.4 发送审核结果
                //2.5 根据回调地址，发送商户审核结果
                $easyPayMerAccessObj = new MerAccessController();
//                $easyPayStoreObj = new EasyPayStoreController();
                $easyPayConfigObj = new EasyPayConfigController();
                $easyPayStoresImagesObj = EasypayStoresImages::where('store_id', $store_id)->first();
                $easypay_config = $easyPayConfigObj->easypay_config($easyPayStoresImagesObj->new_config_id);
                //$easypay_config = $easyPayConfigObj->easypay_config($config_id);
                if (!$easypay_config || !$easypay_config->client_code || !$easypay_config->key) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生支付未配置'
                    ]);
                }

                $client_code = $easypay_config->client_code; //进件机构号
                $key = $easypay_config->key; //进件密钥
//                $channel_id = $easypay_config->channel_id; //渠道编号


                if (!$easyPayStoresImagesObj) {
                    return json_encode([
                        'status' => 2,
                        'message' => "请在'门店列表-通道管理'中补充资料"
                    ]);
                }
                //项目归属ID
                $projectId = '';
                if ($easyPayStoresImagesObj->project_id) {
                    $projectId = $easyPayStoresImagesObj->project_id; //项目归属ID
                }

                $contractNo = $easyPayStoresImagesObj->contract_no; //电子协议编号
                $signatoryName = $easyPayStoresImagesObj->signatory_name; //签署方名称

                if ($easyPayStoresImagesObj->house_number) {
                    $houseNumberResId = $easyPayStoresImagesObj->house_number; //09-门牌号
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => "请在'易生进件'中上传'门牌号'照片"
                    ]);
                }
                if ($easyPayStoresImagesObj->location_photo) {
                    $locationPhotoResId = $easyPayStoresImagesObj->location_photo;
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => "请在'易生进件'中上传'定位照'照片"
                    ]);
                }
                $merchantRegistrationSheetAResId = "";
                $merchantRegistrationSheetBResId = "";
                if ($easyPayStoresImagesObj->merc_regist_form_a) {
                    $merchantRegistrationSheetAResId = $easyPayStoresImagesObj->merc_regist_form_a; //11-商户登记表正面
                }
                if ($easyPayStoresImagesObj->merc_regist_form_b) {
                    $merchantRegistrationSheetBResId = $easyPayStoresImagesObj->merc_regist_form_b; //12-商户登记表反面
                }
//                if ($easyPayStoresImagesObj->merc_regist_form_a) {
//                    $merchantRegistrationSheetAResId = $easyPayStoresImagesObj->merc_regist_form_a; //11-商户登记表正面
//                } else {
//                    return json_encode([
//                        'status' => 2,
//                        'message' => "请在'易生进件'中上传'商户登记表正面'照片"
//                    ]);
//                }
//                if ($easyPayStoresImagesObj->merc_regist_form_b) {
//                    $merchantRegistrationSheetBResId = $easyPayStoresImagesObj->merc_regist_form_b; //12-商户登记表反面
//                } else {
//                    return json_encode([
//                        'status' => 2,
//                        'message' => "请在'易生进件'中上传'商户登记表反面'照片"
//                    ]);
//                }
                if ($easyPayStoresImagesObj->mcc_id) {
                    $merType = $easyPayStoresImagesObj->mcc_id; //商户类型(MCC),银联定义的MCC编码
                    $businScope = $easyPayStoresImagesObj->busin_scope; //主营业务
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => "请在'易生进件'中选择'经营类型（MCC）'"
                    ]);
                }
                $standardFlag = 0;
                if ($easyPayStoresImagesObj->standard_flag) {
                    $standardFlag = $easyPayStoresImagesObj->standard_flag; //行业大类 0-标准、1-优惠、2-减免
                }
                if ($easyPayStoresImagesObj->employee_num) {
                    $employeeNum = $easyPayStoresImagesObj->employee_num; //公司规模:1：0-50 人；2：50-100 人；3:100 以上
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => "请在'易生进件'中选择'公司规模'"
                    ]);
                }
                if ($easyPayStoresImagesObj->busin_form) {
                    $businForm = $easyPayStoresImagesObj->busin_form; //经营形态：02-普通店、01-连锁店
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => "请在'易生进件'中选择'经营形态'"
                    ]);
                }
                if ($easyPayStoresImagesObj->busin_beg_time) {
                    $businBegtime = $easyPayStoresImagesObj->busin_beg_time; //营业时间：开始时间，格式：HHMM
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => "请在'易生进件'中选择'营业时间：开始时间'"
                    ]);
                }
                if ($easyPayStoresImagesObj->busin_end_time) {
                    $businEndtime = $easyPayStoresImagesObj->busin_end_time; //营业时间：结束时间，格式：HHMM
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => "请在'易生进件'中选择'营业时间：结束时间'"
                    ]);
                }
//                if ($easyPayStoresImagesObj->term_mode) {
//                    $termMode = $easyPayStoresImagesObj->term_mode; //c,终端种类,新增时必传(0--POS终端,所有交易走易生的实体终端;2--虚拟终端；4--银联POS,直联POS,终端商户号和终端终端号、秘钥非易生的；5--其他POS,先到银联申请终端商户号和终端号,秘钥用易生的；6--二维码终端,(默认为POS终端))???
//                } else {
//                    return json_encode([
//                        'status' => 2,
//                        'message' => "请在'易生进件'中选择'终端种类'"
//                    ]);
//                }
//                if ($easyPayStoresImagesObj->term_code) {
//                    $termCode = $easyPayStoresImagesObj->term_code; //c,内部终端号,变更时必传???
//                } else {
//                    return json_encode([
//                        'status' => 2,
//                        'message' => "请在'易生进件'中填写'内部终端号'"
//                    ]);
//                }
                $termCode = "123456";
                $terUserName = '';

//                if ($easyPayStoresImagesObj->ter_user_name) {
//                    $terUserName = $easyPayStoresImagesObj->ter_user_name; //终端名称
//                } else {
//                    return json_encode([
//                        'status' => 2,
//                        'message' => "请在'易生进件'中填写'终端名称'"
//                    ]);
//                }
//                if ($easyPayStoresImagesObj->area_no) {
//                    $areaNo = $easyPayStoresImagesObj->area_no; //区号
//                } else {
//                    return json_encode([
//                        'status' => 2,
//                        'message' => "请在'易生进件'中选择'区号'"
//                    ]);
//                }
//                 if ($easyPayStoresImagesObj->term_area) {
//                     $termArea = $easyPayStoresImagesObj->term_area; //终端地区
//                 } else {
//                     return json_encode([
//                         'status' => 2,
//                         'message' => "请在'易生进件'中选择'终端地区'"
//                     ]);
//                 }

                $termModel = "新大陆N900";
                $termModelLic = "123456";
//                if ($easyPayStoresImagesObj->term_model) {
//                    $termModel = $easyPayStoresImagesObj->term_model; //c,设备型号:该字段通过接口获取；实体终端必须上传???
//                } else {
//                    return json_encode([
//                        'status' => 2,
//                        'message' => "请在'易生进件'中选择'设备型号'"
//                    ]);
//                }
//                if ($easyPayStoresImagesObj->term_model_lic) {
//                    $termModelLic = $easyPayStoresImagesObj->term_model_lic; //机具号
//                } else {
//                    return json_encode([
//                        'status' => 2,
//                        'message' => "请在'易生进件'中填写'机具号'"
//                    ]);
//                }
//                if ($easyPayStoresImagesObj->term_mode==4 || $easyPayStoresImagesObj->term_mode==5) {
//                    if (($easyPayStoresImagesObj->term_mode==4 || $easyPayStoresImagesObj->term_mode==5) && $easyPayStoresImagesObj->bang_ding_commer_code) {
//                        $bangDingCommerCode = $easyPayStoresImagesObj->bang_ding_commer_code; //c,渠道商户号：终端类型为4或5时,必传???
//                    } else {
//                        return json_encode([
//                            'status' => 2,
//                            'message' => "请在'易生进件'中填写'渠道商户号'"
//                        ]);
//                    }
//                    if ($easyPayStoresImagesObj->bang_ding_ter_mno) {
//                        $bangDingTerMno = $easyPayStoresImagesObj->bang_ding_ter_mno; //c,渠道终端号：终端类型为4或5时,必传???
//                    } else {
//                        return json_encode([
//                            'status' => 2,
//                            'message' => "请在'易生进件'中填写'渠道终端号'"
//                        ]);
//                    }
//                }
                if ($easyPayStoresImagesObj->capital) {
                    $capital = $easyPayStoresImagesObj->capital; //注册资本：单位-万元
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => "请在'易生进件'中填写'注册资本'"
                    ]);
                }
                if ($easyPayStoresImagesObj->bank_name) {
                    $bankName = $easyPayStoresImagesObj->bank_name; //开户行名称
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => "请在'易生进件'中选择'开户行名称'"
                    ]);
                }
                if ($easyPayStoresImagesObj->bank_code) {
                    $bankCode = $easyPayStoresImagesObj->bank_code; //开户行行号
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => "请在'易生进件'中选择'开户行行号'"
                    ]);
                }
                if ($easyPayStoresImagesObj->mer_area) {
                    $merArea = $easyPayStoresImagesObj->mer_area; //商户区域:银联地区码
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => "请在'易生进件'中选择'商户区域:银联地区码'"
                    ]);
                }
                $DStlmType = 0;
                if ($easyPayStoresImagesObj->d_stlm_type) {
                    $DStlmType = $easyPayStoresImagesObj->d_stlm_type; //借记卡扣率方式:1-封顶;0-不封顶
                }
                if ($easyPayStoresImagesObj->d_calc_val || $easyPayStoresImagesObj->d_calc_val === 0 || $easyPayStoresImagesObj->d_calc_val === '0') {
                    $DCalcVal = $easyPayStoresImagesObj->d_calc_val; //借记卡扣率
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => "请在'易生进件'中选择'借记卡扣率'"
                    ]);
                }
//                if ($easyPayStoresImagesObj->un_d_calc_val || $easyPayStoresImagesObj->un_d_calc_val===0 || $easyPayStoresImagesObj->un_d_calc_val==='0') {
//                    $UnDCalcVal = $easyPayStoresImagesObj->un_d_calc_val; //银联二维码借记卡扣率
//                } else {
//                    return json_encode([
//                        'status' => 2,
//                        'message' => "请在'易生进件'中选择'银联二维码借记卡扣率'"
//                    ]);
//                }
                if ($easyPayStoresImagesObj->d_stlm_max_amt) {
                    $DStlmMaxAmt = $easyPayStoresImagesObj->d_stlm_max_amt; //借记卡封顶金额
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => "请在'易生进件'中选择'借记卡封顶金额'"
                    ]);
                }
                $DFeeLowLimit = '';
                if ($easyPayStoresImagesObj->d_fee_low_limit) {
                    $DFeeLowLimit = $easyPayStoresImagesObj->d_fee_low_limit; //借记卡手续费最低值
                }
                if ($easyPayStoresImagesObj->c_calc_val || $easyPayStoresImagesObj->c_calc_val === 0 || $easyPayStoresImagesObj->c_calc_val === '0') {
                    $CCalcVal = $easyPayStoresImagesObj->c_calc_val; //信用卡扣率
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => "请在'易生进件'中选择'信用卡扣率'"
                    ]);
                }
//                if ($easyPayStoresImagesObj->un_c_calc_val || $easyPayStoresImagesObj->un_c_calc_val===0 || $easyPayStoresImagesObj->un_c_calc_val==='0') {
//                    $UnCCalcVal = $easyPayStoresImagesObj->un_c_calc_val; //银联二维码信用卡扣率
//                } else {
//                    return json_encode([
//                        'status' => 2,
//                        'message' => "请在'易生进件'中选择'银联二维码信用卡扣率'"
//                    ]);
//                }
                $CFeeLowLimit = '';
                if ($easyPayStoresImagesObj->c_fee_low_limit) {
                    $CFeeLowLimit = $easyPayStoresImagesObj->c_fee_low_limit; //信用卡手续费最低值
                }
                $realTimeEntry = 0;
                if ($easyPayStoresImagesObj->real_time_entry) {
                    $realTimeEntry = $easyPayStoresImagesObj->real_time_entry; //实时入账(0-否;1-是)
                    $DCalcType = 0;
                    if ($easyPayStoresImagesObj->d_calc_type) {
                        $DCalcType = $easyPayStoresImagesObj->d_calc_type; //借记卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
                    }
                    $CCalcType = 0;
                    if ($easyPayStoresImagesObj->c_calc_type) {
                        $CCalcType = $easyPayStoresImagesObj->c_calc_type; //信用卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
                    }
                }
                if ($easyPayStoresImagesObj->pub_bank_name) {
                    $pubBankName = $easyPayStoresImagesObj->pub_bank_name; //开户行名称(企业对公)
                }
                if ($easyPayStoresImagesObj->pub_bank_code) {
                    $pubBankCode = $easyPayStoresImagesObj->pub_bank_code; //开户行行号(企业对公)
                }
                if ($easyPayStoresImagesObj->pub_account) {
                    $pubAccount = $easyPayStoresImagesObj->pub_account; //账号(企业对公)
                }
                if ($easyPayStoresImagesObj->pub_acc_name) {
                    $pubAccName = $easyPayStoresImagesObj->pub_acc_name; //账户名称(企业对公)
                }
                $twoBarsOpen = '2';
                $twoBars = [];
                if ($easyPayStoresImagesObj->two_bars_open) {
                    $twoBarsOpen = $easyPayStoresImagesObj->two_bars_open; //是否开启立招(1-开启;2-关闭,默认2)
                    if ($twoBarsOpen == '1') {
                        $twoBars = $easyPayStoresImagesObj->two_bars;
                        if (!$twoBars || $twoBars == '[{}]' || $twoBars == '[{"":""}]') {
                            return json_encode([
                                'status' => 2,
                                'message' => "请在'易生进件'中完善'立招功能'"
                            ]);
                        }
                    }
                }
                $d1IsOpen = 0;
                if ($easyPayStoresImagesObj->d1_is_open) {
                    $d1IsOpen = $easyPayStoresImagesObj->d1_is_open; //是否开通D1(0-否;1-是)
                }
                $calcType = 0;
                if ($easyPayStoresImagesObj->calc_type) {
                    $calcType = $easyPayStoresImagesObj->calc_type; //D1计算方式(1-按比例;0-按笔)
                }
                $calcVal = 0;
                if ($easyPayStoresImagesObj->calc_val || $easyPayStoresImagesObj->calc_val === '0' || $easyPayStoresImagesObj->calc_val === 0) {
                    $calcVal = $easyPayStoresImagesObj->calc_val; //D1手续费
                }
                $payDateType = 0;
                if ($easyPayStoresImagesObj->pay_date_type) {
                    $payDateType = $easyPayStoresImagesObj->pay_date_type; //D1费用处理模式(0-每天计费,1-节假日,2-节假日按天)
                }

                //默认失败
//                $send_ways_data['rate'] = $rate;
//                $send_ways_data['store_id'] = $store_id;
//                $send_ways_data['status'] = '3';
//                $send_ways_data['company'] = 'easypay';

                //公共必传图片
                if (!$StoreImg->head_sfz_img_a) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-法人身份证正面-未上传'
                    ]);
                }
                $headSfzImgA = $this->images_get($StoreImg->head_sfz_img_a); //01-法人身份证正面
                $headSfzImgARes = $easyPayMerAccessObj->upload_image($headSfzImgA, 01, $client_code, $key); //-1 系统错误 1-成功 2-失败
                if ($headSfzImgARes['status'] == 1) {
                    $headSfzImgAResId = $headSfzImgARes['data']['fileId'];
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-法人身份证正面-上传失败'
                    ]);
                }

                if (!$StoreImg->head_sfz_img_b) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-法人身份证反面-未上传'
                    ]);
                }
                $headSfzImgB = $this->images_get($StoreImg->head_sfz_img_b); //02-法人身份证反面
                $headSfzImgBRes = $easyPayMerAccessObj->upload_image($headSfzImgB, 02, $client_code, $key);
                if ($headSfzImgBRes['status'] == 1) {
                    $headSfzImgBResId = $headSfzImgBRes['data']['fileId'];
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-法人身份证反面-上传失败'
                    ]);
                }
                $bankSfzImgAResId = '';
                $bankSfzImgBResId = '';
                if ($Store->head_name != $StoreBank->store_bank_name && ($StoreBank->store_bank_type == '01')) {//01对私人 && 非法人结算
                    if (!$StoreImg->bank_sfz_img_a) {
                        return json_encode([
                            'status' => 2,
                            'message' => '易生进件-商户联系人身份证正面-未上传'
                        ]);
                    }

                    $bankSfzImgA = $this->images_get($StoreImg->bank_sfz_img_a); //03-商户联系人身份证正面
                    $bankSfzImgARes = $easyPayMerAccessObj->upload_image($bankSfzImgA, 03, $client_code, $key);
                    if ($bankSfzImgARes['status'] == 1) {
                        $bankSfzImgAResId = $bankSfzImgARes['data']['fileId'];
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '易生进件-商户联系人身份证正面-上传失败'
                        ]);
                    }
                    if (!$StoreImg->bank_sfz_img_b) {
                        return json_encode([
                            'status' => 2,
                            'message' => '易生进件-商户联系人身份证反面-未上传'
                        ]);
                    }

                    $bankSfzImgB = $this->images_get($StoreImg->bank_sfz_img_b); //04-商户联系人身份证反面
                    $bankSfzImgBRes = $easyPayMerAccessObj->upload_image($bankSfzImgB, 04, $client_code, $key);
                    if ($bankSfzImgBRes['status'] == 1) {
                        $bankSfzImgBResId = $bankSfzImgBRes['data']['fileId'];
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '易生进件-商户联系人身份证反面-上传失败'
                        ]);
                    }
                }

                if (!$StoreImg->store_img_a) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-收银台照片-未上传'
                    ]);
                }

                $storeImgA = $this->images_get($StoreImg->store_img_a); //06-收银台
                $storeImgARes = $easyPayMerAccessObj->upload_image($storeImgA, 06, $client_code, $key);
                if ($storeImgARes['status'] == 1) {
                    $storeImgAResId = $storeImgARes['data']['fileId'];
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-收银台-上传失败'
                    ]);
                }
                if (!$StoreImg->store_img_c) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-内部环境照-未上传'
                    ]);
                }

                $StoreImgC = $this->images_get($StoreImg->store_img_c); //07-内部环境照
                $StoreImgCRes = $easyPayMerAccessObj->upload_image($StoreImgC, 07, $client_code, $key);
                if ($StoreImgCRes['status'] == 1) {
                    $StoreImgCResId = $StoreImgCRes['data']['fileId'];
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-内部环境照-上传失败'
                    ]);
                }
                if (!$StoreImg->store_logo_img) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-营业场所门头照-未上传'
                    ]);
                }

                $storeLogoImg = $this->images_get($StoreImg->store_logo_img); //08-营业场所门头照
                $storeLogoImgRes = $easyPayMerAccessObj->upload_image($storeLogoImg, '08', $client_code, $key);
                if ($storeLogoImgRes['status'] == 1) {
                    $storeLogoImgResId = $storeLogoImgRes['data']['fileId'];
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-营业场所门头照-上传失败'
                    ]);
                }

                if ($Store->store_type == '1') { //1-个体工商户
                    $mer_mode = '1';
                } elseif ($Store->store_type == '2') { //2-企业
                    $mer_mode = '0';
                } else { //3-小微商户
                    $mer_mode = '2';
                }

                $pic_info_list = [];
                $storeLicenseImgId = '';
                if ($Store->store_type != '3') { //非个人,企业（0）或者个体户（1）时，需要传：05-营业执照
                    if (!$StoreImg->store_license_img) {
                        return json_encode([
                            'status' => 2,
                            'message' => '易生进件-营业执照-未上传'
                        ]);
                    }
                    $storeLicenseImg = $this->images_get($StoreImg->store_license_img); //05-营业执照
                    $storeLicenseImgRes = $easyPayMerAccessObj->upload_image($storeLicenseImg, 05, $client_code, $key); //-1 系统错误 1-成功 2-失败
                    if ($storeLicenseImgRes['status'] == 1) {
                        $storeLicenseImgId = $storeLicenseImgRes['data']['fileId'];
                        $pic_info_list[] = [
                            'picMode' => '05',
                            'fileId' => $storeLicenseImgId
                        ];
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '易生进件-营业执照-上传失败'
                        ]);
                    }
                }

                //收款银行为对私时，需要传：14-银行卡；15-清算授权书(电子协议替换)
                $storeIndustryLicenseImgId = '';
                $certOfAuthId = '';
                $bankImgAResId = '';
                if ($StoreBank->store_bank_type == '01') {
                    if ($contractNo == null || $contractNo == "") { //进件接口中上送协议id或协议图片

                    }

                    if ($mer_mode != '2') { //非小微商户
                        $certOfAuthId = $easyPayStoresImagesObj->cert_of_auth; //15-清算授权书
                        if ($certOfAuthId) {
                            $pic_info_list[] = [
                                'picMode' => '15',
                                'fileId' => $certOfAuthId
                            ];
                        } else {
                            return json_encode([
                                'status' => 2,
                                'message' => "请在'易生进件'中上传'清算授权书'照片"
                            ]);
                        }
                    }

                    if (!$StoreImg->bank_img_a) {
                        return json_encode([
                            'status' => 2,
                            'message' => '易生进件-银行卡-未上传'
                        ]);
                    }

                    $bankImgA = $this->images_get($StoreImg->bank_img_a); //14-银行卡
                    $bankImgARes = $easyPayMerAccessObj->upload_image($bankImgA, 14, $client_code, $key);
                    if ($bankImgARes['status'] == 1) {
                        $bankImgAResId = $bankImgARes['data']['fileId'];

                        $pic_info_list[] = [
                            'picMode' => '14', //14-银行卡
                            'fileId' => $bankImgAResId
                        ];
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '易生进件-银行卡-上传失败'
                        ]);
                    }
                }

                $reqData = [
                    'version' => '1.0', //版本号
                    'clientCode' => $client_code, //机构号
                    'backUrl' => url('api/easypay/merchant_apply_notify_url'), //o,回调地址
                    'merInfo' => [
                        'merMode' => $mer_mode, //商户类型（0-企业 1-个体户 2-个人）
                        'merName' => $Store->store_name, //注册名称
                        'businName' => $Store->store_short_name, //经营名称 (店名)
                        'merType' => "$merType", //商户类型（MCC），银联定义的 MCC 编码
                        'standardFlag' => "$standardFlag", //行业大类 0-标准、1-优惠、2-减免
                        'merArea' => $merArea, //商户区域：银联地区码
                        'merAddr' => $Store->province_name . $Store->city_name . $Store->area_name . $Store->store_address, //注册地址
                        'businBegtime' => $businBegtime, //营业时间：开始时间，格式：HHMM
                        'businEndtime' => $businEndtime, //营业时间：结束时间，格式：HHMM
                        'employeeNum' => $employeeNum, //公司规模:1：0-50 人；2：50-100 人；3:100 以上
                        'businForm' => $businForm, //经营形态：02-普通店、01-连锁店
                    ],
                    'plusInfo' => [
                        'merLegal' => $Store->head_name, //法定代表人姓名
                        'legalType' => '0', //证件类型：0-居民身份证或临时身份证;1-外国公民护照;2-港澳居民来往大陆通行证或其他有效旅游证件；3-其他类个人身份有效证件；4-单位证件；5-军人或武警身份证件
                        'legalCode' => $Store->head_sfz_no, //法人证件号码
                        'legalValidity' => $this->EasyPayTimeFormat($Store->head_sfz_stime, $Store->head_sfz_time), //证件有效期 格式：["起始日期","截止日期"],JSON 格式字符串,例如：["2011.01.01","2039.01.01"]
                        'legalPhone' => "$Store->people_phone", //手机号
                        'linkMan' => $Store->people, //商户联系人姓名
                        'linkmanType' => '0', //证件类型：0-居民身份证或临时身份证;1-外国公民护照;2-港澳居民来往大陆通行证或其他有效旅游证件；3-其他类个人身份有效证件；4-单位证件；5-军人或武警身份证件
                        //'linkmanCode' => $Store->head_sfz_no, //证件号码
                        'linkmanCode' => ($StoreBank->store_bank_type == '01') ? $StoreBank->bank_sfz_no : $Store->head_sfz_no, //证件号码
                        'linkmanValidity' => ($StoreBank->store_bank_type == '01') ? $this->EasyPayTimeFormat($StoreBank->bank_sfz_stime, $StoreBank->bank_sfz_time) : $this->EasyPayTimeFormat($Store->head_sfz_stime, $Store->head_sfz_time), //证件有效期 格式：["起始日期","截止日期"],JSON 格式字符串
                        'linkmanPhone' => $Store->people_phone, //手机号
                        'contractNo' => $contractNo, //电子协议编号
                        'signatoryName' => $signatoryName,//签署方名称
                    ],
//                    'sysInfo' => [
//                        [
//                            'termMode' => $termMode, //c,终端种类:0--POS终端,所有交易走易生的实体终端;2--虚拟终端；4--银联POS,直联POS,终端商户号和终端终端号、秘钥非易生的；5--其他POS,先到银联申请终端商户号和终端号,秘钥用易生的；6--二维码终端,(默认为POS终端),新增时必传???
////                            'termCode' => $termCode, //c,内部终端号,变更时必传???
//                            'username' => $terUserName, //终端名称
//                            'areaNo' => $areaNo, //区号
//                            'termArea' => $areaNo, //终端地区
//                            'installaddress' => $Store->province_name.$Store->city_name.$Store->area_name.$Store->store_address, //终端布放地址
//                            'linkMan' => $Store->people, //联系人
//                            'linkPhone' => $Store->people_phone, //联系电话
////                            'termModel' => $termModel, //c,设备型号:该字段通过接口获取；实体终端必须上传???
//                            'termModelLic' => $termModelLic, //机具号
////                            'bangdingcommercode' => ($easyPayStoresImagesObj->term_mode==4 || $easyPayStoresImagesObj->term_mode==5) ? $bangDingCommerCode: '',  //c,渠道商户号：终端类型为4或5时,必传
////                            'bangdingtermno' => ($easyPayStoresImagesObj->term_mode==4 || $easyPayStoresImagesObj->term_mode==5) ? $bangDingTerMno: '', //c,渠道终端号：终端类型为4或5时,必传
//                        ],
//                    ],
                    'licInfo' => [
                        'merLic' => ($Store->store_type == '1' || $Store->store_type == '2') ? $Store->store_license_no : '',  //C，工商注册号/统一社会信用码（营业执照）,merMode 为0,1 时，必传
                        'licValidity' => ($Store->store_type == '1' || $Store->store_type == '2') ? $this->EasyPayTimeFormat($Store->store_license_stime, $Store->store_license_time) : '', //C,营业执照有效期 格式：["起始日期","截止日期"],JSON 格式字符串 ,merMode 为 0,1 时，必传
                        'businScope' => $businScope, //主营业务
                        'capital' => $capital, //注册资本:单位-万元
                        'licAddr' => $Store->province_name . $Store->city_name . $Store->area_name . $Store->store_address, //注册地址
                        'controlerName' => $Store->head_name, //控股股东或实际控制人姓名或名称
                        'controlerLegalCode' => $Store->head_sfz_no, //控股股东或实际控制人证件号码
                        'controlerLegalType' => '0', //控股股东或实际控制人证件种类：0-居民身份证或临时身份证;1-外国公民护照;2-港澳居民来往大陆通行证或其他有效旅游证件；3-其他类个人身份有效证件；4-单位证件；5-军人或武警身份证件
                        'controlerLegalValidity' => $this->EasyPayTimeFormat($Store->head_sfz_stime, $Store->head_sfz_time) //控股股东或实际控制人证件有效期
                    ],
                    'accInfo' => [
                        'bankName' => $bankName, //开户行名称
                        'bankCode' => $bankCode, //开户行行号
                        'account' => $StoreBank->store_bank_no, //账号
                        'accName' => $StoreBank->store_bank_name, //账户名称
                        'accType' => ($StoreBank->store_bank_type == '02') ? '10' : '00', //账户类型：00-个人；10-对公
                        'legalType' => ($StoreBank->store_bank_type == '01') ? '0' : '', //C,证件类型：0-居民身份证或临时身份证;1-外国公民护照;2-港澳居民来往大陆通行证或其他有效旅游证件；3-其他类个人身份有效证件；4-单位证件；5-军人或武警身份证件 accType 为个人时必填
                        'legalCode' => ($StoreBank->store_bank_type == '01') ? $StoreBank->bank_sfz_no : '', //C,证件号 accType 为个人时必填
                        'accPhone' => ($StoreBank->store_bank_type == '01') ? ($StoreBank->reserved_mobile ? $StoreBank->reserved_mobile : ($Store->people_phone ?? '')) : '', //C,对私手机号accType为个人时必填
                    ],
                    'funcInfo' => [
                        [ //支付宝
                            'funcId' => '2', //功能ID
                            'calcVal' => $rate, //扣率
                        ],
                        [ //微信
                            'funcId' => '3', //功能ID
                            'calcVal' => $rate, //扣率
                        ],
                        [ //银联二维码  借记卡0.45   封顶20  贷记卡0.55
                            'funcId' => '12', //功能ID
                            'DStlmType' => "1", //借记卡扣率方式:1-封顶;0-不封顶
                            'DCalcVal' => "0.45", //借记卡扣率（单位：%）
                            'DStlmMaxAmt' => "20", //借记卡封顶金额（单位：元）, 借记卡扣率方式封顶时必填
                            'DFeeLowLimit' => "0", //借记卡手续费最低值（单位：元）
                            'CCalcVal' => "0.55", //信用卡扣率（单位：%）
                            'CFeeLowLimit' => "0" //信用卡手续费最低值（单位：元）
                        ],
                        [ //银联营销业务  借记卡封顶   借记卡费率0.38  8封顶   贷记卡0.38
                            'funcId' => '14', //功能ID
                            'DStlmType' => "1", //借记卡扣率方式:1-封顶;0-不封顶
                            'DCalcVal' => "0.38", //借记卡扣率（单位：%）
                            'DStlmMaxAmt' => "8", //借记卡封顶金额（单位：元）, 借记卡扣率方式封顶时必填
                            'DFeeLowLimit' => "0", //借记卡手续费最低值（单位：元）
                            'CCalcVal' => "0.38", //信用卡扣率（单位：%）
                            'CFeeLowLimit' => "0" //信用卡手续费最低值（单位：元）
                        ],
                    ],
                    'picInfoList' => [
                        [
                            'picMode' => '01', //图片类型,01-法人身份证正面
                            'fileId' => $headSfzImgAResId
                        ],
                        [
                            'picMode' => '02', //02-法人身份证反面
                            'fileId' => $headSfzImgBResId
                        ],
                        [
                            'picMode' => '06', //06-收银台
                            'fileId' => $storeImgAResId
                        ],
                        [
                            'picMode' => '07', //07-内部环境照
                            'fileId' => $StoreImgCResId
                        ],
                        [
                            'picMode' => '08', //08-营业场所门头照
                            'fileId' => $storeLogoImgResId
                        ],
                        [
                            'picMode' => '09', //09-门牌号
                            'fileId' => $houseNumberResId
                        ],
                        [
                            'picMode' => '16', //16-定位照
                            'fileId' => $locationPhotoResId
                        ],
//                        [
//                            'picMode' => '11', //11-商户登记表正面（选填）
//                            'fileId' => $merchantRegistrationSheetAResId
//                        ],
//                        [
//                            'picMode' => '12', //12-商户登记表反面（选填）
//                            'fileId' => $merchantRegistrationSheetBResId
//                        ],
                        // [
                        //     'picMode' => '14', //14-银行卡
                        //     'fileId' => $bankImgAResId
                        // ],
                    ],
                    'operaTrace' => $this->random_number(2) //操作流水,20
                ];
                if ($projectId) {
                    $reqData['merInfo']['projectId'] = $projectId;  //项目编号(易生提供),用于商户分类统计
                }
                if ($realTimeEntry) { //实时入账
                    $reqData['funcInfo'][] = [
                        'funcId' => '25', //功能ID
                        'tradeType' => '2', //交易类型：0-全部；1-银行卡；2-条码支付
                        'DCalcType' => "1", //借记卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
                        'DCalcVal' => '0.02', //借记卡扣率
                        'CCalcType' => "1", //信用卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
                        'CCalcVal' => "0.02" //信用卡扣率
                    ];
//                    $reqData['funcInfo'][] = [
//                        'funcId' => '25', //功能ID
//                        'tradeType' => '0', //交易类型：0-全部；1-银行卡；2-条码支付
//                        'DCalcType' => "$DCalcType", //借记卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
//                        'DCalcVal' => $DCalcVal, //借记卡扣率
//                        'CCalcType' => "$CCalcType", //信用卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
//                        'CCalcVal' => $CCalcVal //信用卡扣率
//                    ];
                }
                if ($Store->store_type == '2' && ($StoreBank->store_bank_type == '01')) {
                    //当商户类型merMode为企业商户时,如果accInfo的账户类型为对私,则该字段必传，且账户必须是对公账户
                    if (!$pubBankName || !$pubBankCode || !$pubAccount || !$pubAccName) {
                        return json_encode([
                            'status' => 2,
                            'message' => "企业对私结算时,请在'易生进件'中补充'第二个对公结算账户信息'"
                        ]);
                    }
                    $reqData['accInfoBak'] = [
                        'bankName' => $pubBankName, //开户行名称
                        'bankCode' => $pubBankCode, //开户行行号
                        'account' => $pubAccount, //账号
                        'accName' => $pubAccName, //账户名称
                        'accType' => '10' //账户类型：10-对公
                    ];
                }

                if (($Store->store_type == '1' || $Store->store_type == '2') && ($StoreBank->store_bank_type == '02')) { //企业 对公
                    if (!$storeIndustryLicenseImgId) {
                        if (!$StoreImg->store_industrylicense_img) {
                            return json_encode([
                                'status' => 2,
                                'message' => '易生进件-开户许可证-未上传'
                            ]);
                        }
                        $storeIndustryLicenseImg = $this->images_get($StoreImg->store_industrylicense_img); //13-开户许可证
                        $storeIndustryLicenseImgRes = $easyPayMerAccessObj->upload_image($storeIndustryLicenseImg, 13, $client_code, $key); //-1 系统错误 1-成功 2-失败
                        if ($storeIndustryLicenseImgRes['status'] == 1) {
                            $storeIndustryLicenseImgId = $storeIndustryLicenseImgRes['data']['fileId'];
                            $pic_info_list[] = [
                                'picMode' => '13',
                                'fileId' => $storeIndustryLicenseImgId
                            ];
                        } else {
                            return json_encode([
                                'status' => 2,
                                'message' => '易生进件-开户许可证-上传失败'
                            ]);
                        }
                    }
                }

                if ($pic_info_list && $pic_info_list != []) {
                    $reqData['picInfoList'] = array_merge($pic_info_list, $reqData['picInfoList']);
                }
                if ($Store->head_name != $StoreBank->store_bank_name && ($StoreBank->store_bank_type == '01')) {//01对私人 && 非法人结算

                    $bankSfzImgA_list[] = [
                        'picMode' => '03', //03-商户联系人身份证正面
                        'fileId' => $bankSfzImgAResId
                    ];
                    $bankSfzImgB_list[] = [
                        'picMode' => '04', //04-商户联系人身份证反面
                        'fileId' => $bankSfzImgBResId
                    ];
                    $reqData['picInfoList'] = array_merge($bankSfzImgA_list, $reqData['picInfoList']);
                    $reqData['picInfoList'] = array_merge($bankSfzImgB_list, $reqData['picInfoList']);

                }
//                //选填 11-商户登记表正面（选填）
//                if ($easyPayStoresImagesObj->merc_regist_form_a) {
//                    $merc_regist_form_a_list[] = [
//                        'picMode' => '11',
//                        'fileId' => $easyPayStoresImagesObj->merc_regist_form_a
//                    ];
//                    $reqData['picInfoList'] = array_merge($merc_regist_form_a_list, $reqData['picInfoList']);
//                }
//                //选填 12-商户登记表反面（选填）
//                if ($easyPayStoresImagesObj->merc_regist_form_b) {
//                    $lmerc_regist_form_b_list[] = [
//                        'picMode' => '12',
//                        'fileId' => $easyPayStoresImagesObj->merc_regist_form_b
//                    ];
//                    $reqData['picInfoList'] = array_merge($lmerc_regist_form_b_list, $reqData['picInfoList']);
//                }
                //17-手持身份证照片（选填）
                if ($easyPayStoresImagesObj->hand_idcard) {
                    $hand_idcard_list[] = [
                        'picMode' => '17',
                        'fileId' => $easyPayStoresImagesObj->hand_idcard
                    ];
                    $reqData['picInfoList'] = array_merge($hand_idcard_list, $reqData['picInfoList']);
                }
                //18-收款人身份证正面（选填）
                if ($easyPayStoresImagesObj->hand_idcard_front) {
                    $hand_idcard_front_list[] = [
                        'picMode' => '18',
                        'fileId' => $easyPayStoresImagesObj->hand_idcard_front
                    ];
                    $reqData['picInfoList'] = array_merge($hand_idcard_front_list, $reqData['picInfoList']);
                }
                //11-租赁合同
                if ($easyPayStoresImagesObj->agreement) {
                    $agreement_front_list[] = [
                        'picMode' => '11',
                        'fileId' => $easyPayStoresImagesObj->agreement
                    ];
                    $reqData['picInfoList'] = array_merge($agreement_front_list, $reqData['picInfoList']);
                    //$agreementResId = $easyPayStoresImagesObj->agreement;
                }

                if ($twoBarsOpen == '1') { //立招
                    $reqData['funcInfo'][] = [
                        'funcId' => '13', //
                        'twoBars' => "$twoBars" //二维码信息,JSON 字符串（LIST）
                    ];
                }
                if ($d1IsOpen == '1' && $realTimeEntry == 0) { //D1增值服务费
                    $reqData['funcInfo'][] = [
                        'funcId' => '10', //
                        'calcType' => "$calcType", //string,1,计算方式:1-按比例计算(单位：%);0-按笔计算(单位：元)
                        'calcVal' => "$calcVal", //string,12,手续费
                        'payDateType' => "$payDateType" //string,1,费用处理模式：0-每天计费；1-节假日；2-节假日按天
                    ];
                }


                //图片id入库
                $updateImageId = [
                    'head_sfz_img_a' => $headSfzImgAResId,
                    'head_sfz_img_b' => $headSfzImgBResId,
                    'link_sfz_img_a' => $bankSfzImgAResId,
                    'link_sfz_img_b' => $bankSfzImgBResId,
                    'cashier' => $storeImgAResId,
                    'inter_env_photo' => $StoreImgCResId,
                    'front_door_photo' => $storeLogoImgResId,
                    // 'bank_card' => $bankImgAResId,
                    'house_number' => $houseNumberResId,
                    //'agreement' => $agreementResId,
                    'merc_regist_form_a' => $merchantRegistrationSheetAResId,
                    'merc_regist_form_b' => $merchantRegistrationSheetBResId,
                ];
                if ($bankImgAResId) $updateImageId['bank_card'] = $bankImgAResId;
                if ($storeIndustryLicenseImgId) $updateImageId['licence'] = $storeIndustryLicenseImgId;
                if ($certOfAuthId) $updateImageId['cert_of_auth'] = $certOfAuthId;
                if ($storeLicenseImgId) $updateImageId['store_license_no'] = $storeLicenseImgId;
                $updateImageIdRes = $easyPayStoresImagesObj->update($updateImageId);
                if (!$updateImageIdRes) {
                    Log::info('易生-商户入网-图片id-更新失败');
                    Log::info($store_id);
                }

                //数组加json_encode($arr ,JSON_UNESCAPED_SLASHES)处理
                foreach ($reqData as &$values) {
                    if (is_array($values)) {
                        $values = json_encode($values, 256); //不转义中文为unicode,对应的数字256;不转义斜杠,对应的数字64
                    }
                }
                //是否已经进件
                $easypay_store = EasypayStore::where('store_id', $store_id)->first();
                if ($easypay_store) {
                    if (!$easypay_store->mer_trace) {
                        return json_encode([
                            'status' => 2,
                            'message' => "易生进件-商户入网修改-缺少'商户唯一标识'"
                        ]);
                    }
                    $reqData['messageType'] = 'AGENTPATCH'; //报文类型
                    Log::info('易生-商户入网修改-入参：');
                    Log::info($reqData);
                    $reqData['merTrace'] = $easypay_store->mer_trace;
                    $re = $easyPayMerAccessObj->execute($reqData, $key);
                    Log::info('易生-商户入网修改-结果：');
                    Log::info($re);
                } else {
                    $reqData['messageType'] = 'AGMERAPPLY'; //报文类型
                    Log::info('易生-商户入网-参数：');
                    Log::info($reqData);
                    $re = $easyPayMerAccessObj->execute($reqData, $key);
                    Log::info('易生-商户入网-结果：');
                    Log::info($re);
                }

                if (isset($re) && !empty($re)) {
                    if ($re['data']['retCode'] == '0000') {
                        $status_desc = "审核中";
                        $status = '2';

                        if ($easypay_store) {
                            $easypay_store->update([
                                'mer_trace' => $re['data']['merTrace'] //商户唯一标识
                            ]);
                        } else {
                            $data_in = [
                                'config_id' => $config_id,
                                'store_id' => $store_id,
                                'mer_trace' => $re['data']['merTrace'], //商户唯一标识 merTrace
                            ];
                            EasypayStore::create($data_in);
                        }
                    } else if ($re['data']['retCode'] == '6012') { //该商户已审核
                        $status = '1';
                        $status_desc = "审核成功[0000]";
                    } else {
                        $status = '3';
                        $status_desc = $re['data']['retMsg'];
                    }
                } else {
                    $status = '3';
                    $status_desc = "三方系统返回错误";
                }

                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $status;
                $data['status_desc'] = $status_desc;
                $data['company'] = 'easypay';
                $return = $this->send_ways_data($data);

                // return json_encode($return);

                return $return;
            }

            //葫芦天下
            if (22999 < $type && $type < 23999) {
                $hltx_manger = new \App\Api\Controllers\Hltx\ManageController();
                $hltx_config = $hltx_manger->pay_config($config_id, $qd);
                if (!$hltx_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => 'HL支付配置不存在请检查配置'
                    ]);
                }

                $hltx_manger->init($hltx_config); //初始化配置
                $HltxStore = HltxStore::where('store_id', $store_id)->first();
                $go = true;
                $status_desc = "审核中";
                $status = 4; //可能需要手动刷新审核状态 跳过管控
                if ($HltxStore && empty($HltxStore->mer_no) && !empty($HltxStore->serial_no) && empty($HltxStore->result_status)) {
                    //已经有工单号 而且没有审核信息 进行查询
                    $return = $hltx_manger->hltx_merchant_result_query($HltxStore->serial_no);
                    if ($return['status'] == 1) {
                        //        00 入驻成功（审核通过并且所有通道报备
                        //        成功）
                        //        01 入驻失败（审核驳回或全部报备失败）
                        //        02 已提交基本信息（审核通过待报备）
                        //        03 报备中（审核通过报备部分成功）
                        //        04 入驻审核中（审核工单审核中）
                        //        05 工单待提交
                        $return = $return['data'];
                        $fail_reason = isset($return['failReason']) ? strip_tags($return['failReason'], '') : '';
                        if ($return['status'] == '00') {
                            $data_update = [
                                'mer_no' => $return['merNo'],
                                'result_status' => $return['status'],//
                            ];
                            if (isset($return['wechatPayRecordMerchantNo'])) {
                                $data_update['wx_merchant_no'] = $return['wechatPayRecordMerchantNo'];
                            }
                            if (isset($return['aliPayRecordMerchantNo'])) {
                                $data_update['ali_merchant_no'] = $return['aliPayRecordMerchantNo'];
                            }
                            if (isset($return['unionPayRecordMerchantNo'])) {
                                $data_update['union_merchant_no'] = $return['unionPayRecordMerchantNo'];
                            }
                            $channelResultInfoList = json_decode($return['channelResultInfoList'], true);
                            $productResultInfoList = $channelResultInfoList[0]['productResultInfoList'];
                            foreach ($productResultInfoList as $v) {
                                if ($v['resultCode'] == 'S' && $v['prdStatus'] == '00') {
                                    continue;
                                } else {
                                    $fail_reason .= $v['paymentPrdNo'] . ":" . $v['msg'];
                                }
                            }
                            $data_update['fail_reason'] = $fail_reason;
                            $status_desc = "开通成功";
                            $status = 1;
                        } elseif ($return['status'] == '03') {//审核通过报备部分成功
                            $data_update = [
                                'mer_no' => $return['merNo'],
                                'result_status' => $return['status'],//
                                'fail_reason' => $fail_reason,//
                            ];
                            if (isset($return['wechatPayRecordMerchantNo'])) {
                                $data_update['wx_merchant_no'] = $return['wechatPayRecordMerchantNo'];
                            }
                            if (isset($return['aliPayRecordMerchantNo'])) {
                                $data_update['ali_merchant_no'] = $return['aliPayRecordMerchantNo'];
                            }
                            if (isset($return['unionPayRecordMerchantNo'])) {
                                $data_update['union_merchant_no'] = $return['unionPayRecordMerchantNo'];
                            }
                            $status_desc = "审核通过=>部分报备成功:" . $fail_reason;
                            $status = 1;
                        } elseif ($return['status'] == '01') {
                            //确认失败后继续进件
                            $data_update = [
                                'result_status' => $return['status'],//
                                'fail_reason' => $fail_reason,//
                            ];
                            $status_desc = "开通失败:" . $fail_reason;
                            $status = 3;
                        } else {
                            return json_encode([
                                'status' => 2,
                                'message' => '商户入驻审核中,请等待:' . $hltx_manger->result_arr[$return['status']]
                            ]);
                        }
                        $HltxStore->update($data_update);
                        $HltxStore->save();
                        $go = false;
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '商户入驻结果查询异常:' . $return['message']
                        ]);
                    }
                }
                if ($go) {
                    $store_type = $Store->store_type;//经营性质 1-个体，2-企业，3-个人
                    //上传图片
                    $pic_arr = [];
                    //公共图片
                    $pic_path = [
                        'store_logo_img' => $StoreImg->store_logo_img,
                        'store_img_a' => $StoreImg->store_img_a,
                        'store_img_b' => $StoreImg->store_img_b,
                        'store_img_c' => $StoreImg->store_img_c,
                    ];
                    if ($store_bank_type == '01') {
                        //结算卡对私
                        if ($Store->head_name && $StoreBank->store_bank_name && $Store->head_name != $StoreBank->store_bank_name) {
                            //法人 结算人 姓名不一致
                            if ($store_type == 1 || $store_type == 2) {
                                $pic_path['bank_sfz_img_a'] = $StoreImg->bank_sfz_img_a;//持卡人身份证
                                $pic_path['bank_sfz_img_b'] = $StoreImg->bank_sfz_img_b;
                                $pic_path['store_auth_bank_img'] = $StoreImg->store_auth_bank_img;//结算授权书
                            } else {
                                return json_encode([
                                    'status' => 2,
                                    'message' => '个人暂不支持对私银行卡非法人信息申请入驻'//
                                ]);
                            }
                        }
                    }

                    if ($store_type == 1) {
                        //个体1） 对公： 开户名与商户名一致
                        $pic_path['head_sfz_img_a'] = $StoreImg->head_sfz_img_a;
                        $pic_path['head_sfz_img_b'] = $StoreImg->head_sfz_img_b;
                        $pic_path['store_license_img'] = $StoreImg->store_license_img;
                        $pic_path['bank_img_a'] = $StoreImg->bank_img_a ? $StoreImg->bank_img_a : $StoreImg->store_license_img;
                        if ($store_bank_type == '02') {
                            $pic_path['store_industrylicense_img'] = $StoreImg->store_industrylicense_img;
                        }
                    } elseif ($store_type == 2) {
                        //企业要求结算账户1） 对公： 开户名与商户名一致
                        $pic_path['head_sfz_img_a'] = $StoreImg->head_sfz_img_a;
                        $pic_path['head_sfz_img_b'] = $StoreImg->head_sfz_img_b;
                        $pic_path['store_license_img'] = $StoreImg->store_license_img;
                        if ($store_bank_type == '02') {
                            $pic_path['store_industrylicense_img'] = $StoreImg->store_industrylicense_img;
                        } else {
                            $pic_path['bank_img_a'] = $StoreImg->bank_img_a ? $StoreImg->bank_img_a : $StoreImg->store_license_img;

                        }
                    } elseif ($store_type == 3) {
                        //个人要求结算账户1） 只能是负责人同名账户2） 只能是对私账户
                        if ($store_bank_type == '02') {
                            return json_encode([
                                'status' => 2,
                                'message' => '暂不支持个人申请对公结算入驻'//
                            ]);
                        }
                        $pic_path['head_sfz_img_a'] = $StoreImg->head_sfz_img_a;
                        $pic_path['head_sfz_img_b'] = $StoreImg->head_sfz_img_b;
                        $pic_path['bank_img_a'] = $StoreImg->bank_img_a;
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '暂不支持企业,个体和个人以外商户类型入驻'//
                        ]);
                    }
                    foreach ($pic_path as $k => $v) {
                        if (empty($v)) {
                            return json_encode([
                                'status' => 2,
                                'message' => '请上传' . $hltx_manger->img_desc_arr[$k]
                            ]);
                        }
                        $upload_img_result = $hltx_manger->upload_img($v, $k);
                        if ($upload_img_result['status'] != 1) {
                            return json_encode([
                                'status' => 2,
                                'message' => $upload_img_result['message']
                            ]);
                        } else {
                            $pic_arr[$k] = $upload_img_result['data']['picId'];
                        }
                    }
                    $pic_arr['merchant_trade_agreement'] = $pic_arr['head_sfz_img_a'];
                    if (!isset($rate)) {
                        $rate = $UserRate->store_all_rate;
                    }
                    $req_id = $hltx_manger->getReqMsgId();
                    if ($HltxStore && !empty($HltxStore->mer_no)) {
                        //已经申请入驻  修改商户基本信息
                        $return = $hltx_manger->hltx_edit_baseinfo_merchant($HltxStore->mer_no, $Store, $MyBankCategory, $StoreBank, $pic_arr);
                    } else {
                        //申请入驻或重新入驻
                        $return = $hltx_manger->hltx_add_merchant($Store, $MyBankCategory, $StoreBank, $pic_arr, $rate, $UserRate, $req_id);
                        \App\Common\Log::write($return, 'test.log');
                    }

                    //不成功
                    if ($return['status'] != 1) {
                        return json_encode([
                            'status' => 2,
                            'message' => $return['message'],
                        ]);
                    }
                    $return = $return['data'];
                    $data_insert = [
                        'store_id' => $store_id,
                        'config_id' => $hltx_config->id,
                        'req_id' => $req_id,
                        'qd' => $qd,
                        'serial_no' => $return['serialNo'],//工单号
                    ];
                    if ($HltxStore) {
                        $data_insert['result_status'] = '';//重置
                        $data_insert['fail_reason'] = '';
                        $HltxStore->update($data_insert);
                        $HltxStore->save();
                    } else {
                        HltxStore::create($data_insert);
                    }
                }

                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $status;
                $data['status_desc'] = $status_desc;
                $data['company'] = 'hltx';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //联动优势 商户入网
            if (4999 < $type && $type < 5999) {
                $linkage_obj = new \App\Api\Controllers\Linkage\BaseController();
                $linkage_config_obj = new LinkageConfigController();
                $linkage_config = $linkage_config_obj->linkage_config($config_id);
                if (!$linkage_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '联动优势支付配置不存在，请检查配置'
                    ]);
                }

                $StoreLinkage = StoreLinkage::where('store_id', $store_id);
                if (!$StoreLinkage) {
                    return json_encode([
                        'status' => '2',
                        'message' => '联动优势商户入网资料不全'
                    ]);
                }

                $acqSpId = $linkage_config->mch_id; //服务商编号(联动平台分配)
                $wechatChannelId = $linkage_config->wechatChannelId; //微信报备的服务商渠道号
                $alipayChannelId = $linkage_config->alipayChannelId; //支付宝报备的服务商渠道号
                $wxAppid = $linkage_config->wxAppid; //微信appid
                $wxSecret = $linkage_config->wxSecret; //微信secret
                $privateKey = $linkage_config->privateKey; //私钥
                $publicKey = $linkage_config->publicKey; //公钥

                //默认失败
                $send_ways_data['rate'] = $rate;
                $send_ways_data['store_id'] = $store_id;
                $send_ways_data['status'] = '3';
                $send_ways_data['company'] = 'linkage';

                //公共必传图片
                $CertPhotoA = $this->images_get($StoreImg->head_sfz_img_a); //法人身份证正面
                $CertPhotoB = $this->images_get($StoreImg->head_sfz_img_b); //法人身份证反面
                $ShopPhoto = $this->images_get($StoreImg->store_logo_img); //门头照片
                $CheckstandPhoto = $this->images_get($StoreImg->store_img_a); //收银台
                $ShopEntrancePhoto = $this->images_get($StoreImg->store_img_b); //门店内景
                $StoreImgC = $this->images_get($StoreImg->store_img_c); //门店内全景

                //1、2.1 商户信息录入
                $merchantApplyData = [
                    'acqSpId' => $acqSpId, //服务商编号(联动平台分配)
                    'merchantName' => $store_short_name, //商户简称
//                    'paper' => [ //json,商户详细信息
//                        'merchantType' => $store_type, //商户类型,1:个体 2:企业 3:小微
//                        'businessLicenseCode' => ($store_type == 1 || $store_type == 2) ? $license_no : '', //C,营业执照编号,merchantType= 1、2必传
//                        'businessLicenseName' => $store_short_name, //C,商户经营名称,merchantType= 1、2必传
//                        'businessLicenseEffective' => ($store_type == 1 || $store_type == 2) ? (isset($Store->store_license_stime) ? date('Ymd', strtotime($Store->store_license_stime)) : '') : '', //C,营业执照生效日期,merchantType= 1、2必传
//                        'businessLicenseExpired' => ($store_type == 1 || $store_type == 2) ? (isset($Store->store_license_stime) ? ($Store->store_license_stime == '长期' ? '永久' : date('Ymd', strtotime($Store->store_license_stime))) : '') : '', //C,营业执照失效日期,merchantType= 1、2必传，长期有效身份证此处请填写“永久”
//                        'industryCategoryId' => $Store->linkageCategoryId, //经营类目,
//                        'businessAddress' => $Store->province_name.$Store->city_name.$Store->area_name.$Store->store_address, //所属地址详细地址
//                        'province' => $Store->province_name, //所属地址省
//                        'city' => $Store->city_name, //所属地址市
//                        'area' => $Store->area_name, //所属地址区
//                        'longitude' => $Store->lng ?? '', //C,经度
//                        'latitude' => $Store->lat ?? '', //C,纬度
//                        'lawyerName' => $Store->head_name ?? '', //C,法人姓名,merchantType= 1、2必传
//                        'lawyerCertType' => '1', //C,法人证件类型,merchantType= 1、2必传，1：身份证 ;5:护照
//                        'lawyerCertNo' => $Store->head_sfz_no ?? '', //C,法人证件号,merchantType= 1、2必传
//                        'certNoEffective' => ($store_type == 1 || $store_type == 2) ? (isset($Store->head_sfz_stime) ? date('Ymd', strtotime($Store->head_sfz_stime)) : '') : (isset($StoreBank->bank_sfz_stime) ? date('Ymd', strtotime($StoreBank->bank_sfz_stime)) : ''), //C,证件生效日期,merchantType=1、2时，为企业法人证件有效日期;merchantType=3时，为结算人证件有效日期；Ex:20190101
//                        'certNoExpired' => ($store_type == 1 || $store_type == 2) ? (isset($Store->head_sfz_time) ? ($Store->head_sfz_time == '长期' ? '永久' : date('Ymd', strtotime($Store->store_license_stime))) : '') : $StoreBank->bank_sfz_time == '长期' ? '永久' : date('Ymd', strtotime($Store->bank_sfz_time)), //C,证件失效日期,merchantType=1、2时，为企业法人证件失效日期；merchantType=3时，为结算人证件失效日期；Ex:20390101，长期有效身份证此处请填写“永久”
//                        'contactPerson' => $Store->head_name ?? '', //联系人姓名
//                        'contactCertType' => '1', //C,联系人证件类型，1：身份证；5：护照，默认1:身份证
//                        'contactPersonId' => $Store->head_sfz_no ?? '', //联系人证件号
//                        'contactPhone' => $Store->people_phone ?? '', //联系人手机号
//                        'serviceTel' => $Store->people_phone ?? '', //客服电话
//                        'email' => $Store->store_email ?? '', //邮箱
//                        'signType' => $StoreLinkage->signType ?? '', //O,签约协议类型：0:纸质协议；1:电子签章协议,默认为电子签章协议；2:免签（暂不支持）；3：电子签章免签商户章
//                        'signName' => $Store-> ?? '', //C,签约人姓名,电子签章协议需要必填
//                        'signMobileNo' => $Store-> ?? '', //C,签约人手机号,电子签章协议需要必填
//                        'signCertNo' => $Store-> ?? '', //C,签约人身份证号,电子签章协议需要必填
//                        'appName' => $Store-> ?? '', //C,APP名称,展示电子合同的app名称或者网址URL,appName和webSite任选其一,电子签章协议需要必填
//                        'webSite' => $Store-> ?? '', //C,商户网址,展示电子合同的app名称或者网址URL,appName和webSite任选其一,电子签章协议需要必填
//                        'settleAccountType' => $Store-> ?? '', //结算账户类型,1:对公账户 2:法人账户3：被授权人账户;merchantType=3时必填2
//                        'settleAccountNo' => $Store-> ?? '', //结算账号
//                        'settleAccount' => $Store-> ?? '', //结算户名
//                        'settlePeriod' => $Store-> ?? '', //结算周期,1:T1 2:D0 3:T0 4:D1
//                        'settleIdNo' => $Store-> ?? '', //身份证号
//                        'settleIdEffective' => $Store-> ?? '', //身份证生效日期,Ex:20190101
//                        'settleIdExpired' => $Store-> ?? '', //身份证失效日期,Ex:20390101，长期有效身份证此处请填写“永久”
//                        'openBank' => $Store-> ?? '', //C,开户银行名称,对公必填 填写银行编号，数据请联系联调人员
//                        'openSubBank' => $Store-> ?? '', //开户支行名称
//                        'terminalType' => $Store-> ?? '', //C,机具来源,2：代理商自备；3：暂无终端；默认为暂无终端;merchantType=3时，暂不支持2：代理商自备
//                        'installProvinceCode' => $Store-> ?? '', //C,装机地址省编码,terminalType为2时必填
//                        'installCityCode' => $Store-> ?? '', //C,装机地址市编码,terminalType为2时必填
//                        'installAreaCode' => $Store-> ?? '', //C,装机地址区(县)编码,terminalType为2时必填
//                        'installDetailAddr' => $Store-> ?? '', //C,装机详细地址,terminalType为2时必填
//                        'terminals' => $Store-> ?? '', //C,终端SN,terminalType为2时，可以不填，后通过增机接口增加终端merchantType=1、2时,终端个数最多为5个;merchantType=3时,暂不支持终端;格式为provId#modelId#snNumber-snNumber=provId#modelId#snNumber-snNumber,eg：8#02#0000010483433721-00001=8#01#SMP2000test008可用终端查询详见：https://midnightdancing.github.io/offlineAPIDocs/#/offline/terminalSearchprovId:厂商编号；modelId：终端型号编号；snNumber：终端SN
//                    ],
//                    'rate' => [ //json,手续费费率
//                        'feeRateAlipay' => $Store-> ?? '', //C,支付宝费率,费率为0.25%上送0.25
//                        'feeRateWechatpay' => $Store-> ?? '', //C,微信费率
//                        'bankCardRateLevel1' => [ //C,银联二维码银行卡费率一档,1000元以上费率
//                            'feeRateUnionpayDebit' => , //C,银联手续费率(借记),费率为0.25%上送0.25
//                            'feeRateUnionpayDebitCap' => , //C,银联手续费率(借记封顶),一挡1000元以上借记卡封顶费率必填，单位：分
//                            'feeRateUnionpayCredit' =>  //C,银联手续费率(贷记),费率为0.25%上送0.25
//                        ],
//                        'bankCardRateLevel2' => [ //C,银联二维码银行卡费率二档,1000元以下费率
//                            'feeRateUnionpayDebit' => , //C,银联手续费率(借记),费率为0.25%上送0.25
//                            'feeRateUnionpayDebitCap' => , //C,银联手续费率(借记封顶),一挡1000元以上借记卡封顶费率必填，单位：分
//                            'feeRateUnionpayCredit' =>  //C,银联手续费率(贷记),费率为0.25%上送0.25
//                        ],
//                        'feeRateD0' => $Store-> ?? '', //C,附加手续费,D0\D1服务如收取附加手续费则进行配置，费率为0.25%上送0.25
//                        'feeRateWithdraw' => $Store-> ?? '', //C,提现手续费,单位：分
//                        'posRateLevel1' => [ //C,银行卡收单费率一档,1000元以上费率；NFC 1000元以上取该费率；借贷记取该费率
//                            'feeRateUnionpayDebit' => , //C,银联手续费率(借记),费率为0.25%上送0.25
//                            'feeRateUnionpayDebitCap' => , //C,银联手续费率(借记封顶),一挡1000元以上借记卡封顶费率必填，单位：分
//                            'feeRateUnionpayCredit' =>  //C,银联手续费率(贷记),费率为0.25%上送0.25
//                        ],
//                        'posRateLevel2' => [ //C,银行卡收单费率二档,1000元（含1000元）以下费率；小额双免取该费率；NFC 1000元(含)取该费率
//                            'feeRateUnionpayDebit' => , //C,银联手续费率(借记),费率为0.25%上送0.25
//                            'feeRateUnionpayDebitCap' => , //C,银联手续费率(借记封顶),一挡1000元以上借记卡封顶费率必填，单位：分
//                            'feeRateUnionpayCredit' =>  //C,银联手续费率(贷记),费率为0.25%上送0.25
//                        ]
//                    ],
                    'wechatChannelId' => $wechatChannelId, //微信报备的服务商渠道号
                    'alipayChannelId' => $alipayChannelId, //支付宝报备的服务商渠道号
                    'backUrl' => url('/api/linkage/merchant_apply_notify_url'), //商户入网成功回调地址
                    'notifyUrl' => url('/api/linkage/pay_notify_url') //POS交易及立牌交易结果通知配置，主被扫交易结果通知地址配置，请详见具体交易接口文档
                ];
                $merchant_apply = $linkage_obj->http_post($linkage_obj->merchantApply, $merchantApplyData, $privateKey, $publicKey);

                //2、2.2 资质上传
                //3、2.8 电子协议下载
                //4、2.6 获取电子签约挑战码
                //5、2.7 电子签约确认
                //6、4.1 开户结果通知
                //7、2.3 商户信息查询
                //8、2.4+2.5 微信支付配置
            }

            //银盛ysepay 商户入网
            if (13999 < $type && $type < 14999) {
                $ysepayStore = new YsepayStoreController();
                $resMap = $ysepayStore->addCustInfoApply($Store, $StoreBank, $StoreImg); //入网申请
                $resMap = json_decode($resMap, true);

                Log::info('---base_open_ways--ysepay-商户入网---response----params-->' . json_encode($resMap));
                if ($resMap['status'] === '00') {//00:入网成功 01:待上传图片 02:待审核 90:审核拒绝 10:转人工审核
                    if ($resMap['message'] == '该进件流水已经进件成功,不允许审核') {
                        $status = 1;
                        $status_desc = '开通成功';
                    } else {
                        $status = 2;
                        $status_desc = $resMap['message'];
                    }
                } else if ($resMap['status'] === '01') {
                    $status = 2;
                    $status_desc = $resMap['message'];
                } else if ($resMap['status'] === '02') {
                    $status = 2;
                    $status_desc = $resMap['message'];
                } else if ($resMap['status'] === '90') {
                    $status = 3;
                    $status_desc = $resMap['message'];
                } else if ($resMap['status'] === '10') {
                    $status = 2;
                    $status_desc = $resMap['message'];
                } else {
                    return $resMap;
                }

                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $status;
                $data['status_desc'] = $status_desc;
                $data['company'] = 'ysepay';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //拉卡拉 商户入网
            if (34000 < $type && $type < 34010) {
                $lklStore = new \App\Api\Controllers\LklPay\StoreController();
                $resMap = $lklStore->addMerchant($Store, $StoreBank, $StoreImg); //入网申请
                $resMap = json_decode($resMap, true);

                Log::info('---base_open_ways--lkl-商户入网---response----params-->' . json_encode($resMap));
                if ($resMap['status'] == 1) {//1:待审核
                    $status = 2;
                    $status_desc = '审核中';
                } else {
                    $status = 3;
                    $status_desc = isset($resMap['data']['message']) ? $resMap['data']['message'] : $resMap['message'];
                }

                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $status;
                $data['status_desc'] = $status_desc;
                $data['company'] = 'lklpay';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //首信易 商户入网
            if (44000 < $type && $type < 44010) {
                $lklStore = new \App\Api\Controllers\EasePay\StoreController();
                $resMap = $lklStore->addMerchant($Store, $StoreBank, $StoreImg); //入网申请
                $resMap = json_decode($resMap, true);

                Log::info('---base_open_ways--首信易-商户入网---response----params-->' . json_encode($resMap));
                if ($resMap['status'] == 1) {//1:待审核
                    $status = 2;
                    $status_desc = '审核中';
                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['status'] = $status;
                    $data['status_desc'] = $status_desc;
                    $data['company'] = 'easepay';
                    $return = $this->send_ways_data($data);
                } else {
                    $status = 3;
                    $status_desc = isset($resMap['message']) ? $resMap['message'] : '审核失败';
                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['status'] = $status;
                    $data['status_desc'] = $status_desc;
                    $data['company'] = 'easepay';
                    $this->send_ways_data($data);
                    $return = json_encode([
                        'status' => 2,
                        'message' => $status_desc,
                    ]);
                }

                return $return;
            }

            return json_encode([
                'status' => 2,
                'message' => '暂时不支持申请此通道！请更换换其他通道',
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => '-1',
                'message' => $exception->getMessage() . ' | ' . $exception->getLine() . ' | ' . $exception->getFile()
            ]);
        }
    }


    //获取路径
    public function alipay_img_public($img_url, $config, $aop)
    {
        try {
            $img_url = explode('/', $img_url);
            $img_url = end($img_url);
            $img = public_path() . '/upload/images/' . $img_url;
            $aop = new AopUploadClient();
            $aop->useHeader = false;
            $aop->appId = $config->app_id;
            $aop->rsaPrivateKey = $config->rsa_private_key;
            $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
            $aop->gatewayUrl = $config->alipay_gateway;
            $aop->apiVersion = '1.0';
            $aop->signType = 'RSA2';
            $aop->format = 'json';
            $aop->method = "ant.merchant.expand.indirect.image.upload";
            $request = new AntMerchantExpandIndirectImageUploadRequest();
            $request->setImageType("jpg");
            $request->setImageContent("@" . $img);
            $result = $aop->execute($request);
            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            if (!empty($resultCode) && $resultCode == 10000) {
                return $result->$responseNode->image_id;
            } else {
                return '';
            }


        } catch (\Exception $exception) {
            return '';
        }
    }


    //通道入库
    public function send_ways_data($data)
    {
        try {
            //开启事务
            $all_pay_ways = DB::table('store_ways_desc')->where('company', $data['company'])
                ->get();
            foreach ($all_pay_ways as $k => $v) {
                $gets = StorePayWay::where('store_id', $data['store_id'])
                    ->where('ways_source', $v->ways_source)
                    ->get();
                $count = count($gets);
                $ways = StorePayWay::where('store_id', $data['store_id'])
                    ->where('ways_type', $v->ways_type)
                    ->first();
                try {
                    DB::beginTransaction();
                    $data = [
                        'store_id' => $data['store_id'],
                        'ways_type' => $v->ways_type,
                        'company' => $v->company,
                        'ways_source' => $v->ways_source,
                        'ways_desc' => $v->ways_desc,
                        'sort' => ($count + 1),
                        'rate' => $data['rate'],
                        'settlement_type' => $v->settlement_type,
                        'status' => $data['status'],
                        'status_desc' => $data['status_desc'],
                    ];

                    if ($v->ways_source == 'unionpay') {
                        $data['pay_amount_e'] = 100;  //银联最大交易金额 100
                    }

                    if ($ways) {
                        $ways->update(
                            [
                                'status' => $data['status'],
                                'status_desc' => $data['status_desc']
                            ]);
                        $ways->save();
                    } else {
                        StorePayWay::create($data);
                    }
                    DB::commit();
                } catch (\Exception $e) {
                    Log::info('入库通道55');
                    Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
                    DB::rollBack();
                    return [
                        'status' => 2,
                        'message' => '通道入库更新失败1',
                    ];
                }
            }


            return [
                'status' => 1,
                'message' => $data['status_desc'],
            ];

        } catch (\Exception $e) {
            return [
                'status' => '-1',
                'message' => '通道入库更新失败2',
            ];
        }
    }


    // 新大陆上传图片
    public function new_land_img($img_url, $img_name)
    {
        $img_url = explode('/', $img_url);
        $img_url = end($img_url);
        $img = public_path() . '/upload/images/' . $img_url;
//        if ($img) {
//            try {
//                //压缩图片
//                $img_obj = \Intervention\Image\Facades\Image::make($img);
//                $img_obj->resize(500, 400);
//                $img = public_path() . '/upload/s_images/' . $img_url;
//                $img_obj->save($img);
//            } catch (\Exception $exception) {
//                throw new \Exception($img_name . '图片资料存在问题，请重新上传');
//            }
//        }

        return bin2hex(file_get_contents($img));
    }


    //快钱组装图片
    public function images_get($img_url)
    {
        $img_url = explode('/', $img_url);
        $img_url = end($img_url);
        $img = public_path() . '/upload/images/' . $img_url;
//        if ($img) {
//            try {
//                //压缩图片
//                $img_obj = \Intervention\Image\Facades\Image::make($img);
//                $img_obj->resize(800, 700);
//                $img = public_path() . '/upload/s_images/' . $img_url;
//                $img_obj->save($img);
//            } catch (\Exception $exception) {
//                Log::info('快钱组装图片-error');
//                Log::info($exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine());
//            }
//        }
        return $img;
    }


    //快钱支付上传图片
    public function uploadIMG($type, $filepath, $config, $image_desc = "图片")
    {
        try {
            $aop = new \App\Api\Controllers\MyBank\BaseController();
            $ao = $aop->aop($config);
            $partner_private_key = $ao->partner_private_key;
            $url = $aop->MY_BANK_uploadphoto;

            $data = [
                'IsvOrgId' => $ao->IsvOrgId,
                'PhotoType' => $type,
                'OutTradeNo' => date('YmdHis') . time() . rand(10000, 99999),
                'Function' => 'ant.mybank.merchantprod.merchant.uploadphoto',
                'Version' => '1.0.0',
                'AppId' => $ao->Appid,
                'ReqTime' => date('YmdHis'),
            ];
            $ParamUtil = new ParamUtil();
            //拼接字符串
            $unsign = $ParamUtil->getOrderSignContent($data, "UTF-8");
            $unsign = urlencode($unsign);
            $RSACert = new RSACert();
            $aop = new \Alipayopen\Sdk\AopClient();
            $aop->rsaPrivateKey = $partner_private_key;

            $sign = $RSACert->sign($unsign, null, $partner_private_key, "RSA2");
            $data['Signature'] = $sign;

            $OBJ = new  \App\Api\Controllers\MyBank\BaseController();
            $OBJ->gatewayUrl_photo = $url;
            $data["Picture"] = file_get_contents($filepath);
            $data["Picture"] = '@' . $filepath;
            $ch = curl_init();
            if (class_exists('\CURLFile')) {
                $data['Picture'] = new \CURLFile(realpath($filepath));
            } else {
                if (defined('CURLOPT_SAFE_UPLOAD')) {
                    curl_setopt($ch, CURLOPT_SAFE_UPLOAD, FALSE);
                }
            }
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $return = curl_exec($ch);
            curl_close($ch);

            $data = Tools::xml_to_array($return);

            if (!isset($data['document']['response']['body']['PhotoUrl'])) {
                return [
                    'status' => 0,
                    'message' => $image_desc . '上传不成功请重新上传',
                ];
            }
            return [
                'status' => 1,
                'url' => $data['document']['response']['body']['PhotoUrl'],
            ];
        } catch (\Exception $exception) {
            return [
                'status' => 0,
                'message' => $exception->getMessage() . $exception->getLine()
            ];
        }
    }


    //随性付上传图片
    public function vbill_uploadIMG($url, $type, $filepath, $config_id, $image_desc = "图片")
    {
        try {
            $config = new VbillConfigController();
            $vbill_config = $config->vbill_config($config_id);
            if (!$vbill_config) {
                return json_encode([
                    'status' => 2,
                    'message' => '随行付配置不存在请检查配置'
                ]);
            }

            $data = [
                'orgId' => $vbill_config->orgId,
                'pictureType' => $type,
                'reqId' => date('YmdHis'),
            ];
            $ch = curl_init();
            if (class_exists('\CURLFile')) {
                $data['file'] = new \CURLFile(realpath($filepath));
            } else {
                if (defined('CURLOPT_SAFE_UPLOAD')) {
                    curl_setopt($ch, CURLOPT_SAFE_UPLOAD, FALSE);
                }
            }
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $return = curl_exec($ch);
            curl_close($ch);
            $data = json_decode($return, true);

            if ($data['code'] != "0000" || $data['respData']['bizCode'] != "0000") {
                return [
                    'status' => 0,
                    'message' => $image_desc . $data['respData']['bizMsg']
                ];
            }

            return [
                'status' => 1,
                'PhotoUrl' => $data['respData']['PhotoUrl'],
                'cdnUrl' => $data['respData']['cdnUrl']
            ];
        } catch (\Exception $exception) {
            return [
                'status' => 0,
                'message' => $exception->getMessage() . $exception->getLine()
            ];
        }
    }


    //汇付上传图片
    public function hui_pay_upload_img($filepath, $config_id, $image_desc = "图片")
    {
        try {
            $config = new HuiPayConfigController();
            $hui_pay_config = $config->hui_pay_config($config_id);
            if (!$hui_pay_config) {
                return json_encode([
                    'status' => 2,
                    'message' => '汇付支付配置不存在请检查配置'
                ]);
            }

            $huiPayBase = new \App\Api\Controllers\HuiPay\BaseController();

            $img_url_arr = explode('/', $filepath);
            $img_name = end($img_url_arr);
            $img = public_path() . '/upload/images/' . $img_name;

            $file_base64 = $this->imgToBase64($img);  //图片转base64

            $sign_data = [
                'file_name' => $img_name,  //文件名，xxx.jpg
                'file_base64' => $file_base64,  //上传图片文件流做base64处理后
            ];

            $post_data = [
                'data' => $sign_data,
                'sign_type' => 'RSA2',
                'mer_cust_id' => $hui_pay_config->mer_cust_id, //商户客户号
                'source_num' => $hui_pay_config->org_id, //渠道来源(机构号)
            ];

            $result = $huiPayBase->execute($huiPayBase->upload_url, $post_data, $sign_data, $hui_pay_config->private_key, $hui_pay_config->public_key);
//            var_dump($result);die;
            if ($result['status'] == '000000') {
                if (isset($result['data']) && !empty($result['data'])) {
                    $re_data = json_decode($result['data'], true);
                    return [
                        'status' => '1',
                        'message' => $result['message'],
                        'file_id' => $re_data['fileToken']
                    ];
                } else {
                    return [
                        'status' => '2',
                        'message' => $result['message']
                    ];
                }
            } else {
                return [
                    'status' => '2',
                    'message' => $result['message'],
                ];
            }

        } catch (\Exception $exception) {
            $this->status = '0';
            $this->message = $exception->getMessage() . $exception->getLine();
            return $this->format();
        }

    }


    //处理时间格式
    public function time_action($time, $set_time)
    {

        //去除中文
        $time = preg_replace('/([\x80-\xff]*)/i', '', $time);

        $is_date = strtotime($time) ? strtotime($time) : false;

        if ($is_date === false) {
            $time = $set_time; //默认时间
        } else {

        }

        return date('Y-m-d', strtotime($time));
    }


    /**
     * 哆啦宝图片图片转base64流并上传
     * @param string $filepath 图片路径
     * @param string $attach_type 附件类型
     * @param string $mch_num 商户编号
     * @param string $config_id
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function dlb_upload_img($filepath, $attach_type, $mch_num, $config_id = '1234')
    {
        try {
            $manager = new ManageController();
            $dlb_config = $manager->pay_config($config_id);
            if (!$dlb_config) {
                return json_encode([
                    'status' => 2,
                    'message' => '哆啦宝配置配置不存在请检查配置'
                ]);
            }

            $timestamp = time();
            $img_url_arr = explode('/', $filepath);
            $img_name = end($img_url_arr);
            $img = public_path() . '/upload/images/' . $img_name;

            $file_base64 = $this->img_2_base64($img);  //图片转base64
            $dlb_upload_data = [
                'attachType' => $attach_type, //附件类型
                'customerNum' => $mch_num, //商户编号
                'file' => $file_base64 //图片的流json格式,200k以内
            ];
            $attach_upload_token = $manager->dlb_sign($timestamp, $manager->dlb_upload, $dlb_upload_data, $dlb_config->secret_key);
            $dlb_upload_header = array(
                "Content-Type" => "application/json",
                "accessKey" => $dlb_config->access_key,
                "timestamp" => $timestamp,
                "token" => $attach_upload_token,
            );

            $dlb_upload_result = $manager->post_func($manager->dlb_host_url . $manager->dlb_upload, $dlb_upload_data, $dlb_upload_header);
            $dlb_result = json_decode($dlb_upload_result, true);
            if ($dlb_result['status'] == '1') {
                $result = json_decode($dlb_result['data'], true);
                if ($result['result'] == 'error') {
                    return json_encode([
                        'status' => 0,
                        'message' => $result['error']['errorCode']
                    ]);
                }

                return json_encode([
                    'status' => 1,
                    'data' => $result['data'],
                ]);
            } else {
                return [
                    'status' => 0,
                    'message' => $dlb_result['message']
                ];
            }
        } catch (\Exception $ex) {
            $this->status = '0';
            $this->message = $ex->getMessage() . $ex->getLine();
            return $this->format();
        }
    }


    public function base64EncodeImage($strTmpName)
    {
        return chunk_split(base64_encode(file_get_contents($strTmpName)));
    }


    /**
     * 图片转base64流
     * @param $img_file string 图片相对路径
     * @return \Illuminate\Http\JsonResponse|mixed|string
     */
    protected function img_2_base64($img_file)
    {
        $img_base64 = '';

        if (file_exists($img_file)) {
            $app_img_file = $img_file; // 图片路径
            $img_info = getimagesize($app_img_file); // 取得图片的大小，类型等

            $fp = fopen($app_img_file, "r"); // 图片是否可读权限
            if ($fp) {
                $filesize = filesize($app_img_file);
                $content = fread($fp, $filesize);
                $file_content = chunk_split(base64_encode($content)); // base64编码
                switch ($img_info[2]) {  //判读图片类型
                    case 1:
                        $img_type = "gif";
                        break;
                    case 2:
                        $img_type = "jpg";
                        break;
                    case 3:
                        $img_type = "png";
                        break;
                }

                $img_base64 = 'data:image/' . $img_type . ';base64,' . $file_content; //合成图片的base64编码
            }

            fclose($fp);
        } else {
            $this->status = '2';
            $this->message = '上传图片路径不存在';
            return $this->format();
        }

        $img_base64 = str_replace("\r\n", "", $img_base64); //去掉换行符

        return $img_base64; //返回图片的base64
    }


    //汇付时间格式处理
    protected function huiPayDate($time)
    {
        if ($time == '长期') {
            return '99991231';
        }
        return date('Ymd', strtotime($time));
    }


    //汇付apply_id生成40位以下，唯一值
    protected function createApplyId($len)
    {
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        $string = time();
        for (; $len >= 1; $len--) {
            $position = rand() % strlen($chars);
            $position2 = rand() % strlen($string);
            $string = substr_replace($string, substr($chars, $position, 1), $position2, 0);
        }

        $isExit = HuiPayStore::where('apply_id', $string)->first();
        if (!$isExit) {
            return $string;
        } else {
            $storepay = new StorePayWaysController();
            $storepay->createApplyId(20);
        }

    }


    //海科融通上传图片
    protected function hkrtUploadImage($image_path, $access_id, $access_key)
    {
        try {
            $imageBase64 = $this->img_2_base64($image_path);

            $upload_image_obj = new PayController();
            $merchant_image_upload_data = [
                'access_id' => $access_id,
                'imageBase64' => $imageBase64,
                'access_key' => $access_key
            ];
            $return_res = $upload_image_obj->merchant_image_upload($merchant_image_upload_data); //0-失败 1-成功
            if ($return_res['status'] == '1') {
                return [
                    'status' => '1',
                    'image_id' => $return_res['data']['image_id']
                ];
            } else {
                return [
                    'status' => '0',
                    'message' => $return_res['message']
                ];
            }
        } catch (\Exception $ex) {
            Log::info('海科融通上传图片-错误');
            Log::info($ex->getMessage());
        }
    }


    //海科融通 服务商申请单号
    protected function createAgentApplyNo($len)
    {
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        $string = time();
        for (; $len >= 1; $len--) {
            $position = rand() % strlen($chars);
            $position2 = rand() % strlen($string);
            $string = substr_replace($string, substr($chars, $position, 1), $position2, 0);
        }

        $isExit = HkrtStore::where('agent_apply_no', $string)->first();
        if (!$isExit) {
            return $string;
        } else {
            $storepay = new StorePayWaysController();
            $storepay->createAgentApplyNo(20);
        }

    }


    //通过支行名称获取银行地区码
    protected function getCityCodeBySubBankName($name)
    {
        if (!$name) {
            return [
                'status' => '0',
                'message' => '银行名称不存在'
            ];
        }

        $bank_infos = BankInfo::where('bankName', $name)
            ->first();
        if ($bank_infos) {
            return [
                'status' => '1',
                'message' => '查询成功',
                'data' => $bank_infos
            ];
        } else {
            return [
                'status' => '0',
                'message' => '银行名称未找到'
            ];
        }
    }


    //易生支付，时间格式
    protected function EasyPayTimeFormat($start_time, $end_time)
    {
        if ($start_time) {
            $start_time = str_replace('-', '.', $start_time);
        } else {
            $start_time = '';
        }

        if ($end_time) {
            if ($end_time == '长期' || $end_time == '9999-12-31') {
                $end_time = '2039.01.01';
            } else {
                $end_time = str_replace('-', '.', $end_time);
            }
        } else {
            $end_time = '';
        }

        //"[\"2012.01.19\", \"2032.01.19\"]"
        return ["$start_time", "$end_time"]; //["2007.03.08","2027.03.08"]
    }


    /**
     * 生成指定位数的随机数
     * @param int $length
     * @return int|string
     */
    protected function random_number($length = 8)
    {
        list($usec, $sec) = explode(' ', microtime());
        $ms = microtime(true) * 10000; //毫秒
        $mi = sprintf("%.0f", ((float)$usec + (float)$sec) * 100000000); //微秒

        $num = mt_rand(1, 9);

        $length = isset($length) ? $length : 8;
        for ($i = 0; $i < $length - 1; $i++) {
            $num .= mt_rand(0, 9);
        }

        $str = $num . $mi;
        $isExist = EasypayStore::where('opera_trace', $str)->first();
        if (!$isExist) {
            return $str;
        } else {
            $this->random_number($length);
        }
    }


}
