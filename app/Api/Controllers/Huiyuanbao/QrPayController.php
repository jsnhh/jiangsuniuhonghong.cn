<?php
namespace App\Api\Controllers\Huiyuanbao;


use App\Api\Controllers\Basequery\AdSelectController;
use App\Models\MemberCzList;
use App\Models\MemberList;
use App\Models\Order;
use App\Models\Store;
use function EasyWeChat\Kernel\Support\get_client_ip;
use function EasyWeChat\Kernel\Support\get_server_ip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class QrPayController extends BaseController
{

    //支付后跳转回调地址
    public function pay_action(Request $request)
    {


        try {


            $return = $request->all();
            $message = $request->get('message', '支付成功');
            $out_trade_no = $request->get('out_trade_no', '');
            $ad_p_id = '';
            $store_id = '';
            $ways_source = "";
            $total_amount = '';
            $device_id = "";
            $order = Order::where('out_trade_no', $out_trade_no)
                ->select('store_id', 'ways_source', 'total_amount', 'device_id')
                ->first();

            if ($order) {
                $store_id = $order->store_id;
                $ways_source = $order->ways_source;
                $total_amount = $order->total_amount;
                $device_id = $order->device_id;
            }

            if ($ways_source == "alipay") {
                //支付宝失败
                $ad_p_id = '3';
            } else {
                //微信失败
                $ad_p_id = '4';

            }
            //支付成功
            if (1 || $order->pay_status == 1) {
                if ($ways_source == "alipay") {
                    $ad_p_id = '1';
                } else {
                    $ad_p_id = '2';

                }
                $url = "&store_id=" . $store_id . '&total_amount=' . $total_amount . "&ad_p_id=" . $ad_p_id;
                $message = '支付成功';
                if ($device_id == "member_cz") {
                    $cz = "";
                    $cz_s = "";
                    $mb_money = "";

                    $MemberCzList = MemberCzList::where('store_id', $store_id)
                        ->where('out_trade_no', $out_trade_no)
                        ->select('cz_money', 'cz_s_money', 'mb_id')
                        ->first();
                    if ($MemberCzList) {
                        $cz = $MemberCzList->cz_money;
                        $cz_s = $MemberCzList->cz_s_money;
                        $MemberList = MemberList::where('store_id', $store_id)
                            ->where('mb_id', $MemberCzList->mb_id)
                            ->first();
                        if ($MemberList) {
                            $mb_money = $MemberList->mb_money;
                        }

                    }


                    $data_url = "&store_id=" . $store_id . '&total_amount=' . $total_amount . "&ad_p_id=" . $ad_p_id . '&cz=' . $cz . "&cz_s=" . $cz_s . "&mb_money=" . $mb_money;
                    $url = url('page/cz_success?message=支付成功') . $data_url;
                } else {
                    $url = url('/page/pay_success?message=') . $message . $url;
                }
            } //用户取消
            elseif ($order->pay_status == 2) {
                $url = "&store_id=" . $store_id . '&total_amount=' . $total_amount . "&ad_p_id=" . $ad_p_id;
                $message = '用户取消';
                $url = url('/page/pay_errors?message=') . $message . $url;

            } //失败
            else {
                $message = '支付失败';
                $url = "&store_id=" . $store_id . '&total_amount=' . $total_amount . "&ad_p_id=" . $ad_p_id;
                $url = url('/page/pay_errors?message=') . $message . $url;
            }

            return redirect($url);

        } catch (\Exception $exception) {

        }
    }


}
