<?php
namespace App\Api\Controllers\User;

use App\Api\Controllers\BaseController;
use App\Models\DomainNameSnNum;
use App\Models\DomainNameDeviceMaxNum;

use Illuminate\Http\Request;

class DevicePermissionsController extends BaseController
{
    /**
     * 查询设备是否有播报权限（同一个域名下设备是否达到最大数量）
     */
    public function getDevicePermissions(Request $request)
    {
        $requestData = $request->all();

        // 参数校验
        $checkData = [
            'domain_name'   => '域名',
            'sn_num'        => '设备编号',
        ];
        $check = $this->check_required($requestData, $checkData);
        if ($check) {
            return $this->sys_response(40000, $check);
        }

        // 公共返回信息
        $resData = [
            'domain_name'   => $requestData['domain_name'],
            'sn_num'        => $requestData['sn_num'],
        ];

        $model = new DomainNameDeviceMaxNum();
        $domainNameSnNumModel = new DomainNameSnNum();

        // 查询域名设定设备数量（最大绑定数量）
        $domainNameDeviceMaxNum = $model->getDomainNameDeviceMaxNum($requestData);
        if (empty($domainNameDeviceMaxNum)) {
            $msg = "该设备暂未设定设备数量";
            $resData['status'] = 2;
            return $this->sys_response(202, $msg, $resData);
        }
        $device_max_num = $domainNameDeviceMaxNum->device_max_num;

        // 设备信息
        $domainNameDeviceNumInfo = $domainNameSnNumModel->getDomainNameSnNumInfo($requestData);
        if ($domainNameDeviceNumInfo) {
            $msg = "该设备已绑定";
            $resData['status'] = 1;
            return $this->sys_response(200, $msg, $resData);
        }

        // 域名绑定的设备数量
        $domainNameDeviceNum = $domainNameSnNumModel->getDomainNameSnNumCount($requestData);

        // 如果设备数量小于设定数量
        if ($domainNameDeviceNum < $device_max_num) {
            $date = date("Y-m-d H:i:s", time());
            $insertData = [
                'domain_name'   => $requestData['domain_name'],
                'sn_num'        => $requestData['sn_num'],
                'created_at'    => $date,
                'updated_at'    => $date,
            ];
            $domainNameSnNumModel->addSn($insertData);
            if (!$domainNameSnNumModel) {
                $msg = "添加设备出错";
                $resData['status'] = 2;
                return $this->sys_response(203, $msg, $resData);
            }

            $code = 200;
            $msg = "操作成功";
            $resData['status'] = 1;
        } else {
            $code = 202;
            $msg = "该设备无权限";
            $resData['status'] = 2;
        }

        return $this->sys_response($code, $msg, $resData);
    }
}
