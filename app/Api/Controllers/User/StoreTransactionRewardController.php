<?php
/**
 * Created by PhpStorm.
 * User: daimingkang
 * Date: 2018/12/21
 * Time: 2:34 PM
 */

namespace App\Api\Controllers\User;


use App\Api\Controllers\BaseController;
use App\Models\ActivityStoreRate;
use App\Models\CashBackRule;
use App\Models\CashBackRuleUser;
use App\Models\Device;
use App\Models\Order;
use App\Models\Store;
use App\Models\StoreMonthOrder;
use App\Models\StorePayWay;
use App\Models\StoreTransactionReward;
use App\Models\TerminalReward;
use App\Models\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StoreTransactionRewardController extends BaseController
{

    public function storeTransactionRewardList(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $user_id = $user->user_id;
            $agent_id = $request->get('agent_id', '');
            $store_name = $request->get('store_name', '');
            $rule_id = $request->get('rule_id', '');
            $standard_status = $request->get('standard_status', '');
            $first_status = $request->get('first_status', '');
            $again_status = $request->get('again_status', '');

            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("store_transaction_reward as s");
            } else {
                $obj = DB::table('store_transaction_reward as s');
            }

            $where = [];
            if($user_id!=1){
                $where[] = ['s.agent_id', $user_id];
            }

             if ($agent_id) {
                 $where[] = ['s.agent_id', $agent_id];
             }
            if ($store_name) {
                $where[] = ['s.store_name', 'like', '%' . $store_name . '%'];
            }
            if ($rule_id) {
                $where[] = ['s.rule_id', $rule_id];
            }
            if ($standard_status) {
                $where[] = ['s.standard_status', $standard_status];
            }
            if ($first_status) {
                $where[] = ['s.first_status', $first_status];
            }
            if ($again_status) {
                $where[] = ['s.again_status', $again_status];
            }
            $obj = $obj->where($where)
                ->leftJoin('cash_back_rule as c', 's.rule_id', '=', 'c.id')
                ->orderBy('s.standard_status', 'desc')
                ->select('s.*','c.rule_name');
            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    public function getStoreTransactionReward(Request $request)
    {
        try {
            $store_id = $request->get('store_id');
            $check_data = [
                'store_id' => '店铺id'
            ];
            $check = $this->check_required($request->all(), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $obj = DB::table('store_transaction_reward as s')
                ->leftJoin('cash_back_rule as c', 's.rule_id', '=', 'c.id')
                ->where('s.store_id', $store_id)
                ->select('s.*', 'c.rule_name', 'c.start_time', 'c.end_time', 'c.standard_amt', 'c.standard_single_amt', 'c.standard_total',
                    'c.standard_term', 'c.first_amt', 'c.first_single_amt', 'c.first_total', 'c.again_amt', 'c.again_single_amt'
                    , 'c.again_single_amt', 'c.again_single_amt', 'c.again_total')
                ->first();
            $this->status = 1;
            $this->message = "数据返回成功";

            return $this->format($obj);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }


}