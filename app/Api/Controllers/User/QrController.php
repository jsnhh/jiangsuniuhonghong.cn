<?php
/**
 * Created by PhpStorm.
 * User: daimingkang
 * Date: 2017/10/24
 * Time: 下午1:35
 */

namespace App\Api\Controllers\User;


use App\Api\Controllers\BaseController;
use App\Models\AgentLevels;
use App\Models\AgentLevelsInfos;
use App\Models\QrCodeHb;
use App\Models\QrCodeUserRelation;
use App\Models\QrList;
use App\Models\QrListInfo;
use App\Models\Store;
use App\Models\StorePayWay;
use App\Models\User;
use App\Models\UserShoppingReward;
use App\Models\UserTransferInfo;
use Comodojo\Zip\Zip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Zxing\QrReader;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class QrController extends BaseController
{

    //
    public function QrLists(Request $request)
    {
        try {

            $user = $this->parseToken();

            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("qr_lists");
            } else {
                $obj = DB::table('qr_lists');
            }

            $obj = $obj->where('user_id', $user->user_id)->orderBy('created_at', 'desc');
            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);


        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }


    public function QrListinfos(Request $request)
    {
        try {
            $user = $this->parseToken();
            $cno = $request->get('cno');
            $status = $request->get('status', 0);
            $code_number = $request->get('code_number', '');
            $store_id = $request->get('store_id');
            $is_buy = $request->get('is_buy');
            $get_type = $request->get('get_type', 'pc');

            $where = [];

            if ($cno) {
                $where[] = ['qr_list_infos.cno', $cno];
            }
            if ($store_id) {
                $where[] = ['qr_list_infos.store_id', $store_id];
            }
            if ($status) {
                if ($status == '2') {
                    $status = '0';
                }
                $where[] = ['qr_list_infos.status', $status];

            }
            if ($user->user_id && $user->user_id != 1) {
                $where[] = ['qr_list_infos.user_id', $user->user_id];
            }
            if ($code_number) {
                $where[] = ['qr_list_infos.code_num', $code_number];
            }
            if ($is_buy) {
                $where[] = ['qr_list_infos.is_buy', $is_buy];
            }


            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("qr_list_infos");
            } else {
                $obj = DB::table('qr_list_infos');
            }

            $obj->leftJoin('stores', 'qr_list_infos.store_id', 'stores.store_id');
            $obj->leftJoin('users', 'qr_list_infos.user_id', 'users.id');
            $obj = $obj->where($where)
                ->select('qr_list_infos.*', 'stores.store_short_name', 'users.name as user_name')
                ->orderBy('qr_list_infos.cno', 'asc')
                ->orderBy('qr_list_infos.code_num', 'asc');
            if ($user->user_id && $user->user_id == 1) {
                $total_num = QrListInfo::select('id')->get();
                $total_bin_num = QrListInfo::where('status', '1')->select('id')->get();
                $total_nbin_num = QrListInfo::where('status', '0')->select('id')->get();
            } else {
                $total_num = QrListInfo::where('user_id', $user->user_id)->select('id')->get();
                $total_bin_num = QrListInfo::where('user_id', $user->user_id)->where('status', '1')->select('id')->get();
                $total_nbin_num = QrListInfo::where('user_id', $user->user_id)->where('status', '0')->select('id')->get();
            }
            $total_num = count($total_num);
            $total_bin_num = count($total_bin_num);
            $total_nbin_num = count($total_nbin_num);


            $this->t = $obj->count();
            $list = $this->page($obj)->get();
            $data = [
                'list' => $list,
                'total_num' => $total_num,
                'total_bin_num' => $total_bin_num,
                'total_nbin_num' => $total_nbin_num,
            ];
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);


        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }

    }


    //绑定空码
    public function bindQr(Request $request)
    {

        try {
            $userToken = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $code = $request->get('code', '');
            $code_number = $request->get('code_number', $code);
            $merchant_id = $request->get('merchant_id', '');
            $is_qr = substr($code_number, 0, 4);
            $user_id = $userToken->user_id;
            if ($is_qr == "http") {
                $url = basename($code_number);//获取链接
                $data = $this->getParams($url);
                $code_number = $data['no'];
            }
            $user = User::where('id', $user_id)->where('is_bind_qr', '1')->where('is_delete', '0')->first();
            if (!$user) {
                return json_encode([
                    'status' => -1,
                    'message' => '没有绑码权限，绑码状态为禁止或者代理不存在'
                ]);
            }
            $QrListInfo = QrListInfo::where('code_num', $code_number)->first();

            if (!$QrListInfo) {
                return json_encode(['status' => -1, 'message' => '二维码不存在']);
            }
            $store = Store::where('store_id', $store_id)
                ->where('user_id', $user_id)
                ->select('id', 'merchant_id', 'user_id', 'store_name', 'store_type')
                ->first();
            if (!$store) {
                return json_encode(['status' => 2, 'message' => '门店ID不存在']);
            }

            $storePayWay = StorePayWay::where('store_id', $store_id)
                ->where('status', 1)
                ->select('status')->get();
            if (!$storePayWay) {
                return json_encode(['status' => -1, 'message' => '商户未完成入网']);
            }

            if ($store->user_id != $QrListInfo->user_id) {
                return json_encode(['status' => -1, 'message' => '该码牌不属于你']);
            }

            if ($QrListInfo->status) {
                $store_id = $QrListInfo->store_id;
                return json_encode(['status' => -1, 'message' => '二维码已经被' . $store_id . '绑定']);
            }
            if (!$merchant_id || $merchant_id == "NULL") {
                $merchant_id = $store->merchant_id;
            }
            $dataInfo['status'] = 1;
            $dataInfo['status_desc'] = '已绑定';
            $dataInfo['merchant_id'] = $merchant_id;
            $dataInfo['bind_time'] = date('Y-m-d H:i:m', time());
            $dataInfo['store_id'] = $store_id;
            //开启事务
            try {
                DB::beginTransaction();
//                $user = User::where('id', $QrListInfo->user_id)->first();
//                $activation_integral = $user->activation_integral + 10;
//                User::where('id', $QrListInfo->user_id)->update(['activation_integral' => $activation_integral]);//修改本级代理积分
//                $reward_info = [
//                    'code_num' => $QrListInfo->code_num,
//                    'user_id' => $QrListInfo->user_id,
//                    'award_amount' => 0,
//                    'store_id' => $store_id,
//                    'store_name' => $store->store_name,
//                    'status' => '1',
//                    'status_desc' => '本级奖励',
//                    'activation_integral' => 10
//                ];
//                QrUserRewardInfo::create($reward_info);
                QrListInfo::where('code_num', $code_number)->update($dataInfo);
                $QrList = QrList::where('cno', $QrListInfo->cno)->first();
                $s_num = $QrList->s_num;
                $QrList->s_num = $s_num + 1;
                $QrList->save();
                DB::commit();
                return json_encode(['status' => 1, 'message' => '绑定收款二维码成功']);

            } catch (\Exception $e) {
                Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
                DB::rollBack();
                return json_encode([
                    'status' => '-1',
                    'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
                ]);
            }


        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }


    //解绑定空码
    public function unbindQr(Request $request)
    {

        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $code_number = $request->get('code_number', '');
            $QrListInfo = QrListInfo::where('code_num', $code_number)->first();
            if (!$QrListInfo) {
                return json_encode(['status' => 2, 'message' => '二维码不存在']);
            }

//            if ($QrListInfo->code_type == 0) {
//                return json_encode(['status' => 2, 'message' => '二维码未绑定任何门店']);
//            }
            $dataInfo = [
                'store_id' => '',
                'status' => '0',
                'status_desc' => '未绑定'
            ];
            QrListInfo::where('code_num', $code_number)->update($dataInfo);

            Cache::forget($code_number);


            return json_encode(['status' => 1, 'message' => '收款二维码解绑成功']);


        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }

    }


    public function DownloadQr(Request $request)
    {
        try {
            $user = $this->parseToken();

            $cno = $request->get('cno');
            $list = QrList::where('cno', $cno)->first();
            if (!$list) {
                return json_encode(['status' => 2, 'message' => '没有任何相关数据']);
            }
            $zip = Zip::create($cno . '.zip');
            $zip->add(public_path() . '/QrCode/' . $cno . '/', true);
            return json_encode(['status' => 1, 'data' => url('/' . $cno . '.zip')]);

        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }


    //个人码合并列表
    public function qr_code_hb_list(Request $request)
    {
        try {
            $user = $this->parseToken();


            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("qr_code_hbs");
            } else {
                $obj = DB::table('qr_code_hbs');
            }

            $code_name = $request->get('code_name');
            $where = [];

            if ($code_name) {
                $where[] = ['code_name', 'like', '%' . $code_name . '%'];
            }

            if ($user->user_id) {
                $where[] = ['user_id', '=', $user->user_id];
            }

            $obj = $obj->where($where)->orderBy('created_at', 'desc');
            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);


        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }


    //个人码合并列表
    public function qr_code_hb_add(Request $request)
    {
        try {

            return json_encode([
                'status' => 2,
                'message' => '暂不对外开放'
            ]);

            $user = $this->parseToken();
            $code_name = $request->get('code_name');
            $ali_code_url = $request->get('ali_code_url');
            $wx_code_url = $request->get('wx_code_url');
            $user_id = $user->user_id;

            $ali = $request->get('ali_code_url');
            $wx = $request->get('wx_code_url');

            if (!$ali_code_url || !$wx_code_url || !$code_name) {
                return json_encode([
                    'status' => 1,
                    'message' => '信息填写完整'
                ]);
            }


            $ali_code_url = explode('/', $ali_code_url);
            $ali_code_url = end($ali_code_url);
            $ali_code_url = public_path() . '/upload/images/' . $ali_code_url;


            $wx_code_url = explode('/', $wx_code_url);
            $wx_code_url = end($wx_code_url);
            $wx_code_url = public_path() . '/upload/images/' . $wx_code_url;


            $a = new QrReader($ali_code_url);
            $a = $a->text(); //return decoded text from QR Code

            $b = new QrReader($wx_code_url);
            $b = $b->text(); //return decoded text from QR Code


            if (!$a) {
                return json_encode([
                    'status' => 1,
                    'message' => '支付宝二维码不清楚'
                ]);
            }

            if (!$b) {
                return json_encode([
                    'status' => 1,
                    'message' => '微信二维码不清楚'
                ]);
            }


            $data = [
                'code_name' => $code_name,
                'user_id' => $user_id,
                'ali_code_url' => $ali,
                'wx_code_url' => $wx,
                'ali_url' => $a,
                'wx_url' => $b,
                'hb_url' => url('/qr_code_hb'),
            ];

            QrCodeHb::create($data);

            return json_encode([
                'status' => 1,
                'message' => '二维码添加成功'
            ]);

        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }


    //个人码合并列表
    public function qr_code_hb_del(Request $request)
    {
        try {
            $user = $this->parseToken();
            $id = $request->get('id');

            QrCodeHb::where('id', $id)
                ->where('user_id', $user->user_id)
                ->delete();

            return json_encode([
                'status' => 1,
                'message' => '二维码删除成功'
            ]);


        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }


    public function getParams($url)
    {

        $refer_url = parse_url($url);

        $params = $refer_url['query'];

        $arr = array();
        if (!empty($params)) {
            $paramsArr = explode('&', $params);

            foreach ($paramsArr as $k => $v) {
                $a = explode('=', $v);
                $arr[$a[0]] = $a[1];
            }
        }
        return $arr;
    }

    //码牌划拨
    public function transferCode(Request $request)
    {
        $token = $this->parseToken();
        $phone = $request->get('phone');
        $type = $request->get('type');
        $code_num = $request->get('code_num');
        $p_id = $request->get('p_id');
        $codeArray = explode(',', $code_num);
        if (!$p_id) {
            $p_id = $token->user_id;
        }
        $user = User::where('phone', $phone)->where('is_delete', '0')->select('*')->first();
        if (!$user) {
            return [
                'status' => '-1',
                'message' => '合作伙伴不存在'
            ];
        }
        $user_id = $user->id;
        $codeArrayLength = count($codeArray);
        //Log::info($codeArrayLength);
        //检查码牌是否属于代理商----开始-----
        $qr_code_s = QrListInfo::where('status', '0')
            ->where('user_id', $p_id)
            ->whereIn('code_num', $codeArray)
            ->get()->toArray();
        if ($codeArrayLength != count($qr_code_s)) {
            return json_encode([
                'status' => -1,
                'message' => '请检查划拨的码牌中是否有以下问题1.是否已绑定，2.是否属于自己，3.码牌编号是否正确'
            ]);
        }
        //检查码牌是否属于代理商----结束-----
        $agentLevel = AgentLevels::where('code_num', '<=', $codeArrayLength)
            ->orderBy('code_num', 'desc')
            ->first();
        $level = $agentLevel->level;
        $level_weight = $agentLevel->level_weight;
        $reward_month = 0;
        $award_amount = 0;
        if ($agentLevel->reward_month) {
            $reward_month = $agentLevel->reward_month;
        }
        if ($agentLevel->activation_amount) {
            $award_amount = $agentLevel->activation_amount;
        }

        $p_amount = 5;
        if ($p_id == 1) {
            $p_amount = 20;
        } else {
            $p_user_level_info = AgentLevelsInfos::where('user_id', $p_id)->first();
            $p_user_level = AgentLevels::where('level_weight', $p_user_level_info->level_weight)->first();
            if ($p_user_level->activation_amount) {
                $p_amount = $p_user_level->activation_amount;
            }
        }

        foreach ($codeArray as $value) {
            $this->setQrCodeUserRelation($user_id, $value, $p_id, $level, $award_amount, $p_amount);
        }
        $code_start_res = array_slice($qr_code_s, 0, 1);
        $code_start = $code_start_res[0]['code_num'];
        $code_end_res = end($qr_code_s);
        $code_end = $code_end_res['code_num'];
        $transfer_info_data = [
            'user_id' => $p_id,
            'sub_user_id' => $user_id,
            'sub_user_name' => $user->name,
            'transfer_type' => '1',
            'transfer_num' => $codeArrayLength,
            'code_start' => $code_start,
            'code_end' => $code_end
        ];
        UserTransferInfo::create($transfer_info_data);
        if ($reward_month != 0) {
            $start_time = date('Y-m-d', strtotime(date('Y-m-01', time())));
            $end_time = date('Y-m-d', strtotime(date('Y-m-01', strtotime($start_time)) . ' +' . $reward_month . ' month -1 day'));
        } else {
            $start_time = date('Y-m-d', time());
            $end_time = date('Y-m-d', time());
        }

        $levelInfo = [
            'level' => $level,
            'level_weight' => $level_weight,
            'user_id' => $user_id,
            'start_time' => $start_time,
            'end_time' => $end_time,
        ];
        $agentLevelsInfo = AgentLevelsInfos::where('user_id', $user_id)->select('*')->first();
        if ($agentLevelsInfo) {
            $current_data = date('Y-m-d', time());
            if (strtotime($agentLevelsInfo->end_time) > strtotime($levelInfo['start_time'])) {
                $levelInfo['up_status'] = '2';
                $levelInfo['up_level'] = $agentLevelsInfo->level_weight;
            }
            if ($level_weight >= $agentLevelsInfo->level_weight) {
                AgentLevelsInfos::where('user_id', $user_id)->update($levelInfo);
            } else {
                if (strtotime($agentLevelsInfo->end_time) < strtotime($current_data)) {
                    AgentLevelsInfos::where('user_id', $user_id)->update($levelInfo);
                }
            }
        } else {
            AgentLevelsInfos::create($levelInfo);
        }


        $sub_reward_num = floor($codeArrayLength / 50) - 1;
        if ($sub_reward_num >= 1) {
            $user_shopping_reward_num = $user->shopping_reward_num + $sub_reward_num;
            User::where('id', $user_id)->update(['shopping_reward_num' => $user_shopping_reward_num]);
        }
        $p_user = User::where('id', $user->pid)->select('*')->first();
        if ($p_user->shopping_reward_num < 1) {
            return json_encode([
                'status' => 1,
                'message' => '划拨成功'
            ]);
        }
        $qrListInfo = QrListInfo::where('user_id', $user->pid)->select('id')->get();
        if ($codeArrayLength >= 50 && count($qrListInfo) >= 50) {
            $userShoppingReward = UserShoppingReward::where('user_id', $user_id)->select('user_id')->first();
            if (!$userShoppingReward) {
                $shoppingRewardDate = [
                    'user_id' => $user->id,
                    'p_user_id' => $user->pid,
                    'p_user_name' => $user->pid_name,
                    'amount' => 150,
                    'quantity' => $codeArrayLength,
                ];
                UserShoppingReward::create($shoppingRewardDate);

                $activation_integral = $p_user->activation_integral + 150;
                $shopping_reward_num = $p_user->shopping_reward_num - 1;
                User::where('id', $user->pid)->update(['activation_integral' => $activation_integral, 'shopping_reward_num' => $shopping_reward_num]);
            }
        }
        return json_encode([
            'status' => 1,
            'message' => '划拨成功'
        ]);

        //码牌奖励调整---结束----
    }

    //码牌回拨
    public function backCode(Request $request)
    {
        $token = $this->parseToken();
        $code_num = $request->get('code_num');
        $codeArray = explode(',', $code_num);
        $users = DB::table('qr_list_infos')
            ->whereIn('code_num', $codeArray)
            ->select('user_id')
            ->distinct()
            ->get();
        if (count($users) >= 2) {
            return json_encode([
                'status' => 2,
                'message' => '回拨失败，存在多个代理'
            ]);
        }
        $user_id = '';
        foreach ($users as $user) {
            $user_id = $user->user_id;
        }
        $qr_code_s = QrListInfo::where('status', '0')
            ->where('user_id', $user_id)
            ->whereIn('code_num', $codeArray)
            ->orderBy('code_num', 'asc')
            ->get()->toArray();
        $codeArrayLength = count($codeArray);
        $sub_reward_num = floor($codeArrayLength / 50) - 1;
        $shopping_reward_num = User::where('id', $user_id)->select('shopping_reward_num')->first();
        if ($shopping_reward_num->shopping_reward_num < $sub_reward_num) {
            User::where('id', $user_id)->update(['shopping_reward_num' => 0]);
        } else {
            $sub_reward_num = $shopping_reward_num - $sub_reward_num;
            User::where('id', $user_id)->update(['shopping_reward_num' => $sub_reward_num]);
        }
        $code_start_res = array_slice($qr_code_s, 0, 1);
        $code_start = $code_start_res[0]['code_num'];
        $code_end_res = end($qr_code_s);
        $code_end = $code_end_res['code_num'];
        $transfer_info_data = [
            'user_id' => $user_id,
            'sub_user_id' => 1,
            'sub_user_name' => '江苏牛付电子商务有限公司',
            'transfer_type' => '2',
            'transfer_num' => $codeArrayLength,
            'code_start' => $code_start,
            'code_end' => $code_end
        ];
        UserTransferInfo::create($transfer_info_data);
        foreach ($codeArray as $value) {
            $update_qr = [
                'user_id' => 1,
                'is_buy' => '0'
            ];
            QrListInfo::where('code_num', $value)->update($update_qr);
        }

        return json_encode([
            'status' => 1,
            'message' => '回拨成功'
        ]);
    }

    //设置码牌代理商奖励关联关系
    public function setQrCodeUserRelation($user_id, $code_num, $p_id, $level, $award_amount, $p_amount)
    {
        $update_qr = [
            'user_id' => $user_id,
            'is_buy' => '2'
        ];
        QrListInfo::where('code_num', $code_num)->update($update_qr);

        $qrInfoDate = [
            'code_num' => $code_num,
            'user_id' => $user_id,
            'p_id' => $p_id,
            'level' => $level,
            'award_amount' => $award_amount,
            'amount' => $p_amount,//如果码牌奖励关联表中没有数据码牌归属于平台，初始奖励金额为20
            'status' => '2'
        ];
        QrCodeUserRelation::create($qrInfoDate);

    }

    //查看激活码牌列表
    public function getActivationQr(Request $request)
    {
        try {
            $user = $this->parseToken();
            //$phone = $request->get('phone', '');
            $store_name = $request->get('store_name');
            $user_id = $request->get('user_id');
            if (!$user_id) {
                $user_id = $user->user_id;
            }

            $where = [];
            $where[] = ['qr_list_infos.status', '1'];
            if ($user_id) {
                $where[] = ['qr_list_infos.user_id', $user_id];
            }
            if ($store_name) {
                if (is_numeric($store_name)) {
                    $where[] = ['stores.people_phone', '=', $store_name];
                } else {
                    $where[] = ['stores.store_name', 'like', '%' . $store_name . '%'];
                }

            }
            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("qr_list_infos");
            } else {
                $obj = DB::table('qr_list_infos');
            }

            $obj->leftJoin('stores', 'qr_list_infos.store_id', 'stores.store_id');
            $obj = $obj->where($where)
                ->select('qr_list_infos.*', 'stores.store_name', 'stores.people_phone')
                ->orderBy('qr_list_infos.bind_time', 'desc');
            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);


        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }

    }

    public function createQr(Request $request)
    {
        try {
            $user = $this->parseToken();
            //生成的批次
            $cno = date("YmdHis", time()) . rand(1000, 9999);
            //生成数量
            $num = $request->get('num', 100);
            if ($num > 1000) {
                return json_encode([
                    'status' => -1,
                    'message' => '生成数量控制在1000以内'
                ]);
            }
            $random = date('YmdHi', time());
            //循环插入插入数据库表qrListInfos
            for ($i = 1; $i <= $num; $i++) {
                $no = '';
                if ($i < 10) {
                    $no = '0000' . $i;
                } else if ($i >= 10 && $i < 100) {
                    $no = '000' . $i;
                } else if ($i >= 100 && $i < 1000) {
                    $no = '00' . $i;
                } else if ($i >= 1000) {
                    $no = '0' . $i;
                }
                $code_number = 'NO_' . $random . $no;//编号
                $qrListInfo = QrListInfo::where('code_num', $code_number)->first();
                if ($qrListInfo) {
                    return json_encode([
                        'status' => -1,
                        'message' => '请过一分钟后重试'
                    ]);
                }

                try {
                    //生成二维码文件
                    if (!is_dir(public_path('QrCode/' . $cno . '/'))) {
                        mkdir(public_path('QrCode/' . $cno . '/'), 0777);
                    }
                    $this->createImg($cno, $code_number);

                } catch (\Exception $exception) {
                    return json_encode([
                        'status' => 2,
                        'message' => $exception->getMessage()
                    ]);

                }
                try {
                    QrListInfo::create([
                        'cno' => $cno,
                        'user_id' => $user->user_id,
                        'code_num' => $code_number,
                        'status_desc' => '未绑定',
                    ]);
                } catch (\Exception $exception) {
                    return json_encode([
                        'status' => 2,
                        'message' => '插入数据库表qrListInfos失败'
                    ]);
                }
            }
            try {
                QrList::create([
                    'cno' => $cno,
                    'user_id' => $user->user_id,
                    'num' => $num,
                    's_num' => 0,
                ]);
            } catch (\Exception $exception) {
                return json_encode([
                    'status' => 2,
                    'message' => '插入数据库表qrLists失败'
                ]);
            }
            return json_encode([
                'status' => 1,
                'message' => '生成二维码成功'
            ]);

        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }

    public function createImg($cno, $code_number)
    {
        try {
            $font = public_path('font/font.ttf');
            $font_size = 12;

            $conn = imagecreatetruecolor(460, 460);//创建画布
            $color = imagecolorallocate($conn, 255, 255, 255);//创建颜色(白)
            imagefill($conn, 0, 0, $color);//颜色填充

            $qr_text = url('/qr?no=' . $code_number);//生成二维码的内容;
            $qr_url = public_path('QrCode/' . $cno . '/' . $code_number . '_qr.png');//生成二维码的路径
            QrCode::size(400)->format('png')->generate($qr_text, $qr_url);//生成二维码
            $qr_code = imagecreatefromstring(file_get_contents($qr_url));//生成一个新的图像

            $txt_max_width = intval(0.8 * 400);
            $content = "";
            for ($i = 0; $i < mb_strlen($code_number); $i++) {
                $letter[] = mb_substr($code_number, $i, 1);
            }
            foreach ($letter as $l) {
                $test_str = $content . " " . $l;
                $test_box = imagettfbbox($font_size, 0, $font, $test_str);
                // 判断拼接后的字符串是否超过预设的宽度。超出宽度添加换行
                if (($test_box[2] > $txt_max_width) && ($content !== "")) {
                    $content .= "\n";
                }
                $content .= $l;
            }

            $txt_width = $test_box[2] - $test_box[0];
            $y = 440; // 文字从何处的高度开始
            $x = (460 - $txt_width) / 2; //文字居中
            $black = imagecolorallocate($conn, 0, 0, 0);//字体颜色
            imagettftext($conn, $font_size, 0, $x, $y, $black, $font, $content); //写 TTF 文字到图中

            imagecopymerge($conn, $qr_code, 30, 15, 0, 0, imagesx($qr_code), imagesy($qr_code), 100);//合并图像
            $qr_img_url = public_path('QrCode/' . $cno . '/' . $code_number . '.png');
            imagepng($conn, $qr_img_url);//存储合成的二维码

            unlink($qr_url);//删除生成的二维码
        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }

    }

    //不分页
    public function getUserQrList(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id', '');
            $code_number = $request->get('code_number', '');
            $where = [];

            if ($user_id) {
                $where[] = ['user_id', $user_id];
            }
            if ($code_number) {
                $where[] = ['code_num', $code_number];
            }
            $qrListInfo = QrListInfo::where($where)->where('status', '0')->select('*')->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($qrListInfo);


        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }

    }

    //修改代理购买码牌奖励次数
    public function upShoppingRewardNum(Request $request)
    {
        try {
            $user_id = $request->get('user_id', '');
            $shopping_reward_num = $request->get('shopping_reward_num', 0);

            if (!$user_id) {
                return json_encode(['status' => 2, 'message' => '请选择代理']);
            }
            if (!$shopping_reward_num) {
                if($shopping_reward_num==0){
                    $res = User::where('id', $user_id)->update(['shopping_reward_num' => 0]);
                    if ($res) {
                        return json_encode(['status' => 1, 'message' => '修改成功']);
                    }
                }
                return json_encode(['status' => -1, 'message' => '请输入代理奖励次数']);
            }
            $res = User::where('id', $user_id)->update(['shopping_reward_num' => $shopping_reward_num]);
            if ($res) {
                return json_encode(['status' => 1, 'message' => '修改成功']);
            }
            return json_encode(['status' => 2, 'message' => '修改失败']);
        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }

}
