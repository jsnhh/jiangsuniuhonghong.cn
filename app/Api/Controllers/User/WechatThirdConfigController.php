<?php
namespace App\Api\Controllers\User;

use Illuminate\Http\Request;
use App\Api\Controllers\BaseController;
use App\Models\WechatThirdConfig;

class WechatThirdConfigController extends BaseController
{
    // 微信第三方平台配置
    public function wechatThirdConfigInfo()
    {
        $model = new WechatThirdConfig();

        $data = $model->getInfo();

        return $this->sys_response_layui(1, '操作成功', $data);
    }

    // 保存信息
    public function saveWechatThirdConfig(Request $request)
    {
        $requestData = $request->all();

        $date = date('Y-m-d H:i:s', time());
        $upData = [
            'app_id'                => $requestData['appid'],
            'app_secret'            => $requestData['secret'],
            'applet_version'        => $requestData['applet_version'],
            'applet_template_id'    => $requestData['applet_template_id'],
            'wechat_server_appid'   => $requestData['wechat_server_appid'],
            'wechat_server_secret'  => $requestData['wechat_server_secret'],
            'token'                 => $requestData['wechat_token'],
            'aes_key'               => $requestData['aes_key'],
            'updated_at'            => $date,
            'company_applet_version'        => $requestData['company_applet_version'],
            'company_applet_template_id'    => $requestData['company_applet_template_id'],
        ];

        $model = new WechatThirdConfig();
        // 查询是否已经有数据了
        $data = $model->getInfo();

        if ($data) {
            $res = $model->updateData($upData);
        } else {
            $upData['config_id'] = 1234;
            $upData['created_at'] = $date;
            $res = $model->saveData($upData);
        }

        if ($res) {
            return $this->sys_response_layui(1, '操作成功');
        } else {
            return $this->sys_response_layui(202, '操作失败');
        }
    }
}
