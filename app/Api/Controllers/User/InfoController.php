<?php
namespace App\Api\Controllers\User;


use App\Api\Controllers\BaseController;
use App\Models\AppMerchantIndex;
use App\Models\AppMyIndex;
use App\Models\Map;
use App\Models\NoticeNew;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class InfoController extends BaseController
{

    public function notice_news(Request $request)
    {

        try {
            $user = $this->parseToken();
            $user_id = $user->user_id;
            $type = $request->get('type', '');
            $title = $request->get('title', '');



            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("notice_news");
            } else {
                $obj = DB::table('notice_news');
            }
            $obj->join('users', 'notice_news.user_id', '=', 'users.id');

            $where[] = ['notice_news.user_id', '=', $user_id];


            if ($type) {
                $where[] = ['notice_news.type', '=', $type];
            }

            if ($title) {
                $where[] = ['notice_news.title', 'like', '%' . $title . '%'];

            }
            $obj = $obj->where($where)
                ->whereIn('notice_news.user_id', $this->getSubIds($user_id))
                ->orderBy('notice_news.updated_at', 'desc');
            $this->t = $obj->count();
            $data = $this->page($obj)
                ->select(
                    'notice_news.id',
                    'users.name as user_name',
                    'notice_news.title', 'notice_news.desc',
                    'notice_news.type', 'notice_news.redirect_url',
                    'notice_news.created_at',
                    'notice_news.type_desc',
                    'notice_news.icon_url'
                )
                ->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    public function add_notice_news(Request $request)
    {

        try {
            $user = $this->parseToken();
            $data = $request->except(['token']);
            $check_data = [
                'title' => '标题',
                'type' => '类型',
                'redirect_url' => '链接'
            ];
            $check = $this->check_required($data, $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $data['config_id'] = $user->config_id;
            $data['user_id'] = $user->user_id;

            NoticeNew::create($data);
            $this->status = 1;
            $this->message = '添加成功';
            return $this->format();

        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    public function del_notice_news(Request $request)
    {

        try {
            $user = $this->parseToken();
            $id = $request->get('id');
            NoticeNew::where('id', $id)->delete();
            $this->status = 1;
            $this->message = '删除成功';
            return $this->format();

        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    //头条精选
    public function toutiao_top(Request $request)
    {

        try {
            $user = $this->parseToken();
            $user_id = $user->user_id;

            //app 展示上级
            //广告部分
            $users = [];
            //配置ID-广告
            $user_id_abc = $user_id;
            $i = [1, 2, 3, 4, 5];
            foreach ($i as $k => $v) {

                $user = User::where('id', $user_id_abc)
                    ->select('id', 'pid')
                    ->first();
                if ($user) {
                    $users[$k]['user_id'] = $user->id;
                    $user_id_abc = $user->pid;
                }
            }

            $obj_data = DB::table('notice_news')
                ->whereIn('user_id', $users)
                ->where('type', 'jx')
                ->take(3)
                ->get();


            //
            if (!$obj_data->isEmpty()) {
                foreach ($obj_data as $k => $v) {
                    $data[] = [
                        'title' => $v->title,
                        'rd' => "1",
                        'dz' => '1',
                        'img_url' => $v->icon_url,
                        'url' => $v->redirect_url,
                    ];
                }
            } else {
                //默认的
                $data = [
                    [
                        'title' => '万亿商机强势来袭，你准备好了吗？',
                        'rd' => '3049',
                        'dz' => '456',
                        'img_url' => 'https://sf6-ttcdn-tos.pstatp.com/img/web.business.image/201808025d0d168e574fc3bb4638945b~640x0.image',
                        'url' => 'https://linkmarket.aliyun.com/index.html?source=ruidao_toutiao_pc',
                    ], [
                        'title' => '万亿商机强势来袭，你准备好了吗？',
                        'rd' => '3049',
                        'dz' => '456',
                        'img_url' => 'https://sf6-ttcdn-tos.pstatp.com/img/web.business.image/201808025d0d168e574fc3bb4638945b~640x0.image',
                        'url' => 'https://linkmarket.aliyun.com/index.html?source=ruidao_toutiao_pc',
                    ], [
                        'title' => '万亿商机强势来袭，你准备好了吗？',
                        'rd' => '3049',
                        'dz' => '456',
                        'img_url' => 'https://sf6-ttcdn-tos.pstatp.com/img/web.business.image/201808025d0d168e574fc3bb4638945b~640x0.image',
                        'url' => 'https://linkmarket.aliyun.com/index.html?source=ruidao_toutiao_pc',
                    ]
                ];
            }

            return json_encode([
                'status' => 1,
                'message' => '数据返回成功',
                'data' => $data
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'message' => '短信验证码不匹配'
            ]);
        }
    }


    public function notice_news_type()
    {
        try {
            $data = [
                [
                    'type' => 'news',
                    'type_desc' => '新闻',
                ], [
                    'type' => 'notice',
                    'type_desc' => '公告',
                ], [
                    'type' => 'jx',
                    'type_desc' => '头条精选',
                ]
            ];
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);


        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    //头条链接
    public function toutiao(Request $request)
    {

        try {
            $user = $this->parseToken();
            $url = Cache::get('user_tt_set');
            if (!$url) {
                $url = 'https://www.qq.com/?fromdefault';
            }
            return json_encode([
                'status' => 1,
                'message' => '数据返回成功',
                'data' => [
                    'url' => $url
                ]
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //app首页功能区 列表
    public function merchant_index(Request $request)
    {
        try {
            $user = $this->parseToken();
            $config_id = $user->config_id;

            $data = AppMerchantIndex::where('config_id', $config_id)
//                ->where('pid', 2)
//                ->where('type', 'url')
//                ->groupBy('pid')
                ->orderBy('sort')
                ->get();
            if ($data) {
                return json_encode([
                    'status' => '1',
                    'message' => '数据返回成功',
                    'data' => $data
                ]);
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '数据返回失败',
                    'data' => []
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '2',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    //app首页功能区 添加
    public function add_merchant_index(Request $request)
    {
        try {
            $user = $this->parseToken();
            $type = $request->get('type', 'url');
            $pid = $request->get('pid', '2');
            $title = $request->get('title', '');
            $icon = $request->get('icon', '');
            $url = $request->get('url', '');
            $sort = $request->get('sort', '');

            $in_data = [
                'type' => $type,
                'pid' => $pid,
                'config_id' => $user->config_id,
                'title' => $title,
                'icon' => $icon,
                'url' => $url,
                'sort' => $sort
            ];

            $res = AppMerchantIndex::create($in_data);
            if ($res) {
                return json_encode([
                    'status' => '1',
                    'message' => '数据添加成功'
                ]);
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '数据添加失败',
                    'data' => $in_data
                ]);
            }
        } catch (\Exception $exception) {
            return json_encode([
                'status' => '2',
                'message' => $exception->getMessage()
            ]);
        }
    }


    //app首页功能区 删除
    public function del_merchant_index(Request $request)
    {
        try {
            $user = $this->parseToken();
            $id = $request->get('id', '');

            $res = AppMerchantIndex::where('id', $id)->delete();
            if ($res) {
                return json_encode([
                    'status' => '1',
                    'message' => '数据删除成功'
                ]);
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '数据删除失败'
                ]);
            }
        } catch (\Exception $exception) {
            return json_encode([
                'status' => '-1',
                'message' => $exception->getMessage()
            ]);
        }
    }


    //app我的功能区 列表
    public function my_index(Request $request)
    {
        try {
            $user = $this->parseToken();
            $config_id = $user->config_id;

            $data = AppMyIndex::where('config_id', $config_id)
//                ->where('type', 'url')
                ->orderBy('sort', 'ASC')
                ->get();
            if ($data) {
                return json_encode([
                    'status' => '1',
                    'message' => '数据返回成功',
                    'data' => $data
                ]);
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '数据返回成功',
                    'data' => []
                ]);
            }
        } catch (\Exception $exception) {
            return json_encode([
                'status' => '2',
                'message' => $exception->getMessage()
            ]);
        }
    }


    //app我的功能区 添加
    public function add_my_index(Request $request)
    {
        try {
            $user = $this->parseToken();
            $title = $request->get('title', '');
            $icon = $request->get('icon', '');
            $url = $request->get('url', '');
            $sort = $request->get('sort', '');

            $in_data = [
                'type' => 'url',
                'config_id' => $user->config_id,
                'title' => $title,
                'icon' => $icon,
                'url' => $url,
                'sort' => $sort
            ];
            $res = AppMyIndex::create($in_data);
            if ($res) {
                return json_encode([
                    'status' => '1',
                    'message' => '数据添加成功'
                ]);
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '数据添加失败',
                    'data' => $in_data
                ]);
            }
        } catch (\Exception $exception) {
            return json_encode([
                'status' => '2',
                'message' => $exception->getMessage()
            ]);
        }
    }


    //app我的功能区 删除
    public function del_my_index(Request $request)
    {
        try {
            $user = $this->parseToken();
            $id = $request->get('id', '');

            $res = AppMyIndex::where('id', $id)->delete();
            if ($res) {
                return json_encode([
                    'status' => '1',
                    'message' => '数据删除成功'
                ]);
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '数据删除失败'
                ]);
            }
        } catch (\Exception $exception) {
            return json_encode([
                'status' => '2',
                'message' => $exception->getMessage()
            ]);
        }
    }


    //app我的功能区 头条链接设置
    public function user_tt_set(Request $request)
    {
        try {
            $user = $this->parseToken();
            $url = $request->get('url', '');

            if ($url) {
                //修改
                Cache::forget('user_tt_set');
                Cache::forever('user_tt_set', $url);

                return json_encode([
                    'status' => '1',
                    'message' => '修改成功',
                    'data' => [
                        'url' => $url
                    ]
                ]);
            } else {
                $url = Cache::get('user_tt_set');

                return json_encode([
                    'status' => '1',
                    'message' => '查询成功',
                    'data' => [
                        'url' => $url
                    ]
                ]);
            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //地图名称编辑
    public function mapEditName(Request $request)
    {
        try {
            $type = $request->get('type', '2'); //2-查询 3-编辑
            $id = $request->get('id', '');
            $nameList = $request->get('nameList', '{}');

            if ($type == '2') {
                $dataArr = Map::get();

                $this->status = 1;
                $this->message = '返回数据成功';
                return $this->format($dataArr);
            }

            if (!$nameList || $nameList == '{}') {
                $this->status = 2;
                $this->message = '名字不能为空';
                return $this->format();
            }

            $nameList = trim($nameList);
            if (is_array($nameList)) {
                $nameList = json_encode($nameList, 320);
            }

            if ($type == '3') {
                if (!$id) {
                    $this->status = 2;
                    $this->message = '地图id不能为空';
                    return $this->format();
                }

                $map_obj = Map::where('id', $id)->first();
                if ($map_obj) {
                    $res = $map_obj->update([
                        'name' => $nameList
                    ]);
                    $this->status = 1;
                    $this->message = '修改成功';
                    return $this->format($res);
                } else {
                    $this->status = 2;
                    $this->message = '未找到该地图记录';
                    return $this->format();
                }
            }

            $res = Map::create([
                'name' => $nameList
            ]);

            $this->status = 1;
            $this->message = '新增成功';
            return $this->format($res);
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


}
