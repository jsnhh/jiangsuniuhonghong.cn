<?php


namespace App\Api\Controllers\User;


use App\Models\User;
use App\Models\UserInfos;
use App\Models\UserStoreSet;
use Illuminate\Http\Request;

class UserInfoController extends \App\Api\Controllers\BaseController
{
    //实名认证
    public function realName(Request $request)
    {
        try {
            $userToken = $this->parseToken();

            $token = $request->get('token');
            $full_name = $request->get('full_name');
            $id_card = $request->get('id_card');
            $bank_user_name = $request->get('bank_user_name');
            $card_no = $request->get('card_no');
            $id_card_front = $request->get('id_card_front');
            $id_card_back = $request->get('id_card_back');
            $bank_card_front = $request->get('bank_card_front');

            $user_id = $userToken->user_id;

            $check_data = [
                'full_name' => '姓名',
                'id_card' => '身份证号',
                'bank_user_name' => '银行开户人',
                'card_no' => '银行卡号',
                'id_card_front' => '身份证正面',
                'id_card_back' => '身份证反面',
                'bank_card_front' => '银行卡号正面',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $userInfo = UserInfos::where('user_id',$user_id)->first();
            if($userInfo){
                 $userInfo->update([
                    'full_name' => $full_name,
                    'id_card' => $id_card,
                    'bank_user_name' => $bank_user_name,
                    'card_no' => $card_no,
                    'id_card_front' => $id_card_front,
                    'id_card_back' => $id_card_back,
                    'bank_card_front' => $bank_card_front,
                ]);
            }else{
                UserInfos::create([
                    'user_id' => $user_id,
                    'full_name' => $full_name,
                    'id_card' => $id_card,
                    'bank_user_name' => $bank_user_name,
                    'card_no' => $card_no,
                    'id_card_front' => $id_card_front,
                    'id_card_back' => $id_card_back,
                    'bank_card_front' => $bank_card_front,
                ]);
            }

            User::where('id',$user_id)->update([
                'auth_status' => '1' //认证状态(0-未认证;1-通过)
            ]);

            $user = User::where('id',$user_id)->first();

            $data = [
                'token' => $token,
                'user_id' => $user->id,
                'user_name' => $user->name,
                'name' => $user->name,
                'phone' => $user->phone,
                'pid' => $user->pid,
                'level' => $user->level,
                's_code' => $user->s_code,
                'config_id' => $user->config_id,
                'source' => $user->source,
                'invite_no' => $user->invite_no,
                'invite_code' => $user->invite_code,
                'auth_status' => $user->auth_status,
                'userType' =>'user'
            ];

            return json_encode([
                'status' => 1,
                'message' => '认证成功',
                'data' => $data
            ]);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    public function getUserInfo(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_id = $token->user_id;

            $userInfo = UserInfos::where('user_id',$user_id)->first();
            if($userInfo){
                return json_encode([
                    'status' => 1,
                    'message' => '操作成功',
                    'data' => $userInfo
                ]);
            }else{
                return json_encode([
                    'status' => 2,
                    'message' => '获取信息失败',
                    'data' => []
                ]);
            }

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

}