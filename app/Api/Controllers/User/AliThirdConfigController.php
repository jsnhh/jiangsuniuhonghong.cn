<?php
namespace App\Api\Controllers\User;

use Illuminate\Http\Request;
use App\Api\Controllers\BaseController;
use App\Models\AliThirdConfig;

class AliThirdConfigController extends BaseController
{
    // 微信第三方平台配置
    public function aliThirdConfigInfo()
    {
        $model = new AliThirdConfig();

        $data = $model->getInfo();

        return $this->sys_response_layui(1, '操作成功', $data);
    }

    // 保存信息
    public function saveAliThirdConfig(Request $request)
    {
        $requestData = $request->all();

        $date = date('Y-m-d H:i:s', time());
        $upData = [
            'aliPay_applet_id' => $requestData['aliPay_applet_id'],
            'aliPay_applet_template_id' => $requestData['aliPay_applet_template_id'],
            'rsa_private_key' => $requestData['rsa_private_key'],
            'rsa_public_key' => $requestData['rsa_public_key'],
            'version' => $requestData['version'],
            'alipay_applet_auth_url' => $requestData['alipay_applet_auth_url'],
            'updated_at' => $date,
        ];

        $model = new AliThirdConfig();
        // 查询是否已经有数据了
        $data = $model->getInfo();

        if ($data) {
            $res = $model->updateData($upData);
        } else {
            $upData['config_id'] = 1234;
            $upData['created_at'] = $date;
            $res = $model->saveData($upData);
        }

        if ($res) {
            return $this->sys_response_layui(1, '操作成功');
        } else {
            return $this->sys_response_layui(202, '操作失败');
        }
    }
}
