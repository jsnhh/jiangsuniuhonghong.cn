<?php

namespace App\Api\Controllers\User;

use App\Api\Controllers\BaseController;
use App\Models\MerchantConsumerDetails;
use App\Models\MerchantMonthlyPayment;
use App\Models\Stores;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MerchantRechargeController extends BaseController
{
//商户余额
    public function getMerchantBalance(Request $request)
    {
        try {
            $token = $this->parseToken();

            $user_id = $request->get('user_id', $token->user_id);
            $store_name = $request->get('store_name', '');//关键词

            $where = [];

            // $where[] = ['user_id', '=', $user_id];

            if ($store_name) {
                $where[] = ['store_name', 'like', '%' . $store_name . '%'];
            }

            // $storesList = Stores::where($where) ->select('merchant_id', 'user_id','store_name')->get();
            //总门店数 含下级
            $storesList = Stores::whereIn('user_id', $this->getSubIds($user_id, true))->select('merchant_id', 'user_id', 'store_name')->get();

            $merchant_details_ids = [];
            foreach ($storesList as $k => $v) {
                $merchant_id = $v->merchant_id;
                $userId = $v->user_id;
                $obj = DB::table('merchant_consumer_details');
                $consumerDetails = $obj->where('user_id', $userId)
                    ->where('merchant_id', $merchant_id)
                    ->whereIn('type', [1, 2])
                    ->orderBy('created_at', 'desc')
                    ->select('id')
                    ->first();

                if (!$consumerDetails) {
                    continue;
                }

                array_push($merchant_details_ids, $consumerDetails->id);

            }

            $merchant_consumer_details_ids = array_unique($merchant_details_ids); //去重

            $merchantRechargesIds = [];
            foreach ($storesList as $k => $v) {
                $merchant_id = $v->merchant_id;
                $userId = $v->user_id;
                //充值记录表
                $objA = DB::table('merchant_recharges');
                $merchantRecharges = $objA->where('user_id', $userId)
                    ->where('merchant_id', $merchant_id)
                    ->where('status', 1)
                    ->orderBy('created_at', 'desc')
                    ->select('id')
                    ->first();

                if (!$merchantRecharges) {
                    continue;
                }

                array_push($merchantRechargesIds, $merchantRecharges->id);
            }

            $merchantRecharges_ids = array_unique($merchantRechargesIds); //去重

            $objTable = DB::table('merchant_consumer_details');
            $objTable = $objTable->join('stores', 'merchant_consumer_details.merchant_id', 'stores.merchant_id');
            $objTable = $objTable->join('merchant_recharges', 'merchant_consumer_details.merchant_id', 'merchant_recharges.merchant_id');

            // $list = $objTable->whereIn('merchant_consumer_details.id',$merchant_consumer_details_ids)
            //     ->where('stores.status', 1)
            //     ->select('merchant_consumer_details.user_id','merchant_consumer_details.merchant_id','merchant_consumer_details.avail_amount', 'merchant_consumer_details.created_at', 'stores.store_id', 'stores.store_name as store_name', 'stores.people_phone as phone')->orderBy('merchant_consumer_details.avail_amount','asc');

            $list = $objTable->whereIn('merchant_consumer_details.id', $merchant_consumer_details_ids)
                ->whereIn('merchant_recharges.id', $merchantRecharges_ids)
                ->where('stores.status', 1)
                ->where('merchant_recharges.status', 1)
                ->select('merchant_consumer_details.user_id', 'merchant_consumer_details.merchant_id', 'merchant_consumer_details.avail_amount', 'merchant_consumer_details.created_at', 'stores.store_id', 'stores.store_name as store_name', 'stores.people_phone as phone','merchant_recharges.pay_type','merchant_recharges.end_time', 'merchant_recharges.created_at as begin_time')
                ->orderBy('merchant_consumer_details.avail_amount', 'asc');

            $this->t = $list->count();
            $data = $this->page($list)->get();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch
        (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    //充值记录
    public function paymentLog(Request $request)
    {
        try {
            $token = $this->parseToken();

            $user_id = $request->get('user_id', $token->user_id);
            $merchant_id = $request->get('merchant_id', '');

            $where = [];

            // $where[] = ['merchant_recharges.user_id', '=', $user_id];
            $where[] = ['merchant_recharges.status', '=', 1];

            $obj = DB::table('merchant_recharges');
            $obj = $obj->join('stores', 'merchant_recharges.merchant_id', 'stores.merchant_id');

            if ($merchant_id) {
                $where[] = ['merchant_recharges.merchant_id', '=', $merchant_id];
            }

            $obj->where($where)
                ->whereIn('merchant_recharges.user_id', $this->getSubIds($user_id, true))
                ->select('merchant_recharges.*', 'stores.store_name as store_name', 'stores.people_phone as phone')
                ->orderBy('merchant_recharges.created_at', 'desc');

            $this->t = $obj->count();
            $data = $this->page($obj)->get();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch
        (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    //查询消费明细
    public function consumer_details(Request $request)
    {
        try {
            $token = $this->parseToken();

            $user_id = $request->get('user_id', $token->user_id);
            $merchant_id = $request->get('merchant_id', '');
            $settlement_type = $request->get('settlement_type', '');

            if ($merchant_id) {
                $obj = MerchantConsumerDetails::where('user_id', $user_id)
                    ->where('merchant_id', $merchant_id)
                    ->where('type', 3) //类型 1=收入 2=支出 3=分润
                    ->select('user_id', 'merchant_id', 'amount', 'avail_amount', 'profit', 'profit_amount', 'type', 'created_at', 'pay_amount')
                    ->orderBy('created_at', 'desc');

                $this->t = $obj->count();
                $data = $this->page($obj)->get();

            } else {

                $User = User::where('id', $user_id)
                    ->select('money', 'settlement_money', 'unsettlement_money')
                    ->first();
                $money = $User->money;

                $obj = DB::table('merchant_consumer_details');
                $obj = $obj->join('stores', 'merchant_consumer_details.merchant_id', 'stores.merchant_id');

                $obj->where('merchant_consumer_details.user_id', $user_id)
                    ->where('merchant_consumer_details.type', 3) //类型 1=收入 2=支出 3=分润
                    ->where('merchant_consumer_details.settlement_type', $settlement_type)
                    ->select('merchant_consumer_details.*', 'stores.store_name as store_name')
                    ->orderBy('merchant_consumer_details.created_at', 'desc');


                $this->t = $obj->count();
                $detail = $this->page($obj)->get();


                $where[] = ['user_id', '=', $user_id];
                $where[] = ['type', '=', 3];
                $settlement_money_where = $where;
                $settlement_money_where[] = ['settlement_type', '=', '02'];
                $unsettlement_money_where = $where;
                $unsettlement_money_where[] = ['settlement_type', '=', '01'];

                $settlement_money = MerchantConsumerDetails::where($settlement_money_where)->select('profit_amount')->sum('profit_amount');
                $unsettlement_money = MerchantConsumerDetails::where($unsettlement_money_where)->select('profit_amount')->sum('profit_amount');

                $data = [
                    'money' => number_format($money, 2, '.', ''),
                    'settlement_money' => '' . floor($settlement_money * 100) / 100 . '',
                    'unsettlement_money' => '' . floor($unsettlement_money * 100) / 100 . '',
                    'detail' => $detail,
                ];

            }

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch
        (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    public function merchant_payments_list(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_id = $request->get('user_id', $token->user_id);
            $merchant_id = $request->get('merchant_id', '');
            $type_value = $request->get('type_value', '');
            $money = $request->get('money', '');
            $amount = $request->get('amount', ''); //PC端
            $quarterly_amount = $request->get('quarterly_amount', ''); //PC端

            $where = [];

            $where[] = ['user_id', '=', $user_id];
            $where[] = ['merchant_id', '=', $merchant_id];

            $obj = MerchantMonthlyPayment::where('user_id', $user_id)
                ->where('merchant_id', $merchant_id)
                ->select()
                ->first();

            if (empty($obj)) {
                $dataIN = [
                    'user_id' => $user_id,
                    'merchant_id' => $merchant_id,
                    'month_package' => '月包',
                    'amount' => 0.00,
                    'quarterly_package' => '季度包',
                    'quarterly_amount' => 0.00,
                ];

                $merchantPayment = MerchantMonthlyPayment::create($dataIN);

                return $this->format($merchantPayment);
            } else {
                if ($type_value == '01') {  // 月包
                    $data['amount'] = $money;

                    $obj->update($data);
                    $obj->save();

                } else if ($type_value == '02') {
                    $data['quarterly_amount'] = $money;

                    $obj->update($data);
                    $obj->save();
                }else if ($type_value == '03') { //PC端
                    $data['amount'] = $amount;
                    $data['quarterly_amount'] = $quarterly_amount;

                    $obj->update($data);
                    $obj->save();
                }
            }

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($obj);
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getLine()
            ]);
        }
    }

    public function payment_type(Request $request)
    {
        try {
            $data = [
                [
                    'value' => '01',
                    'name' => '月包'
                ],
                [
                    'value' => '02',
                    'name' => '季包'
                ]
            ];

            $this->status = 1;
            $this->message = "数据返回成功";

            return $this->format($data);
        } catch (\Exception $e) {
            return json_encode([
                'status' => -1,
                'message' => $e->getMessage() . ' | ' . $e->getLine()
            ]);
        }
    }

}
