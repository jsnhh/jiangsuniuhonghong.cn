<?php

namespace App\Api\Controllers\User;


use App\Api\Controllers\BaseController;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Store;
use App\Models\StoreDayAllOrder;
use App\Models\User;
use App\Models\UserDayOrder;
use App\Models\UserMonthOrder;
use App\Models\UserWalletDetail;
use App\Models\WeixinSharingOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class OrderController extends BaseController
{

    public function order(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $user_id = $request->get('user_id', '');
            $pay_status = $request->get('pay_status', '');
            $ways_source = $request->get('ways_source', '');
            $company = $request->get('company', '');
            $device_id = $request->get('device_id', ''); //设备id
            $device_name = $request->get('device_name', ''); //设备名称

            $ways_type = $request->get('ways_type', '');
            $time_start_s = date('Y-m-d 00:00:00', time());
            $time_start_e = date('Y-m-d 23:59:59', time());

            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');

            if (in_array($time_start, [null, ''])) {
                $time_start = $time_start_s;
            }

            if (in_array($time_end, [null, ''])) {
                $time_end = $time_start_e;
            }

            $amount_start = $request->get('amount_start', '');
            $amount_end = $request->get('amount_end', '');

            $re_data = [
                'time_start' => $time_start,
                'time_end' => $time_end,
            ];
            $check_data = [
                'time_start' => '订单开始时间',
                'time_end' => '订单结束时间',
            ];
            $check = $this->check_required($re_data, $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $sort = $request->get('sort', '');
            $out_trade_no = $request->get('out_trade_no', '');
            $trade_no = $request->get('trade_no', '');

            $day = date('Ymd', strtotime($time_start));
            $table = 'orders_' . $day;

            if ($out_trade_no || $trade_no) {
                $a = array('a', '_', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
                $b = str_ireplace($a, "", $out_trade_no);
                $day = substr($b, 0, 8);
                $table = 'orders_' . $day;
            }

            //限制时间
            $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
            $day = 31;
            $now_time = date('Y-m-d H:i:s', time());
            $end = date('Y-m-d 22:00:00', time());
            if ($now_time > $end) {
                $day = 31;
            }

            if ($date > $day) {
                return json_encode([
                    'status' => 2,
                    'message' => '时间跨度不能超过' . $day . '天'
                ]);
            }

            //跨天操作
            $time_start_db = date('Ymd', strtotime($time_start));
            $time_end_db = date('Ymd', strtotime($time_end));
            $is_ct_time = 0;
            if ($time_start_db != $time_end_db) {
                $is_ct_time = 1;
            }

            if (env('DB_D1_HOST')) {
                //有没有跨天
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj = DB::connection("mysql_d1")->table('order_items');
                    } else {
                        if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                            $obj = DB::connection("mysql_d1")->table('orders2020');
                        } else {
                            $obj = DB::connection("mysql_d1")->table('orders');
                        }
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj = DB::connection("mysql_d1")->table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj = DB::connection("mysql_d1")->table('order_items');

                        } else {
                            if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                                $obj = DB::connection("mysql_d1")->table('orders2020');
                            } else {
                                $obj = DB::connection("mysql_d1")->table('orders');
                            }
                        }
                    }
                }
            } else {
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj = DB::table('order_items');
                    } else {
                        if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                            $obj = DB::table('orders2020');
                        } else {
                            $obj = DB::table('orders');
                        }
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj = DB::table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj = DB::table('order_items');
                        } else {
                            if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                                $obj = DB::table('orders2020');
                            } else {
                                $obj = DB::table('orders');
                            }
                        }
                    }
                }
            }

            $where = [];

            if ($user_id == "") {
                $user_id = $user->user_id;
            }

            if ($user_id == 1) {
                $user_ids = $this->getSubIdsAll($user_id);
            } else {
                $user_ids = $this->getSubIds($user_id);
            }

            if ($amount_start) {
                $where[] = ['total_amount', '>=', $amount_start];
            }

            if ($amount_end) {
                $where[] = ['total_amount', '<=', $amount_end];
            }

            if ($pay_status) {
                $where[] = ['pay_status', '=', $pay_status];
            }

            if ($device_id) {
                $where[] = ['device_id', '=', $device_id];
            }
            if ($device_name) {
                $where[] = ['device_name', '=', $device_name];
            }
            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
            }

            if ($company) {
                $where[] = ['company', '=', $company];
            }

            if ($ways_source) {
                if ($ways_source == 'alipay_face_dis_day') {
                    $where[] = ['ways_source', '=', 'alipay_face'];
                } elseif ($ways_source == 'weixin_face_dis_day') {
                    $where[] = ['pay_method', '=', 'weixin_face'];
                } else {
                    if (in_array($ways_source, ['alipay_face', 'weixin_face'])) {
                        $where[] = ['pay_method', '=', $ways_source];
                    } else {
                        $where[] = ['ways_source', '=', $ways_source];
                    }
                }
            }

            if ($ways_type) {
                $where[] = ['ways_type', '=', $ways_type];
            }

            if ($time_start) {
                $where[] = ['created_at', '>=', $time_start];
            }

            if ($time_end) {
                $where[] = ['created_at', '<=', $time_end];
            }

            if ($device_id) {
                $where[] = ['device_id', '=', $device_id];
            }
            if ($device_name) {
                $where[] = ['device_name', '=', $device_name];
            }

            if ($sort) {
                $obj = $obj->where($where)
                    ->whereIn('user_id', $user_ids)
                    ->orderBy('total_amount', $sort);
            } elseif ($out_trade_no || $trade_no) {
                if ($out_trade_no) {
                    $where1[] = ['out_trade_no', $out_trade_no];
                }

                if ($trade_no) {
                    $where1[] = ['trade_no', $trade_no];
                }

                $obj = $obj->where($where1)
                    ->whereIn('user_id', $user_ids)
                    ->orderBy('created_at', 'desc');
//            } elseif ($ways_source && (in_array($ways_source, ['alipay_face_dis_day', 'weixin_face_dis_day']))) {
//                $obj = $obj->select(
//                    'name',
//                    'store_id',
//                    'store_name',
//                    'merchant_name',
//                    'out_trade_no',
//                    'total_amount',
//                    'rate',
//                    'fee_amount',
//                    'ways_source_desc',
//                    'pay_status',
//                    'pay_status_desc',
//                    'pay_time',
//                    'company',
//                    'remark',
//                    'device_id',
//                    'buyer_id'
//                )
//                    ->distinct('buyer_id')
//                    ->where($where)
//                    ->whereIn('user_id', $user_ids)
//                    ->orderBy('created_at', 'desc')
//                    ->groupBy(DB::raw("date_format(pay_time, '%Y-%m-%d')"), 'buyer_id');
            } else {
                $obj = $obj->where($where)
                    ->whereIn('user_id', $user_ids)
                    ->orderBy('created_at', 'desc');
            }

            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';

            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    public function order_info(Request $request)
    {
        try {
            $user = $this->parseToken();
            $out_trade_no = $request->get('out_trade_no', '');
            $a = array('a', '_', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
            $b = str_ireplace($a, "", $out_trade_no);
            $day = substr($b, 0, 8);
            $table = 'orders_' . $day;

            if (Schema::hasTable($table)) {
                $data = DB::table($table)->where('out_trade_no', $out_trade_no)->first();

            } else {
                if (Schema::hasTable('order_items')) {
                    $data = DB::table('order_items')->where('out_trade_no', $out_trade_no)->first();
                } else {
                    $data = DB::table('orders')->where('out_trade_no', $out_trade_no)->first();
                }
            }

            if (!$data && Schema::hasTable('orders2020')) {
                $data = DB::table('orders2020')->where('out_trade_no', $out_trade_no)->first();
            }

            if (!$data) {
                $this->status = 2;
                $this->message = '订单号不存在';
                return $this->format();
            }

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    //对账统计-比较全-实时
    public function order_count(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $user_id = $request->get('user_id', '');
            $ways_source = $request->get('ways_source', '');
            $company = $request->get('company', '');
            $time_start_s = date('Y-m-d 00:00:00', time());
            $time_start_e = date('Y-m-d 23:59:59', time());
            $device_id = $request->get('device_id', '');
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');

            if (in_array($time_start, [null, ''])) {
                $time_start = $time_start_s;
            }

            if (in_array($time_end, [null, ''])) {
                $time_end = $time_start_e;
            }

            $return_type = $request->get('return_type', '01');
            $amount_start = $request->get('amount_start', '');
            $amount_end = $request->get('amount_end', '');

            //限制时间
            $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
            $day = 31;

            $now_time = date('Y-m-d H:i:s', time());
            $end = date('Y-m-d 22:00:00', time());
            if ($now_time > $end) {
                $day = 31;
            }

            //限制时间
            if ($date > $day) {
                return json_encode([
                    'status' => 2,
                    'message' => '时间跨度不能超过' . $day . '天'
                ]);
            }

            //跨天操作
            $time_start_db = date('Ymd', strtotime($time_start));
            $time_end_db = date('Ymd', strtotime($time_end));
            $is_ct_time = 0;
            if ($time_start_db != $time_end_db) {
                $is_ct_time = 1;
            }

            $check_data = [
                'time_start' => '开始时间',
                'time_end' => '结束时间',
            ];
            $where = [];
            $whereIn = [];
            $store_ids = [];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            //条件查询
            if ($time_start) {
                $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                $where[] = ['created_at', '>=', $time_start];
            }
            if ($time_end) {
                $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                $where[] = ['created_at', '<=', $time_end];
            }
            if ($device_id) {
                $where[] = ['device_id', '=', $device_id];
            }
            if ($amount_start) {
                $where[] = ['total_amount', '>=', $amount_start];
            }
            if ($amount_end) {
                $where[] = ['total_amount', '<=', $amount_end];
            }
            if ($company) {
                $where[] = ['company', $company];
            }
            if ($ways_source) {
                if (in_array($ways_source, ['alipay_face', 'weixin_face'])) {
                    $where[] = ['pay_method', '=', $ways_source];
                } else {
                    $where[] = ['ways_source', '=', $ways_source];
                }
            }
            if ($store_id) {
                $where[] = ['store_id', $store_id];
            }
            if ($user_id) {
                $user_ids = $this->getSubIdsAll($user_id);
            } else {
                $user_ids = $this->getSubIdsAll($user->user_id);
            }

            //区间
            $e_order = '0.00';

            $day = date('Ymd', strtotime($time_start));
            $table = 'orders_' . $day;

            if (env('DB_D1_HOST')) {
                //有没有跨天
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj = DB::connection("mysql_d1")->table('order_items');
                    } else {
                        if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                            $obj = DB::connection("mysql_d1")->table('orders2020');
                        } else {
                            $obj = DB::connection("mysql_d1")->table('orders');
                        }
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj = DB::connection("mysql_d1")->table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj = DB::connection("mysql_d1")->table('order_items');
                        } else {
                            if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                                $obj = DB::connection("mysql_d1")->table('orders2020');
                            } else {
                                $obj = DB::connection("mysql_d1")->table('orders');
                            }
                        }
                    }
                }
            } else {
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj = DB::table('order_items');
                    } else {
                        if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                            $obj = DB::table('orders2020');
                        } else {
                            $obj = DB::table('orders');
                        }
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj = DB::table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj = DB::table('order_items');
                        } else {
                            if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                                $obj = DB::table('orders2020');
                            } else {
                                $obj = DB::table('orders');
                            }
                        }
                    }
                }
            }

            if (env('DB_D1_HOST')) {
                //有没有跨天
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj1 = DB::connection("mysql_d1")->table('order_items');
                    } else {
                        if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                            $obj1 = DB::connection("mysql_d1")->table('orders2020');
                        } else {
                            $obj1 = DB::connection("mysql_d1")->table('orders');
                        }
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj1 = DB::connection("mysql_d1")->table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj1 = DB::connection("mysql_d1")->table('order_items');
                        } else {
                            if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                                $obj1 = DB::connection("mysql_d1")->table('orders2020');
                            } else {
                                $obj1 = DB::connection("mysql_d1")->table('orders');
                            }
                        }
                    }
                }
            } else {
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj1 = DB::table('order_items');
                    } else {
                        if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                            $obj1 = DB::table('orders2020');
                        } else {
                            $obj1 = DB::table('orders');
                        }
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj1 = DB::table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj1 = DB::table('order_items');
                        } else {
                            if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                                $obj1 = DB::table('orders2020');
                            } else {
                                $obj1 = DB::table('orders');
                            }
                        }
                    }
                }
            }

            $order_data = $obj->whereIn('user_id', $user_ids)
                ->where($where)
                ->whereIn('pay_status', [1, 6])//成功+退款
                ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

            $order_mdiscount_data = Order::whereIn('user_id', $user_ids)
                ->where($where)
                ->where('pay_status', 1)//成功
                ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

            $refund_obj = $obj1->whereIn('user_id', $user_ids)
                ->where($where)
                ->where('pay_status', '=', 6) //退款
                ->select('refund_amount');

            $member_data = Order::whereIn('user_id', $user_ids)
                ->where($where)
                ->where('pay_status', 1)
                //->where('pay_status_desc', '会员卡支付成功') // 会员卡支付成功
                ->where('ways_type', '18888') //会员卡支付
                ->select('total_amount', 'refund_amount', 'receipt_amount', 'fee_amount', 'mdiscount_amount');

            //总的
            $total_amount = $order_data->sum('total_amount');//交易金额
            $refund_amount = $refund_obj->sum('refund_amount');//退款金额
            $fee_amount = $order_data->sum('fee_amount');//结算服务费/手续费
            $mdiscount_amount = $order_mdiscount_data->sum('mdiscount_amount');//商家优惠金额
            $total_member_amount = $member_data->sum('total_amount'); //会员卡支付金额
            $get_amount = $total_amount - $refund_amount - $mdiscount_amount - $total_member_amount;//商家实收，交易金额-退款金额-商家优惠金额-会员卡支付金额
            $receipt_amount = $get_amount - $fee_amount;//实际净额，实收-手续费
            $e_order = '' . $e_order . '';
            $total_count = '' . count($order_data->get()) . '';
            $refund_count = count($refund_obj->get());

            $data = [
                'total_amount' => number_format($total_amount, 2, '.', ''),//交易金额
                'total_count' => '' . $total_count . '',//交易笔数
                'refund_count' => '' . $refund_count . '',//退款笔数
                'get_amount' => number_format($get_amount, 2, '.', ''),//商家实收，交易金额-退款金额
                'refund_amount' => number_format($refund_amount, 2, '.', ''),//退款金额
                'receipt_amount' => number_format($receipt_amount, 2, '.', ''),//实际净额，实收-手续费
                'fee_amount' => number_format($fee_amount, 2, '.', ''),//结算服务费/手续费
                'mdiscount_amount' => number_format($mdiscount_amount, 2, '.', ''),//商家优惠金额
                'member_total_amount' => number_format($total_member_amount, 2, '.', '') //会员支付金额
            ];
            //附加流水详情
            if ($return_type == "02") {
                $table = 'orders_' . $day;
                if (env('DB_D1_HOST')) {
                    //有没有跨天
                    if ($is_ct_time) {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj3 = DB::connection("mysql_d1")->table('order_items');
                        } else {
                            if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                                $obj3 = DB::connection("mysql_d1")->table('orders2020');
                            } else {
                                $obj3 = DB::connection("mysql_d1")->table('orders');
                            }
                        }
                    } else {
                        if (Schema::hasTable($table)) {
                            $obj3 = DB::connection("mysql_d1")->table($table);
                        } else {
                            if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                                $obj3 = DB::connection("mysql_d1")->table('order_items');
                            } else {
                                if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                                    $obj3 = DB::connection("mysql_d1")->table('orders2020');
                                } else {
                                    $obj3 = DB::connection("mysql_d1")->table('orders');
                                }
                            }
                        }
                    }
                } else {
                    if ($is_ct_time) {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj3 = DB::table('order_items');
                        } else {
                            if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                                $obj3 = DB::table('orders2020');
                            } else {
                                $obj3 = DB::table('orders');
                            }
                        }
                    } else {
                        if (Schema::hasTable($table)) {
                            $obj3 = DB::table($table);
                        } else {
                            if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                                $obj3 = DB::table('order_items');
                            } else {
                                if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                                    $obj3 = DB::table('orders2020');
                                } else {
                                    $obj3 = DB::table('orders');
                                }
                            }
                        }
                    }
                }

                $obj3 = $obj3->where($where)
                    ->whereIn('user_id', $user_ids)
                    ->orderBy('updated_at', 'desc');
                $this->t = $obj3->count();
                $data['order_list'] = $this->page($obj3)->get();
            }

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    //对账统计-比较全-非实时
    public function order_count1(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $user_id = $request->get('user_id', '');
            $ways_source = $request->get('ways_source', '');
            $company = $request->get('company', '');
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');
            $return_type = $request->get('return_type', '01');

            $check_data = [
                'time_start' => '开始时间',
                'time_end' => '结束时间',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            //限制时间
            $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
            $day = 31;
            if ($date > $day) {
                return json_encode([
                    'status' => 2,
                    'message' => '时间筛选不能超过' . $day . '天'
                ]);
            }

            $where = [];
            $whereIn = [];
            $store_ids = [];

            //条件查询
            if ($time_start) {
                $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                $where[] = ['created_at', '>=', $time_start];
            }
            if ($time_end) {
                $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                $where[] = ['created_at', '<=', $time_end];
            }
            if ($company) {
                $where[] = ['company', $company];
            }
            if ($ways_source) {
                $where[] = ['ways_source', $ways_source];
            }
            if ($store_id) {
                $where[] = ['store_id', $store_id];
            }
            if ($user_id) {
                $user_ids = $this->getSubIds($user_id);

            } else {
                $user_ids = $this->getSubIds($user->user_id);
            }

            //区间
            $time_start1 = date("Ymd", strtotime($time_start));
            $time_end1 = date("Ymd", strtotime($time_end));

            $order_data = UserDayOrder::whereBetween('day', [$time_start1, $time_end1])
                ->whereIn('user_id', $user_ids)
                ->select(
                    'total_amount',
                    'order_sum',
                    'refund_amount',
                    'refund_count',
                    'fee_amount');

            //总的
            $total_amount = $order_data->sum('total_amount');//交易金额
            $refund_amount = $order_data->sum('refund_amount');//退款金额
            $fee_amount = $order_data->sum('fee_amount');//结算服务费/手续费
            $mdiscount_amount = '0.00';//商家优惠金额
            $get_amount = $total_amount - $refund_amount - $mdiscount_amount;//商家实收，交易金额-退款金额
            $receipt_amount = $get_amount - $fee_amount;//实际净额，实收-手续费
            $total_count = $order_data->sum('order_sum');
            $refund_count = $order_data->sum('refund_count');

            $data = [
                'total_amount' => number_format($total_amount, 2, '.', ''),//交易金额
                'total_count' => '' . $total_count . '',//交易笔数
                'refund_count' => '' . $refund_count . '',//退款金额
                'get_amount' => number_format($get_amount, 2, '.', ''),//商家实收，交易金额-退款金额
                'refund_amount' => number_format($refund_amount, 2, '.', ''),//退款金额
                'receipt_amount' => number_format($receipt_amount, 2, '.', ''),//实际净额，实收-手续费
                'fee_amount' => number_format($fee_amount, 2, '.', ''),//结算服务费/手续费
                'mdiscount_amount' => number_format($mdiscount_amount, 2, '.', ''),
            ];
            //附加流水详情
            if ($return_type == "02") {
                $tableName = date('Y', strtotime($time_start));
                if (Schema::hasTable("orders$tableName")) {
                    $obj = DB::table("orders$tableName");
                } else {
                    $obj = DB::table('orders');
                }
                $obj = $obj->where($where)
                    ->whereIn('user_id', $user_ids)
                    ->orderBy('created_at', 'desc');
                $this->t = $obj->count();
                $data['order_list'] = $this->page($obj)->get();
            }

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    //数据对账-比较全-非实时
    public function order_data(Request $request)
    {
        try {
            $token = $this->parseToken();

            //下级账户
            $user_ids = $this->getSubIds($token->user_id);
            $sub_user = count($user_ids);

            $stores = Store::whereIn('user_id', $user_ids)
                ->select('id')
                ->get();
            $sub_store = count($stores);

            if (env('DB_D1_HOST')) {
                $Order_obj = DB::connection("mysql_d1")->table('orders');
            } else {
                $Order_obj = DB::table('orders');
            }

            $order_data = $Order_obj->whereIn('user_id', $user_ids)
                ->where('created_at', '>=', date('Y-m-d 00:00:00', time()))
                ->whereIn('pay_status', [1, 6]) //成功+已退款
                ->select('total_amount');
            $day_amount = $order_data->sum('total_amount');
            $day_count = count($order_data->get());

            $yesterdayTable = date('Y', strtotime("-1 day"));
            if (env('DB_D1_HOST')) {
                if (Schema::hasTable("orders$yesterdayTable")) {
                    $Order_obj1 = DB::connection("mysql_d1")->table("orders$yesterdayTable");
                } else {
                    $Order_obj1 = DB::connection("mysql_d1")->table('orders');
                }
            } else {
                if (Schema::hasTable("orders$yesterdayTable")) {
                    $Order_obj1 = DB::table("orders$yesterdayTable");
                } else {
                    $Order_obj1 = DB::table('orders');
                }
            }

            //昨天数据
            $order_data_o = $Order_obj1->whereIn('user_id', $user_ids)
                ->where('created_at', '>=', date('Y-m-d 00:00:00', strtotime("-1 day")))
                ->where('created_at', '<=', date('Y-m-d 23:59:59', strtotime("-1 day")))
                ->whereIn('pay_status', [1, 6])//成功+退款
                ->select('total_amount');
            $old_day_amount = $order_data_o->sum('total_amount');
            $old_day_count = count($order_data_o->get());

            // 上个月的起始时间:
            $data = date('Y-m-d');
            $base = strtotime(date('Y-m', strtotime($data)) . '-01 00:00:01');
            $begin_time = date('Y-m-01 00:00:00', strtotime('-1 month', $base));

            $lastMonthTable = date('Y', strtotime('-1 month', $base));
            if (env('DB_D1_HOST')) {
                if (Schema::hasTable("orders$lastMonthTable")) {
                    $Order_obj2 = DB::connection("mysql_d1")->table("orders$lastMonthTable");
                } else {
                    $Order_obj2 = DB::connection("mysql_d1")->table('orders');
                }
            } else {

                if (Schema::hasTable("orders$lastMonthTable")) {
                    $Order_obj2 = DB::table("orders$lastMonthTable");
                } else {
                    $Order_obj2 = DB::table('orders');
                }
            }

            $end_time = date("Y-m-d 23:59:59", strtotime(-date('d') . 'day'));

            //上个月
            $order_data_mo = $Order_obj2->whereIn('user_id', $user_ids)
                ->where('created_at', '>=', $begin_time)
                ->where('created_at', '<=', $end_time)
                ->whereIn('pay_status', [1, 6]) //成功+退款
                ->select('total_amount');
            $old_month_amount = $order_data_mo->sum('total_amount');
            $old_month_count = count($order_data_mo->get());

            // 本月起始时间:
            $date = date("Y-m-d");
            // 本月第一天
            $first = date('Y-m-01 0:0:0', strtotime($date));

            $nowMonthTable = date("Y");
            if (env('DB_D1_HOST')) {
                if (Schema::hasTable("orders$nowMonthTable")) {
                    $Order_obj3 = DB::connection("mysql_d1")->table("orders$nowMonthTable");
                } else {
                    $Order_obj3 = DB::connection("mysql_d1")->table('orders');
                }
            } else {
                if (Schema::hasTable("orders$nowMonthTable")) {
                    $Order_obj3 = DB::table("orders$nowMonthTable");
                } else {
                    $Order_obj3 = DB::table('orders');
                }
            }

            //本月
            $order_data_m = $Order_obj3->whereIn('user_id', $user_ids)
                ->where('created_at', '>=', $first)
                ->whereIn('pay_status', [1, 6]) //成功+退款
                ->select('total_amount');
            $month_amount = $order_data_m->sum('total_amount');
            $month_count = count($order_data_m->get());

            return json_encode([
                'status' => 1,
                'data' => [
                    'sub_user' => '' . $sub_user . '',
                    'sub_store' => '' . $sub_store . '',
                    'day_amount' => '' . $day_amount . '',
                    'day_count' => '' . $day_count . '',
                    'old_day_amount' => '' . $old_day_amount . '',
                    'old_day_count' => '' . $old_day_count . '',
                    'month_amount' => '' . $month_amount . '',
                    'month_count' => '' . $month_count . '',
                    'old_month_amount' => '' . $old_month_amount . '',
                    'old_month_count' => '' . $old_month_count . '',
                ]

            ]);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    //门店数据
    public function store_order_data1(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $d = date('Ymd', strtotime("-1 day"));
            $day = $request->get('day', '');
            $day = isset($day) ? $day : $d;
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');
            $order_count = $request->get('order_count', 'desc');
            $order_money = $request->get('order_money', 'desc');
            $user_ids = $this->getSubIds($user->user_id);

            $where = [];

            if ($day) {
                $where[] = ['store_day_all_orders.day', '=', $day];
            }

            if ($store_id) {
                $store_ids = [
                    'store_id' => $store_id
                ];
            } else {
                if (env('DB_D1_HOST')) {
                    $Store = DB::connection("mysql_d1")->table("stores");
                } else {
                    $Store = DB::table('stores');
                }

                $store_ids = $Store->whereIn('user_id', $user_ids)->select('store_id')->get();
                if ($store_ids->isEmpty()) {
                    $store_ids = [
                        'store_id' => '1'
                    ];
                } else {
                    $store_ids = $store_ids->toArray();
                }
            }


            if (env('DB_D1_HOST')) {
                $Order_obj = DB::connection("mysql_d1")->table("store_day_all_orders");
            } else {
                $Order_obj = DB::table('store_day_all_orders');
            }

            $Order_obj = $Order_obj->join('stores', 'store_day_all_orders.store_id', '=', 'stores.store_id')
                ->where($where)
                ->whereIn('store_day_all_orders.store_id', $store_ids)
                ->orderBy('store_day_all_orders.order_sum', $order_count)
                ->orderBy('store_day_all_orders.total_amount', $order_money)
                ->select('stores.store_short_name', 'store_day_all_orders.total_amount', 'store_day_all_orders.order_sum', 'store_day_all_orders.store_id');
            $this->t = $Order_obj->count();
            $data = $this->page($Order_obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch (\Exception $exception) {
            dd($exception);
            $this->status = -1;
            $this->message = $exception->getMessage() . $exception->getLine();
            return $this->format();
        }
    }


    //门店数据
    public function store_order_data(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $d = date('Ymd', strtotime("-1 day"));
            $day = $request->get('day', '');
            $day = isset($day) ? $day : $d;
            $user_ids = $this->getSubIds($user->user_id);
            $where = [];

            if ($day) {
                $where[] = ['store_day_all_orders.day', '=', $day];
            }

            //测试  上线的时候这两行注释掉
            // $store_ids[0]['store_id']='2019091017081561515';
            // $store_ids[1]['store_id']='2018061205492993161';

            //排序只能按一种进行排序
            /*
                传参
                px                          字段
                px_type-asc/px_type-desc    值
            */
            if ($request->input('px') != '') {
                $px = explode('-', $request->input('px'));
                //如果排序接受的是order_money排序
                if ($px[0] == 'order_money') {
                    // $obj = StoreDayAllOrder::join('stores', 'store_day_all_orders.store_id', '=', 'stores.store_id')
                    //     ->where($where)
                    //     ->whereIn('store_day_all_orders.store_id', $store_ids)
                    //     ->orderBy('store_day_all_orders.total_amount', $px[1])
                    //     ->select('stores.store_short_name', 'store_day_all_orders.total_amount', 'store_day_all_orders.order_sum', 'store_day_all_orders.store_id');

                    //检测表  store_day_all_orders
                    if (env('DB_D1_HOST')) {
                        if (Schema::hasTable('store_day_all_orders')) {
                            $obj = DB::connection("mysql_d1")->table('store_day_all_orders')->join('stores', 'store_day_all_orders.store_id', '=', 'stores.store_id')
                                ->where($where)
                                ->whereIn('store_day_all_orders.user_id', $user_ids)
                                ->orderBy('store_day_all_orders.total_amount', $px[1])
                                ->select('stores.store_short_name', 'store_day_all_orders.total_amount', 'store_day_all_orders.order_sum', 'store_day_all_orders.store_id');
                        }
                    } else {
                        if (Schema::hasTable('store_day_all_orders')) {
                            $obj = StoreDayAllOrder::join('stores', 'store_day_all_orders.store_id', '=', 'stores.store_id')
                                ->where($where)
                                ->whereIn('store_day_all_orders.user_id', $user_ids)
                                ->orderBy('store_day_all_orders.total_amount', $px[1])
                                ->select('stores.store_short_name', 'store_day_all_orders.total_amount', 'store_day_all_orders.order_sum', 'store_day_all_orders.store_id');
                        }
                    }
                }

                //如果排序接受的是order_count排序
                if ($px[0] == 'order_count') {
                    // $obj = StoreDayAllOrder::join('stores', 'store_day_all_orders.store_id', '=', 'stores.store_id')
                    //     ->where($where)
                    //     ->whereIn('store_day_all_orders.store_id', $store_ids)
                    //     ->orderBy('store_day_all_orders.order_sum', $px[1])
                    //     ->select('stores.store_short_name', 'store_day_all_orders.total_amount', 'store_day_all_orders.order_sum', 'store_day_all_orders.store_id');

                    //检测表  store_day_all_orders
                    if (env('DB_D1_HOST')) {
                        if (Schema::hasTable('store_day_all_orders')) {
                            $obj = DB::connection("mysql_d1")->table('store_day_all_orders')->join('stores', 'store_day_all_orders.store_id', '=', 'stores.store_id')
                                ->where($where)
                                ->whereIn('store_day_all_orders.user_id', $user_ids)
                                ->orderBy('store_day_all_orders.order_sum', $px[1])
                                ->select('stores.store_short_name', 'store_day_all_orders.total_amount', 'store_day_all_orders.order_sum', 'store_day_all_orders.store_id');
                        }
                    } else {
                        if (Schema::hasTable('store_day_all_orders')) {
                            $obj = StoreDayAllOrder::join('stores', 'store_day_all_orders.store_id', '=', 'stores.store_id')
                                ->where($where)
                                ->whereIn('store_day_all_orders.user_id', $user_ids)
                                ->orderBy('store_day_all_orders.order_sum', $px[1])
                                ->select('stores.store_short_name', 'store_day_all_orders.total_amount', 'store_day_all_orders.order_sum', 'store_day_all_orders.store_id');
                        }
                    }
                }
            } else {
                //如果没有接受到排序参数//默认查询
                // $obj = StoreDayAllOrder::join('stores', 'store_day_all_orders.store_id', '=', 'stores.store_id')
                // ->where($where)
                // ->whereIn('store_day_all_orders.store_id', $store_ids)
                // ->select('stores.store_short_name', 'store_day_all_orders.total_amount', 'store_day_all_orders.order_sum', 'store_day_all_orders.store_id');
                if (env('DB_D1_HOST')) {
                    if (Schema::hasTable('store_day_all_orders')) {
                        $obj = DB::connection("mysql_d1")->table('store_day_all_orders')->join('stores', 'store_day_all_orders.store_id', '=', 'stores.store_id')
                            ->where($where)
                            ->whereIn('store_day_all_orders.user_id', $user_ids)
                            ->select('stores.store_short_name', 'store_day_all_orders.total_amount', 'store_day_all_orders.order_sum', 'store_day_all_orders.store_id');
                    }
                } else {
                    if (Schema::hasTable('store_day_all_orders')) {
                        $obj = StoreDayAllOrder::join('stores', 'store_day_all_orders.store_id', '=', 'stores.store_id')
                            ->where($where)
                            ->whereIn('store_day_all_orders.user_id', $user_ids)
                            ->select('stores.store_short_name', 'store_day_all_orders.total_amount', 'store_day_all_orders.order_sum', 'store_day_all_orders.store_id');
                    }
                }
            }

            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';

            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    public function settle_order(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $user_id = $request->get('user_id', '');
            $pay_status = $request->get('status', '');
            $time_start_s = date('Y-m-d 00:00:00', time());
            $time_start_e = date('Y-m-d 23:59:59', time());
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');

            if (in_array($time_start, [null, ''])) {
                $time_start = $time_start_s;
            }

            if (in_array($time_end, [null, ''])) {
                $time_end = $time_start_e;
            }

            $re_data = [
                'time_start' => $time_start,
                'time_end' => $time_end,
            ];

            $check_data = [
                'time_start' => '订单开始时间',
                'time_end' => '订单结束时间',
            ];
            $check = $this->check_required($re_data, $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $out_trade_no = $request->get('out_trade_no', '');
            $trade_no = $request->get('trade_no', '');

            $day = date('Ymd', strtotime($time_start));
            $table = 'settle_orders_' . $day;

            if ($out_trade_no || $trade_no) {
                $a = array('a', '_', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
                $b = str_ireplace($a, "", $out_trade_no);
                $day = substr($b, 0, 8);
                $table = 'settle_orders_' . $day;
            }

            //限制时间
            $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
            $day = 1;

            if ($date > $day) {
                return json_encode([
                    'status' => 2,
                    'message' => '时间跨度不能超过' . $day . '天'
                ]);
            }

            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table($table);
            } else {
                $obj = DB::table($table);
            }

            $where = [];
            if ($user_id == "") {
                $user_id = $user->user_id;
            }

            $user_ids = $this->getSubIds($user_id);

            if ($pay_status) {
                $where[] = ['pay_status', '=', $pay_status];
            }

            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
            }

            if ($time_start) {
                $where[] = ['created_at', '>=', $time_start];
            }

            if ($time_end) {
                $where[] = ['created_at', '<=', $time_end];
            }

            if ($out_trade_no || $trade_no) {
                if ($out_trade_no) {
                    $where1[] = ['out_trade_no', $out_trade_no];
                }

                if ($trade_no) {
                    $where1[] = ['trade_no', $trade_no];
                }

                $obj = $obj->where($where1)
                    ->whereIn('user_id', $user_ids)
                    ->orderBy('created_at', 'desc');

            } else {
                $obj = $obj->where($where)
                    ->whereIn('user_id', $user_ids)
                    ->orderBy('created_at', 'desc');
            }

            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';

            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = 2;
            $this->message = '暂无数据';
            return $this->format([]);
        }
    }

    //微信分账流水
    public function profit_sharing_lists(Request $request)
    {
        $token = $this->parseToken();
        $token_user_id = $token->user_id;

        $start_time = $request->post('time_start', ''); //开始时间
        $end_time = $request->post('time_end', ''); //结束时间
        $user_id = $request->post('user_id', '');  //服务商id
        $store_id = $request->post('store_id', '');  //门店id
        $amount_start = $request->post('amount_start', ''); //交易开始金额
        $amount_end = $request->post('amount_end', ''); //交易结束金额
        $fz_status = $request->post('fz_status', ''); //状态

        $login_user = User::where('id', $token_user_id)
            ->where('is_delete', '=', '0')
            ->first();
        if (!$login_user) {
            $this->status = '2';
            $this->message = '当前登录状态异常';
            return $this->format();
        }

        if (isset($user_id) && ($user_id != 'undefined') && $user_id) {
            $user_ids = $this->getSubIds($user_id);
        } else {
            $user_ids = $this->getSubIds($token_user_id);
        }

        $where = [];
        if ($amount_start) {
            $where[] = ['fz_amount', '>=', "$amount_start"];
        }
        if ($amount_end) {
            $where[] = ['fz_amount', '<=', "$amount_end"];
        }
        if ($store_id) {
            $where[] = ['store_id', "$store_id"];
        }
        if ($fz_status) {
            $where[] = ['status', "$fz_status"];
        }
        if (!$start_time) {
            $start_time = date('Y-m-d 00:00:00', time());
        }
        if (!$end_time) {
            $end_time = date('Y-m-d 23:59:59', time());
        }

        try {
            $obj = WeixinSharingOrder::where($where)
                ->where('updated_at', '>=', "$start_time")
                ->where('updated_at', '<=', "$end_time")
                ->whereIn('user_id', $user_ids)
                ->orderBy('updated_at', 'desc');

            $this->t = count($obj->get()->toArray());
            $data = $this->page($obj)->get();
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }

        $this->status = '1';
        $this->message = '数据返回成功';
        return $this->format($data);
    }

    //服务商首页 数据概览
    public function indexDataOverview(Request $request)
    {
        $start_time = $request->get('startTime', date('Y-m-01 00:00:00', time()));
        $end_time = $request->get('endTime', date('Y-m-d 23:59:59', time()));
        $token = $this->parseToken();
        $token_user_id = $token->user_id;
        $token_level = $token->level;
        $token_pid = $token->pid;

        try {
            if (date('d', strtotime($end_time) - strtotime($start_time)) > 30) {
                $this->status = 2;
                $this->message = '只支持31内日数据';
                return $this->format();
            }

            $where = [];
            $where[] = ['created_at', '>=', $start_time];
            $where[] = ['created_at', '<=', $end_time];
            $user_ids = $this->getSubIdsAll($token_user_id); //循环获取下级的用户id

            $total_mer_num = Store::where('is_close', '0')
                ->where('is_delete', '0')
                ->whereIn('user_id', $user_ids)
                ->where($where)
                ->count();

            $order_total_2_sum = 0;
            $order_total_3_sum = 0;
            for (; $token_level < 4; $token_level++) {
                if ($token_level == 2) {
                    $user_obj_level_2 = User::whereIn('pid', $user_ids)
                        ->where('level', '2')
                        ->select('id')
                        ->get()
                        ->toArray(); //2级代理商
                    if ($user_obj_level_2 && $user_obj_level_2 != []) {
                        $user_ids_level_2 = array_column($user_obj_level_2, 'id');
                        $order_total_2 = Order::whereIn('user_id', $user_ids_level_2)
                            ->where($where)
                            ->whereIn('pay_status', [1, 6])//成功+退款
                            ->select('total_amount', 'receipt_amount', 'refund_amount', 'mdiscount_amount')
                            ->get();
                        $order_total_2_sum = $order_total_2->sum('total_amount');
                    }
                }
                if ($token_level == 3) {
                    $user_obj_level_3 = User::whereIn('pid', $user_ids)
                        ->where('level', '3')
                        ->select('id')
                        ->get()
                        ->toArray(); //2级代理商
                    if ($user_obj_level_3 && $user_obj_level_3 != []) {
                        $user_ids_level_3 = array_column($user_obj_level_3, 'id');
                        $order_total_3 = Order::whereIn('user_id', $user_ids_level_3)
                            ->where($where)
                            ->whereIn('pay_status', [1, 6])//成功+退款
                            ->select('total_amount', 'receipt_amount', 'refund_amount', 'mdiscount_amount')
                            ->get();
                        $order_total_3_sum = $order_total_3->sum('total_amount');
                    }
                }
            }
            $order_total = Order::whereIn('user_id', $user_ids)
                ->where($where)
                ->whereIn('pay_status', [1, 6]) //成功+退款
                ->select('total_amount', 'receipt_amount', 'refund_amount', 'mdiscount_amount')
                ->get();
            $order_total_sum = $order_total->sum('total_amount');

            $year = date('Y', strtotime($start_time));
            $orderYear = 'orders' . $year;
            if (env('DB_D1_HOST')) {
                if (Schema::hasTable("$orderYear")) {
                    $table = DB::table("$orderYear");
                } else {
                    $table = DB::table('orders');
                }
            } else {
                if (Schema::hasTable("$orderYear")) {
                    $table = DB::table("$orderYear");
                } else {
                    $table = DB::table('orders');
                }
            }

            $total_commission_obj = $table->select([
                DB::raw("SUM((`total_amount` - `mdiscount_amount`) * (rate - cost_rate)) AS `total_commission`"), 'company'
            ])
                ->where($where)
                ->whereIn('pay_status', [1, 6])
                ->whereIn('user_id', $user_ids)
                ->first();
            $total_commission = $total_commission_obj->total_commission;
            $user_profit_ratio = User::where('id', $token_user_id)->select('profit_ratio')->first();
            if ($total_commission_obj->company == 'ccbankpay') { //建行通道特殊处理
                $total_commission = $total_commission / 100;
            } else {
                $total_commission = $total_commission * $user_profit_ratio->profit_ratio / 10000;
            }

//            $user_wallet_detail_obj = UserWalletDetail::whereIn('user_id', $user_ids)
//                ->whereIn('settlement', ['02', '01'])
//                ->where($where)
//                ->select('money');
//            $total_commission = $user_wallet_detail_obj->sum('money');

            $data = [
                'order_total_sum' => number_format($order_total_sum, 2), //交易总额
                'total_commission' => $total_commission ? number_format($total_commission, 2) : 0, //佣金总额
                'order_total_2_sum' => $order_total_2_sum ? number_format($order_total_2_sum, 2) : 0, //二级代理商交易总额
                'order_total_3_sum' => $order_total_3_sum ? number_format($order_total_3_sum, 2) : 0, //三级代理商交易总额
                'total_mer_num' => $total_mer_num, //总商户数
            ];
            $this->status = 1;
            return $this->format($data);
        } catch (\Exception $ex) {
            Log::info('服务商首页-数据概览-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage();
            return $this->format();
        }
    }


    //服务商首页 交易数据
    public function indexTransactionData(Request $request)
    {
        $start_time = $request->get('startTime', date('Y-m-01 00:00:00', time()));
        $end_time = $request->get('endTime', date('Y-m-d 23:59:59', time()));
        $max_amount = $request->get('maxAmount', '');
        $lower_amount = $request->get('lowerAmount', '');
        $token = $this->parseToken();
        $token_user_id = $token->user_id;

        try {
            if (date('d', strtotime($end_time) - strtotime($start_time)) > 30) {
                $this->status = 2;
                $this->message = '只支持31内日数据';
                return $this->format();
            }

            $where = [];
            $where[] = ['created_at', '>=', $start_time];
            $where[] = ['created_at', '<=', $end_time];
            if ($max_amount) {
                $where = ['total_amount', '<=', $max_amount];
            }
            if ($lower_amount) {
                $where = ['total_amount', '>=', $lower_amount];
            }

            $year = date('Y', strtotime($start_time));
            $orderYear = 'orders' . $year;
            if (env('DB_D1_HOST')) {
                if (Schema::hasTable("$orderYear")) {
                    $table = DB::table("$orderYear");
                } else {
                    $table = DB::table('orders');
                }
            } else {
                if (Schema::hasTable("$orderYear")) {
                    $table = DB::table("$orderYear");
                } else {
                    $table = DB::table('orders');
                }
            }

            $order_obj = $table->select([
                'total_amount',
                'company',
                DB::raw("COUNT(`id`) AS `total_commission_num`"),
                DB::raw("SUM((`total_amount` - `mdiscount_amount`) * (rate - cost_rate)) AS `total_commission`"),
                DB::raw("SUM(`total_amount`) AS `transaction_amount`"),
                DB::raw("SUM(`total_amount` - `mdiscount_amount`) AS `actual_revenue`"),
            ])
                ->where($where)
                ->whereIn('pay_status', [1, 6])
                ->where('user_id', $token_user_id)
                ->first();
            $number_of_commission = $order_obj->total_commission_num;
            $my_commission = $order_obj->total_commission;
            $user_profit_ratio = User::where('id', $token_user_id)->select('profit_ratio')->first();
            if ($order_obj->company == 'ccbankpay') { //建行通道特殊处理
                $my_commission = $my_commission / 100;
            } else {
                $my_commission = $my_commission * $user_profit_ratio->profit_ratio / 10000;
            }

            $actual_revenue = $order_obj->actual_revenue;
            $transaction_amount = $order_obj->transaction_amount;

            $order_refund_obj = $table->select('refund_amount')
                ->where($where)
                ->where('pay_status', 6)
                ->where('user_id', $token_user_id);
            $refund_amount = $order_refund_obj->sum('refund_amount');
            $number_of_refunds = $order_refund_obj->count('refund_amount');

            $data = [
                'transaction_amount' => $transaction_amount ? number_format($transaction_amount, 2) : 0.00, //交易金额
                'number_of_commission' => $number_of_commission ? $number_of_commission : 0, //佣金笔数
                'refund_amount' => $refund_amount ? number_format($refund_amount, 2) : 0.00, //退款金额
                'number_of_refunds' => $number_of_refunds ? $number_of_refunds : 0, //退款笔数
                'actual_revenue' => $actual_revenue ? number_format($actual_revenue, 2) : 0.00, //实际营收
                'my_commission' => $my_commission ? number_format($my_commission, 2) : 0.00 //我的佣金
            ];
            $this->status = 1;
            return $this->format($data);
        } catch (\Exception $ex) {
            Log::info('服务商首页-交易数据-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage();
            return $this->format();
        }
    }


    //服务商首页 支付通道金额/笔数
    public function passagewayStatistics(Request $request)
    {
        $start_time = $request->get('startTime', date('Y-m-01 00:00:00', time()));
        $end_time = $request->get('endTime', date('Y-m-d 23:59:59', time()));
        $max_amount = $request->get('maxAmount', '');
        $lower_amount = $request->get('lowerAmount', '');
        $token = $this->parseToken();
        $token_user_id = $token->user_id;

        try {
            if (date('d', strtotime($end_time) - strtotime($start_time)) > 30) {
                $this->status = 2;
                $this->message = '只支持31内日数据';
                return $this->format();
            }

            $where = [];
            $where[] = ['created_at', '>=', $start_time];
            $where[] = ['created_at', '<=', $end_time];
            if ($max_amount) {
                $where = ['total_amount', '<=', $max_amount];
            }
            if ($lower_amount) {
                $where = ['total_amount', '>=', $lower_amount];
            }
            $user_ids = $this->getSubIdsAll($token_user_id); //循环获取下级的用户id

            $year = date('Y', strtotime($start_time));
            $orderYear = 'orders' . $year;
            if (env('DB_D1_HOST')) {
                if (Schema::hasTable("$orderYear")) {
                    $table = DB::table("$orderYear");
                } else {
                    $table = DB::table('orders');
                }
            } else {
                if (Schema::hasTable("$orderYear")) {
                    $table = DB::table("$orderYear");
                } else {
                    $table = DB::table('orders');
                }
            }

            $order_obj = $table->select([
                'company',
                DB::raw("COUNT(`id`) as `company_count`"),
                DB::raw("SUM(`total_amount`) as `company_sum`"),
            ])
                ->where($where)
                ->whereIn('pay_status', [1, 6])
                ->where('user_id', $user_ids)
                ->groupBy('company')
                ->get();

            $this->status = 1;
            return $this->format($order_obj);
        } catch (\Exception $ex) {
            Log::info('服务商首页-支付通道金额/笔数-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage();
            return $this->format();
        }
    }

    public function order_app(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id', '');
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');
            $store_id = $request->get('store_id', '');
            $store_type = $request->get('store_type', '');
            $day = date('Ymd', strtotime($time_start));
            $table = 'orders_' . $day;

            //限制时间
            $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
            $day = 31;
            $now_time = date('Y-m-d H:i:s', time());
            $end = date('Y-m-d 22:00:00', time());
            if ($now_time > $end) {
                $day = 31;
            }

            if ($date > $day) {
                return json_encode([
                    'status' => 2,
                    'message' => '时间跨度不能超过' . $day . '天'
                ]);
            }

            //跨天操作
            $time_start_db = date('Ymd', strtotime($time_start));
            $time_end_db = date('Ymd', strtotime($time_end));
            $is_ct_time = 0;
            if ($time_start_db != $time_end_db) {
                $is_ct_time = 1;
            }

            if (env('DB_D1_HOST')) {
                //有没有跨天
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj = DB::connection("mysql_d1")->table('order_items');
                    } else {
                        if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                            $obj = DB::connection("mysql_d1")->table('orders2020');
                        } else {
                            $obj = DB::connection("mysql_d1")->table('orders');
                        }
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj = DB::connection("mysql_d1")->table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj = DB::connection("mysql_d1")->table('order_items');

                        } else {
                            if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                                $obj = DB::connection("mysql_d1")->table('orders2020');
                            } else {
                                $obj = DB::connection("mysql_d1")->table('orders');
                            }
                        }
                    }
                }
            } else {
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj = DB::table('order_items');
                    } else {
                        if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                            $obj = DB::table('orders2020');
                        } else {
                            $obj = DB::table('orders');
                        }
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj = DB::table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj = DB::table('order_items');
                        } else {
                            if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                                $obj = DB::table('orders2020');
                            } else {
                                $obj = DB::table('orders');
                            }
                        }
                    }
                }
            }

            $where = [];

            if ($user_id == "") {
                $user_id = $user->user_id;
            }
//            if($user_id==1){
//                $ids=$this->getSubIdsAll($user_id);
//            }else{
//                $ids=$this->getSubIds($user_id);
//                if($store_type=='3'){
//                    unset($ids[0]);
//                }
//            }
            $ids = $this->getSubIdsAll($user_id);
            if ($store_type == '3') {
                unset($ids[0]);
            }
            if ($store_id) {
                $where[] = ['store_id', $store_id];
            }
            if ($time_start) {
                $where[] = ['created_at', '>=', $time_start];
            }

            if ($time_end) {
                $where[] = ['created_at', '<=', $time_end];
            }
            if ($store_type == '2') {
                $obj = $obj->where($where)
                    ->whereIn('pay_status', [1, 6])
                    ->where('user_id', $user_id)
                    ->orderBy('created_at', 'desc');
            } else {
                $obj = $obj->where($where)
                    ->whereIn('pay_status', [1, 6])
                    ->whereIn('user_id', $ids)
                    ->orderBy('created_at', 'desc');
            }

            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }

    //对账统计-比较全-实时-app
    public function order_count_app(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id', '');
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');
            $store_id = $request->get('store_id', '');

            //限制时间
            $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
            $day = 31;

            $now_time = date('Y-m-d H:i:s', time());
            $end = date('Y-m-d 22:00:00', time());
            if ($now_time > $end) {
                $day = 31;
            }

            //限制时间
            if ($date > $day) {
                return json_encode([
                    'status' => 2,
                    'message' => '时间跨度不能超过' . $day . '天'
                ]);
            }
            $where = [];
            //条件查询
            if ($time_start) {
                $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                $where[] = ['created_at', '>=', $time_start];
            }
            if ($time_end) {
                $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                $where[] = ['created_at', '<=', $time_end];
            }
            if ($store_id) {
                $where[] = ['store_id', $store_id];
            }
            $obj = DB::table('orders');
            $order_data = $obj->select([
                'total_amount',
                DB::raw("SUM(`total_amount`) as `total_amount`")])
                ->where('user_id', $user_id)
                ->where($where)
                ->whereIn('pay_status', [1, 6])//成功+退款
                ->first();
            $total_amount = $order_data->total_amount;
            $order_obj_num = DB::table('orders');
            $order_data_num = $order_obj_num
                ->where('user_id', $user_id)
                ->where($where)
                ->where('pay_status', 1)//成功+退款
                ->select('id')
                ->get();
            $total_count = count($order_data_num);

            $obj_success = DB::table('orders');
            $order_data_success = $obj_success->select([
                'total_amount',
                DB::raw("SUM(`total_amount`) as `total_amount`")])
                ->where('user_id', $user_id)
                ->where($where)
                ->where('pay_status', 1)//成功
                ->first();
            $get_amount = $order_data_success->total_amount;

            $refund_obj = DB::table('orders');
            $refund_data = $refund_obj->select([
                'total_amount',
                DB::raw("SUM(`total_amount`) as `total_amount`")])
                ->where('user_id', $user_id)
                ->where($where)
                ->where('pay_status', 6)//成功+退款
                ->first();
            $refund_amount = $refund_data->total_amount;

            $refund_obj_num = DB::table('orders');
            $order_data_num = $refund_obj_num
                ->where('user_id', $user_id)
                ->where($where)
                ->where('pay_status', 6)//成功+退款
                ->select('id')
                ->get();
            $refund_count = count($order_data_num);

            $total_amount = $total_amount;//交易金额
            $refund_amount = $refund_amount;//退款金额
            $get_amount = $get_amount;//商家实收，交易金额-退款金额-商家优惠金额-会员卡支付金额


            $data = [
                'total_amount' => number_format($total_amount, 2, '.', ''),//交易金额
                'total_count' => $total_count,//交易笔数
                'refund_count' => $refund_count,//退款笔数
                'get_amount' => number_format($get_amount, 2, '.', ''),//商家实收，交易金额-退款金额
                'refund_amount' => number_format($refund_amount, 2, '.', ''),//退款金额
            ];

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }

    public function sub_order_count_app(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id', '');
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');
            $day = date('Ymd', strtotime($time_start));
            $table = 'orders_' . $day;

            //限制时间
            $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
            $day = 31;
            $now_time = date('Y-m-d H:i:s', time());
            $end = date('Y-m-d 22:00:00', time());
            if ($now_time > $end) {
                $day = 31;
            }

            if ($date > $day) {
                return json_encode([
                    'status' => 2,
                    'message' => '时间跨度不能超过' . $day . '天'
                ]);
            }

            //跨天操作
            $time_start_db = date('Ymd', strtotime($time_start));
            $time_end_db = date('Ymd', strtotime($time_end));
            $is_ct_time = 0;
            if ($time_start_db != $time_end_db) {
                $is_ct_time = 1;
            }

            if (env('DB_D1_HOST')) {
                //有没有跨天
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj = DB::connection("mysql_d1")->table('order_items');
                    } else {
                        if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                            $obj = DB::connection("mysql_d1")->table('orders2020');
                        } else {
                            $obj = DB::connection("mysql_d1")->table('orders');
                        }
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj = DB::connection("mysql_d1")->table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj = DB::connection("mysql_d1")->table('order_items');

                        } else {
                            if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                                $obj = DB::connection("mysql_d1")->table('orders2020');
                            } else {
                                $obj = DB::connection("mysql_d1")->table('orders');
                            }
                        }
                    }
                }
            } else {
                if ($is_ct_time) {
                    if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                        $obj = DB::table('order_items');
                    } else {
                        if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                            $obj = DB::table('orders2020');
                        } else {
                            $obj = DB::table('orders');
                        }
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $obj = DB::table($table);
                    } else {
                        if (Schema::hasTable('order_items') && $time_start >= "2019-12-21 00:00:00") {
                            $obj = DB::table('order_items');
                        } else {
                            if (Schema::hasTable('orders2020') && $time_start < '2021-01-01 00:00:00') {
                                $obj = DB::table('orders2020');
                            } else {
                                $obj = DB::table('orders');
                            }
                        }
                    }
                }
            }

            $where = [];

            if ($user_id == "") {
                $user_id = $user->user_id;
            }
//            if ($user_id == 1) {
//                $ids = $this->getSubIdsAll($user_id);
//
//            }else{
//                $ids = $this->getSubIds($user_id);
//            }
            $ids = $this->getSubIdsAll($user_id);
            unset($ids[0]);
            if ($time_start) {
                $where[] = ['created_at', '>=', $time_start];
            }

            if ($time_end) {
                $where[] = ['created_at', '<=', $time_end];
            }

            $order_data = $obj->select([
                DB::raw("SUM(`total_amount`) as `total_amount`"),
            ])->where($where)
                ->whereIn('user_id', $ids)
                ->where('pay_status', 1)//成功+退款
                ->first();

            $sub_total_amount = $order_data->total_amount;
            $data = [
                'sub_total_amount' => number_format($sub_total_amount, 2, '.', ''),//交易金额
            ];

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


}
