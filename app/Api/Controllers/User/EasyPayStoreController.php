<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2021/3/18
 * Time: 11:57
 */

namespace App\Api\Controllers\User;

use App\Api\Controllers\BaseController;
use App\Api\Controllers\Config\EasyPayConfigController;
use App\Api\Controllers\EasyPay\MerAccessController;
use App\Models\EasypayAreaNo;
use App\Models\EasypayBanks;
use App\Models\EasypayConfig;
use App\Models\EasypayMcc;
use App\Models\EasypayProject;
use App\Models\EasypayStore;
use App\Models\EasypayStoresImages;
use App\Models\Store;
use App\Models\StoreBank;
use App\Models\StoreImg;
use App\Models\StorePayWay;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class EasyPayStoreController extends BaseController
{
    //获取易生地区
    public function getAreaNo(Request $request)
    {
        $nowPage = $request->get('p', 1); //当前页
        $limit = $request->get('l', 20); //每页显示数量
        $areaCode = $request->get('area_code', 0); //编号

        $city = EasypayAreaNo::where('yl_pr', $areaCode);

        $this->t = $city->count();
        $this->p = $nowPage;
        $this->l = $limit;
        $data = $this->page($city)->get();
        $this->status = 1;
        return $this->format($data);
    }


    //获取易生银行信息
    public function getBanks(Request $request)
    {
        $nowPage = $request->get('p', 1); //当前页
        $limit = $request->get('l', 20); //每页显示数量
        $type = $request->get('type', 1); //类型 1-总行；2-支行
        $name = $request->get('name', ''); //支行名称

        if ($type == 1) {
            $banks = EasypayBanks::where('bank_head_name', 'like', $name . '%');
        } else {
            $banks = EasypayBanks::where('bank_name', 'like', $name . '%');
        }

        $this->t = $banks->count();
        $this->p = $nowPage;
        $this->l = $limit;
        $data = $this->page($banks)->get();
        $this->message = '数据返回成功';
        $this->status = 1;
        return $this->format($data);
    }


    //获取易生mcc编码
    public function getMcc(Request $request)
    {
        $nowPage = $request->get('p', 1); //当前页
        $limit = $request->get('l', 20); //每页显示数量
        $name = $request->get('name', ''); //分类名称

        $where = [];
        if ($name) {
            $where[] = ['mcc_name', 'like', '%' . $name . '%'];
        }

        $banks = EasypayMcc::where($where);

        $this->t = $banks->count();
        $this->p = $nowPage;
        $this->l = $limit;
        $data = $this->page($banks)->get();
        $this->status = 1;
        return $this->format($data);
    }

    //获取易生项目ID编码
    public function getProjectId(Request $request)
    {
        $nowPage = $request->get('p', 1); //当前页
        $limit = $request->get('l', 20); //每页显示数量
        $project_name = $request->get('project_name', ''); //项目名称

        $where = [];
        if ($project_name) {
            $where[] = ['project_name', 'like', '%' . $project_name . '%'];
        }

        $projects = EasypayProject::where($where);

        $this->t = $projects->count();
        $this->p = $nowPage;
        $this->l = $limit;
        $data = $this->page($projects)->get();
        $this->status = 1;
        return $this->format($data);
    }

    //易生商户入网 app进件
    public function addEasyPayStoreImages(Request $request, $data)
    {
        try {

            $store_id = $data['store_id'];
            $config_id = $data['config_id'];
            $source_type = $data['source_type'];
            $source = $data['source'];

            //$new_config_id = '1234';

            $easyPayConfigsObj = EasypayConfig::where('is_use', '1')->first();
            if (!$easyPayConfigsObj) {
                return [
                    'status' => -1,
                    'message' => '易生支付未配置'
                ];
            }
            $new_config_id = $easyPayConfigsObj->config_id;
            $client_code = $easyPayConfigsObj->client_code;
            $key = $easyPayConfigsObj->key;

            $saveStep = $request->get('saveStep', '1'); // 1执照信息 、2账户信息 3、门店信息 4、功能信息 5、
            $bankType = $request->get('bankType', ''); // 01 法人对公 02对公 03非法人对私

            if ($saveStep == 1) {
                $mer_area = $request->get('mer_area', ''); //商户区域:银联地区码
                $area_no = $request->get('area_no', '');
                $term_area = $request->get('term_area', '');
                $ter_user_name = $request->get('ter_user_name', ''); //终端名称

                $insert_data = [
                    'config_id' => $config_id,
                    'store_id' => $store_id,
                    'area_no' => $area_no,
                    'mer_area' => $mer_area,
                    'term_area' => $term_area,
                    'ter_user_name' => $ter_user_name,
                    'new_config_id' => $new_config_id
                ];

                $easyPayStoresImagesObj = EasypayStoresImages::where('store_id', $store_id)->first();
                if ($easyPayStoresImagesObj) {

                    $result = $easyPayStoresImagesObj->update($insert_data);
                    if (!$result) {
                        return [
                            'status' => -1,
                            'message' => '更新失败'
                        ];
                    }
                } else {
                    $result = EasypayStoresImages::create($insert_data);
                    if (!$result) {
                        return [
                            'status' => -1,
                            'message' => '添加失败'
                        ];
                    }
                }

            } else if ($saveStep == 2) {
                $hand_idcard_url = '';
                $hand_idcard_front_url = '';
                if ($bankType == '01') {
                    $hand_idcard_url = $request->get('bank_sc_img', ''); //手持身份证照片
                } else if ($bankType == '02') {

                } else if ($bankType == '03') {

                    $hand_idcard_url = $request->get('bank_sc_img', ''); //手持身份证照片
                    $hand_idcard_front_url = $request->get('bank_sfz_img_a', ''); //收款人身份证正面
                }
                $licence = $request->get('store_industrylicense_img', ''); //开户许可证图片地址 企业对私结算时,必传
                $cert_of_auth_url = $request->get('certOfAuth', ''); //清算授权书图片地址(非必填)

                $bank_name = $request->get('sub_bank_name', ''); //开户行名称支行
                $bank_code = $request->get('bank_no', ''); //开户行行号
                $pubBankName = $request->get('sub_bank_name', ''); //第二个对公结算账户信息,开户行名称 企业对私结算时,必传
                $pubBankCode = $request->get('bank_no', ''); //第二个对公结算账户信息,开户行行号 企业对私结算时,必传
                $pubAccount = $request->get('store_bank_no', ''); //第二个对公结算账户信息,账号 企业对私结算时,必传
                $pubAccName = $request->get('store_bank_name', ''); //第二个对公结算账户信息,账户名称 企业对私结算时,必传

                $hand_idcard = '';
                $hand_idcard_front = '';
                if ($hand_idcard_url) {//17-手持身份证照片（选填）
                    $res = $this->upload_image($hand_idcard_url, '17', $client_code, $key);
                    if ($res['status'] == 1) {
                        $hand_idcard = $res['data']['fileId'];
                    }
                }
                if ($hand_idcard_front_url) {//18-收款人身份证正面（选填）
                    $res = $this->upload_image($hand_idcard_front_url, '18', $client_code, $key);
                    if ($res['status'] == 1) {
                        $hand_idcard_front = $res['data']['fileId'];
                    }
                }
                $CertOfAuth = '';
                if ($cert_of_auth_url) {
                    $res = $this->upload_image($cert_of_auth_url, '15', $client_code, $key);
                    if ($res['status'] == 1) {
                        $CertOfAuth = $res['data']['fileId'];
                    }
                }

                $insert_data = [
                    'config_id' => $config_id,
                    'store_id' => $store_id,
                    'bank_name' => $bank_name,
                    'bank_code' => $bank_code,
                    'pub_bank_name' => $pubBankName,
                    'pub_bank_code' => $pubBankCode,
                    'pub_account' => $pubAccount,
                    'pub_acc_name' => $pubAccName,
                    'licence' => $licence,
                    'hand_idcard' => $hand_idcard,
                    'hand_idcard_url' => $hand_idcard_url == null ? '' : $hand_idcard_url,
                    'hand_idcard_front' => $hand_idcard_front,
                    'hand_idcard_front_url' => $hand_idcard_front_url == null ? '' : $hand_idcard_front_url,
                    'cert_of_auth' => $CertOfAuth,
                    'cert_of_auth_url' => $cert_of_auth_url,
                ];

                $easyPayStoresImagesObj = EasypayStoresImages::where('store_id', $store_id)->first();
                if ($easyPayStoresImagesObj) {
                    $result = $easyPayStoresImagesObj->update($insert_data);
                    if (!$result) {
                        return [
                            'status' => -1,
                            'message' => '更新失败'
                        ];
                    }
                } else {
                    $result = EasypayStoresImages::create($insert_data);
                    if (!$result) {
                        return [
                            'status' => -1,
                            'message' => '添加失败'
                        ];
                    }
                }

            } else if ($saveStep == 3) { //门店信息
                $busin_form = $request->get('businForm', '');
                $mcc_id = $request->get('mccId', ''); //mcc值
                $busin_scope = $request->get('businScope', ''); //主营业务
                $agreement = $request->get('agreement', '');    //协议图片地址
                $location_photo_url = $request->get('location_photo_url', ''); //定位照
                $busin_beg_time = $request->get('busin_beg_time', '05:00'); //营业时间:开始时间,格式:HHMM
                $busin_end_time = $request->get('busin_end_time', '23:00'); //营业时间:结束时间,格式:HHMM
                $capital = $request->get('capital', '50'); //注册资本：单位-万元
                $term_mode = $request->get('termMode', '0'); //终端种类(0--POS终端,所有交易走易生的实体终端;2--虚拟终端;4--银联POS,直联POS,终端商户号和终端终端号,秘钥非易生的;5--其他POS,先到银联申请终端商户号和终端号,秘钥用易;6--二维码终端,(默认为0)
                $standard_flag = $request->get('standardFlag', '0'); //行业大类(0-标准,1-优惠,2-减免)
                $employee_num = $request->get('employeeNum', '1'); //公司规模(1:0-50人,2:50-100人,3:100以上)
                $house_number = $request->get('store_logo_img', ''); //门牌号图片地址

                $location_photo = '';
                if ($location_photo_url) {//16-定位照（选填）
                    $res = $this->upload_image($location_photo_url, '16', $client_code, $key);
                    if ($res['status'] == 1) {
                        $location_photo = $res['data']['fileId'];
                    }
                }
                $HouseNumberUrl = '';
                $HouseNumber = '';
                if ($house_number) {
                    $res = $this->upload_image($house_number, '09', $client_code, $key);
                    if ($res['status'] != 1) {
                        return [
                            'status' => -1,
                            'message' => '门牌号上传失败'
                        ];
                    }
                    $HouseNumberUrl = $house_number;
                    $HouseNumber = $res['data']['fileId'];
                }
                $AgreementUrl = '';
                $Agreement = '';
                if ($agreement) {//租赁合同 11
                    $res = $this->upload_image($agreement, '11', $client_code, $key);
                    if ($res['status'] != 1) {
                        return [
                            'status' => -1,
                            'message' => '租赁合同上传失败'
                        ];
                    }
                    $AgreementUrl = $agreement;
                    $Agreement = $res['data']['fileId'];
                }

                $insert_data = [
                    'config_id' => $config_id,
                    'store_id' => $store_id,
                    'busin_form' => $busin_form,
                    'mcc_id' => $mcc_id,
                    'busin_scope' => $busin_scope,
                    'agreement' => $Agreement,
                    'agreement_url' => $AgreementUrl,
                    'location_photo' => $location_photo,
                    'location_photo_url' => $location_photo_url,
                    'busin_beg_time' => $busin_beg_time,
                    'busin_end_time' => $busin_end_time,
                    'capital' => $capital,
                    'term_mode' => $term_mode,
                    'standard_flag' => $standard_flag,
                    'employee_num' => $employee_num,
                    'house_number' => $HouseNumber,
                    'house_number_url' => $HouseNumberUrl,
                ];

                $easyPayStoresImagesObj = EasypayStoresImages::where('store_id', $store_id)->first();
                if ($easyPayStoresImagesObj) {
                    Log::info("----3--->" . $insert_data);
                    $result = $easyPayStoresImagesObj->update($insert_data);
                    if (!$result) {
                        return [
                            'status' => -1,
                            'message' => '更新失败'
                        ];
                    }
                } else {
                    $result = EasypayStoresImages::create($insert_data);
                    if (!$result) {
                        return [
                            'status' => -1,
                            'message' => '添加失败'
                        ];
                    }
                }

            } else if ($saveStep == 4) { //功能信息
                $two_bars = $request->get('twoBars', '[]'); //立招功能([{"twoBarCode":"二维码编号,30字符以内"},{"termName":"二维码名称,60字符以内"}],[]...)
                $two_bars_open = $request->get('twoBarsOpen', '2'); //是否开启立招功能(1-开启;2-关闭)
                $real_time_entry = $request->get('realTimeEntry', '0'); //实时入账(0-否;1-是)
                $d_stlm_type = $request->get('dStlmType', '0'); //借记卡扣率方式(1-封顶;0-不封顶)
                $d_calc_val = $request->get('dCalcVal', '0.02'); //借记卡扣率
                $un_d_calc_val = $request->get('unDCalcVal', '0.02'); //银联二维码借记卡扣率
                $d_stlm_max_amt = $request->get('dStlmMaxAmt', '25'); //借记卡封顶金额
                $d_fee_low_limit = $request->get('dFeeLowLimit', '0'); //借记卡手续费最低值
                $c_calc_val = $request->get('cCalcVal', '0.02'); //信用卡扣率
                $un_c_calc_val = $request->get('unCCalcVal', '0.02'); //银联二维码信用卡扣率
                $c_fee_low_limit = $request->get('cFeeLowLimit', '0'); //信用卡手续费最低值
                $d_calc_type = $request->get('dCalcType', 1); //借记卡计费类型(0-按笔数(单位:元);1-按比例(单位:%)),实时入账必传
                $c_calc_type = $request->get('cCalcType', 1); //信用卡计费类型(0-按笔数(单位:元);1-按比例(单位:%)),实时入账必传
                $calc_type = $request->get('calcType', 1); //D1计算方式(1-按比例;0-按笔)
                $calc_val = $request->get('calcVal', '0'); //D1手续费
                $pay_date_type = $request->get('payDateType', 0); //D1费用处理模式(0-每天计费,1-节假日,2-节假日按天)
                $d1_is_open = $request->get('d1IsOpen', '0'); //是否开通D1(0-否;1-是)

                $insert_data = [
                    'config_id' => $config_id,
                    'store_id' => $store_id,
                    'real_time_entry' => $real_time_entry,
                    'd_stlm_type' => $d_stlm_type,
                    'd_calc_val' => $d_calc_val,
                    'un_d_calc_val ' => $un_d_calc_val,
                    'd_stlm_max_amt' => $d_stlm_max_amt,
                    'd_fee_low_limit' => $d_fee_low_limit,
                    'c_calc_val' => $c_calc_val,
                    'un_c_calc_val' => $un_c_calc_val,
                    'c_fee_low_limit' => $c_fee_low_limit,
                    'd_calc_type' => $d_calc_type,
                    'c_calc_type' => $c_calc_type,
                    'calc_type' => $calc_type,
                    'calc_val' => $calc_val,
                    'pay_date_type' => $pay_date_type,
                    'two_bars_open' => $two_bars_open,
                    'd1_is_open' => $d1_is_open,
                ];

                if ($two_bars_open == '1' && $two_bars && ($two_bars != '[]' || $two_bars != '[[{}]]' || $two_bars != '[[{"":""}]]')) {
                    $insert_data['two_bars'] = json_encode($two_bars);
                }

                $easyPayStoresImagesObj = EasypayStoresImages::where('store_id', $store_id)->first();
                if ($easyPayStoresImagesObj) {
                    Log::info("----4--->" . $insert_data);
                    $result = $easyPayStoresImagesObj->update($insert_data);
                    if (!$result) {
                        return [
                            'status' => -1,
                            'message' => '更新失败'
                        ];
                    }
                } else {
                    $result = EasypayStoresImages::create($insert_data);
                    if (!$result) {
                        return [
                            'status' => -1,
                            'message' => '添加失败'
                        ];
                    }
                }

            } else if ($saveStep == 5) {
                $contract_no = $request->get('contractNo', ''); //电子协议编号
                $signatory_name = $request->get('signatoryName', ''); //签署方名称
                $insert_data = [
                    'config_id' => $config_id,
                    'store_id' => $store_id,
                    'signatory_name' => $signatory_name,
                    'contract_no' => $contract_no,
                ];

                $easyPayStoresImagesObj = EasypayStoresImages::where('store_id', $store_id)->first();
                if ($easyPayStoresImagesObj) {
                    Log::info("----5--->");
                    $result = $easyPayStoresImagesObj->update($insert_data);
                    if (!$result) {
                        return [
                            'status' => -1,
                            'message' => '更新失败'
                        ];
                    }
                } else {
                    $result = EasypayStoresImages::create($insert_data);
                    if (!$result) {
                        return [
                            'status' => -1,
                            'message' => '添加失败'
                        ];
                    }
                }
            }


            return [
                'status' => 1,
                'message' => '操作成功'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 1,
                'message' => $ex->getMessage() . ' | ' . $ex->getLine()
            ];
        }
    }


    //易生商户入网 材料补充
    public function easyPayMerchantAccess(Request $request)
    {
        try {
            $type = $request->get('type', '1'); //2-查询数据
            $store_id = $request->get('storeId', ''); //门店id
            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                $this->status = 2;
                $this->message = '门店不存在或状态异常';
                return $this->format();
            }

            $easyPayStoresImagesObj = EasypayStoresImages::where('store_id', $store_id)->first();
            if ($type == 2) {
                if ($easyPayStoresImagesObj) {
                    $storeImageObj = StoreImg::where('store_id', $store_id)->first();
                    if ($storeImageObj) {

                        $easyPayStoresImagesObj['store_industrylicense_img'] = $storeImageObj->store_industrylicense_img;
                    }
                }
                $this->status = 1;
                $this->message = '返回数据成功';
                return $this->format($easyPayStoresImagesObj);
            }

            $branchName = $request->get('branchName', ''); //合作银行支行
            $branchManager = $request->get('branchManager', ''); //合作银行客户经理
            $merMark = $request->get('merMark', ''); //备注
            $contract_no = $request->get('contractNo', ''); //电子协议编号
            $signatory_name = $request->get('signatoryName', ''); //签署方名称
            $project_id = $request->get('projectId', ''); //projectId项目归属ID
            $mcc_id = $request->get('mccId', ''); //mcc值
            $standard_flag = $request->get('standardFlag', ''); //行业大类(0-标准,1-优惠,2-减免)
            $employee_num = $request->get('employeeNum', ''); //公司规模(1:0-50人,2:50-100人,3:100以上)
            $busin_form = $request->get('businForm', ''); //经营形态(02-普通店,01-连锁店)
            $busin_beg_time = $request->get('businBegTime', ''); //营业时间:开始时间,格式:HHMM
            $busin_end_time = $request->get('businEndTime', ''); //营业时间:结束时间,格式:HHMM
            $term_mode = $request->get('termMode', '0'); //终端种类(0--POS终端,所有交易走易生的实体终端;2--虚拟终端;4--银联POS,直联POS,终端商户号和终端终端号,秘钥非易生的;5--其他POS,先到银联申请终端商户号和终端号,秘钥用易;6--二维码终端,(默认为0)
            $house_number = $request->get('houseNumber', ''); //门牌号图片地址
            $agreement = $request->get('agreement', ''); //协议图片地址
            $merc_regist_form_a = $request->get('mercRegistFormA', ''); //商户登记表正面图片地址
            $merc_regist_form_b = $request->get('mercRegistFormB', ''); //商户登记表反面图片地址
            $cert_of_auth = $request->get('certOfAuth', ''); //清算授权书图片地址
            $term_code = $request->get('termCode', ''); //内部终端号
            $ter_user_name = $request->get('terUserName', ''); //终端名称
            $area_no = $request->get('areaNo', ''); //区号
            $term_area = $request->get('termArea', ''); //终端地区
            $term_model = $request->get('termModel', ''); //设备型号
            $term_model_lic = $request->get('termModelLic', ''); //机具号
            $busin_scope = $request->get('businScope', ''); //主营业务
            $capital = $request->get('capital', ''); //注册资本：单位-万元
            $bank_name = $request->get('bankName', ''); //开户行名称
            $bank_code = $request->get('bankCode', ''); //开户行行号
            $real_time_entry = $request->get('realTimeEntry', '0'); //实时入账(0-否;1-是)
            $mer_area = $request->get('merArea', ''); //商户区域:银联地区码
            $d_stlm_type = $request->get('dStlmType', '0'); //借记卡扣率方式(1-封顶;0-不封顶)
            $d_calc_val = $request->get('dCalcVal', ''); //借记卡扣率
            $un_d_calc_val = $request->get('unDCalcVal', ''); //银联二维码借记卡扣率
            $d_stlm_max_amt = $request->get('dStlmMaxAmt', ''); //借记卡封顶金额
            $d_fee_low_limit = $request->get('dFeeLowLimit', ''); //借记卡手续费最低值
            $c_calc_val = $request->get('cCalcVal', ''); //信用卡扣率
            $un_c_calc_val = $request->get('unCCalcVal', ''); //银联二维码信用卡扣率
            $c_fee_low_limit = $request->get('cFeeLowLimit', ''); //信用卡手续费最低值
            $two_bars_open = $request->get('twoBarsOpen', '2'); //是否开启立招功能(1-开启;2-关闭)
            $two_bars = $request->get('twoBars', '[]'); //立招功能([{"twoBarCode":"二维码编号,30字符以内"},{"termName":"二维码名称,60字符以内"}],[]...)
            $d1_is_open = $request->get('d1IsOpen', '0'); //是否开通D1(0-否;1-是)

            $bang_ding_commer_code = $request->get('bangDingCommerCode', ''); //渠道商户号,终端类型为4或者5时,必须上送该字段
            $bang_ding_ter_mno = $request->get('bangDingTerMno', ''); //渠道终端号,终端类型为4或者5时,必须上送该字段
            $d_calc_type = $request->get('dCalcType', ''); //借记卡计费类型(0-按笔数(单位:元);1-按比例(单位:%)),实时入账必传
            $c_calc_type = $request->get('cCalcType', ''); //信用卡计费类型(0-按笔数(单位:元);1-按比例(单位:%)),实时入账必传
            $pubBankName = $request->get('pubBankName', ''); //第二个对公结算账户信息,开户行名称 企业对私结算时,必传
            $pubBankCode = $request->get('pubBankCode', ''); //第二个对公结算账户信息,开户行行号 企业对私结算时,必传
            $pubAccount = $request->get('pubAccount', ''); //第二个对公结算账户信息,账号 企业对私结算时,必传
            $pubAccName = $request->get('pubAccName', ''); //第二个对公结算账户信息,账户名称 企业对私结算时,必传
            $store_industrylicense_img = $request->get('store_industrylicense_img', ''); //开户许可证图片地址 企业对私结算时,必传
            $calc_type = $request->get('calcType', 1); //D1计算方式(1-按比例;0-按笔)
            $calc_val = $request->get('calcVal', ''); //D1手续费
            $pay_date_type = $request->get('payDateType', 0); //D1费用处理模式(0-每天计费,1-节假日,2-节假日按天)

            $location_photo_url = $request->get('location_photo_url', ''); //定位照
            $hand_idcard_url = $request->get('hand_idcard_url', ''); //手持身份证照片
            $hand_idcard_front_url = $request->get('hand_idcard_front_url', ''); //收款人身份证正面

            $except_token_data = $request->except(['token']);

            $validate = Validator::make($except_token_data, [
                'storeId' => 'required|max:50',
                'mccId' => 'required|max:4',
                'standardFlag' => 'required|in:0,1,2',
                'employeeNum' => 'required|in:1,2,3',
                'businForm' => 'required|in:01,02',
                'businBegTime' => 'required',
                'businEndTime' => 'required',
                'houseNumber' => 'required',
                'agreement' => 'required',
//                'mercRegistFormA' => 'required',
//                'mercRegistFormB' => 'required',
//                'certOfAuth' => 'required',
                'businScope' => 'required|max:500',
                'capital' => 'required|max:20',
                'bankName' => 'required|max:100',
                'bankCode' => 'required|max:20',
                'realTimeEntry' => 'required|in:0,1',
                'merArea' => 'required|max:4',
                'dStlmType' => 'required|in:0,1',
                'dCalcVal' => 'required|max:12',
                'dStlmMaxAmt' => 'required|max:8',
                'dFeeLowLimit' => 'required|max:8',
                'cCalcVal' => 'required|max:12',
                'cFeeLowLimit' => 'required|max:8',
                'dCalcType' => 'required_if:realTimeEntry,1',
                'cCalcType' => 'required_if:realTimeEntry,1',
                'twoBarsOpen' => 'required|in:1,2',
                'twoBars' => 'required_if:twoBarsOpen,1'
            ], [
                'required' => ':attribute参数为必填项',
                'min' => ':attribute参数长度太短',
                'max' => ':attribute参数长度太长',
                'unique' => ':attribute参数已经被人占用',
                'exists' => ':attribute参数不存在',
                'integer' => ':attribute参数必须是数字',
                'required_if' => ':attribute参数满足条件时必传',
            ], [
                'storeId' => '门店id',
                'mccId' => 'mcc值',
                'standardFlag' => '行业大类',
                'employeeNum' => '公司规模',
                'businForm' => '经营形态',
                'businBegTime' => '营业时间:开始时间',
                'businEndTime' => '营业时间:结束时间',
                'houseNumber' => '门牌号图片地址',
                'agreement' => '协议图片地址',
//                'mercRegistFormA' => '商户登记表正面图片地址',
//                'mercRegistFormB' => '商户登记表反面图片地址',
//                'certOfAuth' => '清算授权书图片地址',
                'businScope' => '主营业务',
                'capital' => '注册资本',
                'bankName' => '开户行名称',
                'bankCode' => '开户行行号',
                'realTimeEntry' => '实时入账',
                'merArea' => '商户区域:银联地区码',
                'dStlmType' => '借记卡扣率方式',
                'dCalcVal' => '借记卡扣率',
                'dStlmMaxAmt' => '借记卡封顶金额',
                'dFeeLowLimit' => '借记卡手续费最低值',
                'cCalcVal' => '信用卡扣率',
                'cFeeLowLimit' => '信用卡手续费最低值',
                'dCalcType' => '借记卡计费类型',
                'cCalcType' => '信用卡计费类型',
                'twoBarsOpen' => '是否开启立招功能',
                'twoBars' => '立招',
            ]);
            if ($validate->fails()) {
                $this->status = 2;
                $this->message = $validate->getMessageBag()->first();
                return $this->format();
            }

            if ($busin_beg_time) $busin_beg_time = date('Hi', strtotime($busin_beg_time));
            if ($busin_end_time) $busin_end_time = date('Hi', strtotime($busin_end_time));

//            $new_config_id = '1234';
//            $source = $storeObj->source; //来源 01 02
//            $source_type = $storeObj->source_type;
//            if ($source == '01') {
//                $new_config_id = '1234';
//            } else if ($source == '02') {
//                if ($source_type == '1') {
//                    $new_config_id = '100101';  //河南-中原项目
//                } else {
//                    $new_config_id = '1001';
//                }
//            }
            $new_config_id = $easyPayStoresImagesObj->new_config_id;
            $easyPayConfigsObj = EasypayConfig::where('config_id', $new_config_id)->first();

            if (!$easyPayConfigsObj) {
                $this->status = 2;
                $this->message = '易生支付未配置';
                return $this->format();
            }

            $client_code = $easyPayConfigsObj->client_code;
            $key = $easyPayConfigsObj->key;

            if ($easyPayStoresImagesObj) {
                $HouseNumber = $easyPayStoresImagesObj->house_number;
                $Agreement = $easyPayStoresImagesObj->agreement;
                $MercRegistFormA = $easyPayStoresImagesObj->merc_regist_form_a;
                $MercRegistFormB = $easyPayStoresImagesObj->merc_regist_form_b;
                $CertOfAuth = $easyPayStoresImagesObj->cert_of_auth;
                $HouseNumberUrl = $easyPayStoresImagesObj->house_number_url;
                $AgreementUrl = $easyPayStoresImagesObj->agreement_url;
                $MercRegistFormAUrl = $easyPayStoresImagesObj->merc_regist_form_a_url;
                $MercRegistFormBUrl = $easyPayStoresImagesObj->merc_regist_form_b_url;
                $CertOfAuthUrl = $easyPayStoresImagesObj->cert_of_auth_url;
            } else {
                $HouseNumber = '';
                $Agreement = '';
                $MercRegistFormA = '';
                $MercRegistFormB = '';
                $CertOfAuth = '';
                $HouseNumberUrl = '';
                $AgreementUrl = '';
                $MercRegistFormAUrl = '';
                $MercRegistFormBUrl = '';
                $CertOfAuthUrl = '';
            }

            if ($house_number) {
                $res = $this->upload_image($house_number, '09', $client_code, $key);
                if ($res['status'] != 1) {
                    Log::info('易生商户入网-材料补充-门牌号上传失败');
                    Log::info($res['message']);
                    $this->status = 2;
                    $this->message = '门牌号上传失败';
                    return $this->format();
                }
                $HouseNumberUrl = $house_number;
                $HouseNumber = $res['data']['fileId'];
            }
//            if ($agreement && !$Agreement) {
            if ($agreement) {
                $res = $this->upload_image($agreement, '10', $client_code, $key);
                if ($res['status'] != 1) {
                    Log::info('易生商户入网-材料补充-协议上传失败');
                    Log::info($res['message']);
                    $this->status = 2;
                    $this->message = '协议上传失败';
                    return $this->format();
                }
                $AgreementUrl = $agreement;
                $Agreement = $res['data']['fileId'];
            }
//            if ($merc_regist_form_a && !$MercRegistFormA) {
            if ($merc_regist_form_a) {
                $res = $this->upload_image($merc_regist_form_a, '11', $client_code, $key);
                if ($res['status'] != 1) {
                    Log::info('易生商户入网-材料补充-商户登记表正面上传失败');
                    Log::info($res['message']);
                    $this->status = 2;
                    $this->message = '商户登记表正面上传失败';
                    return $this->format();
                }
                $MercRegistFormAUrl = $merc_regist_form_a;
                $MercRegistFormA = $res['data']['fileId'];
            }
//            if ($merc_regist_form_b && !$MercRegistFormB) {
            if ($merc_regist_form_b) {
                $res = $this->upload_image($merc_regist_form_b, '12', $client_code, $key);
                if ($res['status'] != 1) {
                    Log::info('易生商户入网-材料补充-商户登记表反面上传失败');
                    Log::info($res['message']);
                    $this->status = 2;
                    $this->message = '商户登记表反面上传失败';
                    return $this->format();
                }
                $MercRegistFormBUrl = $merc_regist_form_b;
                $MercRegistFormB = $res['data']['fileId'];
            }
//            if ($cert_of_auth && !$CertOfAuth) {
            if ($cert_of_auth) {
                $res = $this->upload_image($cert_of_auth, '15', $client_code, $key);
                if ($res['status'] != 1) {
                    Log::info('易生商户入网-材料补充-清算授权书上传失败');
                    Log::info($res['message']);
                    $this->status = 2;
                    $this->message = '清算授权书上传失败';
                    return $this->format();
                }
                $CertOfAuthUrl = $cert_of_auth;
                $CertOfAuth = $res['data']['fileId'];
            }

            $location_photo = '';
            $hand_idcard = '';
            $hand_idcard_front = '';

            if ($location_photo_url) {//16-定位照（选填）
                $res = $this->upload_image($location_photo_url, '16', $client_code, $key);
                if ($res['status'] == 1) {
                    $location_photo = $res['data']['fileId'];
                }
            }
            if ($hand_idcard_url) {//17-手持身份证照片（选填）
                $res = $this->upload_image($hand_idcard_url, '17', $client_code, $key);
                if ($res['status'] == 1) {
                    $hand_idcard = $res['data']['fileId'];
                }
            }
            if ($hand_idcard_front_url) {//18-收款人身份证正面（选填）
                $res = $this->upload_image($hand_idcard_front_url, '18', $client_code, $key);
                if ($res['status'] == 1) {
                    $hand_idcard_front = $res['data']['fileId'];
                }
            }

            $insert_data = [
                'config_id' => $storeObj->config_id,
                'store_id' => $store_id,
                'mcc_id' => $mcc_id,
                'project_id' => $project_id,
                'branch_name' => $branchName,
                'branch_manager' => $branchManager,
                'mer_mark' => $merMark,
                'standard_flag' => $standard_flag,
                'employee_num' => $employee_num,
                'busin_form' => $busin_form,
                'busin_beg_time' => $busin_beg_time,
                'busin_end_time' => $busin_end_time,
                'house_number' => $HouseNumber,
                'house_number_url' => $HouseNumberUrl,
                'agreement' => $Agreement,
                'agreement_url' => $AgreementUrl,
                'merc_regist_form_a' => $MercRegistFormA,
                'merc_regist_form_a_url' => $MercRegistFormAUrl,
                'merc_regist_form_b' => $MercRegistFormB,
                'merc_regist_form_b_url' => $MercRegistFormBUrl,
                'cert_of_auth' => $CertOfAuth,
                'cert_of_auth_url' => $CertOfAuthUrl,
                'term_code' => $term_code,
                'ter_user_name' => $ter_user_name,
                'area_no' => $area_no,
                'term_area' => $term_area,
                'term_model' => $term_model,
                'term_model_lic' => $term_model_lic,
                'busin_scope' => $busin_scope,
                'capital' => $capital,
                'bank_name' => $bank_name,
                'bank_code' => $bank_code,
                'real_time_entry' => $real_time_entry,
                'mer_area' => $mer_area,
                'd_stlm_type' => $d_stlm_type,
                'd_calc_val' => $d_calc_val,
                'd_stlm_max_amt' => $d_stlm_max_amt,
                'd_fee_low_limit' => $d_fee_low_limit,
                'c_calc_val' => $c_calc_val,
                'c_fee_low_limit' => $c_fee_low_limit,
                'two_bars_open' => $two_bars_open,
                'd1_is_open' => $d1_is_open,
                'signatory_name' => $signatory_name,
                'contract_no' => $contract_no,
                'location_photo' => $location_photo,
                'hand_idcard' => $hand_idcard,
                'hand_idcard_front' => $hand_idcard_front,
                'location_photo_url' => $location_photo_url,
                'hand_idcard_url' => $hand_idcard_url,
                'hand_idcard_front_url' => $hand_idcard_front_url
            ];
            if ($term_mode || $term_mode === '0' || $term_mode === 0) $insert_data['term_mode'] = $term_mode;
            if ($d_calc_type || $d_calc_type === '0' || $d_calc_type === 0) $insert_data['d_calc_type'] = $d_calc_type;
            if ($c_calc_type || $c_calc_type === '0' || $c_calc_type === 0) $insert_data['c_calc_type'] = $c_calc_type;
            if ($un_d_calc_val || $un_d_calc_val === '0' || $un_d_calc_val === 0) $insert_data['un_d_calc_val'] = $un_d_calc_val;
            if ($un_c_calc_val || $un_c_calc_val === '0' || $un_c_calc_val === 0) $insert_data['un_c_calc_val'] = $un_c_calc_val;
            if ($pubBankName) $insert_data['pub_bank_name'] = $pubBankName;
            if ($pubBankCode) $insert_data['pub_bank_code'] = $pubBankCode;
            if ($pubAccount) $insert_data['pub_account'] = $pubAccount;
            if ($pubAccName) $insert_data['pub_acc_name'] = $pubAccName;
            if ($bang_ding_commer_code) $insert_data['bang_ding_commer_code'] = $bang_ding_commer_code;
            if ($bang_ding_ter_mno) $insert_data['bang_ding_ter_mno'] = $bang_ding_ter_mno;
            if ($two_bars_open == '1' && $two_bars && ($two_bars != '[]' || $two_bars != '[[{}]]' || $two_bars != '[[{"":""}]]')) {
                $insert_data['two_bars'] = json_encode($two_bars);
            }
            if ($calc_type || $calc_type === '0' || $calc_type === 0) $insert_data['calc_type'] = $calc_type;
            if ($calc_val || $calc_val === '0' || $calc_val === 0) $insert_data['calc_val'] = $calc_val;
            if ($pay_date_type || $pay_date_type === '0' || $pay_date_type === 0) $insert_data['pay_date_type'] = $pay_date_type;

            if (isset($store_industrylicense_img) && !empty($store_industrylicense_img)) {
                $store_image_obj = StoreImg::where('store_id', $store_id)->first();
                if ($store_image_obj) {
                    if (isset($store_image_obj->store_industrylicense_img) && !empty($store_image_obj->store_industrylicense_img)) {

                    } else {
                        $store_image_obj->update([
                            'store_industrylicense_img' => $store_industrylicense_img
                        ]);
                    }
                } else {
                    $this->status = 2;
                    $this->message = '门店图片数据不存';
                    return $this->format();
                }
            }

            Log::info('易生商户入网-材料补充-更新数据：');
            Log::info($insert_data);
            if ($easyPayStoresImagesObj) {
                $result = $easyPayStoresImagesObj->update($insert_data);
                if (!$result) {
                    $this->status = 2;
                    $this->message = '更新失败';
                    return $this->format();
                }
            } else {
                $result = EasypayStoresImages::create($insert_data);
                if (!$result) {
                    $this->status = 2;
                    $this->message = '添加失败';
                    return $this->format();
                }
            }

            $this->status = 1;
            return $this->format($result);
        } catch (\Exception $ex) {
            Log::info('易生商户入网 材料补充-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }


    //易生 上传图片  -1 系统错误 1-成功 2-失败
    public function upload_image($file_name, $image_type, $client_code, $key)
    {
        try {
            $img_url = explode('/', $file_name);
            $img_url = end($img_url);
            $img = public_path() . '/upload/images/' . $img_url;
//            if ($img) {
//                try {
//                    //压缩图片
//                    $img_obj = \Intervention\Image\Facades\Image::make($img);
//                    $img_obj->resize(800, 700);
//                    $img = public_path() . '/upload/s_images/' . $img_url;
//                    $img_obj->save($img);
//                } catch (\Exception $e) {
//                    Log::info('易生上传图片-压缩图片-error');
//                    Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
//                }
//            }

            $obj = new MerAccessController();
            $reqData = [
                'version' => '1.0', //版本号
                'fileName' => $img, //图片文件,该字段不参与签名
                'clientCode' => $client_code, //机构号,该字段由易生配置
                'picMode' => $image_type, //图片类型,01-法人身份证正面 ；02-法人身份证反面；03-商户联系人身份证正面；04-商户联系人身份证反面；05-营业执照；06-收银台；07-内部环境照；08-营业场所门头照；09-门牌号；10-协议；11-商户登记表正面；12-商户登记表反面；13-开户许可证；14-银行卡；15-清算授权书；16-定位照（选填）；17-手持身份证照片（选填）
            ];
            $result = $obj->imageCurl($reqData, $key); //1-成功 2-失败
            if ($result['status'] == 1) {
                if (isset($result['data']) && isset($result['data']['retCode']) && $result['data']['retCode'] == '0000') {
                    return [
                        'status' => 1,
                        'data' => $result['data'],
                        'message' => $result['data']['retMsg'] ?? $result['message']
                    ];
                } else {
                    return [
                        'status' => 2,
                        'data' => $result['data'] ?? '',
                        'message' => $result['data']['retMsg'] ?? $result['message']
                    ];
                }
            }

            return [
                'status' => 2,
                'data' => $result['data'] ?? '',
                'message' => $result['message']
            ];
        } catch (\Exception $ex) {
            Log::info('易生支付-图片上传');
            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
            return [
                'status' => -1,
                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine()
            ];
        }
    }


    /**
     * 参数查询
     *
     * 场景说明:该接口用于查询其他接口终端需要的参数,目前仅支持查询终端参数里的设备型号(termModel)
     * @param Request $request
     * @return array|string
     */
    public function getTermModel(Request $request)
    {
        $para_type = $request->get('paraType', '1'); //参数类型,01-查询机具信息
        $para_name = $request->get('paraName', '百富'); //参数名称,paraType为01时,该字段表示厂商名称
        $store_id = $request->get('storeId', ''); //门店id

        $storeObj = Store::where('store_id', $store_id)
            ->where('is_close', 0)
            ->where('is_delete', 0)
            ->first();
        if (!$storeObj) {
            return json_encode([
                'status' => 2,
                'message' => '门店不存在或状态异常'
            ]);
        }

        $new_config_id = '1234';
        $source = $storeObj->source; //来源 01 02
        $source_type = $storeObj->source_type;
        if ($source == '01') {
            $new_config_id = '1234';
        } else if ($source == '02') {
            if ($source_type == '1') {
                $new_config_id = '100101';  //河南-中原项目
            } else {
                $new_config_id = '1001';
            }
        }

        $easypay_configs_obj = EasypayConfig::where('config_id', $new_config_id)->first();

        if (!$easypay_configs_obj && isset($easypay_configs_obj->client_code) && !$easypay_configs_obj->client_code && isset($easypay_configs_obj->key) && !$easypay_configs_obj->key) {
            return json_encode([
                'status' => 2,
                'message' => '易生支付未配置'
            ]);
        }

        $client_code = $easypay_configs_obj->client_code;
        $key = $easypay_configs_obj->key;
        try {
            if ($para_type == '1') $para_type = '01';
            $reqData = [
                'version' => '1.0', //版本号
                'clientCode' => $client_code, //机构号,该字段由易生配置
                'messageType' => 'QUERYPARA', //报文类型
                'paraType' => $para_type,
                'paraName' => $para_name
            ];
            $obj = new MerAccessController();
            $result = $obj->execute($reqData, $key); //1-成功 2-失败
            if ($result['status'] == 1) {
                if (isset($result['data']) && isset($result['data']['retCode']) && $result['data']['retCode'] == '0000') {
                    return [
                        'status' => 1,
                        'data' => isset($result['data']['paraInfo']) ? $result['data']['paraInfo'] : '',
                        'message' => $result['data']['retMsg'] ?? $result['message']
                    ];
                } else {
                    return [
                        'status' => 2,
                        'data' => $result['data'] ?? '',
                        'message' => $result['data']['retMsg'] ?? $result['message']
                    ];
                }
            }

            return [
                'status' => 2,
                'data' => $result['data'] ?? '',
                'message' => $result['message']
            ];
        } catch (\Exception $ex) {
            Log::info('易生支付-参数查询termModel-error');
            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
            return [
                'status' => -1,
                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine()
            ];
        }
    }


    /**
     * 生成指定位数的随机数
     * @param int $length
     * @return int|string
     */
    protected function random_number($length = 8)
    {
        list($usec, $sec) = explode(' ', microtime());
        $ms = microtime(true) * 10000; //毫秒
        $mi = sprintf("%.0f", ((float)$usec + (float)$sec) * 100000000); //微秒

        $num = mt_rand(1, 9);

        $length = isset($length) ? $length : 8;
        for ($i = 0; $i < $length - 1; $i++) {
            $num .= mt_rand(0, 9);
        }

        $str = $num . $mi;
        $isExist = EasypayStore::where('opera_trace', $str)->first();
        if (!$isExist) {
            return $str;
        } else {
            $this->random_number($length);
        }
    }


    //通道入库
    public function send_ways_data($data)
    {
        try {
            //开启事务
            $all_pay_ways = DB::table('store_ways_desc')->where('company', $data['company'])
                ->get();
            foreach ($all_pay_ways as $k => $v) {
                $gets = StorePayWay::where('store_id', $data['store_id'])
                    ->where('ways_source', $v->ways_source)
                    ->get();
                $count = count($gets);
                $ways = StorePayWay::where('store_id', $data['store_id'])
                    ->where('ways_type', $v->ways_type)
                    ->first();
                try {
                    DB::beginTransaction();
                    $data = [
                        'store_id' => $data['store_id'],
                        'ways_type' => $v->ways_type,
                        'company' => $v->company,
                        'ways_source' => $v->ways_source,
                        'ways_desc' => $v->ways_desc,
                        'sort' => ($count + 1),
                        'rate' => $data['rate'],
                        'settlement_type' => $v->settlement_type,
                        'status' => $data['status'],
                        'status_desc' => $data['status_desc'],
                    ];
                    if ($ways) {
                        $ways->update(
                            [
                                'rate' => $data['rate'],
                                'status' => $data['status'],
                                'status_desc' => $data['status_desc']
                            ]);
                        $ways->save();
                    } else {
                        StorePayWay::create($data);
                    }
                    DB::commit();
                } catch (\Exception $e) {
                    Log::info('入库通道66');
                    Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
                    DB::rollBack();
                    return [
                        'status' => 2,
                        'message' => '通道入库更新失败11'
                    ];
                }
            }

            return [
                'status' => 1,
                'message' => $data['status_desc']
            ];
        } catch (\Exception $e) {
            return [
                'status' => '-1',
                'message' => '通道入库更新失败3',
            ];
        }
    }


    //易生支付，时间格式
    protected function EasyPayTimeFormat($start_time, $end_time)
    {
        if ($start_time) {
            $start_time = str_replace('-', '.', $start_time);
        } else {
            $start_time = '';
        }

        if ($end_time) {
            if ($end_time == '长期') {
                $end_time = '2999.12.31';
            } else {
                $end_time = str_replace('-', '.', $end_time);
            }
        } else {
            $end_time = '';
        }

        return ["$start_time", "$end_time"];
    }


    //组装图片
    public function images_get($img_url)
    {
        $img_url = explode('/', $img_url);
        $img_url = end($img_url);
        $img = public_path() . '/upload/images/' . $img_url;
//        if ($img) {
//            try {
//                //压缩图片
//                $img_obj = \Intervention\Image\Facades\Image::make($img);
//                $img_obj->resize(800, 700);
//                $img = public_path() . '/upload/s_images/' . $img_url;
//                $img_obj->save($img);
//            } catch (\Exception $exception) {
//                Log::info('快钱组装图片-error');
//                Log::info($exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine());
//            }
//        }

        return $img;
    }


    /**
     * 商户信息变更
     *
     * 场景说明:
     * 1、该接口用于修改商户信息(包括图片信息)
     * 2、该接口只需送需要变更的字段,不需要变更的字段可以不送
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function alterMerchant(Request $request)
    {
        $token = $this->parseToken();

        $actionType = $request->get('type', '1'); //2-查询数据
        $store_id = $request->get('storeId', ''); //门店id

        try {
            $except_token_data = $request->except(['token']);
            $validate = Validator::make($except_token_data, [
                'storeId' => 'required|max:50'
            ], [
                'required' => ':attribute参数为必填项',
                'min' => ':attribute参数长度太短',
                'max' => ':attribute参数长度太长',
                'unique' => ':attribute参数已经被人占用',
                'exists' => ':attribute参数不存在',
                'integer' => ':attribute参数必须是数字',
                'required_if' => ':attribute参数满足条件时必传',
            ], [
                'storeId' => '门店id'
            ]);
            if ($validate->fails()) {
                $this->status = 2;
                $this->message = $validate->getMessageBag()->first();
                return $this->format();
            }

            $Store = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$Store) {
                $this->status = 2;
                $this->message = '门店不存在或状态异常';
                return $this->format();
            }

            //是否已经进件
            $easypay_store = EasypayStore::where('store_id', $store_id)->first();
            if ($easypay_store) {
                if (!$easypay_store->mer_trace) {
                    return json_encode([
                        'status' => 2,
                        'message' => "易生-商户信息变更-缺少'商户唯一标识'"
                    ]);
                }
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '请商户先入网'
                ]);
            }

            $easyPayStoresImagesObj = EasypayStoresImages::where('store_id', $store_id)->first();
            if ($actionType == 2) {
                $this->status = 1;
                $this->message = '返回数据成功';
                return $this->format($easyPayStoresImagesObj);
            }

            $StoreBank = StoreBank::where('store_id', $store_id)->first();
            if (!$StoreBank) {
                return json_encode([
                    'status' => 2,
                    'message' => '没有绑定银行卡,请先绑定银行卡'
                ]);
            }

            $StoreImg = StoreImg::where('store_id', $store_id)->first();
            if (!$StoreImg) {
                return json_encode([
                    'status' => 2,
                    'message' => '没有商户图片资料'
                ]);
            }

            $config_id = '1234';
//            $source = $Store->source; //来源 01 02
//            $source_type = $Store->source_type;
//            if ($source == '01') {
//                $config_id = '1234';
//            } else if ($source == '02') {
//                if ($source_type == '1') {
//                    $config_id = '100101';  //河南-中原项目
//                } else {
//                    $config_id = '1001';
//                }
//            }
            if ($easyPayStoresImagesObj) {
                $config_id = $easyPayStoresImagesObj->new_config_id;
            }

            $easyPayMerAccessObj = new MerAccessController();
            $easyPayConfigObj = new EasyPayConfigController();
            $easypay_config = $easyPayConfigObj->easypay_config($config_id);
            if (!$easypay_config || !$easypay_config->client_code || !$easypay_config->key) {
                return json_encode([
                    'status' => 2,
                    'message' => '易生支付未配置'
                ]);
            }

            $client_code = $easypay_config->client_code; //进件机构号
            $key = $easypay_config->key; //进件密钥

            $easyPayStoresImagesObj = EasypayStoresImages::where('store_id', $store_id)->first();
            if (!$easyPayStoresImagesObj) {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'门店列表-通道管理'中补充资料"
                ]);
            }
            //项目归属ID
            $projectId = '';
            if ($easyPayStoresImagesObj->project_id) {
                $projectId = $easyPayStoresImagesObj->project_id; //项目归属ID
            }
            $branchName = $easyPayStoresImagesObj->branch_name; //合作银行支行
            $branchManager = $easyPayStoresImagesObj->branch_manager; //合作银行客户经理
            $merMark = $easyPayStoresImagesObj->mer_mark; //备注

            $contractNo = $easyPayStoresImagesObj->contract_no; //电子协议编号
            $signatoryName = $easyPayStoresImagesObj->signatory_name; //签署方名称

            if ($easyPayStoresImagesObj->house_number) {
                $houseNumberResId = $easyPayStoresImagesObj->house_number; //09-门牌号
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中上传'门牌号'照片"
                ]);
            }
            if ($easyPayStoresImagesObj->agreement) {
                $agreementResId = $easyPayStoresImagesObj->agreement; //10-协议
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中上传'协议'照片"
                ]);
            }
            if ($easyPayStoresImagesObj->merc_regist_form_a) {
                $merchantRegistrationSheetAResId = $easyPayStoresImagesObj->merc_regist_form_a; //11-商户登记表正面
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中上传'商户登记表正面'照片"
                ]);
            }
            if ($easyPayStoresImagesObj->merc_regist_form_b) {
                $merchantRegistrationSheetBResId = $easyPayStoresImagesObj->merc_regist_form_b; //12-商户登记表反面
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中上传'商户登记表反面'照片"
                ]);
            }
            if ($easyPayStoresImagesObj->mcc_id) {
                $merType = $easyPayStoresImagesObj->mcc_id; //商户类型(MCC),银联定义的MCC编码
                $businScope = $easyPayStoresImagesObj->busin_scope; //主营业务
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中选择'商户类型（MCC）'"
                ]);
            }
            $standardFlag = '0';
            if ($easyPayStoresImagesObj->standard_flag || $easyPayStoresImagesObj->standard_flag === 0 || $easyPayStoresImagesObj->standard_flag === '0') $standardFlag = $easyPayStoresImagesObj->standard_flag; //行业大类 0-标准、1-优惠、2-减免
            if ($easyPayStoresImagesObj->employee_num) {
                $employeeNum = $easyPayStoresImagesObj->employee_num; //公司规模:1：0-50 人；2：50-100 人；3:100 以上
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中选择'公司规模'"
                ]);
            }
            if ($easyPayStoresImagesObj->busin_form) {
                $businForm = $easyPayStoresImagesObj->busin_form; //经营形态：02-普通店、01-连锁店
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中选择'经营形态'"
                ]);
            }
            if ($easyPayStoresImagesObj->busin_beg_time) {
                $businBegtime = $easyPayStoresImagesObj->busin_beg_time; //营业时间：开始时间，格式：HHMM
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中选择'营业时间：开始时间'"
                ]);
            }
            if ($easyPayStoresImagesObj->busin_end_time) {
                $businEndtime = $easyPayStoresImagesObj->busin_end_time; //营业时间：结束时间，格式：HHMM
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中选择'营业时间：结束时间'"
                ]);
            }

            if ($easyPayStoresImagesObj->capital) {
                $capital = $easyPayStoresImagesObj->capital; //注册资本：单位-万元
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中填写'注册资本'"
                ]);
            }
            if ($easyPayStoresImagesObj->mer_area) {
                $merArea = $easyPayStoresImagesObj->mer_area; //商户区域:银联地区码
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中选择'商户区域:银联地区码'"
                ]);
            }
            if ($easyPayStoresImagesObj->d_stlm_type || $easyPayStoresImagesObj->d_stlm_type === 0 || $easyPayStoresImagesObj->d_stlm_type === '0') $DStlmType = $easyPayStoresImagesObj->d_stlm_type; //借记卡扣率方式:1-封顶;0-不封顶
            if ($easyPayStoresImagesObj->d_calc_val || $easyPayStoresImagesObj->d_calc_val === 0 || $easyPayStoresImagesObj->d_calc_val === '0') {
                $DCalcVal = $easyPayStoresImagesObj->d_calc_val; //借记卡扣率
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中选择'借记卡扣率'"
                ]);
            }

            if ($easyPayStoresImagesObj->d_fee_low_limit || $easyPayStoresImagesObj->d_fee_low_limit === 0 || $easyPayStoresImagesObj->d_fee_low_limit === '0') $DFeeLowLimit = $easyPayStoresImagesObj->d_fee_low_limit; //借记卡手续费最低值
            if ($easyPayStoresImagesObj->c_calc_val || $easyPayStoresImagesObj->c_calc_val === 0 || $easyPayStoresImagesObj->c_calc_val === '0') {
                $CCalcVal = $easyPayStoresImagesObj->c_calc_val; //信用卡扣率
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中选择'信用卡扣率'"
                ]);
            }
            if ($easyPayStoresImagesObj->c_fee_low_limit || $easyPayStoresImagesObj->c_fee_low_limit === 0 || $easyPayStoresImagesObj->c_fee_low_limit === '0') $CFeeLowLimit = $easyPayStoresImagesObj->c_fee_low_limit; //信用卡手续费最低值
            $realTimeEntry = '0';
            if ($easyPayStoresImagesObj->real_time_entry || $easyPayStoresImagesObj->real_time_entry === 0 || $easyPayStoresImagesObj->real_time_entry === '0') {
                $realTimeEntry = $easyPayStoresImagesObj->real_time_entry; //实时入账(0-否;1-是)
                $DCalcType = 0;
                if ($easyPayStoresImagesObj->d_calc_type) {
                    $DCalcType = $easyPayStoresImagesObj->d_calc_type; //借记卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
                }
                $CCalcType = 0;
                if ($easyPayStoresImagesObj->c_calc_type) {
                    $CCalcType = $easyPayStoresImagesObj->c_calc_type; //信用卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
                }
            }
            if ($easyPayStoresImagesObj->pub_bank_name) $pubBankName = $easyPayStoresImagesObj->pub_bank_name; //开户行名称(企业对公)
            if ($easyPayStoresImagesObj->pub_bank_code) $pubBankCode = $easyPayStoresImagesObj->pub_bank_code; //开户行行号(企业对公)
            if ($easyPayStoresImagesObj->pub_account) $pubAccount = $easyPayStoresImagesObj->pub_account; //账号(企业对公)
            if ($easyPayStoresImagesObj->pub_acc_name) $pubAccName = $easyPayStoresImagesObj->pub_acc_name; //账户名称(企业对公)
            $twoBars = [];
            $twoBarsOpen = '';
            if ($easyPayStoresImagesObj->two_bars_open) {
                $twoBarsOpen = $easyPayStoresImagesObj->two_bars_open; //是否开启立招(1-开启;2-关闭,默认2)
                if ($twoBarsOpen == '1') {
                    $twoBars = $easyPayStoresImagesObj->two_bars;
                    if (!$twoBars || $twoBars == '[{}]' || $twoBars == '[{"":""}]') {
                        return json_encode([
                            'status' => 2,
                            'message' => "请在'易生进件'中完善'立招功能'"
                        ]);
                    }
                }
            }

            //查找是否有此通道
            $ways1 = StorePayWay::where('store_id', $store_id)
                ->where('ways_type', '21001')
                ->first();
            if ($ways1) {
                $rate = $ways1->rate; //如果门店设置走门店扫码费率

            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '请设置易生通道费率'
                ]);
            }

            //默认失败
            $send_ways_data['rate'] = $rate;
            $send_ways_data['store_id'] = $store_id;
            $send_ways_data['status'] = '3';
            $send_ways_data['company'] = 'easypay';

            //公共必传图片
            if (!$StoreImg->head_sfz_img_a) {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-法人身份证正面-未上传'
                ]);
            }
            $headSfzImgA = $this->images_get($StoreImg->head_sfz_img_a); //01-法人身份证正面
            $headSfzImgARes = $easyPayMerAccessObj->upload_image($headSfzImgA, 01, $client_code, $key); //-1 系统错误 1-成功 2-失败
            if ($headSfzImgARes['status'] == 1) {
                $headSfzImgAResId = $headSfzImgARes['data']['fileId'];
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-法人身份证正面-上传失败'
                ]);
            }
            if (!$StoreImg->head_sfz_img_b) {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-法人身份证反面-未上传'
                ]);
            }
            $headSfzImgB = $this->images_get($StoreImg->head_sfz_img_b); //02-法人身份证反面
            $headSfzImgBRes = $easyPayMerAccessObj->upload_image($headSfzImgB, 02, $client_code, $key);
            if ($headSfzImgBRes['status'] == 1) {
                $headSfzImgBResId = $headSfzImgBRes['data']['fileId'];
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-法人身份证反面-上传失败'
                ]);
            }
            $bankSfzImgAResId = '';
            $bankSfzImgBResId = '';
            if ($Store->head_name != $StoreBank->store_bank_name && ($StoreBank->store_bank_type == '01')) {//01对私人 && 非法人结算
                if (!$StoreImg->bank_sfz_img_a) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-商户联系人身份证正面-未上传'
                    ]);
                }
                $bankSfzImgA = $this->images_get($StoreImg->bank_sfz_img_a); //03-商户联系人身份证正面
                $bankSfzImgARes = $easyPayMerAccessObj->upload_image($bankSfzImgA, 03, $client_code, $key);
                if ($bankSfzImgARes['status'] == 1) {
                    $bankSfzImgAResId = $bankSfzImgARes['data']['fileId'];
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-商户联系人身份证正面-上传失败'
                    ]);
                }
                if (!$StoreImg->bank_sfz_img_b) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-商户联系人身份证反面-未上传'
                    ]);
                }
                $bankSfzImgB = $this->images_get($StoreImg->bank_sfz_img_b); //04-商户联系人身份证反面
                $bankSfzImgBRes = $easyPayMerAccessObj->upload_image($bankSfzImgB, 04, $client_code, $key);
                if ($bankSfzImgBRes['status'] == 1) {
                    $bankSfzImgBResId = $bankSfzImgBRes['data']['fileId'];
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-商户联系人身份证反面-上传失败'
                    ]);
                }
            }

            if (!$StoreImg->store_img_a) {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-收银台照片-未上传'
                ]);
            }
            $storeImgA = $this->images_get($StoreImg->store_img_a); //06-收银台
            $storeImgARes = $easyPayMerAccessObj->upload_image($storeImgA, 06, $client_code, $key);
            if ($storeImgARes['status'] == 1) {
                $storeImgAResId = $storeImgARes['data']['fileId'];
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-收银台-上传失败'
                ]);
            }
            if (!$StoreImg->store_img_c) {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-内部环境照-未上传'
                ]);
            }
            $StoreImgC = $this->images_get($StoreImg->store_img_c); //07-内部环境照
            $StoreImgCRes = $easyPayMerAccessObj->upload_image($StoreImgC, 07, $client_code, $key);
            if ($StoreImgCRes['status'] == 1) {
                $StoreImgCResId = $StoreImgCRes['data']['fileId'];
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-内部环境照-上传失败'
                ]);
            }
            if (!$StoreImg->store_logo_img) {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-营业场所门头照-未上传'
                ]);
            }
            $storeLogoImg = $this->images_get($StoreImg->store_logo_img); //08-营业场所门头照
            $storeLogoImgRes = $easyPayMerAccessObj->upload_image($storeLogoImg, '08', $client_code, $key);
            if ($storeLogoImgRes['status'] == 1) {
                $storeLogoImgResId = $storeLogoImgRes['data']['fileId'];
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-营业场所门头照-上传失败'
                ]);
            }
            if (!$StoreImg->bank_img_a) {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-银行卡-未上传'
                ]);
            }
            $bankImgA = $this->images_get($StoreImg->bank_img_a); //14-银行卡
            $bankImgARes = $easyPayMerAccessObj->upload_image($bankImgA, 14, $client_code, $key);
            if ($bankImgARes['status'] == 1) {
                $bankImgAResId = $bankImgARes['data']['fileId'];
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-银行卡-上传失败'
                ]);
            }

            if ($Store->store_type == '1') { //1-个体工商户
                $mer_mode = '1';
            } elseif ($Store->store_type == '2') { //2-企业
                $mer_mode = '0';
            } else { //3-小微商户
                $mer_mode = '2';
            }

            $pic_info_list = [];
            $storeLicenseImgId = '';
            if ($Store->store_type != '3') { //非个人,企业（0）或者个体户（1）时，需要传：05-营业执照
                if (!$StoreImg->store_license_img) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-营业执照-未上传'
                    ]);
                }
                $storeLicenseImg = $this->images_get($StoreImg->store_license_img); //05-营业执照
                $storeLicenseImgRes = $easyPayMerAccessObj->upload_image($storeLicenseImg, 05, $client_code, $key); //-1 系统错误 1-成功 2-失败
                if ($storeLicenseImgRes['status'] == 1) {
                    $storeLicenseImgId = $storeLicenseImgRes['data']['fileId'];
                    $pic_info_list[] = [
                        'picMode' => '05',
                        'fileId' => $storeLicenseImgId
                    ];
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-营业执照-上传失败'
                    ]);
                }
            }

            //收款银行为对私时，需要传：14-银行卡；15-清算授权书
            $storeIndustryLicenseImgId = '';
            $certOfAuthId = '';
            $bankImgAResId = '';
            if ($StoreBank->store_bank_type == '01') {
                if ($mer_mode != '2') { //非小微商户
                    $certOfAuthId = $easyPayStoresImagesObj->cert_of_auth; //15-清算授权书
                    if ($certOfAuthId) {
                        $pic_info_list[] = [
                            'picMode' => '15',
                            'fileId' => $certOfAuthId
                        ];
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => "请在'易生进件'中上传'清算授权书'照片"
                        ]);
                    }
                }

                if (!$StoreImg->bank_img_a) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-银行卡-未上传'
                    ]);
                }

                $bankImgA = $this->images_get($StoreImg->bank_img_a); //14-银行卡
                $bankImgARes = $easyPayMerAccessObj->upload_image($bankImgA, 14, $client_code, $key);
                if ($bankImgARes['status'] == 1) {
                    $bankImgAResId = $bankImgARes['data']['fileId'];

                    $pic_info_list[] = [
                        'picMode' => '14', //14-银行卡
                        'fileId' => $bankImgAResId
                    ];
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-银行卡-上传失败'
                    ]);
                }

            } else {
                //收款银行为对公时，需要传：13-开户许可证
                if (!$StoreImg->store_industrylicense_img) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-开户许可证-未上传'
                    ]);
                }
                $storeIndustryLicenseImg = $this->images_get($StoreImg->store_industrylicense_img); //13-开户许可证
                $storeIndustryLicenseImgRes = $easyPayMerAccessObj->upload_image($storeIndustryLicenseImg, 13, $client_code, $key); //-1 系统错误 1-成功 2-失败
                if ($storeIndustryLicenseImgRes['status'] == 1) {
                    $storeIndustryLicenseImgId = $storeIndustryLicenseImgRes['data']['fileId'];
                    $pic_info_list[] = [
                        'picMode' => '13',
                        'fileId' => $storeIndustryLicenseImgId
                    ];
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-开户许可证-上传失败'
                    ]);
                }
            }

            $reqData = [
                'version' => '1.0', //版本号
                'clientCode' => $client_code, //机构号
                'messageType' => 'ALTERMER', //报文类型
                'backUrl' => url('api/easypay/merchant_apply_notify_url'), //o,回调地址
                'merTrace' => $easypay_store->mer_trace, //商户唯一标识
                'operaTrace' => $this->random_number(2), //操作流水,20
                'merInfo' => [
                    'merMode' => $mer_mode, //商户类型（0-企业 1-个体户 2-个人）
                    'merName' => $Store->store_name, //注册名称
                    'businName' => $Store->store_short_name, //经营名称 (店名)
                    'merType' => "$merType", //商户类型（MCC），银联定义的 MCC 编码
                    'standardFlag' => "$standardFlag", //行业大类 0-标准、1-优惠、2-减免
                    'merArea' => $merArea, //商户区域：银联地区码
                    'merAddr' => $Store->province_name . $Store->city_name . $Store->area_name . $Store->store_address, //注册地址
                    'businBegtime' => $businBegtime, //营业时间：开始时间，格式：HHMM
                    'businEndtime' => $businEndtime, //营业时间：结束时间，格式：HHMM
                    'employeeNum' => "$employeeNum", //公司规模:1：0-50 人；2：50-100 人；3:100 以上
                    'businForm' => $businForm, //经营形态：02-普通店、01-连锁店
                    'merMark' => $merMark //备注

                ],
                'plusInfo' => [
                    'merLegal' => $Store->head_name, //法定代表人姓名
                    'legalType' => '0', //证件类型：0-居民身份证或临时身份证;1-外国公民护照;2-港澳居民来往大陆通行证或其他有效旅游证件；3-其他类个人身份有效证件；4-单位证件；5-军人或武警身份证件
                    'legalCode' => $Store->head_sfz_no, //法人证件号码
                    'legalValidity' => $this->EasyPayTimeFormat($Store->head_sfz_stime, $Store->head_sfz_time), //证件有效期 格式：["起始日期","截止日期"],JSON 格式字符串,例如：["2011.01.01","2039.01.01"]
                    'legalPhone' => "$Store->people_phone", //手机号
                    'linkMan' => $Store->people, //商户联系人姓名
                    'linkmanType' => '0', //证件类型：0-居民身份证或临时身份证;1-外国公民护照;2-港澳居民来往大陆通行证或其他有效旅游证件；3-其他类个人身份有效证件；4-单位证件；5-军人或武警身份证件
                    'linkmanCode' => ($StoreBank->store_bank_type == '01') ? $StoreBank->bank_sfz_no : $Store->head_sfz_no, //证件号码
                    'linkmanValidity' => ($StoreBank->store_bank_type == '01') ? $this->EasyPayTimeFormat($StoreBank->bank_sfz_stime, $StoreBank->bank_sfz_time) : $this->EasyPayTimeFormat($Store->head_sfz_stime, $Store->head_sfz_time), //证件有效期 格式：["起始日期","截止日期"],JSON 格式字符串
                    'linkmanPhone' => $Store->people_phone, //手机号
                    'contractNo' => $contractNo, //电子协议编号
                    'signatoryName' => $signatoryName,//签署方名称
                    'branchName' => $branchName, //合作银行支行
                    'branchManager' => $branchManager,//合作银行客户经理
                ],
                'licInfo' => [
                    'merLic' => ($Store->store_type == '1' || $Store->store_type == '2') ? $Store->store_license_no : '',  //C，工商注册号/统一社会信用码（营业执照）,merMode 为0,1 时，必传
                    'licValidity' => ($Store->store_type == '1' || $Store->store_type == '2') ? $this->EasyPayTimeFormat($Store->store_license_stime, $Store->store_license_time) : '', //C,营业执照有效期 格式：["起始日期","截止日期"],JSON 格式字符串 ,merMode 为 0,1 时，必传
                    'businScope' => $businScope, //主营业务
                    'capital' => $capital, //注册资本:单位-万元
                    'licAddr' => $Store->province_name . $Store->city_name . $Store->area_name . $Store->store_address, //注册地址
                    'controlerName' => $Store->head_name, //控股股东或实际控制人姓名或名称
                    'controlerLegalCode' => $Store->head_sfz_no, //控股股东或实际控制人证件号码
                    'controlerLegalType' => '0', //控股股东或实际控制人证件种类：0-居民身份证或临时身份证;1-外国公民护照;2-港澳居民来往大陆通行证或其他有效旅游证件；3-其他类个人身份有效证件；4-单位证件；5-军人或武警身份证件
                    'controlerLegalValidity' => $this->EasyPayTimeFormat($Store->head_sfz_stime, $Store->head_sfz_time) //控股股东或实际控制人证件有效期
                ],
                'picInfoList' => [
                    [
                        'picMode' => '01', //图片类型,01-法人身份证正面
                        'fileId' => $headSfzImgAResId
                    ],
                    [
                        'picMode' => '02', //02-法人身份证反面
                        'fileId' => $headSfzImgBResId
                    ],
//                    [
//                        'picMode' => '03', //03-商户联系人身份证正面
//                        'fileId' => $bankSfzImgAResId
//                    ],
//                    [
//                        'picMode' => '04', //04-商户联系人身份证反面
//                        'fileId' => $bankSfzImgBResId
//                    ],
                    [
                        'picMode' => '06', //06-收银台
                        'fileId' => $storeImgAResId
                    ],
                    [
                        'picMode' => '07', //07-内部环境照
                        'fileId' => $StoreImgCResId
                    ],
                    [
                        'picMode' => '08', //08-营业场所门头照
                        'fileId' => $storeLogoImgResId
                    ],
                    [
                        'picMode' => '09', //09-门牌号
                        'fileId' => $houseNumberResId
                    ],
                    [
                        'picMode' => '10', //10-协议
                        'fileId' => $agreementResId
                    ],
                    [
                        'picMode' => '11', //11-商户登记表正面
                        'fileId' => $merchantRegistrationSheetAResId
                    ],
                    [
                        'picMode' => '12', //12-商户登记表反面
                        'fileId' => $merchantRegistrationSheetBResId
                    ],
//                    [
//                        'picMode' => '14', //14-银行卡
//                        'fileId' => $bankImgAResId
//                    ]
                ]
            ];

            if ($projectId) {
                $reqData['merInfo']['projectId'] = $projectId;  //项目编号(易生提供),用于商户分类统计
            }

            if ($realTimeEntry) { //实时入账
                $reqData['funcInfo'][] = [
                    'funcId' => '25', //功能ID
                    'tradeType' => '2', //交易类型：0-全部；1-银行卡；2-条码支付
                    'DCalcType' => "1", //借记卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
                    'DCalcVal' => '0.02', //借记卡扣率
                    'CCalcType' => "1", //信用卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
                    'CCalcVal' => "0.02" //信用卡扣率
                ];
//                $reqData['funcInfo'][] = [
//                    'funcId' => '25', //功能ID
//                    'tradeType' => '0', //交易类型：0-全部；1-银行卡；2-条码支付
//                    'DCalcType' => "$DCalcType", //借记卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
//                    'DCalcVal' => $DCalcVal, //借记卡扣率
//                    'CCalcType' => "$CCalcType", //信用卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
//                    'CCalcVal' => $CCalcVal //信用卡扣率
//                ];
            }
            if ($Store->store_type == '2' && ($StoreBank->store_bank_type == '01')) {
                //当商户类型merMode为企业商户时,如果accInfo的账户类型为对私,则该字段必传，且账户必须是对公账户
                if (!$pubBankName || !$pubBankCode || !$pubAccount || !$pubAccName) {
                    return json_encode([
                        'status' => 2,
                        'message' => "企业对私结算时,请在'易生进件'中补充'第二个对公结算账户信息'"
                    ]);
                }
                $reqData['accInfoBak'] = [
                    'bankName' => $pubBankName, //开户行名称
                    'bankCode' => $pubBankCode, //开户行行号
                    'account' => $pubAccount, //账号
                    'accName' => $pubAccName, //账户名称
                    'accType' => '10' //账户类型：10-对公
                ];
            }
            if ($pic_info_list && $pic_info_list != []) {
                $reqData['picInfoList'] = array_merge($pic_info_list, $reqData['picInfoList']);
            }
            if ($Store->head_name != $StoreBank->store_bank_name && ($StoreBank->store_bank_type == '01')) {//01对私人 && 非法人结算

                $bankSfzImgA_list[] = [
                    'picMode' => '03', //03-商户联系人身份证正面
                    'fileId' => $bankSfzImgAResId
                ];
                $bankSfzImgB_list[] = [
                    'picMode' => '04', //04-商户联系人身份证反面
                    'fileId' => $bankSfzImgBResId
                ];
                $reqData['picInfoList'] = array_merge($bankSfzImgA_list, $reqData['picInfoList']);
                $reqData['picInfoList'] = array_merge($bankSfzImgB_list, $reqData['picInfoList']);

            }
            if ($twoBarsOpen == '1') { //立招
                $reqData['funcInfo'][] = [
                    'funcId' => '13', //
                    'twoBars' => "$twoBars" //二维码信息,JSON 字符串（LIST）
                ];
            }

            //图片id入库
            $updateImageId = [
                'head_sfz_img_a' => $headSfzImgAResId,
                'head_sfz_img_b' => $headSfzImgBResId,
                'link_sfz_img_a' => $bankSfzImgAResId,
                'link_sfz_img_b' => $bankSfzImgBResId,
                'cashier' => $storeImgAResId,
                'inter_env_photo' => $StoreImgCResId,
                'front_door_photo' => $storeLogoImgResId,
                //'bank_card' => $bankImgAResId,
                'house_number' => $houseNumberResId,
                'agreement' => $agreementResId,
                'merc_regist_form_a' => $merchantRegistrationSheetAResId,
                'merc_regist_form_b' => $merchantRegistrationSheetBResId
            ];
            if ($bankImgAResId) $updateImageId['bank_card'] = $bankImgAResId;
            if ($storeIndustryLicenseImgId) $updateImageId['licence'] = $storeIndustryLicenseImgId;
            if ($certOfAuthId) $updateImageId['cert_of_auth'] = $certOfAuthId;
            if ($storeLicenseImgId) $updateImageId['store_license_no'] = $storeLicenseImgId;
            $updateImageIdRes = $easyPayStoresImagesObj->update($updateImageId);
            if (!$updateImageIdRes) {
                Log::info('易生-商户信息变更-图片id-更新失败');
                Log::info($store_id);
            }

            //数组加json_encode($arr ,JSON_UNESCAPED_SLASHES)处理
            foreach ($reqData as &$values) {
                if (is_array($values)) {
                    $values = json_encode($values, 256); //不转义中文为unicode,对应的数字256;不转义斜杠,对应的数字64
                }
            }

            Log::info('易生-商户信息变更-入参：');
            Log::info($reqData);
            $re = $easyPayMerAccessObj->execute($reqData, $key);
            Log::info('易生-商户信息变更-结果：');
            Log::info($re);
            if (isset($re) && !empty($re)) {
                if ($re['data']['retCode'] == '0000') {
                    $status_desc = "审核中";
                    $status = '2';

                    if (isset($re['data']['merTrace'])) {
                        $easypay_store->update([
                            'mer_trace' => $re['data']['merTrace'] //商户唯一标识
                        ]);
                    }
                } else {
                    $status = '3';
                    $status_desc = $re['data']['retMsg'];
                }
            } else {
                $status = '3';
                $status_desc = "三方系统返回错误";
            }

            $data['store_id'] = $store_id;
            $data['rate'] = $rate;
            $data['status'] = $status;
            $data['status_desc'] = $status_desc;
            $data['company'] = 'easypay';
            $return = $this->send_ways_data($data);

            return json_encode($return);
        } catch (\Exception $ex) {
            Log::info('易生商户入网-商户信息变更-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }


    /**
     * 结算账户变更
     *
     * 场景说明:该接口用于变更结算账户信息
     *
     * 接口说明:picInfoList：变更结算账户,需要补齐所有结算账户对应的图片(开户许可证、银行卡、清算授权书)
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function alterPayAcc(Request $request)
    {
        $token = $this->parseToken();
        $config_id = $token->config_id;

        $actionType = $request->get('type', '1'); //2-查询数据
        $store_id = $request->get('storeId', ''); //门店id

        try {
            $except_token_data = $request->except(['token']);
            $validate = Validator::make($except_token_data, [
                'storeId' => 'required|max:50'
            ], [
                'required' => ':attribute参数为必填项',
                'min' => ':attribute参数长度太短',
                'max' => ':attribute参数长度太长',
                'unique' => ':attribute参数已经被人占用',
                'exists' => ':attribute参数不存在',
                'integer' => ':attribute参数必须是数字',
                'required_if' => ':attribute参数满足条件时必传',
            ], [
                'storeId' => '门店id'
            ]);
            if ($validate->fails()) {
                $this->status = 2;
                $this->message = $validate->getMessageBag()->first();
                return $this->format();
            }

            //是否已经进件
            $easypay_store = EasypayStore::where('store_id', $store_id)->first();
            if ($easypay_store) {
                if (!$easypay_store->mer_trace) {
                    return json_encode([
                        'status' => 2,
                        'message' => "易生-商户信息变更-缺少'商户唯一标识'"
                    ]);
                }
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '请商户先入网'
                ]);
            }

            $easyPayStoresImagesObj = EasypayStoresImages::where('store_id', $store_id)->first();
            if ($actionType == 2) {
                $this->status = 1;
                $this->message = '返回数据成功';
                return $this->format($easyPayStoresImagesObj);
            }

            $Store = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$Store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常'
                ]);
            }

            $StoreBank = StoreBank::where('store_id', $store_id)->first();
            if (!$StoreBank) {
                return json_encode([
                    'status' => 2,
                    'message' => '没有绑定银行卡,请先绑定银行卡'
                ]);
            }

            $StoreImg = StoreImg::where('store_id', $store_id)->first();
            if (!$StoreImg) {
                return json_encode([
                    'status' => 2,
                    'message' => '没有商户图片资料'
                ]);
            }
            if ($easyPayStoresImagesObj) {
                $config_id = $easyPayStoresImagesObj->new_config_id;
            }
            $easyPayMerAccessObj = new MerAccessController();
            $easyPayConfigObj = new EasyPayConfigController();
            $easypay_config = $easyPayConfigObj->easypay_config($config_id);
            if (!$easypay_config || !$easypay_config->client_code || !$easypay_config->key) {
                return json_encode([
                    'status' => 2,
                    'message' => '易生支付未配置'
                ]);
            }

            $client_code = $easypay_config->client_code; //进件机构号
            $key = $easypay_config->key; //进件密钥

            $easyPayStoresImagesObj = EasypayStoresImages::where('store_id', $store_id)->first();
            if (!$easyPayStoresImagesObj) {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'门店列表-通道管理'中补充资料"
                ]);
            }

            $contractNo = $easyPayStoresImagesObj->contract_no; //电子协议编号

            if ($easyPayStoresImagesObj->house_number) {
                $houseNumberResId = $easyPayStoresImagesObj->house_number; //09-门牌号
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中上传'门牌号'照片"
                ]);
            }
            if ($easyPayStoresImagesObj->agreement) {
                $agreementResId = $easyPayStoresImagesObj->agreement; //10-协议
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中上传'协议'照片"
                ]);
            }
            if ($easyPayStoresImagesObj->merc_regist_form_a) {
                $merchantRegistrationSheetAResId = $easyPayStoresImagesObj->merc_regist_form_a; //11-商户登记表正面
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中上传'商户登记表正面'照片"
                ]);
            }
            if ($easyPayStoresImagesObj->merc_regist_form_b) {
                $merchantRegistrationSheetBResId = $easyPayStoresImagesObj->merc_regist_form_b; //12-商户登记表反面
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中上传'商户登记表反面'照片"
                ]);
            }

            if ($easyPayStoresImagesObj->bank_name) {
                $bankName = $easyPayStoresImagesObj->bank_name; //开户行名称
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中选择'开户行名称'"
                ]);
            }
            if ($easyPayStoresImagesObj->bank_code) {
                $bankCode = $easyPayStoresImagesObj->bank_code; //开户行行号
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中选择'开户行行号'"
                ]);
            }

            if ($easyPayStoresImagesObj->pub_bank_name) $pubBankName = $easyPayStoresImagesObj->pub_bank_name; //开户行名称(企业对公)
            if ($easyPayStoresImagesObj->pub_bank_code) $pubBankCode = $easyPayStoresImagesObj->pub_bank_code; //开户行行号(企业对公)
            if ($easyPayStoresImagesObj->pub_account) $pubAccount = $easyPayStoresImagesObj->pub_account; //账号(企业对公)
            if ($easyPayStoresImagesObj->pub_acc_name) $pubAccName = $easyPayStoresImagesObj->pub_acc_name; //账户名称(企业对公)
            $twoBars = [];
            if ($easyPayStoresImagesObj->two_bars_open) {
                $twoBarsOpen = $easyPayStoresImagesObj->two_bars_open; //是否开启立招(1-开启;2-关闭,默认2)
                if ($twoBarsOpen == '1') {
                    $twoBars = $easyPayStoresImagesObj->two_bars;
                    if (!$twoBars || $twoBars == '[{}]' || $twoBars == '[{"":""}]') {
                        return json_encode([
                            'status' => 2,
                            'message' => "请在'易生进件'中完善'立招功能'"
                        ]);
                    }
                }
            }

            //查找是否有此通道
            $ways1 = StorePayWay::where('store_id', $store_id)
                ->where('ways_type', '21001')
                ->first();
            if ($ways1) {
                $rate = $ways1->rate; //如果门店设置走门店扫码费率
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '请设置易生通道费率'
                ]);
            }

            //默认失败
            $send_ways_data['rate'] = $rate;
            $send_ways_data['store_id'] = $store_id;
            $send_ways_data['status'] = '3';
            $send_ways_data['company'] = 'easypay';

            //公共必传图片
            if (!$StoreImg->head_sfz_img_a) {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-法人身份证正面-未上传'
                ]);
            }
            $headSfzImgA = $this->images_get($StoreImg->head_sfz_img_a); //01-法人身份证正面
            $headSfzImgARes = $easyPayMerAccessObj->upload_image($headSfzImgA, 01, $client_code, $key); //-1 系统错误 1-成功 2-失败
            if ($headSfzImgARes['status'] == 1) {
                $headSfzImgAResId = $headSfzImgARes['data']['fileId'];
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-法人身份证正面-上传失败'
                ]);
            }
            if (!$StoreImg->head_sfz_img_b) {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-法人身份证反面-未上传'
                ]);
            }
            $headSfzImgB = $this->images_get($StoreImg->head_sfz_img_b); //02-法人身份证反面
            $headSfzImgBRes = $easyPayMerAccessObj->upload_image($headSfzImgB, 02, $client_code, $key);
            if ($headSfzImgBRes['status'] == 1) {
                $headSfzImgBResId = $headSfzImgBRes['data']['fileId'];
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-法人身份证反面-上传失败'
                ]);
            }
            $bankSfzImgAResId = '';
            $bankSfzImgBResId = '';
            if ($Store->head_name != $StoreBank->store_bank_name && ($StoreBank->store_bank_type == '01')) {//01对私人 && 非法人结算
                if (!$StoreImg->bank_sfz_img_a) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-商户联系人身份证正面-未上传'
                    ]);
                }
                $bankSfzImgA = $this->images_get($StoreImg->bank_sfz_img_a); //03-商户联系人身份证正面
                $bankSfzImgARes = $easyPayMerAccessObj->upload_image($bankSfzImgA, 03, $client_code, $key);
                if ($bankSfzImgARes['status'] == 1) {
                    $bankSfzImgAResId = $bankSfzImgARes['data']['fileId'];
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-商户联系人身份证正面-上传失败'
                    ]);
                }
                if (!$StoreImg->bank_sfz_img_b) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-商户联系人身份证反面-未上传'
                    ]);
                }
                $bankSfzImgB = $this->images_get($StoreImg->bank_sfz_img_b); //04-商户联系人身份证反面
                $bankSfzImgBRes = $easyPayMerAccessObj->upload_image($bankSfzImgB, 04, $client_code, $key);
                if ($bankSfzImgBRes['status'] == 1) {
                    $bankSfzImgBResId = $bankSfzImgBRes['data']['fileId'];
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-商户联系人身份证反面-上传失败'
                    ]);
                }
            }

            if (!$StoreImg->store_img_a) {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-收银台照片-未上传'
                ]);
            }
            $storeImgA = $this->images_get($StoreImg->store_img_a); //06-收银台
            $storeImgARes = $easyPayMerAccessObj->upload_image($storeImgA, 06, $client_code, $key);
            if ($storeImgARes['status'] == 1) {
                $storeImgAResId = $storeImgARes['data']['fileId'];
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-收银台-上传失败'
                ]);
            }
            if (!$StoreImg->store_img_c) {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-内部环境照-未上传'
                ]);
            }
            $StoreImgC = $this->images_get($StoreImg->store_img_c); //07-内部环境照
            $StoreImgCRes = $easyPayMerAccessObj->upload_image($StoreImgC, 07, $client_code, $key);
            if ($StoreImgCRes['status'] == 1) {
                $StoreImgCResId = $StoreImgCRes['data']['fileId'];
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-内部环境照-上传失败'
                ]);
            }
            if (!$StoreImg->store_logo_img) {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-营业场所门头照-未上传'
                ]);
            }
            $storeLogoImg = $this->images_get($StoreImg->store_logo_img); //08-营业场所门头照
            $storeLogoImgRes = $easyPayMerAccessObj->upload_image($storeLogoImg, '08', $client_code, $key);
            if ($storeLogoImgRes['status'] == 1) {
                $storeLogoImgResId = $storeLogoImgRes['data']['fileId'];
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-营业场所门头照-上传失败'
                ]);
            }
            if (!$StoreImg->bank_img_a) {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-银行卡-未上传'
                ]);
            }
            $bankImgA = $this->images_get($StoreImg->bank_img_a); //14-银行卡
            $bankImgARes = $easyPayMerAccessObj->upload_image($bankImgA, 14, $client_code, $key);
            if ($bankImgARes['status'] == 1) {
                $bankImgAResId = $bankImgARes['data']['fileId'];
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '易生进件-银行卡-上传失败'
                ]);
            }

            if ($Store->store_type == '1') { //1-个体工商户
                $mer_mode = '1';
            } elseif ($Store->store_type == '2') { //2-企业
                $mer_mode = '0';
            } else { //3-小微商户
                $mer_mode = '2';
            }

            $pic_info_list = [];
            $storeLicenseImgId = '';
            if ($Store->store_type != '3') { //非个人,企业（0）或者个体户（1）时，需要传：05-营业执照
                if (!$StoreImg->store_license_img) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-营业执照-未上传'
                    ]);
                }
                $storeLicenseImg = $this->images_get($StoreImg->store_license_img); //05-营业执照
                $storeLicenseImgRes = $easyPayMerAccessObj->upload_image($storeLicenseImg, 05, $client_code, $key); //-1 系统错误 1-成功 2-失败
                if ($storeLicenseImgRes['status'] == 1) {
                    $storeLicenseImgId = $storeLicenseImgRes['data']['fileId'];
                    $pic_info_list[] = [
                        'picMode' => '05',
                        'fileId' => $storeLicenseImgId
                    ];
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-营业执照-上传失败'
                    ]);
                }
            }

            //收款银行为对私时，需要传：14-银行卡；15-清算授权书
            $storeIndustryLicenseImgId = '';
            $certOfAuthId = '';
            if ($StoreBank->store_bank_type == '01') {
                if ($contractNo == null || $contractNo == "") { //进件接口中上送协议id或协议图片
                    $certOfAuthId = $easyPayStoresImagesObj->cert_of_auth; //15-清算授权书
                    if ($certOfAuthId) {
                        $pic_info_list[] = [
                            'picMode' => '15',
                            'fileId' => $certOfAuthId
                        ];
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => "请在'易生进件'中上传'清算授权书'照片"
                        ]);
                    }
                }

            } else {
                //收款银行为对公时，需要传：13-开户许可证；14-银行卡
                if (!$StoreImg->store_industrylicense_img) {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-开户许可证-未上传'
                    ]);
                }
                $storeIndustryLicenseImg = $this->images_get($StoreImg->store_industrylicense_img); //13-开户许可证
                $storeIndustryLicenseImgRes = $easyPayMerAccessObj->upload_image($storeIndustryLicenseImg, 13, $client_code, $key); //-1 系统错误 1-成功 2-失败
                if ($storeIndustryLicenseImgRes['status'] == 1) {
                    $storeIndustryLicenseImgId = $storeIndustryLicenseImgRes['data']['fileId'];
                    $pic_info_list[] = [
                        'picMode' => '13',
                        'fileId' => $storeIndustryLicenseImgId
                    ];
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '易生进件-开户许可证-上传失败'
                    ]);
                }
            }

            $reqData = [
                'version' => '1.0', //版本号
                'clientCode' => $client_code, //机构号
                'messageType' => 'ALTERPAYACC', //报文类型
                'backUrl' => url('api/easypay/merchant_apply_notify_url'), //o,回调地址
                'merTrace' => $easypay_store->mer_trace, //商户唯一标识
                'operaTrace' => $this->random_number(2), //操作流水,20
                'accInfo' => [
                    'bankName' => $bankName, //开户行名称
                    'bankCode' => $bankCode, //开户行行号
                    'account' => $StoreBank->store_bank_no, //账号
                    'accName' => $StoreBank->store_bank_name, //账户名称
                    'accType' => ($StoreBank->store_bank_type == '02') ? '10' : '00', //账户类型：00-个人；10-对公
                    'legalType' => ($StoreBank->store_bank_type == '01') ? '0' : '', //C,证件类型：0-居民身份证或临时身份证;1-外国公民护照;2-港澳居民来往大陆通行证或其他有效旅游证件；3-其他类个人身份有效证件；4-单位证件；5-军人或武警身份证件 accType 为个人时必填
                    'legalCode' => ($StoreBank->store_bank_type == '01') ? $StoreBank->bank_sfz_no : '', //C,证件号 accType 为个人时必填
                    'accPhone' => ($StoreBank->store_bank_type == '01') ? ($StoreBank->reserved_mobile ? $StoreBank->reserved_mobile : ($Store->people_phone ?? '')) : '', //C,对私手机号accType为个人时必填
                ],
                'picInfoList' => [
                    [
                        'picMode' => '01', //图片类型,01-法人身份证正面
                        'fileId' => $headSfzImgAResId
                    ],
                    [
                        'picMode' => '02', //02-法人身份证反面
                        'fileId' => $headSfzImgBResId
                    ],
//                    [
//                        'picMode' => '03', //03-商户联系人身份证正面
//                        'fileId' => $bankSfzImgAResId
//                    ],
//                    [
//                        'picMode' => '04', //04-商户联系人身份证反面
//                        'fileId' => $bankSfzImgBResId
//                    ],
                    [
                        'picMode' => '06', //06-收银台
                        'fileId' => $storeImgAResId
                    ],
                    [
                        'picMode' => '07', //07-内部环境照
                        'fileId' => $StoreImgCResId
                    ],
                    [
                        'picMode' => '08', //08-营业场所门头照
                        'fileId' => $storeLogoImgResId
                    ],
                    [
                        'picMode' => '09', //09-门牌号
                        'fileId' => $houseNumberResId
                    ],
                    [
                        'picMode' => '10', //10-协议
                        'fileId' => $agreementResId
                    ],
                    [
                        'picMode' => '11', //11-商户登记表正面
                        'fileId' => $merchantRegistrationSheetAResId
                    ],
                    [
                        'picMode' => '12', //12-商户登记表反面
                        'fileId' => $merchantRegistrationSheetBResId
                    ],
                    [
                        'picMode' => '14', //14-银行卡
                        'fileId' => $bankImgAResId
                    ],
                ]
            ];
            if ($Store->store_type == '2' && ($StoreBank->store_bank_type == '01')) {
                //当商户类型merMode为企业商户时,如果accInfo的账户类型为对私,则该字段必传，且账户必须是对公账户
                if (!$pubBankName || !$pubBankCode || !$pubAccount || !$pubAccName) {
                    return json_encode([
                        'status' => 2,
                        'message' => "企业对私结算时,请在'易生进件'中补充'第二个对公结算账户信息'"
                    ]);
                }
                $reqData['accInfoBak'] = [
                    'bankName' => $pubBankName, //开户行名称
                    'bankCode' => $pubBankCode, //开户行行号
                    'account' => $pubAccount, //账号
                    'accName' => $pubAccName, //账户名称
                    'accType' => '10' //账户类型：10-对公
                ];
            }
            if ($pic_info_list && $pic_info_list != []) {
                $reqData['picInfoList'] = array_merge($pic_info_list, $reqData['picInfoList']);
            }
            if ($Store->head_name != $StoreBank->store_bank_name && ($StoreBank->store_bank_type == '01')) {//01对私人 && 非法人结算

                $bankSfzImgA_list[] = [
                    'picMode' => '03', //03-商户联系人身份证正面
                    'fileId' => $bankSfzImgAResId
                ];
                $bankSfzImgB_list[] = [
                    'picMode' => '04', //04-商户联系人身份证反面
                    'fileId' => $bankSfzImgBResId
                ];
                $reqData['picInfoList'] = array_merge($bankSfzImgA_list, $reqData['picInfoList']);
                $reqData['picInfoList'] = array_merge($bankSfzImgB_list, $reqData['picInfoList']);

            }

            //图片id入库
            $updateImageId = [
                'head_sfz_img_a' => $headSfzImgAResId,
                'head_sfz_img_b' => $headSfzImgBResId,
                'link_sfz_img_a' => $bankSfzImgAResId,
                'link_sfz_img_b' => $bankSfzImgBResId,
                'cashier' => $storeImgAResId,
                'inter_env_photo' => $StoreImgCResId,
                'front_door_photo' => $storeLogoImgResId,
                'bank_card' => $bankImgAResId,
                'house_number' => $houseNumberResId,
                'agreement' => $agreementResId,
                'merc_regist_form_a' => $merchantRegistrationSheetAResId,
                'merc_regist_form_b' => $merchantRegistrationSheetBResId,
            ];
            if ($storeIndustryLicenseImgId) $updateImageId['licence'] = $storeIndustryLicenseImgId;
            if ($certOfAuthId) $updateImageId['cert_of_auth'] = $certOfAuthId;
            if ($storeLicenseImgId) $updateImageId['store_license_no'] = $storeLicenseImgId;
            $updateImageIdRes = $easyPayStoresImagesObj->update($updateImageId);
            if (!$updateImageIdRes) {
                Log::info('易生-结算账户变更-图片id-更新失败');
                Log::info($store_id);
            }

            //数组加json_encode($arr ,JSON_UNESCAPED_SLASHES)处理
            foreach ($reqData as &$values) {
                if (is_array($values)) {
                    $values = json_encode($values, 256); //不转义中文为unicode,对应的数字256;不转义斜杠,对应的数字64
                }
            }

            Log::info('易生-结算账户变更-入参：');
            Log::info($reqData);
            $re = $easyPayMerAccessObj->execute($reqData, $key);
            Log::info('易生-结算账户变更-结果：');
            Log::info($re);

            if (isset($re) && !empty($re)) {
                if ($re['data']['retCode'] == '0000') {
                    $status_desc = "审核中";
                    $status = '2';

                    if (isset($re['data']['merTrace'])) {
                        $easypay_store->update([
                            'mer_trace' => $re['data']['merTrace'] //商户唯一标识
                        ]);
                    }
                } else {
                    $status = '3';
                    $status_desc = $re['data']['retMsg'];
                }
            } else {
                $status = '3';
                $status_desc = "三方系统返回错误";
            }

            $data['store_id'] = $store_id;
            $data['rate'] = $rate;
            $data['status'] = $status;
            $data['status_desc'] = $status_desc;
            $data['company'] = 'easypay';
            $return = $this->send_ways_data($data);

            return json_encode($return);
        } catch (\Exception $ex) {
            Log::info('易生商户入网-结算账户变更-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    /**
     * 结算账户变更(App)
     * ALTERPAYACC
     */
    public function alterPayAccApp(Request $request)
    {
        try {
            $token = $this->parseToken();
            $config_id = $token->config_id;
            $store_id = $request->get('storeId', ''); //门店id

            $Store = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$Store) {
                return json_encode([
                    'status' => -1,
                    'message' => '门店不存在或状态异常'
                ]);
            }

            $StoreBank = StoreBank::where('store_id', $store_id)->first();
            if (!$StoreBank) {
                return json_encode([
                    'status' => -1,
                    'message' => '没有绑定银行卡,请先绑定银行卡'
                ]);
            }

            $StoreImg = StoreImg::where('store_id', $store_id)->first();
            if (!$StoreImg) {
                return json_encode([
                    'status' => -1,
                    'message' => '没有商户图片资料'
                ]);
            }

            $easyPayStoresImagesObj = EasypayStoresImages::where('store_id', $store_id)->first();

            //是否已经进件
            $easypay_store = EasypayStore::where('store_id', $store_id)->first();
            if ($easypay_store) {
                if (!$easypay_store->mer_trace) {
                    return json_encode([
                        'status' => 2,
                        'message' => "易生-商户信息变更-缺少'商户唯一标识'"
                    ]);
                }
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '请商户先入网'
                ]);
            }

            if ($easyPayStoresImagesObj) {
                $config_id = $easyPayStoresImagesObj->new_config_id;
            }
            $easyPayMerAccessObj = new MerAccessController();

            $easyPayConfigObj = new EasyPayConfigController();
            $easypay_config = $easyPayConfigObj->easypay_config($config_id);
            if (!$easypay_config || !$easypay_config->client_code || !$easypay_config->key) {
                return json_encode([
                    'status' => 2,
                    'message' => '易生支付未配置'
                ]);
            }

            $client_code = $easypay_config->client_code; //进件机构号
            $key = $easypay_config->key; //进件密钥

            $bankType = $request->get('bankType', ''); // 01 法人对公 02对公 03非法人对私
            $store_bank_type = $request->get('store_bank_type', ''); //01 对私 02 对公

            $bank_name = $request->get('sub_bank_name', ''); //开户行名称支行
            $bank_code = $request->get('bank_no', ''); //开户行行号
            $pubBankName = $request->get('sub_bank_name', ''); //第二个对公结算账户信息,开户行名称 企业对私结算时,必传
            $pubBankCode = $request->get('bank_no', ''); //第二个对公结算账户信息,开户行行号 企业对私结算时,必传
            $pubAccount = $request->get('store_bank_no', ''); //第二个对公结算账户信息,账号 企业对私结算时,必传
            $pubAccName = $request->get('store_bank_name', ''); //第二个对公结算账户信息,账户名称 企业对私结算时,必传

            //银行卡信息
            $store_banks = [
                'store_id' => $store_id,
                'store_bank_type' => $store_bank_type,
                'store_bank_name' => $request->get('store_bank_name', ''),
                'store_bank_no' => $request->get('store_bank_no', ''),
                'bank_no' => $request->get('bank_no', ''),
                'sub_bank_name' => $request->get('sub_bank_name', ''),
                'bank_name' => $request->get('bank_name', ''),    //银行名称
            ];
            //图片信息
            $store_imgs = [
                'store_id' => $store_id,
            ];

            $updateData = [
                'store_id' => $store_id,
                'bank_name' => $bank_name,
                'bank_code' => $bank_code,
                'pub_bank_name' => $pubBankName,
                'pub_bank_code' => $pubBankCode,
                'pub_account' => $pubAccount,
                'pub_acc_name' => $pubAccName,
            ];


            if ($bankType == '01') {// 01 法人对私
                $bank_img_a = $request->get('bank_img_a', '');

                $bankImgARes = $easyPayMerAccessObj->upload_image($bank_img_a, 14, $client_code, $key);
                if ($bankImgARes['status'] == 1) {
                    $bankImgAResId = $bankImgARes['data']['fileId'];
                } else {
                    return json_encode([
                        'status' => -1,
                        'message' => '易生进件-银行卡-上传失败'
                    ]);
                }

                $updateData['bank_card'] = $bankImgAResId;

                $store_imgs['bank_img_a'] = $bank_img_a;
                $store_imgs['bank_img_b'] = $bank_img_a;

            } else if ($bankType == '02') {// 02对公
                $store_industrylicense_img = $request->get('store_industrylicense_img', ''); //开户许可证图片地址 企业对私结算时,必传

                $storeIndustryLicenseImgRes = $easyPayMerAccessObj->upload_image($store_industrylicense_img, 13, $client_code, $key); //-1 系统错误 1-成功 2-失败
                if ($storeIndustryLicenseImgRes['status'] == 1) {
                    $storeIndustryLicenseImgId = $storeIndustryLicenseImgRes['data']['fileId'];
                } else {
                    return json_encode([
                        'status' => -1,
                        'message' => '易生进件-开户许可证-上传失败'
                    ]);
                }

                $updateData['licence'] = $storeIndustryLicenseImgId;

                $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;

            } else if ($bankType == '03') {// 03非法人对私
                $bank_img_a = $request->get('bank_img_a', '');
                $hand_idcard_url = $request->get('bank_sc_img', ''); //手持身份证照片
                $hand_idcard_front_url = $request->get('bank_sfz_img_a', ''); //收款人身份证正面
                $hand_idcard_back_url = $request->get('bank_sfz_img_b', ''); //收款人身份证反面
                $cert_of_auth_url = $request->get('certOfAuth', ''); //清算授权书图片地址(非必填)

                $bankImgARes = $easyPayMerAccessObj->upload_image($bank_img_a, 14, $client_code, $key);
                if ($bankImgARes['status'] == 1) {
                    $bankImgAResId = $bankImgARes['data']['fileId'];
                } else {
                    return json_encode([
                        'status' => -1,
                        'message' => '易生进件-银行卡-上传失败'
                    ]);
                }

                if ($hand_idcard_url) {//17-手持身份证照片（选填）
                    $res = $this->upload_image($hand_idcard_url, '17', $client_code, $key);
                    if ($res['status'] == 1) {
                        $hand_idcard = $res['data']['fileId'];

                        $updateData['hand_idcard_url'] = $hand_idcard_url;
                        $updateData['hand_idcard'] = $hand_idcard;
                    }
                }
                if ($hand_idcard_front_url) {//18-收款人身份证正面（选填）
                    $res = $this->upload_image($hand_idcard_front_url, '18', $client_code, $key);
                    if ($res['status'] == 1) {
                        $hand_idcard_front = $res['data']['fileId'];

                        $updateData['hand_idcard_front_url'] = $hand_idcard_front_url;
                        $updateData['hand_idcard_front'] = $hand_idcard_front;
                    }
                }
                if ($cert_of_auth_url) {//15-清算授权书
                    $res = $this->upload_image($cert_of_auth_url, '15', $client_code, $key);
                    if ($res['status'] == 1) {
                        $cert_of_auth = $res['data']['fileId'];

                        $updateData['cert_of_auth'] = $cert_of_auth;
                    }
                }

                $updateData['bank_card'] = $bankImgAResId;


                $store_imgs['bank_img_a'] = $bank_img_a;
                $store_imgs['bank_img_b'] = $bank_img_a;
                $store_imgs['bank_sc_img'] = $hand_idcard_url;
                $store_imgs['bank_sfz_img_a'] = $hand_idcard_front_url;
                $store_imgs['bank_sfz_img_b'] = $hand_idcard_back_url;

            }

            //开启事务
            DB::beginTransaction();

            if ($StoreBank) {
                $StoreBank->update(array_filter($store_banks));
                $StoreBank->save();
            }
            if ($StoreImg) {
                $StoreImg->update(array_filter($store_imgs));
                $StoreImg->save();
            }

            if ($easyPayStoresImagesObj) {

                $result = $easyPayStoresImagesObj->update($updateData);
                if (!$result) {
                    DB::rollBack();
                    return [
                        'status' => -1,
                        'message' => '更新失败'
                    ];
                }
            }

            DB::commit();

            $headSfzImgAResId = $easyPayStoresImagesObj->head_sfz_img_a; //01-法人身份证正面
            $headSfzImgBResId = $easyPayStoresImagesObj->head_sfz_img_b; //02-法人身份证反面
            $storeImgAResId = $easyPayStoresImagesObj->cashier;//06-收银台
            $StoreImgCResId = $easyPayStoresImagesObj->inter_env_photo;//07-内部环境照
            $storeLogoImgResId = $easyPayStoresImagesObj->front_door_photo;//08-营业场所门头照
            $houseNumberResId = $easyPayStoresImagesObj->house_number; //09-门牌号
            $agreementResId = $easyPayStoresImagesObj->agreement; //10-协议
            $bankImgAResId = $easyPayStoresImagesObj->bank_card;//14-银行卡

            $contractNo = $easyPayStoresImagesObj->contract_no; //电子协议编号

            $bankName = $easyPayStoresImagesObj->bank_name; //开户行名称
            $bankCode = $easyPayStoresImagesObj->bank_code; //开户行行号
            $pubBankName = $easyPayStoresImagesObj->pub_bank_name; //开户行名称(企业对公)
            $pubBankCode = $easyPayStoresImagesObj->pub_bank_code; //开户行行号(企业对公)
            $pubAccount = $easyPayStoresImagesObj->pub_account; //账号(企业对公)
            $pubAccName = $easyPayStoresImagesObj->pub_acc_name; //账户名称(企业对公)

            $pic_info_list = [];
            if ($Store->store_type != '3') { //非个人,企业（0）或者个体户（1）时，需要传：05-营业执照
                $storeLicenseImgId = $easyPayStoresImagesObj->store_license_no;//05-营业执照
                $pic_info_list[] = [
                    'picMode' => '05',
                    'fileId' => $storeLicenseImgId
                ];
            }

            //收款银行为对私时，需要传：14-银行卡；15-清算授权书(电子协议替换)
            if ($StoreBank->store_bank_type == '01') {
                if ($contractNo == null || $contractNo == "") { //进件接口中上送协议id或协议图片
                    $certOfAuthId = $easyPayStoresImagesObj->cert_of_auth; //15-清算授权书
                    $pic_info_list[] = [
                        'picMode' => '15',
                        'fileId' => $certOfAuthId
                    ];
                }
            } else {
                //收款银行为对公时，需要传：13-开户许可证；14-银行卡
                $storeIndustryLicenseImgId = $easyPayStoresImagesObj->licence;
                $pic_info_list[] = [
                    'picMode' => '13',
                    'fileId' => $storeIndustryLicenseImgId
                ];
            }

            $reqData = [
                'version' => '1.0', //版本号
                'clientCode' => $client_code, //机构号
                'messageType' => 'ALTERPAYACC', //报文类型
                'backUrl' => url('api/easypay/merchant_apply_notify_url'), //o,回调地址
                'merTrace' => $easypay_store->mer_trace, //商户唯一标识
                'operaTrace' => $this->random_number(2), //操作流水,20
                'accInfo' => [
                    'bankName' => $bankName, //开户行名称
                    'bankCode' => $bankCode, //开户行行号
                    'account' => $StoreBank->store_bank_no, //账号
                    'accName' => $StoreBank->store_bank_name, //账户名称
                    'accType' => ($StoreBank->store_bank_type == '02') ? '10' : '00', //账户类型：00-个人；10-对公
                    'legalType' => ($StoreBank->store_bank_type == '01') ? '0' : '', //C,证件类型：0-居民身份证或临时身份证;1-外国公民护照;2-港澳居民来往大陆通行证或其他有效旅游证件；3-其他类个人身份有效证件；4-单位证件；5-军人或武警身份证件 accType 为个人时必填
                    'legalCode' => ($StoreBank->store_bank_type == '01') ? $StoreBank->bank_sfz_no : '', //C,证件号 accType 为个人时必填
                    'accPhone' => ($StoreBank->store_bank_type == '01') ? ($StoreBank->reserved_mobile ? $StoreBank->reserved_mobile : ($Store->people_phone ?? '')) : '', //C,对私手机号accType为个人时必填
                ],
                'picInfoList' => [
                    [
                        'picMode' => '01', //图片类型,01-法人身份证正面
                        'fileId' => $headSfzImgAResId
                    ],
                    [
                        'picMode' => '02', //02-法人身份证反面
                        'fileId' => $headSfzImgBResId
                    ],
                    [
                        'picMode' => '06', //06-收银台
                        'fileId' => $storeImgAResId
                    ],
                    [
                        'picMode' => '07', //07-内部环境照
                        'fileId' => $StoreImgCResId
                    ],
                    [
                        'picMode' => '08', //08-营业场所门头照
                        'fileId' => $storeLogoImgResId
                    ],
                    [
                        'picMode' => '09', //09-门牌号
                        'fileId' => $houseNumberResId
                    ],
                    [
                        'picMode' => '10', //10-协议
                        'fileId' => $agreementResId
                    ],
                    [
                        'picMode' => '14', //14-银行卡
                        'fileId' => $bankImgAResId
                    ],
                ]
            ];
            if ($Store->store_type == '2' && ($StoreBank->store_bank_type == '01')) {
                //当商户类型merMode为企业商户时,如果accInfo的账户类型为对私,则该字段必传，且账户必须是对公账户
                if (!$pubBankName || !$pubBankCode || !$pubAccount || !$pubAccName) {
                    return json_encode([
                        'status' => 2,
                        'message' => "企业对私结算时,请在'易生进件'中补充'第二个对公结算账户信息'"
                    ]);
                }
                $reqData['accInfoBak'] = [
                    'bankName' => $pubBankName, //开户行名称
                    'bankCode' => $pubBankCode, //开户行行号
                    'account' => $pubAccount, //账号
                    'accName' => $pubAccName, //账户名称
                    'accType' => '10' //账户类型：10-对公
                ];
            }
            if ($pic_info_list && $pic_info_list != []) {
                $reqData['picInfoList'] = array_merge($pic_info_list, $reqData['picInfoList']);
            }

            //数组加json_encode($arr ,JSON_UNESCAPED_SLASHES)处理
            foreach ($reqData as &$values) {
                if (is_array($values)) {
                    $values = json_encode($values, 256); //不转义中文为unicode,对应的数字256;不转义斜杠,对应的数字64
                }
            }

            Log::info('易生-结算账户变更app-入参：');
            Log::info($reqData);
            $re = $easyPayMerAccessObj->execute($reqData, $key);
            Log::info('易生-结算账户变更app-结果：');
            Log::info($re);

            if (isset($re) && !empty($re)) {
                if ($re['data']['retCode'] == '0000') {
                    $status_desc = "审核中";
                    $status = '2';

                    if (isset($re['data']['merTrace'])) {
                        $easypay_store->update([
                            'mer_trace' => $re['data']['merTrace'] //商户唯一标识
                        ]);
                    }
                } else {
                    $status = '3';
                    $status_desc = $re['data']['retMsg'];
                }
            } else {
                $status = '3';
                $status_desc = "三方系统返回错误";
            }

            //查找是否有此通道
            $ways1 = StorePayWay::where('store_id', $store_id)
                ->where('ways_type', '21001')
                ->first();

            $rate = $ways1->rate;

            $data['store_id'] = $store_id;
            $data['rate'] = $rate;
            $data['status'] = $status;
            $data['status_desc'] = $status_desc;
            $data['company'] = 'easypay';
            $return = $this->send_ways_data($data);

            return json_encode($return);
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::info('易生商户入网-结算账户变更app-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    /**
     * 商户功能变更
     *
     * 场景说明: 该接口用于变更商户功能信息
     * 说明: 当新开通银行卡收单(funcId=23)时,同时需要上送终端信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function alterFunc(Request $request)
    {
        $token = $this->parseToken();
        $config_id = $token->config_id;

        $actionType = $request->get('type', '1'); //2-查询数据
        $store_id = $request->get('storeId', ''); //门店id

        try {
            $except_token_data = $request->except(['token']);
            $validate = Validator::make($except_token_data, [
                'storeId' => 'required|max:50'
            ], [
                'required' => ':attribute参数为必填项',
                'min' => ':attribute参数长度太短',
                'max' => ':attribute参数长度太长',
                'unique' => ':attribute参数已经被人占用',
                'exists' => ':attribute参数不存在',
                'integer' => ':attribute参数必须是数字',
                'required_if' => ':attribute参数满足条件时必传',
            ], [
                'storeId' => '门店id'
            ]);
            if ($validate->fails()) {
                $this->status = 2;
                $this->message = $validate->getMessageBag()->first();
                return $this->format();
            }

            //是否已经进件
            $easypay_store = EasypayStore::where('store_id', $store_id)->first();
            if ($easypay_store) {
                if (!$easypay_store->mer_trace) {
                    return json_encode([
                        'status' => 2,
                        'message' => "易生-商户信息变更-缺少'商户唯一标识'"
                    ]);
                }
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '请商户先入网'
                ]);
            }

            $easyPayStoresImagesObj = EasypayStoresImages::where('store_id', $store_id)->first();
            if ($actionType == 2) {
                $this->status = 1;
                $this->message = '返回数据成功';
                return $this->format($easyPayStoresImagesObj);
            }

            $Store = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$Store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常'
                ]);
            }

            $StoreBank = StoreBank::where('store_id', $store_id)->first();
            if (!$StoreBank) {
                return json_encode([
                    'status' => 2,
                    'message' => '没有绑定银行卡,请先绑定银行卡'
                ]);
            }

            $StoreImg = StoreImg::where('store_id', $store_id)->first();
            if (!$StoreImg) {
                return json_encode([
                    'status' => 2,
                    'message' => '没有商户图片资料'
                ]);
            }
            if ($easyPayStoresImagesObj) {
                $config_id = $easyPayStoresImagesObj->new_config_id;
            }
            $easyPayMerAccessObj = new MerAccessController();
            $easyPayConfigObj = new EasyPayConfigController();
            $easypay_config = $easyPayConfigObj->easypay_config($config_id);
            if (!$easypay_config || !$easypay_config->client_code || !$easypay_config->key) {
                return json_encode([
                    'status' => 2,
                    'message' => '易生支付未配置'
                ]);
            }

            $client_code = $easypay_config->client_code; //进件机构号
            $key = $easypay_config->key; //进件密钥

            $easyPayStoresImagesObj = EasypayStoresImages::where('store_id', $store_id)->first();
            if (!$easyPayStoresImagesObj) {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'门店列表-通道管理'中补充资料"
                ]);
            }


            $DStlmType = '0';
            if ($easyPayStoresImagesObj->d_stlm_type) $DStlmType = $easyPayStoresImagesObj->d_stlm_type; //借记卡扣率方式:1-封顶;0-不封顶
            if ($easyPayStoresImagesObj->d_calc_val || $easyPayStoresImagesObj->d_calc_val === 0 || $easyPayStoresImagesObj->d_calc_val === '0') {
                $DCalcVal = $easyPayStoresImagesObj->d_calc_val; //借记卡扣率
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中选择'借记卡扣率'"
                ]);
            }
//            if ($easyPayStoresImagesObj->un_d_calc_val || $easyPayStoresImagesObj->un_d_calc_val===0 || $easyPayStoresImagesObj->un_d_calc_val==='0') {
//                $UnDCalcVal = $easyPayStoresImagesObj->un_d_calc_val; //银联二维码借记卡扣率
//            } else {
//                return json_encode([
//                    'status' => 2,
//                    'message' => "请在'易生进件'中选择'银联二维码借记卡扣率'"
//                ]);
//            }
            if ($easyPayStoresImagesObj->d_stlm_max_amt) {
                $DStlmMaxAmt = $easyPayStoresImagesObj->d_stlm_max_amt; //借记卡封顶金额
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中选择'借记卡封顶金额'"
                ]);
            }
            $DFeeLowLimit = '0';
            if ($easyPayStoresImagesObj->d_fee_low_limit) $DFeeLowLimit = $easyPayStoresImagesObj->d_fee_low_limit; //借记卡手续费最低值
            if ($easyPayStoresImagesObj->c_calc_val || $easyPayStoresImagesObj->c_calc_val === 0 || $easyPayStoresImagesObj->c_calc_val === '0') {
                $CCalcVal = $easyPayStoresImagesObj->c_calc_val; //信用卡扣率
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中选择'信用卡扣率'"
                ]);
            }
//            if ($easyPayStoresImagesObj->un_c_calc_val || $easyPayStoresImagesObj->un_c_calc_val===0 || $easyPayStoresImagesObj->un_c_calc_val==='0') {
//                $UnCCalcVal = $easyPayStoresImagesObj->un_c_calc_val; //银联二维码信用卡扣率
//            } else {
//                return json_encode([
//                    'status' => 2,
//                    'message' => "请在'易生进件'中选择'银联二维码信用卡扣率'"
//                ]);
//            }
            $CFeeLowLimit = '0';
            if ($easyPayStoresImagesObj->c_fee_low_limit) $CFeeLowLimit = $easyPayStoresImagesObj->c_fee_low_limit; //信用卡手续费最低值
            $realTimeEntry = '0';
            if ($easyPayStoresImagesObj->real_time_entry) {
                $realTimeEntry = $easyPayStoresImagesObj->real_time_entry; //实时入账(0-否;1-是)
                $DCalcType = 0;
                if ($easyPayStoresImagesObj->d_calc_type) {
                    $DCalcType = $easyPayStoresImagesObj->d_calc_type; //借记卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
                }
                $CCalcType = 0;
                if ($easyPayStoresImagesObj->c_calc_type) {
                    $CCalcType = $easyPayStoresImagesObj->c_calc_type; //信用卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
                }
            }
            $twoBars = [];
            $twoBarsOpen = '';
            if ($easyPayStoresImagesObj->two_bars_open) {
                $twoBarsOpen = $easyPayStoresImagesObj->two_bars_open; //是否开启立招(1-开启;2-关闭,默认2)
                if ($twoBarsOpen == '1') {
                    $twoBars = $easyPayStoresImagesObj->two_bars;
                    if (!$twoBars || $twoBars == '[{}]' || $twoBars == '[{"":""}]') {
                        return json_encode([
                            'status' => 2,
                            'message' => "请在'易生进件'中完善'立招功能'"
                        ]);
                    }
                }
            }

            //查找是否有此通道
            $ways1 = StorePayWay::where('store_id', $store_id)
                ->where('ways_type', '21001')
                ->first();
            if ($ways1) {
                $rate = $ways1->rate; //如果门店设置走门店扫码费率

            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '请设置易生通道费率'
                ]);
            }

            //默认失败
            $send_ways_data['rate'] = $rate;
            $send_ways_data['store_id'] = $store_id;
            $send_ways_data['status'] = '3';
            $send_ways_data['company'] = 'easypay';

            $reqData = [
                'version' => '1.0', //版本号
                'clientCode' => $client_code, //机构号
                'messageType' => 'ALTERFUNC', //报文类型
                'backUrl' => url('api/easypay/merchant_apply_notify_url'), //o,回调地址
                'merTrace' => $easypay_store->mer_trace, //商户唯一标识
                'operaTrace' => $this->random_number(2), //操作流水,20
                'funcInfo' => [
                    [ //支付宝
                        'funcId' => '2', //功能ID
                        'calcVal' => $rate //扣率
                    ],
                    [ //微信
                        'funcId' => '3', //功能ID
                        'calcVal' => $rate //扣率
                    ],
                    [ //银联二维码
                        'funcId' => '12', //功能ID
                        'DStlmType' => "1", //借记卡扣率方式:1-封顶;0-不封顶
                        'DCalcVal' => "0.45", //借记卡扣率（单位：%）
                        'DStlmMaxAmt' => "20", //借记卡封顶金额（单位：元）, 借记卡扣率方式封顶时必填
                        'DFeeLowLimit' => "0", //借记卡手续费最低值（单位：元）
                        'CCalcVal' => "0.55", //信用卡扣率（单位：%）
                        'CFeeLowLimit' => "0" //信用卡手续费最低值（单位：元）
                    ],
                    [ //银联营销业务  借记卡封顶   借记卡费率0.38  8封顶   贷记卡0.38
                        'funcId' => '14', //功能ID
                        'DStlmType' => "1", //借记卡扣率方式:1-封顶;0-不封顶
                        'DCalcVal' => "0.38", //借记卡扣率（单位：%）
                        'DStlmMaxAmt' => "8", //借记卡封顶金额（单位：元）, 借记卡扣率方式封顶时必填
                        'DFeeLowLimit' => "0", //借记卡手续费最低值（单位：元）
                        'CCalcVal' => "0.38", //信用卡扣率（单位：%）
                        'CFeeLowLimit' => "0" //信用卡手续费最低值（单位：元）
                    ],
                ]
            ];

            if ($realTimeEntry) { //实时入账
                $reqData['funcInfo'][] = [
                    'funcId' => '25', //功能ID
                    'tradeType' => '2', //交易类型：0-全部；1-银行卡；2-条码支付
                    'DCalcType' => "1", //借记卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
                    'DCalcVal' => '0.02', //借记卡扣率
                    'CCalcType' => "1", //信用卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
                    'CCalcVal' => "0.02" //信用卡扣率
                ];
//                $reqData['funcInfo'][] = [
//                    'funcId' => '25', //功能ID
//                    'tradeType' => '0', //交易类型：0-全部；1-银行卡；2-条码支付
//                    'DCalcType' => "$DCalcType", //借记卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
//                    'DCalcVal' => $DCalcVal, //借记卡扣率
//                    'CCalcType' => "$CCalcType", //信用卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
//                    'CCalcVal' => $CCalcVal //信用卡扣率
//                ];
            }
            if ($twoBarsOpen == '1') { //立招
                $reqData['funcInfo'][] = [
                    'funcId' => '13', //
                    'twoBars' => "$twoBars" //二维码信息,JSON 字符串（LIST）
                ];
            }

            //数组加json_encode($arr ,JSON_UNESCAPED_SLASHES)处理
            foreach ($reqData as &$values) {
                if (is_array($values)) {
                    $values = json_encode($values, 256); //不转义中文为unicode,对应的数字256;不转义斜杠,对应的数字64
                }
            }

            Log::info('易生-商户功能变更-入参：');
            Log::info($reqData);
            $re = $easyPayMerAccessObj->execute($reqData, $key);
            Log::info('易生-商户功能变更-结果：');
            Log::info($re);

            if (isset($re) && !empty($re)) {
                if ($re['data']['retCode'] == '0000') {
                    $status_desc = "审核中";
                    $status = '2';

                    if (isset($re['data']['merTrace'])) {
                        $easypay_store->update([
                            'mer_trace' => $re['data']['merTrace'] //商户唯一标识
                        ]);
                    }
                } else {
                    $status = '3';
                    $status_desc = $re['data']['retMsg'];
                }
            } else {
                $status = '3';
                $status_desc = "三方系统返回错误";
            }

            $data['store_id'] = $store_id;
            $data['rate'] = $rate;
            $data['status'] = $status;
            $data['status_desc'] = $status_desc;
            $data['company'] = 'easypay';
            $return = $this->send_ways_data($data);

            return json_encode($return);
        } catch (\Exception $ex) {
            Log::info('易生商户入网-商户功能变更-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    /**
     * 商户功能变更 (APP)
     */
    public function alterFuncApp(Request $request)
    {
        try {
            $token = $this->parseToken();
            $config_id = $token->config_id;

            $store_id = $request->get('storeId', ''); //门店id
            $rate = $request->get('rate', '');

            $real_time_entry = $request->get('realTimeEntry', '0'); //实时入账(0-否;1-是)
            $d_calc_type = $request->get('dCalcType', ''); //借记卡计费类型(0-按笔数(单位:元);1-按比例(单位:%)),实时入账必传
            $c_calc_type = $request->get('cCalcType', 1); //信用卡计费类型(0-按笔数(单位:元);1-按比例(单位:%)),实时入账必传
            $calc_type = $request->get('calcType', 1); //D1计算方式(1-按比例;0-按笔)
            $pay_date_type = $request->get('payDateType', 0); //D1费用处理模式(0-每天计费,1-节假日,2-节假日按天)
            $d1_is_open = $request->get('d1IsOpen', '0'); //是否开通D1(0-否;1-是)

            $except_token_data = $request->except(['token']);
            $validate = Validator::make($except_token_data, [
                'storeId' => 'required|max:50'
            ], [
                'required' => ':attribute参数为必填项',
                'min' => ':attribute参数长度太短',
                'max' => ':attribute参数长度太长',
                'unique' => ':attribute参数已经被人占用',
                'exists' => ':attribute参数不存在',
                'integer' => ':attribute参数必须是数字',
                'required_if' => ':attribute参数满足条件时必传',
            ], [
                'storeId' => '门店id'
            ]);
            if ($validate->fails()) {
                $this->status = 2;
                $this->message = $validate->getMessageBag()->first();
                return $this->format();
            }

            $insert_data = [
                'store_id' => $store_id,
                'real_time_entry' => $real_time_entry,
                'd_calc_type' => $d_calc_type,
                'c_calc_type' => $c_calc_type,
                'calc_type' => $calc_type,
                'pay_date_type' => $pay_date_type,
                'd1_is_open' => $d1_is_open,
            ];

            $updateEasyPayStoresImagesObj = EasypayStoresImages::where('store_id', $store_id)->first();
            if ($updateEasyPayStoresImagesObj) {
                $result = $updateEasyPayStoresImagesObj->update($insert_data);
                if (!$result) {
                    return [
                        'status' => -1,
                        'message' => '操作失败'
                    ];
                }
            }

            //是否已经进件
            $easypay_store = EasypayStore::where('store_id', $store_id)->first();
            if ($easypay_store) {
                if (!$easypay_store->mer_trace) {
                    return json_encode([
                        'status' => -1,
                        'message' => "易生-商户信息变更-缺少'商户唯一标识'"
                    ]);
                }
            } else {
                return json_encode([
                    'status' => -1,
                    'message' => '请商户先入网'
                ]);
            }

            $Store = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if ($updateEasyPayStoresImagesObj) {
                $config_id = $updateEasyPayStoresImagesObj->new_config_id;
            }
            $easyPayMerAccessObj = new MerAccessController();
            $easyPayConfigObj = new EasyPayConfigController();
            $easypay_config = $easyPayConfigObj->easypay_config($config_id);
            if (!$easypay_config || !$easypay_config->client_code || !$easypay_config->key) {
                return json_encode([
                    'status' => 2,
                    'message' => '易生支付未配置'
                ]);
            }

            $client_code = $easypay_config->client_code; //进件机构号
            $key = $easypay_config->key; //进件密钥

            $easyPayStoresImagesObj = EasypayStoresImages::where('store_id', $store_id)->first();

            if ($easyPayStoresImagesObj->d_calc_val || $easyPayStoresImagesObj->d_calc_val === 0 || $easyPayStoresImagesObj->d_calc_val === '0') {
                $DCalcVal = $easyPayStoresImagesObj->d_calc_val; //借记卡扣率
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中选择'借记卡扣率'"
                ]);
            }

            if ($easyPayStoresImagesObj->c_calc_val || $easyPayStoresImagesObj->c_calc_val === 0 || $easyPayStoresImagesObj->c_calc_val === '0') {
                $CCalcVal = $easyPayStoresImagesObj->c_calc_val; //信用卡扣率
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => "请在'易生进件'中选择'信用卡扣率'"
                ]);
            }

            $realTimeEntry = '0';
            if ($easyPayStoresImagesObj->real_time_entry) {
                $realTimeEntry = $easyPayStoresImagesObj->real_time_entry; //实时入账(0-否;1-是)
                $DCalcType = 0;
                if ($easyPayStoresImagesObj->d_calc_type) {
                    $DCalcType = $easyPayStoresImagesObj->d_calc_type; //借记卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
                }
                $CCalcType = 0;
                if ($easyPayStoresImagesObj->c_calc_type) {
                    $CCalcType = $easyPayStoresImagesObj->c_calc_type; //信用卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
                }
            }
            $twoBars = [];
            $twoBarsOpen = '';
            if ($easyPayStoresImagesObj->two_bars_open) {
                $twoBarsOpen = $easyPayStoresImagesObj->two_bars_open; //是否开启立招(1-开启;2-关闭,默认2)
                if ($twoBarsOpen == '1') {
                    $twoBars = $easyPayStoresImagesObj->two_bars;
                    if (!$twoBars || $twoBars == '[{}]' || $twoBars == '[{"":""}]') {
                        return json_encode([
                            'status' => 2,
                            'message' => "请在'易生进件'中完善'立招功能'"
                        ]);
                    }
                }
            }

            $reqData = [
                'version' => '1.0', //版本号
                'clientCode' => $client_code, //机构号
                'messageType' => 'ALTERFUNC', //报文类型
                'backUrl' => url('api/easypay/merchant_apply_notify_url'), //o,回调地址
                'merTrace' => $easypay_store->mer_trace, //商户唯一标识
                'operaTrace' => $this->random_number(2), //操作流水,20
                'funcInfo' => [
                    [ //支付宝
                        'funcId' => '2', //功能ID
                        'calcVal' => $rate //扣率
                    ],
                    [ //微信
                        'funcId' => '3', //功能ID
                        'calcVal' => $rate //扣率
                    ],
                    [ //银联二维码
                        'funcId' => '12', //功能ID
                        'DStlmType' => "1", //借记卡扣率方式:1-封顶;0-不封顶
                        'DCalcVal' => "0.45", //借记卡扣率（单位：%）
                        'DStlmMaxAmt' => "20", //借记卡封顶金额（单位：元）, 借记卡扣率方式封顶时必填
                        'DFeeLowLimit' => "0", //借记卡手续费最低值（单位：元）
                        'CCalcVal' => "0.55", //信用卡扣率（单位：%）
                        'CFeeLowLimit' => "0" //信用卡手续费最低值（单位：元）
                    ],
                    [ //银联营销业务  借记卡封顶   借记卡费率0.38  8封顶   贷记卡0.38
                        'funcId' => '14', //功能ID
                        'DStlmType' => "1", //借记卡扣率方式:1-封顶;0-不封顶
                        'DCalcVal' => "0.38", //借记卡扣率（单位：%）
                        'DStlmMaxAmt' => "8", //借记卡封顶金额（单位：元）, 借记卡扣率方式封顶时必填
                        'DFeeLowLimit' => "0", //借记卡手续费最低值（单位：元）
                        'CCalcVal' => "0.38", //信用卡扣率（单位：%）
                        'CFeeLowLimit' => "0" //信用卡手续费最低值（单位：元）
                    ],
                ]
            ];

            if ($realTimeEntry) { //实时入账
                $reqData['funcInfo'][] = [
                    'funcId' => '25', //功能ID
                    'tradeType' => '2', //交易类型：0-全部；1-银行卡；2-条码支付
                    'DCalcType' => "1", //借记卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
                    'DCalcVal' => '0.02', //借记卡扣率
                    'CCalcType' => "1", //信用卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
                    'CCalcVal' => "0.02" //信用卡扣率
                ];
//                $reqData['funcInfo'][] = [
//                    'funcId' => '25', //功能ID
//                    'tradeType' => '0', //交易类型：0-全部；1-银行卡；2-条码支付
//                    'DCalcType' => "$DCalcType", //借记卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
//                    'DCalcVal' => $DCalcVal, //借记卡扣率
//                    'CCalcType' => "$CCalcType", //信用卡计费类型:0-按笔数(单位:元);1-按比例(单位:%)
//                    'CCalcVal' => $CCalcVal //信用卡扣率
//                ];
            }
            if ($twoBarsOpen == '1') { //立招
                $reqData['funcInfo'][] = [
                    'funcId' => '13', //
                    'twoBars' => "$twoBars" //二维码信息,JSON 字符串（LIST）
                ];
            }

            //数组加json_encode($arr ,JSON_UNESCAPED_SLASHES)处理
            foreach ($reqData as &$values) {
                if (is_array($values)) {
                    $values = json_encode($values, 256); //不转义中文为unicode,对应的数字256;不转义斜杠,对应的数字64
                }
            }

            Log::info('易生-商户功能变更app-入参：');
            Log::info($reqData);
            $re = $easyPayMerAccessObj->execute($reqData, $key);
            Log::info('易生-商户功能变更app-结果：');
            Log::info($re);

            if (isset($re) && !empty($re)) {
                if ($re['data']['retCode'] == '0000') {
                    $status_desc = "审核中";
                    $status = '2';

                    if (isset($re['data']['merTrace'])) {
                        $easypay_store->update([
                            'mer_trace' => $re['data']['merTrace'] //商户唯一标识
                        ]);
                    }
                } else {
                    $status = '3';
                    $status_desc = $re['data']['retMsg'];
                }
            } else {
                $status = '3';
                $status_desc = "三方系统返回错误";
            }

            $data['store_id'] = $store_id;
            $data['rate'] = $rate;
            $data['status'] = $status;
            $data['status_desc'] = $status_desc;
            $data['company'] = 'easypay';
            $return = $this->send_ways_data($data);

            return json_encode($return);
        } catch (\Exception $ex) {
            Log::info('易生商户入网-商户功能变更-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }


}
