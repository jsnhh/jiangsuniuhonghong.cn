<?php
namespace App\Api\Controllers\User;

use Alipayopen\Sdk\AopClient;
use Alipayopen\Sdk\Request\AlipayTradeRoyaltyRelationBindRequest;
use App\Api\Controllers\BaseController;
use App\Models\AlipayIsvConfig;
use App\Models\DadaConfig;
use App\Models\DlbConfig;
use App\Models\DongguanConfig;
use App\Models\EasypayConfig;
use App\Models\HConfig;
use App\Models\HltxCofig;
use App\Models\HkrtConfig;
use App\Models\HuiPayConfig;
use App\Models\HwcpayConfig;
use App\Models\JdConfig;
use App\Models\LinkageConfigs;
use App\Models\MqttConfig;
use App\Models\NewLandConfig;
use App\Models\QfpayConfig;
use App\Models\TfConfig;
use App\Models\User;
use App\Models\VbillaConfig;
use App\Models\VbillConfig;
use App\Models\WeixinaConfig;
use App\Models\WeixinConfig;
use App\Models\WechatCashCouponConfig;
use App\Models\WechatMerchantCashCouponConfig;
use App\Models\WftpayConfig;
use App\Models\YinshengConfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ConfigController extends BaseController
{

    //支付宝配置
    public function alipay_isv_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token', 'type']);
            $config_type = $request->get('config_type', '01');
            $type = $request->get('type', '2');

            $user = User::where('id', $user->user_id)->first();
//            $hasPermission = $user->hasPermissionTo('支付宝应用配置');
//
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '没有-支付宝应用配置-权限']);
//            }

            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode(['status' => 2, 'message' => '没有权限配置']);
                }
            }


            $config_id = $user->config_id;//配置的id
            $AlipayIsvConfig = AlipayIsvConfig::where('config_id', $config_id)
                ->where('config_type', $config_type)
                ->first();

            //查询
            if ($type == '2') {
                if ($AlipayIsvConfig) {
                    return json_encode(['status' => 1, 'data' => $AlipayIsvConfig]);
                } else {
                    return json_encode(['status' => 2, 'data' => [], 'message' => '请配置支付参数']);
                }
            }
            //添加修改
            if ($type == '1') {
                $check_data = [
                    'app_id' => '应用appid',
                    'alipay_pid' => '支付宝返佣pid',
                    'rsa_private_key' => '软件生成的私钥',
                    'alipay_rsa_public_key' => '支付宝公钥',
                    'callback' => '回调地址',
                    'notify' => '异步地址',
                    'isv_phone' => 'isv电话',
                    'isv_name' => 'isv名称',
                ];

                $check = $this->check_required($request->except(['token']), $check_data);
                if ($check) {
                    return json_encode([
                        'status' => 2,
                        'message' => $check
                    ]);
                }


                $data['config_id'] = $config_id;
                $data['app_id'] = trim($data['app_id']);
                $data['alipay_pid'] = trim($data['alipay_pid']);
                $data['rsa_private_key'] = trim($data['rsa_private_key']);
                $data['alipay_rsa_public_key'] = trim($data['alipay_rsa_public_key']);
                $data['callback'] = trim($data['callback']);
                $data['notify'] = trim($data['notify']);

                if ($AlipayIsvConfig) {
                    $AlipayIsvConfig->update($data);
                    $AlipayIsvConfig->save();
                } else {
                    AlipayIsvConfig::create($data);
                }

            }


            return json_encode(['status' => 1, 'message' => '添加成功', 'data' => $data]);


        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }


    //支付宝直付通配置
    public function alipay_zft_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token', 'type']);
            $config_type = $request->get('config_type', '03');
            $type = $request->get('type', '2');

            $user = User::where('id', $user->user_id)->first();
//            $hasPermission = $user->hasPermissionTo('支付宝应用配置');
//
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '没有权限配置']);
//            }
            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode(['status' => 2, 'message' => '没有权限配置']);
                }
            }

            $config_id = $user->config_id;//配置的id
            $AlipayIsvConfig = AlipayIsvConfig::where('config_id', $config_id)
                ->where('config_type', $config_type)
		        ->first();

            //查询
            if ($type == '2') {
                if ($AlipayIsvConfig) {
                    return json_encode(['status' => 1, 'data' => $AlipayIsvConfig]);
                } else {
                    return json_encode(['status' => 2, 'data' => []]);
                }
            }

            //添加修改
            if ($type == '1') {
                $check_data = [
                    'app_id' => '应用appid',
                    'alipay_pid' => '支付宝返佣pid',
                    'rsa_private_key' => '软件生成的私钥',
                    'alipay_rsa_public_key' => '支付宝公钥',
                    'callback' => '回调地址',
                    'notify' => '异步地址',
                    'isv_phone' => 'isv电话',
                    'isv_name' => 'isv名称',
                ];
                $check = $this->check_required($request->except(['token']), $check_data);
                if ($check) {
                    return json_encode([
                        'status' => 2,
                        'message' => $check
                    ]);
                }


                //绑定直付通分成账户
                $aop = new AopClient();
                $aop->apiVersion = "2.0";
                $aop->appId = trim($data['app_id']);
                $aop->rsaPrivateKey = trim($data['rsa_private_key']);
                $aop->alipayrsaPublicKey = trim($data['alipay_rsa_public_key']);
                $aop->signType = "RSA2";//升级算法
                $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
                $aop->format = "json";
                $aop->charset = "GBK";
                $aop->method = "alipay.trade.royalty.relation.bind";
                $data_re = [
                    'receiver_list' => [
                        0 => [
                            'type' => 'userId',
                            'account' => trim($data['alipay_pid']),
                            'name' => trim($data['isv_name']),
                        ]
                    ],
                    'out_request_no' => time(),
                ];

                $data_re = json_encode($data_re);
                $requests = new AlipayTradeRoyaltyRelationBindRequest();
                $requests->setBizContent($data_re);
                $result = $aop->execute($requests);
                $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
                $resultCode = $result->$responseNode->code;
                if (!empty($resultCode) && $resultCode == 10000) {

                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => $result->$responseNode->sub_msg
                    ]);
                }

                $data['config_id'] = $config_id;
                $data['app_id'] = trim($data['app_id']);
                $data['alipay_pid'] = trim($data['alipay_pid']);
                $data['rsa_private_key'] = trim($data['rsa_private_key']);
                $data['alipay_rsa_public_key'] = trim($data['alipay_rsa_public_key']);
                $data['callback'] = trim($data['callback']);
                $data['notify'] = trim($data['notify']);
                $data['config_type'] = $config_type;

                if ($AlipayIsvConfig) {
                    $AlipayIsvConfig->update($data);
                    $res = $AlipayIsvConfig->save();
                } else {
                    $res = AlipayIsvConfig::create($data);
                }

                if ($res) {
                    return json_encode([
                        'status' => 1,
                        'message' => '添加成功',
                        'data' => $data
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '添加失败',
                    ]);
                }
            }

            return json_encode(['status' => 1, 'message' => '添加成功', 'data' => $data]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }


    //微信配置
    public function weixin_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token', 'type']);

            $user = User::where('id', $user->user_id)->first();
//            $hasPermission = $user->hasPermissionTo('微信应用配置');
//
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '没有权限配置']);
//            }

            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode(['status' => 2, 'message' => '没有权限配置']);
                }
            }

            $data['config_id'] = $user->config_id; //配置的id
            $config = WeixinConfig::where('config_id', $user->config_id)->first();

            //查询
            if ($request->get('type') == '2') {
                if ($config) {
                    return json_encode(['status' => 1, 'data' => $config]);
                } else {
                    return json_encode(['status' => 1, 'data' => []]);
                }
            }

            //添加修改
            if ($request->get('type') == '1') {
                $check_data = [
                    'app_id' => '公众号appid',
                    'app_secret' => '秘钥',
                    'wx_merchant_id' => '服务商商户号',
                    'key' => '微信支付key',
                    'cert_path' => '证书文件',
                    'key_path' => '秘钥文件',
                    'auth_path' => '域名授权地址',
                ];
                $check = $this->check_required($request->except(['token']), $check_data);
                if ($check) {
                    return json_encode([
                        'status' => 2,
                        'message' => $check
                    ]);
                }

                if ($config) {
                    $re = WeiXinConfig::where('config_id', $user->config_id)->update($data);
                } else {
                    $re = WeiXinConfig::create($data);
                }
                return json_encode([
                    'status' => 1,
                    'message' => '保存成功',
                    'data' => $data,
                ]);
            }

            return json_encode([
                'status' => '2',
                'message' => 'type_参数不正确'
            ]);
        } catch (\Exception $ex) {
            return json_encode([
                'status' => 2,
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    //京东聚合配置
    public function jd_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token', 'type']);
            $user = User::where('id', $user->user_id)->first();
//            $hasPermission = $user->hasPermissionTo('京东金融配置');
//
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '没有权限配置京东金融']);
//            }

            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode(['status' => 2, 'message' => '没有权限配置']);
                }
            }

            $data['config_id'] = $user->config_id;//配置的id
            $config = JdConfig::where('config_id', $user->config_id)->first();

            //查询
            if ($request->get('type') == '2') {
                if ($config) {
                    return json_encode(['status' => 1, 'data' => $config]);
                } else {
                    return json_encode(['status' => 1, 'data' => []]);
                }
            }

            //添加修改
            if ($request->get('type') == '1') {
                $check_data = [
                    'systemId' => '系统名称ID',
                    'store_md_key' => "加签密钥",
                    'store_des_key' => "加密密钥",
                    'agentNo' => "服务商商户号",
                ];

                $check = $this->check_required($request->except(['token']), $check_data);
                if ($check) {
                    return json_encode([
                        'status' => 2,
                        'message' => $check
                    ]);
                }

                if ($config) {
                    $re = JdConfig::where('config_id', $user->config_id)->update($data);
                } else {
                    $re = JdConfig::create($data);
                }
		
                return json_encode([
                    'status' => 1,
                    'message' => '保存成功',
                    'data' => $data,
                ]);
            }
	    
            return json_encode(['status' => 2, 'message' => 'type_参数不正确']);
        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }


    //新大陆配置
    public function new_land_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token', 'type']);

            $user = User::where('id', $user->user_id)->first();
//            $hasPermission = $user->hasPermissionTo('新大陆配置');
//
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '新大陆配置没有权限']);
//            }


            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode(['status' => 2, 'message' => '没有权限配置']);
                }
            }


            $data['config_id'] = $user->config_id;//配置的id
            $config = NewLandConfig::where('config_id', $user->config_id)->first();

            //查询
            if ($request->get('type') == '2') {
                if ($config) {
                    return json_encode(['status' => 1, 'data' => $config]);

                } else {
                    return json_encode(['status' => 1, 'data' => []]);

                }
            }

            //添加修改
            if ($request->get('type') == '1') {
                $check_data = [
                    'org_no' => '机构号',
                    'nl_key' => '机构密钥',
                ];


                $check = $this->check_required($request->except(['token']), $check_data);
                if ($check) {
                    return json_encode([
                        'status' => 2,
                        'message' => $check
                    ]);
                }

                if ($config) {
                    $re = NewLandConfig::where('config_id', $user->config_id)->update($data);
                } else {
                    $re = NewLandConfig::create($data);
                }
                return json_encode([
                    'status' => 1,
                    'message' => '保存成功',
                    'data' => $data,
                ]);
            }
            return json_encode(['status' => 2, 'message' => 'type_参数不正确']);
        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }

    }


    //和融通聚合配置
    public function h_config(Request $request)
    {

        try {
            $user = $this->parseToken();
            $data = $request->except(['token', 'type']);
            $user = User::where('id', $user->user_id)->first();
//            $hasPermission = $user->hasPermissionTo('和融通配置');
//
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '和融通配置没有权限']);
//            }


            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode(['status' => 2, 'message' => '没有权限配置']);
                }
            }


            $data['config_id'] = $user->config_id;//配置的id
            $config = HConfig::where('config_id', $user->config_id)->first();

            //查询
            if ($request->get('type') == '2') {
                if ($config) {
                    return json_encode(['status' => 1, 'data' => $config]);

                } else {
                    return json_encode(['status' => 1, 'data' => []]);

                }
            }

            //添加修改
            if ($request->get('type') == '1') {
//                $check_data = [
//                    'orgNo' => '机构号',
//                    'md_key' => "加签密钥",
//                    'ali_pid' => '支付宝合作者pid',
//                    '	wx_appid' => "微信公众号appid",
//                    'wx_secret' => "微信公众号密钥",
//                ];
//
//
//                $check = $this->check_required($request->except(['token']), $check_data);
//                if ($check) {
//                    return json_encode([
//                        'status' => 2,
//                        'message' => $check
//                    ]);
//                }

                if ($config) {
                    $re = HConfig::where('config_id', $user->config_id)->update($data);
                } else {
                    $re = HConfig::create($data);
                }
                return json_encode([
                    'status' => 1,
                    'message' => '保存成功',
                    'data' => $data,
                ]);
            }
            return json_encode(['status' => 2, 'message' => 'type_参数不正确']);
        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }

    }


    //阿里云mqtt配置
    public function mqtt_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token']);
            $access_key_id = $request->get('access_key_id', '');

            $user = User::where('id', $user->user_id)->first();
//            $hasPermission = $user->hasPermissionTo('MQTT推送');
//
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '没有权限配置']);
//            }


            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode(['status' => 2, 'message' => '没有权限配置']);
                }
            }


            $config_id = $user->config_id;//配置的id
            $MqttConfig = MqttConfig::where('config_id', $config_id)->first();

            //查询
            if ($access_key_id == '') {
                if ($MqttConfig) {
                    return json_encode(['status' => 1, 'data' => $MqttConfig]);
                } else {
                    return json_encode(['status' => 1, 'data' => []]);
                }
            }
            //添加修改

            $check_data = [
                'access_key_id' => '阿里云access_key_id',
                'access_key_secret' => '阿里云access_key_secret',
                'server' => "公网接入点地址",
                'port' => '端口号',
                'instance_id' => 'MQTT实例 ID',
                'group_id' => 'Group ID',
                'topic' => '主题Topic',
            ];

            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }


            $data['config_id'] = $config_id;

            if ($MqttConfig) {
                $MqttConfig->update($data);
                $MqttConfig->save();
            } else {
                MqttConfig::create($data);
            }


            return json_encode(['status' => 1, 'message' => '添加成功', 'data' => $data]);


        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }


    //多啦宝配置
    public function dlb_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token']);
            $getdata = $request->get('getdata', '');

            $user = User::where('id', $user->user_id)->first();
//            $hasPermission = $user->hasPermissionTo('哆啦宝配置');
//
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '没有权限配置']);
//            }


            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode(['status' => 2, 'message' => '没有权限配置']);
                }
            }


            $config_id = $user->config_id;//配置的id
            $DlbConfig = DlbConfig::where('config_id', $config_id)->first();

            //查询
            if ($getdata == 1) {
                if ($DlbConfig) {
                    return json_encode(['status' => 1, 'data' => $DlbConfig]);
                } else {
                    return json_encode(['status' => 3, 'data' => []]);
                }
            }
            //添加修改

            $check_data = [
                'access_key' => 'accessKey',
                'secret_key' => 'secretKey',
                'agent_num' => "代理商编号"
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }


            $data['config_id'] = $config_id;
            $msg = '';
            if ($DlbConfig) {
                $DlbConfig->update($data);
                $DlbConfig->save();
                $msg = '保存成功';
            } else {
                DlbConfig::create($data);
                $msg = '添加成功';
            }


            return json_encode(['status' => 1, 'message' => $msg, 'data' => $data]);
        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }

    }


    //TF支付配置
    public function tf_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token']);
            $getdata = $request->get('getdata', '');
            $qd = $request->get('qd', '1');

            $user = User::where('id', $user->user_id)->first();
//            $hasPermission = $user->hasPermissionTo('TF支付配置');
//
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '没有权限配置']);
//            }


            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode(['status' => 2, 'message' => '没有权限配置']);
                }
            }


            $config_id = $user->config_id;//配置的id
            $TfConfig = TfConfig::where('config_id', $config_id)
                ->where('qd', $qd)
                ->first();

            //查询
            if ($getdata == 1) {
                if ($TfConfig) {
                    return json_encode(['status' => 1, 'data' => $TfConfig]);
                } else {
                    return json_encode(['status' => 3, 'data' => []]);
                }
            }
            //添加修改

            $check_data = [
                'mch_id' => 'mch_id',
                'md_key' => 'md_key',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }


            $data['config_id'] = $config_id;
            $msg = '';
            if ($TfConfig) {
                $TfConfig->update($data);
                $TfConfig->save();
                $msg = '保存成功';
            } else {
                TfConfig::create($data);
                $msg = '添加成功';
            }


            return json_encode(['status' => 1, 'message' => $msg, 'data' => $data]);
        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }

    }


    //代理商升级为独立配置
    public function update_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            if ($user->pid != 0) {
                return json_encode(['status' => 2, 'message' => '没有权限配置']);

            }
            $phone = $request->get('phone', '');
            $user = User::where('phone', $phone)->first();
            if (!$user) {
                return json_encode(['status' => 2, 'message' => '手机号不存在']);
            }
            $user_id = $user->id;

            $user = $this->getSubIds($user_id);
            foreach ($user as $k => $v) {
                User::where('id', $v)->update(['config_id' => $phone]);
            }
            return json_encode(['status' => 1, 'message' => '升级完成']);

        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }

    }


    //随行付
    public function vbill_config(Request $request)
    {

        try {
            $user = $this->parseToken();
            $data = $request->except(['token', 'type']);
            $user = User::where('id', $user->user_id)->first();
//            $hasPermission = $user->hasPermissionTo('随行付配置');
//
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '随行付配置没有权限']);
//            }


            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode(['status' => 2, 'message' => '随行付配置没有权限']);
                }
            }


            $data['config_id'] = $user->config_id;//配置的id
            $config = VbillConfig::where('config_id', $user->config_id)->first();

            //查询
            if ($request->get('type') == '2') {
                if ($config) {
                    return json_encode(['status' => 1, 'data' => $config]);

                } else {
                    return json_encode(['status' => 1, 'data' => []]);

                }
            }

            //添加修改
            if ($request->get('type') == '1') {
//
                if ($config) {
                    $re = VbillConfig::where('config_id', $user->config_id)->update($data);
                } else {
                    $re = VbillConfig::create($data);
                }
                return json_encode([
                    'status' => 1,
                    'message' => '保存成功',
                    'data' => $data,
                ]);
            }
            return json_encode(['status' => 2, 'message' => 'type_参数不正确']);
        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }

    }
    
    
    //汇付配置
    public function hui_pay_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token', 'type']);

            $user = User::where('id', $user->user_id)->first();

//            $hasPermission = $user->hasPermissionTo('汇付支付配置');
//            if (!$hasPermission) {
//                $this->status = '2';
//                $this->message = '汇付配置没有权限';
//                return $this->format();
//            }

            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    $this->status = '2';
                    $this->message = '汇付支付配置没有权限';
                    return $this->format();
                }
            }

            $data['config_id'] = $user->config_id;  //配置的id
            $config = HuiPayConfig::where('config_id', $user->config_id)->first();

            //查询
            if ($request->get('type') == '2') {
                if ($config) {
                    $this->status = '1';
                    $data = $config;
                    return $this->format($data);
                } else {
                    $this->status = '1';
                    $data = [];
                    return $this->format($data);
                }
            }

            //添加修改
            if ($request->get('type') == '1') {
                if ($config) {
                    HuiPayConfig::where('config_id', $user->config_id)->update($data);
                } else {
                    HuiPayConfig::create($data);
                }

                $this->status = '1';
                $this->message = '保存成功';
                return $this->format($data);
            }

            $this->status = '2';
            $this->message = 'type_参数不正确';
            return $this->format();
        } catch (\Exception $ex) {
            $this->status = '2';
            $this->message = $ex->getMessage();
            return $this->format();
        }
    }
    
    
    //葫芦天下
    public function hltx_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token']);
            $getdata = $request->get('getdata', '');
            $qd = $request->get('qd', '1');

            $user = User::where('id', $user->user_id)->first();
//            $hasPermission = $user->hasPermissionTo('HL支付配置');
//            if (!$hasPermission) {
//                return json_encode([
//                    'status' => '2',
//                    'message' => '没有-HL支付配置-权限'
//                ]);
//            }

            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode([
                        'status' => '2',
                        'message' => '非平台用户，config_id与phone不相同'
                    ]);
                }
            }

            $config_id = $user->config_id; //配置的id
            $HltxConfig= HltxCofig::where('config_id', $config_id)
                ->where('qd', $qd)
                ->first();
		
            //查询
            if ($getdata == 1) {
                if ($HltxConfig) {
                    return json_encode([
                        'status' => '1',
                        'data' => $HltxConfig
                    ]);
                } else {
                    return json_encode([
                        'status' => '2',
                        'data' => []
                    ]);
                }
            }
	    
            //添加修改
            $check_data = [
                'isvid' => '服务商ID',
                'public_key' => '支付渠道平台公钥',
                'private_key' => "商户私钥",
                'pay_appid' => "微信支付appid",
                'wx_appid' => "微信appid",
                'wx_secret' => "微信secret",
                'subscribe_appid' => "微信推荐关注subscribe_appid",
                'ali_pid' => "支付宝Pid",
                'wx_channelid' => "微信渠道号",
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => '2',
                    'message' => $check
                ]);
            }

            $data['config_id'] = $config_id;
            $msg = '';
            if ($HltxConfig) {
                $HltxConfig->update($data);
                $res = $HltxConfig->save();
                $msg = '保存成功';
            } else {
                $res = HltxCofig::create($data);
                $msg = '添加成功';
            }

            if ($res) {
                return json_encode([
                    'status' => '1',
                    'message' => $msg,
                    'data' => $data
                ]);
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '操作失败',
                    'data' => $data
                ]);
            }

        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    //随行付A
    public function vbilla_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token', 'type']);
            $user = User::where('id', $user->user_id)->first();

//            $hasPermission = $user->hasPermissionTo('随行付A配置');
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '随行付A配置没有权限']);
//            }

            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode(['status' => 2, 'message' => '随行付A配置没有权限']);
                }
            }

            $data['config_id'] = $user->config_id; //配置的id
            $config = VbillaConfig::where('config_id', $user->config_id)->first();

            //查询
            if ($request->get('type') == '2') {
                if ($config) {
                    return json_encode(['status' => 1, 'data' => $config]);
                } else {
                    return json_encode(['status' => 1, 'data' => []]);
                }
            }

            //添加修改
            if ($request->get('type') == '1') {
                if ($config) {
                    $re = VbillaConfig::where('config_id', $user->config_id)->update($data);
                } else {
                    $re = VbillaConfig::create($data);
                }
		
                return json_encode([
                    'status' => 1,
                    'message' => '保存成功',
                    'data' => $data,
                ]);
            }
	    
            return json_encode(['status' => 2, 'message' => 'type_参数不正确']);
        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }

    }


    //易生支付
    public function easypay_config(Request $request)
    {
        try {
            $token = $this->parseToken();
            $data = $request->except(['token', 'type']);
            $user = User::where('id', $token->user_id)->first();

//            $hasPermission = $user->hasPermissionTo('易生支付配置');
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '易生支付配置没有权限']);
//            }

            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode(['status' => 2, 'message' => '易生配置没有权限']);
                }
            }

            $data['config_id'] = $user->config_id; //配置的id
            $config = EasypayConfig::where('config_id', $user->config_id)->first();

            //查询
            if ($request->get('type') == '2') {
                if ($config) {
                    return json_encode(['status' => 1, 'data' => $config]);
                } else {
                    return json_encode(['status' => 1, 'data' => []]);
                }
            }

            //添加修改
            if ($request->get('type') == '1') {
                if ($config) {
                    $re = EasypayConfig::where('config_id', $user->config_id)->update($data);
                } else {
                    $re = EasypayConfig::create($data);
                }

                return json_encode([
                    'status' => 1,
                    'message' => '保存成功',
                    'data' => $data,
                ]);
            }

            return json_encode([
                'status' => 2,
                'message' => 'type_参数不正确'
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => '-1',
                'message' => $exception->getMessage()
            ]);
        }
    }


    //海科融通
    public function hkrt_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token', 'type']);
            $user = User::where('id', $user->user_id)->first();

//            $hasPermission = $user->hasPermissionTo('海科融通配置');
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '海科融通配置没有权限']);
//            }

            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode(['status' => 2, 'message' => '海科融通配置没有权限']);
                }
            }

            $data['config_id'] = $user->config_id;//配置的id
            $config = HkrtConfig::where('config_id', $user->config_id)->first();

            //查询
            if ($request->get('type') == '2') {
                if ($config) {
                    return json_encode(['status' => 1, 'data' => $config]);
                } else {
                    return json_encode(['status' => 1, 'data' => []]);
                }
            }

            //添加修改
            if ($request->get('type') == '1') {
                if ($config) {
                    $re = HkrtConfig::where('config_id', $user->config_id)->update($data);
                } else {
                    $re = HkrtConfig::create($data);
                }
                return json_encode([
                    'status' => 1,
                    'message' => '保存成功',
                    'data' => $data,
                ]);
            }
            return json_encode([
                'status' => 2,
                'message' => 'type_参数不正确'
            ]);

        } catch (\Exception $ex) {
            return json_encode([
                'status' => 2,
                'message' => $ex->getMessage()
            ]);
        }

    }


    //联动优势
    public function linkage_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token', 'type']);
            $user = User::where('id', $user->user_id)->first();

//            $hasPermission = $user->hasPermissionTo('联动优势配置');
//            if (!$hasPermission) {
//                return json_encode([
//                    'status' => '2',
//                    'message' => '没有-联动优势配置-权限'
//                ]);
//            }

            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode([
                        'status' => '2',
                        'message' => '非平台用户，config_id与phone不同，暂无法操作'
                    ]);
                }
            }

            $data['config_id'] = $user->config_id; //配置的id
            $config = LinkageConfigs::where('config_id', $user->config_id)->first();

            //查询
            if ($request->get('type') == '2') {
                if ($config) {
                    return json_encode([
                        'status' => '1',
                        'data' => $config
                    ]);
                } else {
                    return json_encode([
                        'status' => '1',
                        'data' => []
                    ]);
                }
            }

            //添加修改
            if ($request->get('type') == '1') {
                if ($config) {
                    $re = LinkageConfigs::where('config_id', $user->config_id)->update($data);
                } else {
                    $re = LinkageConfigs::create($data);
                }

                if ($re) {
                    return json_encode([
                        'status' => '1',
                        'message' => '保存成功',
                        'data' => $data,
                    ]);
                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => '保存失败',
                        'data' => $data,
                    ]);
                }
            }

            return json_encode([
                'status' => 2,
                'message' => 'type_参数不正确'
            ]);

        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }

    }


    //威富通
    public function wftpay_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token', 'type']);
            $user = User::where('id', $user->user_id)->first();

//            $hasPermission = $user->hasPermissionTo('威富通配置');
//            if (!$hasPermission) {
//                return json_encode([
//                    'status' => '2',
//                    'message' => '没有-威富通配置-权限'
//                ]);
//            }

            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode([
                        'status' => '2',
                        'message' => '非平台用户，config_id与phone不同，暂无法操作'
                    ]);
                }
            }

            $data['config_id'] = $user->config_id; //配置的id
            $config = WftpayConfig::where('config_id', $user->config_id)->first();

            //查询
            if ($request->get('type') == '2') {
                if ($config) {
                    return json_encode([
                        'status' => '1',
                        'data' => $config
                    ]);
                } else {
                    return json_encode([
                        'status' => '1',
                        'data' => []
                    ]);
                }
            }

            //添加修改
            if ($request->get('type') == '1') {
                if ($config) {
                    $re = WftpayConfig::where('config_id', $user->config_id)->update($data);
                } else {
                    $re = WftpayConfig::create($data);
                }

                if ($re) {
                    return json_encode([
                        'status' => '1',
                        'message' => '保存成功',
                        'data' => $data,
                    ]);
                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => '保存失败',
                        'data' => $data,
                    ]);
                }
            }

            return json_encode([
                'status' => 2,
                'message' => 'type_参数不正确'
            ]);
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    //达达配置
    public function dada_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token', 'type']);
            $user = User::where('id', $user->user_id)->first();

//            $hasPermission = $user->hasPermissionTo('达达配置');
//            if (!$hasPermission) {
//                return json_encode([
//                    'status' => '2',
//                    'message' => '没有-达达配置-权限'
//                ]);
//            }

            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode([
                        'status' => 2,
                        'message' => '非平台用户，config_id与phone不同，暂无法操作'
                    ]);
                }
            }

            $data['config_id'] = $user->config_id; //配置的id
            $config = DadaConfig::where('config_id', $user->config_id)->first();

            //查询
            if ($request->get('type') == '2') {
                if ($config) {
                    return json_encode([
                        'status' => 1,
                        'data' => $config
                    ]);
                } else {
                    return json_encode([
                        'status' => 1,
                        'data' => []
                    ]);
                }
            }

            //添加修改
            if ($request->get('type') == '1') {
                if ($config) {
                    $re = DadaConfig::where('config_id', $user->config_id)->update($data);
                } else {
                    $re = DadaConfig::create($data);
                }

                if ($re) {
                    return json_encode([
                        'status' => 1,
                        'message' => '保存成功',
                        'data' => $data,
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '保存失败',
                        'data' => $data,
                    ]);
                }
            }

            return json_encode([
                'status' => 2,
                'message' => 'type_参数不正确'
            ]);
        } catch (\Exception $ex) {
            return json_encode([
                'status' => -1,
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }

    }


    //汇旺财
    public function hwcpay_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token', 'type']);
            $user = User::where('id', $user->user_id)->first();

//            $hasPermission = $user->hasPermissionTo('汇旺财配置');
//            if (!$hasPermission) {
//                return json_encode([
//                    'status' => '2',
//                    'message' => '没有-汇旺财配置-权限'
//                ]);
//            }

            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode([
                        'status' => '2',
                        'message' => '非平台用户，config_id与phone不同，暂无法操作'
                    ]);
                }
            }

            $data['config_id'] = $user->config_id; //配置的id
            $config = HwcpayConfig::where('config_id', $user->config_id)->first();

            //查询
            if ($request->get('type') == '2') {
                if ($config) {
                    return json_encode([
                        'status' => '1',
                        'data' => $config
                    ]);
                } else {
                    return json_encode([
                        'status' => '1',
                        'data' => []
                    ]);
                }
            }

            //添加修改
            if ($request->get('type') == '1') {
                if ($config) {
                    $re = $config->update($data);
                } else {
                    $re = HwcpayConfig::create($data);
                }

                if ($re) {
                    return json_encode([
                        'status' => '1',
                        'message' => '保存成功',
                        'data' => $data,
                    ]);
                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => '保存失败',
                        'data' => $data,
                    ]);
                }
            }

            return json_encode([
                'status' => 2,
                'message' => 'type_参数不正确'
            ]);
        } catch (\Exception $ex) {
            return json_encode([
                'status' => -1,
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    //微信a
    public function weixina_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token', 'type']);

            $user = User::where('id', $user->user_id)->first();
//            $hasPermission = $user->hasPermissionTo('微信应用配置');
//
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '没有权限配置']);
//            }

            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode(['status' => 2, 'message' => '没有权限配置']);
                }
            }

            $data['config_id'] = $user->config_id; //配置的id
            $config = WeixinaConfig::where('config_id', $user->config_id)->first();

            //查询
            if ($request->get('type') == '2') {
                if ($config) {
                    return json_encode(['status' => 1, 'data' => $config]);
                } else {
                    return json_encode(['status' => 1, 'data' => []]);
                }
            }

            //添加修改
            if ($request->get('type') == '1') {
                $check_data = [
                    'app_id' => '公众号appid',
                    'app_secret' => '秘钥',
                    'wx_merchant_id' => '服务商商户号',
                    'key' => '微信支付key',
                    'cert_path' => '证书文件',
                    'key_path' => '秘钥文件',
                    'auth_path' => '域名授权地址',
                ];
                $check = $this->check_required($request->except(['token']), $check_data);
                if ($check) {
                    return json_encode([
                        'status' => 2,
                        'message' => $check
                    ]);
                }

                if ($config) {
                    $re = WeiXinaConfig::where('config_id', $user->config_id)->update($data);
                } else {
                    $re = WeiXinaConfig::create($data);
                }

                if ($re) {
                    return json_encode([
                        'status' => 1,
                        'message' => '保存成功',
                        'data' => $data,
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '保存失败',
                        'data' => $data,
                    ]);
                }
            }

            return json_encode([
                'status' => '2',
                'message' => 'type_参数不正确'
            ]);
        } catch (\Exception $ex) {
            return json_encode([
                'status' => 2,
                'message' => $ex->getMessage().' | '.$ex->getLine()
            ]);
        }
    }


    //东莞银行
    public function dongguan_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token', 'type']);
            $user = User::where('id', $user->user_id)->first();

//            $hasPermission = $user->hasPermissionTo('东莞银行配置');
//            if (!$hasPermission) {
//                return json_encode([
//                    'status' => '2',
//                    'message' => '没有-东莞银行配置-权限'
//                ]);
//            }

            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return json_encode([
                        'status' => '2',
                        'message' => '非平台用户，config_id与phone不同，暂无法操作'
                    ]);
                }
            }

            $data['config_id'] = $user->config_id; //配置的id
            $config = DongguanConfig::where('config_id', $user->config_id)->first();

            //查询
            if ($request->get('type') == '2') {
                if ($config) {
                    return json_encode([
                        'status' => '1',
                        'data' => $config
                    ]);
                } else {
                    return json_encode([
                        'status' => '1',
                        'data' => []
                    ]);
                }
            }

            //添加修改
            if ($request->get('type') == '1') {
                if ($config) {
                    $re = DongguanConfig::where('config_id', $user->config_id)->update($data);
                } else {
                    $re = DongguanConfig::create($data);
                }

                if ($re) {
                    return json_encode([
                        'status' => '1',
                        'message' => '保存成功',
                        'data' => $data,
                    ]);
                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => '保存失败',
                        'data' => $data,
                    ]);
                }
            }

            return json_encode([
                'status' => 2,
                'message' => 'type_参数不正确'
            ]);

        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().' | '.$ex->getLine()
            ]);
        }

    }


    //银盛
    public function yinsheng_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $type = $request->get('type', 2); //1-新增or更新;2-查询

            $data = $request->except(['token', 'type']);
            $user = User::where('id', $user->user_id)->first();

//            $hasPermission = $user->hasPermissionTo('银盛配置');
//            if (!$hasPermission) {
////                $this->status = 2;
////                $this->message = "没有'银盛配置'权限";
////                return $this->format();
//                return $this->responseDataJson(200001); //
//            }

            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return $this->responseDataJson(100001); //
                }
            }

            $data['config_id'] = $user->config_id; //配置的id
            $config = YinshengConfig::where('config_id', $user->config_id)->first();

            //查询
            if ($type == 2) {
                if ($config) {
                    return $this->responseDataJson(1, $config);
                } else {
                    return $this->responseDataJson(1);
                }
            }

            //添加or修改
            if ($type == 1) {
                if ($config) {
                    $re = YinshengConfig::where('config_id', $user->config_id)->update($data);
                } else {
                    $re = YinshengConfig::create($data);
                }

                if ($re) {
                    return $this->responseDataJson(200003, $data);
                } else {
                    return $this->responseDataJson(200004, $data);
                }
            }

            return $this->responseDataJson(200002);
        } catch (\Exception $ex) {
            Log::info('银盛-支付配置-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            return $this->responseDataJson(-1);
        }

    }


    /**
     * 微信代金券配置
     */
    public function wechatCashCouponConfig(Request $request)
    {
        $input = $request->all();
        $user = $this->parseToken();
        $input['user_id'] = $user->user_id;

        $model = new WechatCashCouponConfig();

        $data = $model->getWechatCashCouponConfig($input);

        return $this->sys_response_layui(1, '请求成功', $data);
    }


    /**
     * 保存微信代金券配置
     */
    public function saveWechatCashCouponConfig(Request $request)
    {
        $input = $request->all();
        $user = $this->parseToken();
        $input['user_id'] = $user->user_id;

        // 参数校验
        $check_data = [
            'wx_merchant_id'    => '商户号',
            'api_v3_secret'     => 'api_v3秘钥',
            'api_serial_no'     => '商户api证书序列号',
            'key_path'          => '商户私钥文件',
            'notify_url'        => '核销回调地址',
            'appid'             => 'appid',
            'secret'            => 'secret',
            'wechat_pay_certificate_key_path' => '微信支付平台证书',
        ];
        $check = $this->check_required($input, $check_data);
        if ($check) {
            return $this->sys_response(40000, $check);
        }

        // 需要插入的数据
        $insert_data = [
            'wx_merchant_id'    => $input['wx_merchant_id'] ?? '',
            'api_v3_secret'     => $input['api_v3_secret'] ?? '',
            'api_serial_no'     => $input['api_serial_no'] ?? '',
            'key_path'          => $input['key_path'] ?? '',
            'notify_url'        => $input['notify_url'] ?? '',
            'appid'             => $input['appid'] ?? '',
            'secret'            => $input['secret'] ?? '',
            'channel'           => $input['channel'] ?? '',
            'wechat_pay_certificate_key_path' => $input['wechat_pay_certificate_key_path'] ?? '',
        ];

        $model = new WechatCashCouponConfig();

        // 查询数据库中是否有这条数据，有就更新，没有就新增
        $data = $model->getWechatCashCouponConfig($input);
        if (!empty($data)) { // 更新数据
            $res = $model->updateWechatCashCouponConfig($input, $insert_data);
        } else { // 新增
            $insert_data['user_id'] = $user->user_id;
            $res = $model->saveWechatCashCouponConfig($insert_data);
        }

        if ($res) {
            return $this->sys_response_layui(1, '操作成功');
        } else {
            return $this->sys_response_layui(202, '操作失败');
        }
    }


    //钱方
    public function qfpay_config(Request $request)
    {
        try {
            $user = $this->parseToken();
            $type = $request->get('type', 2); //1-新增or更新;2-查询

            $data = $request->except(['token', 'type']);
            $user = User::where('id', $user->user_id)->first();

//            $hasPermission = $user->hasPermissionTo('钱方配置');
//            if (!$hasPermission) {
////                $this->status = 2;
////                $this->message = "没有'银盛配置'权限";
////                return $this->format();
//                return $this->responseDataJson(200101); //
//            }

            if ($user->level > 1) {
                if ($user->config_id != $user->phone) {
                    return $this->responseDataJson(100001); //
                }
            }

            $data['config_id'] = $user->config_id; //配置的id
            $config = QfpayConfig::where('config_id', $user->config_id)->first();

            //查询
            if ($type == 2) {
                if ($config) {
                    return $this->responseDataJson(1, $config);
                } else {
                    return $this->responseDataJson(1);
                }
            }

            //添加or修改
            if ($type == 1) {
                if ($config) {
                    $re = QfpayConfig::where('config_id', $user->config_id)->update($data);
                } else {
                    $re = QfpayConfig::create($data);
                }

                if ($re) {
                    return $this->responseDataJson(200103, $data);
                } else {
                    return $this->responseDataJson(200104, $data);
                }
            }

            return $this->responseDataJson(200102);
        } catch (\Exception $ex) {
            Log::info('钱方-支付配置-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            return $this->responseDataJson(-1);
        }
    }


    /**
     * 微信商家券配置
     */
    public function wechatMerchantCashCouponConfig(Request $request)
    {
        $input = $request->all();
        $user = $this->parseToken();
        $input['user_id'] = $user->user_id;

        $model = new WechatMerchantCashCouponConfig();

        $data = $model->getWechatCashCouponConfig($input);

        return $this->sys_response_layui(1, '请求成功', $data);
    }

    /**
     * 保存微信商家券配置
     */
    public function saveWechatMerchantCashCouponConfig(Request $request)
    {
        $input = $request->all();
        $user = $this->parseToken();
        $input['user_id'] = $user->user_id;

        // 参数校验
        $check_data = [
            'merchant_id'    => '商户号',
            'api_v2_secret'     => 'api_v2秘钥',
            'api_v3_secret'     => 'api_v3秘钥',
            'api_serial_no'     => '商户api证书序列号',
            'key_path'          => '商户私钥文件',
            'wechat_pay_certificate_key_path' => '微信支付平台证书',
        ];
        $check = $this->check_required($input, $check_data);
        if ($check) {
            return $this->sys_response(40000, $check);
        }

        $date = date('Y-m-d H:i:s', time());

        // 需要插入的数据
        $insert_data = [
            'config_id'         => $input['config_id'] ?? 1234,
            'user_id'           => $input['user_id'] ?? '',
            'merchant_id'       => $input['merchant_id'] ?? '',
            'api_v2_secret'     => $input['api_v2_secret'] ?? '',
            'api_v3_secret'     => $input['api_v3_secret'] ?? '',
            'api_serial_no'     => $input['api_serial_no'] ?? '',
            'key_path'          => $input['key_path'] ?? '',
            'channel'           => $input['channel'] ?? '',
            'created_at'        => $date,
            'updated_at'        => $date,
            'wechat_pay_certificate_key_path' => $input['wechat_pay_certificate_key_path'] ?? '',
        ];

        $model = new WechatMerchantCashCouponConfig();

        // 查询数据库中是否有这条数据，有就更新，没有就新增
        $data = $model->getWechatCashCouponConfig($input);
        if (!empty($data)) { // 更新数据
            $res = $model->updateWechatCashCouponConfig($input, $insert_data);
        } else { // 新增
            $insert_data['user_id'] = $user->user_id;
            $res = $model->saveWechatCashCouponConfig($insert_data);
        }

        if ($res) {
            return $this->sys_response_layui(1, '操作成功');
        } else {
            return $this->sys_response_layui(202, '操作失败');
        }
    }


}
