<?php

namespace App\Api\Controllers\User;


use App\Api\Controllers\BaseController;
use App\Models\UserAuths;
use App\Models\UserRelations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class UserAuthController extends BaseController
{
    //测试参数
    // protected $wb_appId = 'TIDAoVx1';    //业务流程唯一标识
    // protected $wb_secret = 'prca6FlRI3D6T0tXsX0szVd6tuby2fcjfr2zuqzwfxKFS739v1N9fpiZK5lbCUhj'; //WBappid 对应的密钥
    // protected $licence = 'OcnA9Di9VsHhoZaDYh3VeEkQQWTZj4SfXTTS4sX2yLHFHbpCmQaSU0rxxXPRh4RbLE//M0y03GnZWMSoKv4EqqewuHTY6XGpN+mSXhAu6E5S7qVnevIxEQjOzOvQLZgauG9J18k6+QTEAq4+alZZqI2QUHuQs1lakxeDNntgpuAgfc7qOdEjd8bcFyOIYDPb3FA9naAXnfVNQbdxxgN34WgHdaOh60rdv2uFXXN46zyIniIQrYuKyo9ZJEmBgL0PP+GGVwUlkmwboCNDDS/gCRfkI9d9gCWSD1EadEBPaliMdEpo+fNMjNAzi6Y0qXWGICxfqdJyVpHll7JAOUORgg==';

    //正式参数
    protected $wb_appId = 'IDAWELgz';    //业务流程唯一标识
    protected $wb_secret = 'tjKzWbJ6MVOCVSUueFLDTUai1MKkGPB2JwbEPFl6FEJ6mQ5NeuwQA565q98fKstD'; //WBappid 对应的密钥
    protected $licence = 'TmTGph5DIAHx7iWwmFccqqMEjr6VMBo/uN2a7EpWGGlPvLc7B4SdreQJZuRiEjEKH2LSgdb1N+0ayfQwuShzdTrPBeh7rVjKQ93r1MhWegXUucOH+zw9wHcFjNZTNXOtorpObs6s0eHfGhoPeIW72MRb5tgtR0kWz5p/MMc3xlMgfc7qOdEjd8bcFyOIYDPb3FA9naAXnfVNQbdxxgN34WgHdaOh60rdv2uFXXN46zyIniIQrYuKyo9ZJEmBgL0PP+GGVwUlkmwboCNDDS/gCRfkI9d9gCWSD1EadEBPaliMdEpo+fNMjNAzi6Y0qXWGICxfqdJyVpHll7JAOUORgg==';

    protected $grant_type = 'client_credential';
    protected $version = '1.0.0';

    protected $accessTokenUrl = 'https://miniprogram-kyc.tencentcloudapi.com/api/oauth2/access_token';
    protected $signTicketUrl = 'https://miniprogram-kyc.tencentcloudapi.com/api/oauth2/api_ticket';
    protected $nonceTicketUrl = 'https://miniprogram-kyc.tencentcloudapi.com/api/oauth2/api_ticket';
    protected $faceIdUrl = 'https://miniprogram-kyc.tencentcloudapi.com/api/server/getfaceid?orderNo=';
    protected $queryFaceIdUrl = 'https://miniprogram-kyc.tencentcloudapi.com/api/v2/base/queryfacerecord?orderNo=';


    //实名认证相关
    public function getUserAuthInfo(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_id = $request->get('user_id', '');

            $userAuths = UserAuths::where('user_id', $user_id)->first();
            if ($userAuths) {
                return json_encode([
                    'status' => '1',
                    'message' => '返回成功',
                    'data' => $userAuths
                ]);
            }

            return json_encode([
                'status' => '1',
                'message' => '返回成功',
                'data' => []
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => '-1',
                'message' => $exception->getMessage()
            ]);
        }
    }

    //实名认证相关
    public function getUserAuth(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_id = $token->user_id;

            $userAuths = UserAuths::where('user_id', $user_id)->first();
            if ($userAuths) {
                return json_encode([
                    'status' => '1',
                    'message' => '返回成功',
                    'data' => [
                        'user_id' => $userAuths->user_id,
                        'user_name' => $userAuths->user_name,
                        'id_card' => $userAuths->id_card,
                        'user_type' => $userAuths->user_type,
                        'id_card_front_url' => $userAuths->id_card_front_url,
                        'id_card_back_url' => $userAuths->id_card_back_url,
                    ]
                ]);
            }

            return json_encode([
                'status' => '1',
                'message' => '返回成功',
                'data' => []
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => '-1',
                'message' => $exception->getMessage()
            ]);
        }
    }

    //实名认证相关
    public function userAuth(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_id = $token->user_id;

            $user_name = $request->get('user_name', '');
            $id_card = $request->get('id_card', '');
            $id_card_front_url = $request->get('id_card_front_url', '');
            $id_card_back_url = $request->get('id_card_back_url', '');

            //验证参数不能为空
            if ($user_name == "" || $id_card == "" || $id_card_front_url == "" || $id_card_back_url == "") {
                return json_encode([
                    'status' => '2',
                    'message' => '获取参数失败'
                ]);
            }
            //检查是否已实名
            $exist_userAuths = UserAuths::where('user_name', $user_name)
                ->where('id_card', $id_card)->where('user_type', '1')->first();
            if ($exist_userAuths) {
                return json_encode([
                    'status' => '2',
                    'message' => '已实名，请确认实名信息'
                ]);
            }

            $userAuths = UserAuths::where('user_id', $user_id)->first();
            if ($userAuths) {
                $userAuths->update([
                    'id_card' => $id_card,
                    'user_name' => $user_name,
                    'id_card_front_url' => $id_card_front_url,
                    'id_card_back_url' => $id_card_back_url,
                    'user_type' => '1', //实名认证类型：0 未认证，1 已认证
                ]);
            } else {
                UserAuths::create([
                    'user_id' => $user_id,
                    'id_card' => $id_card,
                    'user_name' => $user_name,
                    'id_card_front_url' => $id_card_front_url,
                    'id_card_back_url' => $id_card_back_url,
                    'user_type' => '1', //实名认证类型：0 未认证，1 已认证
                ]);
            }

            return json_encode([
                'status' => '1',
                'message' => '操作成功'
            ]);

        } catch (\Exception $exception) {
            return json_encode([
                'status' => '-1',
                'message' => $exception->getMessage()
            ]);
        }
    }

    //上传身份信息
    public function getFaceId(Request $request)
    {

        $token = $this->parseToken();
        $user_id = $token->user_id;

        $faceId = "";
        $unionUserId = "";

        $userAuths = UserAuths::where('user_id', $user_id)->first();
        if ($userAuths) {

            $orderNO_exist = $userAuths->order_no;
            $redis_face_id = Cache::get($orderNO_exist);

            if ($redis_face_id) {
                $faceId = $redis_face_id;
                $orderNo = $orderNO_exist;
                $unionUserId = $userAuths->union_user_id;
            } else {
                $unionUserId = 'ys' . time() . mt_rand(10000, 99999);
                $nonceStr = $this->randomStr(); //32位随机数

                $sign_ticket = $this->getSignTicket();

                $params = [
                    'WBappid' => $this->wb_appId,
                    'userId' => $unionUserId, //用户唯一标识
                    'ticket' => $sign_ticket, // SIGN 类型
                    'version' => $this->version,
                    'nonce' => $nonceStr
                ];

                $sign = $this->sign($params);

                $orderNo = 'or' . time() . mt_rand(10000, 99999);

                $paramsData = [
                    'webankAppId' => $this->wb_appId,
                    'orderNo' => $orderNo, //唯一标识
                    'name' => $userAuths->user_name,
                    'idNo' => $userAuths->id_card,
                    'userId' => $unionUserId,
                    'version' => $this->version,
                    'sign' => $sign,
                    'nonce' => $nonceStr
                ];

                $faceIdUrl = $this->faceIdUrl . $orderNo;

                $res = $this->http_post($faceIdUrl, $paramsData);

                Log::info("----add----- face response:" . json_encode($res));

                if ($res['code'] == '0') { //请求成功

                    $faceId = $res['result']['faceId']; //刷脸用户标识，调 SDK 时传入

                    $userAuths->update([
                        'order_no' => $orderNo,
                        'face_id' => $faceId,
                        'nonce' => $nonceStr,
                        'union_user_id' => $unionUserId,
                    ]);

                    Cache::add($orderNo, $faceId, 4);
                }
            }

            $nonce_ticket = $this->getNonceTicket($unionUserId);

            $nonceSign = $this->randomStr(); //32位随机数
            $face_params = [
                'WBappid' => $this->wb_appId,
                'userId' => $unionUserId, //用户唯一标识
                'ticket' => $nonce_ticket, // NONCE  类型
                'version' => $this->version,
                'nonce' => $nonceSign
            ];
            $face_sign = $this->sign($face_params);

            $face_data = [
                'wb_appId' => $this->wb_appId,
                'orderNo' => $orderNo, //唯一标识
                'userId' => $unionUserId,
                'licence' => $this->licence,
                'version' => $this->version,
                'faceId' => $faceId,
                'sign' => $face_sign,
                'nonceStr' => $nonceSign //32位随机数
            ];

            Log::info("-----getFaceId---face data:" . json_encode($face_data));

            return json_encode([
                'status' => '1',
                'message' => '返回成功',
                'data' => $face_data
            ]);

        } else {
            return json_encode([
                'status' => '2',
                'message' => '获取实名信息失败',
            ]);
        }
    }


    //查询核身结果
    public function queryfacerecord(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_id = $token->user_id;
            $order_no = $request->get('order_no', '');

            $nonceStr = $this->randomStr(); //32位随机数

            $sign_ticket = $this->getSignTicket();

            $params = [
                'WBappid' => $this->wb_appId,
                'orderNo' => $order_no,
                'ticket' => $sign_ticket, // SIGN 类型
                'version' => $this->version,
                'nonce' => $nonceStr
            ];

            $sign = $this->sign($params);

            $query_params = [
                'appId' => $this->wb_appId,
                'version' => '1.0.0',
                'nonce' => $nonceStr,
                'orderNo' => $order_no,
                'sign' => $sign
            ];

            $queryFaceIdUrl = $this->queryFaceIdUrl . $order_no;

            $res = $this->http_post($queryFaceIdUrl, $query_params);

            Log::info("----------queryfacerecord res:" . json_encode($res));

            if ($res['code'] == '0') { //请求成功
                Log::info("-----人脸识别成功-----:");
                $userAuths = UserAuths::where('user_id', $user_id)->where('order_no', $order_no)->first();
                if ($userAuths) {
                    $userAuths->update([
                        'user_type' => '1', //实名认证类型：0 未认证，1 已认证
                    ]);
                }

                return json_encode([
                    'status' => '1',
                    'message' => '操作成功'
                ]);
            } else {
                Log::info("-----人脸识别失败-----:" . $res['msg']);

                return json_encode([
                    'status' => '2',
                    'message' => $res['msg']
                ]);
            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => '-1',
                'message' => $exception->getMessage()
            ]);
        }
    }

    //获取 sign ticket,缓存20分账
    public function getSignTicket()
    {
        try {
            //1、获取 Access Token
            $wb_access_token = $this->getAccessToken();

            //2、获取 SIGN ticket
            $sign_ticket = '';
            $redis_sign_ticket = Cache::get('sign_ticket');
            if ($redis_sign_ticket) {
                $sign_ticket = $redis_sign_ticket;
            } else {
                $ticket_params = [
                    'app_id' => $this->wb_appId,
                    'access_token' => $wb_access_token,
                    'type' => 'SIGN',
                    'version' => '1.0.0'
                ];

                $ticketDataParam = $this->getParams($ticket_params);

                $ticketUrl = $this->signTicketUrl . "?" . $ticketDataParam;

                $resTicket = $this->http_get($ticketUrl);

                Log::info("----------get SIGN tickets:" . $resTicket);

                $resTicket = json_decode($resTicket, true);

                if ($resTicket['code'] == '0') { //请求成功
                    $tickets = $resTicket['tickets'][0];
                    $sign_ticket = $tickets['value'];
                    Cache::add('sign_ticket', $sign_ticket, 20);
                }
            }

            return $sign_ticket;
        } catch (\Exception $exception) {
            return json_encode([
                'status' => '-1',
                'message' => $exception->getMessage()
            ]);
        }
    }

    //获取 NONCE ticket，其有效期为120秒，且一次性有效，即每次启动 SDK 刷脸都要重新请求 NONCE ticket
    public function getNonceTicket($unionUserId)
    {
        try {

            //1、获取 Access Token
            $wb_access_token = $this->getAccessToken();

            //2、获取 NONCE ticket
            Log::info("----begin-------获取 NONCE ticket.");

            $nonce_ticket = '';

            $params = [
                'app_id' => $this->wb_appId,
                'access_token' => $wb_access_token,
                'type' => 'NONCE',
                'version' => $this->version,
                'user_id' => $unionUserId
            ];

            $dataParam = $this->getParams($params);

            $nonceTicketUrl = $this->nonceTicketUrl . "?" . $dataParam;

            $resTicket = $this->http_get($nonceTicketUrl);

            // Log::info("-----------------get nonce tickets:" . $resTicket);

            $resTicket = json_decode($resTicket, true);

            if ($resTicket['code'] == '0') { //请求成功
                $tickets = $resTicket['tickets'][0];
                $nonce_ticket = $tickets['value'];
            }

            // Log::info("------success------ nonce tickets:" . $nonce_ticket);

            return $nonce_ticket;
        } catch (\Exception $exception) {
            return json_encode([
                'status' => '-1',
                'message' => $exception->getMessage()
            ]);
        }
    }

    //获取 Access Token,缓存20分账
    public function getAccessToken()
    {

        //1、获取 Access Token
        $wb_access_token = '';
        $redis_access_token = Cache::get('wb_access_token');
        if ($redis_access_token) {
            $wb_access_token = $redis_access_token;
        } else {
            //获取新的accessToken
            $params = [
                'app_id' => $this->wb_appId,
                'secret' => $this->wb_secret,
                'grant_type' => $this->grant_type,
                'version' => $this->version
            ];

            $dataParam = $this->getParams($params);

            $accessTokenUrl = $this->accessTokenUrl . "?" . $dataParam;

            $res = $this->http_get($accessTokenUrl);

            Log::info("-----------------getAccessToken:" . $res);

            $res = json_decode($res, true);

            if ($res['code'] == '0') { //请求成功
                $wb_access_token = $res['access_token'];
                Cache::add('wb_access_token', $wb_access_token, 20);
            }
        }

        return $wb_access_token;
    }

    public function getParams($params)
    {
        ksort($params);
        $params = array_filter($params);
        $string = "";

        foreach ($params as $name => $value) {
            $string .= $name . '=' . $value . '&';
        }
        $result = substr($string, 0, strlen($string) - 1);

        return $result;
    }

    /**
     * 随机字符串
     */
    public function randomStr($length = 32)
    {
        //字符组合
        $str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $len = strlen($str) - 1;
        $randStr = '';
        for ($i = 0; $i < $length; $i++) {
            $randStr .= $str[mt_rand(0, $len)];
        }
        return $randStr;
    }

    /**
     * post发送请求
     *
     * @param string $url 请求地址
     * @param array $myParams 请求数据
     * @param string $public_key 验签公钥地址
     * @param string $response_name 返回数据key值
     * @return array 1-成功;2-失败
     */

    function http_post($url, $data)
    {

        $url = str_replace(' ', '+', $url);  //请求中不能使用空格，使用 + 替换

        $data = json_encode($data);
        $headerArray = array("Content-type:application/json;charset='utf-8'", "Accept:application/json");
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headerArray);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);
        return json_decode($output, true);
    }

    public function http_get($url)
    {
        //$url = 'http://www.example.com/user.php?id=123&name=john';
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_URL, $url);//设置请求地址
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//设置返回结果为字符串
        curl_setopt($ch, CURLOPT_HEADER, 0);//设置请求头部不输出
        $result = curl_exec($ch);//执行curl请求
        curl_close($ch);//关闭curl

        return $result;
    }

    public function sign($params)
    {
        $arr = array_values($params);
        asort($arr, SORT_STRING);
        $arr = implode('', $arr);

        $sign = sha1($arr);
        return strtoupper($sign);
        // foreach ($params as $name => $value) {
        //     Log::info("====value=====". $value);
        //     $string .= $value ;
        // }

//        $string = "1.0.03d5b9rs7s6wC7511oOvhBuaXl9m3eqLQ3laILEmhfz1HE4xEPr357hIWnnj4x6Pl1Xe8UqgONxNFs0TU1dbpI8cR2eNASqg5TIDAoVx1ys169018799223349";
//
//        return strtoupper(sha1($string));
    }

}
