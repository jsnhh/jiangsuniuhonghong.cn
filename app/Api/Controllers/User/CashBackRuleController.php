<?php
/**
 * Created by PhpStorm.
 * User: daimingkang
 * Date: 2018/12/21
 * Time: 2:34 PM
 */

namespace App\Api\Controllers\User;


use App\Api\Controllers\BaseController;
use App\Models\ActivityStoreRate;
use App\Models\CashBackRule;
use App\Models\CashBackRuleUser;
use App\Models\Device;
use App\Models\Order;
use App\Models\Store;
use App\Models\StoreMonthOrder;
use App\Models\StorePayWay;
use App\Models\StoreTransactionReward;
use App\Models\TerminalReward;
use App\Models\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CashBackRuleController extends BaseController
{
    //交易返现规则列查询
    public function ruleList(Request $request)
    {

        try {
            $user = $this->parseToken();//
            $user_id = $user->user_id;
            $id = $request->get('id', '');


            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("cash_back_rule");
            } else {
                $obj = DB::table('cash_back_rule');
            }

            $where = [];

            if ($id) {
                $where[] = ['id', $id];
            }
            $obj = $obj->where($where)
                ->where('user_id', $user_id)
                ->orderBy('created_at', 'desc');
            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);


        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    public function selRule()
    {
        $user = $this->parseToken();
        $user_id = $user->user_id;
        $rules = CashBackRule::where('user_id', $user_id)->select('id', 'rule_name')
            ->orderBy('created_at', 'desc')->get();
        $this->status = 1;
        $this->meassage = '数据请求成功';
        return $this->format($rules);
    }

    //添加交易返现规则
    public function addCashBackRule(Request $request)
    {

        try {
            $user = $this->parseToken();//
            $user_id = $user->user_id;
            $rule_name = $request->get('rule_name', '');
            $time_start = $request->get('start_time', '');
            $time_end = $request->get('end_time', '');
            $standard_amt = $request->get('standard_amt');
            $standard_single_amt = $request->get('standard_single_amt');
            $standard_total = $request->get('standard_total');
            $first_amt = $request->get('first_amt', '');
            $first_single_amt = $request->get('first_single_amt', '');
            $first_total = $request->get('first_total', '');
            $again_amt = $request->get('again_amt');
            $again_single_amt = $request->get('again_single_amt');
            $again_total = $request->get('again_total');
            $standard_term = $request->get('standard_term');

            $check_data = [
                'rule_name' => '规则名称',
                'start_time' => '规则开始时间',
                'end_time' => '规则结束时间',
                'standard_amt' => '激活标准奖励金额',
                'standard_single_amt' => '激活标准奖励单笔金额条件',
                'standard_total' => '激活标准奖励交易笔数条件',
                'first_amt' => '首次奖励金额',
                'first_single_amt' => '首次奖励单笔金额条件',
                'first_total' => '首次交易笔数',
                'again_amt' => '二次奖励金额',
                'again_single_amt' => '二次奖励单笔金额条件',
                'again_total' => '二次奖励交易笔数条件',
                'standard_term' => '激活标准期限'
            ];
            $check = $this->check_required($request->all(), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $insert_data = [
                'user_id' => $user_id,
                'rule_name' => $rule_name,
                'start_time' => $time_start,
                'end_time' => $time_end,
                'standard_amt' => $standard_amt,
                'standard_single_amt' => $standard_single_amt,
                'standard_total' => $standard_total,
                'first_amt' => $first_amt,
                'first_single_amt' => $first_single_amt,
                'first_total' => $first_total,
                'again_amt' => $again_amt,
                'again_single_amt' => $again_single_amt,
                'again_total' => $again_total,
                'standard_term' => $standard_term

            ];
            CashBackRule::create($insert_data);

            return json_encode([
                'status' => 1,
                'message' => '设备终端交易达标规则添加成功'
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    public function getCashBackRuleId(Request $request)
    {
        try {
            $user_id = $request->get('user_id');
            $id = $request->get('id');
            if (!empty($id)) {
                $rule = CashBackRule::where('id', $id)->select('*')->first();
            }
            if (!empty($user_id)) {
                $rule = CashBackRuleUser::where('user_id', $user_id)->select('*')->first();
            }

            if (!$rule) {
                return json_encode([
                    'status' => 2,
                    'message' => '没有添加返现规则，请在交易返现中添加交易返现规则'
                ]);
            }
            $this->status = 1;
            $this->message = "数据返回成功";

            return $this->format($rule);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    public function delCashBackRule(Request $request)
    {

        try {
            $user = $this->parseToken();//
            $id = $request->get('id', '');
            $check_data = [
                'id' => 'id'
            ];
            $check = $this->check_required($request->all(), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $userRule = CashBackRuleUser::where('rule_id', $id)->select('*')->get();
            if (count($userRule) > 1) {
                return json_encode([
                    'status' => 2,
                    'message' => '该规则已绑定代理商禁止删除'
                ]);
            }
            CashBackRule::where('id', $id)->delete();
            return json_encode([
                'status' => 1,
                'message' => '交易返现规则删除成功'
            ]);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    public function upCashBackRule(Request $request)
    {
        try {
            $time_start = $request->get('start_time', '');
            $time_end = $request->get('end_time', '');
            $standard_amt = $request->get('standard_amt');
            $standard_single_amt = $request->get('standard_single_amt');
            $standard_total = $request->get('standard_total');
            $first_amt = $request->get('first_amt', '');
            $first_single_amt = $request->get('first_single_amt', '');
            $first_total = $request->get('first_total', '');
            $again_amt = $request->get('again_amt');
            $again_single_amt = $request->get('again_single_amt');
            $again_total = $request->get('again_total');
            $id = $request->get('id');
            $check_data = [
                'id' => 'id'
            ];
            $check = $this->check_required($request->all(), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $updateData = array();
            $updateData['start_time'] = $time_start;
            $updateData['end_time'] = $time_end;
            $updateData['standard_amt'] = $standard_amt;
            $updateData['standard_single_amt'] = $standard_single_amt;
            $updateData['standard_total'] = $standard_total;
            $updateData['first_amt'] = $first_amt;
            $updateData['first_single_amt'] = $first_single_amt;
            $updateData['first_total'] = $first_total;
            $updateData['again_amt'] = $again_amt;
            $updateData['again_single_amt'] = $again_single_amt;
            $updateData['again_total'] = $again_total;
            $updateData['updated_at'] = date_format(new DateTime(), 'Y-m-d H:i:s');
            $rule = CashBackRule::where('id', $id)->select('*')->first();
            if ($rule->standard_satisfy_total > 0) {
                return json_encode([
                    'status' => 2,
                    'message' => '该规则已产生交易达标奖励禁止编辑'
                ]);
            }
            CashBackRule::where('id', $id)->update($updateData);
            return json_encode([
                'status' => 1,
                'message' => '修改成功'
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    //代理商开启返现规则
    public function addUserRule(Request $request)
    {
        try {
            $user_id = $request->get('user_id');
            $rule_id = $request->get('rule_id');
            $user_name = $request->get('user_name');
            $check_data = [
                'rule_id' => '返现规则',
                'user_id' => '代理商id',
                'user_name' => '代理商名称',

            ];
            $check = $this->check_required($request->all(), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $cashBackRule = CashBackRule::where('id', $rule_id)->select("*")->first();
            //本级代理
            $this->add($user_id, $rule_id, $user_name, $cashBackRule);
            $where[] = ['is_delete', '=', 0];
            $ids = $this->getSubIds($user_id);
            array_shift($ids);
            $obj = DB::table('users')->where($where)->whereIn('id', $ids)->get();
            foreach ($obj as $user) {//下级所有代理
                $this->add($user->id, $rule_id, $user->name, $cashBackRule);
            }
            return json_encode([
                'status' => 1,
                'message' => '返现规则添加成功'
            ]);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    //通过user_id获取返现规则
    public function getUserRule(Request $request)
    {
        try {
            $user_id = $request->get('user_id');
            $data = DB::table('cash_back_rule_user')
                ->leftJoin('cash_back_rule', 'cash_back_rule_user.rule_id', '=', 'cash_back_rule.id')
                ->where('cash_back_rule_user.user_id', $user_id)
                ->select('cash_back_rule_user.id', 'cash_back_rule_user.user_id', 'cash_back_rule_user.rule_id',
                    'cash_back_rule_user.status', 'cash_back_rule.rule_name','cash_back_rule_user.user_name')
                ->first();
            $this->status = 1;
            $this->message = "数据返回成功";

            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    public function getStoreTransaction(Request $request)
    {
        try {
            $agent_id = $request->get('agent_id');
            $check_data = [
                'agent_id' => '返现规则id',
            ];
            $check = $this->check_required($request->all(), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $storeTransactionReward = StoreTransactionReward::where('agent_id', $agent_id)
                ->select('*')
                ->get();
            if (count($storeTransactionReward) > 1) {
                return json_encode([
                    'status' => 3,
                    'message' => "该规则已产生商户检测"
                ]);
            }
            return json_encode([
                'status' => 1,
                'message' => "允许删除"
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    public function closeUserRule(Request $request)
    {
        try {
            $agent_id = $request->get('agent_id');
            $check_data = [
                'agent_id' => '返现规则id',
            ];
            $check = $this->check_required($request->all(), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $ids = $this->getSubIds($agent_id);
            $where[] = ['status', '=', "1"];
            $updateData['status'] = "0";
            DB::table('cash_back_rule_user')->where($where)->whereIn('user_id', $ids)->update($updateData);
            return json_encode([
                'status' => 1,
                'message' => "停用成功"
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    public function openUserRule(Request $request)
    {
        try {
            $agent_id = $request->get('agent_id');
            $check_data = [
                'agent_id' => '返现规则id',
            ];
            $check = $this->check_required($request->all(), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $ids = $this->getSubIds($agent_id);
            $where[] = ['status', '=', "0"];
            $updateData['status'] = "1";
            DB::table('cash_back_rule_user')->where($where)->whereIn('user_id', $ids)->update($updateData);
            return json_encode([
                'status' => 1,
                'message' => "开启成功"
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    public function testing_standard(Request $request)
    {
        try {
            $id = $request->get('id');

            $cashBackRuleUsers = CashBackRuleUser::where('status', "1")
                ->where('rule_id',$id)
                ->select('*')
                ->get();
            if (count($cashBackRuleUsers) < 1) {
                return json_encode([
                    'status' => 1,
                    'message' => '检测成功'
                ]);
            }
            //遍历所有规则下的代理商
            foreach ($cashBackRuleUsers as $cashBackRuleUser) {
                $cashBack = CashBackRule::where("id", $cashBackRuleUser->rule_id)
                    ->select("*")->first();
                $where = [];
                $where[] = ['user_id', '=', $cashBackRuleUser->user_id];
                $where[] = ['store_type', '!=', 3];
                $where[] = ['is_delete', 0];
                $where[] = ['is_close', 0];
                $where[] = ['created_at', '>=', $cashBack->start_time];
                $where[] = ['created_at', '<=', $cashBack->end_time];
                Log::info($where);
                //通过代理商查出所有商户除去小微
                $stores = Store::where($where)
                    ->select('*')->get();
                if (count($stores) > 1) {
                    foreach ($stores as $store) {
                        $cashBackRule = CashBackRule::where("id", $cashBackRuleUser->rule_id)
                            ->select("*")->first();
                        //查询代理商所属规则详情
                        $date = $store->created_at;
                        $standard_end_time = date("Y-m-d H:i:s", strtotime("$date +$cashBackRule->standard_term day"));
                        $first_start_time = date("Y-m-d H:i:s", strtotime("$standard_end_time +30 day"));//检测首次奖励开始时间
                        $first_end_time = date("Y-m-d H:i:s", strtotime("$first_start_time +30 day"));//检测首次奖励结束时间
                        $again_start_time = date("Y-m-d H:i:s", strtotime("$first_end_time +30 day"));//检测首次奖励开始时间
                        $again_end_time = date("Y-m-d H:i:s", strtotime("$again_start_time +30 day"));//检测首次奖励结束时间
                        //激活检测
                        //if (strtotime($cashBackRule->end_time) >= strtotime($standard_end_time)) {
                            $where = [];
                            $where[] = ['total_amount', '>=', $cashBackRule->standard_single_amt];
                            $where[] = ['store_id', '=', $store->store_id];
                            $where[] = ['pay_time', '>=', $date];
                            $where[] = ['pay_time', '<=', $standard_end_time];
                            $where[] = ['pay_status', '=', 1];
                            $orderCount = Order::where($where)
                                ->count();

                            $orders = "";
                            if ($orderCount >= $cashBackRule->standard_total) {
                                $orders = Order::where($where)->orderBy('pay_time', 'ASC')
                                    ->offset($cashBackRule->standard_total - 1)->limit(1)->first();
                            }
                            $storeTransactionReward = StoreTransactionReward::where('store_id', $store->store_id)->select('*')->first();
                            //Log::info("商户" . $storeTransactionReward);
                            if (!$storeTransactionReward) {
                                $data = array();
                                $data['user_id'] = $cashBackRule->user_id;
                                $data['store_id'] = $store->store_id;
                                $data['store_name'] = $store->store_name;
                                $data['agent_id'] = $cashBackRuleUser->user_id;
                                $data['agent_name'] = $cashBackRuleUser->user_name;
                                $data['rule_id'] = $cashBackRuleUser->rule_id;
                                $data['store_bind_time'] = $store->created_at;
                                $data['standard_done_total'] = $orderCount;
                                if ($orders) {
                                    $cashBackRule_total = CashBackRule::where("id", $cashBackRuleUser->rule_id)
                                        ->select('standard_satisfy_total')->first();
                                    //规则激活总数量加一
                                    $ruleDate['standard_satisfy_total'] = $cashBackRule_total->standard_satisfy_total + 1;
                                    CashBackRule::where('id', $cashBackRule->id)->update($ruleDate);
                                    //奖励添加到代理
                                    $userRule = CashBackRuleUser::where('user_id', $store->user_id)->select("*")->first();
                                    if ($userRule) {
                                        $userRuleUp = [
                                            'standard_store_amt' => $userRule->standard_store_amt + 1,
                                            'standard_total_sum' => $userRule->standard_total_sum + $userRule->standard_sum,
                                            'total_sum' => $userRule->total_sum + $userRule->standard_sum
                                        ];
                                        CashBackRuleUser::where('user_id', $userRule->user_id)->update($userRuleUp);
                                        $data['standard_done_amt'] = $userRule->standard_sum;
                                        $data['standard_time'] = $orders->pay_time;
                                        $data['total_amt'] = $userRule->standard_sum;
                                        $data['standard_status'] = "1";
                                    }
                                    //上级抽取返现差
                                    $user = User::where('id', $store->user_id)->select('pid')->first();
                                    $pUserRule = CashBackRuleUser::where('user_id', $user->pid)->select("*")->first();
                                    if ($pUserRule) {
                                        if ($pUserRule->standard_sum > $pUserRule->lower_standard_sum) {
                                            $amt = $pUserRule->standard_sum - $pUserRule->lower_standard_sum;
                                            $pUserRuleUp = [
                                                'lower_standard_store_amt' => $pUserRule->lower_standard_store_amt + 1,
                                                'lower_standard_total_sum' => $pUserRule->lower_standard_total_sum + $amt,
                                                'total_sum' => $pUserRule->total_sum + $amt,
                                            ];
                                            CashBackRuleUser::where('user_id', $pUserRule->user_id)->update($pUserRuleUp);
                                        }
                                    }
                                }
                                $data['standard_end_time'] = $standard_end_time;
                                $data['first_end_time'] = $first_end_time;
                                $data['again_end_time'] = $again_end_time;
                                $data['first_start_time'] = $first_start_time;
                                $data['again_start_time'] = $again_start_time;
                                StoreTransactionReward::create($data);


                            } else {
                                if ($storeTransactionReward->standard_status != "1") {
                                    $update = array();
                                    $update['standard_done_total'] = $orderCount;
                                    if ($orders) {
                                        //规则激活总数量加一
                                        $ruleDate['standard_satisfy_total'] = $cashBackRule->standard_satisfy_total + 1;
                                        CashBackRule::where('id', $cashBackRule->id)->update($ruleDate);
                                        //奖励添加到代理
                                        $userRule = CashBackRuleUser::where('user_id', $store->user_id)->select("*")->first();
                                        if ($userRule) {
                                            $userRuleUp = [
                                                'standard_store_amt' => $userRule->standard_store_amt + 1,
                                                'standard_total_sum' => $userRule->standard_total_sum + $userRule->standard_sum,
                                                'total_sum' => $userRule->total_sum + $userRule->standard_sum
                                            ];
                                            CashBackRuleUser::where('user_id', $userRule->user_id)->update($userRuleUp);
                                            $update['standard_done_amt'] = $userRule->standard_sum;
                                            $update['standard_status'] = "1";
                                            $update['total_amt'] = $storeTransactionReward->total_amt + $userRule->standard_sum;
                                        }
                                        //上级抽取返现差
                                        $user = User::where('id', $store->user_id)->select('pid')->first();
                                        $pUserRule = CashBackRuleUser::where('user_id', $user->pid)->select("*")->first();
                                        if ($pUserRule) {
                                            if ($pUserRule->standard_sum > $pUserRule->lower_standard_sum) {
                                                $amt = $pUserRule->standard_sum - $pUserRule->lower_standard_sum;
                                                $pUserRuleUp = [
                                                    'lower_standard_store_amt' => $pUserRule->lower_standard_store_amt + 1,
                                                    'lower_standard_total_sum' => $pUserRule->lower_standard_total_sum + $amt,
                                                    'total_sum' => $pUserRule->total_sum + $amt,
                                                ];
                                                CashBackRuleUser::where('user_id', $pUserRule->user_id)->update($pUserRuleUp);
                                            }
                                        }
                                    }
                                    StoreTransactionReward::where('store_id', $storeTransactionReward->store_id)->update($update);
                                }
                            }

                        //}
                        $this->StoreTransactionRewardFirst($store, $cashBackRule);//首次奖励检测
                        $this->StoreTransactionRewardAgain($store, $cashBackRule);//二次奖励检测
                    }

                }
            }
            return json_encode([
                'status' => 1,
                'message' => '检测成功'
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //检测首次奖励是否达标
    function StoreTransactionRewardFirst($store, $cashBackRule)
    {
        $storeTransactionReward = StoreTransactionReward::where('store_id', $store->store_id)->select('*')->first();
        if ($storeTransactionReward->standard_status == "1" && $storeTransactionReward->first_status == "0") {

            //if (strtotime($cashBackRule->end_time) >= strtotime($storeTransactionReward->first_end_time)) {
                $where = [];
                //$where[] = ['total_amount', '>=', $cashBackRule->first_single_amt];
                $where[] = ['store_id', '=', $storeTransactionReward->store_id];
                $where[] = ['pay_time', '>', $storeTransactionReward->first_start_time];
                $where[] = ['pay_time', '<=', $storeTransactionReward->first_end_time];
                $where[] = ['pay_status', '=', 1];
                //获取首月交易总笔数
                $orderCount = Order::where($where)
                    ->count();
                //获取首月交易总金额
                $totalAmountSum = Order::where($where)->sum('total_amount');
                $update = array();
                $update['first_done_total'] = $orderCount;
                $update['first_total_amt'] = $totalAmountSum;
                if ($orderCount >= $cashBackRule->first_total && $totalAmountSum >= $cashBackRule->first_single_amt) {
                    //规则首次达标总数量加一
                    $ruleDate['first_satisfy_total'] = $cashBackRule->first_satisfy_total + 1;
                    CashBackRule::where('id', $cashBackRule->id)->update($ruleDate);
                    //奖励添加到代理
                    $userRule = CashBackRuleUser::where('user_id', $store->user_id)->select("*")->first();
                    if ($userRule) {
                        $userRuleUp = [
                            'first_store_amt' => $userRule->first_store_amt + 1,
                            'first_total_sum' => $userRule->first_total_sum + $userRule->first_sum,
                            'total_sum' => $userRule->total_sum + $userRule->first_sum
                        ];
                        CashBackRuleUser::where('user_id', $userRule->user_id)->update($userRuleUp);
                        $update['first_done_amt'] = $userRule->first_sum;
                        $update['first_status'] = "1";
                        $update['total_amt'] = $storeTransactionReward->total_amt + $userRule->first_sum;
                    }
                    //上级抽取返现差
                    $user = User::where('id', $store->user_id)->select('pid')->first();
                    $pUserRule = CashBackRuleUser::where('user_id', $user->pid)->select("*")->first();
                    if ($pUserRule) {
                        if ($pUserRule->first_sum > $pUserRule->lower_first_sum) {
                            $amt = $pUserRule->first_sum - $pUserRule->lower_first_sum;
                            $pUserRuleUp = [
                                'lower_first_store_amt' => $pUserRule->lower_first_store_amt + 1,
                                'lower_first_total_sum' => $pUserRule->lower_first_total_sum + $amt,
                                'total_sum' => $pUserRule->total_sum + $amt,
                            ];
                            CashBackRuleUser::where('user_id', $pUserRule->user_id)->update($pUserRuleUp);
                        }
                    }
                }
                StoreTransactionReward::where('store_id', $storeTransactionReward->store_id)->update($update);
            //}
        }

    }

    //检测二次奖励是否达标
    function StoreTransactionRewardAgain($store, $cashBackRule)
    {
        $storeTransactionReward = StoreTransactionReward::where('store_id', $store->store_id)->select('*')->first();
        if ($storeTransactionReward->standard_status == "1" && $storeTransactionReward->first_status == "1" && $storeTransactionReward->again_status == "0") {
            //if (strtotime($cashBackRule->end_time) >= strtotime($storeTransactionReward->again_end_time)) {
                $where = [];
                //$where[] = ['total_amount', '>=', $cashBackRule->again_single_amt];
                $where[] = ['store_id', '=', $storeTransactionReward->store_id];
                $where[] = ['pay_time', '>', $storeTransactionReward->again_start_time];
                $where[] = ['pay_time', '<=', $storeTransactionReward->again_end_time];
                $where[] = ['pay_status', '=', 1];
                //获取次月交易总笔数
                $orderCount = Order::where($where)
                    ->count();
                //获取次月交易总金额
                $totalAmountSum = Order::where($where)->sum('total_amount');
                $update = array();
                $update['again_done_total'] = $orderCount;
                $update['again_total_amt'] = $totalAmountSum;
                if ($orderCount >= $cashBackRule->again_total && $totalAmountSum >= $cashBackRule->again_single_amt) {
                    //规则二次达标总数量加一
                    $ruleDate['again_satisfy_total'] = $cashBackRule->again_satisfy_total + 1;
                    CashBackRule::where('id', $cashBackRule->id)->update($ruleDate);
                    //奖励添加到代理
                    $userRule = CashBackRuleUser::where('user_id', $store->user_id)->select("*")->first();
                    if ($userRule) {
                        $userRuleUp = [
                            'again_store_amt' => $userRule->again_store_amt + 1,
                            'again_total_sum' => $userRule->again_total_sum + $userRule->again_sum,
                            'total_sum' => $userRule->total_sum + $userRule->again_sum
                        ];
                        CashBackRuleUser::where('user_id', $userRule->user_id)->update($userRuleUp);
                        $update['again_done_amt'] = $userRule->again_sum;
                        $update['again_status'] = "1";
                        $update['total_amt'] = $storeTransactionReward->total_amt + $userRule->again_sum;
                    }
                    //上级抽取返现差
                    $user = User::where('id', $store->user_id)->select('pid')->first();
                    $pUserRule = CashBackRuleUser::where('user_id', $user->pid)->select("*")->first();
                    if ($pUserRule) {
                        if ($pUserRule->again_sum > $pUserRule->lower_again_sum) {
                            $amt = $pUserRule->again_sum - $pUserRule->lower_again_sum;
                            $pUserRuleUp = [
                                'lower_again_store_amt' => $pUserRule->lower_again_store_amt + 1,
                                'lower_again_total_sum' => $pUserRule->lower_again_total_sum + $amt,
                                'total_sum' => $pUserRule->total_sum + $amt,
                            ];
                            CashBackRuleUser::where('user_id', $pUserRule->user_id)->update($pUserRuleUp);
                        }
                    }
                }
                StoreTransactionReward::where('store_id', $storeTransactionReward->store_id)->update($update);
            //}
        }
    }

    function add($user_id, $rule_id, $user_name, $cashBackRule)
    {
        try {
            $cashBackRuleUser = CashBackRuleUser::where('user_id', $user_id)
                ->select("*")
                ->first();
            if ($cashBackRuleUser) {
                $updateDate['status'] = "1";
                $updateDate['rule_id'] = $rule_id;
                $updateDate['standard_sum'] = $cashBackRule->standard_amt;
                $updateDate['first_sum'] = $cashBackRule->first_amt;
                $updateDate['again_sum'] = $cashBackRule->again_amt;
                $updateDate['lower_standard_sum'] = $cashBackRule->standard_amt;
                $updateDate['lower_first_sum'] = $cashBackRule->first_amt;
                $updateDate['lower_again_sum'] = $cashBackRule->again_amt;
                $updateDate['updated_at'] = date('Y-m-d h:i:s');
                CashBackRuleUser::where("user_id", $user_id)->update($updateDate);
            } else {
                $inData = array();
                $inData['user_id'] = $user_id;
                $inData['user_name'] = $user_name;
                $inData['rule_id'] = $rule_id;
                $inData['status'] = "1";
                $inData['standard_sum'] = $cashBackRule->standard_amt;
                $inData['first_sum'] = $cashBackRule->first_amt;
                $inData['again_sum'] = $cashBackRule->again_amt;
                $inData['lower_standard_sum'] = $cashBackRule->standard_amt;
                $inData['lower_first_sum'] = $cashBackRule->first_amt;
                $inData['lower_again_sum'] = $cashBackRule->again_amt;
                $inData['created_at'] = date('Y-m-d h:i:s');
                CashBackRuleUser::create($inData);
            }
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    function editUserRule(Request $request)
    {
        try {
            $user_id = $request->get('user_id');
            $rule_id = $request->get('rule_id');
            $standard_sum = $request->get('standard_sum');
            $first_sum = $request->get('first_sum');
            $again_sum = $request->get('again_sum');
            $lower_standard_sum = $request->get('lower_standard_sum');
            $lower_first_sum = $request->get('lower_first_sum');
            $lower_again_sum = $request->get('lower_again_sum');
            $check_data = [
                'rule_id' => '返现规则',
                'user_id' => '代理商id',
                'lower_standard_sum' => '下级代理激活奖励金额',
                'lower_first_sum' => '下级代理首月奖励金额',
                'lower_again_sum' => '下级代理次月奖励金额',
                'standard_sum' => '代理激活奖励金额',
                'first_sum' => '代理首月奖励金额',
                'again_sum' => '代理次月奖励金额'

            ];
            $check = $this->check_required($request->all(), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            if ($lower_standard_sum > $standard_sum) {
                return json_encode([
                    'status' => 2,
                    'message' => '下级代理激活返现金额不得大于本金代理激活返现金额'
                ]);
            }
            if ($lower_first_sum > $first_sum) {
                return json_encode([
                    'status' => 2,
                    'message' => '下级代理首月返现金额不得大于本金代理首月返现金额'
                ]);
            }
            if ($lower_again_sum > $again_sum) {
                return json_encode([
                    'status' => 2,
                    'message' => '下级代理次月返现金额不得大于本金代理次月返现金额'
                ]);
            }
            $where[] = ['is_delete', '=', 0];
            $ids = $this->getSubIds($user_id);
            $obj = DB::table('users')->where($where)->whereIn('id', $ids)->get();
            foreach ($obj as $user) {
                $up = array();
                $up['updated_at'] = date('Y-m-d h:i:s');

                if ($user->id == $user_id) {
                    $up['lower_standard_sum'] = $lower_standard_sum;
                    $up['lower_first_sum'] = $lower_first_sum;
                    $up['lower_again_sum'] = $lower_again_sum;
                } else {
                    $up['standard_sum'] = $lower_standard_sum;
                    $up['first_sum'] = $lower_first_sum;
                    $up['again_sum'] = $lower_again_sum;
                    $up['lower_standard_sum'] = $lower_standard_sum;
                    $up['lower_first_sum'] = $lower_first_sum;
                    $up['lower_again_sum'] = $lower_again_sum;
                }

                CashBackRuleUser::where("user_id", $user->id)->where('rule_id', $rule_id)->update($up);
            }
            return json_encode([
                'status' => 1,
                'message' => '修改成功'
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }
    function userRules(Request $request){
        try {
            $user = $this->parseToken();//
            $user_id = $user->user_id;
            $agent_id = $request->get('agent_id', '');
            $rule_id = $request->get('rule_id', '');
            $status = $request->get('status', '');
            $obj = DB::table('cash_back_rule_user');
            $where = [];
            if($agent_id){
                $where[] = ['cash_back_rule_user.user_id', $agent_id];
            }
            if($rule_id){
                $where[] = ['cash_back_rule_user.rule_id', $rule_id];
            }

            if($status!=""){
                $where[] = ['cash_back_rule_user.status', $status];
            }
            $ids = $this->getSubIds($user_id);
            $obj = $obj->where($where)
                ->leftJoin('cash_back_rule', 'cash_back_rule_user.rule_id', '=', 'cash_back_rule.id')
                ->whereIn('cash_back_rule_user.user_id', $ids)
                ->orderBy('cash_back_rule_user.created_at', 'desc')
                ->select('cash_back_rule_user.*','cash_back_rule.rule_name');
            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        }catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

}