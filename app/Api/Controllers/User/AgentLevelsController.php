<?php


namespace App\Api\Controllers\User;


use App\Api\Controllers\BaseController;
use App\Models\AgentLevels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class AgentLevelsController extends BaseController
{

    public function agent_level_lists(Request $request)
    {
        try {

            $obj = DB::table('agent_levels');
            $obj->get();
            $this->t = $obj->count();
            $data = $this->page($obj)->get();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $ex) {
            Log::info('error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

    }

    public function add_agent_level(Request $request)
    {
        try {
            $level = $request->get('level');//等级
            $rate = $request->get('rate');//分润
            $start_transaction = $request->get('start_transaction');//交易开始金额
            $end_transaction = $request->get('end_transaction');//交易结束金额
            $code_num = $request->get('code_num');//所需码牌数量
            $sid = $request->get('sid');//所需等级
            $sid_num = $request->get('sid_num');//所需等级数量
            $activation_amount = $request->get('activation_amount');//奖励金额
            $reward_month = $request->get('reward_month');
            $level_weight = $request->get('level_weight');
            $activation_integral = $request->get('activation_integral');
            $agentLevels = AgentLevels::where('level', $level)->first();
            if ($agentLevels) {
                return json_encode([
                    'status' => -1,
                    'message' => '等级已存在'
                ]);
            }
            $in_data = [
                'level' => $level,
                'level_weight' => $level_weight,
                'rate' => $rate,
                'start_transaction' => $start_transaction,
                'end_transaction' => $end_transaction,
                'code_num' => $code_num,
                'sid' => $sid,
                'sid_num' => $sid_num,
                'activation_amount' => $activation_amount,
                'reward_month' => $reward_month,
                'activation_integral' => $activation_integral
            ];
            AgentLevels::create($in_data);
            return json_encode([
                'status' => 1,
                'message' => '新增成功',
            ]);
        } catch (\Exception $ex) {
            Log::info('error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

    }

    //修改代理商等级
    public function update_agent_level(Request $request)
    {
        try {
            $id = $request->get('id');
            $rate = $request->get('rate');//分润
            $start_transaction = $request->get('start_transaction');//交易开始金额
            $end_transaction = $request->get('end_transaction');//交易结束金额
            $code_num = $request->get('code_num');//所需码牌数量
            $sid = $request->get('sid');//所需等级
            $sid_num = $request->get('sid_num');//所需等级数量
            $activation_amount = $request->get('activation_amount');//奖励金额
            $reward_month = $request->get('reward_month');
            $activation_integral = $request->get('activation_integral');
            $up_data = [
                'rate' => $rate,
                'start_transaction' => $start_transaction,
                'end_transaction' => $end_transaction,
                'code_num' => $code_num,
                'sid' => $sid,
                'sid_num' => $sid_num,
                'activation_amount' => $activation_amount,
                'reward_month' => $reward_month,
                'activation_integral' => $activation_integral
            ];
            AgentLevels::where('id', $id)->update($up_data);
            return json_encode([
                'status' => 1,
                'message' => '修改成功',
            ]);
        } catch (\Exception $ex) {
            Log::info('error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

    }


}