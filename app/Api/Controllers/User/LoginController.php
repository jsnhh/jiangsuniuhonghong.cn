<?php

namespace App\Api\Controllers\User;


use App\Api\Controllers\ApiController;

use App\Models\ModelHasRole;
use App\Models\Role;
use App\Models\User;
use App\Models\UserLevels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class LoginController extends ApiController
{

    public $successStatus = 200;


    public function login(Request $request)
    {
        // 这里的登陆验证是email和password
        $credentials = $request->only('phone', 'password');
        $wx_openid = $request->get('wx_openid', '');
        $permission = $request->get('permission', 0);

        //微信登录
        if ($wx_openid) {
            $user = User::where('wx_openid', $wx_openid)->first();
            if (!$user) {
                return json_encode([
                    'status' => '-1',
                    'message' => '你的微信号没有绑定账户'
                ]);
            }

            if ($user->is_delete) {
                return json_encode([
                    'status' => '-1',
                    'message' => '账户已经删除'
                ]);
            }

            $token = JWTAuth::fromUser($user);//根据用户得到token
            $return_data = [
                'status' => '1',
                'message' => '登录成功',
                'data' => [
                    'token' => $token,
                    'user_id' => $user->id,
                    'user_name' => $user->name,
                    'name' => $user->name,
                    'phone' => $user->phone,
                    'pid' => $user->pid,
                    'level' => $user->level,
                    's_code' => $user->s_code,
                    'config_id' => $user->config_id,
                    'source' => $user->source
                ]
            ];

            //返回权限集合
            if ($permission) {
                $permissions = $user->getAllPermissions();
                $data = [];
                foreach ($permissions as $k => $v) {
                    $data[$k]['name'] = $v->name;
                }
                $return_data['permissions'] = $data;
            }

            return json_encode($return_data);
        }

        try {
            //尝试验证凭据并为用户创建令牌
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'status' => '-1',
                    'message' => '账号密码不正确！'
                ]);
            }
        } catch (JWTException $e) {
            //尝试对令牌进行编码时出错
            return response()->json([
                'status' => '-1',
                'message' => $e->getMessage()
            ]);
        }

        $user = User::where('phone', $request->get('phone'))->first();
        if ($user->is_delete) {
            return json_encode([
                'status' => '-1',
                'message' => '账户暂时无法登录'
            ]);
        }

        $return_data = [
            'status' => '1',
            'message' => '登录成功',
            'data' => [
                'token' => $token,
                'user_id' => $user->id,
                'user_name' => $user->name,
                'name' => $user->name,
                'phone' => $user->phone,
                'pid' => $user->pid,
                'level' => $user->level,
                's_code' => $user->s_code,
                'config_id' => $user->config_id,
                'source' => $user->source,
                'invite_no' => $user->invite_no,
                'invite_code' => $user->invite_code,
                'auth_status' => $user->auth_status,
                'userType' => 'user',
                'likeness_url' => $user->likeness_url
            ]
        ];

        //返回权限集合
        if ($permission) {
            $permissions = $user->getAllPermissions();
            $data = [];
            foreach ($permissions as $k => $v) {
                $data[$k]['name'] = $v->name;
            }
            $return_data['permissions'] = $data;
        }
        return json_encode($return_data);
    }


    //注册
    public function register_user(Request $request)
    {
        try {
            DB::beginTransaction();

            $userName = $request->get('userName'); //公司名称
            $phone = $request->get('phone');
            $password = $request->get('password');
            $recode = $request->get('recode');
            $relationPhone = $request->get('relationPhone'); //邀请人手机号

            $validator = \Validator::make($request->except(['token']), [
                'userName' => 'required',
                'phone' => 'required',
                'password' => 'required',
                'recode' => 'required',
            ], [
                'required' => ':attribute参数为必填项',
            ], [
                'userName' => '姓名',
                'phone' => '手机号',
                'password' => '密码',
                'recode' => '验证码',
            ]);

            if ($validator->fails()) {
                return ['status' => 2, 'message' => $validator->getMessageBag()->first()];
            }
            $config_id = "1234";
            $pid_user_id = '1';
            $pid_user_name = '江苏牛付电子商务有限公司';
            $invite_no = '';
            if (isset($relationPhone)) {

                $rel_user = User::where('phone', $relationPhone)->first();

                if ($rel_user) {
                    $invite_no = $rel_user->invite_no;
                    $pid_user_id = $rel_user->id;
                    $pid_user_name = $rel_user->name;
                }
            }

            $dataIn['config_id'] = $config_id;
            $dataIn['pid'] = $pid_user_id;
            $dataIn['pid_name'] = $pid_user_name;
            $dataIn['invite_no'] = $this->createCode($phone);; //自己的邀请码
            $dataIn['invite_code'] = $invite_no; //被邀请人邀请码
            $dataIn['level'] = 1;
            $dataIn['level_name'] = '服务商';
            $dataIn['email'] = $phone . '@139.com';
            $dataIn['password'] = bcrypt($password);
            $dataIn['name'] = $userName;
            $dataIn['phone'] = $phone;
            $dataIn['s_code'] = rand(1000, 9999);

            $User = User::create($dataIn);

            //自动分配权限(默认 服务商)
            $role_name = "agent";

            $roles = Role::where('created_id', 1)->where('name', $role_name)->first();

            ModelHasRole::create([
                'role_id' => $roles->id,
                'model_type' => 'App\Models\User',
                'model_id' => $User->id,
            ]);

            UserLevels::create([
                'user_id' => $User->id,
                'level' => 1,  //用户级别信息
            ]);

            DB::commit();

            return json_encode([
                'status' => 1,
                'message' => '注册成功',
            ]);


        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile()
            ]);
        }
    }

    /**
     * 生成邀请码
     *
     * @param unknown $user_id
     * @return string
     */
    public function createCode($user_id)
    {
        $source_string = "E5FCDG3HQA4B1NOPIJ2RSTUV67MWX89KLYZ";
        $num = $user_id;
        $code = '';

        while ($num > 0) {
            $mod = $num % 35;
            $num = ($num - $mod) / 35;
            $code = $source_string[$mod] . $code;
        }
        if (empty($code[3]))
            $code = str_pad($code, 4, '0', STR_PAD_LEFT);
        return $code;
    }

    public function username()
    {
        return 'phone';
    }


    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        //令牌有效，我们已通过子声明找到用户
        return response()->json(compact('user'));
    }


    //PC端忘记密码
    public function edit_password1(Request $request)
    {
        try {
            $phone = $request->get('phone');
            $password = $request->get('newpassword');
            $code = $request->get('code');

            //有密码的话修改密码
            if ($password && $phone && $code) {
                //验证手机号
                if (!preg_match("/^1[3456789]{1}\d{9}$/", $phone)) {
                    return json_encode([
                        'status' => '2',
                        'message' => '手机号码不正确'
                    ]);
                }

                //验证密码
                if (strlen($password) < 6) {
                    return json_encode([
                        'status' => '2',
                        'message' => '密码长度不符合要求'
                    ]);
                }

                $users = User::where('phone', $phone)->first();
                if (!$users) {
                    return json_encode([
                        'status' => '2',
                        'message' => '此手机号码未注册账号'
                    ]);
                }

                //验证验证码
                $msn_local = Cache::get($phone . 'editpassword-1');
                if ((string)$code != (string)$msn_local) {
                    return json_encode([
                        'status' => '2',
                        'message' => '短信验证码不匹配'
                    ]);
                }

                User::where('phone', $phone)->update([
                    'password' => bcrypt($password)
                ]);

                return json_encode([
                    'status' => '1',
                    'message' => '密码修改成功'
                ]);
            }

            return json_encode([
                'status' => '2',
                'message' => '参数不正确'
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => '-1',
                'message' => $exception->getMessage()
            ]);
        }
    }


    //edit Password
    public function edit_password(Request $request)
    {
        try {
            $phone = $request->get('phone', '');
            $password = $request->get('newpassword', '');
            $password = $request->get('new_password', '' . $password . '');
            $code = $request->get('code', '');

            //验证参数不能为空
            if ($phone == "" && $password == "" && $code == "") {
                return json_encode([
                    'status' => '2',
                    'message' => '参数必须有一项填写'
                ]);
            }

            //验证验证码
            if ($phone && $password == "" && $code) {
                //验证验证码
                $msn_local = Cache::get($phone . 'editpassword-1');
                if ((string)$code != (string)$msn_local) {
                    return json_encode([
                        'status' => '2',
                        'message' => '短信验证码不匹配'
                    ]);
                } else {
                    return json_encode([
                        'status' => '1',
                        'message' => '短信验证码匹配'
                    ]);
                }
            }

            //有密码的话修改密码
            if ($password && $phone && $code) {
                //验证手机号
                if (!preg_match("/^1[3456789]{1}\d{9}$/", $phone)) {
                    return json_encode([
                        'status' => '2',
                        'message' => '手机号码不正确'
                    ]);
                }
                //验证密码
                if (strlen($password) < 6) {
                    return json_encode([
                        'status' => '2',
                        'message' => '密码长度不符合要求'
                    ]);
                }

                $merchant = User::where('phone', $phone)->first();

                if (!$merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '此手机号码未注册账号'
                    ]);
                }
                //验证验证码
                $msn_local = Cache::get($phone . 'editpassword-1');

                if ((string)$code != (string)$msn_local) {
                    return json_encode([
                        'status' => '2',
                        'message' => '短信验证码不匹配'
                    ]);
                }


                User::where('phone', $phone)->update(['password' => bcrypt($password)]);
                $token = JWTAuth::fromUser($merchant); //根据用户得到token

                return json_encode([
                    'status' => '1',
                    'message' => '密码修改成功',
                    'data' => [
                        'token' => $token
                    ]
                ]);
            }

            return json_encode([
                'status' => '2',
                'message' => '参数填写不正确'
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => '-1',
                'message' => $exception->getMessage()
            ]);
        }

    }


}
