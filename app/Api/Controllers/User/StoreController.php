<?php

namespace App\Api\Controllers\User;

use Aliyun\AliSms;
use Alipayopen\Sdk\AopClient;
use Alipayopen\Sdk\Request\AntMerchantExpandOrderQueryRequest;
use App\Api\Controllers\BaseController;
use App\Api\Controllers\Basequery\StorePayWaysController;
use App\Api\Controllers\Config\AlipayIsvConfigController;
use App\Api\Controllers\Config\EasyPayConfigController;
use App\Api\Controllers\Config\HkrtConfigController;
use App\Api\Controllers\Config\HwcPayConfigController;
use App\Api\Controllers\Config\JdConfigController;
use App\Api\Controllers\Config\LinkageConfigController;
use App\Api\Controllers\Config\NewLandConfigController;
use App\Api\Controllers\Config\QfPayConfigController;
use App\Api\Controllers\Config\TfConfigController;
use App\Api\Controllers\Config\VbillConfigController;
use App\Api\Controllers\Config\WeixinConfigController;
use App\Api\Controllers\Config\WftPayConfigController;
use App\Api\Controllers\Config\YinshengConfigController;
use App\Api\Controllers\EasyPay\MerAccessController;
use App\Api\Controllers\Hltx\ManageController;
use App\Api\Controllers\MyBank\MyBankController;
use App\Api\Controllers\Newland\UpdateController;
use App\Api\Controllers\Vbill\PayController;
use App\Api\Controllers\Weixin\PayController as WeixinPayController;
use App\Models\AgentLevelsInfos;
use App\Models\AlipayAppOauthUsers;
use App\Models\AlipayZftStore;
use App\Models\ChangshaStore;
use App\Models\CcBankPayStore;
use App\Models\DadaMerchant;
use App\Models\DadaStore;
use App\Models\Device;
use App\Models\DlbStore;
use App\Models\EasePayStore;
use App\Models\EasyPayContractInfo;
use App\Models\EasypayStore;
use App\Models\EasypayStoreConfig;
use App\Models\EasypayStoresImages;
use App\Models\EasySkpayStore;
use App\Models\EsignAuths;
use App\Models\FuiouStore;
use App\Models\HltxStore;
use App\Models\HkrtBusinessScope;
use App\Models\HkrtStore;
use App\Models\HnaPayStore;
use App\Models\HStore;
use App\Models\HwcpayStore;
use App\Models\JdStore;
use App\Models\LianfuStores;
use App\Models\LianfuyoupayStores;
use App\Models\LinkageStores;
use App\Models\LklStore;
use App\Models\LtfStore;
use App\Models\Merchant;
use App\Models\MerchantStore;
use App\Models\MyBankStore;
use App\Models\NewLandStore;
use App\Models\Order;
use App\Models\PostPayStore;
use App\Models\ProvinceCity;
use App\Models\QfpayStore;
use App\Models\QrListInfo;
use App\Models\SmsConfig;
use App\Models\Store;
use App\Models\StoreBank;
use App\Models\StoreImg;
use App\Models\StorePayWay;
use App\Models\StoreSteps;
use App\Models\SuzhouStore;
use App\Models\TfStore;
use App\Models\User;
use App\Models\UserAuths;
use App\Models\UserMonthLevel;
use App\Models\UserRate;
use App\Models\VbillaStore;
use App\Models\VbillStore;
use App\Models\VbillSyncRate;
use App\Models\VoiceStatistic;
use App\Models\WeixinaStore;
use App\Models\WeixinStore;
use App\Models\WftpayStore;
use App\Models\YinshengStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;

class StoreController extends BaseController
{
    /**
     * 门店列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store_lists(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id', $user->user_id);
            $time_start = $request->get('time_start', '');//关键词
            $time_end = $request->get('time_end', '');//关键词
            $store_name = $request->get('store_name', '');//关键词
            $province_code = $request->get('province_code');//状态
            $city_code = $request->get('city_code');//状态
            $area_code = $request->get('area_code');//状态
            $status = $request->get('status');//状态
            $pid = $request->get('pid', 0);//主店ID
            $is_delete = $request->get('is_delete', 0);//
            $is_close = $request->get('is_close', 0);//
            $where = [];

            $where[] = ['stores.pid', '=', $pid];
            $where[] = ['stores.is_delete', '=', $is_delete];
            $where[] = ['stores.is_close', '=', $is_close];

            if ($status) {
                $where[] = ['stores.status', '=', $status];
            }
            if ($province_code) {
                $where[] = ['stores.province_code', '=', $province_code];
            }
            if ($city_code) {
                $where[] = ['stores.city_code', '=', $city_code];
            }
            if ($area_code) {
                $where[] = ['stores.area_code', '=', $area_code];
            }
            if ($time_start) {
                $time_start = date('Y-m-d 00:00:00', strtotime($time_start));
                $where[] = ['stores.created_at', '>=', $time_start];
            }
            if ($time_end) {
                $time_end = date('Y-m-d 23:59:59', strtotime($time_end));
                $where[] = ['stores.created_at', '<=', $time_end];
            }
            if ($user_id == 1) {
                $ids = $this->getSubIdsAll($user_id);
            } else {
                $ids = $this->getSubIds($user_id);
            }
            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("stores");
            } else {
                $obj = DB::table('stores');
            }
            $obj = $obj->join('users', 'stores.user_id', 'users.id')
                ->leftjoin('merchants', 'merchants.id', 'stores.merchant_id');

            if ($store_name) {
                if (is_numeric($store_name)) {
                    $where1[] = ['stores.store_id', 'like', '%' . $store_name . '%'];
                } else {
                    $where1[] = ['stores.store_name', 'like', '%' . $store_name . '%'];
                }
                $obj->where($where1)
                    ->whereIn('stores.user_id', $ids)
                    ->select('stores.*', 'users.name as user_name', 'merchants.merchant_no')
                    ->orderBy('stores.updated_at', 'desc')
                    ->get();
            } else {
                $obj->where($where)
                    ->whereIn('stores.user_id', $ids)
                    ->select('stores.*', 'users.name as user_name', 'merchants.merchant_no')
                    ->orderBy('stores.updated_at', 'desc')
                    ->get();
            }

            $this->t = $obj->count();
            $data = $this->page($obj)->get();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    /**
     * 门店列表（新）
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store_lists_new(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id', $user->user_id);
            $time_start = $request->get('time_start', '');//关键词
            $time_end = $request->get('time_end', '');//关键词
            $store_name = $request->get('store_name', '');//关键词
            $province_code = $request->get('province_code');//状态
            $city_code = $request->get('city_code');//状态
            $area_code = $request->get('area_code');//状态
            $status = $request->get('status');//状态
            $pid = $request->get('pid', 0);//主店ID
            $is_delete = $request->get('is_delete', 0);//
            $is_close = $request->get('is_close', 0);//

            $where = [];

            $where[] = ['stores.pid', '=', $pid];
            $where[] = ['stores.is_delete', '=', $is_delete];
            $where[] = ['stores.is_close', '=', $is_close];

            if ($status) {
                $where[] = ['store_pay_ways.status', '=', $status];
            }
            if ($province_code) {
                $where[] = ['stores.province_code', '=', $province_code];
            }
            if ($city_code) {
                $where[] = ['stores.city_code', '=', $city_code];
            }
            if ($area_code) {
                $where[] = ['stores.area_code', '=', $area_code];
            }
            if ($time_start) {
                $time_start = date('Y-m-d 00:00:00', strtotime($time_start));
                $where[] = ['stores.created_at', '>=', $time_start];
            }
            if ($time_end) {
                $time_end = date('Y-m-d 23:59:59', strtotime($time_end));
                $where[] = ['stores.created_at', '<=', $time_end];
            }

            $obj = new StorePayWay();
            $obj = $obj->leftjoin('stores', 'stores.store_id', 'store_pay_ways.store_id')
                ->leftjoin('users', 'users.id', 'stores.user_id')
                ->leftjoin('merchants', 'merchants.id', 'stores.merchant_id');


            if ($store_name) {
                if (is_numeric($store_name)) {
                    $where1[] = ['stores.store_id', 'like', '%' . $store_name . '%'];
                } else {
                    $where1[] = ['stores.store_name', 'like', '%' . $store_name . '%'];
                }
                $obj->where($where1)
                    ->whereIn('stores.user_id', $this->getSubIdsAll($user_id))
                    ->with('store_pay_ways')
                    ->select('stores.id', 'stores.store_id', 'stores.merchant_id', 'stores.store_name', 'stores.people', 'stores.people_phone', 'users.name as user_name', 'merchants.merchant_no')
                    ->groupBy('stores.store_id')
                    ->orderBy('stores.updated_at', 'desc');
            } else {
                $obj->where($where)
                    ->whereIn('stores.user_id', $this->getSubIdsAll($user_id))
                    ->with('store_pay_ways')
                    ->select('stores.id', 'stores.store_id', 'stores.merchant_id', 'stores.store_name', 'stores.people', 'stores.people_phone', 'users.name as user_name', 'merchants.merchant_no')
                    ->groupBy('stores.store_id')
                    ->orderBy('stores.updated_at', 'desc');
            }

            $this->t = $obj->count();
            $data = $this->page($obj)->get();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }

    /**
     * 根据审核状态获取门店列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeListByStatus(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_id = $token->user_id;

            $store_name = $request->get('store_name', '');//关键词
            $status = $request->get('status');//状态

            $where = [];

            $where[] = ['stores.is_delete', '=', 0];
            $where[] = ['stores.is_close', '=', 0];

            if ($store_name) {
                if (is_numeric($store_name)) {
                    $where[] = ['stores.store_id', 'like', '%' . $store_name . '%'];
                } else {
                    $where[] = ['stores.store_name', 'like', '%' . $store_name . '%'];
                }
            }

            if ($status == 1) { //未提交
                //  $obj = DB::table('stores')->
                $obj = new Store();
                $obj = $obj->select([
                    'stores.user_id',
                    'stores.store_id',
                    'stores.store_name',
                    'stores.people',
                    'stores.people_phone',
                    'stores.store_type',
                    'stores.store_type_name',
                    'stores.created_at',
                    'stores.updated_at',
                    'merchants.merchant_no',
                    DB::raw("group_concat(distinct(store_pay_ways.company)) as company")
//                    DB::raw("if(store_pay_ways.company is null,'easypay', store_pay_ways.company) as company"),
//                    DB::raw("if(store_pay_ways.status_desc is null,'未提交', store_pay_ways.status_desc) as status_desc"),
//                    DB::raw("if(store_pay_ways.status is null,'0', store_pay_ways.status) as status")
                ])
//                    ->join('store_pay_ways', function ($join) {
//                        $join->on('store_pay_ways.store_id', '=', 'stores.store_id')
//                            ->where('store_pay_ways.company', '=', 'easypay');}, null,null,'left')
                    ->leftjoin('store_pay_ways', 'store_pay_ways.store_id', '=', 'stores.store_id')
                    ->leftjoin('merchants', 'merchants.id', '=', 'stores.merchant_id')
                    ->whereIn('stores.store_id', $this->getUserStoreIds($user_id))
                    ->where($where)
                    ->whereRaw('(store_pay_ways.status = 0  OR store_pay_ways.status IS NULL)')
                    ->with('store_pay_ways')
                    ->groupBy('stores.store_id')
                    ->orderBy('stores.updated_at', 'desc');
            } else if ($status == 2) {     //处理中

                // $obj = DB::table('stores')->select('stores.store_id','stores.store_name','stores.people','stores.people_phone','stores.store_type','stores.store_type_name','stores.created_at','stores.updated_at','store_pay_ways.company','store_pay_ways.status_desc','store_pay_ways.status')
                $obj = new Store();
                $obj = $obj->select('stores.user_id', 'stores.store_id', 'stores.store_name', 'stores.people', 'stores.people_phone', 'stores.store_type', 'stores.store_type_name', 'stores.created_at', 'stores.updated_at', 'merchants.merchant_no')
                    ->leftjoin('store_pay_ways', 'stores.store_id', '=', 'store_pay_ways.store_id')
                    ->leftjoin('merchants', 'merchants.id', '=', 'stores.merchant_id')
                    ->whereIn('stores.store_id', $this->getUserStoreIds($user_id))
                    ->where($where)
                    ->whereIn('store_pay_ways.status', ['2', '3'])
                    ->with('store_pay_ways')
                    ->groupBy('stores.store_id')
                    ->orderBy('stores.updated_at', 'desc');
            } else if ($status == 3) {     //已完成
                // $obj = DB::table('stores')->select('stores.store_id','stores.store_name','stores.people','stores.people_phone','stores.store_type','stores.store_type_name','stores.created_at','stores.updated_at','store_pay_ways.company','store_pay_ways.status_desc','store_pay_ways.status')
                $obj = new Store();
                $obj = $obj->select('stores.user_id', 'stores.store_id', 'stores.store_name', 'stores.people', 'stores.people_phone', 'stores.store_type', 'stores.store_type_name', 'stores.created_at', 'stores.updated_at', 'merchants.merchant_no')
                    ->leftjoin('store_pay_ways', 'stores.store_id', '=', 'store_pay_ways.store_id')
                    ->leftjoin('merchants', 'merchants.id', '=', 'stores.merchant_id')
                    ->whereIn('stores.store_id', $this->getUserStoreIds($user_id))
                    ->where($where)
                    ->where('store_pay_ways.status', '1')
                    ->with('store_pay_ways')
                    ->groupBy('stores.store_id')
                    ->orderBy('stores.updated_at', 'desc');
            }

            $this->t = $obj->count();
            $data = $this->page($obj)->get();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }

    /**
     * 列表 根据门店ID 获取所有门店名称
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store_all_lists(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');//
            $id = $request->get('id', '');//
            $l = $request->get('l', '50');


            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("stores");
            } else {
                $obj = DB::table('stores');
            }

            $obj->orWhere('id', $id)
                ->orWhere('pid', $id)
                ->select('store_id', 'store_short_name', 'zero_rate_type');

            $this->t = $obj->count();
            $data = $this->page($obj)->get();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    /**
     * 列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store_pc_lists(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id', $user->user_id);
            $time_start = $request->get('time_start', '');//关键词
            $time_end = $request->get('time_end', '');//关键词
            $store_name = $request->get('store_name', '');//关键词
            $province_code = $request->get('province_code');//状态
            $city_code = $request->get('city_code');//状态
            $area_code = $request->get('area_code');//状态
            $status = $request->get('status');//状态
            $is_close = $request->get('is_close', 0);//
            $is_delete = $request->get('is_delete', 0);//0 未删除， 1已删除
            $merchant_no = $request->get('merchant_no');//商户号
            $where = [];

            $where[] = ['stores.is_delete', '=', $is_delete];
            if ($is_close) {
                if ($is_close == '2') {
                    $where[] = ['stores.is_close', '=', 0];
                } else {
                    $where[] = ['stores.is_close', '=', $is_close];
                }
            }
            if ($status) {
                $where[] = ['stores.status', '=', $status];
            }
            if ($province_code) {
                $where[] = ['stores.province_code', '=', $province_code];
            }
            if ($city_code) {
                $where[] = ['stores.city_code', '=', $city_code];
            }
            if ($area_code) {
                $where[] = ['stores.area_code', '=', $area_code];
            }

            if (!$time_start) {
                $time_start = date('Y-m-d 00:00:00', time());
            }
            if (!$time_end) {
                $time_end = date('Y-m-d 23:59:59', time());
            }
            if ($time_start) {
                $where[] = ['stores.created_at', '>=', $time_start];
            }
            if ($time_end) {
                $where[] = ['stores.created_at', '<=', $time_end];
            }
            if ($store_name) {
                if (is_numeric($store_name)) {
                    $where[] = ['stores.store_id', 'like', '%' . $store_name . '%'];
                } else {
                    $where[] = ['stores.store_name', 'like', '%' . $store_name . '%'];
                }
            }
            if ($merchant_no) {
                $where[] = ['merchants.merchant_no', '=', $merchant_no];
            }
            if (!$merchant_no || !$store_name || !$status || !$is_close) {

            }
            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("stores");
            } else {
                $obj = DB::table('stores');
            }

            $obj = $obj->join('users', 'stores.user_id', 'users.id');
//            $obj = $obj->join('store_banks', 'stores.store_id', 'store_banks.store_id');
            $obj = $obj->join('merchants', 'stores.merchant_id', 'merchants.id');

            if ($store_name) {
                if (is_numeric($store_name)) {
                    $where1[] = ['stores.store_id', 'like', '%' . $store_name . '%'];
                } else {
                    $where1[] = ['stores.store_name', 'like', '%' . $store_name . '%'];
                }

                $obj->where($where1)
                    ->where($where)
                    ->whereIn('stores.user_id', $this->getSubIdsAll($user_id))
                    ->orderBy('stores.created_at', 'desc')
                    //->select('stores.*', 'users.name as user_name', 'store_banks.bank_sfz_no', 'store_banks.store_bank_name', 'merchants.merchant_no')
                    ->select('stores.*', 'users.name as user_name', 'merchants.merchant_no')
                    ->get();
            } else {
                $obj->where($where)
                    ->whereIn('stores.user_id', $this->getSubIdsAll($user_id))
                    ->orderBy('stores.created_at', 'desc')
                    //->select('stores.*', 'users.name as user_name', 'store_banks.bank_sfz_no', 'store_banks.store_bank_name', 'merchants.merchant_no')
                    ->select('stores.*', 'users.name as user_name', 'merchants.merchant_no')
                    ->get();
            }

            $this->t = $obj->count();
            $data = $this->page($obj)->get();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            Log::info($exception);
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    /**
     * 门店信息查看
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $store = Store::where('store_id', $store_id)->first();
            $store_bank = StoreBank::where('store_id', $store_id)->first();
            $store_img = StoreImg::where('store_id', $store_id)->first();
            $merchantInfo = Merchant::where('id', $store->merchant_id)->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店未认证'
                ]);
            }

            $store_alipay_account = '';
            $AlipayAppOauthUsers = AlipayAppOauthUsers::where('store_id', $store_id)->first();
            if ($AlipayAppOauthUsers) {
                $store_alipay_account = $AlipayAppOauthUsers->alipay_user_account;
            }

            $easyPayStoreObj = EasypayStoresImages::where('store_id', $store_id)->first();

            $type = $request->get('type');
            if ($type == "") {
                if ($easyPayStoreObj) {
                    $type = 'head_info,store_info,account_info,license_info,special_info,easypay_info';
                } else {
                    $type = 'head_info,store_info,account_info,license_info,special_info';
                }
            }
            $type_array = explode(",", $type);
            foreach ($type_array as $k => $v) {
                if ($v == 'head_info') {
                    $data = [
                        'head_name' => $store->head_name,//法人
                        'head_sfz_no' => $store->head_sfz_no,
                        'head_sfz_img_a' => $store_img->head_sfz_img_a,
                        'head_sfz_img_b' => $store_img->head_sfz_img_b,
                        'people' => $store->people,
                        'people_phone' => $store->people_phone,
                        'head_sfz_time' => $store->head_sfz_time,
                        'head_sfz_stime' => $store->head_sfz_stime,
                    ];

                    $data_r[$v] = $data;
                }

                if ($v == 'store_info') {
                    $data = [
                        'id' => $store->id,
                        'people' => $store->people,
                        'people_phone' => $store->people_phone,
                        'store_email' => $store->store_email,
                        'store_name' => $store->store_name,
                        'store_short_name' => $store->store_short_name,
                        'province_code' => $store->province_code,
                        'city_code' => $store->city_code,
                        'area_code' => $store->area_code,
                        'province_name' => $store->province_name,
                        'city_name' => $store->city_name,
                        'area_name' => $store->area_name,
                        'store_address' => $store->store_address,
                        'store_type' => $store->store_type,
                        'store_type_name' => $store->store_type_name,
                        'category_id' => $store->category_id,
                        'category_name' => $store->category_name,
                        'store_logo_img' => $store_img->store_logo_img,
                        'store_img_a' => $store_img->store_img_a,
                        'store_img_b' => $store_img->store_img_b,
                        'store_img_c' => $store_img->store_img_c,
                        'status' => $store->status,
                        'status_desc' => $store->status_desc,
                        'weixin_name' => $store->weixin_name,
                        'weixin_no' => $store->weixin_no,
                        'alipay_name' => $store->alipay_name,
                        'alipay_account' => $store->alipay_account,
                        'source' => $store->source,
                        'source_type' => $store->source_type,
                        'merchant_no' => $merchantInfo->merchant_no,
                        'login_num' => $merchantInfo->login_num,

                    ];

                    $data_r[$v] = $data;
                }

                if ($v == 'account_info') {
                    $data = [
                        'store_alipay_account' => $store_alipay_account,
                        'reserved_mobile' => $store_bank->reserved_mobile,
                        'store_bank_no' => $store_bank->store_bank_no,
                        'store_bank_phone' => $store_bank->store_bank_phone,
                        'store_bank_name' => $store_bank->store_bank_name,
                        'store_bank_type' => $store_bank->store_bank_type,
                        'bank_name' => $store_bank->bank_name,
                        'bank_no' => $store_bank->bank_no,
                        'sub_bank_name' => $store_bank->sub_bank_name,
                        'bank_province_code' => $store_bank->bank_province_code,
                        'bank_city_code' => $store_bank->bank_city_code,
                        'bank_area_code' => $store_bank->bank_area_code,
                        'bank_province_name' => $this->city_name($store_bank->bank_province_code),
                        'bank_city_name' => $this->city_name($store_bank->bank_city_code),
                        'bank_area_name' => $this->city_name($store_bank->bank_area_code),
                        'bank_img_a' => $store_img->bank_img_a,
                        'bank_img_b' => $store_img->bank_img_b,
                        'bank_sfz_img_a' => $store_img->bank_sfz_img_a,
                        'bank_sfz_img_b' => $store_img->bank_sfz_img_b,
                        'bank_sc_img' => $store_img->bank_sc_img,
                        'store_auth_bank_img' => $store_img->store_auth_bank_img,
                        'bank_sfz_time' => $store_bank->bank_sfz_time,
                        'bank_sfz_stime' => $store_bank->bank_sfz_stime,
                        'bank_sfz_no' => $store_bank->bank_sfz_no,
                        'weixin_name' => $store->weixin_name,
                        'weixin_no' => $store->weixin_no,
                        'alipay_name' => $store->alipay_name,
                        'alipay_account' => $store->alipay_account,
                        'store_email' => $store->store_email,

                    ];

                    $data_r[$v] = $data;
                }

                if ($v == 'license_info') {
                    $data = [
                        'store_license_no' => $store->store_license_no,
                        'store_license_time' => $store->store_license_time,
                        'store_license_stime' => $store->store_license_stime,
                        'store_license_img' => $store_img->store_license_img,
                        'store_other_img_a' => $store_img->store_other_img_a,
                        'store_other_img_b' => $store_img->store_other_img_b,
                        'store_other_img_c' => $store_img->store_other_img_c,
                        'store_industrylicense_img' => $store_img->store_industrylicense_img,
                        'head_sc_img' => $store_img->head_sc_img,
                        'head_store_img' => $store_img->head_store_img,
                    ];

                    $data_r[$v] = $data;
                }

                if ($v == 'special_info') {
                    $data = [
                        'food_service_lic' => $store_img->food_service_lic,
                        'food_health_lic' => $store_img->food_health_lic,
                        'food_business_lic' => $store_img->food_business_lic,
                        'food_circulate_lic' => $store_img->food_circulate_lic,
                        'food_production_lic' => $store_img->food_production_lic,
                        'tobacco_img' => $store_img->tobacco_img,
                    ];

                    $data_r[$v] = $data;
                }

                if ($v == 'easypay_info') {
                    if ($easyPayStoreObj) {
                        $data = [
                            'mcc_id' => $easyPayStoreObj->mcc_id,
                            'standard_flag' => $easyPayStoreObj->standard_flag,
                            'employee_num' => $easyPayStoreObj->employee_num,
                            'busin_form' => $easyPayStoreObj->busin_form,
                            'busin_beg_time' => $easyPayStoreObj->busin_beg_time,
                            'busin_end_time' => $easyPayStoreObj->busin_end_time,
                            'term_mode' => $easyPayStoreObj->term_mode,
                            'term_code' => $easyPayStoreObj->term_code,
                            'ter_user_name' => $easyPayStoreObj->ter_user_name,
                            'area_no' => $easyPayStoreObj->area_no,
                            'term_area' => $easyPayStoreObj->term_area,
                            'term_model' => $easyPayStoreObj->term_model,
                            'term_model_lic' => $easyPayStoreObj->term_model_lic,
                            'bang_ding_commer_code' => $easyPayStoreObj->bang_ding_commer_code,
                            'bang_ding_ter_mno' => $easyPayStoreObj->bang_ding_ter_mno,
                            'busin_scope' => $easyPayStoreObj->busin_scope,
                            'capital' => $easyPayStoreObj->capital,
                            'bank_name' => $easyPayStoreObj->bank_name,
                            'bank_code' => $easyPayStoreObj->bank_code,
                            'real_time_entry' => $easyPayStoreObj->real_time_entry,
                            'mer_area' => $easyPayStoreObj->mer_area,
                            'd_stlm_type' => $easyPayStoreObj->d_stlm_type,
                            'd_calc_val' => $easyPayStoreObj->d_calc_val,
                            'd_stlm_max_amt' => $easyPayStoreObj->d_stlm_max_amt,
                            'd_fee_low_limit' => $easyPayStoreObj->d_fee_low_limit,
                            'c_calc_val' => $easyPayStoreObj->c_calc_val,
                            'c_fee_low_limit' => $easyPayStoreObj->c_fee_low_limit,
                            'd_calc_type' => $easyPayStoreObj->d_calc_type,
                            'c_calc_type' => $easyPayStoreObj->c_calc_type,
                            'pub_bank_name' => $easyPayStoreObj->pub_bank_name,
                            'pub_bank_code' => $easyPayStoreObj->pub_bank_code,
                            'pub_account' => $easyPayStoreObj->pub_account,
                            'pub_acc_name' => $easyPayStoreObj->pub_acc_name,
                            'house_number_url' => $easyPayStoreObj->house_number_url,
                            'agreement_url' => $easyPayStoreObj->agreement_url,
                            'merc_regist_form_a_url' => $easyPayStoreObj->merc_regist_form_a_url,
                            'merc_regist_form_b_url' => $easyPayStoreObj->merc_regist_form_b_url,
                            'cert_of_auth_url' => $easyPayStoreObj->cert_of_auth_url
                        ];
                    } else {
                        $data = [
                            'mcc_id' => '',
                            'standard_flag' => '',
                            'employee_num' => '',
                            'busin_form' => '',
                            'busin_beg_time' => '',
                            'busin_end_time' => '',
                            'term_mode' => '',
                            'term_code' => '',
                            'ter_user_name' => '',
                            'area_no' => '',
                            'term_area' => '',
                            'term_model' => '',
                            'term_model_lic' => '',
                            'bang_ding_commer_code' => '',
                            'bang_ding_ter_mno' => '',
                            'busin_scope' => '',
                            'capital' => '',
                            'bank_name' => '',
                            'bank_code' => '',
                            'real_time_entry' => '',
                            'mer_area' => '',
                            'd_stlm_type' => '',
                            'd_calc_val' => '',
                            'd_stlm_max_amt' => '',
                            'd_fee_low_limit' => '',
                            'c_calc_val' => '',
                            'c_fee_low_limit' => '',
                            'd_calc_type' => '',
                            'c_calc_type' => '',
                            'pub_bank_name' => '',
                            'pub_bank_code' => '',
                            'pub_account' => '',
                            'pub_acc_name' => '',
                            'house_number_url' => '',
                            'agreement_url' => '',
                            'merc_regist_form_a_url' => '',
                            'merc_regist_form_b_url' => '',
                            'cert_of_auth_url' => ''
                        ];
                    }

                    $data_r[$v] = $data;
                }
            }

            $this->status = 1;
            $this->message = '成功';
            return $this->format($data_r);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage() . '-' . $exception->getLine();
            return $this->format();
        }
    }

    /**
     * 门店认证修改-app进件操作
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function add_easypay_store(Request $request)
    {
        try {
            $token = $this->parseToken();
            $data = $request->except(['token']);
            $user_id = $request->get('user_id', $token->user_id);

            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => -1,
                    'message' => '用户不存在'
                ]);
            }

//            $userAuths = UserAuths::where('user_id', $user_id)->where('user_type', '1')->first();
//            if (!$userAuths) {
//                return json_encode([
//                    'status' => -1,
//                    'message' => '未实名认证，请先进行实名认证'
//                ]);
//            }
//            //检查是否签署协议
//            $esignAuth = EsignAuths::where('user_id', $user_id)->first();
//            if (!$esignAuth) {
//                return json_encode([
//                    'status' => -1,
//                    'message' => '请先进行协议签署'
//                ]);
//            } else {
//                if ($esignAuth->sign_flow_status != '2') {
//                    return json_encode([
//                        'status' => -1,
//                        'message' => '请先完成协议签署'
//                    ]);
//                }
//            }

            $config_id = $token->config_id;

            $addCompayType = $request->get('addCompayType', '0'); // 是否存在通道，该通道为新增。默认0不存在，1已存在
            $saveStep = $request->get('saveStep', '1'); // 1执照信息 、2账户信息 3、门店信息 4、功能信息 5、

            $store_id = '';
            if ($saveStep != 1 || $addCompayType == '1') {
                $store_id = $request->get('storeId', '');
            }
            //1、执照信息
            $company = $request->get('company', 'easypay'); //默认易生支付
            $store_type_name = $request->get('store_type_name', '');
            $store_type = $request->get('store_type', ''); //门店性质 1-个体，2-企业，3-小微商户
            $store_name = $request->get('store_name', '');
            $store_short_name = $request->get('store_short_name', $store_name); //经营名称 (店名),小微商户不使用默认值
            $store_address = $request->get('store_address', '');
            if ($store_type == '1') {
                $store_type_name = '个体';
            } else if ($store_type == '2') {
                $store_type_name = '企业';
            } else if ($store_type == '3') {
                $store_type_name = '小微商户';
            }

            if ($store_type == '1' || $store_type == '2') {
                $store_license_no = $request->get('store_license_no', '');
                $store_license_img = $request->get('store_license_img', '');
                $store_license_time = $request->get('store_license_time', '');
                $store_license_stime = $request->get('store_license_stime', '');
            }

            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');
            $province_name = $request->get('province_name', '');
            $city_name = $request->get('city_name', '');
            $area_name = $request->get('area_name', '');
            $people_phone = $request->get('people_phone', '');
            $store_email = $request->get('store_email', '');
            $head_name = $request->get('head_name', '');
            $head_sfz_no = $request->get('head_sfz_no', '');
            $people = $request->get('people', '');
            $head_sfz_img_a = $request->get('head_sfz_img_a', '');  //法人身份证正面
            $head_sfz_img_b = $request->get('head_sfz_img_b', '');  //法人身份证反面

            $head_sfz_stime = $request->get('head_sfz_stime', ''); //法人身份证开始日期
            $head_sfz_time = $request->get('head_sfz_time', ''); //法人身份证结束日期
            if ($head_sfz_time != '长期') {
                $head_sfz_time = $this->time($head_sfz_time);
            }
            $head_sfz_stime = $this->time($head_sfz_stime);

            //add easypay_store_img
            $area_no = $request->get('area_no', ''); //易生区域code
            $mer_area = $request->get('mer_area', ''); //易生区域code
            $term_area = $request->get('term_area', '');

            //第二步 2、账户信息

            $bankType = $request->get('bankType', ''); // 01 法人对公 02对公 03非法人对私
            $store_bank_type = $request->get('store_bank_type', ''); //01 对私 02 对公
            $store_bank_name = $request->get('store_bank_name', '');
            $store_bank_no = $request->get('store_bank_no', '');    //银行卡号
            $bank_no = $request->get('bank_no', '');    //联行号
            $sub_bank_name = $request->get('sub_bank_name', '');   //支行名称
            $bank_name = $request->get('bank_name', '');    //银行名称
            $reserved_mobile = $request->get('reserved_mobile', '');    //预留手机号
            $store_industrylicense_img = $request->get('store_industrylicense_img', '');//开户许可证

            if ($bankType == '01') {
                $bank_img_a = $request->get('bank_img_a', '');
                $bank_img_b = $request->get('bank_img_b', '');
                $bank_sc_img = $request->get('bank_sc_img', '');

            } else if ($bankType == '02') {

            } else if ($bankType == '03') {
                $bank_img_a = $request->get('bank_img_a', '');
                $bank_img_b = $request->get('bank_img_b', '');
                $bank_sc_img = $request->get('bank_sc_img', '');
                $bank_sfz_img_a = $request->get('bank_sfz_img_a', '');
                $bank_sfz_img_b = $request->get('bank_sfz_img_b', '');

                $bank_sfz_no = $request->get('bank_sfz_no', '');
                $bank_sfz_stime = $request->get('bank_sfz_stime', '');
                $bank_sfz_time = $request->get('bank_sfz_time', '');
                if ($bank_sfz_time != '长期') {
                    $bank_sfz_time = $this->time($bank_sfz_time);
                }
                $bank_sfz_stime = $this->time($bank_sfz_stime);
            }

            //第三步 3、门店信息

            //门店来源 比如 中原畅立收
            $source_type = $request->get('source_type', '0'); // 1:中原畅立收
            $store_logo_img = $request->get('store_logo_img', '');
            $store_img_a = $request->get('store_img_a', '');
            $store_img_b = $request->get('store_img_b', '');
            $store_img_c = $request->get('store_img_c', '');
            $category_id = $request->get('category_id', ''); //经营类型
            $category_name = $request->get('category_name', '');//经营类型名称

            //add easypay_store_img
            $businForm = $request->get('businForm', '01');
            $projectId = $request->get('projectId', '');
            $mcc_id = $request->get('mccId', '');
            $busin_scope = $request->get('businScope', ''); //主营业务
            $agreement = $request->get('agreement', '');    //协议图片地址
            $location_photo_url = $request->get('location_photo_url', ''); //定位照
            $busin_beg_time = $request->get('busin_beg_time', '05:00'); //营业时间:开始时间,格式:HHMM
            $busin_end_time = $request->get('busin_end_time', '23:00'); //营业时间:结束时间,格式:HHMM
            $capital = $request->get('capital', '50'); //注册资本：单位-万元
            $term_mode = $request->get('termMode', '0'); //终端种类(0--POS终端,所有交易走易生的实体终端;2--虚拟终端;4--银联POS,直联POS,终端商户号和终端终端号,秘钥非易生的;5--其他POS,先到银联申请终端商户号和终端号,秘钥用易;6--二维码终端,(默认为0)
            $standard_flag = $request->get('standardFlag', '0'); //行业大类(0-标准,1-优惠,2-减免)
            $employee_num = $request->get('employeeNum', '1'); //公司规模(1:0-50人,2:50-100人,3:100以上)
            $branchName = $request->get('branchName', '');
            $branchManager = $request->get('branchManager', '');
            $merMark = $request->get('merMark', '');


            //第四步 4、功能信息

            if ($saveStep == 1) {
                $validate = Validator::make($data, [
                    'company' => 'required',
                    'store_name' => 'required',
                    'people' => 'required',
                    'people_phone' => 'required',
                    'province_code' => 'required',
                    'city_code' => 'required',
                    'area_code' => 'required',
                    'store_address' => 'required',
                    'head_name' => 'required',
                    'head_sfz_no' => 'required',
                    'head_sfz_time' => 'required',
                    'head_sfz_stime' => 'required',
                    'store_type' => 'required',
                    'store_type_name' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'company' => '通道类型',
                    'store_name' => '门店名称',
                    'people' => '联系人',
                    'people_phone' => '手机号',
                    'province_code' => '区域',
                    'city_code' => '区域',
                    'area_code' => '区域',
                    'store_address' => '详细地址',
                    'head_name' => '法人',
                    'head_sfz_no' => '身份证号',
                    'head_sfz_time' => '证件成立日期',
                    'head_sfz_stime' => '证件有效期',
                    'store_type' => '企业类型',
                    'store_type_name' => '企业类型',
                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                //开启事务
                try {
                    DB::beginTransaction();
                    if ($addCompayType == '0') { //新增门店

                        $store_id = date('Ymdhis', time()) . rand(1000, 9999);//随机

                        //拼装门店信息
                        $stores = [
                            'config_id' => $config_id,
                            'user_id' => $user_id,
                            'merchant_id' => '',
                            'store_id' => $store_id,
                            'store_name' => trim($store_name),
                            'store_type_name' => trim($store_type_name),
                            'store_email' => trim($store_email),
                            'store_short_name' => trim($store_short_name),
                            'people' => trim($people),//
                            'people_phone' => trim($people_phone),
                            'province_code' => $province_code,
                            'city_code' => $city_code,
                            'area_code' => $area_code,
                            'province_name' => $province_name,
                            'city_name' => $city_name,
                            'area_name' => $area_name,
                            'store_address' => trim($store_address),
                            'head_name' => trim($head_name), //法人
                            'head_sfz_no' => trim($head_sfz_no),
                            'head_sfz_time' => $head_sfz_time,
                            'head_sfz_stime' => $head_sfz_stime,
                            'store_type' => $store_type,
                            'status' => 1,
                            'status_desc' => "自动审核通过",
                            'admin_status' => 1,
                            'admin_status_desc' => "系统自动审核",
                            'weixin_no' => "",
                            'alipay_name' => "",
                            'alipay_account' => "",
                            'source' => $user->source, ////来源,01:畅立收，02:河南畅立收
                        ];
                        if ($store_type == '3') {
                            $stores['status'] = 2;
                            $stores['status_desc'] = "待审核";
                            $stores['admin_status'] = 2;
                            $stores['admin_status_desc'] = "待审核";
                        }
                        if ($store_type == '1' || $store_type == '2') {
                            $stores['store_license_no'] = $store_license_no;
                            $stores['store_license_time'] = $store_license_time;
                            $stores['store_license_stime'] = $store_license_stime;
                        }

                        //图片信息
                        $store_imgs = [
                            'store_id' => $store_id,
                            'head_sfz_img_a' => $head_sfz_img_a,
                            'head_sfz_img_b' => $head_sfz_img_b
                        ];
                        if ($store_type == '1' || $store_type == '2') {
                            $store_imgs['store_license_img'] = $store_license_img;
                        }

                        //80开头+当前日期+4位随机数
                        $merchantNo = $this->generateMerchantNo();
                        //检查是否存在商户号
                        $existMerchantNo = Merchant::where('merchant_no', $merchantNo)->first();
                        if (!$existMerchantNo) {
                            $merchantNo = $this->generateMerchantNo(); //重新获取
                        }

                        //注册merchants账户
                        $merchantData = [
                            'pid' => 0,
                            'type' => 1, //1、店长 2 收银员
                            'name' => $people,
                            'password' => bcrypt('000000'),
                            'pay_password' => bcrypt('000000'),
                            'phone' => $people_phone,
                            'user_id' => $user_id,
                            'config_id' => $config_id,
                            'wx_openid' => '',
                            'source' => $user->source, //来源,01:畅立收，02:河南畅立收
                            'merchant_no' => $merchantNo
                        ];
                        $merchant = Merchant::create($merchantData);
                        $merchant_id = $merchant->id;

                        $stores['merchant_id'] = $merchant_id;
                        MerchantStore::create([
                            'store_id' => $store_id,
                            'merchant_id' => $merchant_id
                        ]);

                        Store::create($stores);
                        StoreImg::create($store_imgs);

                        //初始化费率 默认0.38

                        $data['store_id'] = $store_id;
                        $data['rate'] = 0.38;
                        $data['company'] = $company;
                        $retStorePayWay = $this->app_send_ways_data($data);

                        if ($retStorePayWay['status'] == -1) {
                            DB::rollBack();
                            return json_encode([
                                'status' => -1,
                                'message' => $retStorePayWay['message']
                            ]);
                        }
                    }

                    $easyPayStoreData = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'source_type' => $source_type,
                        'source' => $user->source
                    ];

                    $easyPayStoreObj = new EasyPayStoreController();
                    $easyPayStoreResult = $easyPayStoreObj->addEasyPayStoreImages($request, $easyPayStoreData);

                    if ($easyPayStoreResult['status'] == -1) {
                        DB::rollBack();
                        return json_encode([
                            'status' => -1,
                            'message' => $easyPayStoreResult['message']
                        ]);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                        $storeSteps->save();
                    } else {
                        StoreSteps::create([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 2) {
                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_bank_type' => 'required',
                    'store_bank_name' => 'required',
                    'store_bank_no' => 'required',
                    'bank_no' => 'required',
                    'sub_bank_name' => 'required',
                    'bank_name' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_bank_type' => '结算类型',
                    'store_bank_name' => '开户名称',
                    'store_bank_no' => '银行卡号',
                    'bank_no' => '联行号',
                    'sub_bank_name' => '支行名称',
                    'bank_name' => '银行名称',

                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();
                $storeBank = StoreBank::where('store_id', $store_id)->first();

                //开启事务
                try {
                    DB::beginTransaction();
                    //银行卡信息
                    $store_banks = [
                        'store_id' => $store_id,
                        'store_bank_type' => $store_bank_type,
                        'store_bank_name' => $store_bank_name,
                        'store_bank_no' => $store_bank_no,
                        'bank_no' => $bank_no,
                        'sub_bank_name' => $sub_bank_name,
                        'bank_name' => $bank_name,
                        'reserved_mobile' => $reserved_mobile,
                        'store_bank_phone' => $store->people_phone,
                        'bank_province_code' => $store->province_code,
                        'bank_city_code' => $store->city_code,
                        'bank_area_code' => $store->area_code,
                    ];
                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                    ];

                    if ($bankType == '01') {
                        $store_banks['bank_sfz_no'] = $store->head_sfz_no;
                        $store_banks['bank_sfz_stime'] = $store->head_sfz_stime;
                        $store_banks['bank_sfz_time'] = $store->head_sfz_time;

                        $store_imgs['bank_img_a'] = $bank_img_a;
                        $store_imgs['bank_img_b'] = $bank_img_b;
                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;

                        $store_imgs['bank_sc_img'] = $bank_sc_img;
                        $store_imgs['bank_sfz_img_a'] = "";
                        $store_imgs['bank_sfz_img_b'] = "";
                    } else if ($bankType == '02') {
                        $store_banks['bank_sfz_no'] = $store->head_sfz_no;
                        $store_banks['bank_sfz_stime'] = $store->head_sfz_stime;
                        $store_banks['bank_sfz_time'] = $store->head_sfz_time;

                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                        $store_imgs['bank_img_a'] = "";
                        $store_imgs['bank_img_b'] = "";
                        $store_imgs['bank_sc_img'] = "";
                        $store_imgs['bank_sfz_img_a'] = "";
                        $store_imgs['bank_sfz_img_b'] = "";
                    } else if ($bankType == '03') {
                        $store_banks['bank_sfz_no'] = $bank_sfz_no;
                        $store_banks['bank_sfz_stime'] = $bank_sfz_stime;
                        $store_banks['bank_sfz_time'] = $bank_sfz_time;
                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;

                        $store_imgs['bank_img_a'] = $bank_img_a;
                        $store_imgs['bank_img_b'] = $bank_img_b;
                        $store_imgs['bank_sc_img'] = $bank_sc_img;
                        $store_imgs['bank_sfz_img_a'] = $bank_sfz_img_a;
                        $store_imgs['bank_sfz_img_b'] = $bank_sfz_img_b;
                    }

                    if ($storeBank) {
                        $storeBank->update(array_filter($store_banks));
                        $storeBank->save();
                    } else {
                        StoreBank::create($store_banks);
                    }

                    if ($storeImg) {
                        $storeImg->update(array_filter($store_imgs));
                        $storeImg->save();
                    } else {
                        StoreImg::create($store_imgs);
                    }


                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '账户信息'
                        ]);
                        $storeSteps->save();
                    }

                    $easyPayStoreData = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'source_type' => $source_type,
                        'source' => $user->source
                    ];

                    $easyPayStoreObj = new EasyPayStoreController();
                    $easyPayStoreResult = $easyPayStoreObj->addEasyPayStoreImages($request, $easyPayStoreData);

                    if ($easyPayStoreResult['status'] == -1) {
                        DB::rollBack();
                        return json_encode([
                            'status' => -1,
                            'message' => $easyPayStoreResult['message']
                        ]);
                    }


                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }

            } else if ($saveStep == 3) {

                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_logo_img' => 'required',
                    'store_img_a' => 'required',
                    'store_img_b' => 'required',
                    'store_img_c' => 'required',
                    'category_id' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_logo_img' => '门头照',
                    'store_img_a' => '收银台照',
                    'store_img_b' => '经营内容照',
                    'store_img_c' => '店内全景照',
                    'category_id' => '经营类型',

                ]);

                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }

                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();

                //开启事务
                try {
                    DB::beginTransaction();

                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                        'store_logo_img' => $store_logo_img,
                        'store_img_a' => $store_img_a,
                        'store_img_b' => $store_img_b,
                        'store_img_c' => $store_img_c,
                    ];

                    if ($storeImg) {
                        $storeImg->update(array_filter($store_imgs));
                        $storeImg->save();
                    } else {
                        StoreImg::create($store_imgs);
                    }
                    if ($store) {
                        $stores = [
                            'store_id' => $store_id,
                            'category_id' => $category_id,
                            'category_name' => $category_name,
                            'source_type' => $source_type,
                        ];
                        $store->update(array_filter($stores));
                        $store->save();
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '门店信息'
                        ]);
                        $storeSteps->save();
                    }

                    $easyPayStoreData = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'source_type' => $source_type,
                        'source' => $user->source
                    ];

                    $easyPayStoreObj = new EasyPayStoreController();
                    $easyPayStoreResult = $easyPayStoreObj->addEasyPayStoreImages($request, $easyPayStoreData);

                    if ($easyPayStoreResult['status'] == -1) {
                        DB::rollBack();
                        return json_encode([
                            'status' => -1,
                            'message' => $easyPayStoreResult['message']
                        ]);
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 4) {

                $rate = $request->get('rate', '');

                $store = Store::where('store_id', $store_id)
                    ->first();
                if (!$store) {
                    return json_encode(['status' => -1, 'message' => '门店不存在']);
                }
                //通道支付费率 storePayWay
                if ($rate > 0.40) {
                    return json_encode(['status' => -1, 'message' => '设置费率超限']);
                }

                try {
                    DB::beginTransaction();

                    //查找下级门店共享通道
                    if ($store->pid == 0) {
                        $sub_store = Store::where('pid', $store->id)
                            ->where('pay_ways_type', 1)
                            ->select('store_id')
                            ->get();
                        foreach ($sub_store as $k => $v) {
                            $data['store_id'] = $v->store_id;
                            $data['rate'] = $rate;
                            $data['company'] = $company;

                            $this->app_send_ways_data($data);
                        }
                    }

                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['company'] = $company;
                    $retStorePayWay = $this->app_send_ways_data($data);

                    if ($retStorePayWay['status'] == -1) {
                        DB::rollBack();
                        return json_encode([
                            'status' => -1,
                            'message' => $retStorePayWay['message']
                        ]);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '功能信息'
                        ]);
                        $storeSteps->save();
                    }

                    $easyPayStoreData = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'source_type' => $source_type,
                        'source' => $user->source
                    ];

                    $easyPayStoreObj = new EasyPayStoreController();
                    $easyPayStoreResult = $easyPayStoreObj->addEasyPayStoreImages($request, $easyPayStoreData);

                    if ($easyPayStoreResult['status'] == -1) {
                        DB::rollBack();
                        return json_encode([
                            'status' => -1,
                            'message' => $easyPayStoreResult['message']
                        ]);
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 5) { //提交审核

                $easyPayStoreData = [
                    'store_id' => $store_id,
                    'config_id' => $config_id,
                    'source_type' => $source_type,
                    'source' => $user->source
                ];

                $easyPayStoreObj = new EasyPayStoreController();
                $easyPayStoreResult = $easyPayStoreObj->addEasyPayStoreImages($request, $easyPayStoreData);

                if ($easyPayStoreResult['status'] == -1) {
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $easyPayStoreResult['message']
                    ]);
                } else {

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '提交审核'
                        ]);
                        $storeSteps->save();
                    }

                    $store = Store::where('store_id', $store_id)->first();
                    $code = '';
                    $SettleModeType = '';//结算方式
                    $spwObj = new StorePayWaysController();

                    $store_ways_desc = DB::table('store_ways_desc')
                        ->where('is_show', '1')
                        ->where('company', $company)
                        ->first();
                    $type = $store_ways_desc->ways_type;

                    return $spwObj->base_open_ways($type, $code, $store_id, $SettleModeType, '', '');

                }

            }

            return json_encode([
                'status' => '1',
                'message' => '操作成功',
                'data' => [
                    'store_id' => $store_id
                ]
            ]);
        } catch (\Exception $ex) {
            Log::info($ex);
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    public function update_easypay_store(Request $request)
    {
        try {
            $token = $this->parseToken();
            // $user_id = $token->user_id;
            $data = $request->except(['token']);
            $store_id = $request->get('storeId', '');
            $user_id = $request->get('user_id', '');

            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => -1,
                    'message' => '用户不存在'
                ]);
            }
//            $userAuths = UserAuths::where('user_id', $user_id)->where('user_type', '1')->first();
//            if (!$userAuths) {
//                return json_encode([
//                    'status' => -1,
//                    'message' => '未实名认证，请先进行实名认证'
//                ]);
//            }

            //检查是否签署协议
//            $esignAuth = EsignAuths::where('user_id', $user_id)->first();
//            if (!$esignAuth) {
//                return json_encode([
//                    'status' => -1,
//                    'message' => '请先进行协议签署'
//                ]);
//            } else {
//                if ($esignAuth->sign_flow_status != '2') {
//                    return json_encode([
//                        'status' => -1,
//                        'message' => '请先完成协议签署'
//                    ]);
//                }
//            }

            $config_id = $token->config_id;

            $saveStep = $request->get('saveStep', '1'); // 1执照信息 、2账户信息 3、门店信息 4、功能信息 5、
            $company = $request->get('company', 'easypay'); //默认易生支付
            //1、执照信息
            $store_type_name = $request->get('store_type_name', '');
            $store_type = $request->get('store_type', ''); //门店性质 1-个体，2-企业，3-小微商户
            $store_name = $request->get('store_name', '');
            $store_short_name = $request->get('store_short_name', $store_name); //经营名称 (店名)，小微商户不使用默认值
            $store_address = $request->get('store_address', '');
            if ($store_type == '1') {
                $store_type_name = '个体';
            } else if ($store_type == '2') {
                $store_type_name = '企业';
            } else if ($store_type == '3') {
                $store_type_name = '小微商户';
            }
            if ($store_type == '1' || $store_type == '2') {
                $store_license_no = $request->get('store_license_no', '');
                $store_license_img = $request->get('store_license_img', '');
                $store_license_time = $request->get('store_license_time', '');
                $store_license_stime = $request->get('store_license_stime', '');
            }

            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');
            $province_name = $request->get('province_name', '');
            $city_name = $request->get('city_name', '');
            $area_name = $request->get('area_name', '');
            $people_phone = $request->get('people_phone', '');
            $store_email = $request->get('store_email', '');
            $head_name = $request->get('head_name', '');
            $head_sfz_no = $request->get('head_sfz_no', '');
            $people = $request->get('people', '');
            $head_sfz_img_a = $request->get('head_sfz_img_a', '');  //法人身份证正面
            $head_sfz_img_b = $request->get('head_sfz_img_b', '');  //法人身份证反面

            $head_sfz_stime = $request->get('head_sfz_stime', ''); //法人身份证开始日期
            $head_sfz_time = $request->get('head_sfz_time', ''); //法人身份证结束日期
            if ($head_sfz_time != '长期') {
                $head_sfz_time = $this->time($head_sfz_time);
            }
            $head_sfz_stime = $this->time($head_sfz_stime);

            //add easypay_store_img
            $area_no = $request->get('area_no', ''); //易生区域code
            $mer_area = $request->get('mer_area', ''); //易生区域code
            $term_area = $request->get('term_area', '');

            //第二步 2、账户信息

            $bankType = $request->get('bankType', ''); // 01 法人对公 02对公 03非法人对私
            $store_bank_type = $request->get('store_bank_type', ''); //01 对私 02 对公
            $store_bank_name = $request->get('store_bank_name', '');
            $store_bank_no = $request->get('store_bank_no', '');    //银行卡号
            $bank_no = $request->get('bank_no', '');    //联行号
            $sub_bank_name = $request->get('sub_bank_name', '');   //支行名称
            $bank_name = $request->get('bank_name', '');    //银行名称
            $reserved_mobile = $request->get('reserved_mobile', '');    //预留手机号
            $store_industrylicense_img = $request->get('store_industrylicense_img', '');//开户许可证

            if ($bankType == '01') {
                $bank_img_a = $request->get('bank_img_a', '');
                $bank_img_b = $request->get('bank_img_b', '');
                $bank_sc_img = $request->get('bank_sc_img', '');
            } else if ($bankType == '02') {

            } else if ($bankType == '03') {
                $bank_img_a = $request->get('bank_img_a', '');
                $bank_img_b = $request->get('bank_img_b', '');
                $bank_sc_img = $request->get('bank_sc_img', '');
                $bank_sfz_img_a = $request->get('bank_sfz_img_a', '');
                $bank_sfz_img_b = $request->get('bank_sfz_img_b', '');

                $bank_sfz_no = $request->get('bank_sfz_no', '');
                $bank_sfz_stime = $request->get('bank_sfz_stime', '');
                $bank_sfz_time = $request->get('bank_sfz_time', '');
                if ($bank_sfz_time != '长期') {
                    $bank_sfz_time = $this->time($bank_sfz_time);
                }
                $bank_sfz_stime = $this->time($bank_sfz_stime);
            }

            //第三步 3、门店信息

            //门店来源 比如 中原畅立收
            $source_type = $request->get('source_type', '0'); // 1:中原畅立收
            $store_logo_img = $request->get('store_logo_img', '');
            $store_img_a = $request->get('store_img_a', '');
            $store_img_b = $request->get('store_img_b', '');
            $store_img_c = $request->get('store_img_c', '');
            $category_id = $request->get('category_id', ''); //经营类型
            $category_name = $request->get('category_name', '');//经营类型名称


            //add easypay_store_img
            $businForm = $request->get('businForm', '01');
            $projectId = $request->get('projectId', '');
            $mcc_id = $request->get('mccId', '');
            $busin_scope = $request->get('businScope', ''); //主营业务
            $agreement = $request->get('agreement', '');    //协议图片地址
            $location_photo_url = $request->get('location_photo_url', ''); //定位照
            $busin_beg_time = $request->get('busin_beg_time', '05:00'); //营业时间:开始时间,格式:HHMM
            $busin_end_time = $request->get('busin_end_time', '23:00'); //营业时间:结束时间,格式:HHMM
            $capital = $request->get('capital', '50'); //注册资本：单位-万元
            $term_mode = $request->get('termMode', '0'); //终端种类(0--POS终端,所有交易走易生的实体终端;2--虚拟终端;4--银联POS,直联POS,终端商户号和终端终端号,秘钥非易生的;5--其他POS,先到银联申请终端商户号和终端号,秘钥用易;6--二维码终端,(默认为0)
            $standard_flag = $request->get('standardFlag', '0'); //行业大类(0-标准,1-优惠,2-减免)
            $employee_num = $request->get('employeeNum', '1'); //公司规模(1:0-50人,2:50-100人,3:100以上)
            $branchName = $request->get('branchName', '');
            $branchManager = $request->get('branchManager', '');
            $merMark = $request->get('merMark', '');

            //第四步 4、功能信息

            if ($saveStep == 1) {
                $validate = Validator::make($data, [
                    'store_name' => 'required',
                    'people' => 'required',
                    'people_phone' => 'required',
                    'province_code' => 'required',
                    'city_code' => 'required',
                    'area_code' => 'required',
                    'store_address' => 'required',
                    'head_name' => 'required',
                    'head_sfz_no' => 'required',
                    'head_sfz_time' => 'required',
                    'head_sfz_stime' => 'required',
                    'store_type' => 'required',
                    'store_type_name' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'store_name' => '门店名称',
                    'people' => '联系人',
                    'people_phone' => '手机号',
                    'province_code' => '区域',
                    'city_code' => '区域',
                    'area_code' => '区域',
                    'store_address' => '详细地址',
                    'head_name' => '法人',
                    'head_sfz_no' => '身份证号',
                    'head_sfz_time' => '证件成立日期',
                    'head_sfz_stime' => '证件有效期',
                    'store_type' => '企业类型',
                    'store_type_name' => '企业类型',
                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                //开启事务
                try {
                    DB::beginTransaction();

                    $merchantStore = MerchantStore::where('store_id', $store_id)->select('merchant_id')->first();
                    if ($merchantStore) {
                        $upIN = [
                            'name' => $people,
                            'phone' => $people_phone,
                        ];
                        Merchant::where('id', $merchantStore->merchant_id)->update($upIN);
                    }

                    //拼装门店信息
                    $stores = [
                        'config_id' => $config_id,
                        //'user_id' => $user_id,
                        'merchant_id' => '',
                        'store_id' => $store_id,
                        'store_name' => trim($store_name),
                        'store_type_name' => trim($store_type_name),
                        'store_email' => trim($store_email),
                        'store_short_name' => trim($store_short_name),
                        'people' => trim($people),//
                        'people_phone' => trim($people_phone),
                        'province_code' => $province_code,
                        'city_code' => $city_code,
                        'area_code' => $area_code,
                        'province_name' => $province_name,
                        'city_name' => $city_name,
                        'area_name' => $area_name,
                        'store_address' => trim($store_address),
                        'head_name' => trim($head_name), //法人
                        'head_sfz_no' => trim($head_sfz_no),
                        'head_sfz_time' => $head_sfz_time,
                        'head_sfz_stime' => $head_sfz_stime,
                        'store_type' => $store_type,
                        'status' => 1,
                        'status_desc' => "自动审核通过",
                        'admin_status' => 1,
                        'admin_status_desc' => "系统自动审核",
                        'weixin_no' => "",
                        'alipay_name' => "",
                        'alipay_account' => "",
                        'source' => $user->source, ////来源,01:畅立收，02:河南畅立收
                    ];
                    if ($store_type == '3') {
                        $stores['status'] = 2;
                        $stores['status_desc'] = "待审核";
                        $stores['admin_status'] = 2;
                        $stores['admin_status_desc'] = "待审核";
                    }
                    if ($store_type == '1' || $store_type == '2') {
                        $stores['store_license_no'] = $store_license_no;
                        $stores['store_license_time'] = $store_license_time;
                        $stores['store_license_stime'] = $store_license_stime;
                    }

                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                        'head_sfz_img_a' => $head_sfz_img_a,
                        'head_sfz_img_b' => $head_sfz_img_b
                    ];
                    if ($store_type == '1' || $store_type == '2') {
                        $store_imgs['store_license_img'] = $store_license_img;
                    }
                    $store = Store::where('store_id', $store_id)->first();
                    if (!$store) {
                        return json_encode([
                            'status' => 2,
                            'message' => '门店未认证'
                        ]);
                    } else {

                        $merchantInfo = Merchant::where('id', $store->merchant_id)->first();
                        if ($merchantInfo) {
                            $upIN = [
                                'name' => $people,
                                'phone' => $people_phone,
                            ];
                            $merchantInfo->update($upIN);
                        }

                        $storeImg = StoreImg::where('store_id', $store_id)->first();

                        if ($storeImg) {
                            $storeImg->update(array_filter($store_imgs));
                            $storeImg->save();
                        } else {
                            StoreImg::create($store_imgs);
                        }

                        $store->update(array_filter($stores));
                        $store->save();

                    }

                    $easyPayStoreData = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'source_type' => $source_type,
                        'source' => $user->source
                    ];

                    $easyPayStoreObj = new EasyPayStoreController();
                    $easyPayStoreResult = $easyPayStoreObj->addEasyPayStoreImages($request, $easyPayStoreData);

                    if ($easyPayStoreResult['status'] == -1) {
                        DB::rollBack();
                        return json_encode([
                            'status' => -1,
                            'message' => $easyPayStoreResult['message']
                        ]);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                        $storeSteps->save();
                    } else {
                        StoreSteps::create([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 2) {
                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_bank_type' => 'required',
                    'store_bank_name' => 'required',
                    'store_bank_no' => 'required',
                    'bank_no' => 'required',
                    'sub_bank_name' => 'required',
                    'bank_name' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_bank_type' => '结算类型',
                    'store_bank_name' => '开户名称',
                    'store_bank_no' => '银行卡号',
                    'bank_no' => '联行号',
                    'sub_bank_name' => '支行名称',
                    'bank_name' => '银行名称',

                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();
                $storeBank = StoreBank::where('store_id', $store_id)->first();

                //开启事务
                try {
                    DB::beginTransaction();
                    //银行卡信息
                    $store_banks = [
                        'store_id' => $store_id,
                        'store_bank_type' => $store_bank_type,
                        'store_bank_name' => $store_bank_name,
                        'store_bank_no' => $store_bank_no,
                        'bank_no' => $bank_no,
                        'sub_bank_name' => $sub_bank_name,
                        'bank_name' => $bank_name,
                        'reserved_mobile' => $reserved_mobile,
                        'store_bank_phone' => $store->people_phone,
                        'bank_province_code' => $store->province_code,
                        'bank_city_code' => $store->city_code,
                        'bank_area_code' => $store->area_code,
                    ];
                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                    ];

                    if ($bankType == '01') {
                        $store_banks['bank_sfz_no'] = $store->head_sfz_no;
                        $store_banks['bank_sfz_stime'] = $store->head_sfz_stime;
                        $store_banks['bank_sfz_time'] = $store->head_sfz_time;

                        $store_imgs['bank_img_a'] = $bank_img_a;
                        $store_imgs['bank_img_b'] = $bank_img_b;
                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;

                        $store_imgs['bank_sc_img'] = $bank_sc_img;
                        $store_imgs['bank_sfz_img_a'] = "";
                        $store_imgs['bank_sfz_img_b'] = "";
                    } else if ($bankType == '02') {
                        $store_banks['bank_sfz_no'] = $store->head_sfz_no;
                        $store_banks['bank_sfz_stime'] = $store->head_sfz_stime;
                        $store_banks['bank_sfz_time'] = $store->head_sfz_time;

                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                        $store_imgs['bank_img_a'] = "";
                        $store_imgs['bank_img_b'] = "";
                        $store_imgs['bank_sc_img'] = "";
                        $store_imgs['bank_sfz_img_a'] = "";
                        $store_imgs['bank_sfz_img_b'] = "";
                    } else if ($bankType == '03') {
                        $store_banks['bank_sfz_no'] = $bank_sfz_no;
                        $store_banks['bank_sfz_stime'] = $bank_sfz_stime;
                        $store_banks['bank_sfz_time'] = $bank_sfz_time;
                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;

                        $store_imgs['bank_img_a'] = $bank_img_a;
                        $store_imgs['bank_img_b'] = $bank_img_b;
                        $store_imgs['bank_sc_img'] = $bank_sc_img;
                        $store_imgs['bank_sfz_img_a'] = $bank_sfz_img_a;
                        $store_imgs['bank_sfz_img_b'] = $bank_sfz_img_b;
                    }

                    if ($storeBank) {
                        $storeBank->update(array_filter($store_banks));
                        $storeBank->save();
                    } else {
                        StoreBank::create($store_banks);
                    }

                    if ($storeImg) {
                        $storeImg->update(array_filter($store_imgs));
                        $storeImg->save();
                    } else {
                        StoreImg::create($store_imgs);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '账户信息'
                        ]);
                        $storeSteps->save();
                    }

                    $easyPayStoreData = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'source_type' => $source_type,
                        'source' => $user->source
                    ];

                    $easyPayStoreObj = new EasyPayStoreController();
                    $easyPayStoreResult = $easyPayStoreObj->addEasyPayStoreImages($request, $easyPayStoreData);

                    if ($easyPayStoreResult['status'] == -1) {
                        DB::rollBack();
                        return json_encode([
                            'status' => -1,
                            'message' => $easyPayStoreResult['message']
                        ]);
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }

            } else if ($saveStep == 3) {

                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_logo_img' => 'required',
                    'store_img_a' => 'required',
                    'store_img_b' => 'required',
                    'store_img_c' => 'required',
                    'category_id' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_logo_img' => '门头照',
                    'store_img_a' => '收银台照',
                    'store_img_b' => '经营内容照',
                    'store_img_c' => '店内全景照',
                    'category_id' => '经营类型',

                ]);

                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }

                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();

                //开启事务
                try {
                    DB::beginTransaction();
                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                        'store_logo_img' => $store_logo_img,
                        'store_img_a' => $store_img_a,
                        'store_img_b' => $store_img_b,
                        'store_img_c' => $store_img_c,
                    ];

                    if ($storeImg) {
                        $storeImg->update(array_filter($store_imgs));
                        $storeImg->save();
                    } else {
                        StoreImg::create($store_imgs);
                    }
                    if ($store) {
                        $stores = [
                            'store_id' => $store_id,
                            'category_id' => $category_id,
                            'category_name' => $category_name,
                            'source_type' => $source_type,
                        ];
                        $store->update(array_filter($stores));
                        $store->save();
                    }
                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();
                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '门店信息'
                        ]);
                        $storeSteps->save();
                    }

                    $easyPayStoreData = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'source_type' => $source_type,
                        'source' => $user->source
                    ];

                    $easyPayStoreObj = new EasyPayStoreController();
                    $easyPayStoreResult = $easyPayStoreObj->addEasyPayStoreImages($request, $easyPayStoreData);

                    if ($easyPayStoreResult['status'] == -1) {
                        DB::rollBack();
                        return json_encode([
                            'status' => -1,
                            'message' => $easyPayStoreResult['message']
                        ]);
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 4) {

                $rate = $request->get('rate', '');
                $company = $request->get('company', '');

                $store = Store::where('store_id', $store_id)
                    ->first();
                if (!$store) {
                    return json_encode(['status' => -1, 'message' => '门店不存在']);
                }
                //通道支付费率 storePayWay
                if ($rate > 0.40) {
                    return json_encode(['status' => -1, 'message' => '设置费率超限']);
                }

                try {
                    DB::beginTransaction();

                    //查找下级门店共享通道
                    if ($store->pid == 0) {
                        $sub_store = Store::where('pid', $store->id)
                            ->where('pay_ways_type', 1)
                            ->select('store_id')
                            ->get();
                        foreach ($sub_store as $k => $v) {
                            $data['store_id'] = $v->store_id;
                            $data['rate'] = $rate;
                            $data['company'] = $company;

                            $this->app_send_ways_data($data);
                        }
                    }

                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['company'] = $company;
                    $retStorePayWay = $this->app_send_ways_data($data);

                    if ($retStorePayWay['status'] == -1) {
                        DB::rollBack();
                        return json_encode([
                            'status' => -1,
                            'message' => $retStorePayWay['message']
                        ]);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();
                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '功能信息'
                        ]);
                        $storeSteps->save();
                    }

                    $easyPayStoreData = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'source_type' => $source_type,
                        'source' => $user->source
                    ];

                    $easyPayStoreObj = new EasyPayStoreController();
                    $easyPayStoreResult = $easyPayStoreObj->addEasyPayStoreImages($request, $easyPayStoreData);

                    if ($easyPayStoreResult['status'] == -1) {
                        DB::rollBack();
                        return json_encode([
                            'status' => -1,
                            'message' => $easyPayStoreResult['message']
                        ]);
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 5) { //提交审核
                $company = $request->get('company');

                $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();
                if ($storeSteps) {
                    $storeSteps->update([
                        'store_id' => $store_id,
                        'step' => $saveStep,
                        'company_type' => $company,
                        'remark' => '提交审核'
                    ]);
                    $storeSteps->save();
                }

                $easyPayStoreData = [
                    'store_id' => $store_id,
                    'config_id' => $config_id,
                    'source_type' => $source_type,
                    'source' => $user->source
                ];

                $easyPayStoreObj = new EasyPayStoreController();
                $easyPayStoreResult = $easyPayStoreObj->addEasyPayStoreImages($request, $easyPayStoreData);

                if ($easyPayStoreResult['status'] == -1) {
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $easyPayStoreResult['message']
                    ]);
                } else {
                    $store = Store::where('store_id', $store_id)->first();
                    if ($store->store_type != '3') {
                        $code = '';
                        $SettleModeType = '';//结算方式
                        $spwObj = new StorePayWaysController();

                        $store_ways_desc = DB::table('store_ways_desc')
                            ->where('is_show', '1')
                            ->where('company', $company)
                            ->first();
                        $type = $store_ways_desc->ways_type;

                        return $spwObj->base_open_ways($type, $code, $store_id, $SettleModeType, '', '');
                    }

                }

            }

            return json_encode([
                'status' => '1',
                'message' => '操作成功',
                'data' => [
                    'store_id' => $store_id
                ]
            ]);
        } catch (\Exception $ex) {
            Log::info($ex);
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    //审核易生进件资料
    public function checkEasypayStore(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $status = $request->get('status', '0');
            $status_desc = $request->get('status_desc', '');

            $Store = Store::where('store_id', $store_id)->first();
            if (!$Store) {
                return json_encode([
                    'status' => -1,
                    'message' => '门店不存在'
                ]);
            }

            if ($Store->status == '1') {
                return json_encode([
                    'status' => -1,
                    'message' => '门店已审核通过'
                ]);
            }
            if (!$status_desc) {
                if ($status == '1') {
                    $status_desc = "审核通过";
                } else if ($status == '2') {
                    $status_desc = "审核拒绝";
                }
            }

            $Store->status = $status;

            if ($user->level == 0) {
                $Store->status_desc = $status_desc;
                $Store->admin_status = $status;
                $Store->admin_status_desc = $status_desc;
            } else {
                return json_encode([
                    'status' => -1,
                    'message' => '无权限操作审核'
                ]);
            }

            if ($status == '1') {
                $code = '';
                $SettleModeType = '';//结算方式
                $spwObj = new StorePayWaysController();

                $store_ways_desc = DB::table('store_ways_desc')
                    ->where('is_show', '1')
                    ->where('company', "easypay")
                    ->first();
                $type = $store_ways_desc->ways_type;

                $res = $spwObj->base_open_ways($type, $code, $store_id, $SettleModeType, '', '');
                if (isset($res['status']) && $res['status'] == '1') {
                    $Store->save();

                    return json_encode([
                        'status' => 1,
                        'message' => $res['message'],
                        'data' => $request->except(['token'])
                    ]);
                }
                $resJosn = json_decode($res, true);

                Log::info("=========status======>" . $resJosn['status']);
                if ($resJosn['status'] == '1') {


                } else {
                    $waysList = StorePayWay::where('store_id', $store_id)->where('company', 'easypay')->get();

                    foreach ($waysList as $k => $v) {
                        $ways = StorePayWay::where('store_id', $store_id)->where('ways_type', $v->ways_type)->first();

                        if ($ways) {
                            $ways->update([
                                'store_id' => $store_id,
                                'status' => '3',
                                'status_desc' => $resJosn['message'],
                            ]);
                            $ways->save();
                        }
                    }

                    return json_encode([
                        'status' => -1,
                        'message' => $resJosn['message'],
                        'data' => $request->except(['token'])
                    ]);
                }

            } else {

                $Store->save();

                $waysList = StorePayWay::where('store_id', $store_id)->where('company', 'easypay')->get();

                foreach ($waysList as $k => $v) {
                    $ways = StorePayWay::where('store_id', $store_id)->where('ways_type', $v->ways_type)->first();

                    if ($ways) {
                        $ways->update([
                            'store_id' => $store_id,
                            'status' => '3',
                            'status_desc' => $status_desc,
                        ]);
                        $ways->save();
                    }
                }

                return json_encode([
                    'status' => 1,
                    'message' => '操作成功',
                    'data' => $request->except(['token'])
                ]);
            }

            return json_encode([
                'status' => 1,
                'message' => '操作成功',
                'data' => $request->except(['token'])
            ]);

        } catch (\Exception $exception) {

            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile() . $exception->getLine()
            ]);
        }
    }


    public function add_vbill_store(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_id = $request->get('user_id', $token->user_id);
            $data = $request->except(['token']);

            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => -1,
                    'message' => '用户不存在'
                ]);
            }

            $userAuths = UserAuths::where('user_id', $user_id)->where('user_type', '1')->first();
            if (!$userAuths) {
                return json_encode([
                    'status' => -1,
                    'message' => '未实名认证，请先进行实名认证'
                ]);
            }
            //检查是否签署协议
//            $esignAuth = EsignAuths::where('user_id', $user_id)->first();
//            if (!$esignAuth) {
//                return json_encode([
//                    'status' => -1,
//                    'message' => '请先进行协议签署'
//                ]);
//            } else {
//                if ($esignAuth->sign_flow_status != '2') {
//                    return json_encode([
//                        'status' => -1,
//                        'message' => '请先完成协议签署'
//                    ]);
//                }
//            }

            $config_id = $token->config_id;

            $addCompayType = $request->get('addCompayType', '0'); // 是否存在通道，该通道为新增。默认0不存在，1已存在
            $saveStep = $request->get('saveStep', '1'); // 1执照信息 、2账户信息 3、门店信息 4、功能信息 5、

            $store_id = '';
            if ($saveStep != 1 || $addCompayType == '1') {
                $store_id = $request->get('storeId', '');
            }
            //1、执照信息
            $company = $request->get('company', 'easypay'); //默认易生支付
            $store_type_name = $request->get('store_type_name', '');
            $store_type = $request->get('store_type', ''); //门店性质 1-个体，2-企业，3-小微商户
            $store_name = $request->get('store_name', '');
            $store_short_name = $request->get('store_short_name', $store_name);
            $store_name = $request->get('store_name', '');
            $store_address = $request->get('store_address', '');
            if ($store_type == '1') {
                $store_type_name = '个体';
            } else if ($store_type == '2') {
                $store_type_name = '企业';
            } else if ($store_type == '3') {
                $store_type_name = '小微商户';
            }
            if ($store_type == '1' || $store_type == '2') {
                $store_license_no = $request->get('store_license_no', '');
                $store_license_img = $request->get('store_license_img', '');
                $store_license_time = $request->get('store_license_time', '');
                $store_license_stime = $request->get('store_license_stime', '');
            }

            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');
            $province_name = $request->get('province_name', '');
            $city_name = $request->get('city_name', '');
            $area_name = $request->get('area_name', '');
            $people_phone = $request->get('people_phone', '');
            $store_email = $request->get('store_email', '');
            $head_name = $request->get('head_name', '');
            $head_sfz_no = $request->get('head_sfz_no', '');
            $people = $request->get('people', '');
            $head_sfz_img_a = $request->get('head_sfz_img_a', '');  //法人身份证正面
            $head_sfz_img_b = $request->get('head_sfz_img_b', '');  //法人身份证反面

            $head_sfz_stime = $request->get('head_sfz_stime', ''); //法人身份证开始日期
            $head_sfz_time = $request->get('head_sfz_time', ''); //法人身份证结束日期
            if ($head_sfz_time != '长期') {
                $head_sfz_time = $this->time($head_sfz_time);
            }
            $head_sfz_stime = $this->time($head_sfz_stime);

            //第二步 2、账户信息

            $bankType = $request->get('bankType', ''); // 01 法人对公 02对公 03非法人对私
            $store_bank_type = $request->get('store_bank_type', ''); //01 对私 02 对公
            $store_bank_name = $request->get('store_bank_name', '');
            $store_bank_no = $request->get('store_bank_no', '');    //银行卡号
            $bank_no = $request->get('bank_no', '');    //联行号
            $sub_bank_name = $request->get('sub_bank_name', '');   //支行名称
            $bank_name = $request->get('bank_name', '');    //银行名称
            $reserved_mobile = $request->get('reserved_mobile', '');    //预留手机号
            $store_industrylicense_img = $request->get('store_industrylicense_img', '');//开户许可证
            $store_auth_bank_img = $request->get('certOfAuth', ''); //清算授权书图片地址(非必填)

            if ($bankType == '01') {
                $bank_img_a = $request->get('bank_img_a', '');
                $bank_img_b = $request->get('bank_img_b', '');
                $bank_sc_img = $request->get('bank_sc_img', '');
            } else if ($bankType == '02') {

            } else if ($bankType == '03') {
                $bank_img_a = $request->get('bank_img_a', '');
                $bank_img_b = $request->get('bank_img_b', '');
                $bank_sc_img = $request->get('bank_sc_img', '');
                $bank_sfz_img_a = $request->get('bank_sfz_img_a', '');
                $bank_sfz_img_b = $request->get('bank_sfz_img_b', '');

                $bank_sfz_no = $request->get('bank_sfz_no', '');
                $bank_sfz_stime = $request->get('bank_sfz_stime', '');
                $bank_sfz_time = $request->get('bank_sfz_time', '');
                if ($bank_sfz_time != '长期') {
                    $bank_sfz_time = $this->time($bank_sfz_time);
                }
                $bank_sfz_stime = $this->time($bank_sfz_stime);
            }

            //第三步 3、门店信息

            //门店来源 比如 中原畅立收
            $source_type = $request->get('source_type', '0'); // 1:中原畅立收
            $store_logo_img = $request->get('store_logo_img', '');
            $store_img_a = $request->get('store_img_a', '');
            $store_img_b = $request->get('store_img_b', '');
            $store_img_c = $request->get('store_img_c', '');
            $category_id = $request->get('category_id', ''); //经营类型
            $category_name = $request->get('category_name', '');//经营类型名称

            //第四步 4、功能信息
            if ($saveStep == 1) {
                $validate = Validator::make($data, [
                    'company' => 'required',
                    'store_name' => 'required',
                    'people' => 'required',
                    'people_phone' => 'required',
                    'province_code' => 'required',
                    'city_code' => 'required',
                    'area_code' => 'required',
                    'store_address' => 'required',
                    'head_name' => 'required',
                    'head_sfz_no' => 'required',
                    'head_sfz_time' => 'required',
                    'head_sfz_stime' => 'required',
                    'store_type' => 'required',
                    'store_type_name' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'company' => '通道类型',
                    'store_name' => '门店名称',
                    'people' => '联系人',
                    'people_phone' => '手机号',
                    'province_code' => '区域',
                    'city_code' => '区域',
                    'area_code' => '区域',
                    'store_address' => '详细地址',
                    'head_name' => '法人',
                    'head_sfz_no' => '身份证号',
                    'head_sfz_time' => '证件成立日期',
                    'head_sfz_stime' => '证件有效期',
                    'store_type' => '企业类型',
                    'store_type_name' => '企业类型',
                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                //开启事务
                try {
                    DB::beginTransaction();

                    if ($addCompayType == '0') { //新增门店

                        $store_id = date('Ymdhis', time()) . rand(1000, 9999);//随机

                        //拼装门店信息
                        $stores = [
                            'config_id' => $config_id,
                            'user_id' => $user_id,
                            'merchant_id' => '',
                            'store_id' => $store_id,
                            'store_name' => trim($store_name),
                            'store_type_name' => trim($store_type_name),
                            'store_email' => trim($store_email),
                            'store_short_name' => trim($store_short_name),
                            'people' => trim($people),//
                            'people_phone' => trim($people_phone),
                            'province_code' => $province_code,
                            'city_code' => $city_code,
                            'area_code' => $area_code,
                            'province_name' => $province_name,
                            'city_name' => $city_name,
                            'area_name' => $area_name,
                            'store_address' => trim($store_address),
                            'head_name' => trim($head_name), //法人
                            'head_sfz_no' => trim($head_sfz_no),
                            'head_sfz_time' => $head_sfz_time,
                            'head_sfz_stime' => $head_sfz_stime,
                            'store_type' => $store_type,
                            'status' => 1,
                            'status_desc' => "自动审核通过",
                            'admin_status' => 1,
                            'admin_status_desc' => "自动审核通过",
                            'weixin_no' => "",
                            'alipay_name' => "",
                            'alipay_account' => "",
                            'source' => $user->source, ////来源,01:畅立收，02:河南畅立收
                        ];

                        if ($store_type == '1' || $store_type == '2') {
                            $stores['store_license_no'] = $store_license_no;
                            $stores['store_license_time'] = $store_license_time;
                            $stores['store_license_stime'] = $store_license_stime;
                        }

                        //图片信息
                        $store_imgs = [
                            'store_id' => $store_id,
                            'head_sfz_img_a' => $head_sfz_img_a,
                            'head_sfz_img_b' => $head_sfz_img_b
                        ];
                        if ($store_type == '1' || $store_type == '2') {
                            $store_imgs['store_license_img'] = $store_license_img;
                        }

                        //80开头+当前日期+4位随机数
                        $merchantNo = $this->generateMerchantNo();
                        //检查是否存在商户号
                        $existMerchantNo = Merchant::where('merchant_no', $merchantNo)->first();
                        if (!$existMerchantNo) {
                            $merchantNo = $this->generateMerchantNo(); //重新获取
                        }
                        //注册merchants账户
                        $merchantData = [
                            'pid' => 0,
                            'type' => 1, //1、店长 2 收银员
                            'name' => $people,
                            'password' => bcrypt('000000'),
                            'pay_password' => bcrypt('000000'),
                            'phone' => $people_phone,
                            'user_id' => $user_id,
                            'config_id' => $config_id,
                            'wx_openid' => '',
                            'source' => $user->source, //来源,01:畅立收，02:河南畅立收
                            'merchant_no' => $merchantNo
                        ];
                        $merchant = Merchant::create($merchantData);
                        $merchant_id = $merchant->id;

                        $stores['merchant_id'] = $merchant_id;
                        MerchantStore::create([
                            'store_id' => $store_id,
                            'merchant_id' => $merchant_id
                        ]);

                        Store::create($stores);
                        StoreImg::create($store_imgs);

                        //初始化费率 默认0.38

                        $data['store_id'] = $store_id;
                        $data['rate'] = 0.38;
                        $data['company'] = $company;
                        $retStorePayWay = $this->app_send_ways_data($data);

                        if ($retStorePayWay['status'] == -1) {
                            DB::rollBack();
                            return json_encode([
                                'status' => -1,
                                'message' => $retStorePayWay['message']
                            ]);
                        }
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();
                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '账户信息'
                        ]);
                        $storeSteps->save();
                    } else {
                        StoreSteps::create([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                    }


                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 2) {
                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_bank_type' => 'required',
                    'store_bank_name' => 'required',
                    'store_bank_no' => 'required',
                    'bank_no' => 'required',
                    'sub_bank_name' => 'required',
                    'bank_name' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_bank_type' => '结算类型',
                    'store_bank_name' => '开户名称',
                    'store_bank_no' => '银行卡号',
                    'bank_no' => '联行号',
                    'sub_bank_name' => '支行名称',
                    'bank_name' => '银行名称',

                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();
                $storeBank = StoreBank::where('store_id', $store_id)->first();

                //开启事务
                try {
                    DB::beginTransaction();

                    //银行卡信息
                    $store_banks = [
                        'store_id' => $store_id,
                        'store_bank_type' => $store_bank_type,
                        'store_bank_name' => $store_bank_name,
                        'store_bank_no' => $store_bank_no,
                        'bank_no' => $bank_no,
                        'sub_bank_name' => $sub_bank_name,
                        'bank_name' => $bank_name,
                        'reserved_mobile' => $reserved_mobile,
                        'store_bank_phone' => $store->people_phone,
                        'bank_province_code' => $store->province_code,
                        'bank_city_code' => $store->city_code,
                        'bank_area_code' => $store->area_code,
                    ];
                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                    ];

                    if ($bankType == '01') {
                        $store_banks['bank_sfz_no'] = $store->head_sfz_no;
                        $store_banks['bank_sfz_stime'] = $store->head_sfz_stime;
                        $store_banks['bank_sfz_time'] = $store->head_sfz_time;

                        $store_imgs['bank_img_a'] = $bank_img_a;
                        $store_imgs['bank_img_b'] = $bank_img_b;
                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                        $store_imgs['store_auth_bank_img'] = $store_auth_bank_img;

                        $store_imgs['bank_sc_img'] = $bank_sc_img;
                        $store_imgs['bank_sfz_img_a'] = "";
                        $store_imgs['bank_sfz_img_b'] = "";
                    } else if ($bankType == '02') {
                        $store_banks['bank_sfz_no'] = $store->head_sfz_no;
                        $store_banks['bank_sfz_stime'] = $store->head_sfz_stime;
                        $store_banks['bank_sfz_time'] = $store->head_sfz_time;

                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                        $store_imgs['bank_img_a'] = "";
                        $store_imgs['bank_img_b'] = "";
                        $store_imgs['bank_sc_img'] = "";
                        $store_imgs['bank_sfz_img_a'] = "";
                        $store_imgs['bank_sfz_img_b'] = "";
                    } else if ($bankType == '03') {
                        $store_banks['bank_sfz_no'] = $bank_sfz_no;
                        $store_banks['bank_sfz_stime'] = $bank_sfz_stime;
                        $store_banks['bank_sfz_time'] = $bank_sfz_time;
                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                        $store_imgs['store_auth_bank_img'] = $store_auth_bank_img;

                        $store_imgs['bank_img_a'] = $bank_img_a;
                        $store_imgs['bank_img_b'] = $bank_img_b;
                        $store_imgs['bank_sc_img'] = $bank_sc_img;
                        $store_imgs['bank_sfz_img_a'] = $bank_sfz_img_a;
                        $store_imgs['bank_sfz_img_b'] = $bank_sfz_img_b;
                    }

                    if ($storeBank) {
                        $storeBank->update(array_filter($store_banks));
                        $storeBank->save();
                    } else {
                        StoreBank::create($store_banks);
                    }

                    if ($storeImg) {
                        $storeImg->update(array_filter($store_imgs));
                        $storeImg->save();
                    } else {
                        StoreImg::create($store_imgs);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '账户信息'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }

            } else if ($saveStep == 3) {

                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_logo_img' => 'required',
                    'store_img_a' => 'required',
                    'store_img_b' => 'required',
                    'store_img_c' => 'required',
                    'category_id' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_logo_img' => '门头照',
                    'store_img_a' => '收银台照',
                    'store_img_b' => '经营内容照',
                    'store_img_c' => '店内全景照',
                    'category_id' => '经营类型',

                ]);

                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }

                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();

                //开启事务
                try {
                    DB::beginTransaction();

                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                        'store_logo_img' => $store_logo_img,
                        'store_img_a' => $store_img_a,
                        'store_img_b' => $store_img_b,
                        'store_img_c' => $store_img_c,
                    ];

                    if ($storeImg) {
                        $storeImg->update(array_filter($store_imgs));
                        $storeImg->save();
                    } else {
                        StoreImg::create($store_imgs);
                    }
                    if ($store) {
                        $stores = [
                            'store_id' => $store_id,
                            'category_id' => $category_id,
                            'category_name' => $category_name,
                            'source_type' => $source_type,
                        ];
                        $store->update(array_filter($stores));
                        $store->save();
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();
                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '门店信息'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 4) {

                $rate = $request->get('rate', '');

                $store = Store::where('store_id', $store_id)
                    ->first();
                if (!$store) {
                    return json_encode(['status' => -1, 'message' => '门店不存在']);
                }
                //通道支付费率 storePayWay
                if ($rate > 0.40) {
                    return json_encode(['status' => -1, 'message' => '设置费率超限']);
                }

                try {
                    DB::beginTransaction();
                    //查找下级门店共享通道
                    if ($store->pid == 0) {
                        $sub_store = Store::where('pid', $store->id)
                            ->where('pay_ways_type', 1)
                            ->select('store_id')
                            ->get();
                        foreach ($sub_store as $k => $v) {
                            $data['store_id'] = $v->store_id;
                            $data['rate'] = $rate;
                            $data['company'] = $company;

                            $this->app_send_ways_data($data);
                        }
                    }

                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['company'] = $company;
                    $retStorePayWay = $this->app_send_ways_data($data);

                    if ($retStorePayWay['status'] == -1) {
                        DB::rollBack();
                        return json_encode([
                            'status' => -1,
                            'message' => $retStorePayWay['message']
                        ]);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '提交审核'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();

                    $code = '';
                    $SettleModeType = '';//结算方式
                    $spwObj = new StorePayWaysController();

                    $store_ways_desc = DB::table('store_ways_desc')
                        ->where('is_show', '1')
                        ->where('company', $company)
                        ->first();
                    $type = $store_ways_desc->ways_type;

                    return $spwObj->base_open_ways($type, $code, $store_id, $SettleModeType, '', '');

                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }

            }

            return json_encode([
                'status' => '1',
                'message' => '操作成功',
                'data' => [
                    'store_id' => $store_id
                ]
            ]);
        } catch (\Exception $ex) {
            Log::info($ex);
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    public function update_vbill_store(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_id = $request->get('user_id', '');
            $data = $request->except(['token']);
            $store_id = $request->get('storeId', '');

            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => -1,
                    'message' => '用户不存在'
                ]);
            }
            $userAuths = UserAuths::where('user_id', $user_id)->where('user_type', '1')->first();
            if (!$userAuths) {
                return json_encode([
                    'status' => -1,
                    'message' => '未实名认证，请先进行实名认证'
                ]);
            }
            //检查是否签署协议
//            $esignAuth = EsignAuths::where('user_id', $user_id)->first();
//            if (!$esignAuth) {
//                return json_encode([
//                    'status' => -1,
//                    'message' => '请先进行协议签署'
//                ]);
//            } else {
//                if ($esignAuth->sign_flow_status != '2') {
//                    return json_encode([
//                        'status' => -1,
//                        'message' => '请先完成协议签署'
//                    ]);
//                }
//            }

            $config_id = $token->config_id;

            $saveStep = $request->get('saveStep', '1'); // 1执照信息 、2账户信息 3、门店信息 4、功能信息 5、
            $company = $request->get('company', 'vbill');
            //1、执照信息
            $store_type_name = $request->get('store_type_name', '');
            $store_type = $request->get('store_type', ''); //门店性质 1-个体，2-企业，3-小微商户
            $store_name = $request->get('store_name', '');
            $store_short_name = $request->get('store_short_name', $store_name);
            $store_name = $request->get('store_name', '');
            $store_address = $request->get('store_address', '');
            if ($store_type == '1') {
                $store_type_name = '个体';
            } else if ($store_type == '2') {
                $store_type_name = '企业';
            } else if ($store_type == '3') {
                $store_type_name = '小微商户';
            }
            if ($store_type == '1' || $store_type == '2') {
                $store_license_no = $request->get('store_license_no', '');
                $store_license_img = $request->get('store_license_img', '');
                $store_license_time = $request->get('store_license_time', '');
                $store_license_stime = $request->get('store_license_stime', '');
            }

            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');
            $province_name = $request->get('province_name', '');
            $city_name = $request->get('city_name', '');
            $area_name = $request->get('area_name', '');
            $people_phone = $request->get('people_phone', '');
            $store_email = $request->get('store_email', '');
            $head_name = $request->get('head_name', '');
            $head_sfz_no = $request->get('head_sfz_no', '');
            $people = $request->get('people', '');
            $head_sfz_img_a = $request->get('head_sfz_img_a', '');  //法人身份证正面
            $head_sfz_img_b = $request->get('head_sfz_img_b', '');  //法人身份证反面

            $head_sfz_stime = $request->get('head_sfz_stime', ''); //法人身份证开始日期
            $head_sfz_time = $request->get('head_sfz_time', ''); //法人身份证结束日期
            if ($head_sfz_time != '长期') {
                $head_sfz_time = $this->time($head_sfz_time);
            }
            $head_sfz_stime = $this->time($head_sfz_stime);

            //第二步 2、账户信息

            $bankType = $request->get('bankType', ''); // 01 法人对公 02对公 03非法人对私
            $store_bank_type = $request->get('store_bank_type', ''); //01 对私 02 对公
            $store_bank_name = $request->get('store_bank_name', '');
            $store_bank_no = $request->get('store_bank_no', '');    //银行卡号
            $bank_no = $request->get('bank_no', '');    //联行号
            $sub_bank_name = $request->get('sub_bank_name', '');   //支行名称
            $bank_name = $request->get('bank_name', '');    //银行名称
            $reserved_mobile = $request->get('reserved_mobile', '');    //预留手机号
            $store_industrylicense_img = $request->get('store_industrylicense_img', '');//开户许可证
            $store_auth_bank_img = $request->get('certOfAuth', ''); //清算授权书图片地址(非必填)

            if ($bankType == '01') {
                $bank_img_a = $request->get('bank_img_a', '');
                $bank_img_b = $request->get('bank_img_b', '');
                $bank_sc_img = $request->get('bank_sc_img', '');
            } else if ($bankType == '02') {

            } else if ($bankType == '03') {
                $bank_img_a = $request->get('bank_img_a', '');
                $bank_img_b = $request->get('bank_img_b', '');
                $bank_sc_img = $request->get('bank_sc_img', '');
                $bank_sfz_img_a = $request->get('bank_sfz_img_a', '');
                $bank_sfz_img_b = $request->get('bank_sfz_img_b', '');

                $bank_sfz_no = $request->get('bank_sfz_no', '');
                $bank_sfz_stime = $request->get('bank_sfz_stime', '');
                $bank_sfz_time = $request->get('bank_sfz_time', '');
                if ($bank_sfz_time != '长期') {
                    $bank_sfz_time = $this->time($bank_sfz_time);
                }
                $bank_sfz_stime = $this->time($bank_sfz_stime);
            }

            //第三步 3、门店信息

            //门店来源 比如 中原畅立收
            $source_type = $request->get('source_type', '0'); // 1:中原畅立收
            $store_logo_img = $request->get('store_logo_img', '');
            $store_img_a = $request->get('store_img_a', '');
            $store_img_b = $request->get('store_img_b', '');
            $store_img_c = $request->get('store_img_c', '');
            $category_id = $request->get('category_id', ''); //经营类型
            $category_name = $request->get('category_name', '');//经营类型名称

            //第四步 4、功能信息

            if ($saveStep == 1) {
                $validate = Validator::make($data, [
                    'store_name' => 'required',
                    'people' => 'required',
                    'people_phone' => 'required',
                    'province_code' => 'required',
                    'city_code' => 'required',
                    'area_code' => 'required',
                    'store_address' => 'required',
                    'head_name' => 'required',
                    'head_sfz_no' => 'required',
                    'head_sfz_time' => 'required',
                    'head_sfz_stime' => 'required',
                    'store_type' => 'required',
                    'store_type_name' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'store_name' => '门店名称',
                    'people' => '联系人',
                    'people_phone' => '手机号',
                    'province_code' => '区域',
                    'city_code' => '区域',
                    'area_code' => '区域',
                    'store_address' => '详细地址',
                    'head_name' => '法人',
                    'head_sfz_no' => '身份证号',
                    'head_sfz_time' => '证件成立日期',
                    'head_sfz_stime' => '证件有效期',
                    'store_type' => '企业类型',
                    'store_type_name' => '企业类型',
                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                //开启事务
                try {
                    DB::beginTransaction();

                    $merchantStore = MerchantStore::where('store_id', $store_id)->select('merchant_id')->first();
                    if ($merchantStore) {
                        $upIN = [
                            'name' => $people,
                            'phone' => $people_phone,
                        ];
                        Merchant::where('id', $merchantStore->merchant_id)->update($upIN);
                    }

                    //拼装门店信息
                    $stores = [
                        'config_id' => $config_id,
                        'user_id' => $user_id,
                        'merchant_id' => '',
                        'store_id' => $store_id,
                        'store_name' => trim($store_name),
                        'store_type_name' => trim($store_type_name),
                        'store_email' => trim($store_email),
                        'store_short_name' => trim($store_short_name),
                        'people' => trim($people),//
                        'people_phone' => trim($people_phone),
                        'province_code' => $province_code,
                        'city_code' => $city_code,
                        'area_code' => $area_code,
                        'province_name' => $province_name,
                        'city_name' => $city_name,
                        'area_name' => $area_name,
                        'store_address' => trim($store_address),
                        'head_name' => trim($head_name), //法人
                        'head_sfz_no' => trim($head_sfz_no),
                        'head_sfz_time' => $head_sfz_time,
                        'head_sfz_stime' => $head_sfz_stime,
                        'store_type' => $store_type,
                        'status' => 1,
                        'status_desc' => "自动审核通过",
                        'admin_status' => 1,
                        'admin_status_desc' => "自动审核通过",
                        'weixin_no' => "",
                        'alipay_name' => "",
                        'alipay_account' => "",
                        'source' => $user->source, ////来源,01:畅立收，02:河南畅立收
                    ];

                    if ($store_type == '1' || $store_type == '2') {
                        $stores['store_license_no'] = $store_license_no;
                        $stores['store_license_time'] = $store_license_time;
                        $stores['store_license_stime'] = $store_license_stime;
                    }

                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                        'head_sfz_img_a' => $head_sfz_img_a,
                        'head_sfz_img_b' => $head_sfz_img_b
                    ];
                    if ($store_type == '1' || $store_type == '2') {
                        $store_imgs['store_license_img'] = $store_license_img;
                    }
                    $store = Store::where('store_id', $store_id)->first();
                    if (!$store) {
                        return json_encode([
                            'status' => 2,
                            'message' => '门店未认证'
                        ]);
                    } else {
                        $merchantInfo = Merchant::where('id', $store->merchant_id)->first();
                        if ($merchantInfo) {
                            $upIN = [
                                'name' => $people,
                                'phone' => $people_phone,
                            ];
                            $merchantInfo->update($upIN);
                        }

                        $storeImg = StoreImg::where('store_id', $store_id)->first();

                        if ($storeImg) {
                            $storeImg->update(array_filter($store_imgs));
                            $storeImg->save();
                        } else {
                            StoreImg::create($store_imgs);
                        }

                        $store->update(array_filter($stores));
                        $store->save();

                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                        $storeSteps->save();
                    } else {
                        StoreSteps::create([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 2) {
                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_bank_type' => 'required',
                    'store_bank_name' => 'required',
                    'store_bank_no' => 'required',
                    'bank_no' => 'required',
                    'sub_bank_name' => 'required',
                    'bank_name' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_bank_type' => '结算类型',
                    'store_bank_name' => '开户名称',
                    'store_bank_no' => '银行卡号',
                    'bank_no' => '联行号',
                    'sub_bank_name' => '支行名称',
                    'bank_name' => '银行名称',

                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();
                $storeBank = StoreBank::where('store_id', $store_id)->first();

                //开启事务
                try {
                    DB::beginTransaction();
                    //银行卡信息
                    $store_banks = [
                        'store_id' => $store_id,
                        'store_bank_type' => $store_bank_type,
                        'store_bank_name' => $store_bank_name,
                        'store_bank_no' => $store_bank_no,
                        'bank_no' => $bank_no,
                        'sub_bank_name' => $sub_bank_name,
                        'bank_name' => $bank_name,
                        'reserved_mobile' => $reserved_mobile,
                        'store_bank_phone' => $store->people_phone,
                        'bank_province_code' => $store->province_code,
                        'bank_city_code' => $store->city_code,
                        'bank_area_code' => $store->area_code,
                    ];
                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                    ];

                    if ($bankType == '01') {
                        $store_banks['bank_sfz_no'] = $store->head_sfz_no;
                        $store_banks['bank_sfz_stime'] = $store->head_sfz_stime;
                        $store_banks['bank_sfz_time'] = $store->head_sfz_time;

                        $store_imgs['bank_img_a'] = $bank_img_a;
                        $store_imgs['bank_img_b'] = $bank_img_b;
                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                        $store_imgs['store_auth_bank_img'] = $store_auth_bank_img;

                        $store_imgs['bank_sc_img'] = $bank_sc_img;
                        $store_imgs['bank_sfz_img_a'] = "";
                        $store_imgs['bank_sfz_img_b'] = "";
                    } else if ($bankType == '02') {
                        $store_banks['bank_sfz_no'] = $store->head_sfz_no;
                        $store_banks['bank_sfz_stime'] = $store->head_sfz_stime;
                        $store_banks['bank_sfz_time'] = $store->head_sfz_time;

                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                        $store_imgs['bank_img_a'] = "";
                        $store_imgs['bank_img_b'] = "";
                        $store_imgs['bank_sc_img'] = "";
                        $store_imgs['bank_sfz_img_a'] = "";
                        $store_imgs['bank_sfz_img_b'] = "";
                    } else if ($bankType == '03') {
                        $store_banks['bank_sfz_no'] = $bank_sfz_no;
                        $store_banks['bank_sfz_stime'] = $bank_sfz_stime;
                        $store_banks['bank_sfz_time'] = $bank_sfz_time;
                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                        $store_imgs['store_auth_bank_img'] = $store_auth_bank_img;

                        $store_imgs['bank_img_a'] = $bank_img_a;
                        $store_imgs['bank_img_b'] = $bank_img_b;
                        $store_imgs['bank_sc_img'] = $bank_sc_img;
                        $store_imgs['bank_sfz_img_a'] = $bank_sfz_img_a;
                        $store_imgs['bank_sfz_img_b'] = $bank_sfz_img_b;
                    }

                    if ($storeBank) {
                        $storeBank->update(array_filter($store_banks));
                        $storeBank->save();
                    } else {
                        StoreBank::create($store_banks);
                    }

                    if ($storeImg) {
                        $storeImg->update(array_filter($store_imgs));
                        $storeImg->save();
                    } else {
                        StoreImg::create($store_imgs);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '账户信息'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }

            } else if ($saveStep == 3) {

                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_logo_img' => 'required',
                    'store_img_a' => 'required',
                    'store_img_b' => 'required',
                    'store_img_c' => 'required',
                    'category_id' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_logo_img' => '门头照',
                    'store_img_a' => '收银台照',
                    'store_img_b' => '经营内容照',
                    'store_img_c' => '店内全景照',
                    'category_id' => '经营类型',

                ]);

                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }

                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();

                //开启事务
                try {
                    DB::beginTransaction();
                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                        'store_logo_img' => $store_logo_img,
                        'store_img_a' => $store_img_a,
                        'store_img_b' => $store_img_b,
                        'store_img_c' => $store_img_c,
                    ];

                    if ($storeImg) {
                        $storeImg->update(array_filter($store_imgs));
                        $storeImg->save();
                    } else {
                        StoreImg::create($store_imgs);
                    }
                    if ($store) {
                        $stores = [
                            'store_id' => $store_id,
                            'category_id' => $category_id,
                            'category_name' => $category_name,
                            'source_type' => $source_type,
                        ];
                        $store->update(array_filter($stores));
                        $store->save();
                    }
                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();
                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '门店信息'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 4) {

                $rate = $request->get('rate', '');

                $store = Store::where('store_id', $store_id)
                    ->first();
                if (!$store) {
                    return json_encode(['status' => -1, 'message' => '门店不存在']);
                }
                //通道支付费率 storePayWay
                if ($rate > 0.40) {
                    return json_encode(['status' => -1, 'message' => '设置费率超限']);
                }

                try {
                    DB::beginTransaction();

                    //查找下级门店共享通道
                    if ($store->pid == 0) {
                        $sub_store = Store::where('pid', $store->id)
                            ->where('pay_ways_type', 1)
                            ->select('store_id')
                            ->get();
                        foreach ($sub_store as $k => $v) {
                            $data['store_id'] = $v->store_id;
                            $data['rate'] = $rate;
                            $data['company'] = $company;

                            $this->app_send_ways_data($data);
                        }
                    }

                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['company'] = $company;
                    $retStorePayWay = $this->app_send_ways_data($data);

                    if ($retStorePayWay['status'] == -1) {
                        DB::rollBack();
                        return json_encode([
                            'status' => -1,
                            'message' => $retStorePayWay['message']
                        ]);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();
                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '提交审核'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();

                    $code = '';
                    $SettleModeType = '';//结算方式
                    $spwObj = new StorePayWaysController();

                    $store_ways_desc = DB::table('store_ways_desc')
                        ->where('is_show', '1')
                        ->where('company', $company)
                        ->first();
                    $type = $store_ways_desc->ways_type;

                    return $spwObj->base_open_ways($type, $code, $store_id, $SettleModeType, '', '');

                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }

            }

            return json_encode([
                'status' => '1',
                'message' => '操作成功',
                'data' => [
                    'store_id' => $store_id
                ]
            ]);
        } catch (\Exception $ex) {
            Log::info($ex);
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    /**
     * 银盛通道
     */
    public function add_ysepay_store(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_id = $request->get('user_id', $token->user_id);
            $data = $request->except(['token']);

            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => -1,
                    'message' => '用户不存在'
                ]);
            }
//            $userAuths = UserAuths::where('user_id', $user_id)->where('user_type', '1')->first();
//            if (!$userAuths) {
//                return json_encode([
//                    'status' => -1,
//                    'message' => '未实名认证，请先进行实名认证'
//                ]);
//            }
            //检查是否签署协议
//            $esignAuth = EsignAuths::where('user_id', $user_id)->first();
//            if (!$esignAuth) {
//                return json_encode([
//                    'status' => -1,
//                    'message' => '请先进行协议签署'
//                ]);
//            } else {
//                if ($esignAuth->sign_flow_status != '2') {
//                    return json_encode([
//                        'status' => -1,
//                        'message' => '请先完成协议签署'
//                    ]);
//                }
//            }

            $config_id = $token->config_id;

            $addCompayType = $request->get('addCompayType', '0'); // 是否存在通道，该通道为新增。默认0不存在，1已存在
            $saveStep = $request->get('saveStep', '1'); // 1执照信息 、2账户信息 3、门店信息 4、功能信息 5、

            $store_id = '';
            if ($saveStep != 1 || $addCompayType == '1') {
                $store_id = $request->get('storeId', '');
            }
            //1、执照信息
            $company = $request->get('company', 'ysepay'); //默认
            $store_type_name = $request->get('store_type_name', '');
            $store_type = $request->get('store_type', ''); //门店性质 1-个体，2-企业，3-小微商户
            $store_name = $request->get('store_name', '');
            $store_short_name = $request->get('store_short_name', $store_name);
            $store_name = $request->get('store_name', '');
            $store_address = $request->get('store_address', '');
            if ($store_type == '1') {
                $store_type_name = '个体';
            } else if ($store_type == '2') {
                $store_type_name = '企业';
            } else if ($store_type == '3') {
                $store_type_name = '小微商户';
            }
            if ($store_type == '1' || $store_type == '2') {
                $store_license_no = $request->get('store_license_no', '');
                $store_license_img = $request->get('store_license_img', '');
                $store_license_time = $request->get('store_license_time', '');
                $store_license_stime = $request->get('store_license_stime', '');
            }

            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');
            $province_name = $request->get('province_name', '');
            $city_name = $request->get('city_name', '');
            $area_name = $request->get('area_name', '');
            $people_phone = $request->get('people_phone', '');
            $store_email = $request->get('store_email', '');
            $head_name = $request->get('head_name', '');
            $head_sfz_no = $request->get('head_sfz_no', '');
            $people = $request->get('people', '');
            $head_sfz_img_a = $request->get('head_sfz_img_a', '');  //法人身份证正面
            $head_sfz_img_b = $request->get('head_sfz_img_b', '');  //法人身份证反面

            $head_sfz_stime = $request->get('head_sfz_stime', ''); //法人身份证开始日期
            $head_sfz_time = $request->get('head_sfz_time', ''); //法人身份证结束日期
            if ($head_sfz_time != '长期') {
                $head_sfz_time = $this->time($head_sfz_time);
            }
            $head_sfz_stime = $this->time($head_sfz_stime);

            //第二步 2、账户信息

            $bankType = $request->get('bankType', ''); // 01 法人对公 02对公 03非法人对私
            $store_bank_type = $request->get('store_bank_type', ''); //01 对私 02 对公
            $store_bank_name = $request->get('store_bank_name', '');
            $store_bank_no = $request->get('store_bank_no', '');    //银行卡号
            $bank_no = $request->get('bank_no', '');    //联行号
            $sub_bank_name = $request->get('sub_bank_name', '');   //支行名称
            $bank_name = $request->get('bank_name', '');    //银行名称
            $reserved_mobile = $request->get('reserved_mobile', '');    //预留手机号
            $store_industrylicense_img = $request->get('store_industrylicense_img', '');//开户许可证

            if ($bankType == '01') {
                $bank_img_a = $request->get('bank_img_a', '');
                $bank_img_b = $request->get('bank_img_b', '');
                $bank_sc_img = $request->get('bank_sc_img', '');
            } else if ($bankType == '02') {

            } else if ($bankType == '03') {
                $bank_img_a = $request->get('bank_img_a', '');
                $bank_img_b = $request->get('bank_img_b', '');
                $bank_sc_img = $request->get('bank_sc_img', '');
                $bank_sfz_img_a = $request->get('bank_sfz_img_a', '');
                $bank_sfz_img_b = $request->get('bank_sfz_img_b', '');
                $store_auth_bank_img = $request->get('store_auth_bank_img', '');//非法人结算，结算授权书

                $bank_sfz_no = $request->get('bank_sfz_no', '');
                $bank_sfz_stime = $request->get('bank_sfz_stime', '');
                $bank_sfz_time = $request->get('bank_sfz_time', '');
                if ($bank_sfz_time != '长期') {
                    $bank_sfz_time = $this->time($bank_sfz_time);
                }
                $bank_sfz_stime = $this->time($bank_sfz_stime);
            }

            //第三步 3、门店信息

            //门店来源 比如 中原畅立收
            $source_type = $request->get('source_type', '0'); // 1:中原畅立收
            $store_logo_img = $request->get('store_logo_img', '');
            $store_img_a = $request->get('store_img_a', '');
            $store_img_b = $request->get('store_img_b', '');
            $store_img_c = $request->get('store_img_c', '');
            $category_id = $request->get('category_id', ''); //经营类型
            $category_name = $request->get('category_name', '');//经营类型名称

            //第四步 4、功能信息
            if ($saveStep == 1) {
                $validate = Validator::make($data, [
                    'company' => 'required',
                    'store_name' => 'required',
                    'people' => 'required',
                    'people_phone' => 'required',
                    'province_code' => 'required',
                    'city_code' => 'required',
                    'area_code' => 'required',
                    'store_address' => 'required',
                    'head_name' => 'required',
                    'head_sfz_no' => 'required',
                    'head_sfz_time' => 'required',
                    'head_sfz_stime' => 'required',
                    'store_type' => 'required',
                    'store_type_name' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'company' => '通道类型',
                    'store_name' => '门店名称',
                    'people' => '联系人',
                    'people_phone' => '手机号',
                    'province_code' => '区域',
                    'city_code' => '区域',
                    'area_code' => '区域',
                    'store_address' => '详细地址',
                    'head_name' => '法人',
                    'head_sfz_no' => '身份证号',
                    'head_sfz_time' => '证件成立日期',
                    'head_sfz_stime' => '证件有效期',
                    'store_type' => '企业类型',
                    'store_type_name' => '企业类型',
                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                //开启事务
                try {
                    DB::beginTransaction();

                    if ($addCompayType == '0') { //新增门店

                        $store_id = date('Ymdhis', time()) . rand(1000, 9999);//随机

                        //拼装门店信息
                        $stores = [
                            'config_id' => $config_id,
                            'user_id' => $user_id,
                            'merchant_id' => '',
                            'store_id' => $store_id,
                            'store_name' => trim($store_name),
                            'store_type_name' => trim($store_type_name),
                            'store_email' => trim($store_email),
                            'store_short_name' => trim($store_short_name),
                            'people' => trim($people),//
                            'people_phone' => trim($people_phone),
                            'province_code' => $province_code,
                            'city_code' => $city_code,
                            'area_code' => $area_code,
                            'province_name' => $province_name,
                            'city_name' => $city_name,
                            'area_name' => $area_name,
                            'store_address' => trim($store_address),
                            'head_name' => trim($head_name), //法人
                            'head_sfz_no' => trim($head_sfz_no),
                            'head_sfz_time' => $head_sfz_time,
                            'head_sfz_stime' => $head_sfz_stime,
                            'store_type' => $store_type,
                            'status' => 1,
                            'status_desc' => "自动审核通过",
                            'admin_status' => 1,
                            'admin_status_desc' => "自动审核通过",
                            'weixin_no' => "",
                            'alipay_name' => "",
                            'alipay_account' => "",
                            'source' => $user->source, ////来源,01:畅立收，02:河南畅立收
                        ];

                        if ($store_type == '1' || $store_type == '2') {
                            $stores['store_license_no'] = $store_license_no;
                            $stores['store_license_time'] = $store_license_time;
                            $stores['store_license_stime'] = $store_license_stime;
                        }

                        //图片信息
                        $store_imgs = [
                            'store_id' => $store_id,
                            'head_sfz_img_a' => $head_sfz_img_a,
                            'head_sfz_img_b' => $head_sfz_img_b
                        ];
                        if ($store_type == '1' || $store_type == '2') {
                            $store_imgs['store_license_img'] = $store_license_img;
                        }

                        //80开头+当前日期+4位随机数
                        $merchantNo = $this->generateMerchantNo();
                        //检查是否存在商户号
                        $existMerchantNo = Merchant::where('merchant_no', $merchantNo)->first();
                        if (!$existMerchantNo) {
                            $merchantNo = $this->generateMerchantNo(); //重新获取
                        }

                        //注册merchants账户
                        $merchantData = [
                            'pid' => 0,
                            'type' => 1, //1、店长 2 收银员
                            'name' => $people,
                            'password' => bcrypt('000000'),
                            'pay_password' => bcrypt('000000'),
                            'phone' => $people_phone,
                            'user_id' => $user_id,
                            'config_id' => $config_id,
                            'wx_openid' => '',
                            'source' => $user->source, //来源,01:畅立收，02:河南畅立收
                            'merchant_no' => $merchantNo
                        ];
                        $merchant = Merchant::create($merchantData);
                        $merchant_id = $merchant->id;

                        $stores['merchant_id'] = $merchant_id;
                        MerchantStore::create([
                            'store_id' => $store_id,
                            'merchant_id' => $merchant_id
                        ]);

                        Store::create($stores);
                        StoreImg::create($store_imgs);

                        //初始化费率 默认0.38

                        $data['store_id'] = $store_id;
                        $data['rate'] = 0.38;
                        $data['company'] = $company;
                        $retStorePayWay = $this->app_send_ways_data($data);

                        if ($retStorePayWay['status'] == -1) {
                            DB::rollBack();
                            return json_encode([
                                'status' => -1,
                                'message' => $retStorePayWay['message']
                            ]);
                        }
                    }
                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                        $storeSteps->save();
                    } else {
                        StoreSteps::create([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 2) {
                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_bank_type' => 'required',
                    'store_bank_name' => 'required',
                    'store_bank_no' => 'required',
                    'bank_no' => 'required',
                    'sub_bank_name' => 'required',
                    'bank_name' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_bank_type' => '结算类型',
                    'store_bank_name' => '开户名称',
                    'store_bank_no' => '银行卡号',
                    'bank_no' => '联行号',
                    'sub_bank_name' => '支行名称',
                    'bank_name' => '银行名称',

                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();
                $storeBank = StoreBank::where('store_id', $store_id)->first();

                //开启事务
                try {
                    DB::beginTransaction();

                    //银行卡信息
                    $store_banks = [
                        'store_id' => $store_id,
                        'store_bank_type' => $store_bank_type,
                        'store_bank_name' => $store_bank_name,
                        'store_bank_no' => $store_bank_no,
                        'bank_no' => $bank_no,
                        'sub_bank_name' => $sub_bank_name,
                        'bank_name' => $bank_name,
                        'reserved_mobile' => $reserved_mobile,
                        'store_bank_phone' => $store->people_phone,
                        'bank_province_code' => $store->province_code,
                        'bank_city_code' => $store->city_code,
                        'bank_area_code' => $store->area_code,
                    ];
                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                    ];

                    if ($bankType == '01') {
                        $store_banks['bank_sfz_no'] = $store->head_sfz_no;
                        $store_banks['bank_sfz_stime'] = $store->head_sfz_stime;
                        $store_banks['bank_sfz_time'] = $store->head_sfz_time;

                        $store_imgs['bank_img_a'] = $bank_img_a;
                        $store_imgs['bank_img_b'] = $bank_img_b;
                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;

                        $store_imgs['bank_sc_img'] = $bank_sc_img;
                        $store_imgs['bank_sfz_img_a'] = "";
                        $store_imgs['bank_sfz_img_b'] = "";
                    } else if ($bankType == '02') {
                        $store_banks['bank_sfz_no'] = $store->head_sfz_no;
                        $store_banks['bank_sfz_stime'] = $store->head_sfz_stime;
                        $store_banks['bank_sfz_time'] = $store->head_sfz_time;

                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                        $store_imgs['bank_img_a'] = "";
                        $store_imgs['bank_img_b'] = "";
                        $store_imgs['bank_sc_img'] = "";
                        $store_imgs['bank_sfz_img_a'] = "";
                        $store_imgs['bank_sfz_img_b'] = "";
                    } else if ($bankType == '03') {
                        $store_banks['bank_sfz_no'] = $bank_sfz_no;
                        $store_banks['bank_sfz_stime'] = $bank_sfz_stime;
                        $store_banks['bank_sfz_time'] = $bank_sfz_time;
                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                        $store_imgs['store_auth_bank_img'] = $store_auth_bank_img;

                        $store_imgs['bank_img_a'] = $bank_img_a;
                        $store_imgs['bank_img_b'] = $bank_img_b;
                        $store_imgs['bank_sc_img'] = $bank_sc_img;
                        $store_imgs['bank_sfz_img_a'] = $bank_sfz_img_a;
                        $store_imgs['bank_sfz_img_b'] = $bank_sfz_img_b;
                    }

                    if ($storeBank) {
                        $storeBank->update(array_filter($store_banks));
                        $storeBank->save();
                    } else {
                        StoreBank::create($store_banks);
                    }

                    if ($storeImg) {
                        $storeImg->update(array_filter($store_imgs));
                        $storeImg->save();
                    } else {
                        StoreImg::create($store_imgs);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '账户信息'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }

            } else if ($saveStep == 3) {

                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_logo_img' => 'required',
                    'store_img_a' => 'required',
                    'store_img_b' => 'required',
                    'store_img_c' => 'required',
                    'category_id' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_logo_img' => '门头照',
                    'store_img_a' => '收银台照',
                    'store_img_b' => '经营内容照',
                    'store_img_c' => '店内全景照',
                    'category_id' => '经营类型',

                ]);

                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }

                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();

                //开启事务
                try {
                    DB::beginTransaction();

                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                        'store_logo_img' => $store_logo_img,
                        'store_img_a' => $store_img_a,
                        'store_img_b' => $store_img_b,
                        'store_img_c' => $store_img_c,
                    ];

                    if ($storeImg) {
                        $storeImg->update(array_filter($store_imgs));
                        $storeImg->save();
                    } else {
                        StoreImg::create($store_imgs);
                    }
                    if ($store) {
                        $stores = [
                            'store_id' => $store_id,
                            'category_id' => $category_id,
                            'category_name' => $category_name,
                            'source_type' => $source_type,
                        ];
                        $store->update(array_filter($stores));
                        $store->save();
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();
                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '门店信息'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 4) {

                $rate = $request->get('rate', '');

                $store = Store::where('store_id', $store_id)
                    ->first();
                if (!$store) {
                    return json_encode(['status' => -1, 'message' => '门店不存在']);
                }
                //通道支付费率 storePayWay
                if ($rate > 0.40) {
                    return json_encode(['status' => -1, 'message' => '设置费率超限']);
                }

                try {
                    DB::beginTransaction();
                    //查找下级门店共享通道
                    if ($store->pid == 0) {
                        $sub_store = Store::where('pid', $store->id)
                            ->where('pay_ways_type', 1)
                            ->select('store_id')
                            ->get();
                        foreach ($sub_store as $k => $v) {
                            $data['store_id'] = $v->store_id;
                            $data['rate'] = $rate;
                            $data['company'] = $company;

                            $this->app_send_ways_data($data);
                        }
                    }

                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['company'] = $company;
                    $retStorePayWay = $this->app_send_ways_data($data);

                    if ($retStorePayWay['status'] == -1) {
                        DB::rollBack();
                        return json_encode([
                            'status' => -1,
                            'message' => $retStorePayWay['message']
                        ]);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '提交审核'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();

                    $code = '';
                    $SettleModeType = '';//结算方式
                    $spwObj = new StorePayWaysController();

                    $store_ways_desc = DB::table('store_ways_desc')
                        ->where('is_show', '1')
                        ->where('company', $company)
                        ->first();
                    $type = $store_ways_desc->ways_type;

                    return $spwObj->base_open_ways($type, $code, $store_id, $SettleModeType, '', '');

                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 5) { //提交审核


            }

            return json_encode([
                'status' => '1',
                'message' => '操作成功',
                'data' => [
                    'store_id' => $store_id
                ]
            ]);
        } catch (\Exception $ex) {
            Log::info($ex);
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    public function update_ysepay_store(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_id = $request->get('user_id', '');
            $data = $request->except(['token']);
            $store_id = $request->get('storeId', '');

            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => -1,
                    'message' => '用户不存在'
                ]);
            }
//            $userAuths = UserAuths::where('user_id', $user_id)->where('user_type', '1')->first();
//            if (!$userAuths) {
//                return json_encode([
//                    'status' => -1,
//                    'message' => '未实名认证，请先进行实名认证'
//                ]);
//            }
            //检查是否签署协议
//            $esignAuth = EsignAuths::where('user_id', $user_id)->first();
//            if (!$esignAuth) {
//                return json_encode([
//                    'status' => -1,
//                    'message' => '请先进行协议签署'
//                ]);
//            } else {
//                if ($esignAuth->sign_flow_status != '2') {
//                    return json_encode([
//                        'status' => -1,
//                        'message' => '请先完成协议签署'
//                    ]);
//                }
//            }

            $config_id = $token->config_id;

            $saveStep = $request->get('saveStep', '1'); // 1执照信息 、2账户信息 3、门店信息 4、功能信息 5、
            $company = $request->get('company', 'vbill');
            //1、执照信息
            $store_type_name = $request->get('store_type_name', '');
            $store_type = $request->get('store_type', ''); //门店性质 1-个体，2-企业，3-小微商户
            $store_name = $request->get('store_name', '');
            $store_short_name = $request->get('store_short_name', $store_name);
            $store_name = $request->get('store_name', '');
            $store_address = $request->get('store_address', '');
            if ($store_type == '1') {
                $store_type_name = '个体';
            } else if ($store_type == '2') {
                $store_type_name = '企业';
            } else if ($store_type == '3') {
                $store_type_name = '小微商户';
            }
            if ($store_type == '1' || $store_type == '2') {
                $store_license_no = $request->get('store_license_no', '');
                $store_license_img = $request->get('store_license_img', '');
                $store_license_time = $request->get('store_license_time', '');
                $store_license_stime = $request->get('store_license_stime', '');
            }

            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');
            $province_name = $request->get('province_name', '');
            $city_name = $request->get('city_name', '');
            $area_name = $request->get('area_name', '');
            $people_phone = $request->get('people_phone', '');
            $store_email = $request->get('store_email', '');
            $head_name = $request->get('head_name', '');
            $head_sfz_no = $request->get('head_sfz_no', '');
            $people = $request->get('people', '');
            $head_sfz_img_a = $request->get('head_sfz_img_a', '');  //法人身份证正面
            $head_sfz_img_b = $request->get('head_sfz_img_b', '');  //法人身份证反面

            $head_sfz_stime = $request->get('head_sfz_stime', ''); //法人身份证开始日期
            $head_sfz_time = $request->get('head_sfz_time', ''); //法人身份证结束日期
            if ($head_sfz_time != '长期') {
                $head_sfz_time = $this->time($head_sfz_time);
            }
            $head_sfz_stime = $this->time($head_sfz_stime);

            //第二步 2、账户信息

            $bankType = $request->get('bankType', ''); // 01 法人对公 02对公 03非法人对私
            $store_bank_type = $request->get('store_bank_type', ''); //01 对私 02 对公
            $store_bank_name = $request->get('store_bank_name', '');
            $store_bank_no = $request->get('store_bank_no', '');    //银行卡号
            $bank_no = $request->get('bank_no', '');    //联行号
            $sub_bank_name = $request->get('sub_bank_name', '');   //支行名称
            $bank_name = $request->get('bank_name', '');    //银行名称
            $reserved_mobile = $request->get('reserved_mobile', '');    //预留手机号
            $store_industrylicense_img = $request->get('store_industrylicense_img', '');//开户许可证

            if ($bankType == '01') {
                $bank_img_a = $request->get('bank_img_a', '');
                $bank_img_b = $request->get('bank_img_b', '');
                $bank_sc_img = $request->get('bank_sc_img', '');
            } else if ($bankType == '02') {

            } else if ($bankType == '03') {
                $bank_img_a = $request->get('bank_img_a', '');
                $bank_img_b = $request->get('bank_img_b', '');
                $bank_sc_img = $request->get('bank_sc_img', '');
                $bank_sfz_img_a = $request->get('bank_sfz_img_a', '');
                $bank_sfz_img_b = $request->get('bank_sfz_img_b', '');
                $store_auth_bank_img = $request->get('store_auth_bank_img', '');//非法人结算，结算授权书

                $bank_sfz_no = $request->get('bank_sfz_no', '');
                $bank_sfz_stime = $request->get('bank_sfz_stime', '');
                $bank_sfz_time = $request->get('bank_sfz_time', '');
                if ($bank_sfz_time != '长期') {
                    $bank_sfz_time = $this->time($bank_sfz_time);
                }
                $bank_sfz_stime = $this->time($bank_sfz_stime);
            }

            //第三步 3、门店信息

            //门店来源 比如 中原畅立收
            $source_type = $request->get('source_type', '0'); // 1:中原畅立收
            $store_logo_img = $request->get('store_logo_img', '');
            $store_img_a = $request->get('store_img_a', '');
            $store_img_b = $request->get('store_img_b', '');
            $store_img_c = $request->get('store_img_c', '');
            $category_id = $request->get('category_id', ''); //经营类型
            $category_name = $request->get('category_name', '');//经营类型名称

            //第四步 4、功能信息

            if ($saveStep == 1) {
                $validate = Validator::make($data, [
                    'store_name' => 'required',
                    'people' => 'required',
                    'people_phone' => 'required',
                    'province_code' => 'required',
                    'city_code' => 'required',
                    'area_code' => 'required',
                    'store_address' => 'required',
                    'head_name' => 'required',
                    'head_sfz_no' => 'required',
                    'head_sfz_time' => 'required',
                    'head_sfz_stime' => 'required',
                    'store_type' => 'required',
                    'store_type_name' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'store_name' => '门店名称',
                    'people' => '联系人',
                    'people_phone' => '手机号',
                    'province_code' => '区域',
                    'city_code' => '区域',
                    'area_code' => '区域',
                    'store_address' => '详细地址',
                    'head_name' => '法人',
                    'head_sfz_no' => '身份证号',
                    'head_sfz_time' => '证件成立日期',
                    'head_sfz_stime' => '证件有效期',
                    'store_type' => '企业类型',
                    'store_type_name' => '企业类型',
                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                //开启事务
                try {
                    DB::beginTransaction();

                    $merchantStore = MerchantStore::where('store_id', $store_id)->select('merchant_id')->first();
                    if ($merchantStore) {
                        $upIN = [
                            'name' => $people,
                            'phone' => $people_phone,
                        ];
                        Merchant::where('id', $merchantStore->merchant_id)->update($upIN);
                    }

                    //拼装门店信息
                    $stores = [
                        'config_id' => $config_id,
                        //'user_id' => $user_id,
                        'merchant_id' => '',
                        'store_id' => $store_id,
                        //'store_name' => trim($store_name),
                        'store_type_name' => trim($store_type_name),
                        'store_email' => trim($store_email),
                        'store_short_name' => trim($store_short_name),
                        'people' => trim($people),//
                        'people_phone' => trim($people_phone),
                        'province_code' => $province_code,
                        'city_code' => $city_code,
                        'area_code' => $area_code,
                        'province_name' => $province_name,
                        'city_name' => $city_name,
                        'area_name' => $area_name,
                        'store_address' => trim($store_address),
                        'head_name' => trim($head_name), //法人
                        'head_sfz_no' => trim($head_sfz_no),
                        'head_sfz_time' => $head_sfz_time,
                        'head_sfz_stime' => $head_sfz_stime,
                        'store_type' => $store_type,
                        'status' => 1,
                        'status_desc' => "自动审核通过",
                        'admin_status' => 1,
                        'admin_status_desc' => "自动审核通过",
                        'weixin_no' => "",
                        'alipay_name' => "",
                        'alipay_account' => "",
                        'source' => $user->source, ////来源,01:畅立收，02:河南畅立收
                    ];

                    if ($store_type == '1' || $store_type == '2') {
                        $stores['store_license_no'] = $store_license_no;
                        $stores['store_license_time'] = $store_license_time;
                        $stores['store_license_stime'] = $store_license_stime;
                    }

                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                        'head_sfz_img_a' => $head_sfz_img_a,
                        'head_sfz_img_b' => $head_sfz_img_b
                    ];
                    if ($store_type == '1' || $store_type == '2') {
                        $store_imgs['store_license_img'] = $store_license_img;
                    }
                    $store = Store::where('store_id', $store_id)->first();
                    if (!$store) {
                        return json_encode([
                            'status' => 2,
                            'message' => '门店未认证'
                        ]);
                    } else {
                        $merchantInfo = Merchant::where('id', $store->merchant_id)->first();
                        if ($merchantInfo) {
                            $upIN = [
                                'name' => $people,
                                'phone' => $people_phone,
                            ];
                            $merchantInfo->update($upIN);
                        }

                        $storeImg = StoreImg::where('store_id', $store_id)->first();

                        if ($storeImg) {
                            $storeImg->update(array_filter($store_imgs));
                            $storeImg->save();
                        } else {
                            StoreImg::create($store_imgs);
                        }

                        $store->update(array_filter($stores));
                        $store->save();

                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                        $storeSteps->save();
                    } else {
                        StoreSteps::create([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 2) {
                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_bank_type' => 'required',
                    'store_bank_name' => 'required',
                    'store_bank_no' => 'required',
                    'bank_no' => 'required',
                    'sub_bank_name' => 'required',
                    'bank_name' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_bank_type' => '结算类型',
                    'store_bank_name' => '开户名称',
                    'store_bank_no' => '银行卡号',
                    'bank_no' => '联行号',
                    'sub_bank_name' => '支行名称',
                    'bank_name' => '银行名称',

                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();
                $storeBank = StoreBank::where('store_id', $store_id)->first();

                //开启事务
                try {
                    DB::beginTransaction();
                    //银行卡信息
                    $store_banks = [
                        'store_id' => $store_id,
                        'store_bank_type' => $store_bank_type,
                        'store_bank_name' => $store_bank_name,
                        'store_bank_no' => $store_bank_no,
                        'bank_no' => $bank_no,
                        'sub_bank_name' => $sub_bank_name,
                        'bank_name' => $bank_name,
                        'reserved_mobile' => $reserved_mobile,
                        'store_bank_phone' => $store->people_phone,
                        'bank_province_code' => $store->province_code,
                        'bank_city_code' => $store->city_code,
                        'bank_area_code' => $store->area_code,
                    ];
                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                    ];

                    if ($bankType == '01') {
                        $store_banks['bank_sfz_no'] = $store->head_sfz_no;
                        $store_banks['bank_sfz_stime'] = $store->head_sfz_stime;
                        $store_banks['bank_sfz_time'] = $store->head_sfz_time;

                        $store_imgs['bank_img_a'] = $bank_img_a;
                        $store_imgs['bank_img_b'] = $bank_img_b;
                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;

                        $store_imgs['bank_sc_img'] = $bank_sc_img;
                        $store_imgs['bank_sfz_img_a'] = "";
                        $store_imgs['bank_sfz_img_b'] = "";
                    } else if ($bankType == '02') {
                        $store_banks['bank_sfz_no'] = $store->head_sfz_no;
                        $store_banks['bank_sfz_stime'] = $store->head_sfz_stime;
                        $store_banks['bank_sfz_time'] = $store->head_sfz_time;

                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                        $store_imgs['bank_img_a'] = "";
                        $store_imgs['bank_img_b'] = "";
                        $store_imgs['bank_sc_img'] = "";
                        $store_imgs['bank_sfz_img_a'] = "";
                        $store_imgs['bank_sfz_img_b'] = "";
                    } else if ($bankType == '03') {
                        $store_banks['bank_sfz_no'] = $bank_sfz_no;
                        $store_banks['bank_sfz_stime'] = $bank_sfz_stime;
                        $store_banks['bank_sfz_time'] = $bank_sfz_time;
                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                        $store_imgs['store_auth_bank_img'] = $store_auth_bank_img;

                        $store_imgs['bank_img_a'] = $bank_img_a;
                        $store_imgs['bank_img_b'] = $bank_img_b;
                        $store_imgs['bank_sc_img'] = $bank_sc_img;
                        $store_imgs['bank_sfz_img_a'] = $bank_sfz_img_a;
                        $store_imgs['bank_sfz_img_b'] = $bank_sfz_img_b;
                    }

                    if ($storeBank) {
                        $storeBank->update(array_filter($store_banks));
                        $storeBank->save();
                    } else {
                        StoreBank::create($store_banks);
                    }

                    if ($storeImg) {
                        $storeImg->update(array_filter($store_imgs));
                        $storeImg->save();
                    } else {
                        StoreImg::create($store_imgs);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '账户信息'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }

            } else if ($saveStep == 3) {

                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_logo_img' => 'required',
                    'store_img_a' => 'required',
                    'store_img_b' => 'required',
                    'store_img_c' => 'required',
                    'category_id' => 'required',

                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_logo_img' => '门头照',
                    'store_img_a' => '收银台照',
                    'store_img_b' => '经营内容照',
                    'store_img_c' => '店内全景照',
                    'category_id' => '经营类型',

                ]);

                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }

                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();

                //开启事务
                try {
                    DB::beginTransaction();
                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                        'store_logo_img' => $store_logo_img,
                        'store_img_a' => $store_img_a,
                        'store_img_b' => $store_img_b,
                        'store_img_c' => $store_img_c,
                    ];

                    if ($storeImg) {
                        $storeImg->update(array_filter($store_imgs));
                        $storeImg->save();
                    } else {
                        StoreImg::create($store_imgs);
                    }
                    if ($store) {
                        $stores = [
                            'store_id' => $store_id,
                            'category_id' => $category_id,
                            'category_name' => $category_name,
                            'source_type' => $source_type,
                        ];
                        $store->update(array_filter($stores));
                        $store->save();
                    }
                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();
                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '门店信息'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 4) {

                $rate = $request->get('rate', '');

                $store = Store::where('store_id', $store_id)
                    ->first();
                if (!$store) {
                    return json_encode(['status' => -1, 'message' => '门店不存在']);
                }
                //通道支付费率 storePayWay
                if ($rate > 0.40) {
                    return json_encode(['status' => -1, 'message' => '设置费率超限']);
                }

                try {
                    DB::beginTransaction();

                    //查找下级门店共享通道
                    if ($store->pid == 0) {
                        $sub_store = Store::where('pid', $store->id)
                            ->where('pay_ways_type', 1)
                            ->select('store_id')
                            ->get();
                        foreach ($sub_store as $k => $v) {
                            $data['store_id'] = $v->store_id;
                            $data['rate'] = $rate;
                            $data['company'] = $company;

                            $this->app_send_ways_data($data);
                        }
                    }

                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['company'] = $company;
                    $retStorePayWay = $this->app_send_ways_data($data);

                    if ($retStorePayWay['status'] == -1) {
                        DB::rollBack();
                        return json_encode([
                            'status' => -1,
                            'message' => $retStorePayWay['message']
                        ]);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();
                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '提交审核'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();

                    $code = '';
                    $SettleModeType = '';//结算方式
                    $spwObj = new StorePayWaysController();

                    $store_ways_desc = DB::table('store_ways_desc')
                        ->where('is_show', '1')
                        ->where('company', $company)
                        ->first();
                    $type = $store_ways_desc->ways_type;

                    return $spwObj->base_open_ways($type, $code, $store_id, $SettleModeType, '', '');

                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 5) { //提交审核


            }

            return json_encode([
                'status' => '1',
                'message' => '操作成功',
                'data' => [
                    'store_id' => $store_id
                ]
            ]);
        } catch (\Exception $ex) {
            Log::info($ex);
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    /**
     * 拉卡拉通道
     */
    public function add_lkl_store(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_id = $request->get('user_id', $token->user_id);
            $data = $request->except(['token']);

            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => -1,
                    'message' => '用户不存在'
                ]);
            }

            $config_id = $token->config_id;

            $addCompayType = $request->get('addCompayType', '0'); // 是否存在通道，该通道为新增。默认0不存在，1已存在
            $saveStep = $request->get('saveStep', '1'); // 1执照信息 、2账户信息 3、门店信息 4、功能信息 5、

            $store_id = '';
            if ($saveStep != 1 || $addCompayType == '1') {
                $store_id = $request->get('storeId', '');
            }
            //1、执照信息
            $company = $request->get('company', 'lklpay'); //默认
            $store_type_name = $request->get('store_type_name', '');
            $store_type = $request->get('store_type', ''); //门店性质 1-个体，2-企业，3-小微商户
            $store_name = $request->get('store_name', '');
            $store_short_name = $request->get('store_short_name', $store_name);
            $store_name = $request->get('store_name', '');
            $store_address = $request->get('store_address', '');

            if ($store_type == '1' || $store_type == '2') {
                $store_license_no = $request->get('store_license_no', '');
                $store_license_img = $request->get('store_license_img', '');
                $store_license_time = $request->get('store_license_time', '');
                $store_license_stime = $request->get('store_license_stime', '');
            }

            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');
            $province_name = $request->get('province_name', '');
            $city_name = $request->get('city_name', '');
            $area_name = $request->get('area_name', '');
            $people_phone = $request->get('people_phone', '');
            $store_email = $request->get('store_email', '');
            $head_name = $request->get('head_name', '');
            $head_sfz_no = $request->get('head_sfz_no', '');
            $people = $request->get('people', '');
            $head_sfz_img_a = $request->get('head_sfz_img_a', '');  //法人身份证正面
            $head_sfz_img_b = $request->get('head_sfz_img_b', '');  //法人身份证反面

            $head_sfz_stime = $request->get('head_sfz_stime', ''); //法人身份证开始日期
            $head_sfz_time = $request->get('head_sfz_time', ''); //法人身份证结束日期
            if ($head_sfz_time != '长期') {
                $head_sfz_time = $this->time($head_sfz_time);
            }
            $head_sfz_stime = $this->time($head_sfz_stime);

            //第二步 2、账户信息

            $bankType = $request->get('bankType', ''); // 01 法人对公 02对公 03非法人对私
            $store_bank_type = $request->get('store_bank_type', ''); //01 对私 02 对公
            $store_bank_name = $request->get('store_bank_name', '');
            $store_bank_no = $request->get('store_bank_no', '');    //银行卡号
            $bank_no = $request->get('bank_no', '');    //联行号
            $sub_bank_name = $request->get('sub_bank_name', '');   //支行名称
            $bank_name = $request->get('bank_name', '');    //银行名称
            $reserved_mobile = $request->get('reserved_mobile', '');    //预留手机号
            $store_industrylicense_img = $request->get('store_industrylicense_img', '');//开户许可证
            $settle_province_code = $request->get('settle_province_code', '');
            $settle_province_name = $request->get('settle_province_name', '');
            $settle_city_code = $request->get('settle_city_code', '');
            $settle_city_name = $request->get('settle_city_name', '');
            $openning_bank_code = $request->get('openning_bank_code', '');
            $openning_bank_name = $request->get('openning_bank_name', '');
            $clearing_bank_code = $request->get('clearing_bank_code', '');


            if ($bankType == '01') {
                $bank_img_a = $request->get('bank_img_a', '');
                $bank_img_b = $request->get('bank_img_b', '');
            } else if ($bankType == '02') {

            } else if ($bankType == '03') {
                $bank_img_a = $request->get('bank_img_a', '');
                $bank_img_b = $request->get('bank_img_b', '');
                $bank_sfz_img_a = $request->get('bank_sfz_img_a', '');
                $bank_sfz_img_b = $request->get('bank_sfz_img_b', '');

                $bank_sfz_no = $request->get('bank_sfz_no', '');
                $bank_sfz_stime = $request->get('bank_sfz_stime', '');
                $bank_sfz_time = $request->get('bank_sfz_time', '');
                if ($bank_sfz_time != '长期') {
                    $bank_sfz_time = $this->time($bank_sfz_time);
                }
                $bank_sfz_stime = $this->time($bank_sfz_stime);
                $store_auth_bank_img = $request->get('certOfAuth', '');
            }

            //第三步 3、门店信息

            //门店来源 比如 中原畅立收
            $source_type = $request->get('source_type', '0'); // 1:中原畅立收
            $store_logo_img = $request->get('store_logo_img', '');
            $store_img_a = $request->get('store_img_a', '');
            $store_img_b = $request->get('store_img_b', '');
            $store_img_c = $request->get('store_img_c', '');
            $category_id = $request->get('category_id', ''); //经营类型
            $category_name = $request->get('category_name', '');//经营类型名称
            $mcc_type = $request->get('mcc_type', '');
            $mcc = $request->get('mcc', '');
            $mcc_name = $request->get('mcc_name', '');
            $email = $request->get('email', '');
            $lkl_area_code = $request->get('lkl_area_code', '');
            $lkl_city_code = $request->get('lkl_city_code', '');
            $lkl_province_code = $request->get('lkl_province_code', '');
            $lkl_area_name = $request->get('lkl_area_name', '');
            $lkl_city_name = $request->get('lkl_city_name', '');
            $lkl_province_name = $request->get('lkl_province_name', '');

            //第四步 4、功能信息
            if ($saveStep == 1) {
                $validate = Validator::make($data, [
                    'company' => 'required',
                    'store_name' => 'required',
                    'people' => 'required',
                    'people_phone' => 'required',
                    'province_code' => 'required',
                    'city_code' => 'required',
                    'area_code' => 'required',
                    'store_address' => 'required',
                    'head_name' => 'required',
                    'head_sfz_no' => 'required',
                    'head_sfz_time' => 'required',
                    'head_sfz_stime' => 'required',
                    'store_type' => 'required',
                    'store_type_name' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'company' => '通道类型',
                    'store_name' => '门店名称',
                    'people' => '联系人',
                    'people_phone' => '手机号',
                    'province_code' => '区域',
                    'city_code' => '区域',
                    'area_code' => '区域',
                    'store_address' => '详细地址',
                    'head_name' => '法人',
                    'head_sfz_no' => '身份证号',
                    'head_sfz_time' => '证件成立日期',
                    'head_sfz_stime' => '证件有效期',
                    'store_type' => '企业类型',
                    'store_type_name' => '企业类型',
                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                //开启事务
                try {
                    DB::beginTransaction();

                    if ($addCompayType == '0') { //新增门店
//                        $Merchant = Merchant::where('phone', $people_phone)->select('id')->first();
//
//                        $is_merchant = 0;
//                        if ($Merchant) {
//                            $is_merchant = 1;
//                        }
//                        if ($is_merchant == 1) {
//                            return json_encode([
//                                'status' => -1,
//                                'message' => '手机号已被注册,请更换'
//                            ]);
//                        }
                        $store_id = date('Ymdhis', time()) . rand(1000, 9999);//随机
                        if ($store_type == '3') {

                        }
                        //拼装门店信息
                        $stores = [
                            'config_id' => $config_id,
                            'user_id' => $user_id,
                            'merchant_id' => '',
                            'store_id' => $store_id,
                            'store_name' => trim($store_name),
                            'store_type_name' => trim($store_type_name),
                            'store_email' => trim($store_email),
                            'store_short_name' => trim($store_short_name),
                            'people' => trim($people),//
                            'people_phone' => trim($people_phone),
                            'province_code' => $province_code,
                            'city_code' => $city_code,
                            'area_code' => $area_code,
                            'province_name' => $province_name,
                            'city_name' => $city_name,
                            'area_name' => $area_name,
                            'store_address' => trim($store_address),
                            'head_name' => trim($head_name), //法人
                            'head_sfz_no' => trim($head_sfz_no),
                            'head_sfz_time' => $head_sfz_time,
                            'head_sfz_stime' => $head_sfz_stime,
                            'store_type' => $store_type,
                            'status' => 1,
                            'status_desc' => "自动审核通过",
                            'admin_status' => 1,
                            'admin_status_desc' => "自动审核通过",
                            'weixin_no' => "",
                            'alipay_name' => "",
                            'alipay_account' => "",
                            'source' => $user->source, ////来源,01:畅立收，02:河南畅立收
                        ];

                        if ($store_type == '1' || $store_type == '2') {
                            $stores['store_license_no'] = $store_license_no;
                            $stores['store_license_time'] = $store_license_time;
                            $stores['store_license_stime'] = $store_license_stime;
                        }

                        //图片信息
                        $store_imgs = [
                            'store_id' => $store_id,
                            'head_sfz_img_a' => $head_sfz_img_a,
                            'head_sfz_img_b' => $head_sfz_img_b
                        ];
                        if ($store_type == '1' || $store_type == '2') {
                            $store_imgs['store_license_img'] = $store_license_img;
                        }
                        //80开头+当前日期+4位随机数
                        $merchantNo = $this->generateMerchantNo();
                        //检查是否存在商户号
                        $existMerchantNo = Merchant::where('merchant_no', $merchantNo)->first();
                        if (!$existMerchantNo) {
                            $merchantNo = $this->generateMerchantNo(); //重新获取
                        }
                        //注册merchants账户
                        $merchantData = [
                            'pid' => 0,
                            'type' => 1, //1、店长 2 收银员
                            'name' => $people,
                            'password' => bcrypt('000000'),
                            'pay_password' => bcrypt('000000'),
                            'phone' => $people_phone,
                            'user_id' => $user_id,
                            'config_id' => $config_id,
                            'wx_openid' => '',
                            'source' => $user->source, ////来源,01:畅立收，02:河南畅立收
                            'merchant_no' => $merchantNo
                        ];
                        $merchant = Merchant::create($merchantData);
                        $merchant_id = $merchant->id;

                        $stores['merchant_id'] = $merchant_id;
                        MerchantStore::create([
                            'store_id' => $store_id,
                            'merchant_id' => $merchant_id
                        ]);

                        Store::create($stores);
                        StoreImg::create($store_imgs);

                        //初始化费率 默认0.38

                        $data['store_id'] = $store_id;
                        $data['rate'] = 0.38;
                        $data['company'] = $company;
                        $retStorePayWay = $this->app_send_ways_data($data);

                        if ($retStorePayWay['status'] == -1) {
                            DB::rollBack();
                            return json_encode([
                                'status' => -1,
                                'message' => $retStorePayWay['message']
                            ]);
                        }
                    }
                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                        $storeSteps->save();
                    } else {
                        StoreSteps::create([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 2) {

                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_bank_type' => 'required',
                    'store_bank_name' => 'required',
                    'store_bank_no' => 'required',
                    'bank_no' => 'required',
                    'sub_bank_name' => 'required',
                    'bank_name' => 'required',
                    'settle_province_code' => 'required',
                    'settle_city_code' => 'required',
                    'openning_bank_code' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_bank_type' => '结算类型',
                    'store_bank_name' => '开户名称',
                    'store_bank_no' => '银行卡号',
                    'bank_no' => '联行号',
                    'sub_bank_name' => '支行名称',
                    'bank_name' => '银行名称',
                    'settle_province_code' => '补充资料开户行省名称',
                    'settle_city_code' => '补充资料开户行市名称',
                    'openning_bank_code' => '补充资料开户行名称',
                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                if ($bankType == '03' && !$store_auth_bank_img) {
                    return json_encode([
                        'status' => 2,
                        'message' => '非法人对私请上传法人授权函'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();
                $storeBank = StoreBank::where('store_id', $store_id)->first();
                $lklStore = LklStore::where('store_id', $store_id)->select('*')->first();

                //开启事务
                try {
                    DB::beginTransaction();

                    //银行卡信息
                    $store_banks = [
                        'store_id' => $store_id,
                        'store_bank_type' => $store_bank_type,
                        'store_bank_name' => $store_bank_name,
                        'store_bank_no' => $store_bank_no,
                        'bank_no' => $bank_no,
                        'sub_bank_name' => $sub_bank_name,
                        'bank_name' => $bank_name,
                        'reserved_mobile' => $reserved_mobile,
                        'store_bank_phone' => $store->people_phone,
                        'bank_province_code' => $store->province_code,
                        'bank_city_code' => $store->city_code,
                        'bank_area_code' => $store->area_code,
                    ];
                    //拉卡拉商户信息
                    $store_lkl = [
                        'settle_province_code' => $settle_province_code,
                        'settle_province_name' => $settle_province_name,
                        'settle_city_code' => $settle_city_code,
                        'settle_city_name' => $settle_city_name,
                        'openning_bank_code' => $openning_bank_code,
                        'openning_bank_name' => $openning_bank_name,
                        'clearing_bank_code' => $clearing_bank_code,
                    ];
                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                    ];

                    if ($bankType == '01') {
                        $store_banks['bank_sfz_no'] = $store->head_sfz_no;
                        $store_banks['bank_sfz_stime'] = $store->head_sfz_stime;
                        $store_banks['bank_sfz_time'] = $store->head_sfz_time;

                        $store_imgs['bank_img_a'] = $bank_img_a;
                        $store_imgs['bank_img_b'] = $bank_img_b;
                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                        $store_imgs['bank_sfz_img_a'] = "";
                        $store_imgs['bank_sfz_img_b'] = "";
                    } else if ($bankType == '02') {
                        $store_banks['bank_sfz_no'] = $store->head_sfz_no;
                        $store_banks['bank_sfz_stime'] = $store->head_sfz_stime;
                        $store_banks['bank_sfz_time'] = $store->head_sfz_time;

                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                        $store_imgs['bank_img_a'] = "";
                        $store_imgs['bank_img_b'] = "";
                        $store_imgs['bank_sfz_img_a'] = "";
                        $store_imgs['bank_sfz_img_b'] = "";
                    } else if ($bankType == '03') {
                        $store_banks['bank_sfz_no'] = $bank_sfz_no;
                        $store_banks['bank_sfz_stime'] = $bank_sfz_stime;
                        $store_banks['bank_sfz_time'] = $bank_sfz_time;
                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                        $store_imgs['store_auth_bank_img'] = $store_auth_bank_img;
                        $store_imgs['bank_img_a'] = $bank_img_a;
                        $store_imgs['bank_img_b'] = $bank_img_b;
                        $store_imgs['bank_sfz_img_a'] = $bank_sfz_img_a;
                        $store_imgs['bank_sfz_img_b'] = $bank_sfz_img_b;
                    }

                    if ($storeBank) {
                        StoreBank::where('store_id', $store_id)->update($store_banks);
                    } else {
                        StoreBank::create($store_banks);
                    }

                    if ($storeImg) {
                        StoreImg::where('store_id', $store_id)->update($store_imgs);
                    } else {
                        StoreImg::create($store_imgs);
                    }
                    if ($lklStore) {
                        LklStore::where('store_id', $store_id)->update($store_lkl);
                    } else {
                        $store_lkl['store_id'] = $store_id;
                        LklStore::create($store_lkl);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '账户信息'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }

            } else if ($saveStep == 3) {

                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_logo_img' => 'required',
                    'store_img_a' => 'required',
                    'store_img_b' => 'required',
                    'category_id' => 'required',
                    'mcc_type' => 'required',
                    'mcc' => 'required',
                    'lkl_area_code' => 'required',
                    'lkl_city_code' => 'required',
                    'lkl_province_code' => 'required',
                    'email' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_logo_img' => '门头照',
                    'store_img_a' => '收银台照',
                    'store_img_b' => '经营内容照',
                    'category_id' => '经营类型',
                    'mcc_type' => '经营大类',
                    'mcc' => '经营小类',
                    'lkl_area_code' => '所在区域区县',
                    'lkl_city_code' => '所在区域市',
                    'lkl_province_code' => '所在区域省',
                    'email' => '商户邮箱',
                ]);

                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }

                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();
                $lklStore = LklStore::where('store_id', $store_id)->select('*')->first();

                //开启事务
                try {
                    DB::beginTransaction();

                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                        'store_logo_img' => $store_logo_img,
                        'store_img_a' => $store_img_a,
                        'store_img_b' => $store_img_b,
                    ];
                    //拉卡拉商户信息
                    $store_lkl = [
                        'mcc_type' => $mcc_type,
                        'mcc' => $mcc,
                        'mcc_name' => $mcc_name,
                        'email' => $email,
                        'province_code' => $lkl_province_code,
                        'province_name' => $lkl_province_name,
                        'city_code' => $lkl_city_code,
                        'city_name' => $lkl_city_name,
                        'area_code' => $lkl_area_code,
                        'area_name' => $lkl_area_name,
                    ];

                    if ($storeImg) {
                        StoreImg::where('store_id', $store_id)->update($store_imgs);
                    } else {
                        StoreImg::create($store_imgs);
                    }
                    if ($store) {
                        $stores = [
                            'store_id' => $store_id,
                            'category_id' => $category_id,
                            'category_name' => $category_name,
//                            'source_type' => $source_type,
                        ];
                        Store::where('store_id', $store_id)->update($stores);
                    }
                    if ($lklStore) {
                        LklStore::where('store_id', $store_id)->update($store_lkl);
                    } else {
                        $store_lkl['store_id'] = $store_id;
                        LklStore::create($store_lkl);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();
                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '门店信息'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 4) {

                $rate = $request->get('rate', '');
                $settle_type = $request->get('settle_type', '');
                $validate = Validator::make($data, [
                    'rate' => 'required',
                    'settle_type' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'rate' => '费率',
                    'settle_type' => '结算方式',
                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                $store = Store::where('store_id', $store_id)
                    ->first();
                $lklStore = LklStore::where('store_id', $store_id)->select('id')
                    ->first();
                if (!$store) {
                    return json_encode(['status' => -1, 'message' => '门店不存在']);
                }
                //通道支付费率 storePayWay
                if ($rate > 0.40) {
                    return json_encode(['status' => -1, 'message' => '设置费率超限']);
                }

                try {
                    DB::beginTransaction();
                    //查找下级门店共享通道
                    if ($store->pid == 0) {
                        $sub_store = Store::where('pid', $store->id)
                            ->where('pay_ways_type', 1)
                            ->select('store_id')
                            ->get();
                        foreach ($sub_store as $k => $v) {
                            $data['store_id'] = $v->store_id;
                            $data['rate'] = $rate;
                            $data['company'] = $company;

                            $this->app_send_ways_data($data);
                        }
                    }

                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['company'] = $company;
                    $retStorePayWay = $this->app_send_ways_data($data);

                    if ($retStorePayWay['status'] == -1) {
                        DB::rollBack();
                        return json_encode([
                            'status' => -1,
                            'message' => $retStorePayWay['message']
                        ]);
                    }
                    if ($lklStore) {
                        LklStore::where('store_id', $store_id)->update(['settle_type' => $settle_type]);
                    } else {
                        LklStore::create(['settle_type' => $settle_type, 'store_id' => $store_id]);
                    }
                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '功能'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();

                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 5) { //提交审核
                $others = $request->get('others', '');
                $agree_ment = $request->get('agree_ment', '');
                $lklStore = LklStore::where('store_id', $store_id)->select('id')
                    ->first();
                $store_lkl = [
                    'others_url' => $others,
                    'agree_ment_url' => $agree_ment,
                ];
                if ($lklStore) {
                    LklStore::where('store_id', $store_id)->update($store_lkl);
                } else {
                    $store_lkl['store_id'] = $store_id;
                    LklStore::create($store_lkl);
                }
                $code = '';
                $SettleModeType = '';//结算方式
                $spwObj = new StorePayWaysController();

                $store_ways_desc = DB::table('store_ways_desc')
                    ->where('is_show', '1')
                    ->where('company', $company)
                    ->first();
                $type = $store_ways_desc->ways_type;

                return $spwObj->base_open_ways($type, $code, $store_id, $SettleModeType, '', '');
            }

            return json_encode([
                'status' => '1',
                'message' => '操作成功',
                'data' => [
                    'store_id' => $store_id
                ]
            ]);
        } catch (\Exception $ex) {
            Log::info($ex);
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    public function update_lkl_store(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_id = $request->get('user_id', '');
            $data = $request->except(['token']);
            $store_id = $request->get('storeId', '');

            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => -1,
                    'message' => '用户不存在'
                ]);
            }
//            $userAuths = UserAuths::where('user_id', $user_id)->where('user_type', '1')->first();
//            if (!$userAuths) {
//                return json_encode([
//                    'status' => -1,
//                    'message' => '未实名认证，请先进行实名认证'
//                ]);
//            }
            //检查是否签署协议
//            $esignAuth = EsignAuths::where('user_id', $user_id)->first();
//            if (!$esignAuth) {
//                return json_encode([
//                    'status' => -1,
//                    'message' => '请先进行协议签署'
//                ]);
//            } else {
//                if ($esignAuth->sign_flow_status != '2') {
//                    return json_encode([
//                        'status' => -1,
//                        'message' => '请先完成协议签署'
//                    ]);
//                }
//            }

            $config_id = $token->config_id;

            $saveStep = $request->get('saveStep', '1'); // 1执照信息 、2账户信息 3、门店信息 4、功能信息 5、
            $company = $request->get('company', 'lklpay');
            //1、执照信息
            $store_type_name = $request->get('store_type_name', '');
            $store_type = $request->get('store_type', ''); //门店性质 1-个体，2-企业，3-小微商户
            $store_name = $request->get('store_name', '');
            $store_short_name = $request->get('store_short_name', $store_name);
            $store_name = $request->get('store_name', '');
            $store_address = $request->get('store_address', '');

            if ($store_type == '1' || $store_type == '2') {
                $store_license_no = $request->get('store_license_no', '');
                $store_license_img = $request->get('store_license_img', '');
                $store_license_time = $request->get('store_license_time', '');
                $store_license_stime = $request->get('store_license_stime', '');
            }

            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');
            $province_name = $request->get('province_name', '');
            $city_name = $request->get('city_name', '');
            $area_name = $request->get('area_name', '');
            $people_phone = $request->get('people_phone', '');
            $store_email = $request->get('store_email', '');
            $head_name = $request->get('head_name', '');
            $head_sfz_no = $request->get('head_sfz_no', '');
            $people = $request->get('people', '');
            $head_sfz_img_a = $request->get('head_sfz_img_a', '');  //法人身份证正面
            $head_sfz_img_b = $request->get('head_sfz_img_b', '');  //法人身份证反面

            $head_sfz_stime = $request->get('head_sfz_stime', ''); //法人身份证开始日期
            $head_sfz_time = $request->get('head_sfz_time', ''); //法人身份证结束日期
            if ($head_sfz_time != '长期') {
                $head_sfz_time = $this->time($head_sfz_time);
            }
            $head_sfz_stime = $this->time($head_sfz_stime);

            //第二步 2、账户信息

            $bankType = $request->get('bankType', ''); // 01 法人对公 02对公 03非法人对私
            $store_bank_type = $request->get('store_bank_type', ''); //01 对私 02 对公
            $store_bank_name = $request->get('store_bank_name', '');
            $store_bank_no = $request->get('store_bank_no', '');    //银行卡号
            $bank_no = $request->get('bank_no', '');    //联行号
            $sub_bank_name = $request->get('sub_bank_name', '');   //支行名称
            $bank_name = $request->get('bank_name', '');    //银行名称
            $reserved_mobile = $request->get('reserved_mobile', '');    //预留手机号
            $store_industrylicense_img = $request->get('store_industrylicense_img', '');//开户许可证
            $settle_province_code = $request->get('settle_province_code', '');
            $settle_province_name = $request->get('settle_province_name', '');
            $settle_city_code = $request->get('settle_city_code', '');
            $settle_city_name = $request->get('settle_city_name', '');
            $openning_bank_code = $request->get('openning_bank_code', '');
            $openning_bank_name = $request->get('openning_bank_name', '');
            $clearing_bank_code = $request->get('clearing_bank_code', '');
            $store_auth_bank_img = $request->get('certOfAuth', '');

            if ($bankType == '01') {
                $bank_img_a = $request->get('bank_img_a', '');
                $bank_img_b = $request->get('bank_img_b', '');
            } else if ($bankType == '02') {

            } else if ($bankType == '03') {
                $bank_img_a = $request->get('bank_img_a', '');
                $bank_img_b = $request->get('bank_img_b', '');
                $bank_sfz_img_a = $request->get('bank_sfz_img_a', '');
                $bank_sfz_img_b = $request->get('bank_sfz_img_b', '');

                $bank_sfz_no = $request->get('bank_sfz_no', '');
                $bank_sfz_stime = $request->get('bank_sfz_stime', '');
                $bank_sfz_time = $request->get('bank_sfz_time', '');
                if ($bank_sfz_time != '长期') {
                    $bank_sfz_time = $this->time($bank_sfz_time);
                }
                $bank_sfz_stime = $this->time($bank_sfz_stime);

            }

            //第三步 3、门店信息

            //门店来源 比如 中原畅立收
            $source_type = $request->get('source_type', '0'); // 1:中原畅立收
            $store_logo_img = $request->get('store_logo_img', '');
            $store_img_a = $request->get('store_img_a', '');
            $store_img_b = $request->get('store_img_b', '');
            $store_img_c = $request->get('store_img_c', '');
            $category_id = $request->get('category_id', ''); //经营类型
            $category_name = $request->get('category_name', '');//经营类型名称
            $mcc_type = $request->get('mcc_type', '');
            $mcc = $request->get('mcc', '');
            $mcc_name = $request->get('mcc_name', '');
            $email = $request->get('email', '');
            $lkl_area_code = $request->get('lkl_area_code', '');
            $lkl_city_code = $request->get('lkl_city_code', '');
            $lkl_province_code = $request->get('lkl_province_code', '');
            $lkl_area_name = $request->get('lkl_area_name', '');
            $lkl_city_name = $request->get('lkl_city_name', '');
            $lkl_province_name = $request->get('lkl_province_name', '');

            //第四步 4、功能信息

            if ($saveStep == 1) {
                $validate = Validator::make($data, [
                    'store_name' => 'required',
                    'people' => 'required',
                    'people_phone' => 'required',
                    'province_code' => 'required',
                    'city_code' => 'required',
                    'area_code' => 'required',
                    'store_address' => 'required',
                    'head_name' => 'required',
                    'head_sfz_no' => 'required',
                    'head_sfz_time' => 'required',
                    'head_sfz_stime' => 'required',
                    'store_type' => 'required',
                    'store_type_name' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'store_name' => '门店名称',
                    'people' => '联系人',
                    'people_phone' => '手机号',
                    'province_code' => '区域',
                    'city_code' => '区域',
                    'area_code' => '区域',
                    'store_address' => '详细地址',
                    'head_name' => '法人',
                    'head_sfz_no' => '身份证号',
                    'head_sfz_time' => '证件成立日期',
                    'head_sfz_stime' => '证件有效期',
                    'store_type' => '企业类型',
                    'store_type_name' => '企业类型',
                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                //开启事务
                try {
                    DB::beginTransaction();

                    $merchantStore = MerchantStore::where('store_id', $store_id)->select('merchant_id')->first();
                    if ($merchantStore) {
                        $upIN = [
                            'name' => $people,
                            'phone' => $people_phone,
                        ];
                        Merchant::where('id', $merchantStore->merchant_id)->update($upIN);
                    }

                    //拼装门店信息
                    $stores = [
                        'config_id' => $config_id,
                        //'user_id' => $user_id,
                        'merchant_id' => '',
                        'store_id' => $store_id,
                        'store_name' => trim($store_name),
                        'store_type_name' => trim($store_type_name),
                        'store_email' => trim($store_email),
                        'store_short_name' => trim($store_short_name),
                        'people' => trim($people),//
                        'people_phone' => trim($people_phone),
                        'province_code' => $province_code,
                        'city_code' => $city_code,
                        'area_code' => $area_code,
                        'province_name' => $province_name,
                        'city_name' => $city_name,
                        'area_name' => $area_name,
                        'store_address' => trim($store_address),
                        'head_name' => trim($head_name), //法人
                        'head_sfz_no' => trim($head_sfz_no),
                        'head_sfz_time' => $head_sfz_time,
                        'head_sfz_stime' => $head_sfz_stime,
                        'store_type' => $store_type,
                        'status' => 1,
                        'status_desc' => "自动审核通过",
                        'admin_status' => 1,
                        'admin_status_desc' => "自动审核通过",
                        'weixin_no' => "",
                        'alipay_name' => "",
                        'alipay_account' => "",
                        'source' => $user->source, ////来源,01:畅立收，02:河南畅立收
                    ];

                    if ($store_type == '1' || $store_type == '2') {
                        $stores['store_license_no'] = $store_license_no;
                        $stores['store_license_time'] = $store_license_time;
                        $stores['store_license_stime'] = $store_license_stime;
                    }

                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                        'head_sfz_img_a' => $head_sfz_img_a,
                        'head_sfz_img_b' => $head_sfz_img_b
                    ];
                    if ($store_type == '1' || $store_type == '2') {
                        $store_imgs['store_license_img'] = $store_license_img;
                    }
                    $store = Store::where('store_id', $store_id)->first();
                    if (!$store) {
                        return json_encode([
                            'status' => 2,
                            'message' => '门店未认证'
                        ]);
                    } else {
                        $storeImg = StoreImg::where('store_id', $store_id)->first();

                        if ($storeImg) {
                            StoreImg::where('store_id', $store_id)->update($store_imgs);
                        } else {
                            StoreImg::create($store_imgs);
                        }

                        $store->update(array_filter($stores));
                        $store->save();

                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                        $storeSteps->save();
                    } else {
                        StoreSteps::create([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 2) {
                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_bank_type' => 'required',
                    'store_bank_name' => 'required',
                    'store_bank_no' => 'required',
                    'bank_no' => 'required',
                    'sub_bank_name' => 'required',
                    'bank_name' => 'required',
                    'settle_province_code' => 'required',
                    'settle_city_code' => 'required',
                    'openning_bank_code' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_bank_type' => '结算类型',
                    'store_bank_name' => '开户名称',
                    'store_bank_no' => '银行卡号',
                    'bank_no' => '联行号',
                    'sub_bank_name' => '支行名称',
                    'bank_name' => '银行名称',
                    'settle_province_code' => '补充资料开户行省名称',
                    'settle_city_code' => '补充资料开户行市名称',
                    'openning_bank_code' => '补充资料开户行名称',
                ]);
                if ($bankType == '03' && !$store_auth_bank_img) {
                    return json_encode([
                        'status' => 2,
                        'message' => '非法人对私请上传法人授权函'
                    ]);
                }
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();
                $storeBank = StoreBank::where('store_id', $store_id)->first();
                $lklStore = LklStore::where('store_id', $store_id)->select('id')->first();

                //开启事务
                try {
                    DB::beginTransaction();
                    //银行卡信息
                    $store_banks = [
                        'store_id' => $store_id,
                        'store_bank_type' => $store_bank_type,
                        'store_bank_name' => $store_bank_name,
                        'store_bank_no' => $store_bank_no,
                        'bank_no' => $bank_no,
                        'sub_bank_name' => $sub_bank_name,
                        'bank_name' => $bank_name,
                        'reserved_mobile' => $reserved_mobile,
                        'store_bank_phone' => $store->people_phone,
                        'bank_province_code' => $store->province_code,
                        'bank_city_code' => $store->city_code,
                        'bank_area_code' => $store->area_code,
                    ];
                    //拉卡拉商户信息
                    $store_lkl = [
                        'settle_province_code' => $settle_province_code,
                        'settle_province_name' => $settle_province_name,
                        'settle_city_code' => $settle_city_code,
                        'settle_city_name' => $settle_city_name,
                        'openning_bank_code' => $openning_bank_code,
                        'openning_bank_name' => $openning_bank_name,
                        'clearing_bank_code' => $clearing_bank_code,
                    ];

                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                    ];

                    if ($bankType == '01') {
                        $store_banks['bank_sfz_no'] = $store->head_sfz_no;
                        $store_banks['bank_sfz_stime'] = $store->head_sfz_stime;
                        $store_banks['bank_sfz_time'] = $store->head_sfz_time;

                        $store_imgs['bank_img_a'] = $bank_img_a;
                        $store_imgs['bank_img_b'] = $bank_img_b;
                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;

                        $store_imgs['bank_sfz_img_a'] = "";
                        $store_imgs['bank_sfz_img_b'] = "";
                    } else if ($bankType == '02') {
                        $store_banks['bank_sfz_no'] = $store->head_sfz_no;
                        $store_banks['bank_sfz_stime'] = $store->head_sfz_stime;
                        $store_banks['bank_sfz_time'] = $store->head_sfz_time;

                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                        $store_imgs['bank_img_a'] = "";
                        $store_imgs['bank_img_b'] = "";
                        $store_imgs['bank_sfz_img_a'] = "";
                        $store_imgs['bank_sfz_img_b'] = "";
                    } else if ($bankType == '03') {
                        $store_banks['bank_sfz_no'] = $bank_sfz_no;
                        $store_banks['bank_sfz_stime'] = $bank_sfz_stime;
                        $store_banks['bank_sfz_time'] = $bank_sfz_time;
                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                        $store_imgs['store_auth_bank_img'] = $store_auth_bank_img;
                        $store_imgs['bank_img_a'] = $bank_img_a;
                        $store_imgs['bank_img_b'] = $bank_img_b;
                        $store_imgs['bank_sfz_img_a'] = $bank_sfz_img_a;
                        $store_imgs['bank_sfz_img_b'] = $bank_sfz_img_b;
                    }

                    if ($storeBank) {
                        StoreBank::where('store_id', $store_id)->update($store_banks);
                    } else {
                        StoreBank::create($store_banks);
                    }

                    if ($storeImg) {
                        StoreImg::where('store_id', $store_id)->update($store_imgs);
                    } else {
                        StoreImg::create($store_imgs);
                    }
                    if ($lklStore) {
                        LklStore::where('store_id', $store_id)->update($store_lkl);
                    } else {
                        $store_lkl['store_id'] = $store_id;
                        LklStore::create($store_lkl);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '账户信息'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }

            } else if ($saveStep == 3) {

                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_logo_img' => 'required',
                    'store_img_a' => 'required',
                    'store_img_b' => 'required',
                    'category_id' => 'required',
                    'mcc_type' => 'required',
                    'mcc' => 'required',
                    'lkl_area_code' => 'required',
                    'lkl_city_code' => 'required',
                    'lkl_province_code' => 'required',
                    'email' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_logo_img' => '门头照',
                    'store_img_a' => '收银台照',
                    'store_img_b' => '经营内容照',
                    'category_id' => '经营类型',
                    'mcc_type' => '经营大类',
                    'mcc' => '经营小类',
                    'lkl_area_code' => '所在区域区县',
                    'lkl_city_code' => '所在区域市',
                    'lkl_province_code' => '所在区域省',
                    'email' => '商户邮箱',
                ]);

                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }

                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();
                $lklStore = LklStore::where('store_id', $store_id)->select('id')->first();

                //开启事务
                try {
                    DB::beginTransaction();
                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                        'store_logo_img' => $store_logo_img,
                        'store_img_a' => $store_img_a,
                        'store_img_b' => $store_img_b,
                    ];
                    //拉卡拉商户信息
                    $store_lkl = [
                        'mcc_type' => $mcc_type,
                        'mcc' => $mcc,
                        'mcc_name' => $mcc_name,
                        'province_code' => $lkl_province_code,
                        'province_name' => $lkl_province_name,
                        'city_code' => $lkl_city_code,
                        'city_name' => $lkl_city_name,
                        'area_code' => $lkl_area_code,
                        'area_name' => $lkl_area_name,
                        'email' => $email,
                    ];
                    if ($storeImg) {
                        StoreImg::where('store_id', $store_id)->update($store_imgs);
                    } else {
                        StoreImg::create($store_imgs);
                    }
                    if ($store) {
                        $stores = [
                            'store_id' => $store_id,
                            'category_id' => $category_id,
                            'category_name' => $category_name,
                            //'source_type' => $source_type,
                        ];
                        Store::where('store_id', $store_id)->update($stores);
                    }
                    if ($lklStore) {
                        LklStore::where('store_id', $store_id)->update($store_lkl);
                    } else {
                        $store_lkl['store_id'] = $store_id;
                        LklStore::create($store_lkl);
                    }
                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();
                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '门店信息'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 4) {

                $rate = $request->get('rate', '');
                $settle_type = $request->get('settle_type', '');
                $validate = Validator::make($data, [
                    'rate' => 'required',
                    'settle_type' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'rate' => '费率',
                    'settle_type' => '结算方式',
                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                $store = Store::where('store_id', $store_id)
                    ->first();
                $lklStore = LklStore::where('store_id', $store_id)->select('id')
                    ->first();
                if (!$store) {
                    return json_encode(['status' => -1, 'message' => '门店不存在']);
                }
                //通道支付费率 storePayWay
                if ($rate > 0.40) {
                    return json_encode(['status' => -1, 'message' => '设置费率超限']);
                }

                try {
                    DB::beginTransaction();

                    //查找下级门店共享通道
                    if ($store->pid == 0) {
                        $sub_store = Store::where('pid', $store->id)
                            ->where('pay_ways_type', 1)
                            ->select('store_id')
                            ->get();
                        foreach ($sub_store as $k => $v) {
                            $data['store_id'] = $v->store_id;
                            $data['rate'] = $rate;
                            $data['company'] = $company;

                            $this->app_send_ways_data($data);
                        }
                    }

                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['company'] = $company;
                    $retStorePayWay = $this->app_send_ways_data($data);

                    if ($retStorePayWay['status'] == -1) {
                        DB::rollBack();
                        return json_encode([
                            'status' => -1,
                            'message' => $retStorePayWay['message']
                        ]);
                    }
                    if ($lklStore) {
                        LklStore::where('store_id', $store_id)->update(['settle_type' => $settle_type]);
                    } else {
                        LklStore::create(['settle_type' => $settle_type, 'store_id' => $store_id]);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();
                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '提交审核'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();


                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 5) { //提交审核
                $others = $request->get('others', '');
                $agree_ment = $request->get('agree_ment', '');
                $lklStore = LklStore::where('store_id', $store_id)->select('id')
                    ->first();
                $store_lkl = [
                    'others_url' => $others,
                    'agree_ment_url' => $agree_ment,
                ];
                if ($lklStore) {
                    LklStore::where('store_id', $store_id)->update($store_lkl);
                } else {
                    $store_lkl['store_id'] = $store_id;
                    LklStore::create($store_lkl);
                }

                $code = '';
                $SettleModeType = '';//结算方式
                $spwObj = new StorePayWaysController();

                $store_ways_desc = DB::table('store_ways_desc')
                    ->where('is_show', '1')
                    ->where('company', $company)
                    ->first();
                $type = $store_ways_desc->ways_type;

                return $spwObj->base_open_ways($type, $code, $store_id, $SettleModeType, '', '');
            }

            return json_encode([
                'status' => '1',
                'message' => '操作成功',
                'data' => [
                    'store_id' => $store_id
                ]
            ]);
        } catch (\Exception $ex) {
            Log::info($ex);
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    /**
     * 首信易通道
     */
    public function add_easePay_store(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_id = $request->get('user_id', $token->user_id);
            $data = $request->except(['token']);

            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => -1,
                    'message' => '用户不存在'
                ]);
            }

            $config_id = $token->config_id;

            $addCompayType = $request->get('addCompayType', '0'); // 是否存在通道，该通道为新增。默认0不存在，1已存在
            $saveStep = $request->get('saveStep', '1'); // 1执照信息 、2账户信息 3、门店信息 4、功能信息 5、

            $store_id = '';
            if ($saveStep != 1 || $addCompayType == '1') {
                $store_id = $request->get('storeId', '');
            }
            //1、执照信息
            $company = $request->get('company', 'easepay'); //默认
            $store_type_name = $request->get('store_type_name', '');
            $store_type = $request->get('store_type', ''); //门店性质 1-个体，2-企业，3-小微商户
            $store_name = $request->get('store_name', '');
            $store_short_name = $request->get('store_short_name', $store_name);
            $store_name = $request->get('store_name', '');
            $store_address = $request->get('store_address', '');

            if ($store_type == '1' || $store_type == '2') {
                $store_license_no = $request->get('store_license_no', '');
                $store_license_img = $request->get('store_license_img', '');
                $store_license_time = $request->get('store_license_time', '');
                $store_license_stime = $request->get('store_license_stime', '');
            }

            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');
            $province_name = $request->get('province_name', '');
            $city_name = $request->get('city_name', '');
            $area_name = $request->get('area_name', '');
            $people_phone = $request->get('people_phone', '');
            $store_email = $request->get('store_email', '');
            $head_name = $request->get('head_name', '');
            $head_sfz_no = $request->get('head_sfz_no', '');
            $people = $request->get('people', '');
            $head_sfz_img_a = $request->get('head_sfz_img_a', '');  //法人身份证正面
            $head_sfz_img_b = $request->get('head_sfz_img_b', '');  //法人身份证反面

            $head_sfz_stime = $request->get('head_sfz_stime', ''); //法人身份证开始日期
            $head_sfz_time = $request->get('head_sfz_time', ''); //法人身份证结束日期
            if ($head_sfz_time == '长期') {
                $head_sfz_time = '2099-12-31';
            }
            $head_sfz_stime = $this->time($head_sfz_stime);
            $occupation = $request->get('occupation', ''); //法人职业
            $occupation_name = $request->get('occupation_name', ''); //法人职业名称
            $contact_email = $request->get('contact_email', ''); //邮箱
            //第二步 2、账户信息

            $bankType = $request->get('bankType', ''); // 01 法人对公 02对公 03非法人对私
            $store_bank_type = $request->get('store_bank_type', ''); //01 对私 02 对公
            $store_bank_name = $request->get('store_bank_name', '');
            $store_bank_no = $request->get('store_bank_no', '');    //银行卡号
            $bank_no = $request->get('bank_no', '');    //联行号
            $sub_bank_name = $request->get('sub_bank_name', '');   //支行名称
            $bank_name = $request->get('bank_name', '');    //银行名称
            $reserved_mobile = $request->get('reserved_mobile', '');    //预留手机号
            $store_industrylicense_img = $request->get('store_industrylicense_img', '');//开户许可证
            $bank_img_a = $request->get('bank_img_a', '');//银行卡

            //第三步 3、门店信息

            //门店来源 比如 中原畅立收
            $source_type = $request->get('source_type', '0'); // 1:中原畅立收
            $store_logo_img = $request->get('store_logo_img', '');
            $store_img_b = $request->get('store_img_b', '');
            $category_id = $request->get('category_id', ''); //经营类型
            $category_name = $request->get('category_name', '');//经营类型名称
            $businScope = $request->get('businScope', '');
            $mccId = $request->get('mccId', '');
            $bank_sc_img = $request->get('bank_sc_img', '');//手持身份证
            $wechat_applet = $request->get('wechat_applet', '');//微信公众号流程图


            //第四步 4、功能信息
            if ($saveStep == 1) {
                $validate = Validator::make($data, [
                    'company' => 'required',
                    'store_name' => 'required',
                    'people' => 'required',
                    'people_phone' => 'required',
                    'province_code' => 'required',
                    'city_code' => 'required',
                    'area_code' => 'required',
                    'store_address' => 'required',
                    'head_name' => 'required',
                    'head_sfz_no' => 'required',
                    'head_sfz_time' => 'required',
                    'head_sfz_stime' => 'required',
                    'store_type' => 'required',
                    'store_type_name' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'company' => '通道类型',
                    'store_name' => '门店名称',
                    'people' => '联系人',
                    'people_phone' => '手机号',
                    'province_code' => '区域',
                    'city_code' => '区域',
                    'area_code' => '区域',
                    'store_address' => '详细地址',
                    'head_name' => '法人',
                    'head_sfz_no' => '身份证号',
                    'head_sfz_time' => '证件成立日期',
                    'head_sfz_stime' => '证件有效期',
                    'store_type' => '企业类型',
                    'store_type_name' => '企业类型',
                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }

                //开启事务
                try {
                    DB::beginTransaction();

                    if ($addCompayType == '0') { //新增门店
                        $store_id = date('Ymdhis', time()) . rand(1000, 9999);//随机

                        //拼装门店信息
                        $stores = [
                            'config_id' => $config_id,
                            'user_id' => $user_id,
                            'merchant_id' => '',
                            'store_id' => $store_id,
                            'store_name' => trim($store_name),
                            'store_type_name' => trim($store_type_name),
                            'store_email' => trim($store_email),
                            'store_short_name' => trim($store_short_name),
                            'people' => trim($people),//
                            'people_phone' => trim($people_phone),
                            'province_code' => $province_code,
                            'city_code' => $city_code,
                            'area_code' => $area_code,
                            'province_name' => $province_name,
                            'city_name' => $city_name,
                            'area_name' => $area_name,
                            'store_address' => trim($store_address),
                            'head_name' => trim($head_name), //法人
                            'head_sfz_no' => trim($head_sfz_no),
                            'head_sfz_time' => $head_sfz_time,
                            'head_sfz_stime' => $head_sfz_stime,
                            'store_type' => $store_type,
                            'status' => 1,
                            'status_desc' => "自动审核通过",
                            'admin_status' => 1,
                            'admin_status_desc' => "自动审核通过",
                            'weixin_no' => "",
                            'alipay_name' => "",
                            'alipay_account' => "",
                            'source' => $user->source, ////来源,01:畅立收，02:河南畅立收
                        ];

                        if ($store_type == '1' || $store_type == '2') {
                            $stores['store_license_no'] = $store_license_no;
                            $stores['store_license_time'] = $store_license_time;
                            $stores['store_license_stime'] = $store_license_stime;
                        }

                        //图片信息
                        $store_imgs = [
                            'store_id' => $store_id,
                            'head_sfz_img_a' => $head_sfz_img_a,
                            'head_sfz_img_b' => $head_sfz_img_b
                        ];
                        if ($store_type == '1' || $store_type == '2') {
                            $store_imgs['store_license_img'] = $store_license_img;
                        }

                        //80开头+当前日期+4位随机数
                        $merchantNo = $this->generateMerchantNo();
                        //检查是否存在商户号
                        $existMerchantNo = Merchant::where('merchant_no', $merchantNo)->first();
                        if (!$existMerchantNo) {
                            $merchantNo = $this->generateMerchantNo(); //重新获取
                        }

                        //注册merchants账户
                        $merchantData = [
                            'pid' => 0,
                            'type' => 1, //1、店长 2 收银员
                            'name' => $people,
                            'password' => bcrypt('000000'),
                            'pay_password' => bcrypt('000000'),
                            'phone' => $people_phone,
                            'user_id' => $user_id,
                            'config_id' => $config_id,
                            'wx_openid' => '',
                            'source' => $user->source, ////来源,01:畅立收，02:河南畅立收
                            'merchant_no' => $merchantNo
                        ];
                        $merchantInfo = Merchant::where('phone', $people_phone)->where('is_login', '1')->first();
                        if ($merchantInfo) {
                            $merchantData['is_login'] = '2';
                        }
                        $merchant = Merchant::create($merchantData);
                        $merchant_id = $merchant->id;

                        $stores['merchant_id'] = $merchant_id;
                        MerchantStore::create([
                            'store_id' => $store_id,
                            'merchant_id' => $merchant_id
                        ]);

                        Store::create($stores);
                        StoreImg::create($store_imgs);


                        //初始化费率 默认0.38

                        $data['store_id'] = $store_id;
                        $data['rate'] = 0.38;
                        $data['company'] = $company;
                        $retStorePayWay = $this->app_send_ways_data($data);

                        if ($retStorePayWay['status'] == -1) {
                            DB::rollBack();
                            return json_encode([
                                'status' => -1,
                                'message' => $retStorePayWay['message']
                            ]);
                        }
                    }

                    $easePayStore = EasePayStore::where('store_id', $store_id)->select('*')->first();
                    if ($easePayStore) {
                        EasePayStore::where('store_id', $store_id)->update([
                            'profession' => $occupation,
                            'profession_name' => $occupation_name,
                            'contact_email' => $contact_email
                        ]);
                    } else {
                        EasePayStore::create([
                            'store_id' => $store_id,
                            'profession' => $occupation,
                            'profession_name' => $occupation_name,
                            'contact_email' => $contact_email
                        ]);
                    }
                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                        $storeSteps->save();
                    } else {
                        StoreSteps::create([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 2) {

                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_bank_type' => 'required',
                    'store_bank_name' => 'required',
                    'store_bank_no' => 'required',
                    'bank_no' => 'required',
                    'sub_bank_name' => 'required',
                    'bank_name' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_bank_type' => '结算类型',
                    'store_bank_name' => '开户名称',
                    'store_bank_no' => '银行卡号',
                    'bank_no' => '联行号',
                    'sub_bank_name' => '支行名称',
                    'bank_name' => '银行名称',

                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }

                $storeImg = StoreImg::where('store_id', $store_id)->first();
                $storeBank = StoreBank::where('store_id', $store_id)->first();

                //开启事务
                try {
                    DB::beginTransaction();

                    //银行卡信息
                    $store_banks = [
                        'store_id' => $store_id,
                        'store_bank_type' => $store_bank_type,
                        'store_bank_name' => $store_bank_name,
                        'store_bank_no' => $store_bank_no,
                        'bank_no' => $bank_no,
                        'sub_bank_name' => $sub_bank_name,
                        'bank_name' => $bank_name,
                        'reserved_mobile' => $reserved_mobile,
                        'store_bank_phone' => $store->people_phone,
                        'bank_province_code' => $store->province_code,
                        'bank_city_code' => $store->city_code,
                        'bank_area_code' => $store->area_code,
                    ];
                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                    ];

                    if ($bankType == '01') {
                        $store_imgs['bank_img_a'] = $bank_img_a;
                    } else if ($bankType == '02') {
                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                    } else if ($bankType == '03') {
                        $store_imgs['bank_img_a'] = $bank_img_a;
                    }
                    if ($storeBank) {
                        StoreBank::where('store_id', $store_id)->update($store_banks);
                    } else {
                        StoreBank::create($store_banks);
                    }

                    if ($storeImg) {
                        StoreImg::where('store_id', $store_id)->update($store_imgs);
                    } else {
                        StoreImg::create($store_imgs);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '账户信息'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }

            } else if ($saveStep == 3) {

                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_logo_img' => 'required',
                    'store_img_b' => 'required',
                    'category_id' => 'required',
                    'mccId' => 'required',
                    'bank_sc_img' => 'required',
                    'wechat_applet' => 'required'
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_logo_img' => '门头照',
                    'store_img_b' => '经营内容照',
                    'category_id' => '经营类型',
                    'mccId' => '业务分类',
                    'bank_sc_img' => '手持身份证',
                    'wechat_applet' => '微信公众号流程图',
                ]);

                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }

                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();
                $easePayStore = EasePayStore::where('store_id', $store_id)->select('*')->first();

                //开启事务
                try {
                    DB::beginTransaction();

                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                        'store_logo_img' => $store_logo_img,
                        'store_img_b' => $store_img_b,
                        'bank_sc_img' => $bank_sc_img
                    ];

                    $store_ease = [
                        'mcc_name' => $businScope,
                        'mcc_id' => $mccId,
                        'wechat_applet' => $wechat_applet
                    ];
                    if ($storeImg) {
                        StoreImg::where('store_id', $store_id)->update($store_imgs);
                    } else {
                        StoreImg::create($store_imgs);
                    }
                    $stores = [
                        'store_id' => $store_id,
                        'category_id' => $category_id,
                        'category_name' => $category_name,
                    ];
                    Store::where('store_id', $store_id)->update($stores);
                    if ($easePayStore) {
                        EasePayStore::where('store_id', $store_id)->update($store_ease);
                    } else {
                        $store_ease['store_id'] = $store_id;
                        EasePayStore::create($store_ease);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();
                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '门店信息'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 4) {

                $rate = $request->get('rate', '');
                $store = Store::where('store_id', $store_id)
                    ->first();
                if (!$store) {
                    return json_encode(['status' => -1, 'message' => '门店不存在']);
                }

                try {

                    //查找下级门店共享通道
                    if ($store->pid == 0) {
                        $sub_store = Store::where('pid', $store->id)
                            ->where('pay_ways_type', 1)
                            ->select('store_id')
                            ->get();
                        foreach ($sub_store as $k => $v) {
                            $data['store_id'] = $v->store_id;
                            $data['rate'] = $rate;
                            $data['company'] = $company;

                            $this->app_send_ways_data($data);
                        }
                    }

                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['company'] = $company;
                    $retStorePayWay = $this->app_send_ways_data($data);

                    if ($retStorePayWay['status'] == -1) {
                        return json_encode([
                            'status' => -1,
                            'message' => $retStorePayWay['message']
                        ]);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '功能'
                        ]);
                        $storeSteps->save();
                    }
//                    $code = '';
//                    $SettleModeType = '';//结算方式
//                    $spwObj = new StorePayWaysController();
//
//                    $store_ways_desc = DB::table('store_ways_desc')
//                        ->where('is_show', '1')
//                        ->where('company', $company)
//                        ->first();
//                    $type = $store_ways_desc->ways_type;
//
//                    return $spwObj->base_open_ways($type, $code, $store_id, $SettleModeType, '', '');


                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 5) { //提交审核

            }

            return json_encode([
                'status' => '1',
                'message' => '操作成功',
                'data' => [
                    'store_id' => $store_id
                ]
            ]);
        } catch (\Exception $ex) {
            Log::info($ex);
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    public function update_easePay_store(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_id = $request->get('user_id', $token->user_id);
            $data = $request->except(['token']);
            $store_id = $request->get('storeId', '');

            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => -1,
                    'message' => '用户不存在'
                ]);
            }

            $config_id = $token->config_id;

            $saveStep = $request->get('saveStep', '1'); // 1执照信息 、2账户信息 3、门店信息 4、功能信息 5、
            $company = $request->get('company', 'lklpay');
            //1、执照信息
            $store_type_name = $request->get('store_type_name', '');
            $store_type = $request->get('store_type', ''); //门店性质 1-个体，2-企业，3-小微商户
            $store_name = $request->get('store_name', '');
            $store_short_name = $request->get('store_short_name', $store_name);
            $store_name = $request->get('store_name', '');
            $store_address = $request->get('store_address', '');

            if ($store_type == '1' || $store_type == '2') {
                $store_license_no = $request->get('store_license_no', '');
                $store_license_img = $request->get('store_license_img', '');
                $store_license_time = $request->get('store_license_time', '');
                $store_license_stime = $request->get('store_license_stime', '');
            }

            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');
            $province_name = $request->get('province_name', '');
            $city_name = $request->get('city_name', '');
            $area_name = $request->get('area_name', '');
            $people_phone = $request->get('people_phone', '');
            $store_email = $request->get('store_email', '');
            $head_name = $request->get('head_name', '');
            $head_sfz_no = $request->get('head_sfz_no', '');
            $people = $request->get('people', '');
            $head_sfz_img_a = $request->get('head_sfz_img_a', '');  //法人身份证正面
            $head_sfz_img_b = $request->get('head_sfz_img_b', '');  //法人身份证反面

            $head_sfz_stime = $request->get('head_sfz_stime', ''); //法人身份证开始日期
            $head_sfz_time = $request->get('head_sfz_time', ''); //法人身份证结束日期
            if ($head_sfz_time == '长期') {
                $head_sfz_time = '2099-12-31';
            }
            $head_sfz_stime = $this->time($head_sfz_stime);
            $occupation = $request->get('occupation', ''); //法人职业
            $occupation_name = $request->get('occupation_name', ''); //法人职业名称
            $contact_email = $request->get('contact_email', ''); //邮箱
            //第二步 2、账户信息

            $bankType = $request->get('bankType', ''); // 01 法人对公 02对公 03非法人对私
            $store_bank_type = $request->get('store_bank_type', ''); //01 对私 02 对公
            $store_bank_name = $request->get('store_bank_name', '');
            $store_bank_no = $request->get('store_bank_no', '');    //银行卡号
            $bank_no = $request->get('bank_no', '');    //联行号
            $sub_bank_name = $request->get('sub_bank_name', '');   //支行名称
            $bank_name = $request->get('bank_name', '');    //银行名称
            $reserved_mobile = $request->get('reserved_mobile', '');    //预留手机号
            $store_industrylicense_img = $request->get('store_industrylicense_img', '');//开户许可证
            $store_auth_bank_img = $request->get('certOfAuth', '');//授权书
            $bank_img_a = $request->get('bank_img_a', '');//银行卡

            //第三步 3、门店信息

            $source_type = $request->get('source_type', '0'); // 1:中原畅立收
            $store_logo_img = $request->get('store_logo_img', '');
            $store_img_b = $request->get('store_img_b', '');
            $category_id = $request->get('category_id', ''); //经营类型
            $category_name = $request->get('category_name', '');//经营类型名称
            $businScope = $request->get('businScope', '');
            $mccId = $request->get('mccId', '');
            $bank_sc_img = $request->get('bank_sc_img', '');//手持身份证
            $wechat_applet = $request->get('wechat_applet', '');//微信公众号流程图

            //第四步 4、功能信息

            if ($saveStep == 1) {
                $validate = Validator::make($data, [
                    'store_name' => 'required',
                    'people' => 'required',
                    'people_phone' => 'required',
                    'province_code' => 'required',
                    'city_code' => 'required',
                    'area_code' => 'required',
                    'store_address' => 'required',
                    'head_name' => 'required',
                    'head_sfz_no' => 'required',
                    'head_sfz_time' => 'required',
                    'head_sfz_stime' => 'required',
                    'store_type' => 'required',
                    'store_type_name' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'store_name' => '门店名称',
                    'people' => '联系人',
                    'people_phone' => '手机号',
                    'province_code' => '区域',
                    'city_code' => '区域',
                    'area_code' => '区域',
                    'store_address' => '详细地址',
                    'head_name' => '法人',
                    'head_sfz_no' => '身份证号',
                    'head_sfz_time' => '证件成立日期',
                    'head_sfz_stime' => '证件有效期',
                    'store_type' => '企业类型',
                    'store_type_name' => '企业类型',
                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                if (mb_strlen(trim($store_short_name), 'UTF-8') > 20) {
                    return json_encode([
                        'status' => 2,
                        'message' => '经营名称不能超过20个字'
                    ]);
                }
                //开启事务
                try {
                    DB::beginTransaction();

                    $merchantStore = MerchantStore::where('store_id', $store_id)->select('merchant_id')->first();
                    if ($merchantStore) {
                        $upIN = [
                            'name' => $people,
                            'phone' => $people_phone,
                        ];
                        Merchant::where('id', $merchantStore->merchant_id)->update($upIN);
                    }

                    //拼装门店信息
                    $stores = [
                        'config_id' => $config_id,
                        //'user_id' => $user_id,
                        'merchant_id' => '',
                        'store_id' => $store_id,
                        'store_name' => trim($store_name),
                        'store_type_name' => trim($store_type_name),
                        'store_email' => trim($store_email),
                        'store_short_name' => trim($store_short_name),
                        'people' => trim($people),//
                        'people_phone' => trim($people_phone),
                        'province_code' => $province_code,
                        'city_code' => $city_code,
                        'area_code' => $area_code,
                        'province_name' => $province_name,
                        'city_name' => $city_name,
                        'area_name' => $area_name,
                        'store_address' => trim($store_address),
                        'head_name' => trim($head_name), //法人
                        'head_sfz_no' => trim($head_sfz_no),
                        'head_sfz_time' => $head_sfz_time,
                        'head_sfz_stime' => $head_sfz_stime,
                        'store_type' => $store_type,
                        'status' => 1,
                        'status_desc' => "自动审核通过",
                        'admin_status' => 1,
                        'admin_status_desc' => "自动审核通过",
                        'weixin_no' => "",
                        'alipay_name' => "",
                        'alipay_account' => "",
                        'source' => $user->source, ////来源,01:畅立收，02:河南畅立收
                    ];

                    if ($store_type == '1' || $store_type == '2') {
                        $stores['store_license_no'] = $store_license_no;
                        $stores['store_license_time'] = $store_license_time;
                        $stores['store_license_stime'] = $store_license_stime;
                    }

                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                        'head_sfz_img_a' => $head_sfz_img_a,
                        'head_sfz_img_b' => $head_sfz_img_b
                    ];
                    if ($store_type == '1' || $store_type == '2') {
                        $store_imgs['store_license_img'] = $store_license_img;
                    }
                    $store = Store::where('store_id', $store_id)->first();
                    if (!$store) {
                        return json_encode([
                            'status' => 2,
                            'message' => '门店未认证'
                        ]);
                    } else {
                        $storeImg = StoreImg::where('store_id', $store_id)->first();

                        if ($storeImg) {
                            StoreImg::where('store_id', $store_id)->update($store_imgs);
                        } else {
                            StoreImg::create($store_imgs);
                        }

                        $store->update(array_filter($stores));
                        $store->save();
                        $easePayStore = EasePayStore::where('store_id', $store_id)->select('*')->first();
                        if ($easePayStore) {
                            EasePayStore::where('store_id', $store_id)->update([
                                'profession' => $occupation,
                                'profession_name' => $occupation_name,
                                'contact_email' => $contact_email
                            ]);
                        } else {
                            EasePayStore::create([
                                'store_id' => $store_id,
                                'profession' => $occupation,
                                'profession_name' => $occupation_name,
                                'contact_email' => $contact_email
                            ]);
                        }

                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                        $storeSteps->save();
                    } else {
                        StoreSteps::create([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '执照信息'
                        ]);
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 2) {
                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_bank_type' => 'required',
                    'store_bank_name' => 'required',
                    'store_bank_no' => 'required',
                    'bank_no' => 'required',
                    'sub_bank_name' => 'required',
                    'bank_name' => 'required',
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_bank_type' => '结算类型',
                    'store_bank_name' => '开户名称',
                    'store_bank_no' => '银行卡号',
                    'bank_no' => '联行号',
                    'sub_bank_name' => '支行名称',
                    'bank_name' => '银行名称',
                ]);
                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }
                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();
                $storeBank = StoreBank::where('store_id', $store_id)->first();

                //开启事务
                try {
                    DB::beginTransaction();
                    //银行卡信息
                    $store_banks = [
                        'store_id' => $store_id,
                        'store_bank_type' => $store_bank_type,
                        'store_bank_name' => $store_bank_name,
                        'store_bank_no' => $store_bank_no,
                        'bank_no' => $bank_no,
                        'sub_bank_name' => $sub_bank_name,
                        'bank_name' => $bank_name,
                        'reserved_mobile' => $reserved_mobile,
                        'store_bank_phone' => $store->people_phone,
                        'bank_province_code' => $store->province_code,
                        'bank_city_code' => $store->city_code,
                        'bank_area_code' => $store->area_code,
                    ];

                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                    ];

                    if ($bankType == '01') {
                        $store_imgs['bank_img_a'] = $bank_img_a;
                    } else if ($bankType == '02') {
                        $store_imgs['store_industrylicense_img'] = $store_industrylicense_img;
                    } else if ($bankType == '03') {
                        $store_imgs['store_auth_bank_img'] = $store_auth_bank_img;
                        $store_imgs['bank_img_a'] = $bank_img_a;
                    }
                    if ($store_type == '3') {
                        $store_imgs['bank_sc_img'] = $bank_sc_img;
                    }
                    if ($storeBank) {
                        StoreBank::where('store_id', $store_id)->update($store_banks);
                    } else {
                        StoreBank::create($store_banks);
                    }

                    if ($storeImg) {
                        StoreImg::where('store_id', $store_id)->update($store_imgs);
                    } else {
                        StoreImg::create($store_imgs);
                    }

                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();

                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '账户信息'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }

            } else if ($saveStep == 3) {

                $validate = Validator::make($data, [
                    'storeId' => 'required',
                    'store_logo_img' => 'required',
                    'store_img_b' => 'required',
                    'category_id' => 'required',
                    'mccId' => 'required',
                    'bank_sc_img' => 'required',
                    'wechat_applet' => 'required'
                ], [
                    'required' => ':attribute参数为必填项'
                ], [
                    'storeId' => '门店ID',
                    'store_logo_img' => '门头照',
                    'store_img_b' => '经营内容照',
                    'category_id' => '经营类型',
                    'mccId' => '业务分类',
                    'bank_sc_img' => '手持身份证',
                    'wechat_applet' => '微信公众号流程图',
                ]);

                if ($validate->fails()) {
                    $this->status = -1;
                    $this->message = $validate->getMessageBag()->first();
                    return $this->format();
                }

                $store = Store::where('store_id', $store_id)->first();
                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店未认证'
                    ]);
                }
                $storeImg = StoreImg::where('store_id', $store_id)->first();
                $easePayStore = EasePayStore::where('store_id', $store_id)->select('id')->first();

                //开启事务
                try {
                    DB::beginTransaction();
                    //图片信息
                    $store_imgs = [
                        'store_id' => $store_id,
                        'store_logo_img' => $store_logo_img,
                        'store_img_b' => $store_img_b,
                    ];
                    $store_ease = [
                        'mcc_name' => $businScope,
                        'mcc_id' => $mccId,
                        'wechat_applet' => $wechat_applet
                    ];
                    if ($storeImg) {
                        StoreImg::where('store_id', $store_id)->update($store_imgs);
                    } else {
                        StoreImg::create($store_imgs);
                    }
                    $stores = [
                        'store_id' => $store_id,
                        'category_id' => $category_id,
                        'category_name' => $category_name,
                    ];
                    Store::where('store_id', $store_id)->update($stores);
                    if ($easePayStore) {
                        EasePayStore::where('store_id', $store_id)->update($store_ease);
                    } else {
                        $store_ease['store_id'] = $store_id;
                        EasePayStore::create($store_ease);
                    }
                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();
                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '门店信息'
                        ]);
                        $storeSteps->save();
                    }

                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 4) {

                $rate = $request->get('rate', '');
                $store = Store::where('store_id', $store_id)
                    ->first();
                if (!$store) {
                    return json_encode(['status' => -1, 'message' => '门店不存在']);
                }

                try {


                    //查找下级门店共享通道
                    if ($store->pid == 0) {
                        $sub_store = Store::where('pid', $store->id)
                            ->where('pay_ways_type', 1)
                            ->select('store_id')
                            ->get();
                        foreach ($sub_store as $k => $v) {
                            $data['store_id'] = $v->store_id;
                            $data['rate'] = $rate;
                            $data['company'] = $company;

                            $this->app_send_ways_data($data);
                        }
                    }

                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['company'] = $company;
                    $retStorePayWay = $this->app_send_ways_data($data);

                    if ($retStorePayWay['status'] == -1) {
                        return json_encode([
                            'status' => -1,
                            'message' => $retStorePayWay['message']
                        ]);
                    }


                    $storeSteps = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();
                    if ($storeSteps) {
                        $storeSteps->update([
                            'store_id' => $store_id,
                            'step' => $saveStep,
                            'company_type' => $company,
                            'remark' => '提交审核'
                        ]);
                        $storeSteps->save();
                    }
//                    $code = '';
//                    $SettleModeType = '';//结算方式
//                    $spwObj = new StorePayWaysController();
//
//                    $store_ways_desc = DB::table('store_ways_desc')
//                        ->where('is_show', '1')
//                        ->where('company', $company)
//                        ->first();
//                    $type = $store_ways_desc->ways_type;
//
//                    return $spwObj->base_open_ways($type, $code, $store_id, $SettleModeType, '', '');


                } catch (\Exception $ex) {
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => -1,
                        'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                    ]);
                }
            } else if ($saveStep == 5) { //提交审核

            }

            return json_encode([
                'status' => '1',
                'message' => '操作成功',
                'data' => [
                    'store_id' => $store_id
                ]
            ]);
        } catch (\Exception $ex) {
            Log::info($ex);
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    public function get_easypay_store(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_id = $token->user_id;
            $store_id = $request->get('storeId', ''); //门店id
            $company = $request->get('company', 'easypay');

            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => -1,
                    'message' => '用户不存在'
                ]);
            }

            $store = Store::where('store_id', $store_id)->first();

            if (!$store) {
                return json_encode([
                    'status' => -1,
                    'message' => '门店未认证'
                ]);
            }
            $merchantInfo = Merchant::where('id', $store->merchant_id)->first();
            $store_bank = StoreBank::where('store_id', $store_id)->first();
            $store_img = StoreImg::where('store_id', $store_id)->first();
            $storePayWay = StorePayWay::where('store_id', $store_id)->where('company', $company)->first();
            $easyPayStoreObj = EasypayStoresImages::where('store_id', $store_id)->first();

            $type = 'head_info,store_info,account_info,license_info,easypay_info,rate_info';

            $store_step = StoreSteps::where('store_id', $store_id)->where('company_type', $company)->first();
            if (!$store_step) {
                $data_r['store_step'] = 0;
            } else {
                $data_r['store_step'] = $store_step->step;
            }

            $type_array = explode(",", $type);
            foreach ($type_array as $k => $v) {
                if ($v == 'rate_info') {
                    if ($storePayWay) {
                        $data = [
                            'rate' => $storePayWay->rate,//法人
                            'company' => $storePayWay->company,
                        ];
                    } else {
                        $data = [];
                    }

                    $data_r[$v] = $data;
                }

                if ($v == 'head_info') {
                    $data = [
                        'head_name' => $store->head_name,//法人
                        'head_sfz_no' => $store->head_sfz_no,
                        'head_sfz_img_a' => $store_img->head_sfz_img_a,
                        'head_sfz_img_b' => $store_img->head_sfz_img_b,
                        'people' => $store->people,
                        'people_phone' => $store->people_phone,
                        'head_sfz_time' => $store->head_sfz_time,
                        'head_sfz_stime' => $store->head_sfz_stime,
                    ];

                    $data_r[$v] = $data;
                }

                if ($v == 'store_info') {
                    $data = [
                        'id' => $store->id,
                        'user_id' => $store->user_id,
                        'people' => $store->people,
                        'people_phone' => $store->people_phone,
                        'store_email' => $store->store_email,
                        'store_name' => $store->store_name,
                        'store_short_name' => $store->store_short_name,
                        'province_code' => $store->province_code,
                        'city_code' => $store->city_code,
                        'area_code' => $store->area_code,
                        'province_name' => $store->province_name,
                        'city_name' => $store->city_name,
                        'area_name' => $store->area_name,
                        'store_address' => $store->store_address,
                        'store_type' => $store->store_type,
                        'store_type_name' => $store->store_type_name,
                        'category_id' => $store->category_id,
                        'category_name' => $store->category_name,
                        'store_logo_img' => $store_img->store_logo_img,
                        'store_img_a' => $store_img->store_img_a,
                        'store_img_b' => $store_img->store_img_b,
                        'store_img_c' => $store_img->store_img_c,
                        'status' => $store->status,
                        'status_desc' => $store->status_desc,
                        'weixin_name' => $store->weixin_name,
                        'weixin_no' => $store->weixin_no,
                        'alipay_name' => $store->alipay_name,
                        'alipay_account' => $store->alipay_account,
                        'source' => $store->source,
                        'source_type' => $store->source_type,
                        'merchant_no' => $merchantInfo->merchant_no,

                    ];

                    $data_r[$v] = $data;
                }

                if ($v == 'account_info') {
                    if ($store_bank) {
                        $data = [
                            'reserved_mobile' => $store_bank->reserved_mobile,
                            'store_bank_no' => $store_bank->store_bank_no,
                            'store_bank_phone' => $store_bank->store_bank_phone,
                            'store_bank_name' => $store_bank->store_bank_name,
                            'store_bank_type' => $store_bank->store_bank_type,
                            'bank_name' => $store_bank->bank_name,
                            'bank_no' => $store_bank->bank_no,
                            'sub_bank_name' => $store_bank->sub_bank_name,
                            'bank_province_code' => $store_bank->bank_province_code,
                            'bank_city_code' => $store_bank->bank_city_code,
                            'bank_area_code' => $store_bank->bank_area_code,
                            'bank_province_name' => $store_bank->bank_province_name,
                            'bank_city_name' => $store_bank->bank_city_name,
                            'bank_area_name' => $store_bank->bank_area_name,
                            'bank_img_a' => $store_img->bank_img_a,
                            'bank_img_b' => $store_img->bank_img_b,
                            'bank_sfz_img_a' => $store_img->bank_sfz_img_a,
                            'bank_sfz_img_b' => $store_img->bank_sfz_img_b,
                            'bank_sc_img' => $store_img->bank_sc_img,
                            'store_auth_bank_img' => $store_img->store_auth_bank_img,
                            'bank_sfz_time' => $store_bank->bank_sfz_time,
                            'bank_sfz_stime' => $store_bank->bank_sfz_stime,
                            'bank_sfz_no' => $store_bank->bank_sfz_no,
                            'store_email' => $store->store_email,
                        ];
                    } else {
                        $data = [];
                    }

                    $data_r[$v] = $data;
                }

                if ($v == 'license_info') {

                    $data = [
                        'store_license_no' => $store->store_license_no,
                        'store_license_time' => $store->store_license_time,
                        'store_license_stime' => $store->store_license_stime,
                        'store_license_img' => $store_img->store_license_img,
                        'store_other_img_a' => $store_img->store_other_img_a,
                        'store_other_img_b' => $store_img->store_other_img_b,
                        'store_other_img_c' => $store_img->store_other_img_c,
                        'store_industrylicense_img' => $store_img->store_industrylicense_img,
                        'head_sc_img' => $store_img->head_sc_img,
                        'head_store_img' => $store_img->head_store_img,
                    ];

                    $data_r[$v] = $data;
                }

                if ($v == 'easypay_info') {
                    if ($easyPayStoreObj) {
                        $data = [
                            'mcc_id' => $easyPayStoreObj->mcc_id,
                            'standard_flag' => $easyPayStoreObj->standard_flag,
                            'employee_num' => $easyPayStoreObj->employee_num,
                            'busin_form' => $easyPayStoreObj->busin_form,
                            'busin_beg_time' => $easyPayStoreObj->busin_beg_time,
                            'busin_end_time' => $easyPayStoreObj->busin_end_time,
                            'term_mode' => $easyPayStoreObj->term_mode,
                            'term_code' => $easyPayStoreObj->term_code,
                            'ter_user_name' => $easyPayStoreObj->ter_user_name,
                            'two_bars_open' => $easyPayStoreObj->two_bars_open,
                            'two_bars' => $easyPayStoreObj->two_bars,
                            'area_no' => $easyPayStoreObj->area_no,
                            'term_area' => $easyPayStoreObj->term_area,
                            'term_model' => $easyPayStoreObj->term_model,
                            'term_model_lic' => $easyPayStoreObj->term_model_lic,
                            'branch_name' => $easyPayStoreObj->branch_name,
                            'branch_manager' => $easyPayStoreObj->branch_manager,
                            'mer_mark' => $easyPayStoreObj->mer_mark,
                            'busin_scope' => $easyPayStoreObj->busin_scope,
                            'capital' => $easyPayStoreObj->capital,
                            'bank_name' => $easyPayStoreObj->bank_name,
                            'bank_code' => $easyPayStoreObj->bank_code,
                            'real_time_entry' => $easyPayStoreObj->real_time_entry,
                            'mer_area' => $easyPayStoreObj->mer_area,
                            'd_stlm_type' => $easyPayStoreObj->d_stlm_type,
                            'd_calc_val' => $easyPayStoreObj->d_calc_val,
                            'd_stlm_max_amt' => $easyPayStoreObj->d_stlm_max_amt,
                            'd_fee_low_limit' => $easyPayStoreObj->d_fee_low_limit,
                            'c_calc_val' => $easyPayStoreObj->c_calc_val,
                            'c_fee_low_limit' => $easyPayStoreObj->c_fee_low_limit,
                            'd_calc_type' => $easyPayStoreObj->d_calc_type,
                            'c_calc_type' => $easyPayStoreObj->c_calc_type,
                            'un_d_calc_val' => $easyPayStoreObj->un_d_calc_val,
                            'un_c_calc_val' => $easyPayStoreObj->un_c_calc_val,
                            'calc_type' => $easyPayStoreObj->calc_type,
                            'calc_val' => $easyPayStoreObj->calc_val,
                            'pay_date_type' => $easyPayStoreObj->pay_date_type,
                            'd1_is_open' => $easyPayStoreObj->d1_is_open,
                            'pub_bank_name' => $easyPayStoreObj->pub_bank_name,
                            'pub_bank_code' => $easyPayStoreObj->pub_bank_code,
                            'pub_account' => $easyPayStoreObj->pub_account,
                            'pub_acc_name' => $easyPayStoreObj->pub_acc_name,
                            'house_number_url' => $easyPayStoreObj->house_number_url,
                            'agreement_url' => $easyPayStoreObj->agreement_url,
                            'merc_regist_form_a_url' => $easyPayStoreObj->merc_regist_form_a_url,
                            'merc_regist_form_b_url' => $easyPayStoreObj->merc_regist_form_b_url,
                            'cert_of_auth_url' => $easyPayStoreObj->cert_of_auth_url,
                            'location_photo_url' => $easyPayStoreObj->location_photo_url,
                            'projectId' => $easyPayStoreObj->project_id,
                            'contract_no' => $easyPayStoreObj->contract_no,
                            'signatory_name' => $easyPayStoreObj->signatory_name,

                        ];
                    } else {
                        $data = [
                            'mcc_id' => '',
                            'standard_flag' => '',
                            'employee_num' => '',
                            'busin_form' => '',
                            'busin_beg_time' => '',
                            'busin_end_time' => '',
                            'term_mode' => '',
                            'term_code' => '',
                            'ter_user_name' => '',
                            'two_bars_open' => '',
                            'two_bars' => '',
                            'area_no' => '',
                            'term_area' => '',
                            'term_model' => '',
                            'term_model_lic' => '',
                            'branch_name' => '',
                            'branch_manager' => '',
                            'mer_mark' => '',
                            'busin_scope' => '',
                            'capital' => '',
                            'bank_name' => '',
                            'bank_code' => '',
                            'real_time_entry' => '',
                            'mer_area' => '',
                            'd_stlm_type' => '',
                            'd_calc_val' => '',
                            'd_stlm_max_amt' => '',
                            'd_fee_low_limit' => '',
                            'c_calc_val' => '',
                            'c_fee_low_limit' => '',
                            'd_calc_type' => '',
                            'c_calc_type' => '',
                            'un_d_calc_val' => '',
                            'un_c_calc_val' => '',
                            'calc_type' => '',
                            'calc_val' => '',
                            'pay_date_type' => '',
                            'd1_is_open' => '',
                            'pub_bank_name' => '',
                            'pub_bank_code' => '',
                            'pub_account' => '',
                            'pub_acc_name' => '',
                            'house_number_url' => '',
                            'agreement_url' => '',
                            'merc_regist_form_a_url' => '',
                            'merc_regist_form_b_url' => '',
                            'cert_of_auth_url' => '',
                            'location_photo_url' => '',
                            'projectId' => '',
                            'contract_no' => '',
                            'signatory_name' => '',
                        ];
                    }

                    $data_r[$v] = $data;
                }
            }

            $this->status = 1;
            $this->message = '成功';
            return $this->format($data_r);

        } catch (\Exception $ex) {
            Log::info($ex);
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    //修改费率APP
    public function app_send_ways_data($data)
    {
        try {
            $all_pay_ways = DB::table('store_ways_desc')
                ->where('is_show', '1')
                ->where('company', $data['company'])
                ->get();
            foreach ($all_pay_ways as $k => $v) {
                $gets = StorePayWay::where('store_id', $data['store_id'])
                    ->where('ways_source', $v->ways_source)
                    ->get();
                $count = count($gets);
                $ways = StorePayWay::where('store_id', $data['store_id'])->where('ways_type', $v->ways_type)->first();

                $data = [
                    'store_id' => $data['store_id'],
                    'ways_type' => $v->ways_type,
                    'company' => $v->company,
                    'ways_source' => $v->ways_source,
                    'ways_desc' => $v->ways_desc,
                    'sort' => ($count + 1),
                    'rate' => $data['rate'],
                    'settlement_type' => $v->settlement_type,
                ];
                if ($v->ways_source == 'unionpay') {
                    $data['pay_amount_e'] = 100;  //银联最大交易金额 100
                }
                if ($ways) {
                    $ways->update([
                        'rate' => $data['rate'],
                        'status' => '0',
                        'status_desc' => '未开通'
                    ]);
                    $ways->save();
                } else {
                    StorePayWay::create($data);
                }
            }

            return [
                'status' => 1,
                'message' => '修改成功',
            ];
        } catch (\Exception $e) {
            return [
                'status' => -1,
                'message' => '通道入库更新失败',
            ];
        }
    }

    /**
     * 门店认证修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function up_store(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_login_id = $token->user_id;
            $data = $request->except(['token']);

            $is_send_tpl_mess = $request->post('isSendTplMess', ''); //是否发送模板消息(1-开启;2-关闭)
            $is_get_phone = $request->post('is_get_phone', 2); //是否获取手机号(1-开启;2-关闭;默认2)
            $hkrt_bus_scope_code = $request->post('hkrt_bus_scope_code', ''); //经营范围
            $reserved_mobile = $request->post('reserved_mobile', ''); //预留手机号

            $store_id = date('Ymdhis', time()) . rand(1000, 9999);//随机
            $store_id = $request->get('store_id', $store_id);
            $config_id = $token->config_id;
            $user_id = $token->user_id;
            $user_id = $request->get('user_id', $user_id);

            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '用户不存在'
                ]);
            }
            $qrListInfo = QrListInfo::where('user_id', $user_id)->select('*')->get();
            $user_qr_num = count($qrListInfo);
            if ($user_qr_num < 10) {
                $qr_gap_num = 10 - $user_qr_num;
                return json_encode([
                    'status' => 2,
                    'message' => '不可展业，库存差' . $qr_gap_num . '张'
                ]);
            }
            $merchant_no = $request->get('merchant_no', ''); //商户号
            //法人信息
            $people_phone = $request->get('people_phone', '');
            $store_email = $request->get('store_email', '');
            $head_name = $request->get('head_name', '');
            $head_sfz_no = $request->get('head_sfz_no', '');
            $bank_sfz_no = $request->get('bank_sfz_no', '');
            $people = $request->get('people', '');

            $head_sfz_img_a = $request->get('head_sfz_img_a', '');
            $head_sfz_img_b = $request->get('head_sfz_img_b', '');

            $head_sfz_time = $request->get('head_sfz_time', '');
            $head_sfz_stime = $request->get('head_sfz_stime', '');
            //$head_sfz_time = $this->time($head_sfz_time);
            if ($head_sfz_time != '长期') {
                $head_sfz_time = $this->time($head_sfz_time);
            }
            $head_sfz_stime = $this->time($head_sfz_stime);

            //门店信息
            $store_name = $request->get('store_name', '');
            $store_short_name = $request->get('store_short_name', $store_name);
            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');
            $store_address = $request->get('store_address', '');
            $store_type = $request->get('store_type', '');
            $store_type_name = $request->get('store_type_name', '');
            $dlb_micro_biz_type = $request->get('DLB_micro_biz_type', '');
            $dlb_industry = $request->get('DLB_industry', '');
            $dlb_second_industry = $request->get('DLB_second_industry', '');
            $dlb_province = $request->get('DLB_province', '');
            $dlb_city = $request->get('DLB_city', '');
            $dlb_bank = $request->get('DLB_bank', '');
            $dlb_sub_bank = $request->get('DLB_sub_bank', '');
            $dlb_pay_bank_list = $request->get('DLB_pay_bank_list', '');
            if ($dlb_pay_bank_list) {
                $dlb_pay_bank_list = json_encode($dlb_pay_bank_list);
            }
            $longitude = $request->get('longitude', '');
            $latitude = $request->get('latitude', '');

            $category_id = $request->get('category_id', '');
            $category_name = $request->get('category_name', '');
            $store_logo_img = $request->get('store_logo_img', '');
            $store_img_a = $request->get('store_img_a', '');
            $store_img_b = $request->get('store_img_b', '');
            $store_img_c = $request->get('store_img_c', '');
            $weixin_name = $request->get('weixin_name', '');
            $weixin_no = $request->get('weixin_no', '');
            $alipay_name = $request->get('alipay_name', '');
            $alipay_account = $request->get('alipay_account', '');

            //收款信息
            $store_alipay_account = $request->get('store_alipay_account', '');
            $store_bank_no = $request->get('store_bank_no', '');
            $store_bank_phone = $request->get('store_bank_phone', '');
            $store_bank_name = $request->get('store_bank_name', '');
            $store_bank_type = $request->get('store_bank_type', '');
            $bank_name = $request->get('bank_name', '');
            $bank_no = $request->get('bank_no', '');
            $sub_bank_name = $request->get('sub_bank_name', '');
            $bank_province_code = $request->get('bank_province_code', '');
            $bank_city_code = $request->get('bank_city_code', '');
            $bank_area_code = $request->get('bank_area_code', '');
            $bank_img_a = $request->get('bank_img_a', '');
            $bank_img_b = $request->get('bank_img_b', '');

            $bank_sfz_img_a = $request->get('bank_sfz_img_a', '');
            $bank_sfz_img_b = $request->get('bank_sfz_img_b', '');
            $bank_sc_img = $request->get('bank_sc_img', '');
            $store_auth_bank_img = $request->get('store_auth_bank_img', '');

            $bank_sfz_time = $request->get('bank_sfz_time', '');
            $bank_sfz_stime = $request->get('bank_sfz_stime', '');
            //$bank_sfz_time = $this->time($bank_sfz_time);
            if ($bank_sfz_time != '长期') {
                $bank_sfz_time = $this->time($bank_sfz_time);
            }
            $bank_sfz_stime = $this->time($bank_sfz_stime);

            //证照信息
            $store_license_no = $request->get('store_license_no', '');
            $store_license_time = $request->get('store_license_time', '');
            $store_license_stime = $request->get('store_license_stime', '');

            $head_sc_img = $request->get('head_sc_img', '');
            $head_store_img = $request->get('head_store_img', '');

            $store_license_img = $request->get('store_license_img', '');
            $store_industrylicense_img = $request->get('store_industrylicense_img', '');
            $store_other_img_a = $request->get('store_other_img_a', '');
            $store_other_img_b = $request->get('store_other_img_b', '');
            $store_other_img_c = $request->get('store_other_img_c', '');
            $dlb_settler_hold_settlecard = $request->get('DLB_settler_hold_settlecard', ''); //哆啦宝-结算人手持结算卡


            $food_service_lic = $request->get('food_service_lic', '');
            $food_health_lic = $request->get('food_health_lic', '');
            $food_business_lic = $request->get('food_business_lic', '');
            $food_circulate_lic = $request->get('food_circulate_lic', '');
            $food_production_lic = $request->get('food_production_lic', '');
            $tobacco_img = $request->get('tobacco_img', '');

            $app_dest = $request->get('srcApp', '');

            //拼装门店信息
            $stores = [
                'config_id' => $config_id,
                'user_id' => $user_id,
                'merchant_id' => '',
                'store_id' => $store_id,
                'store_name' => $store_name,
                'store_type_name' => $store_type_name,
                'store_email' => $store_email,
                'store_short_name' => $store_short_name,
                'people' => $people, //
                'people_phone' => $people_phone,
                'province_code' => $province_code,
                'city_code' => $city_code,
                'area_code' => $area_code,
                'store_address' => $store_address,
                'head_name' => $head_name, //法人
                'head_sfz_no' => $head_sfz_no,
                'head_sfz_time' => $head_sfz_time,
                'head_sfz_stime' => $head_sfz_stime,
                'category_id' => $category_id,
                'category_name' => $category_name,
                'weixin_name' => isset($weixin_name) ? $weixin_name : "",
                'weixin_no' => isset($weixin_no) ? $weixin_no : "",
                'alipay_name' => isset($alipay_name) ? $alipay_name : "",
                'alipay_account' => isset($alipay_account) ? $alipay_account : "",
                'source' => $user->source ////来源,01:畅立收，02:河南畅立收
            ];
            $stores['status'] = 1;
            $stores['status_desc'] = '自动审核通过';
            $stores['admin_status'] = 1;
            $stores['admin_status_desc'] = '系统自动审核';
            // if ($app_dest=='app'){
            //     $stores['status']=1;
            //     $stores['status_desc']='自动审核通过';
            //     $stores['admin_status']=1;
            //     $stores['admin_status_desc']='系统自动审核';
            // }else{
            //     $stores['status']=0;
            //     $stores['status_desc']='等待审核';
            //     $stores['admin_status']=0;
            //     $stores['admin_status_desc']='等待审核';
            // }
            if ($store_license_no) {
                $stores['store_license_no'] = $store_license_no;
            }
            if ($store_license_time) {
                $stores['store_license_time'] = $store_license_time;
            }
            if ($store_license_stime) {
                $stores['store_license_stime'] = $store_license_stime;
            }
            if ($dlb_micro_biz_type) {
                $stores['dlb_micro_biz_type'] = $dlb_micro_biz_type;
            } //哆啦宝-经营类型
            if ($dlb_industry) {
                $stores['dlb_industry'] = $dlb_industry;
            } //哆啦宝店铺一级行业
            if ($dlb_second_industry) {
                $stores['dlb_second_industry'] = $dlb_second_industry;
            } //店铺二级行业
            if ($dlb_province) {
                $stores['dlb_province'] = $dlb_province;
            } //哆啦宝-商户省份
            if ($dlb_city) {
                $stores['dlb_city'] = $dlb_city;
            } //哆啦宝商户城市
            if ($dlb_bank) {
                $stores['dlb_bank'] = $dlb_bank;
            } //哆啦宝-银行名称
            if ($dlb_sub_bank) {
                $stores['dlb_sub_bank'] = $dlb_sub_bank;
            } //哆啦宝-银行分行名称
            if ($dlb_pay_bank_list) {
                $stores['dlb_pay_bank_list'] = $dlb_pay_bank_list;
            } //支付类型+费率
            if ($hkrt_bus_scope_code) {
                $stores['hkrt_bus_scope_code'] = $hkrt_bus_scope_code;
            } //经营范围
            if ($store_type) {
                $stores['store_type'] = $store_type;
            }
            if ($province_code) {
                $stores['province_name'] = $this->city_name($province_code);
            }
            if ($city_code) {
                $stores['city_name'] = $this->city_name($city_code);
            }
            if ($area_code) {
                $stores['area_name'] = $this->city_name($area_code);
            }
            if ($longitude) {
                $stores['lng'] = $longitude;
            } //经度
            if ($latitude) {
                $stores['lat'] = $latitude;
            } //纬度
            if ($is_send_tpl_mess) {
                $stores['is_send_tpl_mess'] = $is_send_tpl_mess;
            }
            if ($is_get_phone) {
                $stores['is_get_phone'] = $is_get_phone;
            }

            //图片信息
            $store_imgs = [
                'store_id' => $store_id,
                'head_sfz_img_a' => $head_sfz_img_a,
                'head_sfz_img_b' => $head_sfz_img_b,
                'store_license_img' => $store_license_img,
                'store_industrylicense_img' => $store_industrylicense_img,
                'store_logo_img' => $store_logo_img,
                'store_img_a' => $store_img_a,
                'store_img_b' => $store_img_b,
                'store_img_c' => $store_img_c,
                'bank_img_a' => $bank_img_a,
                'bank_img_b' => $bank_img_b,
                'store_other_img_a' => $store_other_img_a,
                'store_other_img_b' => $store_other_img_b,
                'store_other_img_c' => $store_other_img_c,
                'head_sc_img' => $head_sc_img,
                'head_store_img' => $head_store_img,
                'bank_sfz_img_a' => $bank_sfz_img_a,
                'bank_sfz_img_b' => $bank_sfz_img_b,
                'bank_sc_img' => $bank_sc_img,
                'store_auth_bank_img' => $store_auth_bank_img,
                'food_service_lic' => $food_service_lic,
                'food_health_lic' => $food_health_lic,
                'food_business_lic' => $food_business_lic,
                'food_circulate_lic' => $food_circulate_lic,
                'food_production_lic' => $food_production_lic,
                'tobacco_img' => $tobacco_img,
            ];
            if ($dlb_settler_hold_settlecard) {
                $store_imgs['dlb_settler_hold_settlecard'] = $dlb_settler_hold_settlecard;
            } //哆啦宝-结算人手持结算卡

            //银行卡信息
            $store_banks = [
                'store_id' => $store_id,
                'store_bank_no' => $store_bank_no,
                'store_bank_name' => $store_bank_name,
                'store_bank_phone' => $store_bank_phone,
                'store_bank_type' => $store_bank_type,
                'bank_name' => $bank_name,
                'bank_no' => $bank_no,
                'sub_bank_name' => $sub_bank_name,
                'bank_province_code' => $bank_province_code,
                'bank_city_code' => $bank_city_code,
                'bank_area_code' => $bank_area_code,
                'bank_sfz_no' => $bank_sfz_no,//持卡人默认是法人
                'bank_sfz_time' => $bank_sfz_time,
                'bank_sfz_stime' => $bank_sfz_stime
            ];
            if ($reserved_mobile) {
                $store_banks['reserved_mobile'] = $reserved_mobile;
            } //海科融通结算卡预留手机号

            $store = Store::where('store_id', $store_id)->first();
            $store_bank = StoreBank::where('store_id', $store_id)->first();
            $store_img = StoreImg::where('store_id', $store_id)->first();

            //80开头+当前日期+4位随机数
            $merchantNo = '';
            if ($merchant_no) { //更新操作
                $merchantNo = $merchant_no;
            } else {
                $merchantNo = $this->generateMerchantNo();
                //检查是否存在商户号
                $existMerchantNo = Merchant::where('merchant_no', $merchantNo)->first();
                if (!$existMerchantNo) {
                    $merchantNo = $this->generateMerchantNo(); //重新获取
                }
            }

            //提交的新商户必须填写
            if (!$store) {
                $check_data = [
                    'store_id' => '门店ID'
                ];
                $check = $this->check_required($request->except(['token']), $check_data);
                if ($check) {
                    return json_encode([
                        'status' => 2,
                        'message' => $check
                    ]);
                }
            }

            //开启事务
            try {
                DB::beginTransaction();

                if ($merchant_no) { //更新操作
                    $merchantInfo = Merchant::where('merchant_no', $merchant_no)->first();
                    if ($merchantInfo) {
                        $upIN = [
                            'name' => $people,
                            'phone' => $people_phone,
                        ];
                        $merchantInfo->update($upIN);
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '商户不存在'
                        ]);
                    }
                } else {
                    //注册账户
                    $dataIN = [
                        'pid' => 0,
                        'type' => 1,
                        'name' => $people,
                        'email' => '',
                        'password' => bcrypt('000000'),
                        'pay_password' => bcrypt('000000'),
                        'phone' => $people_phone,
                        'user_id' => $user_id, //推广员id
                        'config_id' => $config_id,
                        'wx_openid' => '',
                        'source' => $user->source, //来源,01:畅立收，02:河南畅立收
                        'merchant_no' => $merchantNo,
                    ];
                    $merchant = Merchant::create($dataIN);
                    $merchant_id = $merchant->id;

                    $stores['merchant_id'] = $merchant_id;
                    MerchantStore::create([
                        'store_id' => $store_id,
                        'merchant_id' => $merchant_id
                    ]);
                }

                if ($store) {
                    //修改   不修改归属ID
                    $stores['user_id'] = $store->user_id;
                    $stores['config_id'] = $store->config_id;
                    $stores = array_filter($stores, function ($v) {
                        if ($v == "") {
                            return false;
                        } else {
                            return true;
                        }
                    });
                    $store->update($stores);
                    $store->save();
                } else {

                    Store::create($stores);
                }

                if ($store_img) {
                    $store_img->update(array_filter($store_imgs));
                    $store_img->save();
                } else {
                    StoreImg::create($store_imgs);
                }

                if ($store_bank) {
                    $store_bank->update(array_filter($store_banks));
                    $store_bank->save();
                } else {
                    $stores['store_type'] = 1;
                    $stores['store_type_name'] = '个体';
                    StoreBank::create($store_banks);
                }
                DB::commit();


            } catch (\Exception $ex) {
                Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                DB::rollBack();
                return json_encode([
                    'status' => -1,
                    'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
                ]);
            }

            return json_encode([
                'status' => '1',
                'message' => '资料添加成功',
                'data' => [
                    'store_id' => $store_id
                ]
            ]);
        } catch (\Exception $ex) {
            Log::info($ex);
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    // 获取门店信息
    public function getStoreBaseInfo(Request $request)
    {
        $inputData = $request->all();

        // 参数校验
        $check_data = [
            'store_id' => '门店id',
        ];
        $check = $this->check_required($inputData, $check_data);
        if ($check) {
            return $this->sys_response(40000, $check);
        }

        $model = new Store();
        $data = $model->getStoreBaseInfo($inputData);

        if (empty($data)) {
            return $this->sys_response(40000, "暂无数据");
        } else {
            return $this->sys_response(200, "操作成功", $data);

        }

    }


    //城市名称
    public function city_name($city_code)
    {
        $city_name = "";
        $data = ProvinceCity::where('area_code', $city_code)->first();
        if ($data) {
            $city_name = $data->area_name;
        }

        return $city_name;
    }


    //所有系统通道
    public function pay_ways_all(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id');

            $store = Store::where('store_id', $store_id)
                ->select('user_id')
                ->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在',
                    'data' => []
                ]);
            }

            $data = [];
            $store_ways_desc = DB::table('store_ways_desc')
                ->where('is_show', '1')
                ->select()
                ->orderBy('company')
                ->get();

            $store_ways_desc = json_decode(json_encode($store_ways_desc), true);
            foreach ($store_ways_desc as $k => $value) {
                //代理不开通 不显示 --删除
//                $UserRate = UserRate::where('company', $value['company'])
//                    ->where('user_id', $store->user_id)
//                    ->select('id')
//                    ->first();
//                if (!$UserRate) {
//                    continue;
//                }

                $has = StorePayWay::where('store_id', $store_id)
                    ->where('ways_type', $value['ways_type'])
                    ->first();
                if ($has) {
                    $data[$k] = $value;
                    $data[$k]['rate'] = $has->rate;
                    $data[$k]['status'] = $has->status;
                    $data[$k]['status_desc'] = $has->status_desc;
                    $data[$k]['icon'] = '';
                    $data[$k]['rate_a'] = $has->rate_a;
                    $data[$k]['rate_b'] = $has->rate_b;
                    $data[$k]['rate_b_top'] = $has->rate_b_top;
                    $data[$k]['rate_c'] = $has->rate_c;
                    $data[$k]['rate_d'] = $has->rate_d;
                    $data[$k]['rate_d_top'] = $has->rate_d_top;
                    $data[$k]['rate_e'] = $has->rate_e;
                    $data[$k]['rate_f'] = $has->rate_f;
                    $data[$k]['rate_f_top'] = $has->rate_f_top;
                    //如果是刷卡费率读取
                    //新大陆刷卡
                    if (in_array($value['ways_type'], [8005, 6005])) {
                        $data[$k]['rate'] = '贷记卡=' . $has->rate_e . '%,借记卡=' . $has->rate_f;
                    }

                    //银联扫码
                    if (in_array($value['ways_type'], [8004, 6004])) {
                        $data[$k]['rate'] = $has->rate_c;
                    }

                    $data = array_values($data);
                } else {
                    $rate = $value['store_all_rate'];//默认是系统费率
                    $rate_a = $value['store_all_rate_a'];//默认是系统费率
                    $rate_b = $value['store_all_rate_b'];//默认是系统费率
                    $rate_b_top = $value['store_all_rate_b_top'];//默认是系统费率
                    $rate_e = $value['store_all_rate_e'];//默认是系统费率
                    $rate_c = $value['store_all_rate_c'];//默认是系统费率
                    $rate_d = $value['store_all_rate_d'];//默认是系统费率
                    $rate_d_top = $value['store_all_rate_d_top'];//默认是系统费率
                    $rate_f = $value['store_all_rate_f'];//默认是系统费率
                    $rate_f_top = $value['store_all_rate_f_top'];//默认是系统费率
                    $data[$k] = $value;
                    //代理商的费率
                    $user_rate = UserRate::where('user_id', $store->user_id)
                        ->where('ways_type', $value['ways_type'])
                        ->first();
                    if ($user_rate) {
                        $rate = $user_rate->store_all_rate;//显示代理商的默认费率
                        $rate_a = $user_rate->store_all_rate_a;//默认是系统费率
                        $rate_b = $user_rate->store_all_rate_b;//默认是系统费率
                        $rate_c = $user_rate->store_all_rate_c;//默认是系统费率
                        $rate_b_top = $user_rate->store_all_rate_b_top;//默认是系统费率
                        $rate_d = $user_rate->store_all_rate_d;//默认是系统费率
                        $rate_d_top = $user_rate->store_all_rate_d_top;//默认是系统费率
                        $rate_e = $user_rate->store_all_rate_e;//默认是系统费率
                        $rate_f = $user_rate->store_all_rate_f;//默认是系统费率
                        $rate_f_top = $user_rate->store_all_rate_f_top;//默认是系统费率
                    }

                    $data[$k]['rate'] = $rate; //
                    $data[$k]['status'] = 0;
                    $data[$k]['status_desc'] = '未开通';
                    $data[$k]['icon'] = '';
                    $data[$k]['rate_a'] = $rate_a;
                    $data[$k]['rate_b'] = $rate_b;
                    $data[$k]['rate_c'] = $rate_c;
                    $data[$k]['rate_d'] = $rate_d;
                    $data[$k]['rate_d_top'] = $rate_d_top;
                    $data[$k]['rate_b_top'] = $rate_b_top;
                    $data[$k]['rate_e'] = $rate_e;
                    $data[$k]['rate_f'] = $rate_f;
                    $data[$k]['rate_f_top'] = $rate_f_top;
                    //如果是刷卡费率读取
                    //新大陆刷卡
                    if (in_array($value['ways_type'], [8005, 6005])) {
                        $data[$k]['rate'] = '贷记卡=' . $rate_e . '%,借记卡=' . $rate_f;
                    }

                    //银联扫码
                    if (in_array($value['ways_type'], [8004, 6004])) {
                        $data[$k]['rate'] = $rate_c;
                    }

                    $data = array_values($data);
                }
            }

            return json_encode([
                'status' => 1,
                'code' => 0,
                'data' => $data
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => 0, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }
    }


    //所有通道信息
    public function pay_ways_all_new()
    {
        try {
            $token = $this->parseToken();

            $data = DB::table('store_ways_desc')
                ->where('is_show', '1')
                ->select('id', 'ways_type', 'ways_desc', 'ways_source', 'company')
                ->get();
        } catch (\Exception $exception) {
            $this->status = '2';
            $this->message = $exception->getMessage() . ' - ' . $exception->getLine();
            return $this->format();
        }

        $this->status = '1';
        $this->message = '成功返回数据';
        return $this->format($data);
    }


    //所有系统通道-new
    public function store_all_pay_way_lists(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id');

            $data = [];
            $store_all_pay_way_lists = DB::table('store_all_pay_way_lists')
                ->where('is_show', '1')
                ->select('ways_count', 'company', 'company_desc')
                ->get();
            $store_all_pay_way_lists = json_decode(json_encode($store_all_pay_way_lists), true);
            $i = 0;
            foreach ($store_all_pay_way_lists as $k => $value) {
                $has = StorePayWay::where('store_id', $store_id)
                    ->where('company', $value['company'])
                    ->select('status')
                    ->first();

                //代理不开通不
                $UserRate = UserRate::where('company', $value['company'])
                    ->where('user_id', $user->user_id)
                    ->select('id')
                    ->first();
                if (!$UserRate) {
                    continue;
                }


                $data[$i] = $value;
                $data[$i]['status'] = '0';
                $data[$i]['status_desc'] = '未开通';
                //
                if ($has && $has->status == 1) {
                    $data[$i]['status'] = '1';
                    $data[$i]['status_desc'] = '开通成功';
                }

                if ($has && $has->status == 2) {
                    $data[$i]['status'] = '2';
                    $data[$i]['status_desc'] = '审核中';
                }

                if ($has && $has->status == 3) {
                    $data[$i]['status'] = '3';
                    $data[$i]['status_desc'] = '开通失败';
                }

                $i = $i + 1;
            }
            return json_encode(['status' => 1, 'data' => $data]);
        } catch (\Exception $exception) {
            return json_encode(['status' => 0, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }
    }


    //所有系统通道已经开通的-new
    public function store_open_pay_way_lists(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');

            if ($store_id) {
                $data = DB::table('store_all_pay_way_lists')
                    ->join('store_pay_ways', 'store_all_pay_way_lists.company', '=', 'store_pay_ways.company')
                    ->where('store_pay_ways.store_id', $store_id)
                    ->where('store_pay_ways.status', 1)
                    ->select('store_all_pay_way_lists.company', 'store_all_pay_way_lists.company_desc')
                    ->distinct()
                    ->get();
            } else {
                $data = DB::table('store_all_pay_way_lists')->where('is_show', '1')
                    ->get();
            }


            return json_encode(['status' => 1, 'data' => $data]);
        } catch (\Exception $exception) {
            return json_encode(['status' => 0, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }
    }


    //单个通道信息查询-new
    public function company_pay_ways_info(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id');
            $company = $request->get('company');
            $data = [];
            $store_ways_desc = DB::table('store_ways_desc')
                ->where('is_show', '1')
                ->where('company', $company)->get();


            $store = Store::where('store_id', $store_id)
                ->select('user_id')
                ->first();

            $ways_desc = "";
            $ways_status = "";
            foreach ($store_ways_desc as $k => $v) {
                $data[$k]['ways_desc'] = $v->ways_desc;
                $data[$k]['ways_source'] = $v->ways_source;
                $data[$k]['settlement_type'] = $v->settlement_type;
                $data[$k]['ways_type'] = $v->ways_type;

                $has = StorePayWay::where('store_id', $store_id)
                    ->where('ways_type', $v->ways_type)
                    ->first();

                $ways_desc = $ways_desc . $data[$k]['ways_desc'] . ',';


                if ($has) {
                    $data[$k]['rate'] = $has->rate;
                    $data[$k]['status'] = $has->status;
                    $data[$k]['status_desc'] = $has->status_desc;
                    $data[$k]['status_content'] = $has->status_desc;
                } else {
                    $rate = $v->rate;//默认是系统费率
                    //代理商的费率
                    $user_rate = UserRate::where('user_id', $store->user_id)
                        ->where('ways_type', $v->ways_type)
                        ->select('store_all_rate')
                        ->first();
                    if ($user_rate) {
                        $rate = $user_rate->store_all_rate;
                    }
                    $data[$k]['rate'] = $rate; //
                    $data[$k]['status'] = 0;
                    $data[$k]['status_desc'] = '未开通';
                    $data[$k]['status_content'] = '未开通';
                }

                $ways_status = $data[$k]['status'];
            }


            return json_encode(['status' => 1, 'message' => '数据返回成功', 'ways_status' => $ways_status, 'ways_desc' => $ways_desc, 'data' => $data]);
        } catch (\Exception $exception) {
            return json_encode(['status' => 0, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }
    }


    //申请通道-new
    public function open_company_pay_ways(Request $request)
    {
        try {
            $user = $this->parseToken();
            $company = $request->get('company');
            $store_id = $request->get('store_id');
            $code = $request->get('code', '888888');
            $SettleModeType = $request->get('Settle_mode_type', '01');//结算方式
            $sp = new StorePayWaysController();

            $store_ways_desc = DB::table('store_ways_desc')
                ->where('is_show', '1')
                ->where('company', $company)
                ->first();
            $type = $store_ways_desc->ways_type;

            return $sp->base_open_ways($type, $code, $store_id, $SettleModeType, '', '');
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '2',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine(),
            ]);
        }
    }


    //查询单个通道详细
    public function pay_ways_info(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id');
            $ways_type = $request->get('ways_type');

            $store = Store::where('store_id', $store_id)
                ->select('user_id')
                ->first();

            if (!$store) {
                return json_encode(['status' => 2, 'message' => '门店不存在', 'data' => []]);
            }
            $data = [];
            $store_ways_desc = DB::table('store_ways_desc')->where('is_show', '1')->where('ways_type', $ways_type)->first();
            $data['ways_desc'] = $store_ways_desc->ways_desc;
            $data['ways_source'] = $store_ways_desc->ways_source;
            $data['settlement_type'] = $store_ways_desc->settlement_type;
            $data['ways_type'] = $store_ways_desc->ways_type;

            $has = StorePayWay::where('store_id', $store_id)->where('ways_type', $ways_type)->first();
            if ($has) {
                $data['rate'] = $has->rate;
                $data['status'] = $has->status;
                $data['status_desc'] = $has->status_desc;
            } else {
                $rate = $store_ways_desc->rate;//默认是系统费率
                //代理商的费率
                $user_rate = UserRate::where('user_id', $store->user_id)
                    ->where('ways_type', $ways_type)
                    ->select('rate')
                    ->first();
                if ($user_rate) {
                    $rate = $user_rate->rate;
                }
                $data['rate'] = $rate; //
                $data['status'] = 0;
                $data['status_desc'] = '未开通';
            }


            return json_encode(['status' => 1, 'data' => $data]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }
    }


    //门店设置费率-扫码
    public function edit_store_rate(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id');
            $ways_type = $request->get('ways_type');
            $rate = $request->get('rate');

            $store = Store::where('store_id', $store_id)
                ->first();
            if (!$store) {
                return json_encode(['status' => 2, 'message' => '门店不存在', 'data' => []]);
            }

            if ($rate > 30) {
                return json_encode(['status' => 2, 'message' => '设置超限', 'data' => []]);
            }

            if (31000 < $ways_type && $ways_type < 31010) { //建行通道
                if ($rate < 0.1) {
                    return json_encode(['status' => 2, 'message' => '结算费率不能低于0.1', 'data' => []]);
                }
            }

            //共享通道不支持修改费率
            if ($user->level > 0 && $store->pay_ways_type) {
                $store_pid = Store::where('id', $store->pid)
                    ->select('store_id')
                    ->first();
                if ($store_pid) {
                    $StorePayWay = StorePayWay::where('store_id', $store_pid->store_id)
                        ->where('ways_type', $ways_type)
                        ->select('rate')
                        ->first();
                    if ($StorePayWay) {
                        if ($rate !== $StorePayWay->rate) {
                            return json_encode(['status' => 2, 'message' => '共享通道不支持修改费率']);
                        }
                    }
                }
            }

            $store_pid = $store->pid;
            $config_id = $store->config_id;

            $all_pay_ways = DB::table('store_ways_desc')
                ->where('is_show', '1')
                ->where('ways_type', $ways_type)
                ->select('company')
                ->first();
            $company = $all_pay_ways->company;

            $has = StorePayWay::where('store_id', $store_id)->where('ways_type', $ways_type)
                ->first();
            if ($has) {
                //修改费率
                $user = User::where('id', $user->user_id)->first();
//                $hasPermission = $user->hasPermissionTo('修改费率-' . $company);
//                if (!$hasPermission) {
//                    return json_encode(['status' => 2, 'message' => '没有权限设置费率']);
//                }
                $userlevel = $user->level;
                if ($userlevel > 0) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请联系管理员修改费率'
                    ]);
                }
                $status = $has->status;
                $status_desc = $has->status_desc;

                //如果通道审核中不允许修改费率
                if ($status == 2) {
                    return json_encode(['status' => 2, 'message' => '通道审核中不允许修改费率', 'data' => []]);
                }

                //京东收银通道修改费率
                if ($user->level > 0 && $has->status == 1 && 5999 < $ways_type && $ways_type < 6999) {
                    return json_encode(['status' => 2, 'message' => '京东通道开通后不支持软件修改', 'data' => $request->except(['token'])]);
                }

                //快钱支付同步平台费率
                if ($has->status == 1 && 2999 < $ways_type && $ways_type < 3999) {
                    $Merchant = MyBankStore::where('OutMerchantId', $store_id)
                        ->select('MerchantId')
                        ->first();

                    if (!$Merchant) {
                        return json_encode(['status' => 2, 'message' => '快钱商户信息不存在']);
                    }

                    //暂时全部修改
                    $data = [
                        'ta_rate' => $rate + 0.02,
                        'tb_rate' => $rate,
                        'MerchantId' => $Merchant->MerchantId,
                    ];
                    $obj = new MyBankController();
                    $re = $obj->StoreRate($data);

                    if ($re['status'] == 2) {
                        return json_encode($re);
                    }
                }

                //新大陆同步平台费率
                if ($user->level > 0 && $has->status == 1 && 7999 < $ways_type && $ways_type < 8999) {
                    $OBJ = new  UpdateController();

                    //修改费率
                    $up_data = [
                        'store_id' => $store_id,
                        'email' => $store->people_phone . '@139.com',
                        'phone' => $store->people_phone,
                        'rate' => $rate,
                        'rate_a' => $has->rate_a,
                        'rate_c' => $has->rate_c,
                        'rate_f' => $has->rate_f,
                        'rate_f_top' => $has->rate_f_top,
                        'rate_e' => $has->rate_e,
                    ];

                    // $return = $OBJ->update_store_rate($up_data);
                    //  return $return;

                    return json_encode(['status' => 2, 'message' => '暂不支持修改', 'data' => $request->except(['token'])]);
                }

                //传化银行同步平台费率
                if ($has->status == 1 && 11999 < $ways_type && $ways_type < 12999) {
                    $config = new TfConfigController();
                    $h_merchant = $config->tf_merchant($store_id, $store_pid);
                    if (!$h_merchant) {
                        return json_encode([
                            'status' => 2,
                            'message' => '传化商户号不存在'
                        ]);
                    }

                    $h_config = $config->tf_config($config_id, $h_merchant->qd);
                    if (!$h_config) {
                        return json_encode([
                            'status' => 2,
                            'message' => '传化配置不存在请检查配置'
                        ]);
                    }

                    $sub_mch_id = $h_merchant->sub_mch_id;
                    $rate1 = $rate * 10;
                    $post_data = [
                        'sub_mch_id' => $sub_mch_id,
                        'WECHAT_POS' => $rate1,
                        'WECHAT_SCAN' => $rate1,
                        'WECHAT_MP' => $rate1,
                        'ALIPAY_POS' => $rate1,
                        'ALIPAY_SCAN' => $rate1,
                        'ALIPAY_MP' => $rate1,
                    ];

                    $method = '/openapi/merchant/rate';
                    $obj = new \App\Api\Controllers\Tfpay\BaseController();
                    $obj->mch_id = $h_config->mch_id;
                    $obj->pub_key = $h_config->pub_key;
                    $obj->pri_key = $h_config->pri_key;
                    $re = $obj->api($post_data, $method, false);
                    if ($re['code'] != 0) {
                        return json_encode([
                            'status' => 2,
                            'message' => $re['msg']
                        ]);
                    }
                }

                //随行付银行同步平台费率
                if ($has->status == 1 && 12999 < $ways_type && $ways_type < 13999) {
                    $config = new VbillConfigController();
                    $vbill_config = $config->vbill_config($config_id);
                    if (!$vbill_config) {
                        return json_encode([
                            'status' => 2,
                            'message' => '随行付配置不存在请检查配置'
                        ]);
                    }

                    $vbill_merchant = $config->vbill_merchant($store_id, $store_pid);
                    if (!$vbill_merchant) {
                        return json_encode([
                            'status' => 2,
                            'message' => '随行付商户号不存在'
                        ]);
                    }

                    $obj = new \App\Api\Controllers\Vbill\BaseController();
                    $data = [
                        'orgId' => $vbill_config->orgId,
                        'reqId' => time(),
                        'version' => '1.0',
                        'timestamp' => date('Ymdhis', time()),
                        'signType' => 'RSA',
                        'reqData' => [
                            'mno' => $vbill_merchant->mno, //商户编号
                            'qrcodeList' => [
                                [
                                    'rateType' => '01', //微信
                                    'rate' => $rate,
                                ],
                                [
                                    'rateType' => '02', //支付宝
                                    'rate' => $rate,
                                ],
                                [
                                    'rateType' => '06', //银联单笔小于等于1000
                                    'rate' => '0.6',
                                ],
                                [
                                    'rateType' => '07', //银联单笔大于1000
                                    'rate' => '0.6',
                                ]
                            ]
                        ],
                    ];
                    $url = $obj->update_rate; //合作方通过本接口修改商户功能信息。区别于商户信息修改接口，本接口是同步流程，调用成功实时生效（费率次日0点生效）
                    $re = $obj->execute($data, $url, $vbill_config->privateKey, $vbill_config->sxfpublic);
                    //系统错误
                    if ($re['data']['respData']['bizCode'] == '0000') {
                        //同步费率操作记录
                        $sync_rate_res = VbillSyncRate::create([
                            'config_id' => $config_id,
                            'store_id' => $store_id,
                            'rate' => $has->rate,
                            'ori_type' => 1, //1-vbill;2-vbilla
                            'act_status' => 0 //0-未同步;1-已同步
                        ]);
                        if ($sync_rate_res) {
                            return json_encode([
                                'status' => 1,
                                'message' => '修改成功,费率次日0点生效'
                            ]);
                        } else {
                            return json_encode([
                                'status' => 2,
                                'message' => '同步费率,入库失败'
                            ]);
                        }
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => $re['data']['respData']['bizMsg']
                        ]);
                    }
                }

                //随行付a银行同步平台费率
                if ($has->status == 1 && 18999 < $ways_type && $ways_type < 19999) {
                    $config = new VbillConfigController();
                    $vbill_config = $config->vbilla_config($config_id);
                    if (!$vbill_config) {
                        return json_encode([
                            'status' => 2,
                            'message' => '随行付配置不存在请检查配置'
                        ]);
                    }
                    $vbill_merchant = $config->vbilla_merchant($store_id, $store_pid);
                    if (!$vbill_merchant) {
                        return json_encode([
                            'status' => 2,
                            'message' => '随行付商户号不存在'
                        ]);
                    }

                    $obj = new \App\Api\Controllers\Vbill\BaseController();
                    $data = [
                        'orgId' => $vbill_config->orgId,
                        'reqId' => time(),
                        'version' => '1.0',
                        'timestamp' => date('Ymdhis', time()),
                        'signType' => 'RSA',
                        'reqData' => [
                            'mno' => $vbill_merchant->mno,//商户编号
                            'qrcodeList' => [
                                [
                                    'rateType' => '01',
                                    'rate' => $rate,
                                ],
                                [
                                    'rateType' => '02',
                                    'rate' => $rate,
                                ],
                                [
                                    'rateType' => '06',
                                    'rate' => '0.6',
                                ],
                                [
                                    'rateType' => '07',
                                    'rate' => '0.6',
                                ]
                            ]
                        ],
                    ];
                    $url = $obj->update_rate;
                    $re = $obj->execute($data, $url, $vbill_config->privateKey, $vbill_config->sxfpublic);
                    //成功
                    if ($re['data']['respData']['bizCode'] == '0000') {
                        //同步费率操作记录
                        $sync_rate_res = VbillSyncRate::create([
                            'config_id' => $config_id,
                            'store_id' => $store_id,
                            'rate' => $has->rate,
                            'ori_type' => 2, //1-vbill;2-vbilla
                            'act_status' => 0 //0-未同步;1-已同步
                        ]);
                        if ($sync_rate_res) {
                            return json_encode([
                                'status' => 1,
                                'message' => '修改成功,费率次日0点生效'
                            ]);
                        } else {
                            return json_encode([
                                'status' => 2,
                                'message' => '同步费率,入库失败'
                            ]);
                        }
                    } else {
                        return json_encode(['status' => 2, 'message' => $re['data']['respData'] ['bizMsg']]);
                    }
                }

                //葫芦天下
                if ($has->status == 1 && 22999 < $ways_type && $ways_type < 23999) {
                    $HltxStore = HltxStore::where('store_id', $store_id)->first();
                    if ($HltxStore && !empty($HltxStore->mer_no)) {
                        //允许修改费率
                        $obj = new ManageController();
                        $hltx_config = $obj->pay_config($config_id);
                        $obj->init($hltx_config);//初始化配置
                        $return = $obj->hltx_edit_payinfo_merchant($HltxStore->mer_no, $rate);
                        if ($return['status'] != 1) {
                            return json_encode(['status' => 2, 'message' => 'HL费率修改失败:' . $return['message'], 'data' => []]);
                        }
                        $return = $return['data'];
                        if ($return['status'] == '00') {
                            //成功
                        } else {
                            return json_encode(['status' => 2, 'message' => '请求申请修改用户HL通道费率业务失败', 'data' => []]);
                        }
                    }
                }
            } else {
                //添加
                $user = User::where('id', $user->user_id)->first();
//                $hasPermission = $user->hasPermissionTo('设置费率-' . $company);
//                if (!$hasPermission) {
//                    return json_encode(['status' => 2, 'message' => '没有权限设置费率']);
//                }


                $status = 0;
                $status_desc = '未开通';
            }

            //查找下级门店共享通道
            if ($store->pid == 0) {
                $sub_store = Store::where('pid', $store->id)
                    ->where('pay_ways_type', 1)
                    ->select('store_id')
                    ->get();

                foreach ($sub_store as $k => $v) {
                    //暂时全部修改
                    $data['store_id'] = $v->store_id;
                    $data['rate'] = $rate;
                    $data['status'] = $status;
                    $data['status_desc'] = $status_desc;
                    $data['company'] = $company;
                    $this->send_ways_data($data);
                }
            }

            //暂时全部修改
            $data['store_id'] = $store_id;
            $data['rate'] = $rate;
            $data['status'] = $status;
            $data['status_desc'] = $status_desc;
            $data['company'] = $company;
            $return = $this->send_ways_data($data);
            return $return;
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . ' - ' . $exception->getLine()
            ]);
        }
    }


    //门店设置费率-刷卡
    public function edit_store_un_rate(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id');
            $ways_type = $request->get('ways_type');
            $rate_e = $request->get('rate_e');
            $rate_f = $request->get('rate_f');
            $rate_f_top = $request->get('rate_f_top', '20');
            $store = Store::where('store_id', $store_id)
                ->first();

            if (!$store) {
                return json_encode(['status' => 2, 'message' => '门店不存在', 'data' => []]);
            }

            //共享通道不支持修改费率
            if ($store->pay_ways_type) {
                return json_encode(['status' => 2, 'message' => '共享通道不支持修改费率']);
            }


            $ways = StorePayWay::where('store_id', $store_id)->where('ways_type', $ways_type)
                ->first();

            if (!$ways) {
                return json_encode(['status' => 2, 'message' => '请先设置扫码通道', 'data' => []]);
            } else {
                $status = $ways->status;
                $status_desc = $ways->status_desc;

                //如果通道审核中不允许修改费率
                if ($status == 2) {
                    return json_encode(['status' => 2, 'message' => '通道审核中不允许修改费率', 'data' => []]);
                }

                //新大陆同步平台费率
                if ($user->level > 0 && $ways->status == 1 && 7999 < $ways_type && $ways_type < 8999) {
                    $OBJ = new  UpdateController();
                    //修改费率
                    $up_data = [
                        'store_id' => $store_id,
                        'email' => $store->people_phone . '@139.com',
                        'phone' => $store->people_phone,
                        'rate' => $ways->rate,
                        'rate_a' => $ways->rate_a,
                        'rate_c' => $ways->rate_c,
                        'rate_f' => $rate_f,
                        'rate_f_top' => $rate_f_top,
                        'rate_e' => $rate_e,
                    ];

                    //  $return = $OBJ->update_store_rate($up_data);
                    // return $return;

                    return json_encode(['status' => 2, 'message' => '暂不支持软件修改', 'data' => $request->except(['token'])]);
                }


                //京东收银通道修改费率
                if ($user->level > 0 && $ways->status == 1 && 5999 < $ways_type && $ways_type < 6999) {
                    return json_encode(['status' => 2, 'message' => '京东通道开通后不支持软件修改', 'data' => $request->except(['token'])]);
                }
            }


            //全部修改
            $company = $ways->company;
            StorePayWay::where('store_id', $store_id)
                ->where('company', $company)
                ->update([
                    'rate_e' => $rate_e,
                    'rate_f' => $rate_f,
                    'rate_f_top' => $rate_f_top,
                ]);


            return json_encode(['status' => 1, 'message' => '设置成功', 'data' => []]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }
    }


    //门店设置费率-银联扫码
    public function edit_store_unqr_rate(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id');
            $ways_type = $request->get('ways_type');
            $rate_a = $request->get('rate_a');
            $rate_b = $request->get('rate_b');
            $rate_b_top = $request->get('rate_b_top', '20');
            $rate_c = $request->get('rate_c');
            $rate_d = $request->get('rate_d');
            $rate_d_top = $request->get('rate_d_top', '20');
            $store = Store::where('store_id', $store_id)
                ->first();

            if (!$store) {
                return json_encode(['status' => 2, 'message' => '门店不存在', 'data' => []]);
            }

            //共享通道不支持修改费率
            if ($store->pay_ways_type) {
                return json_encode(['status' => 2, 'message' => '共享通道不支持修改费率']);
            }


            //代理商的费率
            $user_rate = UserRate::where('user_id', $store->user_id)
                ->where('ways_type', $ways_type)
                ->select('rate_a', 'rate_b')
                ->first();


            if (!$user_rate) {
                return json_encode(['status' => 2, 'message' => '代理商未设置费率', 'data' => []]);
            }

            //不能大于代理商的成本
            if ($rate_a < $user_rate->rate_a) {
                return json_encode(['status' => 2, 'message' => '小于1000贷记卡费率不能低于代理商的费率', 'data' => []]);
            }
            //不能大于代理商的成本
            if ($rate_b < $user_rate->rate_b) {
                return json_encode(['status' => 2, 'message' => '小于1000费率不能低于代理商的费率', 'data' => []]);
            }


            //不能大于代理商的成本
            if ($rate_c < $user_rate->rate_c) {
                return json_encode(['status' => 2, 'message' => '大于1000贷记卡费率不能低于代理商的费率', 'data' => []]);
            }
            //不能大于代理商的成本
            if ($rate_d < $user_rate->rate_d) {
                return json_encode(['status' => 2, 'message' => '大于1000借记卡费率不能低于代理商的费率', 'data' => []]);
            }

            $ways = StorePayWay::where('store_id', $store_id)->where('ways_type', $ways_type)
                ->first();

            if (!$ways) {
                return json_encode(['status' => 2, 'message' => '请先设置扫码通道', 'data' => []]);
            } else {
                $status = $ways->status;
                $status_desc = $ways->status_desc;

                //如果通道审核中不允许修改费率
                if ($status == 2) {
                    return json_encode(['status' => 2, 'message' => '通道审核中不允许修改费率', 'data' => []]);
                }

                //新大陆同步平台费率
                if ($user->level > 0 && $ways->status == 1 && 7999 < $ways_type && $ways_type < 8999) {
                    $OBJ = new  UpdateController();
                    //修改费率
                    $up_data = [
                        'store_id' => $store_id,
                        'email' => $store->people_phone . '@139.com',
                        'phone' => $store->people_phone,
                        'rate' => $ways->rate,
                        'rate_a' => $rate_a,
                        'rate_c' => $rate_c,
                        'rate_f' => $ways->rate_f,
                        'rate_f_top' => $ways->rate_f_top,
                        'rate_e' => $ways->rate_e,
                    ];

                    //$return = $OBJ->update_store_rate($up_data);
                    //  return $return;

                    return json_encode(['status' => 2, 'message' => '暂不支持软件修改', 'data' => $request->except(['token'])]);
                }


                //京东收银通道修改费率
                if ($user->level > 0 && $ways->status == 1 && 5999 < $ways_type && $ways_type < 6999) {
                    return json_encode(['status' => 2, 'message' => '京东通道开通后不支持软件修改', 'data' => $request->except(['token'])]);
                }
            }

            $company = $ways->company;

            //全部设置
            $update_data = [
                'rate_a' => $rate_a,
                'rate_b' => $rate_b,
                'rate_b_top' => $rate_b_top,
                'rate_c' => $rate_c,
                'rate_d' => $rate_d,
                'rate_d_top' => $rate_d_top,
            ];
            StorePayWay::where('store_id', $store_id)
                ->where('company', $company)
                ->update($update_data);


            return json_encode(['status' => 1, 'message' => '设置成功', 'data' => []]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }
    }


    //pc端开通通道
    public function open_ways_type(Request $request)
    {
        try {
            $token = $this->parseToken();
            $pay_status = $request->get('pay_status', '1');
            $pay_status_desc = $request->get('pay_status_desc', '开通成功');
            $store_id = $request->get('store_id');
            $ways_type = $request->get('ways_type', '');

            //支付宝
            $alipay_store_id = $request->get('alipay_store_id', $store_id);
            $out_store_id = $request->get('out_store_id', $store_id);

            if ($alipay_store_id == "") {
                $alipay_store_id = "";
            }

            if ($out_store_id == "") {
                $out_store_id = "";
            }

            $user = User::where('id', $token->user_id)->first();
//            $hasPermission = $user->hasPermissionTo('通道商户号');
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '没有权限配置商户号']);
//            }

            $weixin_path = $request->get('weixin_path', '');
            $weixin_path = isset($weixin_path) ? $weixin_path : "";

            //微信
            $wx_sub_merchant_id = $request->get('wx_sub_merchant_id', '');
            $wx_shop_id = $request->get('wx_shop_id', '');
            $wx_shop_id = isset($wx_shop_id) ? $wx_shop_id : "";
            $wxSharingOpen = $request->get('wxSharingOpen', ''); //是否开启分账
            $wxSharingType = $request->get('wxSharingType', ''); //分账接收方类型
            $wxSharingTccount = $request->get('wxSharingTccount', ''); //分账接收方帐号
            $wxSharingName = $request->get('wxSharingName', ''); //分账接收方全称
            $wxSharingRelationType = $request->get('wxSharingRelationType', ''); //与分账方的关系类型
            $wxSharingRate = $request->get('wxSharingRate', 0); //分账比例

            //京东
            $merchant_no = $request->get('merchant_no', '');
            $md_key = $request->get('md_key', '');
            $des_key = $request->get('des_key', '');

            //快钱
            $MerchantId = $request->get('MerchantId', '');

            //新大陆
            $nl_key = $request->get('nl_key', '');
            $nl_mercId = $request->get('nl_mercId', '');
            $trmNo = $request->get('trmNo', '');
            $wx_sub_appid = $request->post('wx_sub_appid', ''); //微信子公众号id

            //和融通
            $h_mid = $request->get('h_mid', '');

            //联拓富
            $appId = $request->get('appId', '');
            $ltf_md_key = $request->get('md_key', '');
            $merchantCode = $request->get('merchantCode', '');

            //哆啦宝
            $getdlbdata = $request->get('getdlbdata', '');
            $mch_num = $request->get('mch_num', '');
            $shop_num = $request->get('shop_num', '');
            $machine_num = $request->get('machine_num', '');

            //随行付
            $wx_channel_appid = $request->get('wx_channel_appid', ''); //微信渠道号
            $xcx_channel_appid = $request->get('xcx_channel_appid', ''); //小程序渠道号

            //汇付天下-商户号
            $user_cust_id = $request->get('mer_id', '');

            //工行
            $signkey = $request->get('signkey', '');

            //易生支付
            $easyPayOperaTrace = $request->get('easyPayOperaTrace', ''); //原操作流水号
            $easyPayMerCode = $request->get('easyPayMerCode', ''); //终端商户号
            $easyPayTermCode = $request->get('easyPayTermCode', ''); //终端终端号

            //邮驿付支付
            $custId = $request->get('cust_id', ''); //商户号


            //葫芦
            $qd = $request->get('qd', '');

            //长沙银行
            $DeptId = $request->get('DeptId', '');
            $StaffId = $request->get('StaffId', '');

            //邮政
            $pos_sn = $request->get('pos_sn', '');

            //联动优势
            $linkageAcqMerId = $request->get('linkageAcqMerId', ''); //商户号
            $linkageSubAppid = $request->get('linkageSubAppid', ''); //子商户号
            $linkageSubMchId = $request->get('linkageSubMchId', ''); //子商户号
            $linkageMerId = $request->get('linkageMerId', ''); //报备编号

            //威富通
            $wftpayMchId = $request->get('wftpayMchId', ''); //商户号
            $hwcpayMchId = $request->get('hwcpayMchId', ''); //汇旺财门店号
            $hwcpayMerchantNum = $request->get('hwcpayMerchantNum', ''); //汇旺财商户号

            //海科融通
            $hkrtMerchNo = $request->get('hkrtMerchNo', ''); //商户号
            $hkrtAgentApplyNo = $request->get('hkrtAgentApplyNo', ''); //服务商申请单号

            //银盛
            $yinshengPartnerId = $request->get('yinshengPartnerId', ''); //商户号
            $yinshengSellerName = $request->get('yinshengSellerName', ''); //银盛平台注册时公司名称
            $yinshengBusinessCode = $request->get('yinshengBusinessCode', ''); //业务代码

            //钱方
            $qfPayMchid = $request->get('qfPayMchid', ''); //商户号

            //建设银行
            $jh_cust_id = $request->get('jhCustId', ''); //商户号
            $pos_id = $request->get('posId', ''); //柜台号
            $branch_id = $request->get('branchId', ''); //分行号
            $termno1 = $request->get('termno1', ''); //终端编号1
            $termno2 = $request->get('termno2', ''); //终端编号2
            $public_key = $request->get('publicKey', ''); //商户柜台秘钥

            //易生数科
            $skMerId = $request->get('skMerId', ''); //商户号

            //通联支付
            $allinCusId = $request->get('allinCusId', ''); //商户号
            $allinTermno = $request->get('allinTermno', ''); //终端号
            $appid = $request->get('appid', ''); //appid  通联平台分配
            $private_key = $request->get('private_key', ''); //private_key  rsa私钥

            //富友
            $mchnt_cd = $request->get('mchnt_cd', ''); //商户号

            $Store = Store::where('store_id', $store_id)
                ->first();
            if (!$Store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在',
                ]);
            }

            $config_id = $Store->config_id;
            $store_name = $Store->store_name;
            $store_pid = $Store->pid;

            //费率 默认商户的费率为代理商的费率
            $rate = 0.38;

            //支付宝
            if ($ways_type == '1000') {
                $storeInfo = AlipayAppOauthUsers::where('store_id', $store_id)->first();
                if ($alipay_store_id == "" && $out_store_id) {
                    if ($storeInfo) {
                        return json_encode([
                            'status' => 1,
                            'data' => [
                                'alipay_store_id' => $storeInfo->alipay_store_id,
                                'out_store_id' => $storeInfo->out_store_id,
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    if ($storeInfo) {
                        $storeInfo->alipay_store_id = $alipay_store_id;
                        $storeInfo->out_store_id = $out_store_id;
                        $storeInfo->save();
                    } else {
                        //添加
                        $insert_data = [
                            'alipay_user_id' => '',
                            'alipay_store_id' => $alipay_store_id,
                            'out_store_id' => $out_store_id,
                            'merchant_id' => $Store->merchant_id,
                            'alipay_user_account' => '',
                            'alipay_user_account_name' => '',
                            'config_type' => '01',
                            'pid' => $Store->pid,
                            'store_id' => $store_id,
                            'is_delete' => 0,
                            'auth_app_id' => '',
                            'app_auth_token' => '',
                            'app_refresh_token' => '',
                            'expires_in' => '',
                            're_expires_in' => '',
                            'trade_pay_rate' => 0.6,
                        ];
                        AlipayAppOauthUsers::create($insert_data);
                    }
                }
            }

            //微信官方
            if ($ways_type == '2000') {
                $WeixinStoreObj = WeixinStore::where('store_id', $store_id)
//                    ->select('wx_sub_merchant_id', 'wx_shop_id')
                    ->first();
//                if (!$WeixinStoreObj) {
//                    //看下是否有修改权限
//                    $hasPermission = $user->hasPermissionTo('微信商户号添加');
//                    if (!$hasPermission) {
//                        return json_encode(['status' => 2, 'message' => '没有权限']);
//                    }
//                } else {
//                    //看下是否有添加权限
//                    $hasPermission = $user->hasPermissionTo('微信商户号修改');
//                    if (!$hasPermission) {
//                        return json_encode(['status' => 2, 'message' => '没有权限修改']);
//                    }
//                }

                if ($wx_sub_merchant_id == '') {
                    if ($WeixinStoreObj) {
                        return json_encode([
                            'status' => 1,
                            'data' => $WeixinStoreObj
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    if ($WeixinStoreObj) {
                        $wx_store_update = [
                            'config_id' => $config_id,
                            'store_id' => $store_id,
                            'status' => 1,
                            'status_desc' => '成功',
                            'wx_shop_id' => $wx_shop_id,
                            'wx_sub_merchant_id' => $wx_sub_merchant_id,
                        ];
                        if ($wxSharingOpen) {
                            $wx_store_update['is_profit_sharing'] = $wxSharingOpen;
                            if ($wxSharingType) {
                                $wx_store_update['wx_sharing_type'] = $wxSharingType;
                            }
                            if ($wxSharingTccount) {
                                $wx_store_update['wx_sharing_account'] = $wxSharingTccount;
                            }
                            if ($wxSharingName) {
                                $wx_store_update['wx_sharing_name'] = $wxSharingName;
                            }
                            if ($wxSharingRelationType) {
                                $wx_store_update['wx_sharing_relation_type'] = $wxSharingRelationType;
                            }
                            if ($wxSharingRate) {
                                $wx_store_update['wx_sharing_rate'] = $wxSharingRate;
                            }

                            //添加分账接收方失败，再添加一次
                            if ($WeixinStoreObj->wx_sharing_status == 0 && $wxSharingType) {
                                $config = new WeixinConfigController();
                                $options = $config->weixin_config($config_id);

                                $profit_sharing_add_receiver_data = [
                                    'store_id' => $store_id,
                                    'options' => $options,
                                    'wx_sub_merchant_id' => $wx_sub_merchant_id,
                                    'ways_type' => $ways_type
                                ];
                                $wx_pay_obj = new WeixinPayController();
                                $res = $wx_pay_obj->profit_sharing_add_receiver($profit_sharing_add_receiver_data);
                            }
                        }
                        $WeixinStoreObj->update($wx_store_update);
                    } else {
                        $gets2 = StorePayWay::where('store_id', $store_id)->where('ways_source', 'weixin')->get();
                        $ways = StorePayWay::where('store_id', $store_id)->where('ways_type', 2000)->first();
                        $count = count($gets2);

                        $rate = 0.6;
                        $store = Store::where('store_id', $store_id)
                            ->select('user_id')
                            ->first();
                        if (!$store) {
                            return json_encode([
                                'status' => 2,
                                'msg' => '门店不存在'
                            ]);
                        }

                        $UserRate = UserRate::where('user_id', $store->user_id)
                            ->where('ways_type', $ways_type)
                            ->first();
                        if ($UserRate) {
                            $rate = $UserRate->rate;
                        }
                        $data1 = [
                            'store_id' => $store_id,
                            'ways_type' => 2000,
                            'ways_source' => 'weixin',
                            'ways_desc' => '微信支付',
                            'sort' => ($count + 1),
                            'status' => 1,
                            'status_desc' => '成功',
                            'rate' => $rate,
                            'settlement_type' => 'T1',
                            'company' => 'weixin',
                        ];
                        if ($ways) {
                            $ways->update($data1);
                            $ways->save();
                        } else {
                            StorePayWay::create($data1);
                        }

                        $wx_store_create = [
                            'config_id' => $config_id,
                            'store_id' => $store_id,
                            'status' => 1,
                            'status_desc' => '成功',
                            'wx_sub_merchant_id' => $wx_sub_merchant_id,
                            'wx_shop_id' => $wx_shop_id,
                        ];
                        if ($wxSharingOpen) {
                            $wx_store_create['is_profit_sharing'] = $wxSharingOpen;
                            if ($wxSharingType) {
                                $wx_store_create['wx_sharing_type'] = $wxSharingType;
                            }
                            if ($wxSharingTccount) {
                                $wx_store_create['wx_sharing_account'] = $wxSharingTccount;
                            }
                            if ($wxSharingName) {
                                $wx_store_create['wx_sharing_name'] = $wxSharingName;
                            }
                            if ($wxSharingRelationType) {
                                $wx_store_create['wx_sharing_relation_type'] = $wxSharingRelationType;
                            }
                            if ($wxSharingRate) {
                                $wx_store_create['wx_sharing_rate'] = $wxSharingRate;
                            }
                        }
                        $result = WeiXinStore::create($wx_store_create);
                        if ($result) {
                            //添加分账接收方
                            $config = new WeixinConfigController();
                            $options = $config->weixin_config($config_id);

                            $profit_sharing_add_receiver_data = [
                                'store_id' => $store_id,
                                'options' => $options,
                                'wx_sub_merchant_id' => $wx_sub_merchant_id,
                                'ways_type' => $ways_type
                            ];
                            $wx_pay_obj = new WeixinPayController();
                            $res = $wx_pay_obj->profit_sharing_add_receiver($profit_sharing_add_receiver_data);
                        }
                    }
                }
            }

            //微信官方a
            if ($ways_type == '4000') {
                $WeixinStoreObj = WeixinaStore::where('store_id', $store_id)
//                    ->select('wx_sub_merchant_id', 'wx_shop_id')
                    ->first();
//                if (!$WeixinStoreObj) {
//                    //看下是否有修改权限
//                    $hasPermission = $user->hasPermissionTo('微信商户号添加');
//                    if (!$hasPermission) {
//                        return json_encode(['status' => 2, 'message' => '没有权限']);
//                    }
//                } else {
//                    //看下是否有添加权限
//                    $hasPermission = $user->hasPermissionTo('微信商户号修改');
//                    if (!$hasPermission) {
//                        return json_encode(['status' => 2, 'message' => '没有权限修改']);
//                    }
//                }

                if ($wx_sub_merchant_id == '') {
                    if ($WeixinStoreObj) {
                        return json_encode([
                            'status' => 1,
                            'data' => $WeixinStoreObj
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    if ($WeixinStoreObj) {
                        $wx_store_update = [
                            'config_id' => $config_id,
                            'store_id' => $store_id,
                            'status' => 1,
                            'status_desc' => '成功',
                            'wx_shop_id' => $wx_shop_id,
                            'wx_sub_merchant_id' => $wx_sub_merchant_id,
                        ];
                        if ($wxSharingOpen) {
                            $wx_store_update['is_profit_sharing'] = $wxSharingOpen;
                            if ($wxSharingType) {
                                $wx_store_update['wx_sharing_type'] = $wxSharingType;
                            }
                            if ($wxSharingTccount) {
                                $wx_store_update['wx_sharing_account'] = $wxSharingTccount;
                            }
                            if ($wxSharingName) {
                                $wx_store_update['wx_sharing_name'] = $wxSharingName;
                            }
                            if ($wxSharingRelationType) {
                                $wx_store_update['wx_sharing_relation_type'] = $wxSharingRelationType;
                            }
                            if ($wxSharingRate) {
                                $wx_store_update['wx_sharing_rate'] = $wxSharingRate;
                            }

                            //添加分账接收方失败，再添加一次
                            if ($WeixinStoreObj->wx_sharing_status == 0 && $wxSharingType) {
                                $config = new WeixinConfigController();
                                $options = $config->weixina_config($config_id);

                                $profit_sharing_add_receiver_data = [
                                    'store_id' => $store_id,
                                    'options' => $options,
                                    'wx_sub_merchant_id' => $wx_sub_merchant_id,
                                    'ways_type' => $ways_type
                                ];
                                $wx_pay_obj = new WeixinPayController();
                                $res = $wx_pay_obj->profit_sharing_add_receiver($profit_sharing_add_receiver_data);
                            }
                        }
                        $WeixinStoreObj->update($wx_store_update);
                    } else {
                        $gets2 = StorePayWay::where('store_id', $store_id)->where('ways_source', 'weixin')->get();
                        $ways = StorePayWay::where('store_id', $store_id)->where('ways_type', 4000)->first();
                        $count = count($gets2);

                        $rate = 0.6;
                        $store = Store::where('store_id', $store_id)
                            ->select('user_id')
                            ->first();
                        if (!$store) {
                            return json_encode([
                                'status' => 2,
                                'msg' => '门店不存在'
                            ]);
                        }

                        $UserRate = UserRate::where('user_id', $store->user_id)
                            ->where('ways_type', $ways_type)
                            ->first();
                        if ($UserRate) {
                            $rate = $UserRate->rate;
                        }
                        $data1 = [
                            'store_id' => $store_id,
                            'ways_type' => 4000,
                            'ways_source' => 'weixin',
                            'ways_desc' => '微信支付a',
                            'sort' => ($count + 1),
                            'status' => 1,
                            'status_desc' => '成功',
                            'rate' => $rate,
                            'settlement_type' => 'T1',
                            'company' => 'weixina',
                        ];
                        if ($ways) {
                            $ways->update($data1);
                        } else {
                            StorePayWay::create($data1);
                        }

                        $wx_store_create = [
                            'config_id' => $config_id,
                            'store_id' => $store_id,
                            'status' => 1,
                            'status_desc' => '成功',
                            'wx_sub_merchant_id' => $wx_sub_merchant_id,
                            'wx_shop_id' => $wx_shop_id,
                        ];
                        if ($wxSharingOpen) {
                            $wx_store_create['is_profit_sharing'] = $wxSharingOpen;
                            if ($wxSharingType) {
                                $wx_store_create['wx_sharing_type'] = $wxSharingType;
                            }
                            if ($wxSharingTccount) {
                                $wx_store_create['wx_sharing_account'] = $wxSharingTccount;
                            }
                            if ($wxSharingName) {
                                $wx_store_create['wx_sharing_name'] = $wxSharingName;
                            }
                            if ($wxSharingRelationType) {
                                $wx_store_create['wx_sharing_relation_type'] = $wxSharingRelationType;
                            }
                            if ($wxSharingRate) {
                                $wx_store_create['wx_sharing_rate'] = $wxSharingRate;
                            }
                        }
                        $result = WeiXinaStore::create($wx_store_create);
                        if ($result) {
                            //添加分账接收方
                            $config = new WeixinConfigController();
                            $options = $config->weixina_config($config_id);

                            $profit_sharing_add_receiver_data = [
                                'store_id' => $store_id,
                                'options' => $options,
                                'wx_sub_merchant_id' => $wx_sub_merchant_id,
                                'ways_type' => $ways_type
                            ];
                            $wx_pay_obj = new WeixinPayController();
                            $res = $wx_pay_obj->profit_sharing_add_receiver($profit_sharing_add_receiver_data);
                        }
                    }
                }
            }

            //富友
            if ($ways_type == "11001" || $ways_type == "11002" || $ways_type == "11003") {
                $FuiouStore = FuiouStore::where('store_id', $store_id)
                    ->first();
//                if (!$FuiouStore) {
//                    //看下是否有修改权限
//                    $hasPermission = $user->hasPermissionTo('富友商户号添加');
//                    if (!$hasPermission) {
//                        return json_encode(['status' => 2, 'message' => '没有权限']);
//                    }
//                } else {
//                    //看下是否有添加权限
//                    $hasPermission = $user->hasPermissionTo('富友商户号修改');
//                    if (!$hasPermission) {
//                        return json_encode(['status' => 2, 'message' => '没有权限修改']);
//                    }
//                }

                if ($mchnt_cd == '') {
                    if ($FuiouStore) {
                        return json_encode([
                            'status' => 1,
                            'data' => [
                                'mchnt_cd' => $FuiouStore->mchnt_cd
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    if ($FuiouStore) {
                        FuiouStore::where('store_id', $store_id)
                            ->update([
                                'store_id' => $store_id,
                                'config_id' => $config_id,
                                'mchnt_cd' => $mchnt_cd,
                            ]);
                    } else {
                        FuiouStore::create([
                            'store_id' => $store_id,
                            'config_id' => $config_id,
                            'mchnt_cd' => $mchnt_cd,
                        ]);
                    }
                }


                //费率 默认商户的费率为代理商的费率
                $UserRate = UserRate::where('user_id', $Store->user_id)
                    ->where('ways_type', 11001) //目前是一样的直接读取支付宝就行
                    ->first();
                if (!$UserRate) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请联系代理商开启此通道',
                    ]);
                }

                $rate = $UserRate->store_all_rate;
                //读取商户的费率
                $StorePayWay = StorePayWay::where('ways_type', $ways_type)
                    ->where('store_id', $store_id)
                    ->select('rate')
                    ->first();
                if ($StorePayWay) {
                    $rate = $StorePayWay->rate;
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户通道费率未设置',
                    ]);
                }

                //默认支付通道未注册成功
                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $pay_status;
                $data['status_desc'] = $pay_status_desc;
                $data['company'] = 'fuiou';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //随行付
            if ($ways_type == "13001" || $ways_type == "13002" || $ways_type == "13004") {
                $VbillStore = VbillStore::where('store_id', $store_id)->first();

//                if (!$VbillStore) {
//                    //看下是否有修改权限
//                    $hasPermission = $user->hasPermissionTo('随行付商户号添加');
//                    if (!$hasPermission) {
//                        return json_encode(['status' => 2, 'message' => '没有权限']);
//                    }
//                } else {
//                    //看下是否有添加权限
//                    $hasPermission = $user->hasPermissionTo('随行付商户号修改');
//                    if (!$hasPermission) {
//                        return json_encode(['status' => 2, 'message' => '没有权限修改']);
//                    }
//                }
                if ($MerchantId == '') {
                    if ($VbillStore) {
                        return json_encode([
                            'status' => 1,
                            'data' => [
                                'MerchantId' => $VbillStore->mno,
                                'wx_channel_appid' => $VbillStore->wx_channel_appid,
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    $config = new VbillConfigController();
                    $vbill_config = $config->vbill_config($config_id);
                    if (!$vbill_config) {
                        return json_encode([
                            'status' => 2,
                            'message' => '随行付配置不存在请检查配置'
                        ]);
                    }

                    //查询状态
                    if ($VbillStore && $VbillStore->applicationId) {
                        $obj = new \App\Api\Controllers\Vbill\BaseController();
                        $url = $obj->open_query_store_url;
                        $data = [
                            'orgId' => $vbill_config->orgId,
                            'reqId' => time(),
                            'version' => '1.0',
                            'timestamp' => date('Ymdhis', time()),
                            'signType' => 'RSA',
                            'reqData' => [
                                'applicationId' => $VbillStore->applicationId,
                            ],
                        ];

                        $open_query_store = $obj->execute($data, $url, $vbill_config->privateKey, $vbill_config->sxfpublic);
                        if ($open_query_store['data']['code'] == "0000" && $open_query_store['data']['respData']['bizCode'] == "0000") {
                            if (isset($open_query_store['data']['respData']['taskStatus'])) {
                                //审核失败
                                if ($open_query_store['data']['respData']['taskStatus'] == "2" || $open_query_store['data']['respData']['taskStatus'] == "3") {
                                    $data_up = [
                                        'status' => 3,
                                        'status_desc' => $open_query_store['data']['respData']['suggestion'],
                                    ];
                                    StorePayWay::where('store_id', $store_id)
                                        ->where('company', 'vbill')
                                        ->update($data_up);

                                    $in_data = [
                                        'taskStatus' => $open_query_store['data']['respData']['taskStatus'],
                                        'wx_channel_appid' => ($xcx_channel_appid) ? $xcx_channel_appid : $wx_channel_appid
                                    ];
                                    VbillStore::where('store_id', $store_id)->update($in_data);

                                    return json_encode([
                                        'status' => 2,
                                        'message' => $open_query_store['data']['respData']['suggestion'],
                                    ]);
                                }
                            }
                        }
                    }

                    //商户信息查询接口
                    $obj = new \App\Api\Controllers\Vbill\BaseController();
                    $url = $obj->store_query_info;
                    $data = [
                        'orgId' => $vbill_config->orgId,
                        'reqId' => time(),
                        'version' => '1.0',
                        'timestamp' => date('Ymdhis', time()),
                        'signType' => 'RSA',
                        'reqData' => [
                            'mno' => $MerchantId,
                        ],
                    ];

                    $re = $obj->execute($data, $url, $vbill_config->privateKey, $vbill_config->sxfpublic);
                    if ($re['data']['code'] == "0000" && $re['data']['respData']['bizCode'] != "0000") {
                        return json_encode([
                            'status' => 2,
                            'message' => $re['data']['respData']['bizMsg']
                        ]);
                    }

                    //读取商户的费率
                    $StorePayWay = StorePayWay::where('ways_type', $ways_type)
                        ->where('store_id', $store_id)
                        ->select('rate')
                        ->first();
                    if ($StorePayWay) {
                        $rate = $StorePayWay->rate;
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '商户通道费率未设置',
                        ]);
                    }

                    //门店不一致无法保存
//                    if ($re['data']['respData']['mecDisNm'] != $store_name) {
//                        return json_encode([
//                            'status' => 2,
//                            'message' => '门店名称不一致、无法保存'
//                        ]);
//                    }

                    //费率不一致无法保存
                    if ($re['data']['respData']['qrcodeList'][0]['rate'] != $rate) {
                        return json_encode([
                            'status' => 2,
                            'message' => '费率设置不一致、无法保存-' . $re['data']['respData']['qrcodeList'][0]['rate']
                        ]);
                    }

                    $childNo = "";
                    foreach ($re['data']['respData']['repoInfo'] as $k => $v) {
                        if ($v['childNoType'] == "WX") {
                            $childNo = $v['childNo'];
                        } else {
                            continue;
                        }
                    }

                    //报备微信授权目录
                    $obj = new \App\Api\Controllers\Vbill\BaseController();
                    $url = $obj->weixin_config;
                    $data = [
                        'orgId' => $vbill_config->orgId,
                        'reqId' => time(),
                        'version' => '1.0',
                        'timestamp' => date('Ymdhis', time()),
                        'signType' => 'RSA',
                        'reqData' => [
                            'mno' => $MerchantId,
                            'subMchId' => $childNo,
                            'type' => '03',
                            'jsapiPath' => $weixin_path ? $weixin_path : env('APP_URL') . '/api/vbill/weixin/'
                        ]
                    ];
                    $abc = $obj->execute($data, $url, $vbill_config->privateKey, $vbill_config->sxfpublic);
//                    Log::info('随行付-强开-报备微信授权目录结果');
//                    Log::info($abc);

                    $insert_data = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'mno' => $MerchantId,
                        'wx_channel_appid' => ($xcx_channel_appid) ? $xcx_channel_appid : $wx_channel_appid
                    ];

                    if ($VbillStore) {
                        if ($VbillStore->applicationId == "") {
                            $insert_data['taskStatus'] = 1;
                        }
                    } else {
                        $insert_data['taskStatus'] = 1;
                    }

                    if ($VbillStore) {
                        VbillStore::where('store_id', $store_id)
                            ->update($insert_data);
                    } else {
                        VbillStore::create($insert_data);
                    }
                }


                //费率 默认商户的费率为代理商的费率
                $UserRate = UserRate::where('user_id', $Store->user_id)
                    ->where('ways_type', 13001)//目前是一样的直接读取支付宝就行
                    ->first();
                if (!$UserRate) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请联系代理商开启此通道',
                    ]);
                }

                //默认支付通道未注册成功
                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $pay_status;
                $data['status_desc'] = $pay_status_desc;
                $data['company'] = 'vbill';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //随行付A
            if ($ways_type == "19001" || $ways_type == "19002" || $ways_type == "19004") {
                $VbillStore = VbillaStore::where('store_id', $store_id)->first();
//                if (!$VbillStore) {
//                    //看下是否有修改权限
//                    $hasPermission = $user->hasPermissionTo('随行付A商户号添加');
//
//                    if (!$hasPermission) {
//                        return json_encode(['status' => 2, 'message' => '没有权限']);
//                    }
//                } else {
//                    //看下是否有添加权限
//                    $hasPermission = $user->hasPermissionTo('随行付A商户号修改');
//                    if (!$hasPermission) {
//                        return json_encode(['status' => 2, 'message' => '没有权限修改']);
//                    }
//                }
                if ($MerchantId == '') {
                    if ($VbillStore) {
                        return json_encode([
                            'status' => 1,
                            'data' => [
                                'MerchantId' => $VbillStore->mno,
                                'wx_channel_appid' => $VbillStore->wx_channel_appid,
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    $config = new VbillConfigController();
                    $vbill_config = $config->vbilla_config($config_id);
                    if (!$vbill_config) {
                        return json_encode([
                            'status' => 2,
                            'message' => '随行付A配置不存在请检查配置'
                        ]);
                    }

                    //查询状态
                    if ($VbillStore && $VbillStore->applicationId) {
                        $obj = new \App\Api\Controllers\Vbill\BaseController();
                        $url = $obj->open_query_store_url;
                        $data = [
                            'orgId' => $vbill_config->orgId,
                            'reqId' => time(),
                            'version' => '1.0',
                            'timestamp' => date('Ymdhis', time()),
                            'signType' => 'RSA',
                            'reqData' => [
                                'applicationId' => $VbillStore->applicationId,
                            ],
                        ];

                        $open_query_store = $obj->execute($data, $url, $vbill_config->privateKey, $vbill_config->sxfpublic);
                        if ($open_query_store['data']['code'] == "0000" && $open_query_store['data']['respData']['bizCode'] == "0000") {
                            if (isset($open_query_store['data']['respData']['taskStatus'])) {
                                //审核失败
                                if ($open_query_store['data']['respData']['taskStatus'] == "2" || $open_query_store['data']['respData']['taskStatus'] == "3") {
                                    $data_up = [
                                        'status' => 3,
                                        'status_desc' => $open_query_store['data']['respData']['suggestion'],
                                    ];
                                    StorePayWay::where('store_id', $store_id)
                                        ->where('company', 'vbilla')
                                        ->update($data_up);

                                    $in_data = [
                                        'taskStatus' => $open_query_store['data']['respData']['taskStatus'],
                                        'wx_channel_appid' => ($xcx_channel_appid) ? $xcx_channel_appid : $wx_channel_appid
                                    ];
                                    VbillaStore::where('store_id', $store_id)->update($in_data);

                                    return json_encode([
                                        'status' => 2,
                                        'message' => $open_query_store['data']['respData']['suggestion'],
                                    ]);
                                }
                            }
                        }
                    }

                    //商户信息查询接口
                    $obj = new \App\Api\Controllers\Vbill\BaseController();
                    $url = $obj->store_query_info;
                    $data = [
                        'orgId' => $vbill_config->orgId,
                        'reqId' => time(),
                        'version' => '1.0',
                        'timestamp' => date('Ymdhis', time()),
                        'signType' => 'RSA',
                        'reqData' => [
                            'mno' => $MerchantId,
                        ],
                    ];

                    $re = $obj->execute($data, $url, $vbill_config->privateKey, $vbill_config->sxfpublic);
                    if ($re['data']['code'] == "0000" && $re['data']['respData']['bizCode'] != "0000") {
                        return json_encode([
                            'status' => 2,
                            'message' => $re['data']['respData']['bizMsg']
                        ]);
                    }


                    //读取商户的费率
                    $StorePayWay = StorePayWay::where('ways_type', $ways_type)
                        ->where('store_id', $store_id)
                        ->select('rate')
                        ->first();
                    if ($StorePayWay) {
                        $rate = $StorePayWay->rate;
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '商户通道费率未设置',
                        ]);
                    }


                    //门店不一致无法保存
//                    if ($re['data']['respData']['mecDisNm'] != $store_name) {
//                        return json_encode([
//                            'status' => 2,
//                            'message' => '门店名称不一致、无法保存'
//                        ]);
//                    }

                    //费率不一致无法保存
                    if ($re['data']['respData']['qrcodeList'][0]['rate'] != $rate) {
                        return json_encode([
                            'status' => 2,
                            'message' => '费率设置不一致、无法保存-' . $re['data']['respData']['qrcodeList'][0]['rate']
                        ]);
                    }

                    $childNo = "";
                    foreach ($re['data']['respData']['repoInfo'] as $k => $v) {
                        if ($v['childNoType'] == "WX") {
                            $childNo = $v['childNo'];
                        } else {
                            continue;
                        }
                    }

                    //报备微信授权目录
                    $obj = new \App\Api\Controllers\Vbill\BaseController();
                    $url = $obj->weixin_config;
                    $data = [
                        'orgId' => $vbill_config->orgId,
                        'reqId' => time(),
                        'version' => '1.0',
                        'timestamp' => date('Ymdhis', time()),
                        'signType' => 'RSA',
                        'reqData' => [
                            'mno' => $MerchantId,
                            'subMchId' => $childNo,
                            'type' => '03',
                            'jsapiPath' => env('APP_URL') . '/api/vbill/weixin/'
                        ],
                    ];
                    $abc = $obj->execute($data, $url, $vbill_config->privateKey, $vbill_config->sxfpublic);
//                    Log::info('随行付A-强开-报备微信授权目录结果');
//                    Log::info($abc);

                    //随行付A--提交实名认证申请接口
//                    $wx_submit_url = $obj->wechat_commit_apply;
//                    $wx_submit_data = [
//                        'orgId' => $vbill_config->orgId,
//                        'reqId' => time(),
//                        'version' => '1.0',
//                        'timestamp' => date('Ymdhis', time()),
//                        'signType' => 'RSA',
//                        'reqData' => [
//                            'mno' => $MerchantId
//                        ],
//                    ];
//                    $wx_submit_res = $obj->execute($wx_submit_data, $wx_submit_url, $vbill_config->privateKey, $vbill_config->sxfpublic);
//                    Log::info('随行付A-强开-提交实名认证申请结果');
//                    Log::info($wx_submit_res);

                    $insert_data = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'mno' => $MerchantId,
                        'wx_channel_appid' => ($xcx_channel_appid) ? $xcx_channel_appid : $wx_channel_appid
                    ];

                    if ($VbillStore) {
                        if ($VbillStore->applicationId == "") {
                            $insert_data['taskStatus'] = 1;
                        }
                    } else {
                        $insert_data['taskStatus'] = 1;
                    }

                    if ($VbillStore) {
                        VbillaStore::where('store_id', $store_id)
                            ->update($insert_data);
                    } else {
                        VbillaStore::create($insert_data);
                    }
                }

                //费率 默认商户的费率为代理商的费率
                $UserRate = UserRate::where('user_id', $Store->user_id)
                    ->where('ways_type', $ways_type) //目前是一样的直接读取支付宝就行
                    ->first();
                if (!$UserRate) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请联系代理商开启此通道',
                    ]);
                }

                //默认支付通道未注册成功
                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $pay_status;
                $data['status_desc'] = $pay_status_desc;
                $data['company'] = 'vbilla';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //京东
            if (5999 < $ways_type && $ways_type < 6999) {
                $JdStore = JdStore::where('store_id', $store_id)
                    ->first();
//                if (!$JdStore) {
//                    //看下是否有修改权限
//                    $hasPermission = $user->hasPermissionTo('京东商户号添加');
//                    if (!$hasPermission) {
//                        return json_encode(['status' => 2, 'message' => '没有权限']);
//                    }
//                } else {
//                    //看下是否有添加权限
//                    $hasPermission = $user->hasPermissionTo('京东商户号修改');
//                    if (!$hasPermission) {
//                        return json_encode(['status' => 2, 'message' => '没有权限修改']);
//                    }
//                }
                if ($merchant_no == '') {
                    if ($JdStore) {
                        return json_encode([
                            'status' => 1,
                            'data' => $JdStore
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    $config = new JdConfigController();
                    $jd_config = $config->jd_config($config_id);
                    //更新密钥
                    $store_keys = [
                        'request_url' => 'https://psi.jd.com/merchant/status/queryMerchantKeys',
                        'agentNo' => $jd_config->agentNo,
                        'merchantNo' => $merchant_no,
                        'serialNo' => "" . time() . "",
                        'store_md_key' => $jd_config->store_md_key,
                        'store_des_key' => $jd_config->store_des_key,
                    ];

                    $OBJ = new \App\Api\Controllers\Jd\StoreController();
                    $re = $OBJ->store_keys($store_keys);
                    if ($re['code'] == "0000") {
                        $des_key = $re['data']['desKey'];
                        $md_key = $re['data']['mdKey'];
                    }

                    if ($JdStore) {
                        JdStore::where('store_id', $store_id)
                            ->update([
                                'config_id' => $config_id,
                                'store_id' => $store_id,
                                'merchant_no' => $merchant_no,
                                'md_key' => $md_key,
                                'des_key' => $des_key,
                            ]);
                    } else {
                        JdStore::create([
                            'config_id' => $config_id,
                            'store_id' => $store_id,
                            'merchant_no' => $merchant_no,
                            'md_key' => $md_key,
                            'des_key' => $des_key,
                            'store_true' => 1,
                            'pay_true' => 1
                        ]);
                    }
                }

                //费率 默认商户的费率为代理商的费率
                $UserRate = UserRate::where('user_id', $Store->user_id)
                    ->where('ways_type', 6001)//目前是一样的直接读取支付宝就行
                    ->first();
                if (!$UserRate) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请联系代理商开启此通道',
                    ]);
                }

                $rate = $UserRate->store_all_rate;

                //读取商户的费率
                $StorePayWay = StorePayWay::where('ways_type', $ways_type)
                    ->where('store_id', $store_id)
                    ->select('rate')
                    ->first();
                if ($StorePayWay) {
                    $rate = $StorePayWay->rate;
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户通道费率未设置',
                    ]);
                }

                //默认支付通道未注册成功
                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $pay_status;
                $data['status_desc'] = $pay_status_desc;
                $data['company'] = 'jdjr';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //联拓富
            if (9999 < $ways_type && $ways_type < 10999) {
                //费率 默认商户的费率为代理商的费率
                $UserRate = UserRate::where('user_id', $Store->user_id)
                    ->where('ways_type', 10001)//目前是一样的直接读取支付宝就行
                    ->first();
                if (!$UserRate) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请联系代理商开启此通道',
                    ]);
                }

                $rate = $UserRate->store_all_rate;

                $LtfStore = LtfStore::where('store_id', $store_id)
                    ->first();
//                if (!$LtfStore) {
//                    //看下是否有修改权限
//                    $hasPermission = $user->hasPermissionTo('联拓富商户号添加');
//
//                    if (!$hasPermission) {
//                        return json_encode(['status' => 2, 'message' => '没有权限']);
//                    }
//                } else {
//                    //看下是否有添加权限
//                    $hasPermission = $user->hasPermissionTo('联拓富商户号修改');
//                    if (!$hasPermission) {
//                        return json_encode(['status' => 2, 'message' => '没有权限修改']);
//                    }
//                }

                if ($merchantCode == '') {
                    if ($LtfStore) {
                        return json_encode([
                            'status' => 1,
                            'data' => $LtfStore
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    if ($LtfStore) {
                        LtfStore::where('store_id', $store_id)
                            ->update([
                                'config_id' => $config_id,
                                'store_id' => $store_id,
                                'appId' => $appId,
                                'md_key' => $ltf_md_key,
                                'merchantCode' => $merchantCode
                            ]);
                    } else {
                        LtfStore::create([
                            'config_id' => $config_id,
                            'store_id' => $store_id,
                            'appId' => $appId,
                            'md_key' => $ltf_md_key,
                            'merchantCode' => $merchantCode
                        ]);
                    }
                }

                //读取商户的费率
                $StorePayWay = StorePayWay::where('ways_type', $ways_type)
                    ->where('store_id', $store_id)
                    ->select('rate')
                    ->first();
                if ($StorePayWay) {
                    $rate = $StorePayWay->rate;
                }

                //默认支付通道未注册成功
                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $pay_status;
                $data['status_desc'] = $pay_status_desc;
                $data['company'] = 'ltf';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //快钱支付
            if (2999 < $ways_type && $ways_type < 3999) {
                $MyBankStore = MyBankStore::where('OutMerchantId', $store_id)->select('MerchantId')->first();
                if (!$MyBankStore) {
                    //看下是否有修改权限
                    $hasPermission = $user->hasPermissionTo('快钱商户号添加');

                    if (!$hasPermission) {
                        return json_encode(['status' => 2, 'message' => '没有权限']);
                    }
                } else {
                    //看下是否有添加权限
                    $hasPermission = $user->hasPermissionTo('快钱商户号修改');
                    if (!$hasPermission) {
                        return json_encode(['status' => 2, 'message' => '没有权限修改']);
                    }
                }

                if ($MerchantId == '') {
                    if ($MyBankStore) {
                        return json_encode([
                            'status' => 1,
                            'data' => $MyBankStore
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                }

                //费率 默认商户的费率为代理商的费率
                $UserRate = UserRate::where('user_id', $Store->user_id)
                    ->where('company', 'mybank')//目前是一样的直接读取支付宝就行
                    ->first();
                if (!$UserRate) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请联系代理商开启此通道',
                    ]);
                }

                $rate = $UserRate->store_all_rate;

                //读取商户的费率
                $StorePayWay = StorePayWay::where('ways_type', $ways_type)
                    ->where('store_id', $store_id)
                    ->select('rate')
                    ->first();
                if ($StorePayWay) {
                    $rate = $StorePayWay->rate;
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户通道费率未设置',
                    ]);
                }

                //查询商户资料
                //商户信息查询接口
                $aop = new \App\Api\Controllers\MyBank\BaseController();
                $ao = $aop->aop();
                $ao->url = $aop->MY_BANK_request2;
                $ao->Function = "ant.mybank.merchantprod.merchant.query";

                $data = [
                    'MerchantId' => $MerchantId,
                ];
                $re = $ao->Request($data);
                if ($re['status'] == 0) {
                    return json_encode([
                        'status' => 2,
                        'message' => $re['message'],
                    ]);
                }
                $body = $re['data']['document']['response']['body'];

                //这个地方代表需要排查
                if ($body['RespInfo']['ResultStatus'] != 'S') {
                    return [
                        'status' => 2,
                        'message' => $body['RespInfo']['ResultMsg']
                    ];
                }
                $FeeParamList = base64_decode($re['data']['document']['response']['body']['FeeParamList']);

                $FeeParamList = json_decode($FeeParamList, true);
                $FeeValue = "";
                foreach ($FeeParamList as $k => $v) {
                    if ($v['FeeType'] == "02") {
                        $FeeValue = $v['FeeValue'];
                        break;
                    } else {
                        continue;
                    }
                }
                if ($rate / 100 != $FeeValue) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请将通道费率设置为' . $FeeValue * 100,
                    ]);
                }

                $in = [
                    'status' => '1',
                    'OrderNo' => '',
                    'config_id' => $config_id,
                    'smid' => "",
                    'RateVersion' => '',
                    'OutMerchantId' => $store_id,
                    'MerchantId' => $MerchantId,
                    'MerchantName' => $store_name,
                    'MerchantType' => '',
                    'DealType' => '',
                    'SupportPrepayment' => '',
                    'SettleMode' => '',
                    'Mcc' => '',
                    'MerchantDetail' => '',
                    'TradeTypeList' => '',
                    'PrincipalCertType' => '',
                    'PayChannelList' => '',
                    'DeniedPayToolList' => '',
                    'FeeParamList' => '',
                    'BankCardParam' => '',
                    'SupportStage' => '',
                    'PartnerType' => '',
                    'wx_Path' => url('/api/mybank/weixin/'),
                    'wx_AppId' => '',
                    'wx_Secret' => '',
                    'wx_SubscribeAppId' => '',
                    'is_yulibao' => '',
                ];

                if ($MyBankStore) {
                    MyBankStore::where('OutMerchantId', $store_id)
                        ->update($in);
                } else {
                    MyBankStore::create($in);
                }

                //微信子商户支付配置接口
                try {
                    $ao->url = $aop->MY_BANK_request2;
                    $ao->Function = "ant.mybank.merchantprod.merchant.addMerchantConfig";
                    $data = [
                        'MerchantId' => $MerchantId,
                        'Path' => $weixin_path ? $weixin_path : env('APP_URL') . '/api/mybank/weixin/',
                        'OutTradeNo' => date('YmdHis') . time() . rand(10000, 99999),
                    ];
                    $re = $ao->Request($data);
                } catch (\Exception $exception) {
                }

                //默认支付通道未注册成功
                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $pay_status;
                $data['status_desc'] = $pay_status_desc;
                $data['company'] = 'mybank';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //新大陆
            if (7999 < $ways_type && $ways_type < 8999) {
                $NewLandStore = NewLandStore::where('store_id', $store_id)
                    //->where('nl_mercId', $nl_mercId)
                    ->first();
                if (!$NewLandStore) {
                    //看下是否有修改权限
                    $hasPermission = $user->hasPermissionTo('新大陆商户号添加');

                    if (!$hasPermission) {
                        return json_encode(['status' => 2, 'message' => '没有权限']);
                    }
                } else {
                    //看下是否有添加权限
                    $hasPermission = $user->hasPermissionTo('新大陆商户号修改');
                    if (!$hasPermission) {
                        return json_encode(['status' => 2, 'message' => '没有权限修改']);
                    }
                }

                if ($nl_mercId == '') {
                    if ($NewLandStore) {
                        return json_encode([
                            'status' => 1,
                            'data' => $NewLandStore
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    //修改先查询信息
                    $config_obj = new NewLandConfigController();
                    $config = $config_obj->new_land_config($config_id);
                    $aop = new \App\Common\XingPOS\Aop();
                    $aop->key = $config->nl_key;
                    $aop->version = 'V1.0.1';
                    $aop->org_no = $config->org_no;
                    $aop->url = 'https://gateway.starpos.com.cn/emercapp';//测试地址

                    $sign_data = [
                        'mercId' => $nl_mercId,
                    ];
                    $is_qd = $config->is_qd;
                    $request_obj = new  \App\Common\XingPOS\Request\XingStoreShangHuChaXun();
                    $request_obj->setBizContent($sign_data);
                    $return = $aop->executeStore($request_obj);
                    //不成功
                    if ($return['msg_cd'] != '000000') {
                        return json_encode([
                            'status' => 2,
                            'message' => $return['msg_dat'],
                        ]);
                    }

                    //直接通过
                    if ($return['check_flag'] == 1) {
                        $pay_status = 1;
                        $pay_status_desc = '开通成功';
                        $nl_key = $return['key'];
                        // $trmNo = $return['REC'][0]['trmNo'];
                        $nl_stoe_id = $return['REC'][0]['stoe_id'];
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '商户未开通成功',
                        ]);
                    }
                    $in_data = [
                        'config_id' => $config_id,
                        'store_id' => $store_id,
                        'nl_key' => $nl_key,
                        'trmNo' => $trmNo,
                        'nl_mercId' => $nl_mercId,
                        'nl_stoe_id' => $nl_stoe_id,
                        'jj_status' => 1,
                        'img_status' => 1,
                        'tj_status' => 1,
                        'check_flag' => 1,
                        'check_qm' => 1,
                        'is_qd' => $is_qd,
                    ];
                    if ($NewLandStore) {
                        NewLandStore::where('store_id', $store_id)
                            ->where('nl_mercId', $nl_mercId)
                            ->update($in_data);
                    } else {
                        NewLandStore::create($in_data);
                    }
                }

                if ($wx_sub_appid == '') {
                    if ($NewLandStore) {
                        return json_encode([
                            'status' => '1',
                            'data' => $NewLandStore
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'data' => []
                        ]);
                    }
                } else {
                    $NewLandStore->update([
                        'wx_sub_appid' => $wx_sub_appid
                    ]);
                    $NewLandStore->save();
                }

                //费率 默认商户的费率为代理商的费率
                $UserRate = UserRate::where('user_id', $Store->user_id)
                    ->where('ways_type', 8001)//目前是一样的直接读取支付宝就行
                    ->first();
                if (!$UserRate) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请联系代理商开启此通道',
                    ]);
                }

                $rate = $UserRate->store_all_rate;
                //读取商户的费率
                $StorePayWay = StorePayWay::where('ways_type', $ways_type)
                    ->where('store_id', $store_id)
                    ->select('rate')
                    ->first();
                if ($StorePayWay) {
                    $rate = $StorePayWay->rate;
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户通道费率未设置',
                    ]);
                }

                //默认支付通道未注册成功
                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $pay_status;
                $data['status_desc'] = $pay_status_desc;
                $data['company'] = 'newland';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //和融通
            if (8999 < $ways_type && $ways_type < 9999) {
                $JdStore = HStore::where('store_id', $store_id)
                    ->first();
                if (!$JdStore) {
                    //看下是否有修改权限
                    $hasPermission = $user->hasPermissionTo('和融通商户号添加');
                    if (!$hasPermission) {
                        return json_encode(['status' => 2, 'message' => '没有权限']);
                    }
                } else {
                    //看下是否有添加权限
                    $hasPermission = $user->hasPermissionTo('和融通商户号修改');
                    if (!$hasPermission) {
                        return json_encode(['status' => 2, 'message' => '没有权限修改']);
                    }
                }

                if ($h_mid == '') {
                    if ($JdStore) {
                        return json_encode([
                            'status' => 1,
                            'data' => $JdStore
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    if ($JdStore) {
                        HStore::where('store_id', $store_id)
                            ->update([
                                'config_id' => $config_id,
                                'store_id' => $store_id,
                                'h_mid' => $h_mid,
                                'h_status' => $pay_status,
                                'h_status_desc' => $pay_status_desc,
                            ]);
                    } else {
                        HStore::create([
                            'config_id' => $config_id,
                            'store_id' => $store_id,
                            'h_mid' => $h_mid,
                            'h_status' => $pay_status,
                            'h_status_desc' => $pay_status_desc,
                        ]);
                    }
                }

                //费率 默认商户的费率为代理商的费率
                $UserRate = UserRate::where('user_id', $Store->user_id)
                    ->where('ways_type', 9001) //目前是一样的直接读取支付宝就行
                    ->first();
                if (!$UserRate) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请联系代理商开启此通道',
                    ]);
                }

                $rate = $UserRate->store_all_rate;

                //读取商户的费率
                $StorePayWay = StorePayWay::where('ways_type', $ways_type)
                    ->where('store_id', $store_id)
                    ->select('rate')
                    ->first();
                if ($StorePayWay) {
                    $rate = $StorePayWay->rate;
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户通道费率未设置',
                    ]);
                }

                //默认支付通道未注册成功
                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $pay_status;
                $data['status_desc'] = $pay_status_desc;
                $data['company'] = 'herongtong';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //哆啦宝
            if (14999 < $ways_type && $ways_type < 15999) {
                $DlbStore = DlbStore::where('store_id', $store_id)->first();
                if (!$DlbStore) {
                    //看下是否有修改权限
                    $hasPermission = $user->hasPermissionTo('哆啦宝商户号添加');
                    if (!$hasPermission) {
                        return json_encode(['status' => 2, 'message' => '没有权限']);
                    }
                } else {
                    //看下是否有添加权限
                    $hasPermission = $user->hasPermissionTo('哆啦宝商户号修改');
                    if (!$hasPermission) {
                        return json_encode(['status' => 2, 'message' => '没有权限修改']);
                    }
                }

                if ($getdlbdata == 1) {
                    if ($DlbStore) {
                        return json_encode([
                            'status' => 1,
                            'data' => [
                                'mch_num' => $DlbStore->mch_num,
                                'shop_num' => $DlbStore->shop_num,
                                'machine_num' => $DlbStore->machine_num
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'message' => '没有配置',
                            'data' => []
                        ]);
                    }
                } else {
                    $check_data = [
                        'mch_num' => '商户编号',
                        'shop_num' => '门店编号'
                    ];
                    $check = $this->check_required($request->except(['token']), $check_data);
                    if ($check) {
                        return json_encode([
                            'status' => 2,
                            'message' => $check
                        ]);
                    }
                    if ($DlbStore) {
                        DlbStore::where('store_id', $store_id)
                            ->update([
                                'store_id' => $store_id,
                                'config_id' => $config_id,
                                'mch_num' => $mch_num,
                                'shop_num' => $shop_num,
                                'machine_num' => $machine_num
                            ]);
                    } else {
                        DlbStore::create([
                            'store_id' => $store_id,
                            'config_id' => $config_id,
                            'mch_num' => $mch_num,
                            'shop_num' => $shop_num,
                            'machine_num' => $machine_num
                        ]);
                    }
                }

                //费率 默认商户的费率为代理商的费率
                $UserRate = UserRate::where('user_id', $Store->user_id)
                    ->where('ways_type', 15001) //目前是一样的直接读取支付宝就行
                    ->first();
                if (!$UserRate) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请联系代理商开启此通道',
                    ]);
                }

                $rate = $UserRate->store_all_rate;

                //读取商户的费率
                $StorePayWay = StorePayWay::where('ways_type', $ways_type)
                    ->where('store_id', $store_id)
                    ->select('rate')
                    ->first();
                if ($StorePayWay) {
                    $rate = $StorePayWay->rate;
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户通道费率未设置',
                    ]);
                }

                //默认支付通道未注册成功
                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $pay_status;
                $data['status_desc'] = $pay_status_desc;
                $data['company'] = 'dlb';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //传化
            if (11999 < $ways_type && $ways_type < 12999) {
                $TfStore = TfStore::where('store_id', $store_id)->select('sub_mch_id', 'qd')->first();
                if (!$TfStore) {

                    //看下是否有修改权限
                    $hasPermission = $user->hasPermissionTo('TF商户号添加');
                    if (!$hasPermission) {
                        return json_encode(['status' => 2, 'message' => '没有权限']);
                    }
                } else {
                    //看下是否有添加权限
                    $hasPermission = $user->hasPermissionTo('TF商户号修改');
                    if (!$hasPermission) {
                        return json_encode(['status' => 2, 'message' => '没有权限修改']);
                    }
                }


                if ($MerchantId == '') {
                    if ($TfStore) {
                        $config = new TfConfigController();
                        $tf_config = $config->tf_config($config_id, $TfStore->qd);

                        if (!$tf_config) {
                            return json_encode([
                                'status' => 2,
                                'message' => '传化配置不存在请检查配置'
                            ]);
                        }

                        $obj = new \App\Api\Controllers\Tfpay\BaseController();
                        $obj->mch_id = $tf_config->mch_id;
                        $obj->pub_key = $tf_config->pub_key;
                        $obj->pri_key = $tf_config->pri_key;
                        $method = '/openapi/merchant/open-query';

                        $channel = '0';
                        if ($ways_type == "12002") {
                            $channel = '1';
                        };
                        //传化支付宝报备
                        $post_data = [
                            'sub_mch_id' => $TfStore->sub_mch_id,
                            'channel' => $channel,
                        ];

                        $re1 = $obj->api($post_data, $method, false);
                        $channel_mch_id = isset($re1['data']['channel_mch_id']) ? $re1['data']['channel_mch_id'] : "";

                        return json_encode([
                            'status' => 1,
                            'data' => [
                                'MerchantId' => $TfStore->sub_mch_id,
                                'qd' => $TfStore->qd,
                                'channel_mch_id' => $channel_mch_id,
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                }

                //费率 默认商户的费率为代理商的费率
                $UserRate = UserRate::where('user_id', $Store->user_id)
                    ->where('company', 'tfpay')//目前是一样的直接读取支付宝就行
                    ->first();
                if (!$UserRate) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请联系代理商开启此通道',
                    ]);
                }

                $rate = $UserRate->store_all_rate;
                //读取商户的费率
                $StorePayWay = StorePayWay::where('ways_type', $ways_type)
                    ->where('store_id', $store_id)
                    ->select('rate')
                    ->first();
                if ($StorePayWay) {
                    $rate = $StorePayWay->rate;
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户通道费率未设置',
                    ]);
                }

                //传化报备
                $config = new TfConfigController();
                $tf_config = $config->tf_config($config_id, $qd);

                if (!$tf_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '传化配置不存在请检查配置'
                    ]);
                }
                $obj = new \App\Api\Controllers\Tfpay\BaseController();
                $obj->mch_id = $tf_config->mch_id;
                $obj->pub_key = $tf_config->pub_key;
                $obj->pri_key = $tf_config->pri_key;
                $sub_mch_id = $MerchantId;

                //签署电子合同
                $post_data = [
                    'sub_mch_id' => $sub_mch_id,
                ];

                $method = '/openapi/merchant/contract';
                $re = $obj->api($post_data, $method, false);

                //费率同步
                $rate1 = $rate * 10;
                $post_data = [
                    'sub_mch_id' => $sub_mch_id,
                    'WECHAT_POS' => $rate1,
                    'WECHAT_SCAN' => $rate1,
                    'WECHAT_MP' => $rate1,
                    'ALIPAY_POS' => $rate1,
                    'ALIPAY_SCAN' => $rate1,
                    'ALIPAY_MP' => $rate1,
                ];

                $method = '/openapi/merchant/rate';
                $re = $obj->api($post_data, $method, false);
                if ($re['code'] != 0) {
                    return json_encode([
                        'status' => 2,
                        'message' => $re['msg']
                    ]);
                }

                //新增app ID
                $post_data = [
                    'sub_mch_id' => $MerchantId,
                    'sub_appid' => $tf_config->wx_appid,
                ];
                $method = '/openapi/merchant/wechat/appid';
                $appid = $obj->api($post_data, $method, false);

                //报备支付授权目录
                $post_data = [
                    'sub_mch_id' => $MerchantId,
                    'jsapi_path' => env('APP_URL') . '/api/tfpay/weixin/',
                ];
                $method = '/openapi/merchant/wechat/jsapi-path';
                $jsapipath = $obj->api($post_data, $method, false);

                $in = [
                    'store_id' => $store_id,
                    'config_id' => $config_id,
                    'agent_id' => $tf_config->mch_id,
                    'sub_mch_id' => $MerchantId,
                    'status' => 1,
                    'qd' => $qd
                ];
                if ($TfStore) {
                    TfStore::where('store_id', $store_id)->update($in);
                } else {
                    TfStore::create($in);
                }

                //默认支付通道未注册成功
                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $pay_status;
                $data['status_desc'] = $pay_status_desc;
                $data['company'] = 'tfpay';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //工行
            if (19999 < $ways_type && $ways_type < 20999) {
                $data = LianfuStores::where('store_id', $store_id)
                    ->select('apikey as MerchantId', 'signkey')
                    ->first();

                if ($MerchantId == '') {
                    if ($data) {
                        return json_encode([
                            'status' => 1,
                            'data' => $data
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    $in = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'apikey' => $MerchantId,
                        'signkey' => $signkey,
                    ];
                    if ($data) {
                        LianfuStores::where('store_id', $store_id)->update($in);
                    } else {
                        LianfuStores::create($in);
                    }

                    //默认支付通道未注册成功
                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['status'] = $pay_status;
                    $data['status_desc'] = $pay_status_desc;
                    $data['company'] = 'lianfu';
                    $return = $this->send_ways_data($data);

                    return $return;
                }
            }

            //直付通
            if (15999 < $ways_type && $ways_type < 16999) {
                $data = AlipayZftStore::where('store_id', $store_id)
                    ->select('smid as MerchantId', 'order_id')
                    ->first();

                if ($MerchantId == '') {
                    if ($data) {
                        return json_encode([
                            'status' => 1,
                            'data' => $data
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    if ($data) {
                        $order_id = $data->order_id;
                        //配置
                        $isvconfig = new AlipayIsvConfigController();
                        $config_type = '03';//直付通
                        $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);
                        if ($config) {
                            $aop = new AopClient();
                            $aop->useHeader = false;
                            $aop->appId = $config->app_id;
                            $aop->rsaPrivateKey = $config->rsa_private_key;
                            $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                            $aop->gatewayUrl = $config->alipay_gateway;
                            $aop->apiVersion = '1.0';
                            $aop->signType = 'RSA2';
                            $aop->format = 'json';
                            $aop->method = "ant.merchant.expand.order.query";
                            $request = new AntMerchantExpandOrderQueryRequest();
                            $request->setBizContent("{" .
                                "\"order_id\":\"" . $order_id . "\"" .
                                "  }");
                            $result = $aop->execute($request);

                            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
                            $resultCode = $result->$responseNode->code;
                            if (!empty($resultCode) && $resultCode == 10000) {
                                $ext_info = $result->$responseNode->ext_info;
                                $data = json_decode($ext_info, true);
                                //成功
                                if ($result->$responseNode->status == "99") {
                                    $smid = $data['smid'];
                                    $card_alias_no = isset($data['card_alias_no']) ? $data['card_alias_no'] : "";

                                    AlipayZftStore::where('store_id', $store_id)
                                        ->update([
                                            'ext_info' => $ext_info,
                                            'status' => '99',
                                            'smid' => $smid,
                                            'card_alias_no' => $card_alias_no,
                                        ]);

                                    $data_up = [
                                        'status' => 1,
                                        'status_desc' => '审核成功',
                                    ];

                                    StorePayWay::where('store_id', $store_id)
                                        ->where('company', 'zft')
                                        ->update($data_up);
                                }

                                //失败
                                if ($result->$responseNode->status == "-1") {
                                    AlipayZftStore::where('store_id', $store_id)
                                        ->update([
                                            'ext_info' => $ext_info,
                                            'status' => '-1',
                                        ]);

                                    $data_up = [
                                        'status' => 3,
                                        'status_desc' => '审核失败',
                                    ];

                                    StorePayWay::where('store_id', $store_id)
                                        ->where('company', 'zft')
                                        ->update($data_up);
                                }

                                //审核中
                                $data_up = [
                                    'status' => 2,
                                    'status_desc' => '等待商户支付宝确认',
                                ];
                            }
                        }
                    } else {
                        $in = [
                            'store_id' => $store_id,
                            'config_id' => $config_id,
                            'smid' => $MerchantId,
                            'order_id' => '1',
                            'ip_role_id' => '1',
                            'ext_info' => '1',
                            'store_name' => $store_name,
                        ];
                        if ($data) {
                            AlipayZftStore::where('store_id', $store_id)->update($in);
                        } else {
                            AlipayZftStore::create($in);
                        }
                    }

                    //默认支付通道未注册成功
                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['status'] = $pay_status;
                    $data['status_desc'] = $pay_status_desc;
                    $data['company'] = 'zft';
                    $return = $this->send_ways_data($data);

                    return $return;
                }
            }

            //苏州银行
            if (17000 < $ways_type && $ways_type < 17999) {
                $data = SuzhouStore::where('store_id', $store_id)
                    ->select('MerchantId')->first();
                if ($MerchantId == '') {
                    if ($data) {
                        return json_encode([
                            'status' => 1,
                            'data' => $data
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    $in = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'MerchantId' => $MerchantId,
                    ];
                    if ($data) {
                        SuzhouStore::where('store_id', $store_id)->update($in);
                    } else {
                        SuzhouStore::create($in);
                    }

                    //默认支付通道未注册成功
                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['status'] = $pay_status;
                    $data['status_desc'] = $pay_status_desc;
                    $data['company'] = 'suzhou';
                    $return = $this->send_ways_data($data);

                    return $return;
                }
            }

            //葫芦天下
            if (22999 < $ways_type && $ways_type < 23999) {
                $HltxStore = HltxStore::where('store_id', $store_id)->first();
                $obj = new ManageController();
                $hltx_config = $obj->pay_config($config_id);
                if (!$hltx_config) {
                    return json_encode(['status' => 2, 'message' => 'HL通道配置不存在']);
                }
                $obj->init($hltx_config);

                if ($MerchantId == '') {
                    if ($HltxStore) {
                        //查询状态
                        if ($HltxStore->serial_no) {
                            $re = $obj->hltx_merchant_result_query($HltxStore->serial_no);
                            // dd($re);
                        }

                        return json_encode([
                            'status' => 1,
                            'data' => [
                                'MerchantId' => $HltxStore->mer_no,
                                'wx_merchant_no' => $HltxStore->wx_merchant_no,
                                'ali_merchant_no' => $HltxStore->ali_merchant_no,
                                'union_merchant_no' => $HltxStore->union_merchant_no,
                                'qd' => $HltxStore->qd
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    $in = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'mer_no' => $MerchantId,
                        'status' => '00',
                        'qd' => $qd
                    ];
                    if ($HltxStore) {
                        HltxStore::where('store_id', $store_id)->update($in);
                    } else {
                        HltxStore::create($in);
                    }
                    $params = [
                        'merNo' => $MerchantId,
                        'path' => $weixin_path ? $weixin_path : env('APP_URL') . '/api/hltx/weixin/',
                        'appId' => $hltx_config->pay_appid,
                        'subScribeAppIds' => $hltx_config->subscribe_appid,
                    ];

                    $obj->merchant_wxsetconig($params);

                    //默认支付通道未注册成功
                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['status'] = $pay_status;
                    $data['status_desc'] = $pay_status_desc;
                    $data['company'] = 'hltx';
                    $return = $this->send_ways_data($data);

                    return $return;
                }
            }

            //长沙银行
            if (25000 < $ways_type && $ways_type < 25999) {
                $data = ChangshaStore::where('store_id', $store_id)
                    ->select('ECustId as MerchantId', 'DeptId', 'StaffId')->first();
                if ($MerchantId == '') {
                    if ($data) {
                        return json_encode([
                            'status' => 1,
                            'data' => $data
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    $in = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'ECustId' => $MerchantId,
                        'DeptId' => $DeptId,
                        'StaffId' => $StaffId
                    ];

                    if ($data) {
                        ChangshaStore::where('store_id', $store_id)->update($in);
                    } else {
                        ChangshaStore::create($in);
                    }

                    //默认支付通道未注册成功
                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['status'] = $pay_status;
                    $data['status_desc'] = $pay_status_desc;
                    $data['company'] = 'changsha';
                    $return = $this->send_ways_data($data);

                    return $return;
                }
            }

            //邮政
            if (26000 < $ways_type && $ways_type < 26999) {
                $data = LianfuyoupayStores::where('store_id', $store_id)
                    ->select(
                        'apikey as MerchantId',
                        'signkey',
                        'pos_sn'
                    )
                    ->first();

                if ($MerchantId == '') {
                    if ($data) {
                        return json_encode([
                            'status' => 1,
                            'data' => $data
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    $in = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'apikey' => $MerchantId,
                        'signkey' => $signkey,
                        'pos_sn' => $pos_sn,
                    ];
                    if ($data) {
                        LianfuyoupayStores::where('store_id', $store_id)->update($in);
                    } else {
                        LianfuyoupayStores::create($in);
                    }

                    //默认支付通道未注册成功
                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['status'] = $pay_status;
                    $data['status_desc'] = $pay_status_desc;
                    $data['company'] = 'lianfuyoupay';
                    $return = $this->send_ways_data($data);

                    return $return;
                }
            }

            //海科融通
            if (21999 < $ways_type && $ways_type < 22999) {
                $hkrt_store = HkrtStore::where('store_id', $store_id)->first();
                if (!$hkrt_store) {
                    //看下是否有修改权限
                    $hasPermission = $user->hasPermissionTo('海科融通商户号添加');
                    if (!$hasPermission) {
                        return json_encode(['status' => 2, 'message' => '没有-海科融通商户号添加-权限']);
                    }
                } else {
                    //看下是否有添加权限
                    $hasPermission = $user->hasPermissionTo('海科融通商户号修改');
                    if (!$hasPermission) {
                        return json_encode(['status' => 2, 'message' => '没有-海科融通商户号修改-权限']);
                    }
                }

                if ($hkrtMerchNo == '') {
                    if ($hkrt_store) {
                        return json_encode([
                            'status' => '1',
                            'data' => [
                                'hkrtMerchNo' => $hkrt_store->merch_no,
                                'hkrtAgentApplyNo' => $hkrt_store->agent_apply_no
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'data' => []
                        ]);
                    }
                } else {
                    $config = new HkrtConfigController();
                    $hkrt_config = $config->hkrt_config($config_id);
                    if (!$hkrt_config) {
                        return json_encode([
                            'status' => 2,
                            'message' => '海科融通配置不存在请检查配置'
                        ]);
                    }

                    //商户入驻状态查询
                    if ($hkrt_store && $hkrt_store->merch_no) {
                        $obj = new \App\Api\Controllers\Hkrt\PayController();
                        $hkrt_data = [
                            'access_id' => $hkrt_config->access_id,
                            'agent_no' => $hkrt_config->agent_no,
                            'agent_apply_no' => $hkrt_store->agent_apply_no,
                            'apply_type' => '1', //1-商户入网申请;2-微信业务申请;3-支付宝业务申请;4-银行卡业务申请;10-商户分账业务申请;11-商户分账关系申请;12-商户结算周期变更申请;13-商户分账业务修改
                            'access_key' => $hkrt_config->access_key
                        ];
//                        Log::info('海科融通-商户入驻状态查询');
//                        Log::info($hkrt_data);
                        $open_query_store = $obj->merchant_query($hkrt_data); //0-系统错误 1-提交失败 2-已受理 3-自动审核 4-待审核 5-审核失败 6-审核拒绝 7-审核成功 8-审核中 9-待报备 10-报备中 11-报备失败 12-报备成功
                        Log::info('海科融通-商户入驻状态查询结果');
                        Log::info($open_query_store);
                        //审核状态
                        if ($open_query_store['status'] != '0') {
                            if ($open_query_store['status'] != '7' || $open_query_store['status'] != '3') {
                                $data_up = [
                                    'status' => 3,
                                    'status_desc' => $open_query_store['message'],
                                ];
                                StorePayWay::where('store_id', $store_id)
                                    ->where('company', 'hkrt')
                                    ->update($data_up);

                                $hkrt_update_data = [
                                    'status' => $open_query_store['status']
                                ];
                                if ($hkrtAgentApplyNo) {
                                    $hkrt_update_data['agent_apply_no'] = $hkrtAgentApplyNo;
                                } //服务商申请单号
                                $hkrt_res = HkrtStore::where('store_id', $store_id)
                                    ->update($hkrt_update_data);
                                if (!$hkrt_res) {
                                    Log::info('海科融通-商户入网-入库更新失败');
                                    Log::info($store_id);
                                }

                                return json_encode([
                                    'status' => 2,
                                    'message' => $open_query_store['message'],
                                ]);
                            }
                        }
                    }

                    //读取商户的费率
                    $StorePayWay = StorePayWay::where('ways_type', $ways_type)
                        ->where('store_id', $store_id)
                        ->select('rate')
                        ->first();
                    if ($StorePayWay) {
                        $rate = $StorePayWay->rate;
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '商户通道费率未设置',
                        ]);
                    }

                    $hkrt_obj = new \App\Api\Controllers\Hkrt\PayController();
                    //报备微信授权目录
                    $hkrt_data = [
                        'access_id' => $hkrt_config->access_id,
                        'agent_no' => $hkrt_config->agent_no,
                        'merch_no' => $MerchantId,
                        'conf_key' => 'auth_path', //授权目录
                        'conf_value' => $weixin_path,
                        'access_key' => $hkrt_config->access_key
                    ];
                    $hkrt_res = $hkrt_obj->wx_appid_conf_add($hkrt_data); //0-失败 1-成功
                    Log::info('商户号报备微信');
                    Log::info($hkrt_res);
                    if ($hkrt_res['status'] == '1') {
                        Log::info('商户号报备微信成功');
                    }

                    if ($hkrt_store) {
                        HkrtStore::where('store_id', $store_id)
                            ->update([
                                'merch_no' => $MerchantId
                            ]);
                    } else {
                        HkrtStore::create([
                            'store_id' => $store_id,
                            'config_id' => $config_id,
                            'merch_no' => $MerchantId
                        ]);
                    }
                }

                //费率 默认商户的费率为代理商的费率
                $UserRate = UserRate::where('user_id', $Store->user_id)
                    ->where('ways_type', 22001) //目前是一样的直接读取支付宝就行
                    ->first();
                if (!$UserRate) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请联系代理商开启此通道',
                    ]);
                }

                //默认支付通道未注册成功
                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $pay_status;
                $data['status_desc'] = $pay_status_desc;
                $data['company'] = 'hkrt';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //易生支付
            if (20999 < $ways_type && $ways_type < 21999) {
                $easypaystore = EasypayStore::where('store_id', $store_id)->first();
//                if (!$easypaystore) {
//                    $hasPermission = $user->hasPermissionTo('易生支付商户号添加');
//                    if (!$hasPermission) {
//                        return json_encode([
//                            'status' => 2,
//                            'message' => '没有-易生支付商户号添加-权限'
//                        ]);
//                    }
//                } else {
//                    $hasPermission = $user->hasPermissionTo('易生支付商户号修改');
//                    if (!$hasPermission) {
//                        return json_encode([
//                            'status' => 2,
//                            'message' => '没有-易生支付商户号修改-权限'
//                        ]);
//                    }
//                }

                if ($easyPayMerCode == '' && $easyPayTermCode == '') {
                    if ($easypaystore) {
                        return json_encode([
                            'status' => '1',
                            'data' => [
                                'operaTrace' => $easypaystore->opera_trace,
                                'merTrace' => $easypaystore->mer_trace,
                                'easyPayMerCode' => $easypaystore->term_mercode,
                                'easyPayTermCode' => $easypaystore->term_termcode
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'data' => []
                        ]);
                    }
                } else {
                    $config = new EasyPayConfigController();
                    $easypayconfig = $config->easypay_config($config_id);
                    if (!$easypayconfig) {
                        return json_encode([
                            'status' => '2',
                            'message' => '易生支付配置不存在请检查配置'
                        ]);
                    }

                    //商户审核结果查询
                    if ($easypaystore && $easypaystore->opera_trace) {
                        $obj = new MerAccessController();
                        $easypaydata = [
                            'clientCode' => $easypayconfig->client_code, //机构号
                            'merTrace' => $easypaystore->mer_trace ?? '', //O,商户唯一标识
                            'operaTrace' => $easypaystore->opera_trace, //原操作流水号
                            'key' => $easypayconfig->key //进件密钥
                        ];
//                        Log::info('易生支付-商户审核结果查询-入参：');
//                        Log::info($easypaydata);
                        $open_query_store = $obj->merchant_query($easypaydata); //0-审核通过 1-审核驳回 2-失败 3-待审核 9-待处理 X-审核失败
//                        Log::info('易生支付-商户审核结果查询-结果：');
//                        Log::info($open_query_store);
                        //审核状态
                        if ($open_query_store['status'] == '0') {
                            $data_up = [
                                'status' => 1,
                                'status_desc' => $open_query_store['message'],
                            ];
                            StorePayWay::where('store_id', $store_id)
                                ->where('company', 'easypay')
                                ->update($data_up);

                            EasypayStore::where('store_id', $store_id)
                                ->update([
                                    'audit_status' => 0,
                                    'audit_msg' => $open_query_store['message']
                                ]);
                        } else {
                            $data_up = [
                                'status' => 3,
                                'status_desc' => $open_query_store['message'],
                            ];
                            StorePayWay::where('store_id', $store_id)
                                ->where('company', 'easypay')
                                ->update($data_up);

                            EasypayStore::where('store_id', $store_id)
                                ->update([
                                    'audit_status' => $open_query_store['status'],
                                    'audit_msg' => $open_query_store['message']
                                ]);

                            return json_encode([
                                'status' => 2,
                                'message' => $open_query_store['message'],
                            ]);
                        }
                    }

                    //易生平台进件成功
//                    StorePayWay::where('store_id', $store_id)
//                        ->where('company', 'easypay')
//                        ->update([
//                            'status' => '1',
//                            'status_desc' => '开通成功'
//                        ]);

                    //读取商户的费率
                    $StorePayWay = StorePayWay::where('ways_type', $ways_type)
                        ->where('store_id', $store_id)
                        ->select('rate')
                        ->first();
                    if ($StorePayWay) {
                        $rate = $StorePayWay->rate;
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '商户通道费率未设置',
                        ]);
                    }

//                    $easypayobj = new \App\Api\Controllers\easypay\PayController();
//                    //报备微信授权目录
//                    $easypaydata = [
//                        'access_id' => $easypayconfig->access_id,
//                        'agent_no' => $easypayconfig->agent_no,
//                        'merch_no' => $MerchantId,
//                        'conf_key' => 'auth_path', //授权目录
//                        'conf_value' => $weixin_path,
//                        'access_key' => $easypayconfig->access_key
//                    ];
//                    $easypayres = $easypayobj->wx_appid_conf_add($easypaydata); //0-失败 1-成功
//                    Log::info('商户号报备微信');
//                    Log::info($easypayres);
//                    if ($easypayres['status'] == '1') {
//                        Log::info('商户号报备微信成功');
//                    }

                    if ($easypaystore) {
                        EasypayStore::where('store_id', $store_id)
                            ->update([
                                'opera_trace' => $easyPayOperaTrace,
                                'term_mercode' => $easyPayMerCode,
                                'term_termcode' => $easyPayTermCode
                            ]);
                    } else {
                        $insertData = [
                            'store_id' => $store_id,
                            'config_id' => $config_id,
                            'audit_status' => '0',
                            'audit_msg' => '审核通过'
                        ];
                        if ($easyPayOperaTrace) $insertData['opera_trace'] = $easyPayOperaTrace;
                        if ($easyPayMerCode) $insertData['term_mercode'] = $easyPayMerCode;
                        if ($easyPayTermCode) $insertData['term_termcode'] = $easyPayTermCode;
                        EasypayStore::create($insertData);
                    }
                }

                //费率 默认商户的费率为代理商的费率
                $UserRate = UserRate::where('user_id', $Store->user_id)
                    ->where('ways_type', 21001) //目前是一样的直接读取支付宝就行
                    ->first();
                if (!$UserRate) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请联系代理商开启此通道',
                    ]);
                }

                //默认支付通道未注册成功
                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $pay_status;
                $data['status_desc'] = $pay_status_desc;
                $data['company'] = 'easypay';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //联动优势
            if (4999 < $ways_type && $ways_type < 5999) {
                $linkageStore = LinkageStores::where('store_id', $store_id)->first();
                if (!$linkageStore) {
                    $hasPermission = $user->hasPermissionTo('联动优势商户号添加');
                    if (!$hasPermission) {
                        return json_encode([
                            'status' => '2',
                            'message' => '没有-联动优势商户号添加-权限'
                        ]);
                    }
                } else {
                    $hasPermission = $user->hasPermissionTo('联动优势商户号修改');
                    if (!$hasPermission) {
                        return json_encode([
                            'status' => '2',
                            'message' => '没有-联动优势商户号修改-权限'
                        ]);
                    }
                }

                if ($linkageAcqMerId == '') {
                    if ($linkageStore) {
                        return json_encode([
                            'status' => '1',
                            'data' => [
                                'acqMerId' => $linkageStore->acqMerId, //商户号(联动平台分配)
                                'subAppid' => $linkageStore->subAppid, //子商户SubAPPID
                                'subMchId' => $linkageStore->subMchId, //微信子商户号
                                'merId' => $linkageStore->merId //报备编号
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'data' => []
                        ]);
                    }
                } else {
                    $config = new LinkageConfigController();
                    $linkageConfig = $config->linkage_config($config_id);
                    if (!$linkageConfig) {
                        return json_encode([
                            'status' => '2',
                            'message' => '联动优势支付配置不存在，请检查配置'
                        ]);
                    }

                    //读取商户的费率
                    $StorePayWay = StorePayWay::where('ways_type', $ways_type)
                        ->where('store_id', $store_id)
                        ->select('rate')
                        ->first();
                    if ($StorePayWay) {
                        $rate = $StorePayWay->rate;
                    } else {
                        return json_encode([
                            'status' => '2',
                            'message' => '商户通道费率未设置'
                        ]);
                    }

                    $linkageObj = new \App\Api\Controllers\Linkage\PayController();
                    if ($weixin_path) {
                        //微信参数配置-支付授权目录
                        $linkageData = [
                            'acqSpId' => $linkageConfig->mch_id, //服务商编号(联动平台分配)
                            'acqMerId' => $linkageAcqMerId, //商户号
                            'jsapiPath' => $weixin_path ? $weixin_path : env('APP_URL') . '/api/linkage/weixin/',
                            'publicKey' => $linkageConfig->publicKey,
                            'privateKey' => $linkageConfig->privateKey
                        ];
                        $linkageRes = $linkageObj->merchantsJsApiPath($linkageData); //-1系统错误 0-其他 1-成功 2-验签失败 3-失败
                        Log::info('联动优势-报备微信授权目录');
                        Log::info($linkageRes);
                        if ($linkageRes['status'] != '1') {
                            Log::info('联动优势-报备微信授权目录-error');
                            Log::info($linkageRes['message']);
                            return json_encode([
                                'status' => '2',
                                'message' => $linkageRes['message']
                            ]);
                        }
                    }
                    Log::info($linkageSubAppid);
                    //微信参数配置-子商户appId
                    if ($linkageSubAppid) {
                        $linkageSubData = [
                            'acqSpId' => $linkageConfig->mch_id, //服务商编号(联动平台分配)
                            'acqMerId' => $linkageAcqMerId, //商户号
                            'subAppid' => $linkageSubAppid, //子商户SubAPPID
                            'publicKey' => $linkageConfig->publicKey,
                            'privateKey' => $linkageConfig->privateKey
                        ];
                        $linkageSubRes = $linkageObj->merchantsSubAppid($linkageSubData); //-1系统错误 0-其他 1-成功 2-验签失败 3-失败
                        if ($linkageSubRes['status'] != '1') {
                            Log::info('联动优势-微信参数配置-子商户appId-error');
                            Log::info($linkageSubRes['message']);
                            return json_encode([
                                'status' => '2',
                                'message' => $linkageSubRes['message']
                            ]);
                        }
                    }

                    if ($linkageStore) {
                        $updateData = [
                            'acqMerId' => $linkageAcqMerId
                        ];
                        if ($linkageSubAppid) {
                            $updateData['subAppid'] = $linkageSubAppid;
                        }
                        if ($linkageSubMchId) {
                            $updateData['subMchId'] = $linkageSubMchId;
                        }
                        if ($linkageMerId) {
                            $updateData['merId'] = $linkageMerId;
                        }
                        $linkageStore->update($updateData);
                    } else {
                        $linkageDataInsert = [
                            'store_id' => $store_id,
                            'config_id' => $config_id,
                            'acqMerId' => $linkageAcqMerId,
                            'status' => '1'
                        ];
                        if ($linkageMerId) {
                            $linkageDataInsert['merId'] = $linkageMerId;
                        }
                        if ($linkageSubAppid) {
                            $linkageDataInsert['subAppid'] = $linkageSubAppid;
                        }
                        if ($linkageSubMchId) {
                            $linkageDataInsert['subMchId'] = $linkageSubMchId;
                        }
                        LinkageStores::create($linkageDataInsert);
                    }
                }

                //费率 默认商户的费率为代理商的费率
                $UserRate = UserRate::where('user_id', $Store->user_id)
                    ->where('ways_type', 5001) //目前是一样的直接读取支付宝就行
                    ->first();
                if (!$UserRate) {
                    return json_encode([
                        'status' => '2',
                        'message' => '请联系代理商开启此通道'
                    ]);
                }

                //默认支付通道未注册成功
                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $pay_status;
                $data['status_desc'] = $pay_status_desc;
                $data['company'] = 'linkage';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //威富通
            if (26999 < $ways_type && $ways_type < 27999) {
                $wftpayStore = WftpayStore::where('store_id', $store_id)->first();
                if (!$wftpayStore) {
                    $hasPermission = $user->hasPermissionTo('威富通商户号添加');
                    if (!$hasPermission) {
                        return json_encode([
                            'status' => '2',
                            'message' => '没有-威富通商户号添加-权限'
                        ]);
                    }
                } else {
                    $hasPermission = $user->hasPermissionTo('威富通商户号修改');
                    if (!$hasPermission) {
                        return json_encode([
                            'status' => '2',
                            'message' => '没有-威富通商户号修改-权限'
                        ]);
                    }
                }

                if ($wftpayMchId == '') {
                    if ($wftpayStore) {
                        return json_encode([
                            'status' => '1',
                            'data' => [
                                'wftpayMchId' => $wftpayStore->mch_id, //商户号
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'data' => []
                        ]);
                    }
                } else {
                    $config = new WftPayConfigController();
                    $wftpayConfig = $config->wftpay_config($config_id);
                    if (!$wftpayConfig) {
                        return json_encode([
                            'status' => '2',
                            'message' => '威富通支付配置不存在，请检查配置'
                        ]);
                    }

                    //读取商户的费率
                    $StorePayWay = StorePayWay::where('ways_type', $ways_type)
                        ->where('store_id', $store_id)
                        ->select('rate')
                        ->first();
                    if ($StorePayWay) {
                        $rate = $StorePayWay->rate;
                    } else {
                        return json_encode([
                            'status' => '2',
                            'message' => '商户通道费率未设置'
                        ]);
                    }

                    if ($wftpayStore) {
                        $updateData = [
                            'mch_id' => $wftpayMchId
                        ];
                        $wftpayStore->update($updateData);
                    } else {
                        $wftpayDataInsert = [
                            'store_id' => $store_id,
                            'config_id' => $config_id,
                            'mch_id' => $wftpayMchId,
                            'status' => 1
                        ];
                        WftpayStore::create($wftpayDataInsert);
                    }

                    //todo:默认第三方商户入网成功
                    $pay_status = 1;
                    $pay_status_desc = '开通成功';
                }

                //费率 默认商户的费率为代理商的费率
                $UserRate = UserRate::where('user_id', $Store->user_id)
                    ->where('ways_type', 27001) //目前是一样的直接读取支付宝就行
                    ->first();
                if (!$UserRate) {
                    return json_encode([
                        'status' => '2',
                        'message' => '请联系代理商开启此通道'
                    ]);
                }

                //默认支付通道未注册成功
                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $pay_status;
                $data['status_desc'] = $pay_status_desc;
                $data['company'] = 'wftpay';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //汇旺财
            if (27999 < $ways_type && $ways_type < 28999) {
                $hwcpayStore = HwcpayStore::where('store_id', $store_id)->first();
                if (!$hwcpayStore) {
                    $hasPermission = $user->hasPermissionTo('汇旺财商户号添加');
                    if (!$hasPermission) {
                        return json_encode([
                            'status' => '2',
                            'message' => '没有-汇旺财商户号添加-权限'
                        ]);
                    }
                } else {
                    $hasPermission = $user->hasPermissionTo('汇旺财商户号修改');
                    if (!$hasPermission) {
                        return json_encode([
                            'status' => '2',
                            'message' => '没有-汇旺财商户号修改-权限'
                        ]);
                    }
                }

                if ($hwcpayMchId == '') {
                    if ($hwcpayStore) {
                        return json_encode([
                            'status' => '1',
                            'data' => [
                                'hwcpayMchId' => $hwcpayStore->mch_id, //门店号
                                'hwcpayMerchantNum' => $hwcpayStore->merchant_num, //商户号
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => '1',
                            'data' => []
                        ]);
                    }
                } else {
                    $config = new HwcPayConfigController();
                    $hwcpayConfig = $config->hwcpay_config($config_id);
                    if (!$hwcpayConfig) {
                        return json_encode([
                            'status' => '2',
                            'message' => '汇旺财支付配置不存在，请检查配置'
                        ]);
                    }

                    //读取商户的费率
                    $StorePayWay = StorePayWay::where('ways_type', $ways_type)
                        ->where('store_id', $store_id)
                        ->select('rate')
                        ->first();
                    if ($StorePayWay) {
                        $rate = $StorePayWay->rate;
                    } else {
                        return json_encode([
                            'status' => '2',
                            'message' => '商户通道费率未设置'
                        ]);
                    }

                    if ($hwcpayStore) {
                        $updateData = [
                            'mch_id' => $hwcpayMchId,
                            'merchant_num' => $hwcpayMerchantNum
                        ];
                        $hwcpayStore->update($updateData);
                    } else {
                        $hwcpayDataInsert = [
                            'store_id' => $store_id,
                            'config_id' => $config_id,
                            'mch_id' => $hwcpayMchId,
                            'merchant_num' => $hwcpayMerchantNum,
                            'status' => 1
                        ];
                        HwcpayStore::create($hwcpayDataInsert);
                    }

                    //todo:默认第三方商户入网成功
                    $pay_status = 1;
                    $pay_status_desc = '开通成功';
                }

                //费率 默认商户的费率为代理商的费率
                $UserRate = UserRate::where('user_id', $Store->user_id)
                    ->where('ways_type', 28001) //目前是一样的直接读取支付宝就行
                    ->first();
                if (!$UserRate) {
                    return json_encode([
                        'status' => '2',
                        'message' => '请联系代理商开启此通道'
                    ]);
                }

                //默认支付通道未注册成功
                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $pay_status;
                $data['status_desc'] = $pay_status_desc;
                $data['company'] = 'hwcpay';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //银盛
            if (13999 < $ways_type && $ways_type < 14999) {
                $yinshengStore = YinshengStore::where('store_id', $store_id)->first();

                if ($yinshengPartnerId == '' || $yinshengSellerName == '' || $yinshengBusinessCode == '') {
                    if ($yinshengStore) {
                        return json_encode([
                            'status' => 1,
                            'data' => [
                                'yinshengPartnerId' => $yinshengStore->partner_id, //商户号
                                'yinshengSellerName' => $yinshengStore->seller_name, //公司名称
                                'yinshengBusinessCode' => $yinshengStore->business_code, //业务代码
                                'ali_mer' => $yinshengStore->ali_mer,
                                'wei_mer' => $yinshengStore->wei_mer,
                                'mer_code' => $yinshengStore->mer_code
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    $config = new YinshengConfigController();
                    $yinshengConfig = $config->yinsheng_config($config_id);
                    if (!$yinshengConfig) {
                        return json_encode([
                            'status' => '2',
                            'message' => '银盛支付配置不存在,请检查配置'
                        ]);
                    }

                    //读取商户的费率
                    $StorePayWay = StorePayWay::where('ways_type', $ways_type)
                        ->where('store_id', $store_id)
                        ->select('rate')
                        ->first();
                    if ($StorePayWay) {
                        $rate = $StorePayWay->rate;
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '商户通道费率未设置'
                        ]);
                    }

                    if ($yinshengStore) {
                        $updateData = [
                            'partner_id' => $yinshengPartnerId,
                            'seller_name' => $yinshengSellerName,
                            'business_code' => $yinshengBusinessCode
                        ];
                        $yinshengStore->update($updateData);
                    } else {
                        $yinshengDataInsert = [
                            'store_id' => $store_id,
                            'config_id' => $config_id,
                            'partner_id' => $yinshengPartnerId,
                            'seller_name' => $yinshengSellerName,
                            'business_code' => $yinshengBusinessCode,
                            'status' => 1
                        ];
                        YinshengStore::create($yinshengDataInsert);
                    }

                    //todo:默认第三方商户入网成功
                    $pay_status = 1;
                    $pay_status_desc = '开通成功';
                }

                //费率 默认商户的费率为代理商的费率
//                $UserRate = UserRate::where('user_id', 1)
//                    ->where('ways_type', 14001) //目前是一样的直接读取支付宝就行
//                    ->first();
//                if (!$UserRate) {
//                    return json_encode([
//                        'status' => '2',
//                        'message' => '请联系代理商开启此通道'
//                    ]);
//                }

                //默认支付通道未注册成功
                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $pay_status;
                $data['status_desc'] = $pay_status_desc;
                $data['company'] = 'yinsheng';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //钱方
            if (23999 < $ways_type && $ways_type < 24999) {
                $qfPayStore = QfpayStore::where('store_id', $store_id)->first();
                if (!$qfPayStore) {
                    $hasPermission = $user->hasPermissionTo('钱方商户号添加');
                    if (!$hasPermission) {
                        return json_encode([
                            'status' => '2',
                            'message' => "没有'钱方商户号添加'权限"
                        ]);
                    }
                } else {
                    $hasPermission = $user->hasPermissionTo('钱方商户号修改');
                    if (!$hasPermission) {
                        return json_encode([
                            'status' => '2',
                            'message' => "没有'钱方商户号修改'权限"
                        ]);
                    }
                }

                if ($qfPayMchid == '') {
                    if ($qfPayStore) {
                        return json_encode([
                            'status' => 1,
                            'data' => [
                                'qfPayMchid' => $qfPayStore->mchid //商户号
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    $config = new QfPayConfigController();
                    $qfPayConfig = $config->qfpay_config($config_id);
                    if (!$qfPayConfig) {
                        return json_encode([
                            'status' => 2,
                            'message' => '钱方支付配置不存在,请检查配置'
                        ]);
                    }

                    //读取商户的费率
                    $StorePayWay = StorePayWay::where('ways_type', $ways_type)
                        ->where('store_id', $store_id)
                        ->select('rate')
                        ->first();
                    if ($StorePayWay) {
                        $rate = $StorePayWay->rate;
                    } else {
                        return json_encode([
                            'status' => 2,
                            'message' => '商户通道费率未设置'
                        ]);
                    }

                    if ($qfPayStore) {
                        $updateData = [
                            'mchid' => $qfPayMchid
                        ];
                        $qfPayStore->update($updateData);
                    } else {
                        $qfPayDataInsert = [
                            'config_id' => $config_id,
                            'store_id' => $store_id,
                            'mchid' => $qfPayMchid,
                            'status' => 1
                        ];
                        QfpayStore::create($qfPayDataInsert);
                    }

                    //todo:默认第三方商户入网成功
                    $pay_status = 1;
                    $pay_status_desc = '开通成功';
                }

                //费率 默认商户的费率为代理商的费率
                $UserRate = UserRate::where('user_id', $Store->user_id)
                    ->where('ways_type', 24001) //目前是一样的直接读取支付宝就行
                    ->first();
                if (!$UserRate) {
                    return json_encode([
                        'status' => '2',
                        'message' => '请联系代理商开启此通道'
                    ]);
                }

                //默认支付通道未注册成功
                $data['store_id'] = $store_id;
                $data['rate'] = $rate;
                $data['status'] = $pay_status;
                $data['status_desc'] = $pay_status_desc;
                $data['company'] = 'qfpay';
                $return = $this->send_ways_data($data);

                return $return;
            }

            //邮驿付
            if ($ways_type == "29001" || $ways_type == "29002" || $ways_type == "29003") {
                $PostPayStore = PostPayStore::where('store_id', $store_id)->first();
                if ($custId == '') {
                    if ($PostPayStore) {
                        return json_encode([
                            'status' => 1,
                            'data' => [
                                'cust_id' => $PostPayStore->cust_id,
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    $in = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'cust_id' => $custId,
                        'audit_status' => '0',
                        'audit_msg' => '审核通过'
                    ];
                    if ($PostPayStore) {
                        $driveNo = substr(microtime(), 2, 8); //终端号
                        $in['drive_no'] = $driveNo;
                        PostPayStore::where('store_id', $store_id)->update($in);
                    } else {
                        PostPayStore::create($in);
                    }

                    //默认支付通道未注册成功
                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['status'] = 1;
                    $data['status_desc'] = '开通成功';
                    $data['company'] = 'postpay';
                    $return = $this->send_ways_data($data);

                    return $return;
                }
            }

            //建设银行
            if ($ways_type == "31001" || $ways_type == "31002" || $ways_type == "31003") {
                $data = CcBankPayStore::where('store_id', $store_id)->first();
                Log::info($jh_cust_id);
                if ($jh_cust_id == '') {
                    if ($data) {
                        return json_encode([
                            'status' => 1,
                            'data' => [
                                'jhCustId' => $data->cust_id,
                                'posId' => $data->pos_id,
                                'branchId' => $data->branch_id,
                                'termno1' => $data->termno1,
                                'termno2' => $data->termno2,
                                'publicKey' => $data->public_key,
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    $in = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'cust_id' => $jh_cust_id,
                        'pos_id' => $pos_id,
                        'branch_id' => $branch_id,
                        'termno1' => $termno1,
                        'termno2' => $termno2,
                        'public_key' => $public_key,
                    ];
                    if ($data) {
                        CcBankPayStore::where('store_id', $store_id)->update($in);
                    } else {
                        CcBankPayStore::create($in);
                    }

                    //默认支付通道未注册成功
                    $data['store_id'] = $store_id;
                    $data['rate'] = 0.1;  //默认参数0.1
                    $data['status'] = 1;
                    $data['status_desc'] = '开通成功';
                    $data['company'] = 'ccbankpay';
                    $return = $this->send_ways_data($data);

                    return $return;
                }
            }

            //易生数科技
            if ($ways_type == "32001" || $ways_type == "32002" || $ways_type == "32003") {
                $EasySkpayStore = EasySkpayStore::where('store_id', $store_id)->first();
                if ($skMerId == '') {
                    if ($EasySkpayStore) {
                        return json_encode([
                            'status' => 1,
                            'data' => [
                                'skMerId' => $EasySkpayStore->mer_id,
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    $in = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'mer_id' => $skMerId,
                        'audit_status' => '0',
                        'audit_msg' => '审核通过'
                    ];
                    if ($EasySkpayStore) {
                        EasySkpayStore::where('store_id', $store_id)->update($in);
                    } else {
                        EasySkpayStore::create($in);
                    }

                    //默认支付通道未注册成功
                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['status'] = 1;
                    $data['status_desc'] = '开通成功';
                    $data['company'] = 'easyskpay';
                    $return = $this->send_ways_data($data);

                    return $return;
                }
            }

            //通联
            if ($ways_type == "33001" || $ways_type == "33002" || $ways_type == "33003") {
                $AllinPayStore = AllinPayStore::where('store_id', $store_id)->first();
                if ($allinCusId == '') {
                    if ($AllinPayStore) {
                        return json_encode([
                            'status' => 1,
                            'data' => [
                                'allinCusId' => $AllinPayStore->cus_id,
                                'allinTermno' => $AllinPayStore->termno,
                                'appid' => $AllinPayStore->appid,
                                'private_key' => $AllinPayStore->private_key,
                            ]
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'data' => []
                        ]);
                    }
                } else {
                    $in = [
                        'store_id' => $store_id,
                        'config_id' => $config_id,
                        'cus_id' => $allinCusId,
                        'termno' => $allinTermno,
                        'appid' => $appid,
                        'private_key' => $private_key,
                        'audit_status' => '0',
                        'audit_msg' => '审核通过'
                    ];
                    if ($AllinPayStore) {
                        AllinPayStore::where('store_id', $store_id)->update($in);
                    } else {
                        AllinPayStore::create($in);
                    }

                    //默认支付通道未注册成功
                    $data['store_id'] = $store_id;
                    $data['rate'] = $rate;
                    $data['status'] = 1;
                    $data['status_desc'] = '开通成功';
                    $data['company'] = 'allinpay';
                    $return = $this->send_ways_data($data);
                    return $return;
                }
            }

            return json_encode([
                'status' => '1',
                'message' => '保存成功'
            ]);
        } catch (\Exception $exception) {
            Log::info('PC-user-开通通道-error');
            Log::info($exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine());
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . ' | ' . $exception->getLine()
            ]);
        }
    }


    //修改费率
    public function send_ways_data($data)
    {
        try {
            $all_pay_ways = DB::table('store_ways_desc')->where('is_show', '1')->where('company', $data['company'])->get();
            Log::info(json_encode($all_pay_ways));
            foreach ($all_pay_ways as $k => $v) {
                $gets = StorePayWay::where('store_id', $data['store_id'])
                    ->where('ways_source', $v->ways_source)
                    ->get();
                $count = count($gets);
                $ways = StorePayWay::where('store_id', $data['store_id'])->where('ways_type', $v->ways_type)->first();
                try {
                    //开启事务
                    DB::beginTransaction();
                    $data = [
                        'store_id' => $data['store_id'],
                        'ways_type' => $v->ways_type,
                        'company' => $v->company,
                        'ways_source' => $v->ways_source,
                        'ways_desc' => $v->ways_desc,
                        'sort' => ($count + 1),
                        'rate' => $data['rate'],
                        'settlement_type' => $v->settlement_type,
                        'status' => $data['status'],
                        'status_desc' => $data['status_desc'],
                    ];
                    if ($v->ways_source == 'unionpay') {
                        $data['pay_amount_e'] = 100;  //银联最大交易金额 100
                    }
                    if ($ways) {
                        $ways->update([
                            'status' => $data['status'],
                            'rate' => $data['rate'],
                            'status_desc' => $data['status_desc'],
                            'pcredit' => '02',
                        ]);
                        $ways->save();
                    } else {
                        StorePayWay::create($data);
                    }
                    DB::commit();
                } catch (\Exception $ex) {
                    Log::info('user-store-入库通道修改费率-error');
                    Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
                    DB::rollBack();
                    return [
                        'status' => 2,
                        'message' => '通道入库费率更新失败',
                    ];
                }
            }

            return [
                'status' => 1,
                'message' => '修改成功',
            ];
        } catch (\Exception $e) {
//            dd($e);
            Log::info('user-store-入库通道22-error');
            Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
            return [
                'status' => 2,
                'message' => '通道入库更新失败',
            ];
        }
    }


    /**
     * 添加分店
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add_sub_store(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_name = $request->get('store_name', '');
            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');
            $store_address = $request->get('store_address', '');
            $pid = $request->get('pid', '');


            if ($store_name == "") {
                $this->status = 2;
                $this->message = '店铺名称必须填写';
                return $this->format();
            }

            if ($area_code == "") {
                $this->status = 2;
                $this->message = '地区编号必须填写';
                return $this->format();
            }
            $pid_store = Store::where('id', $pid)->first();
            if (!$pid_store) {
                return json_encode([
                    'status' => 2,
                    'message' => '总门店不存在'
                ]);
            }

            $User = User::where('id', $pid_store->user_id)
                ->select('is_delete')
                ->first();
            if (!$User) {
                return json_encode([
                    'status' => 2,
                    'message' => '业务员被删除无法添加门店'
                ]);
            }

            if ($User->is_delete) {
                return json_encode([
                    'status' => 2,
                    'message' => '业务员被删除无法添加门店'
                ]);
            }

            if ($pid_store->pid) {
                return json_encode([
                    'status' => 2,
                    'message' => '分店暂不允许再创建分店'
                ]);
            }

            $store_id = date('YmdHis', time()) . rand(10000, 99999);
            $data = [
                'store_id' => $store_id,
                'config_id' => $pid_store->config_id,
                'merchant_id' => $pid_store->merchant_id,
                'pid' => $pid_store->id,
                'store_name' => $store_name,
                'store_short_name' => $store_name,
                'province_code' => $province_code,
                'city_code' => $city_code,
                'area_code' => $area_code,
                'province_name' => $this->city_name($province_code),
                'city_name' => $this->city_name($city_code),
                'area_name' => $this->city_name($area_code),
                'store_address' => $store_address,
                'user_id' => $pid_store->user_id,
                'user_pid' => $pid_store->user_pid,
                'store_type' => $pid_store->store_type,
                'store_type_name' => $pid_store->store_type_name,
                'category_id' => $pid_store->category_id,
                'category_name' => $pid_store->category_name,
                'source' => $pid_store->source,
            ];
            //80开头+当前日期+4位随机数
            $merchantNo = $this->generateMerchantNo();
            //检查是否存在商户号
            $existMerchantNo = Merchant::where('merchant_no', $merchantNo)->first();
            if (!$existMerchantNo) {
                $merchantNo = $this->generateMerchantNo(); //重新获取
            }


            //开启事务
            try {
                DB::beginTransaction();
                //注册merchants账户
                $merchantData = [
                    'pid' => 0,
                    'type' => 1, //1、店长 2 收银员
                    'name' => $store_name,
                    'password' => bcrypt('000000'),
                    'pay_password' => bcrypt('000000'),
                    'phone' => $pid_store->people_phone,
                    'user_id' => $pid_store->user_id,
                    'config_id' => $pid_store->config_id,
                    'wx_openid' => '',
                    'source' => $user->source, //来源,01:畅立收，02:河南畅立收
                    'merchant_no' => $merchantNo,
                    'is_login' => '2'
                ];
                $merchant = Merchant::create($merchantData);
                $merchant_id = $merchant->id;

                $data['merchant_id'] = $merchant_id;
                Store::create($data);
                MerchantStore::create([
                    'store_id' => $store_id,
                    'merchant_id' => $merchant_id
                ]);


                StoreBank::create([
                    'store_id' => $store_id,
                ]);
                StoreImg::create([
                    'store_id' => $store_id,
                ]);

                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                return json_encode([
                    'status' => '-1',
                    'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
                ]);
            }

            $this->status = 1;
            $this->message = '分店添加成功';
            $data = [
                'store_id' => $store_id,
            ];

            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    public function store_pay_qr(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $ways_type = $request->get('ways_type', '');
            $qrListInfo = QrListInfo::where('store_id', $store_id)->select('*')->first();
            $this->status = 1;
            $this->message = '数据返回成功';
            if (!$qrListInfo) {
                $this->status = -1;
                $this->message = '暂未绑定收款码';
                $data = [];
                return $this->format($data);
            }
            $data = [
                'store_pay_qr' => url('/qr?no=' . $qrListInfo->code_num),
                'store_id' => $store_id,
                'ways_type' => $ways_type
            ];
            return $this->format($data);
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    public function store_openid_qr(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');

            $this->status = 1;
            $this->message = '数据返回成功';
            $store = Store::where('store_id', $store_id)->select('openid')->first();
            $data = [
                'store_openid_qr' => url('/openidQr?store_id=' . $store_id),
                'store_id' => $store_id,
            ];
            if (empty($store->openid)) {
                $data['openid'] = '暂未获取，请联系门店管理员获取';
            } else {
                $data['openid'] = $store->openid;
            }
            return $this->format($data);
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    public function couponQr(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $template_id = $request->get('template_id', '');
            $this->status = 1;
            $this->message = '数据返回成功';
            $data = [
                'store_pay_qr' => url('/cr?store_id=' . $store_id . '&template_id=' . $template_id),
                'store_id' => $store_id,
            ];

            return $this->format($data);
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }


    /**
     * 支付宝授权
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function alipay_auth(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id');
            $store = Store::where('store_id', $store_id)
                ->select('id', 'merchant_id', 'config_id')
                ->first();
            if (!$store) {
                return [
                    'status' => 2,
                    'message' => '门店不存在',
                ];
            }


            //第一次授权
            $AlipayAppOauthUsers = AlipayAppOauthUsers::where('store_id', $store_id)
                ->select('id')
                ->first();

            $user = User::where('id', $user->user_id)->first();
//            if ($AlipayAppOauthUsers) {
//                //看下是否有修改权限
//                $hasPermission = $user->hasPermissionTo('支付宝授权');
//
//                if (!$hasPermission) {
//                    return json_encode(['status' => 2, 'message' => '没有权限']);
//                }
//            } else {
//                //看下是否有添加权限
//                $hasPermission = $user->hasPermissionTo('支付宝授权修改');
//                if (!$hasPermission) {
//                    return json_encode(['status' => 2, 'message' => '没有权限修改']);
//                }
//            }

            $data = [
                'redirect_url' => 'alipays://platformapi/startapp?appId=20000067&url=' . url('/merchant/appAlipay?store_id=' . $store_id . '&merchant_id=' . $store->merchant_id . "&config_id=" . $store->config_id),
                'qr_url' => url('/merchant/appAlipay?store_id=' . $store_id . '&merchant_id=' . $store->merchant_id . "&config_id=" . $store->config_id)
            ];
            $this->status = 1;
            $this->message = '数据请求成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }

    //审核APP门店
    public function check_appStore(Request $request)
    {
        try {
            $store_id = $request->get('store_id', '');
            $status = $request->get('status', '0'); //0 新增门店中 1 审核通过，2  待审核  3审核拒绝
            $status_desc = "";

            if ($status == '0') {
                return json_encode([
                    'status' => -1,
                    'message' => '门店进行中，不允许审核'
                ]);
            }
            if ($status == '1') {
                $status_desc = "审核通过";
            } else if ($status == '2') {
                $status_desc = "待审核";
            } else if ($status == '3') {
                $status_desc = "审核拒绝";
            }

            $Store = Store::where('store_id', $store_id)->first();
            if (!$Store) {
                return json_encode([
                    'status' => -1,
                    'message' => '门店不存在'
                ]);
            }

            if ($Store->status == '1') { //已审核通过
                return json_encode([
                    'status' => 1,
                    'message' => '已审核通过'
                ]);
            } else {
                //开启事务
                DB::beginTransaction();

                $Store->update([
                    'store_id' => $store_id,
                    'status' => $status,
                    'status_desc' => $status_desc,
                    'admin_status' => $status,
                    'admin_status_desc' => $status_desc,
                ]);
                $Store->save();

                //同时提交对应通道去审核
                if ($status == '1') { //审核通过
                    $ways = StorePayWay::where('store_id', $store_id)->where('sort', 0)->where('status', 0)->first();
                    if ($ways) {
                        $type = $ways->ways_type;
                        $code = '';
                        $SettleModeType = '';//结算方式

                        $spwObj = new StorePayWaysController();
                        $spwObj->base_open_ways($type, $code, $store_id, $SettleModeType, '', '');
                    }
                }
                DB::commit();

                return json_encode([
                    'status' => 1,
                    'message' => '操作成功'
                ]);
            }

        } catch (\Exception $exception) {
            DB::rollBack();
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile() . $exception->getLine()
            ]);
        }
    }

    //审核门店
    public function check_store(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $status = $request->get('status', '1');
            $status_desc = $request->get('status_desc', '');

            $Store = Store::where('store_id', $store_id)->first();
            if (!$Store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在'
                ]);
            }

            //todo:待优化判断
//            //是否有审核
//            $UserStoreSet = UserStoreSet::where('user_id', 0)->first();
//            if ($UserStoreSet) {
//                //需要服务商审核
//                if ($UserStoreSet && $UserStoreSet->status_check == 1 && $Store->status != 1) {
//                    $message = "服务商未审核商户";
//                    if ($Store->status == 3) {
//                        $message = '审核失败：' . $Store->status_desc;
//                    }
//                    return json_encode([
//                        'status' => 2,
//                        'message' => $message
//                    ]);
//                }
//
//                //需要平台审核
//                if ($UserStoreSet && $UserStoreSet->admin_status_check == 1 && $Store->admin_status != 1) {
//                    $message = "平台未审核商户";
//                    if ($Store->status == 3) {
//                        $message = '审核失败：' . $Store->admin_status_desc;
//                    }
//                    return json_encode([
//                        'status' => 2,
//                        'message' => $message
//                    ]);
//                }
//            } else {
//                $UserStoreSet = UserStoreSet::where('user_id', $Store->user_id)->first();
//                //需要服务商审核
//                if ($UserStoreSet && $UserStoreSet->status_check == 1 && $Store->status != 1) {
//                    $message = "服务商未审核商户";
//                    if ($Store->status == 3) {
//                        $message = '审核失败：' . $Store->status_desc;
//                    }
//                    return json_encode([
//                        'status' => 2,
//                        'message' => $message
//                    ]);
//                }
//
//                //需要平台审核
//                if ($UserStoreSet && $UserStoreSet->admin_status_check == 1 && $Store->admin_status != 1) {
//                    $message = "平台未审核商户";
//                    if ($Store->status == 3) {
//                        $message = '审核失败：' . $Store->admin_status_desc;
//                    }
//                    return json_encode([
//                        'status' => 2,
//                        'message' => $message
//                    ]);
//                }
//            }
//            if ($Store->status == 1) {
//                return json_encode([
//                    'status' => 1,
//                    'message' => '已审核成功，请勿重复操作',
//                    'data' => $request->except(['token'])
//                ]);
//            }

            //地址转经纬度
            if (env('LBS_KEY')) {
                if (isset($Store->city_name) && isset($Store->city_code)) {
                    $address = '';
                    if (isset($Store->province_name) && isset($Store->province_code)) {
                        $address = $Store->province_name;
                    }
                    if (isset($Store->city_name) && isset($Store->city_code)) {
                        $address .= $Store->city_name;
                    }
                    if (isset($Store->area_name) && isset($Store->area_code)) {
                        $address .= $Store->area_name;
                    }
                    if ($address) {
                        $address .= $Store->store_address;
                        $address = trim($address);
                        $api_res = $this->query_address($address, env('LBS_KEY'));
                        if ($api_res['status'] == '1') {
                            $store_lng = $api_res['result']['lng'];
                            $store_lat = $api_res['result']['lat'];
                            $Store->update([
                                'lng' => $store_lng,
                                'lat' => $store_lat,
                            ]);
                            $Store->save();
                        } else {
//                            Log::info('添加门店-获取经纬度错误');
//                            Log::info($api_res['message']);
                        }
                    }
                }
            }

            if ($status == '1') {
                $status_desc = "审核成功";
            }

            $Store->status = $status;
            $Store->status_desc = $status_desc;
            if ($user->level == 0) {
                $Store->admin_status = $status;
                $Store->admin_status_desc = $status_desc;
            }

            $result = $Store->save();
            if ($result) {
                $config_id = $Store->config_id;
                $phone = $Store->people_phone;
                //审核通过后，发送短信息通知
                $config = SmsConfig::where('type', '8')->where('config_id', $config_id)->first();
                if ($config && $config->app_key && $phone) {
                    $phone = trim($phone);
//                    $salt_phone = substr($phone, 0, 3).'****'.substr($phone, 7); //手机号加盐
                    $sms_data = [
                        'Manage' => $phone, //商户管理账号
                        'password' => '000000'
                    ];
                    $sms_result = $this->sendSms($phone, $config->app_key, $config->app_secret, $config->SignName, $config->TemplateCode, $sms_data);
                    if ($sms_result->Code == 'OK') {
//                        Log::info('门店审核通过，'.$phone.'发送短信成功');
                    } else {
//                        Log::info('门店审核通过，'.$phone.'发送短信失败: '.$sms_result->Message);
                    }
                }

                return json_encode([
                    'status' => 1,
                    'message' => '门店状态更改成功',
                    'data' => $request->except(['token'])
                ]);
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '门店状态更改失败',
                    'data' => $request->except(['token'])
                ]);
            }
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile() . $exception->getLine()
            ]);
        }
    }


    //删除门店
    public function del_store(Request $request)
    {
        try {
            $token = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $branch_store_type = $request->get('type', '');  //伪删除分店传2
            $store_ids = explode(',', $store_id);

            foreach ($store_ids as $k => $v) {
                $Store = Store::where('store_id', $v)->first();
                if (!$Store) {
                    return json_encode(['status' => 2, 'message' => '门店不存在']);
                }
                //有分店不允许删除
                $sub_Store = Store::where('pid', $Store->id)->first();
                if ($sub_Store) {
                    return json_encode(['status' => 2, 'message' => $Store->store_name . "有分店不允许删除请先删除分店"]);
                }
            }
            foreach ($store_ids as $k => $v) {
                //门店不存在
                $Store = Store::where('store_id', $v)->first();
                if (!$Store) {
                    continue;
                }

                //有分店不允许删除
                $sub_Store = Store::where('pid', $Store->id)->first();
                if ($sub_Store && empty($branch_store_type)) {
                    continue;
                }
                if ($sub_Store) {
                    $merchant_id = $sub_Store->merchant_id;
                } else {
                    $merchant_id = '0';
                }

                //不是自己或者不是下级的商户
                $users = $this->getSubIdsAll($token->user_id); //获取所有下级
                if (!in_array($Store->user_id, $users)) {
                    continue;
                }

                //假删除
                Store::where('store_id', $v)->update([
                    'is_delete' => 1,
                ]);

//                $order = Order::where('store_id', $v)
//                    ->select('id')
//                    ->first();
//                if (!$order) {
//                    //彻底删除门店
//                    try {
//                        DB::beginTransaction(); //开启事务
//
//                        $merchant_store_obj = MerchantStore::where('store_id', $v)->get();
//
//                        if ($merchant_store_obj) {
//                            $merchant_store_arr = $merchant_store_obj->toArray();
//
//                            if (is_array($merchant_store_arr) && $merchant_store_arr) {
//                                foreach ($merchant_store_arr as $val) {
//                                    if ($val['merchant_id'] != $merchant_id) {
//                                        Merchant::find($val['merchant_id'])->delete();
//                                    }
//
//                                }
//                            }
//                        }
//                        Merchant::where('id', $Store->merchant_id)->delete();
//                        Store::where('store_id', $v)->delete();
//                        StorePayWay::where('store_id', $v)->delete();
//                        MerchantStore::where('store_id', $v)->delete();
//                        Device::where('store_id', $v)->delete();
//                        StoreImg::where('store_id', $v)->delete();
//                        StoreBank::where('store_id', $v)->delete();
//                        //删除易生电子协议
//                        EasyPayContractInfo::where('store_id', $v)->delete();
//
//                        DB::commit();
//                    } catch (\Exception $ex) {
//                        DB::rollBack();
//                        continue;
//                    }
//                } else {
//                    Store::where('store_id', $v)->update([
//                        'is_delete' => 1,
//                    ]);
//                }

            }
            return json_encode([
                'status' => 1,
                'message' => '门店删除成功',
                'data' => [
                    'store_id' => $store_id,
                ]
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine()
            ]);
        }
    }


    //恢复门店
    public function rec_store(Request $request)
    {
        try {
            $token = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $store_ids = explode(',', $store_id);

//            $user = User::where('id', $token->user_id)->first();
//            $hasPermission = $user->hasPermissionTo('恢复门店');
//
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '没有权限']);
//            }

            foreach ($store_ids as $k => $v) {
                //门店不存在
                $Store = Store::where('store_id', $v)->first();
                if (!$Store) {
                    continue;
                }

                //有分店不允许删除
                $sub_Store = Store::where('pid', $Store->id)->first();
                if ($sub_Store) {
                    continue;
                }

                //不是自己或者不是下级的商户
                $users = $this->getSubIdsAll($token->user_id); //获取所有下级
                if (!in_array($Store->user_id, $users)) {
                    continue;
                }

                Store::where('store_id', $v)->update([
                    'is_delete' => 0,
                ]);
            }

            return json_encode([
                'status' => 1,
                'message' => '门店恢复成功',
                'data' => [
                    'store_id' => $store_id,
                ]
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile()
            ]);
        }
    }


    //直接清除门店
    public function clear_store(Request $request)
    {
        try {
            $token = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $store_ids = explode(',', $store_id);

            foreach ($store_ids as $k => $v) {
                //门店不存在
                $Store = Store::where('store_id', $v)
                    ->select('is_delete')
                    ->first();
                if (!$Store) {
                    continue;
                }

                //有分店不允许删除
                $sub_Store = Store::where('pid', $Store->id)
                    ->select('id')
                    ->first();
                if ($sub_Store) {
                    continue;
                }

                //有设备清除不了
                $Device = Device::where('store_id', $v)->select('id')->first();
                if ($Device) {
                    continue;
                }

                //彻底删除门店
                //开启事务
                $store = Store::where('store_id', $v)->select('merchant_id')->first();
                Store::where('store_id', $v)->delete();
                StorePayWay::where('store_id', $v)->delete();
                MerchantStore::where('store_id', $v)->delete();
                Device::where('store_id', $v)->delete();
                StoreImg::where('store_id', $v)->delete();
                StoreBank::where('store_id', $v)->delete();
                Merchant::where('id', $store->merchant_id)->delete();
                //删除易生电子协议
                EasyPayContractInfo::where('store_id', $v)->delete();
//                try {
//                    DB::beginTransaction();
//                    $store = Store::where('sotre_id', $v)->select('merchant_id')->first();
//                    Store::where('store_id', $v)->delete();
//                    StorePayWay::where('store_id', $v)->delete();
//                    MerchantStore::where('store_id', $v)->delete();
//                    Device::where('store_id', $v)->delete();
//                    StoreImg::where('store_id', $v)->delete();
//                    StoreBank::where('store_id', $v)->delete();
//                    Merchant::where('id', $store->merchant_id)->delete();
//                    //删除易生电子协议
//                    EasyPayContractInfo::where('store_id', $v)->delete();
//
//                    DB::commit();
//                } catch (\Exception $e) {
//                    DB::rollBack();
//                    continue;
//                }
            }
            return json_encode([
                'status' => 1,
                'message' => '门店删除成功',
                'data' => [
                    'store_id' => $store_id,
                ]
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile()
            ]);
        }
    }


    //门店关闭接口
    public function col_store(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $store_ids = explode(',', $store_id);

            foreach ($store_ids as $k => $v) {
                $Store = Store::where('store_id', $v)->first();
                //门店不存在
                if (!$Store) {
                    continue;
                }
                $Store->is_close = 1;
                $Store->save();
            }
            return json_encode([
                'status' => 1,
                'message' => '门店关闭成功',
                'data' => [
                    'store_id' => $store_id,
                ]
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile()
            ]);
        }
    }


    //门店开启接口
    public function ope_store(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $store_ids = explode(',', $store_id);

            //目前只有平台可以删除
            if ($user->level > 0) {
                return json_encode([
                    'status' => 2,
                    'message' => '暂时不支持开启门店'
                ]);
            }

            foreach ($store_ids as $k => $v) {
                $Store = Store::where('store_id', $v)->first();
                //门店不存在
                if (!$Store) {
                    continue;
                }
                $Store->is_close = 0;
                $Store->save();
            }
            return json_encode([
                'status' => 1,
                'message' => '门店开启成功',
                'data' => [
                    'store_id' => $store_id,
                ]
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile()
            ]);
        }
    }


    //门店转移
    public function update_user(Request $request)
    {
        try {
            $public = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $user_id = $request->get('user_id', '');

            $store_ids = explode(',', $store_id);
            $user = User::where('id', $user_id)->first();//要转出的人
            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '用户不存在'
                ]);
            }
            $config_id = $user->config_id;
            foreach ($store_ids as $k => $v) {
                $Store = Store::where('store_id', $v)
                    ->select('user_id', 'config_id', 'id')
                    ->first();

                //门店不存在
                if (!$Store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店不存在'
                    ]);
                }

                if ($public->pid > 1 & $config_id != $Store->config_id) {
                    return json_encode([
                        'status' => 2,
                        'message' => '门店不支持转移到其他服务商下面-config_id'
                    ]);
                }


                //开启事务
                try {
                    DB::beginTransaction();
                    //门店转移
                    Store::where('store_id', $v)->update(['user_id' => $user_id]);
                    //分店也转移
                    Store::where('pid', $Store->id)->update(['user_id' => $user_id]);

                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollBack();
                    continue;
                }
                Cache::forget($v . 'qr-pay');//清除商户缓存(扫码支付)
            }

            return json_encode([
                'status' => 1,
                'message' => '门店转移成功',
                'data' => [
                    'store_id' => $store_id,
                ]
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile()
            ]);
        }
    }


    public function time($time)
    {
        try {

            //去除中文
            $time = preg_replace('/([\x80-\xff]*)/i', '', $time);

            $is_date = strtotime($time) ? strtotime($time) : false;

            if ($is_date) {
                $time = date('Y-m-d', strtotime($time));
            }

            return $time;
        } catch (\Exception $ex) {
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getFile());
        }

        return $time;
    }


    //汇付-获取商户信息接口
    public function get_mer_id(Request $request)
    {
        try {
            $store_id = $request->get('store_id', '');
            $ways_type = $request->get('ways_type', '');
            $token = $this->parseToken();

            if (17999 < $ways_type && $ways_type < 18999) {
                $store_info = Store::where('store_id', $store_id)
                    ->where('is_close', '0')
                    ->where('is_delete', '0')
                    ->first();
                if (!$store_info) {
                    return json_encode([
                        'status' => '2',
                        'message' => '门店不存在或状态异常'
                    ]);
                }

                $store_pid = $store_info->pid ?? '';

                $hui_pay_config_obj = new \App\Api\Controllers\Config\HuiPayConfigController();
                $hui_pay_config = $hui_pay_config_obj->hui_pay_config($store_info->config_id);
                if (!$hui_pay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '汇付配置不存在'
                    ]);
                }

                $hui_pay_store = $hui_pay_config_obj->hui_pay_merchant($store_id, $store_pid);
                if (!$hui_pay_store) {
                    return json_encode([
                        'status' => '2',
                        'message' => '汇付商户不存在'
                    ]);
                }

                $hui_pay_base_obj = new \App\Api\Controllers\HuiPay\BaseController();

                $user_cust_id = $hui_pay_store->user_cust_id;
                if (!$user_cust_id) {
                    return json_encode([
                        'status' => '2',
                        'message' => '汇付用户客户号不存在'
                    ]);
                }

                $mer_id = $hui_pay_store->mer_id;  //商户号
                if ($mer_id) {
                    $data = [
                        'mer_id' => $mer_id,
                        'device_id' => $hui_pay_store->device_id ?? ''
                    ];
                } else {  //商户号不存在就去查
                    $sign_data = ['user_cust_id' => $user_cust_id];
                    $post_data = [
                        'data' => $sign_data,
                        'sign_type' => 'RSA2',
                        'mer_cust_id' => $hui_pay_config->mer_cust_id,
                        'source_num' => $hui_pay_config->org_id
                    ];
                    $result = $hui_pay_base_obj->execute($hui_pay_base_obj->store_detail, $post_data, $sign_data, $hui_pay_config->private_key, $hui_pay_config->public_key);
//                    var_dump($result);die;
                    if ($result['status'] == '000000' && isset($result['data']) && !empty($result['data'])) {
                        $data_json = json_decode($result['data'], true);
                        $data = [
                            'mer_id' => $data_json['mer_id']
                        ];

                        $hui_pay_store->update($data);
                    } else {
                        return json_encode([
                            'status' => '2',
                            'message' => $result['message']
                        ]);
                    }
                }
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '支付类型参数不对'
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => $ex->getCode(),
                'message' => $ex->getMessage()
            ]);
        }

        $this->status = '1';
        $this->message = '数据返回成功';
        return $this->format($data);
    }


    //汇付-门店绑定机具
    public function hui_pay_bind(Request $request)
    {
        try {
//            var_dump($request->all());die;
            $token = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $ways_type = $request->get('ways_type', '');
            $device_id = $request->get('device_id', ''); //汇付机具id

            if (17999 < $ways_type && $ways_type < 18999) {
                $store_info = Store::where('store_id', $store_id)
                    ->where('is_close', '0')
                    ->where('is_delete', '0')
                    ->first();
                if (!$store_info) {
                    return json_encode([
                        'status' => '2',
                        'message' => '门店不存在或状态异常'
                    ]);
                }

//                $store_pid = $store_info->pid ?? '';

                $hui_pay_config_obj = new HuiPayConfigController();
                $hui_pay_config = $hui_pay_config_obj->hui_pay_config($store_info->config_id);
                if (!$hui_pay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '汇付配置不存在'
                    ]);
                }

                $hui_pay_store = $hui_pay_config_obj->hui_pay_merchant($store_id, '');
                if (!$hui_pay_store) {
                    return json_encode([
                        'status' => '2',
                        'message' => '汇付商户不存在'
                    ]);
                }

                $hui_pay_base_obj = new \App\Api\Controllers\HuiPay\BaseController();

                $user_cust_id = $hui_pay_store->user_cust_id;
                if (!$user_cust_id) {
                    return json_encode([
                        'status' => '2',
                        'message' => '汇付用户客户号不存在'
                    ]);
                }

                $shop_id = $hui_pay_store->shop_id;  //绑定门店id
                if (!$shop_id) {
                    return json_encode([
                        'status' => '2',
                        'message' => '汇付绑定门店id不存在'
                    ]);
                }

                $huipay_device_id = $hui_pay_store->device_id;
                //门店是否绑定设备
                if ($huipay_device_id && ($huipay_device_id != $device_id)) {
                    //表中已存在机具id且和新id不同解绑
                    $sign_data = [
                        'device_id' => $device_id,  //必须，设备号
                        'shop_id' => $shop_id,     //必须，绑定门店id
                    ];
                    $post_data = [
                        'data' => $sign_data,
                        'sign_type' => 'RSA2',
                        'mer_cust_id' => $hui_pay_config->mer_cust_id,
                        'source_num' => $hui_pay_config->org_id
                    ];
                    $result = $hui_pay_base_obj->execute($hui_pay_base_obj->device_unbind, $post_data, $sign_data, $hui_pay_config->private_key, $hui_pay_config->public_key);
                    if ($result['status'] == '0') {
                        $this->status = '0';
                        $this->message = '解绑验签失败';
                        return $this->format();
                    }
//                    var_dump($result);die;
                    if ($result['status'] == '000000') {
                        //解绑成功后，有新的机具id就绑定
                        if ($device_id) {
                            $bind_sign_data = [
                                'device_id' => $device_id,  //必须，设备号
                                'device_name' => '汇付机具',     //必须，设备名称
                                'shop_id' => $shop_id,     //必须，绑定门店id
                            ];
                            $bind_post_data = [
                                'data' => $bind_sign_data,
                                'sign_type' => 'RSA2',
                                'mer_cust_id' => $hui_pay_config->mer_cust_id,
                                'source_num' => $hui_pay_config->org_id
                            ];
                            $bind_result = $hui_pay_base_obj->execute($hui_pay_base_obj->device_bind, $bind_post_data, $bind_sign_data, $hui_pay_config->private_key, $hui_pay_config->public_key);
//                    var_dump($bind_result);die;
                            if ($bind_result['status'] == '0') {
                                return json_encode([
                                    'status' => '0',
                                    'message' => '更改机具绑定验签失败'
                                ]);
                            }
                            if ($bind_result['status'] == '000000') {
                                $this->status = '1';
                                $this->message = '汇付机具重新绑定成功';
                                return $this->format();
                            } else {
                                return json_encode([
                                    'status' => '2',
                                    'message' => $bind_result['message']
                                ]);
                            }
                        }
                    } else {
                        //解绑失败
                        return json_encode([
                            'status' => '2',
                            'message' => $result['message']
                        ]);
                    }
                } else {
                    //表中没有机具id，绑定新的机具id
                    if ($device_id) {
                        $bind_sign_data = [
                            'device_id' => $device_id,  //必须，设备号
                            'device_name' => '汇付机具',     //必须，设备名称
                            'shop_id' => $shop_id,     //必须，绑定门店id
                        ];
                        $bind_post_data = [
                            'data' => $bind_sign_data,
                            'sign_type' => 'RSA2',
                            'mer_cust_id' => $hui_pay_config->mer_cust_id,
                            'source_num' => $hui_pay_config->org_id
                        ];
                        $bind_result = $hui_pay_base_obj->execute($hui_pay_base_obj->device_bind, $bind_post_data, $bind_sign_data, $hui_pay_config->private_key, $hui_pay_config->public_key);
//                    var_dump($bind_result);die;
                        if ($bind_result['status'] == '0') {
                            return json_encode([
                                'stauts' => '0',
                                'message' => '绑定机具验签失败'
                            ]);
                        }
                        if ($bind_result['status'] == '000000') {
                            $this->status = '1';
                            $this->message = '汇付机具绑定成功';
                            return $this->format();
                        } else {
                            return json_encode([
                                'status' => '2',
                                'message' => $bind_result['message']
                            ]);
                        }
                    }
                }
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '通道类型参数不对'
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => $ex->getCode(),
                'message' => $ex->getMessage()
            ]);
        }
    }


    public function sendSms($phone, $app_key, $app_secret, $SignName, $TemplateCode, $data)
    {
        $demo = new AliSms($app_key, $app_secret);
        $response = $demo->sendSms(
            $SignName, // 短信签名
            $TemplateCode, // 短信模板编号
            $phone, // 短信接收者
            $data
        );

        return $response;
    }


    /**
     * 通过地址解析获得经纬度
     * @param string $key_words 地址（注：地址中请包含城市名称，否则会影响解析效果）
     * @param string $key 开发密钥（Key）
     * @return array
     */
    public function query_address($key_words, $key)
    {
        $url = "https://apis.map.qq.com/ws/geocoder/v1/?key=" . $key . "&address=" . $key_words;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $output = curl_exec($ch);

        $errmsg = '';
        if (curl_errno($ch)) {
            $errmsg = '腾讯地图api请求错误：' . curl_error($ch);
        }

        curl_close($ch); //释放curl句柄

        if ($errmsg != '') {
//            Log::info($errmsg);
        }

        $res = json_decode($output, true);
        if ($res['status'] == '0') {
            $data = [
                'status' => '1',
                'result' => $res['result']['location']
            ];
        } else {
            $data = [
                'status' => '0',
                'message' => $res['message']
            ];
        }

        return $data;
    }


    //哆啦宝-获取相关信息
    public function dlb_store_info(Request $request)
    {
        try {
            $token = $this->parseToken();
            $store_id = $request->get('store_id', ''); //门店id
            $select_type = $request->get('select_type', ''); //查询数据类型
            $industry_num = $request->get('industry_num', ''); //店铺一级行业code
            $province_code = $request->get('province_code', ''); //省份code
            $bank_keywords = $request->get('bank_keywords', ''); //银行关键字
            $bank_code = $request->get('bank_code', ''); //银行code
            $sub_branch_keywords = $request->get('sub_branch_keywords', ''); //支行关键字

            $store_info = Store::where('store_id', $store_id)
                ->where('is_close', '0')
                ->where('is_delete', '0')
                ->first();
            if (!$store_info) {
                return json_encode([
                    'status' => '2',
                    'message' => '门店不存在或状态异常'
                ]);
            }

            $config_id = $store_info->config_id;
            $timestamp = time();

            $manager = new \App\Api\Controllers\DuoLaBao\ManageController();
            $dlb_config = $manager->pay_config($config_id);
            if (!$dlb_config) {
                return json_encode([
                    'status' => 2,
                    'message' => '哆啦宝配置配置不存在请检查配置'
                ]);
            }

            //店铺一级行业
            if ($select_type == '1') {
                $dlb_primary_industry = Cache::get('primary_industry');
                if (!$dlb_primary_industry) {
                    $dlb_token = $manager->dlb_sign($timestamp, $manager->dlb_category, '', $dlb_config->secret_key);
                    $dlb_header = array(
                        "Content-Type" => "application/json",
                        "accessKey" => $dlb_config->access_key,
                        "timestamp" => $timestamp,
                        "token" => $dlb_token,
                    );
                    $dlb_result = $manager->post_func($manager->dlb_host_url . $manager->dlb_category, '', $dlb_header, '10', '1');
                    if ($dlb_result['status'] == '1') {
                        $result = json_decode($dlb_result['data'], true);
                        if ($result['result'] == 'error') {
                            return json_encode([
                                'status' => 2,
                                'message' => $result['error']['errorCode']
                            ]);
                        }

                        Cache::put('primary_industry', $result['data']['industryList'], 1000000);

                        return json_encode([
                            'status' => 1,
                            'data' => $result['data']['industryList'],
                            'message' => '获取成功'
                        ]);
                    } else {
                        return json_encode([
                            'status' => 0,
                            'data' => [],
                            'message' => $dlb_result['message']
                        ]);
                    }
                }

                return json_encode([
                    'status' => 1,
                    'data' => $dlb_primary_industry,
                    'message' => '获取成功'
                ]);
            }

            //店铺二级行业
            if ($select_type == '2' && $industry_num) {
                $dlb_second_industry = Cache::get('second_industry_' . $industry_num);
                if (!$dlb_second_industry) {
                    $dlb_token = $manager->dlb_sign($timestamp, $manager->dlb_second_category . $industry_num, '', $dlb_config->secret_key);
                    $dlb_header = array(
                        "Content-Type" => "application/json",
                        "accessKey" => $dlb_config->access_key,
                        "timestamp" => $timestamp,
                        "token" => $dlb_token,
                    );
                    $dlb_result = $manager->post_func($manager->dlb_host_url . $manager->dlb_second_category . $industry_num, '', $dlb_header, '10', '1');
                    if ($dlb_result['status'] == '1') {
                        $result = json_decode($dlb_result['data'], true);
                        if ($result['result'] == 'error') {
                            return json_encode([
                                'status' => 2,
                                'message' => $result['error']['errorCode']
                            ]);
                        }

                        Cache::put('second_industry_' . $industry_num, $result['data']['industryList'], 1000000);

                        return json_encode([
                            'status' => 1,
                            'data' => $result['data']['industryList'],
                            'message' => '获取成功'
                        ]);
                    } else {
                        return json_encode([
                            'status' => 0,
                            'data' => [],
                            'message' => $dlb_result['message']
                        ]);
                    }
                }

                return json_encode([
                    'status' => 1,
                    'data' => $dlb_second_industry,
                    'message' => '获取成功'
                ]);
            }

            //省份
            if ($select_type == '3') {
                $dlb_province = Cache::get('dlb_province');
                if (!$dlb_province) {
                    //哆啦宝省份数据
                    $dlb_token = $manager->dlb_sign($timestamp, $manager->dlb_province, '', $dlb_config->secret_key);
                    $dlb_header = array(
                        "Content-Type" => "application/json",
                        "accessKey" => $dlb_config->access_key,
                        "timestamp" => $timestamp,
                        "token" => $dlb_token,
                    );
                    $dlb_result = $manager->post_func($manager->dlb_host_url . $manager->dlb_province, '', $dlb_header, '10', '1');
                    if ($dlb_result['status'] == '1') {
                        $result = json_decode($dlb_result['data'], true);
                        if ($result['result'] == 'error') {
                            return json_encode([
                                'status' => 2,
                                'message' => $result['error']['errorCode']
                            ]);
                        }

                        Cache::put('dlb_province', $result['data']['addressList'], 1000000);

                        return json_encode([
                            'status' => 1,
                            'message' => '数据请求成功',
                            'data' => $result['data']['addressList']
                        ]);
                    } else {
                        return json_encode([
                            'status' => 0,
                            'message' => $dlb_result['message']
                        ]);
                    }
                }

                return json_encode([
                    'status' => 1,
                    'message' => '数据请求成功',
                    'data' => $dlb_province
                ]);
            }

            //城市
            if ($select_type == '4' && $province_code) {
                $dlb_city = Cache::get('dlb_city_' . $province_code);
                if (!$dlb_city) {
                    //哆啦宝城市数据
                    $dlb_token = $manager->dlb_sign($timestamp, $manager->dlb_city . $province_code, '', $dlb_config->secret_key);
                    $dlb_header = array(
                        "Content-Type" => "application/json",
                        "accessKey" => $dlb_config->access_key,
                        "timestamp" => $timestamp,
                        "token" => $dlb_token,
                    );
                    $dlb_result = $manager->post_func($manager->dlb_host_url . $manager->dlb_city . $province_code, '', $dlb_header, '10', '1');
                    //                var_dump($dlb_result);die;
                    if ($dlb_result['status'] == '1') {
                        $result = json_decode($dlb_result['data'], true);
                        if ($result['result'] == 'error') {
                            return json_encode([
                                'status' => 2,
                                'message' => $result['error']['errorCode']
                            ]);
                        }

                        Cache::put('dlb_city_' . $province_code, $result['data']['addressList'], 1000000);

                        return json_encode([
                            'status' => 1,
                            'data' => $result['data']['addressList']
                        ]);
                    } else {
                        return json_encode([
                            'status' => 0,
                            'message' => $dlb_result['message']
                        ]);
                    }
                }

                return json_encode([
                    'status' => 1,
                    'data' => $dlb_city
                ]);
            }

            //银行名称
            if ($select_type == '5' && $bank_keywords) {
                $dlb_bank_info = Cache::get('dlb_bank_info' . $bank_keywords);
                if (!$dlb_bank_info) {
                    //哆啦宝银行名称
                    $new_bank_keywords = urlencode($bank_keywords);
                    $dlb_token = $manager->dlb_sign($timestamp, $manager->dlb_bank_list . $bank_keywords, '', $dlb_config->secret_key);
                    $dlb_header = array(
                        "Content-Type" => "application/json",
                        "accessKey" => $dlb_config->access_key,
                        "timestamp" => $timestamp,
                        "token" => $dlb_token,
                    );
                    $dlb_result = $manager->post_func($manager->dlb_host_url . $manager->dlb_bank_list . $new_bank_keywords, '', $dlb_header, '10', '1');
//                    var_dump($dlb_result);die;
                    if ($dlb_result['status'] == '1') {
                        $result = json_decode($dlb_result['data'], true);
                        if ($result['result'] == 'error') {
                            return json_encode([
                                'status' => 2,
                                'message' => $result['error']['errorCode']
                            ]);
                        }

                        Cache::put('dlb_bank_info' . $bank_keywords, $result['data']['bankList'], 1000000);

                        return json_encode([
                            'status' => 1,
                            'data' => $result['data']['bankList']
                        ]);
                    } else {
                        return json_encode([
                            'status' => 0,
                            'message' => $dlb_result['message']
                        ]);
                    }
                }

                return json_encode([
                    'status' => 1,
                    'data' => $dlb_bank_info
                ]);
            }

            //银行分行名称
            if ($select_type == '6' && $bank_code && $sub_branch_keywords) {
                $dlb_sub_branch_bank = Cache::get('dlb_sub_branch_bank' . $bank_code . $sub_branch_keywords);
                if (!$dlb_sub_branch_bank) {
                    //哆啦宝-银行分行名称
                    $new_sub_branch_keywords = urlencode($sub_branch_keywords);
                    $dlb_token = $manager->dlb_sign($timestamp, $manager->dlb_sub_bank_list . $bank_code . '/' . $sub_branch_keywords, '', $dlb_config->secret_key);
                    $dlb_header = array(
                        "Content-Type" => "application/json",
                        "accessKey" => $dlb_config->access_key,
                        "timestamp" => $timestamp,
                        "token" => $dlb_token,
                    );
                    $dlb_result = $manager->post_func($manager->dlb_host_url . $manager->dlb_sub_bank_list . $bank_code . '/' . $new_sub_branch_keywords, '', $dlb_header, '10', '1');
//                    var_dump($dlb_result);die;
                    if ($dlb_result['status'] == '1') {
                        $result = json_decode($dlb_result['data'], true);
                        if ($result['result'] == 'error') {
                            return json_encode([
                                'status' => 2,
                                'message' => $result['error']['errorCode']
                            ]);
                        }

                        Cache::put('dlb_sub_branch_bank' . $bank_code . $sub_branch_keywords, $result['data']['bankSubList'], 1000000);

                        return json_encode([
                            'status' => 1,
                            'data' => $result['data']['bankSubList']
                        ]);
                    } else {
                        return json_encode([
                            'status' => 0,
                            'message' => $dlb_result['message']
                        ]);
                    }
                }

                return json_encode([
                    'status' => 1,
                    'data' => $dlb_sub_branch_bank
                ]);
            }

            //支付银行
            if ($select_type == '7') {
                $dlb_pay_bank_info = Cache::get('dlb_pay_bankinfo');
                if (!$dlb_pay_bank_info) {
                    //哆啦宝-支付银行
                    $dlb_token = $manager->dlb_sign($timestamp, $manager->dlb_pay_bankinfo, '', $dlb_config->secret_key);
                    $dlb_header = array(
                        "Content-Type" => "application/json",
                        "accessKey" => $dlb_config->access_key,
                        "timestamp" => $timestamp,
                        "token" => $dlb_token,
                    );
                    $dlb_result = $manager->post_func($manager->dlb_host_url . $manager->dlb_pay_bankinfo, '', $dlb_header, '10', '1');
//                    var_dump($dlb_result);die;
                    if ($dlb_result['status'] == '1') {
                        $result = json_decode($dlb_result['data'], true);
                        if ($result['result'] == 'error') {
                            return json_encode([
                                'status' => 2,
                                'message' => $result['error']['errorCode']
                            ]);
                        }

                        Cache::put('dlb_pay_bankinfo', $result['data']['payBankList'], 1000000);

                        return json_encode([
                            'status' => 1,
                            'data' => $result['data']['payBankList']
                        ]);
                    } else {
                        return json_encode([
                            'status' => 0,
                            'message' => $dlb_result['message']
                        ]);
                    }
                }

                return json_encode([
                    'status' => 1,
                    'data' => $dlb_pay_bank_info
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ]);
        }
    }


    /**
     * 判断是否为合法的身份证号码
     * @param string $vStr 身份证号码
     * @return int
     */
    protected function check_sfz($vStr)
    {
        $vCity = ['11', '12', '13', '14', '15', '21', '22', '23', '31', '32', '33', '34', '35', '36', '37', '41', '42', '43', '44', '45', '46', '50', '51', '52', '53', '54', '61', '62', '63', '64', '65', '71', '81', '82', '91'];

        if (!preg_match('/^([\d]{17}[xX\d]|[\d]{15})$/', $vStr)) {
            return false;
        }

        if (!in_array(substr($vStr, 0, 2), $vCity)) {
            return false;
        }

        $vStr = preg_replace('/[xX]$/i', 'a', $vStr);
        $vLength = strlen($vStr);
        if ($vLength == 18) {
            $vBirthday = substr($vStr, 6, 4) . '-' . substr($vStr, 10, 2) . '-' . substr($vStr, 12, 2);
        } else {
            $vBirthday = '19' . substr($vStr, 6, 2) . '-' . substr($vStr, 8, 2) . '-' . substr($vStr, 10, 2);
        }

        if (date('Y-m-d', strtotime($vBirthday)) != $vBirthday) {
            return false;
        }
        if ($vLength == 18) {
            $vSum = 0;
            for ($i = 17; $i >= 0; $i--) {
                $vSubStr = substr($vStr, 17 - $i, 1);
                $vSum += (pow(2, $i) % 11) * (($vSubStr == 'a') ? 10 : intval($vSubStr, 11));
            }
            if ($vSum % 11 != 1) {
                return false;
            }
        }

        return true;
    }


    //验证进件参数
    public function checkAddStoreInfo(Request $request)
    {
        try {
            $token = $this->parseToken();
            $people_phone = $request->get('people_phone', '');
            $store_id = $request->get('store_id', '');

            //用户是否存在
            $user_id = $token->user_id;
            $user_info = User::where('id', $user_id)
                ->where('is_delete', '0')
                ->first();
            if (!$user_info) {
                return json_encode([
                    'status' => '2',
                    'message' => '用户不存在或被禁用，请联系管理员'
                ]);
            }


            //判断手机号是否被注册
            if ($people_phone) {
                //验证手机号
                if (!preg_match("/^1[3456789]{1}\d{9}$/", $people_phone)) {
                    return json_encode([
                        'status' => '2',
                        'message' => '手机号码格式不正确'
                    ]);
                }

                $is_merchant = 0;
                $Merchant = Merchant::where('phone', $people_phone)
                    ->select('id')
                    ->first();
                if ($Merchant) {
                    $is_merchant = 1;
                }

                if ($is_merchant == 1) {
                    return json_encode([
                        'status' => '2',
                        'message' => '手机号已被商户注册,请更换'
                    ]);
                }

                return json_encode([
                    'status' => '1',
                    'message' => '手机号尚未被注册'
                ]);
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '手机号不能为空'
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine()
            ]);
        }
    }


    //海科融通 经营范围
    public function business_scope(Request $request)
    {
        try {
            $token = $this->parseToken();
            $store_id = $request->get('store_id', '');

            $hkrt_data = Cache::get('hkrt_business_scope');
            if (!$hkrt_data) {
                $hkrt_data = HkrtBusinessScope::select('code', 'name')->get();
                Cache::put('hkrt_business_scope', $hkrt_data, 1000000);
            }

            if ($hkrt_data) {
                $this->status = '1';
                $this->message = '返回数据成功';
                $this->data = $hkrt_data;
                return $this->format($hkrt_data);
            } else {
                return json_encode([
                    'status' => '0',
                    'message' => '',
                    'data' => []
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine()
            ]);
        }
    }


    //编辑门店信息 TODO:尚未完成
    public function update_store(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_id = $token->user_id;
            $data = $request->except(['token']);

            $is_send_tpl_mess = $request->post('isSendTplMess', ''); //是否发送模板消息(1-开启;2-关闭)
            $hkrt_bus_scope_code = $request->post('hkrt_bus_scope_code', ''); //海科融通经营范围
            $reserved_mobile = $request->post('reserved_mobile', ''); //预留手机号

            $store_id = $request->get('store_id', '');
            $config_id = $token->config_id;

            $user_id = $request->get('user_id', $user_id);

            $user = User::where('id', $user_id)->first();
            if (!$user) {
                returnjson_encode([
                    'status' => 2,
                    'message' => '用户不存在'
                ]);
            }

            //法人信息
            $people_phone = $request->get('people_phone', '');
            $store_email = $request->get('store_email', '');
            $head_name = $request->get('head_name', '');
            $head_sfz_no = $request->get('head_sfz_no', '');
            $bank_sfz_no = $request->get('bank_sfz_no', '');
            $people = $request->get('people', '');

            $head_sfz_img_a = $request->get('head_sfz_img_a', '');
            $head_sfz_img_b = $request->get('head_sfz_img_b', '');

            $head_sfz_time = $request->get('head_sfz_time', '');
            $head_sfz_stime = $request->get('head_sfz_stime', '');
            $head_sfz_time = $this->time($head_sfz_time);
            $head_sfz_stime = $this->time($head_sfz_stime);

            //门店信息
            $store_name = $request->get('store_name', '');
            $store_short_name = $request->get('store_short_name', $store_name);
            $province_code = $request->get('province_code', '');
            $city_code = $request->get('city_code', '');
            $area_code = $request->get('area_code', '');
            $store_address = $request->get('store_address', '');
            $store_type = $request->get('store_type', '');
            $store_type_name = $request->get('store_type_name', '');
            $dlb_micro_biz_type = $request->get('DLB_micro_biz_type', '');
            $dlb_industry = $request->get('DLB_industry', '');
            $dlb_second_industry = $request->get('DLB_second_industry', '');
            $dlb_province = $request->get('DLB_province', '');
            $dlb_city = $request->get('DLB_city', '');
            $dlb_bank = $request->get('DLB_bank', '');
            $dlb_sub_bank = $request->get('DLB_sub_bank', '');
            $dlb_pay_bank_list = $request->get('DLB_pay_bank_list', '');
            if ($dlb_pay_bank_list) {
                $dlb_pay_bank_list = json_encode($dlb_pay_bank_list);
            }

            $category_id = $request->get('category_id', '');
            $category_name = $request->get('category_name', '');
            $store_logo_img = $request->get('store_logo_img', '');
            $store_img_a = $request->get('store_img_a', '');
            $store_img_b = $request->get('store_img_b', '');
            $store_img_c = $request->get('store_img_c', '');
            $weixin_name = $request->get('weixin_name', '');
            $weixin_no = $request->get('weixin_no', '');
            $alipay_name = $request->get('alipay_name', '');
            $alipay_account = $request->get('alipay_account', '');

            //收款信息
            $store_alipay_account = $request->get('store_alipay_account', '');
            $store_bank_no = $request->get('store_bank_no', '');
            $store_bank_phone = $request->get('store_bank_phone', '');
            $store_bank_name = $request->get('store_bank_name', '');
            $store_bank_type = $request->get('store_bank_type', '');
            $bank_name = $request->get('bank_name', '');
            $bank_no = $request->get('bank_no', '');
            $sub_bank_name = $request->get('sub_bank_name', '');
            $bank_province_code = $request->get('bank_province_code', '');
            $bank_city_code = $request->get('bank_city_code', '');
            $bank_area_code = $request->get('bank_area_code', '');
            $bank_img_a = $request->get('bank_img_a', '');
            $bank_img_b = $request->get('bank_img_b', '');

            $bank_sfz_img_a = $request->get('bank_sfz_img_a', '');
            $bank_sfz_img_b = $request->get('bank_sfz_img_b', '');
            $bank_sc_img = $request->get('bank_sc_img', '');
            $store_auth_bank_img = $request->get('store_auth_bank_img', '');

            $bank_sfz_time = $request->get('bank_sfz_time', '');
            $bank_sfz_stime = $request->get('bank_sfz_stime', '');
            $bank_sfz_time = $this->time($bank_sfz_time);
            $bank_sfz_stime = $this->time($bank_sfz_stime);

            //证照信息
            $store_license_no = $request->get('store_license_no', '');
            $store_license_time = $request->get('store_license_time', '');
            $store_license_stime = $request->get('store_license_stime', '');

            $head_sc_img = $request->get('head_sc_img', '');
            $head_store_img = $request->get('head_store_img', '');

            $store_license_img = $request->get('store_license_img', '');
            $store_industrylicense_img = $request->get('store_industrylicense_img', '');
            $store_other_img_a = $request->get('store_other_img_a', '');
            $store_other_img_b = $request->get('store_other_img_b', '');
            $store_other_img_c = $request->get('store_other_img_c', '');
            $dlb_settler_hold_settlecard = $request->get('DLB_settler_hold_settlecard', ''); //哆啦宝-结算人手持结算卡

            //必填字段
            $check_data = [
                'store_id' => '门店ID',
                'store_type' => '商户类型',
                'store_type_name' => '商户类型名称',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => '2',
                    'message' => $check
                ]);
            }

            //拼装门店信息
            $stores = [
                'config_id' => $config_id,
                'user_id' => $user_id,
                'merchant_id' => '',
                'store_id' => $store_id,
                'store_name' => $store_name,
//                'store_type' => $store_type,
                'store_type_name' => $store_type_name,
                'store_email' => $store_email,
                'store_short_name' => $store_short_name,
                'people' => $people, //负责人
                'people_phone' => $people_phone,
                'province_code' => $province_code,
                'city_code' => $city_code,
                'area_code' => $area_code,
                'store_address' => $store_address,
                'head_name' => $head_name, //法人
                'head_sfz_no' => $head_sfz_no,
                'head_sfz_time' => $head_sfz_time,
                'head_sfz_stime' => $head_sfz_stime,
                'category_id' => $category_id,
                'category_name' => $category_name,
                'store_license_no' => $store_license_no,
                'store_license_time' => $store_license_time,
                'store_license_stime' => $store_license_stime,
                'weixin_name' => isset($weixin_name) ? $weixin_name : "",
                'weixin_no' => isset($weixin_no) ? $weixin_no : "",
                'alipay_name' => isset($alipay_name) ? $alipay_name : "",
                'alipay_account' => isset($alipay_account) ? $alipay_account : "",
                'dlb_micro_biz_type' => $dlb_micro_biz_type, //哆啦宝-经营类型
                'dlb_industry' => $dlb_industry, //哆啦宝店铺一级行业
                'dlb_second_industry' => $dlb_second_industry, //店铺二级行业
                'dlb_province' => $dlb_province, //哆啦宝-商户省份
                'dlb_city' => $dlb_city, //哆啦宝商户城市
                'dlb_bank' => $dlb_bank, //哆啦宝-银行名称
                'dlb_sub_bank' => $dlb_sub_bank, //哆啦宝-银行分行名称
                'dlb_pay_bank_list' => $dlb_pay_bank_list, //支付类型+费率
                'source' => $user->source //来源,01:畅立收，02:河南畅立收
            ];

            if ($store_type) {
                $stores['store_type'] = $store_type;
            }
            if ($province_code) {
                $stores['province_name'] = $this->city_name($province_code);
            }
            if ($city_code) {
                $stores['city_name'] = $this->city_name($city_code);
            }
            if ($area_code) {
                $stores['area_name'] = $this->city_name($area_code);
            }
            if ($hkrt_bus_scope_code) {
                $stores['hkrt_bus_scope_code'] = $hkrt_bus_scope_code; //海科融通经营范围
            }
            if ($is_send_tpl_mess) {
                $stores['is_send_tpl_mess'] = $is_send_tpl_mess;
            }

            //图片信息
            $store_imgs = [
                'store_id' => $store_id,
                'head_sfz_img_a' => $head_sfz_img_a,
                'head_sfz_img_b' => $head_sfz_img_b,
                'store_license_img' => $store_license_img,
                'store_industrylicense_img' => $store_industrylicense_img,
                'store_logo_img' => $store_logo_img,
                'store_img_a' => $store_img_a,
                'store_img_b' => $store_img_b,
                'store_img_c' => $store_img_c,
                'bank_img_a' => $bank_img_a,
                'bank_img_b' => $bank_img_b,
                'store_other_img_a' => $store_other_img_a,
                'store_other_img_b' => $store_other_img_b,
                'store_other_img_c' => $store_other_img_c,
                'head_sc_img' => $head_sc_img,
                'head_store_img' => $head_store_img,
                'bank_sfz_img_a' => $bank_sfz_img_a,
                'bank_sfz_img_b' => $bank_sfz_img_b,
                'bank_sc_img' => $bank_sc_img,
                'store_auth_bank_img' => $store_auth_bank_img,
                'dlb_settler_hold_settlecard' => $dlb_settler_hold_settlecard, //哆啦宝-结算人手持结算卡
            ];

            //银行卡信息
            $store_banks = [
                'store_id' => $store_id,
                'store_bank_no' => $store_bank_no,
                'store_bank_name' => $store_bank_name,
                'store_bank_phone' => $store_bank_phone,
                'store_bank_type' => $store_bank_type,
                'bank_name' => $bank_name,
                'bank_no' => $bank_no,
                'sub_bank_name' => $sub_bank_name,
                'bank_province_code' => $bank_province_code,
                'bank_city_code' => $bank_city_code,
                'bank_area_code' => $bank_area_code,
                'bank_sfz_no' => $bank_sfz_no,//持卡人默认是法人
                'bank_sfz_time' => $bank_sfz_time,
                'bank_sfz_stime' => $bank_sfz_stime,
                'reserved_mobile' => $reserved_mobile, //海科融通结算卡预留手机号
            ];

            $store = Store::where('store_id', $store_id)->first();
            $store_bank = StoreBank::where('store_id', $store_id)->first();
            $store_img = StoreImg::where('store_id', $store_id)->first();


            $is_merchant = 0;
            //判断手机号是否被注册
            if ($people_phone) {
                //验证手机号
                if (!preg_match("/^1[3456789]{1}\d{9}$/", $people_phone)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '手机号码格式不正确'
                    ]);
                }

                $Merchant = Merchant::where('phone', $people_phone)
                    ->select('id')->first();

                if ($Merchant) {
                    $is_merchant = 1;
                }

                if ($is_merchant == 1) {
                    return json_encode([
                        'status' => 2,
                        'message' => '手机号已被注册,请更换'
                    ]);
                }
            }

            //开启事务
            try {
                DB::beginTransaction();

                //入库账户
                if ($people_phone) {
                    //未注册
                    if ($is_merchant == 0) {
                        //注册账户
                        $dataIN = [
                            'pid' => 0,
                            'type' => 1,
                            'name' => $people,
                            'email' => '',
                            'password' => bcrypt('000000'),
                            'pay_password' => bcrypt('000000'),
                            'phone' => $people_phone,
                            'user_id' => $user_id, //推广员id
                            'config_id' => $config_id,
                            'wx_openid' => '',
                            'source' => $user->source //来源,01:畅立收，02:河南畅立收
                        ];
                        $merchant = Merchant::create($dataIN);
                        $merchant_id = $merchant->id;

                        $stores['merchant_id'] = $merchant_id;
                        MerchantStore::create([
                            'store_id' => $store_id,
                            'merchant_id' => $merchant_id
                        ]);
                    }
                }

                if ($store) {
                    //修改   不修改归属ID
                    $stores['user_id'] = $store->user_id;
                    $stores['config_id'] = $store->config_id;
                    $stores = array_filter($stores, function ($v) {
                        if ($v == "") {
                            return false;
                        } else {
                            return true;
                        }
                    });
                    $store->update($stores);
                    $store->save();
                } else {
//                    $rules = [
//                        'store_name' => 'unique:stores',
//                    ];
//                    $validator = Validator::make($data, $rules);
//                    if ($validator->fails()) {
//                        return json_encode([
//                            'status' => 2,
//                            'message' => '门店名称在系统中已经存在',
//                        ]);
//                    }

                    Store::create($stores);
                }
                if ($store_img) {
                    $store_img->update(array_filter($store_imgs));
                    $store_img->save();
                } else {
                    StoreImg::create($store_imgs);
                }


                if ($store_bank) {
                    $store_bank->update(array_filter($store_banks));
                    $store_bank->save();
                } else {
                    $stores['store_type'] = 1;
                    $stores['store_type_name'] = '个体';
                    StoreBank::create($store_banks);
                }

                DB::commit();
            } catch (\Exception $e) {
                Log::info($e);
                DB::rollBack();
                return json_encode(['status' => -1, 'message' => $e->getMessage()]);
            }

            return json_encode([
                'status' => 1,
                'message' => '资料添加成功',
                'data' => [
                    'store_id' => $store_id
                ]
            ]);
        } catch (\Exception $exception) {
            Log::info($exception);
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    //安装人员获取门店信息
    public function storeListToInstall(Request $request)
    {
        try {
            $token = $this->parseToken();
            $install_uer = $request->post('installUser', ''); //安装业务员用户id
            $store_id = $request->post('storeId', ''); //门店id
            $province_code = $request->post('provinceCode'); //省份code
            $city_code = $request->post('cityCode'); //城市code
            $area_code = $request->post('areaCode'); //地区code
            $install_status = $request->post('installStatus', 0); //安装状态

            $where = [];
            $where[] = ['stores.is_delete', '=', 0];
            $where[] = ['stores.is_close', '=', 0];

            if ($install_uer) {
                $where[] = ['stores.installUser', '=', $install_uer];
            }

            if ($store_id) {
                $where[] = ['stores.store_id', '=', $store_id];
            }

            if ($province_code) {
                $where[] = ['stores.province_code', '=', $province_code];
            }

            if ($city_code) {
                $where[] = ['stores.city_code', '=', $city_code];
            }

            if ($area_code) {
                $where[] = ['stores.area_code', '=', $area_code];
            }

            $obj = Store::where($where)
                ->select('id', 'pid', 'store_id', 'installUser', 'merchant_id', 'store_name', 'store_short_name', 'people_phone', 'province_name', 'city_name', 'area_name', 'store_address', 'store_type', 'lat', 'lng', 'installStatus', 'created_at', 'updated_at')
                ->where('stores.installStatus', '=', $install_status)
                ->orderBy('updated_at', 'desc');

            $this->t = $obj->count();
            $data = $this->page($obj)->get();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }


    //更新安装进度
    public function updateStoreInstallStatus(Request $request)
    {
        try {
            $token = $this->parseToken();
            $install_uer = $request->post('installUser', ''); //安装业务员用户id
            $store_id = $request->post('storeId', ''); //门店id
            $install_status = $request->post('installStatus', 0); //安装状态

            $user_info = User::where('id', $install_uer)->first();
            if (!$user_info) {
                $this->status = 2;
                $this->message = '无此业务员信息';
                return $this->format();
            }

            $store_info = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$store_info) {
                $this->status = 2;
                $this->message = '无此门店信息或状态异常';
                return $this->format();
            }

            if ($store_info->installStatus == $install_status) {
                $this->status = 2;
                $this->message = '请勿重复更新安装状态';
                return $this->format();
            }

            $updateData = [
                'installStatus' => $install_status
            ];
            if ($install_uer) {
                $updateData['installUser'] = $install_uer;
            }
            $obj = $store_info->update($updateData);
            if ($obj) {
                $this->status = 1;
                $this->message = '更新状态成功';
                return $this->format();
            } else {
                $this->status = 2;
                $this->message = '更新状态失败';
                return $this->format();
            }
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }


    //达达配置
    public function updateDaDaConfig(Request $request)
    {
        $token = $this->parseToken();
        $type = $request->post('type', '');
        $store_id = $request->post('storeId', '');
        $source_id = $request->post('sourceId', '');
        $shop_no = $request->post('shopNo', '');

        if ($type) {
            $dada_merchants = DadaMerchant::where('store_id', $store_id)
                ->select('store_id', 'source_id', 'status')
                ->first();
            $dada_orders = DadaStore::where('store_id', $store_id)
                ->select('store_id', 'shop_no', 'origin_shop_id', 'new_shop_id', 'status')
                ->first();

            $data = [];
            if ($dada_merchants) {
                $data['source_id'] = $dada_merchants->source_id;
            }
            if ($dada_orders) {
                $data['shop_no'] = $dada_orders->shop_no;
            }
            $this->status = 200;
            $this->message = '数据返回成功';
            return $this->format($data);
        }

        try {
            $merchants_res = DadaMerchant::where('store_id', $store_id)->first();
            $store_res = DadaStore::where('store_id', $store_id)->first();

            if ($source_id && $shop_no) {
                if ($merchants_res && $store_res) {
                    $merchants_up_res = $merchants_res->update([
                        'source_id' => $source_id
                    ]);

                    $store_up_res = $store_res->update([
                        'shop_no' => $shop_no
                    ]);

                    if ($merchants_up_res && $store_up_res) {
                        $this->status = 200;
                        $this->message = '更新成功';
                        return $this->format();
                    }
                } else {
                    $merchants_create_res = DadaMerchant::create([
                        'store_id' => $store_id,
                        'source_id' => $source_id,
                        'status' => 1
                    ]);

                    $store_create_res = DadaStore::create([
                        'store_id' => $store_id,
                        'shop_no' => $shop_no,
                        'status' => 1
                    ]);

                    if ($merchants_create_res && $store_create_res) {
                        $this->status = 200;
                        $this->message = '创建成功';
                        return $this->format();
                    }
                }
            }

            $this->status = 200;
            $this->message = '没有任何更改';
            return $this->format();
        } catch (\Exception $e) {
            $this->status = -1;
            $this->message = $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine();
            return $this->format();
        }
    }


    //微信实名认证 提交
    public function applyWeChatRealName(Request $request)
    {
        try {
            $token = $this->parseToken();
            $ways_type = $request->post('waysType', ''); //支付类型
            $store_id = $request->post('storeId', ''); //门店id

            $check_data = [
                'waysType' => '支付类型',
                'storeId' => '门店ID'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                $this->status = 2;
                $this->message = $check;
                return $this->format();
            }

            //门店是否存在
            $store_obj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$store_obj) {
                $this->status = 2;
                $this->message = '门店不存在或状态异常';
                return $this->format();
            }
            $config_id = $store_obj->config_id;

            //通道是否开通
            $store_pay_ways_obj = StorePayWay::where('ways_type', $ways_type)
                ->where('store_id', $store_id)
                ->where('status', 1)
                ->first();
            if (!$store_pay_ways_obj) {
                $this->status = 2;
                $this->message = '通道未开通或状态异常';
                return $this->format();
            }

            //随行付
            if ((12999 < $ways_type) && ($ways_type < 13999)) {
                $config_obj = new VbillConfigController();
                $vbill_config_obj = $config_obj->vbill_config($config_id);
                if (!$vbill_config_obj) {
                    $this->status = 2;
                    $this->message = '随行付支付未配置';
                    return $this->format();
                }

                $vbill_merchant_obj = $config_obj->vbill_merchant($store_id, 0);
                if (!$vbill_merchant_obj) {
                    $this->status = 2;
                    $this->message = '随行付-商户未入网';
                    return $this->format();
                }
                if (!$vbill_merchant_obj->mno) {
                    $this->status = 2;
                    $this->message = '随行付-商户号不存在';
                    return $this->format();
                }

                if (($vbill_merchant_obj->idenStatus == 0) || ($vbill_merchant_obj->idenStatus == 7)) {
                    $vbill_pay_obj = new PayController();
                    $commit_apply_data = [
                        'mno' => $vbill_merchant_obj->mno,
//                    'backUrl' => url('/api/vbill/weChatApplyNotify'),
                        'orgId' => $vbill_config_obj->orgId,
                        'privateKey' => $vbill_config_obj->privateKey,
                        'sxfpublic' => $vbill_config_obj->sxfpublic
                    ];
                    $result = $vbill_pay_obj->weChatRealNameCommitApply($commit_apply_data);

                    if ($result['status'] == 1) {
                        $wxApplyNo = isset($result['data']['wxApplyNo']) ? $result['data']['wxApplyNo'] : '';
                        if (!$wxApplyNo) {
                            $this->status = 2;
                            $this->message = '随行付-微信申请单编号-未返回';
                            return $this->format();
                        }
                        $update_res = $vbill_merchant_obj->update([
                            'wxApplyNo' => $wxApplyNo,
                            'idenStatus' => 4
                        ]);
                        if ($update_res) {
                            $this->status = 1;
                            $this->message = '操作成功';
                            return $this->format($vbill_merchant_obj);
                        } else {
                            $this->status = 2;
                            $this->message = '随行付-微信申请单编号-入库失败';
                            return $this->format($vbill_merchant_obj);
                        }
                    } else {
                        $this->status = 2;
                        $this->message = $result['message'] ?? '随行付-微信实名认证-失败';
                        return $this->format($vbill_merchant_obj);
                    }
                } else {
                    $this->status = 1;
                    $this->message = "'未认证'或'已作废'状态方可提交认证";
                    return $this->format($vbill_merchant_obj);
                }
            }

            //随行付a
            if ((18999 < $ways_type) && ($ways_type < 19999)) {
                $config_obj = new VbillConfigController();
                $vbill_config_obj = $config_obj->vbilla_config($config_id);
                if (!$vbill_config_obj) {
                    $this->status = 2;
                    $this->message = '随行付a-支付未配置';
                    return $this->format();
                }

                $vbill_merchant_obj = $config_obj->vbilla_merchant($store_id, 0);
                if (!$vbill_merchant_obj) {
                    $this->status = 2;
                    $this->message = '随行付a-商户尚未入网';
                    return $this->format();
                }
                if (!$vbill_merchant_obj->mno) {
                    $this->status = 2;
                    $this->message = '随行付a-商户号不存在';
                    return $this->format();
                }

                if (($vbill_merchant_obj->idenStatus == 0) || ($vbill_merchant_obj->idenStatus == 7)) {
                    $vbill_pay_obj = new PayController();
                    $commit_apply_data = [
                        'mno' => $vbill_merchant_obj->mno,
//                    'backUrl' => url('/api/vbill/weChatApplyNotifyA'),
                        'orgId' => $vbill_config_obj->orgId,
                        'privateKey' => $vbill_config_obj->privateKey,
                        'sxfpublic' => $vbill_config_obj->sxfpublic
                    ];
                    $result = $vbill_pay_obj->weChatRealNameCommitApply($commit_apply_data);

                    if ($result['status'] == 1) {
                        $wxApplyNo = isset($result['data']['wxApplyNo']) ? $result['data']['wxApplyNo'] : '';
                        if (!$wxApplyNo) {
                            $this->status = 2;
                            $this->message = '随行付-微信申请单编号-未返回';
                            return $this->format();
                        }
                        $update_res = $vbill_merchant_obj->update([
                            'wxApplyNo' => $wxApplyNo,
                            'idenStatus' => 4
                        ]);
                        if ($update_res) {
                            $this->status = 1;
                            $this->message = '操作成功';
                            return $this->format($vbill_merchant_obj);
                        } else {
                            $this->status = 2;
                            $this->message = '随行付a-微信申请单编号-入库失败';
                            return $this->format($vbill_merchant_obj);
                        }
                    } else {
                        $this->status = 2;
                        $this->message = $result['message'] ?? '随行付a-微信实名认证-失败';
                        return $this->format($vbill_merchant_obj);
                    }
                } else {
                    $this->status = 1;
                    $this->message = "'未认证'或'已作废'状态方可提交认证";
                    return $this->format($vbill_merchant_obj);
                }
            }

            $this->status = 2;
            $this->message = '所选支付类型不存在';
            return $this->format();
        } catch (\Exception $ex) {
            Log::info('提交微信实名认证error:');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }


    //微信实名认证 结果查询
    public function queryWeChatRealName(Request $request)
    {
        try {
            $token = $this->parseToken();
            $ways_type = $request->post('waysType', ''); //支付类型
            $store_id = $request->post('storeId', ''); //门店id

            $check_data = [
                'waysType' => '支付类型',
                'storeId' => '门店ID'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                $this->status = 2;
                $this->message = $check;
                return $this->format();
            }

            //门店是否存在
            $store_obj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$store_obj) {
                $this->status = 2;
                $this->message = '门店不存在或状态异常';
                return $this->format();
            }
            $config_id = $store_obj->config_id;

            //通道是否开通
            $store_pay_ways_obj = StorePayWay::where('ways_type', $ways_type)
                ->where('store_id', $store_id)
                ->where('status', 1)
                ->first();
            if (!$store_pay_ways_obj) {
                $this->status = 2;
                $this->message = '通道未开通或状态异常';
                return $this->format();
            }

            //随行付
            if ((12999 < $ways_type) && ($ways_type < 13999)) {
                $config_obj = new VbillConfigController();
                $vbill_config_obj = $config_obj->vbill_config($config_id);
                if (!$vbill_config_obj) {
                    $this->status = 2;
                    $this->message = '随行付支付未配置';
                    return $this->format();
                }
                $vbill_merchant_obj = $config_obj->vbill_merchant($store_id, 0);
                if (!$vbill_merchant_obj) {
                    $this->status = 2;
                    $this->message = '随行付-商户未入网';
                    return $this->format();
                }
                if (!$vbill_merchant_obj->mno) {
                    $this->status = 2;
                    $this->message = '随行付-商户号不存在';
                    return $this->format();
                }
                if (!$vbill_merchant_obj->wxApplyNo) {
                    $this->status = 2;
                    $this->message = '随行付-微信申请单编号不存在';
                    return $this->format();
                }

                $vbill_pay_obj = new PayController();
                $commit_apply_data = [
                    'mno' => $vbill_merchant_obj->mno,
                    'wxApplyNo' => $vbill_merchant_obj->wxApplyNo,
                    'orgId' => $vbill_config_obj->orgId,
                    'privateKey' => $vbill_config_obj->privateKey,
                    'sxfpublic' => $vbill_config_obj->sxfpublic
                ];
                $result = $vbill_pay_obj->weChatRealNameQuery($commit_apply_data);

                if ($result['status'] == 1) {
                    $idenStatus = isset($result['data']['idenStatus']) ? $result['data']['idenStatus'] : ''; //认证状态
                    $infoQrcode = isset($result['data']['infoQrcode']) ? $result['data']['infoQrcode'] : ''; //确认二维码链接地址
                    $rejectCode = isset($result['data']['rejectCode']) ? $result['data']['rejectCode'] : ''; //驳回参数
                    $rejectInfo = isset($result['data']['rejectInfo']) ? $result['data']['rejectInfo'] : ''; //驳回原因
                    if (!$idenStatus) {
                        $this->status = 2;
                        $this->message = '随行付-微信实名认证状态-未返回';
                        return $this->format();
                    }

                    switch ($idenStatus) {
                        case 'APPLYMENT_STATE_WAITTING_FOR_AUDIT':
                            $iden_status = 4;
                            break;
                        case 'APPLYMENT_STATE_WAITTING_FOR_CONFIRM_CONTACT':
                            $iden_status = 2;
                            break;
                        case 'APPLYMENT_STATE_WAITTING_FOR_CONFIRM_LEGALPERSON':
                            $iden_status = 3;
                            break;
                        case 'APPLYMENT_STATE_PASSED':
                            $iden_status = 1;
                            break;
                        case 'APPLYMENT_STATE_REJECTED':
                            $iden_status = 5;
                            break;
                        case 'APPLYMENT_STATE_FREEZED':
                            $iden_status = 6;
                            break;
                        case 'APPLYMENT_STATE_CANCELED':
                            $iden_status = 7;
                            break;
                        case 'APPLYMENT_STATE_OPENACCOUNT':
                            $iden_status = 8;
                            break;
                        default:
                            $iden_status = 9;
                    }

                    if (!$iden_status) {
                        $this->status = 2;
                        $this->message = '随行付-微信实名认证状态-未知';
                        return $this->format();
                    }

                    $update_data = [
                        'idenStatus' => $iden_status, //0-未认证;1-审核通过;2-待确认联系信息;3-待账户验证;4-审核中;5-审核驳回;6-已冻结;7-已作废;8-重复认证;9-其他
                    ];
                    if ($infoQrcode) {
                        $update_data['infoQrcode'] = $infoQrcode;
                    }
                    if ($rejectCode) {
                        $update_data['rejectCode'] = $rejectCode;
                    }
                    if ($rejectInfo) {
                        $update_data['rejectInfo'] = $rejectInfo;
                    }
                    $update_res = $vbill_merchant_obj->update($update_data);
                    if ($update_res) {
                        $this->status = 1;
                        $this->message = '操作成功';
                        return $this->format($vbill_merchant_obj);
                    } else {
                        $this->status = 2;
                        $this->message = '随行付-微信实名认证查询结果-更新失败';
                        return $this->format($vbill_merchant_obj);
                    }
                } else {
                    $this->status = 2;
                    $this->message = $result['message'] ?? '随行付-微信实名认证查询-失败';
                    return $this->format($vbill_merchant_obj);
                }
            }

            //随行付a
            if ((18999 < $ways_type) && ($ways_type < 19999)) {
                $config_obj = new VbillConfigController();
                $vbill_config_obj = $config_obj->vbilla_config($config_id);
                if (!$vbill_config_obj) {
                    $this->status = 2;
                    $this->message = '随行付a-支付未配置';
                    return $this->format();
                }

                $vbill_merchant_obj = $config_obj->vbilla_merchant($store_id, 0);
                if (!$vbill_merchant_obj) {
                    $this->status = 2;
                    $this->message = '随行付a-商户尚未入网';
                    return $this->format();
                }
                if (!$vbill_merchant_obj->mno) {
                    $this->status = 2;
                    $this->message = '随行付a-商户号不存在';
                    return $this->format();
                }
                if (!$vbill_merchant_obj->wxApplyNo) {
                    $this->status = 2;
                    $this->message = '随行付a-微信申请单编号不存在';
                    return $this->format();
                }

                $vbill_pay_obj = new PayController();
                $commit_apply_data = [
                    'mno' => $vbill_merchant_obj->mno,
                    'wxApplyNo' => $vbill_merchant_obj->wxApplyNo,
                    'orgId' => $vbill_config_obj->orgId,
                    'privateKey' => $vbill_config_obj->privateKey,
                    'sxfpublic' => $vbill_config_obj->sxfpublic
                ];
                $result = $vbill_pay_obj->weChatRealNameQuery($commit_apply_data);

                if ($result['status'] == 1) {
                    $idenStatus = isset($result['data']['idenStatus']) ? $result['data']['idenStatus'] : ''; //认证状态
                    $infoQrcode = isset($result['data']['infoQrcode']) ? $result['data']['infoQrcode'] : ''; //确认二维码链接地址
                    $rejectCode = isset($result['data']['rejectCode']) ? $result['data']['rejectCode'] : ''; //驳回参数
                    $rejectInfo = isset($result['data']['rejectInfo']) ? $result['data']['rejectInfo'] : ''; //驳回原因
                    if (!$idenStatus) {
                        $this->status = 2;
                        $this->message = '随行付a-微信实名认证状态-未返回';
                        return $this->format();
                    }

                    switch ($idenStatus) {
                        case 'APPLYMENT_STATE_WAITTING_FOR_AUDIT':
                            $iden_status = 4;
                            break;
                        case 'APPLYMENT_STATE_WAITTING_FOR_CONFIRM_CONTACT':
                            $iden_status = 2;
                            break;
                        case 'APPLYMENT_STATE_WAITTING_FOR_CONFIRM_LEGALPERSON':
                            $iden_status = 3;
                            break;
                        case 'APPLYMENT_STATE_PASSED':
                            $iden_status = 1;
                            break;
                        case 'APPLYMENT_STATE_REJECTED':
                            $iden_status = 5;
                            break;
                        case 'APPLYMENT_STATE_FREEZED':
                            $iden_status = 6;
                            break;
                        case 'APPLYMENT_STATE_CANCELED':
                            $iden_status = 7;
                            break;
                        case 'APPLYMENT_STATE_OPENACCOUNT':
                            $iden_status = 8;
                            break;
                        default:
                            $iden_status = 9;
                    }

                    if (!$iden_status) {
                        $this->status = 2;
                        $this->message = '随行付a-微信实名认证状态-未知';
                        return $this->format();
                    }

                    $update_data = [
                        'idenStatus' => $iden_status, //0-未认证;1-审核通过;2-待确认联系信息;3-待账户验证;4-审核中;5-审核驳回;6-已冻结;7-已作废;8-重复认证;9-其他
                    ];
                    if ($infoQrcode) {
                        $update_data['infoQrcode'] = $infoQrcode;
                    }
                    if ($rejectCode) {
                        $update_data['rejectCode'] = $rejectCode;
                    }
                    if ($rejectInfo) {
                        $update_data['rejectInfo'] = $rejectInfo;
                    }
                    $update_res = $vbill_merchant_obj->update($update_data);
                    if ($update_res) {
                        $this->status = 1;
                        $this->message = '操作成功';
                        return $this->format($vbill_merchant_obj);
                    } else {
                        $this->status = 2;
                        $this->message = '随行付a-微信实名认证查询结果-更新失败';
                        return $this->format($vbill_merchant_obj);
                    }
                } else {
                    $this->status = 2;
                    $this->message = $result['message'] ?? '随行付a-微信实名认证查询-失败';
                    return $this->format($vbill_merchant_obj);
                }
            }

            $this->status = 2;
            $this->message = '所选支付类型不存在';
            return $this->format();
        } catch (\Exception $ex) {
            Log::info('查询-微信实名认证error:');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }


    //微信实名认证 申请撤销
    public function backWeChatRealName(Request $request)
    {
        try {
            $token = $this->parseToken();
            $ways_type = $request->post('waysType', ''); //支付类型
            $store_id = $request->post('storeId', ''); //门店id

            $check_data = [
                'waysType' => '支付类型',
                'storeId' => '门店ID'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                $this->status = 2;
                $this->message = $check;
                return $this->format();
            }

            //门店是否存在
            $store_obj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$store_obj) {
                $this->status = 2;
                $this->message = '门店不存在或状态异常';
                return $this->format();
            }
            $config_id = $store_obj->config_id;

            //通道是否开通
            $store_pay_ways_obj = StorePayWay::where('ways_type', $ways_type)
                ->where('store_id', $store_id)
                ->where('status', 1)
                ->first();
            if (!$store_pay_ways_obj) {
                $this->status = 2;
                $this->message = '通道未开通或状态异常';
                return $this->format();
            }

            //随行付
            if ((12999 < $ways_type) && ($ways_type < 13999)) {
                $config_obj = new VbillConfigController();
                $vbill_config_obj = $config_obj->vbill_config($config_id);
                if (!$vbill_config_obj) {
                    $this->status = 2;
                    $this->message = '随行付支付未配置';
                    return $this->format();
                }
                $vbill_merchant_obj = $config_obj->vbill_merchant($store_id, 0);
                if (!$vbill_merchant_obj) {
                    $this->status = 2;
                    $this->message = '随行付-商户未入网';
                    return $this->format();
                }
                if (!$vbill_merchant_obj->mno) {
                    $this->status = 2;
                    $this->message = '随行付-商户号不存在';
                    return $this->format();
                }
                if (!$vbill_merchant_obj->wxApplyNo) {
                    $this->status = 2;
                    $this->message = '随行付-微信申请单编号不存在';
                    return $this->format();
                }

                if (($vbill_merchant_obj->idenStatus == 4) || ($vbill_merchant_obj->idenStatus == 5)) {
                    $vbill_pay_obj = new PayController();
                    $commit_apply_data = [
                        'mno' => $vbill_merchant_obj->mno,
                        'wxApplyNo' => $vbill_merchant_obj->wxApplyNo,
                        'orgId' => $vbill_config_obj->orgId,
                        'privateKey' => $vbill_config_obj->privateKey,
                        'sxfpublic' => $vbill_config_obj->sxfpublic
                    ];
                    $result = $vbill_pay_obj->weChatRealNameBack($commit_apply_data);
                    if ($result['status'] == 1) {
                        $update_data = [
                            'idenStatus' => 7, //0-未认证;1-审核通过;2-待确认联系信息;3-待账户验证;4-审核中;5-审核驳回;6-已冻结;7-已作废(表示申请单已被撤销,无需再对其进行操作);8-重复认证;9-其他
                        ];
                        $update_res = $vbill_merchant_obj->update($update_data);
                        if ($update_res) {
                            $this->status = 1;
                            $this->message = '撤销成功';
                            return $this->format($vbill_merchant_obj);
                        } else {
                            $this->status = 2;
                            $this->message = '随行付-微信实名认证撤销-更新库失败';
                            return $this->format($vbill_merchant_obj);
                        }
                    } else {
                        $this->status = 2;
                        $this->message = $result['message'] ?? '随行付-微信实名认证撤销-失败';
                        return $this->format($vbill_merchant_obj);
                    }
                } else {
                    $this->status = 2;
                    $this->message = "'审核中'或'审核驳回'方可撤销";
                    return $this->format($vbill_merchant_obj);
                }
            }

            //随行付a
            if ((18999 < $ways_type) && ($ways_type < 19999)) {
                $config_obj = new VbillConfigController();
                $vbill_config_obj = $config_obj->vbilla_config($config_id);
                if (!$vbill_config_obj) {
                    $this->status = 2;
                    $this->message = '随行付a-支付未配置';
                    return $this->format();
                }

                $vbill_merchant_obj = $config_obj->vbilla_merchant($store_id, 0);
                if (!$vbill_merchant_obj) {
                    $this->status = 2;
                    $this->message = '随行付a-商户尚未入网';
                    return $this->format();
                }
                if (!$vbill_merchant_obj->mno) {
                    $this->status = 2;
                    $this->message = '随行付a-商户号不存在';
                    return $this->format();
                }
                if (!$vbill_merchant_obj->wxApplyNo) {
                    $this->status = 2;
                    $this->message = '随行付a-微信申请单编号不存在';
                    return $this->format();
                }

                if (($vbill_merchant_obj->idenStatus == 4) || ($vbill_merchant_obj->idenStatus == 5)) {
                    $vbill_pay_obj = new PayController();
                    $commit_apply_data = [
                        'mno' => $vbill_merchant_obj->mno,
                        'wxApplyNo' => $vbill_merchant_obj->wxApplyNo,
                        'orgId' => $vbill_config_obj->orgId,
                        'privateKey' => $vbill_config_obj->privateKey,
                        'sxfpublic' => $vbill_config_obj->sxfpublic
                    ];
                    $result = $vbill_pay_obj->weChatRealNameBack($commit_apply_data);
                    if ($result['status'] == 1) {
                        $update_data = [
                            'idenStatus' => 7, //7-已作废(表示申请单已被撤销,无需再对其进行操作)
                        ];
                        $update_res = $vbill_merchant_obj->update($update_data);
                        if ($update_res) {
                            $this->status = 1;
                            $this->message = '撤销成功';
                            return $this->format($vbill_merchant_obj);
                        } else {
                            $this->status = 2;
                            $this->message = '随行付a-微信实名认证撤销-更新库失败';
                            return $this->format($vbill_merchant_obj);
                        }
                    } else {
                        $this->status = 2;
                        $this->message = $result['message'] ?? '随行付a-微信实名认证撤销-失败';
                        return $this->format($vbill_merchant_obj);
                    }
                } else {
                    $this->status = 2;
                    $this->message = "'审核中'或'审核驳回'方可撤销";
                    return $this->format($vbill_merchant_obj);
                }
            }

            $this->status = 2;
            $this->message = '所选支付类型不存在';
            return $this->format();
        } catch (\Exception $ex) {
            Log::info('微信实名认证-撤销-error:');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }


    //微信子商户授权状态 查询
    public function queryGrantStatusWeChatRealName(Request $request)
    {
        try {
            $token = $this->parseToken();
            $ways_type = $request->post('waysType', ''); //支付类型
            $store_id = $request->post('storeId', ''); //门店id

            $check_data = [
                'waysType' => '支付类型',
                'storeId' => '门店ID'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                $this->status = 2;
                $this->message = $check;
                return $this->format();
            }

            //门店是否存在
            $store_obj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$store_obj) {
                $this->status = 2;
                $this->message = '门店不存在或状态异常';
                return $this->format();
            }
            $config_id = $store_obj->config_id;

            //通道是否开通
            $store_pay_ways_obj = StorePayWay::where('ways_type', $ways_type)
                ->where('store_id', $store_id)
                ->where('status', 1)
                ->first();
            if (!$store_pay_ways_obj) {
                $this->status = 2;
                $this->message = '通道未开通或状态异常';
                return $this->format();
            }

            //随行付
            if ((12999 < $ways_type) && ($ways_type < 13999)) {
                $config_obj = new VbillConfigController();
                $vbill_config_obj = $config_obj->vbill_config($config_id);
                if (!$vbill_config_obj) {
                    $this->status = 2;
                    $this->message = '随行付支付未配置';
                    return $this->format();
                }
                $vbill_merchant_obj = $config_obj->vbill_merchant($store_id, 0);
                if (!$vbill_merchant_obj) {
                    $this->status = 2;
                    $this->message = '随行付-商户未入网';
                    return $this->format();
                }
                if (!$vbill_merchant_obj->mno) {
                    $this->status = 2;
                    $this->message = '随行付-商户号不存在';
                    return $this->format();
                }
                if (!$vbill_merchant_obj->childNo) {
                    $this->status = 2;
                    $this->message = '随行付-微信子商户号不存在';
                    return $this->format();
                }

                $vbill_pay_obj = new PayController();
                $commit_apply_data = [
                    'childNo' => $vbill_merchant_obj->childNo,
                    'orgId' => $vbill_config_obj->orgId,
                    'privateKey' => $vbill_config_obj->privateKey,
                    'sxfpublic' => $vbill_config_obj->sxfpublic
                ];
                $result = $vbill_pay_obj->weChatRealNameQueryGrantStatus($commit_apply_data);

                if ($result['status'] == 1) {
                    $authStatus = isset($result['data']['authStatus']) ? $result['data']['authStatus'] : ''; //授权状态
                    if (!$authStatus) {
                        $this->status = 2;
                        $this->message = '随行付-微信子商户授权状态-未返回';
                        return $this->format();
                    }

                    if ($authStatus == 'AUTHORIZE_STATE_AUTHORIZED') {
                        $auth_status = 1;
                    } elseif ($authStatus == 'AUTHORIZE_STATE_UNAUTHORIZED') {
                        $auth_status = 2;
                    } else {
                        $this->status = 2;
                        $this->message = '随行付-微信子商户授权状态-未知';
                        return $this->format();
                    }

                    $update_data = [
                        'authStatus' => $auth_status, //0-未知;1-已授权;2--未授权
                    ];
                    $update_res = $vbill_merchant_obj->update($update_data);
                    if ($update_res) {
                        $this->status = 1;
                        $this->message = '成功';
                        return $this->format($vbill_merchant_obj);
                    } else {
                        $this->status = 2;
                        $this->message = '随行付-微信子商户授权状态-更新入库失败';
                        return $this->format($vbill_merchant_obj);
                    }
                } else {
                    $this->status = 2;
                    $this->message = $result['message'] ?? '随行付-微信子商户授权状态-查询失败';
                    return $this->format($vbill_merchant_obj);
                }
            }

            //随行付a
            if ((18999 < $ways_type) && ($ways_type < 19999)) {
                $config_obj = new VbillConfigController();
                $vbill_config_obj = $config_obj->vbilla_config($config_id);
                if (!$vbill_config_obj) {
                    $this->status = 2;
                    $this->message = '随行付a-支付未配置';
                    return $this->format();
                }
                $vbill_merchant_obj = $config_obj->vbilla_merchant($store_id, 0);
                if (!$vbill_merchant_obj) {
                    $this->status = 2;
                    $this->message = '随行付a-商户未入网';
                    return $this->format();
                }
                if (!$vbill_merchant_obj->mno) {
                    $this->status = 2;
                    $this->message = '随行付a-商户号不存在';
                    return $this->format();
                }
                if (!$vbill_merchant_obj->childNo) {
                    $this->status = 2;
                    $this->message = '随行付a-微信子商户号不存在';
                    return $this->format();
                }

                $vbill_pay_obj = new PayController();
                $commit_apply_data = [
                    'childNo' => $vbill_merchant_obj->childNo,
                    'orgId' => $vbill_config_obj->orgId,
                    'privateKey' => $vbill_config_obj->privateKey,
                    'sxfpublic' => $vbill_config_obj->sxfpublic
                ];
                $result = $vbill_pay_obj->weChatRealNameQueryGrantStatus($commit_apply_data);

                if ($result['status'] == 1) {
                    $authStatus = isset($result['data']['authStatus']) ? $result['data']['authStatus'] : ''; //授权状态
                    if (!$authStatus) {
                        $this->status = 2;
                        $this->message = '随行付-微信子商户授权状态-未返回';
                        return $this->format();
                    }

                    if ($authStatus == 'AUTHORIZE_STATE_AUTHORIZED') {
                        $auth_status = 1;
                    } elseif ($authStatus == 'AUTHORIZE_STATE_UNAUTHORIZED') {
                        $auth_status = 2;
                    } else {
                        $this->status = 2;
                        $this->message = '随行付-微信子商户授权状态-未知';
                        return $this->format();
                    }

                    $update_data = [
                        'authStatus' => $auth_status, //0-未知;1-已授权;2--未授权
                    ];
                    $update_res = $vbill_merchant_obj->update($update_data);
                    if ($update_res) {
                        $this->status = 1;
                        $this->message = '成功';
                        return $this->format($vbill_merchant_obj);
                    } else {
                        $this->status = 2;
                        $this->message = '随行付a-微信子商户授权状态-更新入库失败';
                        return $this->format($vbill_merchant_obj);
                    }
                } else {
                    $this->status = 2;
                    $this->message = $result['message'] ?? '随行付a-微信子商户授权状态-查询失败';
                    return $this->format($vbill_merchant_obj);
                }
            }

            $this->status = 2;
            $this->message = '所选支付类型不存在';
            return $this->format();
        } catch (\Exception $ex) {
            Log::info('查询-微信子商户授权状态查询error:');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }


    public function putVoice(Request $request)
    {
        try {
            $user = $this->parseToken();
            $config_id = $request->post('configId', '1234');
            $action_type = $request->post('type', 1); //1-读 0-减

            $list_obj = VoiceStatistic::where('config_id', $config_id)
                ->first();
            if (!$list_obj) {
                $this->status = 2;
                $this->message = '用户尚未购买套餐';
                return $this->format();
            }

            if ($action_type) {
                $this->status = 1;
                $this->message = '数据返回成功';
                return $this->format($list_obj);
            } else {
                if ($list_obj->avali_num < 1) {
                    $this->status = 2;
                    $this->message = '套餐可以次数已用完，请充值';
                    return $this->format();
                }

                $avali_num = $list_obj->avali_num - 1;
                $update_res = $list_obj->update([
                    'avali_num' => max($avali_num, 0)
                ]);
                if ($update_res) {
                    $this->status = 1;
                    $this->message = '更新可用次数成功';
                    return $this->format($list_obj);
                } else {
                    $this->status = 2;
                    $this->message = '更新可用次数失败';
                    return $this->format($list_obj);
                }
            }
        } catch (\Exception $ex) {
            Log::info('xunfei语音合成-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }


    public function voicePayQr(Request $request)
    {
        try {
            $user = $this->parseToken();

            $data = [
                'store_pay_qr' => 'https://test.yunsoyi.cn/qr?store_id=2020518155323231527&voiceType=1'
            ];

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    //设置门店零费率开关
    public function edit_store_zero_rate(Request $request)
    {
        try {
            $user = $this->parseToken();
            $id = $request->get('id', '');
            $zero_rate_type = $request->get('zero_rate_type');

            $store = Store::where('id', $id)->first();
            if (!$store) {
                return json_encode(['status' => 2, 'message' => '门店不存在', 'data' => []]);
            }

            Store::where('id', $id)
                ->update([
                    'zero_rate_type' => $zero_rate_type,
                ]);

            return json_encode(['status' => 1, 'message' => '设置成功', 'data' => []]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . ' - ' . $exception->getLine()
            ]);
        }
    }

    //小程序端获取排序为1的支付通道
    public function getFirstPayWay(Request $request)
    {
        try {
            $store_id = $request->get('store_id');
            if (empty($store_id)) {
                return json_encode([
                    'status' => 2,
                    'message' => '商户id不能为空'
                ]);
            }
            $firstPayWay = StorePayWay::where('status', 1)
                ->where('ways_source', 'weixin')
                ->where('store_id', $store_id)
                ->select()
                ->orderBy('sort')
                ->first();
            if (empty($firstPayWay)) {
                return json_encode([
                    'status' => 2,
                    'message' => '商户还没有开通任何支付通道'
                ]);
            }
            return json_encode([
                'status' => 1,
                'data' => $firstPayWay,
                'message' => '获取成功'
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . ' - ' . $exception->getLine()
            ]);
        }
    }

    public function getUserLevel(Request $request)
    {
        try {
            $user_id = $request->get('user_id');
            $firstDayOfLastMonth = strtotime('first day of last month');
            $time = date('Y-m-d', $firstDayOfLastMonth);
            $AgentLevelsInfos = UserMonthLevel::where('user_id', $user_id)
                ->where('start_time', $time)
                ->select('*')
                ->first();
            $level = 'V1';
            if ($AgentLevelsInfos) {
                $level = 'V' . $AgentLevelsInfos->level_weight;
            }
            return json_encode([
                'status' => 1,
                'data' => $level,
                'message' => '获取成功'
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . ' - ' . $exception->getLine()
            ]);
        }

    }

    public function getUserLevelIs(Request $request)
    {
        try {
            $user_id = $request->get('user_id');
            $time = date('Y-m-d H:i:m', time());
            $AgentLevelsInfos = AgentLevelsInfos::where('user_id', $user_id)
                ->where('end_time', '>=', $time)
                ->select('*')
                ->first();
            $level = 'V1';
            if ($AgentLevelsInfos) {
                $level = $AgentLevelsInfos->level;
            }
            return json_encode([
                'status' => 1,
                'data' => $level,
                'message' => '获取成功'
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . ' - ' . $exception->getLine()
            ]);
        }

    }


    public function get_user_qr_num(Request $request)
    {
        try {
            $user_id = $request->get('user_id');

            $userAuths = UserAuths::where('user_id', $user_id)->where('user_type', '1')->first();
            if (!$userAuths) {
                return json_encode([
                    'status' => -1,
                    'message' => '未实名认证，请先进行实名认证'
                ]);
            }
            $user = User::where('id', $user_id)->where('is_incoming', '1')->where('is_delete', '0')->first();
            if (!$user) {
                return json_encode([
                    'status' => -1,
                    'message' => '没有进件权限，进件状态为禁止或者代理不存在'
                ]);
            }
            $qrListInfo = QrListInfo::where('user_id', $user_id)->select('*')->get();
            $user_qr_num = count($qrListInfo);
            if ($user_qr_num < 10) {
                $qr_gap_num = 10 - $user_qr_num;
                return json_encode([
                    'status' => 2,
                    'message' => '不可展业，库存差' . $qr_gap_num . '张'
                ]);
            }
            return json_encode([
                'status' => 1,
                'message' => '获取成功'
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . ' - ' . $exception->getLine()
            ]);
        }
    }

    /**
     * PC端为商户创建商户号
     */
    public function createMerchantNo(Request $request)
    {
        $merchant_id = $request->get('merchant_id', '');
        if ($merchant_id == '') {
            return json_encode([
                'status' => -1,
                'message' => '参数错误'
            ]);
        }

        //查询是否存在商户号
        $merchantInfo = Merchant::where('id', $merchant_id)->where('login_num', '0')
            ->select('merchant_no', 'id', 'phone', 'login_num')->first();
        if ($merchantInfo) {
            if ($merchantInfo->merchant_no) {
                $merchantNo = $merchantInfo->merchant_no;
            } else {
                //80开头+当前日期+4位随机数
                $merchantNo = $this->generateMerchantNo();
                //检查是否存在商户号
                $existMerchantNo = Merchant::where('merchant_no', $merchantNo)->first();
                if ($existMerchantNo) {
                    return json_encode([
                        'status' => -1,
                        'message' => '请重新生成，存在相同商户号:' . $merchantNo
                    ]);
                }

                $upIN = [
                    'merchant_no' => $merchantNo,
                ];
                $merchantInfo->update($upIN);
            }

            return json_encode([
                'status' => 1,
                'message' => '操作成功',
                'data' => $merchantNo
            ]);
            //您的商户号为：' . $merchant_no . ' ,下次请使用商户号登录，手机号将回收
        } else {
            return json_encode([
                'status' => -1,
                'message' => '获取商户信息失败',
                'data' => null
            ]);
        }

    }

    //重置登录密码
    public function reset_store_pwd(Request $request)
    {
        try {

            $merchant_id = $request->get('merchant_id', '');

            $Merchant = Merchant::where('id', $merchant_id)
                ->first();
            if ($Merchant) {
                $new_pwd = '000000';
                $dataIN = [
                    'password' => bcrypt($new_pwd),
                ];
                $res = $Merchant->update($dataIN);
                if ($res) {
                    $data = [
                        'status' => 1,
                        'message' => '密码重置成功',
                    ];
                } else {
                    $data = [
                        'status' => 2,
                        'message' => '密码重置失败',
                    ];
                }

                return json_encode($data);
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '用户不存在或状态异常'
                ]);
            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }
}
