<?php

namespace App\Api\Controllers\User;


use App\Api\Controllers\BaseController;
use App\Models\AlipayAppOauthUsers;
use App\Models\CcBankPayStore;
use App\Models\DlbStore;
use App\Models\DlbStoreUpload;
use App\Models\EasypayConfig;
use App\Models\EasypayStore;
use App\Models\EasySkpayStore;
use App\Models\HltxStore;
use App\Models\HkrtStore;
use App\Models\HStore;
use App\Models\HwcpayStore;
use App\Models\LianfuyoupayStores;
use App\Models\LinkageStores;
use App\Models\LklStore;
use App\Models\Merchant;
use App\Models\MerchantStore;
use App\Models\MyBankStore;
use App\Models\MyBankStoreTem;
use App\Models\NewLandStore;
use App\Models\Order;
use App\Models\PostPayStore;
use App\Models\QfpayStore;
use App\Models\QrListInfo;
use App\Models\Store;
use App\Models\StoreMonthOrder;
use App\Models\StorePayWay;
use App\Models\TfStore;
use App\Models\User;
use App\Models\UserWalletDetail;
use App\Models\VbillaStore;
use App\Models\VbillStore;
use App\Models\WeixinaStore;
use App\Models\WeixinStore;
use App\Models\WftpayStore;
use App\Models\YinshengStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Mybank\Mybank;


class SelectController extends BaseController
{
    //下级拓展码
    public function s_code_url(Request $request)
    {
//        $s_code = $request->get('s_code', '');
//        $user_info = User::where('s_code', $s_code)
//            ->where('is_delete', '0')
//            ->first();
//        if (!$user_info) {
//            $message = '用户不存在或状态被禁用';
//             return view('errors.page_errors', compact('message'));
//        }
//        $data = [
//            'pid' => $user_info->id,
//            'pid_name' => $user_info->name,
//            'level' => $user_info->level + 1,
//            'profit_ratio' => $user_info->sub_profit_ratio
//        ];
//
//        return view('expansioncode.wechat', compact('data'));

        $message = '暂时无法添加,请使用其他方式';
        return view('errors.page_errors', compact('message'));

    }

    public function sub_code_url(Request $request)
    {
        $message = '暂时无法添加,请使用其他方式';
        return view('errors.page_errors', compact('message'));

    }


    //门店交易统计（交易榜）
    public function ranking(Request $request)
    {
        try {
            $token = $this->parseToken(); //用户信息
            $user_id = $request->get('user_id', '');
            $type = $request->get('type', '1'); //排序：1-交易笔数,由大到小;2-交易笔数由小到大;3-交易金额由大到小;4-交易金额由小到大
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');

            //限制时间
            if (empty($time_end)) {
                $time_end = date('Y-m-d H:i:s', time());
            } else {
                if ($time_start != $time_end) {
                    $date = ((strtotime($time_end) > strtotime($time_start))) ? true : false;
                    if (!$date) {
                        return json_encode([
                            'status' => 2,
                            'message' => '开始时间不能超过结束时间'
                        ]);
                    }
                }
                $time_end = "$time_end 23:59:59";
            }
            $time_start = "$time_start 00:00:00";

            if ($user_id) {
                $user_ids = $this->getSubIds($user_id);
            } else {
                $user_ids = $this->getSubIds($token->user_id);
            }

            if ($type == "1" || $type == "2") {
                $sort = 'desc';
                if ($type == "2") {
                    $sort = 'asc';
                }
                $order_field = "COUNT(`o`.`total_amount`)";
            } else {
                $sort = 'desc';
                if ($type == "4") {
                    $sort = 'asc';
                }

                $order_field = "SUM(`o`.`total_amount`)";
            }

            $table = date('Y', strtotime($time_start));
            if (env('DB_D1_HOST')) {
                if (Schema::hasTable("orders$table")) {
                    $ordersObj = DB::connection("mysql_d1")->table("orders$table  as o");
                } else {
                    $ordersObj = DB::connection("mysql_d1")->table('orders  as o');
                }
            } else {
                if (Schema::hasTable("orders$table")) {
                    $ordersObj = DB::table("orders$table  as o");
                } else {
                    $ordersObj = DB::table('orders as o');
                }
            }

            $obj = $ordersObj->select([
                'o.store_id',
                'o.store_name',
                'o.created_at',
                DB::raw("s.created_at AS `store_created_at`"),
                DB::raw("SUM(`o`.`total_amount`) AS `total_amount`, COUNT(`o`.`total_amount`) AS `total_count`"),
                DB::raw("SUM(`o`.`mdiscount_amount`) AS `mdiscount_amount`"),
                DB::raw("SUM(IF(`o`.`ways_source`='alipay', `total_amount`, 0)) AS `alipay_amount`"),
                DB::raw("COUNT(IF(`o`.`ways_source`='alipay', true, null)) AS `alipay_count`"),
                DB::raw("SUM(IF(`o`.`ways_source`='weixin', `total_amount`, 0)) AS `weixin_amount`"),
                DB::raw("COUNT(IF(`o`.`ways_source`='weixin', true, null)) AS `weixin_count`"),
                DB::raw("COUNT(IF(`o`.`pay_method`='alipay_face', true, null)) as `alipay_face_count`"),
                DB::raw("COUNT(IF(`o`.`pay_method`='weixin_face', true, null)) as `weixin_face_count`")
            ])
                ->leftjoin('stores as s', 's.store_id', '=', 'o.store_id')
                ->whereIn('o.user_id', $user_ids)
                ->where('o.pay_status', 1)
                ->where('o.created_at', '>', $time_start)
                ->where('o.created_at', '<', $time_end)
                ->groupBy('o.store_id')
                ->orderBy(DB::raw($order_field), $sort);

            $this->t = $obj->paginate(10)->total();
            $data = $this->page($obj)->get();
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine()
            ]);
        }

        $this->status = 1;
        $this->message = '数据返回成功';
        return $this->format($data);
    }


    //门店交易量
    public function volume(Request $request)
    {

        try {
            $user = $this->parseToken(); //用户信息
            $user_id = $request->get('user_id', '');
            $type = $request->get('type', '1'); //排序：1-交易笔数,由大到小;2-交易笔数由小到大;
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');

            if (!isset($time_start) && empty($time_start)) {
                $time_start = date('Y-m-d 00:00:00', time());
            } else {
                $time_start = "$time_start 00:00:00";
            }

            if (!isset($time_end) && empty($time_end)) {
                $time_end = date('Y-m-d H:i:s', time());
            } else {
                $time_end = "$time_end 23:59:59";
            }

            //限制时间
            $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
            $day = 31;
            if ($date > $day) {
                return json_encode([
                    'status' => 2,
                    'message' => '时间筛选不能超过' . $day . '天'
                ]);
            }

            if ($time_start != $time_end) {
                $date = ((strtotime($time_end) > strtotime($time_start))) ? true : false;
                if (!$date) {
                    return json_encode([
                        'status' => 2,
                        'message' => '开始时间不能超过结束时间'
                    ]);
                }
            }

            if ($type == "1") {
                $sort = 'desc';
                $order_field = "count_order";
            } else {
                $sort = 'asc';
                $order_field = "count_order";
            }

            if (env('DB_D1_HOST')) {
                $Stores_obj = DB::connection("mysql_d1")->table('stores');
            } else {
                $Stores_obj = DB::table('stores');
            }
            $obj = $Stores_obj->select([
                'stores.store_name',
                'stores.store_id',
                'stores.store_short_name',
                'stores.people_phone',
                DB::raw("COUNT(`o`.`id`) as `count_order`")
            ]);

            $table = date('Y', strtotime($time_start));
            if (Schema::hasTable("orders$table")) {
                $ordersObj = "orders$table as o";
            } else {
                $ordersObj = 'orders as o';
            }

            $obj = $obj->leftJoin($ordersObj, function ($join) use ($time_start, $time_end) {
                $join->on('stores.store_id', '=', 'o.store_id');
                $join->where('o.created_at', '>', "$time_start");
                $join->where('o.created_at', '<', "$time_end");
            });

            $obj = $obj->groupBy('stores.store_id')
//                ->where('o.pay_status', 1)
//                ->where('o.created_at', '>', $time_start)
//                ->where('o.created_at', '<', $time_end)
                ->orderBy(DB::raw($order_field), $sort);

            $this->t = $obj->paginate(10)->total();
            $data = $this->page($obj)->get();
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine()
            ]);
        }

        $this->status = 1;
        $this->message = '数据返回成功';
        return $this->format($data);
    }


//    public function ranking(Request $request)
//    {
//
//        try {
//            $user = $this->parseToken();//
//            $type = $request->get('type', '1');//1 交易笔数 由大到小
//            $month = $request->get('month', '');
//
//
//            if (env('DB_D1_HOST')) {
//                $obj = DB::connection("mysql_d1")->table("stores");
//            } else {
//                $obj = DB::table('stores');
//            }
//
//            //1 交易笔数
//            if ($type == "1" || $type == "2") {
//                $sort = 'desc';
//                if ($type == "2") {
//                    $sort = 'asc';
//                }
//
//                $obj = $obj->join('store_month_counts', 'stores.store_id', 'store_month_counts.store_id')
//                    ->where('store_month_counts.month', $month)
//                    ->orderBy('store_month_counts.total_count', $sort)
//                    ->select('store_month_counts.*', 'stores.store_name');
//            } else {
//                $sort = 'desc';
//                if ($type == "4") {
//                    $sort = 'asc';
//                }
//                $obj = $obj->join('store_month_counts', 'stores.store_id', 'store_month_counts.store_id')
//                    ->where('store_month_counts.month', $month)
//                    ->orderBy('store_month_counts.total_amount', $sort)
//                    ->select('store_month_counts.*', 'stores.store_name');
//
//            }
//
//
//            $this->t = $obj->count();
//            $data = $this->page($obj)->get();
//            $this->status = 1;
//            $this->message = '数据返回成功';
//            return $this->format($data);
//
//
//        } catch (\Exception $exception) {
//            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
//        }
//
//    }


    //打款查询
    public function dk_select(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');//1 交易笔数 由大到小
            $time = $request->get('time', '');
            $company = $request->get('company', '');

            $check_data = [
                'company' => '通道类型',
                'time' => '打款时间',
                'store_id' => '门店ID'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $store = Store::where('store_id', $store_id)
                ->select('store_name')
                ->first();
            if (!$store) {
                return json_encode(['status' => 2, 'message' => '门店ID不存在']);
            }
            $store_name = $store->store_name;

            return json_encode([
                'status' => 1,
                'message' => '查询成功',
                'data' => [
                    'store_name' => $store_name,
                    'time' => $time,
                    'total_amount' => '0.01',
                    'dk_time' => $time,
                    'dk_desc' => '打款成功'
                ]
            ]);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }


    //代理商上级查询
    public function select_pid_user(Request $request)
    {
        try {
            $user_name = $request->get('user_name', '');
            $user = User::orWhere('name', $user_name)
                ->orWhere('phone', $user_name)
                ->select('level', 'id', 'name', 'phone', 'pid', 's_code', 'money', 'is_zero_rate', 'profit_ratio', 'logo')
                ->first();
            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '用户不存在'
                ]);
            }

            $user_obj = $user;

            $pid = $user->pid;
            $user_name = $user->name . '(' . $user->phone . ')';
            for ($i = 0; $i < $user->level + 2; $i++) {
                if ($pid == 0) {
                    break;
                }
                $user = User::where('id', $pid)
                    ->select('level', 'id', 'name', 'phone', 'pid', 's_code', 'money')
                    ->first();

                $pid = $user->pid;
                $user_name = $user_name . '=>' . $user->name . '(' . $user->phone . ')';
            }

            return json_encode([
                'status' => 1,
                'message' => '数据返回成功',
                'data' => [
                    'info' => $user_name,
                    'user_info' => $user_obj
                ]
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'message' => $exception->getMessage() . ' | ' . $exception->getLine()
            ]);
        }
    }


    //清除通道
    public function clear_ways_type(Request $request)
    {
        try {
            $company = $request->get('company', '');
            $store_id = $request->get('store_id', '');
            $user = $this->parseToken();

//            $user = User::where('id', $user->user_id)->first();
//            $hasPermission = $user->hasPermissionTo('清除通道');
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '没有权限清除通道']);
//            }

            //传化
            if ($company == "tfpay") {
                $TfStore = TfStore::where('store_id', $store_id)->delete();
                $StorePayWay = StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();
                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }

            //支付宝
            if ($company == "alipay") {
                $AlipayAppOauthUsers = AlipayAppOauthUsers::where('store_id', $store_id)->delete();
                $StorePayWay = StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();
                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }

            //和融通
            if ($company == "herongtong") {
                $TfStore = HStore::where('store_id', $store_id)->delete();
                $StorePayWay = StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();
                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }

            //新大陆
            if ($company == "newland") {
                $NewLandStore = NewLandStore::where('store_id', $store_id)->delete();
                $StorePayWay = StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();
                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }

            //快钱
            if ($company == "mybank") {
                $MyBankStore = MyBankStoreTem::where('OutMerchantId', $store_id)->delete();
                $MyBankStore = MyBankStore::where('OutMerchantId', $store_id)->delete();
                $StorePayWay = StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();
                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }

            //微信官方
            if ($company == "weixin") {
                $NewLandStore = WeixinStore::where('store_id', $store_id)->delete();
                $StorePayWay = StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();

                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }

            //随行付
            if ($company == "vbill") {
                VbillStore::where('store_id', $store_id)->delete();
                StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();

                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }

            //哆啦宝
            if ($company == "dlb") {
                DlbStoreUpload::where('store_id', $store_id)->delete();
                DlbStore::where('store_id', $store_id)->delete();
                StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();

                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }

            //邮政
            if ($company == "lianfuyoupay") {
                $lianFuYouPayStores = LianfuyoupayStores::where('store_id', $store_id)->delete();
                $StorePayWay = StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();

                return json_encode([
                    'status' => 1,
                    'message' => '邮政通道删除成功'
                ]);
            }

            //葫芦天下
            if ($company == "hltx") {
                $HltxStore = HltxStore::where('store_id', $store_id)->delete();
                $StorePayWay = StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();

                return json_encode([
                    'status' => 1,
                    'message' => 'hl删除成功'
                ]);
            }

            //随行付a
            if ($company == "vbilla") {
                VbillaStore::where('store_id', $store_id)->delete();
                StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();

                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }

            //海科融通
            if ($company == "hkrt") {
                $HkrtStore = HkrtStore::where('store_id', $store_id)->delete();
                $StorePayWay = StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();

                return json_encode([
                    'status' => '1',
                    'message' => '删除成功'
                ]);
            }

            //易生支付
            if ($company == "easypay") {
                EasypayStore::where('store_id', $store_id)->delete();
                StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();
                EasypayConfig::where('config_id', '1234')->update(['getSignKey' => '']); //清除签名密钥

                return json_encode([
                    'status' => '1',
                    'message' => '删除成功'
                ]);
            }

            //联动优势
            if ($company == "linkage") {
                LinkageStores::where('store_id', $store_id)->delete();
                StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();

                return json_encode([
                    'status' => '1',
                    'message' => '删除成功'
                ]);
            }

            //微信官方a
            if ($company == "weixina") {
                $NewLandStore = WeixinaStore::where('store_id', $store_id)->delete();
                $StorePayWay = StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();

                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }

            //威富通
            if ($company == "wftpay") {
                $NewLandStore = WftpayStore::where('store_id', $store_id)->delete();
                $StorePayWay = StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();

                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }

            //汇旺财
            if ($company == "hwcpay") {
                $NewLandStore = HwcpayStore::where('store_id', $store_id)->delete();
                $StorePayWay = StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();

                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }

            //银盛
            if ($company == "ysepay") {
                $NewLandStore = YinshengStore::where('store_id', $store_id)->delete();
                $StorePayWay = StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();

                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }

            //钱方
            if ($company == "qfpay") {
                $NewLandStore = QfpayStore::where('store_id', $store_id)->delete();
                $StorePayWay = StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();

                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }

            //建行
            if ($company == "ccbankpay") {
                CcBankPayStore::where('store_id', $store_id)->delete();
                StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();

                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }
            //邮驿付
            if ($company == "postpay") {
                PostPayStore::where('store_id', $store_id)->delete();
                StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();

                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }
            //易生数科
            if ($company == "easyskpay") {
                EasySkpayStore::where('store_id', $store_id)->delete();
                StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();

                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }
            //拉卡拉
            if ($company == "lklpay") {
                LklStore::where('store_id', $store_id)->delete();
                StorePayWay::where('company', $company)->where('store_id', $store_id)->delete();

                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }

            return json_encode([
                'status' => 2,
                'message' => '暂不支持清除此通道'
            ]);
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '2',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ]);
        }
    }


    /**
     * 根据门店ID查看收银员列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function merchant_lists(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $name = $request->get('name', '');

            $where = [];
            if ($name) {
                $where[] = ['merchants.name', 'like', '%' . $name . '%'];
            }
            if ($store_id) {
                $where[] = ['stores.store_id', '=', $store_id];
            }

            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("merchant_stores");
            } else {
                $obj = DB::table('merchant_stores');
            }

            $obj->join('merchants', 'merchant_stores.merchant_id', '=', 'merchants.id')
                ->join('stores', 'merchant_stores.store_id', '=', 'stores.store_id')
                ->where($where)
                ->select('stores.store_name', 'merchants.id as merchant_id', 'merchants.name', 'merchants.phone', 'merchants.type', 'merchants.logo', 'merchants.wx_logo');

            $this->t = $obj->count();
            $data = $this->page($obj)->get();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }


    /**
     * 添加收银员
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add_merchant(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $type = $request->get('type', '2');
            $name = $request->get('name', '');
            $phone = $request->get('phone', '');
            $password = $request->get('password', '');
            $data = $request->except(['token']);

            if ($phone == "") {
                $this->status = 2;
                $this->message = '手机号必填';
                return $this->format();
            }

            $rules = [
                'phone' => 'required|min:11|max:11',
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                return json_encode([
                    'status' => 2,
                    'message' => '手机号位数不正确',
                ]);
            }

            $stores = Store::where('store_id', $store_id)
                ->first();
            if (!$stores) {
                $this->status = 2;
                $this->message = '门店不存在';
                return $this->format();
            }

            if ($password == "") {
                $password = '000000';
            }
            if ($name == "") {
                $this->status = 2;
                $this->message = '姓名必填';
                return $this->format();
            }

            //验证密码
            if (strlen($password) < 6) {
                return json_encode([
                    'status' => 2,
                    'message' => '密码长度不符合要求'
                ]);
            }

            $rules = [
                'phone' => 'required|unique:merchants',
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                return json_encode([
                    'status' => 2,
                    'message' => '手机号已经注册'
                ]);
            } else {
                //80开头+当前日期+4位随机数
                $merchantNo = $this->generateMerchantNo();
                //检查是否存在商户号
                $existMerchantNo = Merchant::where('merchant_no', $merchantNo)->first();
                if (!$existMerchantNo) {
                    $merchantNo = $this->generateMerchantNo(); //重新获取
                }

                $dataIN = [
                    'pid' => $stores->merchant_id ? $stores->merchant_id : "0",
                    'type' => $type,
                    'name' => $name,
                    'email' => '',
                    'password' => bcrypt($password),
                    'phone' => $phone,
                    'user_id' => $stores->user_id,//推广员id
                    'config_id' => $stores->config_id,
                    'wx_openid' => '',
                    'source' => $stores->source, //来源,01:畅立收，02:河南畅立收
                    'merchant_no' => $merchantNo
                ];
                $insert = Merchant::create($dataIN);
                $merchant_id = $insert->id;
            }

            MerchantStore::create([
                'store_id' => $store_id,
                'merchant_id' => $merchant_id,
            ]);

            $this->status = 1;
            $this->message = '收银员添加成功';
            return $this->format();
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }


    /**
     * 绑定收银员
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bind_merchant(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id');
            $type = $request->get('type');
            $merchant_id = $request->get('merchant_id');

            $check_data = [
                'store_id' => '门店ID',
                'type' => '角色类型',
                'merchant_id' => '收银员'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $MerchantStore = MerchantStore::where('store_id', $store_id)
                ->where('merchant_id', $merchant_id)
                ->first();
            if ($MerchantStore) {
                return json_encode([
                    'status' => 2,
                    'message' => '收银员已经绑定无需重复绑定'
                ]);
            }

            MerchantStore::create([
                'store_id' => $store_id,
                'merchant_id' => $merchant_id,
                'type' => $type,
            ]);

            $this->status = 1;
            $this->message = '收银员绑定成功';
            return $this->format();
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    /**
     * 修改收银员
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function up_merchant(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $merchant_id = $request->get('merchant_id');
            $store_id = $request->get('store_id');
            $name = $request->get('name', '');
            $password = $request->get('password', '');
            $data = $request->except(['token', 'merchant_id', 'store_id', 'password']);

            if ($password == "") {
                $password = '000000';
            }
            if ($name == "") {
                $this->status = 2;
                $this->message = '姓名必填';
                return $this->format();
            }
            if ($password && $password != '000000') {
                //验证密码
                if (strlen($password) < 6) {
                    return json_encode([
                        'status' => 2,
                        'message' => '密码长度不符合要求'
                    ]);
                }

                $data['password'] = bcrypt($password);
            }


            $dataIN = $data;

            $Merchant = Merchant::where('id', $merchant_id)
                ->select('phone')
                ->first();

            $phone = $Merchant->phone;
            if ($phone != $data['phone']) {
                $rules = [
                    'phone' => 'required|unique:merchants',
                ];
                $validator = Validator::make($data, $rules);
                if ($validator->fails()) {

                    return json_encode([
                        'status' => 2,
                        'message' => '手机号已经注册'
                    ]);
                }
            }

            Merchant::where('id', $merchant_id)
                ->update($dataIN);

            $this->status = 1;
            $this->message = '收银员修改成功';

            return $this->format();
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    /**
     * 删除收银员
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function del_merchant(Request $request)
    {
        try {
            $user = $this->parseToken();
            $merchant_id = $request->get('merchant_id', '');
            $store_id = $request->get('store_id', '');

            $check_data = [
                'merchant_id' => '收银员',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }


//            $user = User::where('id', $user->user_id)->first();
//            $hasPermission = $user->hasPermissionTo('删除收银员');
//
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '没有权限']);
//            }


            if ($merchant_id == "") {
                $this->status = 2;
                $this->message = '请选择收银员';
                return $this->format();
            }
            $Merchant = Merchant::where('id', $merchant_id)->first();

            if (!$Merchant) {
                $this->status = 2;
                $this->message = '收银员不存在';
                return $this->format();
            }

            if ($store_id) {
                //删除收银员关联表
                $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)
                    ->where('store_id', $store_id)
                    ->delete();
            } else {
                //删除收银员关联表
                $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)
                    ->delete();
            }

            $Merchant->delete();


            $this->status = 1;
            $this->message = '收银员删除成功';
            return $this->format();


        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }


    /**
     * 单个收银员信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function merchant_info(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $merchant_id = $request->get('merchant_id');
            $store_id = $request->get('store_id', '');

            $check_data = [
                'merchant_id' => '收银员',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }


            $data = Merchant::where('id', $merchant_id)->first();
            if (!$data) {
                $this->status = 2;
                $this->message = '收银员不存在';
                return $this->format();
            }


            //门店id存在
            if ($store_id) {
                //收银员的收款聚合码
                $data->pay_qr = url('/qr?store_id=' . $store_id . '&merchant_id=' . $merchant_id . '');
            }

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);


        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }

    }

    //app首页获取当日数据
    public function getIndexDayData(Request $request)
    {
        try {

            $user_id = $request->get('user_id');
            $check_data = [
                'user_id' => '代理id',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $users = User::where('pid', $user_id)->select('id')->get()
                ->toArray();
            $subIDs = array_column($users, 'id');
            $userID = [$user_id];
            $ids = $this->getSubIdsAll($user_id);

            //Log::info($str_ids);
            $start_time = date('Y-m-d 00:00:00', time());
            $end_time = date('Y-m-d 23:59:59', time());
            $qrListInfos = [];
            if ($user_id == 1) {
                $qrListInfos = QrListInfo::where('status', '1')
                    ->where('updated_at', '>=', $start_time)
                    ->where('updated_at', '<=', $end_time)
                    ->select('id')
                    ->get();
                //新增代理数量
                $users = User::where('is_delete', '0')
                    ->where('created_at', '>=', $start_time)
                    ->where('created_at', '<=', $end_time)
                    ->select('id')
                    ->get();
                //当月交易量

                $table = DB::table('orders');
                $order_obj = $table->select([
                    DB::raw("SUM(`total_amount`) as `total_amount_sum`"),
                ])
                    ->where('created_at', '>=', $start_time)
                    ->where('created_at', '<=', $end_time)
                    ->where('pay_status', 1)
                    ->first();
            } else {
                $qrListInfos = QrListInfo::where('status', '1')
                    ->where('updated_at', '>=', $start_time)
                    ->where('updated_at', '<=', $end_time)
                    ->whereIn('user_id', $ids)
                    ->select('id')
                    ->get();
                //新增代理数量
                $users = User::where('is_delete', '0')
                    ->where('created_at', '>=', $start_time)
                    ->where('created_at', '<=', $end_time)
                    ->whereIn('pid', $ids)
                    ->select('id')
                    ->get();
                $table = DB::table('orders');
                $order_obj = $table->select([
                    DB::raw("SUM(`total_amount`) as `total_amount_sum`"),
                ])
                    ->where('created_at', '>=', $start_time)
                    ->where('created_at', '<=', $end_time)
                    ->where('pay_status', 1)
                    ->whereIn('user_id', $ids)
                    ->first();

            }

            $activation_num = count($qrListInfos);
            //Log::info($ids);
            $users_num = count($users);
            //当月交易量

            $transaction_sum = $order_obj->total_amount_sum;
            if (!$transaction_sum) {
                $transaction_sum = 0.00;
            }
            $data = [
                'activation_num' => $activation_num,
                'users_num' => $users_num,
                'transaction_sum' => $transaction_sum
            ];
            return json_encode([
                'status' => 1,
                'message' => '获取成功',
                'data' => $data
            ]);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    //app首页获取当月数据
    public function getIndexMonthData(Request $request)
    {
        try {

            $user_id = $request->get('user_id');
            $check_data = [
                'user_id' => '代理id',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $start_time = date('Y-m-1 00:00:00', time());
            $end_time = date('Y-m-d 23:59:59', strtotime(date('Y-m-01', time()) . ' 1 month -1 day'));

            $ids = $this->getSubIdsAll($user_id); //循环获取下级的用户id

//            $settlementLists = DB::table('settlement_lists');
//            $obj = $settlementLists->select([
//                'commission_amount',
//                DB::raw("SUM(`commission_amount`) as `amount_sum`")
//            ])->whereIn('user_id', $ids)
//                ->where('updated_at', '>=', $start_time)
//                ->where('updated_at', '<=', $end_time)
//                ->first();
//            $settlementListM = DB::table('settlement_month_list');
//            $month_obj = $settlementListM->select([
//                'total_amount',
//                DB::raw("SUM(`total_amount`) as `amount_sum`")
//            ])->whereIn('user_id', $ids)
//                ->where('updated_at', '>=', $start_time)
//                ->where('updated_at', '<=', $end_time)
//                ->first();
//
//            $qrUserRewardInfo = DB::table('qr_user_return_amount_info');
//            $userReward_obj = $qrUserRewardInfo->select([
//                'amount',
//                DB::raw("SUM(`amount`) as `reward_amount`")
//            ])->whereIn('user_id', $ids)
//                ->where('updated_at', '>=', $start_time)
//                ->where('updated_at', '<=', $end_time)
//                ->first();
//            $user_income = $obj->amount_sum + $month_obj->amount_sum + $userReward_obj->reward_amount;
            //$user_income = $obj->amount_sum + 0 + $userReward_obj->reward_amount;
            //Log::info($str_ids);
            //查询激活商户量
            $qrListInfos = QrListInfo::where('status', '1')
                ->where('updated_at', '>=', $start_time)
                ->where('updated_at', '<=', $end_time)
                ->whereIn('user_id', $ids)
                ->select('id')
                ->get();
            //新增代理数量
            $users = User::where('is_delete', '0')
                ->where('created_at', '>=', $start_time)
                ->where('created_at', '<=', $end_time)
                ->whereIn('pid', $ids)
                ->select('id')
                ->get();
            $table = DB::table('orders');
            $order_obj = $table->select([
                DB::raw("SUM(`total_amount`) as `total_amount_sum`"),
            ])
                ->where('created_at', '>=', $start_time)
                ->where('created_at', '<=', $end_time)
                ->where('pay_status', 1)
                ->whereIn('user_id', $ids)
                ->first();
            $activation_num = count($qrListInfos);
            //新增代理数量
            $users_num = count($users);
            //今日交易量

            $transaction_sum = $order_obj->total_amount_sum;
            if (!$transaction_sum) {
                $transaction_sum = 0.00;
            }
            $user_income = $transaction_sum * 0.001;
            $data = [
                'activation_num' => $activation_num,
                'users_num' => $users_num,
                'transaction_sum' => $transaction_sum,
                'user_income' => $user_income
            ];
            return json_encode([
                'status' => 1,
                'message' => '获取成功',
                'data' => $data
            ]);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    public function app_get_sub_users(Request $request)
    {
        try {
            $token = $this->parseToken();
            $login_user_id = $token->user_id;
            $user_id = $request->get('user_id', $login_user_id);
            $self = $request->get('self', false);

            $users = User::where('pid', $user_id)->select('id')->get()
                ->toArray();
            $subIDs = array_column($users, 'id');
            $userID = [$user_id];
            $User_id_s = array_unique(array_merge($userID, $subIDs));

            $return_type = $request->get('return_type', '');
            $user_name = $request->get('user_name', '');
            $sub_type = $request->get('sub_type', '');
            $level = $request->get('level', '');
            $Display = $request->get('display', ''); //是否全部显示手机号

            $ty = $request->get('ty', '');

            $where = [];
            $where[] = ['is_delete', '=', 0];
            if ($user_name) {
                $where[] = ['name', 'like', '%' . $user_name . '%'];
            } else {
                if ($level) {
                    $where[] = ['level', '=', $level];
                }
            }

            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("users");
            } else {
                $obj = DB::table('users');
            }

            if ($sub_type == "1") {
                $where[] = ['level', '<', '2'];
            }

            if ($return_type == "layui") {
                $Users = User::where('id', $login_user_id)
                    ->where($where)
                    ->with('children')
                    ->select('id', 'pid', 'money', 'name', 'phone', 'level', 's_code', 'logo', 'profit_ratio', 'sub_profit_ratio', 'is_withdraw', 'source', 'is_zero_rate')
                    ->get()->toArray();

                if (!$Display) {
                    array_walk_recursive($Users, function (&$values, $keys) {
                        if ($keys == 'phone') {
                            $values = substr($values, 0, 3) . '****' . substr($values, 7); //手机号加盐
                        }

                        return $values;
                    });
                }

                return json_encode([
                    'status' => '1',
                    'data' => $Users
                ]);
            }
            if ($ty == 'admin') {
                $obj = User::where('id', $login_user_id)
                    ->where('level', '<=', '3')
                    ->with('children');
                $data = $obj->get();
            } else {
                $obj = $obj->where($where)
                    ->whereIn('id', $User_id_s);
                $data = $obj->get();
            }
            $sql = $obj->tosql();

            return json_encode([
                'status' => 1,
                'message' => '数据返回成功',
                'data' => $data,
                'sql' => $sql
            ]);
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    //获取代理总分润 app
    public function getIncome(Request $request)
    {

        try {
            $public = $this->parseToken();
            $user_id = $request->get('user_id', '');

            $settlementLists = DB::table('settlement_lists');
            $obj = $settlementLists->select([
                'commission_amount',
                DB::raw("SUM(`commission_amount`) as `amount_sum`")
            ])->where('user_id', $user_id)
                ->first();

            $settlementListM = DB::table('settlement_month_list');
            $month_obj = $settlementListM->select([
                'total_amount',
                DB::raw("SUM(`total_amount`) as `amount_sum`"),
            ])->where('user_id', $user_id)
                ->where('settlement_type', '1')
                ->first();
            $settlementListM_p = DB::table('settlement_month_list');
            $month_obj_p = $settlementListM_p->select([
                'total_amount',
                DB::raw("SUM(`total_amount`) as `amount_sum`"),
            ])->where('p_user_id', $user_id)
                ->where('settlement_type', '2')
                ->first();
            $month_amount_sum = 0;
            if ($month_obj->amount_sum) {
                $month_amount_sum = $month_obj->amount_sum;
            }
            $month_amount_sum_p = 0;
            if ($month_obj_p->amount_sum) {
                $month_amount_sum_p = $month_obj_p->amount_sum;
            }
            $month_amount_sum = $month_amount_sum + $month_amount_sum_p;
            $qrUserRewardInfo = DB::table('qr_user_reward_info');
            $userReward_obj = $qrUserRewardInfo->select([
                'award_amount',
                DB::raw("SUM(`activation_integral`) as `reward_amount`")
            ])->where('user_id', $user_id)
                ->first();

            $returnAmountInfo = DB::table('qr_user_return_amount_info');
            $returnAmount_obj = $returnAmountInfo->select([
                'amount', 'integral',
                DB::raw("SUM(`amount`) as `amount`"),
                DB::raw("SUM(`integral`) as `integral`")
            ])->where('user_id', $user_id)
                ->first();

            $userTrainReward = DB::table('user_train_reward');
            $userTrainReward_obj = $userTrainReward->select([
                'train_reward_amount',
                DB::raw("SUM(`train_reward_amount`) as `train_reward_amount`"),
            ])->where('user_id', $user_id)
                ->first();

            $userShoppingReward = DB::table('user_shopping_reward');
            $userShoppingReward_obj = $userShoppingReward->select([
                'amount',
                DB::raw("SUM(`amount`) as `user_shopping_reward`"),
            ])->where('p_user_id', $user_id)
                ->first();

            $userSubTrainReward = DB::table('user_sub_train_reward');
            $userSubTrainReward_obj = $userSubTrainReward->select([
                'amount',
                DB::raw("SUM(`amount`) as `sub_train_reward_amount`"),
            ])->where('p_user_id', $user_id)
                ->where('status', '1')
                ->first();

            $userBoxRebateInfo = DB::table('user_box_rebate_info');
            $userBoxRebateInfo_obj = $userBoxRebateInfo->select([
                'amount',
                DB::raw("SUM(`amount`) as `user_box_rebate_amount`"),
            ])->where('user_id', $user_id)
                ->first();

            $userArchitectureReward = DB::table('user_architecture_rewards');
            $userArchitectureReward_obj = $userArchitectureReward->select([
                'reward_amount',
                DB::raw("SUM(`reward_amount`) as `user_architecture_reward`"),
            ])->where('user_id', $user_id)
                ->first();

            $data = [
                'day_amount_sum' => $obj->amount_sum ? $obj->amount_sum : 0.00,//日分润
                'month_amount_sum' => $month_amount_sum,//月分润
                'reward_amount' => $userReward_obj->reward_amount ? $userReward_obj->reward_amount : 0.00,//激活奖励总金额
                'return_amount' => $returnAmount_obj->amount ? $returnAmount_obj->amount : 0.00,
                'return_integral' => $returnAmount_obj->integral ? $returnAmount_obj->integral : 0.00,
                'train_reward_amount' => $userTrainReward_obj->train_reward_amount ? $userTrainReward_obj->train_reward_amount : 0.00,
                'user_shopping_reward' => $userShoppingReward_obj->user_shopping_reward ? $userShoppingReward_obj->user_shopping_reward : 0.00,
                'sub_train_reward_amount' => $userSubTrainReward_obj->sub_train_reward_amount ? $userSubTrainReward_obj->sub_train_reward_amount : 0.00,
                'user_box_rebate_amount' => $userBoxRebateInfo_obj->user_box_rebate_amount ? $userBoxRebateInfo_obj->user_box_rebate_amount : 0.00,
                'user_architecture_reward' => $userArchitectureReward_obj->user_architecture_reward ? $userArchitectureReward_obj->user_architecture_reward : 0.00,
            ];
            return json_encode([
                'status' => 1,
                'message' => '获取成功',
                'data' => $data
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //获取代理分润明细 app
    public function getUserWalletDetails(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $user_id = $request->get('user_id', '');
            $commission_type = $request->get('commission_type', '');

            $check_data = [
                'user_id' => 'user_id',
                'commission_type' => 'commission_type'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            if ($commission_type == '1') {
                $obj = DB::table('settlement_lists');
                $obj = $obj->where('user_id', $user_id)
                    ->select('*');
                $this->message = '数据返回成功';
                $this->t = $obj->count();
            } else {
                $obj = DB::table('settlement_month_list');
                $obj = $obj->where('user_id', $user_id)
                    ->where('settlement_type', '1')
                    ->select('user_name', 'total_amount', 'transaction_amount', 'created_at', 'settlement_type')
                    ->unionAll(DB::table('settlement_month_list')->where('p_user_id', $user_id)
                        ->where('settlement_type', '2')
                        ->select('user_name', 'total_amount', 'transaction_amount', 'created_at', 'settlement_type'));
                $obj_count = count($obj->get());
                $this->message = '数据返回成功';
                $this->t = $obj_count;

            }
            $data = $this->page($obj)->orderBy('created_at', 'desc')->get();
            return $this->format($data);

        } catch (\Exception $exception) {
            return json_encode([
                'status' => 0,
                'message' => $exception->getMessage() . ' | ' . $exception->getLine()
            ]);
        }

    }

    //获取代理奖励明细 app
    public function getUserRewardList(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $user_id = $request->get('user_id', '');
            $terminal = $request->get('terminal', '');
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');

            $check_data = [
                'user_id' => 'user_id',
            ];

            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $where = [];
            if ($time_start) {
                $where[] = ['created_at', '>=', $time_start];
            }

            if ($time_end) {
                $where[] = ['created_at', '<=', $time_end];
            }
            $obj = DB::table('qr_user_reward_info');
            if ($terminal == 'pc' && $user_id == 1) {
                $obj = $obj->where($where)
                    ->select('*');
            } else {
                $obj = $obj->where('user_id', $user_id)
                    ->select('*');
            }

            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $data = $this->page($obj)->orderBy('created_at', 'desc')->get();
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 0,
                'message' => $exception->getMessage() . ' | ' . $exception->getLine()
            ]);
        }

    }

    //获取代理交易达标奖励 app
    public function getUserReturnAmountListApp(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $user_id = $request->get('user_id', '');
            $terminal = $request->get('terminal', '');
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');

            $check_data = [
                'user_id' => 'user_id',
            ];

            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $obj = DB::table('qr_user_return_amount_info');
            $obj = $obj->where('user_id', $user_id)
                ->select('*');

            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $data = $this->page($obj)->orderBy('created_at', 'desc')->get();
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 0,
                'message' => $exception->getMessage() . ' | ' . $exception->getLine()
            ]);
        }

    }

    //获取代理提现记录 app
    public function getUserWithdrawalList(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $user_id = $request->get('user_id', '');
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');
            $where = [];
            if ($user_id) {
                $where[] = ['user_id', $user_id];
            }
            if ($time_start) {
                $where[] = ['created_at', '>=', $time_start];
            }

            if ($time_end) {
                $where[] = ['created_at', '<=', $time_end];
            }
            $check_data = [
                'user_id' => 'user_id',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $obj = DB::table('user_withdrawals_records');
            $obj = $obj->where($where)
                ->where('status', 1)
                ->select('*');
            $money = $obj->sum('amount');

            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $list = $this->page($obj)->orderBy('created_at', 'desc')->get();
            $data = [
                'list' => $list,
                'money' => $money
            ];
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 0,
                'message' => $exception->getMessage() . ' | ' . $exception->getLine()
            ]);
        }

    }

    //获取代理服务费记录 app
    public function getUserServiceChargeListApp(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $user_id = $request->get('user_id', '');
            $where = [];
            if ($user_id) {
                $where[] = ['user_id', $user_id];
            }
            $check_data = [
                'user_id' => 'user_id',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $obj = DB::table('user_service_charge');
            $obj = $obj->where($where)
                ->select('*');
            $cost = $obj->sum('cost');

            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $list = $this->page($obj)->orderBy('created_at', 'desc')->get();
            $data = [
                'list' => $list,
                'money' => $cost
            ];
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 0,
                'message' => $exception->getMessage() . ' | ' . $exception->getLine()
            ]);
        }

    }

    //业绩交易量 app
    public function getPerformance(Request $request)
    {
        try {

            $user_id = $request->get('user_id');
            $check_data = [
                'user_id' => '代理id',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $start_time = date('Y-m-1 00:00:00', time());
            $end_time = date('Y-m-d 23:59:59', strtotime(date('Y-m-01', time()) . ' 1 month -1 day'));
            $subIDs = $this->getSubIdsAll($user_id);
            unset($subIDs[0]);
            //本级新增代理数量
            $users = User::where('is_delete', '0')
                ->where('created_at', '>=', $start_time)
                ->where('created_at', '<=', $end_time)
                ->where('pid', $user_id)
                ->select('id')
                ->get();
            $users_num = count($users);

            //下级新增代理数量
            $sub_users = User::where('is_delete', '0')
                ->where('created_at', '>=', $start_time)
                ->where('created_at', '<=', $end_time)
                ->whereIn('pid', $subIDs)
                ->select('id')
                ->get();
            //下级新增商户数量
            $sub_store = Store::where('created_at', '>=', $start_time)
                ->where('created_at', '<=', $end_time)
                ->whereIn('user_id', $subIDs)
                ->select('id')
                ->get();
            //下级交易量
            $sub_table = DB::table('orders');
            $sub_order_obj = $sub_table->select([
                DB::raw("SUM(`total_amount`) as `total_amount_sum`"),
            ])
                ->where('created_at', '>=', $start_time)
                ->where('created_at', '<=', $end_time)
                ->where('pay_status', 1)
                ->whereIn('user_id', $subIDs)
                ->first();

            $sub_users_num = count($sub_users);

            //本级新增商户数量
            $store = Store::where('created_at', '>=', $start_time)
                ->where('created_at', '<=', $end_time)
                ->where('user_id', $user_id)
                ->select('id')
                ->get();
            $store_num = count($store);


            $sub_store_num = count($sub_store);

            //本级交易量
            $table = DB::table('orders');
            $order_obj = $table->select([
                DB::raw("SUM(`total_amount`) as `total_amount_sum`"),
            ])
                ->where('created_at', '>=', $start_time)
                ->where('created_at', '<=', $end_time)
                ->where('pay_status', 1)
                ->where('user_id', $user_id)
                ->first();
            $transaction_sum = $order_obj->total_amount_sum;
            if (!$transaction_sum) {
                $transaction_sum = 0.00;
            }


            $sub_transaction_sum = $sub_order_obj->total_amount_sum;
            if (!$sub_transaction_sum) {
                $sub_transaction_sum = 0.00;
            }

            $data = [
                'users_num' => $users_num,
                'sub_users_num' => $sub_users_num,
                'store_num' => $store_num,
                'sub_store_num' => $sub_store_num,
                'transaction_sum' => $transaction_sum,
                'sub_transaction_sum' => $sub_transaction_sum
            ];
            return json_encode([
                'status' => 1,
                'message' => '获取成功',
                'data' => $data
            ]);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    //业绩交易量 app
    public function getPerformanceActivation(Request $request)
    {
        try {

            $user_id = $request->get('user_id');
            $check_data = [
                'user_id' => '代理id',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $month_start_time = date('Y-m-1 00:00:00', time());
            $month_end_time = date('Y-m-d 23:59:59', strtotime(date('Y-m-01', time()) . ' 1 month -1 day'));

            $zt_start_time = date("Y-m-d 00:00:00", strtotime("-1 day"));
            $zt_end_time = date("Y-m-d 23:59:59", strtotime("-1 day"));
            $subIDs = $this->getSubIdsAll($user_id);
            unset($subIDs[0]);

            //本级昨日激活
            $zt_activation = QrListInfo::where('status', '1')
                ->where('bind_time', '>=', $zt_start_time)
                ->where('bind_time', '<=', $zt_end_time)
                ->where('user_id', $user_id)
                ->select('id')
                ->get();
            //本级本月激活
            $month_activation = QrListInfo::where('status', '1')
                ->where('bind_time', '>=', $month_start_time)
                ->where('bind_time', '<=', $month_end_time)
                ->where('user_id', $user_id)
                ->select('id')
                ->get();
            //本级累计激活
            $activation = QrListInfo::where('status', '1')
                ->where('user_id', $user_id)
                ->select('id')
                ->get();
            //下级昨日激活
            $sub_zt_activation = QrListInfo::where('status', '1')
                ->where('bind_time', '>=', $zt_start_time)
                ->where('bind_time', '<=', $zt_end_time)
                ->whereIn('user_id', $subIDs)
                ->select('id')
                ->get();
            //下级本月激活
            $sub_month_activation = QrListInfo::where('status', '1')
                ->where('bind_time', '>=', $month_start_time)
                ->where('bind_time', '<=', $month_end_time)
                ->whereIn('user_id', $subIDs)
                ->select('id')
                ->get();
            //下级累计激活
            $sub_activation = QrListInfo::where('status', '1')
                ->whereIn('user_id', $subIDs)
                ->select('id')
                ->get();


            $zt_activation_num = count($zt_activation);
            $month_activation_num = count($month_activation);
            $activation_num = count($activation);
            $sub_zt_activation_num = count($sub_zt_activation);
            $sub_month_activation_num = count($sub_month_activation);
            $sub_activation_num = count($sub_activation);


            $data = [
                'zt_activation_num' => $zt_activation_num,
                'month_activation_num' => $month_activation_num,
                'activation_num' => $activation_num,
                'sub_zt_activation_num' => $sub_zt_activation_num,
                'sub_month_activation_num' => $sub_month_activation_num,
                'sub_activation_num' => $sub_activation_num
            ];
            return json_encode([
                'status' => 1,
                'message' => '获取成功',
                'data' => $data
            ]);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    //日分润列表 pc
    public function getSettlementDayList(Request $request)
    {
        try {
            $public = $this->parseToken();
            $time_start = $request->get('time_start', date("Y-m-d 00:00:00", time()));
            $time_end = $request->get('time_end', date("Y-m-d H:i:s", time()));
            $user_id = $request->get('user_id', '');
            $out_trade_no = $request->get('out_trade_no', '');
            $store_id = $request->get('store_id', '');
            $settlement_type = $request->get('settlement_type', '');
            $source_type = $request->get('source_type', '');
            $where = [];
            //限制时间
            $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
            $day = 31;
            if ($date > $day) {
                return json_encode([
                    'status' => 2,
                    'message' => '时间筛选不能超过' . $day . '天'
                ]);
            }

            if ($out_trade_no) {
                $where[] = ['user_wallet_details.out_trade_no', '=', $out_trade_no];
            }
            if ($time_start) {
                $where[] = ['user_wallet_details.created_at', '>=', $time_start];
            }

            if ($time_end) {
                $where[] = ['user_wallet_details.created_at', '<=', $time_end];
            }

            if ($store_id) {
                $where[] = ['user_wallet_details.store_id', '=', $store_id];
            }
            if ($settlement_type) {
                $where[] = ['user_wallet_details.settlement', '=', $settlement_type];
            }
            if ($source_type) {
                $where[] = ['user_wallet_details.source_type', '=', $source_type];
            }

            if (!$user_id) {
                $user_id = $public->user_id;

            }
            $obj = DB::table('user_wallet_details');
            if ($user_id == 1) {
                $users = $this->getSubIds($user_id);
                $obj->join('stores', 'user_wallet_details.store_id', 'stores.store_id');
                $obj->where($where)
                    ->whereIn('user_wallet_details.user_id', $users)
                    ->select('user_wallet_details.*', 'stores.store_name');

            } else {
                $where[] = ['user_wallet_details.user_id', '=', $user_id];
                $obj->join('stores', 'user_wallet_details.store_id', 'stores.store_id');
                $obj->where($where)
                    ->select('user_wallet_details.*', 'stores.store_name');
            }

            $where['settlement'] = '01';
            $settlement_money = UserWalletDetail::where($where)->select('money')->sum('money');
            $where['settlement'] = '02';
            $un_settlement_money = UserWalletDetail::where($where)->select('money')->sum('money');
            if ($settlement_type) {
                if ($settlement_type == '01') {
                    $un_settlement_money = 0;
                } else {
                    $settlement_money = 0;

                }
            }
            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $list = $this->page($obj)->orderBy('created_at', 'desc')->get();
            $data = [
                'list' => $list,
                'money' => $settlement_money + $un_settlement_money,
                'settlement_money' => $settlement_money,
                'un_settlement_money' => $un_settlement_money
            ];
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }

    //月分润列表 pc
    public function getSettlementMonthList(Request $request)
    {
        try {
            $public = $this->parseToken();
            $time_start = $request->get('time_start', date("Y-m", time()));
            $time_end = $request->get('time_end', date('Y-m', strtotime(date('Y-m', time()) . ' 1 month')));
            $user_id = $request->get('user_id', '');
            $settlement_type = $request->get('settlement_type', '');

            $where = [];

            if ($time_start) {
                $where[] = ['created_at', '>=', $time_start];
            }

            if ($time_end) {
                $where[] = ['created_at', '<=', $time_end];
            }
            if (!$user_id) {
                $user_id = $public->user_id;
            }
            $obj = DB::table('settlement_month_list');
            if ($user_id == 1) {
                $users = $this->getSubIds($user_id);
                $obj->where($where)
                    ->whereIn('user_id', $users)
                    ->select('*');
            } else {
                $obj->where($where)
                    ->where('user_id', $user_id)
                    ->orWhere('p_user_id', $user_id)
                    ->select('*');
            }
//            $obj->where($where)
//                ->where('user_id',$user_id)
//                ->orWhere('p_user_id',$user_id)
//                ->select('*');
            //Log::info($where);
            $money = $obj->sum('total_amount');
            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $list = $this->page($obj)->orderBy('created_at', 'desc')->get();
            $data = [
                'list' => $list,
                'money' => $money,
            ];
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }

    //激活奖励列表 pc
    public function getActivationList(Request $request)
    {
        $public = $this->parseToken();
        $time_start = $request->get('time_start', date("Y-m-d 00:00:00", time()));
        $time_end = $request->get('time_end', date("Y-m-d H:i:s", time()));
        $user_id = $request->get('user_id', '');
        $code_num = $request->get('code_num', '');
        $store_id = $request->get('store_id', '');
        $status = $request->get('status', '');
        $where = [];
        if (!$user_id) {
            $user_id = $public->user_id;
//            Log::info($user_id);
        }
        //限制时间
        $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
        $day = 31;
        if ($date > $day) {
            return json_encode([
                'status' => 2,
                'message' => '时间筛选不能超过' . $day . '天'
            ]);
        }
        if ($time_start) {
            $where[] = ['qr_user_reward_info.created_at', '>=', $time_start];
        }

        if ($time_end) {
            $where[] = ['qr_user_reward_info.created_at', '<=', $time_end];
        }
        if ($code_num) {
            $where[] = ['qr_user_reward_info.code_num', '=', $code_num];
        }
        if ($store_id) {
            $where[] = ['qr_user_reward_info.store_id', '=', $store_id];
        }
        if ($status) {
            $where[] = ['qr_user_reward_info.status', '=', $status];
        }
        $obj = DB::table('qr_user_reward_info');
        if ($user_id == 1) {
            $users = $this->getSubIds($user_id);
            $obj->join('users', 'qr_user_reward_info.user_id', 'users.id');
            $obj->where($where)
                ->whereIn('qr_user_reward_info.user_id', $users)
                ->select('qr_user_reward_info.*', 'users.name');
        } else {
            $where[] = ['qr_user_reward_info.user_id', '=', $user_id];
            $obj->join('users', 'qr_user_reward_info.user_id', 'users.id');
            $obj->where($where)
                ->select('qr_user_reward_info.*', 'users.name');
        }
//        Log::info($where);
        $money = $obj->sum('award_amount');
        $this->message = '数据返回成功';
        $this->t = $obj->count();
        $list = $this->page($obj)->orderBy('created_at', 'desc')->get();
        $data = [
            'list' => $list,
            'money' => $money,
        ];
        return $this->format($data);

    }

    //交易达标返现列表 pc
    public function getUserReturnAmountList(Request $request)
    {
        $public = $this->parseToken();
        $time_start = $request->get('time_start', date("Y-m-d 00:00:00", time()));
        $time_end = $request->get('time_end', date("Y-m-d H:i:s", time()));
        $user_id = $request->get('user_id', '');
        $code_num = $request->get('code_num', '');
        $store_id = $request->get('store_id', '');
        $where = [];
        if (!$user_id) {
            $user_id = $public->user_id;
//            Log::info($user_id);
        }
        //限制时间
        $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
        $day = 31;
        if ($date > $day) {
            return json_encode([
                'status' => 2,
                'message' => '时间筛选不能超过' . $day . '天'
            ]);
        }
        if ($time_start) {
            $where[] = ['qr_user_return_amount_info.created_at', '>=', $time_start];
        }

        if ($time_end) {
            $where[] = ['qr_user_return_amount_info.created_at', '<=', $time_end];
        }
        if ($code_num) {
            $where[] = ['qr_user_return_amount_info.code_num', '=', $code_num];
        }
        if ($store_id) {
            $where[] = ['qr_user_return_amount_info.store_id', '=', $store_id];
        }
        $obj = DB::table('qr_user_return_amount_info');
        if ($user_id == 1) {
            $users = $this->getSubIdsAll($user_id);
            $obj->join('users', 'qr_user_return_amount_info.user_id', 'users.id');
            $obj->where($where)
                ->whereIn('qr_user_return_amount_info.user_id', $users)
                ->select('qr_user_return_amount_info.*', 'users.name');
        } else {
            $where[] = ['qr_user_return_amount_info.user_id', '=', $user_id];
            $obj->join('users', 'qr_user_return_amount_info.user_id', 'users.id');
            $obj->where($where)
                ->select('qr_user_return_amount_info.*', 'users.name');
        }
        $this->message = '数据返回成功';
        $this->t = $obj->count();
        $data = $this->page($obj)->orderBy('created_at', 'desc')->get();
        return $this->format($data);

    }

    //代理商服务费列表 pc
    public function getUserServiceChargeList(Request $request)
    {
        try {
            $public = $this->parseToken();
            $time_start = $request->get('time_start', date("Y-m", time()));
            $time_end = $request->get('time_end', date('Y-m', strtotime(date('Y-m', time()) . ' 1 month')));
            $user_id = $request->get('user_id', '');

            $where = [];

            if ($time_start) {
                $where[] = ['created_at', '>=', $time_start];
            }

            if ($time_end) {
                $where[] = ['created_at', '<=', $time_end];
            }
            if (!$user_id) {
                $user_id = $public->user_id;
            }
            $obj = DB::table('user_service_charge');
            if ($user_id == 1) {
                $users = $this->getSubIds($user_id);
                $obj->where($where)
                    ->whereIn('user_id', $users)
                    ->select('*');
            } else {
                $where[] = ['user_id', '=', $user_id];
                $obj->where($where)
                    ->select('*');
            }
            //Log::info($where);
            $money = $obj->sum('cost');
            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $list = $this->page($obj)->orderBy('created_at', 'desc')->get();
            $data = [
                'list' => $list,
                'money' => $money,
            ];
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }

    //代理商培养将列表 pc
    public function getUserTrainRewardList(Request $request)
    {
        try {
            $public = $this->parseToken();
            $time_start = $request->get('time_start', date("Y-m", time()));
            $time_end = $request->get('time_end', date('Y-m', strtotime(date('Y-m', time()) . ' 1 month')));
            $user_id = $request->get('user_id', '');

            $where = [];

            if ($time_start) {
                $where[] = ['user_train_reward.created_at', '>=', $time_start];
            }

            if ($time_end) {
                $where[] = ['user_train_reward.created_at', '<=', $time_end];
            }
            if (!$user_id) {
                $user_id = $public->user_id;
            }
            $obj = DB::table('user_train_reward');
            if ($user_id == 1) {
                $users = $this->getSubIds($user_id);
                $obj = $obj->join('users', 'user_train_reward.sub_user_id', 'users.id')
                    ->where($where)
                    ->whereIn('user_train_reward.user_id', $users)
                    ->select('user_train_reward.*', 'users.name as sub_user_name');
            } else {
                $where[] = ['user_id', '=', $user_id];
                $obj = $obj->join('users', 'user_train_reward.sub_user_id', 'users.id')
                    ->where($where)
                    ->select('user_train_reward.*', 'users.name as sub_user_name');
            }
            //Log::info($where);
            $money = $obj->sum('train_reward_amount');
            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $list = $this->page($obj)->orderBy('created_at', 'desc')->get();
            $data = [
                'list' => $list,
                'money' => $money,
            ];
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }

    //代理商培养将列表 app
    public function getUserTrainRewardListApp(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $user_id = $request->get('user_id', '');
            $where = [];
            if ($user_id) {
                $where[] = ['user_id', $user_id];
            }
            $check_data = [
                'user_id' => 'user_id',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $obj = DB::table('user_train_reward');
            $obj = $obj->join('users', 'user_train_reward.sub_user_id', 'users.id')
                ->where($where)
                ->select('user_train_reward.*', 'users.name as sub_user_name');


            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $data = $this->page($obj)->orderBy('created_at', 'desc')->get();
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 0,
                'message' => $exception->getMessage() . ' | ' . $exception->getLine()
            ]);
        }

    }

    //下级购买商品返佣列表
    public function getUserShoppingRewardList(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $user_id = $request->get('user_id', '');
            $get_type = $request->get('get_type', 'pc');
            $where = [];
            $check_data = [
                'user_id' => 'user_id',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            if ($user_id && $get_type == 'app') {
                $where[] = ['user_shopping_reward.p_user_id', $user_id];
            }
            $obj = DB::table('user_shopping_reward');
            $obj = $obj->join('users', 'user_shopping_reward.user_id', 'users.id')
                ->where($where)
                ->select('user_shopping_reward.*', 'users.name as sub_user_name');

            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $data = $this->page($obj)->orderBy('created_at', 'desc')->get();
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 0,
                'message' => $exception->getMessage() . ' | ' . $exception->getLine()
            ]);
        }

    }

    //下级交易奖励列表
    public function getSubUserTrainRewardList(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $user_id = $request->get('user_id', '');
            $get_type = $request->get('get_type', 'pc');
            $where = [];
            $check_data = [
                'user_id' => 'user_id',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            if ($user_id && $get_type == 'app') {
                $where[] = ['user_sub_train_reward.p_user_id', $user_id];
            }
            $obj = DB::table('user_sub_train_reward');
            $obj = $obj->join('users', 'user_sub_train_reward.user_id', 'users.id')
                ->where($where)
                ->where('user_sub_train_reward.status', '1')
                ->select('user_sub_train_reward.*', 'users.name as sub_user_name');

            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $data = $this->page($obj)->orderBy('created_at', 'desc')->get();
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 0,
                'message' => $exception->getMessage() . ' | ' . $exception->getLine()
            ]);
        }

    }

    //商城购买商品奖励 pc
    public function getUserShoppingRewardListPc(Request $request)
    {
        try {
            $public = $this->parseToken();
            $time_start = $request->get('time_start', date("Y-m", time()));
            $time_end = $request->get('time_end', date('Y-m', strtotime(date('Y-m', time()) . ' 1 month')));
            $user_id = $request->get('user_id', '');
            $user_name = $request->get('user_name', '');
            $p_user_name = $request->get('p_user_name', '');

            $where = [];

            if ($time_start) {
                $where[] = ['user_shopping_reward.created_at', '>=', $time_start];
            }
            if ($time_end) {
                $where[] = ['user_shopping_reward.created_at', '<=', $time_end];
            }
            if ($user_name) {
                $where[] = ['users.name', 'like', '%' . $user_name . '%'];
            }
            if ($p_user_name) {
                $where[] = ['user_shopping_reward.p_user_name', 'like', '%' . $p_user_name . '%'];
            }
            if (!$user_id) {
                $user_id = $public->user_id;
            }
            $obj = DB::table('user_shopping_reward');
            $obj = $obj->join('users', 'user_shopping_reward.user_id', 'users.id');
            if ($user_id == 1) {
                $users = $this->getSubIdsAll($user_id);
                $obj->where($where)
                    ->whereIn('user_shopping_reward.user_id', $users)
                    ->select('user_shopping_reward.*', 'users.name as user_name');
            } else {
                $obj->where($where)
                    ->where('user_shopping_reward.p_user_id', $user_id)
                    ->select('user_shopping_reward.*', 'users.name as user_name');
            }
            $money = $obj->sum('amount');
            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $list = $this->page($obj)->orderBy('user_shopping_reward.created_at', 'desc')->get();
            $data = [
                'list' => $list,
                'money' => $money,
            ];
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }

    //商城购买商品奖励 pc
    public function getSubUserTrainRewardListPc(Request $request)
    {
        try {
            $public = $this->parseToken();
            $time_start = $request->get('time_start', date("Y-m", time()));
            $time_end = $request->get('time_end', date('Y-m', strtotime(date('Y-m', time()) . ' 1 month')));
            $user_id = $request->get('user_id', '');
            $user_name = $request->get('user_name', '');
            $p_user_name = $request->get('p_user_name', '');

            $where = [];

            if ($time_start) {
                $where[] = ['user_sub_train_reward.created_at', '>=', $time_start];
            }
            if ($time_end) {
                $where[] = ['user_sub_train_reward.created_at', '<=', $time_end];
            }
            if ($user_name) {
                $where[] = ['users.name', 'like', '%' . $user_name . '%'];
            }
            if ($p_user_name) {
                $where[] = ['user_sub_train_reward.p_user_name', 'like', '%' . $p_user_name . '%'];
            }
            if (!$user_id) {
                $user_id = $public->user_id;
            }
            $obj = DB::table('user_sub_train_reward');
            $obj = $obj->join('users', 'user_sub_train_reward.user_id', 'users.id');
            if ($user_id == 1) {
                $users = $this->getSubIdsAll($user_id);
                $obj->where($where)
                    ->where('user_sub_train_reward.status', '1')
                    ->whereIn('user_sub_train_reward.user_id', $users)
                    ->select('user_sub_train_reward.*', 'users.name as user_name');
            } else {
                $obj->where($where)
                    ->where('user_sub_train_reward.status', '1')
                    ->where('user_sub_train_reward.p_user_id', $user_id)
                    ->select('user_sub_train_reward.*', 'users.name as user_name');
            }
            $money = $obj->sum('amount');
            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $list = $this->page($obj)->orderBy('user_sub_train_reward.created_at', 'desc')->get();
            $data = [
                'list' => $list,
                'money' => $money,
            ];
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }

    //下级购买商品返佣列表
    public function getUserBoxRebateList(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $user_id = $request->get('user_id', $user->user_id);
            $get_type = $request->get('get_type', 'pc');
            $user_name = $request->get('user_name', '');
            $store_name = $request->get('store_name', '');
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');
            $where = [];
            if ($user_name) {
                $where[] = ['user_name', 'like', '%' . $user_name . '%'];
            }
            if ($store_name) {
                $where[] = ['store_name', 'like', '%' . $store_name . '%'];
            }
            if ($time_start) {
                $where[] = ['created_at', '>=', $time_start];
            }
            if ($time_end) {
                $where[] = ['created_at', '<=', $time_end];
            }
            if ($get_type == 'pc' && $user_id == 1) {
                $obj = DB::table('user_box_rebate_info')
                    ->where($where)
                    ->whereIn('user_id', $this->getSubIdsAll($user_id))
                    ->select('*');
            } else {
                $obj = DB::table('user_box_rebate_info')
                    ->where($where)
                    ->select('*');
            }
            $money = $obj->sum('amount');
            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $list = $this->page($obj)->orderBy('created_at', 'desc')->get();
            $data = [
                'list' => $list,
                'money' => $money,
            ];
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 0,
                'message' => $exception->getMessage() . ' | ' . $exception->getLine()
            ]);
        }
    }
    //架构奖列表
    public function getUserArchitectureRewardList(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $user_id = $request->get('user_id', $user->user_id);
            $get_type = $request->get('get_type', 'pc');
            $user_name = $request->get('user_name', '');
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');
            $where = [];
            if ($user_name) {
                $where[] = ['user_name', 'like', '%' . $user_name . '%'];
            }
            if ($time_start) {
                $where[] = ['created_at', '>=', $time_start];
            }
            if ($time_end) {
                $where[] = ['created_at', '<=', $time_end];
            }
            if ($get_type == 'pc' && $user_id == 1) {
                $obj = DB::table('user_architecture_rewards')
                    ->where($where)
                    ->whereIn('user_id', $this->getSubIdsAll($user_id))
                    ->select('*');
            } else {
                $obj = DB::table('user_architecture_rewards')
                    ->where('user_id',$user_id)
                    ->where($where)
                    ->select('*');
            }
            $money = $obj->sum('reward_amount');
            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $list = $this->page($obj)->orderBy('created_at', 'desc')->get();
            $data = [
                'list' => $list,
                'money' => $money,
            ];
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 0,
                'message' => $exception->getMessage() . ' | ' . $exception->getLine()
            ]);
        }


    }
    //查询调拨记录
    public function getUserTransferList(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $user_id = $request->get('user_id', $user->user_id);
            $transfer_type = $request->get('transfer_type', '');
            $user_name = $request->get('user_name', '');
            $sub_user_name = $request->get('sub_user_name', '');
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');
            $where = [];
            if ($user_name) {
                $where[] = ['users.name', 'like', '%' . $user_name . '%'];
            }
            if ($sub_user_name) {
                $where[] = ['user_transfer_info.sub_user_name', 'like', '%' . $sub_user_name . '%'];
            }
            if ($transfer_type) {
                $where[] = ['user_transfer_info.transfer_type', '=', $transfer_type];
            }
            if ($time_start) {
                $where[] = ['user_transfer_info.created_at', '>=', $time_start];
            }
            if ($time_end) {
                $where[] = ['user_transfer_info.created_at', '<=', $time_end];
            }
            $obj = DB::table('user_transfer_info');
            if ($user_id == 1) {
                $obj = $obj->join('users', 'user_transfer_info.user_id', 'users.id')
                    ->where($where)
                    ->select('user_transfer_info.*', 'users.name as user_name');
            } else {
                $obj = $obj->join('users', 'user_transfer_info.user_id', 'users.id')
                    ->where($where)
                    ->where('user_id',$user_id)
                    ->select('user_transfer_info.*', 'users.name as user_name');
            }
            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $data = $this->page($obj)->orderBy('created_at', 'desc')->get();
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => 0,
                'message' => $exception->getMessage() . ' | ' . $exception->getLine()
            ]);
        }
    }

}