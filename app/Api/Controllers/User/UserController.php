<?php

namespace App\Api\Controllers\User;


use Aliyun\AliSms;
use App\Api\Controllers\BaseController;
use App\Api\Controllers\Rsa\RsaE;
use App\Api\Controllers\Sms\TxSmsController;
use App\Models\AgentLevels;
use App\Models\AgentLevelsInfos;
use App\Models\EasypayStore;
use App\Models\EsignAuths;
use App\Models\Order;
use App\Models\QrListInfo;
use App\Models\Role;
use App\Models\SettlementList;
use App\Models\Store;
use App\Models\User;
use App\Models\UserAuths;
use App\Models\UserDayOrder;
use App\Models\UserLevels;
use App\Models\UserMonthLevel;
use App\Models\UserMonthOrder;
use App\Models\UserRate;
use App\Models\ModelHasRole;
use App\Models\UserRateProfit;
use App\Models\UserRelations;
use App\Models\YinshengStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class UserController extends BaseController
{
    public function userInfo(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $user->user_id;
            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '用户不存在'
                ]);
            }
            //查询用户角色
            $obj = DB::table('roles as r');
            $roleList = $obj->select('r.name', 'r.display_name')
                ->leftJoin('model_has_roles as mr', 'mr.role_id', '=', 'r.id')
                ->leftJoin('users as u', 'mr.model_id', '=', 'u.id')
                ->where('u.id', '=', $user_id)
                ->distinct()
                ->get()->toArray();

            $role = [];
            $roleName = [];

            if ($roleList) {
                foreach ($roleList as $val) {
                    array_push($role, $val->name);
                    array_push($roleName, $val->display_name);
                }
            }

            //查询用户权限
            if ($user_id == 1) { //admin权限

            } else {

            }
            $obj1 = DB::table('permissions as p');
            $permissionsList = $obj1->select('p.perms')
                ->leftJoin('role_has_permissions as rp', 'rp.permission_id', '=', 'p.id')
                ->leftJoin('roles as r', 'r.id', '=', 'rp.role_id')
                ->leftJoin('model_has_roles as mr', 'mr.role_id', '=', 'r.id')
                ->leftJoin('users as u', 'u.id', '=', 'mr.model_id')
                ->where('u.id', '=', $user_id)
                ->distinct()
                ->get()->toArray();

            $permissions = [];

            if ($permissionsList) {
                foreach ($permissionsList as $val) {
                    if ($val->perms) {
                        array_push($permissions, $val->perms);
                    }
                }
            }

            return json_encode([
                'status' => 1,
                'message' => '成功',
                'data' => $user,
                'roles' => $role,
                'roleName' => $roleName,
                'permissions' => $permissions
            ]);

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile()
            ]);
        }

    }

    //app 首页的统计
    public function get_my_data(Request $request)
    {

        try {
            $user = $this->parseToken();
            $user_id = $user->user_id;
            //总下级数量
            //$sub_user_count = count($this->getSubIdsAll($user_id, false));
            //总门店数 含下级
            //$store_count = Store::whereIn('user_id', $this->getSubIdsAll($user_id, true))->select('id')->count('id');

            //今日
            //总下级数量

            $beginToday = date("Y-m-d 00:00:00", time());
            $endToday = date("Y-m-d H:i:s", time());

            // $sub_user_count1 = count($this->getSubIdsAll($user_id, false, $beginToday, $endToday));
            // //总门店数 含下级
            // $store_count1 = Store::whereIn('user_id', $this->getSubIdsAll($user_id, true, $beginToday, $endToday))->select('id')->count('id');

            //昨日
            //总下级数量
            $beginYesterday = date("Y-m-d 00:00:00", strtotime("-1 day"));
            $endYesterday = date("Y-m-d 23:59:59", strtotime("-1 day"));

            // $sub_user_count2 = count($this->getSubIdsAll($user_id, false, $beginYesterday, $endYesterday));
            //总门店数 含下级
            // $store_count2 = Store::whereIn('user_id', $this->getSubIdsAll($user_id, true, $beginYesterday, $endYesterday))->select('id')->count('id');

            //$new_store_count = $store_count2 - $store_count1;
            //$new_sub_user_count = $sub_user_count2 - $sub_user_count1;

            //今日流水
            // $day = date('Ymd', time());
            // $day_order = UserDayOrder::whereIn('user_id', $this->getSubIdsAll($user_id))
            //     ->where('day', $day)
            //     ->select('total_amount')
            //     ->sum('total_amount');

            //昨日流水
            // $old_day = date("Ymd", time() - 24 * 60 * 60);
            // $day = date('Ymd', time());
            // $old_day_order = UserDayOrder::whereIn('user_id', $this->getSubIdsAll($user_id))
            //     ->where('day', $old_day)
            //     ->select('total_amount')
            //     ->sum('total_amount');

            //月流水
            // $month = date('Ym', time());
            // $month_order = UserMonthOrder::whereIn('user_id', $this->getSubIdsAll($user_id))
            //     ->where('month', $month)
            //     ->select('total_amount')
            //     ->sum('total_amount');

            //下级账户
            $user_idsStr = '';
            $user_ids = $this->getSubIdsAll($user_id);
            foreach ($user_ids as $key => $val) {
                $user_idsStr = $user_idsStr . $val . ',';
            }
            $user_idsStr = substr($user_idsStr,0,strlen($user_idsStr)-1);

            //待进件数量
            // $incoming_parts_count = Store::whereIn('user_id', $this->getSubIdsAll($user_id, true))
            //     ->where('status', '=', '2')
            //     ->where('pid', '=', '0')
            //     ->where('is_delete', '=', '0')
            //     ->where('is_close', '=', '0')
            //     ->count('id');
            //已进件数量
            // $in_audit_count = Store::whereIn('user_id', $this->getSubIdsAll($user_id, true))
            //     ->where('status', '=', '1')
            //     ->where('pid', '=', '0')
            //     ->where('is_delete', '=', '0')
            //     ->where('is_close', '=', '0')
            //     ->count('id');
            //驳回数量
            // $reject_count = Store::whereIn('user_id', $this->getSubIdsAll($user_id, true))
            //     ->where('status', '=', '3')
            //     ->where('pid', '=', '0')
            //     ->where('is_delete', '=', '0')
            //     ->where('is_close', '=', '0')
            //     ->count('id');

            //未处理进件数量
            // $sqlQuery = "select * from (
            //     select distinct s.store_id,if(p.company is null,'easypay', p.company) as company,if(p.status is null,'0', p.status) as status
            //     from stores s left join store_pay_ways p on s.store_id = p.store_id  and p.company = 'easypay'
            //     where s.store_id in (select store_id from stores where user_id in ( " .$user_idsStr. " ))
            //     ) s where s.status = '0'";
            $sqlQuery = "select * from (
                select distinct s.store_id,if(p.company is null,'easypay', p.company) as company,if(p.status is null,'0', p.status) as status
                from stores s left join store_pay_ways p on s.store_id = p.store_id 
                where s.is_close = '0' and s.is_delete ='0' and s.store_id in (select store_id from stores where user_id in ( " .$user_idsStr. " )) group by s.store_id
                ) s where s.status = '0'";
            $un_audit_count = DB::select($sqlQuery);
            $un_audit_count = count($un_audit_count);

            //处理中进件数量
            // $sqlQueryIn = "select * from (
            //     select distinct s.store_id,p.company,if(p.status is null,'0', p.status) as status
            //     from stores s left join store_pay_ways p on s.store_id = p.store_id
            //     where s.store_id in (select store_id from stores where user_id in ( " .$user_idsStr. " ))
            //     and p.company = 'easypay'
            //     ) s where s.status in('2','3')";
            $sqlQueryIn = "select * from (
                select distinct s.store_id,p.company,if(p.status is null,'0', p.status) as status
                from stores s left join store_pay_ways p on s.store_id = p.store_id
                where s.is_close = '0' and s.is_delete ='0' and s.store_id in (select store_id from stores where user_id in ( " .$user_idsStr. " ))
                ) s where s.status in('2','3') group by s.store_id";
            $process_audit_count = DB::select($sqlQueryIn);
            $process_audit_count = count($process_audit_count);

            //已完成进件数量
            // $sqlQueryOk = "select * from (
            //     select distinct s.store_id,p.company,if(p.status is null,'0', p.status) as status
            //     from stores s left join store_pay_ways p on s.store_id = p.store_id
            //     where s.store_id in (select store_id from stores where user_id in ( " .$user_idsStr. " ))
            //     and p.company = 'easypay'
            //     ) s where s.status = '1'";
            $sqlQueryOk = "select * from (
                select distinct s.store_id,p.company,if(p.status is null,'0', p.status) as status
                from stores s left join store_pay_ways p on s.store_id = p.store_id
                where s.is_close = '0' and s.is_delete ='0' and s.store_id in (select store_id from stores where user_id in ( " .$user_idsStr. " ))
                ) s where s.status = '1' group by s.store_id";
            $ok_audit_count = DB::select($sqlQueryOk);
            $ok_audit_count = count($ok_audit_count);

            return json_encode([
                'status' => 1,
                'message' => '数据请求成功',
                'data' => [
                    //'sub_user_count' => '' . $sub_user_count . '',
                    //'store_count' => '' . $store_count . '',
                    //'new_store_count' => '' . $new_store_count . '',
                    //'new_sub_user_count' => '' . $new_sub_user_count . '',
                    // 'day_order' => '' . $day_order . '',
                    // 'old_day_order' => '' . $old_day_order . '',
                    //'month_order' => '' . $month_order . '',
                    //'incoming_count' => '' . $incoming_parts_count . '',
                    //'in_audit_count' => '' . $in_audit_count . '',
                    //'reject_count' => '' . $reject_count . '',
                    'un_audit_count' => '' . $un_audit_count . '', //未处理
                    'process_audit_count' => '' . $process_audit_count . '',//处理中
                    'ok_audit_count' => '' . $ok_audit_count . '', //已完成
                ]
            ]);


        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    public function get_sub_users(Request $request)
    {
        try {
            $token = $this->parseToken();
            $login_user_id = $token->user_id;
            $user_id = $request->get('user_id', $login_user_id);
            $self = $request->get('self', false);
            $User_id_s = $this->getSubIdsAll($user_id, $self);


            $return_type = $request->get('return_type', '');
            $user_name = $request->get('user_name', '');
            $sub_type = $request->get('sub_type', '');
            $level = $request->get('level', '');
            $Display = $request->get('display', ''); //是否全部显示手机号

            $ty = $request->get('ty', '');

            $where = [];
            $where[] = ['is_delete', '=', 0];
            if ($user_name) {
                $where[] = ['name', 'like', '%' . $user_name . '%'];
            } else {
                if ($level) {
                    $where[] = ['level', '=', $level];
                }
            }

            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("users");
            } else {
                $obj = DB::table('users');
            }

            if ($sub_type == "1") {
                $where[] = ['level', '<', '2'];
            }

            if ($return_type == "layui") {
                $Users = User::where('id', $login_user_id)
                    ->where($where)
                    ->with('children')
                    ->select('id', 'pid', 'money', 'name', 'phone', 'level', 's_code', 'logo', 'profit_ratio', 'sub_profit_ratio', 'is_withdraw', 'source', 'is_zero_rate')
                    ->get()->toArray();

                if (!$Display) {
                    array_walk_recursive($Users, function (&$values, $keys) {
                        if ($keys == 'phone') {
                            $values = substr($values, 0, 3) . '****' . substr($values, 7); //手机号加盐
                        }

                        return $values;
                    });
                }

                return json_encode([
                    'status' => '1',
                    'data' => $Users
                ]);
            }
            if ($ty == 'admin') {
                $obj = User::where('id', $login_user_id)
                    ->where('level', '<=', '3')
                    ->with('children');
                $data = $obj->get();
            } else {
                $obj = $obj->where($where)
                    ->whereIn('id', $User_id_s);
                $data = $obj->get();
            }
            $sql = $obj->tosql();

            return json_encode([
                'status' => 1,
                'message' => '数据返回成功',
                'data' => $data,
                'sql' => $sql
            ]);
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    /**
     * 跨数据库-获取首付科技-用户列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function get_sf_sub_users(Request $request)
    {
        try {
            $token = $this->parseToken();
            $user_name = $request->get('user_name', '');
            $login_user_id = $token->user_id;

            $user_info = User::where('id', $login_user_id)
                ->where('is_delete', '=', 0)
                ->first();
            if (!$user_info) {
                $this->status = '2';
                $this->message = '当前用户状态异常或用户不存在';
                return $this->format();
            }

            $where = [];
            if ($user_name) {
                $where[] = ['name', 'like', '%' . $user_name . '%'];
            }

            if (env('DB_DATABASE_FIRSTPAY')) {
                $db = DB::connection("mysql_firstpay");
            } else {
                $this->status = '3';
                $this->message = '首付科技数据连接异常';
                return $this->format();
            }

            $obj = $db->table("users")
                ->where($where)
                ->where('is_delete', '=', 0);
            $this->t = $obj->count();
            $data = $this->page($obj)->get();


        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();

            return $this->format();
        }

        $this->status = '1';
        $this->message = '数据返回成功';
        return $this->format($data);
    }


    public function get_del_sub_users(Request $request)
    {
        try {
            $user = $this->parseToken();
            $login_user_id = $user->user_id;
            $user_id = $request->get('user_id', $login_user_id);
            $self = $request->get('self', false);
            $User_id_s = $this->getSubIdsAll($user_id, $self);//不含自己
            $user_name = $request->get('user_name', '');

            $where = [];
            $where[] = ['is_delete', '=', 1];
            if ($user_name) {
                $where[] = ['name', 'like', '%' . $user_name . '%'];
            }


            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("users");
            } else {
                $obj = DB::table('users');
            }

            $obj = $obj->where($where)
                ->whereIn('id', $User_id_s);
            $this->t = $obj->count();
            $data = $this->page($obj)
                ->orderBy('updated_at', 'desc')
                ->get();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();

            return $this->format();

        }

    }


    //添加代理商或者业务员
    public function add_sub_user(Request $request)
    {
        try {
            $token = $this->parseToken();
            $login_user_id = $token->user_id;
            $phone = $request->get('phone');
            $password = $request->get('password');
            $name = $request->get('user_name'); //公司名称
            $people = $request->get('people', ''); //联系人姓名
            $user_id = $request->get('user_id', '');  //上级代理id
            $user_id = isset($user_id) ? $user_id : $token->user_id;
            $province_name = $request->get('province_name', '全国');
            $city_name = $request->get('city_name', '全国');
            $area_name = $request->get('area_name', '全国');
            $address = $request->get('address', '全国');
            $p_id = $request->get('pid', '');  //上级id
            $is_withdraw = $request->get('is_withdraw', 1); //提现设置 1=正常 0=禁止，字段默认为1
            $invite_code = $request->get('invite_code', '');
            $id_card = $request->get('id_card', '');
            $marketing_province_name = $request->get('marketing_province_name', '');
            $marketing_province_code = $request->get('marketing_province_code', '');
            if (!$p_id && $invite_code) {
                $pid_user = User::where('invite_code', $invite_code)
                    ->where('is_delete', '0')
                    ->first();
            }
            if ($p_id) {
                $pid_user = User::where('id', $p_id)
                    ->where('is_delete', '0')
                    ->first();
                if (!$pid_user) {
                    return json_encode([
                        'status' => '2',
                        'message' => '上级代理不存在或状态异常'
                    ]);
                }
            }

            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '用户不存在'
                ]);
            }

            $login_user = User::where('id', $login_user_id)->first();

            //4级以后如果没有开权限就不能在开了
//            if ($user->level > 5) {
//                return json_encode([
//                    'status' => 2,
//                    'message' => '暂不支持开下级账户'
//                ]);
//            }
            $s_code = rand(1000, 9999);
            $check_data = [
                'phone' => '手机号',
                'password' => '密码',
                'user_name' => '公司名字',
                'people' => '联系人姓名',
                //'id_card' => '身份证号'
            ];

            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            //验证手机号
            if (!preg_match("/^1[3456789]{1}\d{9}$/", $phone)) {
                return json_encode([
                    'status' => 0,
                    'message' => '手机号码不正确'
                ]);
            }

//            //验证身份证号
//            if (!preg_match("/^[1-9]\d{5}(18|19|20|(3\d))\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/", $id_card)) {
//                return json_encode([
//                    'status' => 0,
//                    'message' => '身份证号不正确'
//                ]);
//            }

            $data = $request->all();
            $rules = [
                'phone' => 'required|min:11|max:11|unique:users',
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                return json_encode([
                    'status' => 2,
                    'message' => '账号已注册请直接登录'
                ]);
            }

            $rules = [
                's_code' => 'required|min:4|max:4|unique:users',
            ];
            $validator = Validator::make(['s_code' => $s_code], $rules);
            if ($validator->fails()) {
                return json_encode([
                    'status' => 2,
                    'message' => '激活码生成失败，请重新提交资料'
                ]);
            }

            //验证密码
            if (strlen($password) < 6) {
                return json_encode([
                    'status' => 2,
                    'message' => '密码长度不符合要求'
                ]);
            }
            $sub_user = User::where('name', $name)->first();
            if ($sub_user) {
                return json_encode([
                    'status' => 2,
                    'message' => '代理商名称已存在'
                ]);
            }
            $level = $user->level + 1;
            $level_name = "业务员";
            $role_name = "业务员";
            $invite_no = $this->gen_invite_code();
            if ($level == 1) {
                $level_name = "服务商";
                $role_name = "服务商";
            }
            if ($level == 2) {
                $level_name = "代理商";
                $role_name = "代理商";
            }
            if ($p_id) {
                $dataIn['pid'] = $pid_user->id;
                $dataIn['pid_name'] = $pid_user->name;
            } else {
                $dataIn['pid'] = $user->id;
                $dataIn['pid_name'] = $user->name;
            }
//            $level_name=$request->get('level_name',$level_name); //展示名称
            $dataIn['config_id'] = $user->config_id;
            $dataIn['email'] = $phone . '@139.com';
//            $dataIn['pid'] = $pid_user->id;
//            $dataIn['pid_name'] = $pid_user->name;
            $dataIn['level'] = $level;
            $dataIn['level_name'] = $level_name;
            $dataIn['logo'] = $people;  //联系人姓名
            $dataIn['password'] = bcrypt($password);
            $dataIn['name'] = $name;
            $dataIn['phone'] = $phone;
            $dataIn['s_code'] = $s_code;
            $dataIn['province_name'] = $province_name;
            $dataIn['city_name'] = $city_name;
            $dataIn['area_name'] = $area_name;
            $dataIn['address'] = $address;
            $dataIn['s_code_url'] = url('/api/user/s_code_url?s_code=' . $s_code);
            $dataIn['sub_code_url'] = url('/api/user/sub_code_url?s_code=' . $s_code);
            $dataIn['is_withdraw'] = $is_withdraw;
            $dataIn['profit_ratio'] = $user->profit_ratio;
            //$dataIn['sub_profit_ratio'] = $user->profit_ratio;
            $dataIn['source'] = $user->source;
            $dataIn['invite_no'] = $invite_no;
            $dataIn['invite_code'] = $invite_code;
            $dataIn['id_card'] = $id_card;
            $dataIn['marketing_province_name'] = $marketing_province_name;
            $dataIn['marketing_province_code'] = $marketing_province_code;

            //超级服务商
            if ($user->pid == 0) {
                $dataIn['config_id'] = $phone;
                $dataIn['level'] = 1;
            }

            $User = User::create($dataIn);

            //自动分配权限(默认 服务商)
            $role_name = "agent";

            $roles = Role::where('created_id', $login_user_id)->where('name', $role_name)->first();

            ModelHasRole::create([
                'role_id' => $roles->id,
                'model_type' => 'App\Models\User',
                'model_id' => $User->id,
            ]);

            UserLevels::created([
                'user_id' => $User->id,
                'level' => 1,  //用户级别信息
            ]);

            //添加成功后，发送短信通知
//            if ($User && $phone) {
//                $config_id = $user->config_id;
//
//                //发送"合伙人通知"短信息通知
//                $config = SmsConfig::where('type', '12')
//                    ->where('config_id', $config_id)
//                    ->first();
//                if (!$config) {
//                    $config = SmsConfig::where('type', '12')
//                        ->where('config_id', '1234')
//                        ->first();
//                }
//                if ($config && $config->app_key && $phone && $password) {
//                    $phone = trim($phone);
//                    $sms_data = [
//                        'Managee' => $phone,
//                        'passwordd' => $password
//                    ];
//                    $sms_result = $this->sendSms($phone, $config->app_key, $config->app_secret, $config->SignName, $config->TemplateCode, $sms_data);
//                    if ($sms_result->Code == 'OK') {
////                        Log::info('合伙人通知，'.$phone.'发送短信成功');
//                    } else {
//                        Log::info('合伙人通知，' . $phone . '发送短信失败: ' . $sms_result->Message);
//                    }
//                }
//            }

            return json_encode([
                'status' => 1,
                'message' => '下级账号添加成功',
                'data' => $request->except(['token'])
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile()
            ]);
        }
    }


    //修改代理或者自己的信息
    public function up_user(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id', $user->user_id);

            $data = $request->except(['token', 'password']);

            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '用户不存在'
                ]);
            }
            $user->update($data);
            $user->save();
            return json_encode([
                'status' => 1,
                'message' => '修改成功',
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile()
            ]);
        }
    }


    //设置登录密码
    public function set_password(Request $request)
    {
        try {
            $user = $this->parseToken();
            $oldpassword = $request->get('oldpassword', '');
            $newpassword = $request->get('newpassword', '');
            $newpassword_confirmed = $request->get('newpassword_confirmed', '');


            $check_data = [
                'oldpassword' => '旧密码',
                'newpassword' => '新密码',
                'newpassword_confirmed' => '确认新密码'
            ];

            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $local = User::where('id', $user->user_id)->first();
            //验证旧的密码
            if (!Hash::check($oldpassword, $local->password)) {
                return json_encode([
                    'status' => 2,
                    'message' => '旧的登陆密码不匹配'
                ]);
            }
            if ($newpassword !== $newpassword_confirmed) {
                return json_encode([
                    'status' => 2,
                    'message' => '两次密码不一致'
                ]);
            }
            if (strlen($newpassword) < 6) {
                return json_encode([
                    'status' => 2,
                    'message' => '密码长度不符合要求'
                ]);
            }
            $dataIN = [
                'password' => bcrypt($newpassword),

            ];
            User::where('id', $user->user_id)->update($dataIN);


            $data = [
                'status' => 1,
                'message' => '密码修改成功',
            ];
            return json_encode($data);

        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }


    //修改登录手机号
    public function edit_login_phone(Request $request)
    {

        try {
            $user = $this->parseToken();
            $data = $request->all();
            $password = $request->get('password', '');
            $new_phone = $request->get('phone', '');
            $code_b = $request->get('code_b', '');
            //如果只传password代表校验
            if ($password && $new_phone == "" && $code_b == "") {
                //验证验证码
                $local = User::where('id', $user->user_id)->first();
                //验证旧的密码
                if (!Hash::check($password, $local->password)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '密码不匹配'
                    ]);
                } else {
                    return json_encode([
                        'status' => 1,
                        'message' => '密码匹配成功',
                    ]);
                }
            } else {
                if ($code_b && $new_phone == '' & $password == "") {
                    //验证新手机验证码
                    $msn_local = Cache::get($new_phone . 'editphone-1');
                    if ((string)$code_b != (string)$msn_local) {
                        return json_encode([
                            'status' => 2,
                            'message' => '新手机号码短信验证码不匹配'
                        ]);
                    } else {
                        return json_encode([
                            'status' => 1,
                            'message' => '短信验证码匹配成功',
                            'data' => [],
                        ]);
                    }
                }

                //换手机号码
                //验证新的手机号
                if (!preg_match("/^1[3456789]{1}\d{9}$/", $new_phone)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '手机号码不正确'
                    ]);
                }
                if ($new_phone == $user->phone) {
                    return json_encode([
                        'status' => 2,
                        'message' => '手机号码未更改'
                    ]);
                }
                $rules = [
                    'phone' => 'required|min:11|max:11|unique:users',
                ];
                $validator = Validator::make($data, $rules);
                if ($validator->fails()) {
                    return json_encode([
                        'status' => 2,
                        'message' => '账号已注册请更换'
                    ]);
                }


                //验证新手机验证码
                $msn_local = Cache::get($new_phone . 'editphone-1');
                if ((string)$code_b != (string)$msn_local) {
                    return json_encode([
                        'status' => 2,
                        'message' => '新手机号码短信验证码不匹配'
                    ]);
                }

                User::where('id', $user->user_id)->update(['phone' => $new_phone]);


                $data = [
                    'status' => 1,
                    'message' => '手机号修改成功',
                ];
                return json_encode($data);

            }
        } catch
        (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }


    //删除代理
    public function del_sub_user(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id', '');
            $login_user_id = $user->user_id;
            $province_name = $request->get('province_name', '全国');
            $city_name = $request->get('city_name', '全国');
            $area_name = $request->get('area_name', '全国');
            $address = $request->get('address', '全国');

            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '用户不存在'
                ]);
            }


            $login_user = User::where('id', $login_user_id)->first();

//            $hasPermission = $login_user->hasPermissionTo('删除下级代理');
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '暂不支持删除下级代理']);
//            }
            if ($login_user_id == $user_id) {

                return json_encode([
                    'status' => 2,
                    'message' => '不能删除自己'
                ]);
            }

            $users = $this->getSubIdsAll($user_id);

            User::whereIn('id', $users)->update(['is_delete' => 1]);

            return json_encode([
                'status' => 1,
                'message' => '下级账号删除成功',
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile()
            ]);
        }
    }


    //彻底删除代理
    public function cdel_sub_user(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id', '');
            $level = $user->level;
            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '用户不存在'
                ]);
            }

            if ($user->user_id == $user_id) {
                return json_encode([
                    'status' => 2,
                    'message' => '不能删除自己'
                ]);
            }
            $sub_user = User::where('pid', $user_id)->first();
            if ($sub_user) {
                return json_encode([
                    'status' => 2,
                    'message' => '账户存在下级请先删除下级账户'
                ]);
            }

            $order = Order::where('user_id', $user_id)
                ->select('id')
                ->first();
            if ($level && $order) {
                return json_encode([
                    'status' => 2,
                    'message' => '账户存在关联订单暂不支持删除'
                ]);
            }


            User::where('id', $user_id)->delete();

            //删除实名认证
            UserAuths::where('user_id', $user_id)->delete();

            return json_encode([
                'status' => 1,
                'message' => '下级账号清除成功',
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile()
            ]);
        }
    }


    //恢复代理
    public function fog_sub_user(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id', '');
            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '用户不存在'
                ]);
            }

            if ($user->user_id == $user_id) {
                return json_encode([
                    'status' => 2,
                    'message' => '不能恢复自己'
                ]);
            }

            $user->update(['is_delete' => 0]);
            $user->save();

            return json_encode([
                'status' => 1,
                'message' => '下级账号恢复成功',
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile()
            ]);
        }
    }


    //我的信息
    public function my_info(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $user->user_id;
            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '用户不存在'
                ]);
            }
            $sub = $this->getSubIdsAll($user_id, false);
            $user->sub_user_count = "" . count($sub) . "";
            //默认头像
            if ($user->logo == "") {
                $user->logo = url('/app/img/user/fwslogo.png');
            }
            return json_encode([
                'status' => 1,
                'message' => '成功',
                'data' => $user
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile()
            ]);
        }
    }


    public function user_info(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id', '');
            $user = User::where('id', $user_id)->first();

            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '用户不存在'
                ]);
            }
            //默认头像
            if ($user->logo == "") {
                $user->logo = url('/app/img/user/fwslogo.png');
            }
            return json_encode([
                'status' => 1,
                'message' => '返回成功',
                'data' => $user
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile()
            ]);
        }
    }


    //查询代理成本费率情况
    public function user_ways_all(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id');

            $data = [];
            $store_ways_desc = DB::table('store_ways_desc')
                ->where('is_show', '1')
                ->orderBy('company')
                ->get();
            foreach ($store_ways_desc as $k => $value) {
                $UserRate = UserRate::where('company', $value->company)
                    ->where('user_id', $user->user_id)
                    ->select('id')
                    ->first();
                if (!$UserRate) {
                    continue;
                }

                $has = UserRate::where('user_id', $user_id)->where('ways_type', $value->ways_type)->first();
                if ($has) {
                    $data[$k]['ways_type'] = $value->ways_type;
                    $data[$k]['status'] = $has->status;
                    $data[$k]['ways_desc'] = $value->ways_desc;
                    $data[$k]['rate'] = $has->rate;
                    $data[$k]['rate_a'] = $has->rate_a;
                    $data[$k]['rate_b'] = $has->rate_b;
                    $data[$k]['rate_b_top'] = $has->rate_b_top;
                    $data[$k]['rate_c'] = $has->rate_c;
                    $data[$k]['rate_d'] = $has->rate_d;
                    $data[$k]['rate_d_top'] = $has->rate_d_top;
                    $data[$k]['rate_e'] = $has->rate_e;
                    $data[$k]['rate_f'] = $has->rate_f;
                    $data[$k]['rate_f_top'] = $has->rate_f_top;

                    //如果是刷卡费率读取
                    if (in_array($value->ways_type, [8005, 6005])) {
                        $data[$k]['rate'] = $has->rate_e;
                    }

                    //如果是银联扫码费率读取
                    if (in_array($value->ways_type, [8004, 6004])) {
                        $data[$k]['rate'] = $has->rate_a;
                    }
                    $data = array_values($data);
                } else {
                    $data[$k]['ways_type'] = $value->ways_type;
                    $data[$k]['ways_desc'] = $value->ways_desc;
                    $data[$k]['rate_a'] = $value->rate_a;
                    $data[$k]['rate_b'] = $value->rate_b;
                    $data[$k]['rate_b_top'] = $value->rate_b_top;
                    $data[$k]['rate_c'] = $value->rate_c;
                    $data[$k]['rate_d'] = $value->rate_d;
                    $data[$k]['rate_d_top'] = $value->rate_d_top;
                    $data[$k]['rate_e'] = $value->rate_e;
                    $data[$k]['rate_f'] = $value->rate_f;
                    $data[$k]['rate_f_top'] = $value->rate_f_top;
                    $data[$k]['rate'] = '';
                    $data[$k]['status'] = 2;
                    $data = array_values($data);
                }
            }

            return json_encode([
                'status' => 1,
                'data' => $data
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => '-1',
                'message' => $exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine()
            ]);
        }
    }


    //查询代理各个通道商户的默认费率
    public function user_ways_default(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id');

            $data = [];
            $store_ways_desc = DB::table('store_ways_desc')->where('is_show', '1')->get();
            foreach ($store_ways_desc as $k => $value) {

                //代理不开通不
                $UserRate = UserRate::where('company', $value->company)
                    ->where('user_id', $user_id)
                    ->select('id')
                    ->first();

                if (!$UserRate) {
                    continue;
                }

                $has = UserRate::where('user_id', $user_id)->where('ways_type', $value->ways_type)->first();
                if ($has) {
                    $data[$k]['ways_type'] = $value->ways_type;
                    $data[$k]['ways_desc'] = $value->ways_desc;
                    $data[$k]['store_all_rate'] = $has->store_all_rate;
                    $data[$k]['store_all_rate_a'] = $has->store_all_rate_a;
                    $data[$k]['store_all_rate_b'] = $has->store_all_rate_b;
                    $data[$k]['store_all_rate_b_top'] = $has->store_all_rate_b_top;
                    $data[$k]['store_all_rate_c'] = $has->store_all_rate_c;
                    $data[$k]['store_all_rate_d'] = $has->store_all_rate_d;
                    $data[$k]['store_all_rate_d_top'] = $has->store_all_rate_d_top;
                    $data[$k]['store_all_rate_e'] = $has->store_all_rate_e;
                    $data[$k]['store_all_rate_f'] = $has->store_all_rate_f;
                    $data[$k]['store_all_rate_f_top'] = $has->store_all_rate_f_top;


                    //如果是刷卡费率读取
                    //新大陆刷卡
                    if (in_array($value->ways_type, [8005, 6005])) {

                        $data[$k]['store_all_rate'] = $has->store_all_rate_e;
                    }

                    //银联扫码
                    if (in_array($value->ways_type, [8004, 6004])) {
                        $data[$k]['store_all_rate'] = $has->store_all_rate_a;
                    }


                    $data = array_values($data);


                } else {
                    $data[$k]['ways_type'] = $value->ways_type;
                    $data[$k]['ways_desc'] = $value->ways_desc;
                    $data[$k]['store_all_rate'] = $value->store_all_rate;
                    $data[$k]['store_all_rate_a'] = $value->store_all_rate_a;
                    $data[$k]['store_all_rate_b'] = $value->store_all_rate_b;
                    $data[$k]['store_all_rate_b_top'] = $value->store_all_rate_b_top;
                    $data[$k]['store_all_rate_c'] = $value->store_all_rate_c;
                    $data[$k]['store_all_rate_d'] = $value->store_all_rate_d;
                    $data[$k]['store_all_rate_d_top'] = $value->store_all_rate_d_top;
                    $data[$k]['store_all_rate_e'] = $value->store_all_rate_e;
                    $data[$k]['store_all_rate_f'] = $value->store_all_rate_f;
                    $data[$k]['store_all_rate_f_top'] = $value->store_all_rate_f_top;
                    //如果是刷卡费率读取
                    //新大陆刷卡
                    if (in_array($value->ways_type, [8005, 6005])) {

                        $data[$k]['store_all_rate'] = $value->store_all_rate_e;
                    }

                    //银联扫码
                    if (in_array($value->ways_type, [8004, 6004])) {
                        $data[$k]['store_all_rate'] = $value->store_all_rate_a;
                    }

                    $data = array_values($data);
                }

            }

            return json_encode(['status' => 1, 'data' => $data]);


        } catch (\Exception $exception) {
            return json_encode(['status' => 0, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }
    }


    //修改代理带费率-扫码
    public function edit_user_rate(Request $request)
    {

        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id');
            $ways_type = $request->get('ways_type');

            //扫码
            $rate = $request->get('rate');
            $edit_user = User::where('id', $user_id)->first();
            if (!$edit_user) {
                return json_encode(['status' => 2, 'message' => '代理商不存在', 'data' => []]);
            }


            if ($user_id == $user->user_id && $user->level != 0) {
                return json_encode(['status' => 2, 'message' => '不能设置自己的费率请联系上级', 'data' => []]);
            }

            if ($rate > 1) {
                return json_encode(['status' => 2, 'message' => '费率超过系统上限', 'data' => []]);
            }

            //上级代理商的费率
            $user_pid_rate = UserRate::where('user_id', $edit_user->pid)
                ->where('ways_type', $ways_type)
                ->select('rate')
                ->first();
            if (!$user_pid_rate && $edit_user->pid) {
                return json_encode(['status' => 2, 'message' => '上级代理商未设置费率', 'data' => []]);
            }


            //被设置者是平台以下的账户不能大于上级的成本
            if ($edit_user->level > 0 && $edit_user->pid) {
                if ($rate < $user_pid_rate->rate) {
                    return json_encode(['status' => 2, 'message' => '费率不能低于上级代理商的费率', 'data' => []]);
                }
            }

            $astore_ways_desc = DB::table('store_ways_desc')
                ->where('ways_type', $ways_type)
                ->select('company')
                ->first();
            if (!$astore_ways_desc) {
                return json_encode(['status' => 2, 'message' => '通道基础数据不存在']);
            }

            //扫码需要参数
            $data = [
                'user_id' => $user_id,
                'rate' => $rate,
                //'company' => $astore_ways_desc->company,
                'ways_type' => $ways_type,
            ];


            $this->send_ways_data($data);

            return json_encode(['status' => 1, 'message' => '修改成功', 'data' => $request->except(['token'])]);


        } catch (\Exception $exception) {
            return json_encode(['status' => 0, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }

    }


    //关停和删除通道
    public function del_user_rate(Request $request)
    {

        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id');
            $ways_type = $request->get('ways_type');
            $status = $request->get('status', '2');
            //
            $edit_user = User::where('id', $user_id)->first();
            if (!$edit_user) {
                return json_encode(['status' => 2, 'message' => '代理商不存在', 'data' => []]);
            }

            if ($user->pid && $status == '1') {
                return json_encode(['status' => 2, 'message' => '暂不支持开启', 'data' => []]);
            }

            $arr = $this->getSubIdsAll($user->user_id);

            if (!in_array($user_id, $arr)) {
                return json_encode(['status' => 2, 'message' => '代理商不存在', 'data' => []]);
            }


            UserRate::where('ways_type', $ways_type)
                ->where('user_id', $user_id)
                ->update(['status' => $status]);


            return json_encode(['status' => 1, 'message' => '操作成功', 'data' => $request->except(['token'])]);


        } catch (\Exception $exception) {
            return json_encode(['status' => 0, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }

    }


    //修改代理带费率-刷卡
    public function edit_user_un_rate(Request $request)
    {

        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id');
            $ways_type = $request->get('ways_type');

            $rate_e = $request->get('rate_e');
            $rate_f = $request->get('rate_f');
            $rate_f_top = $request->get('rate_f_top', '20');

            $edit_user = User::where('id', $user_id)->first();
            if (!$edit_user) {
                return json_encode(['status' => 2, 'message' => '代理商不存在', 'data' => []]);
            }


            if ($user_id == $user->user_id && $user->level != 0) {
                return json_encode(['status' => 2, 'message' => '不能设置自己的费率请联系上级', 'data' => []]);
            }

            //上级代理商的费率
            $user_pid_rate = UserRate::where('user_id', $edit_user->pid)
                ->where('ways_type', $ways_type)
                ->select('rate_e', 'rate_f')
                ->first();

            if (!$user_pid_rate && $edit_user->pid) {
                return json_encode(['status' => 2, 'message' => '上级代理商未设置费率', 'data' => []]);
            }

            //被设置者是平台以下的账户不能大于上级的成本
            if ($edit_user->level > 0) {
                //不能大于代理商的成本
                if ($rate_e < $user_pid_rate->rate_e) {
                    return json_encode(['status' => 2, 'message' => '贷记卡费率不能低于上级代理商的费率', 'data' => []]);
                }
                //不能大于代理商的成本
                if ($rate_f < $user_pid_rate->rate_f) {
                    return json_encode(['status' => 2, 'message' => '借记卡费率不能低于上级代理商的费率', 'data' => []]);
                }
            }

            $astore_ways_desc = DB::table('store_ways_desc')
                ->where('ways_type', $ways_type)
                ->select('company')
                ->first();
            if (!$astore_ways_desc) {
                return json_encode(['status' => 2, 'message' => '通道基础数据不存在']);
            }


            $ways = UserRate::where('user_id', $user_id)->where('ways_type', $ways_type)->first();

            //请先设置扫码通道的费率
            if (!$ways) {
                return json_encode(['status' => 2, 'message' => '请先设置扫码通道的费率']);
            }

            $company = $ways->company;
            $data = [
                'rate_e' => $rate_e,
                'rate_f' => $rate_f,
                'rate_f_top' => $rate_f_top,
            ];
            UserRate:: where('user_id', $user_id)->where('company', $company)
                ->update($data);


            return json_encode(['status' => 1, 'message' => '修改成功', 'data' => $request->except(['token'])]);


        } catch (\Exception $exception) {
            return json_encode(['status' => 0, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }

    }


    //修改代理带费率-银联扫码
    public function edit_user_unqr_rate(Request $request)
    {

        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id');
            $ways_type = $request->get('ways_type');

            $rate_a = $request->get('rate_a');
            $rate_b = $request->get('rate_b');
            $rate_b_top = $request->get('rate_b_top', '20');
            $rate_c = $request->get('rate_c');
            $rate_d = $request->get('rate_d');
            $rate_d_top = $request->get('rate_d_top', '20');

            $edit_user = User::where('id', $user_id)->first();
            if (!$edit_user) {
                return json_encode(['status' => 2, 'message' => '代理商不存在', 'data' => []]);
            }


            //上级代理商的费率
            $user_pid_rate = UserRate::where('user_id', $edit_user->pid)
                ->where('ways_type', $ways_type)
                ->select('rate_a', 'rate_b', 'rate_c', 'rate_d')
                ->first();
            if (!$user_pid_rate && $edit_user->pid != "0") {
                return json_encode(['status' => 2, 'message' => '上级代理商未设置费率', 'data' => []]);
            }

            if ($user_id == $user->user_id && $user->level != 0) {
                return json_encode(['status' => 2, 'message' => '不能设置自己的费率请联系上级', 'data' => []]);
            }

            //被设置者是平台以下的账户不能大于上级的成本
            if ($edit_user->level > 0) {
                //不能大于代理商的成本
                if ($rate_a < $user_pid_rate->rate_a) {
                    return json_encode(['status' => 2, 'message' => '小于1000贷记卡费率不能低于上级代理商的费率', 'data' => []]);
                }
                //不能大于代理商的成本
                if ($rate_b < $user_pid_rate->rate_b) {
                    return json_encode(['status' => 2, 'message' => '小于1000借记卡费率不能低于上级代理商的费率', 'data' => []]);
                }

                //不能大于代理商的成本
                if ($rate_c < $user_pid_rate->rate_c) {
                    return json_encode(['status' => 2, 'message' => '大于1000贷记卡费率不能低于上级代理商的费率', 'data' => []]);
                }
                //不能大于代理商的成本
                if ($rate_d < $user_pid_rate->rate_d) {
                    return json_encode(['status' => 2, 'message' => '大于1000借记卡费率不能低于上级代理商的费率', 'data' => []]);
                }

            }

            $astore_ways_desc = DB::table('store_ways_desc')
                ->where('ways_type', $ways_type)
                ->select('company')
                ->first();
            if (!$astore_ways_desc) {
                return json_encode(['status' => 2, 'message' => '通道基础数据不存在']);
            }


            $ways = UserRate::where('user_id', $user_id)->where('ways_type', $ways_type)->first();

            //请先设置扫码通道的费率
            if (!$ways) {
                return json_encode(['status' => 2, 'message' => '请先设置扫码通道的费率']);
            }
            $company = $ways->company;

            $data = [
                'rate_a' => $rate_a,
                'rate_b' => $rate_b,
                'rate_b_top' => $rate_b_top,
                'rate_c' => $rate_c,
                'rate_d' => $rate_d,
                'rate_d_top' => $rate_d_top,
            ];

            UserRate::where('user_id', $user_id)->where('company', $company)->update($data);


            return json_encode(['status' => 1, 'message' => '修改成功', 'data' => $request->except(['token'])]);


        } catch (\Exception $exception) {
            return json_encode(['status' => 0, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }

    }


    //修改商户统一默认费率-扫码
    public function edit_user_store_all_rate(Request $request)
    {

        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id');
            $ways_type = $request->get('ways_type');

            //扫码
            $store_all_rate = $request->get('store_all_rate');
            $edit_user = User::where('id', $user_id)->first();
            if (!$edit_user) {
                return json_encode(['status' => 2, 'message' => '代理商不存在', 'data' => []]);
            }

            if ($store_all_rate > 1) {
                return json_encode(['status' => 2, 'message' => '费率超过系统上限', 'data' => []]);
            }

            //自己的费率
            $user_rate = UserRate::where('user_id', $user_id)
                ->where('ways_type', $ways_type)
                ->first();
            if (!$user_rate) {
                return json_encode(['status' => 2, 'message' => '通道成本费率未设置请联系上级', 'data' => []]);

            }

            //不能小于自己的成本
            if ($store_all_rate < $user_rate->rate) {
                return json_encode(['status' => 2, 'message' => '费率不能低于成本费率', 'data' => []]);
            }


            $astore_ways_desc = DB::table('store_ways_desc')
                ->where('ways_type', $ways_type)
                ->select('company')
                ->first();
            if (!$astore_ways_desc) {
                return json_encode(['status' => 2, 'message' => '通道基础数据不存在']);
            }

            //扫码需要参数
            $data = [
                'user_id' => $user_id,
                'store_all_rate' => $store_all_rate,
//                'company' => $astore_ways_desc->company,
                'ways_type' => $ways_type,
            ];


            $this->send_ways_store_all_data($data);

            return json_encode(['status' => 1, 'message' => '修改成功', 'data' => $request->except(['token'])]);


        } catch (\Exception $exception) {
            return json_encode(['status' => 0, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }

    }


    //修改商户默认统一费率-刷卡
    public function edit_user_un_store_all_rate(Request $request)
    {

        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id');
            $ways_type = $request->get('ways_type');

            $store_all_rate_e = $request->get('store_all_rate_e');
            $store_all_rate_f = $request->get('store_all_rate_f');
            $store_all_rate_f_top = $request->get('store_all_rate_f_top', '20');

            $edit_user = User::where('id', $user_id)->first();
            if (!$edit_user) {
                return json_encode(['status' => 2, 'message' => '代理商不存在', 'data' => []]);
            }


            //自己的费率
            $user_rate = UserRate::where('user_id', $user_id)
                ->where('ways_type', $ways_type)
                ->first();

            if (!$user_rate) {
                return json_encode(['status' => 2, 'message' => '通道成本费率未设置请联系上级', 'data' => []]);

            }

            //不能小于自己的成本
            if ($store_all_rate_e < $user_rate->rate_e) {
                return json_encode(['status' => 2, 'message' => '贷记卡费率不能低于成本费率', 'data' => []]);
            }
            //不能小于自己的成本
            if ($store_all_rate_f < $user_rate->rate_f) {
                return json_encode(['status' => 2, 'message' => '借记卡费率不能低于成本费率', 'data' => []]);
            }

            //不能小于自己的成本
            if ($store_all_rate_f_top < $user_rate->rate_f_top) {
                return json_encode(['status' => 2, 'message' => '借记卡封顶金额不能小于成本', 'data' => []]);
            }


            $astore_ways_desc = DB::table('store_ways_desc')
                ->where('ways_type', $ways_type)
                ->select('company')
                ->first();
            if (!$astore_ways_desc) {
                return json_encode(['status' => 2, 'message' => '通道基础数据不存在']);
            }


            $ways = UserRate::where('user_id', $user_id)->where('ways_type', $ways_type)->first();

            //请先设置扫码通道的费率
            if (!$ways) {
                return json_encode(['status' => 2, 'message' => '请先设置扫码通道的费率']);
            }
            $company = $ways->company;

            $data = [
                'store_all_rate_e' => $store_all_rate_e,
                'store_all_rate_f' => $store_all_rate_f,
                'store_all_rate_f_top' => $store_all_rate_f_top,
            ];

            UserRate::where('user_id', $user_id)->where('company', $company)->update($data);


            return json_encode(['status' => 1, 'message' => '修改成功', 'data' => $request->except(['token'])]);


        } catch (\Exception $exception) {
            return json_encode(['status' => 0, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }

    }


    //修改商户默认统一费率-银联扫码
    public function edit_user_unqr_store_all_rate(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id');
            $ways_type = $request->get('ways_type');

            $store_all_rate_a = $request->get('store_all_rate_a');
            $store_all_rate_b = $request->get('store_all_rate_b');
            $store_all_rate_b_top = $request->get('store_all_rate_b_top', '20');
            $store_all_rate_c = $request->get('store_all_rate_c');
            $store_all_rate_d = $request->get('store_all_rate_d');
            $store_all_rate_d_top = $request->get('store_all_rate_d_top', '20');

            $edit_user = User::where('id', $user_id)->first();
            if (!$edit_user) {
                return json_encode(['status' => 2, 'message' => '代理商不存在', 'data' => []]);
            }


            //自己的费率
            $user_rate = UserRate::where('user_id', $user_id)
                ->where('ways_type', $ways_type)
                ->first();

            if (!$user_rate) {
                return json_encode(['status' => 2, 'message' => '通道成本费率未设置请联系上级', 'data' => []]);

            }
            //不能小于自己的成本
            if ($store_all_rate_a < $user_rate->rate_a) {
                return json_encode(['status' => 2, 'message' => '小于1000贷记卡费率不能低于自己的成本', 'data' => []]);
            }

            //不能小于自己的成本
            if ($store_all_rate_b < $user_rate->rate_b) {
                return json_encode(['status' => 2, 'message' => '小于1000借记卡费率不能低于自己的成本', 'data' => []]);
            }

            //不能小于自己的成本
            if ($store_all_rate_b_top < $user_rate->rate_b_top) {
                return json_encode(['status' => 2, 'message' => '小于1000借记卡封顶金额不能小于自己的成本', 'data' => []]);
            }


            //不能小于自己的成本
            if ($store_all_rate_c < $user_rate->rate_c) {
                return json_encode(['status' => 2, 'message' => '大于1000贷记卡费率不能低于自己的成本', 'data' => []]);
            }
            //不能小于自己的成本
            if ($store_all_rate_d < $user_rate->rate_d) {
                return json_encode(['status' => 2, 'message' => '大于1000借记卡费率不能低于自己的成本', 'data' => []]);
            }

            //不能小于自己的成本
            if ($store_all_rate_d_top < $user_rate->rate_d_top) {
                return json_encode(['status' => 2, 'message' => '大于1000借记卡封顶金额不能小于自己的成本', 'data' => []]);
            }


            $astore_ways_desc = DB::table('store_ways_desc')
                ->where('ways_type', $ways_type)
                ->select('company')
                ->first();
            if (!$astore_ways_desc) {
                return json_encode(['status' => 2, 'message' => '通道基础数据不存在']);
            }


            $ways = UserRate::where('user_id', $user_id)->where('ways_type', $ways_type)->first();

            //请先设置扫码通道的费率
            if (!$ways) {
                return json_encode(['status' => 2, 'message' => '请先设置扫码通道的费率']);
            }
            $company = $ways->company;

            $data = [
                'store_all_rate_a' => $store_all_rate_a,
                'store_all_rate_b' => $store_all_rate_b,
                'store_all_rate_b_top' => $store_all_rate_b_top,
                'store_all_rate_c' => $store_all_rate_c,
                'store_all_rate_d' => $store_all_rate_d,
                'store_all_rate_d_top' => $store_all_rate_d_top,
            ];


            UserRate::where('user_id', $user_id)->where('company', $company)
                ->update($data);


            return json_encode(['status' => 1, 'message' => '修改成功', 'data' => $request->except(['token'])]);


        } catch (\Exception $exception) {
            return json_encode(['status' => 0, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }

    }


    //查询单个通道详细
    public function user_ways_info(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id');
            $ways_type = $request->get('ways_type');

            $data = [];
            $store_ways_desc = DB::table('store_ways_desc')->where('ways_type', $ways_type)->where('is_show', '1')->first();
            $data['ways_desc'] = $store_ways_desc->ways_desc;
            $data['ways_source'] = $store_ways_desc->ways_source;
            $data['settlement_type'] = $store_ways_desc->settlement_type;
            $data['ways_type'] = $store_ways_desc->ways_type;

            $has = UserRate::where('user_id', $user_id)->where('ways_type', $ways_type)->first();
            if ($has) {
                $data['rate'] = $has->rate;
                $data['rate_a'] = $has->rate_a;
                $data['rate_b'] = $has->rate_b;
                $data['rate_b_top'] = $has->rate_b_top;
                $data['rate_c'] = $has->rate_c;
                $data['rate_d'] = $has->rate_d;
                $data['rate_d_top'] = $has->rate_d_top;
                $data['store_all_rate'] = $has->store_all_rate;
                $data['store_all_rate_a'] = $has->store_all_rate_a;
                $data['store_all_rate_b'] = $has->store_all_rate_b;
                $data['store_all_rate_b_top'] = $has->store_all_rate_b_top;
                $data['store_all_rate_c'] = $has->store_all_rate_c;
                $data['store_all_rate_d'] = $has->store_all_rate_d;
                $data['store_all_rate_d_top'] = $has->store_all_rate_d_top;

            } else {
                $data['rate'] = '';
                $data['store_all_rate'] = '';
                $data['rate_a'] = '';
                $data['rate_b'] = '';
                $data['rate_b_top'] = '';
                $data['store_all_rate_a'] = '';
                $data['store_all_rate_b'] = '';
                $data['store_all_rate_b_top'] = '';
                $data['rate_c'] = '';
                $data['rate_d'] = '';
                $data['rate_d_top'] = '';
                $data['store_all_rate_c'] = '';
                $data['store_all_rate_d'] = '';
                $data['store_all_rate_d_top'] = '';
            }


            return json_encode(['status' => 1, 'data' => $data]);


        } catch (\Exception $exception) {
            return json_encode(['status' => 0, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }
    }


    //添加支付密码
    public function add_pay_password(Request $request)
    {
        $rsa = new RsaE();
        try {
            $user = $this->parseToken();
            $user_id = $user->user_id;

            //客户端用的我的公钥加密 我用私钥解密
            $data = $rsa->privDecrypt($request->get('sign'));//解密
            parse_str($data, $output);
            $pay_password = isset($output['pay_password']) ? $output['pay_password'] : "";
            $pay_password_confirmed = isset($output['pay_password_confirmed']) ? $output['pay_password_confirmed'] : "";

            if ($pay_password !== $pay_password_confirmed) {
                return json_encode([
                    'status' => 2,
                    'message' => '两次密码不一致'
                ]);
            }

            if ($pay_password) {
                //验证密码
                if (strlen($pay_password) != 6) {
                    return json_encode([
                        'status' => 2,
                        'message' => '密码长度不符合要求'
                    ]);
                }
                $dataIN = [
                    'pay_password' => bcrypt($pay_password),

                ];
                User::where('id', $user_id)->update($dataIN);
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '参数填写不完整'
                ]);
            }

            return json_encode([
                'status' => 1,
                'message' => '支付密码添加成功',
            ]);


        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //修改支付密码
    public function edit_pay_password(Request $request)
    {
        $rsa = new RsaE();
        try {
            $data = $rsa->privDecrypt($request->get('sign'));//解密
            parse_str($data, $output);
            $user = $this->parseToken();
            $user_id = $user->user_id;

            $oldpassword = isset($output['old_pay_password']) ? $output['old_pay_password'] : "";
            $newpassword = isset($output['new_pay_password']) ? $output['new_pay_password'] : "";

            if ($oldpassword && $newpassword == "") {
                $local = User::where('id', $user_id)->first();
                if (!Hash::check($oldpassword, $local->pay_password)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '旧的支付密码不匹配'
                    ]);
                } else {
                    return json_encode([
                        'status' => 1,
                        'message' => '旧的支付密码匹配'
                    ]);
                }
            }

            if (strlen($newpassword) != 6) {
                return json_encode([
                    'status' => 2,
                    'message' => '密码长度不符合要求'
                ]);
            }
            $dataIN = [
                'pay_password' => bcrypt($newpassword),

            ];

            User::where('id', $user_id)->update($dataIN);

            return json_encode([
                'status' => 1,
                'message' => '支付密码修改成功',
                'data' => []
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //忘记支付密码
    public function forget_pay_password(Request $request)
    {
        $rsa = new RsaE();
        try {
            $user = $this->parseToken();
            $user_id = $user->user_id;
            //客户端用的我的公钥加密 我用私钥解密
            $data = $rsa->privDecrypt($request->get('sign'));//解密
            parse_str($data, $output);
            $newpassword = isset($output['new_pay_password']) ? $output['new_pay_password'] : "";
            $code = isset($output['code']) ? $output['code'] : "";
            $msn_local = Cache::get($user->phone . 'editpassword-1');
            //验证验证码
            if ($code != "" && $newpassword == "") {
                if ($code != $msn_local) {
                    return json_encode([
                        'status' => 2,
                        'message' => '短信验证码不匹配'
                    ]);
                } else {
                    return json_encode([
                        'status' => 1,
                        'message' => '短信验证码正确'
                    ]);
                }
            }

            //验证密码
            if (strlen($newpassword) < 6) {
                return json_encode([
                    'status' => 2,
                    'message' => '密码长度不符合要求'
                ]);
            }


            $User = User::where('id', $user_id)->first();

            //验证验证码
            $msn_local = Cache::get($User->phone . 'editpassword-1');
            if ((string)$code != (string)$msn_local) {
                return json_encode([
                    'status' => 2,
                    'message' => '短信验证码不匹配'
                ]);
            }

            $User->update(['pay_password' => bcrypt($newpassword)]);
            $User->save();

            return json_encode([
                'status' => 1,
                'message' => '支付密码修改成功',
                'data' => [],
            ]);


        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }


    //校验支付密码
    public function check_pay_password(Request $request)
    {
        $rsa = new RsaE();
        try {
            $user = $this->parseToken();
            $user_id = $user->user_id;
            $data = $rsa->privDecrypt($request->get('sign'));//解密
            parse_str($data, $output);

            $pay_password = isset($output['pay_password']) ? $output['pay_password'] : "";


            if (strlen($pay_password) != 6) {
                return json_encode([
                    'status' => 2,
                    'message' => '密码长度不符合要求'
                ]);
            }
            $local_pay_password = User::where('id', $user_id)->first();
            if ($local_pay_password && $local_pay_password->pay_password) {
                if (Hash::check($pay_password, $local_pay_password->pay_password)) {
                    return json_encode([
                        'status' => 1,
                        'message' => '支付密码匹配'
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '支付密码不匹配'
                    ]);
                }
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '账号未设置支付密码'
                ]);
            }


        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //检测是否设置过支付密码
    public function is_pay_password(Request $request)
    {
        $user = $this->parseToken();
        $user_id = $user->user_id;
        $User = User::where('id', $user_id)->first();

        if ($User->pay_password) {

            $is_pay_password = 1;
        } else {
            $is_pay_password = 0;
        }

        return json_encode([
            'status' => 1,
            'data' => [
                'is_pay_password' => $is_pay_password,
            ]
        ]);
    }


    public function send_ways_data($data)
    {
        try {
            //开启事务
            $all_pay_ways = DB::table('store_ways_desc')->where('ways_type', $data['ways_type'])->where('is_show', '1')->first();
            $ways = UserRate::where('user_id', $data['user_id'])->where('ways_type', $data['ways_type'])->first();

            // if( $all_pay_ways->ways_source == 'unionpay'){ //云闪付设置为0  20220817
            //     $data['rate'] = '0';
            // }
            $data = [
                'rate' => $data['rate'],
                'settlement_type' => $all_pay_ways->settlement_type,
                'user_id' => $data['user_id'],
                'ways_type' => $all_pay_ways->ways_type,
                'company' => $all_pay_ways->company,
            ];

            if ($ways) {
                $ways->update($data);
                $ways->save();
            } else {
                UserRate::create($data);
            }

//            foreach ($all_pay_ways as $k => $v) {
//                $ways = UserRate::where('user_id', $data['user_id'])->where('ways_type', $v->ways_type)->first();
//                try {
//                    DB::beginTransaction();
//                    if( $v->ways_source == 'unionpay'){ //云闪付设置为0  20220817
//                        $data['rate'] = '0';
//                    }
//                    $data = [
//                        'rate' => $data['rate'],
//                        'settlement_type' => $v->settlement_type,
//                        'user_id' => $data['user_id'],
//                        'ways_type' => $v->ways_type,
//                        'company' => $v->company,
//                    ];
//
//                    if ($ways) {
//                        $ways->update($data);
//                        $ways->save();
//                    } else {
//                        UserRate::create($data);
//                    }
//                    DB::commit();
//                } catch (\Exception $e) {
//                    DB::rollBack();
//                    return [
//                        'status' => 2,
//                        'message' => '通道入库更新失败',
//                    ];
//                }
//            }


            return [
                'status' => $data['status'],
                'message' => $data['status_desc'],
            ];

        } catch (\Exception $e) {
            return [
                'status' => 2,
                'message' => '通道入库更新失败',
            ];
        }
    }


    public function send_ways_store_all_data($data)
    {
        try {
            //开启事务
            $all_pay_ways = DB::table('store_ways_desc')->where('ways_type', $data['ways_type'])->where('is_show', '1')->first();
            $ways = UserRate::where('user_id', $data['user_id'])->where('ways_type', $data['ways_type'])->first();

            // if( $all_pay_ways->ways_source == 'unionpay'){ //云闪付设置为0  20220817
            //     $data['store_all_rate'] = '0';
            // }

            $data = [
                'store_all_rate' => $data['store_all_rate'],
                'settlement_type' => $all_pay_ways->settlement_type,
                'user_id' => $data['user_id'],
                'ways_type' => $all_pay_ways->ways_type,
                'company' => $all_pay_ways->company,
            ];

            if ($ways) {
                $ways->update($data);
                $ways->save();
            } else {
                UserRate::create($data);
            }

//            foreach ($all_pay_ways as $k => $v) {
//                $ways = UserRate::where('user_id', $data['user_id'])->where('ways_type', $v->ways_type)
//                    ->first();
//                try {
//                    DB::beginTransaction();
//                    $data = [
//                        'store_all_rate' => $data['store_all_rate'],
//                        'settlement_type' => $v->settlement_type,
//                        'user_id' => $data['user_id'],
//                        'ways_type' => $v->ways_type,
//                        'company' => $v->company,
//                    ];
//                    if ($ways) {
//                        $ways->update($data);
//                        $ways->save();
//                    } else {
//                        UserRate::create($data);
//                    }
//                    DB::commit();
//                } catch (\Exception $e) {
//                    DB::rollBack();
//                    return [
//                        'status' => 2,
//                        'message' => '通道入库更新失败',
//                    ];
//                }
//            }


            return [
                'status' => $data['status'],
                'message' => $data['status_desc'],
            ];

        } catch (\Exception $e) {
            return [
                'status' => 2,
                'message' => '通道入库更新失败',
            ];
        }
    }


    //登录人修改密码
    public function edit_pwd(Request $request)
    {
        try {
            $token = $this->parseToken();

            $user_id = $request->get('user_id', '');
            $old_pwd = $request->get('old_pwd', '');
            $new_pwd = $request->get('new_pwd', '');
            $re_new_pwd = $request->get('re_new_pwd', '');

            $id = $token->user_id;

            $check_data = [
                'old_pwd' => '旧密码',
                'new_pwd' => '新密码',
                're_new_pwd' => '确认新密码'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            if ($user_id) {
                $id = $user_id;
            }

            $user_info = User::where('id', $id)
                ->where('is_delete', '0')
                ->first();
            if ($user_info) {
                //验证旧的密码
                if (!Hash::check($old_pwd, $user_info->password)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '旧的登陆密码不匹配'
                    ]);
                }
                if ($new_pwd !== $re_new_pwd) {
                    return json_encode([
                        'status' => 2,
                        'message' => '两次密码不一致'
                    ]);
                }
                if (strlen($new_pwd) < 6) {
                    return json_encode([
                        'status' => 2,
                        'message' => '密码长度不能低于6位'
                    ]);
                }
                $dataIN = [
                    'password' => bcrypt($new_pwd),
                ];
                $res = $user_info->update($dataIN);
                if ($res) {
                    $data = [
                        'status' => 1,
                        'message' => '密码修改成功',
                    ];
                } else {
                    $data = [
                        'status' => 2,
                        'message' => '密码修改失败',
                    ];
                }

                return json_encode($data);
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '用户不存在或状态异常'
                ]);
            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }

    //重置登录密码
    public function reset_pwd(Request $request)
    {
        try {
            $token = $this->parseToken();

            $user_id = $request->get('user_id', '');

            $id = $token->user_id;

            if ($user_id) {
                $id = $user_id;
            }

            $user_info = User::where('id', $id)
                ->where('is_delete', '0')
                ->first();
            if ($user_info) {

                $new_pwd = '000000';
                $dataIN = [
                    'password' => bcrypt($new_pwd),
                ];
                $res = $user_info->update($dataIN);
                if ($res) {
                    $data = [
                        'status' => 1,
                        'message' => '密码重置成功',
                    ];
                } else {
                    $data = [
                        'status' => 2,
                        'message' => '密码重置失败',
                    ];
                }

                return json_encode($data);
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '用户不存在或状态异常'
                ]);
            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }

    public function sendSms($phone, $app_key, $app_secret, $SignName, $TemplateCode, $data)
    {
        $demo = new AliSms($app_key, $app_secret);
        $response = $demo->sendSms(
            $SignName, // 短信签名
            $TemplateCode, // 短信模板编号
            $phone, // 短信接收者
            $data
        );

        return $response;
    }


    //添加下级业务员
//    public function add_sub_user_new(Request $request)
//    {
//        try {
//            $token = $this->parseToken();
//            $login_user_id = $token->user_id;
//
//            $pid_name = $request->get('pid_name'); //上级公司名称
//            $level = $request->get('level'); //等级
//            $sub_profit_ratio = $request->get('subProfitRatio', '');  //下级利润比例%
//            $profit_ratio = $request->get('profitRatio', '');  //利润比例%
//
//            $phone = $request->get('phone'); //手机号
//            $password = $request->get('password'); //密码
//            $name = $request->get('user_name'); //公司名称
//            $people = $request->get('people', ''); //联系人姓名
//            $province_name = $request->get('province_name', '全国');
//            $city_name = $request->get('city_name', '全国');
//            $area_name = $request->get('area_name', '全国');
//            $address = $request->get('address', '全国');
//            $p_id = $request->get('pid', '');  //上级id
//            $p_id = isset($p_id) ? $p_id : $token->user_id;
//
//            $pid_user = User::where('id', $p_id)
//                ->where('is_delete', '0')
//                ->first();
//            if (!$pid_user) {
//                return json_encode([
//                    'status'=>'2',
//                    'message'=>'上级代理不存在或状态异常'
//                ]);
//            }
//
//            $login_user = User::where('id', $login_user_id)->first();
//            $hasPermission = $login_user->hasPermissionTo('添加下级代理');
//            if (!$hasPermission) {
//                return json_encode([
//                    'status' => 2,
//                    'message' => '暂不支持添加下级代理'
//                ]);
//            }
//
//            //4级以后如果没有开权限就不能在开了
////            if ($token->level > 5) {
////                return json_encode([
////                    'status' => 2,
////                    'message' => '暂不支持开下级账户'
////                ]);
////            }
//
//            $s_code = rand(1000, 9999);
//            $check_data = [
//                'phone' => '手机号',
//                'password' => '密码',
//                'user_name' => '公司名字',
//                'people' => '联系人姓名'
//            ];
//            $check = $this->check_required($request->except(['token']), $check_data);
//            if ($check) {
//                return json_encode([
//                    'status' => '2',
//                    'message' => $check
//                ]);
//            }
//
//            //验证手机号
//            if (!preg_match("/^1[3456789]{1}\d{9}$/", $phone)) {
//                return json_encode([
//                    'status' => '0',
//                    'message' => '手机号码不正确'
//                ]);
//            }
//
//            $data = $request->all();
//            $rules = [
//                'phone' => 'required|min:11|max:11|unique:users',
//            ];
//            $validator = Validator::make($data, $rules);
//            if ($validator->fails()) {
//                return json_encode([
//                    'status' => '2',
//                    'message' => '账号已注册请直接登录'
//                ]);
//            }
//
//            $rules = [
//                's_code' => 'required|min:4|max:4|unique:users',
//            ];
//            $validator = Validator::make(['s_code' => $s_code], $rules);
//            if ($validator->fails()) {
//                return json_encode([
//                    'status' => '2',
//                    'message' => '激活码生成失败，请重新提交资料'
//                ]);
//            }
//
//            //验证密码
//            if (strlen($password) < 6) {
//                return json_encode([
//                    'status' => 2,
//                    'message' => '密码长度不得少于6位'
//                ]);
//            }
//
//            $level_name = "业务员";
//            if ($level == 1) {
//                $level_name = "服务商";
//            }
//            if ($level == 2) {
//                $level_name = "代理商";
//            }
//
////            $level_name=$request->get('level_name',$level_name); //展示名称
//            $dataIn['config_id'] = $pid_user->config_id;
//            $dataIn['email'] = $phone . '@139.com';
//            $dataIn['level'] = $level;
//            $dataIn['level_name'] = $level_name;
//            $dataIn['logo'] = $people;  //联系人姓名
//            $dataIn['password'] = bcrypt($password);
//            $dataIn['name'] = $name;
//            $dataIn['phone'] = $phone;
//            $dataIn['s_code'] = $s_code;
//            $dataIn['pid'] = $p_id;
//            $dataIn['pid_name'] = $pid_name;
//            $dataIn['province_name'] = $province_name;
//            $dataIn['city_name'] = $city_name;
//            $dataIn['area_name'] = $area_name;
//            $dataIn['address'] = $address;
//            if (isset($profit_ratio) && $profit_ratio) { //分润比例
//                $dataIn['profit_ratio'] = $profit_ratio;
//            } else {
//                $dataIn['profit_ratio'] = $pid_user->sub_profit_ratio;
//            }
//            if (isset($sub_profit_ratio) && $sub_profit_ratio) $dataIn['sub_profit_ratio'] = $sub_profit_ratio; //下级分润比例
//            $dataIn['s_code_url'] = url('/api/user/s_code_url?s_code=' . $s_code);
//            $dataIn['sub_code_url'] = url('/api/user/sub_code_url?s_code=' . $s_code);
//
//            //超级服务商
//            if ($pid_user->pid == 0) {
//                $dataIn['config_id'] = $phone;
//                $dataIn['level'] = 1;
//            }
//
//            $User = User::create($dataIn);
//
//            //自动分配权限
//            try {
//                $user_id = $User->id;
//                $user = User::where('id', $user_id)->first();
//                $Permission = Role::where('display_name', $level_name)->first();
//                if ($Permission) {
//                    $user->assignRole($Permission->id);
//                }
//            } catch (\Exception $ex) {
//                Log::info('添加代理商-错误');
//                Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
//                return json_encode([
//                    'status' => -1,
//                    'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine(),
//                ]);
//            }
//
////			  添加成功后，发送短信通知
//            if ($User && $phone) {
//                $config_id = $user->config_id;
//
//                //发送"合伙人通知"短信息通知
//                $config = SmsConfig::where('type', '12')
//                    ->where('config_id', $config_id)
//                    ->first();
//                if (!$config) {
//                    $config = SmsConfig::where('type', '12')
//                        ->where('config_id', '1234')
//                        ->first();
//                }
//                if ($config && $phone && $password) {
//                    $phone = trim($phone);
//                    $sms_data = [
//                        'Managee' => $phone,
//                        'passwordd' => $password
//                    ];
//                    $sms_result = $this->sendSms($phone, $config->app_key, $config->app_secret, $config->SignName, $config->TemplateCode, $sms_data);
//                    if ($sms_result->Code == 'OK') {
//                        Log::info('合伙人通知，'.$phone.'发送短信成功');
//                    } else {
//                        Log::info('合伙人通知，'.$phone.'发送短信失败: '.$sms_result->Message);
//                    }
//                }
//            }
//
//            return json_encode([
//                'status' => 1,
//                'message' => '下级账号添加成功',
//                'data' => $request->except(['token'])
//            ]);
//        } catch (\Exception $exception) {
//            return json_encode([
//                'status' => -1,
//                'message' => $exception->getMessage() . $exception->getFile()
//            ]);
//        }
//    }


    //代理商转移
    public function update_affiliation(Request $request)
    {
        $user_id = $request->get('userId', ''); //代理商id
        $user_pid = $request->get('userPid', ''); //转移到谁下
        Log::info($user_id);
        $check_data = [
            'userId' => '待转代理商',
            'userPid' => '目标代理商'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            return json_encode([
                'status' => '2',
                'message' => $check
            ]);
        }

        $token = $this->parseToken();
        if ($token->level != '0') {
            return json_encode([
                'status' => '2',
                'message' => '非平台账号无权操作',
            ]);
        }

        $user_info = User::where('id', $user_id)
            ->where('is_delete', '0')
            ->first();

        if (!$user_info) {
            return json_encode([
                'status' => '2',
                'message' => '代理商不存在或状态异常',
            ]);
        }

        $user_pid_info = User::where('id', $user_pid)
            ->where('is_delete', '0')
            ->first();
        if (!$user_pid_info) {
            return json_encode([
                'status' => '2',
                'message' => '目标代理商不存在或状态异常',
            ]);
        }

        if ($token->user_id == $user_id) {
            return json_encode([
                'status' => '2',
                'message' => '平台账号不能被转移',
            ]);
        }

        if ($user_info->pid == $user_pid_info->id) {
            return json_encode([
                'status' => '2',
                'message' => '已是所属关系，请勿重复操作',
            ]);
        }

        $level = $user_pid_info->level + 1;

        $level_name = "业务员";
        if ($level == 1) {
            $level_name = "服务商";
        }
        if ($level == 2) {
            $level_name = "代理商";
        }

        try {
            $res = $user_info->update([
                'pid' => $user_pid_info->id,
                'level' => $level,
                'level_name' => $level_name,
                'pid_name' => $user_pid_info->name
            ]);
            if ($res) {
                return json_encode([
                    'status' => '1',
                    'message' => '转移成功'
                ]);
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '转移失败'
                ]);
            }

        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ]);
        }
    }


    //验证平台支付密码
    public function check_platform_pay_password(Request $request)
    {
        $rsa = new RsaE();
        try {
            $token = $this->parseToken();

            $data = $rsa->privDecrypt($request->post('sign')); //解密
            parse_str($data, $output);
            $pay_password = isset($output['pay_password']) ? $output['pay_password'] : "";
            if (strlen($pay_password) != 6) {
                return json_encode([
                    'status' => '2',
                    'message' => '密码长度不符合要求'
                ]);
            }

            $platform_pay_password = User::where('level', '0')
                ->where('pid', '0')
                ->first();
            if ($platform_pay_password && $platform_pay_password->pay_password) {
                if (Hash::check($pay_password, $platform_pay_password->pay_password)) {
                    return json_encode([
                        'status' => '1',
                        'message' => '支付密码匹配'
                    ]);
                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => '支付密码不匹配'
                    ]);
                }
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '账号未设置支付密码'
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine()
            ]);
        }
    }

    //修改赏金比例
    public function percentage(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id', $user->user_id);
            $profit_ratio = $request->get('percentage', '100');
            $data = [
                'profit_ratio' => $profit_ratio,
            ];
            $user = User::where('id', $user_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '用户不存在'
                ]);
            }

            $obj = User::where('id', $user_id)->update($data);
            if ($obj) {
                return json_encode([
                    'status' => 1,
                    'message' => '修改成功',
                ]);
            } else {
                return json_encode([
                    'status' => -1,
                    'message' => '修改失败',
                ]);
            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile()
            ]);
        }
    }


    //设置提现
    public function set_withdraw(Request $request)
    {
        try {
            $user_id = $request->get('user_id', '');
            $local = User::where('id', $user_id)->first();
            $is_withdraw = $local->is_withdraw;
            if ($is_withdraw == 1) {
                $dataIN = [
                    'is_withdraw' => 0
                ];
            } else {
                $dataIN = [
                    'is_withdraw' => 1
                ];
            }

            User::where('id', $user_id)->update($dataIN);
            $data = [
                'status' => 1,
                'is_withdraw' => $dataIN['is_withdraw'],
                'message' => '提现设置成功',
            ];
            return json_encode($data);

        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }

    //设置零费率开关
    public function set_zeroRate(Request $request)
    {
        try {
            $user_id = $request->get('user_id', '');
            $local = User::where('id', $user_id)->first();
            $is_zero_rate = $local->is_zero_rate;
            if ($is_zero_rate == 1) {
                $dataIN = [
                    'is_zero_rate' => 0
                ];
            } else {
                $dataIN = [
                    'is_zero_rate' => 1
                ];
            }

            User::where('id', $user_id)->update($dataIN);
            $data = [
                'status' => 1,
                'is_zero_rate' => $dataIN['is_zero_rate'],
                'message' => '设置成功',
            ];
            return json_encode($data);

        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }

    //设置进件状态
    public function set_incoming(Request $request)
    {
        try {
            $user_id = $request->get('user_id', '');
            $local = User::where('id', $user_id)->first();
            $is_incoming = $local->is_incoming;
            if ($is_incoming == 1) {
                $dataIN = [
                    'is_incoming' => 0
                ];
            } else {
                $dataIN = [
                    'is_incoming' => 1
                ];
            }

            User::where('id', $user_id)->update($dataIN);
            $data = [
                'status' => 1,
                'is_incoming' => $dataIN['is_incoming'],
                'message' => '进件状态设置成功',
            ];
            return json_encode($data);

        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }

    //设置绑码状态
    public function set_bind_qr(Request $request)
    {
        try {
            $user_id = $request->get('user_id', '');
            $local = User::where('id', $user_id)->first();
            $is_bind_qr = $local->is_bind_qr;
            if ($is_bind_qr == 1) {
                $dataIN = [
                    'is_bind_qr' => 0
                ];
            } else {
                $dataIN = [
                    'is_bind_qr' => 1
                ];
            }

            User::where('id', $user_id)->update($dataIN);
            $data = [
                'status' => 1,
                'is_bind_qr' => $dataIN['is_bind_qr'],
                'message' => '绑码状态设置成功',
            ];
            return json_encode($data);

        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }

    public function users_lists_all(Request $request)
    {
        try {
            $token = $this->parseToken();
            $login_user_id = $token->user_id;
            $user_id = $request->get('user_id', $login_user_id);
            $phone = $request->get('phone', '');
            $user_name = $request->get('user_name', '');
            $sub_type = $request->get('sub_type', '');
            $level = $request->get('level', '');
            $Display = $request->get('display', ''); //是否全部显示手机号
            $pid = $request->get('pid', ''); //是否全部显示手机号
            $where = [];
            $where[] = ['u.is_delete', '=', 0];
            if ($user_name) {
                $where[] = ['u.name', 'like', '%' . $user_name . '%'];
            } else {
                if ($level) {
                    $where[] = ['u.level', '=', $level];
                }
            }
            if ($phone) {
                $where[] = ['u.phone', '=', $phone];
            }
            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("users as u");
            } else {
                $obj = DB::table('users as u');
            }

            if ($sub_type == "1") {
                $where[] = ['u.level', '<', '2'];
            }
            if ($pid) {
                $user_id = $pid;
            }
            // $querySql = " select id, pid, money, name, phone, level,level_name, s_code,
            // logo, profit_ratio, sub_profit_ratio,is_withdraw from users where id = 54
            // or id in( SELECT id FROM ( SELECT t1.id,IF(FIND_IN_SET(pid, @pids) > 0, @pids := CONCAT(@pids, ',', id), 0)
            // AS ischild FROM (SELECT id,pid FROM users t ORDER BY pid, id ) t1, (SELECT @pids :=54) t2 ) t3 WHERE ischild != 0 ) order by pid,level";

            if ($user_id == 1) {
                $obj->where($where)
                    ->whereIn("u.id", $this->getSubIdsAll($user_id))
                    ->select('u.id', 'u.pid', 'u.money', 'u.name', 'u.phone', 'u.level',
                        'u.s_code', 'u.logo', 'u.profit_ratio', 'u.sub_profit_ratio',
                        'u.is_withdraw', 'u.pid_name', 'u.source', 'u.is_zero_rate', 'u.reward_money', 'u.updated_at', 'u.is_incoming', 'u.is_bind_qr', 'u.shopping_reward_num')
                    ->selectRaw('IFNULL((SELECT sum(amount) from user_withdrawals_records WHERE user_id=u.id and status=1),0) as tx_amount')
                    ->get();
            } else {
                $obj->where($where)
                    ->whereIn("u.id", $this->getSubIdsAll($user_id))
                    ->select('u.id', 'u.pid', 'u.money', 'u.name', 'u.phone', 'u.level',
                        'u.s_code', 'u.logo', 'u.profit_ratio', 'u.sub_profit_ratio',
                        'u.is_withdraw', 'u.pid_name', 'u.source', 'u.is_zero_rate', 'u.reward_money', 'u.updated_at', 'u.is_incoming', 'u.is_bind_qr', 'u.shopping_reward_num')
                    ->selectRaw('IFNULL((SELECT sum(amount) from user_withdrawals_records WHERE user_id=u.id and status=1),0) as tx_amount')
                    ->get();
            }


            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $time = date("Y-m-01 00:00:00", strtotime("-1 months"));
            foreach ($data as $user) {
                $agentLevelsInfos = UserMonthLevel::where('user_id', $user->id)
                    ->where('start_time', $time)->select('level_weight')->first();
                if ($agentLevelsInfos) {
                    $user->agent_level = 'V' . $agentLevelsInfos->level_weight;
                } else {
                    $user->agent_level = 'V1';
                }
                $user->sum = number_format($user->money + $user->reward_money, 2, '.', '');
            }
            if (!$Display) {
                array_walk_recursive($obj, function (&$values, $keys) {
                    if ($keys == 'phone') {
                        $values = substr($values, 0, 3) . '****' . substr($values, 7); //手机号加盐
                    }

                    return $values;
                });
            }
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    function getUserPhone(Request $request)
    {
        //$phone = $request->get('phone', '');
        $user_id = $request->get('user_id', '');
        $password = $request->get('password', '');
        $user = User::where('id', $user_id)->first();
        if (password_verify($password, $user->password)) {
            return json_encode([
                'status' => 1,
                'message' => '获取成功',
                'data' => $user
            ]);
        } else {
            return json_encode([
                'status' => -1,
                'message' => '登录密码错误'
            ]);
        }

    }

    //查询代理分润比例（建行通道）
    public function getRateProfit(Request $request)
    {
        try {
            $userToken = $this->parseToken();
            $user_id = $request->get('user_id');
            $p_id = $request->get('p_id');

            $user = User::where('id', $p_id)->first();
            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '获取数据失败'
                ]);
            }

            $level = 1;
            if ($user->pid == 0) { //顶级代理
                $level = 1;
                $p_profit = 100;
                // }else if($user->pid == 1){
                //     $level = 2;
            }
            //获取上级代理信息
            if ($user->pid != 0) {
                // $Users = User::where('id', $p_id)
                //     ->select('id', 'pid', 'pid_name', 'name', 'logo','is_withdraw','source')
                //     ->get()->first();

                $newPid = $user->pid;

                $entity = UserRateProfit::where('user_id', $p_id)
                    ->where('pid', $newPid)
                    ->select()
                    ->first();

                if ($entity) {
                    $level = $entity->level + 1;
                    $p_profit = $entity->profit;
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '获取数据失败'
                    ]);
                }

            }

            $data = [];

            $profit = UserRateProfit::where('user_id', $user_id)
                ->where('pid', $p_id)
                ->where('level', $level)
                ->select()
                ->first();

            if (empty($profit)) {
                $data['id'] = '';
                $data['user_id'] = '';
                $data['pid'] = '';
                $data['level'] = $level;
                $data['profit'] = '';
                $data['p_profit'] = $p_profit;

            } else {
                $data['id'] = $profit->id;
                $data['user_id'] = $profit->user_id;
                $data['pid'] = $profit->pid;
                $data['level'] = $profit->level;
                $data['profit'] = $profit->profit;
                $data['p_profit'] = $p_profit;
            }

            return json_encode([
                'status' => 1,
                'data' => $data
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => '-1',
                'message' => $exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine()
            ]);
        }
    }

    //编辑代理分润比例（建行通道）
    public function editRateProfit(Request $request)
    {
        try {
            $userToken = $this->parseToken();
            $user_id = $request->get('user_id');
            $p_id = $request->get('p_id');
            $level = $request->get('level', '1');
            $p_profit = $request->get('p_profit');
            $profit = $request->get('profit');

            $check_data = [
                'profit' => '设置比例',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            //分润比例设置不得超过上级的数值，例如 0.8，可设置0——0.8
            if ($profit > $p_profit) {
                return json_encode([
                    'status' => 2,
                    'message' => '分润比例设置不得超过上级的数值'
                ]);
            }

            $user = User::where('id', $p_id)->first();

            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '获取数据失败'
                ]);
            }

            //获取上级代理信息
            if ($user->pid != 0) {
                // $Users = User::where('id', $p_id)
                //     ->select('id', 'pid', 'pid_name', 'name', 'logo','is_withdraw','source')
                //     ->get()->first();

                $newPid = $user->pid;

                $entity = UserRateProfit::where('user_id', $p_id)
                    ->where('pid', $newPid)
                    ->select()
                    ->first();

                if ($entity) {
                    $level = $entity->level + 1;
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '获取数据失败'
                    ]);
                }
            }

            $obj = UserRateProfit::where('user_id', $user_id)
                ->where('pid', $p_id)
                ->where('level', $level)
                ->select()
                ->first();

            if (empty($obj)) {
                $dataIN = [
                    'user_id' => $user_id,
                    'pid' => $p_id,
                    'level' => $level,
                    'profit' => $profit,
                ];

                $obj = UserRateProfit::create($dataIN);
            } else {

                $data['profit'] = $profit;
                $data['level'] = $level;

                $obj->update($data);
                $obj->save();
            }

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($obj);

        } catch (\Exception $exception) {
            return json_encode([
                'status' => '-1',
                'message' => $exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine()
            ]);
        }
    }

    //设置代理零费率结算方式
    public function editSettlementType(Request $request)
    {
        try {
            $userToken = $this->parseToken();
            $user_id = $request->get('user_id');
            $settlement_type = $request->get('settlement_type');

            $check_data = [
                'settlement_type' => '结算方式',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $user = User::where('id', $user_id)
                ->select()
                ->first();
            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '代理商不存在'
                ]);
            }
            User::where('id', $user_id)->update(['settlement_type' => $settlement_type]);
            return json_encode([
                'status' => 1,
                'message' => '设置成功'
            ]);

        } catch (\Exception $exception) {
            return json_encode([
                'status' => '-1',
                'message' => $exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine()
            ]);
        }
    }

    public function register_user(Request $request)
    {
        try {
            $phone = $request->get('phone');
            $password = $request->get('password');
            $name = $request->get('user_name'); //公司名称
            $people = $request->get('people', ''); //联系人姓名
            $province_name = $request->get('province_name', '全国');
            $city_name = $request->get('city_name', '全国');
            $area_name = $request->get('area_name', '全国');
            $address = $request->get('address', '全国');
            $is_withdraw = $request->get('is_withdraw', 1); //提现设置 1=正常 0=禁止，字段默认为1
            $invite_code = $request->get('invite_code', '');
            $id_card = $request->get('id_card', '');
            $marketing_province_name = $request->get('marketing_province_name', '');
            $marketing_province_code = $request->get('marketing_province_code', '');
            $pid_user = User::where('invite_no', $invite_code)
                ->where('is_delete', '0')
                ->select('*')
                ->first();
            if (!$pid_user) {
                return json_encode([
                    'status' => '-1',
                    'message' => '上级代理不存在或状态异常'
                ]);
            }
            $p_id = $pid_user->id;
            $s_code = rand(1000, 9999);
            $check_data = [
                'phone' => '手机号',
                'password' => '密码',
                'user_name' => '公司名字',
                'people' => '联系人姓名',
                'id_card' => '身份证号',
                'marketing_province_name' => '展业省份名称',
                'marketing_province_code' => '展业省份编码'
            ];

            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            //验证手机号
            if (!preg_match("/^1[3456789]{1}\d{9}$/", $phone)) {
                return json_encode([
                    'status' => 0,
                    'message' => '手机号码不正确'
                ]);
            }

            //验证身份证号
            if (!preg_match("/^[1-9]\d{5}(18|19|20|(3\d))\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/", $id_card)) {
                return json_encode([
                    'status' => 0,
                    'message' => '身份证号不正确'
                ]);
            }
            $data = $request->all();
            $rules = [
                'phone' => 'required|min:11|max:11|unique:users',
                'id_card' => 'required|min:18|max:18|unique:users',
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                return json_encode([
                    'status' => 2,
                    'message' => '账号已注册请直接登录'
                ]);
            }

            $rules = [
                's_code' => 'required|min:4|max:4|unique:users',
            ];
            $validator = Validator::make(['s_code' => $s_code], $rules);
            if ($validator->fails()) {
                return json_encode([
                    'status' => 2,
                    'message' => '激活码生成失败，请重新提交资料'
                ]);
            }

            //验证密码
            if (strlen($password) < 6) {
                return json_encode([
                    'status' => 2,
                    'message' => '密码长度不符合要求'
                ]);
            }
            $sub_user = User::where('name', $name)->first();
            if ($sub_user) {
                return json_encode([
                    'status' => 2,
                    'message' => '代理商名称已存在'
                ]);
            }
            $level = $pid_user->level + 1;
            $level_name = "业务员";
            $role_name = "业务员";
            $invite_no = $this->gen_invite_code();
            if ($level == 1) {
                $level_name = "服务商";
                $role_name = "服务商";
            }
            if ($level == 2) {
                $level_name = "代理商";
                $role_name = "代理商";
            }
            if ($p_id) {
                $dataIn['pid'] = $pid_user->id;
                $dataIn['pid_name'] = $pid_user->name;
            }
            $dataIn['config_id'] = $pid_user->config_id;
            $dataIn['email'] = $phone . '@139.com';
            $dataIn['level'] = $level;
            $dataIn['level_name'] = $level_name;
            $dataIn['logo'] = $people;  //联系人姓名
            $dataIn['password'] = bcrypt($password);
            $dataIn['name'] = $name;
            $dataIn['phone'] = $phone;
            $dataIn['s_code'] = $s_code;
            $dataIn['province_name'] = $province_name;
            $dataIn['city_name'] = $city_name;
            $dataIn['area_name'] = $area_name;
            $dataIn['address'] = $address;
            $dataIn['s_code_url'] = url('/api/user/s_code_url?s_code=' . $s_code);
            $dataIn['sub_code_url'] = url('/api/user/sub_code_url?s_code=' . $s_code);
            $dataIn['is_withdraw'] = $is_withdraw;
            $dataIn['profit_ratio'] = $pid_user->profit_ratio;
            $dataIn['source'] = $pid_user->source;
            $dataIn['invite_no'] = $invite_no;
            $dataIn['invite_code'] = $invite_code;
            $dataIn['id_card'] = $id_card;
            $dataIn['marketing_province_name'] = $marketing_province_name;
            $dataIn['marketing_province_code'] = $marketing_province_code;

            //超级服务商
            if ($pid_user->pid == 0) {
                $dataIn['config_id'] = $phone;
                $dataIn['level'] = 1;
            }

            $User = User::create($dataIn);
            UserLevels::created([
                'user_id' => $User->id,
                'level' => 1,  //用户级别信息
            ]);


            return json_encode([
                'status' => 1,
                'message' => '注册成功',
                'data' => $request->except(['token'])
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile()
            ]);
        }
    }

    public function getSettlementType(Request $request)
    {
        try {
            $user_id = $request->get('user_id');
            $check_data = [
                'user_id' => '代理id',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $user = User::where('id', $user_id)
                ->select()
                ->first();
            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '代理不存在'
                ]);
            }
            $this->status = 1;
            $this->message = "数据返回成功";

            return $this->format($user);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    public function app_get_sub_users(Request $request)
    {
        try {
            $token = $this->parseToken();
            $login_user_id = $token->user_id;
            $user_id = $request->get('user_id', $login_user_id);
            $user_name = $request->get('user_name', '');

            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("users");
            } else {
                $obj = DB::table('users');
            }
            $where = [];
            $where[] = ['pid', $user_id];
            if ($user_name) {
                $where[] = ['name', 'like', '%' . $user_name . '%'];
            }
            //获取上一个月时间
            $time_start_upper = date("Y-m-01 00:00:00", strtotime("-1 months")); //获取上个月月初的时间
            //获取上一个月最后一天
            $time_end_upper = date("Y-m-d 23:59:59", strtotime(-date('d') . 'day'));
            //获取本月时间
            $time_start = date("Y-m-01 00:00:00");
            //获取本月最后一天
            $time_end = date("Y-m-d 23:59:59", strtotime("$time_start +1 month -1 day"));

            $obj = $obj->where($where)
                ->where('is_delete', 0)
                ->orderBy('created_at', 'desc')->get();

            // $new_data = $obj->get();

            // $this->t = $new_data->count();
            // $new_data = $this->page($obj)->get();

            foreach ($obj as $user) {
                $ids = $this->getSubIdsAll($user->id);
                $orders_obj_upper = DB::table('orders')->select([
                    'total_amount',
                    DB::raw("SUM(`total_amount`) as `total_amount`")])
                    ->whereIn('user_id', $ids)
                    ->where('pay_status', 1)//成功
                    ->where('created_at', '>=', $time_start_upper)
                    ->where('created_at', '<=', $time_end_upper)
                    ->first();
                $total_amount_upper = $orders_obj_upper->total_amount ? $orders_obj_upper->total_amount : 0.00;
                $user->total_amount_upper = $total_amount_upper;

                $orders_obj = DB::table('orders')
                    ->select([
                        'total_amount',
                        DB::raw("SUM(`total_amount`) as `total_amount`")])
                    ->whereIn('user_id', $ids)
                    ->where('pay_status', 1)//成功
                    ->where('created_at', '>=', $time_start)
                    ->where('created_at', '<=', $time_end)
                    ->first();
                $total_amount = $orders_obj->total_amount ? $orders_obj->total_amount : 0.00;
                $user->total_amount = $total_amount;

                $store_obj = Store::where('status', 1)//成功
                ->where('admin_status', 1)//成功
                ->where('created_at', '>=', $time_start_upper)
                    ->where('created_at', '<=', $time_end_upper)
                    ->whereIn('user_id', $ids)
                    ->select('id')
                    ->get();
                $store_num_upper = count($store_obj);
                $user->store_num_upper = $store_num_upper;

                $store_obj = Store::where('status', 1)//成功
                ->where('admin_status', 1)//成功
                ->where('created_at', '>=', $time_start)
                    ->where('created_at', '<=', $time_end)
                    ->whereIn('user_id', $ids)
                    ->select('id')
                    ->get();
                $store_num = count($store_obj);
                $user->store_num = $store_num;

            }
            // $newData = $obj->get();

            // $this->t = $newData->count();
            // $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($obj);
            // return json_encode([
            //     'status' => 1,
            //     'message' => '返回成功',
            //     'data' => $obj
            // ]);
        } catch (\Exception $ex) {
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine();
            return $this->format();
        }
    }

    public function app_user_info(Request $request)
    {
        try {
            $user = $this->parseToken();
            $user_id = $request->get('user_id', '');
            $user = User::where('id', $user_id)->first();

            if (!$user) {
                return json_encode([
                    'status' => 2,
                    'message' => '用户不存在'
                ]);
            }

            $sub_user = User::where('pid', $user_id)->where('is_delete', 0)->get();
            $sub_user_num = count($sub_user);

            $store = Store::where('user_id', $user_id)->get();
            $store_num = count($store);

            $table = DB::table('orders');
            $order_obj = $table->select([
                DB::raw("SUM(`total_amount`) as `total_amount_sum`"),
            ])
                ->where('pay_status', 1)
                ->where('user_id', $user_id)
                ->first();
            $transaction_sum = $order_obj->total_amount_sum;
            if (!$transaction_sum) {
                $transaction_sum = 0.00;
            }
            $time_start = date("Y-m-01 00:00:00", strtotime("-1 months")); //获取上个月月初的时间
            $userMonthLevel = UserMonthLevel::where('user_id', $user_id)
                ->where('start_time', $time_start)
                ->select('level_weight')
                ->first();
            $level = 'V1';
            if ($userMonthLevel) {
                $agentLevels = AgentLevels::where('level_weight', $userMonthLevel->level_weight)->select('level')->first();
                $level = $agentLevels->level;
            }
            $data = [
                'user_name' => $user->name,
                'phone' => $user->phone,
                'sub_user_num' => $sub_user_num,
                'store_num' => $store_num,
                'transaction_sum' => $transaction_sum,
                'level' => $level
            ];
            return json_encode([
                'status' => 1,
                'message' => '返回成功',
                'data' => $data
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile()
            ]);
        }
    }

    /**
     * 生成邀请码
     */
    public function gen_invite_code()
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";

        while (true) {
            for ($i = 0; $i < 8; $i++) {
                $res .= $chars[mt_rand(0, strlen($chars) - 1)];
            }
            $user = User::where('invite_code', $res)->first();
            if (!$user) {
                break;
            }
        }

        return $res;
    }

    //更换头像
    public function up_likeness(Request $request)
    {
        try {
            $user_id = $request->get('user_id', '');
            $likeness_url = $request->get('likeness_url', '');
            $user = User::where('id', $user_id)->update(['likeness_url' => $likeness_url]);
            if ($user) {
                return json_encode([
                    'status' => 1,
                    'message' => '更换成功'
                ]);
            }
            return json_encode([
                'status' => -1,
                'message' => '更换失败'
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . $exception->getFile()
            ]);
        }
    }

    public function forget_password(Request $request)
    {
        $phone = $request->get('phone', '');
        $user = User::where('is_delete', 0)->where('phone', $phone)->first();
        $templateId = '1858928';
        $nonce = rand(100000, 999999) . '';
        if ($user) {
            $txSmsController = new TxSmsController();
            $txSmsController->sendSms($phone, $nonce);
        } else {
            return json_encode([
                'status' => -1,
                'message' => '手机号未注册'
            ]);
        }
    }

    public function getYsStore(Request $request)
    {
        $store_id = $request->get('store_id', '');
        $YinshengStore = YinshengStore::where('store_id', $store_id)->select('*')->first();
        if ($YinshengStore) {
            return json_encode([
                'status' => 1,
                'message' => '获取数据成功',
                'data' => $YinshengStore
            ]);
        }
        return json_encode([
            'status' => 2,
            'message' => '银盛未入网',
        ]);
    }

    public function getEsStore(Request $request)
    {
        $store_id = $request->get('store_id', '');
        $EasypayStore = EasypayStore::where('store_id', $store_id)->select('*')->first();
        if ($EasypayStore) {
            return json_encode([
                'status' => 1,
                'message' => '获取数据成功',
                'data' => $EasypayStore
            ]);
        }
        return json_encode([
            'status' => 2,
            'message' => '易生未入网',
        ]);
    }

    public function emptyUserAuth(Request $request)
    {
        $user_id = $request->get('user_id');
        $res = UserAuths::where('user_id', $user_id)->delete();
        if ($res) {
            return json_encode([
                'status' => 1,
                'message' => '清除成功',
            ]);
        } else {
            return json_encode([
                'status' => 2,
                'message' => '清除失败',
            ]);
        }

    }

    public function emptyEsign(Request $request)
    {
        $user_id = $request->get('user_id');
        $res = EsignAuths::where('user_id', $user_id)->delete();
        if ($res) {
            return json_encode([
                'status' => 1,
                'message' => '清除成功',
            ]);
        } else {
            return json_encode([
                'status' => 2,
                'message' => '清除失败',
            ]);
        }

    }


}
