<?php
namespace App\Api\Controllers\User;


use App\Api\Controllers\BaseController;
use App\Models\AlipayHbOrder;
use App\Models\AlipayHbrate;
use App\Models\AlipayHbReturn;
use App\Models\AlipayUserHbrate;
use App\Models\MerchantStore;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AlipayFqOrderController extends BaseController
{

    public function order(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $user_id = $request->get('user_id', '');
            $user_id = $user_id? $user_id : $user->user_id;
            $pay_status = $request->get('pay_status', '');
            $pay_status = isset($pay_status) ? $pay_status : '';
            $time_start = $request->get('time_start', '');
            $time_start = isset($time_start) ? $time_start : '';

            $time_end = $request->get('time_end', '');
            $time_end = isset($time_end) ? $time_end : '';

            $hb_fq_num = $request->get('hb_fq_num', '');
            $hb_fq_num = isset($hb_fq_num) ? $hb_fq_num : '';

            $out_trade_no = $request->get('out_trade_no', '');
            $out_trade_no = isset($out_trade_no) ? $out_trade_no : '';

            $user_ids = $this->getSubIds($user_id);

            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("alipay_hb_orders");
            } else {
                $obj = DB::table('alipay_hb_orders');
            }
            $where = [];

            if ($out_trade_no) {
                $where[] = ['out_trade_no', 'like', '%' . $out_trade_no . '%'];
            }

            if ($hb_fq_num) {
                $where[] = ['hb_fq_num', '=', $hb_fq_num];
            }

            if ($pay_status) {
                $where[] = ['pay_status', '=', $pay_status];
            }

            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
            }

            if ($time_start) {
                $where[] = ['updated_at', '>=', $time_start];
            }

            if ($time_end) {
                $where[] = ['updated_at', '<=', $time_end];
            }

            $obj = $obj->where($where)
                ->whereIn('user_id', $user_ids)
                ->orderBy('created_at', 'desc');
            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';

            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine();
            return $this->format();
        }
    }


    public function order_info(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $out_trade_no = $request->get('out_trade_no', '');
            $data = AlipayHbOrder::where('out_trade_no', $out_trade_no)->first();
            if (!$data) {
                $this->status = 2;
                $this->message = '订单号不存在';
                return $this->format();
            }
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    public function set_fq_get_list(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');

            $AlipayHbReturn = AlipayHbReturn::where('store_id', $store_id)->get();

            return json_encode([
                'status' => 1,
                'message' => '返回成功',
                'data' => $AlipayHbReturn,
            ]);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }


    public function set_fq_get_rate(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $rate = $request->get('rate', '');//返还比例
            $num = $request->get('num', '');
            $r_type = $request->get('r_type', '');
            $zc_type = $request->get('zc_type', '');

            //门店
            $store = Store::where('store_id', $store_id)
                ->select('user_id')
                ->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在'
                ]);
            }
	    
            //门店费率
            $hb_fq_num_rate = 'hb_fq_num_' . $num;
            $AlipayHbrate = AlipayHbrate::where('store_id', $store_id)
                ->select($hb_fq_num_rate)
                ->first();
            if (!$AlipayHbrate) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店费率未设置'
                ]);
            }
	    
            $store_rate = $AlipayHbrate->$hb_fq_num_rate;

            //代理
            $AlipayUserHbrate = AlipayUserHbrate::where('user_id', $store->user_id)
                ->where('num', $num)
                ->first();
            if (!$AlipayUserHbrate) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店归属代理未设置'
                ]);
            }
	    
            $user_rate = $AlipayUserHbrate->rate;

            //商户归属的代理自己承担这个返还
            if (($rate + $user_rate) > $store_rate) {
                return json_encode([
                    'status' => 2,
                    'message' => '返还费率+成本费率大于商户费率'
                ]);
            }

            //服务商承担这个返还不支持
            if ($zc_type == "1"||$zc_type == "0") {
                return json_encode([
                    'status' => 2,
                    'message' => '暂时不支持服务商和平台承担'
                ]);
            }

            $re_data = [
                'store_id' => $store_id,
                'num' => $num,
                'rate' => $rate,
                'r_type' => $r_type,
                'zc_type' => $zc_type,
            ];

            $check_data = [
                'store_id' => '门店ID',
                'num' => '分期数',
                'rate' => '返还比例',
                'r_type' => '返还类型',
                'zc_type' => '承担方',
            ];
            $check = $this->check_required($re_data, $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $AlipayHbReturn = AlipayHbReturn::where('store_id', $store_id)
                ->where('num', $num)
                ->first();

            if ($AlipayHbReturn) {
                $AlipayHbReturn->update($re_data);
                $AlipayHbReturn->save();
            } else {
                AlipayHbReturn::create($re_data);
            }

            return json_encode([
                'status' => 1,
                'message' => '添加成功',
                'data' => $re_data,
            ]);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }
    

    public function del_fq_get_rate(Request $request)
    {
        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');
            $num = $request->get('num', '');

            $re_data = [
                'store_id' => $store_id,
                'num' => $num,
            ];

            $check_data = [
                'store_id' => '门店ID',
                'num' => '分期数',
            ];
            $check = $this->check_required($re_data, $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            AlipayHbReturn::where('store_id', $store_id)
                ->where('num', $num)
                ->delete();

            return json_encode([
                'status' => 1,
                'message' => '删除成功',
                'data' => $re_data,
            ]);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }
    
    
}
