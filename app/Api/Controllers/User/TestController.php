<?php


namespace App\Api\Controllers\User;


use App\Api\Controllers\BaseController;
use App\Api\Controllers\EasyPay\PayController;
use App\Api\Controllers\Merchant\TemplateMessageController;
use App\Models\SettlementMonthList;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class TestController extends BaseController
{
    public $private_key = 'MIICeQIBADANBgkqhkiG9w0BAQEFAASCAmMwggJfAgEAAoGBAMY5ZLwUzN0wLQhGdgieiaGnpCRzGBflG1Gdv9Du07sLeFe0s4QPDH9+oOqfWQUmwlE8QoI9/XKkLp9TyM1ddf54DwPL38j5K3ACFi7kk7sgM2mTWW0MWF5QE1fHfTLnvxNfFXAJwLdD8bkfXiSJF7hI1FDWKVZtPGU2RbkH5uZdAgMBAAECgYEAispexO4PbwOvVC75sBlJJp3ZaQgJI5nv4dqBFspSB5IFZeuJEfrzdkV0aDLAQsIbpoN50fWTgvSADGC+pUQ+O32PXRSIbI/SEp0WmVeY5uJp3j8TfhL3MfwmEwXcd+fFQ0inLV+OPCK7/T6qzeKu7Gq5xaETiUBxi0IVI+irZ2UCQQDrsEbvEpEm+gT52ZQFpxkpKI8fPZ/mcXwB9vpcJE3H+wif5cQ09BIeWe62FoE7cjgwTZ4Ojz83AI2OX2Zt0m1rAkEA106VTCBPW2/Cl6tth6Es0SmzesEqVIpJC2z1Apmpen6vPHmeo3pz23Q1WwOma1D2YGGybGnb609agvWdUknlVwJBALtv/Xm0Mmvk+oAZYXb9ZS0ubGRUBg6mwhBt8r3EwXcBVeKUQG48jQxGSWnWjABS3gzMLZ82nPeJIQrRJAzDBPECQQCVK87opWIV+cKajp0eYR7J89hz7i1D0WN2VRETsh7nzanffmvY29g1bbghzM+afseUU1vpHRbPW3VcUoujHauVAkEAsn6NKKFeYqQZO+lyFxLVdd2fg/bz4ZrLgrYGS6sk/oHPV0ANTE6khUxD+vUrbg3sbokh2wwb0f+1NRtvbdc/hw==';
    public $public_key = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDGOWS8FMzdMC0IRnYInomhp6QkcxgX5RtRnb/Q7tO7C3hXtLOEDwx/fqDqn1kFJsJRPEKCPf1ypC6fU8jNXXX+eA8Dy9/I+StwAhYu5JO7IDNpk1ltDFheUBNXx30y578TXxVwCcC3Q/G5H14kiRe4SNRQ1ilWbTxlNkW5B+bmXQIDAQAB';
    public $url = 'http://testpay.xiandaixingyi.cn/api/merchant/pay_scanPay';
    public $order_query_url = 'http://testpay.xiandaixingyi.cn/api/merchant/order_query_org';
    public $refund_url = 'http://testpay.xiandaixingyi.cn/api/merchant/refund_org';

    public $orgId = 'CLS02004130502';
    public $storeId = '2023322112132161001';
    public $notify_url = '自己的回调地址';

    public function pay_test(Request $request)
    {
        try {
            $orgId = $request->get('orgId');//机构编号
            $storeId = $request->get('storeId');//商户编号
            $total_amount = $request->get('total_amount');//支付金额
            $code = $request->get('code', '');//支付条码
            $out_trade_no = $request->get('out_trade_no');//订单号
            $check_data = [
                'orgId' => '机构编号',
                'storeId' => '商户编号',
                'orgId' => '机构编号',
                'total_amount' => '付款金额',
                'code' => '付款码编号',
                'out_trade_no' => '订单号'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $data = [
                'total_amount' => $total_amount,
                'code' => $code,
                'out_trade_no' => $out_trade_no
            ];
            $sign = $this->getSign($data, $this->private_key);//生成签名
            $clsData = [
                'orgId' => $orgId,
                'storeId' => $storeId,
                'data' => $data,
                'sign' => $sign,
            ];
            $res = $this->request($clsData, $this->url);//发送请求
            Log::info($res);
        } catch (\Exception $ex) {
            Log::info('error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

    }

    public function pay_jsapi_test(Request $request)
    {
        try {
            $orgId = $request->get('orgId');//机构编号
            $storeId = $request->get('storeId');//商户编号
            $total_amount = $request->get('total_amount');//支付金额
            $total_code = $request->get('total_code', '');//交易类型 21001=易生支付宝 21002=易生微信 21004=易生云闪付
            $out_trade_no = $request->get('out_trade_no');//订单号
            $payerId = $request->get('payerId');//用户唯一标识
            $check_data = [
                'orgId' => '机构编号',
                'storeId' => '商户编号',
                'orgId' => '机构编号',
                'total_amount' => '付款金额',
                'total_code' => '交易类型',
                'out_trade_no' => '订单号',
                'payerId' => '用户唯一标识'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $data = [
                'total_amount' => $total_amount,
                'total_code' => $total_code,
                'out_trade_no' => $out_trade_no,
                'payerId' => $payerId
            ];
            $sign = $this->getSign($data, $this->private_key);//生成签名
            $clsData = [
                'orgId' => $orgId,
                'storeId' => $storeId,
                'data' => $data,
                'notify_url' => $this->notify_url,
                'sign' => $sign,
            ];
            $res = $this->request($clsData, $this->url);//发送请求
            Log::info($res);
        } catch (\Exception $ex) {
            Log::info('error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

    }

    public function order_query_test(Request $request)
    {
        try {
            $orgId = $request->get('orgId');//机构编号
            $storeId = $request->get('storeId');//商户编号
            $out_trade_no = $request->get('out_trade_no');//订单号
            //$refund_amount=$request->get('refund_amount');//退款金额
            $check_data = [
                'orgId' => '机构编号',
                'storeId' => '商户编号',
                'out_trade_no' => '订单号',
                //'refund_amount'=>'退款金额'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $data = [
                'out_trade_no' => $out_trade_no,
                //'refund_amount'=>$refund_amount
            ];
            $sign = $this->getSign($data, $this->private_key);//生成签名
            $clsData = [
                'orgId' => $orgId,
                'storeId' => $storeId,
                'data' => $data,
                'sign' => $sign,
            ];
            $res = $this->request($clsData, $this->order_query_url);//发送请求
            Log::info($res);
            return $res;
        } catch (\Exception $ex) {
            Log::info('error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

    }

    //退款
    public function refund_test(Request $request)
    {
        try {
            $orgId = $request->get('orgId');//机构编号
            $storeId = $request->get('storeId');//商户编号
            $out_trade_no = $request->get('out_trade_no');//订单号
            $check_data = [
                'orgId' => '机构编号',
                'storeId' => '商户编号',
                'out_trade_no' => '订单号'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $data = [
                'out_trade_no' => $out_trade_no
            ];
            $sign = $this->getSign($data, $this->private_key);//生成签名
            $clsData = [
                'orgId' => $orgId,
                'storeId' => $storeId,
                'data' => $data,
                'sign' => $sign,
            ];
            $res = $this->request($clsData, $this->refund_url);//发送请求
            Log::info($res);
            return $res;
        } catch (\Exception $ex) {
            Log::info('error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

    }

    //退款查询
    public function refund_query(Request $request)
    {
        try {
            $channel_id = $request->get('channel_id');
            $mer_id = $request->get('mer_id');
            $device_id = $request->get('device_id');
            $refund_no = $request->get('refund_no');

            $data = [
                'channel_id' => $channel_id,
                'mer_id' => $mer_id,
                'device_id' => $device_id,
                'refund_no' => $refund_no
            ];
            $obj = new \App\Api\Controllers\EasyPay\PayController();
            $res = $obj->refund_query($data);//发送请求
            Log::info($res);
            return $res;
        } catch (\Exception $ex) {
            Log::info('error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

    }

    //生成签名
    public function getSign($content, $privateKey)
    {
        $data = $this->formatBizQueryParaMap($content, false);

        $privateKey = "-----BEGIN RSA PRIVATE KEY-----\n" .
            wordwrap($privateKey, 64, "\n", true) .
            "\n-----END RSA PRIVATE KEY-----";
        $key = openssl_get_privatekey($privateKey);

        openssl_sign($data, $signature, $key, OPENSSL_ALGO_SHA256);
        openssl_free_key($key);
        $sign = base64_encode($signature);
        return $sign;
    }

    //校验 RSA2 签名
    function verifySign($data, $sign, $pubKey)
    {
        $sign = base64_decode($sign);

        ksort($data);
        $data = $this->formatBizQueryParaMap($data, false);

        $pubKey = "-----BEGIN PUBLIC KEY-----\n" .
            wordwrap($pubKey, 64, "\n", true) .
            "\n-----END PUBLIC KEY-----";
        // $pubKey_file = file_get_contents('E://Workspaces-phpPHP/pos.shengdalianmeng.cn/data/cer/rsa_public_key.pem');//公钥
        // if (empty($pubKey_file ))
        // {
        //     return False;
        // }

        $key = openssl_pkey_get_public($pubKey);
        // $key = openssl_get_publickey($pubKey_file);
        $result = openssl_verify($data, base64_decode($sign), $key, OPENSSL_ALGO_SHA256) === 1;

        // exit("verifySign is ：".json_encode($result));

        return $result;
    }

    //组装签名函数
    function formatBizQueryParaMap($paraMap, $urlencode)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if ($urlencode) {
                $v = urlencode($v);
            }
            $buff .= $k . "=" . $v . "&";
        }
        $reqPar = '';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }

    public function request($params, $url)
    {
        $data = stripslashes(json_encode($params));
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); //访问超时时间
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json; charset=UTF-8',
        ]);
        //本地忽略校验证书
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $tmpInfo = curl_exec($curl);
        return $tmpInfo;
    }

    public function testShopping(Request $request)
    {
        $templateMessageController = new TemplateMessageController();
        $templateMessageController->shopping_template_message('2023081612093820348121');
    }

    public function apUsermoney()
    {
        $baseController = new BaseController();
        $ids = $baseController->getSubIdsAll(22);
        unset($ids[0]);
        unset($ids[1]);
        unset($ids[2]);
        unset($ids[3]);
        $users = User::whereIn('id', $ids)->select('id')->get();
        foreach ($users as $user){
            $SettlementMonthLists = SettlementMonthList::where('settlement_type', '2')
                ->where('start_time', '2023-08-01 00:00:00')
                ->where('p_user_id', $user->id)
                ->select('*')->get();
            foreach ($SettlementMonthLists as $SettlementMonthList) {
                $user = User::where('id', $SettlementMonthList->p_user_id)->select('*')->first();
                $money = $user->money - $SettlementMonthList->total_amount;
                $res = User::where('id', $SettlementMonthList->p_user_id)->update(['money' => $money]);
                if ($res) {
                    SettlementMonthList::where('settlement_type', '2')
                        ->where('start_time', '2023-08-01 00:00:00')
                        ->where('p_user_id', $SettlementMonthList->p_user_id)->delete();
                }
            }
        }
//        $SettlementMonthList = SettlementMonthList::where('settlement_type', '1')
//            ->where('start_time', '2023-08-01 00:00:00')
//            ->where('user_id', 23)
//            ->select('*')->first();
//
//        $user = User::where('id', 23)->select('*')->first();
//        $money = $user->money - $SettlementMonthList->total_amount;
//        $res = User::where('id', 23)->update(['money' => $money]);
//        if ($res) {
//            SettlementMonthList::where('settlement_type', '1')
//                ->where('start_time', '2023-08-01 00:00:00')
//                ->where('user_id', 23)->delete();
//        }
        $SettlementMonthLists = SettlementMonthList::where('settlement_type', '2')
            ->where('start_time', '2023-08-01 00:00:00')
            ->where('p_user_id', 23)
            ->select('*')->get();
        foreach ($SettlementMonthLists as $SettlementMonthList) {
            $user = User::where('id', $SettlementMonthList->p_user_id)->select('*')->first();
            $money = $user->money - $SettlementMonthList->total_amount;
            $res = User::where('id', $SettlementMonthList->p_user_id)->update(['money' => $money]);
            if ($res) {
                SettlementMonthList::where('settlement_type', '2')
                    ->where('start_time', '2023-08-01 00:00:00')
                    ->where('p_user_id', $SettlementMonthList->p_user_id)->delete();
            }
        }
    }

    public function getUserId(Request $request)
    {
        $user_id = $request->get('user_id');//机构编号
        $baseController = new BaseController();
        $ids = $baseController->getSubIdsAll($user_id);
        return json_encode(
            ['status' => 1,
                'data' => $ids]
        );
    }
}