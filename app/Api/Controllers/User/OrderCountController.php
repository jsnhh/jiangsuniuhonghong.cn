<?php

namespace App\Api\Controllers\User;

use App\Api\Controllers\BaseController;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use vendor\laravel\framework\src\Illuminate\Database\Query;

class OrderCountController extends BaseController
{
    //获取当前用户下的所有代理
    public function get_users(){
        try {

            $user = $this->parseToken();
            $user_id = $user->user_id;
            $user_ids = $this->getSubIds($user_id);
//            dd($user_ids);
            if (env('DB_D1_HOST')) {
                if (Schema::hasTable('users')) {
                    $obj = DB::connection("mysql_d1")->table('users')->whereIn('id',$user_ids)->select('id','users.name')->get();
                }
            } else {
                if (Schema::hasTable('users')) {
                    $obj = Db::table('users')->whereIn('id',$user_ids)->select('id','users.name')->get();
                }
            }
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($obj);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage() . $exception->getLine();
            return $this->format();
        }
    }

    //代理按月统计
    public function Order_mouth_count(Request $request){
        try {
            $user = $this->parseToken();
            $user_id = $user->user_id;
            $user_id = $request->get('user_id',$user_id);//代理id,默认全部查询
            $month = $request->get('month',date('Ym'));//获取月份 设置或默认当月
            $source_type = $request->get('source_type','');   //设置或默认支付类型
            $company = $request->get('company','');   //设置或默认通道类型   alipay
            $own = $request->get('own','0');   //
            $user_ids = $this->getSubIds($user_id);
            $where = [];
            if ($month==''){
                $month =  date('Ym');
            }
            if ($user_id) {
                $where[] = ['user_id', '=', $user_id];
            }
            if ($month) {
                $where[] = ['month', '=', $month];
            }
            if ($source_type) {
                $where[] = ['source_type', '=', $source_type];
            }
            if ($company) {
                $where[] = ['company', '=', $company];
            }
            //仅自己
            if($own==0){
                if (env('DB_D1_HOST')) {
                    if (Schema::hasTable('user_month_orders')) {
                        $obj = DB::connection("mysql_d1")
                            ->table('user_month_orders')
                            ->join('users','user_id','users.id')
                            ->where($where)
                            //->select('user_month_orders.*','users.name','users.phone');
                            ->select('user_month_orders.user_id','user_month_orders.month','user_month_orders.total_amount','user_month_orders.type','user_month_orders.company','user_month_orders.source_type','user_month_orders.order_sum','user_month_orders.refund_amount','user_month_orders.refund_count','user_month_orders.fee_amount','users.name','users.phone');
                    }
                } else {
                    if (Schema::hasTable('user_month_orders')) {
                        $obj = Db::table('user_month_orders')
                            ->join('users','user_id','users.id')
                            ->where($where)
                            //->select('user_month_orders.*','users.name','users.phone');
                            ->select('user_month_orders.user_id','user_month_orders.month','user_month_orders.total_amount','user_month_orders.type','user_month_orders.company','user_month_orders.source_type','user_month_orders.order_sum','user_month_orders.refund_amount','user_month_orders.refund_count','user_month_orders.fee_amount','users.name','users.phone');
                    }
                }
            }
            //包含自己下所有代理
            if($own==1){
                if (env('DB_D1_HOST')) {
                    if (Schema::hasTable('user_month_orders')) {
                        $obj = DB::connection("mysql_d1")
                            ->table('user_month_orders')
                            ->join('users','user_id','users.id')
                            ->where($where)
                            ->whereIn('user_month_orders.user_id',$user_ids)
                            //->select('user_month_orders.*','users.name','users.phone');
                            ->select('user_month_orders.user_id','user_month_orders.month','user_month_orders.total_amount','user_month_orders.type','user_month_orders.company','user_month_orders.source_type','user_month_orders.order_sum','user_month_orders.refund_amount','user_month_orders.refund_count','user_month_orders.fee_amount','users.name','users.phone');
                    }
                } else {
                    if (Schema::hasTable('user_month_orders')) {
                        $obj = Db::table('user_month_orders')
                            ->join('users','user_id','users.id')
                            ->where($where)
                            ->whereIn('user_month_orders.user_id',$user_ids)
                            //->select('user_month_orders.*','users.name','users.phone');
                            ->select('user_month_orders.user_id','user_month_orders.month','user_month_orders.total_amount','user_month_orders.type','user_month_orders.company','user_month_orders.source_type','user_month_orders.order_sum','user_month_orders.refund_amount','user_month_orders.refund_count','user_month_orders.fee_amount','users.name','users.phone');
                    }
                }

            }

            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage() . $exception->getLine();
            return $this->format();
        }
    }


    //门店按月 统计
    public function order_month_count(Request $request)
    {

        try {
            $user = $this->parseToken();
            $store_id = $request->get('store_id', '');  //1
            $user_id = $request->get('user_id', ''); //0
            $source_type = $request->get('source_type', '');
            $company = $request->get('company', '');

            $month = $request->get('month', date('Ym'));
            if ($month==''){
                $month =  date('Ym');
            }
            $where=[];
            if ($company) {
                $where[] = ['store_month_orders.company', $company];
            }
            if ($month) {
                $where[] = ['store_month_orders.month', $month];
            }

            if ($source_type) {
                $where[] = ['store_month_orders.source_type', '=', $source_type];
            }
            if ($store_id) {
                $where[] = ['store_month_orders.store_id', $store_id];
            }

//            dd($where);
            if (env('DB_D1_HOST')) {
                if (Schema::hasTable('store_month_orders')) {
                    $Order_obj = DB::connection("mysql_d1")->table('store_month_orders')
                        ->join('stores','store_month_orders.store_id','stores.store_id')
                        ->where($where)
                        ->select('store_month_orders.store_id','store_month_orders.month','store_month_orders.total_amount','store_month_orders.company','store_month_orders.source_type','store_month_orders.order_sum','store_month_orders.refund_amount','store_month_orders.refund_amount','store_month_orders.refund_count','store_month_orders.fee_amount','stores.store_name');

                }
            } else {
                if (Schema::hasTable('store_month_orders')) {
                    $Order_obj = DB::table('store_month_orders')
                        ->join('stores','store_month_orders.store_id','stores.store_id')
                        ->where($where)
                        ->select('store_month_orders.store_id','store_month_orders.month','store_month_orders.total_amount','store_month_orders.company','store_month_orders.source_type','store_month_orders.order_sum','store_month_orders.refund_amount','store_month_orders.refund_amount','store_month_orders.refund_count','store_month_orders.fee_amount','stores.store_name');
                }
            }

            $order_data = $Order_obj;
            $this->t = $order_data->count();
            $data = $this->page($order_data)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage();
            return $this->format();
        }
    }
}