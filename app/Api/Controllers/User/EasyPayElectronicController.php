<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2021/3/18
 * Time: 11:57
 */

namespace App\Api\Controllers\User;

use App\Api\Controllers\BaseController;
use App\Models\EasyPayContractInfo;
use App\Models\EasyPayMpsInfo;
use App\Models\EasypayStoresImages;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class EasyPayElectronicController extends BaseController
{
    //测试参数
//    public $url = 'https://api-test.eycard.cn:9443/EasypayMPSystem/ContractServet';
//    public $download_url = 'https://api-test.eycard.cn:9443/EasypayMPSystem/DownloadContractServet.do'; //合同下载
//    public $url_key = 'https://api-test.eycard.cn:9443/EasypayMPSystem/SignServet'; //获取签名key
//
//    public $channelId = '616161622101901';    //渠道编号:616161622101901
//    public $merId = 'MER021322101901';   //商户编号:MER021322101901
//    public $termId = 'TE101901';            //终端编号:TE101901
//    public $templateId = "QT_18432"; //模板编号

    //正式参数
    public $url = 'https://auth.eycard.cn:8443/EasypayMPSystem/ContractServet';
    public $download_url = 'https://auth.eycard.cn:8443/EasypayMPSystem/DownloadContractServet.do'; //合同下载
    public $url_key = 'https://auth.eycard.cn:8443/EasypayMPSystem/SignServet'; //获取签名key

    public $channelId = 'D01X00000801706';    //渠道编号
    public $merId = 'EM0000000000211';   //商户编号
    public $termId = 'ET000211';            //终端编号

    public $channelIdTwo = 'D01X00000801706';    //渠道编号
    public $merIdTwo = 'EM0000000000211';   //商户编号
    public $termIdTwo = 'ET000211';            //终端编号
    public $templateId = "QT_19457"; //模板编号

    //生成签名
    function getSign($params, $key)
    {
        ksort($params);
        $params = array_filter($params);
        $string = "";

        foreach ($params as $name => $value) {
            $string .= $name . '=' . $value . '&';
        }

        $string .= 'key=' . $key;

        return strtoupper(md5($string));
    }

    public function postUrl($params, $url)
    {
        $data = json_encode($params, JSON_UNESCAPED_UNICODE);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); //访问超时时间
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(json_decode($data)));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded;charset=UTF-8'));
        //本地忽略校验证书
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($curl);
        return $result;
    }

    // 封装公共参数
    public function publicParam($new_config_id)
    {

        //$easypayStoresImages = EasypayStoresImages::where('store_id', $storeId)->select('Two')->first();

        $data = [
            'tradeTrace' => date('YmdHis') . mt_rand(1000, 9999), //请求流水，唯一，不包括特殊字符
            'version' => '1.0'
        ];
        if ($new_config_id == '1234') {
            $data['channelId'] = $this->channelId;
            $data['merId'] = $this->merId;
            $data['termId'] = $this->termId;
        }
        if ($new_config_id == '1001') {
            $data['channelId'] = $this->channelIdTwo;
            $data['merId'] = $this->merIdTwo;
            $data['termId'] = $this->termIdTwo;
        }

        return $data;
    }

    // 获取head
    public function getHeadInfo()
    {

        $headInfo = [
            'txTime' => date('YmdHis', time()) // 交易时间：yyyyMMddHHmmss
        ];

        return $headInfo;
    }

    //查询合同 步骤
    public function queryContract(Request $request)
    {

        $token = $this->parseToken();
        $storeId = $request->get('storeId');

        $created_step = 1;
        $electronic = EasyPayMpsInfo::where('store_id', $storeId)->first();
        if ($electronic) {
            $created_step = $electronic->created_step;
        }

        $contractInfo = EasyPayContractInfo::where('store_id', $storeId)->first();

        $data = [
            'created_step' => $created_step,
            'electronicInfo' => $electronic,
            'contractInfo' => $contractInfo,
        ];

        $this->status = 1;
        $this->message = 'success';
        return $this->format($data);

    }

    //添加合同信息
    public function addContractInfo(Request $request)
    {

        $token = $this->parseToken();
        $storeId = $request->get('storeId');

        if (!$storeId) {
            $this->status = -1;
            $this->message = '门店ID为空,请选择门店';
            return $this->format();
        }

        // $electronic = EasyPayMpsInfo::where('store_id', $storeId)->first();
        // if (!$electronic) {
        //     $this->status = -1;
        //     $this->message = '尚未完成开户操作';
        //     return $this->format();
        // }

        $contractInfo = EasyPayContractInfo::where('store_id', $storeId)->first();
        if ($contractInfo) {

            $result = $contractInfo->update([
                "merName" => $request->get('merName'),  // 商户名/注册名
                "merLegal" => $request->get('merLegal'), //法人
                "merAddr" => $request->get('merAddr'),  //商户地址/注册地址
                "contractDateYear" => $request->get('contractDateYear'),    //合同日期 YYYY
                "contractDateMonth" => $request->get('contractDateMonth'),
                "contractDateDay" => $request->get('contractDateDay'),
                "bizName" => $request->get('bizName'),  //经营名称
                "businLic" => $request->get('businLic'),  //统一 社会信用代码
                "bizAddr" => $request->get('bizAddr'),  //经营地址
                "legalCode" => $request->get('legalCode'), //法人身份证号
                "legalPhone" => $request->get('legalPhone'),  //法人电话
                "linkMan" => $request->get('linkMan'),   //联系人
                "linkmanPhone" => $request->get('linkmanPhone'), //联系人电话
                "accName" => $request->get('accName'), //账号名称
                "bankName" => $request->get('bankName'), //开户行
                "account" => $request->get('account'),  //账号
                "chargeTypeDebit" => $request->get('chargeTypeDebit'), //卡支付借记卡
                "chargeTypeDebitMax" => $request->get('chargeTypeDebitMax'), //卡支付借记卡封顶值
                "chargeTypeCredit" => $request->get('chargeTypeCredit'), //卡支付贷记卡
                "chargeTypeUnionpayDebit" => $request->get('chargeTypeUnionpayDebit'), //银联二维码借记卡
                "chargeTypeUnionpayDebitMax" => $request->get('chargeTypeUnionpayDebitMax'), //银联二维码借记卡封顶值
                "chargeTypeUnionpayCredit" => $request->get('chargeTypeUnionpayCredit'), //银联二维码贷记卡
                "chargeTypeUnionpayDisDebit" => $request->get('chargeTypeUnionpayDisDebit'), //银联营销借记卡
                "chargeTypeUnionpayDisDebitMax" => $request->get('chargeTypeUnionpayDisDebitMax'), //银联营销借记卡封顶值
                "chargeTypeUnionpayDisCredit" => $request->get('chargeTypeUnionpayDisCredit'), //银联营销贷记卡
                "chargeTypeWxpay" => $request->get('chargeTypeWxpay'), //微信
                "chargeTypeAlipay" => $request->get('chargeTypeAlipay'), //支付宝
                "d1SettlementRatio" => $request->get('d1SettlementRatio'), //D1结算按比例
                "d1SettlementFee" => $request->get('d1SettlementFee'), //D1结算按笔
                "d0SettlementRatio" => $request->get('d0SettlementRatio'), //D0结算按比例
                "manager" => $request->get('manager'),  //客户经理
                "managerPhone" => $request->get('managerPhone'), //客户经理电话
                "contractDate" => $request->get('contractDate'), //合同日期 YYYY年MM月DD日
                "checkSameCard" => $request->get('checkSameCard', '0'), //同卡支付
                "checkSettleT1" => $request->get('checkSettleT1', '0'), //T1结算
                "checkSettleD1" => $request->get('checkSettleD1', '0'), //D1结算
                "checkSettleD1Ratio" => $request->get('checkSettleD1Ratio', '0'), //D1结算按比例
                "checkSettleD1Fee" => $request->get('checkSettleD1Fee', '0'), //D1结算按笔
                "checkSettleD0" => $request->get('checkSettleD0', '0'), //D0结算
            ]);
        } else {
            $data = [
                "merName" => $request->get('merName'),  // 商户名/注册名
                "merLegal" => $request->get('merLegal'), //法人
                "merAddr" => $request->get('merAddr'),  //商户地址/注册地址
                "contractDateYear" => $request->get('contractDateYear'),    //合同日期 YYYY
                "contractDateMonth" => $request->get('contractDateMonth'),
                "contractDateDay" => $request->get('contractDateDay'),
                "bizName" => $request->get('bizName'),  //经营名称
                "businLic" => $request->get('businLic'),  //统一 社会信用代码
                "bizAddr" => $request->get('bizAddr'),  //经营地址
                "legalCode" => $request->get('legalCode'), //法人身份证号
                "legalPhone" => $request->get('legalPhone'),  //法人电话
                "linkMan" => $request->get('linkMan'),   //联系人
                "linkmanPhone" => $request->get('linkmanPhone'), //联系人电话
                "accName" => $request->get('accName'), //账号名称
                "bankName" => $request->get('bankName'), //开户行
                "account" => $request->get('account'),  //账号
                "chargeTypeDebit" => $request->get('chargeTypeDebit'), //卡支付借记卡
                "chargeTypeDebitMax" => $request->get('chargeTypeDebitMax'), //卡支付借记卡封顶值
                "chargeTypeCredit" => $request->get('chargeTypeCredit'), //卡支付贷记卡
                "chargeTypeUnionpayDebit" => $request->get('chargeTypeUnionpayDebit'), //银联二维码借记卡
                "chargeTypeUnionpayDebitMax" => $request->get('chargeTypeUnionpayDebitMax'), //银联二维码借记卡封顶值
                "chargeTypeUnionpayCredit" => $request->get('chargeTypeUnionpayCredit'), //银联二维码贷记卡
                "chargeTypeUnionpayDisDebit" => $request->get('chargeTypeUnionpayDisDebit'), //银联营销借记卡
                "chargeTypeUnionpayDisDebitMax" => $request->get('chargeTypeUnionpayDisDebitMax'), //银联营销借记卡封顶值
                "chargeTypeUnionpayDisCredit" => $request->get('chargeTypeUnionpayDisCredit'), //银联营销贷记卡
                "chargeTypeWxpay" => $request->get('chargeTypeWxpay'), //微信
                "chargeTypeAlipay" => $request->get('chargeTypeAlipay'), //支付宝
                "d1SettlementRatio" => $request->get('d1SettlementRatio'), //D1结算按比例
                "d1SettlementFee" => $request->get('d1SettlementFee'), //D1结算按笔
                "d0SettlementRatio" => $request->get('d0SettlementRatio'), //D0结算按比例
                "manager" => $request->get('manager'),  //客户经理
                "managerPhone" => $request->get('managerPhone'), //客户经理电话
                "contractDate" => $request->get('contractDate'), //合同日期 YYYY年MM月DD日
                "checkSameCard" => $request->get('checkSameCard', '0'), //同卡支付
                "checkSettleT1" => $request->get('checkSettleT1', '0'), //T1结算
                "checkSettleD1" => $request->get('checkSettleD1', '0'), //D1结算
                "checkSettleD1Ratio" => $request->get('checkSettleD1Ratio', '0'), //D1结算按比例
                "checkSettleD1Fee" => $request->get('checkSettleD1Fee', '0'), //D1结算按笔
                "checkSettleD0" => $request->get('checkSettleD0', '0'), //D0结算
                "store_id" => $storeId,
                "templateId" => $this->templateId
            ];

            $result = EasyPayContractInfo::create($data);
        }

        //成功
        if ($result) {
            $this->status = 1;
            $this->message = '添加合同成功';
            return $this->format();
        } else {
            $this->status = -1;
            $this->message = "添加合同信息失败";
            return $this->format();
        }
    }


    /**
     * 电子协议 开户
     * @param Request $request
     */
    public function easypayElectronic(Request $request)
    {
        try {

            $token = $this->parseToken();
            $storeId = $request->get('storeId');

            if (!$storeId) {
                return json_encode([
                    'status' => -1,
                    'message' => '门店ID为空,请选择门店'
                ]);
            }

            //个人开户3001  Personal account
            $txCode = $request->get('txCode');
            $easyPayStoresImages = EasypayStoresImages::where('store_id', $storeId)->select('new_config_id')->first();
            if ($txCode == 3001) {

                $personName = $request->get('personName');
                $identTypeCode = $request->get('identTypeCode', '0');
                $identNo = $request->get('identNo');
                $mobilePhone = $request->get('mobilePhone');
                $address = $request->get('address', '');
                $authenticationMode = $request->get('authenticationMode', '公安');

                $person = [
                    "personName" => $personName,
                    "identTypeCode" => $identTypeCode,
                    "identNo" => $identNo,
                    "mobilePhone" => $mobilePhone,
                    "address" => $address,
                    "authenticationMode" => $authenticationMode
                ];

                $contractBody = [
                    'head' => $this->getHeadInfo(),
                    'person' => $person,
                    'notSendPwd' => "1"
                ];

                $data = $this->publicParam($easyPayStoresImages->new_config_id);

                $data['txCode'] = strval($txCode); // 个人开户 3001; 企业开户 3002; 商户开户 3003;
                $data['contractBody'] = json_encode($contractBody, JSON_UNESCAPED_UNICODE);
                $easyPay_key = '';
                if ($easyPayStoresImages->new_config_id == '1234') {
                    $easyPay_key = $this->easyPay_key;
                }
                if ($easyPayStoresImages->new_config_id == '1001') {
                    $easyPay_key = $this->easyPay_key_two;
                }
                $sign = $this->getSign($data, $easyPay_key);

                $data['sign'] = $sign;

                Log::info('====个人开户======请求参数 params:' . json_encode($data, JSON_UNESCAPED_UNICODE));

                $result = $this->postUrl($data, $this->url);

                Log::info('====个人开户======Response params:' . $result);

                $res_arr = json_decode($result, true);

                //成功
                if ($res_arr['resultCode'] == '00') {

                    $contractBody = json_decode($res_arr['contractBody'], true);

                    //用户ID
                    $userId = $contractBody['person']['userId'];

                    $electronic = EasyPayMpsInfo::where('user_id', $userId)->first();
                    if (!$electronic) {

                        $insert_data = [
                            "store_id" => $storeId,
                            "tx_code" => $txCode,
                            "person_name" => $personName,
                            "ident_type_code" => $identTypeCode,
                            "ident_no" => $identNo,
                            "mobile_phone" => $mobilePhone,
                            "address" => $address,
                            "authentication_mode" => $authenticationMode,
                            "user_id" => $userId,
                            "created_step" => 2 //2、开户成功
                        ];

                        $result = EasyPayMpsInfo::create($insert_data);

                        if ($result) {
                            $this->status = 1;
                            $this->message = '开户成功';
                            return $this->format($userId);
                        }

                    } else {

                        $update_data = [
                            "store_id" => $storeId,
                            "tx_code" => $txCode,
                            "person_name" => $personName,
                            "ident_type_code" => $identTypeCode,
                            "ident_no" => $identNo,
                            "mobile_phone" => $mobilePhone,
                            "address" => $address,
                            "authentication_mode" => $authenticationMode,
                            "user_id" => $userId,
                            "created_step" => 2 //2、开户成功
                        ];

                        $electronic->update($update_data);
                        $electronic->save();

                        $this->status = 1;
                        $this->message = '已开户成功';
                        return $this->format($userId);
                    }
                } else {
                    return json_encode([
                        'status' => -1,
                        'message' => $res_arr['resultMsg'],
                    ]);
                }
            } else if ($txCode == 3002) { //企业开户

                $enterpriseName = $request->get('enterpriseName');
                $identTypeCode = $request->get('identTypeCode');
                $identNo = $request->get('identNo');
                $mobilePhone = $request->get('mobilePhone');
                $landlinePhone = $request->get('landlinePhone');
                $authenticationMode = $request->get('authenticationMode', '公安');
                $transactorName = $request->get('transactorName');
                $transactor_ident_type_code = $request->get('transactorIdentTypeCode');
                $transactor_ident_no = $request->get('transactorIdentNo');
                $address = $request->get('address');

                $enterprise = [
                    "enterpriseName" => $enterpriseName, //企业名称
                    "identTypeCode" => $identTypeCode, //企业营业执照
                    "identNo" => $identNo,
                    "mobilePhone" => $mobilePhone,
                    "landlinePhone" => $landlinePhone, //企业联系电话
                    "authenticationMode" => $authenticationMode
                ];
                $enterpriseTransactor = [
                    "transactorName" => $transactorName,
                    "identTypeCode" => $transactor_ident_type_code,
                    "identNo" => $transactor_ident_no,
                    "address" => $address
                ];

                $contractBody = [
                    'head' => $this->getHeadInfo(),
                    'enterprise' => $enterprise,
                    'enterpriseTransactor' => $enterpriseTransactor,
                    'notSendPwd' => 1 //0：发送；1：不发送；建议取值1
                ];

                $data = $this->publicParam($easyPayStoresImages->new_config_id);

                $data['txCode'] = strval($txCode); // 个人开户 3001; 企业开户 3002
                $data['contractBody'] = json_encode($contractBody, JSON_UNESCAPED_UNICODE);

                $easyPay_key = '';
                if ($easyPayStoresImages->new_config_id == '1234') {
                    $easyPay_key = $this->easyPay_key;
                }
                if ($easyPayStoresImages->new_config_id == '1001') {
                    $easyPay_key = $this->easyPay_key_two;
                }
                $sign = $this->getSign($data, $easyPay_key);

                $data['sign'] = $sign;

                Log::info('=====企业开户=====请求参数 params:' . json_encode($data, JSON_UNESCAPED_UNICODE));

                $result = $this->postUrl($data, $this->url);

                Log::info('=====企业开户=======Response params:' . $result);

                $res_arr = json_decode($result, true);

                //成功
                if ($res_arr['resultCode'] == '00') {

                    $contractBody = json_decode($res_arr['contractBody'], true);

                    //用户ID
                    $userId = $contractBody['enterprise']['userId'];

                    $electronic = EasyPayMpsInfo::where('user_id', $userId)->first();
                    if (!$electronic) {

                        $insert_data = [
                            "store_id" => $storeId,
                            "tx_code" => $txCode,
                            "enterprise_name" => $enterpriseName,
                            "ident_type_code" => $identTypeCode,
                            "ident_no" => $identNo,
                            "mobile_phone" => $mobilePhone,
                            "landline_phone" => $landlinePhone,
                            "authentication_mode" => $authenticationMode,
                            "transactor_name" => $transactorName,
                            "transactor_ident_type_code" => $transactor_ident_type_code,
                            "transactor_ident_no" => $transactor_ident_no,
                            "address" => $address,
                            "user_id" => $userId,
                            "created_step" => 2 //2、开户成功
                        ];

                        $result = EasyPayMpsInfo::create($insert_data);

                        if ($result) {
                            $this->status = 1;
                            $this->message = '开户成功';
                            return $this->format($userId);
                        }

                    } else {

                        $update_data = [
                            "store_id" => $storeId,
                            "tx_code" => $txCode,
                            "enterprise_name" => $enterpriseName,
                            "ident_type_code" => $identTypeCode,
                            "ident_no" => $identNo,
                            "mobile_phone" => $mobilePhone,
                            "landline_phone" => $landlinePhone,
                            "authentication_mode" => $authenticationMode,
                            "transactor_name" => $transactorName,
                            "transactor_ident_type_code" => $transactor_ident_type_code,
                            "transactor_ident_no" => $transactor_ident_no,
                            "address" => $address,
                            "user_id" => $userId,
                            "created_step" => 2 //2、开户成功
                        ];

                        $electronic->update($update_data);
                        $electronic->save();

                        $this->status = 1;
                        $this->message = '已开户成功';
                        return $this->format($userId);
                    }

                } else {

                    $this->status = -1;
                    $this->message = $res_arr['resultMsg'];
                    return $this->format();

                }
            }

        } catch (\Exception $ex) {
            Log::info('易生电子协议--error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage() . ' | ' . $ex->getLine();
            return $this->format();
        }

    }


    /**
     * 电子协议 发送验证码
     * @param Request $request
     */
    public function sendSms(Request $request)
    {

        $token = $this->parseToken();
        $storeId = $request->get('storeId');
        if (!$storeId) {
            $this->status = -1;
            $this->message = '门店ID为空,请选择门店';
            return $this->format();
        }

        $electronic = EasyPayMpsInfo::where('store_id', $storeId)->first();
        // if (!$electronic) {
        //     $this->status = -1;
        //     $this->message =  '尚未完成开户操作';
        //     return $this->format();
        // }

        //3101
        $txCode = $request->get('txCode', '3101');

        $proxySign = [
            'userId' => $electronic->user_id,
            'randomCode' => strval(rand(10, 99))
        ];

        $contractBody = [
            'head' => $this->getHeadInfo(),
            'proxySign' => $proxySign
        ];
        $easyPayStoresImages = EasypayStoresImages::where('store_id', $storeId)->select('new_config_id')->first();
        $data = $this->publicParam($easyPayStoresImages->new_config_id);

        $data['txCode'] = strval($txCode);
        $data['contractBody'] = json_encode($contractBody, JSON_UNESCAPED_UNICODE);

        $easyPay_key = '';
        if ($easyPayStoresImages->new_config_id == '1234') {
            $easyPay_key = $this->easyPay_key;
        }
        if ($easyPayStoresImages->new_config_id == '1001') {
            $easyPay_key = $this->easyPay_key_two;
        }
        $sign = $this->getSign($data, $easyPay_key);

        $data['sign'] = $sign;

        Log::info('=====发送验证码=====请求参数 params:' . json_encode($data, JSON_UNESCAPED_UNICODE));

        $result = $this->postUrl($data, $this->url);

        Log::info('==========发送验证码========Response params:' . $result);

        $res_arr = json_decode($result, true);

        //成功
        if ($res_arr['resultCode'] == '00') {

            $contractBody = json_decode($res_arr['contractBody'], true);
            //用户ID
            $userId = $contractBody['proxySign']['userId'];

            EasyPayMpsInfo::where('user_id', $userId)
                ->where('store_id', $storeId)
                ->update([
                    "created_step" => 3,//3、短信发送成功
                ]);

            $this->status = 1;
            $this->message = $contractBody['head']['retMessage'];
            return $this->format();

        } else {

            $this->status = 1;
            $this->message = $res_arr['resultMsg'];
            return $this->format();

        }
    }

    //创建合同 3201
    public function createContract(Request $request)
    {

        $token = $this->parseToken();
        $storeId = $request->get('storeId');
        $txCode = $request->get('txCode', '3201');

        if (!$storeId) {
            $this->status = -1;
            $this->message = '门店ID为空,请选择门店';
            return $this->format();
        }

        $electronic = EasyPayMpsInfo::where('store_id', $storeId)->first();
        // if (!$electronic) {
        //     $this->status = -1;
        //     $this->message = '尚未完成开户操作';
        //     return $this->format();

        // }

        $userId = $electronic->user_id;

        $contractInfo = EasyPayContractInfo::where('store_id', $storeId)->first();

        if (!$contractInfo) {
            $this->status = -1;
            $this->message = '尚未创建合同';
            return $this->format();

        }

        $note = [
            'checkCode' => $request->get('checkCode'), //验证码
            'randomCode' => strval(rand(10, 99))
        ];

        $projectCode = 'QR_' . date('YmdHis', time()) . mt_rand(1000, 9999);    //项目编号 自定义

        $signInfos = [
            'userId' => $userId,
            'authorizationTime' => date('YmdHis', time()),// 授权时间：yyyyMMddHHmmss
            'location' => '154.8.143.104',
            'signLocation' => 's1;s2', // 签名域的标签值
            'projectCode' => $projectCode,
            'isProxySign' => '0', //是否代签  0：不代签；1：代签；默认为0。 取1时，可不用再调用 “签署合同”
        ];

        $textValueInfo = [
            "merName" => $contractInfo->merName,  // 商户名/注册名
            "merLegal" => $contractInfo->merLegal, //法人
            "merAddr" => $contractInfo->merAddr,  //商户地址/注册地址
            "contractDateYear" => $contractInfo->contractDateYear,    //合同日期 YYYY
            "contractDateMonth" => $contractInfo->contractDateMonth,
            "contractDateDay" => $contractInfo->contractDateDay,
            "bizName" => $contractInfo->bizName,  //经营名称
            "businLic" => $contractInfo->businLic,  //统一 社会信用代码
            "bizAddr" => $contractInfo->bizAddr,  //经营地址
            "legalCode" => $contractInfo->legalCode, //法人身份证号
            "legalPhone" => $contractInfo->legalPhone,  //法人电话
            "linkMan" => $contractInfo->linkMan,   //联系人
            "linkmanPhone" => $contractInfo->linkmanPhone, //联系人电话
            "accName" => $contractInfo->accName, //账号名称
            "bankName" => $contractInfo->bankName, //开户行
            "account" => $contractInfo->account,  //账号
            "chargeTypeDebit" => $contractInfo->chargeTypeDebit, //卡支付借记卡
            "chargeTypeDebitMax" => $contractInfo->chargeTypeDebitMax, //卡支付借记卡封顶值
            "chargeTypeCredit" => $contractInfo->chargeTypeCredit, //卡支付贷记卡
            "chargeTypeUnionpayDebit" => $contractInfo->chargeTypeUnionpayDebit, //银联二维码借记卡
            "chargeTypeUnionpayDebitMax" => $contractInfo->chargeTypeUnionpayDebitMax, //银联二维码借记卡封顶值
            "chargeTypeUnionpayCredit" => $contractInfo->chargeTypeUnionpayCredit, //银联二维码贷记卡
            "chargeTypeUnionpayDisDebit" => $contractInfo->chargeTypeUnionpayDisDebit, //银联营销借记卡
            "chargeTypeUnionpayDisDebitMax" => $contractInfo->chargeTypeUnionpayDisDebitMax, //银联营销借记卡封顶值
            "chargeTypeUnionpayDisCredit" => $contractInfo->chargeTypeUnionpayDisCredit, //银联营销贷记卡
            "chargeTypeWxpay" => $contractInfo->chargeTypeWxpay, //微信
            "chargeTypeAlipay" => $contractInfo->chargeTypeAlipay, //支付宝
            "d1SettlementRatio" => $contractInfo->d1SettlementRatio, //D1结算按比例
            "d1SettlementFee" => $contractInfo->d1SettlementFee, //D1结算按笔
            "d0SettlementRatio" => $contractInfo->d0SettlementRatio, //D0结算按比例
            "manager" => $contractInfo->manager,  //客户经理
            "managerPhone" => $contractInfo->managerPhone, //客户经理电话
            "contractDate" => $contractInfo->contractDate, //合同日期 YYYY年MM月DD日
            "checkSameCard" => $contractInfo->checkSameCard = '1' ? '√' : '', //同卡支付
            "checkSettleT1" => $contractInfo->checkSettleT1 = '1' ? '√' : '', //T1结算
            "checkSettleD1" => $contractInfo->checkSettleD1 = '1' ? '√' : '', //D1结算
            "checkSettleD1Ratio" => $contractInfo->checkSettleD1Ratio = '1' ? '√' : '',//D1结算按比例
            "checkSettleD1Fee" => $contractInfo->checkSettleD1Fee = '1' ? '√' : '', //D1结算按笔
            "checkSettleD0" => $contractInfo->checkSettleD0 = '1' ? '√' : '', //D0结算
        ];

        $createContract = [
            'templateId' => $this->templateId,
            'isSign' => "1",  //签署类型 易生平台自身是否要签署该协议。0：抄送；1：签署；2：暂不签署；默认为0
            'signInfos' => array($signInfos),
            'textValueInfo' => $textValueInfo,
            'signLocation' => 's0'  // 易生签名域的标签值

        ];

        $contractBody = [
            'head' => $this->getHeadInfo(),
            'note' => $note,
            'createContract' => $createContract
        ];
        $easyPayStoresImages = EasypayStoresImages::where('store_id', $storeId)->select('new_config_id')->first();
        $data = $this->publicParam($easyPayStoresImages->new_config_id);

        $data['txCode'] = strval($txCode); // 个人开户 3001; 企业开户 3002; 商户开户 3003;
        $data['contractBody'] = json_encode($contractBody, JSON_UNESCAPED_UNICODE);

        $easyPay_key = '';
        if ($easyPayStoresImages->new_config_id == '1234') {
            $easyPay_key = $this->easyPay_key;
        }
        if ($easyPayStoresImages->new_config_id == '1001') {
            $easyPay_key = $this->easyPay_key_two;
        }
        $sign = $this->getSign($data, $easyPay_key);

        $data['sign'] = $sign;

        Log::info('====创建合同======请求参数 params:' . json_encode($data, JSON_UNESCAPED_UNICODE));

        $result = $this->postUrl($data, $this->url);

        Log::info('====创建合同=======Response params:' . $result);

        $res_arr = json_decode($result, true);

        //成功
        if ($res_arr['resultCode'] == '00') {

            $contractBody = json_decode($res_arr['contractBody'], true);

            //合同编号
            $contractNo = $contractBody['contract']['contractNo'];
            $contractState = $contractBody['contract']['contractState'];

            //开启事务
            try {
                DB::beginTransaction();

                EasyPayContractInfo::where('store_id', $storeId)
                    ->update([
                        'contractNo' => $contractNo,
                        'contractState' => $contractState
                    ]);

                EasyPayMpsInfo::where('user_id', $userId)
                    ->where('store_id', $storeId)
                    ->update([
                        "created_step" => 4,//4、创建合同成功
                        'project_code' => $projectCode,
                        'contract_no' => $contractNo,
                        'contract_state' => $contractState
                    ]);


                $this->status = 1;
                $this->message = '创建合同成功';
                return $this->format($contractBody['contract']);

                DB::commit();
            } catch (\Exception $e) {
                Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
                DB::rollBack();
                return json_encode([
                    'status' => -1,
                    'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
                ]);
            }

        } else {

            $this->status = -1;
            $this->message = $res_arr['resultMsg'];
            return $this->format();

        }
    }

    //签署合同 3206
    public function signContract(Request $request)
    {

        $token = $this->parseToken();
        $storeId = $request->get('storeId');
        $txCode = $request->get('txCode', '3206');

        if (!$storeId) {

            $this->status = -1;
            $this->message = '门店ID为空,请选择门店';
            return $this->format();
        }

        $electronic = EasyPayMpsInfo::where('store_id', $storeId)->first();
        // if (!$electronic) {
        //     $this->status = -1;
        //     $this->message = '尚未完成开户操作';
        //     return $this->format();
        // }

        $userId = $electronic->user_id;
        $contractNo = $electronic->contract_no;
        $projectCode = $electronic->project_code;

        $signInfo = [
            'userId' => $userId,
            'authorizationTime' => date('YmdHis', time()),// 授权时间：yyyyMMddHHmmss
            'location' => '154.8.143.104',
            'signLocation' => 's1;s2', // 签名域的标签值
            'projectCode' => $projectCode, //项目编号
        ];

        $signContract = [
            'contractNo' => $contractNo,
            'signInfo' => $signInfo,
        ];

        $contractBody = [
            'head' => $this->getHeadInfo(),
            'signContract' => $signContract
        ];
        $easyPayStoresImages = EasypayStoresImages::where('store_id', $storeId)->select('new_config_id')->first();
        $data = $this->publicParam($easyPayStoresImages->new_config_id);

        $data['txCode'] = strval($txCode);
        $data['contractBody'] = json_encode($contractBody, JSON_UNESCAPED_UNICODE);

        $easyPay_key = '';
        if ($easyPayStoresImages->new_config_id == '1234') {
            $easyPay_key = $this->easyPay_key;
        }
        if ($easyPayStoresImages->new_config_id == '1001') {
            $easyPay_key = $this->easyPay_key_two;
        }
        $sign = $this->getSign($data, $easyPay_key);

        $data['sign'] = $sign;

        Log::info('====签署合同======请求参数 Params:' . json_encode($data, JSON_UNESCAPED_UNICODE));

        $result = $this->postUrl($data, $this->url);

        Log::info('====签署合同======== Response Params:' . $result);

        $res_arr = json_decode($result, true);

        //成功
        if ($res_arr['resultCode'] == '00') {

            $contractBody = json_decode($res_arr['contractBody'], true);

            //合同信息
            $signContract = $contractBody['signContract'];

            EasyPayMpsInfo::where('user_id', $userId)
                ->where('store_id', $storeId)
                ->where('contract_no', $contractNo)
                ->update([
                    'contract_state' => '1', //状态 0未完成；1已完成； 2已拒绝； 3正在签署； 4锁定待签 ；9已过期
                ]);

            EasyPayContractInfo::where('store_id', $storeId)
                ->where('contractNo', $contractNo)
                ->update([
                    'contractState' => '1', //状态 0未完成；1已完成； 2已拒绝； 3正在签署； 4锁定待签 ；9已过期
                ]);

            $this->status = 1;
            $this->message = 'success';
            return $this->format($signContract);

        } else {

            $this->status = -1;
            $this->message = $res_arr['resultMsg'];
            return $this->format();
        }

    }

    //查询合同 3210
    public function getContract(Request $request)
    {

        $token = $this->parseToken();
        $storeId = $request->get('storeId');
        $txCode = $request->get('txCode', '3210');

        $electronic = EasyPayMpsInfo::where('store_id', $storeId)->first();

        $contractNo = '';
        $signatoryName = '';
        $contractState = '';
        if ($electronic) {
            $contractNo = $electronic->contract_no;
            $contractState = $electronic->contract_state;
        }

        $contractInfo = EasyPayContractInfo::where('store_id', $storeId)->first();

        if ($contractInfo) {
            $signatoryName = $contractInfo->merName;
        }
        $data = [
            'contractNo' => $contractNo, //合同编号
            'signatoryName' => $signatoryName,
            'contractState' => $contractState
        ];


        $this->status = 1;
        $this->message = 'OK';
        return $this->format($data);


//        $contractNo = [
//            'contractNo' => $electronic->contract_no//合同编号
//        ];
//
//        $contractBody = [
//            'head' => $this->getHeadInfo(),
//            'contract'  => $contractNo
//        ];
//
//        $data = $this->publicParam();
//
//        $data['txCode'] = strval($txCode);
//        $data['contractBody'] = json_encode($contractBody,JSON_UNESCAPED_UNICODE);
//
//        $sign = $this->getSign($data, $this->easyPay_key);
//
//        $data['sign'] = $sign;
//
//        Log::info('===查询合同====请求参数 params:'.json_encode($data,JSON_UNESCAPED_UNICODE));
//
//        $result = $this->postUrl($data, $this->url);
//
//        Log::info('===查询合同===== Response Params:' . $result );
//
//        $res_arr = json_decode($result, true);
//
//        //成功
//        if ($res_arr['resultCode'] == '00' ) {
//
//            $contractBody = json_decode($res_arr['contractBody'],true);
//            //合同信息
//            $contractInfo = $contractBody['contract'];
//
//            Log::info('==========合同查询==contractInfo:' . json_encode($contractInfo));
//
//            $this->status = 1;
//            $this->message = 'success';
//            return $this->format($contractInfo);
//
//        }else{
//
//            return json_encode([
//                'status' => -1,
//                'message' => $res_arr['resultMsg']
//            ]);
//        }
    }

    //下载合同
    public function downloadContract(Request $request)
    {

        $token = $this->parseToken();
        $storeId = $request->get('storeId');

        if (!$storeId) {
            return json_encode([
                'status' => -1,
                'message' => '门店ID为空,请选择门店'
            ]);
        }

        $electronic = EasyPayMpsInfo::where('store_id', $storeId)->first();
        // if (!$electronic) {
        //     return json_encode([
        //         'status' => -1,
        //         'message' => '尚未完成开户操作'
        //     ]);
        // }

        $contractNo = $electronic->contract_no;//合同编号

        $easyPayStoresImages = EasypayStoresImages::where('store_id', $storeId)->select('new_config_id')->first();
        $data = $this->publicParam($easyPayStoresImages->new_config_id);
        $data['contractNo'] = $contractNo;

        $easyPay_key = '';
        if ($easyPayStoresImages->new_config_id == '1234') {
            $easyPay_key = $this->easyPay_key;
        }
        if ($easyPayStoresImages->new_config_id == '1001') {
            $easyPay_key = $this->easyPay_key_two;
        }
        $sign = $this->getSign($data, $easyPay_key);

        $data['sign'] = $sign;

        $result = $this->postUrl($data, $this->download_url);
//        Log::info('==========合同下载========');
//        Log::info($result);
        if ($result) {
            $res_arr = json_decode($result, true);
            //成功
            if ($res_arr['resultCode'] == '00') {
                //合同信息
                $pdfFileData = $res_arr['pdfFileData'];
//                $img = base64_decode($pdfFileData);
//                $a = file_put_contents($contractNo.'.pdf', $img);//返回的是字节数  //放置路径的文件名后缀可跟需求改变而改变
//                print_r($a);

                $this->status = 1;
                $this->message = $contractNo;
                return $this->format($pdfFileData);

            } else {

                $this->status = -1;
                $this->message = $res_arr['resultMsg'];
                return $this->format();
            }
        } else {

            $this->status = -1;
            $this->message = 'Error';
            return $this->format();
        }
    }

    //删除
    public function deleteContract(Request $request)
    {

        try {

            $token = $this->parseToken();
            $contractNo = $request->get('contractNo');

            EasyPayMpsInfo::where('contract_no', $contractNo)->delete();

            EasyPayContractInfo::where('contractNo', $contractNo)->delete();

            $this->status = 1;
            $this->message = '删除成功';
            return $this->format();

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine()
            ]);
        }

    }


}
