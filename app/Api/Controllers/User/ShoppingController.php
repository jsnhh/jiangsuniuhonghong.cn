<?php
/**
 * Created by PhpStorm.
 * User: daimingkang
 * Date: 2018/12/21
 * Time: 2:34 PM
 */

namespace App\Api\Controllers\User;


use App\Api\Controllers\BaseController;
use App\Api\Controllers\Config\EasyPayConfigController;

use App\Api\Controllers\Merchant\TemplateMessageController;
use App\Models\AgentLevels;
use App\Models\AgentLevelsInfos;
use App\Models\EasypayStore;
use App\Models\EasypayStoresImages;
use App\Models\QrCodeUserRelation;
use App\Models\QrListInfo;
use App\Models\ShoppingAddress;
use App\Models\ShoppingCategory;
use App\Models\ShoppingGoods;
use App\Models\ShoppingOrder;
use App\Models\Store;
use App\Models\User;
use App\Models\UserShoppingReward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ShoppingController extends BaseController
{
    private $notify_url = '/api/merchant/win_pay_notify';
    private $payUrl = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
    public $jsapiUrl = 'https://platform.eycard.cn:8443/standard/jsapi';//jsapi支付地址 生产线 易生
    public $store_id = '202308220658053671';

    //收货地址列表查询
    public function address_list(Request $request)
    {

        try {
            $user_id = $request->get('user_id', '');
            $name = $request->get('name', '');
            $where = [];
            if ($user_id) {
                $where[] = ['user_id', '=', $user_id];
            }
            if ($name) {
                $where[] = ['name', 'like', '%' . $name . '%'];
            }
            $obj = DB::table('shopping_address');
            $obj = $obj->where($where)
                ->orderBy('is_default', 'desc');
            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);


        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //新增收货地址
    public function add_address(Request $request)
    {

        try {
            $user_id = $request->get('user_id', '');
            $user_name = $request->get('user_name', '');
            $name = $request->get('name', '');
            $province = $request->get('province', '');
            $city = $request->get('city', '');
            $county = $request->get('county', '');
            $address_detail = $request->get('address_detail', '');
            $tel = $request->get('tel', '');
            $e_mail = $request->get('e_mail', '');
            $is_default = $request->get('is_default', '');

            $id = $request->get('id', '');
            $insert_data = [
                'name' => $name,
                'user_id' => $user_id,
                'user_name' => $user_name,
                'province' => $province,
                'city' => $city,
                'county' => $county,
                'address_detail' => $address_detail,
                'tel' => $tel,
                'e_mail' => $e_mail,
                'is_default' => $is_default,

            ];
            if ($is_default == '1') {
                ShoppingAddress::where("user_id", $user_id)->update([
                    'is_default' => 0,
                ]);
            }

            if (!empty($id)) {
                ShoppingAddress::where('id', $id)->update($insert_data);
                return json_encode([
                    'status' => 1,
                    'message' => '修改地址成功'
                ]);
            } else {
                ShoppingAddress::create($insert_data);
                return json_encode([
                    'status' => 1,
                    'message' => '新增地址成功'
                ]);
            }

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //删除收货地址
    public function del_address(Request $request)
    {

        try {
            $id = $request->get('id', '');
            ShoppingAddress::where('id', $id)->delete();
            return json_encode([
                'status' => 1,
                'message' => '删除成功'
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //商品类别列表查询
    public function shopping_category_list(Request $request)
    {

        try {

            $name = $request->get('name', '');
            $where = [];
            if ($name) {
                $where[] = ['name', 'like', '%' . $name . '%'];
            }
            $obj = DB::table('shopping_category');
            $obj = $obj->where($where)
                ->orderBy('updated_at', 'desc');
            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //商品类别select
    public function shopping_category_select(Request $request)
    {

        try {
            $shoppingCategory = ShoppingCategory::get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($shoppingCategory);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //新增商品类别
    public function add_shopping_category(Request $request)
    {

        try {
            $name = $request->get('name', '');
            $describe = $request->get('describe', '');
            $id = $request->get('id', '');
            $insert_data = [
                'name' => $name,
                'describe' => $describe
            ];

            if (!empty($id)) {
                $where = [];
                $where[] = ['name', '=', $name];
                $where[] = ['id', '!=', $id];
                $shoppingCategory = ShoppingCategory::where($where)->get();
                if ($shoppingCategory->isEmpty()) {
                    ShoppingCategory::where('id', $id)->update($insert_data);
                    return json_encode([
                        'status' => 1,
                        'message' => '修改商品分类成功'
                    ]);

                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '商品分类名称已存在'
                    ]);
                }

            } else {
                $shoppingCategory = ShoppingCategory::where("name", $name)->get();
                //Log::info($shoppingCategory->isEmpty());
                if ($shoppingCategory->isEmpty()) {
                    ShoppingCategory::create($insert_data);
                    return json_encode([
                        'status' => 1,
                        'message' => '新增商品分类成功'
                    ]);
                } else {
                    return json_encode([
                        'status' => 2,
                        'message' => '商品分类名称已存在'
                    ]);
                }


            }

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //删除商品分类
    public function del_shopping_category(Request $request)
    {

        try {
            $id = $request->get('id', '');
            $shoppingGoods = ShoppingGoods::where('category_id', $id)->get();
            if ($shoppingGoods->isEmpty()) {
                ShoppingCategory::where('id', $id)->delete();
                return json_encode([
                    'status' => 1,
                    'message' => '删除成功'
                ]);
            }
            return json_encode([
                'status' => 2,
                'message' => '商品分类中存在商品，不允许删除'
            ]);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //商品列表查询
    public function shopping_goods_list(Request $request)
    {

        try {
            $name = $request->get('name', '');
            $category_id = $request->get('category_id', '');
            $is_on_sale = $request->get('is_on_sale', '');
            $where = [];
            if ($name) {
                $where[] = ['shopping_goods.goods_name', 'like', '%' . $name . '%'];
            }
            if ($category_id) {
                $where[] = ['shopping_goods.category_id', '=', $category_id];
            }
            if ($is_on_sale) {
                $where[] = ['shopping_goods.is_on_sale', '=', $is_on_sale];
            }
            $obj = DB::table('shopping_goods');
            $obj = $obj->where($where)
                ->leftJoin('shopping_category', 'shopping_category.id', '=', 'shopping_goods.category_id')
                ->select('shopping_goods.*', 'shopping_category.name')
                ->orderBy('shopping_goods.quantity', 'desc');
            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //新增商品
    public function add_goods(Request $request)
    {

        try {
            $goods_sn = $request->get('goods_sn', '');
            $goods_name = $request->get('goods_name', '');
            $category_id = $request->get('category_id', '');
            $brief = $request->get('brief', '');
            $pic_url = $request->get('pic_url', '');
            $unit = $request->get('unit', '');
            $original_price = $request->get('original_price', '');
            $present_price = $request->get('present_price', '');
            $stock = $request->get('stock', '');
            $quantity = $request->get('quantity', '');
            $is_on_sale = $request->get('is_on_sale', '');
            $id = $request->get('id', '');
            $is_integral = $request->get('is_integral', '');
            $integral_num = $request->get('integral_num', '');
            $integral_consume = $request->get('integral_consume', '');
            $insert_data = [
                'goods_sn' => $goods_sn,
                'goods_name' => $goods_name,
                'category_id' => $category_id,
                'brief' => $brief,
                'pic_url' => $pic_url,
                'unit' => $unit,
                'original_price' => $original_price,
                'present_price' => $present_price,
                'stock' => $stock,
                'quantity' => $quantity,
                'is_on_sale' => $is_on_sale,
                'is_integral' => $is_integral,
                'integral_num' => $integral_num,
                'integral_consume' => $integral_consume
            ];

            if (!empty($id)) {
                ShoppingGoods::where('id', $id)->update($insert_data);
                return json_encode([
                    'status' => 1,
                    'message' => '修改商品成功'
                ]);
            } else {
                ShoppingGoods::create($insert_data);
                return json_encode([
                    'status' => 1,
                    'message' => '新增商品成功'
                ]);
            }

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //删除商品
    public function del_goods(Request $request)
    {

        try {
            $id = $request->get('id', '');
            ShoppingGoods::where('id', $id)->delete();
            return json_encode([
                'status' => 1,
                'message' => '删除成功'
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //获取商品详情
    public function get_goods_info(Request $request)
    {

        try {
            $id = $request->get('id', '');
            $obj = DB::table('shopping_goods');
            $obj = $obj->where('shopping_goods.id', $id)
                ->leftJoin('shopping_category', 'shopping_category.id', '=', 'shopping_goods.category_id')
                ->select('shopping_goods.*', 'shopping_category.name')
                ->first();
            return json_encode([
                'status' => 1,
                'message' => '获取成功',
                'data' => $obj
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //订单列表查询
    public function shopping_order_list(Request $request)
    {

        try {
            $goods_name = $request->get('goods_name', '');
            $user_id = $request->get('user_id', '');
            $user_id_app = $request->get('user_id_app', '');
            $order_id = $request->get('order_id', '');
            $order_status = $request->get('order_status', '');
            $recipient = $request->get('recipient', '');
            $user_name = $request->get('user_name', '');
            $goods_id = $request->get('goods_id', '');

            $ids = $this->getSubIds($user_id_app);
            $where = [];
            if ($goods_name) {
                $where[] = ['shopping_goods.goods_name', 'like', '%' . $goods_name . '%'];
            }
            if ($user_id) {
                $where[] = ['shopping_order.user_id', '=', $user_id];
            }
            if ($order_id) {
                $where[] = ['shopping_order.order_id', '=', $order_id];
            }
            if ($order_status) {
                $where[] = ['shopping_order.order_status', '=', $order_status];
            }
            if ($recipient) {
                $where[] = ['shopping_order.recipient', '=', $recipient];
            }
            if ($user_name) {
                $where[] = ['users.name', 'like', '%' . $user_name . '%'];
            }
            if ($user_name) {
                $where[] = ['users.name', 'like', '%' . $user_name . '%'];
            }
            if ($goods_id) {
                $where[] = ['shopping_order.goods_id', '=', $goods_id];
            }

            $obj = DB::table('shopping_order');
            if ($user_id_app != 1) {
                $obj = $obj->where($where)
                    ->whereIn('shopping_order.user_id', $ids)
                    ->whereIn('shopping_order.order_status', [1, 4, 5, 9])
                    ->leftJoin('shopping_goods', 'shopping_order.goods_id', '=', 'shopping_goods.id')
                    ->leftJoin('users', 'shopping_order.user_id', '=', 'users.id')
                    ->leftJoin('shopping_category', 'shopping_goods.category_id', '=', 'shopping_category.id')
                    ->select('shopping_order.*', 'shopping_goods.goods_name', 'users.name as user_name', 'users.pid_name as p_name', 'shopping_category.name as type_name',
                        'shopping_goods.pic_url')
                    ->orderBy('shopping_order.updated_at', 'desc');
            } else {
                $obj = $obj->where($where)
                    ->whereIn('shopping_order.order_status', [1, 4, 5, 9])
                    ->leftJoin('shopping_goods', 'shopping_order.goods_id', '=', 'shopping_goods.id')
                    ->leftJoin('users', 'shopping_order.user_id', '=', 'users.id')
                    ->leftJoin('shopping_category', 'shopping_goods.category_id', '=', 'shopping_category.id')
                    ->select('shopping_order.*', 'shopping_goods.goods_name', 'users.name as user_name', 'users.pid_name as p_name', 'shopping_category.name as type_name',
                        'shopping_goods.pic_url')
                    ->orderBy('shopping_order.updated_at', 'desc');
            }

            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //更新订单状态
    public function edit_order_status(Request $request)
    {

        try {
            $id = $request->get('id', '');
            $order_status = $request->get('order_status', '');
            $message = '';
            if ($order_status == '9') {
                $message = '收货成功';
            }
            if ($order_status == '8') {
                $message = '取消成功';
            }
            $update_data = [
                'order_status' => $order_status
            ];
            ShoppingOrder::where('id', $id)->update($update_data);
            return json_encode([
                'status' => 1,
                'message' => $message
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //修改订单
    public function edit_order(Request $request)
    {
        try {
            $id = $request->get('id', '');
            $recipient = $request->get('recipient', '');
            $address = $request->get('address', '');
            $phone = $request->get('phone', '');
            $remark = $request->get('remark', '');
            $check_data = [
                'id' => '订单id',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $update_data = [
                'recipient' => $recipient,
                'address' => $address,
                'phone' => $phone,
                'remark' => $remark,
            ];
            ShoppingOrder::where('id', $id)->update($update_data);
            return json_encode([
                'status' => 1,
                'message' => '修改成功'
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //删除订单
    public function del_order(Request $request)
    {

        try {
            $id = $request->get('id', '');
            ShoppingOrder::where('id', $id)->delete();
            return json_encode([
                'status' => 1,
                'message' => '删除成功'
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //发货
    public function deliver_goods(Request $request)
    {

        try {
            $id = $request->get('id', '');
            $tracking_code = $request->get('tracking_code', '');
            $express_name = $request->get('express_name', '');
            $start_code = $request->get('start_code', '');
            $end_code = $request->get('end_code', '');

            $qrListInfo = QrListInfo::where('status', '0')->where('is_buy', '0')
                ->whereBetween('code_num', [$start_code, $end_code])->get();

            $code_num = count($qrListInfo);

            $shoppingOrder = ShoppingOrder::where('id', $id)->first();
            if ($code_num != $shoppingOrder->quantity) {
                return json_encode([
                    'status' => -1,
                    'message' => '发货失败,发货数量和购买数量不一致，请查看码牌状态或者号段是否正确'
                ]);
            }
            $agentLevelsInfo = AgentLevelsInfos::where('user_id', $shoppingOrder->user_id)->first();
            $agentLevel = AgentLevels::where('level_weight', $agentLevelsInfo->level_weight)->first();
            foreach ($qrListInfo as $qr) {
                $qrInfoDate = [
                    'code_num' => $qr->code_num,
                    'user_id' => $shoppingOrder->user_id,
                    'p_id' => 0,
                    'level' => $agentLevel->level,
                    'level_weight' => $agentLevel->level_weight,
                    'award_amount' => $agentLevel->activation_amount,
                    'amount' => $agentLevel->activation_amount,
                    'status' => '1'
                ];
                QrCodeUserRelation::create($qrInfoDate);
            }
            $up_qrListInfo = [
                'user_id' => $shoppingOrder->user_id,
                'is_buy' => '1'
            ];
            QrListInfo::whereBetween('code_num', [$start_code, $end_code])
                ->update($up_qrListInfo);
            $update_data = [
                'tracking_code' => $tracking_code,
                'express_name' => $express_name,
                'order_status' => '5',
            ];
            ShoppingOrder::where('id', $id)->update($update_data);
            return json_encode([
                'status' => 1,
                'message' => '发货成功'
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //商城订单支付
    public function shopping_pay(Request $request)
    {
        try {
            $user_id = $request->get('user_id', "");//代理id
            $goods_id = $request->get('goods_id', "");//商品id
            $quantity = $request->get('quantity', "");//下单数量
            $price = $request->get('price', "");//下单单价
            $total_amount = $request->get('total_amount', 0);//下单总价
            $address_id = $request->get('address_id', "");//地址id
            $recipient = $request->get('recipient', "");//收货人
            $address = $request->get('address', "");//收货地址
            $phone = $request->get('phone', "");//收货人手机号
            $remark = $request->get('remark', "");//备注
            $total = round($total_amount * 100); // 将元转成分
            $body = '下单支付';
            $id = $request->get('order_id', "");//订单id
            $openid = $request->get('openid', "");
            $payMods = $request->get('payMods', "");
            $payment = $request->get('payment', "");
            $integral_consume = $request->get('integral_consume', "");
            //开启事务
            try {
                DB::beginTransaction();
                $order_status = '2';
                $integral_amount = 0.00;
                $balance_amount = 0.00;
                if ($payment == 3 || $total_amount <= 0) {//积分兑换和余额购买
                    $user = User::where('id', $user_id)->select('*')->first();
                    if ($payment == 3) {
                        $integral = $user->activation_integral - $integral_consume * $quantity;
                        if ($integral < 0) {
                            return json_encode([
                                'status' => "-1",
                                'message' => "下单失败，积分不足",
                            ]);
                        }
                        User::where('id', $user_id)->update(["activation_integral" => $integral]);
                        $integral_amount = $integral_consume * $quantity;
                    }
                    if ($payment == 2 && $total_amount <= 0) {
                        if ($price * $quantity > $user->reward_money) {
                            $money = ($user->reward_money + $user->money) - $price * $quantity;
                            if ($money < 0) {
                                return json_encode([
                                    'status' => "-1",
                                    'message' => "下单失败，余额不足",
                                ]);
                            }
                            User::where('id', $user_id)->update(["reward_money" => 0, "money" => $money]);
                        } else {
                            $reward_money = $user->reward_money - $price * $quantity;
                            User::where('id', $user_id)->update(["reward_money" => $reward_money]);
                        }
                        $balance_amount = $price * $quantity;
                        if ($quantity >= 50) {
                            $userShoppingReward = UserShoppingReward::where('user_id', $user->id)->select('user_id')->first();
                            if (!$userShoppingReward) {
                                $shoppingRewardDate = [
                                    'user_id' => $user->id,
                                    'p_user_id' => $user->pid,
                                    'p_user_name' => $user->name,
                                    'amount' => 150,
                                    'quantity' => $quantity
                                ];
                                UserShoppingReward::create($shoppingRewardDate);
                                $p_user = User::where('id', $user->pid)->select('*')->first();
                                $activation_integral = $p_user->activation_integral + 150;
                                User::where('id', $user->pid)->update(['activation_integral' => $activation_integral]);
                            }
                        }
                    }

                    $order_status = '1';
                    $this->setUserLevel($quantity, $user_id, $goods_id);
                } elseif ($payment == 2 && $total_amount > 0) {
                    $balance_amount = $price * $quantity - $total_amount;
                }

                //支付和订单入库
                $out_trade_no = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%02d', rand(0, 99));

                $data_in = [
                    'order_id' => $out_trade_no,
                    'user_id' => $user_id,
                    'goods_id' => $goods_id,
                    'quantity' => $quantity,
                    'price' => $price,
                    'total_amount' => $total_amount,
                    'address_id' => $address_id,
                    'order_status' => $order_status,
                    'recipient' => $recipient,
                    'address' => $address,
                    'phone' => $phone,
                    'remark' => $remark,
                    'payment' => $payment,
                    'balance_amount' => $balance_amount,
                    'integral_amount' => $integral_amount
                ];
                ShoppingOrder::create($data_in);//添加到商户订单表
                //设置代理展业
                $user = User::where('id', $user_id)->select('*')->first();
                if ($order_status == '1' && $quantity >= $user->qr_gap_num && $user->isMarketing == '1') {
                    User::where('id', $user_id)->update(['isMarketing' => '2', 'qr_gap_num' => 0]);
                }
                if ($order_status == '1' && $quantity < $user->qr_gap_num && $user->isMarketing == '1') {
                    $qr_gap_num = $user->qr_gap_num - $quantity;
                    User::where('id', $user_id)->update(['qr_gap_num' => $qr_gap_num]);
                }
                if ($total_amount > 0) {
                    $data = [
                        'pay_type' => $payMods,
                        'total_amount' => $total_amount,
                        'code' => $openid,
                        'out_trade_no' => $out_trade_no
                    ];
                    $res = $this->easyPay($data);
                    if ($res['status'] == '2') {
                        DB::rollBack();
                        return json_encode([
                            'status' => "-1",
                            'message' => "支付失败，请联系管理员",
                        ]);
                    }
                    $res_data['orderInfo'] = $res['orderInfo'];
                }
                DB::commit();
                $res_data['status'] = "1";
                $res_data['message'] = "下单成功";
                return json_encode($res_data);


            } catch (\Exception $e) {
                Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
                DB::rollBack();
                return json_encode([
                    'status' => '-1',
                    'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
                ]);
            }


        } catch
        (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    //微信支付
    public function wx_pay($total_fee, $out_trade_no, $body)
    {

        $xml = $this->arrayToXml($this->getOptions($total_fee, $out_trade_no, $body));
        // 微信支付post提交
        $data = $this->postXmlCurl($xml, $this->payUrl);
        // 把xml转成array
        $array_data = json_decode(json_encode(simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA)), true);

        if ($array_data['return_code'] == 'SUCCESS') {
            $conf = [
                'appid' => $this->appid,
                'partnerid' => $this->mch_id,
                'prepayid' => $array_data['prepay_id'],
                'package' => 'Sign=WXPay',
                'noncestr' => md5('app' . time()),
                'timestamp' => time(),
            ];
            $conf['sign'] = strtoupper(MD5('appid=' . $conf['appid'] . '&noncestr=' . $conf['noncestr'] . '&package=Sign=WXPay&partnerid=' . $conf['partnerid'] . '&prepayid=' . $conf['prepayid'] . '&timestamp=' . $conf['timestamp'] . '&key=' . $this->key));
            $array_data['return_code'] = 'SUCCESS';
            $array_data['orderInfo'] = $conf;
            return $array_data;
        } else {
            return $array_data;
        }
    }

    //易生支付
    public function easyPay($data)
    {
        $config = new EasyPayConfigController();
        $easyPayConfig = $config->easypay_config('1234');
        if (!$easyPayConfig) {
            return json_encode([
                'status' => '2',
                'message' => '易生支付配置不存在请检查配置'
            ]);
        }
        $easyPayMerchant = EasypayStore::where('store_id', $this->store_id)->first();
        if (!$easyPayMerchant) {
            return json_encode([
                'status' => '2',
                'message' => '易生支付商户号不存在'
            ]);
        }
        $out_trade_no = $data['out_trade_no'];
        $store = Store::where('store_id', $this->store_id)->first();
        $location = '';
        if (!$store->lat || !$store->lng) {
            $storeController = new \App\Api\Controllers\User\StoreController();
            $address = $store->province_name . $store->city_name . $store->area_name . $store->store_address;//获取经纬度的地址
            $api_res = $storeController->query_address($address, env('LBS_KEY'));
            if ($api_res['status'] == '1') {
                $store_lng = $api_res['result']['lng'];
                $store_lat = $api_res['result']['lat'];
                $store->update([
                    'lng' => $store_lng,
                    'lat' => $store_lat,
                ]);
                $location = "+" . $store_lat . "/-" . $store_lng;
            } else {
//                            Log::info('添加门店-获取经纬度错误');
//                            Log::info($api_res['message']);
            }

        } else {
            $location = "+" . $store->lat . "/-" . $store->lng;
        }
        //获取到账标识
        $EasyPayStoresImages = EasypayStoresImages::where('store_id', $this->store_id)
            ->select('real_time_entry')
            ->first();
        $patnerSettleFlag = '0'; //秒到
        if ($EasyPayStoresImages) {
            if ($EasyPayStoresImages->real_time_entry == 1) {
                $patnerSettleFlag = '0';
            } else {
                $patnerSettleFlag = '1';
            }
        }

        $easypay_data = [
            'channel_id' => $easyPayConfig->channel_id,
            'mno' => $easyPayMerchant->term_mercode,
            'device_id' => $easyPayMerchant->term_termcode,
            'out_trade_no' => $out_trade_no,
            'pay_type' => $data['pay_type'],
            'total_amount' => $data['total_amount'],
            'code' => $data['code'],
            'notify_url' => url('/api/user/easy_pay_notify'),
            'location' => $location,
            'patnerSettleFlag' => $patnerSettleFlag,
            'subAppid' => $this->min_appid,
            'store_name' => $store->store_name
        ];
        $return = $this->qr_submit($easypay_data);
        if ($return['status'] == '1') {
            if ($data['pay_type'] == 'wx_applet') {
                $weChatData = json_decode($return['data']['wxWcPayData'], true);
                if ($weChatData) {
                    if (isset($weChatData['timeStamp'])) $return['data']['payTimeStamp'] = $weChatData['timeStamp'];
                    if (isset($weChatData['package'])) $return['data']['payPackage'] = $weChatData['package'];
                    if (isset($weChatData['paySign'])) $return['data']['paySign'] = $weChatData['paySign'];
                    if (isset($weChatData['signType'])) $return['data']['paySignType'] = $weChatData['signType'];
                    if (isset($weChatData['nonceStr'])) $return['data']['paynonceStr'] = $weChatData['nonceStr'];

                    return [
                        'status' => '1',
                        'orderInfo' => $weChatData,
                        'out_trade_no' => $out_trade_no
                    ];
                }
            }
        }
        if ($return['status'] == '0') {
            return [
                'status' => '2',
                'message' => '支付失败',
            ];
        }
    }

    //易生支付 提交
    public function qr_submit($data)
    {
        try {
            $channel_id = $data['channel_id'] ?? '';
            $mer_id = $data['mno'] ?? '';
            $term_id = $data['device_id'] ?? '';
            $out_trade_no = $data['out_trade_no'] ?? '';
            $pay_type = $data['pay_type'] ?? '';
            $total_amount = $data['total_amount'] ?? '';
            $trade_amt = isset($total_amount) ? intval($total_amount * 100) : '';
            $code = $data['code'] ?? '';
            $notify_url = $data['notify_url'] ?? '';
            $patnerSettleFlag = $data['patnerSettleFlag'] ?? '';
            $subAppid = $data['subAppid'];
            if ($pay_type == 'ALIPAY') {
                $opt = 'WAJS1';
            } else if ($pay_type == 'WECHAT') {
                $opt = 'WTJS1';
            } else if ($pay_type == 'UNIONPAY') {
                $opt = 'WUJS1';
            } else if ($pay_type == 'wx_applet') {
                $opt = 'WTJS2';
            }
            $terminalinfo = [
                'location' => $data['location'],
                'terminalip' => '154.8.143.104'
            ];
            $appendData = [
                'terminalinfo' => $terminalinfo
            ];
            $data = [
                'tradeCode' => $opt,
                'tradeAmt' => $trade_amt,
                'orderInfo' => 'qrPay',
                'orgBackUrl' => $notify_url,
                'wxSubAppid' => $subAppid,
                'payerId' => $code,
                'patnerSettleFlag' => $patnerSettleFlag,
                'delaySettleFlag' => $patnerSettleFlag,
                'splitSettleFlag' => '0'//分账使用0=不分账1=分账
            ];
            if ($opt == 'WUJS1') {
                $data['orgFrontUrl'] = url('/api/easypay/pay_notify_url_u');
            }
            $BaseController = new \App\Api\Controllers\EasyPay\BaseController();
            $sign = $BaseController->getSign($data, $BaseController->key);
            $easypay_data = [
                'orgId' => $channel_id,
                'orgMercode' => $mer_id,
                'orgTermno' => $term_id,
                'orgTrace' => $out_trade_no,
                'sign' => $sign,
                'signType' => 'RSA2',
                'appendData' => $appendData,
                'data' => $data
            ];
            Log::info('商城订单-easy-params：');
            Log::info($easypay_data);

            $res_obj = $BaseController->payment->request($easypay_data, $this->jsapiUrl);
            Log::info('商城订单-easy-res：');
            Log::info($res_obj);

            //系统错误
            if (!$res_obj) {
                return [
                    'status' => '-1',
                    'message' => '系统错误',
                ];
            }

            $res_arr = json_decode($res_obj, true);

            //成功
            if ($res_arr['sysRetcode'] != '000000') {
                return [
                    'status' => '0',
                    'message' => $res_arr['sysRetmsg']
                ];
            } else {
                if ($res_arr['data']['finRetcode'] == '00') {
                    return [
                        'status' => '1',
                        'message' => '交易成功',
                        'data' => $res_arr['data'],
                    ];
                } elseif ($res_arr['data']['finRetcode'] == '99') {
                    return [
                        'status' => '1',
                        'message' => '待支付',
                        'data' => $res_arr['data'],
                    ];
                } else {
                    return [
                        'status' => '0',
                        'message' => '支付失败',
                        'data' => $res_arr['data'],
                    ];
                }

            }
        } catch (\Exception $ex) {
            Log::info('易生支付-静态码提交-错误');
            Log::info($ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine());
            return [
                'status' => '-1',
                'message' => $ex->getMessage() . '|' . $ex->getFile() . '|' . $ex->getLine(),
            ];
        }
    }

    //微信支付回调
    public function win_pay_notify(Request $request)
    {
        try {
            $data = $request->getContent();
            Log::info('购买商品-支付回调');
            Log::info($data);
            $jsonxml = json_encode(simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA));
            $result = json_decode($jsonxml, true);//转成数组，
            //Log::info(json_encode($result));
            if ($result) {
                //如果成功返回了
                $out_trade_no = $result['out_trade_no'];
                if ($result['return_code'] == 'SUCCESS' && $result['result_code'] == 'SUCCESS') {
                    $ShoppingOrder = ShoppingOrder::where('order_id', $out_trade_no)->first();//获取最新一条数据
                    if (empty($ShoppingOrder)) {
                        return false;
                    }
                    if ($ShoppingOrder->order_status == '1') {
                        return false;
                    }

                    ShoppingOrder::where('order_id', $out_trade_no)->update(['order_status' => '1']);
                    $params = [
                        'return_code' => 'SUCCESS',
                        'return_msg' => 'OK'
                    ];
                    return $this->array_to_xml($params);
                } else {
                    $params = [
                        'return_code' => 'FAIL',
                        'return_msg' => 'err'
                    ];
                    return $this->array_to_xml($params);
                }
            }
            $params = [
                'return_code' => 'SUCCESS',
                'return_msg' => 'OK'
            ];
            return $this->array_to_xml($params);
        } catch (\Exception $ex) {
            Log::info('微信支付异步');
            Log::info($ex->getMessage() . ' | ' . $ex->getLine());
        }
    }

    //易生支付回调
    public function easy_pay_notify(Request $request)
    {
        try {
            $data = $request->getContent();
            Log::info('购买商品-支付回调');
            Log::info($data);
            $data = json_decode($data, true);
            if (isset($data) && !empty($data)) {
                if (isset($data['data']['oriOrgTrace'])) {
                    $out_trade_no = $data['data']['oriOrgTrace']; //商户订单号(原交易流水)
                    $ShoppingOrder = ShoppingOrder::where('order_id', $out_trade_no)->select('*')->first();//获取最新一条数据
                    if (empty($ShoppingOrder)) {
                        return false;
                    }
                    if ($ShoppingOrder->order_status == '1') {
                        return false;
                    }
                    $this->setUserLevel($ShoppingOrder->quantity, $ShoppingOrder->user_id, $ShoppingOrder->goods_id);
                    ShoppingOrder::where('order_id', $out_trade_no)->update(['order_status' => '1']);

                    //设置代理展业情况
                    $user = User::where('id', $ShoppingOrder->user_id)->select('*')->first();
                    //减去库存
                    $shoppingGoods = ShoppingGoods::where('id', $ShoppingOrder->goods_id)->select('*')->first();
                    $stock = $shoppingGoods->stock - $ShoppingOrder->quantity;
                    ShoppingGoods::where('id', $ShoppingOrder->goods_id)->update(["stock" => $stock]);//修改库存
                    //扣除
                    if ($ShoppingOrder->payment == '2' && $ShoppingOrder->total_amount > 0) {
                        User::where('id', $ShoppingOrder->user_id)->update(['money' => 0, 'reward_money' => 0]);

                    }
                    if ($ShoppingOrder->payment == 1 || $ShoppingOrder->payment == 2) {
                        if ($ShoppingOrder->quantity >= 50) {
                            $userShoppingReward = UserShoppingReward::where('user_id', $user->id)->select('user_id')->first();
                            if (!$userShoppingReward) {
                                $shoppingRewardDate = [
                                    'user_id' => $user->id,
                                    'p_user_id' => $user->pid,
                                    'p_user_name' => $user->name,
                                    'amount' => 150,
                                    'quantity' => $ShoppingOrder->quantity
                                ];
                                UserShoppingReward::create($shoppingRewardDate);
                                $p_user = User::where('id', $user->pid)->select('*')->first();
                                $activation_integral = $p_user->activation_integral + 150;
                                User::where('id', $user->pid)->update(['activation_integral' => $activation_integral]);
                            }
                        }
                    }

                    $TemplateMessageController = new TemplateMessageController();
                    $TemplateMessageController->shopping_template_message($out_trade_no);
                    return 'ok';
                }
            }
        } catch (\Exception $ex) {
            Log::info('易生支付异步');
            Log::info($ex->getMessage() . ' | ' . $ex->getLine());
        }
    }

    public function setUserLevel($quantity, $user_id, $goods_id)
    {
        try {
            DB::beginTransaction();
            //计算代理商等级
            $agentLevel = AgentLevels::where('code_num', '<=', $quantity)
                ->orderBy('code_num', 'desc')
                ->first();
            $level = $agentLevel->level;
            $level_weight = $agentLevel->level_weight;
            $levelInfo = [
                'level' => $level,
                'user_id' => $user_id,
                'level_weight' => $level_weight,
                'start_time' => date('Y-m-d', time()),
                'end_time' => date('Y-m-d', time())
            ];
            if ($agentLevel->reward_month) {
                $reward_month = $agentLevel->reward_month;
                $start_time = date('Y-m-d', strtotime(date('Y-m-01', time())));
                $end_time = date('Y-m-d', strtotime(date('Y-m-01', strtotime($start_time)) . ' +' . $reward_month . ' month -1 day'));
                $levelInfo['start_time'] = $start_time;
                $levelInfo['end_time'] = $end_time;
            }
            $agentLevelsInfo = AgentLevelsInfos::where('user_id', $user_id)->select('*')->first();


            if ($agentLevelsInfo) {
                $current_data = date('Y-m-d', time());
                if (strtotime($agentLevelsInfo->end_time) > strtotime($levelInfo['start_time'])) {
                    $levelInfo['up_status'] = '2';
                    $levelInfo['up_level'] = $agentLevelsInfo->level_weight;
                }
                if ($agentLevel->level_weight >= $agentLevelsInfo->level_weight) {
                    AgentLevelsInfos::where('user_id', $user_id)->update($levelInfo);
                } else {
                    if (strtotime($agentLevelsInfo->end_time) < strtotime($current_data)) {
                        AgentLevelsInfos::where('user_id', $user_id)->update($levelInfo);
                    }
                }
            } else {
                AgentLevelsInfos::create($levelInfo);
            }

            DB::commit();
        } catch (\Exception $ex) {
            Log::info('设置代理等级错误');
            DB::rollBack();
            Log::info($ex->getMessage() . ' | ' . $ex->getLine());
        }

    }

    /**
     *  作用：array转xml
     */
    private function arrayToXml($arr)
    {
        $xml = "<xml>";
        foreach ($arr as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";
        return $xml;
    }

    public function array_to_xml($params)
    {
        if (!is_array($params) || count($params) <= 0) {
            return false;
        }
        $xml = "<xml>";
        foreach ($params as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";
        return $xml;
    }

    /*
     * 设置统一下单参数
     * $total_fee  总金额
     * $out_trade_no  订单号
     * $body  商品描述
     */
    public function getOptions($total_fee, $out_trade_no, $body)
    {
        $conf = [
            'appid' => $this->appid,
            'mch_id' => $this->mch_id,
            'nonce_str' => md5('app' . time()),
            'trade_type' => 'APP',
            'notify_url' => url($this->notify_url),
            'spbill_create_ip' => $_SERVER['REMOTE_ADDR'],
            'total_fee' => $total_fee,
            'out_trade_no' => $out_trade_no,
            'body' => $body,
        ];

        $conf['sign'] = $this->getSign($conf);

        return $conf;
    }

    /**
     *  作用：以post方式提交xml到对应的接口url
     */
    private function postXmlCurl($xml, $url, $second = 30)
    {
        //初始化curl
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        //这里设置代理，如果有的话
        //curl_setopt($ch,CURLOPT_PROXY, '8.8.8.8');
        //curl_setopt($ch,CURLOPT_PROXYPORT, 8080);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        //设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //post提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        //运行curl
        $data = curl_exec($ch);
        curl_close($ch);
        //返回结果
        if ($data) {
            return $data;
        } else {
            $error = curl_errno($ch);
            curl_close($ch);
            return false;
        }
    }

    /**
     *  作用：生成签名
     */
    private function getSign($Parameters)
    {
        //签名步骤一：按字典序排序参数
        ksort($Parameters);
        $String = $this->formatBizQueryParaMap($Parameters, false);
        //echo '【string1】'.$String.'</br>';
        //签名步骤二：在string后加入KEY
        $String = $String . "&key=" . $this->key;
        //echo "【string2】".$String."</br>";
        //签名步骤三：MD5加密
        $String = md5($String);
        //echo "【string3】 ".$String."</br>";
        //签名步骤四：所有字符转为大写
        $result_ = strtoupper($String);
        //echo "【result】 ".$result_."</br>";
        return $result_;
    }

}