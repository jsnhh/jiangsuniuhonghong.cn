<?php
namespace App\Api\Controllers\Qwx;


use Alipayopen\Sdk\AopClient;
use Alipayopen\Sdk\Request\AlipayTradeQueryRequest;
use App\Api\Controllers\BaseController;
use App\Models\Order;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SelectController extends BaseController
{

    public function query($data)
    {

    }


    //通过authcode获取订单信息
    public function get_order_by_authcode(Request $request)
    {
        try {
            $token = $this->parseToken();

            $bar_code = $request->post('barCode', ''); //付款码
            $store_id = $request->post('storeId', ''); //门店id
            $faceType = $request->post('faceType', ''); //刷脸标志 1-支付宝 2-微信

            $store_info = Store::where('store_id', $store_id)
                ->where('is_close', '0')
                ->where('is_delete', '0')
                ->where('status', '1')
                ->first();
            if (!$store_info) {
                return json_encode([
                    'status' => '2',
                    'message' => '门店不存在或门店状态异常，请联系管理员'
                ]);
            }

            $order_info = Order::where('auth_code', $bar_code)
                ->orderBy('updated_at', 'desc')
                ->first();
            if (!$order_info) {
                return json_encode([
                    'status' => '2',
                    'message' => '订单不存'
                ]);
            }

            //判断是否是刷脸支付
            if ($faceType == '1') {
                $pay_method = 'alipay_face';
            } elseif ($faceType == '2') {
                $pay_method = 'weixin_face';
            } else {
                $pay_method = 'qr_code';
            }

            if (($order_info->pay_method == 'qr_code') && ($pay_method != 'qr_code')) {
                $res = $order_info->update([
                    'pay_method' => $pay_method
                ]);
                if (!$res) {
                    Log::info('微收银-刷脸标志-更新失败');
                    Log::info($order_info->out_trade_no);
                }
            }

            $this->status = '1';
            $this->message = '查询成功';
            return $this->format($order_info);
        } catch (\Exception $ex) {
            Log::info('微收银查询-通过authcode获取订单信息-报错');
            Log::info($ex->getMessage() .' | '. $ex->getFile() .' | '. $ex->getLine());
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage() .' | '. $ex->getFile() .' | '. $ex->getLine()
            ]);
        }
    }


}
