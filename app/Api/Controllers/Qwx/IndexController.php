<?php

namespace App\Api\Controllers\Qwx;

use Alipayopen\Sdk\AopClient;
use Alipayopen\Sdk\Request\AlipayTradeFastpayRefundQueryRequest;
use Alipayopen\Sdk\Request\AlipayTradeQueryRequest;
use Alipayopen\Sdk\Request\AlipayTradeRefundRequest;
use App\Api\Controllers\Config\AlipayIsvConfigController;
use App\Api\Controllers\Config\AllinPayConfigController;
use App\Api\Controllers\Config\CcBankPayConfigController;
use App\Api\Controllers\Config\EasyPayConfigController;
use App\Api\Controllers\Config\EasySkPayConfigController;
use App\Api\Controllers\Config\FuiouConfigController;
use App\Api\Controllers\Config\HConfigController;
use App\Api\Controllers\Config\HkrtConfigController;
use App\Api\Controllers\Config\HuiPayConfigController;
use App\Api\Controllers\Config\HwcPayConfigController;
use App\Api\Controllers\Config\JdConfigController;
use App\Api\Controllers\Config\LianfuConfigController;
use App\Api\Controllers\Config\LianfuyoupayConfigController;
use App\Api\Controllers\Config\LinkageConfigController;
use App\Api\Controllers\Config\LtfConfigController;
use App\Api\Controllers\Config\MyBankConfigController;
use App\Api\Controllers\Config\NewLandConfigController;
use App\Api\Controllers\Config\PostPayConfigController;
use App\Api\Controllers\Config\QfPayConfigController;
use App\Api\Controllers\Config\TfConfigController;
use App\Api\Controllers\Config\VbillConfigController;
use App\Api\Controllers\Config\WeixinConfigController;
use App\Api\Controllers\Config\WftPayConfigController;
use App\Api\Controllers\Config\YinshengConfigController;
use App\Api\Controllers\DuoLaBao\ManageController;
use App\Api\Controllers\Merchant\TransactionDeductionController;
use App\Api\Controllers\MyBank\TradePayController;
use App\Api\Controllers\Newland\PayController;
use App\Api\Controllers\EasyPay\PayController as EasypayPayController;
use App\Api\Controllers\Linkage\PayController as LinkagePayController;
use App\Api\Controllers\QfPay\PayController as QfPayController;
use App\Api\Controllers\HwcPay\PayController as HwcPayPayController;
use App\Api\Controllers\WftPay\PayController as WftPayPayController;
use App\Common\PaySuccessAction;
use App\Common\StoreDayMonthOrder;
use App\Common\UserGetMoney;
use App\Models\AlipayAppOauthUsers;
use App\Models\AlipayIsvConfig;
use App\Models\MerchantWalletDetail;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\RefundOrder;
use App\Models\Shop;
use App\Models\Store;
use App\Models\UserWalletDetail;
use EasyWeChat\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use MyBank\Tools;

class IndexController extends BaseController
{

    //扫一扫收款
    public function scan_pay(Request $request)
    {
        try {
            //获取请求参数
            $data = $request->getContent();
            //  $data_all = $request->all();
//            {
//                "merchantId":"2020518155323231527",
//                "outTradeNo":"dm5326838259",
//                "totalFee":1,
//                "orderIp":"100.120.35.131",
//                "authCode":"134622026953793748", //barCode 付款码
//                "channel":"wx_barcode_pay",
//                "device_id":"632686",
//                "Version":"2.0.0",
//                "TimeStamp":"2020-06-04T03:51:46Z",
//                "SignatureNonce":2607,
//                "sign":"782e07cd8650e37514378be063e2fe38"
//            }

            $data = json_decode($data, true);

            //验证签名
            $check = $this->check_md5($data);
            if ($check['return_code'] == 'FALL') {
                return $this->return_data($check);
            }

            $code = $data['authCode'];
            $pay_method = 'qr_code';

            //刷脸处理
            $str = substr($code, 0, 2);
            if (in_array($str, ['28']) || in_array($str, ['13', '14'])) {
                $strlen = strlen($code);
                if ($strlen > 18) {
                    $substr = substr($code, -1); //截取判断最后一个
                    $code = substr($code, 0, -1); //去掉最后一个

                    //支付宝刷脸
                    if ($substr == '2' && in_array($str, ['28'])) {
                        $pay_method = 'alipay_face';
                    }

                    //微信刷脸
                    if ($substr == '2' && in_array($str, ['13', '14'])) {
                        $pay_method = 'weixin_face';
                    }
                }

            }
//            $strlen = strlen($code);
//            if ($strlen > 18) {
//                $substr = substr($code, -1); //截取判断最后一个
//                $code = substr($code, 0, -1); //去掉最后一个
//                $str = substr($code, 0, 2);
//
//                //支付宝刷脸
//                if ($substr == '2' && in_array($str, ['28'])) {
//                    $pay_method = 'alipay_face';
//                }
//
//                //微信刷脸
//                if ($substr == '2' && in_array($str, ['13', '14'])) {
//                    $pay_method = 'weixin_face';
//                }
//            }

            //调用系统前参数
            $ro_data = [
                'store_id' => $data['merchantId'],
                'code' => $code,
                'total_amount' => $data['totalFee'] / 100,
                'shop_price' => $data['totalFee'] / 100,
                'qwx_no' => $data['outTradeNo'],
                'remark' => '',
                'device_id' => $data['device_id'],
                'shop_name' => '商品',
                'shop_desc' => '商品',
                'pay_method' => $pay_method
            ];

            //发起交易
            $order = new TradepayTwoController();
            $tra_data = $order->scan_pay($ro_data);
            $tra_data_arr = json_decode($tra_data, true);
            $out_transaction_id = '';
            $time_end = '';
            $re_data = [
                'return_code' => 'SUCCESS', //SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                'return_msg' => null,
                'result_code' => '',
                'result_msg' => '',
                'outTradeNo' => $data['outTradeNo'], //微收银唯一订单号
                'tradeNo' => '', //第三方平台订单号即你们的订单号
                'totalFee' => number_format($data['totalFee'], 0, '.', ''), //交易金额，以分
                'channel' => $data['channel'], //支付类型 微信主扫：wx_barcode_pay 支付宝：ali_barcode_pay
                'out_transaction_id' => $out_transaction_id, //微信，支付宝支付凭证上的退款条形码
                'time_end' => $time_end //支付完成时间为yyyyMMddHHmmss,20141030133525
            ];

            //用户支付成功
            if ($tra_data_arr['status'] == 1) {
                //微信，支付宝支付凭证
                $out_transaction_id = isset($tra_data_arr['data']['out_transaction_id']) ? $tra_data_arr['data']['out_transaction_id'] : "";

                //快钱微信处理
                $mybank_weixin = substr($out_transaction_id, 0, 4);
                if ($mybank_weixin == "MYBK") {
                    $out_transaction_id = strtolower($out_transaction_id); //大写转小写
                }
                $re_data['result_code'] = 'SUCCESS';
                $re_data['result_msg'] = '支付成功';
                $re_data['tradeNo'] = $tra_data_arr['data']['out_trade_no'];
                $re_data['out_transaction_id'] = $out_transaction_id;
                if (isset($tra_data_arr['data']['pay_time'])) $re_data['time_end'] = date('YmdHis', strtotime($tra_data_arr['data']['pay_time']));
            } //正在支付
            elseif ($tra_data_arr['status'] == 9) {
                $re_data['result_code'] = 'USERPAYING';
                $re_data['result_msg'] = '用户正在支付';
                $re_data['out_transaction_id'] = '';
                $re_data['tradeNo'] = $tra_data_arr['data']['out_trade_no'];
            } //其他错误
            else {
                $re_data['result_code'] = 'FALL';
                $re_data['result_msg'] = $tra_data_arr['message'];
            }

            return $this->return_data($re_data);
        } catch (\Exception $exception) {
            Log::info('微收银-扫一扫收款error');
            Log::info($exception->getMessage() . $exception->getLine());
            $err = [
                'return_code' => 'FALL',
                'return_msg' => $exception->getMessage() . $exception->getLine()
            ];
            return $this->return_data($err);
        }
    }


    //查询订单号状态
    public function order_query(Request $request)
    {
        try {
            //获取请求参数
            $data = $request->getContent();
            $data = json_decode($data, true);
//            Log::info('微收银-查询订单号状态');
//            Log::info($data);
//            $data = array (
//                'merchantId' => '2020521144958530603', //store_id
//                'outTradeNo' => 'dm5296345130', //qwx_no
//                'out_transaction_id' => '187580042826202011101281254585', //trade_no
//                'channel' => 'wx_barcode_pay',
//                'Version' => '2.0.0',
//                'TimeStamp' => '2020-05-26T02:47:31Z',
//                'SignatureNonce' => 2865,
//                'sign' => '129ee3280083a5c206d2aa5b1211946c',
//            );

            //验证签名
            $check = $this->check_md5($data);
            if ($check['return_code'] == 'FALL') {
                return $this->return_data($check);
            }

            //调用系统前参数
            $where = [];
            $c_o = 'no_id_qwx';
            if (isset($data['tradeNo'])) {
                $c_o = 'q' . $data['tradeNo'];
                $where[] = ['out_trade_no', '=', $data['tradeNo']];
            }

            if (isset($data['out_transaction_id'])) {
                $c_o = 'q' . $data['out_transaction_id'];
                $where[] = ['trade_no', '=', $data['out_transaction_id']];
//                $where[] = ['out_trade_no', '=', $data['out_transaction_id']];
            }

            if (isset($data['outTradeNo'])) {
                $c_o = 'q' . $data['outTradeNo'];
                $where[] = ['qwx_no', '=', $data['outTradeNo']];
            }

            $order = Cache::get($c_o);
            if (!$order) {
                $day = date('Ymd', time());
                $table = 'orders_' . $day;
                if (Schema::hasTable($table)) {
                    $order = DB::table($table)->where('store_id', $data['merchantId']);
                } else {
                    $order = Order::where('store_id', $data['merchantId']);
                }

                //发起查询
                $order = $order->where($where)
                    ->select(
                        'id',
                        'ways_source',
                        'company',
                        'ways_type',
                        'ways_type_desc',
                        'pay_status',
                        'out_trade_no',
                        'trade_no',
                        'total_amount',
                        'qwx_no',
                        'rate',
                        'merchant_id',
                        'store_id',
                        'user_id',
                        'store_name',
                        'fee_amount'
                    )
                    ->first();

                Cache::put($c_o, $order, 1);
            }

            //如果订单号为空或者不存在
            if (!$order) {
                $re_data['result_code'] = 'FALL';
                $re_data['result_msg'] = '订单号不存在';
                return $this->return_data($re_data);
            }

            $out_transaction_id = '';
            $time_end = '';
            $trade_no = $order->trade_no;

            $channel = isset($data['channel']) ? $data['channel'] : "";
            if (!$channel) {
                if ($order->ways_source == "alipay") {
                    $channel = "ali_barcode_pay";
                }

                if ($order->ways_source == "weixin") {
                    $channel = "wx_barcode_pay";
                }
            }

            $re_data = [
                'return_code' => 'SUCCESS', //SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                'return_msg' => null,
                'result_code' => '',
                'result_msg' => '',
                'tradeNo' => $order->out_trade_no,
                'outTradeNo' => '', //微收银唯一订单号
                'out_transaction_id' => $out_transaction_id, //微信，支付宝支付凭证上的退款条形码
                'totalFee' => number_format($order->total_amount * 100, 0, '.', ''), //交易金额，以分
                'channel' => $channel, //支付类型 微信主扫：wx_barcode_pay 支付宝：ali_barcode_pay
                'time_end' => $time_end //支付完成时间为yyyyMMddHHmmss,20141030133525
            ];

            $type = $order->ways_type;
            $out_trade_no = $order->out_trade_no;
            $store = Store::where('store_id', $data['merchantId'])
                ->select('config_id', 'merchant_id', 'pid', 'people_phone', 'source')
                ->first();
            $config_id = $store->config_id;
            $store_id = $data['merchantId'];
            $store_pid = $store->pid;

            //官方支付宝查询
            if (999 < $type && $type < 1999) {
                //配置
                $isvconfig = new AlipayIsvConfigController();
                $storeInfo = $isvconfig->alipay_auth_info($store_id, $store_pid);
                $config = $isvconfig->AlipayIsvConfig($config_id);

                $aop = new AopClient();
                $aop->apiVersion = "2.0";
                $aop->appId = $config->app_id;
                $aop->rsaPrivateKey = $config->rsa_private_key;
                $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                $aop->signType = "RSA2"; //升级算法
                $aop->gatewayUrl = $config->alipay_gateway;
                $aop->format = "json";
                $aop->charset = "GBK";
                $aop->version = "2.0";
                $aop->method = 'alipay.trade.query';
                $requests = new AlipayTradeQueryRequest();
                $requests->setBizContent("{" .
                    "    \"out_trade_no\":\"" . $order->out_trade_no . "\"" .
                    "  }");
                $status = $aop->execute($requests, '', $storeInfo->app_auth_token);
                if (isset($status->alipay_trade_query_response->trade_status)) {
                    //支付成功
                    if ($status->alipay_trade_query_response->trade_status == "TRADE_SUCCESS") {
                        $re_data['result_code'] = 'SUCCESS';
                        $re_data['result_msg'] = '支付成功';
                        $re_data['outTradeNo'] = $order->qwx_no;
                        $re_data['out_transaction_id'] = $order->out_trade_no; //$status->alipay_trade_query_response->trade_no;
                        $re_data['time_end'] = date('YmdHis', strtotime($status->alipay_trade_query_response->send_pay_date));
                        $re_data['receipt_amount'] = $status->alipay_trade_query_response->receipt_amount;


                        if ($order->pay_status != 1) {
                            Cache::forget($c_o);

                            $insert_data = [
                                'status' => 'TRADE_SUCCESS',
                                'pay_status' => 1,
                                'pay_status_desc' => '支付成功',
                                'buyer_logon_id' => $status->alipay_trade_query_response->buyer_user_id,
                                'trade_no' => $status->alipay_trade_query_response->trade_no,
                                'pay_time' => $status->alipay_trade_query_response->send_pay_date,
                                'receipt_amount' => $status->alipay_trade_query_response->receipt_amount,

                            ];
                            $this->update_day_order($insert_data, $out_trade_no);

                            //支付成功后的动作
                            $data = [
                                'ways_type' => $order->ways_type,
                                'ways_type_desc' => $order->ways_type_desc,
                                'source_type' => '1000', //返佣来源
                                'source_desc' => '支付宝', //返佣来源说明
                                'total_amount' => $order->total_amount,
                                'out_trade_no' => $order->out_trade_no,
                                'rate' => $order->rate,
                                'merchant_id' => $order->merchant_id,
                                'store_id' => $order->store_id,
                                'user_id' => $order->user_id,
                                'config_id' => $config_id,
                                'store_name' => $order->store_name,
                                'ways_source' => $order->ways_source,
                                'company' => $order->company,
                                'pay_time' => $status->alipay_trade_query_response->send_pay_date,
                                'fee_amount' => $order->fee_amount,
                                'no_push' => '1', //不推送
                                'no_fuwu' => '1', //不服务消息
                                'no_print' => '1', //不打印
                                //'no_v' => '1', //不小盒子播报

                            ];
                            PaySuccessAction::action($data);
                        }

                        return $this->return_data($re_data);
                    } //等待付款
                    elseif ($status->alipay_trade_query_response->trade_status == "WAIT_BUYER_PAY") {
                        $re_data['result_code'] = 'USERPAYING';
                        $re_data['result_msg'] = '等待用户付款';
                        $re_data['outTradeNo'] = $order->qwx_no;

                        return $this->return_data($re_data);
                    } //订单关闭
                    elseif ($status->alipay_trade_query_response->trade_status == 'TRADE_CLOSED') {
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = '订单关闭';
                        $re_data['outTradeNo'] = $order->qwx_no;

                        return $this->return_data($re_data);
                    } else {
                        $message = $status->alipay_trade_query_response->sub_msg;
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $message;
                        $re_data['outTradeNo'] = $order->qwx_no;

                        return $this->return_data($re_data);
                    }
                } else { //其他情况
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '状态未知';
                    $re_data['outTradeNo'] = $order->qwx_no;

                    return $this->return_data($re_data);
                }

            }

            //直付通
            if (16000 < $type && $type < 16999) {
                $config_type = '03';
                //配置
                $isvconfig = new AlipayIsvConfigController();
                $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);

                $aop = new AopClient();
                $aop->apiVersion = "2.0";
                $aop->appId = $config->app_id;
                $aop->rsaPrivateKey = $config->rsa_private_key;
                $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                $aop->signType = "RSA2"; //升级算法
                $aop->gatewayUrl = $config->alipay_gateway;
                $aop->format = "json";
                $aop->charset = "GBK";
                $aop->version = "2.0";
                $aop->method = 'alipay.trade.query';
                $requests = new AlipayTradeQueryRequest();
                $requests->setBizContent("{" .
                    "    \"out_trade_no\":\"" . $order->out_trade_no . "\"" .
                    "  }");
                $status = $aop->execute($requests, '', '');

                if (isset($status->alipay_trade_query_response->trade_status)) {
                    //支付成功
                    if ($status->alipay_trade_query_response->trade_status == "TRADE_SUCCESS") {
                        $re_data['result_code'] = 'SUCCESS';
                        $re_data['result_msg'] = '支付成功';
                        $re_data['outTradeNo'] = $order->qwx_no;
                        $re_data['out_transaction_id'] = $order->out_trade_no; //$status->alipay_trade_query_response->trade_no;
                        $re_data['time_end'] = date('YmdHis', strtotime($status->alipay_trade_query_response->send_pay_date));

                        if ($order->pay_status != 1) {
                            Cache::forget($c_o);

                            $insert_data = [
                                'status' => 'TRADE_SUCCESS',
                                'pay_status' => 1,
                                'pay_status_desc' => '支付成功',
                                'buyer_logon_id' => $status->alipay_trade_query_response->buyer_user_id,
                                'trade_no' => $status->alipay_trade_query_response->trade_no,
                                'pay_time' => $status->alipay_trade_query_response->send_pay_date
                            ];
                            $this->update_day_order($insert_data, $out_trade_no);

                            //支付成功后的动作
                            $data = [
                                'ways_type' => $order->ways_type,
                                'ways_type_desc' => $order->ways_type_desc,
                                'source_type' => '16000', //返佣来源
                                'source_desc' => '支付宝', //返佣来源说明
                                'total_amount' => $order->total_amount,
                                'out_trade_no' => $order->out_trade_no,
                                'rate' => $order->rate,
                                'merchant_id' => $order->merchant_id,
                                'store_id' => $order->store_id,
                                'user_id' => $order->user_id,
                                'config_id' => $config_id,
                                'store_name' => $order->store_name,
                                'ways_source' => $order->ways_source,
                                'company' => $order->company,
                                'pay_time' => $status->alipay_trade_query_response->send_pay_date,
                                'fee_amount' => $order->fee_amount,
                                'no_push' => '1', //不推送
                                'no_fuwu' => '1', //不服务消息
                                'no_print' => '1', //不打印
                                //'no_v' => '1', //不小盒子播报
                            ];
                            PaySuccessAction::action($data);
                        }

                        return $this->return_data($re_data);
                    } //等待付款
                    elseif ($status->alipay_trade_query_response->trade_status == "WAIT_BUYER_PAY") {
                        $re_data['result_code'] = 'USERPAYING';
                        $re_data['result_msg'] = '等待用户付款';
                        $re_data['outTradeNo'] = $order->qwx_no;

                        return $this->return_data($re_data);
                    } //订单关闭
                    elseif ($status->alipay_trade_query_response->trade_status == 'TRADE_CLOSED') {
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = '订单关闭';
                        $re_data['outTradeNo'] = $order->qwx_no;

                        return $this->return_data($re_data);
                    } else {
                        //其他情况
                        $message = $status->alipay_trade_query_response->sub_msg;
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $message;
                        $re_data['outTradeNo'] = $order->qwx_no;

                        return $this->return_data($re_data);
                    }
                } else {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '状态未知';
                    $re_data['outTradeNo'] = $order->qwx_no;

                    return $this->return_data($re_data);
                }
            }

            //官方微信查询
            if (1999 < $type && $type < 2999) {
                $config = new WeixinConfigController();
                $options = $config->weixin_config($config_id);
                $weixin_store = $config->weixin_merchant($store_id, $store_pid);
                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;

                $config = [
                    'app_id' => $options['app_id'],
                    'mch_id' => $options['payment']['merchant_id'],
                    'key' => $options['payment']['key'],
                    'cert_path' => $options['payment']['cert_path'], //XXX: 绝对路径！
                    'key_path' => $options['payment']['key_path'],     //XXX: 绝对路径！
                    'sub_mch_id' => $wx_sub_merchant_id,
                    // 'device_info'     => '013467007045764',
                    // 'sub_app_id'      => ''
                ];
                $payment = Factory::payment($config);
                $query = $payment->order->queryByOutTradeNumber($order->out_trade_no);
                //成功
                if ($query['trade_state'] == 'SUCCESS') {
                    //删除商品
                    if ($weixin_store->wx_shop_id && Schema::hasTable('shops')) {
                        Shop::where('store_id', $store_id)
                            ->where('merchant_id', $order->merchant_id)
                            ->delete();
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $order->out_trade_no; //$query['transaction_id'];
                    $re_data['time_end'] = $query['time_end'];

                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => 'TRADE_SUCCESS',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => $query['openid'],
                            'trade_no' => $query['transaction_id'],
                            'pay_time' => date('Y-m-d H:i:s', strtotime($query['time_end']))
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '2000', //返佣来源
                            'source_desc' => '微信支付', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'company' => $order->company,
                            'fee_amount' => $order->fee_amount,
                            'pay_time' => date('Y-m-d H:i:s', strtotime($query['time_end'])),
                            'no_push' => '1', //不推送
                            'no_fuwu' => '1', //不服务消息
                            'no_print' => '1', //不打印
                            //'no_v' => '1', //不小盒子播报

                        ];
                        PaySuccessAction::action($data);
                    }

                    Log::info("ly-微收银查询接口");
                    Log::info($query);
                    // 如果用户输入密码，微信被扫没有返回优惠金额、实付金额等，所以通过查询订单来更新
                    // settlement_total_fee 当订单使用了免充值型优惠券后返回该参数
                    // 如果有 settlement_total_fee 这个参数，商家实收金额等于 settlement_total_fee
                    // 否则商家实收金额等于 total_fee，优惠金额不纳入计算
                    if (isset($query['settlement_total_fee']) && !empty($query['settlement_total_fee'])) {
                        if ($order->receipt_amount != $query['settlement_total_fee'] / 100) {
                            $data_update['receipt_amount'] = $query['settlement_total_fee'] / 100;
                            if (isset($query['coupon_fee']) && !empty($query['coupon_fee'])) {
                                $data_update['mdiscount_amount'] = $query['coupon_fee'] / 100;
                            }
                            $this->update_day_order($data_update, $out_trade_no);
                        }
                    } else if (isset($query['total_fee']) && !empty($query['total_fee'])) {
                        if ($order->receipt_amount != $query['total_fee'] / 100) {
                            $data_update['receipt_amount'] = $query['total_fee'] / 100;
                            if (isset($query['coupon_fee']) && !empty($query['coupon_fee'])) {
                                $data_update['discount_amount'] = $query['coupon_fee'] / 100;
                            }
                            $this->update_day_order($data_update, $out_trade_no);
                        }
                    }
                } elseif ($query['trade_state'] == "USERPAYING") {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '等待用户付款';
                    $re_data['outTradeNo'] = $order->qwx_no;

                    return $this->return_data($re_data);
                } else {
                    //其他情况
                    $message = $query['trade_state_desc'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;

                    return $this->return_data($re_data);
                }
            }

            //官方微信a 查询
            if (3999 < $type && $type < 4999) {
                $config = new WeixinConfigController();
                $options = $config->weixina_config($config_id);
                $weixin_store = $config->weixina_merchant($store_id, $store_pid);
                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;

                $config = [
                    'app_id' => $options['app_id'],
                    'mch_id' => $options['payment']['merchant_id'],
                    'key' => $options['payment']['key'],
                    'cert_path' => $options['payment']['cert_path'], //XXX: 绝对路径！
                    'key_path' => $options['payment']['key_path'],     //XXX: 绝对路径！
                    'sub_mch_id' => $wx_sub_merchant_id,
                    // 'device_info'     => '013467007045764',
                    // 'sub_app_id'      => ''
                ];
                $payment = Factory::payment($config);
                $query = $payment->order->queryByOutTradeNumber($order->out_trade_no);
                //成功
                if ($query['trade_state'] == 'SUCCESS') {
                    //删除商品
                    if ($weixin_store->wx_shop_id && Schema::hasTable('shops')) {
                        Shop::where('store_id', $store_id)
                            ->where('merchant_id', $order->merchant_id)
                            ->delete();
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $order->out_trade_no; //$query['transaction_id'];
                    $re_data['time_end'] = $query['time_end'];

                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => 'TRADE_SUCCESS',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => $query['openid'],
                            'trade_no' => $query['transaction_id'],
                            'pay_time' => date('Y-m-d H:i:s', strtotime($query['time_end']))
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '4000', //返佣来源
                            'source_desc' => '微信支付a', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'company' => $order->company,
                            'fee_amount' => $order->fee_amount,
                            'pay_time' => date('Y-m-d H:i:s', strtotime($query['time_end'])),
                            'no_push' => '1', //不推送
                            'no_fuwu' => '1', //不服务消息
                            'no_print' => '1', //不打印
                            //'no_v' => '1', //不小盒子播报
                        ];
                        PaySuccessAction::action($data);
                    }

                    // 如果用户输入密码，微信被扫没有返回优惠金额、实付金额等，所以通过查询订单来更新
                    // settlement_total_fee 当订单使用了免充值型优惠券后返回该参数
                    // 如果有 settlement_total_fee 这个参数，商家实收金额等于 settlement_total_fee
                    // 否则商家实收金额等于 total_fee，优惠金额不纳入计算
                    if (isset($query['settlement_total_fee']) && !empty($query['settlement_total_fee'])) {
                        if ($order->receipt_amount != $query['settlement_total_fee'] / 100) {
                            $data_update['receipt_amount'] = $query['settlement_total_fee'] / 100;
                            if (isset($query['coupon_fee']) && !empty($query['coupon_fee'])) {
                                $data_update['mdiscount_amount'] = $query['coupon_fee'] / 100;
                            }
                            $this->update_day_order($data_update, $out_trade_no, $table);
                        }
                    } else if (isset($query['total_fee']) && !empty($query['total_fee'])) {
                        if ($order->receipt_amount != $query['total_fee'] / 100) {
                            $data_update['receipt_amount'] = $query['total_fee'] / 100;
                            if (isset($query['coupon_fee']) && !empty($query['coupon_fee'])) {
                                $data_update['discount_amount'] = $query['coupon_fee'] / 100;
                            }
                            $this->update_day_order($data_update, $out_trade_no, $table);
                        }
                    }
                } elseif ($query['trade_state'] == "USERPAYING") {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '等待用户付款';
                    $re_data['outTradeNo'] = $order->qwx_no;

                    return $this->return_data($re_data);
                } else {
                    //其他情况
                    $message = $query['trade_state_desc'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;

                    return $this->return_data($re_data);
                }
            }

            //京东收银支付
            if (5999 < $type && $type < 6999) {
                $config = new JdConfigController();
                $jd_config = $config->jd_config($config_id);
                if (!$jd_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '京东配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $jd_merchant = $config->jd_merchant($store_id, $store_pid);
                if (!$jd_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '京东商户号不存在';
                    return $this->return_data($re_data);
                }

                $obj = new \App\Api\Controllers\Jd\PayController();
                $data = [];
                $data['out_trade_no'] = $order->out_trade_no;
                $data['request_url'] = $obj->order_query_url; //请求地址
                $data['merchant_no'] = $jd_merchant->merchant_no;
                $data['md_key'] = $jd_merchant->md_key; //
                $data['des_key'] = $jd_merchant->des_key; //
                $data['systemId'] = $jd_config->systemId; //
                $return = $obj->order_query($data);

                //支付成功
                if ($return["status"] == 1) {
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        $trade_no = $return['data']['tradeNo'];
                        $channelNoSeq = isset($return['data']['channelNoSeq']) ? $return['data']['channelNoSeq'] : $trade_no; //条码
                        $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['payFinishTime']));
                        $buyer_pay_amount = $return['data']['piAmount'] / 100;
                        $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $channelNoSeq,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '6000', //返佣来源
                            'source_desc' => '京东金融', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'fee_amount' => $order->fee_amount,
                            'no_push' => '1', //不推送
                            'no_fuwu' => '1', //不服务消息
                            'no_print' => '1', //不打印
                            //'no_v' => '1', //不小盒子播报
                        ];
                        PaySuccessAction::action($data);
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $order->out_trade_no; //$query['transaction_id'];
                    $re_data['time_end'] = date('YmdHis', strtotime($return['data']['payFinishTime']));
                } //等待付款
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '等待用户付款';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //订单退款
                elseif ($return["status"] == 4) {
                    $message = '订单已经退款';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //快钱支付
            if (2999 < $type && $type < 3999) {
                //读取配置
                $MyBankobj = new MyBankConfigController();
                $mybank_merchant = $MyBankobj->mybank_merchant($store_id, $store_pid);
                if (!$mybank_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '快钱商户号不存在';
                    return $this->return_data($re_data);
                }

                $MerchantId = $mybank_merchant->MerchantId;
                $wx_AppId = $mybank_merchant->wx_AppId;

                $MyBankConfig = $MyBankobj->MyBankConfig($config_id, $wx_AppId);
                if (!$MyBankConfig) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '快钱配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $obj = new TradePayController();
                $return = $obj->mybankOrderQuery($MerchantId, $config_id, $order->out_trade_no);
                if ($return['status'] == 0) {

                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $return['message'];
                    return $this->return_data($re_data);
                }
                $body = $return['data']['document']['response']['body'];
                $TradeStatus = $body['TradeStatus'];

                //成功
                if ($TradeStatus == 'succ') {
                    $OrderNo = $body['MerchantOrderNo'];
                    $GmtPayment = $body['GmtPayment'];
                    $buyer_id = '';

                    if ($type == 3004) {
                        $buyer_id = $body['SubOpenId'];
                    }

                    if ($type == 3003) {
                        $buyer_id = $body['BuyerUserId'];
                    }

                    $pay_time = date('Y-m-d H:i:s', strtotime($GmtPayment));
                    $payment_method = strtolower($body['Credit']);

                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => 1,
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_id' => $buyer_id,
                            'trade_no' => $OrderNo,
                            'pay_time' => $pay_time,
                            'payment_method' => $payment_method,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '3000', //返佣来源
                            'source_desc' => '快钱', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'fee_amount' => $order->fee_amount,
                            'no_push' => '1', //不推送
                            'no_fuwu' => '1', //不服务消息
                            'no_print' => '1', //不打印
                            //'no_v' => '1', //不小盒子播报

                        ];
                        PaySuccessAction::action($data);
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $order->out_trade_no; //$query['transaction_id'];
                    $re_data['time_end'] = date('YmdHis', strtotime($pay_time));
                } elseif ($TradeStatus == 'paying') {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '等待用户付款';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //其他情况
                else {
                    $message = '请重新扫码';
                    //其他情况
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //新大陆收银支付
            if (7999 < $type && $type < 8999) {
                //读取配置
                $config = new NewLandConfigController();
                $new_land_config = $config->new_land_config($config_id);
                if (!$new_land_config) {

                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '新大陆配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $new_land_merchant = $config->new_land_merchant($store_id, $store_pid);
                if (!$new_land_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '和融通商户号不存在';
                    return $this->return_data($re_data);
                }

                $request_data = [
                    'out_trade_no' => $order->out_trade_no,
                    'key' => $new_land_merchant->nl_key,
                    'org_no' => $new_land_config->org_no,
                    'merc_id' => $new_land_merchant->nl_mercId,
                    'trm_no' => $new_land_merchant->trmNo,
                    'op_sys' => '3',
                    'opr_id' => $store->merchant_id,
                    'trm_typ' => 'T'
                ];
                $obj = new PayController();
                $return = $obj->order_query($request_data);

                //支付成功
                if ($return["status"] == 1) {
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        $trade_no = $return['data']['orderNo'];
                        $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['sysTime']));
                        $buyer_pay_amount = $return['data']['amount'] / 100;
                        $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '8000', //返佣来源
                            'source_desc' => '新大陆', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'fee_amount' => $order->fee_amount,
                            'pay_time' => $pay_time,
                            'no_push' => '1', //不推送
                            'no_fuwu' => '1', //不服务消息
                            'no_print' => '1', //不打印
                            // 'no_v' => '1', //不小盒子播报
                        ];
                        PaySuccessAction::action($data);
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $trade_no; //$query['transaction_id'];
                    $re_data['time_end'] = date('YmdHis', strtotime($return['data']['sysTime']));
                } //等待付款
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '等待用户付款';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }//订单退款
                elseif ($return["status"] == 4) {
                    $message = '订单已经退款';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //和融通收银支付
            if (8999 < $type && $type < 9999) {
                //读取配置
                $config = new HConfigController();
                $h_config = $config->h_config($config_id);
                if (!$h_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '和融通配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $h_merchant = $config->h_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '和融通商户号不存在';
                    return $this->return_data($re_data);
                }

                $obj = new \App\Api\Controllers\Huiyuanbao\PayController();
                $data = [];
                $data['out_trade_no'] = $order->out_trade_no;
                $data['request_url'] = $obj->order_query_url; //请求地址
                $data['md_key'] = $h_config->md_key; //
                $data['mid'] = $h_merchant->h_mid; //
                $data['orgNo'] = $h_config->orgNo; //

                $return = $obj->order_query($data);

                //支付成功
                if ($return["status"] == 1) {
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        $trade_no = $return['data']['transactionId'];
                        $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['timeEnd']));
                        $buyer_pay_amount = $return['data']['totalFee'];
                        $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');


                        Cache::forget($c_o);


                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];

                        $this->update_day_order($insert_data, $out_trade_no);


                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '9000', //返佣来源
                            'source_desc' => '和融通', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'fee_amount' => $order->fee_amount,
                            'pay_time' => $pay_time,
                            'no_push' => '1', //不推送
                            'no_fuwu' => '1', //不服务消息
                            'no_print' => '1', //不打印
                            //'no_v' => '1', //不小盒子播报

                        ];


                        PaySuccessAction::action($data);

                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $trade_no; //$query['transaction_id'];
                    $re_data['time_end'] = date('YmdHis', strtotime($return['data']['timeEnd']));
                } //等待付款
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '等待用户付款';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //订单退款
                elseif ($return["status"] == 4) {
                    $message = '订单已经退款';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //联拓富收银支付
            if (9999 < $type && $type < 10999) {
                //读取配置
                $config = new LtfConfigController();
                $ltf_merchant = $config->ltf_merchant($store_id, $store_pid);
                if (!$ltf_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '商户号不存在';
                    return $this->return_data($re_data);
                }

                $obj = new \App\Api\Controllers\Ltf\PayController();
                $data = [];
                $data['out_trade_no'] = $order->out_trade_no;
                $data['request_url'] = $obj->order_query_url; //请求地址
                $data['merchant_no'] = $ltf_merchant->merchantCode;
                $data['appId'] = $ltf_merchant->appId; //
                $data['key'] = $ltf_merchant->md_key; //
                $return = $obj->order_query($data);
                //支付成功
                if ($return["status"] == 1) {
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        $trade_no = $return['data']['outTransactionId'];
                        $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['payTime']));
                        $buyer_pay_amount = $return['data']['receiptAmount'];
                        $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                        Cache::forget($c_o);


                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];

                        $this->update_day_order($insert_data, $out_trade_no);


                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '10000', //返佣来源
                            'source_desc' => '联拓覆', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'fee_amount' => $order->fee_amount,
                            'no_push' => '1', //不推送
                            'no_fuwu' => '1', //不服务消息
                            'no_print' => '1', //不打印
                            //'no_v' => '1', //不小盒子播报

                        ];


                        PaySuccessAction::action($data);

                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $order->out_trade_no; //$query['transaction_id'];
                    $re_data['time_end'] = date('YmdHis', strtotime($return['data']['payTime']));
                } //等待付款
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '等待用户付款';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //订单退款
                elseif ($return["status"] == 4) {
                    $message = '订单已经退款';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //随行付支付
            if (12999 < $type && $type < 13999) {
                $config = new VbillConfigController();
                $vbill_config = $config->vbill_config($config_id);
                if (!$vbill_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付配置不存在请检查配置'
                    ]);
                }

                $vbill_merchant = $config->vbill_merchant($store_id, $store_pid);
                if (!$vbill_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Vbill\PayController();
                $data['notify_url'] = url('/api/vbill/pay_notify_url'); //回调地址
                $data['request_url'] = $obj->order_query_url; //请求地址
                $data['mno'] = $vbill_merchant->mno;
                $data['privateKey'] = $vbill_config->privateKey; //
                $data['sxfpublic'] = $vbill_config->sxfpublic; //
                $data['orgId'] = $vbill_config->orgId; //
                $data['out_trade_no'] = $order->out_trade_no;
                $return = $obj->order_query($data);
                //支付成功
                if ($return["status"] == 1) {
                    $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['respData']['tranTime']));
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        $trade_no = $return['data']['respData']['sxfUuid'];
                        $buyer_pay_amount = $return['data']['respData']['oriTranAmt'];
                        $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                        $settleAmt = isset($return['data']['respData']['settleAmt']) ? $return['data']['respData']['settleAmt'] : 0; //商家入账金额,说明：包含手续费、预充值、平台补贴（优惠），不含免充值代金券（商家补贴）

                        Cache::forget($c_o);
                        $fee_amount = round(($order->rate * $buyer_pay_amount) / 100, 2);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'fee_amount' => $fee_amount
                        ];
                        if ($settleAmt) $insert_data['receipt_amount'] = $settleAmt;
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '13000', //返佣来源
                            'source_desc' => '随行付', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'fee_amount' => $fee_amount,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $trade_no; //$query['transaction_id'];
                    $re_data['time_end'] = date('YmdHis', strtotime($return['data']['respData']['tranTime']));
                } //等待付款
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '等待用户付款';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //订单退款
                elseif ($return["status"] == 4) {
                    $message = '订单已经退款';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //随行付A支付
            if (18999 < $type && $type < 19999) {
                $config = new VbillConfigController();
                $vbill_config = $config->vbilla_config($config_id);
                if (!$vbill_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付配置不存在请检查配置'
                    ]);
                }

                $vbill_merchant = $config->vbilla_merchant($store_id, $store_pid);
                if (!$vbill_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Vbill\PayController();
                $data['notify_url'] = url('/api/vbill/pay_notify_a_url'); //回调地址
                $data['request_url'] = $obj->order_query_url; //请求地址
                $data['mno'] = $vbill_merchant->mno;
                $data['privateKey'] = $vbill_config->privateKey; //
                $data['sxfpublic'] = $vbill_config->sxfpublic; //
                $data['orgId'] = $vbill_config->orgId; //
                $data['out_trade_no'] = $order->out_trade_no;
                $return = $obj->order_query($data);
                //支付成功
                if ($return["status"] == 1) {
                    $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['respData']['tranTime']));
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        $trade_no = $return['data']['respData']['sxfUuid'];
                        $buyer_pay_amount = $return['data']['respData']['oriTranAmt'];
                        $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                        $settleAmt = isset($return['data']['respData']['settleAmt']) ? $return['data']['respData']['settleAmt'] : 0; //商家入账金额,说明：包含手续费、预充值、平台补贴（优惠），不含免充值代金券（商家补贴）

                        Cache::forget($c_o);

                        $fee_amount = round(($order->rate * $buyer_pay_amount) / 100, 2);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'fee_amount' => $fee_amount
                        ];
                        if ($settleAmt) $insert_data['receipt_amount'] = $settleAmt;
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '19000', //返佣来源
                            'source_desc' => '随行付A', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'fee_amount' => $fee_amount,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : ""
                        ];
                        PaySuccessAction::action($data);
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $trade_no; //$query['transaction_id'];
                    $re_data['time_end'] = date('YmdHis', strtotime($return['data']['respData']['tranTime']));
                } //等待付款
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '等待用户付款';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);

                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);

                } //订单退款
                elseif ($return["status"] == 4) {

                    $message = '订单已经退款';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);

                } //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //哆啦宝支付
            if (14999 < $type && $type < 15999) {
                $manager = new ManageController();
                $dlbconfig = $manager->pay_config($config_id);
                if (!$dlbconfig) {
                    return json_encode([
                        'status' => 2,
                        'message' => '哆啦宝配置配置不存在请检查配置'
                    ]);
                }

                $dlb_merchant = $manager->dlb_merchant($store_id, $store_pid);
                if (!$dlb_merchant && !empty($dlb_merchant->mch_num) && !empty($dlb_merchant->shop_num)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '哆啦宝配置商户未补充商户编号等信息!'
                    ]);
                }

                $query_data = [
                    'agentNum' => $dlbconfig->agent_num,
                    'customerNum' => $dlb_merchant->mch_num,
                    'shopNum' => $dlb_merchant->shop_num,
                    'requestNum' => $order->out_trade_no,
                    'secretKey' => $dlbconfig->secret_key,
                    'accessKey' => $dlbconfig->access_key,
                ];
                $return = $manager->query_bill($query_data);
                if ($return['status'] == 1) {
                    $query_result = $return['data'];
                    if ($query_result['status'] == "SUCCESS") {
                        try {
                            $pay_time = $query_result['completeTime'];
                            $trade_no = $query_result['orderNum'];
                            $buyer_pay_amount = $query_result['orderAmount'];
                            $buyer_id = isset($query_result['openId']) ? $query_result['openId'] : "";

                            //改变数据库状态
                            if ($order->pay_status != 1) {
                                Cache::forget($c_o);

                                $insert_data = [
                                    'status' => '1',
                                    'pay_status' => 1,
                                    'pay_status_desc' => '支付成功',
                                    'buyer_id' => $buyer_id,
                                    'trade_no' => $trade_no,
                                    'pay_time' => $pay_time,
                                    'buyer_pay_amount' => $buyer_pay_amount,
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);

                                //支付成功后的动作
                                $data = [
                                    'ways_type' => $order->ways_type,
                                    'ways_type_desc' => $order->ways_type_desc,
                                    'source_type' => '15000', //返佣来源
                                    'source_desc' => '哆啦宝', //返佣来源说明
                                    'company' => $order->company, //
                                    'fee_amount' => $order->fee_amount, //
                                    'total_amount' => $order->total_amount,
                                    'out_trade_no' => $order->out_trade_no,
                                    'rate' => $order->rate,
                                    'merchant_id' => $order->merchant_id,
                                    'store_id' => $order->store_id,
                                    'user_id' => $order->user_id,
                                    'config_id' => $config_id,
                                    'store_name' => $order->store_name,
                                    'ways_source' => $order->ways_source,
                                    'pay_time' => $pay_time,
                                    'device_id' => isset($order->device_id) ? $order->device_id : "",

                                ];


                                PaySuccessAction::action($data);

                            }
                            $re_data['result_code'] = 'SUCCESS';
                            $re_data['result_msg'] = '支付成功';
                            $re_data['outTradeNo'] = $order->qwx_no;
                            $re_data['out_transaction_id'] = $trade_no; //$query['transaction_id'];
                            $re_data['time_end'] = date('YmdHis', strtotime($pay_time));
                        } catch (\Exception $e) {
                            $message = '订单查询失败' . $e->getLine() . $e->getMessage();
                            $re_data['result_code'] = 'FALL';
                            $re_data['result_msg'] = $message;
                            $re_data['outTradeNo'] = $order->qwx_no;
                        }
                    } elseif ($query_result['status'] == "INIT") {
                        $re_data['result_code'] = 'USERPAYING';
                        $re_data['result_msg'] = '等待用户付款';
                        $re_data['outTradeNo'] = $order->qwx_no;
                    } elseif ($query_result['status'] == "CANCEL") {
                        $message = '订单支付失败';
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $message;
                        $re_data['outTradeNo'] = $order->qwx_no;
                    } elseif ($query_result['status'] == "REFUND") {
                        $message = '订单已经退款';
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $message;
                        $re_data['outTradeNo'] = $order->qwx_no;
                        return $this->return_data($re_data);
                    } elseif ($query_result['status'] == "REFUNDING") {
                        $message = '订单退款中';
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $message;
                        $re_data['outTradeNo'] = $order->qwx_no;
                    } elseif ($query_result['status'] == "FUNDFAIL") {
                        $message = '订单退款失败';
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $message;
                        $re_data['outTradeNo'] = $order->qwx_no;
                    } else {
                        $message = '订单退款失败';
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $message;
                        $re_data['outTradeNo'] = $order->qwx_no;
                    }
                } //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                }
            }

            //传化收银支付
            if (11999 < $type && $type < 12999) {
                //读取配置
                $config = new TfConfigController();
                $h_merchant = $config->tf_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '传化商户号不存在';
                    return $this->return_data($re_data);
                }

                $h_config = $config->tf_config($config_id, $h_merchant->qd);
                if (!$h_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '传化配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $obj = new \App\Api\Controllers\Tfpay\PayController();
                $data['mch_id'] = $h_config->mch_id; //
                $data['pub_key'] = $h_config->pub_key; //
                $data['pri_key'] = $h_config->pri_key; //
                $data['sub_mch_id'] = $h_merchant->sub_mch_id; //
                $data['out_trade_no'] = $out_trade_no; //
                $data['date'] = date('Y-m-d', time()); //
                $return = $obj->order_query($data);
                //支付成功
                if ($return["status"] == 1) {
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        $trade_no = $return['data']['channel_no'];
                        $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['paid_at']));
                        $buyer_pay_amount = $return['data']['total_fee'];
                        $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '12000', //返佣来源
                            'source_desc' => 'TF', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'fee_amount' => $order->fee_amount,
                            'pay_time' => $pay_time,
                            'no_push' => '1', //不推送
                            'no_fuwu' => '1', //不服务消息
                            'no_print' => '1', //不打印
                            //'no_v' => '1', //不小盒子播报
                        ];
                        PaySuccessAction::action($data);
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $trade_no; //$query['transaction_id'];
                    $re_data['time_end'] = date('YmdHis', strtotime($return['data']['paid_at']));
                } //等待付款
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '等待用户付款';
                    $re_data['outTradeNo'] = $order->qwx_no;

                    return $this->return_data($re_data);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //订单退款
                elseif ($return["status"] == 4) {
                    $message = '订单已经退款';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;

                    return $this->return_data($re_data);
                } //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;

                    return $this->return_data($re_data);
                }
            }

            //工行收银支付
            if (19999 < $type && $type < 20999) {
                $config = new LianfuConfigController();
                $h_merchant = $config->lianfu_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\lianfu\PayController();
                $data['apikey'] = $h_merchant->apikey; //
                $data['signkey'] = $h_merchant->signkey; //
                $data['out_trade_no'] = $out_trade_no; //
                $return = $obj->order_query($data);
                Log::info('微收银-工行-订单查询-结果');
                Log::info($return);
                //支付成功
                if ($return["status"] == 1) {
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        $trade_no = isset($return['data']['tp_order_id']) ? $return['data']['tp_order_id'] : $out_trade_no;
                        $pay_time = date('Y-m-d H:i:s', time());
                        $buyer_pay_amount = $order->total_amount;
                        $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '20200', //返佣来源
                            'source_desc' => 'lianfu', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'fee_amount' => $order->fee_amount,
                            'pay_time' => $pay_time,
                            'no_push' => '1', //不推送
                            'no_fuwu' => '1', //不服务消息
                            'no_print' => '1', //不打印
                            //'no_v' => '1', //不小盒子播报
                        ];
                        PaySuccessAction::action($data);
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $trade_no; //$query['transaction_id'];
                    $re_data['time_end'] = date('YmdHis', time());
                } //等待付款
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '等待用户付款';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }//订单退款
                elseif ($return["status"] == 4) {
                    $message = '订单已经退款';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //海科融通 交易查询
            if (21999 < $type && $type < 22999) {
                //读取配置
                $config = new HkrtConfigController();
                $hkrt_config = $config->hkrt_config($config_id);
                if (!$hkrt_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '海科融通配置不存在请检查配置'
                    ]);
                }

                $hkrt_merchant = $config->hkrt_merchant($store_id, $store_pid);
                if (!$hkrt_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '海科融通商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Hkrt\PayController();
                $hkrt_data = [
                    'trade_no' => $order->trade_no, //trade_no和out_trade_no和channel_trade_no必传其中一个，三个都传则以trade_no为准，推荐使用trade_no
                    'out_trade_no' => $order->out_trade_no, //服务商的交易订单编号
                    'channel_trade_no' => '', //凭证条码订单号
                    'access_id' => $hkrt_config->access_id,
                    'access_key' => $hkrt_config->access_key
                ];
//                Log::info('海科融通-微收银-交易查询-入参');
//                Log::info($hkrt_data);
                $return = $obj->order_query($hkrt_data); //0-系统错误 1-交易成功；2-交易失败；3-交易进行中；4-交易超时
//                Log::info('海科融通-微收银-交易查询-结果');
//                Log::info($return);

                //支付成功
                if ($return["status"] == 1) {
                    $pay_time = $return['data']['trade_end_time'];
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        $trade_no = $return['data']['trade_no'];
                        $buyer_pay_amount = $return['data']['total_amount'];

                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '22000', //返佣来源
                            'source_desc' => '海科融通', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'fee_amount' => $order->fee_amount,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $trade_no; //
                    $re_data['time_end'] = date('YmdHis', strtotime($pay_time));
                } //交易进行中
                elseif ($return["status"] == 3) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '等待用户付款';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //交易失败
                elseif ($return["status"] == 2) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }//交易超时
                elseif ($return["status"] == 4) {
                    $message = '交易超时';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } else {
                    //其他情况
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //易生支付 交易查询
            if (20999 < $type && $type < 21999) {
                $config = new EasyPayConfigController();
                $easypay_config = $config->easypay_config($config_id);
                if (!$easypay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生支付配置不存在请检查配置'
                    ]);
                }

                $easypay_merchant = $config->easypay_merchant($store_id, $store_pid);
                if (!$easypay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生支付商户号不存在'
                    ]);
                }

                $obj = new EasypayPayController();
                $easypay_data = [
                    'channel_id' => $easypay_config->channel_id, //渠道编号
                    'mer_id' => $easypay_merchant->term_mercode, //终端商户编号
                    'device_id' => $easypay_merchant->term_termcode, //终端编号
                    'out_trade_no' => $order->out_trade_no, //原交易流水
                ];
//                Log::info('易生支付-微收银-交易查询');
//                Log::info($easypay_data);
                $return = $obj->order_query($easypay_data); //-1 系统错误 0-其他 1-成功 2-下单失败 3-订单未支付
//                Log::info('易生支付-微收银-交易查询-结果');
//                Log::info($return);
                //1.0
//                $pay_time = isset($return['data']['wxtimeend']) ? date('Y-m-d H:i:s', strtotime($return['data']['wxtimeend'])): ''; //支付完成时间，格式为 yyyyMMddhhmmss
//                $trade_no = $return['data']['wtorderid'] ?? ''; //系统订单号
//                $buyer_pay_amount = isset($return['data']['payamt']) ? ($return['data']['payamt'] / 100): ''; //实付金额，单位分
//                $buyer_id = $return['data']['wxopenid'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
                //2.0
                $pay_time = (isset($return['data']['timeEnd']) && !empty($return['data']['timeEnd'])) ? date('Y-m-d H:i:m', strtotime($return['data']['timeEnd'])) : date('Y-m-d H:i:m', time()); //支付完成时间，格式为 yyyyMMddhhmmss，如 2009年12月27日9点10分10秒表示为 20091227091010
                $trade_no = $return['data']['outTrace'] ?? ''; //系统订单号
                $buyer_pay_amount = isset($return['data']['payerAmt']) ? ($return['data']['payerAmt'] / 100) : ''; //实付金额，单位分
                $buyer_id = $return['data']['payerId'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)

                //支付成功
                if ($return['status'] == '1') {
                    //改变数据库状态
                    if ($order->pay_status != '1') {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => '1',
                            'pay_status_desc' => '支付成功',
                            'buyer_id' => $buyer_id,
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'receipt_amount' => $buyer_pay_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '21000', //返佣来源
                            'source_desc' => '易生支付', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'fee_amount' => $order->fee_amount,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $trade_no;
                    $re_data['time_end'] = date('YmdHis', strtotime($pay_time));
                } //交易进行中
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '订单未支付';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //交易失败
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
                //交易超时
//                elseif ($return["status"] == 4) {
//                    $message = '交易超时';
//                    $re_data['result_code'] = 'FALL';
//                    $re_data['result_msg'] = $message;
//                    $re_data['outTradeNo'] = $order->qwx_no;
//                    return $this->return_data($re_data);
//                }
                //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //邮政收银支付
            if (26000 < $type && $type < 26999) {
                $config = new LianfuyoupayConfigController();
                $h_merchant = $config->lianfu_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\lianfuyouzheng\PayController();
                $data['apikey'] = $h_merchant->apikey; //
                $data['signkey'] = $h_merchant->signkey; //
                $data['out_trade_no'] = $out_trade_no; //
                $data['pos_sn'] = $h_merchant->pos_sn;
                $data['goods_name'] = '商品'; //

                $return = $obj->order_query($data);
                //支付成功
                if ($return["status"] == 1) {
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        $trade_no = isset($return['data']['tp_order_id']) ? $return['data']['tp_order_id'] : $out_trade_no;
                        $pay_time = date('Y-m-d H:i:s', time());
                        $buyer_pay_amount = $order->total_amount;
                        $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '26000', //返佣来源
                            'source_desc' => 'lianfuyoupay', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'fee_amount' => $order->fee_amount,
                            'pay_time' => $pay_time,
                            'no_push' => '1', //不推送
                            'no_fuwu' => '1', //不服务消息
                            'no_print' => '1', //不打印
                            //'no_v' => '1', //不小盒子播报
                        ];
                        PaySuccessAction::action($data);
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $trade_no; //$query['transaction_id'];
                    $re_data['time_end'] = date('YmdHis', time());
                } //等待付款
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '等待用户付款';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }//订单退款
                elseif ($return["status"] == 4) {
                    $message = '订单已经退款';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //葫芦天下
            if (22999 < $type && $type < 23999) {
                $manager = new \App\Api\Controllers\Hltx\ManageController();
                $hltx_merchant = $manager->pay_merchant($store_id, $store_pid);
                $qd = $hltx_merchant->qd;

                $hltx_config = $manager->pay_config($config_id, $qd);
                if (!$hltx_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => 'HL支付配置不存在请检查配置'
                    ]);
                }

                $manager->init($hltx_config);

                switch ($type) {
                    case 23001:
                        $pay_channel = 'ALI';
                        break;
                    case 23002:
                        $pay_channel = 'WX';
                        break;
                    case 23004:
                        $pay_channel = 'UPAY';
                        break;
                    default:
                        $pay_channel = 'ALI';
                        break;
                }

                if (!$hltx_merchant || empty($hltx_merchant->mer_no)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户未成功开通HL通道!'
                    ]);
                }

                $hltx_data = [
                    'merNo' => $hltx_merchant->mer_no,
                    'orderNo' => $order->out_trade_no
                ];
                $return = $manager->query_order($hltx_data);
                if ($return['status'] == 1) {
                    $return = $return['data'];
                    if ($return['tradeStatus'] == "S") {
                        if ($return['subTradeStatus'] == 'COMPLETE') {
                            try {
                                $pay_time = $return['payCompleteTime'];
                                $trade_no = $return['transIndex'];
                                $buyer_pay_amount = $return['amount'] / 100;
                                //改变数据库状态
                                if ($order->pay_status != 1) {
                                    Cache::forget($c_o);

                                    $insert_data = [
                                        'status' => '1',
                                        'pay_status' => 1,
                                        'pay_status_desc' => '支付成功',
                                        'buyer_logon_id' => '',
                                        'trade_no' => $trade_no,
                                        'pay_time' => $pay_time,
                                        'buyer_pay_amount' => $buyer_pay_amount,
                                    ];
                                    $this->update_day_order($insert_data, $out_trade_no);

                                    //支付成功后的动作
                                    $data = [
                                        'ways_type' => $order->ways_type,
                                        'ways_type_desc' => $order->ways_type_desc,
                                        'source_type' => '23000', //返佣来源
                                        'source_desc' => 'HL', //返佣来源说明
                                        'company' => 'hltx', //返佣来源说明
                                        'total_amount' => $order->total_amount,
                                        'out_trade_no' => $order->out_trade_no,
                                        'rate' => $order->rate,
                                        'merchant_id' => $order->merchant_id,
                                        'store_id' => $order->store_id,
                                        'user_id' => $order->user_id,
                                        'config_id' => $config_id,
                                        'store_name' => $order->store_name,
                                        'ways_source' => $order->ways_source,
                                        'pay_time' => $pay_time,
                                        'device_id' => isset($order->device_id) ? $order->device_id : "",
                                    ];
                                    PaySuccessAction::action($data);
                                }
                                $re_data['result_code'] = 'SUCCESS';
                                $re_data['result_msg'] = '支付成功';
                                $re_data['outTradeNo'] = $order->qwx_no;
                                $re_data['out_transaction_id'] = $trade_no; //$query['transaction_id'];
                                $re_data['time_end'] = date('YmdHis', strtotime($pay_time));
                            } catch (\Exception $e) {
                                $message = '订单查询失败' . $e->getLine() . $e->getMessage();
                                $re_data['result_code'] = 'FALL';
                                $re_data['result_msg'] = $message;
                                $re_data['outTradeNo'] = $order->qwx_no;
                            }
                        } elseif (in_array($return['subTradeStatus'], ['FAILED', 'SYS_FAILED', 'CANCEL', 'CLOSE'])) {
                            //其他情况
                            $message = '订单支付失败';
                            $re_data['result_code'] = 'FALL';
                            $re_data['result_msg'] = $message;
                            $re_data['outTradeNo'] = $order->qwx_no;
                        } else {
                            $re_data['result_code'] = 'USERPAYING';
                            $re_data['result_msg'] = '等待用户付款';
                            $re_data['outTradeNo'] = $order->qwx_no;
                        }
                    } elseif ($return['tradeStatus'] == "E") {
                        //其他情况
                        $message = '订单支付失败';
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $message;
                        $re_data['outTradeNo'] = $order->qwx_no;
                    } else {
                        $re_data['result_code'] = 'USERPAYING';
                        $re_data['result_msg'] = '等待用户付款';
                        $re_data['outTradeNo'] = $order->qwx_no;
                    }
                } else {
                    //其他情况
                    $message = '订单支付失败' . $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                }
            }

            //联动优势 交易查询
            if (4999 < $type && $type < 5999) {
                $config = new LinkageConfigController();
                $linkage_config = $config->linkage_config($config_id);
                if (!$linkage_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '联动优势支付配置不存在请检查配置'
                    ]);
                }

                $linkage_merchant = $config->linkage_merchant($store_id, $store_pid);
                if (!$linkage_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '联动优势商户号不存在'
                    ]);
                }

                $obj = new LinkagePayController();
                $linkage_data = [
                    'acqSpId' => $linkage_config->mch_id, //代理商编号
                    'privateKey' => $linkage_config->privateKey, //
                    'publicKey' => $linkage_config->publicKey, //
                    'acqMerId' => $linkage_merchant->acqMerId, //商户号
                    'trade_no' => $order->trade_no, //联动优势的订单号，建议优先使用
                    'out_trade_no' => $out_trade_no //商户订单号
                ];
//                Log::info('联动优势-微收银-交易查询');
//                Log::info($linkage_data);
                $return = $obj->order_query($linkage_data); //-1系统错误；0-其他；1-交易成功；2-验签失败；3-转入退款；4-交易结果未明；5-已关闭；6-已撤销(付款码支付)；7-明确支付失败
//                Log::info($return);

                if ($return['status'] == '1') { //支付成功
                    $pay_time = $return['data']['payTime'] ? date('Y-m-d H:i:s', strtotime(substr($return['data']['platDate'], 0, 4) . $return['data']['payTime'])) : ''; //交易时间 格式：MMDDhhmmss
                    $trade_no = $return['data']['transactionId']; //联动优势的流水号
                    $buyer_pay_amount = $return['data']['txnAmt'] ? ($return['data']['txnAmt'] / 100) : ''; //订单金额 (打印小票使用)
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                    $paySeq = $return['data']['paySeq'] ?? ''; //支付流水号（条形码），成功返回
                    $depBankSeq = $return['data']['depBankSeq'] ?? ''; //第三方流水号（微信/支付宝/银联的交易流水号）

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'auth_code' => $paySeq,
                            'other_no' => $depBankSeq
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '5000', //返佣来源
                            'source_desc' => '联动优势', //返佣来源说明
                            'total_amount' => $buyer_pay_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $trade_no;
                    $re_data['time_end'] = date('YmdHis', strtotime($pay_time));
                }
                //交易进行中
//                elseif ($return["status"] == 3) {
//                    $re_data['result_code'] = 'USERPAYING';
//                    $re_data['result_msg'] = '等待用户付款';
//                    $re_data['outTradeNo'] = $order->qwx_no;
//                    return $this->return_data($re_data);
//                }
                //交易失败
                elseif ($return["status"] == 2) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
                //交易超时
//                elseif ($return["status"] == 4) {
//                    $message = '交易超时';
//                    $re_data['result_code'] = 'FALL';
//                    $re_data['result_msg'] = $message;
//                    $re_data['outTradeNo'] = $order->qwx_no;
//                    return $this->return_data($re_data);
//                }
                //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //威富通 交易查询
            if (26999 < $type && $type < 27999) {
                $config = new WftPayConfigController();
                $wftpay_config = $config->wftpay_config($config_id);
                if (!$wftpay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '威富通支付配置不存在请检查配置'
                    ]);
                }

                $wftpay_merchant = $config->wftpay_merchant($store_id, $store_pid);
                if (!$wftpay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '威富通商户号不存在'
                    ]);
                }

                $obj = new WftPayPayController();
                $wftpay_data = [
                    'mch_id' => $wftpay_merchant->mch_id,
                    'out_trade_no' => $out_trade_no,
                    'private_rsa_key' => $wftpay_config->private_rsa_key,
                    'public_rsa_key' => $wftpay_config->public_rsa_key
                ];
//                Log::info('威富通-微收银-交易查询');
//                Log::info($wftpay_data);
                $return = $obj->order_query($wftpay_data); //0-系统错误 1-成功 2-失败 3-转入退款 4-未支付 5-已关闭 6-已冲正 7-已撤销 8-用户支付中
//                Log::info($return);

                if ($return['status'] == 1) {
                    $pay_time = $return['data']['time_end'] ? date('Y-m-d H:i:s', strtotime($return['data']['time_end'])) : ''; //支付完成时间，格式为yyyyMMddHHmmss，如2009年12月27日9点10分10秒表示为20091227091010
                    $trade_no = $return['data']['transaction_id']; //平台交易号
                    $buyer_pay_amount = $return['data']['total_fee'] ? ($return['data']['total_fee'] / 100) : ''; //总金额，以分为单位，不允许包含任何字、符号
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                    $depBankSeq = $return['data']['out_transaction_id'] ?? ''; //第三方交易号

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'other_no' => $depBankSeq
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '27000', //返佣来源
                            'source_desc' => '威富通', //返佣来源说明
                            'total_amount' => $buyer_pay_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $trade_no;
                    $re_data['time_end'] = date('YmdHis', strtotime($pay_time));
                } //交易进行中
                elseif ($return["status"] == 8) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '用户支付中';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //交易失败
                elseif ($return["status"] == 2) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
                //交易超时
//                elseif ($return["status"] == 4) {
//                    $message = '未支付';
//                    $re_data['result_code'] = 'FALL';
//                    $re_data['result_msg'] = $message;
//                    $re_data['outTradeNo'] = $order->qwx_no;
//                    return $this->return_data($re_data);
//                }
                //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //汇旺财 交易查询
            if (27999 < $type && $type < 28999) {
                $config = new HwcPayConfigController();
                $hwcpay_config = $config->hwcpay_config($config_id);
                if (!$hwcpay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '汇旺财支付配置不存在请检查配置'
                    ]);
                }

                $hwcpay_merchant = $config->hwcpay_merchant($store_id, $store_pid);
                if (!$hwcpay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '汇旺财商户号不存在'
                    ]);
                }

                $obj = new HwcPayPayController();
                $hwcpay_data = [
                    'mch_id' => $hwcpay_merchant->mch_id,
                    'out_trade_no' => $out_trade_no,
                    'private_rsa_key' => $hwcpay_config->private_rsa_key,
                    'public_rsa_key' => $hwcpay_config->public_rsa_key
                ];
//                Log::info('汇旺财-微收银-交易查询');
//                Log::info($hwcpay_data);
                $return = $obj->order_query($hwcpay_data); //0-系统错误 1-成功 2-失败 3-转入退款 4-未支付 5-已关闭 6-已冲正 7-已撤销 8-用户支付中
//                Log::info($return);

                if ($return['status'] == 1) {
                    $pay_time = $return['data']['time_end'] ? date('Y-m-d H:i:s', strtotime($return['data']['time_end'])) : ''; //支付完成时间，格式为yyyyMMddHHmmss，如2009年12月27日9点10分10秒表示为20091227091010
                    $trade_no = isset($return['data']['third_order_no']) ? $return['data']['third_order_no'] : $return['data']['transaction_id']; //平台交易号
                    $buyer_pay_amount = $return['data']['total_fee'] ? ($return['data']['total_fee'] / 100) : ''; //总金额，以分为单位，不允许包含任何字、符号
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                    $depBankSeq = $return['data']['out_transaction_id'] ?? ''; //第三方交易号

                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'other_no' => $depBankSeq
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '28000', //返佣来源
                            'source_desc' => '汇旺财', //返佣来源说明
                            'total_amount' => $buyer_pay_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $trade_no;
                    $re_data['time_end'] = date('YmdHis', strtotime($pay_time));
                } //交易进行中
                elseif ($return["status"] == 8) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '用户支付中';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //交易失败
                elseif ($return["status"] == 2) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
                //交易超时
//                elseif ($return["status"] == 4) {
//                    $message = '未支付';
//                    $re_data['result_code'] = 'FALL';
//                    $re_data['result_msg'] = $message;
//                    $re_data['outTradeNo'] = $order->qwx_no;
//                    return $this->return_data($re_data);
//                }
                //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //钱方 交易查询
            if (23999 < $type && $type < 24999) {
                $qfPayConfigControllerObj = new QfPayConfigController();
                $qfpay_config = $qfPayConfigControllerObj->qfpay_config($config_id);
                if (!$qfpay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '钱方支付配置不存在请检查配置'
                    ]);
                }

                $qfpay_merchant = $qfPayConfigControllerObj->qfpay_merchant($store_id, $store_pid);
                if (!$qfpay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '钱方商户号不存在'
                    ]);
                }

                $obj = new QfPayController();
                $qfpay_data = [
                    'mchid' => $qfpay_merchant->mchid,  //商户号
                    'key' => $qfpay_config->key,  //加签key
                    'code' => $qfpay_config->code,  //钱方唯一标识
                    'out_trade_no' => $out_trade_no,  //否,外部订单号查询,开发者平台订单号
                    'syssn' => $order->trade_no  //否,钱方订单号查询,多个以英文逗号区分开
                ];
                Log::info('钱方-微收银-交易查询-入参');
                Log::info($qfpay_data);
                $return = $obj->query($qfpay_data); //1-成功 2-请求下单成功 3-交易中
                Log::info('钱方-微收银-交易查询-结果');
                Log::info($return);
                if ($return['status'] == 1) {
                    $trade_no = isset($return['data']['syssn']) ? $return['data']['syssn'] : ''; //钱方订单号
                    $txamt = isset($return['data']['txamt']) ? $return['data']['txamt'] : 0; //订单支付金额，单位分
                    $txamt = number_format($txamt / 100, 2, '.', '');
                    $txdtm = isset($return['data']['txdtm']) ? $return['data']['txdtm'] : ''; //请求交易时间 格式为：YYYY-MM-DD HH:MM:SS
                    $sysdtm = isset($return['data']['sysdtm']) ? $return['data']['sysdtm'] : $txdtm; //系统交易时间
                    $other_no = isset($return['data']['out_trade_no']) ? $return['data']['out_trade_no'] : ''; //外部订单号，开发者平台订单号
                    $pay_type = isset($return['data']['pay_type']) ? $return['data']['pay_type'] : ''; //支付类型,多个以英文逗号区分开,支付宝扫码:800101；支付宝反扫:800108；支付宝服务窗：800107；微信扫码:800201；微信刷卡:800208；微信公众号支付:800207
                    $order_type = isset($return['data']['order_type']) ? $return['data']['order_type'] : ''; //订单类型:支付的订单：payment；退款的订单：refund；关闭的订单：close
                    $cancel = isset($return['data']['cancel']) ? $return['data']['cancel'] : ''; //撤销/退款标记 正常交易：0；已撤销：2；已退货：3
                    $respcd = isset($return['data']['respcd']) ? $return['data']['respcd'] : ''; //支付结果返回码 0000表示交易支付成功；1143、1145表示交易中，需继续查询交易结果； 其他返回码表示失败
                    $errmsg = isset($return['data']['errmsg']) ? $return['data']['errmsg'] : ''; //支付结果描述
                    $cardtp = isset($return['data']['cardtp']) ? $return['data']['cardtp'] : ''; //卡类型 未识别卡=0,借记卡=1,信用卡(贷记卡)=2,准贷记卡=3,储值卡= 4,第三方帐号=5

                    //改变数据库状态
                    if ($order->pay_status != 1 && $trade_no) {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'buyer_pay_amount' => $txamt,
                            'receipt_amount' => $txamt
                        ];
                        if ($sysdtm) $insert_data['pay_time'] = $sysdtm;
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '24000', //返佣来源
                            'source_desc' => '钱方', //返佣来源说明
                            'total_amount' => $txamt,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $sysdtm,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $trade_no;
                    $re_data['time_end'] = date('YmdHis', strtotime($sysdtm));
                } elseif ($return["status"] == 3) { //交易进行中
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '用户支付中';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } else { //其他情况
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //邮驿付支付 交易查询
            if (29000 < $type && $type < 29010) {
                //读取配置
                $config = new PostPayConfigController();
                $post_config = $config->post_pay_config($config_id);
                if (!$post_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '邮驿付支付配置不存在请检查配置'
                    ]);
                }

                $post_merchant = $config->post_pay_merchant($store_id, $store_pid);
                if (!$post_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '邮驿付支付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\PostPay\PayController();
                $post_data = [
                    'out_trade_no' => $order->out_trade_no,//原交易流水 //渠道编号
                    'custLogin' => $store->people_phone, //商户手机号
                    'custId' => $post_merchant->cust_id, //终端编号
                    'agetId' => $post_config->org_id
                ];
                $return = $obj->order_query($post_data); //-1 系统错误 0-其他 1-成功 2-下单失败 3-订单未支付

                //支付成功
                if ($return['status'] == '1') {

                    $pay_time = $return['data']['orderTime'] ? date('Y-m-d H:i:s', strtotime($return['data']['orderTime'])) : ''; //支付完成时间，如2009年12月27日9点10分10秒表示为20091227091010
                    $trade_no = $return['data']['orderNo']; //系统订单号
                    $buyer_logon_id = $return['data']['openId'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
                    $buyer_pay_amount = $return['data']['txamt'] ? ($return['data']['txamt'] / 100) : ''; //实付金额，单位分
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'receipt_amount' => $buyer_pay_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '29000', //返佣来源
                            'source_desc' => '邮驿付支付', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                        $re_data['result_code'] = 'SUCCESS';
                        $re_data['result_msg'] = '支付成功';
                        $re_data['outTradeNo'] = $order->qwx_no;
                        $re_data['out_transaction_id'] = $trade_no;
                        $re_data['time_end'] = date('YmdHis', strtotime($pay_time));
                    }
                } //交易进行中
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '订单未支付';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //交易失败
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
                //交易超时
//                elseif ($return["status"] == 4) {
//                    $message = '交易超时';
//                    $re_data['result_code'] = 'FALL';
//                    $re_data['result_msg'] = $message;
//                    $re_data['outTradeNo'] = $order->qwx_no;
//                    return $this->return_data($re_data);
//                }
                //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //建设银行 交易查询
            if (31000 < $type && $type < 31010) {
                //设置缓存记录订单查询次数
                $qrytime = Cache::get($out_trade_no);
                if (!$qrytime) {
                    Cache::add($out_trade_no, 1, 10);
                    $qrytime = 1;
                }
                //读取配置
                $config = new CcBankPayConfigController();
                $ccBank_merchant = $config->ccBank_pay_merchant($store_id, $store_pid);
                if (!$ccBank_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '建设商户号不存在'
                    ]);
                }

                if ($type == 31001) {
                    $qrCodeType = '3';
                }
                if ($type == 31002) {
                    $qrCodeType = '2';
                }
                if ($type == 31003) {
                    $qrCodeType = '1';
                }
                $obj = new \App\Api\Controllers\CcBankPay\PayController();
                $ccBank_data = [
                    'cust_id' => $ccBank_merchant->cust_id,
                    'pos_id' => $ccBank_merchant->pos_id,
                    'branch_id' => $ccBank_merchant->branch_id,
                    'out_trade_no' => $out_trade_no,
                    'qr_code_type' => $qrCodeType,
                    'public_key' => $ccBank_merchant->public_key,
                    'termno1' => $ccBank_merchant->termno1,
                    'termno2' => $ccBank_merchant->termno2,
                    'qrytime' => $qrytime
                ];
                $return = $obj->order_query($ccBank_data);

                //支付成功
                if ($return['status'] == '1') {

                    $pay_time = date('Y-m-d H:i:m', time());
                    //$trade_no = $return['data']['TRACEID']; //系统订单号
                    //$buyer_logon_id = $return['data']['OPENID'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
                    $buyer_pay_amount = $return['data']['AMOUNT'] ? ($return['data']['AMOUNT']) : ''; //实付金额
                    $receipt_amount = $return['data']['AMOUNT'] ? ($return['data']['AMOUNT']) : ''; //实付金额
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'receipt_amount' => $receipt_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '31000', //返佣来源
                            'source_desc' => '建设银行', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                            'no_user_money' => '1',//不计算返佣
                        ];

                        PaySuccessAction::action($data);

                        $store = Store::where('store_id', $store_id)->select('zero_rate_type')->first();
                        if ($store->zero_rate_type == '0') {//0收费  1不收费
                            $TransactionDeductionController = new TransactionDeductionController();
                            $deduction = [
                                'merchant_id' => $order->merchant_id,
                                'total_amount' => $order->total_amount,
                                'user_id' => $order->user_id,
                                'order_id' => $order->out_trade_no,
                                'company' => $order->company,
                                'rate' => $order->rate, //结算费率
                            ];
                            $TransactionDeductionController->deduction($deduction);//交易扣款
                        }
                        $re_data['result_code'] = 'SUCCESS';
                        $re_data['result_msg'] = '支付成功';
                        $re_data['outTradeNo'] = $order->qwx_no;
                        $re_data['out_transaction_id'] = $order->trade_no;
                        $re_data['time_end'] = date('YmdHis', strtotime($pay_time));
                        Cache::forget($out_trade_no);//交易成功删除缓存
                    }
                } //交易进行中
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '订单未支付';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $value = Cache::pull($out_trade_no);//先取出查询次数在删除
                    $num = $value + 1;
                    Cache::add($out_trade_no, $num, 10);//重新存入此订单查询次数
                    return $this->return_data($re_data);
                } //交易失败
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    Cache::forget($out_trade_no);
                    return $this->return_data($re_data);
                }
                //交易超时
//                elseif ($return["status"] == 4) {
//                    $message = '交易超时';
//                    $re_data['result_code'] = 'FALL';
//                    $re_data['result_msg'] = $message;
//                    $re_data['outTradeNo'] = $order->qwx_no;
//                    return $this->return_data($re_data);
//                }
                //其他情况
                else {
                    Cache::forget($out_trade_no);
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //河南畅立收
            if ($store->source == '02') {
                //易生数科支付 交易查询
                if (32000 < $type && $type < 32010) {
                    $config = new EasySkPayConfigController();
                    $easyskpay_config = $config->easyskpay_config($config_id);
                    if (!$easyskpay_config) {
                        return json_encode([
                            'status' => '2',
                            'message' => '易生数科支付配置不存在请检查配置'
                        ]);
                    }

                    $easyskpay_merchant = $config->easyskpay_merchant($store_id, $store_pid);
                    if (!$easyskpay_merchant) {
                        return json_encode([
                            'status' => '2',
                            'message' => '易生数科支付商户号不存在'
                        ]);
                    }

                    $obj = new \App\Api\Controllers\EasySkPay\PayController();
                    $easyskpay_data = [
                        'org_id' => $easyskpay_config->org_id, //渠道编号
                        'mer_id' => $easyskpay_merchant->mer_id, //终端商戶编号
                        'orig_request_no' => $order->out_trade_no //原交易流水
                    ];
//                Log::info('易生数科支付-微收银-交易查询');
//                Log::info($easypay_data);
                    $return = $obj->order_query($easyskpay_data); //-1 系统错误 0-其他 1-成功 2-下单失败 3-订单未支付
//                Log::info('易生支付-微收银-交易查询-结果');
//                Log::info($return);
                    $trade_no = $return['data']['tradeNo']; //系统订单号
                    $pay_time = (isset($return['data']['bizData']['payTime']) && !empty($return['data']['bizData']['payTime'])) ? date('Y-m-d H:i:m', strtotime($return['data']['bizData']['payTime'])) : date('Y-m-d H:i:m', time());
                    $buyer_pay_amount = isset($return['data']['bizData']['amount']) ? ($return['data']['bizData']['amount'] / 100) : ''; //实付金额，单位分
                    $buyer_id = $return['data']['bizData']['channelOpenId'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)

                    //支付成功
                    if ($return['status'] == '1') {
                        //改变数据库状态
                        if ($order->pay_status != '1') {
                            Cache::forget($c_o);

                            $insert_data = [
                                'status' => '1',
                                'pay_status' => '1',
                                'pay_status_desc' => '支付成功',
                                'buyer_id' => $buyer_id,
                                'buyer_logon_id' => '',
                                'trade_no' => $trade_no,
                                'pay_time' => $pay_time,
                                'buyer_pay_amount' => $buyer_pay_amount,
                                'receipt_amount' => $buyer_pay_amount
                            ];
                            $this->update_day_order($insert_data, $out_trade_no);

                            //支付成功后的动作
                            $data = [
                                'ways_type' => $order->ways_type,
                                'company' => $order->company,
                                'ways_type_desc' => $order->ways_type_desc,
                                'source_type' => '32000', //返佣来源
                                'source_desc' => '易生数科支付', //返佣来源说明
                                'total_amount' => $order->total_amount,
                                'out_trade_no' => $order->out_trade_no,
                                'rate' => $order->rate,
                                'merchant_id' => $order->merchant_id,
                                'store_id' => $order->store_id,
                                'user_id' => $order->user_id,
                                'config_id' => $config_id,
                                'store_name' => $order->store_name,
                                'ways_source' => $order->ways_source,
                                'fee_amount' => $order->fee_amount,
                                'pay_time' => $pay_time,
                                'device_id' => isset($order->device_id) ? $order->device_id : "",
                            ];
                            PaySuccessAction::action($data);
                        }

                        $re_data['result_code'] = 'SUCCESS';
                        $re_data['result_msg'] = '支付成功';
                        $re_data['outTradeNo'] = $order->qwx_no;
                        $re_data['out_transaction_id'] = $trade_no;
                        $re_data['time_end'] = date('YmdHis', strtotime($pay_time));
                    } //交易进行中
                    elseif ($return["status"] == 2) {
                        $re_data['result_code'] = 'USERPAYING';
                        $re_data['result_msg'] = '订单未支付';
                        $re_data['outTradeNo'] = $order->qwx_no;
                        return $this->return_data($re_data);
                    } //交易失败
                    elseif ($return["status"] == 3) {
                        //其他情况
                        $message = '订单支付失败';
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $message;
                        $re_data['outTradeNo'] = $order->qwx_no;
                        return $this->return_data($re_data);
                    } //其他情况
                    else {
                        $message = $return['message'];
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $message;
                        $re_data['outTradeNo'] = $order->qwx_no;
                        return $this->return_data($re_data);
                    }
                }
            }

            //通联支付 交易查询
            if (33000 < $type && $type < 33010) {
                $config = new AllinPayConfigController();
                $allin_config = $config->allin_pay_config($config_id);
                if (!$allin_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '通联支付配置不存在请检查配置'
                    ]);
                }

                $allin_merchant = $config->allin_pay_merchant($store_id, $store_pid);
                if (!$allin_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生数科支付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\AllinPay\PayController();
                $allin_data = [
                    'ageId' => $allin_config->org_id, //渠道编号
                    'cusId' => $allin_merchant->cus_id, //终端商戶编号
                    'out_trade_no' => $order->out_trade_no, //原交易流水
                    'trxid' => $order->trade_no,
                    'appid' => $allin_merchant->appid
                ];
                $return = $obj->order_query($allin_data); //-1 系统错误 0-其他 1-成功 2-下单失败 3-订单未支付
                $trade_no = $return['data']['trxid']; //系统订单号
                $pay_time = isset($return['data']['fintime']) ? ($return['data']['fintime']) : date('Y-m-d H:i:m', time());
                $buyer_pay_amount = isset($return['data']['trxamt']) ? ($return['data']['amount'] / 100) : ''; //实付金额，单位分
                $buyer_id = $return['data']['acct'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)

                //支付成功
                if ($return['status'] == '1') {
                    //改变数据库状态
                    if ($order->pay_status != '1') {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => '1',
                            'pay_status_desc' => '支付成功',
                            'buyer_id' => $buyer_id,
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'receipt_amount' => $buyer_pay_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '33000', //返佣来源
                            'source_desc' => '通联支付', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'fee_amount' => $order->fee_amount,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $trade_no;
                    $re_data['time_end'] = date('YmdHis', strtotime($pay_time));
                } //交易进行中
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '订单未支付';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //交易失败
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //富友支付 交易查询
            if (11000 < $type && $type < 11010) {
                $config = new FuiouConfigController();
                $fuiou_config = $config->fuiou_config($config_id);
                if (!$fuiou_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '富友支付配置不存在请检查配置'
                    ]);
                }

                $fuiou_merchant = $config->fuiou_merchant($store_id, $store_pid);
                if (!$fuiou_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '富友支付商户号不存在'
                    ]);
                }
                $obj = new \App\Api\Controllers\Fuiou\PayController();
                $order_data = [
                    'ins_cd' => $fuiou_config->ins_cd,//机构号
                    'mchnt_cd' => $fuiou_merchant->mchnt_cd,
                    'mchnt_order_no' => $out_trade_no
                ];
                if ($order->ways_source == 'alipay') {
                    $order_data['order_type'] = 'ALIPAY';
                }
                if ($order->ways_source == 'weixin') {
                    $order_data['order_type'] = 'WECHAT';
                }
                if ($order->ways_source == 'unionpay') {
                    $order_data['order_type'] = 'UNIONPAY';
                }
                $return = $obj->order_query($order_data); //-1 系统错误 0-其他 1-成功 2-下单失败 3-订单未支付

                $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['reserved_txn_fin_ts']));
                $trade_no = $return['data']['transaction_id'];
                $buyer_id = $return['data']['buyer_id'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
                $buyer_pay_amount = $return['data']['order_amt'] ? ($return['data']['order_amt'] / 100) : ''; //实付金额，单位分
                $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                //支付成功
                if ($return['status'] == '1') {
                    //改变数据库状态
                    if ($order->pay_status != '1') {
                        Cache::forget($c_o);

                        $insert_data = [
                            'status' => '1',
                            'pay_status' => '1',
                            'pay_status_desc' => '支付成功',
                            'buyer_id' => $buyer_id,
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'receipt_amount' => $buyer_pay_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '11000', //返佣来源
                            'source_desc' => '富友支付', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'fee_amount' => $order->fee_amount,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '支付成功';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    $re_data['out_transaction_id'] = $trade_no;
                    $re_data['time_end'] = date('YmdHis', strtotime($pay_time));
                } //交易进行中
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '订单未支付';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //交易失败
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }

            //银盛支付 交易查询
            if (14000 < $type && $type < 14010) {
                //读取配置
                $config = new YinshengConfigController();
                $yinsheng_config = $config->yinsheng_config($config_id);
                if (!$yinsheng_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '银盛配置不存在请检查配置'
                    ]);
                }

                $yinsheng_merchant = $config->yinsheng_merchant($store_id, $store_pid);
                if (!$yinsheng_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '银盛商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\YinSheng\PayController();
                $ys_data = [
                    'out_trade_no' => $order->out_trade_no,
                    'partner_id' => $yinsheng_config->partner_id, //商户号
                    'trade_no' => $order->trade_no,
                    'shop_date' => $order->created_at,
                    'seller_id' => $yinsheng_merchant->mer_code
                ];
                $return = $obj->order_query($ys_data); //-1 系统错误 0-其他 1-成功 2-下单失败 3-订单未支付

                //支付成功
                if ($return['status'] == '1') {

                    $pay_time = date('Y-m-d H:i:s', time()); //支付完成时间，如2009年12月27日9点10分10秒表示为20091227091010
                    $trade_no = $return['data']['trade_no']; //系统订单号
                    $buyer_logon_id = $return['data']['openid'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
                    $buyer_pay_amount = $return['data']['total_amount'] ? ($return['data']['total_amount']) : ''; //实付金额，单位分
                    $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                    //改变数据库状态
                    if ($order->pay_status != 1) {
                        $insert_data = [
                            'status' => '1',
                            'pay_status' => 1,
                            'pay_status_desc' => '支付成功',
                            'buyer_logon_id' => '',
                            'trade_no' => $trade_no,
                            'pay_time' => $pay_time,
                            'buyer_pay_amount' => $buyer_pay_amount,
                            'receipt_amount' => $buyer_pay_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        //支付成功后的动作
                        $data = [
                            'ways_type' => $order->ways_type,
                            'company' => $order->company,
                            'ways_type_desc' => $order->ways_type_desc,
                            'source_type' => '14000', //返佣来源
                            'source_desc' => '银盛支付', //返佣来源说明
                            'total_amount' => $order->total_amount,
                            'out_trade_no' => $order->out_trade_no,
                            'rate' => $order->rate,
                            'fee_amount' => $order->fee_amount,
                            'merchant_id' => $order->merchant_id,
                            'store_id' => $order->store_id,
                            'user_id' => $order->user_id,
                            'config_id' => $config_id,
                            'store_name' => $order->store_name,
                            'ways_source' => $order->ways_source,
                            'pay_time' => $pay_time,
                            'device_id' => isset($order->device_id) ? $order->device_id : "",
                        ];
                        PaySuccessAction::action($data);
                        $re_data['result_code'] = 'SUCCESS';
                        $re_data['result_msg'] = '支付成功';
                        $re_data['outTradeNo'] = $order->qwx_no;
                        $re_data['out_transaction_id'] = $trade_no;
                        $re_data['time_end'] = date('YmdHis', strtotime($pay_time));
                    }
                } //交易进行中
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '订单未支付';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //交易失败
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
                //交易超时
//                elseif ($return["status"] == 4) {
//                    $message = '交易超时';
//                    $re_data['result_code'] = 'FALL';
//                    $re_data['result_msg'] = $message;
//                    $re_data['outTradeNo'] = $order->qwx_no;
//                    return $this->return_data($re_data);
//                }
                //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                }
            }



            return $this->return_data($re_data);
        } catch (\Exception $exception) {
            Log::info('qwx 查询报错');
            Log::info($exception->getMessage() . '|' . $exception->getFile() . '|' . $exception->getLine());
            $err = [
                'return_code' => 'FALL',
                'return_msg' => $exception->getMessage() . '|' . $exception->getLine()
            ];

            return $this->return_data($err);
        }
    }


    //退款接口
    public function refund(Request $request)
    {
        try {
            //获取请求参数
            $data = $request->getContent();
            //Log::info('微收银退款入参：'.$data);
            //{"merchantId":"202016164042044276","refundNo":"2020011001488","refundFee":1,"channel":"wx_barcode_pay","Version":"2.0.0","outTradeNo":"dm4905460344","TimeStamp":"2020-01-10T11:49:54Z","SignatureNonce":null,"sign":"0eff012457d6a21b0d74c6675c495175"}
            $data = json_decode($data, true);

            //验证签名
            $check = $this->check_md5($data);
            if ($check['return_code'] == 'FALL') {
                return $this->return_data($check);
            }

            $where = [];
            if (isset($data['tradeNo'])) {
                $where[] = ['out_trade_no', '=', $data['tradeNo']];
            }

            if (isset($data['out_transaction_id'])) {
                // $where[] = ['trade_no', '=', $data['out_transaction_id']];
                $where[] = ['out_trade_no', '=', $data['out_transaction_id']];
            }

            if (isset($data['outTradeNo'])) {
                $where[] = ['qwx_no', '=', $data['outTradeNo']];
            }

            $day = date('Ymd', time());
            $table = 'orders_' . $day;
            if (Schema::hasTable($table)) {
                $order = DB::table($table)->where('store_id', $data['merchantId'])
                    ->where($where)
                    ->first();
            } else {
                $order = Order::where('store_id', $data['merchantId'])
                    ->where($where)
                    ->first();
            }

            //如果订单号为空或者不存在
            if (!$order) {
                $re_data['result_code'] = 'FALL';
                $re_data['result_msg'] = '订单号不存在';
                return $this->return_data($re_data);
            } else {
                //判断有没有退款全部完成
                if ($order->refund_amount == $order->total_amount) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '此订单号已经全部退款';
                    return $this->return_data($re_data);
                }
            }

            $OutRefundNo = $data['refundNo']; //$order->out_trade_no . '123';
            $out_transaction_id = $order->trade_no;
            $out_trade_no = $order->out_trade_no;
            $refundFee_fen = $data['refundFee']; //退款金额单位,分

            $already_refund_amount = ($order->refund_amount * 100); //已退款金额,分
            if (($refundFee_fen + $already_refund_amount) > ($order->total_amount * 100)) {
                $re_data['result_code'] = 'FALL';
                $re_data['result_msg'] = '退款金额大于总金额';
                return $this->return_data($re_data);
            }
            $refundFee_yuan = number_format($refundFee_fen / 100, 2, '.', '');
            $refund_amount = $refundFee_yuan;

            $total_amount = $order->total_amount; //交易总额
            $rate = $order->rate; //订单但是费率

            //退全款
            if (($refund_amount * 100 - $order->total_amount * 100) == 0) {
                Log::info('微收银退款,退全款');
                $refund_amount = !empty($order->receipt_amount * 100) ? $order->receipt_amount : $order->total_amount - $order->mdiscount_amount;
            } else { //部分退款
                Log::info('微收银退款,部分退款');
                $newReceiptAmount = !empty($order->receipt_amount * 100) ? $order->receipt_amount : $order->total_amount - $order->mdiscount_amount;
                if (($refund_amount * 100 - $newReceiptAmount * 100) > 0) {
                    Log::info('微收银退款,退款 > 实付');
                    return json_encode([
                        'status' => '2',
                        'message' => '退款金额大于实付金额'
                    ]);
                }
            }

            $new_refund_amount = max($order->refund_amount + $refund_amount, 0); //总退款

            $new_fee_amount = round((max($order->total_amount - $order->mdiscount_amount - $new_refund_amount, 0)) * ($rate / 100), 2); //退款后的手续费

            //暂时只支持退全款
//            if ($refundFee_yuan != $order->total_amount) {
//                $re_data['result_code'] = 'FALL';
//                $re_data['result_msg'] = '只支持退全额';
//                return $this->return_data($re_data);
//            }

            $time_end = '';
            $re_data = [
                'return_code' => 'SUCCESS', //SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                'return_msg' => null,
                'result_code' => '',
                'result_msg' => '',
                'tradeNo' => $order->out_trade_no,
                'outTradeNo' => $order->qwx_no, //微收银唯一订单号
                'out_transaction_id' => $out_transaction_id, //微信，支付宝支付凭证上的退款条形码
                'refundNo' => '', //退款订单号
                'refundFee' => '', // $order->total_amount * 100, //交易金额，以分
                'channel' => $data['channel'] //支付类型 微信主扫：wx_barcode_pay 支付宝：ali_barcode_pay
            ];

            $type = $order->ways_type;
            $store = Store::where('store_id', $data['merchantId'])
                ->select('config_id', 'merchant_id', 'pid', 'people_phone', 'source')
                ->first();
            $config_id = $store->config_id;
            $store_id = $data['merchantId'];
            $store_pid = $store->pid;

            //支付宝官方
            if (999 < $type && $type < 1999) {
                //配置
                $isvconfig = new AlipayIsvConfigController();
                $config_type = '01';
                $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);

                //获取token
                $storeInfo = $isvconfig->alipay_auth_info($store_id, $store_pid);

                $aop = new AopClient();
                $aop->apiVersion = "2.0";
                $aop->appId = $config->app_id;
                $aop->rsaPrivateKey = $config->rsa_private_key;
                $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                $aop->signType = "RSA2"; //升级算法
                $aop->gatewayUrl = $config->alipay_gateway;
                $aop->format = "json";
                $aop->charset = "GBK";
                $aop->version = "2.0";
                $aop->method = "alipay.trade.refund";

                $requests = new AlipayTradeRefundRequest();
                $data_req_ali = "{" .
                    "\"out_trade_no\":\"" . $order->out_trade_no . "\"," .
                    "\"refund_amount\":\"" . $refund_amount . "\"," .
                    "\"out_request_no\":\"" . $OutRefundNo . "\"," .
                    "\"refund_reason\":\"正常退款\"" .
                    "}";
                $requests->setBizContent($data_req_ali);
                $result = $aop->execute($requests, null, $storeInfo->app_auth_token);
                $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
                $resultCode = $result->$responseNode->code;

                //退款成功
                if (!empty($resultCode) && $resultCode == 10000) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => 3,
                        'status' => 3,
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $data['merchantId'],
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } //退款失败
                else {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $result->$responseNode->sub_msg;
                }
            }

            //直付通
            if (16000 < $type && $type < 16999) {
                $config_type = '03';
                //配置
                $isvconfig = new AlipayIsvConfigController();
                $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);
                $aop = new AopClient();
                $aop->apiVersion = "2.0";
                $aop->appId = $config->app_id;
                $aop->rsaPrivateKey = $config->rsa_private_key;
                $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                $aop->signType = "RSA2"; //升级算法
                $aop->gatewayUrl = $config->alipay_gateway;
                $aop->format = "json";
                $aop->charset = "GBK";
                $aop->version = "2.0";
                $aop->method = "alipay.trade.refund";

                $requests = new AlipayTradeRefundRequest();
                $data_re = array(
                    'out_trade_no' => $out_trade_no,
                    'refund_amount' => $refund_amount,
                    'out_request_no' => $OutRefundNo,
                    'refund_reason' => '正常退款'
                );

                $a = array('a', '_', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
                $b = str_ireplace($a, "", $out_trade_no);
                $day = substr($b, 0, 8);
                $table = 'settle_orders_' . $day;
                if (Schema::hasTable($table)) {
                    $settle_orders = DB::table($table)->where('out_trade_no', $out_trade_no)
                        ->where('store_id', $store_id)
                        ->first();
                    if ($settle_orders && $settle_orders->order_settle_amount > 0) {
                        $data_re['refund_royalty_parameters'] = array(
                            0 => array(
                                'trans_out' => $settle_orders->trans_out,
                                'amount' => $settle_orders->order_settle_amount,
                                'desc' => '退款分账'
                            )
                        );
                    }
                }

                $data_re = json_encode($data_re);
                $requests->setBizContent($data_re);
                $result = $aop->execute($requests, null, '');
                $responseNode = str_replace(".", "_", $requests->getApiMethodName()) . "_response";
                $resultCode = $result->$responseNode->code;

                //退款成功
                if (!empty($resultCode) && $resultCode == 10000) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => 3,
                        'status' => 3,
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $data['merchantId'],
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } else {
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $result->$responseNode->sub_msg;
                }
            }

            //微信官方扫码退款
            if (1999 < $type && $type < 2999) {
                $config = new WeixinConfigController();
                $options = $config->weixin_config($config_id);
                $weixin_store = $config->weixin_merchant($store_id, $store_pid);
                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;

                $config = [
                    'app_id' => $options['app_id'],
                    'mch_id' => $options['payment']['merchant_id'],
                    'key' => $options['payment']['key'],
                    'cert_path' => $options['payment']['cert_path'], // XXX: 绝对路径！！！！
                    'key_path' => $options['payment']['key_path'],     // XXX: 绝对路径！！！！
                    'sub_mch_id' => $wx_sub_merchant_id
                ];

                $payment = Factory::payment($config);
                // 参数分别为：商户订单号、商户退款单号、订单金额、退款金额、其他参数
                $refund = $payment->refund->byOutTradeNumber($order->out_trade_no, $OutRefundNo, $order->total_amount * 100, $refund_amount * 100);
                //Log::info('微信官方扫码退款');
                //Log::info($refund);
                if ($refund['return_code'] == "SUCCESS") {
                    //退款成功
                    if ($refund['result_code'] == "SUCCESS") {
                        $insert_data = [
                            'status' => 6,
                            'pay_status' => 6,
                            'pay_status_desc' => '已退款',
                            'refund_amount' => $new_refund_amount,
                            'fee_amount' => $new_fee_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        RefundOrder::create([
                            'pay_status' => 6,
                            'status' => 6,
                            'type' => $type,
                            'ways_source' => $order->ways_source,
                            'status_desc' => '退款',
                            'refund_amount' => $refund_amount, //退款金额
                            'refund_no' => $OutRefundNo, //退款单号
                            'store_id' => $data['merchantId'],
                            'merchant_id' => (int)$store->merchant_id,
                            'out_trade_no' => $order->out_trade_no,
                            'trade_no' => $order->trade_no
                        ]);

                        $re_data['result_code'] = 'SUCCESS';
                        $re_data['result_msg'] = '退款成功';
                        $re_data['refundNo'] = $OutRefundNo;
                        $re_data['refundFee'] = $refund_amount * 100;
                    } else {
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $refund['result_msg'];
                    }
                } else {
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $refund['return_msg'];
                }
            }

            //微信官方a 扫码退款
            if (3999 < $type && $type < 4999) {
                $config = new WeixinConfigController();
                $options = $config->weixina_config($config_id);
                $weixin_store = $config->weixina_merchant($store_id, $store_pid);
                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;

                $config = [
                    'app_id' => $options['app_id'],
                    'mch_id' => $options['payment']['merchant_id'],
                    'key' => $options['payment']['key'],
                    'cert_path' => $options['payment']['cert_path'], // XXX: 绝对路径！！！！
                    'key_path' => $options['payment']['key_path'],     // XXX: 绝对路径！！！！
                    'sub_mch_id' => $wx_sub_merchant_id
                ];
                $payment = Factory::payment($config);

                // 参数分别为：商户订单号、商户退款单号、订单金额、退款金额、其他参数
                $refund = $payment->refund->byOutTradeNumber($order->out_trade_no, $OutRefundNo, $order->total_amount * 100, $refund_amount * 100);
                //Log::info('微信官方扫码退款');
                //Log::info($refund);
                if ($refund['return_code'] == "SUCCESS") {
                    //退款成功
                    if ($refund['result_code'] == "SUCCESS") {
                        $insert_data = [
                            'status' => 6,
                            'pay_status' => 6,
                            'pay_status_desc' => '已退款',
                            'refund_amount' => $new_refund_amount,
                            'fee_amount' => $new_fee_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        RefundOrder::create([
                            'pay_status' => 6,
                            'status' => 6,
                            'type' => $type,
                            'ways_source' => $order->ways_source,
                            'status_desc' => '退款',
                            'refund_amount' => $refund_amount, //退款金额
                            'refund_no' => $OutRefundNo, //退款单号
                            'store_id' => $data['merchantId'],
                            'merchant_id' => (int)$store->merchant_id,
                            'out_trade_no' => $order->out_trade_no,
                            'trade_no' => $order->trade_no
                        ]);

                        $re_data['result_code'] = 'SUCCESS';
                        $re_data['result_msg'] = '退款成功';
                        $re_data['refundNo'] = $OutRefundNo;
                        $re_data['refundFee'] = $refund_amount * 100;
                    } else {
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $refund['result_msg'];
                    }
                } else {
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $refund['return_msg'];
                }
            }

            //京东收银支付退款
            if (5999 < $type && $type < 6999) {
                //读取配置
                $config = new JdConfigController();
                $jd_config = $config->jd_config($config_id);
                if (!$jd_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '京东配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $jd_merchant = $config->jd_merchant($store_id, $store_pid);
                if (!$jd_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '京东商户号不存在';
                    return $this->return_data($re_data);
                }

                $obj = new \App\Api\Controllers\Jd\PayController();
                $data = [];
                $data['out_trade_no'] = $order->out_trade_no;
                $data['request_url'] = $obj->refund_url; //请求地址;
                $data['notifyUrl'] = url('/api/jd/refund_url'); //通知地址;
                $data['merchant_no'] = $jd_merchant->merchant_no;
                $data['md_key'] = $jd_merchant->md_key; //
                $data['des_key'] = $jd_merchant->des_key; //
                $data['systemId'] = $jd_config->systemId; //
                $data['outRefundNo'] = $OutRefundNo;
                $data['amount'] = $refund_amount;
                $return = $obj->refund($data);
                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => 6,
                        'status' => 6,
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } //其他情况
                else {
                    $message = $return['message'];
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //联拓富收银支付退款
            if (9999 < $type && $type < 10999) {
                //读取配置
                $config = new LtfConfigController();

                $ltf_merchant = $config->ltf_merchant($store_id, $store_pid);
                if (!$ltf_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '商户号不存在';
                    return $this->return_data($re_data);
                }
                $obj = new \App\Api\Controllers\Ltf\PayController();
                $data = [];
                $data['out_trade_no'] = $order->out_trade_no;
                $data['request_url'] = $obj->refund_url; //请求地址;
                $data['notifyUrl'] = url('/api/jd/refund_url'); //通知地址;
                $data['merchant_no'] = $ltf_merchant->merchantCode;
                $data['appId'] = $ltf_merchant->appId; //
                $data['key'] = $ltf_merchant->md_key; //
                $data['outRefundNo'] = $OutRefundNo;
                $data['amount'] = $refund_amount;

                $return = $obj->refund($data);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => 6,
                        'status' => 6,
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $data['merchant_no'],
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } else {
                    //其他情况
                    $message = $return['message'];
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //快钱收银支付退款
            if (2999 < $type && $type < 3999) {
                //读取配置
                $config = new MyBankConfigController();

                $mybank_merchant = $config->mybank_merchant($store_id, $store_pid);
                if (!$mybank_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '快钱商户号不存在';
                    return $this->return_data($re_data);
                }
                $wx_AppId = $mybank_merchant->wx_AppId;
                $MyBankConfig = $config->MyBankConfig($config_id, $wx_AppId);
                if (!$MyBankConfig) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '快钱配置不存在请检查配置';
                    return $this->return_data($re_data);
                }
                $obj = new TradePayController();
                $MerchantId = $mybank_merchant->MerchantId;
                $RefundAmount = $refund_amount;
                $return = $obj->mybankrefund($MerchantId, $order->out_trade_no, $OutRefundNo, $RefundAmount, $config_id);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => 3,
                        'status' => 3,
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $order->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } else {
                    //其他情况
                    $message = $return['message'];
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //和融通收银支付退款
            if (8999 < $type && $type < 9999) {
                //读取配置
                $config = new HConfigController();
                $h_config = $config->h_config($config_id);
                if (!$h_config) {

                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '和融通配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $h_merchant = $config->h_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '和融通商户号不存在';
                    return $this->return_data($re_data);
                }
                $obj = new \App\Api\Controllers\Huiyuanbao\PayController();
                $data = [];
                $data['trade_no'] = $order->trade_no;
                $data['request_url'] = $obj->refund_url; //请求地址;
                $data['notifyUrl'] = url('/api/jd/refund_url'); //通知地址;
                $data['mid'] = $h_merchant->h_mid;
                $data['md_key'] = $h_config->md_key; //
                $data['orgNo'] = $h_merchant->orgNo; //
                $data['outRefundNo'] = $OutRefundNo;
                $data['amount'] = $refund_amount;

                $return = $obj->refund($data);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => 3,
                        'status' => 3,
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } else {
                    //其他情况
                    $message = $return['message'];
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //随行付支付退款
            if (12999 < $type && $type < 13999) {
                //读取配置
                $config = new VbillConfigController();
                $vbill_config = $config->vbill_config($config_id);
                if (!$vbill_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付配置不存在请检查配置'
                    ]);
                }

                $vbill_merchant = $config->vbill_merchant($store_id, $store_pid);
                if (!$vbill_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Vbill\PayController();
                $data['notify_url'] = url('/api/vbill/refund_notify_url'); //回调地址
                $data['request_url'] = $obj->refund_url; //请求地址;
                $data['mno'] = $vbill_merchant->mno;
                $data['privateKey'] = $vbill_config->privateKey; //
                $data['sxfpublic'] = $vbill_config->sxfpublic; //
                $data['orgId'] = $vbill_config->orgId; //
                $data['out_trade_no'] = $order->out_trade_no;
                $data['refund_amount'] = $refund_amount;
//                Log::info('随行付微收银退款请求参数');
//                Log::info($data);
                $return = $obj->refund($data);
//                Log::info('随行付微收银退款请求结果');
//                Log::info($return);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => 6,
                        'status' => 6,
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $data['merchantId'],
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } elseif (($return['status'] == 0) && ($return['data']['code'] == '0000')) {
                    //TODO: 退款状态未知，先让微收银显示成功
//                    Log::info('随行付退款状态未知，先让微收银显示成功');
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } else {
                    //其他情况
                    $message = $return['message'];
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //随行付a支付退款
            if (18999 < $type && $type < 19999) {
                //读取配置
                $config = new VbillConfigController();
                $vbill_config = $config->vbilla_config($config_id);
                if (!$vbill_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付A配置不存在请检查配置'
                    ]);
                }

                $vbill_merchant = $config->vbilla_merchant($store_id, $store_pid);
                if (!$vbill_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '随行付A商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Vbill\PayController();
                $data['notify_url'] = url('/api/vbill/refund_notify_url'); //回调地址
                $data['request_url'] = $obj->refund_url; //请求地址;
                $data['mno'] = $vbill_merchant->mno;
                $data['privateKey'] = $vbill_config->privateKey; //
                $data['sxfpublic'] = $vbill_config->sxfpublic; //
                $data['orgId'] = $vbill_config->orgId; //
                $data['out_trade_no'] = $order->out_trade_no;
                $data['refund_amount'] = $refund_amount;
//                Log::info('随行付A微收银退款请求参数');
//                Log::info($data);
                $return = $obj->refund($data);
//                Log::info('随行付A微收银退款请求结果');
//                Log::info($return);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => 6,
                        'status' => 6,
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $data['merchantId'],
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } elseif (($return['status'] == 0) && ($return['data']['code'] == '0000')) {
                    //TODO: 退款状态未知，先让微收银显示成功
//                    Log::info('随行付A退款状态未知，先让微收银显示成功');
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } else {
                    //其他情况
                    $message = $return['message'];
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //传化收银支付退款
            if (11999 < $type && $type < 12999) {
                //读取配置
                $config = new TfConfigController();
                $h_merchant = $config->tf_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '传化商户号不存在';
                    return $this->return_data($re_data);
                }

                $h_config = $config->tf_config($config_id, $h_merchant->qd);
                if (!$h_config) {

                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '传化配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $OutRefundNo = time();
                $data = [];
                $obj = new \App\Api\Controllers\Tfpay\PayController();
                $data['mch_id'] = $h_config->mch_id; //
                $data['pub_key'] = $h_config->pub_key; //
                $data['pri_key'] = $h_config->pri_key; //
                $data['sub_mch_id'] = $h_merchant->sub_mch_id; //
                $data['out_trade_no'] = $out_trade_no; //
                $data['date'] = date('Y-m-d', time()); //
                $data['refund_trade_no'] = $OutRefundNo; //
                $data['refund_fee'] = $refund_amount; //

                $return = $obj->refund($data);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => 3,
                        'status' => 3,
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } else {
                    //其他情况
                    $message = $return['message'];
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //新大陆
            if (7999 < $type && $type < 8999) {
                //读取配置
                $config = new NewLandConfigController();
                $new_land_config = $config->new_land_config($config_id);
                if (!$new_land_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '新大陆通道配置不存在';
                    return $this->return_data($re_data);
                }

                $new_land_merchant = $config->new_land_merchant($store_id, $store_pid);
                if (!$new_land_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '商户新大陆通道未开通';
                    return $this->return_data($re_data);
                }
                $request_data = [
                    'out_trade_no' => $out_trade_no,
                    'trade_no' => $order->trade_no,
                    'key' => $new_land_merchant->nl_key,
                    'org_no' => $new_land_config->org_no,
                    'merc_id' => $new_land_merchant->nl_mercId,
                    'trm_no' => $new_land_merchant->trmNo,
                    'op_sys' => '3',
                    'opr_id' => $store->merchant_id,
                    'trm_typ' => 'T',
                    'txnAmt' => $refund_amount * 100,
                ];
                $obj = new PayController();
                $return = $obj->refund($request_data);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => 3,
                        'status' => 3,
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } else {
                    //其他情况
                    $message = $return['message'];
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //哆啦宝
            if (14999 < $type && $type < 15999) {
                $OutRefundNo = "dlbscan" . date('YmdHis') . str_pad(rand(0, 9999), 4, 0);
                $manager = new ManageController();
                $dlb_config = $manager->pay_config($config_id);

                if (!$dlb_config) {

                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '哆啦宝配置配置不存在请检查配置';
                    return $this->return_data($re_data);
                }
                $dlb_merchant = $manager->dlb_merchant($store_id, $store_pid);
                if (!$dlb_merchant && !empty($dlb_merchant->mch_num) && !empty($dlb_merchant->shop_num)) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '哆啦宝配置商户未补充商户编号等信息';
                    return $this->return_data($re_data);
                }
                $refund_data = [
                    "accessKey" => $dlb_config->access_key,
                    "secretKey" => $dlb_config->secret_key,
                    "agentNum" => $dlb_config->agent_num,
                    "customerNum" => $dlb_merchant->mch_num,
                    "shopNum" => $dlb_merchant->shop_num,
                    "requestNum" => $out_trade_no,
                    "refundRequestNum" => $OutRefundNo,
                    'refundPartAmount' => $refund_amount
                ];
                $return = $manager->pay_refund($refund_data);

                if ($return['status'] == 1) {
                    //退款请求成功
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => 3,
                        'status' => 3,
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } else {
                    $message = $return['message'];
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //汇付-收银通道 退款
            if (17999 < $type && $type < 18999) {
                //读取配置
                $config = new HuiPayConfigController();
                $hui_pay_config = $config->hui_pay_config($config_id);
                if (!$hui_pay_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '汇付配置不存在请检查配置'
                    ]);
                }

                $hui_pay_merchant = $config->hui_pay_merchant($store_id, $store_pid);
                if (!$hui_pay_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '汇付商户不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\HuiPay\PayController();
                $data['notify_url'] = url('/api/huipay/return_pay_notify_url'); //回调地址
                $data['request_url'] = $obj->user_refund; //请求地址
                $data['mer_cust_id'] = $hui_pay_config->mer_cust_id; //商户客户号
                $data['user_cust_id'] = $hui_pay_merchant->user_cust_id; //用户客户号
                $data['private_key'] = $hui_pay_config->private_key;
                $data['public_key'] = $hui_pay_config->public_key;
                $data['org_id'] = $hui_pay_config->org_id;
                $data['out_trade_no'] = $order->out_trade_no; //订单号
                $data['refund_amount'] = $refundFee_yuan; //退款金额
                $data['device_id'] = $hui_pay_merchant->device_id ?? ''; //机具id
                $data['trade_no'] = $order->trade_no; //支付订单号

                $return = $obj->refund($data); //0-系统错误 1-成功 2-退款中 3-失败
//                Log::info('微收银汇付退款请求结果' . str_replace("\\/", "/", json_encode($return, JSON_UNESCAPED_UNICODE)));

                //退款请求成功
                if ($return["status"] == '1') {
                    if (isset($return['data']) && !empty($return['data'])) {
                        $return_data = $return['data'];
                    }
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no); //更新订单状态

                    //退款成后更新退款表
                    RefundOrder::create([
                        'pay_status' => 6,
                        'status' => 6,
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $data['merchantId'],
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } elseif ($return["status"] == '2') {
                    //退款中
                    if (isset($return['data']) && !empty($return['data'])) {
                        $return_data = $return['data'];
                    }
                    $insert_data = [
                        'status' => '5',
                        'pay_status' => '5',
                        'pay_status_desc' => '退款中',
                        'fee_amount' => isset($return_data) ? $return_data['refund_fee_amt'] : '0',
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款中';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $order->total_amount * 100;
                } else {
                    //其他情况
                    $message = $return['message'];
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //工行 退款
            if (19999 < $type && $type < 20999) {
                $config = new LianfuConfigController();
                $h_merchant = $config->lianfu_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\lianfu\PayController();
                $data['apikey'] = $h_merchant->apikey; //
                $data['signkey'] = $h_merchant->signkey; //
                $data['out_trade_no'] = $out_trade_no; //
                $data['refund_no'] = time(); //
                $data['pay_amount'] = $refund_amount; //

                $return = $obj->refund($data);

                if ($return['status'] == 1) {
                    //退款请求成功
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => 3,
                        'status' => 3,
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } else {
                    $message = $return['message'];
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //海科融通 退款
            if (21999 < $type && $type < 22999) {
                //读取配置
                $config = new HkrtConfigController();
                $hkrt_config = $config->hkrt_config($config_id);
                if (!$hkrt_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '海科融通配置不存在请检查配置'
                    ]);
                }

                $hkrt_merchant = $config->hkrt_merchant($store_id, $store_pid);
                if (!$hkrt_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '海科融通商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Hkrt\PayController();
                $hkrt_return_data = [
                    'access_id' => $hkrt_config->access_id,
                    'refund_amount' => $refund_amount, //退款金额,(银联二维码只能全额退款)退款金额，以元为单位
                    'trade_no' => $order->trade_no, //SaaS平台的交易订单编号
                    'out_trade_no' => $order->out_trade_no,
                    'notify_url' => url('/api/hkrt/refund_notify_url'), //回调地址
                    'access_key' => $hkrt_config->access_key
                ];
//                Log::info('海科融通-微收银-退款');
//                Log::info($hkrt_return_data);
                $return = $obj->refund($hkrt_return_data); //0-系统错误 1-成功 2-失败 3-结果未知
//                Log::info($return);

                //退款成功
                if ($return['status'] == 1) {
                    $hkrt_refund_no = $return['data']['refund_no']; //SaaS平台的退款订单编号
                    $refund_amount = $return['data']['refunded_amount']; //已退款金额
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => 6,
                        'status' => 6,
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount,
                        'refund_no' => $hkrt_refund_no,
                        'store_id' => $data['merchantId'],
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $hkrt_refund_no;
                    $re_data['refundFee'] = $refund_amount * 100;
                } //失败
                elseif ($return['status'] == '2') {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '退款失败';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } //未知
                elseif ($return['status'] == '3') {
                    //TODO: 退款状态未知，先让微收银显示成功
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = $return['message'];
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } //其他情况
                else {
                    $message = $return['message'];
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //易生支付 退款
            if (20999 < $type && $type < 21999) {
                $config = new EasyPayConfigController();
                $easypay_config = $config->easypay_config($config_id);
                if (!$easypay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生支付配置不存在请检查配置'
                    ]);
                }

                $easypay_merchant = $config->easypay_merchant($store_id, $store_pid);
                if (!$easypay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '易生支付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\EasyPay\PayController();
                $easypay_return_data = [
                    'channel_id' => $easypay_config->channel_id, //渠道编号
                    'mer_id' => $easypay_merchant->term_mercode, //终端商戶编号
                    'term_id' => $easypay_merchant->term_termcode, //终端编号
                    'out_trade_no' => $out_trade_no, //订单号
                    'trade_no' => $order->trade_no, //系统订单号
                    'refund_amount' => $refund_amount, //退货金额,不大于原交易金额与已成功退货金额之差
                ];
                Log::info('易生支付-微收银-退款-入参');
                Log::info($easypay_return_data);
                $return = $obj->refund($easypay_return_data); //-1 系统错误 0-其他 1-成功 2-失败
                Log::info('易生支付-微收银-退款-结果');
                Log::info($return);

                //$easypay_refund_no = $return['data']['oriwtorderid']; //易生退货单号1.0
                $easypay_refund_no = $return['orgTrace']; //易生退货单号2.0

                //退款成功
                if ($return['status'] == '1') {
                    $refund_amount = $order->total_amount; //已退款金额
                    $insert_data = [
                        'status' => '6',
                        'pay_status' => '6',
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => '6',
                        'status' => '6',
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount,
                        'refund_no' => $easypay_refund_no,
                        'store_id' => $data['merchantId'],
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $easypay_refund_no;
                    $re_data['refundFee'] = $refund_amount * 100;
                } //失败
                elseif ($return['status'] == '2') {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '退款失败';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                }
                //未知
//                elseif($return['status'] == '3') {
//                    //TODO: 退款状态未知，先让微收银显示成功
//                    $re_data['result_code'] = 'SUCCESS';
//                    $re_data['result_msg'] = $return['message'];
//                    $re_data['refundNo'] = $OutRefundNo;
//                    $re_data['refundFee'] = $refund_amount * 100;
//                }
                //其他情况
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL'; //退款失败
                    $re_data['result_msg'] = $message;
                }

            }

            //邮政 退款
            if (26000 < $type && $type < 26999) {
                $config = new LianfuyoupayConfigController();
                $h_merchant = $config->lianfu_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\lianfuyouzheng\PayController();
                $data['apikey'] = $h_merchant->apikey; //
                $data['signkey'] = $h_merchant->signkey; //
                $data['pos_sn'] = $h_merchant->pos_sn; //
                $data['out_trade_no'] = $out_trade_no; //
                $data['refund_no'] = time(); //
                $data['pay_amount'] = $refund_amount; //
                $data['goods_name'] = '商品'; //

                $return = $obj->refund($data);

                if ($return['status'] == 1) {
                    //退款请求成功
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => 3,
                        'status' => 3,
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $store_id,
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } else {
                    $message = $return['message'];
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //葫芦天下
            if (22999 < $type && $type < 23999) {
                $manager = new \App\Api\Controllers\Hltx\ManageController();

                $hltx_merchant = $manager->pay_merchant($store_id, $store_pid);
                $qd = $hltx_merchant->qd;
                $hltx_config = $manager->pay_config($config_id, $qd);
                if (!$hltx_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => 'HL支付配置不存在请检查配置'
                    ]);
                }

                $manager->init($hltx_config);
                switch ($type) {
                    case 23001:
                        $pay_channel = 'ALI';
                        break;
                    case 23002:
                        $pay_channel = 'WX';
                        break;
                    case 23004:
                        $pay_channel = 'UPAY';
                        break;
                    default:
                        $pay_channel = 'ALI';
                        break;
                }

                if (!$hltx_merchant || empty($hltx_merchant->mer_no)) {
                    return json_encode([
                        'status' => 2,
                        'message' => '商户未成功开通HL通道!'
                    ]);
                }

                $OutRefundNo = "hltx" . date('YmdHis') . str_pad(rand(0, 9999), 4, 0);
                $hltx_data = [
                    'merNo' => $hltx_merchant->mer_no,
                    'amount' => $refund_amount * 100,
                    'orderNo' => $OutRefundNo,
                    'oriOrderNo' => $out_trade_no,
                    'orderInfo' => '用户退款',
                    'deviceIp' => \EasyWeChat\Kernel\Support\get_client_ip()
                ];
                $return = $manager->refund_order($hltx_data);
                if ($return['status'] == 1) {
                    $return = $return['data'];
                    if ($return['tradeStatus'] == 'S' || $return['tradeStatus'] == 'R') {
                        //退款请求成功
                        $insert_data = [
                            'status' => 6,
                            'pay_status' => 6,
                            'pay_status_desc' => '已退款',
                            'refund_amount' => $new_refund_amount,
                            'fee_amount' => $new_fee_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        RefundOrder::create([
                            'pay_status' => 3,
                            'status' => 3,
                            'type' => $type,
                            'ways_source' => $order->ways_source,
                            'status_desc' => '退款',
                            'refund_amount' => $refund_amount, //退款金额
                            'refund_no' => $OutRefundNo, //退款单号
                            'store_id' => $store_id,
                            'merchant_id' => $store->merchant_id,
                            'out_trade_no' => $order->out_trade_no,
                            'trade_no' => $order->trade_no
                        ]);

                        $re_data['result_code'] = 'SUCCESS';
                        $re_data['result_msg'] = '退款成功';
                        $re_data['refundNo'] = $OutRefundNo;
                        $re_data['refundFee'] = $refund_amount * 100;
                    } /*elseif($return['tradeStatus']=='R'){
                        $message = '等待退款';
                        //退款失败
                        $re_data['result_code'] = 'USERPAYING';
                        $re_data['result_msg'] = $message;
                        $re_data['refundNo'] = $OutRefundNo;
                        $re_data['refundFee'] = intval($refund_amount * 100).'';
                        $number=5;
                        for($i=0;$i<$number;$i++){
                            sleep(1);
                            \App\Common\Log::write('退款中查询','refund.log');
                            $hltx_data=[
                                'merNo'=>$hltx_merchant->mer_no,
                                'orderNo'=>$OutRefundNo,
                            ];
                            $query_result=$manager->query_order($hltx_data);
                            \App\Common\Log::write($query_result,'refund.log');
                            if($query_result['status']==1){
                                $query_result=$query_result['data'];
                                if($query_result['tradeStatus']=='R'){
                                    if($query_result['subTradeStatus']=='REFUNDING'){
                                        continue;
                                    }elseif($query_result['subTradeStatus']=='FAILED'||$query_result['subTradeStatus']=='SYS_FAILED'){
                                        $message = '退款失败';
                                        //退款失败
                                        $re_data['result_code'] = 'FALL';
                                        $re_data['result_msg'] = $message;
                                        break;
                                    }elseif($query_result['subTradeStatus']=='COMPLETE'){
                                        $insert_data = [
                                            'status' => 6,
                                            'pay_status' => 6,
                                            'pay_status_desc' => '已退款',
                                            'fee_amount' => 0,
                                            'refund_amount' => $refund_amount
                                        ];
                                        $this->update_day_order($insert_data, $out_trade_no);

                                        RefundOrder::create([
                                            'pay_status' => 3,
                                            'status' => 3,
                                            'type' => $type,
                                            'ways_source' => $order->ways_source,
                                            'status_desc' => '退款',
                                            'refund_amount' => $refund_amount, //退款金额
                                            'refund_no' => $OutRefundNo, //退款单号
                                            'store_id' => $store_id,
                                            'merchant_id' => $store->merchant_id,
                                            'out_trade_no' => $order->out_trade_no,
                                            'trade_no' => $order->trade_no
                                        ]);

                                        $re_data['result_code'] = 'SUCCESS';
                                        $re_data['result_msg'] = '退款成功';
                                        $re_data['refundNo'] = $OutRefundNo;
                                        $re_data['refundFee'] = $refund_amount * 100;
                                        break;
                                    }
                                }elseif($query_result['tradeStatus']=='S'){
                                    if($query_result['subTradeStatus']=='COMPLETE'){
                                        $insert_data = [
                                            'status' => 6,
                                            'pay_status' => 6,
                                            'pay_status_desc' => '已退款',
                                            'fee_amount' => 0,
                                            'refund_amount' => $refund_amount
                                        ];
                                        $this->update_day_order($insert_data, $out_trade_no);

                                        RefundOrder::create([
                                            'pay_status' => 3,
                                            'status' => 3,
                                            'type' => $type,
                                            'ways_source' => $order->ways_source,
                                            'status_desc' => '退款',
                                            'refund_amount' => $refund_amount, //退款金额
                                            'refund_no' => $OutRefundNo, //退款单号
                                            'store_id' => $store_id,
                                            'merchant_id' => $store->merchant_id,
                                            'out_trade_no' => $order->out_trade_no,
                                            'trade_no' => $order->trade_no
                                        ]);

                                        $re_data['result_code'] = 'SUCCESS';
                                        $re_data['result_msg'] = '退款成功';
                                        $re_data['refundNo'] = $OutRefundNo;
                                        $re_data['refundFee'] = $refund_amount * 100;
                                        break;
                                    }
                                }else{
                                    break;
                                }
                            }else{
                                break;
                            }
                        }
                    }*/
                    else {
                        $message = '退款失败';
                        //退款失败
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $message;
                    }
                } else {
                    $message = $return['message'];
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //联动优势 退款
            if (4999 < $type && $type < 5999) {
                $config = new LinkageConfigController();
                $linkage_config = $config->linkage_config($config_id);
                if (!$linkage_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '联动优势支付配置不存在，请检查配置'
                    ]);
                }

                $linkage_merchant = $config->linkage_merchant($store_id, $store_pid);
                if (!$linkage_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '联动优势商户号不存在'
                    ]);
                }

                //针对交易当天的成功订单可以进行撤销，过了账期则需要走退费接口。仅支持微信支付宝刷卡交易（即用户被扫）的撤销，其他支付成功订单如需实现相同功能请调用3.5退款请求接口
                $obj = new \App\Api\Controllers\Linkage\PayController();
                $now_time_start = strtotime(date('Y-m-d 00:00:00', time()));
                $now_time_end = strtotime(date('Y-m-d 23:59:59', time()));
                $order_pay_time = strtotime($order->pay_time);

                if (($order_pay_time > $now_time_start) && ($order_pay_time <= $now_time_end)) {
                    $linkage_revoke_data = [
                        'acqSpId' => $linkage_config->mch_id, //
                        'acqMerId' => $linkage_merchant->acqMerId, //
                        'out_trade_no' => $out_trade_no, //
                        'privateKey' => $linkage_config->privateKey, //
                        'publicKey' => $linkage_config->publicKey //
                    ];
//                    Log::info('微收银-联动优势-当日交易撤销-入参');
//                    Log::info($linkage_revoke_data);
                    $return = $obj->order_revoke($linkage_revoke_data); //-1系统错误；0-其他；1-处理成功；2-验签失败
//                    Log::info('微收银-联动优势-当日交易撤销-结果');
//                    Log::info($return);
                    //成功
                    if ($return['status'] == '1') {
                        $linkage_refund_no = $return['data']['transactionId']; //联动流水号
                        $refund_amount = $order->total_amount;
                        $insert_data = [
                            'status' => '6',
                            'pay_status' => '6',
                            'pay_status_desc' => '已退款',
                            'refund_amount' => $new_refund_amount,
                            'fee_amount' => $new_fee_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        RefundOrder::create([
                            'pay_status' => '6',
                            'status' => '6',
                            'type' => $type,
                            'ways_source' => $order->ways_source,
                            'status_desc' => '退款',
                            'refund_amount' => $refund_amount,
                            'refund_no' => $linkage_refund_no,
                            'store_id' => $data['merchantId'],
                            'merchant_id' => $store->merchant_id,
                            'out_trade_no' => $out_trade_no,
                            'trade_no' => $order->trade_no
                        ]);

                        $re_data['result_code'] = 'SUCCESS';
                        $re_data['result_msg'] = '退款成功';
                        $re_data['refundNo'] = $linkage_refund_no;
                        $re_data['refundFee'] = $refund_amount * 100;
                    } else {
                        //其他情况
                        $message = $return['message'];
                        $re_data['result_code'] = 'FALL'; //退款失败
                        $re_data['result_msg'] = $message;
                    }
                } else { //退款
                    $linkage_return_data = [
                        'acqSpId' => $linkage_config->mch_id, //
                        'acqMerId' => $linkage_merchant->acqMerId, //
                        'out_trade_no' => $out_trade_no, //
                        'total_amount' => $order->total_amount, //
                        'refund_amount' => $refund_amount, //
                        'privateKey' => $linkage_config->privateKey,
                        'publicKey' => $linkage_config->publicKey
                    ];
//                    Log::info('联动优势-微收银-退款-入参');
//                    Log::info($linkage_return_data);
                    $return = $obj->refund($linkage_return_data); //（支持部分退款，支持对90天以内的成功订单） -1系统错误；0-其他；1-成功；2-验签失败；3-失败
//                    Log::info('联动优势-微收银-退款-结果');
//                    Log::info($return);

                    $linkage_refund_no = $return['data']['transactionId']; //联动退款流水号

                    //退款成功
                    if ($return['status'] == '1') {
                        $refund_amount = $order->total_amount; //已退款金额
                        $insert_data = [
                            'status' => '6',
                            'pay_status' => 6,
                            'pay_status_desc' => '已退款',
                            'refund_amount' => $new_refund_amount,
                            'fee_amount' => $new_fee_amount
                        ];
                        $this->update_day_order($insert_data, $out_trade_no);

                        RefundOrder::create([
                            'pay_status' => '6',
                            'status' => '6',
                            'type' => $type,
                            'ways_source' => $order->ways_source,
                            'status_desc' => '退款',
                            'refund_amount' => $refund_amount,
                            'refund_no' => $linkage_refund_no,
                            'store_id' => $data['merchantId'],
                            'merchant_id' => $store->merchant_id,
                            'out_trade_no' => $order->out_trade_no,
                            'trade_no' => $order->trade_no
                        ]);

                        $re_data['result_code'] = 'SUCCESS';
                        $re_data['result_msg'] = '退款成功';
                        $re_data['refundNo'] = $linkage_refund_no;
                        $re_data['refundFee'] = $refund_amount * 100;
                    } //失败
                    elseif ($return['status'] == '3') {
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = '退款失败';
                        $re_data['refundNo'] = $OutRefundNo;
                        $re_data['refundFee'] = $refund_amount * 100;
                    } //其他
                    else {
                        $message = $return['message'];
                        $re_data['result_code'] = 'FALL'; //退款失败
                        $re_data['result_msg'] = $message;
                    }
                }
            }

            //威富通 退款
            if (26999 < $type && $type < 27999) {
                $config = new WftPayConfigController();
                $wftpay_config = $config->wftpay_config($config_id);
                if (!$wftpay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '威富通支付配置不存在，请检查配置'
                    ]);
                }

                $wftpay_merchant = $config->wftpay_merchant($store_id, $store_pid);
                if (!$wftpay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '威富通支付商户号不存在'
                    ]);
                }

                $wftpay_return_data = [
                    'mch_id' => $wftpay_merchant->mch_id,
                    'out_trade_no' => $out_trade_no,
                    'total_amount' => $order->total_amount,
                    'refund_fee' => $refund_amount,
                    'private_rsa_key' => $wftpay_config->private_rsa_key,
                    'public_rsa_key' => $wftpay_config->public_rsa_key
                ];
//                Log::info('威富通-微收银-退款-入参');
//                Log::info($wftpay_return_data);
                $obj = new WftPayPayController();
                $return = $obj->refund($wftpay_return_data); //0-系统错误 1-成功 2-退款失败 3-退款处理中 4-退款到银行发现用户的卡作废或者冻结了,导致原路退款银行卡失败
//                Log::info('威富通-微收银-退款');
//                Log::info($return);

                //退款成功
                if ($return['status'] == 1) {
                    $refund_count = $return['data']['refund_count'] - 1; //退款记录数
                    $wftpay_refund_no = isset($return['data']['out_refund_id_' . $refund_count]) ? $return['data']['out_refund_id_' . $refund_count] : $return['data']['refund_id_' . $refund_count]; //第三方退款单号
                    $refund_amount = isset($return['data']['refund_fee_' . $refund_count]) ? ($return['data']['refund_fee_' . $refund_count] / 100) : $order->total_amount; //已退款金额
                    $insert_data = [
                        'status' => '6',
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => '6',
                        'status' => '6',
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount,
                        'refund_no' => $wftpay_refund_no,
                        'store_id' => $data['merchantId'],
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $wftpay_refund_no;
                    $re_data['refundFee'] = $refund_amount * 100;
                } //失败
                elseif ($return['status'] == 2) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '退款失败';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } //退款处理中
                elseif ($return['status'] == 3) {
                    $refund_count = $return['data']['refund_count'] - 1; //退款记录数
                    $wftpay_refund_no = isset($return['data']['out_refund_id_' . $refund_count]) ? $return['data']['out_refund_id_' . $refund_count] : $return['data']['refund_id_' . $refund_count]; //第三方退款单号
                    $refund_amount = isset($return['data']['refund_fee_' . $refund_count]) ? ($return['data']['refund_fee_' . $refund_count] / 100) : $order->total_amount; //退款总金额,单位为分,可以做部分退款
                    $insert_data = [
                        'status' => '5',
                        'pay_status' => 5,
                        'pay_status_desc' => '退款中',
                        'fee_amount' => '0',
                        'refund_amount' => $refund_amount
                    ];
                    $re = $this->update_day_order($insert_data, $out_trade_no);
                    if (!$re) {
                        Log::info('威富通-微收银-退款中-订单更新失败');
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款中';
                    $re_data['refundNo'] = $wftpay_refund_no;
                    $re_data['refundFee'] = $refund_amount * 100;
                } //其他
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL'; //退款失败
                    $re_data['result_msg'] = $message;
                }
            }

            //汇旺财 退款
            if (27999 < $type && $type < 28999) {
                $config = new HwcPayConfigController();
                $hwcpay_config = $config->hwcpay_config($config_id);
                if (!$hwcpay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '汇旺财支付配置不存在，请检查配置'
                    ]);
                }

                $hwcpay_merchant = $config->hwcpay_merchant($store_id, $store_pid);
                if (!$hwcpay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '汇旺财支付商户号不存在'
                    ]);
                }

                $hwcpay_return_data = [
                    'mch_id' => $hwcpay_merchant->mch_id, //门店号
                    'merchant_num' => $hwcpay_merchant->merchant_num, //商户号
                    'out_trade_no' => $out_trade_no,
                    'total_amount' => $order->total_amount,
                    'refund_fee' => $refund_amount,
                    'private_rsa_key' => $hwcpay_config->private_rsa_key,
                    'public_rsa_key' => $hwcpay_config->public_rsa_key
                ];
//                Log::info('汇旺财-微收银-退款-入参');
//                Log::info($hwcpay_return_data);
                $obj = new HwcPayPayController();
                $return = $obj->refund($hwcpay_return_data); //0-系统错误 1-成功 2-退款失败 3-退款处理中 4-退款到银行发现用户的卡作废或者冻结了,导致原路退款银行卡失败
//                Log::info('汇旺财-微收银-退款');
//                Log::info($return);
                //退款成功
                if ($return['status'] == 1) {
                    $refund_count = $return['data']['refund_count'] - 1; //退款记录数
                    $hwcpay_refund_no = isset($return['data']['out_refund_id_' . $refund_count]) ? $return['data']['out_refund_id_' . $refund_count] : $return['data']['refund_id_' . $refund_count]; //第三方退款单号
                    $refund_amount = isset($return['data']['refund_fee_' . $refund_count]) ? ($return['data']['refund_fee_' . $refund_count] / 100) : $order->total_amount; //已退款金额
                    $insert_data = [
                        'status' => '6',
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => '6',
                        'status' => '6',
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount,
                        'refund_no' => $hwcpay_refund_no,
                        'store_id' => $data['merchantId'],
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $hwcpay_refund_no;
                    $re_data['refundFee'] = $refund_amount * 100;
                } //失败
                elseif ($return['status'] == 2) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '退款失败';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } //退款处理中
                elseif ($return['status'] == 3) {
                    $refund_count = $return['data']['refund_count'] - 1; //退款记录数
                    $wftpay_refund_no = isset($return['data']['out_refund_id_' . $refund_count]) ? $return['data']['out_refund_id_' . $refund_count] : $return['data']['refund_id_' . $refund_count]; //第三方退款单号
                    $refund_amount = isset($return['data']['refund_fee_' . $refund_count]) ? ($return['data']['refund_fee_' . $refund_count] / 100) : $order->total_amount; //已退款金额
                    $insert_data = [
                        'status' => '5',
                        'pay_status' => 5,
                        'pay_status_desc' => '退款中',
                        'fee_amount' => '0',
                        'refund_amount' => $refund_amount
                    ];
                    $re = $this->update_day_order($insert_data, $out_trade_no);
                    if (!$re) {
                        Log::info('汇旺财-微收银-退款中-更新订单失败');
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款中';
                    $re_data['refundNo'] = $wftpay_refund_no;
                    $re_data['refundFee'] = $refund_amount * 100;
                } //其他
                else {
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL'; //退款失败
                    $re_data['result_msg'] = $message;
                }

            }

            //钱方 退款
            if (23999 < $type && $type < 24999) {
                $config = new QfPayConfigController();
                $qfpay_config = $config->qfpay_config($config_id);
                if (!$qfpay_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '钱方支付配置不存在，请检查配置'
                    ]);
                }

                $qfpay_merchant = $config->qfpay_merchant($store_id, $store_pid);
                if (!$qfpay_merchant) {
                    return json_encode([
                        'status' => '2',
                        'message' => '钱方支付商户号不存在'
                    ]);
                }

                $qfpay_return_data = [
                    'key' => $qfpay_config->key,  //加签key
                    'code' => $qfpay_config->code,  //钱方唯一标识
                    'mchid' => $qfpay_merchant->mchid,  //子商户号,标识子商户身份
                    'trade_no' => $order->trade_no,  //钱方订单号,退款对应的原订单syssn
                    'out_trade_no' => $out_trade_no,  //外部订单号,同子商户(mchid)下,每次成功调用支付与退款接口,该参数值均不能重复使用,也不能与支付时的外部订单号相同,保证单号唯一,长度不超过128字符
                    'total_amount' => $refund_amount  //订单支付金额
                ];
                Log::info('钱方-微收银-退款-入参');
                Log::info($qfpay_return_data);
                $obj = new QfPayController();
                $return = $obj->refund($qfpay_return_data); //1-成功
                Log::info('钱方-微收银-退款-结果');
                Log::info($return);
                //退款成功
                if ($return['status'] == 1) {
                    $syssn = isset($return['data']['syssn']) ? $return['data']['syssn'] : ''; //退款交易唯一流水号
                    $orig_syssn = isset($return['data']['orig_syssn']) ? $return['data']['orig_syssn'] : ''; //原订交易流水号
                    $txamt = isset($return['data']['txamt']) ? $return['data']['txamt'] : 0; //订单支付金额，单位分
                    $txdtm = isset($return['data']['txdtm']) ? $return['data']['txdtm'] : ''; //请求方交易时间 格式为：YYYY-mm-dd HH:MM:DD
                    $sysdtm = isset($return['data']['sysdtm']) ? $return['data']['sysdtm'] : ''; //系统时间
                    $notifyurl = isset($return['data']['notifyurl']) ? $return['data']['notifyurl'] : ''; //异步通知地址用于请求POS的SDK（HF POS交易特有返回参数）
                    $termid = isset($return['data']['termid']) ? $return['data']['termid'] : ''; //逻辑终端号（HF POS交易额外返回参数）
                    $meroperid = isset($return['data']['meroperid']) ? $return['data']['meroperid'] : ''; //操作员号（HF POS交易额外返回参数）
                    $mchntid = isset($return['data']['mchntid']) ? $return['data']['mchntid'] : ''; //汇付商户号（HF POS交易额外返回参数）

                    $insert_data = [
                        'status' => '6',
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount,
                        'refund_no' => $syssn
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => '6',
                        'status' => '6',
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount,
                        'refund_no' => $syssn,
                        'store_id' => $data['merchantId'],
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $orig_syssn
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $syssn;
                    $re_data['refundFee'] = $txamt;
                } else {  //其他
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL'; //退款失败
                    $re_data['result_msg'] = $message;
                }

            }

            //邮驿付  退款
            if (29000 < $type && $type < 29010) {
                //读取配置
                $config = new PostPayConfigController();
                $post_config = $config->post_pay_config($config_id);
                if (!$post_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '邮驿付配置不存在请检查配置'
                    ]);
                }

                $post_merchant = $config->post_pay_merchant($store_id, $store_pid);
                if (!$post_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '邮驿付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\PostPay\PayController();
                if (strlen($OutRefundNo) > 20) {
                    $OutRefundNo = substr($OutRefundNo, 0, 15) . substr(microtime(), 2, 5);
                }
                $data = [
                    'out_trade_no' => $order->trade_no, //订单号
                    'phone' => $store->people_phone,
                    'agetId' => $post_config->org_id,
                    'custId' => $post_merchant->cust_id,
                    'driveNo' => $post_merchant->drive_no,
                    'refund_amount' => $refund_amount,
                    'orderNo' => $OutRefundNo
                ];
                if ($type == '29001') {
                    $data['tag'] = '1';//支付宝
                } elseif ($type == '29002') {
                    $data['tag'] = '2';//微信
                } else {
                    $data['tag'] = '9';//银联
                }

                $return = $obj->refund($data);
                Log::info('微收银退款返回---' . json_encode($return));

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => 6,
                        'status' => 6,
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $data['merchantId'],
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } elseif (($return['status'] == 1) && ($return['data']['code'] == '0000')) {
                    //TODO: 退款状态未知，先让微收银显示成功
//                    Log::info('随行付退款状态未知，先让微收银显示成功');
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } else {
                    //其他情况
                    $message = $return['message'];
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //河南畅立收
            if ($store->source == '02') {
                //易生数科支付 退款
                if (32000 < $type && $type < 32010) {
                    $config = new EasySkPayConfigController();
                    $easyskpay_config = $config->easyskpay_config($config_id);
                    if (!$easyskpay_config) {
                        return json_encode([
                            'status' => '2',
                            'message' => '易生数科支付配置不存在请检查配置'
                        ]);
                    }

                    $easyskpay_merchant = $config->easyskpay_merchant($store_id, $store_pid);
                    if (!$easyskpay_merchant) {
                        return json_encode([
                            'status' => '2',
                            'message' => '易生数科支付商户号不存在'
                        ]);
                    }

                    //当天的交易只能全额撤销,日切时间为渠道服务器的00:00；非当日才可退款
                    $obj = new \App\Api\Controllers\EasySkPay\PayController();
                    $now_time_start = strtotime(date('Y-m-d 00:00:00', time()));
                    $now_time_end = strtotime(date('Y-m-d 23:59:59', time()));
                    $order_pay_time = strtotime($order->pay_time);

                    if (($order_pay_time > $now_time_start) && ($order_pay_time <= $now_time_end)) {
                        $easysk_data_close = [
                            'org_id' => $easyskpay_config->org_id, //渠道编号
                            'mer_id' => $easyskpay_merchant->mer_id, //终端商戶编号
                            'request_no' => $OutRefundNo,
                            'orig_request_no' => $out_trade_no, //订单号
                            'orig_trade_no' => $order->trade_no //系统订单号
                        ];
                        Log::info('微收银易生数科支付-当日交易撤销-入参');
                        Log::info($easysk_data_close);
                        $return = $obj->order_close($easysk_data_close); //-1 系统错误 0-其他 1-成功
                        Log::info('微收银易生数科支付-当日交易撤销-结果');
                        Log::info($return);

                        //成功
                        if ($return['status'] == '1') {
                            $easypay_refund_no = $return['data']['requestNo']; //撤销的商户订单号2.0
                            $refund_amount = isset($return['data']['bizData']['amount']) ? ($return['data']['bizData']['amount'] / 100) : $refund_amount; //结算金额，单位分2.0
                            $insert_data = [
                                'status' => '6',
                                'pay_status' => '6',
                                'pay_status_desc' => '已退款',
                                'refund_amount' => $new_refund_amount,
                                'fee_amount' => $new_fee_amount
                            ];
                            $this->update_day_order($insert_data, $out_trade_no);

                            RefundOrder::create([
                                'pay_status' => '6',
                                'status' => '6',
                                'type' => $type,
                                'ways_source' => $order->ways_source,
                                'status_desc' => '退款',
                                'refund_amount' => $refund_amount,
                                'refund_no' => $easypay_refund_no,
                                'store_id' => $data['merchantId'],
                                'merchant_id' => $store->merchant_id,
                                'out_trade_no' => $order->out_trade_no,
                                'trade_no' => $return['data']['tradeNo']
                            ]);

                            $re_data['result_code'] = 'SUCCESS';
                            $re_data['result_msg'] = '退款成功';
                            $re_data['refundNo'] = $easypay_refund_no;
                            $re_data['refundFee'] = $refund_amount * 100;
                        } else {
                            //其他情况
                            $message = $return['message'];
                            $re_data['result_code'] = 'FALL'; //退款失败
                            $re_data['result_msg'] = $message;
                        }
                    } else {
                        $easysk_data_refund = [
                            'org_id' => $easyskpay_config->org_id, //渠道编号
                            'mer_id' => $easyskpay_merchant->mer_id, //终端商戶编号
                            'request_no' => $OutRefundNo,
                            'orig_request_no' => $out_trade_no, //订单号
                            'orig_trade_no' => $order->trade_no //系统订单号
                        ];
                        Log::info('易生数科支付-微收银-退款-入参');
                        Log::info($easysk_data_refund);
                        $return = $obj->refund($easysk_data_refund); //-1 系统错误 0-其他 1-成功 2-失败
                        Log::info('易生数科支付-微收银-退款-结果');
                        Log::info($return);
                        $easypay_refund_no = $return['data']['requestNo'];

                        //退款成功
                        if ($return['status'] == '1') {
                            $refund_amount = $order->total_amount; //已退款金额
                            $insert_data = [
                                'status' => '6',
                                'pay_status' => '6',
                                'pay_status_desc' => '已退款',
                                'refund_amount' => $new_refund_amount,
                                'fee_amount' => $new_fee_amount
                            ];
                            $this->update_day_order($insert_data, $out_trade_no);

                            RefundOrder::create([
                                'pay_status' => '6',
                                'status' => '6',
                                'type' => $type,
                                'ways_source' => $order->ways_source,
                                'status_desc' => '退款',
                                'refund_amount' => $refund_amount,
                                'refund_no' => $easypay_refund_no,
                                'store_id' => $data['merchantId'],
                                'merchant_id' => $store->merchant_id,
                                'out_trade_no' => $order->out_trade_no,
                                'trade_no' => $return['data']['tradeNo']
                            ]);

                            $re_data['result_code'] = 'SUCCESS';
                            $re_data['result_msg'] = '退款成功';
                            $re_data['refundNo'] = $easypay_refund_no;
                            $re_data['refundFee'] = $refund_amount * 100;
                        } //失败
                        elseif ($return['status'] == '2') {
                            $re_data['result_code'] = 'FALL';
                            $re_data['result_msg'] = '退款失败';
                            $re_data['refundNo'] = $OutRefundNo;
                            $re_data['refundFee'] = $refund_amount * 100;
                        } //其他情况
                        else {
                            $message = $return['message'];
                            $re_data['result_code'] = 'FALL'; //退款失败
                            $re_data['result_msg'] = $message;
                        }
                    }
                }
            }

            //通联支付 退款
            if (33000 < $type && $type < 33010) {
                $config = new AllinPayConfigController();
                $allin_config = $config->allin_pay_config($config_id);
                if (!$allin_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '通联支付配置不存在请检查配置'
                    ]);
                }

                $allin_merchant = $config->allin_pay_merchant($store_id, $store_pid);
                if (!$allin_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '通联支付支付商户号不存在'
                    ]);
                }

                //当天的交易只能全额撤销,日切时间为渠道服务器的00:00；非当日才可退款
                $obj = new \App\Api\Controllers\AllinPay\PayController();
                $now_time_start = strtotime(date('Y-m-d 00:00:00', time()));
                $now_time_end = strtotime(date('Y-m-d 23:59:59', time()));
                $order_pay_time = strtotime($order->pay_time);
                $allin_refund_data = [
                    'agetId' => $allin_config->org_id, //代理编号
                    'appid' => $allin_merchant->appid,//平台分配的appid
                    'cusId' => $allin_merchant->cus_id, //商户编号
                    'orderNo' => $OutRefundNo, //退款单号
                    'oldreqsn' => $out_trade_no, //订单号
                    'oldtrxid' => $order->trade_no, //系统订单号
                    'refund_amount' => $refund_amount, //退货金额,不大于原交易金额与已成功退货金额之差
                ];
                Log::info('通联支付-退款');
                Log::info($allin_refund_data);
                //当天交易-撤销
                if (($order_pay_time > $now_time_start) && ($order_pay_time <= $now_time_end)) {
                    $return = $obj->cancel($allin_refund_data); // -1 系统错误 0-其他 1-成功 2-失败
                } else {//隔天交易-退款
                    $return = $obj->refund($allin_refund_data); // -1 系统错误 0-其他 1-成功 2-失败
                }
                Log::info('微收银易生数科支付-当日交易撤销-结果');
                Log::info($return);
                if ($return['status'] == '1') {
                    $refund_no = $return['data']['trxid']; //撤销的商户订单号
                    $refund_amount = $refund_amount; //结算金额，单位分
                    $insert_data = [
                        'status' => '6',
                        'pay_status' => '6',
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => '6',
                        'status' => '6',
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount,
                        'refund_no' => $refund_no,
                        'store_id' => $data['merchantId'],
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $return['data']['tradeNo']
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $refund_no;
                    $re_data['refundFee'] = $refund_amount * 100;
                } else {
                    //其他情况
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL'; //退款失败
                    $re_data['result_msg'] = $message;
                }
            }

            //富友支付 退款
            if (11000 < $type && $type < 11010) {
                $config = new FuiouConfigController();
                $fuiou_config = $config->fuiou_config($config_id);
                if (!$fuiou_config) {
                    return json_encode([
                        'status' => '2',
                        'message' => '富友支付配置不存在请检查配置'
                    ]);
                }

                $fuiou_merchant = $config->fuiou_merchant($store_id, $store_pid);
                if (!$fuiou_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '富友支付商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\Fuiou\PayController();
                $fuiou_refund_data = [
                    'ins_cd' => $fuiou_config->ins_cd, //代理编号
                    'mchnt_cd' => $fuiou_merchant->mchnt_cd,//平台分配的appid
                    'mchnt_order_no' => $out_trade_no, //订单号
                    'refund_order_no' => $OutRefundNo, //退款单号
                    'total_amt' => $refund_amount, //订单金额
                    'refund_amt' => $refund_amount, //退货金额,不大于原交易金额与已成功退货金额之差
                ];
                if ($order->ways_source == 'alipay') {
                    $fuiou_refund_data['order_type'] = 'ALIPAY';
                }
                if ($order->ways_source == 'weixin') {
                    $fuiou_refund_data['order_type'] = 'WECHAT';
                }
                if ($order->ways_source == 'unionpay') {
                    $fuiou_refund_data['order_type'] = 'UNIONPAY';
                }
                $return = $obj->refund($fuiou_refund_data); // -1 系统错误 0-其他 1-成功 2-失败
                if ($return['status'] == '1') {
                    $refund_no = $return['data']['refund_order_no']; //撤销的商户订单号
                    $refund_amount = $refund_amount; //结算金额，单位分
                    $insert_data = [
                        'status' => '6',
                        'pay_status' => '6',
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => '6',
                        'status' => '6',
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount,
                        'refund_no' => $refund_no,
                        'store_id' => $data['merchantId'],
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $return['data']['tradeNo']
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $refund_no;
                    $re_data['refundFee'] = $refund_amount * 100;
                } else {
                    //其他情况
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL'; //退款失败
                    $re_data['result_msg'] = $message;
                }
            }

            //银盛  退款
            if (14000 < $type && $type < 14010) {
                //读取配置
                $config = new YinshengConfigController();
                $yinsheng_config = $config->yinsheng_config($config_id);
                if (!$yinsheng_config) {
                    return json_encode([
                        'status' => 2,
                        'message' => '银盛配置不存在请检查配置'
                    ]);
                }

                $yinsheng_merchant = $config->yinsheng_merchant($store_id, $store_pid);
                if (!$yinsheng_merchant) {
                    return json_encode([
                        'status' => 2,
                        'message' => '银盛商户号不存在'
                    ]);
                }

                $obj = new \App\Api\Controllers\YinSheng\PayController();

                $data = [
                    'out_trade_no' => $order->out_trade_no, //订单号
                    'partner_id' => $yinsheng_config->partner_id,
                    'shop_date' => $order->pay_time,
                    'tran_type' => '1',
                    'refund_amount' => $refund_amount,
                    'orderNo' => $OutRefundNo,
                    'trade_no' => $order->trade_no
                ];

                $return = $obj->refund($data);

                //退款请求成功
                if ($return["status"] == 1) {
                    $insert_data = [
                        'status' => 6,
                        'pay_status' => 6,
                        'pay_status_desc' => '已退款',
                        'refund_amount' => $new_refund_amount,
                        'fee_amount' => $new_fee_amount
                    ];
                    $this->update_day_order($insert_data, $out_trade_no);

                    RefundOrder::create([
                        'pay_status' => 6,
                        'status' => 6,
                        'type' => $type,
                        'ways_source' => $order->ways_source,
                        'status_desc' => '退款',
                        'refund_amount' => $refund_amount, //退款金额
                        'refund_no' => $OutRefundNo, //退款单号
                        'store_id' => $data['merchantId'],
                        'merchant_id' => $store->merchant_id,
                        'out_trade_no' => $order->out_trade_no,
                        'trade_no' => $order->trade_no
                    ]);

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } elseif (($return['status'] == 1) && ($return['data']['code'] == '0000')) {
                    //TODO: 退款状态未知，先让微收银显示成功
//                    Log::info('随行付退款状态未知，先让微收银显示成功');
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $OutRefundNo;
                    $re_data['refundFee'] = $refund_amount * 100;
                } else {
                    //其他情况
                    $message = $return['message'];
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

        } catch (\Exception $ex) {
            $re_data = [
                'return_code' => 'FALL',
                'return_msg' => $ex->getMessage() . $ex->getLine()
            ];
        }

        return $this->return_data($re_data);
    }


    //退款查询
    public function refund_query(Request $request)
    {
        try {
            //获取请求参数
            $data = $request->getContent();
            $data = json_decode($data, true);
            //Log::info('微收银-退款-获得参数');
            //Log::info($data);

            //验证签名
            $check = $this->check_md5($data);
            if ($check['return_code'] == 'FALL') {
                return $this->return_data($check);
            }

            //调用系统前参数
            $where = [];
            if (isset($data['tradeNo'])) {
                $where[] = ['out_trade_no', '=', $data['tradeNo']];
            }
            if (isset($data['out_transaction_id'])) {
                // $where[] = ['trade_no', '=', $data['out_transaction_id']];
                $where[] = ['out_trade_no', '=', $data['out_transaction_id']];

            }
            if (isset($data['outTradeNo'])) {
                $where[] = ['qwx_no', '=', $data['outTradeNo']];
            }

            //发起查询
            $day = date('Ymd', time());
            $table = 'orders_' . $day;
            if (Schema::hasTable($table)) {
                $order = DB::table($table)->where('store_id', $data['merchantId'])
                    ->where($where)
                    ->select('ways_type', 'qwx_no', 'updated_at', 'out_trade_no')//2.0 ways_type 1.0 type
                    ->first();
            } else {
                $order = Order::where('store_id', $data['merchantId'])
                    ->where($where)
                    ->select('ways_type', 'qwx_no', 'updated_at', 'out_trade_no')//2.0 ways_type 1.0 type
                    ->first();
            }

            //如果订单号为空或者不存在
            if (!$order) {
                $re_data['result_code'] = 'FALL';
                $re_data['result_msg'] = '订单号不存在';
                return $this->return_data($re_data);
            }

            $refundNo = $data['refundNo'];
            //上线后去掉开始
            $refund = RefundOrder::where('out_trade_no', $order->out_trade_no)
                ->select('refund_no')
                ->first();

            //如果订单号为空或者不存在
            if (!$refund) {
                $re_data['result_code'] = 'FALL';
                $re_data['result_msg'] = '订单号不存在';
                return $this->return_data($re_data);
            }
            $refundNo = $refund->refund_no;

            //上线后去掉结束
            $channel = '';
            if ($order->ways_type == 1001) {
                $channel = 'ali_barcode_pay';
            }

            if ($order->ways_type == 2001) {
                $channel = "wx_barcode_pay";
            }

            $re_data = [
                'return_code' => 'SUCCESS', //SUCCESS/FALL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
                'return_msg' => null,
                'result_code' => '',
                'result_msg' => '',
                'tradeNo' => $order->out_trade_no,
                'outTradeNo' => $order->qwx_no, //微收银唯一订单号
                'refundNo' => '',
                'refundFee' => '',
                'channel' => $channel, //$data['channel'], //支付类型 微信主扫：wx_barcode_pay 支付宝：ali_barcode_pay
                'totalFee' => '',
                'refundTime' => '',
                'refund_count' => '',
            ];

            $type = $order->ways_type;
            $store = Store::where('store_id', $data['merchantId'])
                ->select('config_id', 'merchant_id', 'source')
                ->first();
            $config_id = $store->config_id;
            $store_id = $data['merchantId'];
            $store_pid = $store->pid;
            $out_trade_no = $order->out_trade_no;

            //官方支付宝查询
            if (999 < $type && $type < 1999) {
                $config = AlipayIsvConfig::where('config_id', $config_id)
                    ->where('config_type', '01')
                    ->first();

                //获取token
                $storeInfo = AlipayAppOauthUsers::where('store_id', $data['merchantId'])
                    ->select('app_auth_token')
                    ->first();

                $aop = new AopClient();
                $aop->apiVersion = "2.0";
                $aop->appId = $config->app_id;
                $aop->rsaPrivateKey = $config->rsa_private_key;
                $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                $aop->signType = "RSA2"; //升级算法
                $aop->gatewayUrl = $config->alipay_gateway;
                $aop->format = "json";
                $aop->charset = "GBK";
                $aop->version = "2.0";
                $aop->method = 'alipay.trade.fastpay.refund.query';
                $requests = new AlipayTradeFastpayRefundQueryRequest();
                $requests->setBizContent("{" .
                    "    \"out_trade_no\":\"" . $order->out_trade_no . "\"," .
                    "    \"out_request_no\":\"" . $refundNo . "\"" .
                    "  }");
                $refund = $aop->execute($requests, '', $storeInfo->app_auth_token);

                //支付成功
                if ($refund->alipay_trade_fastpay_refund_query_response->code == 10000) {
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $refund->alipay_trade_fastpay_refund_query_response->out_request_no;
                    $re_data['refundFee'] = $refund->alipay_trade_fastpay_refund_query_response->refund_amount * 100;
                    $re_data['totalFee'] = $refund->alipay_trade_fastpay_refund_query_response->total_amount * 100;
                    $re_data['refundTime'] = '' . $order->updated_at . '';
                    $re_data['refund_count'] = '1';
                } else {
                    //其他情况
                    $message = $refund->alipay_trade_fastpay_refund_query_response->sub_msg;
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //微信官方扫码
            if (1999 < $type && $type < 2999) {
                $config = new WeixinConfigController();
                $options = $config->weixin_config($config_id);
                $weixin_store = $config->weixin_merchant($store_id, $store_pid);
                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;
                $config = [
                    'app_id' => $options['app_id'],
                    'mch_id' => $options['payment']['merchant_id'],
                    'key' => $options['payment']['key'],
                    'cert_path' => $options['payment']['cert_path'], // XXX: 绝对路径！！！！
                    'key_path' => $options['payment']['key_path'],     // XXX: 绝对路径！！！！
                    'sub_mch_id' => $wx_sub_merchant_id,
                    // 'device_info'     => '013467007045764',
                    // 'sub_app_id'      => '',
                    // ...
                ];

                $payment = Factory::payment($config);
                $refund_query = $payment->refund->queryByOutTradeNumber($order->out_trade_no);
                if ($refund_query['return_code'] == "SUCCESS") {
                    //退款成功
                    if ($refund_query['result_code'] == "SUCCESS") {
                        $re_data['result_code'] = 'SUCCESS';
                        $re_data['result_msg'] = '退款成功';
                        $re_data['refundNo'] = $refund_query['refund_status_0'];
                        $re_data['refundFee'] = $refund_query['refund_fee'];
                        $re_data['totalFee'] = $refund_query['total_fee'];
                        $re_data['refundTime'] = $refund_query['refund_success_time_0'];
                        $re_data['refund_count'] = $refund_query['refund_count'];
                    } else {
                        //
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $refund_query['result_msg'];
                    }
                } else {
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $refund_query['return_msg'];
                }
            }

            //微信官方a 扫码
            if (3999 < $type && $type < 4999) {
                $config = new WeixinConfigController();
                $options = $config->weixina_config($config_id);
                $weixin_store = $config->weixina_merchant($store_id, $store_pid);
                $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;

                $config = [
                    'app_id' => $options['app_id'],
                    'mch_id' => $options['payment']['merchant_id'],
                    'key' => $options['payment']['key'],
                    'cert_path' => $options['payment']['cert_path'], // XXX: 绝对路径！！！！
                    'key_path' => $options['payment']['key_path'],     // XXX: 绝对路径！！！！
                    'sub_mch_id' => $wx_sub_merchant_id,
                    // 'device_info'     => '013467007045764',
                    // 'sub_app_id'      => '',
                    // ...
                ];
                $payment = Factory::payment($config);
                $refund_query = $payment->refund->queryByOutTradeNumber($order->out_trade_no);
                if ($refund_query['return_code'] == "SUCCESS") {
                    //退款成功
                    if ($refund_query['result_code'] == "SUCCESS") {
                        $re_data['result_code'] = 'SUCCESS';
                        $re_data['result_msg'] = '退款成功';
                        $re_data['refundNo'] = $refund_query['refund_status_0'];
                        $re_data['refundFee'] = $refund_query['refund_fee'];
                        $re_data['totalFee'] = $refund_query['total_fee'];
                        $re_data['refundTime'] = $refund_query['refund_success_time_0'];
                        $re_data['refund_count'] = $refund_query['refund_count'];
                    } else {
                        //
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = $refund_query['result_msg'];
                    }
                } else {
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $refund_query['return_msg'];
                }
            }

            //京东收银支付退款查询
            if (5999 < $type && $type < 6999) {
                //读取配置
                $config = new JdConfigController();
                $jd_config = $config->jd_config($config_id);
                if (!$jd_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '京东配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $jd_merchant = $config->jd_merchant($store_id, $store_pid);
                if (!$jd_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '京东商户号不存在';
                    return $this->return_data($re_data);
                }
                $obj = new \App\Api\Controllers\Jd\PayController();
                $data = [];
                $data['request_url'] = $obj->refund_query_url; //请求地址;
                $data['merchant_no'] = $jd_merchant->merchant_no;
                $data['md_key'] = $jd_merchant->md_key; //
                $data['des_key'] = $jd_merchant->des_key; //
                $data['systemId'] = $jd_config->systemId; //
                $data['outRefundNo'] = $refundNo;

                $return = $obj->refund_query($data);

                //退款成功
                if ($return["status"] == 1) {
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $refundNo;
                    $re_data['refundFee'] = $return['data']['amount'];
                    $re_data['totalFee'] = $return['data']['amount'];
                    $re_data['refundTime'] = date('Y-m-d H:i:s', strtotime($return['data']['payFinishTime']));
                    $re_data['refund_count'] = '1';

                } //等待付款
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '退款中';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);

                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;

                } else {
                    //其他情况
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }

            }

            //联拓富收银支付退款查询
            if (9999 < $type && $type < 10999) {
                //读取配置
                $config = new LtfConfigController();

                $ltf_merchant = $config->ltf_merchant($store_id, $store_pid);
                if (!$ltf_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '京东商户号不存在';
                    return $this->return_data($re_data);
                }
                $obj = new \App\Api\Controllers\Ltf\PayController();
                $data = [];
                $data['request_url'] = $obj->refund_query_url; //请求地址;
                $data['merchant_no'] = $ltf_merchant->merchantCode;
                $data['appId'] = $ltf_merchant->appId; //
                $data['key'] = $ltf_merchant->md_key; //
                $data['outRefundNo'] = $refundNo;

                $return = $obj->refund_query($data);

                //退款成功
                if ($return["status"] == 1) {
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $refundNo;
                    $re_data['refundFee'] = $return['data']['refundAmount'];
                    $re_data['totalFee'] = $return['data']['refundAmount'];
                    $re_data['refundTime'] = date('Y-m-d H:i:s', strtotime($return['data']['time']));
                    $re_data['refund_count'] = '1';
                } //等待付款
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '退款中';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                } else {
                    //其他情况
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //快钱支付
            if (2999 < $type && $type < 3999) {
                //读取配置
                $config = new MyBankConfigController();


                $mybank_merchant = $config->mybank_merchant($store_id, $store_pid);
                if (!$mybank_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '快钱商户号不存在';
                    return $this->return_data($re_data);
                }
                $wx_AppId = $mybank_merchant->wx_AppId;

                $MyBankConfig = $config->MyBankConfig($config_id, $wx_AppId);
                if (!$MyBankConfig) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '快钱配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $aop = new \App\Api\Controllers\MyBank\BaseController();
                $ao = $aop->aop($config_id);
                $ao->url = $aop->MY_BANK_request2;
                $ao->Function = "ant.mybank.bkmerchanttrade.refundQuery";

                $data = [
                    'MerchantId' => $mybank_merchant->MerchantId,
                    'OutRefundNo' => $refundNo,
                ];
                $re = $ao->Request($data);
                if ($re['status'] == 0) {
                    //退款失败
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $re['msg'];
                    return $this->return_data($re_data);

                }

                $body = $re['data']['document']['response']['body'];
                $Result = $body['RespInfo'];
                //支付成功
                if ($Result['ResultStatus'] == "S") {
                    $GmtRefundment = $body['GmtRefundment'];

                    //退款成功
                    if ($body['TradeStatus'] == "succ") {
                        $re_data['result_code'] = 'SUCCESS';
                        $re_data['result_msg'] = '退款成功';
                        $re_data['refundNo'] = $body['OutRefundNo'];
                        $re_data['refundFee'] = '' . ($order->total_amount * 100) . '';
                        $re_data['totalFee'] = $body['RefundAmount'];
                        $re_data['refundTime'] = '' . $GmtRefundment . '';
                        $re_data['refund_count'] = '1';
                    }

                    //退款中
                    if ($body['TradeStatus'] == "refunding") {
                        $re_data['result_code'] = 'USERPAYING';
                        $re_data['result_msg'] = '退款中';
                        $re_data['refundNo'] = $refundNo;
                    }

                    //失败
                    if ($body['TradeStatus'] == "fail") {
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = '退款失败';
                        $re_data['refundNo'] = $refundNo;
                    }
                } else {
                    //其他情况
                    $message = $Result['ResultMsg'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //和融通
            if (8999 < $type && $type < 9999) {
                //读取配置
                $config = new HConfigController();
                $h_config = $config->h_config($config_id);
                if (!$h_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '和融通配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $h_merchant = $config->h_merchant($store_id, $store_pid);
                if (!$h_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '和融通商户号不存在';
                    return $this->return_data($re_data);
                }

                $obj = new \App\Api\Controllers\Huiyuanbao\PayController();
                $data = [];
                $data['out_trade_no'] = $order->out_trade_no;
                $data['request_url'] = $obj->order_query_url; //暂时用查询代替
                $data['mid'] = $h_merchant->h_mid;
                $data['md_key'] = $h_config->md_key; //
                $data['orgNo'] = $h_merchant->orgNo; //

                $return = $obj->order_query($data); //暂时用查询代替

                //退款成功
                if ($return["status"] == 4) {
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $refundNo;
                    $re_data['refundFee'] = $return['data']['amount'];
                    $re_data['totalFee'] = $return['data']['amount'];
                    $re_data['refundTime'] = date('Y-m-d H:i:s', strtotime($return['data']['payFinishTime']));
                    $re_data['refund_count'] = '1';
                } //等待付款
                elseif ($return["status"] == 5) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '退款中';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);

                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;

                } else {
                    //其他情况
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //传化
            if (11999 < $type && $type < 12999) {
                //读取配置
                $config = new TfConfigController();

                $h_merchant = $config->tf_merchant($store_id, $store_pid);

                if (!$h_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '传化商户号不存在';
                    return $this->return_data($re_data);
                }

                $h_config = $config->tf_config($config_id, $h_merchant->qd);

                if (!$h_config) {

                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '传化配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $data = [];
                $obj = new \App\Api\Controllers\Tfpay\PayController();
                $data['mch_id'] = $h_config->mch_id; //
                $data['pub_key'] = $h_config->pub_key; //
                $data['pri_key'] = $h_config->pri_key; //
                $data['sub_mch_id'] = $h_merchant->sub_mch_id; //
                $data['refund_trade_no'] = $out_trade_no; //
                $data['date'] = date('Y-m-d', time()); //

                $return = $obj->refund_query($data); //暂时用查询代替

                //退款成功
                if ($return["status"] == 4) {
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $refundNo;
                    $re_data['refundFee'] = $return['data']['refund_fee'];
                    $re_data['totalFee'] = $return['data']['refund_fee'];
                    $re_data['refundTime'] = date('Y-m-d H:i:s', time());
                    $re_data['refund_count'] = '1';

                } //等待付款
                elseif ($return["status"] == 5) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '退款中';
                    $re_data['outTradeNo'] = $order->qwx_no;
                    return $this->return_data($re_data);

                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;

                } else {
                    //其他情况
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }

            }

            //随行付 支付退款
            if (12999 < $type && $type < 13999) {
                //读取配置
                $config = new VbillConfigController();
                $vbill_config = $config->vbill_config($config_id);
                if (!$vbill_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '随行付配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $vbill_merchant = $config->vbill_merchant($store_id, $store_pid);
                if (!$vbill_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '随行付商户号不存在';

                    return $this->return_data($re_data);
                }

                $obj = new \App\Api\Controllers\Vbill\PayController();
                $data['notify_url'] = url('/api/vbill/refund_notify_url'); //回调地址
                $data['request_url'] = $obj->refund_url; //请求地址;
                $data['mno'] = $vbill_merchant->mno;
                $data['privateKey'] = $vbill_config->privateKey; //
                $data['sxfpublic'] = $vbill_config->sxfpublic; //
                $data['orgId'] = $vbill_config->orgId; //
                $data['out_trade_no'] = $order->out_trade_no;

                $return = $obj->refund_query($data);

                //退款成功
                if ($return["status"] == 1) {
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $refundNo;
                    $re_data['refundTime'] = date('Y-m-d H:i:s', time());
                    $re_data['refund_count'] = '1';
                } //等待付款
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '正在查询退款中';
                    $re_data['outTradeNo'] = $order->qwx_no;

                    return $this->return_data($re_data);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                } else {
                    //其他情况
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //随行付a 支付退款
            if (18999 < $type && $type < 19999) {
                //读取配置
                $config = new VbillConfigController();
                $vbill_config = $config->vbilla_config($config_id);
                if (!$vbill_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '随行付a配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $vbill_merchant = $config->vbilla_merchant($store_id, $store_pid);
                if (!$vbill_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '随行付a商户号不存在';

                    return $this->return_data($re_data);
                }

                $obj = new \App\Api\Controllers\Vbill\PayController();
                $data['notify_url'] = url('/api/vbill/refund_notify_a_url'); //回调地址
                $data['request_url'] = $obj->refund_url; //请求地址;
                $data['mno'] = $vbill_merchant->mno;
                $data['privateKey'] = $vbill_config->privateKey; //
                $data['sxfpublic'] = $vbill_config->sxfpublic; //
                $data['orgId'] = $vbill_config->orgId; //
                $data['out_trade_no'] = $order->out_trade_no;

                $return = $obj->refund_query($data);

                //退款成功
                if ($return["status"] == 1) {
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $refundNo;
                    $re_data['refundTime'] = date('Y-m-d H:i:s', time());
                    $re_data['refund_count'] = '1';
                } //等待付款
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '正在查询退款中';
                    $re_data['outTradeNo'] = $order->qwx_no;

                    return $this->return_data($re_data);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '订单支付失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                } else {
                    //其他情况
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //海科融通 退款查询
            if (21999 < $type && $type < 22999) {
                //读取配置
                $config = new HkrtConfigController();
                $hkrt_config = $config->hkrt_config($config_id);
                if (!$hkrt_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '海科融通配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $hkrt_merchant = $config->hkrt_merchant($store_id, $store_pid);
                if (!$hkrt_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '海科融通商户号不存在';
                    return $this->return_data($re_data);
                }

                $obj = new \App\Api\Controllers\Hkrt\PayController();

                $hkrt_return_data = [
                    'access_id' => $hkrt_config->access_id,
                    'refund_no' => $refundNo, //退款订单号,refund_no和 out_refund_no和channel_trade_no必传其中一个，三个都传则以refund_no为准，推荐使用refund_no
                    'access_key' => $hkrt_config->access_key
                ];
//                Log::info('微收银-海科融通-退款查询');
//                Log::info($hkrt_return_data);
                $return = $obj->refund_query($hkrt_return_data); //0-系统错误 1-成功 2-失败 3-未知
//                Log::info($return);

                //退款请求成功
                if ($return['status'] == '1') {
                    $hkrt_refund_amount = $return['data']['refund_amount']; //退款金额
                    $hkrt_total_amount = $return['data']['total_amount']; //原消费交易总金额
                    $hkrt_remanent_amount = $return['data']['remanent_amount']; //剩余可退款金额
                    $hkrt_refund_no = $return['data']['refund_no']; //SaaS平台的退款订单编号
                    $hkrt_return_time = $return['data']['trade_end_time']; //SaaS平台交易完成时间，交易完成以后会返回

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $hkrt_refund_no;
                    $re_data['refundFee'] = '' . ($hkrt_refund_amount * 100) . '';
                    $re_data['totalFee'] = $hkrt_total_amount;
                    $re_data['refundTime'] = '' . $hkrt_return_time . '';
                    $re_data['refund_count'] = '1';
                    return json_encode($re_data);
                } else {
                    //其他情况
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '退款失败';
                    $re_data['refundNo'] = $refundNo;
                }
            }

            //易生支付 退款查询
            if (20999 < $type && $type < 21999) {
                $config = new EasyPayConfigController();
                $easypay_config = $config->easypay_config($config_id);
                if (!$easypay_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '易生支付配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $easypay_merchant = $config->easypay_merchant($store_id, $store_pid);
                if (!$easypay_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '易生支付商户号不存在';
                    return $this->return_data($re_data);
                }

                $obj = new EasypayPayController();
                $easypay_return_data = [
                    'channel_id' => $easypay_config->channel_id,
                    'mer_id' => $easypay_merchant->term_mercode,
                    'device_id' => $easypay_merchant->term_termcode,
                    'refund_no' => $refund->refund_no
                ];
//                Log::info('微收银-易生支付-退款查询');
//                Log::info($easypay_return_data);
                $return = $obj->refund_query($easypay_return_data); //-1 系统错误 0-其他 1-成功 2-下单失败
//                Log::info($return);

                //退款请求成功
                if ($return['status'] == '1') {
                    //$easypay_refund_no = $return['data']['oriwtorderid']; //易生退货单号1.0
                    $easypay_refund_no = $refund->refund_no; //易生退货单号2.0
                    $easypay_refund_amount = $order->total_amount; //退款金额
                    $easypay_total_amount = $order->total_amount; //原消费交易总金额
                    $easypay_return_time = $order->pay_time; //交易完成时间

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $easypay_refund_no;
                    $re_data['refundFee'] = '' . ($easypay_refund_amount * 100) . '';
                    $re_data['totalFee'] = $easypay_total_amount;
                    $re_data['refundTime'] = '' . $easypay_return_time . '';
                    $re_data['refund_count'] = '1';
                    return json_encode($re_data);
                } else {
                    //其他情况
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '退款失败';
                    $re_data['refundNo'] = $refundNo;
                }
            }

            //联动优势 退款查询
            if (4999 < $type && $type < 5999) {
                $config = new LinkageConfigController();
                $linkage_config = $config->linkage_config($config_id);
                if (!$linkage_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '联动优势支付配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $linkage_merchant = $config->linkage_merchant($store_id, $store_pid);
                if (!$linkage_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '联动优势支付商户号不存在';
                    return $this->return_data($re_data);
                }

                $obj = new \App\Api\Controllers\Linkage\PayController();
                $linkage_return_data = [
                    'channel_id' => $linkage_config->channel_id,
                    'mer_id' => $linkage_merchant->termMercode,
                    'device_id' => $linkage_merchant->termTermcode,
                    'out_trade_no' => $order->out_trade_no
                ];
//                Log::info('微收银-联动优势支付-退款查询-入参');
//                Log::info($linkage_return_data);
                $return = $obj->refund_query($linkage_return_data); //-1系统错误；0-其他；1-成功；2-验签失败；3-失败；4-处理中
//                Log::info('微收银-联动优势支付-退款查询-结果');
//                Log::info($return);

                //退款请求成功
                if ($return['status'] == '1') {
                    $linkage_refund_no = $return['data']['orderNo']; //退款流水号
                    $linkage_trade_no = $return['data']['transactionId']; //联动流水号
                    $linkage_refund_amount = $return['data']['txnAmt']; //退款金额
                    $linkage_total_amount = $order->total_amount; //原消费交易总金额
                    $linkage_return_time = $order->pay_time; //交易完成时间

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $linkage_refund_no;
                    $re_data['refundFee'] = '' . ($linkage_refund_amount * 100) . '';
                    $re_data['totalFee'] = $linkage_total_amount;
                    $re_data['refundTime'] = '' . $linkage_return_time . '';
                    $re_data['refund_count'] = '1';
                    return json_encode($re_data);
                } else {
                    //其他情况
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '退款失败';
                    $re_data['refundNo'] = $refundNo;
                }
            }

            //威富通 退款查询
            if (26999 < $type && $type < 27999) {
                $config = new WftPayConfigController();
                $wftpay_config = $config->wftpay_config($config_id);
                if (!$wftpay_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '威富通支付配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $wftpay_merchant = $config->wftpay_merchant($store_id, $store_pid);
                if (!$wftpay_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '威富通支付商户号不存在';
                    return $this->return_data($re_data);
                }

                $obj = new WftpayPayController();
                $wftpay_return_data = [
                    'mch_id' => $wftpay_merchant->mch_id,
                    'out_trade_no' => $order->out_trade_no,
                    'private_rsa_key' => $wftpay_config->private_rsa_key,
                    'public_rsa_key' => $wftpay_config->public_rsa_key
                ];
//                Log::info('威富通支付-微收银-退款查询-入参');
//                Log::info($wftpay_return_data);
                $return = $obj->refund_query($wftpay_return_data); //0-系统错误 1-成功 2-退款失败 3-退款处理中 4-退款到银行发现用户的卡作废或者冻结了,导致原路退款银行卡失败
//                Log::info('威富通支付-微收银-退款查询');
//                Log::info($return);
                //退款请求成功
                if ($return['status'] == 1) {
                    $refund_count = $return['data']['refund_count'] - 1; //退款记录数
                    $wftpay_refund_no = $return['data']['out_refund_no_' . $refund_count] ?? ''; //商户退款单号
                    $wftpay_trade_no = $return['data']['out_refund_id_' . $refund_count] ?? ''; //第三方退款单号
                    $wftpay_refund_amount = isset($return['data']['refund_fee_' . $refund_count]) ? ($return['data']['refund_fee_' . $refund_count] / 100) : $order->total_amount; //退款总金额,单位为分,可以做部分退款
                    $wftpay_total_amount = $order->total_amount; //原消费交易总金额
                    $wftpay_return_time = $order->pay_time; //交易完成时间
                    $pay_time = isset($return['data']['refund_time_' . $refund_count]) ? date('Y-m-d H:i:s', strtotime($return['data']['refund_time_' . $refund_count])) : ''; //退款时间

                    if ($order->pay_status != 6) {
                        $insert_data = [
                            'status' => '6',
                            'pay_status' => 6,
                            'pay_status_desc' => '退款成功',
                            'refund_no' => $wftpay_refund_no,
                            'refund_amount' => $wftpay_refund_amount
                        ];
                        if ($pay_time) $insert_data['pay_time'] = $pay_time;
                        $this->update_day_order($insert_data, $out_trade_no, '');

                        //退款成功后的动作
                        $refund_order_obj = RefundOrder::where('out_trade_no', $out_trade_no)->first();
                        if (!$refund_order_obj) {
                            $return_data = [
                                'out_trade_no' => $out_trade_no,
                                'trade_no' => $order->trade_no,
                                'store_id' => $order->store_id,
                                'merchant_id' => $order->merchant_id,
                                'type' => $order->ways_type,
                                'ways_source' => $order->ways_source,
                                'refund_amount' => $wftpay_refund_amount,
                                'refund_no' => $wftpay_refund_no ?? $wftpay_trade_no
                            ];
                            RefundOrder::created($return_data);
                        }
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $wftpay_trade_no;
                    $re_data['refundFee'] = '' . $wftpay_refund_amount . '';
                    $re_data['totalFee'] = $wftpay_total_amount;
                    $re_data['refundTime'] = '' . $wftpay_return_time . '';
                    $re_data['refund_count'] = '1';
                    return json_encode($re_data);
                } else {
                    //其他情况
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '退款失败';
                    $re_data['refundNo'] = $refundNo;
                }
            }

            //汇旺财 退款查询
            if (27999 < $type && $type < 28999) {
                $config = new HwcPayConfigController();
                $hwcpay_config = $config->hwcpay_config($config_id);
                if (!$hwcpay_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '汇旺财支付配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $hwcpay_merchant = $config->hwcpay_merchant($store_id, $store_pid);
                if (!$hwcpay_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '汇旺财支付商户号不存在';
                    return $this->return_data($re_data);
                }

                $obj = new HwcPayPayController();
                $hwcpay_return_data = [
                    'mch_id' => $hwcpay_merchant->mch_id,
                    'out_trade_no' => $order->out_trade_no,
                    'private_rsa_key' => $hwcpay_config->private_rsa_key,
                    'public_rsa_key' => $hwcpay_config->public_rsa_key
                ];
//                Log::info('汇旺财支付-微收银-退款查询-入参');
//                Log::info($hwcpay_return_data);
                $return = $obj->refund_query($hwcpay_return_data); //0-系统错误 1-成功 2-退款失败 3-退款处理中 4-退款到银行发现用户的卡作废或者冻结了,导致原路退款银行卡失败
//                Log::info('汇旺财支付-微收银-退款查询');
//                Log::info($return);
                //退款请求成功
                if ($return['status'] == 1) {
                    $refund_count = $return['data']['refund_count'] - 1; //退款记录数
                    $hwcpay_refund_no = $return['data']['out_refund_no_' . $refund_count]; //商户退款单号
                    $hwcpay_trade_no = $return['data']['out_refund_id_' . $refund_count]; //第三方退款单号
                    $hwcpay_refund_amount = isset($return['data']['refund_fee_' . $refund_count]) ? ($return['data']['refund_fee_' . $refund_count] / 100) : $order->total_amount; //退款总金额,单位为分,可以做部分退款
                    $hwcpay_total_amount = $order->total_amount; //原消费交易总金额
                    $hwcpay_return_time = $order->pay_time; //交易完成时间
                    $pay_time = isset($return['data']['refund_time_' . $refund_count]) ? date('Y-m-d H:i:s', strtotime($return['data']['refund_time_' . $refund_count])) : ''; //退款时间

                    if ($order->pay_status != 6) {
                        $insert_data = [
                            'status' => '6',
                            'pay_status' => 6,
                            'pay_status_desc' => '退款成功',
                            'refund_no' => $hwcpay_refund_no,
                            'refund_amount' => $hwcpay_refund_amount
                        ];
                        if ($pay_time) $insert_data['pay_time'] = $pay_time;
                        $this->update_day_order($insert_data, $out_trade_no, '');

                        //退款成功后的动作
                        $refund_order_obj = RefundOrder::where('out_trade_no', $out_trade_no)->first();
                        if (!$refund_order_obj) {
                            $return_data = [
                                'out_trade_no' => $out_trade_no,
                                'trade_no' => $order->trade_no,
                                'store_id' => $order->store_id,
                                'merchant_id' => $order->merchant_id,
                                'type' => $order->ways_type,
                                'ways_source' => $order->ways_source,
                                'refund_amount' => $hwcpay_refund_amount,
                                'refund_no' => $hwcpay_refund_no ?? $hwcpay_trade_no
                            ];
                            RefundOrder::created($return_data);
                        }
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $hwcpay_trade_no;
                    $re_data['refundFee'] = '' . $hwcpay_refund_amount . '';
                    $re_data['totalFee'] = $hwcpay_total_amount;
                    $re_data['refundTime'] = '' . $hwcpay_return_time . '';
                    $re_data['refund_count'] = '1';
                    return json_encode($re_data);
                } else {
                    //其他情况
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '退款失败';
                    $re_data['refundNo'] = $refundNo;
                }
            }

            //钱方 退款查询
            if (23999 < $type && $type < 24999) {
                $config = new QfPayConfigController();
                $qfpay_config = $config->qfpay_config($config_id);
                if (!$qfpay_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '钱方支付配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $qfpay_merchant = $config->qfpay_merchant($store_id, $store_pid);
                if (!$qfpay_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '钱方支付商户号不存在';
                    return $this->return_data($re_data);
                }

                $obj = new QfPayController();
                $obj = new QfPayController();
                $qfpay_data = [
                    'mchid' => $qfpay_merchant->mchid,  //商户号
                    'key' => $qfpay_config->key,  //加签key
                    'code' => $qfpay_config->code,  //钱方唯一标识
                    'out_trade_no' => $out_trade_no,  //否,外部订单号查询,开发者平台订单号
                    'syssn' => $order->trade_no  //否,钱方订单号查询,多个以英文逗号区分开
                ];
                Log::info('钱方-微收银-交易查询-入参');
                Log::info($qfpay_data);
                $return = $obj->query($qfpay_data); //1-成功 2-请求下单成功 3-交易中
                Log::info('钱方-微收银-交易查询-结果');
                Log::info($return);
                if ($return['status'] == 1) {
                    $trade_no = isset($return['data']['syssn']) ? $return['data']['syssn'] : ''; //钱方订单号
                    $txamt = isset($return['data']['txamt']) ? $return['data']['txamt'] : 0; //订单支付金额，单位分
                    $refund_amount = number_format($txamt / 100, 2, '.', '');
                    $txdtm = isset($return['data']['txdtm']) ? $return['data']['txdtm'] : ''; //请求交易时间 格式为：YYYY-MM-DD HH:MM:SS
                    $sysdtm = isset($return['data']['sysdtm']) ? $return['data']['sysdtm'] : $txdtm; //系统交易时间
                    $other_no = isset($return['data']['out_trade_no']) ? $return['data']['out_trade_no'] : ''; //外部订单号，开发者平台订单号
                    $pay_type = isset($return['data']['pay_type']) ? $return['data']['pay_type'] : ''; //支付类型,多个以英文逗号区分开,支付宝扫码:800101；支付宝反扫:800108；支付宝服务窗：800107；微信扫码:800201；微信刷卡:800208；微信公众号支付:800207
                    $order_type = isset($return['data']['order_type']) ? $return['data']['order_type'] : ''; //订单类型:支付的订单：payment；退款的订单：refund；关闭的订单：close
                    $cancel = isset($return['data']['cancel']) ? $return['data']['cancel'] : ''; //撤销/退款标记 正常交易：0；已撤销：2；已退货：3
                    $respcd = isset($return['data']['respcd']) ? $return['data']['respcd'] : ''; //支付结果返回码 0000表示交易支付成功；1143、1145表示交易中，需继续查询交易结果； 其他返回码表示失败
                    $errmsg = isset($return['data']['errmsg']) ? $return['data']['errmsg'] : ''; //支付结果描述
                    $cardtp = isset($return['data']['cardtp']) ? $return['data']['cardtp'] : ''; //卡类型 未识别卡=0,借记卡=1,信用卡(贷记卡)=2,准贷记卡=3,储值卡= 4,第三方帐号=5

                    if ($order->pay_status != 6 && $trade_no) {
                        $insert_data = [
                            'status' => '6',
                            'pay_status' => 6,
                            'pay_status_desc' => '退款成功',
                            'refund_no' => $trade_no,
                            'refund_amount' => $order->refund_amount + $refund_amount
                        ];
                        if ($sysdtm) $insert_data['pay_time'] = $sysdtm;
                        $this->update_day_order($insert_data, $out_trade_no, '');

                        //退款成功后的动作
                        $refund_order_obj = RefundOrder::where('out_trade_no', $out_trade_no)->first();
                        if (!$refund_order_obj) {
                            $return_data = [
                                'out_trade_no' => $out_trade_no,
                                'trade_no' => $order->trade_no,
                                'store_id' => $order->store_id,
                                'merchant_id' => $order->merchant_id,
                                'type' => $order->ways_type,
                                'ways_source' => $order->ways_source,
                                'refund_amount' => $refund_amount,
                                'refund_no' => $trade_no
                            ];
                            RefundOrder::created($return_data);
                        }
                    }

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $trade_no;
                    $re_data['refundFee'] = '' . $txamt . '';
                    $re_data['totalFee'] = $order->total_amount * 100;
                    $re_data['refundTime'] = '' . $sysdtm . '';
                    $re_data['refund_count'] = '1';
                    return json_encode($re_data);
                } else {
                    //其他情况
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '退款失败';
                    $re_data['refundNo'] = $refundNo;
                }
            }

            //邮驿付 退款查询
            if (29000 < $type && $type < 29010) {
                //读取配置
                $config = new PostPayConfigController();
                $post_config = $config->post_pay_config($config_id);
                if (!$post_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['return_msg'] = '邮驿付配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $post_merchant = $config->post_pay_merchant($store_id, $store_pid);
                if (!$post_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['return_msg'] = '邮驿付商户号不存在';

                    return $this->return_data($re_data);
                }

                $obj = new \App\Api\Controllers\PostPay\PayController();
                $data['agetId'] = $post_config->org_id;
                $data['custId'] = $post_merchant->cust_id;
                $data['out_trade_no'] = $refundNo;

                $return = $obj->refund_query($data);

                //退款成功
                if ($return["status"] == 1) {
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $refundNo;
                    $re_data['refundTime'] = date('Y-m-d H:i:s', time());
                    $re_data['refund_count'] = '1';
                } //等待付款
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '正在查询退款中';
                    $re_data['outTradeNo'] = $order->qwx_no;

                    return $this->return_data($re_data);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '退款失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                } else {
                    //其他情况
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            //河南畅立收
            if ($store->source == '02') {
                //易生支付 退款查询
                if (32000 < $type && $type < 32010) {
                    $config = new EasySkPayConfigController();
                    $easyskpay_config = $config->easyskpay_config($config_id);
                    if (!$easyskpay_config) {
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = '易生数科支付配置不存在请检查配置';
                        return $this->return_data($re_data);
                    }

                    $easyskpay_merchant = $config->easyskpay_merchant($store_id, $store_pid);
                    if (!$easyskpay_merchant) {
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = '易生数科支付商户号不存在';
                        return $this->return_data($re_data);
                    }
                    $obj = new \App\Api\Controllers\EasySkPay\PayController();
                    $easypay_return_data = [
                        'org_id' => $easyskpay_config->org_id,
                        'mer_id' => $easyskpay_merchant->mer_id,
                        'orig_request_no' => $refundNo
                    ];
//                Log::info('微收银-易生数科支付-退款查询');
//                Log::info($easypay_return_data);
                    $return = $obj->refund_query($easypay_return_data); //-1 系统错误 0-其他 1-成功 2-下单失败
//                Log::info($return);

                    //退款请求成功
                    if ($return['status'] == '1') {
                        $easypay_refund_no = $refundNo;
                        $easypay_refund_amount = $order->total_amount; //退款金额
                        $easypay_total_amount = $order->total_amount; //原消费交易总金额
                        $easypay_return_time = $order->pay_time; //交易完成时间

                        $re_data['result_code'] = 'SUCCESS';
                        $re_data['result_msg'] = '退款成功';
                        $re_data['refundNo'] = $easypay_refund_no;
                        $re_data['refundFee'] = '' . ($easypay_refund_amount * 100) . '';
                        $re_data['totalFee'] = $easypay_total_amount;
                        $re_data['refundTime'] = '' . $easypay_return_time . '';
                        $re_data['refund_count'] = '1';
                        return json_encode($re_data);
                    } else {
                        //其他情况
                        $re_data['result_code'] = 'FALL';
                        $re_data['result_msg'] = '退款失败';
                        $re_data['refundNo'] = $refundNo;
                    }
                }
            }

            //通联支付 退款查询
            if (33000 < $type && $type < 33010) {
                $config = new AllinPayConfigController();
                $allin_config = $config->allin_pay_config($config_id);
                if (!$allin_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '通联支付配置不存在请检查配置';
                    return $this->return_data($re_data);
                }
                $allin_merchant = $config->allin_pay_merchant($store_id, $store_pid);
                if (!$allin_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '通联支付商户号不存在';
                    return $this->return_data($re_data);
                }
                $order = RefundOrder::where('out_trade_no', $out_trade_no)->select('*')->first();

                //退款请求成功
                if ($order->pay_status == '6') {
                    $refund_no = $refundNo;
                    $refund_amount = $order->total_amount; //退款金额
                    $total_amount = $order->total_amount; //原消费交易总金额
                    $return_time = $order->updated_at; //交易完成时间

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $refund_no;
                    $re_data['refundFee'] = '' . ($refund_amount * 100) . '';
                    $re_data['totalFee'] = $total_amount;
                    $re_data['refundTime'] = '' . $return_time . '';
                    $re_data['refund_count'] = '1';
                    return json_encode($re_data);
                } else {
                    //其他情况
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '退款失败';
                    $re_data['refundNo'] = $refundNo;
                }
            }

            //富友支付 退款查询
            if (11000 < $type && $type < 11010) {
                $config = new FuiouConfigController();
                $fuiou_config = $config->fuiou_config($config_id);
                if (!$fuiou_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '富友支付配置不存在请检查配置';
                    return $this->return_data($re_data);
                }
                $fuiou_merchant = $config->fuiou_merchant($store_id, $store_pid);
                if (!$fuiou_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '富友支付商户号不存在';
                    return $this->return_data($re_data);
                }
                $order = RefundOrder::where('out_trade_no', $out_trade_no)->select('*')->first();

                //退款请求成功
                if ($order->pay_status == '6') {
                    $refund_no = $refundNo;
                    $refund_amount = $order->total_amount; //退款金额
                    $total_amount = $order->total_amount; //原消费交易总金额
                    $return_time = $order->updated_at; //交易完成时间

                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $refund_no;
                    $re_data['refundFee'] = '' . ($refund_amount * 100) . '';
                    $re_data['totalFee'] = $total_amount;
                    $re_data['refundTime'] = '' . $return_time . '';
                    $re_data['refund_count'] = '1';
                    return json_encode($re_data);
                } else {
                    //其他情况
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = '退款失败';
                    $re_data['refundNo'] = $refundNo;
                }
            }

            //银盛 退款查询
            if (14000 < $type && $type < 14010) {
                //读取配置
                $config = new YinshengConfigController();
                $yinsheng_config = $config->yinsheng_config($config_id);
                if (!$yinsheng_config) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['return_msg'] = '银盛配置不存在请检查配置';
                    return $this->return_data($re_data);
                }

                $yinsheng_merchant = $config->yinsheng_merchant($store_id, $store_pid);
                if (!$yinsheng_merchant) {
                    $re_data['result_code'] = 'FALL';
                    $re_data['return_msg'] = '银盛商户号不存在';

                    return $this->return_data($re_data);
                }

                $obj = new \App\Api\Controllers\YinSheng\PayController();
                $data['partner_id'] = $yinsheng_config->partner_id;
                $data['out_trade_no'] = $out_trade_no;
                $data['trade_no'] = $order->trade_no;
                $data['out_request_no'] = $refundNo;
                $return = $obj->refund_query($data);

                //退款成功
                if ($return["status"] == 1) {
                    $re_data['result_code'] = 'SUCCESS';
                    $re_data['result_msg'] = '退款成功';
                    $re_data['refundNo'] = $refundNo;
                    $re_data['refundTime'] = date('Y-m-d H:i:s', time());
                    $re_data['refund_count'] = '1';
                } //等待付款
                elseif ($return["status"] == 2) {
                    $re_data['result_code'] = 'USERPAYING';
                    $re_data['result_msg'] = '正在查询退款中';
                    $re_data['outTradeNo'] = $order->qwx_no;

                    return $this->return_data($re_data);
                } //订单失败关闭
                elseif ($return["status"] == 3) {
                    //其他情况
                    $message = '退款失败';
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                } else {
                    //其他情况
                    $message = $return['message'];
                    $re_data['result_code'] = 'FALL';
                    $re_data['result_msg'] = $message;
                }
            }

            return $this->return_data($re_data);
        } catch (\Exception $e) {
            $re_data = [
                'return_code' => 'FALL',
                'return_msg' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
            ];
        }

        return $this->return_data($re_data);
    }


}
