<?php

namespace App\Api\Controllers\Export;


use App\Api\Controllers\BaseController;
use App\Models\MerchantStore;
use App\Models\Order;
use App\Models\Store;
use App\Models\User;
use App\Models\UserRate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ShoppingOrderExportController extends BaseController
{

    //商城订单导出
    public function shoppingOrderExcelDown(Request $request)
    {
        $goods_name = $request->get('goods_name', '');
        $user_id = $request->get('user_id', '');
        $order_id = $request->get('order_id', '');
        $order_status = $request->get('order_status', '');
        $recipient = $request->get('recipient', '');
        $where = [];
        if ($goods_name) {
            $where[] = ['shopping_goods.goods_name', 'like', '%' . $goods_name . '%'];
        }
        if ($user_id) {
            $where[] = ['shopping_order.user_id', '=', $user_id];
        }
        if ($order_id) {
            $where[] = ['shopping_order.order_id', '=', $order_id];
        }
        if ($order_status) {
            $where[] = ['shopping_order.order_status', '=', $order_status];
        }
        if ($recipient) {
            $where[] = ['shopping_order.recipient', '=', $recipient];
        }
        $obj = DB::table('shopping_order');
        $dataArray = $obj->where($where)
            ->leftJoin('shopping_goods', 'shopping_order.goods_id', '=', 'shopping_goods.id')
            ->leftJoin('users', 'shopping_order.user_id', '=', 'users.id')
            ->leftJoin('shopping_category', 'shopping_goods.category_id', '=', 'shopping_category.id')
            ->select('shopping_order.*', 'shopping_goods.goods_name', 'users.name as user_name', 'shopping_category.name as type_name',
                'shopping_goods.pic_url')
            ->orderBy('shopping_order.updated_at', 'desc')
            ->get();
        $tmp = array();
        foreach ($dataArray as $order) {
            if ($order->order_status == "1") {
                $order_status = '支付成功';
            } elseif ($order->order_status == "2") {
                $order_status = '待支付';
            } elseif ($order->order_status == "5") {
                $order_status = '已发货';
            } elseif ($order->order_status == "8") {
                $order_status = '已取消';
            } elseif ($order->order_status == "9") {
                $order_status = '交易完成';
            }
            $tmp[] = array(
                $order->order_id,
                $order->user_name,
                $order->goods_name,
                $order->price,
                $order->total_amount,
                $order->recipient,
                $order->address,
                $order->phone,
                $order_status,
                $order->tracking_code,
                $order->express_name,
                $order->created_at
            );
        }

        $filename = '商城订单导出.csv';
        $tileArray = ['订单号', '下单代理商', '下单商品', '单价', '总金额', '收货人', '收货地址', '收货电话', '订单状态', '快递单号', '快递渠道', '下单时间'];

        return $this->exportToExcel($filename, $tileArray, $tmp);
    }

    public function exportToExcel($filename, $tileArray = [], $dataArray = [])
    {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 0);
        ob_end_clean();
        ob_start();
        header("Content-Type: text/csv");
        header("Content-Disposition:filename=" . $filename);
        $fp = fopen('php://output', 'w');
        fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));//转码 防止乱码(比如微信昵称(乱七八糟的))
        fputcsv($fp, []);
        fputcsv($fp, $tileArray);
        $index = 0;
        foreach ($dataArray as $item) {
            fputcsv($fp, $item);
            $index++;
        }

        ob_flush();
        flush();
        ob_end_clean();
    }


}
