<?php
namespace App\Api\Controllers\Export;


use App\Api\Controllers\BaseController;
use App\Models\MerchantStore;
use App\Models\Order;
use App\Models\Store;
use App\Models\User;
use App\Models\UserRate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class SettlementCommissionExportController extends BaseController
{

    //未结算佣金导出
    public function SettlementCommissionExcelDown(Request $request)
    {
        $token = $this->parseToken();
        $login_user_id = $token->user_id;
        $user_id = $request->get('user_id', $login_user_id);

        $user_name = $request->get('user_name', '');
        $sub_type = $request->get('sub_type', '');
        $level = $request->get('level', '');
        $where = [];
        $where[] = ['u.is_delete', '=', 0];
        if ($user_name) {
            $where[] = ['u.name', 'like', '%' . $user_name . '%'];
        } else {
            if ($level) {
                $where[] = ['u.level', '=', $level];
            }
        }

        if (env('DB_D1_HOST')) {
            $obj = DB::connection("mysql_d1")->table("users as u");
        } else {
            $obj = DB::table('users as u');
        }

        if ($sub_type == "1") {
            $where[] = ['u.level', '<', '2'];
        }
        if ($login_user_id == '1') { //顶级账户
            $dataArray=$obj->where($where)
                ->select('u.id', 'u.pid', 'u.money', 'u.name', 'u.phone', 'u.level',
                    'u.s_code', 'u.logo', 'u.profit_ratio', 'u.sub_profit_ratio', 'u.is_withdraw', 'u.pid_name')
                ->selectRaw('IFNULL((SELECT sum(amount) from user_withdrawals_records WHERE user_id=u.id and status=1),0) as tx_amount')
                ->get();
        } else {
            $dataArray=$obj->where($where)
                ->whereIn("u.id", $this->getSubIds($user_id))
                ->select('u.id', 'u.pid', 'u.money', 'u.name', 'u.phone', 'u.level',
                    'u.s_code', 'u.logo', 'u.profit_ratio', 'u.sub_profit_ratio', 'u.is_withdraw', 'u.pid_name')
                ->selectRaw('IFNULL((SELECT sum(amount) from user_withdrawals_records WHERE user_id=u.id and status=1),0) as tx_amount')
                ->get();
        }
        $tmp = array();
        foreach ($dataArray as $user) {
            if($user->is_withdraw==1){
                $is_withdraw='可提现';
            }else{
                $is_withdraw='不可提现';
            }
            $tmp[] = array(
                $user->name,
                $user->phone,
                $user->sum = $user->money + $user->tx_amount,
                $user->tx_amount,
                $user->money,
                $user->pid_name,
                $user->profit_ratio,
                $is_withdraw
            );
        }

        $filename = '佣金结算详情导出.csv';
        $tileArray = ['代理商名称', '手机号', '累计收益', '已提现金额', '可提现金额', '所属代理商', '利率比例', '提现状态'];

        return $this->exportToExcel($filename, $tileArray, $tmp);
    }
    public function exportToExcel($filename, $tileArray = [], $dataArray = [])
    {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 0);
        ob_end_clean();
        ob_start();
        header("Content-Type: text/csv");
        header("Content-Disposition:filename=" . $filename);
        $fp = fopen('php://output', 'w');
        fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));//转码 防止乱码(比如微信昵称(乱七八糟的))
        fputcsv($fp, []);
        fputcsv($fp, $tileArray);
        $index = 0;
        foreach ($dataArray as $item) {
            fputcsv($fp, $item);
            $index++;
        }

        ob_flush();
        flush();
        ob_end_clean();
    }


}
