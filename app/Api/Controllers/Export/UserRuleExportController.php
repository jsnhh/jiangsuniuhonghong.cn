<?php

namespace App\Api\Controllers\Export;


use App\Api\Controllers\BaseController;
use App\Models\MerchantStore;
use App\Models\Order;
use App\Models\Store;
use App\Models\User;
use App\Models\UserRate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Log;

class UserRuleExportController extends BaseController
{

    //代理返现导出
    public function userRuleExportDown(Request $request)
    {
        $user = $this->parseToken();//
        $user_id = $user->user_id;
        $agent_id = $request->get('agent_id', '');
        $rule_id = $request->get('rule_id', '');
        $status = $request->get('status', '');
        $obj = DB::table('cash_back_rule_user');
        $where = [];
        if($agent_id){
            $where[] = ['cash_back_rule_user.user_id', $agent_id];
        }
        if($rule_id){
            $where[] = ['cash_back_rule_user.rule_id', $rule_id];
        }

        if($status!=""){
            $where[] = ['cash_back_rule_user.status', $status];
        }
        $ids = $this->getSubIds($user_id);
        $dataArray = $obj->where($where)
            ->leftJoin('cash_back_rule', 'cash_back_rule_user.rule_id', '=', 'cash_back_rule.id')
            ->whereIn('cash_back_rule_user.user_id', $ids)
            ->orderBy('cash_back_rule.created_at', 'desc')
            ->select('cash_back_rule_user.*','cash_back_rule.rule_name')
            ->get();;
        $tmp = array();
        foreach ($dataArray as $userRule) {
            if($userRule->status=="1"){
                $status="正常";
            }else{
                $status="停用";
            }
            $tmp[] = array(
                $userRule->rule_name,
                $userRule->user_name,
                $status,
                $userRule->standard_sum,
                $userRule->standard_store_amt,
                $userRule->standard_total_sum,
                $userRule->first_sum,
                $userRule->first_store_amt,
                $userRule->first_total_sum,
                $userRule->again_sum,
                $userRule->again_store_amt,
                $userRule->again_total_sum,
                $userRule->lower_standard_sum,
                $userRule->lower_standard_store_amt,
                $userRule->lower_standard_total_sum,
                $userRule->lower_first_sum,
                $userRule->lower_first_store_amt,
                $userRule->lower_first_total_sum,
                $userRule->lower_again_sum,
                $userRule->lower_again_store_amt,
                $userRule->lower_again_total_sum,
                $userRule->total_sum
            );
        }

        $filename = '代理返现详情表.csv';
        $tileArray = ['规则名称', '代理商名称', '状态', '激活奖励金额', '激活达标商户数量', '激活获得奖励金额', '首月奖励金额', '首月达标商户数量', '首月获得奖励金额',
            '次月奖励金额', '次月达标商户数量', '次月获得奖励金额', '下级激活奖励金额', '下级激活达标商户数量', '下级激活获得奖励金额', '下级首月奖励金额',
            '下级首月达标商户数量', '下级首月获得奖励金额', '下级次月奖励金额', '下级次月达标商户数量', '下级次月获得奖励金额', '获得总奖励金额'];

        return $this->exportToExcel($filename, $tileArray, $tmp);
    }

    public function exportToExcel($filename, $tileArray = [], $dataArray = [])
    {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 0);
        ob_end_clean();
        ob_start();
        header("Content-Type: text/csv");
        header("Content-Disposition:filename=" . $filename);
        $fp = fopen('php://output', 'w');
        fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));//转码 防止乱码(比如微信昵称(乱七八糟的))
        fputcsv($fp, []);
        fputcsv($fp, $tileArray);
        $index = 0;
        foreach ($dataArray as $item) {
            fputcsv($fp, $item);
            $index++;
        }

        ob_flush();
        flush();
        ob_end_clean();
    }


}
