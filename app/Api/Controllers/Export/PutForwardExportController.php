<?php

namespace App\Api\Controllers\Export;


use App\Api\Controllers\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class PutForwardExportController extends BaseController
{

    //提现记录导出
    public function putForwardExcelDown(Request $request)
    {
        $public = $this->parseToken();
        $time_start = $request->get('time_start', ''); //交易时间
        $time_end = $request->get('time_end', ''); //交易时间
        $status = $request->get('status', '');
        $user_id = $request->get('user_id', '');
        $where = [];

        if ($public->type == "merchant") {
            $user_id = $public->merchant_id;
            if($status){
                $where[] = ['merchant_withdrawals_records.status', '=', $status];
            }
            if ($time_start) {
                $where[] = ['merchant_withdrawals_records.updated_at', '>=', $time_start];
            }
            if ($time_end) {
                $where[] = ['merchant_withdrawals_records.updated_at', '<=', $time_end];
            }
            $where[] = ['merchant_id', '=', $user_id];

            if (env('DB_D1_HOST')) {
                $MerchantWithdrawalsRecords = DB::connection("mysql_d1")->table("merchant_withdrawals_records");
            } else {
                $MerchantWithdrawalsRecords = DB::table('merchant_withdrawals_records');
            }

            $data = $MerchantWithdrawalsRecords-> select([
                'merchant_withdrawals_records.*',
                DB::raw("users.name AS `agent_name`")
            ])->where($where)
                ->leftjoin('merchants','merchants.id','=','merchant_withdrawals_records.merchant_id')
                ->leftjoin('users','users.id','=','merchants.user_id')
                ->orderBy('merchant_withdrawals_records.updated_at', 'desc')->get();
        }

        if ($public->type == "user") {
            if($status){
                $where[] = ['user_withdrawals_records.status', '=', $status];
            }
            if ($time_start) {
                $where[] = ['user_withdrawals_records.updated_at', '>=', $time_start];
            }
            if ($time_end) {
                $where[] = ['user_withdrawals_records.updated_at', '<=', $time_end];
            }
            if ($user_id) {
                $users = $this->getSubIds($user_id);
            } else {
                $user_id = $public->user_id;
                $users = $this->getSubIds($public->user_id);

            }
            if (!in_array($user_id, $users)) {
                return json_encode(['status' => 2, 'message' => '非上下级关系']);
            }

            if (env('DB_D1_HOST')) {
                $UserWithdrawalsRecords = DB::connection("mysql_d1")->table("user_withdrawals_records");
            } else {
                $UserWithdrawalsRecords = DB::table('user_withdrawals_records');
            }
            //获取下级所有返佣
            if ($request->get('user_id')) {
                $where[] = ['user_withdrawals_records.user_id', '=', $request->get('user_id')];

                $data = $UserWithdrawalsRecords-> select([
                    'user_withdrawals_records.*',
                    DB::raw("users.name AS `agent_name`")
                ])->where($where)
                    ->leftjoin('users','users.id','=','user_withdrawals_records.user_id')
                    ->orderBy('user_withdrawals_records.updated_at', 'desc')->get();
            } else {
                $data = $UserWithdrawalsRecords-> select([
                    'user_withdrawals_records.*',
                    DB::raw("users.name AS `agent_name`")
                ])->where($where)
                    ->leftjoin('users','users.id','=','user_withdrawals_records.user_id')
                    ->whereIn('user_withdrawals_records.user_id', $this->getSubIds($public->user_id))
                    ->orderBy('user_withdrawals_records.updated_at', 'desc')->get();
            }

        }
        $tmp = array();
        foreach ($data as $putForward) {
            $tmp[] = array(
                $putForward->agent_name,
                $putForward->out_trade_no,
                $putForward->amount,
                $putForward->commission_amount,
                $putForward->reward_amount,
                $putForward->account,
                $putForward->status_desc,
                $putForward->created_at
            );
        }
        $filename = '提现记录明细导出.csv';
        $tileArray = ['代理商名称', '订单号', '提现总金额','提现分润金额','提现奖励金额', '账号', '提现状态', '创建时间'];

        return $this->exportToExcel($filename, $tileArray, $tmp);
    }

    public function exportToExcel($filename, $tileArray = [], $dataArray = [])
    {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 0);
        ob_end_clean();
        ob_start();
        header("Content-Type: text/csv");
        header("Content-Disposition:filename=" . $filename);
        $fp = fopen('php://output', 'w');
        fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));//转码 防止乱码(比如微信昵称(乱七八糟的))
        fputcsv($fp, []);
        fputcsv($fp, $tileArray);
        $index = 0;
        foreach ($dataArray as $item) {
            fputcsv($fp, $item);
            $index++;
        }

        ob_flush();
        flush();
        ob_end_clean();
    }


}
