<?php
namespace App\Api\Controllers\Export;


use App\Api\Controllers\BaseController;
use App\Models\MerchantStore;
use App\Models\Order;
use App\Models\Store;
use App\Models\User;
use App\Models\UserRate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class OrderExportController extends BaseController
{

    //商户导出
    public function MerchantOrderExcelDown(Request $request)
    {
        $token = $this->parseToken();
        $store_id = $request->get('store_id', '');
        $merchant_id = $request->get('merchant_id', '');
        $pay_status = $request->get('pay_status', '');
        $ways_source = $request->get('ways_source', '');
        $ways_type = $request->get('ways_type', '');
        $time_start_s = date('Y-m-d 00:00:00', time());
        $time_start_e = date('Y-m-d 23:59:59', time());

        $time_start = $request->get('time_start', '');
        $time_end = $request->get('time_end', '');

        if (in_array($time_start,[null,''])){
            $time_start=$time_start_s;
        }

        if (in_array($time_end,[null,''])){
            $time_end=$time_start_e;
        }

        $out_trade_no = $request->get('out_trade_no', '');
        $trade_no = $request->get('trade_no', '');
        $where_desc = '';

        //限制时间
        $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
        $day = 31;
        $now_time = date('Y-m-d H:i:s', time());
        $end = date('Y-m-d 22:00:00', time());
        if ($now_time > $end) {
            $day = 31;
        }

        if ($date > $day) {
            echo '时间筛选不能超过' . $day . '天';
            dd();
        }

        //跨天操作
        $time_start_db = date('Ymd', strtotime($time_start));
        $time_end_db = date('Ymd', strtotime($time_end));
        $is_ct_time=0;
        if ($time_start_db != $time_end_db) {
            $is_ct_time=1;
        }

        $MerchantOrderExcelDown = Cache::get('ExcelDown');
        if ($MerchantOrderExcelDown) {
            echo '正在生成数据请1分钟后再来操作';
            die;
        } else {
            Cache::put('ExcelDown', '1', 1);
        }

        $where = [];
        $store_ids = [];
        if ($out_trade_no && $out_trade_no != "undefined") {
            $where[] = ['out_trade_no', 'like', '%' . $out_trade_no . '%'];
            $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
        }

        if ($trade_no && $trade_no != "undefined") {
            $where[] = ['trade_no', 'like', '%' . $trade_no . '%'];
            $where_desc = $where_desc . ';订单编号：' . $trade_no;
        }

        //收银员
        if ($token->merchant_type == 2) {
            $where[] = ['merchant_id', '=', $token->merchant_id];
            $where_desc = $where_desc . ';收银员ID：' . $token->merchant_id;
        }

        //是否传收银员ID
        if ($merchant_id) {
            $where[] = ['merchant_id', '=', $merchant_id];
            $where_desc = $where_desc . ';收银员ID：' . $merchant_id;
        }
        if ($pay_status) {
            $where[] = ['pay_status', '=', $pay_status];
            $where_desc = $where_desc . ';支付状态：' . $pay_status;
        }
        if ($store_id) {
            $store_ids = [
                [
                    'store_id' => $store_id,
                ]
            ];
        } else {
            $MerchantStore = MerchantStore::where('merchant_id', $token->merchant_id)
                ->select('store_id')
                ->get();

            if (!$MerchantStore->isEmpty()) {
                $store_ids = $MerchantStore->toArray();
            }
        }
        if ($ways_source) {
            $where[] = ['ways_source', '=', $ways_source];
            $where_desc = $where_desc . ';支付方式：' . $ways_source;
        }
        if ($ways_type) {
            $where[] = ['ways_type', '=', $ways_type];
            $where_desc = $where_desc . ';支付类型：' . $ways_type;
        }
        if ($time_start) {
            $time_start = date('Y-m-d H:i:s', strtotime($time_start));
            $where[] = ['created_at', '>=', $time_start];
            $where_desc = $where_desc . ';订单开始时间：' . $time_start;
        }
        if ($time_end) {
            $time_end = date('Y-m-d H:i:s', strtotime($time_end));
            $where[] = ['created_at', '<=', $time_end];
            $where_desc = $where_desc . ';订单结束时间：' . $time_end;
        }

        $day = date('Ymd', strtotime($time_start));
        $table = 'orders_' . $day;
        if (env('DB_D1_HOST')) {
            //有没有跨天
            if ($is_ct_time) {
                if (Schema::hasTable('order_items')&&$time_start>="2019-12-21 00:00:00") {
                    $obj = DB::connection("mysql_d1")->table('order_items');
                } else {
                    $obj = DB::connection("mysql_d1")->table('orders');
                }
            } else {
                if (Schema::hasTable($table)) {
                    $obj = DB::connection("mysql_d1")->table($table);
                } else {
                    if (Schema::hasTable('order_items')&&$time_start>="2019-12-21 00:00:00") {
                        $obj = DB::connection("mysql_d1")->table('order_items');
                    } else {
                        $obj = DB::connection("mysql_d1")->table('orders');
                    }
                }
            }
        } else {
            if ($is_ct_time) {
                if (Schema::hasTable('order_items')&&$time_start>="2019-12-21 00:00:00") {
                    $obj = DB::table('order_items');
                } else {
                    $obj = DB::table('orders');
                }
            } else {
                if (Schema::hasTable($table)) {
                    $obj = DB::table($table);
                } else {
                    if (Schema::hasTable('order_items')&&$time_start>="2019-12-21 00:00:00") {
                        $obj = DB::table('order_items');
                    } else {
                        $obj = DB::table('orders');
                    }
                }
            }
        }

        $dataArray = $obj->where($where)
            ->whereIn('store_id', $store_ids)
            ->orderBy('updated_at', 'desc')
            ->select(
                'store_id',
                'store_name',
                'merchant_name',
                'out_trade_no',
                'total_amount',
                'rate',//商户交易时的费率
                'fee_amount',
                'ways_source_desc',
                'pay_status',//系统状态
                'pay_status_desc',
                'pay_time',
                'company',//通道方
                "remark"
            )
            ->get()->map(function ($value) {
                return (array)$value;
            })->toArray();

        $filename = '订单导出.csv';
        $s_array = ['筛选条件：' . $where_desc];
        $tileArray = ['门店ID', '门店名称', '收银员', '订单号', '订单金额', '费率', '手续费', '支付方式', '支付状态', '支付状态说明', '付款时间', '通道', '备注'];

        return $this->exportToExcel($filename, $s_array, $tileArray, $dataArray);
    }


    //服务商导出
    public function UserOrderExcelDown(Request $request)
    {
        $user = $this->parseToken();
        $store_id = $request->get('store_id', '');
        $user_id = $request->get('user_id', '');
        $pay_status = $request->get('pay_status', '');
        $ways_source = $request->get('ways_source', '');
        $company = $request->get('company', '');
        $ways_type = $request->get('ways_type', '');
        $time_start = $request->get('time_start', '');
        $time_end = $request->get('time_end', '');
        $amount_start = $request->get('amount_start', ''); //开始金额
        $amount_end = $request->get('amount_end', ''); //结束金额
        $device_id = $request->get('device_id', ''); //设备id
        $device_name = $request->get('device_name', ''); //设备名称
        $time_start_e = date('Y-m-d 23:59:59', time());
        $time_start_s = date('Y-m-d 00:00:00', time());

        if (in_array($time_start,[null,''])){
            $time_start=$time_start_s;
        }

        if (in_array($time_end,[null,''])){
            $time_end=$time_start_e;
        }

        $out_trade_no = $request->get('out_trade_no', '');
        $trade_no = $request->get('trade_no', '');
        $where_desc = '';
        $where = [];

        //限制时间
        $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
        $day = 31;
        $now_time = date('Y-m-d H:i:s', time());
        $end = date('Y-m-d 22:00:00', time());
        if ($now_time > $end) {
            $day = 31;
        }

        if ($date > $day) {
            echo '时间筛选不能超过' . $day . '天';
            dd();
        }

        //跨天操作
        $time_start_db = date('Ymd', strtotime($time_start));
        $time_end_db = date('Ymd', strtotime($time_end));
        $is_ct_time=0;
        if ($time_start_db != $time_end_db) {
            $is_ct_time=1;
        }

//        $MerchantOrderExcelDown = Cache::get('ExcelDown');
//        if ($MerchantOrderExcelDown) {
//            echo '正在生成数据请1分钟后再来操作';
//            die;
//        } else {
//            Cache::put('ExcelDown', '1', 1);
//        }

        if ($user_id == "") {
            $user_id = $user->user_id;
        }
        $user_ids = $this->getSubIdsAll($user_id);

        $day = date('Ymd', strtotime($time_start));
        $table = 'orders_' . $day;

        if (env('DB_D1_HOST')) {
            //有没有跨天
            if ($is_ct_time) {
                if (Schema::hasTable('order_items')&&$time_start>="2019-12-21 00:00:00") {
                    if ($pay_status) {
                        $where[] = ['order_items.pay_status', '=', $pay_status];
                        $where_desc = $where_desc . ';支付状态：' . $pay_status;
                    }
                    if ($store_id) {
                        $where[] = ['order_items.store_id', '=', $store_id];
                        $where_desc = $where_desc . ';门店ID：' . $store_id;
                    }
                    if ($company) {
                        $where[] = ['order_items.company', '=', $company];
                        $where_desc = $where_desc . ';通道：' . $company;
                    }
                    if ($ways_source) {
                        $where[] = ['order_items.ways_source', '=', $ways_source];
                        $where_desc = $where_desc . ';支付方式：' . $ways_source;
                    }
                    if ($ways_type) {
                        $where[] = ['order_items.ways_type', '=', $ways_type];
                        $where_desc = $where_desc . ';支付类型：' . $ways_type;
                    }
                    if ($time_start) {
                        $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                        $where[] = ['order_items.created_at', '>=', $time_start];
                        $where_desc = $where_desc . ';订单开始时间：' . $time_start;
                    }
                    if ($time_end) {
                        $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                        $where[] = ['order_items.created_at', '<=', $time_end];
                        $where_desc = $where_desc . ';订单结束时间：' . $time_end;
                    }
                    if ($out_trade_no && $out_trade_no != "undefined") {
                        $where[] = ['order_items.out_trade_no', 'like', '%' . $out_trade_no . '%'];
                        $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                    }
                    if ($trade_no && $out_trade_no != "undefined") {
                        $where[] = ['order_items.trade_no', 'like', '%' . $trade_no . '%'];
                        $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                    }
                    if ($amount_start) {
                        $where[] = ['order_items.total_amount', '>=', $amount_start];
                        $where_desc = $where_desc . ';订单金额大于等于：' . $amount_start;
                    }
                    if ($amount_end) {
                        $where[] = ['order_items.total_amount', '<=', $amount_end];
                        $where_desc = $where_desc . ';订单金额小于等于：' . $amount_end;
                    }
                    $obj = DB::connection("mysql_d1")->table('order_items');
                    $dataArray = $obj->where($where)
                        ->join('users', 'order_items.user_id', '=', 'users.id')
                        ->whereIn('order_items.user_id', $user_ids)
                        ->orderBy('order_items.updated_at', 'desc')
                        ->select(
                            'users.name',
                            'order_items.store_id',
                            'order_items.store_name',
                            'order_items.merchant_name',
                            'order_items.out_trade_no',
                            'order_items.total_amount',
                            'order_items.rate',//商户交易时的费率
                            'order_items.fee_amount',
                            'order_items.ways_source_desc',
                            'order_items.pay_status',//系统状态
                            'order_items.pay_status_desc',
                            'order_items.pay_time',
                            'order_items.company',//通道方
                            "order_items.remark"
                        )
                        ->get()->map(function ($value) {
                            return (array)$value;
                        })->toArray();
                } else {
                    $obj = DB::connection("mysql_d1")->table('orders');
                    if ($pay_status) {
                        $where[] = ['orders.pay_status', '=', $pay_status];
                        $where_desc = $where_desc . ';支付状态：' . $pay_status;
                    }
                    if ($store_id) {
                        $where[] = ['orders.store_id', '=', $store_id];
                        $where_desc = $where_desc . ';门店ID：' . $store_id;
                    }
                    if ($company) {
                        $where[] = ['orders.company', '=', $company];
                        $where_desc = $where_desc . ';通道：' . $company;
                    }
                    if ($ways_source) {
                        $where[] = ['orders.ways_source', '=', $ways_source];
                        $where_desc = $where_desc . ';支付方式：' . $ways_source;
                    }
                    if ($ways_type) {
                        $where[] = ['orders.ways_type', '=', $ways_type];
                        $where_desc = $where_desc . ';支付类型：' . $ways_type;
                    }
                    if ($time_start) {
                        $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                        $where[] = ['orders.created_at', '>=', $time_start];
                        $where_desc = $where_desc . ';订单开始时间：' . $time_start;
                    }
                    if ($time_end) {
                        $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                        $where[] = ['orders.created_at', '<=', $time_end];
                        $where_desc = $where_desc . ';订单结束时间：' . $time_end;
                    }
                    if ($out_trade_no && $out_trade_no != "undefined") {
                        $where[] = ['orders.out_trade_no', 'like', '%' . $out_trade_no . '%'];
                        $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                    }
                    if ($trade_no && $out_trade_no != "undefined") {
                        $where[] = ['orders.trade_no', 'like', '%' . $trade_no . '%'];
                        $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                    }
                    if ($amount_start) {
                        $where[] = ['orders.total_amount', '>=', $amount_start];
                        $where_desc = $where_desc . ';订单金额大于等于：' . $amount_start;
                    }
                    if ($amount_end) {
                        $where[] = ['orders.total_amount', '<=', $amount_end];
                        $where_desc = $where_desc . ';订单金额小于等于：' . $amount_end;
                    }
                    $dataArray = $obj->where($where)
                        ->join('users', 'orders.user_id', '=', 'users.id')
                        ->whereIn('orders.user_id', $user_ids)
                        ->orderBy('orders.updated_at', 'desc')
                        ->select(
                            'users.name',
                            'orders.store_id',
                            'orders.store_name',
                            'orders.merchant_name',
                            'orders.out_trade_no',
                            'orders.total_amount',
                            'orders.rate',//商户交易时的费率
                            'orders.fee_amount',
                            'orders.ways_source_desc',
                            'orders.pay_status',//系统状态
                            'orders.pay_status_desc',
                            'orders.pay_time',
                            'orders.company',//通道方
                            "orders.remark"
                        )
                        ->get()->map(function ($value) {
                            return (array)$value;
                        })->toArray();
                }
            } else {
                if (Schema::hasTable($table)) {
                    $obj = DB::connection("mysql_d1")->table($table);
                    if ($pay_status) {
                        $where[] = [ ''.$table.'.pay_status', '=', $pay_status];
                        $where_desc = $where_desc . ';支付状态：' . $pay_status;
                    }
                    if ($store_id) {
                        $where[] = [ ''.$table.'.store_id', '=', $store_id];
                        $where_desc = $where_desc . ';门店ID：' . $store_id;
                    }
                    if ($company) {
                        $where[] = [ ''.$table.'.company', '=', $company];
                        $where_desc = $where_desc . ';通道：' . $company;
                    }
                    if ($ways_source) {
                        $where[] = [ ''.$table.'.ways_source', '=', $ways_source];
                        $where_desc = $where_desc . ';支付方式：' . $ways_source;
                    }
                    if ($ways_type) {
                        $where[] = [ ''.$table.'.ways_type', '=', $ways_type];
                        $where_desc = $where_desc . ';支付类型：' . $ways_type;
                    }
                    if ($time_start) {
                        $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                        $where[] = [ ''.$table.'.created_at', '>=', $time_start];
                        $where_desc = $where_desc . ';订单开始时间：' . $time_start;
                    }
                    if ($time_end) {
                        $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                        $where[] = [ ''.$table.'.created_at', '<=', $time_end];
                        $where_desc = $where_desc . ';订单结束时间：' . $time_end;
                    }
                    if ($out_trade_no && $out_trade_no != "undefined") {
                        $where[] = [ ''.$table.'.out_trade_no', 'like', '%' . $out_trade_no . '%'];
                        $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                    }
                    if ($trade_no && $out_trade_no != "undefined") {
                        $where[] = [ ''.$table.'.trade_no', 'like', '%' . $trade_no . '%'];
                        $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                    }
                    if ($amount_start) {
                        $where[] = [''.$table.'.total_amount', '>=', $amount_start];
                        $where_desc = $where_desc . ';订单金额大于等于：' . $amount_start;
                    }
                    if ($amount_end) {
                        $where[] = [''.$table.'.total_amount', '<=', $amount_end];
                        $where_desc = $where_desc . ';订单金额小于等于：' . $amount_end;
                    }
                    $dataArray = $obj->where($where)
                        ->join('users',   ''.$table.'.user_id', '=', 'users.id')
                        ->whereIn(  ''.$table.'.user_id', $user_ids)
                        ->orderBy(  ''.$table.'.updated_at', 'desc')
                        ->select(
                            'users.name',
                            ''.$table.'.store_id',
                            ''.$table.'.store_name',
                            ''.$table.'.merchant_name',
                            ''.$table.'.out_trade_no',
                            ''.$table.'.total_amount',
                            ''.$table.'.rate',//商户交易时的费率
                            ''.$table.'.fee_amount',
                            ''.$table.'.ways_source_desc',
                            ''.$table.'.pay_status',//系统状态
                            ''.$table.'.pay_status_desc',
                            ''.$table.'.pay_time',
                            ''.$table.'.company',//通道方
                            ''.$table.'.remark'//
                        )
                        ->get()->map(function ($value) {
                            return (array)$value;
                        })->toArray();
                } else {
                    if (Schema::hasTable('order_items')&&$time_start>="2019-12-21 00:00:00") {
                        if ($pay_status) {
                            $where[] = ['order_items.pay_status', '=', $pay_status];
                            $where_desc = $where_desc . ';支付状态：' . $pay_status;
                        }
                        if ($store_id) {
                            $where[] = ['order_items.store_id', '=', $store_id];
                            $where_desc = $where_desc . ';门店ID：' . $store_id;
                        }
                        if ($company) {
                            $where[] = ['order_items.company', '=', $company];
                            $where_desc = $where_desc . ';通道：' . $company;
                        }
                        if ($ways_source) {
                            $where[] = ['order_items.ways_source', '=', $ways_source];
                            $where_desc = $where_desc . ';支付方式：' . $ways_source;
                        }
                        if ($ways_type) {
                            $where[] = ['order_items.ways_type', '=', $ways_type];
                            $where_desc = $where_desc . ';支付类型：' . $ways_type;
                        }
                        if ($time_start) {
                            $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                            $where[] = ['order_items.created_at', '>=', $time_start];
                            $where_desc = $where_desc . ';订单开始时间：' . $time_start;
                        }
                        if ($time_end) {
                            $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                            $where[] = ['order_items.created_at', '<=', $time_end];
                            $where_desc = $where_desc . ';订单结束时间：' . $time_end;
                        }
                        if ($out_trade_no && $out_trade_no != "undefined") {
                            $where[] = ['order_items.out_trade_no', 'like', '%' . $out_trade_no . '%'];
                            $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                        }
                        if ($trade_no && $out_trade_no != "undefined") {
                            $where[] = ['order_items.trade_no', 'like', '%' . $trade_no . '%'];
                            $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                        }
                        if ($amount_start) {
                            $where[] = ['order_items.total_amount', '>=', $amount_start];
                            $where_desc = $where_desc . ';订单金额大于等于：' . $amount_start;
                        }
                        if ($amount_end) {
                            $where[] = ['order_items.total_amount', '<=', $amount_end];
                            $where_desc = $where_desc . ';订单金额小于等于：' . $amount_end;
                        }
                        $obj = DB::connection("mysql_d1")->table('order_items');
                        $dataArray = $obj->where($where)
                            ->join('users', 'order_items.user_id', '=', 'users.id')
                            ->whereIn('order_items.user_id', $user_ids)
                            ->orderBy('order_items.updated_at', 'desc')
                            ->select(
                                'users.name',
                                'order_items.store_id',
                                'order_items.store_name',
                                'order_items.merchant_name',
                                'order_items.out_trade_no',
                                'order_items.total_amount',
                                'order_items.rate',//商户交易时的费率
                                'order_items.fee_amount',
                                'order_items.ways_source_desc',
                                'order_items.pay_status',//系统状态
                                'order_items.pay_status_desc',
                                'order_items.pay_time',
                                'order_items.company',//通道方
                                "order_items.remark"
                            )
                            ->get()->map(function ($value) {
                                return (array)$value;
                            })->toArray();
                    } else {
                        $obj = DB::connection("mysql_d1")->table("orders");
                        if ($pay_status) {
                            $where[] = ['orders.pay_status', '=', $pay_status];
                            $where_desc = $where_desc . ';支付状态：' . $pay_status;
                        }
                        if ($store_id) {
                            $where[] = ['orders.store_id', '=', $store_id];
                            $where_desc = $where_desc . ';门店ID：' . $store_id;
                        }
                        if ($company) {
                            $where[] = ['orders.company', '=', $company];
                            $where_desc = $where_desc . ';通道：' . $company;
                        }
                        if ($ways_source) {
                            $where[] = ['orders.ways_source', '=', $ways_source];
                            $where_desc = $where_desc . ';支付方式：' . $ways_source;
                        }
                        if ($ways_type) {
                            $where[] = ['orders.ways_type', '=', $ways_type];
                            $where_desc = $where_desc . ';支付类型：' . $ways_type;
                        }
                        if ($time_start) {
                            $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                            $where[] = ['orders.created_at', '>=', $time_start];
                            $where_desc = $where_desc . ';订单开始时间：' . $time_start;
                        }
                        if ($time_end) {
                            $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                            $where[] = ['orders.created_at', '<=', $time_end];
                            $where_desc = $where_desc . ';订单结束时间：' . $time_end;
                        }
                        if ($out_trade_no && $out_trade_no != "undefined") {
                            $where[] = ['orders.out_trade_no', 'like', '%' . $out_trade_no . '%'];
                            $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                        }
                        if ($trade_no && $out_trade_no != "undefined") {
                            $where[] = ['orders.trade_no', 'like', '%' . $trade_no . '%'];
                            $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                        }
                        if ($amount_start) {
                            $where[] = ['orders.total_amount', '>=', $amount_start];
                            $where_desc = $where_desc . ';订单金额大于等于：' . $amount_start;
                        }
                        if ($amount_end) {
                            $where[] = ['orders.total_amount', '<=', $amount_end];
                            $where_desc = $where_desc . ';订单金额小于等于：' . $amount_end;
                        }
                        $dataArray = $obj->where($where)
                            ->join('users', 'orders.user_id', '=', 'users.id')
                            ->whereIn('orders.user_id', $user_ids)
                            ->orderBy('orders.updated_at', 'desc')
                            ->select(
                                'users.name',
                                'orders.store_id',
                                'orders.store_name',
                                'orders.merchant_name',
                                'orders.out_trade_no',
                                'orders.total_amount',
                                'orders.rate',//商户交易时的费率
                                'orders.fee_amount',
                                'orders.ways_source_desc',
                                'orders.pay_status',//系统状态
                                'orders.pay_status_desc',
                                'orders.pay_time',
                                'orders.company',//通道方
                                "orders.remark"
                            )
                            ->get()->map(function ($value) {
                                return (array)$value;
                            })->toArray();
                    }
                }
            }
        } else {
            if ($is_ct_time) {
                if (Schema::hasTable('order_items')&&$time_start>="2019-12-21 00:00:00") {
                    $obj = DB::table('order_items');
                    if ($pay_status) {
                        $where[] = ['order_items.pay_status', '=', $pay_status];
                        $where_desc = $where_desc . ';支付状态：' . $pay_status;
                    }
                    if ($store_id) {
                        $where[] = ['order_items.store_id', '=', $store_id];
                        $where_desc = $where_desc . ';门店ID：' . $store_id;
                    }
                    if ($company) {
                        $where[] = ['order_items.company', '=', $company];
                        $where_desc = $where_desc . ';通道：' . $company;
                    }
                    if ($ways_source) {
                        $where[] = ['order_items.ways_source', '=', $ways_source];
                        $where_desc = $where_desc . ';支付方式：' . $ways_source;
                    }
                    if ($ways_type) {
                        $where[] = ['order_items.ways_type', '=', $ways_type];
                        $where_desc = $where_desc . ';支付类型：' . $ways_type;
                    }
                    if ($time_start) {
                        $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                        $where[] = ['order_items.created_at', '>=', $time_start];
                        $where_desc = $where_desc . ';订单开始时间：' . $time_start;
                    }
                    if ($time_end) {
                        $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                        $where[] = ['order_items.created_at', '<=', $time_end];
                        $where_desc = $where_desc . ';订单结束时间：' . $time_end;
                    }
                    if ($out_trade_no && $out_trade_no != "undefined") {
                        $where[] = ['order_items.out_trade_no', 'like', '%' . $out_trade_no . '%'];
                        $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                    }
                    if ($trade_no && $out_trade_no != "undefined") {
                        $where[] = ['order_items.trade_no', 'like', '%' . $trade_no . '%'];
                        $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                    }
                    if ($amount_start) {
                        $where[] = ['order_items.total_amount', '>=', $amount_start];
                        $where_desc = $where_desc . ';订单金额大于等于：' . $amount_start;
                    }
                    if ($amount_end) {
                        $where[] = ['order_items.total_amount', '<=', $amount_end];
                        $where_desc = $where_desc . ';订单金额小于等于：' . $amount_end;
                    }
                    $dataArray = $obj->where($where)
                        ->join('users', 'order_items.user_id', '=', 'users.id')
                        ->whereIn('order_items.user_id', $user_ids)
                        ->orderBy('order_items.updated_at', 'desc')
                        ->select(
                            'users.name',
                            'order_items.store_id',
                            'order_items.store_name',
                            'order_items.merchant_name',
                            'order_items.out_trade_no',
                            'order_items.total_amount',
                            'order_items.rate',//商户交易时的费率
                            'order_items.fee_amount',
                            'order_items.ways_source_desc',
                            'order_items.pay_status',//系统状态
                            'order_items.pay_status_desc',
                            'order_items.pay_time',
                            'order_items.company',//通道方
                            "order_items.remark"
                        )
                        ->get()->map(function ($value) {
                            return (array)$value;
                        })->toArray();
                } else {
                    $obj = DB::table('orders');
                    if ($pay_status) {
                        $where[] = ['orders.pay_status', '=', $pay_status];
                        $where_desc = $where_desc . ';支付状态：' . $pay_status;
                    }
                    if ($store_id) {
                        $where[] = ['orders.store_id', '=', $store_id];
                        $where_desc = $where_desc . ';门店ID：' . $store_id;
                    }
                    if ($company) {
                        $where[] = ['orders.company', '=', $company];
                        $where_desc = $where_desc . ';通道：' . $company;
                    }
                    if ($ways_source) {
                        //alipay_face_dis_day支付宝刷脸单日去重 weixin_face_dis_day微信刷脸单日去重
//                        if ($ways_source == 'alipay_face_dis_day') {
//                            $where[] = ['orders.pay_method', '=', 'alipay_face'];
//                        } elseif ($ways_source == 'weixin_face_dis_day') {
//                            $where[] = ['orders.pay_method', '=', 'weixin_face'];
//                        } else {
//                        }
                        if (in_array($ways_source, ['alipay_face', 'weixin_face'])) {
                            $where[] = ['orders.pay_method', '=', $ways_source];
                        } else {
                            $where[] = ['orders.ways_source', '=', $ways_source];
                        }
                        $where_desc = $where_desc . ';支付方式：' . $ways_source;
                    }
                    if ($ways_type) {
                        $where[] = ['orders.ways_type', '=', $ways_type];
                        $where_desc = $where_desc . ';支付类型：' . $ways_type;
                    }
                    if ($time_start) {
                        $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                        $where[] = ['orders.created_at', '>=', $time_start];
                        $where_desc = $where_desc . ';订单开始时间：' . $time_start;
                    }
                    if ($time_end) {
                        $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                        $where[] = ['orders.created_at', '<=', $time_end];
                        $where_desc = $where_desc . ';订单结束时间：' . $time_end;
                    }
                    if ($out_trade_no && $out_trade_no != "undefined") {
                        $where[] = ['orders.out_trade_no', 'like', '%' . $out_trade_no . '%'];
                        $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                    }
                    if ($trade_no && $out_trade_no != "undefined") {
                        $where[] = ['orders.trade_no', 'like', '%' . $trade_no . '%'];
                        $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                    }
                    if ($amount_start) {
                        $where[] = ['orders.total_amount', '>=', $amount_start];
                        $where_desc = $where_desc . ';订单金额大于等于：' . $amount_start;
                    }
                    if ($amount_end) {
                        $where[] = ['orders.total_amount', '<=', $amount_end];
                        $where_desc = $where_desc . ';订单金额小于等于：' . $amount_end;
                    }
                    if ($device_id) {
                        $where[] = ['orders.device_id', '=', $device_id];
                        $where_desc = $where_desc . ';设备号：' . $device_id;
                    }
                    if ($device_name) {
                        $where[] = ['orders.device_name', '=', $device_name];
                        $where_desc = $where_desc . ';设备类型：' . $device_name;
                    }

//                    if ($ways_source && in_array($ways_source, ['alipay_face_dis_day', 'weixin_face_dis_day'])) {
//                        $dataArray = $obj->select(
//                            'users.name',
//                            'orders.store_id',
//                            'orders.store_name',
////                            'orders.merchant_name',
////                            'orders.out_trade_no',
////                            'orders.total_amount',
//                            'orders.rate',
////                            'orders.fee_amount',
//                            'orders.ways_source_desc',
//                            'orders.pay_status',
//                            'orders.pay_status_desc',
//                            DB::raw("date_format(orders.pay_time, '%Y-%m-%d') as pay_time"),
//                            'orders.company',
//                            'orders.remark',
////                            'orders.device_id',
//                            DB::raw("count(orders.id) as count_num"),
//                            DB::raw("sum(orders.total_amount) as sum_amount")
//                        )
//                            ->where($where)
//                            ->join('users', 'orders.user_id', '=', 'users.id')
//                            ->distinct('orders.buyer_id')
//                            ->whereIn('orders.user_id', $user_ids)
//                            ->orderBy('orders.updated_at', 'desc')
//                            ->groupBy(DB::raw("date_format(orders.pay_time, '%Y-%m-%d')"), 'orders.store_id', 'orders.pay_status')
//                            ->get()
//                            ->map(function ($value) {
//                                return (array)$value;
//                            })
//                            ->toArray();
//                    } else {
                    $dataArray = $obj->where($where)
                        ->join('users', 'orders.user_id', '=', 'users.id')
                        ->whereIn('orders.user_id', $user_ids)
                        ->orderBy('orders.updated_at', 'desc')
                        ->select(
                            'users.name',
                            'orders.store_id',
                            'orders.store_name',
                            'orders.merchant_name',
                            'orders.out_trade_no',
                            'orders.total_amount',
                            'orders.rate', //商户交易时的费率
                            'orders.fee_amount',
                            'orders.ways_source_desc',
                            'orders.pay_status', //系统状态
                            'orders.pay_status_desc',
                            'orders.pay_time',
                            'orders.company', //通道方
                            'orders.remark',
                            'orders.device_id',
                            'orders.device_name'
                        )
                        ->get()->map(function ($value) {
                            return (array)$value;
                        })->toArray();
                }
//                }
            } else {
                if (Schema::hasTable($table)) {
                    $obj = DB::table($table);
                    if ($pay_status) {
                        $where[] = [ ''.$table.'.pay_status', '=', $pay_status];
                        $where_desc = $where_desc . ';支付状态：' . $pay_status;
                    }
                    if ($store_id) {
                        $where[] = [ ''.$table.'.store_id', '=', $store_id];
                        $where_desc = $where_desc . ';门店ID：' . $store_id;
                    }
                    if ($company) {
                        $where[] = [ ''.$table.'.company', '=', $company];
                        $where_desc = $where_desc . ';通道：' . $company;
                    }
                    if ($ways_source) {
                        $where[] = [ ''.$table.'.ways_source', '=', $ways_source];
                        $where_desc = $where_desc . ';支付方式：' . $ways_source;
                    }
                    if ($ways_type) {
                        $where[] = [ ''.$table.'.ways_type', '=', $ways_type];
                        $where_desc = $where_desc . ';支付类型：' . $ways_type;
                    }
                    if ($time_start) {
                        $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                        $where[] = [ ''.$table.'.created_at', '>=', $time_start];
                        $where_desc = $where_desc . ';订单开始时间：' . $time_start;
                    }
                    if ($time_end) {
                        $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                        $where[] = [ ''.$table.'.created_at', '<=', $time_end];
                        $where_desc = $where_desc . ';订单结束时间：' . $time_end;
                    }
                    if ($out_trade_no && $out_trade_no != "undefined") {
                        $where[] = [ ''.$table.'.out_trade_no', 'like', '%' . $out_trade_no . '%'];
                        $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                    }
                    if ($trade_no && $out_trade_no != "undefined") {
                        $where[] = [ ''.$table.'.trade_no', 'like', '%' . $trade_no . '%'];
                        $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                    }
                    if ($amount_start) {
                        $where[] = [''.$table.'total_amount', '>=', $amount_start];
                        $where_desc = $where_desc . ';订单金额大于等于：' . $amount_start;
                    }
                    if ($amount_end) {
                        $where[] = [''.$table.'total_amount', '<=', $amount_end];
                        $where_desc = $where_desc . ';订单金额小于等于：' . $amount_end;
                    }
                    $dataArray = $obj->where($where)
                        ->join('users',   ''.$table.'.user_id', '=', 'users.id')
                        ->whereIn(  ''.$table.'.user_id', $user_ids)
                        ->orderBy(  ''.$table.'.updated_at', 'desc')
                        ->select(
                            'users.name',
                            ''.$table.'.store_id',
                            ''.$table.'.store_name',
                            ''.$table.'.merchant_name',
                            ''.$table.'.out_trade_no',
                            ''.$table.'.total_amount',
                            ''.$table.'.rate',//商户交易时的费率
                            ''.$table.'.fee_amount',
                            ''.$table.'.ways_source_desc',
                            ''.$table.'.pay_status',//系统状态
                            ''.$table.'.pay_status_desc',
                            ''.$table.'.pay_time',
                            ''.$table.'.company',//通道方
                            ''.$table.'.remark'//
                        )
                        ->get()->map(function ($value) {
                            return (array)$value;
                        })->toArray();
                } else {
                    if (Schema::hasTable('order_items')&&$time_start>="2019-12-21 00:00:00") {
                        $obj = DB::table('order_items');
                        if ($pay_status) {
                            $where[] = ['order_items.pay_status', '=', $pay_status];
                            $where_desc = $where_desc . ';支付状态：' . $pay_status;
                        }
                        if ($store_id) {
                            $where[] = ['order_items.store_id', '=', $store_id];
                            $where_desc = $where_desc . ';门店ID：' . $store_id;
                        }
                        if ($company) {
                            $where[] = ['order_items.company', '=', $company];
                            $where_desc = $where_desc . ';通道：' . $company;
                        }
                        if ($ways_source) {
                            $where[] = ['order_items.ways_source', '=', $ways_source];
                            $where_desc = $where_desc . ';支付方式：' . $ways_source;
                        }
                        if ($ways_type) {
                            $where[] = ['order_items.ways_type', '=', $ways_type];
                            $where_desc = $where_desc . ';支付类型：' . $ways_type;
                        }
                        if ($time_start) {
                            $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                            $where[] = ['order_items.created_at', '>=', $time_start];
                            $where_desc = $where_desc . ';订单开始时间：' . $time_start;
                        }
                        if ($time_end) {
                            $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                            $where[] = ['order_items.created_at', '<=', $time_end];
                            $where_desc = $where_desc . ';订单结束时间：' . $time_end;
                        }
                        if ($out_trade_no && $out_trade_no != "undefined") {
                            $where[] = ['order_items.out_trade_no', 'like', '%' . $out_trade_no . '%'];
                            $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                        }
                        if ($trade_no && $out_trade_no != "undefined") {
                            $where[] = ['order_items.trade_no', 'like', '%' . $trade_no . '%'];
                            $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                        }
                        if ($amount_start) {
                            $where[] = ['order_items.total_amount', '>=', $amount_start];
                            $where_desc = $where_desc . ';订单金额大于等于：' . $amount_start;
                        }
                        if ($amount_end) {
                            $where[] = ['order_items.total_amount', '<=', $amount_end];
                            $where_desc = $where_desc . ';订单金额小于等于：' . $amount_end;
                        }
                        $dataArray = $obj->where($where)
                            ->join('users', 'order_items.user_id', '=', 'users.id')
                            ->whereIn('order_items.user_id', $user_ids)
                            ->orderBy('order_items.updated_at', 'desc')
                            ->select(
                                'users.name',
                                'order_items.store_id',
                                'order_items.store_name',
                                'order_items.merchant_name',
                                'order_items.out_trade_no',
                                'order_items.total_amount',
                                'order_items.rate',//商户交易时的费率
                                'order_items.fee_amount',
                                'order_items.ways_source_desc',
                                'order_items.pay_status',//系统状态
                                'order_items.pay_status_desc',
                                'order_items.pay_time',
                                'order_items.company',//通道方
                                "order_items.remark"
                            )
                            ->get()->map(function ($value) {
                                return (array)$value;
                            })->toArray();
                    } else {
                        $obj = DB::table('orders');
                        if ($pay_status) {
                            $where[] = ['orders.pay_status', '=', $pay_status];
                            $where_desc = $where_desc . ';支付状态：' . $pay_status;
                        }
                        if ($store_id) {
                            $where[] = ['orders.store_id', '=', $store_id];
                            $where_desc = $where_desc . ';门店ID：' . $store_id;
                        }
                        if ($company) {
                            $where[] = ['orders.company', '=', $company];
                            $where_desc = $where_desc . ';通道：' . $company;
                        }
                        if ($ways_source) {
                            //alipay_face_dis_day支付宝刷脸单日去重 weixin_face_dis_day微信刷脸单日去重
//                            if ($ways_source == 'alipay_face_dis_day') {
//                                $where[] = ['orders.pay_method', '=', 'alipay_face'];
//                            } elseif ($ways_source == 'weixin_face_dis_day') {
//                                $where[] = ['orders.pay_method', '=', 'weixin_face'];
//                            } else {

//                            }
                            if (in_array($ways_source, ['alipay_face', 'weixin_face'])) {
                                $where[] = ['orders.pay_method', '=', $ways_source];
                            } else {
                                $where[] = ['orders.ways_source', '=', $ways_source];
                            }
                            $where_desc = $where_desc . ';支付方式：' . $ways_source;
                        }
                        if ($ways_type) {
                            $where[] = ['orders.ways_type', '=', $ways_type];
                            $where_desc = $where_desc . ';支付类型：' . $ways_type;
                        }
                        if ($time_start) {
                            $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                            $where[] = ['orders.created_at', '>=', $time_start];
                            $where_desc = $where_desc . ';订单开始时间：' . $time_start;
                        }
                        if ($time_end) {
                            $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                            $where[] = ['orders.created_at', '<=', $time_end];
                            $where_desc = $where_desc . ';订单结束时间：' . $time_end;
                        }
                        if ($out_trade_no && $out_trade_no != "undefined") {
                            $where[] = ['orders.out_trade_no', 'like', '%' . $out_trade_no . '%'];
                            $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                        }
                        if ($trade_no && $out_trade_no != "undefined") {
                            $where[] = ['orders.trade_no', 'like', '%' . $trade_no . '%'];
                            $where_desc = $where_desc . ';订单编号：' . $out_trade_no;
                        }
                        if ($amount_start) {
                            $where[] = ['orders.total_amount', '>=', $amount_start];
                            $where_desc = $where_desc . ';订单金额大于等于：' . $amount_start;
                        }
                        if ($amount_end) {
                            $where[] = ['orders.total_amount', '<=', $amount_end];
                            $where_desc = $where_desc . ';订单金额小于等于：' . $amount_end;
                        }
                        if ($device_id) {
                            $where[] = ['orders.device_id', '=', $device_id];
                            $where_desc = $where_desc . ';设备号：' . $device_id;
                        }
                        if ($device_name) {
                            $where[] = ['orders.device_name', '=', $device_name];
                            $where_desc = $where_desc . ';设备类型：' . $device_name;
                        }

//                        if ($ways_source && in_array($ways_source, ['alipay_face_dis_day', 'weixin_face_dis_day'])) {
//                            $dataArray = $obj->select(
//                                'users.name',
//                                'orders.store_id',
//                                'orders.store_name',
////                                'orders.merchant_name',
////                                'orders.out_trade_no',
////                                'orders.total_amount',
//                                'orders.rate',
////                                'orders.fee_amount',
//                                'orders.ways_source_desc',
//                                'orders.pay_status',
//                                'orders.pay_status_desc',
//                                DB::raw("date_format(orders.pay_time, '%Y-%m-%d') as pay_time"),
//                                'orders.company',
//                                'orders.remark',
////                                'orders.device_id',
//                                DB::raw("count(orders.buyer_id) as count_num"),
//                                DB::raw("sum(orders.total_amount) as sum_amount")
//                            )
//                                ->where($where)
//                                ->join('users', 'orders.user_id', '=', 'users.id')
//                                ->distinct('orders.buyer_id')
//                                ->whereIn('orders.user_id', $user_ids)
//                                ->orderBy('orders.updated_at', 'desc')
//                                ->groupBy(DB::raw("date_format(orders.pay_time, '%Y-%m-%d')"), 'orders.store_id', 'orders.pay_status')
//                                ->get()
//                                ->map(function ($value) {
//                                    return (array)$value;
//                                })
//                                ->toArray();
//                        } else {
                        $dataArray = $obj->where($where)
                            ->join('users', 'orders.user_id', '=', 'users.id')
                            ->whereIn('orders.user_id', $user_ids)
                            ->orderBy('orders.updated_at', 'desc')
                            ->select(
                                'users.name',
                                'orders.store_id',
                                'orders.store_name',
                                'orders.merchant_name',
                                'orders.out_trade_no',
                                'orders.total_amount',
                                'orders.rate',//商户交易时的费率
                                'orders.fee_amount',
                                'orders.ways_source_desc',
                                'orders.pay_status',//系统状态
                                'orders.pay_status_desc',
                                'orders.pay_time',
                                'orders.company',//通道方
                                'orders.remark',
                                'orders.device_id',
                                'orders.device_name'
                            )
                            ->get()->map(function ($value) {
                                return (array)$value;
                            })->toArray();
                    }
//                    }
                }
            }
        }

        $filename = '订单导出.csv';
        $s_array = ['筛选条件：' . $where_desc];
        $tileArray = ['代理商ID', '门店ID', '门店名称', '收银员', '订单号', '订单金额', '费率', '手续费', '支付方式', '支付状态', '支付状态说明', '付款时间', '通道', '备注', '设备号','设备类型'];
//        if ($ways_source && in_array($ways_source, ['alipay_face_dis_day', 'weixin_face_dis_day'])) {
//            $tileArray = ['代理商ID', '门店ID', '门店名称', '费率', '支付方式', '支付状态', '支付状态说明', '付款时间', '通道', '备注', '笔数', '交易额'];
//        } else {
//        }

        return $this->exportToExcel($filename, $s_array, $tileArray, $dataArray);
    }


    public function exportToExcel($filename, $s_array, $tileArray = [], $dataArray = [])
    {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 0);
        ob_end_clean();
        ob_start();
        header("Content-Type: text/csv");
        header("Content-Disposition:filename=" . $filename);
        $fp = fopen('php://output', 'w');
        fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));//转码 防止乱码(比如微信昵称(乱七八糟的))

        fputcsv($fp, $s_array);
        fputcsv($fp, []);
        fputcsv($fp, $tileArray);
        $index = 0;
        foreach ($dataArray as $item) {
            $item['rate'] = $item['rate'] / 100;
            $index++;
            fputcsv($fp, $item);
        }

        ob_flush();
        flush();
        ob_end_clean();
    }


    //商户对账统计导出
    public function MerchantOrderCountExcelDown(Request $request)
    {
        $merchant = $this->parseToken();
        $store_id = $request->get('store_id', '');
        $merchant_id = $request->get('merchant_id', '');
        $time_start_s = date('Y-m-d 00:00:00', time());
        $time_start_e = date('Y-m-d 23:59:59', time());

        $time_start = $request->get('time_start', '');
        $time_end = $request->get('time_end', '');

        if (in_array($time_start,[null,''])){
            $time_start=$time_start_s;
        }

        if (in_array($time_end,[null,''])){
            $time_end=$time_start_e;
        }
        $count_data = $request->get('count_data', '');
        $where_desc = '';
        $store_name = "全部门店";

        //限制时间
        $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
        $day = 2;
        $now_time = date('Y-m-d H:i:s', time());
        $end = date('Y-m-d 22:00:00', time());
        if ($now_time > $end) {
            $day = 31;
        }

        if ($date > $day) {
            echo '时间筛选不能超过' . $day . '天';
        }

        //跨天操作
        $time_start_db = date('Ymd', strtotime($time_start));
        $time_end_db = date('Ymd', strtotime($time_end));
        $is_ct_time=0;
        if ($time_start_db != $time_end_db) {
            $is_ct_time=1;
        }

        $MerchantOrderExcelDown = Cache::get('ExcelDown');
        if ($MerchantOrderExcelDown) {
            echo '正在生成数据请1分钟后再来操作';
            die;
        } else {
            Cache::put('ExcelDown', '1', 1);
        }

        if ($store_id) {
            $store = Store::where('store_id', $store_id)
                ->select('store_short_name')
                ->first();

            if ($store) {
                $store_name = $store->store_short_name;
            }
        }

        $count_data = json_decode($count_data, true);

        $filename = '订单导出.csv';
        $tileArray = [
            '时间',
            '门店名称',
            '支付渠道',
            '交易金额',
            '交易笔数',
            '商家实收',
            '实际净额',
            '结算服务费/手续费',
            '退款金额',
            '退款笔数'

        ];
        $data = [
            [
                'time' => $time_start . '-' . $time_end,
                'store_name' => $store_name,
                'ways_type' => '支付宝',
                'total_amount' => $count_data['alipay_total_amount'],
                'total_count' => $count_data['alipay_total_count'],
                'alipay_get_amount' => $count_data['alipay_get_amount'],
                'receipt_amount' => $count_data['alipay_receipt_amount'],
                'fee_amount' => $count_data['alipay_fee_amount'],
                'refund_amount' => $count_data['alipay_refund_amount'],
                'refund_count' => $count_data['alipay_refund_count'],
            ],
            [
                'time' => $time_start . '-' . $time_end,
                'store_name' => $store_name,
                'ways_type' => '微信支付',
                'total_amount' => $count_data['weixin_total_amount'],
                'total_count' => $count_data['weixin_total_count'],
                'alipay_get_amount' => $count_data['weixin_get_amount'],
                'receipt_amount' => $count_data['weixin_receipt_amount'],
                'fee_amount' => $count_data['weixin_fee_amount'],
                'refund_amount' => $count_data['weixin_refund_amount'],
                'refund_count' => $count_data['weixin_refund_count'],
            ],
            [
                'time' => $time_start . '-' . $time_end,
                'store_name' => $store_name,
                'ways_type' => '京东',
                'total_amount' => $count_data['jd_total_amount'],
                'total_count' => $count_data['jd_total_count'],
                'alipay_get_amount' => $count_data['jd_get_amount'],
                'receipt_amount' => $count_data['jd_receipt_amount'],
                'fee_amount' => $count_data['jd_fee_amount'],
                'refund_amount' => $count_data['jd_refund_amount'],
                'refund_count' => $count_data['jd_refund_count'],
            ],
            [
                'time' => $time_start . '-' . $time_end,
                'store_name' => $store_name,
                'ways_type' => '刷卡',
                'total_amount' => $count_data['un_total_amount'],
                'total_count' => $count_data['un_total_count'],
                'alipay_get_amount' => $count_data['un_get_amount'],
                'receipt_amount' => $count_data['un_receipt_amount'],
                'fee_amount' => $count_data['un_fee_amount'],
                'refund_amount' => $count_data['un_refund_amount'],
                'refund_count' => $count_data['un_refund_count'],
            ],
            [
                'time' => $time_start . '-' . $time_end,
                'store_name' => $store_name,
                'ways_type' => '云闪付',
                'total_amount' => $count_data['unqr_total_amount'],
                'total_count' => $count_data['unqr_total_count'],
                'alipay_get_amount' => $count_data['unqr_get_amount'],
                'receipt_amount' => $count_data['unqr_receipt_amount'],
                'fee_amount' => $count_data['unqr_fee_amount'],
                'refund_amount' => $count_data['unqr_refund_amount'],
                'refund_count' => $count_data['unqr_refund_count'],
            ],

            [
                'time' => $time_start . '-' . $time_end,
                'store_name' => $store_name,
                'ways_type' => '总计',
                'total_amount' => $count_data['total_amount'],
                'total_count' => $count_data['total_count'],
                'alipay_get_amount' => $count_data['get_amount'],
                'receipt_amount' => $count_data['receipt_amount'],
                'fee_amount' => $count_data['fee_amount'],
                'refund_amount' => $count_data['refund_amount'],
                'refund_count' => $count_data['refund_count'],
            ],
        ];
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 0);
        ob_end_clean();
        ob_start();
        header("Content-Type: text/csv");
        header("Content-Disposition:filename=" . $filename);
        $fp = fopen('php://output', 'w');
        fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));//转码 防止乱码(比如微信昵称(乱七八糟的))
        fputcsv($fp, []);
        fputcsv($fp, $tileArray);
        $index = 0;
        foreach ($data as $item) {
            $index++;
            fputcsv($fp, $item);
        }
        ob_flush();
        flush();
        ob_end_clean();
    }


    //服务商-分润账单导出
    public function CommissionExcelDown(Request $request)
    {
        $time = $request->get('time_start', ''); //交易时间，年-月
        $user_id = $request->get('user_id', '');  //服务商id
        $amount_start = $request->get('amount_start', ''); //开始金额
        $amount_end = $request->get('amount_end', ''); //结束金额

        $token = $this->parseToken();
        $token_user_id = $token->user_id;

        $MerchantOrderExcelDown = Cache::get('ExcelDown_fr');
        if ($MerchantOrderExcelDown) {
            echo '正在生成数据请1分钟后再来操作';
            die;
        } else {
            Cache::put('ExcelDown_fr', '1', 1);
        }

        $re_data = [
            'time_start' => $time,
            'user_id' => $user_id
        ];
        $check_data = [
            'time_start' => '交易时间',
            'user_id' => '代理商名称'
        ];
        $check = $this->check_required($re_data, $check_data);
        if ($check) {
            $this->status = '2';
            $this->message = $check;
            return $this->format();
        }

        $time_arr = explode('-', $time);
        $year = $time_arr[0];
        $month = $time_arr[1];
        if (!isset($year) || empty($year) || !isset($month) || empty($month)) {
            $this->status = '2';
            $this->message = '时间格式不正确';
            return $this->format();
        }
        $month_day = date('t', strtotime($time));
        $start_time = date('Y-m-d H:i:s', mktime(0, 0, 0, $month, 1, $year));
        $end_time = date('Y-m-d H:i:s', mktime(23, 59, 59, $month, $month_day, $year));

        //限制时间
//        $now_time = date('Y-m-d H:i:s', time());
//        $true_time = date('Y-m-d 22:00:00', time());
//        if ($now_time < $true_time){
//            echo '请在22:00:00以后导出';
//            die;
//        }

        $where = [];
        $where_desc = '';
        if ($amount_start) {
            $where[] = ['o.total_amount', '>=', "$amount_start"];
            $where_desc = '交易开始金额: '.$amount_start;
        }

        if ($amount_end) {
            $where[] = ['o.total_amount', '<=', "$amount_end"];
            $where_desc = $where_desc.' 交易结束金额: '.$amount_end;
        }

        if (isset($user_id) && ($user_id != 'undefined') && $user_id) {
            $user_ids = $this->getSubIdsAll($user_id);

            $user_info = DB::table('users')->where('id', $user_id)
                ->where('is_delete', '0')
                ->first();
            if ($user_info) {
                $where_desc = $where_desc.' 服务商: '.$user_info->name;
            }
        } else {
            $user_ids = $this->getSubIdsAll($token_user_id);
        }

        $where_desc = $where_desc.' 交易开始时间: '.$start_time;
        $where_desc = $where_desc.' 交易结束时间: '.$end_time;

        $filter_array = ['筛选条件：' . $where_desc];

        $orderYear = 'orders'.$year;
        if ( env('DB_D1_HOST') ) {
            if ( Schema::hasTable("$orderYear") ) {
                $table = DB::table("$orderYear as o");
            } else {
                $table = DB::table('orders as o');
            }
        } else {
            if ( Schema::hasTable("$orderYear") ) {
                $table = DB::table("$orderYear as o");
            } else {
                $table = DB::table('orders as o');
            }
        }
//        $obj = DB::table('orders as o')->select([
        $obj = $table->select([
            DB::raw("date_format(o.pay_time, '%Y-%m-%d') AS `modify_pay_time`"),
            'o.store_name',
            'o.company',
            'o.rate',
            DB::raw("u.rate AS `cost_rate`"),  //成本费率
            'o.ways_source_desc',
            DB::raw("SUM(`o`.`total_amount`) AS `total_amount`"), //交易金额
            DB::raw(" 0 + CAST((`o`.`rate` - `u`.`rate`)/100 *SUM(`o`.`total_amount`)  as char) as `profit_amount`"), //分润
            DB::raw("SUM(`o`.`refund_amount`) AS `total_refund_amount`"), //退款交易金额
            DB::raw("SUM(`o`.`mdiscount_amount`) AS `mdiscount_amount_sum`"), //商家优惠金额
            'o.pay_status',
            'o.pay_status_desc',
            DB::raw("COUNT(`o`.`total_amount`) as `count_order`")
        ])
            ->leftjoin('user_rates as u', function ($join) use ($user_id) {
                $join->on('u.ways_type', '=', 'o.ways_type')
                    ->where('u.user_id', '=', $user_id);
            })
            ->where($where)
            ->where('o.pay_time', '>=', "$start_time")
            ->where('o.pay_time', '<=', "$end_time")
            ->where('o.ways_type', '<>', '18888') //会员卡支付
            ->where('o.ways_type', '<>', '2005')
            ->where('o.company', '<>', 'alipay')
            ->where('o.company', '<>', 'weixin')
            ->where('o.company', '<>', 'weixina')
            ->whereNotNull('o.total_amount')
//            ->whereIn('o.pay_status', ['1', '6'])
            ->where(function($query) {
                $query->orWhere('o.pay_status', '=', 1)
                    ->orWhere('o.pay_status', '=', 6);
            })
            ->whereIn('o.user_id', $user_ids)
            ->groupBy(['modify_pay_time', 'o.store_id', 'o.company', 'o.rate', 'o.ways_source', 'o.pay_status'])
            ->orderBy('modify_pay_time', 'asc');

        if (isset($user_info) && $user_info) {
            $filename = $time.'-'.$user_info->name.'-'.$user_info->level_name.'-分润账单.csv';
        } else {
            $filename = $time.'-分润账单.csv';
        }
        $tileArray = [
            '交易时间',
            '商户名称',
            '交易渠道',
            '费率(%)',
            '成本费率(%)',
            '支付类型',
            '交易金额',
            '分润',
            '商家优惠金额',
            '支付状态',
            '支付状态描述',
            '交易笔数'
        ];

        $data = $obj->get()->toArray();
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 0);
        ob_end_clean();
        ob_start();
        header("Content-Type: text/csv");
        header("Content-Disposition:filename=" . $filename);
        $fp = fopen('php://output', 'w');
        fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));//转码 防止乱码(比如微信昵称(乱七八糟的))
        fputcsv($fp, $filter_array);
        fputcsv($fp, []);
        fputcsv($fp, $tileArray);
        $index = 0;
        foreach ($data as $item) {
            $item = get_object_vars($item);

            switch ($item['company']) {
                case 'mybank': $item['company'] = '快钱支付'; break;
                case 'herongtong': $item['company'] = '和融通'; break;
                case 'newland': $item['company'] = '新大陆'; break;
                case 'vbill': $item['company'] = '随行付'; break;
                case 'dlb': $item['company'] = '哆啦宝'; break;
                case 'tfpay': $item['company'] = 'TF通道'; break;
                case 'zft': $item['company'] = '花呗'; break;
                case 'fuiou': $item['company'] = '富友'; break;
                case 'alipay': $item['company'] = '支付宝官方'; break;
                case 'weixin': $item['company'] = '微信官方'; break;
                case 'jdjr': $item['company'] = '京东聚合'; break;
                case 'hkrt': $item['company'] = '海科融通'; break;
                case 'ltf': $item['company'] = '联拓付'; break;
                case 'easypay': $item['company'] = '易生支付'; break;
                case 'hulu': $item['company'] = '葫芦天下'; break;
                case 'linkage': $item['company'] = '联动优势'; break;
                case 'huipay': $item['company'] = '汇付'; break;
                case 'lianfu': $item['company'] = '工行'; break;
                case 'changsha': $item['company'] = '长沙银行'; break;
                case 'lianfuyouzheng': $item['company'] = '邮政'; break;
            }

            if ($item['pay_status'] == '6') {
                $item['total_amount'] = $item['total_refund_amount'];
                unset($item['total_refund_amount']);
            } else {
                unset($item['total_refund_amount']);
            }

            $index++;
            fputcsv($fp, $item);
        }
        ob_flush();
        flush();
        ob_end_clean();
    }


    //服务商-sfkj-分润账单导出
    public function SFKJCommissionExcelDown(Request $request)
    {
        $time = $request->get('time_start', '');
        $user_id = $request->get('user_id', '');  //服务商id
        $amount_start = $request->get('amount_start', '');
        $amount_end = $request->get('amount_end', '');

        $token = $this->parseToken();
        $token_user_id = $token->user_id;

        $MerchantOrderExcelDown = Cache::get('ExcelDown_sfkj');
        if ($MerchantOrderExcelDown) {
            echo '正在生成数据请1分钟后再来操作';
            die;
        } else {
            Cache::put('ExcelDown_sfkj', '1', 1);
        }

        if (env('DB_DATABASE_FIRSTPAY')) {
            $db = DB::connection('mysql_firstpay'); //连接首付科技数据库
        } else {
            $this->status = '3';
            $this->message = '首付科技数据库连接异常';
            return $this->format();
        }

        $re_data = [
            'time_start' => $time,
        ];
        $check_data = [
            'time_start' => '交易时间',
        ];
        $check = $this->check_required($re_data, $check_data);
        if ($check) {
            $this->status = '2';
            $this->message = $check;
            return $this->format();
        }

        $time_arr = explode('-', $time);
        $year = $time_arr[0];
        $month = $time_arr[1];
        if (!isset($year) || empty($year) || !isset($month) || empty($month)) {
            $this->status = '2';
            $this->message = '时间格式不正确';
            return $this->format();
        }
        $month_day = date('t', strtotime($time));
        $start_time = date('Y-m-d H:i:s', mktime(0,0,0,$month,1,$year));
        $end_time = date('Y-m-d H:i:s', mktime(23,59,59,$month,$month_day,$year));

        //限制时间
//        $now_time = date('Y-m-d H:i:s', time());
//        $true_time = date('Y-m-d 22:00:00', time());
//        if ($now_time < $true_time){
//            echo '请在22:00:00以后导出';
//            die;
//        }

        $where = [];
        $where_desc = '';
        if ($amount_start) {
            $where[] = ['total_amount', '>=', "$amount_start"];
            $where_desc = '交易开始金额: '.$amount_start;
        }
        if ($amount_end) {
            $where[] = ['total_amount', '<=', "$amount_end"];
            $where_desc = $where_desc.' 交易结束金额: '.$amount_end;
        }
        if (isset($user_id) && ($user_id != 'undefined') && $user_id) {
            $user_ids = $this->getSubIdsAll($user_id);

            $user_info = $db->table('users')->where('id', $user_id)
                ->where('is_delete', '0')
                ->first();
            if ($user_info) {
                $where_desc = $where_desc.' 服务商: '.$user_info->name;
            }
        } else {
            $user_ids = $this->getSubIdsAll($token_user_id);
        }
        if (1) {
            $where[] = ['ways_type', '!=', '2005'];
        }
        $where_desc = $where_desc.' 交易开始时间: '.$start_time;
        $where_desc = $where_desc.' 交易结束时间: '.$end_time;

        $filter_array = ['筛选条件：' . $where_desc];

        $obj = $db->table('orders')->select([
            DB::raw("date_format(pay_time, '%Y-%m-%d') AS `modify_pay_time`"),
            'store_name',
            'company',
            'rate',
            'cost_rate',  //成本费率
            'ways_source_desc',
            DB::raw("SUM(`total_amount`) AS `total_amount_sum`"),
            DB::raw("SUM(`mdiscount_amount`) AS `mdiscount_amount_sum`"),
            DB::raw("SUM(IF((`pay_status`=1), `total_amount`, 0)) as `total_success_amount`"),
            DB::raw("SUM(IF((`pay_status`=6), `total_amount`, 0)) as `total_return_amount`"),
            'pay_status',
            DB::raw("COUNT(`total_amount`) as `total_amount_count`"),
            'refund_amount' //分润金额
        ])
            ->where($where)
            ->where('pay_time', '>=', "$start_time")
            ->where('pay_time', '<=', "$end_time")
            ->where('pay_status_desc', '<>', '会员卡支付成功')
            ->where('company', '<>', 'alipay')
            ->where('company', '<>', 'weixin')
            ->whereNotNull('total_amount')
            ->whereIn('pay_status', ['1', '6'])
            ->whereIn('user_id', $user_ids)
            ->groupBy(['modify_pay_time', 'store_id', 'company', 'rate', 'ways_source', 'pay_status'])
            ->orderBy('modify_pay_time', 'asc');

        if (isset($user_info) && $user_info) {
            $filename = $time.'-'.$user_info->name.'-'.$user_info->level_name.'-分润账单_首付科技.csv';
        } else {
            $filename = $time.'-分润账单_首付科技.csv';
        }
        $tileArray = [
            '交易时间',
//            '代理商名称',
//            '代理商所属',
            '商户名称',
            '交易渠道',
            '费率',
            '成本费率',
            '支付类型',
            '交易金额',
            '商家优惠金额',
            '支付成功金额',
            '退款金额',
            '支付状态',
            '交易笔数',
            '分润金额',
        ];

        $data = $obj->get()->toArray();
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 0);
        ob_end_clean();
        ob_start();
        header("Content-Type: text/csv");
        header("Content-Disposition:filename=" . $filename);
        $fp = fopen('php://output', 'w');
        fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));//转码 防止乱码(比如微信昵称(乱七八糟的))
        fputcsv($fp, $filter_array);
        fputcsv($fp, []);
        fputcsv($fp, $tileArray);
        $index = 0;
        foreach ($data as $item) {
            $item = get_object_vars($item);

//            $item['pay_time'] = date('Y-m-d', strtotime($item['pay_time']));

//            if ($item['level'] == 0) {
//                $item['name'] = '苏州云收易网络科技有限公司';
//                $item['pid_name'] = '';
//            }
//            elseif($item['level'] == 1) {
//                $item['pid_name'] = '收吖旗下';
//            }

            $item['rate'] = $item['rate'] / 100;

            switch ($item['company']) {
                case 'mybank': $item['cost_rate'] = '0.0022';
                    break;
                case 'herongtong': $item['cost_rate'] = '0.0021';
                    break;
                case 'newland': $item['cost_rate'] = '0.0025';
                    break;
                case 'vbill': $item['cost_rate'] = '0.0021';
                    break;
                case 'dlb': $item['cost_rate'] = '0.0023';
                    break;
                case 'tfpay': $item['cost_rate'] = '0.0021';
                    break;
                default: unset($item['cost_rate']);
            }

            if((bccomp($item['rate'], $item['cost_rate'], 3) > 0) && isset($item['rate']) && ($item['pay_status']=='1')){
                $item['refund_amount'] = number_format(($item['total_success_amount']-$item['total_return_amount']-$item['mdiscount_amount_sum'])*(($item['rate']-$item['cost_rate'])), 3);
            } else {
                $item['refund_amount'] = 0.000;
            }

            switch ($item['pay_status']) {
                case '1': $item['pay_status'] = '支付成功';
                    break;
                case '6': $item['pay_status'] = '已退款';
                    break;
                default: unset($item['pay_status']);
            }

            switch ($item['company']) {
                case 'mybank': $item['company'] = '快钱支付';
                    break;
                case 'herongtong': $item['company'] = '和融通';
                    break;
                case 'newland': $item['company'] = '新大陆';
                    break;
                case 'vbill': $item['company'] = '随行付';
                    break;
                case 'dlb': $item['company'] = '哆啦宝';
                    break;
                case 'tfpay': $item['company'] = 'TF通道';
                    break;
                default: unset($item['company']);
            }

            $index++;
            fputcsv($fp, $item);
        }
        ob_flush();
        flush();
        ob_end_clean();
    }


    //刷脸支付统计 导出
    public function facePaymentExcelDown(Request $request)
    {
        $token = $this->parseToken();
        $token_user_id = $token->user_id;

        $start_time = $request->get('start_time', date('Y-m-d 00:00:00', time())); //开始时间
        $end_time = $request->get('end_time', date('Y-m-d 23:59:59', time())); //结束时间
        $user_id = $request->get('user_id', '');  //服务商id
        $device_id = $request->get('device_id', '');  //设备id
        $amount_start = $request->get('amount_start', 2.00); //交易开始金额
        $amount_end = $request->get('amount_end', ''); //交易结束金额
        $pay_status = $request->get('pay_status', 1); //支付状态

        $login_user = User::where('id', $token_user_id)
            ->where('is_delete', '=', '0')
            ->first();
        if (!$login_user) {
            $this->status = '2';
            $this->message = '当前登录状态异常';
            return $this->format();
        }

        $MerchantOrderExcelDown = Cache::get('facePaymentExcelDown');
        if ($MerchantOrderExcelDown) {
            echo '正在生成数据请1分钟后再来操作';die;
        } else {
            Cache::put('facePaymentExcelDown', '1', 1);
        }

        if (isset($user_id) && ($user_id != 'undefined') && $user_id) {
            $user_ids = $this->getSubIdsAll($user_id);
        } else {
            $user_ids = $this->getSubIdsAll($token_user_id);
        }

        $where = [];
        $where_desc = '';
        if ($start_time) {
            $where[] = ['o.pay_time', '>=', "$start_time"];
            $where_desc .=' 交易开始时间: '.$start_time;
        }
        if ($end_time) {
            $where[] = ['o.pay_time', '<=', "$end_time"];
            $where_desc .= ' 交易结束时间: '.$end_time;
        }
        if ($amount_start) {
            $where[] = ['o.total_amount', '>=', "$amount_start"];
            $where_desc .=' 交易开始金额: '.$amount_start;
        }
        if ($amount_end) {
            $where[] = ['o.total_amount', '<=', "$amount_end"];
            $where_desc .= ' 交易结束金额: '.$amount_end;
        }
        if ($device_id) {
            $where[] = ['o.device_id', "$device_id"];
            $where_desc .= ' 设备编号: '.$device_id;
        }
        if ($pay_status) {
            $where[] = ['o.pay_status', "$pay_status"];
        }

        $filter_array = ['筛选条件：' . $where_desc];

        $obj = DB::table('orders as o')
            ->select([
                'o.device_id',
                'o.store_name',
                'o.ways_source_desc',
                DB::raw("COUNT(distinct(`o`.`buyer_id`)) as `count_order`"),
                DB::raw("date_format(o.pay_time, '%Y-%m-%d') AS `modify_pay_time`")
            ])
            ->where($where)
            ->whereIn('o.pay_method', ['alipay_face', 'weixin_face'])
            ->whereIn('o.user_id', $user_ids)
            ->groupBy(['modify_pay_time', 'o.device_id'])
            ->orderBy('modify_pay_time', 'asc');

        $filename = '刷脸支付统计.csv';

        $tileArray = [
            '设备号',
            '门店名称',
            '支付方式',
            '笔数(去重)',
            '交易时间',
        ];

        $data = $obj->get()->toArray();
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 0);
        ob_end_clean();
        ob_start();
        header("Content-Type: text/csv");
        header("Content-Disposition:filename=" . $filename);
        $fp = fopen('php://output', 'w');
        fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));//转码 防止乱码(比如微信昵称(乱七八糟的))
        fputcsv($fp, $filter_array);
        fputcsv($fp, []);
        fputcsv($fp, $tileArray);
        $index = 0;
        foreach ($data as $item) {
            $item = get_object_vars($item);
            $index++;
            fputcsv($fp, $item);
        }
        ob_flush();
        flush();
        ob_end_clean();
    }


    //刷脸设备支付统计 导出
    public function facePaymentDisExcelDown(Request $request)
    {
        $token = $this->parseToken();
        $token_user_id = $token->user_id;

        $start_time = $request->get('start_time', date('Y-m-d 00:00:00', time())); //开始时间
        $end_time = $request->get('end_time', date('Y-m-d 23:59:59', time())); //结束时间
        $user_id = $request->get('user_id', '');  //服务商id
        $device_id = $request->get('device_id', '');  //设备id
        $amount_start = $request->get('amount_start', 2.00); //交易开始金额
        $amount_end = $request->get('amount_end', ''); //交易结束金额
        $pay_status = $request->get('pay_status', 1); //支付状态

        $login_user = User::where('id', $token_user_id)
            ->where('is_delete', '=', '0')
            ->first();
        if (!$login_user) {
            $this->status = '2';
            $this->message = '当前登录状态异常';
            return $this->format();
        }

        $MerchantOrderExcelDown = Cache::get('facePaymentDisExcelDown');
        if ($MerchantOrderExcelDown) {
            echo '正在生成数据请1分钟后再来操作';die;
        } else {
            Cache::put('facePaymentDisExcelDown', '1', 1);
        }

        if (isset($user_id) && ($user_id != 'undefined') && $user_id) {
            $user_ids = $this->getSubIdsAll($user_id);
        } else {
            $user_ids = $this->getSubIdsAll($token_user_id);
        }

        $where = [];
        $where_desc = '';
        if ($start_time) {
            $where[] = ['pay_time', '>=', "$start_time"];
            $where_desc .=' 交易开始时间: '.$start_time;
        }
        if ($end_time) {
            $where[] = ['pay_time', '<=', "$end_time"];
            $where_desc .= ' 交易结束时间: '.$end_time;
        }
        if ($amount_start) {
            $where[] = ['total_amount', '>=', "$amount_start"];
            $where_desc .=' 交易开始金额: '.$amount_start;
        }
        if ($amount_end) {
            $where[] = ['total_amount', '<=', "$amount_end"];
            $where_desc .= ' 交易结束金额: '.$amount_end;
        }
        if ($device_id) {
            $where[] = ['device_id', "$device_id"];
            $where_desc .= ' 设备编号: '.$device_id;
        }
        if ($pay_status) {
            $where[] = ['pay_status', "$pay_status"];
        }

        $filter_array = ['筛选条件：' . $where_desc];

        $obj = DB::table('orders')
            ->select([
                'device_id',
                'store_name',
                'ways_source_desc',
                DB::raw("COUNT(distinct(`buyer_id`)) as `count_order`")
            ])
            ->where($where)
            ->whereIn('pay_method', ['alipay_face', 'weixin_face'])
            ->whereIn('user_id', $user_ids)
            ->groupBy('device_id');

        $filename = '刷脸支付统计.csv';

        $tileArray = [
            '设备号',
            '门店名称',
            '支付方式',
            '笔数(去重)',
            '交易时间',
        ];

        $data = $obj->get()->toArray();
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 0);
        ob_end_clean();
        ob_start();
        header("Content-Type: text/csv");
        header("Content-Disposition:filename=" . $filename);
        $fp = fopen('php://output', 'w');
        fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));//转码 防止乱码(比如微信昵称(乱七八糟的))
        fputcsv($fp, $filter_array);
        fputcsv($fp, []);
        fputcsv($fp, $tileArray);
        $index = 0;
        foreach ($data as $item) {
            $item = get_object_vars($item);
            $index++;
            fputcsv($fp, $item);
        }
        ob_flush();
        flush();
        ob_end_clean();
    }

    //结算列表导出
    public function settlerecordExcelDown(Request $request)
    {
        try {
            $public = $this->parseToken();
            $config_id = $public->config_id;

            $time_start = $request->get('time_start', ''); //交易时间，年-月
            $time_end = $request->get('time_end', ''); //交易时间，年-月
            $amount_start = $request->get('amount_start', ''); //开始金额
            $amount_end = $request->get('amount_end', ''); //结束金额

            $where = [];

            if ($config_id) {
                $where[] = ['config_id', $config_id];
            }

            if ($amount_start) {
                $where[] = ['get_amount', '>=', "$amount_start"];
            }
            if ($amount_end) {
                $where[] = ['get_amount', '<=', "$amount_end"];
            }
            if ($time_start) {
                $where[] = ['created_at', '>=', "$time_start"];
            }
            if ($time_end) {
                $where[] = ['created_at', '<=', "$time_end"];
            }

            $settlement_lists = DB::table('settlement_lists');

            $dataArray = $settlement_lists->where($where)
                ->orderBy('updated_at', 'desc')
                ->select(
                    'is_settlement',
                    's_time',
                    'e_time',
                    'source_type',
                    'dx_desc',
                    'total_amount',
                    'rate',
                    'get_amount',
                    //'is_true',
                    'created_at',
                    'updated_at'
                )
                ->get()->map(function ($value) {
                    return (array)$value;
                })->toArray();

            $filename = '结算列表导出.csv';
            $s_array = ['筛选条件：' ];
            $tileArray = ['进度','开始时间', '结束时间', '返佣来源', '结算对象', '需结算金额', '税点', '扣税结算金额', '创建时间', '确认时间'];

            return $this->exportToExcel($filename, $s_array, $tileArray, $dataArray);

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


}
