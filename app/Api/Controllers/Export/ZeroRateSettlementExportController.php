<?php

namespace App\Api\Controllers\Export;


use App\Api\Controllers\BaseController;
use App\Models\MerchantStore;
use App\Models\Order;
use App\Models\Store;
use App\Models\User;
use App\Models\UserRate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ZeroRateSettlementExportController extends BaseController
{

    //零费率日结算记录导出
    public function ZeroRateSettlementDayExcelDown(Request $request)
    {
        $public = $this->parseToken();
        $time_start = $request->get('time_start', ''); //交易时间，年-月
        $time_end = $request->get('time_end', ''); //交易时间，年-月
        $amount_start = $request->get('amount_start', ''); //开始金额
        $amount_end = $request->get('amount_end', ''); //结束金额
        $user_id = $request->get('user_id', ''); //服务商id
        if ($user_id == "") {
            $user_id = $public->user_id;
        }
        $user_ids = $this->getSubIds($user_id);
        $settlement_lists = DB::table('zero_rate_settlement_day_infos as s');
        $where = [];

        if ($amount_start) {
            $where[] = ['s.total_amount', '>=', "$amount_start"];
        }
        if ($amount_end) {
            $where[] = ['s.total_amount', '<=', "$amount_end"];
        }
        if ($time_start) {
            $where[] = ['s.created_at', '>=', "$time_start"];
        }
        if ($time_end) {
            $where[] = ['s.created_at', '<=', "$time_end"];
        }
        if ($user_id == '1') {
            $dataArray = $settlement_lists->where($where)
                ->leftJoin('users as u', 's.user_id', '=', 'u.id')
                ->orderBy('s.updated_at', 'desc')
                ->select('s.*', 'u.name')
                ->get();
        } else {
            $dataArray = $settlement_lists->where($where)
                ->whereIn('s.user_id', $user_ids)
                ->leftJoin('users as u', 's.user_id', '=', 'u.id')
                ->orderBy('s.updated_at', 'desc')
                ->select('s.*', 'u.name')
                ->get();
        }
        $tmp = array();
        foreach ($dataArray as $settlement) {
            if ($settlement->is_true == 1) {
                $is_true = '确认';
            } else {
                $is_true = '未确认';
            }
            $tmp[] = array(
                $settlement->start_time,
                $settlement->end_time,
                $settlement->name,
                $settlement->total_amount,
                $is_true,
                $settlement->created_at,
                $settlement->updated_at,
                $settlement->remark
            );
        }

        $filename = '零费率日结明细导出.csv';
        $tileArray = ['开始时间', '结束时间', '结算对象', '需结算金额', '是否确认', '创建时间', '确认时间', '备注'];

        return $this->exportToExcel($filename, $tileArray, $tmp);
    }

    //零费率日结算记录导出
    public function ZeroRateSettlementMonthExcelDown(Request $request)
    {
        $public = $this->parseToken();
        $time_start = $request->get('time_start', ''); //交易时间，年-月
        $time_end = $request->get('time_end', ''); //交易时间，年-月
        $amount_start = $request->get('amount_start', ''); //开始金额
        $amount_end = $request->get('amount_end', ''); //结束金额
        $user_id = $request->get('user_id', ''); //服务商id
        if ($user_id == "") {
            $user_id = $public->user_id;
        }
        $user_ids = $this->getSubIds($user_id);
        $settlement_lists = DB::table('settlement_month_infos as s');
        $where = [];

        if ($amount_start) {
            $where[] = ['s.total_amount', '>=', "$amount_start"];
        }
        if ($amount_end) {
            $where[] = ['s.total_amount', '<=', "$amount_end"];
        }
        if ($time_start) {
            $where[] = ['s.created_at', '>=', "$time_start"];
        }
        if ($time_end) {
            $where[] = ['s.created_at', '<=', "$time_end"];
        }
        if ($user_id == '1') {
            $dataArray = $settlement_lists->where($where)
                ->leftJoin('users as u', 's.user_id', '=', 'u.id')
                ->orderBy('s.updated_at', 'desc')
                ->select('s.*', 'u.name')
                ->get();
        } else {
            $dataArray = $settlement_lists->where($where)
                ->whereIn('s.user_id', $user_ids)
                ->leftJoin('users as u', 's.user_id', '=', 'u.id')
                ->orderBy('s.updated_at', 'desc')
                ->select('s.*', 'u.name')
                ->get();
        }
        $tmp = array();
        foreach ($dataArray as $settlement) {
            if ($settlement->is_true == 1) {
                $is_true = '确认';
            } else {
                $is_true = '未确认';
            }
            $tmp[] = array(
                $settlement->start_time,
                $settlement->end_time,
                $settlement->name,
                $settlement->total_amount,
                $is_true,
                $settlement->created_at,
                $settlement->updated_at,
                $settlement->remark
            );
        }

        $filename = '零费率月结明细导出.csv';
        $tileArray = ['开始时间', '结束时间', '结算对象', '需结算金额', '是否确认', '创建时间', '确认时间', '备注'];

        return $this->exportToExcel($filename, $tileArray, $tmp);
    }

    public function exportToExcel($filename, $tileArray = [], $dataArray = [])
    {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 0);
        ob_end_clean();
        ob_start();
        header("Content-Type: text/csv");
        header("Content-Disposition:filename=" . $filename);
        $fp = fopen('php://output', 'w');
        fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));//转码 防止乱码(比如微信昵称(乱七八糟的))
        fputcsv($fp, []);
        fputcsv($fp, $tileArray);
        $index = 0;
        foreach ($dataArray as $item) {
            fputcsv($fp, $item);
            $index++;
        }

        ob_flush();
        flush();
        ob_end_clean();
    }


}
