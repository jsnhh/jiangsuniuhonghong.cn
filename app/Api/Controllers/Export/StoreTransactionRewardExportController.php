<?php

namespace App\Api\Controllers\Export;


use App\Api\Controllers\BaseController;
use App\Models\MerchantStore;
use App\Models\Order;
use App\Models\Store;
use App\Models\User;
use App\Models\UserRate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Log;

class StoreTransactionRewardExportController extends BaseController
{

    //商户交易达标返现导出
    public function StoreTransactionRewardExportDown(Request $request)
    {
        $user = $this->parseToken();//
        $user_id = $user->user_id;
        $agent_id = $request->get('agent_id', '');
        $store_name = $request->get('store_name', '');
        $rule_id = $request->get('rule_id', '');
        $standard_status = $request->get('standard_status', '');
        $first_status = $request->get('first_status', '');
        $again_status = $request->get('again_status', '');

        $where = [];

        if($user_id!=1){
            $where[] = ['s.agent_id', $user_id];
        }
        if ($agent_id) {
            $where[] = ['s.agent_id', $agent_id];
        }
        if ($store_name) {
            $where[] = ['s.store_name', 'like', '%' . $store_name . '%'];
        }
        if ($rule_id) {
            $where[] = ['s.rule_id', $rule_id];
        }
        if ($standard_status) {
            $where[] = ['s.standard_status', $standard_status];
        }
        if ($first_status) {
            $where[] = ['s.first_status', $first_status];
        }
        if ($again_status) {
            $where[] = ['s.again_status', $again_status];
        }
        $obj = DB::table('store_transaction_reward as s');
        $ids = $this->getSubIds($user_id);
        $dataArray = $obj->where($where)
            ->leftJoin('cash_back_rule as c', 's.rule_id', '=', 'c.id')
            ->whereIn('s.user_id', $ids)
            ->select('s.*', 'c.rule_name', 'c.start_time', 'c.end_time', 'c.standard_amt', 'c.standard_single_amt', 'c.standard_total',
                'c.first_amt', 'c.first_single_amt', 'c.first_total', 'c.again_amt', 'c.again_single_amt', 'c.again_total')
            ->orderBy('s.standard_status', 'desc')
            ->get();
        $tmp = array();
        foreach ($dataArray as $storeTransactionReward) {
            if ($storeTransactionReward->standard_status == "1") {
                $standard_status = '达标';
            } else {
                $standard_status = '未达标';
            }
            if ($storeTransactionReward->first_status == "1") {
                $first_status = '达标';
            } else {
                $first_status = '未达标';
            }
            if ($storeTransactionReward->again_status == "1") {
                $again_status = '达标';
            } else {
                $again_status = '未达标';
            }
            $tmp[] = array(
                $storeTransactionReward->rule_name,
                $storeTransactionReward->agent_name,
                $storeTransactionReward->store_name,
                $storeTransactionReward->start_time,
                $storeTransactionReward->end_time,
                $storeTransactionReward->store_bind_time,
                $standard_status,
                $storeTransactionReward->standard_total,
                $storeTransactionReward->standard_done_total,
                $storeTransactionReward->standard_amt,
                $storeTransactionReward->standard_done_amt,
                $storeTransactionReward->standard_time,
                $storeTransactionReward->standard_end_time,
                $storeTransactionReward->first_start_time,
                $first_status,
                $storeTransactionReward->first_total,
                $storeTransactionReward->first_done_total,
                $storeTransactionReward->first_amt,
                $storeTransactionReward->first_done_amt,
                $storeTransactionReward->first_single_amt,
                $storeTransactionReward->first_total_amt,
                $storeTransactionReward->first_end_time,
                $storeTransactionReward->again_start_time,
                $again_status,
                $storeTransactionReward->again_total,
                $storeTransactionReward->again_done_total,
                $storeTransactionReward->again_amt,
                $storeTransactionReward->again_done_amt,
                $storeTransactionReward->again_single_amt,
                $storeTransactionReward->again_total_amt,
                $storeTransactionReward->again_end_time,
                $storeTransactionReward->total_amt
            );
        }

        $filename = '商户交易返现详情表.csv';
        $tileArray = ['规则名称', '代理商名称', '商户名称', '开始日期', '结束日期', '商户入网时间', '激活奖励状态', '激活需交易笔数', '激活期间已交易笔数',
            '激活奖励金额', '获得激活奖励金额', '激活奖励日期', '激活截止日期', 'T+1月奖励开始日期', 'T+1月奖励状态', 'T+1月奖励需交易笔数', 'T+1月奖励期间已交易笔数',
            'T+1月奖励金额', '获得T+1月奖励金额', 'T+1月需交易总金额','T+1月已交易总金额', 'T+1月奖励截止日期', 'T+2月奖励开始日期', 'T+2月奖励状态', 'T+2月奖励需交易笔数',
            'T+2月奖励期间已交易笔数', 'T+2月奖励金额', '获得T+2月奖励金额', 'T+2月需交易总金额','T+2月已交易总金额', 'T+2月奖励截止日期', '获得总奖励金额'];

        return $this->exportToExcel($filename, $tileArray, $tmp);
    }

    public function exportToExcel($filename, $tileArray = [], $dataArray = [])
    {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 0);
        ob_end_clean();
        ob_start();
        header("Content-Type: text/csv");
        header("Content-Disposition:filename=" . $filename);
        $fp = fopen('php://output', 'w');
        fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));//转码 防止乱码(比如微信昵称(乱七八糟的))
        fputcsv($fp, []);
        fputcsv($fp, $tileArray);
        $index = 0;
        foreach ($dataArray as $item) {
            fputcsv($fp, $item);
            $index++;
        }

        ob_flush();
        flush();
        ob_end_clean();
    }


}
