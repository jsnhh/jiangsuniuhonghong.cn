<?php

namespace App\Api\Controllers\Export;


use App\Api\Controllers\BaseController;
use App\Models\MerchantStore;
use App\Models\Order;
use App\Models\Store;
use App\Models\User;
use App\Models\UserRate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Log;

class PassagewayUseExportController extends BaseController
{

    //商户充值记录导出
    public function rechargeRecordExportDown(Request $request)
    {
        $user = $this->parseToken();
        $user_id = $request->get('user_id', '');
        $store_id = $request->get('store_id', '');//商户id
        $pay_status = $request->get('pay_status', '');//支付状态
        $pay_type = $request->get('pay_type', '');//支付状态
        $time_start_s = date('Y-m-d 00:00:00', time());
        $time_start_e = date('Y-m-d 23:59:59', time());
        $time_start = $request->get('time_start', $time_start_s);//开始时间
        $time_end = $request->get('time_end', $time_start_e);//结束时间
        $out_trade_no = $request->get('out_trade_no', '');//订单号
        $where = [];
        $obj = DB::table('merchant_recharges as m');
        if ($user_id == "") {
            $user_id = $user->user_id;
        }
        $user_ids = $this->getSubIds($user_id);
        if ($time_start) {
            $where[] = ['m.created_at', '>=', $time_start];
        }

        if ($time_end) {
            $where[] = ['m.created_at', '<=', $time_end];
        }
        if ($store_id) {
            $where[] = ['s.store_id', '=', $store_id];
        }
        if ($pay_status) {
            $where[] = ['m.status', '=', $pay_status];
        }
        if ($out_trade_no) {
            $where[] = ['m.order_id', '=', $out_trade_no];
        }
        if ($pay_type) {
            $where[] = ['m.pay_type', '=', $pay_type];
        }
        if ($user_id == '1') {
            $dataArray = $obj->where($where)
                ->leftJoin('stores as s', 'm.merchant_id', '=', 's.merchant_id')
                ->leftJoin('users as u', 'm.user_id', '=', 'u.id')
                ->orderBy('m.created_at', 'desc')
                ->select('m.*', 's.store_name', 'u.name')
                ->get();
        } else {
            $dataArray = $obj->where($where)
                ->whereIn('m.user_id', $user_ids)
                ->leftJoin('stores as s', 'm.merchant_id', '=', 's.merchant_id')
                ->leftJoin('users as u', 'm.user_id', '=', 'u.id')
                ->orderBy('m.created_at', 'desc')
                ->select('m.*', 's.store_name', 'u.name')
                ->get();
        }

        $tmp = array();
        foreach ($dataArray as $recharge) {
            if ($recharge->status == "1") {
                $status = "支付成功";
            } elseif ($recharge->status == "2") {
                $status = "等待支付";
            } else {
                $status = "支付失败";
            }
            if ($recharge->pay_type == "1") {
                $pay_type = '流量';
            } elseif ($recharge->pay_type == "2") {
                $pay_type = '月包';
            } else {
                $pay_type = '季包';
            }

            $tmp[] = array(
                $recharge->created_at,
                $recharge->store_name,
                $recharge->name,
                $recharge->amount,
                $recharge->order_id,
                $status,
                $pay_type,
                $recharge->end_time
            );
        }

        $filename = '商户充值记录表.csv';
        $tileArray = ['充值时间', '门店', '所属代理', '充值金额', '订单号', '状态', '充值方式', '月包/季包到期日'];

        return $this->exportToExcel($filename, $tileArray, $tmp);
    }

    //通道使用记录导出
    public function passagewayRecordExportDown(Request $request)
    {
        $user = $this->parseToken();
        $user_id = $request->get('user_id', '');
        $store_id = $request->get('store_id', '');//商户id
        $type = $request->get('type', '');
        $settlement_type = $request->get('settlement_type', '');
        $time_start_s = date('Y-m-d 00:00:00', time());
        $time_start_e = date('Y-m-d 23:59:59', time());
        $time_start = $request->get('time_start', $time_start_s);//开始时间
        $time_end = $request->get('time_end', $time_start_e);//结束时间
        $out_trade_no = $request->get('out_trade_no', '');//订单号
        $where = [];
        $obj = DB::table('merchant_consumer_details as m');
        if ($user_id == "") {
            $user_id = $user->user_id;
        }
        $user_ids = $this->getSubIds($user_id);
        if ($time_start) {
            $where[] = ['m.created_at', '>=', $time_start];
        }
        if ($time_end) {
            $where[] = ['m.created_at', '<=', $time_end];
        }
        if ($store_id) {
            $where[] = ['s.store_id', '=', $store_id];
        }
        if ($out_trade_no) {
            $where[] = ['m.order_id', '=', $out_trade_no];
        }
        if ($type) {
            $where[] = ['m.type', '=', $type];
        }
        if ($settlement_type) {
            $where[] = ['m.settlement_type', '=', $settlement_type];
        }
        if ($user_id == '1') {
            $dataArray = $obj->where($where)
                ->leftJoin('stores as s', 'm.merchant_id', '=', 's.merchant_id')
                ->leftJoin('users as u', 'm.user_id', '=', 'u.id')
                ->orderBy('m.created_at', 'desc')
                ->select('m.*', 's.store_name', 'u.name')
                ->get();
        } else {
            $dataArray = $obj->where($where)
                ->whereIn('m.user_id', $user_ids)
                ->leftJoin('stores as s', 'm.merchant_id', '=', 's.merchant_id')
                ->leftJoin('users as u', 'm.user_id', '=', 'u.id')
                ->orderBy('m.created_at', 'desc')
                ->select('m.*', 's.store_name', 'u.name')
                ->get();
        }

        $tmp = array();
        foreach ($dataArray as $data) {
            if ($data->type == "1") {
                $type = "商户充值";
            } elseif ($data->type == "2") {
                $type = "交易扣款";
            } else {
                $type = "支付失败";
            }
            if ($data->settlement_type == "01") {
                $settlement_type = "未结算";
            } elseif ($data->settlement_type == "02") {
                $settlement_type = "已结算";
            }
            $tmp[] = array(
                $data->created_at,
                $data->store_name,
                $data->name,
                $data->avail_amount,
                $data->amount,
                $data->rate,
                $data->profit_amount,
                $data->total_profit_amount,
                $data->pay_amount,
                $data->order_id,
                $type,
                $settlement_type
            );
        }

        $filename = '通道使用记录表.csv';
        $tileArray = ['交易时间', '门店', '所属代理', '商户余额','通道使用费', '分润比例%', '代理分润', '分润总金额', '交易金额', '订单号', '使用类型', '是否结算'];

        return $this->exportToExcel($filename, $tileArray, $tmp);
    }

    public function exportToExcel($filename, $tileArray = [], $dataArray = [])
    {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 0);
        ob_end_clean();
        ob_start();
        header("Content-Type: text/csv");
        header("Content-Disposition:filename=" . $filename);
        $fp = fopen('php://output', 'w');
        fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));//转码 防止乱码(比如微信昵称(乱七八糟的))
        fputcsv($fp, []);
        fputcsv($fp, $tileArray);
        $index = 0;
        foreach ($dataArray as $item) {
            fputcsv($fp, $item);
            $index++;
        }

        ob_flush();
        flush();
        ob_end_clean();
    }
}
