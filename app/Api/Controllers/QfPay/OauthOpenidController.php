<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2021/03/01
 * Time: 10:10AM
 */
namespace App\Api\Controllers\QfPay;

use App\Api\Controllers\Config\QfPayConfigController;
use App\Api\Controllers\Config\TfConfigController;
use EasyWeChat\Factory;
use Illuminate\Http\Request;

class OauthOpenidController extends \App\Api\Controllers\DevicePay\BaseController
{

    //第三方非想用平台来获取快钱的openid
    public function oauth_sign_openid(Request $request)
    {
        //第三方传过来的信息
        //获取请求参数
        $data = $request->all();
        //验证签名
        $check = $this->check_md5($data);
        if ($check['return_code'] == 'FALL') {
            return $this->return_data($check);
        }
        $config_id = $data['config_id'];
        $callback_url = $data['callback_url'];

        $config = new QfPayConfigController();
        $qfpay_config = $config->qfpay_config($config_id);
        if (!$qfpay_config) {
            return json_encode([
                'status' => 2,
                'message' => '钱方配置不存在请检查配置'
            ]);
        }
        $config = [
            'app_id' => $qfpay_config->wx_appid,
            'scope' => 'snsapi_base',
            'oauth' => [
                'scopes' => ['snsapi_base'],
                'response_type' => 'code',
                'callback' => url('api/qfpay/weixin/oauth_sign_callback_openid?wx_AppId=' . $qfpay_config->wx_appid . '&wx_Secret=' . $qfpay_config->wx_secret . '&callback_url=' . $callback_url),
            ]
        ];
        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;
        return $oauth->redirect();

    }

    //第三方非想用平台来获取快钱的openid
    public function oauth_sign_callback_openid(Request $request)
    {
        $callback_url = $request->get('callback_url');
        $code = $request->get('code');
        $wx_AppId = $request->get('wx_AppId');
        $wx_Secret = $request->get('wx_Secret');
        $config = [
            'app_id' => $wx_AppId,
            "secret" => $wx_Secret,
            "code" => $code,
            "grant_type" => "authorization_code"
        ];

        $app = Factory::officialAccount($config);
        $oauth = $app->oauth;
        $user = $oauth->user();
        $open_id = $user->getId();

        return redirect($callback_url . '?open_id=' . $open_id);
    }


}
