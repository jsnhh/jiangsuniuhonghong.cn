<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2021/03/01
 * Time: 10:10AM
 */
namespace App\Api\Controllers\QfPay;

use Illuminate\Support\Facades\Log;

class BaseController
{
//    public $host_url = 'https://openapi-test.qfpay.com'; //测试线
    public $host_url = 'https://openapi.qfpay.com'; //生产线


    /**
     * @param $data
     * @param $key
     * @return array
     */
    public function get_sign($data, $key)
    {
        ksort($data);
        $str1 = '';
        foreach ($data as $k => $v) {
            $str1 .='&'.$k . "=" . $v;
        }
        $str = $str1. $key;

        $sign = strtoupper( md5(ltrim($str, '&')));
        return [$sign, ltrim($str1, '&')];
    }


    /**
     * Unicode字符转换成utf8字符
     * @param string [type] $unicode_str Unicode字符
     * @return string [type]       Utf-8字符
     */
    public function unicode_to_utf8($unicode_str)
    {
        $utf8_str = '';
        $code = intval(hexdec($unicode_str));
        //这里注意转换出来的code一定得是整形，这样才会正确的按位操作
        $ord_1 = decbin(0xe0 | ($code >> 12));
        $ord_2 = decbin(0x80 | (($code >> 6) & 0x3f));
        $ord_3 = decbin(0x80 | ($code & 0x3f));
        $utf8_str = chr(bindec($ord_1)) . chr(bindec($ord_2)) . chr(bindec($ord_3));

        return $utf8_str;
    }


    /**
     * curl请求  1-成功 2-失败 3-交易中
     * @param string $url  请求地址
     * @param array $params  请求数据
     * @param array $header  请求头
     * @param int $time_out  超时时间
     * @param bool $is_post  请求方式
     * @return array
     */
    public function curlRequest($url, $params = array(), $header = array(), $time_out = 10, $is_post = true)
    {
        $ch = curl_init(); //初始化curl
        curl_setopt($ch, CURLOPT_URL, $url); //抓取指定网页
        curl_setopt($ch, CURLOPT_HEADER, 0); //设置是否返回response header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //要求结果为字符串且输出到屏幕上;当需要通过curl_getinfo来获取发出请求的header信息时，该选项需要设置为true
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $time_out);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $time_out);
        curl_setopt($ch, CURLOPT_POST, $is_post);

        if ($is_post) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }

        if ($header) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        $response = curl_exec($ch);
        //打印请求的header信息
        $request_header = curl_getinfo($ch, CURLINFO_HEADER_OUT);
        curl_close($ch);

        return $response;
    }


    /**
     *
     * @param $code
     * @return string
     */
    public function getMessageByCode($code)
    {
        switch ($code) {
            case '0000': return '交易成功；success'; break;
            case '1143': return '订单还未支付，或者正在输入密码中'; break;
            case '1145': return '处理中，请稍等'; break;
            case '1150': return '您的结算方式为T0，不支持撤销交易'; break;
            case '1201': return '余额不足，请更换支付方式支付'; break;
            case '1202': return '付款码错误或过期，请出示正确的付款码或刷新付款码后重新支付'; break;
            case '1203': return '账户错误，请确认支付账户可用'; break;
            case '1204': return '银行错误，请确认支付账户可用'; break;
            case '1205': return '交易失败，请稍后重试'; break;
            case '1212': return '请消费者使用银联境外码付款'; break;
            case '1241': return '店铺不存在或状态不正确，请勿进行支付	需要联系钱方人员处理'; break;
            case '1242': return '客官，该店铺配置有些问题，无法进行交易	需要联系钱方人员处理'; break;
            case '1243': return '该店铺被禁用，请勿进行支付，联系店主确认	需要联系钱方人员处理'; break;
            case '1250': return '该交易禁止进行，如有疑问请联系钱方客服4000360280	需要联系钱方人员处理'; break;
            case '1251': return '客官，该店铺配置有些问题，我们正在加紧解决中out_trade_no is used ：外部订单号重复'; break;
            case '1254': return '客官，系统出了点小差，我们正在加紧解决中	需要联系钱方人员处理'; break;
            case '1260': return '该订单已支付，请确认交易结果后再操作'; break;
            case '1261': return '该订单未支付，请确认交易结果后再操作'; break;
            case '1262': return '该订单已退款，请确认订单状态再操作'; break;
            case '1263': return '该订单已撤销，请确认订单状态再操作'; break;
            case '1264': return '该订单已关闭，请确认订单状态再操作'; break;
            case '1265': return '该笔订单被禁止退款，23:30-00:30的交易以及部分特殊交易会被禁止'; break;
            case '1266': return '该订单金额错误，请确认后再操作'; break;
            case '1267': return '该订单信息不匹配，请确认后再操作'; break;
            case '1268': return '该订单不存在，请确认后再操作'; break;
            case '1269': return '当日未结算金额不足，无法操作，请确认余额充足'; break;
            case '1270': return '该币种不支持部分退款'; break;
            case '1271': return '该交易不支持部分退款'; break;
            case '1272': return '该退款金额大于原交易可退款的最大金额'; break;
            case '1294': return '该笔交易可能存在风险，已被银行禁止交易'; break;
            case '1295': return '网络有些拥堵，客官莫急，我们正在加速解决中	需要联系钱方人员处理'; break;
            case '1296': return '网络有些拥堵，客官莫急，请稍后再试或使用其他支付方式	需要联系钱方人员处理'; break;
            case '1297': return '银行系统繁忙，客官莫急，请稍后再试或使用其他支付方式	需要联系钱方人员处理'; break;
            case '1298': return '网络有些拥堵，客官莫急，切勿重复支付，如已支付请稍后确认结果'; break;
            case '2005': return '消费者付款码错误或已过期请刷新后重新支付'; break;
            case '2011': return '交易流水号重复'; break;
            case '1108': return 'APPCODE非法：请求头APPCODE传值错误或APPCODE与mchid不匹配,签名错误'; break;
            default: return 'qfpay unknow'; break;
        }
    }


}
