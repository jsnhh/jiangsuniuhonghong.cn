<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/6/8
 * Time: 下午5:36
 */

namespace App\Api\Controllers\EasySkPay;

use App\Api\Controllers\Config\EasyPayConfigController;
use App\Models\EasypayConfig;
use App\Models\EasySkpayConfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class BaseController
{
    //测试地址
//    private $url = [
//        //资料上传
//        '1' => 'https://testkjcld.bhecard.com:8463/base',
//        //商户注册
//        '2' => 'https://testkjcld.bhecard.com:8463/mcht/register',
//        //交易
//        '3' => 'https://testkjcld.bhecard.com:8463/qrtran'
//    ];
//生产地址
    private $url = [
        //资料上传
        '1' => 'https://kjprd.bhecard.com/base',
        //商户注册
        '2' => 'https://kjprd.bhecard.com/api/mcht/register',
        //交易
        '3' => 'https://kjprd.bhecard.com/api/qrtran'
    ];

    private $key;

    public $wx_appid;

    function __construct()
    {
        $this->key = base64_decode('t4iHENG9d9P3jJE5vwM7yw=='); //要先base64解密为一个16位的key才能使用 测试对称加密秘钥mhP6wrUjC40ezYfBwCOxFw==

        $easypay_config_obj = EasySkpayConfig::where('config_id', '1001')->first();
        if (!$easypay_config_obj) {
            return [
                'status' => '2',
                'message' => '易生数科支付参数未配置'
            ];
        }

        $this->wx_appid = $easypay_config_obj->wx_appid;

    }

    /**
     * 支付请求封装
     * @param array $postArr -业务参数
     * @param string $tranCode -接口code
     * @param string $urlCode -接口urlCode
     * @param string $merId -商户代码
     * @return mixed
     * @throws \Exception
     */
    public function requestChannel($postArr, $tranCode, $urlCode, $merId = '', $requestNo, $appId)
    {
        try {
            $requestData = [
                'version' => '1.0',
                'signType' => '01',
                'encoding' => "UTF-8",
                'tranTime' => date('YmdHis'),
                'appId' => $appId, //测试机构号"QY0064"
            ];
            $requestData['merId'] = $merId;
            $requestData['requestNo'] = $requestNo;
            $requestData['tranCode'] = $tranCode;
            $requestData['bizData'] = $this->encrypt(json_encode($postArr));
            $requestData['signature'] = $this->signStr($requestData);
            $requestData_json = json_encode($requestData);

            Log::info("易生数科 业务请求 requestData_json:" . $requestData_json);

            $resultJson = $this->post_curl_json($requestData_json, $this->url[$urlCode]);
            $resultArr = json_decode($resultJson, true);
            return $resultArr;
        } catch (\Exception $e) {
            //TODO
        }
    }

    /**
     * 加密
     * @param String input 加密的字符串
     * @param String key   解密的key
     * @return String
     */
    public function encrypt($dataStr)
    {
        //AES, 128 ECB模式加密数据
        $data = openssl_encrypt($dataStr, 'AES-128-ECB', $this->key, OPENSSL_RAW_DATA);
        $data = base64_encode($data);

        return $data;
    }

    /**
     * 解密
     * @param String input 解密的字符串
     * @param String key   解密的key
     * @return String
     */
    public function decrypt($sStr)
    {
        //AES, 128 ECB模式加密数据
        $decrypted = openssl_decrypt(base64_decode($sStr), 'AES-128-ECB', $this->key, OPENSSL_RAW_DATA);
        return $decrypted;
    }

    /**
     * CURL通过POST发送JSON数据
     * @param $json
     * @param $url
     * @param int $time
     * @return string
     */
    private function post_curl_json($json, $url, $time = 20)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json; charset=utf-8',
                'Content-Length: ' . strlen($json))
        );
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, $time * 1000);
        ob_start();
        curl_exec($ch);
        $return_content = ob_get_contents();
        ob_end_clean();
        $return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $res = array($return_code, $return_content);
        $des = json_decode($res['1'], true);
        if (empty($res) || empty($des['respCode'])) {
            return json_encode(array('respCode' => '404', 'respMsg' => '请求渠道异常！'), JSON_UNESCAPED_UNICODE);
        } else {
            return $res['1'];
        }
    }

    /**
     * 参数序列化拼接
     * @param $paramArr
     * @return string
     */
    public function kSortStr($paramArr)
    {
        if (empty($paramArr)) return '';
        ksort($paramArr);
        $str = '';
        foreach ($paramArr as $k => $v) {
            if (!empty($v)) {
                $str .= $k . '=' . $v . '&';
            }
        }
        return rtrim($str, '&');
    }

    /**
     * 生成签名
     * @param $params
     * @return string
     */
    public function signStr($params)
    {
        //参数序列化
        $str = $this->kSortStr($params);
        //获取私钥
        $privateKey = file_get_contents(dirname(__FILE__) . '/key/private.pem');//读取私钥

        return $this->signRsa($str, $privateKey);
    }

    /**
     * 私钥签名
     * @param $data
     * @param $privateKey
     * @return string
     */
    private function signRsa($data, $privateKey)
    {
        $private_key = openssl_pkey_get_private($privateKey);
        openssl_sign($data, $sign, $private_key, OPENSSL_ALGO_SHA256);
        openssl_free_key($private_key);
        return base64_encode($sign);//最终的签名　　　
    }

}
