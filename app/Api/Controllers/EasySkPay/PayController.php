<?php

namespace App\Api\Controllers\EasySkPay;

use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PayController extends BaseController
{

    public $tranCode = [
        //银联用户标识
        'unionPayCode' => 'P2007',
        //付款码支付B扫C
        'bToC' => 'P2008',
        //付款码支付C扫B
        'cToB' => 'P2013',
        //支付查询
        'orderQuery' => 'P2011',
        //支付退款
        'refund' => 'P2012',
        //支付关闭
        'orderClose' => 'P2017',
        //退款查询
        'refundQuery' => 'P2019',
        //支付结果异步回调
        'notifyPayResult' => 'N2010',
        //账户结果异步回调
        'notifyAccountResult' => 'N2011',
        //对账文件下载
        'reconciliation' => 'U2021'
    ];

    public $requestParams = [];

    public function setParam($key, $value)
    {
        $this->requestParams[$key] = $value;
    }

    public function getParam($key)
    {
        return $this->requestParams[$key];
    }

    public function getAllParams()
    {
        return $this->requestParams;
    }

    public function deleteParam($key)
    {
        if (isset($this->requestParams[$key])) {
            unset($this->requestParams[$key]);
        }
    }

    public function deleteAllParams()
    {
        $this->requestParams = [];
    }

    public function codePayByBtoC($data)
    {
        $orgId = $data['org_id'];
        $amount = $data['amount'] * 100;
        $authCode = $data['authCode'];
        $pay_type = $data['pay_type'];
        $merId = $data['mer_id'];//"QY0201000003968"
        $requestNo = $data['request_no'];
        $location = $data['location'];
        if ($pay_type == 'ALIPAY') {
            $prodCode = 'SM100';
        } else if ($pay_type == 'WECHAT') {
            $prodCode = 'SM102';
        } else if ($pay_type == 'UNIONPAY') {
            $prodCode = 'SM101';
        }
        $this->deleteAllParams();
        $this->setParam('prodCode', $prodCode);
        $this->setParam('amount', strval($amount)); //单位：分
        $this->setParam('authCode', $authCode); //支付授权码
        $this->setParam('orderDesc', "河南畅立收"); //按实际填写
        $this->setParam('expireTime', 60); //过期时间 1分钟
        $this->setParam('location', $location); //经纬度
//        $this->setParam('terminalIp','154.8.143.104'); //设备IP地址
        $res = $this->requestChannel($this->getAllParams(), $this->tranCode['bToC'], 3, $merId, $requestNo, $orgId); //使用正确商户号
        Log::info("易生数科 扫一扫 res:" . json_encode($res));
        if ($res['bizData']) {
            $bizData = $this->decrypt($res['bizData']);
            $bizData = json_decode($bizData, true);
        } else {
            return [
                'status' => '3',
                'message' => $res['respMsg'],
                'data' => $res,
            ];
        }
        if ($bizData['tradeStatus'] == 'TRADE_PROCESS' || $bizData['tradeStatus'] == 'TRADE_WAIT_PAY') { //待支付（等待输入密码）
            $res['bizData'] = $bizData;
            return [
                'status' => '2',
                'message' => '订单未支付',
                'data' => $res,
            ];
        } elseif ($bizData['tradeStatus'] == 'TRADE_SUCCESS') {
            $res['bizData'] = $bizData;
            return [
                'status' => '1',
                'message' => '支付成功',
                'data' => $res,
            ];
        } else {
            $res['bizData'] = $bizData;
            return [
                'status' => '3',
                'message' => '支付失败',
                'data' => $res,
            ];
        }

    }

    public function codePayByCtoB($data)
    {
        $orgId = $data['org_id'];
        $amount = $data['amount'] * 100;
        $openId = $data['open_id'];
        $pay_type = $data['pay_type'];
        $notifyURL = $data['notify_url'];
        $subAppId = $data['sub_app_id'];
        $merId = $data['mer_id'];//"QY0201000003968"
        $requestNo = $data['request_no'];
        $location = $data['location'];
        $this->deleteAllParams();
        if ($pay_type == 'ALIPAY') {
            $prodCode = 'SM100';
        } else if ($pay_type == 'WECHAT') {
            $prodCode = 'SM102';
        } else if ($pay_type == 'UNIONPAY') {
            $prodCode = 'SM101';
            $this->setParam('forwardURL', "https://pay.jiangsuniuhonghong.cn/api/easyskpay/pay_notify_url_u"); //前台通知地址 银联上送
            $this->setParam('ipAddr', '154.8.143.104'); //ip地址      银联上送
        }
        $this->setParam('prodCode', $prodCode);
        $this->setParam('amount', strval($amount)); //单位：分
        $this->setParam('openId', $openId); //用户唯一标识
        $this->setParam('notifyURL', $notifyURL); //支付回调地址
        $this->setParam('subAppId', $subAppId); //微信appid
        $this->setParam('orderDesc', "河南畅立收"); //按实际填写
        $this->setParam('expireTime', 60); //过期时间 1分钟
        $this->setParam('location', $location); //经纬度
//        $this->setParam('terminalIp','154.8.143.104'); //设备IP地址

        $res = $this->requestChannel($this->getAllParams(), $this->tranCode['cToB'], 3, $merId, $requestNo, $orgId); //使用正确商户号

        Log::info("易生数科 cToB res:" . json_encode($res));
        if ($res['bizData']) {
            $bizData = $this->decrypt($res['bizData']);
            $bizData = json_decode($bizData, true);
        } else {
            return [
                'status' => '3',
                'message' => $res['respMsg'],
                'data' => $res,
            ];
        }
        if ($bizData['tradeStatus'] == 'TRADE_PROCESS' || $bizData['tradeStatus'] == 'TRADE_WAIT_PAY') { //待支付（等待输入密码）
            $res['bizData'] = $bizData;
            return [
                'status' => '1',
                'message' => '订单未支付',
                'data' => $res,
            ];
        } elseif ($bizData['tradeStatus'] == 'TRADE_SUCCESS') {
            $res['bizData'] = $bizData;
            return [
                'status' => '1',
                'message' => '支付成功',
                'data' => $res,
            ];
        } else {
            $res['bizData'] = $bizData;
            return [
                'status' => '3',
                'message' => '支付失败',
                'data' => $res,
            ];
        }

    }

    public function order_query($data)

    {
        $orgId = $data['org_id'];
        $merId = $data['mer_id'];//"QY0201000003968"
        $requestNo = date('YmdHis') . mt_rand(1000, 9999);
        $origRequestNo = $data['orig_request_no'];
        $this->deleteAllParams();
        $this->setParam('origRequestNo', $origRequestNo);
        $res = $this->requestChannel($this->getAllParams(), $this->tranCode['orderQuery'], 3, $merId, $requestNo, $orgId); //使用正确商户号

        Log::info("易生数科 订单查询 res:" . json_encode($res));
        if ($res['bizData']) {
            $bizData = $this->decrypt($res['bizData']);
            $bizData = json_decode($bizData, true);
        } else {
            return [
                'status' => '3',
                'message' => $res['respMsg'],
                'data' => $res,
            ];
        }
        if ($bizData['tradeStatus'] == 'TRADE_PROCESS' || $bizData['tradeStatus'] == 'TRADE_WAIT_PAY') { //待支付（等待输入密码）
            $res['bizData'] = $bizData;
            return [
                'status' => '2',
                'message' => '订单未支付',
                'data' => $res,
            ];
        } elseif ($bizData['tradeStatus'] == 'TRADE_SUCCESS') {
            $res['bizData'] = $bizData;
            return [
                'status' => '1',
                'message' => '支付成功',
                'data' => $res,
            ];
        } else {
            $res['bizData'] = $bizData;
            return [
                'status' => '3',
                'message' => $res['respMsg'],
                'data' => $res,
            ];
        }

    }

    public function order_close($data)
    {
        $orgId = $data['org_id'];
        $merId = $data['mer_id'];//"QY0201000003968"
        $requestNo = $data['request_no'];
        $origRequestNo = $data['orig_request_no'];
        $origTradeNo = $data['orig_trade_no'];
        $this->deleteAllParams();
        $this->setParam('origRequestNo', $origRequestNo);
        $this->setParam('origTradeNo', $origTradeNo);
        $res = $this->requestChannel($this->getAllParams(), $this->tranCode['orderClose'], 3, $merId, $requestNo, $orgId); //使用正确商户号

        Log::info("易生数科 订单撤销 res:" . json_encode($res));
        if ($res['bizData']) {
            $bizData = $this->decrypt($res['bizData']);
            $bizData = json_decode($bizData, true);
        } else {
            return [
                'status' => '3',
                'message' => $res['respMsg'],
                'data' => $res,
            ];
        }
        if ($res['respCode'] == '0000') {
            $res['bizData'] = $bizData;
            return [
                'status' => '1',
                'message' => '撤销成功',
                'data' => $res,
            ];
        } else {
            $res['bizData'] = $bizData;
            return [
                'status' => '2',
                'message' => $res['respMsg'],
                'data' => $res,
            ];
        }

    }

    public function refund($data)
    {
        $orgId = $data['org_id'];
        $merId = $data['mer_id'];//"QY0201000003968"
        $requestNo = $data['request_no'];
        $origRequestNo = $data['orig_request_no'];
        $origTradeNo = $data['orig_trade_no'];
        $amount = $data['amount'] * 100;
        $this->deleteAllParams();
        $this->setParam('origRequestNo', $origRequestNo);
        $this->setParam('origTradeNo', $origTradeNo);
        $this->setParam('amount', strval($amount));
        $res = $this->requestChannel($this->getAllParams(), $this->tranCode['refund'], 3, $merId, $requestNo, $orgId); //使用正确商户号

        Log::info("易生数科 支付退款 res:" . json_encode($res));
        if ($res['bizData']) {
            $bizData = $this->decrypt($res['bizData']);
            $bizData = json_decode($bizData, true);
        } else {
            return [
                'status' => '3',
                'message' => $res['respMsg'],
                'data' => $res,
            ];
        }
        if ($res['respCode'] == '0000') {
            $res['bizData'] = $bizData;
            return [
                'status' => '1',
                'message' => '退款成功',
                'data' => $res,
            ];
        } else {
            $res['bizData'] = $bizData;
            return [
                'status' => '2',
                'message' => $res['respMsg'],
                'data' => $res,
            ];
        }

    }

    public function refund_query($data)
    {
        $orgId = $data['org_id'];
        $merId = $data['mer_id'];//"QY0201000003968"
        $requestNo = date('YmdHis') . mt_rand(1000, 9999);;
        $origRequestNo = $data['orig_request_no'];
        $this->deleteAllParams();
        $this->setParam('origRequestNo', $origRequestNo);
        $res = $this->requestChannel($this->getAllParams(), $this->tranCode['refundQuery'], 3, $merId, $requestNo, $orgId); //使用正确商户号

        Log::info("易生数科 退款查询 res:" . json_encode($res));
        if ($res['bizData']) {
            $bizData = $this->decrypt($res['bizData']);
            $bizData = json_decode($bizData, true);
        } else {
            return [
                'status' => '3',
                'message' => $res['respMsg'],
                'data' => $res,
            ];
        }
        if ($bizData['tradeStatus'] == 'TRADE_SUCCESS') {
            $res['bizData'] = $bizData;
            return [
                'status' => '1',
                'message' => '退款成功',
                'data' => $res,
            ];
        } else {
            $res['bizData'] = $bizData;
            return [
                'status' => '2',
                'message' => $res['respMsg'],
                'data' => $res,
            ];
        }

    }

    public function getUnOpenid($data)
    {
        $orgId = $data['org_id'];
        $merId = $data['mer_id'];//"QY0201000003968"
        $unionPayAuthCode = $data['unionPayAuthCode'];
        $unionPayAppID = $data['unionPayAppID'];
        $domain = strstr($unionPayAuthCode, '+');
        if ($domain) {
            $unionPayAuthCode = str_replace('+', '$#$#$', $unionPayAuthCode);
        }
        $requestNo = date('YmdHis') . mt_rand(1000, 9999);
        $this->deleteAllParams();
        $this->setParam('prodCode', "SM101");
        $this->setParam('unionPayAuthCode', $unionPayAuthCode);
        $this->setParam('unionPayAppID', $unionPayAppID);
        $res = $this->requestChannel($this->getAllParams(), $this->tranCode['unionPayCode'], 3, $merId, $requestNo, $orgId); //使用正确商户号

        Log::info("易生数科 获取银联用户标识 res:" . json_encode($res));
        if ($res['bizData']) {
            $bizData = $this->decrypt($res['bizData']);
            $bizData = json_decode($bizData, true);
        } else {
            return [
                'status' => '3',
                'message' => '获取失败',
                'data' => $res,
            ];
        }
        if ($res['respCode'] == '0000') {
            $res['bizData'] = $bizData;
            return [
                'status' => '1',
                'message' => '获取成功',
                'data' => $res,
            ];
        } else {
            return [
                'status' => '2',
                'message' => '获取失败',
                'data' => $res,
            ];
        }

    }

//    public function getReconciliation($data)
    public function getReconciliation()
    {
        $orgId = 'QY0064';
        $merId = 'QY0201000003968';//"QY0201000003968"
        $settleDate = date('YmdHis');
        $fileType = '01';
        $requestNo = date('YmdHis') . mt_rand(1000, 9999);
        $this->deleteAllParams();
        $this->setParam('settleDate', $settleDate);
        $this->setParam('fileType', $fileType);
        $res = $this->requestChannel($this->getAllParams(), $this->tranCode['reconciliation'], 1, $merId, $requestNo, $orgId); //使用正确商户号

        Log::info("易生数科 付账文件下载 res:" . json_encode($res));
        if ($res['bizData']) {
            $bizData = $this->decrypt($res['bizData']);
            $bizData = json_decode($bizData, true);
        } else {
            return [
                'status' => '3',
                'message' => $res['respMsg'],
                'data' => $res,
            ];
        }
        if ($res['respCode'] == '0000') {
            $fileContent = $this->decrypt($bizData['fileContent']);
            $fileContent = json_decode($fileContent, true);
            $res['bizData']['fileContent'] = $fileContent;
            $res['bizData'] = $bizData;
            return [
                'status' => '1',
                'message' => '获取成功',
                'data' => $res,
            ];
        } else {
            return [
                'status' => '2',
                'message' => $res['respMsg'],
                'data' => $res,
            ];
        }

    }


    private static function returnFun($data = null, $code = '00', $msg = 'SUCCESS')
    {
        return ['respCode' => $code, 'respMsg' => $msg, 'respData' => $data];
    }


}