<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2020/6/8
 * Time: 下午5:36
 */

namespace App\Api\Controllers\EasySkPay;

use App\Api\Controllers\BaseController;
use App\Common\PaySuccessAction;
use App\Models\Order;
use App\Models\UserOrg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class NotifyController extends BaseController
{
    //易生数科 支付回调
    public function pay_notify_url(Request $request)
    {
        try {
            $data = $request->all();
            Log::info('易生数科-支付回调');
            if ($data['bizData']) {
                $BaseController = new \App\Api\Controllers\EasySkPay\BaseController();
                $bizData = $BaseController->decrypt($data['bizData']);
                $bizData = json_decode($bizData, true);
                $data['bizData'] = $bizData;
            } else {
                return [
                    'status' => '3',
                    'message' => $data['respMsg'],
                    'data' => $data,
                ];
            }
            Log::info($data);
            if (isset($data) && !empty($data)) {
                if (isset($data['requestNo'])) {
                    $out_trade_no = $data['requestNo']; //商户订单号(原交易流水)
                    $trade_no = $data['tradeNo']; //系统订单号
                    $day = date('Ymd', time());
                    $table = 'orders_' . $day;
                    if (Schema::hasTable($table)) {
                        $order = DB::table($table)->where('out_trade_no', $out_trade_no)->first();
                    } else {
                        $order = Order::where('out_trade_no', $out_trade_no)->first();
//                        if (!$order) {
//                            $order = Order::where('trade_no', $trade_no)->first();
//                        }
                    }
                    Log::info($order);
                    //订单存在
                    if ($order) {
                        //系统订单未成功
                        if ($order->pay_status == '2') {
                            $pay_time = (isset($data['bizData']['payTime']) && !empty($data['bizData']['payTime'])) ? date('Y-m-d H:i:m', strtotime($data['bizData']['payTime'])) : '';
                            $buyer_pay_amount = isset($data['bizData']['amount']) ? ($data['bizData']['amount'] / 100) : ''; //实付金额，单位分
                            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                            $buyer_id = $data['bizData']['channelOpenId'] ? ($data['bizData']['channelOpenId']) : '';
                            $in_data = [
                                'status' => '1',
                                'pay_status' => 1,
                                'pay_status_desc' => '支付成功',
                                'trade_no' => $trade_no,
                                'pay_time' => $pay_time,
                                'buyer_id' => $buyer_id,
                                'buyer_pay_amount' => $buyer_pay_amount,
                                'receipt_amount' => $buyer_pay_amount,
                            ];

                            $this->update_day_order($in_data, $out_trade_no);

                            if (strpos($out_trade_no, 'scan')) {

                            } else {
                                //支付成功后的动作
                                $data = [
                                    'ways_type' => $order->ways_type,
                                    'company' => $order->company,
                                    'ways_type_desc' => $order->ways_type_desc,
                                    'source_type' => '32000', //返佣来源
                                    'source_desc' => '易生数科', //返佣来源说明
                                    'total_amount' => $order->total_amount,
                                    'out_trade_no' => $order->out_trade_no,
                                    'other_no' => $order->other_no,
                                    'rate' => $order->rate,
                                    'fee_amount' => $order->fee_amount,
                                    'merchant_id' => $order->merchant_id,
                                    'store_id' => $order->store_id,
                                    'user_id' => $order->user_id,
                                    'config_id' => $order->config_id,
                                    'store_name' => $order->store_name,
                                    'ways_source' => $order->ways_source,
                                    'pay_time' => $pay_time,
                                    'device_id' => $order->device_id,
                                ];
                                PaySuccessAction::action($data);
                            }
                        }
                    }
                }

                return json_encode([
                    'respCode' => '0000',
                    'respMsg' => '成功'
                ]);
            } else {
                Log::info('易生数科-支付回调-结果');
                Log::info($data);
            }

        } catch (\Exception $ex) {
            Log::info('易生数科支付回调-异步报错');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }
    }

    function pay_notify_url_u(Request $request)
    {
        $message = "支付成功";
        return view('success.success', compact('message'));
    }


}
