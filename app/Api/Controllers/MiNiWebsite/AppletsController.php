<?php

namespace App\Api\Controllers\MiNiWebsite;

use App\Api\Controllers\BaseController;
use App\Api\Controllers\User\StoreController;
use App\Models\Customer;
use App\Models\Statistics;
use App\Models\WechatsUser;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class AppletsController extends BaseController
{
    //微官网 意向用户
    public function getIntentionality(Request $request)
    {
        $token = $this->parseToken();
        $config_id = $token->config_id;
        $page = $request->get('p', '1'); //当前页数
        $limit = $request->get('l', '10'); //每页显示数量
        $id = $request->post('id', ''); //用户id
        $store_id = $request->post('storeId', ''); //门店id
        $name = $request->post('name', ''); //客户姓名
        $wechat_mobile = $request->post('wechatMobile', ''); //客户手机号
        $intentionality = $request->post('intentionality', ''); //意向度
        $storeObj = Store::where('store_id', $store_id)
            ->where('is_close', 0)
            ->where('is_delete', 0)
            ->first();
        if (!$storeObj) {
            return json_encode([
                'status' => 2,
                'message' => '门店不存在或状态异常,请联系管理员'
            ]);
        }
        try {
            $where = [];
            if ($id) {
                $where[] = ['id', '=', $id];
            }
            if ($name) {
                $where[] = ['name', '=', $name];
            }
            if ($wechat_mobile) {
                $where[] = ['wechat_mobile', '=', $wechat_mobile];
            }
            if ($intentionality) {
                $where[] = ['intentionality', '=', $intentionality];
            }
            $customerObj = WechatsUser::where('store_id', $store_id)
//                ->whereNotNull('wechat_mobile')
                ->where($where)
                ->orderBy('created_at', 'desc');
            $this->t = $customerObj->count();
            $customerObjData = $this->page($customerObj)->get();
            foreach ($customerObjData as $k => $v) {
                $customerObjData[$k]['wechat_username'] = json_decode($v['wechat_username']);
            }
            $this->status = 1;
            $this->message = '查询成功';
            return $this->format($customerObjData);
        } catch (\Exception $ex) {
            Log::info('微官网-获取意向用户-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }

    //微官网 访问用户
//    public function visitorNumber(Request $request)
//    {
//        $token = $this->parseToken();
//        $config_id = $token->config_id;
//        $store_id = $request->post('storeId', ''); //门店id
//        $type = $request->post('type', ''); //门店id
//        $storeObj = Store::where('store_id', $store_id)
//            ->where('is_close', 0)
//            ->where('is_delete', 0)
//            ->first();
//        if (!$storeObj) {
//            return json_encode([
//                'status' => 2,
//                'message' => '门店不存在或状态异常,请联系管理员'
//            ]);
//        }
//        try {
//            $where = [];
//            if ($id) {
//                $where[] = ['id', '=', $id];
//            }
//            if ($name) {
//                $where[] = ['name', '=', $name];
//            }
//            if ($wechat_mobile) {
//                $where[] = ['wechat_mobile', '=', $wechat_mobile];
//            }
//            if ($intentionality) {
//                $where[] = ['intentionality', '=', $intentionality];
//            }
//            $customerObj = Customer::where('store_id', $store_id)
//                ->where($where)
//                ->orderBy('updated_at', 'asc');
//            $this->t = $customerObj->count();
//            $customerObjData = $this->page($customerObj)->get();
//            $this->status = 1;
//            $this->message = '查询成功';
//            return $this->format($customerObjData);
//        } catch (\Exception $ex) {
//            Log::info('微官网-访问用户error');
//            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
//            $this->status = -1;
//            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
//            return $this->format();
//        }
//    }

    //统计数量
    public function memberCounts(Request $request)
    {
        try {
            $token = $this->parseToken();
            $merchant_id = $token->merchant_id;
            $config_id = $token->config_id;
            $store_id = $request->get('storeId', ''); //门店id
            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }
            $time_start = date('Y-m-d 00:00:00', time());
            $time_end = date('Y-m-d 23:59:59', time());
            $time_yes_start = date('Y-m-d 00:00:00', strtotime("-1 day"));
            $time_yes_end = date('Y-m-d 23:59:59', strtotime("-1 day"));

            //总点击
            $all_click = Statistics::where('store_id', $store_id)
                ->select('id')
                ->get()
                ->count('id');

            //今日点击
            $day_click = Statistics::where('store_id', $store_id)
                ->where('time_start', '=', $time_start)
                ->where('time_end', '=', $time_end)
                ->select('id')
                ->get()
                ->count('id');

            //昨日点击
            $yesterday_click = Statistics::where('store_id', $store_id)
                ->where('time_start', '=', $time_yes_start)
                ->where('time_end', '=', $time_yes_end)
                ->select('id')
                ->get()
                ->count('id');

            //今日意向用户
            $day_customer = WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->whereNotNull('wechat_mobile')
                ->select('id')
                ->get()
                ->count('id');

            //昨日意向用户
            $yesterday_customer = WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_yes_start)
                ->where('created_at', '<=', $time_yes_end)
                ->whereNotNull('wechat_mobile')
                ->select('id')
                ->get()
                ->count('id');

            //今日访问用户
            $day_visitor= WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->select('id')
                ->get()
                ->count('id');

            //昨日访问用户
            $yesterday_visitor= WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_yes_start)
                ->where('created_at', '<=', $time_yes_end)
                ->select('id')
                ->get()
                ->count('id');

            //意向客户
            $customer_count = WechatsUser::where('store_id', $store_id)
                ->whereNotNull('wechat_mobile')
                ->select('id')
                ->get()
                ->count('id');

            //非意向客户
            $visitor_count = WechatsUser::where('store_id', $store_id)
                ->whereNull('wechat_mobile')
                ->select('id')
                ->get()
                ->count('id');

            $secondsObj = WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->whereNotNull('wechat_mobile')
                ->sum('seconds');

            $yesSeconds = WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_yes_start)
                ->where('created_at', '<=', $time_yes_end)
                ->whereNotNull('wechat_mobile')
                ->sum('seconds');

            $secondsAll = WechatsUser::where('store_id', $store_id)
                ->whereNotNull('wechat_mobile')
                ->sum('seconds');

            if ($day_customer == 0) {
                $day_customer_seconds = $secondsObj;
            } else {
                $day_customer_seconds = round($secondsObj / $day_customer);
            }
            if ($customer_count == 0) {
                $all_customer_seconds = $secondsAll;
            } else {
                $all_customer_seconds = round($secondsAll / $customer_count);
            }
            if ($yesterday_customer == 0) {
                $yesterday_customer_seconds = $yesSeconds;
            } else {
                $yesterday_customer_seconds = round($yesSeconds / $yesterday_customer);
            }


            return json_encode(
                [
                    'status' => 1,
                    'message' => '查询成功',
                    'data' => [
                        'all_click' => $all_click,
                        'day_click' => $day_click,
                        'day_customer' => $day_customer,
                        'day_visitor' => $day_visitor,
                        'customer_count' => $customer_count,
                        'visitor_count' => $visitor_count,
                        'yesterday_click' => $yesterday_click,
                        'yesterday_customer' => $yesterday_customer,
                        'yesterday_visitor' => $yesterday_visitor,
                        'day_customer_seconds' => $day_customer_seconds,
                        'all_customer_seconds' => $all_customer_seconds,
                        'yesterday_customer_seconds' => $yesterday_customer_seconds
                    ],
                ]
            );
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }

    //统计七天数量
    public function sevenCounts(Request $request)
    {
        try {
            $token = $this->parseToken();
            $merchant_id = $token->merchant_id;
            $config_id = $token->config_id;
            $store_id = $request->get('storeId', ''); //门店id
            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }
            $time_start = date('Y-m-d 00:00:00', time());
            $time_end = date('Y-m-d 23:59:59', time());

            $time_yes_start = date('Y-m-d 00:00:00', strtotime("-1 day"));
            $time_yes_end = date('Y-m-d 23:59:59', strtotime("-1 day"));

            $time_before_yes_start = date('Y-m-d 00:00:00', strtotime("-2 day"));
            $time_before_yes_end = date('Y-m-d 23:59:59', strtotime("-2 day"));

            $time_three_start = date('Y-m-d 00:00:00', strtotime("-3 day"));
            $time_three_end = date('Y-m-d 23:59:59', strtotime("-3 day"));

            $time_four_start = date('Y-m-d 00:00:00', strtotime("-4 day"));
            $time_four_end = date('Y-m-d 23:59:59', strtotime("-4 day"));

            $time_five_start = date('Y-m-d 00:00:00', strtotime("-5 day"));
            $time_five_end = date('Y-m-d 23:59:59', strtotime("-5 day"));

            $time_six_start = date('Y-m-d 00:00:00', strtotime("-6 day"));
            $time_six_end = date('Y-m-d 23:59:59', strtotime("-6 day"));

            //今日意向用户
            $day_customer = WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->whereNotNull('wechat_mobile')
                ->select('id')
                ->get()
                ->count('id');

            //昨日意向用户
            $yesterday_customer = WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_yes_start)
                ->where('created_at', '<=', $time_yes_end)
                ->whereNotNull('wechat_mobile')
                ->select('id')
                ->get()
                ->count('id');

            //前天意向用户
            $before_customer = WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_before_yes_start)
                ->where('created_at', '<=', $time_before_yes_end)
                ->whereNotNull('wechat_mobile')
                ->select('id')
                ->get()
                ->count('id');

            //三天前意向用户
            $three_customer = WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_three_start)
                ->where('created_at', '<=', $time_three_end)
                ->whereNotNull('wechat_mobile')
                ->select('id')
                ->get()
                ->count('id');

            //四天前意向用户
            $four_customer = WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_four_start)
                ->where('created_at', '<=', $time_four_end)
                ->whereNotNull('wechat_mobile')
                ->select('id')
                ->get()
                ->count('id');

            //五天前意向用户
            $five_customer = WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_five_start)
                ->where('created_at', '<=', $time_five_end)
                ->whereNotNull('wechat_mobile')
                ->select('id')
                ->get()
                ->count('id');

            //六天前意向用户
            $six_customer = WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_six_start)
                ->where('created_at', '<=', $time_six_end)
                ->whereNotNull('wechat_mobile')
                ->select('id')
                ->get()
                ->count('id');

            //今日访问用户
            $day_visitor= WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->select('id')
                ->get()
                ->count('id');

            //昨日访问用户
            $yesterday_visitor= WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_yes_start)
                ->where('created_at', '<=', $time_yes_end)
                ->select('id')
                ->get()
                ->count('id');

            //前天访问用户
            $before_visitor = WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_before_yes_start)
                ->where('created_at', '<=', $time_before_yes_end)
                ->select('id')
                ->get()
                ->count('id');

            //三天前访问用户
            $three_visitor = WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_three_start)
                ->where('created_at', '<=', $time_three_end)
                ->select('id')
                ->get()
                ->count('id');

            //四天前访问用户
            $four_visitor = WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_four_start)
                ->where('created_at', '<=', $time_four_end)
                ->select('id')
                ->get()
                ->count('id');

            //五天前访问用户
            $five_visitor = WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_five_start)
                ->where('created_at', '<=', $time_five_end)
                ->select('id')
                ->get()
                ->count('id');

            //六天前访问用户
            $six_visitor = WechatsUser::where('store_id', $store_id)
                ->where('created_at', '>=', $time_six_start)
                ->where('created_at', '<=', $time_six_end)
                ->select('id')
                ->get()
                ->count('id');

            return json_encode(
                [
                    'status' => 1,
                    'message' => '查询成功',
                    'data' => [
                        'day_customer' => $day_customer,
                        'yesterday_customer' => $yesterday_customer,
                        'before_customer' => $before_customer,
                        'three_customer' => $three_customer,
                        'four_customer' => $four_customer,
                        'five_customer' => $five_customer,
                        'six_customer' => $six_customer,
                        'day_visitor' => $day_visitor,
                        'yesterday_visitor' => $yesterday_visitor,
                        'before_visitor' => $before_visitor,
                        'three_visitor' => $three_visitor,
                        'four_visitor' => $four_visitor,
                        'five_visitor' => $five_visitor,
                        'six_visitor' => $six_visitor,
                    ],
                ]
            );
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }

    //当天点击量更新
    public function updateDailyHits(Request $request)
    {
//        $token = $this->parseToken();
//        $merchant_id = $token->merchant_id;
//        $config_id = $token->config_id;
        $store_id = $request->post('storeId', ''); //门店id
        $storeObj = Store::where('store_id', $store_id)
            ->where('is_close', 0)
            ->where('is_delete', 0)
            ->first();
        if (!$storeObj) {
            return json_encode([
                'status' => 2,
                'message' => '门店不存在或状态异常,请联系管理员'
            ]);
        }
        $time_start = date("Y-m-d 00:00:00", time());
        $time_end = date("Y-m-d 23:59:59", time());

        try {
            $where = [];
            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
            }
            if ($time_start) {
                $where[] = ['time_start', '>=', $time_start];
            }
            if ($time_end) {
                $where[] = ['time_end', '<=', $time_end];
            }
            //判断当天有无数据无数据新增访问数
            $infoObj = DB::table('statistics')->where($where)->first();
            if (!empty($infoObj)) {
                $times = $infoObj->times;
                $dataInfo = DB::table('statistics')->where($where)->update([
                    'times'                   => $times+1,
//                    'updated_at' => date("Y-m-d H:i:s", time())
                ]);
            } else {
                $data = [
                    'times' => 1,
                    'store_id' => $store_id,
                    'time_start' =>$time_start,
                    'time_end' =>$time_end,
                ];
                $dataInfo = Statistics::create($data);
            }
            if ($dataInfo) {
                return $this->responseDataJson(200, "操作成功");
            } else {
                return $this->responseDataJson(202, "操作失败");
            }
        } catch (\Exception $ex) {
            Log::info('微官网-每次点击量-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }

    //更新用户点击量
    public function updateBodyHits(Request $request)
    {
//        $token = $this->parseToken();
//        $merchant_id = $token->merchant_id;
//        $config_id = $token->config_id;
        $store_id = $request->get('storeId', ''); //门店id
        $wechat_openid = $request->get('openId', ''); //用户openid
        $storeObj = Store::where('store_id', $store_id)
            ->where('is_close', 0)
            ->where('is_delete', 0)
            ->first();
        if (!$storeObj) {
            return json_encode([
                'status' => 2,
                'message' => '门店不存在或状态异常,请联系管理员'
            ]);
        }
        try {
            $where = [];
            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
            }
            if ($wechat_openid) {
                $where[] = ['wechat_openid', '=', $wechat_openid];
            }
            //判断当天有无数据无数据新增访问数
            $infoObj = DB::table('wechats_user')->where($where)->first();
            if (!empty($infoObj)) {
                $times = $infoObj->times;
                $dataInfo = DB::table('wechats_user')->where($where)->update([
                    'times'                   => $times+1,
                ]);
            } else {
                $dataInfo = [];
            }
            if ($dataInfo) {
                return $this->responseDataJson(200, "操作成功");
            } else {
                return $this->responseDataJson(202, "操作失败");
            }
        } catch (\Exception $ex) {
            Log::info('微官网-用户点击量-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }

    //修改用户备注
    public function remarksUp(Request $request)
    {
        try {
            $token = $this->parseToken();
            $merchant_id = $token->merchant_id;
            $config_id = $token->config_id;
            $id = $request->get('id', '');
            $store_id = $request->get('storeId', '');
            $remarks = $request->get('remarks', '');
            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }

            $data = [
                'remarks' => $remarks,
            ];
            $obj = WechatsUser::where('id', $id)->update($data);
            if ($obj) {
                return $this->responseDataJson(200, "操作成功");
            } else {
                return $this->responseDataJson(202, "操作失败");
            }
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    //修改用户时长
    public function momentUp(Request $request)
    {
        try {
            $store_id = $request->get('storeId', ''); //门店id
            $wechat_openid = $request->get('openId', ''); //用户openid
            $seconds = $request->get('seconds', '');//秒数
            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }
            $where = [];
            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
            }
            if ($wechat_openid) {
                $where[] = ['wechat_openid', '=', $wechat_openid];
            }
            //判断有无当前用户
            $infoObj = DB::table('wechats_user')->where($where)->first();
            if (!$infoObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '当前用户不存在'
                ]);
            }
            $long_time = isset($infoObj->long_times) ? $infoObj->long_times : 0;
            if (($long_time - $seconds)> 0) {
                $longTime = $long_time;
            } else {
                $longTime = $seconds;
            }

            $intentionality = $infoObj->intentionality;
            $long_times = $infoObj->long_times;
            $login_times = $infoObj->times;

            $new_long_times = min(($long_times/100)*40, 40); //最长时长意向度
            $new_login_times = min(($login_times/10)*40, 40); //访问次数意向度
            $new_intentionality = min($intentionality + $new_long_times + $new_login_times, 100);

            $data = [
                'intentionality' => $new_intentionality,
                'seconds'   => $seconds,
                'long_times' => isset($longTime) ? $longTime : "",
            ];
            $dataInfo = WechatsUser::where($where)->update($data);
            if ($dataInfo) {
                return $this->responseDataJson(200, "操作成功");
            } else {
                return $this->responseDataJson(202, "操作失败");
            }
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    //判断openid用户是否获取手机号
    public function getOpenPhone(Request $request)
    {
        try {
            $store_id = $request->get('storeId', ''); //门店id
            $wechat_openid = $request->get('openId', ''); //用户openid
            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }
            $where = [];
            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
            }

            // 查询门店是否弹获取手机号
            $storeInfo = DB::table('stores')->select('is_get_phone')->where($where)->first();
            if (empty($storeInfo)) {
                return json_encode([
                    'status' => 200,
                    'message' => '暂无门店信息',
                ]);
            }

            if ($storeInfo->is_get_phone == 2) {
                return json_encode([
                    'status' => 200,
                    'message' => '当前门店关闭弹窗',
                ]);
            }

            if ($wechat_openid) {
                $where[] = ['wechat_openid', '=', $wechat_openid];
            }
            //判断有无当前用户
            $infoObj = DB::table('wechats_user')->where($where)->first();
            if (!$infoObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '当前用户不存在'
                ]);
            } else {
                $phone = $infoObj->wechat_mobile;
                if (!$phone) {
                    return json_encode([
                        'status' => 2,
                        'message' => '当前用户没有手机号'
                    ]);
                } else {
                    return json_encode([
                        'status' => 200,
                        'message' => '当前用户手机号',
                        'data' => $phone
                    ]);
                }
            }
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }
}
