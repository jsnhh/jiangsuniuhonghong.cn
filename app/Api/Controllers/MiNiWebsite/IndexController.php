<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2021/4/8
 * Time: 11:46
 */
namespace App\Api\Controllers\MiNiWebsite;

use App\Api\Controllers\BaseController;
use App\Api\Controllers\User\StoreController;
use App\Models\MiniWebsiteContact;
use App\Models\MiniWebsiteIndex;
use App\Models\MiniWebsiteShow;
use App\Models\MiniWebsiteTag;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class IndexController extends BaseController
{

    //微官网 编辑 首页模块
    public function updateMiniWebIndex(Request $request)
    {
        $token = $this->parseToken();
        $merchant_id = $token->merchant_id;
        $config_id = $token->config_id;

        $id = $request->post('id', ''); //id,更新必传
        $store_id = $request->post('storeId', ''); //门店id
        $pid = $request->post('pid', '0'); //上级分类id
        $type = $request->post('type', ''); //类型位置(1-首页;2-主营业务;3-关于我们;4-最新资讯;5-展示;6-other)
        $sort = $request->post('sort', ''); //排序,1-999
        $title = $request->post('title', ''); //标题
        $sub_title = $request->post('subTitle', ''); //副标题
        $icon_url = $request->post('iconUrl', ''); //小图标地址
        $cover_url = $request->post('coverUrl', ''); //封面图片地址
        $video_url = $request->post('videoUrl', ''); //视频地址
        $content = $request->post('content', ''); //内容
        $status = $request->post('status', ''); //状态(1-开启;2-禁用)

        $data = [
            'config_id' => $config_id
        ];
        if ($store_id) {
            $storeObj = $this->storeIsExists($store_id);
            if (!$storeObj) {
                $this->status = 2;
                $this->message = '门店不存在或状态异常';
                return $this->format();
            }
            $data['store_id'] = $store_id;
        }
        if ($pid) $data['pid'] = $pid;
        if ($type) $data['type'] = $type;
        if ($sort) $data['sort'] = $sort;
        if ($title) $data['title'] = $this->filteringUserInput($title);
        if ($sub_title) $data['sub_title'] = $this->filteringUserInput($sub_title);
        if ($icon_url) $data['icon_url'] = $icon_url;
        if ($cover_url) $data['cover_url'] = $cover_url;
        if ($video_url) $data['video_url'] = $video_url;
        if ($content) $data['content'] = $this->filteringUserInput($content);
        if ($status) $data['status'] = $status;

        try {
            if ($id) {
                $obj = MiniWebsiteIndex::find($id);
                $res = $obj->update($data);
                if ($res) {
                    $this->status = 1;
                    $this->message = '更新成功';
                    return $this->format();
                } else {
                    $this->status = 2;
                    $this->message = '更新失败';
                    return $this->format();
                }
            } else {
                $res = MiniWebsiteIndex::create($data);
                if ($res) {
                    $this->status = 1;
                    $this->message = '添加成功';
                    return $this->format($res);
                } else {
                    $this->status = 2;
                    $this->message = '添加失败';
                    return $this->format();
                }
            }
        } catch (\Exception $ex) {
            Log::info('微官网-首页模块编辑-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //微官网 获取 首页模块
    public function getMiniWebIndex(Request $request)
    {
//        $token = $this->parseToken();
//        $config_id = $token->config_id;

        $page = $request->get('p', '1'); //当前页数
        $limit = $request->get('l', '10'); //每页显示数量
        $id = $request->post('id', ''); //id,更新必传
        $store_id = $request->post('storeId', ''); //门店id
        $pid = $request->post('pid', '0'); //上级分类id
        $type = $request->post('type', ''); //类型位置(1-首页;2-主营业务;3-关于我们;4-最新资讯;5-展示;6-other)
        $status = $request->post('status', ''); //状态(1-开启;2-禁用)
        $order_by = $request->post('orderBy', 'asc'); //排序规则(asc-升序;desc-降序)

        try {
            $where = [];
            if ($id) {
                $where[] = ['id', '=', $id];
            }
            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
            }
            if ($pid) {
                $where[] = ['pid', '=', $pid];
            }
            if ($type) {
                $where[] = ['type', '=', $type];
            }
            if ($status) {
                $where[] = ['status', '=', $status];
            }
            if (!$order_by) $order_by = 'asc';
            $miniWebsiteIndexObj = MiniWebsiteIndex::where($where)
                ->orderBy('sort', $order_by);

            $this->t = $miniWebsiteIndexObj->count();
            $miniWebsiteIndexData = $this->page($miniWebsiteIndexObj)->get();
            $this->status = 1;
            $this->message = '查询成功';
            return $this->format($miniWebsiteIndexData);
        } catch (\Exception $ex) {
            Log::info('微官网-获取首页模块-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //微官网 删除 首页模块
    public function delMiniWebIndex(Request $request)
    {
        $cid = $request->post('id', ''); //id,必传

        $check_data = [
            'id' => '操作id'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = -1;
            $this->message = $check;
            return $this->format();
        }

        try {
            $res = MiniWebsiteIndex::find($cid)->delete();
            if ($res) {
                $this->status = 1;
                $this->message = '删除成功';
                return $this->format();
            } else {
                $this->status = 2;
                $this->message = '删除失败';
                return $this->format();
            }
        } catch (\Exception $ex) {
            Log::info('微官网-删除首页模块-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }
    
    
    //微官网 编辑 标签
    public function updateMiniWebTag(Request $request)
    {
        $token = $this->parseToken();
        $config_id = $token->config_id;

        $id = $request->post('id', ''); //id,更新必传
        $store_id = $request->post('storeId', ''); //门店id
        $name = $request->post('name', ''); //名称
        $sort = $request->post('sort', ''); //排序,1-999
        $status = $request->post('status', ''); //状态(1-开启;2-禁用)

        try {
            $data = [
                'config_id' => $config_id
            ];
            if ($store_id) {
                $storeObj = $this->storeIsExists($store_id);
                if (!$storeObj) {
                    $this->status = 2;
                    $this->message = '门店不存在或状态异常';
                    return $this->format();
                }
                $data['store_id'] = $store_id;
            }
            if ($name) $data['name'] = $name;
            if ($sort) $data['status'] = $sort;
            if ($status) $data['sort'] = $status;

            if ($id) {
                $obj = MiniWebsiteTag::find($id);
                $res = $obj->update($data);
                if ($res) {
                    $this->status = 1;
                    $this->message = '更新成功';
                    return $this->format();
                } else {
                    $this->status = 2;
                    $this->message = '更新失败';
                    return $this->format();
                }
            } else {
                $res = MiniWebsiteTag::create($data);
                if ($res) {
                    $this->status = 1;
                    $this->message = '新增成功';
                    return $this->format();
                } else {
                    $this->status = 2;
                    $this->message = '新增失败';
                    return $this->format();
                }
            }
        } catch (\Exception $ex) {
            Log::info('微官网-编辑标签-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //微官网 获取 标签
    public function getMiniWebTag(Request $request)
    {
//        $token = $this->parseToken();
//        $config_id = $token->config_id;

        $id = $request->post('id', ''); //标签id
        $store_id = $request->post('storeId', ''); //门店id
        $name = $request->post('name', ''); //名称
        $order_by = $request->post('orderBy', 'asc'); //排序规则(asc-升序;desc-降序)
        $status = $request->post('status', ''); //状态(1-开启;2-禁用)

        try {
            $where = [];
            if ($store_id) $where[] = ['store_id', '=', $store_id];
            if ($name) $where[] = ['name', 'like', $name.'%'];
            if ($status) $where[] = ['status', '=', $status];
            if ($id) $where[] = ['id', '=', $id];
            if (!$order_by) $order_by = 'asc';

            $miniWebsiteTagObj = MiniWebsiteTag::where($where)
                ->orderBy('sort', $order_by)
                ->get();
            if ($miniWebsiteTagObj) {
                $this->status = 1;
                $this->message = 'ok';
                return $this->format($miniWebsiteTagObj);
            } else {
                $this->status = 2;
                $this->message = 'fails';
                return $this->format();
            }

        } catch (\Exception $ex) {
            Log::info('微官网-获取标签-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //微官网 删除 标签
    public function delMiniWebTag(Request $request)
    {
        $token = $this->parseToken();
        $config_id = $token->config_id;

        $id = $request->post('id', ''); //标签id
        $store_id = $request->post('storeId', ''); //门店id

        try {
            $where = [];
            if ($store_id) $where[] = ['store_id', '=', $store_id];
            if ($id) $where[] = ['id', '=', $id];

            $miniWebsiteTagObj = MiniWebsiteTag::where($where)->delete();
            if ($miniWebsiteTagObj) {
                $this->status = 1;
                $this->message = '删除成功';
                return $this->format($miniWebsiteTagObj);
            } else {
                $this->status = 2;
                $this->message = '删除失败';
                return $this->format();
            }
        } catch (\Exception $ex) {
            Log::info('微官网-删除标签-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //微官网 编辑 展示列表
    public function updateMiniWebShowList(Request $request)
    {
        $token = $this->parseToken();
        $config_id = $token->config_id;

        $id = $request->post('id', ''); //id,更新必传
        $store_id = $request->post('storeId', ''); //门店id
        $tag_id = $request->post('tagId', ''); //标签id
        $price_show = $request->post('priceShow', ''); //是否显示价格(1-显示;2-不显示)
        $price = $request->post('price', ''); //价格
        $sort = $request->post('sort', ''); //排序,1-999
        $title = $request->post('title', ''); //标题
        $sub_title = $request->post('subTitle', ''); //副标题
        $cover_url = $request->post('coverUrl', ''); //封面图片地址
        $banner_url = $request->post('bannerUrl', ''); //banner图片地址
        $video_url = $request->post('videoUrl', ''); //视频地址
        $content = $request->post('content', ''); //内容
        $status = $request->post('status', ''); //状态(1-开启;2-禁用)

        try {
            $data = [
                'config_id' => $config_id
            ];
            if ($store_id) {
                $storeObj = $this->storeIsExists($store_id);
                if (!$storeObj) {
                    $this->status = 2;
                    $this->message = '门店不存在或状态异常';
                    return $this->format();
                }
                $data['store_id'] = $store_id;
            }
            if ($tag_id) $data['tag_id'] = $tag_id;
            if ($price_show) $data['price_show'] = $price_show;
            if ($price) $data['price'] = $price;
            if ($sort) $data['sort'] = $sort;
            if ($title) $data['title'] = $title;
            if ($sub_title) $data['sub_title'] = $sub_title;
            if ($cover_url) $data['cover_url'] = $cover_url;
            if ($banner_url) $data['banner_url'] = $banner_url;
            if ($video_url) $data['video_url'] = $video_url;
            if ($content) $data['content'] = $this->filteringUserInput($content);
            if ($status) $data['status'] = $status;

            if ($id) {
                $obj = MiniWebsiteShow::find($id);
                $res = $obj->update($data);
                if ($res) {
                    $this->status = 1;
                    $this->message = '更新成功';
                    return $this->format();
                } else {
                    $this->status = 2;
                    $this->message = '更新失败';
                    return $this->format();
                }
            } else {
                $res = MiniWebsiteShow::create($data);
                if ($res) {
                    $this->status = 1;
                    $this->message = '新增成功';
                    return $this->format();
                } else {
                    $this->status = 2;
                    $this->message = '新增失败';
                    return $this->format();
                }
            }
        } catch (\Exception $ex) {
            Log::info('微官网-编辑展示列表-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //微官网 获取 展示列表
    public function getMiniWebShowList(Request $request)
    {
//        $token = $this->parseToken();
//        $config_id = $token->config_id;

        $page = $request->get('p', '1'); //当前页数
        $limit = $request->get('l', '10'); //每页显示数量
        $id = $request->post('id', ''); //id
        $store_id = $request->post('storeId', ''); //门店id
        $tag_id = $request->post('tagId', ''); //标签id
        $order_by = $request->post('orderBy', 'asc'); //排序规则(asc-升序;desc-降序)
        $status = $request->post('status', ''); //状态(1-开启;2-禁用)
        $price_show = $request->post('priceShow', ''); //是否显示价格(1-显示;2-不显示)

        try {
            $where = [];
            if ($id) $where[] = ['mws.id', '=', $id];
            if ($store_id) $where[] = ['mws.store_id', '=', $store_id];
            if ($tag_id) $where[] = ['mws.tag_id', '=', $tag_id];
            if ($status) $where[] = ['mws.status', '=', $status];
            if ($price_show) $where[] = ['mws.price_show', '=', $price_show];
            if (!$order_by) $order_by = 'asc';

            $miniWebsiteShowObj = DB::table('mini_website_shows as mws')
                ->select('mws.*', 'mwt.name as tag_name')
                ->where($where)
                ->leftjoin('mini_website_tags as mwt', function ($join) {
                    $join->on('mwt.id', '=', 'mws.tag_id')
                    ->where('mwt.status', '=', '1');
                })
                ->orderBy('mws.sort', $order_by);

            $this->t = $miniWebsiteShowObj->count();
            $miniWebsiteShowDate = $this->page($miniWebsiteShowObj)->get();
            $this->status = 1;
            return $this->format($miniWebsiteShowDate);
        } catch (\Exception $ex) {
            Log::info('微官网-获取标签-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //微官网 删除 展示列表
    public function delMiniWebShowList(Request $request)
    {
        $token = $this->parseToken();
        $config_id = $token->config_id;

        $id = $request->post('id', ''); //标签id
        $store_id = $request->post('storeId', ''); //门店id

        try {
            $where = [];
            if ($store_id) $where[] = ['store_id', '=', $store_id];
            if ($id) $where[] = ['id', '=', $id];

            $miniWebsiteTagObj = MiniWebsiteShow::where($where)->delete();
            if ($miniWebsiteTagObj) {
                $this->status = 1;
                $this->message = '删除成功';
                return $this->format($miniWebsiteTagObj);
            } else {
                $this->status = 2;
                $this->message = '删除失败';
                return $this->format();
            }
        } catch (\Exception $ex) {
            Log::info('微官网-删除展示列表-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //微官网 编辑 联系我们
    public function updateMiniWebContact(Request $request)
    {
        $token = $this->parseToken();
        $config_id = $token->config_id;

        $id = $request->post('id', ''); //id,更新必传
        $store_id = $request->post('storeId', ''); //门店id
        $store_name = $request->post('storeName', ''); //门店名称
        $s_time = $request->post('sTime', ''); //营业开始时间
        $e_time = $request->post('eTime', ''); //营业结束时间
        $cover_url = $request->post('coverUrl', ''); //封面图片地址
        $telephone = $request->post('telephone', ''); //电话
        $email = $request->post('email', ''); //邮箱
        $website = $request->post('website', ''); //网址
        $address = $request->post('address', ''); //地址
        $status = $request->post('status', ''); //状态(1-开启;2-禁用)
        $lng = $request->post('lng', ''); //经度
        $lat = $request->post('lat', ''); //纬度

        try {
            $data = [
                'config_id' => $config_id
            ];
            $storeObj = '';
            if ($store_id) {
                $storeObj = Store::where('store_id', $store_id)
                    ->where('is_close', 0)
                    ->where('is_delete', 0)
                    ->select('store_name', 'store_short_name', 's_time', 'e_time', 'lat', 'lng', 'province_name', 'city_name', 'area_name', 'store_address')
                    ->first();
                if (!$storeObj) {
                    $this->status = 2;
                    $this->message = '门店不存在或状态异常';
                    return $this->format();
                }
                $data['store_id'] = $store_id;
            }
            if ($store_name) {
                $data['store_name'] = $store_name;
            } else {
                if ($storeObj) {
                    $data['store_name'] = $storeObj->store_name;
                }
            }
            if ($s_time) {
                $data['s_time'] = $s_time;
            } else {
                if ($storeObj) {
                    $data['s_time'] = $storeObj->s_time;
                }
            }
            if ($e_time) {
                $data['e_time'] = $e_time;
            } else {
                if ($storeObj) {
                    $data['s_time'] = $storeObj->e_time;
                }
            }
            if ($cover_url) $data['cover_url'] = $cover_url;
            if ($telephone) $data['telephone'] = $telephone;
            if ($email) $data['email'] = $email;
            if ($website) $data['website'] = $website;
            if ($lng) $data['lng'] = $lng;
            if ($lat) $data['lat'] = $lat;
            if ($address) {
                $address = trim($address);
                $data['address'] = $address;

                $queryAddressObj = new StoreController();
                $api_res = $queryAddressObj->query_address($address, env('LBS_KEY'));
                if ($api_res['status'] == '1') {
                    $store_lng = $api_res['result']['lng'];
                    $store_lat = $api_res['result']['lat'];

                    $data['lat'] = $store_lat;
                    $data['lng'] = $store_lng;
                } else {
                    Log::info('微官网 编辑 联系我们-获取经纬度错误');
                    Log::info($api_res['message']);
                }
            } else {
                if ($storeObj) {
                    $data['address'] = $storeObj->province_name.$storeObj->city_name.$storeObj->area_name.$storeObj->store_address;

                    if (!isset($data['lat']) && !isset($data['lng'])) {
                        $data['lat'] = $storeObj->lat;
                        $data['lng'] = $storeObj->lng;
                    } else {
                        $new_address = '';
                        if (isset($storeObj->province_name) && isset($storeObj->province_code)) {
                            $new_address = $storeObj->province_name;
                        }
                        if (isset($storeObj->city_name) && isset($storeObj->city_code)) {
                            $new_address .= $storeObj->city_name;
                        }
                        if (isset($storeObj->area_name) && isset($storeObj->area_code)) {
                            $new_address .= $storeObj->area_name;
                        }
                        $new_address .= $storeObj->store_address;
                        $new_address = trim($new_address);
                        $queryAddressObj = new StoreController();
                        $api_res = $queryAddressObj->query_address($new_address, env('LBS_KEY'));
                        if ($api_res['status'] == '1') {
                            $store_lng = $api_res['result']['lng'];
                            $store_lat = $api_res['result']['lat'];

                            $data['lat'] = $store_lat;
                            $data['lng'] = $store_lng;
                        } else {
                            Log::info('微官网-编辑联系我们-获取经纬度错误22: ');
                            Log::info($api_res['message']);
                        }
                    }
                }
            }
            if ($status) $data['status'] = $status;

            if ($id) {
                $obj = MiniWebsiteContact::find($id);
                $res = $obj->update($data);
                if ($res) {
                    $this->status = 1;
                    $this->message = '更新成功';
                    return $this->format();
                } else {
                    $this->status = 2;
                    $this->message = '更新失败';
                    return $this->format();
                }
            } else {
                $res = MiniWebsiteContact::create($data);
                if ($res) {
                    $this->status = 1;
                    $this->message = '新增成功';
                    return $this->format();
                } else {
                    $this->status = 2;
                    $this->message = '新增失败';
                    return $this->format();
                }
            }
        } catch (\Exception $ex) {
            Log::info('微官网-编辑联系我们-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //微官网 获取 联系我们
    public function getMiniWebContact(Request $request)
    {
//        $token = $this->parseToken();
        $id = $request->post('id', ''); //标签id
        $store_id = $request->post('storeId', ''); //门店id
        $status = $request->post('status', '1'); //状态(1-开启;2-禁用)

        try {
            $where = [];
            if ($id) $where[] = ['id', '=', $id];
            if ($store_id) $where[] = ['store_id', '=', $store_id];
            if ($status) $where[] = ['status', '=', $status];

            $miniWebsiteShowObj = MiniWebsiteContact::where($where)
                ->first();
            if ($miniWebsiteShowObj) {
                $this->status = 1;
                $this->message = 'ok';
                return $this->format($miniWebsiteShowObj);
            } else {
                $this->status = 2;
                $this->message = 'fails';
                return $this->format();
            }
        } catch (\Exception $ex) {
            Log::info('微官网-获取联系我们-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }


    //微官网 删除 联系我们
    public function delMiniWebContact(Request $request)
    {
        $token = $this->parseToken();
        $id = $request->post('id', ''); //id
        $store_id = $request->post('storeId', ''); //门店id

        try {
            $where = [];
            if ($store_id) $where[] = ['store_id', '=', $store_id];
            if ($id) $where[] = ['id', '=', $id];

            $miniWebsiteTagObj = MiniWebsiteContact::where($where)->delete();
            if ($miniWebsiteTagObj) {
                $this->status = 1;
                $this->message = '删除成功';
                return $this->format();
            } else {
                $this->status = 2;
                $this->message = '删除失败';
                return $this->format();
            }
        } catch (\Exception $ex) {
            Log::info('微官网-删除联系我们-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
            $this->status = -1;
            $this->message = $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine();
            return $this->format();
        }
    }
    


    /**
     * 过滤用户输入
     * @param string $params
     * @return string
     */
    protected function filteringUserInput($params = '')
    {
        $text = trim($params);
        $text = strip_tags($text);
        $text = htmlentities($text, ENT_QUOTES, "UTF-8");

        return $text;
    }


    /**
     * 门店是否存在或状态异常
     * @param string $store_id
     * @return bool
     */
    protected function storeIsExists($store_id = '')
    {
        if (!$store_id) {
            return false;
        }

        $storeObj = Store::where('store_id', $store_id)
            ->where('is_close', 0)
            ->where('is_delete', 0)
            ->exists();
        if ($storeObj) {
            return true;
        } else {
            return false;
        }
    }


}
