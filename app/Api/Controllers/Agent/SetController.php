<?php
namespace App\Api\Controllers\Agent;


use App\Api\Controllers\BaseController;
use App\Models\StorePayWay;
use App\Models\User;
use App\Models\UserStoreSet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;

class SetController extends BaseController
{

    //扣款顺序列表
    public function pay_ways_sort(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');

            if ($store_id) {
                $alipay = DB::table('store_pay_ways')
                    ->where('store_id', $store_id)
                    ->where('ways_source', 'alipay')
                    ->select('id as store_pay_ways_id', 'ways_desc', 'ways_type', 'store_id', 'sort', 'ways_source')
                    ->where('status', 1)
                    ->orderBy('store_pay_ways.sort', 'asc')
                    ->get();
                $weixin = DB::table('store_pay_ways')
                    ->where('store_id', $store_id)
                    ->where('ways_source', 'weixin')
                    ->select('id as store_pay_ways_id', 'ways_desc', 'ways_type', 'store_id', 'sort', 'ways_source')
                    ->where('status', 1)
                    ->orderBy('store_pay_ways.sort', 'asc')
                    ->get();


                $jd = DB::table('store_pay_ways')
                    ->where('store_id', $store_id)
                    ->where('ways_source', 'jd')
                    ->select('id as store_pay_ways_id', 'ways_desc', 'ways_type', 'store_id', 'sort', 'ways_source')
                    ->where('status', 1)
                    ->orderBy('store_pay_ways.sort', 'asc')
                    ->get();
                $unionpay = DB::table('store_pay_ways')
                    ->where('store_id', $store_id)
                    ->where('ways_source', 'unionpay')
                    ->select('id as store_pay_ways_id', 'ways_desc', 'ways_type', 'store_id', 'sort', 'ways_source')
                    ->where('status', 1)
                    ->orderBy('store_pay_ways.sort', 'asc')
                    ->get();

                return json_encode(['status' => 1, 'is_open' => 1, 'message' => '数据返回成功', 'data' => [
                    'ailpay' => $alipay,
                    'weixin' => $weixin,
                    'jd' => $jd,
                    'unionpay' => $unionpay
                ]
                ]);

            } else {
                return json_encode(['status' => 2, 'message' => '门店ID必须传']);
            }

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }


    //扣款顺序初始化
    public function pay_ways_sort_start(Request $request)
    {
        try {
            $user = $this->parseToken(); //
            $store_id = $request->get('store_id', '');
            $StorePayWay = StorePayWay::where('store_id', $store_id)
                ->where('status', 1)
                ->select('ways_source', 'sort', 'id')
                ->get();

            $a = 0;
            $b = 0;
            $c = 0;
            $d = 0;


            $user = User::where('id', $user->user_id)->first();
//            $hasPermission = $user->hasPermissionTo('通道初始化');
//
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '没有权限']);
//            }

            foreach ($StorePayWay as $k => $v) {
                //支付宝
                if ($v['ways_source'] == "alipay") {
                    $a = $a + 1;
                    StorePayWay::where('id', $v->id)->update(['sort' => $a]);
                }
                //微信
                if ($v['ways_source'] == "weixin") {
                    $b = $b + 1;
                    StorePayWay::where('id', $v->id)->update(['sort' => $b]);


                }

                //jd
                if ($v['ways_source'] == "jd") {

                    $c = $c + 1;
                    StorePayWay::where('id', $v->id)->update(['sort' => $c]);

                }

                //unionpay
                if ($v['ways_source'] == "unionpay") {
                    $d = $d + 1;
                    StorePayWay::where('id', $v->id)->update(['sort' => $d]);
                }

            }

            return json_encode(['status' => 1, 'message' => '初始化成功']);


        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }


    //扣款顺序修改
    public function pay_ways_sort_edit(Request $request)
    {
        try {
            $user = $this->parseToken(); //
            $store_pay_ways_id = (int)$request->get('store_pay_ways_id');
            $store_id = $request->get('store_id');
            $new_sort = $request->get('new_sort');
            $check_data = [
                'store_pay_ways_id' => '通道类型id',
                'new_sort' => '新位置',
            ];

            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }



            $user = User::where('id', $user->user_id)->first();
//            $hasPermission = $user->hasPermissionTo('收款顺序修改');
//
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '没有权限']);
//            }


            $ch_storePayWay = StorePayWay::where('id', $store_pay_ways_id)->first();

            $store_id = $ch_storePayWay->store_id;
            $ways_source = $ch_storePayWay->ways_source;
            $sort = $ch_storePayWay->sort;//旧的位置


            if ((int)$sort == (int)$new_sort) {
                return json_encode(['status' => 2, 'message' => '位置没有任何改动']);

            }

            $old_StorePayWay = StorePayWay::where('sort', $new_sort)
                ->where('store_id', $store_id)
                ->where('ways_source', $ways_source)
                ->first();

            //开启事务
            try {
                DB::beginTransaction();

                //先零时配置一个
                $old_StorePayWay->update([
                    'sort' => time(),
                ]);

                $ch_storePayWay->update([
                    'sort' => $new_sort,
                ]);
                $ch_storePayWay->save();
                $old_StorePayWay->save();


                //修正
                $old_StorePayWay->update([
                    'sort' => $sort,
                ]);
                $old_StorePayWay->save();


                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                return json_encode(['status' => 2, 'message' => $e->getMessage()]);
            }


            return json_encode(['status' => 1, 'message' => '顺序修改成功']);

        } catch (\Exception $exception) {

            return json_encode(['status' => 0, 'message' => $exception->getMessage()]);
        }
    }


    //收款方式置顶
    public function pay_ways_sort_top(Request $request)
    {

        try {
            $token_info = $this->parseToken();
            $store_ids = $request->get('store_ids', '[]'); //需要修改的门店数组
            $ways_type = $request->get('ways_type', ''); //需要修改的支付方式

            $user = User::where('id', $token_info->user_id)->first();
            //$user->hasPermissionTo('批量收款顺序置顶');  //判断是否有权限

//            if ($user->level != 0) {
//                $this->status = '2';
//                $this->message = '您不是平台账户，无权操作';
//                return $this->format();
//            }

            $check_data = [
                'store_ids' => '门店id集',
                'ways_type' => '收款方式',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                $this->status = '2';
                $this->message = $check;
                return $this->format();
            }

            if ($store_ids == '[]') {
                $this->status = '2';
                $this->message = '门店信息为空';
                return $this->format();
            }
            $store_id_arr = array_column(json_decode($store_ids, true), 'store_id');

            if ($store_id_arr && is_array($store_id_arr)) {
                foreach ($store_id_arr as $store_id) {
                    $store_pay_way_obj = StorePayWay::where('store_id', $store_id)
                        ->where('ways_type', $ways_type)
                        ->where('status', 1)
                        ->where('is_close', 0)
                        ->orderBy('updated_at', 'desc')
                        ->first();
                    if (!$store_pay_way_obj) {
                        continue;
                    }

                    $sort = $store_pay_way_obj->sort; //旧的位置
                    $ways_source = $store_pay_way_obj->ways_source; //旧的支付类型

                    //已经置顶的跳过
                    if ((int)$sort == 1) {
                        continue;
                    }

                    //同支付类型，只能有一个置顶
                    $min_ways_source = StorePayWay::where('store_id', $store_id)
                        ->where('ways_source', $ways_source)
                        ->where('sort', 1)
                        ->where('status', 1)
                        ->where('is_close', 0)
                        ->orderBy('updated_at', 'desc')
                        ->first();
                    if ($min_ways_source) {
                        //开启事务
                        try {
                            DB::beginTransaction();

                            //将原有置顶排序减一
                            $min_ways_source->update([
                                'sort' => 2,
                            ]);
                            $min_ways_source->save();

                            $store_pay_way_obj->update([
                                'sort' => 1,
                            ]);
                            $store_pay_way_obj->save();

                            DB::commit();
                        } catch (\Exception $ex) {
                            DB::rollBack();
                            $this->status = '2';
                            $this->message = $ex->getMessage();
                            return $this->format();
                        }
                    } else {
                        $store_pay_way_obj->update([
                            'sort' => 1,
                        ]);
                        $store_pay_way_obj->save();
                    }
                }

            } else {
                $this->status = '2';
                $this->message = '门店信息不能为空';
                return $this->format();
            }

        } catch (\Exception $exception) {
            $this->status = '0';
            $this->message = $exception->getMessage();
            return $this->format();
        }

        $this->status = '1';
        $this->message = '收款方式置顶成功';
        return $this->format();
    }


    //查看是否需要审核商户
    public function user_store_set_status(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $user_id = $request->get('user_id', '');
            $status_check = $request->get('status_check', '');
            $admin_status_check = $request->get('admin_status_check', '');

            if ($user_id == "") {
                return json_encode(['status' => 2, 'message' => 'user_id必须传']);
            }

            $UserStoreSet = UserStoreSet::where('user_id', $user_id)->first();

            //查看
            if ($status_check == "") {
                if ($UserStoreSet) {
                    return json_encode(['status' => 1, 'data' => $UserStoreSet]);
                } else {
                    return json_encode(['status' => 1, 'data' => [
                        'user_id' => $user_id,
                        'status_check' => 0,
                        'admin_status_check' => 0,
                    ]
                    ]);
                }
            }

            $data = [
                'user_id' => $user_id,
                'status_check' => $status_check,
                'admin_status_check' => $admin_status_check,
            ];

            if ($UserStoreSet) {
                $UserStoreSet->update($data);
                $UserStoreSet->save();
            } else {
                UserStoreSet::create($data);
            }


            return json_encode(['status' => 1, 'message' => '修改成功']);


        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }


}
