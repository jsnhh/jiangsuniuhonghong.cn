<?php
namespace App\Api\Controllers\Agent;


use App\Api\Controllers\BaseController;
use App\Models\ActivityStoreRate;
use App\Models\Store;
use App\Models\StoreMonthOrder;
use App\Models\StorePayWay;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ActivityController extends BaseController
{

    //活动列表查询
    public function activity_store_rate_list(Request $request)
    {

        try {
            $user = $this->parseToken();//
            $user_id = $user->user_id;
            $store_id = $request->get('store_id', '');


            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("activity_store_rate");
            } else {
                $obj = DB::table('activity_store_rate');
            }

            $where = [];

            $user_ids = $this->getSubIds($user_id);

            if ($store_id) {
                $where[] = ['store_id', $store_id];
            }

            $obj = $obj->where($where)
                ->whereIn('user_id', $user_ids)
                ->orderBy('created_at', 'desc');
            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);


        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }


    public function add_activity_store_rate(Request $request)
    {

        try {
            $token = $this->parseToken(); //
            $user_id = $token->user_id;
            $config_id = $token->config_id;
            $store_id = $request->get('store_id', '');
            $company = $request->get('company', '');
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');
            $total_amount_e = $request->get('total_amount_e', '');
            $total_amount_s = $request->get('total_amount_s', '');
            $weixin = $request->get('weixin', '');
            $alipay = $request->get('alipay', '');

            $check_data = [
                'time_start' => '订单开始时间',
                'time_end' => '订单结束时间',
                'total_amount_e' => '金额结束',
                'total_amount_s' => '金额结束',
                'alipay' => '支付宝返还比例',
                'weixin' => '微信返还比例',
                'company' => '通道类型'
            ];
            $check = $this->check_required($request->all(), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            //传化限制
            if ($company == "tfpay") {
                if ($total_amount_s != '2' || $total_amount_e != '300') {
                    return json_encode([
                        'status' => 2,
                        'message' => '金额必须2-300'
                    ]);
                }
            }

            //权限报名
            $user = User::where('id', $user_id)->first();
            //$hasPermission = $user->hasPermissionTo('add_activity_'.$company);
//            if (!$hasPermission) {
//                return json_encode([
//                    'status' => 2,
//                    'message' => '没有权限-add_activity_' . $company
//                ]);
//            }

            $Store = Store::where('store_id', $store_id)
                ->select('store_name')
                ->first();
            if (!$Store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在'
                ]);
            }

            $StorePayWay = StorePayWay::where('company', $company)
                ->where('store_id', $store_id)
                ->where('status', 1)
                ->first();
            if (!$StorePayWay) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店通道类型不存在'
                ]);
            }

            $store_name = $Store->store_name;

            $ActivityStoreRate = ActivityStoreRate::where('company', $company)
                ->where('store_id', $store_id)
                ->select('id')
                ->first();
            if ($ActivityStoreRate) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店通道类型活动已经存在不需要重复报名'
                ]);
            }

            $insert_data = [
                'store_id' => $store_id,
                'store_name' => $store_name,
                'config_id' => $config_id,
                'user_id' => $user_id,
                'company' => $company,
                'time_start' => $time_start,
                'time_end' => $time_end,
                'total_amount_e' => $total_amount_e,
                'total_amount_s' => $total_amount_s,
                'alipay' => $alipay,
                'weixin' => $weixin
            ];
            $res = ActivityStoreRate::create($insert_data);
            if ($res) {
                return json_encode([
                    'status' => 1,
                    'message' => '门店活动报名添加成功'
                ]);
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '门店活动报名添加失败'
                ]);
            }
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage().' | '.$exception->getLine()
            ]);
        }

    }


    public function del_activity_store_rate(Request $request)
    {

        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');
            $id = $request->get('id', '');
            $company = $request->get('company', '');

            $check_data = [
                'company' => '通道类型',
                'id' => 'id',
                'store_id' => 'store_id',
            ];
            $check = $this->check_required($request->all(), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }


            $ActivityStoreRate = ActivityStoreRate::where('company', $company)
                ->where('store_id', $store_id)
                ->where('id', $id)->delete();

            return json_encode([
                'status' => 1,
                'message' => '门店活动报名删除成功'
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }


}