<?php

namespace App\Api\Controllers\Wallet;


use App\Api\Controllers\BaseController;
use App\Common\UserGetMoney;
use App\Models\ActivityStoreRate;
use App\Models\EsignAuths;
use App\Models\QxgjUser;
use App\Models\SettlementDay;
use App\Models\Merchant;
use App\Models\MerchantAccount;
use App\Models\MerchantStore;
use App\Models\MerchantWalletDayCount;
use App\Models\MerchantWalletDetail;
use App\Models\MerchantWithdrawalsRecords;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\SettlementConfig;
use App\Models\SettlementList;
use App\Models\SettlementListInfo;
use App\Models\Store;
use App\Models\User;
use App\Models\UserAccount;
use App\Models\UserAuths;
use App\Models\UserWalletDetail;
use App\Models\UserWithdrawalsRecords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class SelectController extends BaseController
{

    //返佣类型
    public function source_type(Request $request)
    {
        try {
            $public = $this->parseToken();

            if ($public->type == "merchant") {
                $user_id = $public->merchant_id;
                $user = Merchant::where('id', $user_id)->first();
            }

            if ($public->type == "user") {
                $user_id = $public->user_id;
                $user = User::where('id', $user_id)->first();
            }

            $source = $user->source;


            $where = [];
            if ($source) {
                $where[] = ['remark', '=', $source];
            }
            $where[] = ['name', '=', 'source_type'];

            $dataObj = DB::table('data_dictionaries')->where($where)->select('name', 'code', 'code_value', 'remark')->get();
            $data = [];
            if ($dataObj) {
                foreach ($dataObj as $k => $v) {
                    array_push($data, [
                        'source_type' => $v->code,
                        'source_desc' => $v->code_value,
                    ]);
                }
                $this->status = 1;
                $this->message = '数据返回成功';
            } else {
                $this->status = -1;
                $this->message = '查询数据失败';
            }

            $this->status = 1;
            $this->message = '数据返回成功';

            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //返佣列表
    public function source_query(Request $request)
    {
        try {
            $public = $this->parseToken();
            $source_type = $request->get('source_type', '');
            $settlement = $request->get('settlement', '');
            $time_start = $request->get('time_start', date("Y-m-d 00:00:00", time()));
            $time_end = $request->get('time_end', date("Y-m-d H:i:s", time()));
            $return_type = $request->get('return_type', '');
            $user_id = $request->get('user_id', '');
            $out_trade_no = $request->get('out_trade_no', '');
            $store_id = $request->get('store_id', '');
            $data = [];
            $where = [];
            //限制时间
            $date = (strtotime($time_end) - strtotime($time_start)) / 86400;
            $day = 31;
            if ($date > $day) {
                return json_encode([
                    'status' => 2,
                    'message' => '时间筛选不能超过' . $day . '天'
                ]);
            }

            if ($source_type) {
                $where[] = ['source_type', '=', $source_type];
            }

            if ($settlement) {
                $where[] = ['settlement', '=', $settlement];
            }

            if ($public->type == "merchant") {
                $user_id = $public->merchant_id;
                $where[] = ['merchant_id', '=', $user_id];

                if ($time_start) {
                    $where[] = ['created_at', '>=', $time_start];
                }

                if ($time_end) {
                    $where[] = ['created_at', '<=', $time_end];
                }

                if ($store_id) {
                    $where[] = ['store_id', '=', $store_id];
                }

                $merchant = Merchant::where('id', $user_id)
                    ->select('money', 'settlement_money', 'unsettlement_money')
                    ->first();
                $money = $merchant->money;

                $table = 'merchant_wallet_details' . '_' . date('Ymd', strtotime($time_start));
                if (env('DB_D1_HOST')) {
                    if (Schema::hasTable($table)) {
                        $merchant_wallet_details = DB::connection("mysql_d1")->table($table);
                    } else {
                        $merchant_wallet_details = DB::connection("mysql_d1")->table("merchant_wallet_details");
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $merchant_wallet_details = DB::table($table);
                    } else {
                        $merchant_wallet_details = DB::table('merchant_wallet_details');
                    }
                }

                if (env('DB_D1_HOST')) {
                    if (Schema::hasTable($table)) {
                        $merchant_wallet_details1 = DB::connection("mysql_d1")->table($table);
                    } else {
                        $merchant_wallet_details1 = DB::connection("mysql_d1")->table("merchant_wallet_details");
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $merchant_wallet_details1 = DB::table($table);
                    } else {
                        $merchant_wallet_details1 = DB::table('merchant_wallet_details');
                    }
                }

                if (env('DB_D1_HOST')) {
                    if (Schema::hasTable($table)) {
                        $merchant_wallet_details2 = DB::connection("mysql_d1")->table($table);
                    } else {
                        $merchant_wallet_details2 = DB::connection("mysql_d1")->table("merchant_wallet_details");
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $merchant_wallet_details2 = DB::table($table);
                    } else {
                        $merchant_wallet_details2 = DB::table('merchant_wallet_details');
                    }
                }

                //返佣金额
                $settlement_money_where = $where;
                $settlement_money_where[] = ['settlement', '=', '01'];
                $unsettlement_money_where = $where;
                $unsettlement_money_where[] = ['settlement', '=', '02'];
                $settlement_money = $merchant_wallet_details->where($settlement_money_where)->select('money')->sum('money');
                $unsettlement_money = $merchant_wallet_details1->where($unsettlement_money_where)->select('money')->sum('money');

                $detail = $merchant_wallet_details2->where($where);
            }

            if ($public->type == "user") {
                //不允许跨天操作
                $time_start_db = date('Ymd', strtotime($time_start));
                $time_end_db = date('Ymd', strtotime($time_end));
                if (0 && $time_start_db != $time_end_db) {
                    return json_encode([
                        'status' => 2,
                        'message' => '暂不支持跨天查询'
                    ]);
                }

                if ($out_trade_no) {
                    $where[] = ['out_trade_no', '=', $out_trade_no];
                } else {
                    if ($time_start) {
                        $where[] = ['created_at', '>=', $time_start];
                    }

                    if ($time_end) {
                        $where[] = ['created_at', '<=', $time_end];
                    }

                    if ($store_id) {
                        $where[] = ['store_id', '=', $store_id];
                    }
                }

                if ($user_id) {
                    $users = $this->getSubIdsAll($user_id);
                } else {
                    $user_id = $public->user_id;
                    $users = $this->getSubIdsAll($public->user_id);
                }

                if (!in_array($user_id, $users)) {
                    return json_encode(['status' => 2, 'message' => '非上下级关系']);
                }

                $User = User::where('id', $user_id)
                    ->select('money', 'settlement_money', 'unsettlement_money')
                    ->first();
                $money = $User->money;

                $table = 'user_wallet_details' . '_' . date('Ymd', strtotime($time_start));
                if (env('DB_D1_HOST')) {
                    if (Schema::hasTable($table)) {
                        $UserWalletDetail = DB::connection("mysql_d1")->table($table);
                    } else {
                        $UserWalletDetail = DB::connection("mysql_d1")->table("user_wallet_details");
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $UserWalletDetail = DB::table($table);
                    } else {
                        $UserWalletDetail = DB::table('user_wallet_details');
                    }
                }

                //获取下级所有返佣
                if ($request->get('user_id')) {
                    $detail = $UserWalletDetail->where($where)->where('user_id', $request->get('user_id'));
                } else {
                    $user_id = $public->user_id;
                    $detail = $UserWalletDetail->where($where)->where('user_id', $this->getSubIdsAll($public->user_id));
                }

                if (env('DB_D1_HOST')) {
                    if (Schema::hasTable($table)) {
                        $UserWalletDetail2 = DB::connection("mysql_d1")->table($table);
                    } else {
                        $UserWalletDetail2 = DB::connection("mysql_d1")->table("user_wallet_details");
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $UserWalletDetail2 = DB::table($table);
                    } else {
                        $UserWalletDetail2 = DB::table('user_wallet_details');
                    }
                }

                if (env('DB_D1_HOST')) {
                    if (Schema::hasTable($table)) {
                        $UserWalletDetail3 = DB::connection("mysql_d1")->table($table);
                    } else {
                        $UserWalletDetail3 = DB::connection("mysql_d1")->table("user_wallet_details");
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $UserWalletDetail3 = DB::table($table);
                    } else {
                        $UserWalletDetail3 = DB::table('user_wallet_details');
                    }
                }

                $where[] = ['user_id', '=', $user_id];
                $settlement_money_where = $where;
                $settlement_money_where[] = ['settlement', '=', '01'];
                $unsettlement_money_where = $where;
                $unsettlement_money_where[] = ['settlement', '=', '02'];
                $settlement_money = $UserWalletDetail2->where($settlement_money_where)->select('money')->sum('money');
                $unsettlement_money = $UserWalletDetail3->where($unsettlement_money_where)->select('money')->sum('money');
            }

            $this->t = $detail->count();
            $detail = $this->page($detail)->orderBy('updated_at', 'desc')->get();
            $this->status = 1;
            $this->message = '数据返回成功';

            if ($return_type == '02') {
                $data = $detail;
            } elseif ($return_type == '03') {
                $data = $detail;
                //floor 保留两位小数并且不四舍五入
                $this->order_data = [
                    'all_money' => $settlement_money + $unsettlement_money,
                    'settlement_money' => '' . floor($settlement_money * 100) / 100 . '',
                    'unsettlement_money' => '' . floor($unsettlement_money * 100) / 100 . '',
                ];
            } else {
                $data = [
                    // 'money' => '' . floor($money*100)/100 . '',
                    'money' => number_format($money, 2, '.', ''),
                    'settlement_money' => '' . floor($settlement_money * 100) / 100 . '',
                    'unsettlement_money' => '' . floor($unsettlement_money * 100) / 100 . '',
                    'detail' => $detail,
                ];
            }

            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //单个返佣信息查询
    public function source_query_info(Request $request)
    {
        try {
            $public = $this->parseToken();
            $source_query_id = $request->get('source_query_id');
            if ($public->type == "user") {

                if (env('DB_D1_HOST')) {
                    $UserWalletDetail = DB::connection("mysql_d1")->table("user_wallet_details");
                } else {
                    $UserWalletDetail = DB::table('user_wallet_details');
                }

                $data = $UserWalletDetail->where('id', $source_query_id)->first();

            } else {
                if (env('DB_D1_HOST')) {
                    $merchant_wallet_details = DB::connection("mysql_d1")->table("merchant_wallet_details");
                } else {
                    $merchant_wallet_details = DB::table('merchant_wallet_details');
                }

                $data = $merchant_wallet_details->where('id', $source_query_id)->first();

            }
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {

            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


    //查询支付宝账户
    public function account(Request $request)
    {
        try {
            $public = $this->parseToken();
            if ($public->type == "merchant") {
                $user_id = $public->merchant_id;
                $account = MerchantAccount::where('merchant_id', $user_id)->get();
            }

            if ($public->type == "user") {
                $user_id = $public->user_id;
                $account = UserAccount::where('user_id', $user_id)->get();
            }

            if ($account) {
//                $alipay_account = $account->alipay_account;
//                $alipay_name = $account->alipay_name;
//                $data = [
//                    'alipay_account' => $alipay_account,
//                    'alipay_name' => $alipay_name,
//                    'type' => $account->type,
//                ];
                $this->status = 1;
                $this->message = '数据返回成功';
                return $this->format($account);

            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '未绑定支付宝账户'
                ]);
            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }

    }

    //查询支付宝账户
    public function select_account(Request $request)
    {
        try {
            $public = $this->parseToken();
            $account_id = $request->get('accountId', '');

            $where = [];
            if ($account_id) {
                $where[] = ['id', '=', $account_id];
            }

            if ($public->type == "merchant") {
                $user_id = $public->merchant_id;
                $where[] = ['merchant_id', '=', $user_id];
//                $account = MerchantAccount::where('merchant_id', $user_id)->where('id', $account_id)->first();
                $account = DB::table('merchant_accounts')->where($where)->first();
            }

            if ($public->type == "user") {
                $user_id = $public->user_id;
                $where[] = ['user_id', '=', $user_id];
//                $account = UserAccount::where('user_id', $user_id)->where('id', $account_id)->first();
                $account = DB::table('user_accounts')->where($where)->first();
            }
            if ($account) {
                $alipay_account = $account->alipay_account;
                $alipay_name = $account->alipay_name;
                $data = [
                    'id' => $account->id,
                    'alipay_account' => $alipay_account,
                    'alipay_name' => $alipay_name,
                    'type' => $account->type,
                ];
                $this->status = 1;
                $this->message = '数据返回成功';
                return $this->format($data);

            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '未绑定支付宝账户'
                ]);
            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }

    }


    //添加修改支付宝账户
    public function add_account(Request $request)
    {
        try {
            $public = $this->parseToken();
            $alipay_account = $request->get('alipay_account', '');
            $alipay_name = $request->get('alipay_name', '');
            $type = $request->get('type', '');
            $code = $request->get('code', '');
            Log::info($type);

            //校验短信验证码
            if ($code != "" && $alipay_account == "" && $alipay_name == "") {
                //验证手机验证码
                $msn_local = Cache::get($public->phone . 'account');
                if ((string)$code != (string)$msn_local) {
                    return json_encode([
                        'status' => 2,
                        'message' => '短信验证码不匹配'
                    ]);
                } else {
                    return json_encode([
                        'status' => 1,
                        'message' => '短信验证码匹配成功',
                    ]);
                }
            }


            if ($public->type == "merchant") {
                $user_id = $public->merchant_id;
                $account = MerchantAccount::where('merchant_id', $user_id)->where('type', $type)->first();
            }

            if ($public->type == "user") {
                $user_id = $public->user_id;
                $account = UserAccount::where('user_id', $user_id)->where('type', $type)->first();

            }
            //修改
            if ($account) {
                $check_data = [
                    'alipay_account' => '支付宝账户',
                    'alipay_name' => '支付宝名称',
                    'type' => '账户类型',
//                    'code' => '短信验证码',
                ];
                $check = $this->check_required($request->except(['token']), $check_data);
                if ($check) {
                    return json_encode([
                        'status' => 2,
                        'message' => $check
                    ]);
                }

                $msn_local = Cache::get($public->phone . 'account');
                if ((string)$code != (string)$msn_local) {
                    return json_encode([
                        'status' => 2,
                        'message' => '短信验证码不匹配'
                    ]);
                }


                $account->alipay_account = $alipay_account;
                $account->alipay_name = $alipay_name;
                $account->type = $type;
                $account->save();


            } //添加
            else {

                $check_data = [
                    'alipay_account' => '支付宝账户',
                    'alipay_name' => '支付宝名称',
                    'type' => '账户类型',
                ];

                $check = $this->check_required($request->except(['token']), $check_data);
                if ($check) {
                    return json_encode([
                        'status' => 2,
                        'message' => $check
                    ]);
                }


                if ($public->type == "merchant") {
                    $user_id = $public->merchant_id;
                    MerchantAccount::create([
                        'merchant_id' => $user_id,
                        'alipay_account' => $alipay_account,
                        'alipay_name' => $alipay_name,
                        'type' => $type,
                    ]);
                }

                if ($public->type == "user") {
                    $user_id = $public->user_id;
                    UserAccount::create([
                        'user_id' => $user_id,
                        'alipay_account' => $alipay_account,
                        'alipay_name' => $alipay_name,
                        'type' => $type,
                    ]);

                }

            }

            return json_encode([
                'status' => 1,
                'message' => $type,
                'data' => $request->except(['token'])
            ]);


        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }

    }


    //提交提现操作
    public function out_wallet(Request $request)
    {

        try {

            $public = $this->parseToken();
            $config_id = $public->config_id;
            $phone = $public->phone;
            $total_amount = $request->get('total_amount');
            $alipay_account = $request->get('alipay_account');
            $account_type = $request->get('account_type');
            $reward_money = $request->get('reward_money');
            $money = $request->get('money');

            if ($public->type == "merchant") {
                $user_id = $public->merchant_id;
                $money = Merchant::where('id', $user_id)->first()->money;//钱包的钱
                $accounts = MerchantAccount::where('merchant_id', $user_id)->where('type', $account_type)->first();
                $SettlementConfig = SettlementConfig::where('dx', '2')->first();
            }
            $money_balance = '';
            $reward_money_balance = '';
            if ($public->type == "user") {
                $user_id = $public->user_id;
                $money_balance = User::where('id', $user_id)->first()->money;//分润余额
                $reward_money_balance = User::where('id', $user_id)->first()->reward_money;//奖励金额
                $accounts = UserAccount::where('user_id', $user_id)->where('type', $account_type)->first();
                $SettlementConfig = SettlementConfig::where('dx', '1')->first();
                if ($money_balance < $money) {
                    return json_encode(['status' => 2, 'message' => '你的分润余额里只有' . $money_balance . '元没有办法提现' . $money]);
                }
                if ($reward_money_balance < $reward_money) {
                    return json_encode(['status' => 2, 'message' => '你的奖励余额里只有' . $money_balance . '元没有办法提现' . $reward_money]);
                }

            }

            $userAuths = UserAuths::where('user_id', $user_id)->where('user_type', '1')->first();
            if (!$userAuths) {
                return json_encode([
                    'status' => 2,
                    'message' => '未实名认证，请先进行实名认证'
                ]);
            }

            //检查是否签署协议
//            $esignAuth = EsignAuths::where('user_id', $user_id)->first();
//            if (!$esignAuth) {
//                return json_encode([
//                    'status' => -1,
//                    'message' => '请先进行协议签署'
//                ]);
//            } else {
//                if ($esignAuth->sign_flow_status != '2') {
//                    return json_encode([
//                        'status' => -1,
//                        'message' => '请先完成协议签署'
//                    ]);
//                }
//            }

            if (!$accounts) {
                return json_encode(['status' => 2, 'message' => '提现账户未设置']);
            }

            if (!$SettlementConfig) {
                return json_encode(['status' => 2, 'message' => '服务商提现未设置']);
            }
            if ($SettlementConfig->out_status == "2") {
                return json_encode(['status' => 2, 'message' => '提现失败，请联系管理员']);
            }
            $out_time = date('Y-m-d H:i:m', time());
            $start_time = date('Y-m-d ' . $SettlementConfig->start_time, time());
            $end_time = date('Y-m-d ' . $SettlementConfig->end_time, time());
            if(strtotime($out_time)<strtotime($start_time)||strtotime($out_time)>strtotime($end_time)){
                return json_encode(['status' => 2, 'message' => '提现失败，请在此时间段内提现'.$SettlementConfig->start_time.'至'.$SettlementConfig->end_time]);
            }
            $sxf_amount = $SettlementConfig->sxf_amount;
            $tx_remark = $SettlementConfig->tx_remark;
            $rate = $SettlementConfig->rate;

            if ($total_amount < $sxf_amount) {
                return json_encode(['status' => 2, 'message' => '提现金额小于手续费' . $sxf_amount]);
            }

            if ($total_amount < $SettlementConfig->s_amount) {
                return json_encode(['status' => 2, 'message' => '提现金额不能小于' . $SettlementConfig->s_amount . '元']);
            }

            if ($total_amount > $SettlementConfig->e_amount) {
                return json_encode(['status' => 2, 'message' => '提现金额不能大于' . $SettlementConfig->e_amount . '元']);
            }

            $is_number = is_numeric($total_amount);
            if (!$is_number) {
                return json_encode(['status' => 2, 'message' => '提现金额类型不正确']);
            }

            $is_withdraw = User::where('id', $user_id)->first()->is_withdraw;//是否可以提现
            if ($is_withdraw == 0) {
                return json_encode(['status' => 2, 'message' => '您的帐户暂时被禁止提现']);
            }

            $total_amount = number_format($total_amount, 2, '.', '');
            $time = date('Y-m-d h:i:s', time());
            $out_trade_no = date('Ymdhis', time());
            if ($accounts && $accounts->alipay_account) {
                //提交过来的支付宝必须是绑定的支付宝
                if ($alipay_account != $accounts->alipay_account) {
                    return json_encode(['status' => 2, 'message' => '提现账户和绑定的账户不一致']);
                }
                //开启事务
                try {
                    DB::beginTransaction();
                    if ($public->type == "merchant") {
                        $user_id = $public->merchant_id;
                        $updateMonery = $money - $total_amount;
                        $data = [
                            'merchant_id' => $user_id,
                            'out_trade_no' => $out_trade_no,
                            'trade_no' => '',
                            'account' => $accounts->alipay_account,
                            'name' => $accounts->alipay_name,
                            'amount' => $total_amount,
                            'account_amount' => $updateMonery,
                            'status' => 2,
                            'status_desc' => '提现申请中',
                            'config_id' => $config_id,
                            'sxf_amount' => $sxf_amount,
                            'remark' => ''
                        ];
                        MerchantWithdrawalsRecords::create($data);
                        Merchant::where('id', $user_id)->update(['money' => $updateMonery]);

                    }

                    if ($public->type == "user") {
                        $user_id = $public->user_id;
                        $updateMoney = $money + $reward_money - $total_amount;
                        $data = [
                            'user_id' => $user_id,
                            'out_trade_no' => $out_trade_no,
                            'trade_no' => $out_trade_no,
                            'account' => $accounts->alipay_account,
                            'name' => $accounts->alipay_name,
                            'amount' => $total_amount,
                            'account_amount' => $updateMoney,
                            'status' => 2,
                            'status_desc' => '提现申请中',
                            'config_id' => $config_id,
                            'sxf_amount' => $sxf_amount,
                            'remark' => '',
                            'reward_amount' => $reward_money,
                            'commission_amount' => $money,
                        ];
                        $updateUser = [];
                        if ($money) {
                            $updateMoney = $money_balance - $money;
                            $updateUser['money'] = $updateMoney;
                        }
                        if ($reward_money) {
                            $updateRewardMoney = $reward_money_balance - $reward_money;
                            $updateUser['reward_money'] = $updateRewardMoney;
                        }
                        UserWithdrawalsRecords::create($data);
                        User::where('id', $user_id)->update($updateUser);

                    }
                    DB::commit();
                } catch (\Exception $e) {
                    Log::info($e);
                    DB::rollBack();
                    return json_encode(['status' => 2, 'message' => $e->getMessage()]);
                }

                $AlipayTransfer = new \App\Common\AlipayTransfer($out_trade_no, $public->type, $total_amount, $accounts->alipay_account, $accounts->alipay_name, $config_id, $sxf_amount, $tx_remark, $rate, $money, $reward_money);
                $AlipayTransfer->insert();

//                $account = QxgjUser::where('user_id', $user_id)->first();
//
//                if ($account) {
//                    $QxgjController = new QxgjController();
//                    $resMsg = $QxgjController->revenueOrder($public->type, $account->phone, $account_type, $accounts->alipay_account, $out_trade_no, $total_amount, $sxf_amount);
//                } else {
//                    $resMsg = '';
//                }

            } else {
                return json_encode(['status' => 2, 'message' => '没有绑定收银员账号']);
            }

//            return json_encode(['status' => 1, 'message' => '提现申请成功-' . $resMsg, 'data' => $request->except(['token'])]);
            return json_encode(['status' => 1, 'message' => '提现申请成功', 'data' => $request->except(['token'])]);

        } catch (\Exception $exception) {
            Log::info($exception);
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }


    }

    //修改提现状态
    public function edit_wallet_status(Request $request)
    {
        try {
            $user_id = $request->get('user_id', '');
            $status = $request->get('status', '');
            $id = $request->get('id', '');
            $status_desc = '';
            if ($status == '1') {
                $status_desc = '提现成功';
            }
            if ($status == '3') {
                $status_desc = '提现失败';
                $UserWithdrawalsRecords = UserWithdrawalsRecords::where('id', $id)->first();
                $user = User::where('id', $UserWithdrawalsRecords->user_id)->first();
                $user->money = $user->money + $UserWithdrawalsRecords->amount;
                $user->save();
            }

            UserWithdrawalsRecords::where('id', $id)->where('user_id', $user_id)->update(['status' => $status, 'status_desc' => $status_desc]);
            return json_encode(['status' => 1, 'message' => '提现状态更新成功']);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

    //提现记录
    public function out_wallet_list(Request $request)
    {

        try {
            $public = $this->parseToken();
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');
            $user_id = $request->get('user_id', '');
            $status = $request->get('status', '');
            $where = [];
            if ($public->type == "merchant") {
                $user_id = $public->merchant_id;
                if ($status) {
                    $where[] = ['merchant_withdrawals_records.status', '=', $status];
                }
                if ($time_start) {
                    $where[] = ['merchant_withdrawals_records.updated_at', '>=', $time_start];
                }
                if ($time_end) {
                    $where[] = ['merchant_withdrawals_records.updated_at', '<=', $time_end];
                }
                $where[] = ['merchant_id', '=', $user_id];

                if (env('DB_D1_HOST')) {
                    $MerchantWithdrawalsRecords = DB::connection("mysql_d1")->table("merchant_withdrawals_records");
                } else {
                    $MerchantWithdrawalsRecords = DB::table('merchant_withdrawals_records');
                }

                $data = $MerchantWithdrawalsRecords->select([
                    'merchant_withdrawals_records.*',
                    DB::raw("users.name AS `agent_name`")
                ])->where($where)
                    ->leftjoin('merchants', 'merchants.id', '=', 'merchant_withdrawals_records.merchant_id')
                    ->leftjoin('users', 'users.id', '=', 'merchants.user_id');
            }

            if ($public->type == "user") {
                if ($status) {
                    $where[] = ['user_withdrawals_records.status', '=', $status];
                }
                if ($time_start) {
                    $where[] = ['user_withdrawals_records.updated_at', '>=', $time_start];
                }
                if ($time_end) {
                    $where[] = ['user_withdrawals_records.updated_at', '<=', $time_end];
                }
                if ($user_id) {
                    $users = $this->getSubIdsAll($user_id);
                } else {
                    $user_id = $public->user_id;
                    $users = $this->getSubIdsAll($public->user_id);

                }
                if (!in_array($user_id, $users)) {
                    return json_encode(['status' => 2, 'message' => '非上下级关系']);
                }

                if (env('DB_D1_HOST')) {
                    $UserWithdrawalsRecords = DB::connection("mysql_d1")->table("user_withdrawals_records");
                } else {
                    $UserWithdrawalsRecords = DB::table('user_withdrawals_records');
                }
                //获取下级所有返佣
                if ($request->get('user_id')) {
                    $where[] = ['user_withdrawals_records.user_id', '=', $request->get('user_id')];

                    $data = $UserWithdrawalsRecords->select([
                        'user_withdrawals_records.*',
                        DB::raw("users.name AS `agent_name`")
                    ])->where($where)
                        ->leftjoin('users', 'users.id', '=', 'user_withdrawals_records.user_id');
                } else {
                    $data = $UserWithdrawalsRecords->select([
                        'user_withdrawals_records.*',
                        DB::raw("users.name AS `agent_name`")
                    ])->where($where)
                        ->leftjoin('users', 'users.id', '=', 'user_withdrawals_records.user_id')
                        ->whereIn('user_withdrawals_records.user_id', $this->getSubIdsAll($public->user_id));
                }

            }
            $this->message = '数据返回成功';
            $this->t = $data->count();
            $data = $this->page($data)->orderBy('updated_at', 'desc')->get();
            return $this->format($data);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    //根据user_id 获取提现记录
    public function getUserOutWallerList(Request $request)
    {

        try {
            $public = $this->parseToken();
            $user_id = $request->get('user_id', '');
            $where = [];


            if (env('DB_D1_HOST')) {
                $UserWithdrawalsRecords = DB::connection("mysql_d1")->table("user_withdrawals_records");
            } else {
                $UserWithdrawalsRecords = DB::table('user_withdrawals_records');
            }
            //获取下级所有返佣

            $where[] = ['user_withdrawals_records.user_id', '=', $user_id];
            $where[] = ['user_withdrawals_records.status', '=', 1];

            $data = $UserWithdrawalsRecords->select([
                'user_withdrawals_records.*',
                DB::raw("users.name AS `agent_name`")
            ])->where($where)
                ->leftjoin('users', 'users.id', '=', 'user_withdrawals_records.user_id');


            $this->message = '数据返回成功';
            $this->t = $data->count();
            $data = $this->page($data)->orderBy('updated_at', 'desc')->get();
            return $this->format($data);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

//单个提现记录信息查询
    public
    function records_query_info(Request $request)
    {
        try {
            $public = $this->parseToken();
            $records_query_id = $request->get('records_query_id');
            if ($public->type == "user") {
                if (env('DB_D1_HOST')) {
                    $UserWithdrawalsRecords = DB::connection("mysql_d1")->table("user_withdrawals_records");
                } else {
                    $UserWithdrawalsRecords = DB::table('user_withdrawals_records');
                }
                $data = $UserWithdrawalsRecords->where('id', $records_query_id)->first();
            } else {
                if (env('DB_D1_HOST')) {
                    $MerchantWithdrawalsRecords = DB::connection("mysql_d1")->table("merchant_withdrawals_records");
                } else {
                    $MerchantWithdrawalsRecords = DB::table('merchant_withdrawals_records');
                }
                $data = $MerchantWithdrawalsRecords->where('id', $records_query_id)->first();
            }
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {

            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


//结算 目前只支持 代理结算
    public
    function settlement(Request $request)
    {
        try {
            $public = $this->parseToken();
            $user_id = $request->get('user_id', '');
            $user_name = $request->get('user_name', '');
            $store_name = $request->get('store_name', '');
            $store_id = $request->get('store_id', '');
            $time_start = $request->get('time_start');
            $time_end = $request->get('time_end');
            $pay_password = $request->get('pay_password');
            $source_type = $request->get('source_type');
            $source_type_desc = $request->get('source_type_desc');
            $dx = $request->get('dx', '');
            $rate = $request->get('rate', '');

            $config_id = $public->config_id;
            $s_no = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999)) . $config_id . $user_id . $source_type;
            $day = date('Ymd', strtotime($time_start));


            $check_data = [
                'time_start' => '开始时间',
                'time_end' => '结束时间',
                'pay_password' => '支付密码',
                'source_type' => '返佣来源',
                'source_type_desc' => '返佣来源说明',
                'dx' => '对象',
                'rate' => '税点'

            ];


            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }


            if (env('DB_D1_HOST')) {
                $settlement_lists = DB::connection("mysql_d1")->table("settlement_lists");
            } else {
                $settlement_lists = DB::table('settlement_lists');
            }

            //如果有未结算的记录 不允许结算
            $SettlementList = $settlement_lists->where('is_true', 0)
                ->where('config_id', $config_id)
                ->select('id')
                ->first();

            if ($SettlementList) {
                return json_encode([
                    'status' => 2,
                    'message' => '请先处理未结算订单'
                ]);
            }


            $user = User::where('id', $public->user_id)->first();
            //$hasPermission = $user->hasPermissionTo('结算');
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '没有权限结算']);
//            }
//
//            if ($public->pid) {
//                return json_encode(['status' => 2, 'message' => '没有权限结算']);
//            }


            //服务商
            if ($dx == "1") {
                $dx_desc = "服务商";
                if ($user_name && $user_name != "NULL") {
                    $dx_desc = $user_name;
                }

                //不允许跨天操作
                $time_start_db = date('Ymd', strtotime($time_start));
                $time_end_db = date('Ymd', strtotime($time_end));
                if (0 && $time_start_db != $time_end_db) {
                    return json_encode([
                        'status' => 2,
                        'message' => '暂不支持跨天查询'
                    ]);
                }

                //查出时间段的返佣未结算用户列表
                $where = [];
                $where[] = ['settlement', '02'];
                $where[] = ['money', '!=', '0.0000'];

                if ($source_type) {
                    $where[] = ['source_type', $source_type];
                }
                if ($time_start) {
                    $where[] = ['created_at', '>=', $time_start];
                }
                if ($time_end) {
                    $where[] = ['created_at', '<=', $time_end];
                }


                $table = 'user_wallet_details' . '_' . $day;
                if (env('DB_D1_HOST')) {
                    if (Schema::hasTable($table)) {
                        $UserWalletDetail = DB::connection("mysql_d1")->table($table);
                    } else {
                        $UserWalletDetail = DB::connection("mysql_d1")->table("user_wallet_details");
                    }
                } else {
                    if (Schema::hasTable($table)) {
                        $UserWalletDetail = DB::table($table);
                    } else {
                        $UserWalletDetail = DB::table('user_wallet_details');
                    }
                }
                //选择某个服务商
                if ($user_id && $user_id != "NULL") {
                    $user_ids = $this->getSubIdsAll($user_id);
                    $users_obj = $UserWalletDetail->where($where)
                        ->whereIn('user_id', $user_ids)
                        ->select('user_id')
                        ->distinct('user_id')//去重
                        ->get();

                } else {
                    $users_obj = $UserWalletDetail->where($where)
                        ->select('user_id')
                        ->distinct('user_id')//去重
                        ->get();
                }


                if ($users_obj->isEmpty()) {
                    return json_encode([
                        'status' => 2,
                        'message' => '没有找到结算记录'
                    ]);
                }

                //开启事务
                try {
                    DB::beginTransaction();

                    $total_amount = 0;
                    $total_get_amount = 0;
                    //整个大的平台
                    $insert1 = [
                        'config_id' => $config_id,
                        's_no' => $s_no,
                        'dx' => $dx,
                        'dx_desc' => $dx_desc,
                        's_time' => $time_start,
                        'e_time' => $time_end,
                        'source_type' => $source_type,
                        'source_type_desc' => $source_type_desc,
                        'total_amount' => $total_amount,
                        'get_amount' => $total_get_amount,
                        'rate' => $rate,
                        'is_true' => 0,
                        'dx_id' => isset($user_id) ? $user_id : "",
                    ];
                    $SettlementList = SettlementList::create($insert1);
                    $settlement_list_id = $SettlementList->id;

                    foreach ($users_obj as $k => $v) {
                        $user_id = $v->user_id;

                        $table = 'user_wallet_details' . '_' . $day;
                        if (env('DB_D1_HOST')) {
                            if (Schema::hasTable($table)) {
                                $UserWalletDetail1 = DB::connection("mysql_d1")->table($table);
                            } else {
                                $UserWalletDetail1 = DB::connection("mysql_d1")->table("user_wallet_details");
                            }
                        } else {
                            if (Schema::hasTable($table)) {
                                $UserWalletDetail1 = DB::table($table);
                            } else {
                                $UserWalletDetail1 = DB::table('user_wallet_details');
                            }
                        }

                        $UserWalletDetail1 = $UserWalletDetail1->where('source_type', $source_type)
                            ->where('created_at', '>', $time_start)
                            ->where('created_at', '<', $time_end)
                            ->where('settlement', '02')
                            ->where('user_id', $v->user_id)
                            ->select('money');


                        $money = $UserWalletDetail1->sum('money');
                        $money = floor($money * 100) / 100; //保留两位小数并且不四舍五入
                        //总金额
                        $total_amount = $total_amount + $money;
                        $j = number_format($rate / 100, 4, '.', '');
                        $get_amount = $money - (($money * $j));
                        $get_amount = floor($get_amount * 100) / 100;
                        $total_get_amount = $total_get_amount + $get_amount;


                        //个人金额
                        $insert = [
                            's_no' => $s_no,
                            'config_id' => $config_id,
                            'dx' => $dx,
                            's_time' => $time_start,
                            'e_time' => $time_end,
                            'source_type' => $source_type,
                            'source_type_desc' => $source_type_desc,
                            'total_amount' => $money,
                            'get_amount' => $get_amount,
                            'user_id' => $user_id,
                            'is_true' => 0,
                            'rate' => $rate,
                            'settlement_list_id' => $settlement_list_id
                        ];
                        SettlementListInfo::create($insert);
                    }

                    SettlementList::where('id', $settlement_list_id)->update([
                        'get_amount' => $total_get_amount,
                        'total_amount' => $total_amount,
                        's_time' => $time_start,
                        'e_time' => $time_end,
                    ]);


                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollBack();
                    return json_encode(['status' => 2, 'message' => $e->getMessage()]);
                }


            } else {

                $store = Store::where('store_id', $store_id)
                    ->select('merchant_id')
                    ->first();

                if (!$store) {
                    return json_encode([
                        'status' => 2,
                        'message' => '请选择门店'
                    ]);
                }
                $user_id = $store->merchant_id;
                $dx_desc = "全部活动商户";
                if ($store_name && $store_name != "NULL") {
                    $dx_desc = $store_name;
                }
                //查出时间段的返佣未结算用户列表
                $where = [];
                $where[] = ['settlement', '02'];
                $where[] = ['money', '!=', '0.0000'];

                if ($source_type) {
                    $where[] = ['source_type', $source_type];
                }
                if ($store_id) {
                    $where[] = ['store_id', '=', $store_id];
                }
                $time_start_YMD = date('Y-m-d', strtotime($time_start));
                $time_end_YMD = date('Y-m-d', strtotime($time_end));


                if (env('DB_D1_HOST')) {
                    $merchant_wallet_day_counts = DB::connection("mysql_d1")->table("merchant_wallet_day_counts");
                } else {
                    $merchant_wallet_day_counts = DB::table('merchant_wallet_day_counts');
                }

                $MerchantWalletDayCount = $merchant_wallet_day_counts->whereBetween('day', [$time_start_YMD, $time_end_YMD])
                    ->where($where)
                    ->select('id')
                    ->first();


                if (!$MerchantWalletDayCount) {
                    return json_encode([
                        'status' => 2,
                        'message' => '没有找到结算记录'
                    ]);
                }

                //开启事务
                try {
                    DB::beginTransaction();

                    $total_amount = 0;
                    $total_get_amount = 0;
                    //整个大的平台
                    $insert1 = [
                        'config_id' => $config_id,
                        's_no' => $s_no,
                        'dx' => $dx,
                        'dx_id' => $user_id,
                        'dx_desc' => $dx_desc,
                        's_time' => $time_start,
                        'e_time' => $time_end,
                        'source_type' => $source_type,
                        'source_type_desc' => $source_type_desc,
                        'total_amount' => $total_amount,
                        'get_amount' => $total_get_amount,
                        'rate' => $rate,
                        'is_true' => 0,
                        'is_settlement' => 0,
                    ];
                    $SettlementList = SettlementList::create($insert1);
                    $settlement_list_id = $SettlementList->id;


                    $table = 'merchant_wallet_details' . '_' . date('Ymd', strtotime($time_start));
                    if (env('DB_D1_HOST')) {
                        if (Schema::hasTable($table)) {
                            $merchant_wallet_details = DB::connection("mysql_d1")->table($table);
                        } else {
                            $merchant_wallet_details = DB::connection("mysql_d1")->table("merchant_wallet_details");
                        }
                    } else {
                        if (Schema::hasTable($table)) {
                            $merchant_wallet_details = DB::table($table);
                        } else {
                            $merchant_wallet_details = DB::table('merchant_wallet_details');
                        }
                    }

                    $MerchantWalletDetail = $merchant_wallet_details->where('source_type', $source_type)
                        ->where('created_at', '>', $time_start)
                        ->where('created_at', '<', $time_end)
                        ->where('settlement', '02')
                        ->where('store_id', $store_id)
                        ->select('money');

                    $money = $MerchantWalletDetail->sum('money');
                    $money = floor($money * 100) / 100; //保留两位小数并且不四舍五入
                    //总金额
                    $total_amount = $total_amount + $money;
                    $j = number_format($rate / 100, 4, '.', '');
                    $get_amount = $money - (($money * $j));
                    $get_amount = floor($get_amount * 100) / 100;
                    $total_get_amount = $total_get_amount + $get_amount;


                    //每个门店的金额
                    $insert = [
                        's_no' => $s_no,
                        'config_id' => $config_id,
                        'dx' => $dx,
                        's_time' => $time_start,
                        'e_time' => $time_end,
                        'source_type' => $source_type,
                        'source_type_desc' => $source_type_desc,
                        'total_amount' => $money,
                        'get_amount' => $get_amount,
                        'user_id' => $user_id,//收银员ID
                        'is_true' => 0,
                        'rate' => $rate,
                        'settlement_list_id' => $settlement_list_id
                    ];
                    SettlementListInfo::create($insert);


                    SettlementList::where('id', $settlement_list_id)->update([
                        'get_amount' => $total_get_amount,
                        'total_amount' => $total_amount,
                    ]);


                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollBack();
                    return json_encode(['status' => 2, 'message' => $e->getMessage()]);
                }

            }

            return json_encode(['status' => 1, 'message' => '结算请求处理添加成功']);

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }

    }


//结算列表
    public
    function settlement_lists(Request $request)
    {
        try {
            $public = $this->parseToken();
            $config_id = $public->config_id;
            $dx = $request->get('dx', '');
            $source_type = $request->get('source_type', '');

            $time_start = $request->get('time_start', ''); //交易时间，年-月
            $time_end = $request->get('time_end', ''); //交易时间，年-月
            $amount_start = $request->get('amount_start', ''); //开始金额
            $amount_end = $request->get('amount_end', ''); //结束金额
            $user_id = $request->get('user_id', ''); //服务商id
            $where = [];
            if ($dx) {
                $where[] = ['dx', $dx];
            }
            if ($config_id) {
                $where[] = ['config_id', $config_id];
            }
            if ($source_type) {
                $where[] = ['source_type', $source_type];
            }
            if ($amount_start) {
                $where[] = ['get_amount', '>=', "$amount_start"];
            }
            if ($amount_end) {
                $where[] = ['get_amount', '<=', "$amount_end"];
            }
            if ($time_start) {
                $where[] = ['created_at', '>=', "$time_start"];
            }
            if ($time_end) {
                $where[] = ['created_at', '<=', "$time_end"];
            }

            if ($user_id) {
                $where[] = ['dx_id', '=', $user_id];
            }
            if (env('DB_D1_HOST')) {
                $settlement_lists = DB::connection("mysql_d1")->table("settlement_lists");
            } else {
                $settlement_lists = DB::table('settlement_lists');
            }
            $obj = $settlement_lists->where($where);
            $this->t = $obj->count();
            $data = $this->page($obj)->orderBy('updated_at', 'desc')->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }

//结算明细
    public
    function settlement_list_infos(Request $request)
    {
        try {
            $public = $this->parseToken();
            $dx = $request->get('dx', '');
            $settlement_list_id = $request->get('settlement_list_id', '');
            $user_name = $request->get('user_name', '');
            $user_id = $request->get('user_id', '');


            if (env('DB_D1_HOST')) {
                $settlement_lists = DB::connection("mysql_d1")->table("settlement_lists");
            } else {
                $settlement_lists = DB::table('settlement_lists');
            }

            $SettlementList = $settlement_lists->where('id', $settlement_list_id)
                ->select('dx')
                ->first();

            if (!$SettlementList) {
                return json_encode(['status' => 2, 'message' => '没有找到记录']);

            }

            $dx = $SettlementList->dx;//对象类型 1 代理商 2服务商

            $where = [];
            $config_id = $public->config_id;

            if ($settlement_list_id) {
                $where[] = ['settlement_list_infos.settlement_list_id', $settlement_list_id];
            }
            if ($user_id) {
                $where[] = ['settlement_list_infos.user_id', $user_id];
            }

            if ($settlement_list_id) {
                $where[] = ['settlement_list_infos.settlement_list_id', $settlement_list_id];
            }

            if ($dx) {
                $where[] = ['settlement_list_infos.dx', $dx];
            }
            if ($config_id) {
                $where[] = ['settlement_list_infos.config_id', $config_id];
            }


            if (env('DB_D1_HOST')) {
                $settlement_list_infos = DB::connection("mysql_d1")->table("settlement_list_infos");
            } else {
                $settlement_list_infos = DB::table('settlement_list_infos');
            }

            if ($user_name) {
                if (is_numeric($user_name)) {
                    if ($dx == 1) {
                        $obj = $settlement_list_infos->where($where)
                            ->join('users', 'settlement_list_infos.user_id', '=', 'users.id')
                            ->where('users.phone', $user_name);
                    } else {
                        $obj = $settlement_list_infos->where($where)
                            ->join('merchants', 'settlement_list_infos.user_id', '=', 'merchants.id')
                            ->where('merchants.phone', $user_name);
                    }

                } else {
                    if ($dx == 1) {
                        $obj = $settlement_list_infos->where($where)
                            ->join('users', 'settlement_list_infos.user_id', '=', 'users.id')
                            ->where('users.name', $user_name);
                    } else {
                        $obj = $settlement_list_infos->where($where)
                            ->join('merchants', 'settlement_list_infos.user_id', '=', 'merchants.id')
                            ->where('merchants.name', $user_name);
                    }

                }

            } else {
                if ($dx == 1) {
                    $obj = $settlement_list_infos->join('users', 'settlement_list_infos.user_id', '=', 'users.id')
                        ->where($where);
                } else {
                    $obj = $settlement_list_infos->join('merchants', 'settlement_list_infos.user_id', '=', 'merchants.id')
                        ->where($where);
                }


            }

            $this->t = $obj->count();

            if ($dx == 1) {
                $data = $this->page($obj)->select('settlement_list_infos.*', 'users.name as user_name', 'users.phone')
                    ->get();
            } else {
                $data = $this->page($obj)->select('settlement_list_infos.*', 'merchants.name as user_name', 'merchants.phone')
                    ->get();
            }


            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


//删除结算列表明细
    public
    function settlement_list_del(Request $request)
    {
        try {
            $public = $this->parseToken();
            $config_id = $public->config_id;
            $settlement_list_id = $request->get('settlement_list_id', '');
            $where = [];


            $s = SettlementList::where('id', $settlement_list_id)
                ->where('config_id', $config_id)
                ->first();
            if (!$s) {
                return json_encode([
                    'status' => 2,
                    'message' => '结算ID不存在'
                ]);
            }

            if ($s->is_true) {
                return json_encode([
                    'status' => 2,
                    'message' => '已经确认的结算无法删除'
                ]);
            }

            //开启事务
            try {
                DB::beginTransaction();

                //删除列表
                $s->delete();

                //删除明细
                SettlementListInfo::where('settlement_list_id', $settlement_list_id)
                    ->where('config_id', $config_id)
                    ->delete();

                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                return json_encode(['status' => 2, 'message' => $e->getMessage()]);
            }

            $this->status = 1;
            $this->message = '删除成功';
            return $this->format([]);

        } catch (\Exception $exception) {
            return json_encode(
                [
                    'status' => -1,
                    'message' => $exception->getMessage()
                ]
            );
        }
    }


//确认结算
    public
    function settlement_list_true(Request $request)
    {
        try {
            $public = $this->parseToken();
            $config_id = $public->config_id;
            $settlement_list_id = $request->get('settlement_list_id', '');


            $user = User::where('id', $public->user_id)->first();
            //$hasPermission = $user->hasPermissionTo('确认结算');
//            if (!$hasPermission) {
//                return json_encode(['status' => 2, 'message' => '没有权限确认']);
//            }

            if ($public->pid) {
                return json_encode(['status' => 2, 'message' => '没有权限确认']);
            }

            $where = [];
            $list_info_id = $request->get('list_info_id', '');

            if ($list_info_id) {

                if (env('DB_D1_HOST')) {
                    $settlement_list_infos = DB::connection("mysql_d1")->table("settlement_list_infos");
                } else {
                    $settlement_list_infos = DB::table('settlement_list_infos');
                }

                $SettlementListInfo = $settlement_list_infos->where('id', $list_info_id)
                    ->where('config_id', $config_id)
                    ->first();


                if ($SettlementListInfo) {
                    //$SettlementListInfo->is_true = 1;
                    $settlement_list_infos->where('id', $list_info_id)->update(['is_true' => 1]);
                }
                return json_encode([
                    'status' => 1,
                    'message' => '确认成功'
                ]);

            }


            if ($settlement_list_id) {
                $s = SettlementList::where('id', $settlement_list_id)
                    ->where('config_id', $config_id)
                    ->first();

                if (!$s) {
                    return json_encode([
                        'status' => 2,
                        'message' => 'settlement_list_id不存在'
                    ]);
                }


            } else {
                return json_encode([
                    'status' => 2,
                    'message' => 'settlement_list_id不存在'
                ]);
            }


            //开启事务
            try {
                DB::beginTransaction();

                //1.状态全部改成结算
                $s_time = $s->s_time;//开始时间
                $e_time = $s->e_time;//结束时间
                $source_type = $s->source_type;
                $s_no = $s->s_no;
                $dx = $s->dx;
                $dx_id = $s->dx_id;
                $day = date('Ymd', strtotime($s_time));

                //服务商
                if ($dx == 1) {
                    $UserWalletDetailwhere = [];
                    //如果针对代理商就是单独结算
                    if ($dx_id) {
                        $UserWalletDetailwhere[] = ['user_id', $dx_id];
                    }

                    $table = 'user_wallet_details' . '_' . $day;


                    if (Schema::hasTable($table)) {
                        $UserWalletDetail = DB::table($table);
                    } else {
                        $UserWalletDetail = DB::table('user_wallet_details');
                    }


                    $UserWalletDetail->where('source_type', $source_type)
                        ->where('created_at', '>=', $s_time)
                        ->where('created_at', '<=', $e_time)
                        ->where('settlement', '02')
                        ->where($UserWalletDetailwhere)
                        ->update([
                            'settlement' => '01',
                            'settlement_desc' => "已结算",
                        ]);


                    //2.将已经结算的金额入库
                    $SettlementListInfo = SettlementListInfo::where('settlement_list_id', $settlement_list_id)
                        ->where('s_no', $s_no)
                        ->select('get_amount', 'user_id')
                        ->get();

                    foreach ($SettlementListInfo as $k => $v) {
                        if ($v->get_amount == 0.0000) {
                            continue;
                        }

                        //金额相加
                        $user = User::where('id', $v->user_id)->first();

                        if (!$user) {
                            continue;
                        }
                        $user->money = $user->money + $v->get_amount;
                        $user->save();
                    }

                    $s->is_settlement = 1;
                    $s->is_true = 1;
                    $s->save();

                } else {
                    //商户
                    //针对单个门店
                    $MerchantWalletDetailwhere = [];
                    if ($dx_id) {
                        $MerchantWalletDetailwhere[] = ['merchant_id', $dx_id];
                    }


                    $table = 'merchant_wallet_details' . '_' . $day;

                    if (Schema::hasTable($table)) {
                        $merchant_wallet_details = DB::table($table);
                    } else {
                        $merchant_wallet_details = DB::table('merchant_wallet_details');
                    }

                    $merchant_wallet_details->where('source_type', $source_type)
                        ->where($MerchantWalletDetailwhere)
                        ->where('created_at', '>=', $s_time)
                        ->where('created_at', '<=', $e_time)
                        ->where('settlement', '02')
                        ->update([
                            'settlement' => '01',
                            'settlement_desc' => "已结算",
                        ]);

                    //2.将已经结算的金额入库
                    $SettlementListInfo = SettlementListInfo::where('settlement_list_id', $settlement_list_id)
                        ->where('s_no', $s_no)
                        ->select('get_amount', 'user_id')
                        ->get();

                    foreach ($SettlementListInfo as $k => $v) {

                        if ($v->get_amount == 0.0000) {
                            continue;
                        }

                        //金额相加
                        $Merchant = Merchant::where('id', $v->user_id)->first();

                        if (!$Merchant) {
                            continue;
                        }
                        $Merchant->money = $Merchant->money + $v->get_amount;
                        $Merchant->save();
                    }

                    $s->is_settlement = 1;
                    $s->is_true = 1;
                    $s->save();
                }

                DB::commit();
            } catch (\Exception $e) {
                Log::info($e);
                DB::rollBack();
                return json_encode(['status' => 2, 'message' => $e->getMessage()]);
            }

            $this->status = 1;
            $this->message = '结算成功';
            return $this->format([]);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1,
                'message' => $exception->getMessage()]);
        }
    }


//商户结算补录
    public
    function tf_settlemment(Request $request)
    {
        try {

            //传化
            $source_type = '12000';
            $source_type = $request->get('source_type', $source_type);
            $time = date("Y-m-d", time());
            $time = $request->get('time', $time);
            $settlement = $request->get('settlement', '02');

            $MerchantWalletDayCount = MerchantWalletDayCount::where('day', '<', $time)
                ->where('source_type', $source_type)
                ->where('settlement', $settlement)
                ->select('store_id', 'merchant_id', 'money', 'day', 'id')
                ->first();

            echo '通道类型:' . $source_type;
            echo '</br>';


            if ($MerchantWalletDayCount) {

                echo '门店ID:' . $MerchantWalletDayCount->store_id;
                echo '</br>';


                $merchant_id = $MerchantWalletDayCount->merchant_id;
                $money = $MerchantWalletDayCount->money;
                $day = $MerchantWalletDayCount->day;
                $s_time = date("Y-m-d 00:00:00", strtotime($day));
                $e_time = date("Y-m-d 23:59:59", strtotime($day));


                echo '结算时间段:' . $s_time . '-' . $e_time;
                echo '</br>';


                echo '结算金额:' . $money;
                echo '</br>';


                $id = $MerchantWalletDayCount->id;
                //开启事务
                try {
                    DB::beginTransaction();


                    //总表结算
                    MerchantWalletDayCount::where('id', $id)->update([
                        'settlement' => '01',
                    ]);


                    $table = 'merchant_wallet_details' . '_' . $day;

                    if (Schema::hasTable($table)) {
                        $merchant_wallet_details = DB::table($table);
                    } else {
                        $merchant_wallet_details = DB::table('merchant_wallet_details');
                    }


                    //明细结算
                    $merchant_wallet_details->where('source_type', $source_type)
                        ->where('merchant_id', $merchant_id)
                        ->where('created_at', '>=', $s_time)
                        ->where('created_at', '<=', $e_time)
                        ->where('settlement', '02')
                        ->update([
                            'settlement' => '01',
                            'settlement_desc' => "已结算",
                        ]);
                    //用户金额结算
                    $Merchant = Merchant::where('id', $merchant_id)
                        ->first();
                    if ($Merchant) {
                        echo '商户余额:' . $Merchant->money;
                        echo '</br>';
                        $all = $Merchant->money + $money;
                        echo '结算后商户余额:' . $all;
                        echo '</br>';

                        $Merchant->money = $Merchant->money + $money;
                        $Merchant->save();

                        echo '结算成功';
                        echo '</br>';
                    }


                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollBack();
                    dd($e);
                }
            } else {
                dd('暂无需要结算记录');
            }
        } catch (\Exception $exception) {
            dd($exception);
        }
    }


//结算商户未结算的信息
    public
    function tf_settlemment_m(Request $request)
    {
        try {

            //传化
            $source_type = '12000';
            $source_type = $request->get('source_type', $source_type);
            $day = $request->get('day', '');
            $settlement = $request->get('settlement', '02');


            echo '通道类型:' . $source_type;
            echo '</br>';

            //开启事务
            try {
                DB::beginTransaction();


                $table = 'merchant_wallet_details' . '_' . $day;
                $merchant_wallet_details = DB::table($table);


                $merchant_obj_store = $merchant_wallet_details->where('source_type', $source_type)
                    ->where('settlement', $settlement)
                    ->select('store_id', 'merchant_id', 'money')
                    ->first();
                if (!$merchant_obj_store) {
                    dd('全部结算结束');
                }

                $table = 'merchant_wallet_details' . '_' . $day;
                $merchant_wallet_details = DB::table($table);

                //明细结算
                $money = $merchant_wallet_details->where('source_type', $source_type)
                    ->where('settlement', $settlement)
                    ->where('store_id', $merchant_obj_store->store_id)
                    ->select('money')
                    ->sum('money');


                $merchant_id = $merchant_obj_store->merchant_id;

                //用户金额结算
                $Merchant = Merchant::where('id', $merchant_id)->first();
                if ($Merchant) {
                    echo '门店ID:' . $merchant_obj_store->store_id;
                    echo '</br>';
                    echo '商户余额:' . $Merchant->money;
                    echo '</br>';
                    echo '补结算余额:' . $money;
                    echo '</br>';
                    $all = $Merchant->money + $money;
                    echo '结算后商户余额:' . $all;
                    echo '</br>';

                    $Merchant->money = $Merchant->money + $money;
                    $Merchant->save();

                    $table = 'merchant_wallet_details' . '_' . $day;
                    $merchant_wallet_details = DB::table($table);

                    $merchant_wallet_details->where('settlement', $settlement)
                        ->where('store_id', $merchant_obj_store->store_id)
                        ->update(['settlement' => '01', 'settlement_desc' => "已结算"]);
                    echo '结算成功';
                    echo '</br>';


                } else {
                    $table = 'merchant_wallet_details' . '_' . $day;
                    $merchant_wallet_details = DB::table($table);

                    $merchant_wallet_details->where('settlement', $settlement)
                        ->where('store_id', $merchant_obj_store->store_id)
                        ->update(['settlement' => '04', 'settlement_desc' => "找不到结算人"]);
                    echo '找不到结算人';
                    echo '</br>';
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                dd($e);
            }

        } catch (\Exception $exception) {
            dd($exception);
        }
    }


//代理结算补录
    public
    function tf_user_settlemment(Request $request)
    {
        try {

            //传化
            $company = $request->get('company', 'tfpay');
            $is_settlement_user = $request->get('is_settlement_user', '02');
            $out_trade_no = $request->get('out_trade_no', '');


            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("orders");
            } else {
                $obj = DB::table('orders');
            }

            $where = [];
            if ($out_trade_no) {
                $where[] = ['out_trade_no', $out_trade_no];

                $order = $obj->where($where)
                    ->where('pay_status', 1)
                    ->select(
                        'id',
                        'out_trade_no',
                        'config_id',
                        'rate',
                        'store_id',
                        'user_id',
                        'ways_type',
                        'total_amount',
                        'created_at'
                    )
                    ->first();
            } else {
                $order = $obj->where('company', $company)
                    ->where('is_settlement_user', $is_settlement_user)
                    ->where('created_at', '>', '2019-11-11 00:00:00')
                    ->where('created_at', '<', date('Y-m-d 00:00:00', time()))
                    ->where('pay_status', 1)
                    ->select(
                        'id',
                        'out_trade_no',
                        'config_id',
                        'rate',
                        'store_id',
                        'user_id',
                        'ways_type',
                        'total_amount',
                        'created_at'
                    )
                    ->first();
            }


            if (!$order) {
                dd('全部补录制完毕');
            }
            $time_start = $order->created_at;
            $table = 'user_wallet_details' . '_' . date('Ymd', strtotime($time_start));
            if (env('DB_D1_HOST')) {
                if (Schema::hasTable($table)) {
                    $UserWalletDetail = DB::connection("mysql_d1")->table($table);
                } else {
                    $UserWalletDetail = DB::connection("mysql_d1")->table("user_wallet_details");
                }
            } else {
                if (Schema::hasTable($table)) {
                    $UserWalletDetail = DB::table($table);
                } else {
                    $UserWalletDetail = DB::table('user_wallet_details');
                }
            }
            $UserWalletDetail = $UserWalletDetail->where('out_trade_no', $order->out_trade_no)
                ->select('id')
                ->first();
            if ($UserWalletDetail) {
                Order::where('id', $order->id)->update(['is_settlement_user' => '01']);
                echo '订单号：' . $order->out_trade_no;
                echo '<br>';
                echo '无需补录';
            } else {
                $UserGetMoney = [
                    'config_id' => $order->config_id,//服务商ID
                    'user_id' => $order->user_id,//直属代理商
                    'store_id' => $order->store_id,//门店
                    'out_trade_no' => $order->out_trade_no,//订单号
                    'ways_type' => $order->ways_type,//通道类型
                    'order_total_amount' => $order->total_amount,//交易金额
                    'store_ways_type_rate' => $order->rate,//交易时的费率
                    'source_type' => '12000',//返佣来源
                    'source_desc' => 'TF-补',//返佣来源说明
                ];

                $obj = new  UserGetMoney($UserGetMoney);
                $obj->insert();
                echo '订单号：' . $order->out_trade_no;
                echo '<br>';
                echo '补录成功';
                Order::where('id', $order->id)->update(['is_settlement_user' => '01']);
            }


        } catch (\Exception $exception) {
            dd($exception);
        }
    }


//总结算
    public
    function tf_settlemment_all(Request $request)
    {
        try {

            //传化
            $store_id = $request->get('store_id', '');
            $time_s = "2019-09-01 00:00:00";
            $time_e = "2019-11-28 23:59:59";

            $store = Store::where('store_id', $store_id)->select('id', 'pid')->first();
            if (!$store) {
                dd('门店不存在');
            }

            $store_ids = $this->getStore_id($store_id, $store->id);
            //分店
            if ($store->pid) {
                dd('请提供总店ID');
            }
            //开启事务
            try {
                DB::beginTransaction();


                //300以下交易额
                $total_amount = Order::whereIn('store_id', $store_ids)
                    ->where('pay_status', 1)
                    ->where('total_amount', '>', '2')
                    ->where('company', 'tfpay')
                    ->where('total_amount', '<', '300')
                    ->where('created_at', '>=', $time_s)
                    ->where('created_at', '<=', $time_e)
                    ->select('total_amount')
                    ->sum('total_amount');

                echo '门店ID' . $store_id;
                echo '<br>';
                echo '时间：' . $time_s . '-' . $time_e;
                echo '<br>';
                echo '300以下总交易金额：' . $total_amount;
                echo '<br>';


                //获取到收银员ID
                $MerchantStore = MerchantStore::where('store_id', $store_id)
                    ->orderBy('created_at', 'asc')
                    ->select('merchant_id')
                    ->first();

                $merchant_id = $MerchantStore->merchant_id;
                echo '收银员ID：' . $merchant_id;
                echo '<br>';

                $ActivityStoreRate = ActivityStoreRate::where('store_id', $store_id)
                    ->where('company', 'tfpay')
                    ->select('alipay')
                    ->first();

                if ($ActivityStoreRate) {
                    echo '300以下应返比例：' . $ActivityStoreRate->alipay . '%';
                    echo '<br>';
                    $get = $total_amount * 0.0038 * ($ActivityStoreRate->alipay / 100);
                    echo '300以下应返金额：' . $get;
                    echo '<br>';
                } else {

                    $get = $total_amount * 0.0038;
                    echo '300以下应返金额：' . $get;
                    echo '<br>';
                }


                //已经提现的金额
                $amount = MerchantWithdrawalsRecords::where('merchant_id', $merchant_id)
                    ->where('status', 1)
                    ->select('amount')
                    ->sum('amount');
                echo '已经提现金额：' . $amount;
                echo '<br>';


                $Merchant = Merchant::where('id', $merchant_id)
                    ->first();
                echo '商户余额：' . $Merchant->money;
                echo '<br>';
                //差额
                $money = ($get - $amount - $Merchant->money);
                echo '差额：' . $money;
                echo '<br>';
                if ($money > 0) {
                    $Merchant->money = $Merchant->money + $money;
                    $Merchant->save();
                    echo '差额补录成功：' . $Merchant->name . '-' . $Merchant->phone;
                    echo '<br>';

                } else {
                    echo '无需补录';
                }


                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                dd($e);
            }

        } catch (\Exception $exception) {
            dd($exception);
        }
    }

// 首付科技-分润账单列表
    public
    function sf_commission_lists(Request $request)
    {
        try {
            $token = $this->parseToken();
            $token_user_id = $token->user_id;

            $time = $request->get('time_start', '');
            $user_id = $request->get('user_id', '');  //服务商id
            $amount_start = $request->get('amount_start', '');
            $amount_end = $request->get('amount_end', '');

            $login_user = User::where('id', $token_user_id)
                ->where('is_delete', '=', '0')
                ->first();
            if (!$login_user) {
                $this->status = '2';
                $this->message = '当前登录状态异常';
                return $this->format();
            }
            if ($login_user->level != '0') {
                $this->status = '3';
                $this->message = '非平台账户无此全权';
                return $this->format();
            }

            $re_data = [
                'time_start' => $time,
            ];
            $check_data = [
                'time_start' => '交易时间',
            ];
            $check = $this->check_required($re_data, $check_data);
            if ($check) {
                $this->status = '2';
                $this->message = $check;
                return $this->format();
            }

            $time_arr = explode('-', $time);
            $year = $time_arr[0];
            $month = $time_arr[1];
            $month_day = date('t', strtotime($time));
            $start_time = date('Y-m-d H:i:s', mktime(0, 0, 0, $month, 1, $year));
            $end_time = date('Y-m-d H:i:s', mktime(23, 59, 59, $month, $month_day, $year));

            if (isset($user_id) && ($user_id != 'undefined') && $user_id) {
                $user_ids = $this->getSubIdsAll($user_id);
            } else {
                $user_ids = $this->getSubIdsAll($token_user_id);
            }

            $where = [];
            if ($amount_start) {
                $where[] = ['total_amount', '>=', "$amount_start"];
            }
            if ($amount_end) {
                $where[] = ['total_amount', '<=', "$amount_end"];
            }
            if (1) {
                $where[] = ['ways_type', '!=', '2005'];
            }

            if (env('DB_DATABASE_FIRSTPAY')) {
                $db = DB::connection('mysql_firstpay'); //连接首付科技数据库
            } else {
                $this->status = '3';
                $this->message = '首付科技数据库连接异常';
                return $this->format();
            }

            $obj = $db->table('orders')->select([
                DB::raw("date_format(pay_time, '%Y-%m-%d') AS `modify_pay_time`"),
                'store_name',
                'company',
                'rate',
                'cost_rate',  //成本费率
                'ways_source_desc',
                DB::raw("SUM(`total_amount`) AS `total_amount_sum`"),
                DB::raw("SUM(`mdiscount_amount`) AS `mdiscount_amount_sum`"),
                DB::raw("SUM(IF((`pay_status`=1), `total_amount`, 0)) as `total_success_amount`"),
                DB::raw("SUM(IF((`pay_status`=6), `total_amount`, 0)) as `total_return_amount`"),
                'pay_status_desc',
                DB::raw("COUNT(`total_amount`) as `total_amount_count`"),
                'refund_amount' //分润金额
            ])
                ->where($where)
                ->where('pay_time', '>=', "$start_time")
                ->where('pay_time', '<=', "$end_time")
                ->where('pay_status_desc', '<>', '会员卡支付成功')
                ->where('company', '<>', 'alipay')
                ->where('company', '<>', 'weixin')
                ->whereIn('pay_status', ['1', '6'])
                ->whereIn('user_id', $user_ids)
                ->groupBy(['modify_pay_time', 'store_id', 'company', 'rate', 'ways_source', 'pay_status'])
                ->orderBy('modify_pay_time', 'asc');

            $this->t = count($obj->get()->toArray());
            $data = $this->page($obj)->get();
//            $this->order_data = [
//                'login_level' => $login_user->level,
//                'total_money' => number_format($total_money, 2),
//                'total_number' => $total_number,
//                'total_profit' => ,
//            ];
        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage() . '-' . $exception->getLine();
            return $this->format();
        }

        $this->status = '1';
        $this->message = '数据返回成功';
        return $this->format($data);
    }


//新增日结算设置
    public
    function addSettlement(Request $request)
    {
        try {
            Log::info($request);
            $public = $this->parseToken();
            $store_id = $request->get('store_key_ids');
            $user_id = $request->get('user_ids');
            $rate = $request->get('rate', '');
            $config_id = $public->config_id;
            $store_ids = explode(',', $store_id);
            $user_ids = explode(',', $user_id);

            $check_data = [
                'store_key_ids' => '返佣来源',
                'user_ids' => '服务商',
                'rate' => '税点'
            ];

            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            foreach ($store_ids as $k => $v) {
                foreach ($user_ids as $key => $val) {
                    //有信息不允许添加
                    $SettlementDay = SettlementDay::where('user_id', $val)->where('source_type', $v)->where('config_id', $config_id)->first();
                    if ($SettlementDay) {
                        return json_encode(['status' => 2, 'message' => "勾选内容中已有信息不允许添加"]);
                    }
                }
            }

            foreach ($store_ids as $k => $v) {
                //信息存在不允许添加
                foreach ($user_ids as $key => $val) {
                    $SettlementDay = SettlementDay::where('user_id', $val)->where('source_type', $v)->where('config_id', $config_id)->first();
                    if ($SettlementDay) {
                        continue;
                    }
                    try {
                        DB::beginTransaction(); //开启事务
                        $user_id = $val;
                        $store_id = $v;
                        $service = DB::table('users')->where('id', $val)->select('name')->get();
                        $name = $service[0]->name;
                        //服务商数据
                        $insert = [
                            'user_id' => $user_id,
                            'config_id' => $config_id,
                            'source_type' => $store_id,
                            'name' => $name,
                            'status' => 2,
                            'rate' => $rate,
                        ];
                        SettlementDay::create($insert);
                        DB::commit();
                    } catch (\Exception $ex) {
                        DB::rollBack();
                        continue;
                    }
                }


            }
            return json_encode([
                'status' => 1,
                'message' => '信息添加成功',
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine()
            ]);
        }
    }

//日结算列表
    public
    function settlementDayQuery(Request $request)
    {
        try {
            $public = $this->parseToken();
            $config_id = $public->config_id;
            $name = $request->get('name', '');
            $status = $request->get('status', '');
            $source_type = $request->get('status', '');
            $where = [];
            if ($name) {
                $where[] = ['name', '=', $name];
            }
            if ($config_id) {
                $where[] = ['config_id', '=', $config_id];
            }
            if ($status) {
                $where[] = ['status', '=', $status];
            }
            $obj = DB::table('settlement_day')->where($where);
            $this->message = '数据返回成功';
            $this->t = $obj->count();
            $data = $this->page($obj)->orderBy('updated_at', 'desc')->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }

//日结算单条信息
    public
    function dayInfo(Request $request)
    {
        try {
            $public = $this->parseToken();

            $id = $request->get('id', '');
            $data = SettlementDay::where('id', $id)->first();
            $this->message = '数据返回成功';
            return $this->format($data);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }

//更改税率
    public
    function upSettlement(Request $request)
    {
        try {
//            $public = $this->parseToken();

//            if ($public->type == "merchant") {
//                $user_id = $public->merchant_id;
//            }
//
//            if ($public->type == "user") {
//                $user_id = $public->user_id;
//            }

            $id = $request->get('id');
            $status = $request->get('status');
            $rate = $request->get('rate');
            $data = [
                'status' => $status,
                'rate' => $rate,
            ];
            SettlementDay::where('id', $id)->update($data);
            return json_encode([
                'status' => 1,
                'message' => '修改成功',
            ]);

        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }
    }


//关闭和开启日常任务
    public
    function changeDaily(Request $request)
    {

        try {
            $user = $this->parseToken();
            $id = $request->get('id');
            $status = $request->get('status', '2');
            $data = SettlementDay::where('id', $id)->first();
            if (!$data) {
                return json_encode(['status' => 2, 'message' => '任务不存在', 'data' => []]);
            }

            SettlementDay::where('id', $id)
                ->update(['status' => $status]);

            return json_encode(['status' => 1, 'message' => '操作成功', 'data' => $request->except(['token'])]);

        } catch (\Exception $exception) {
            return json_encode(['status' => 0, 'message' => $exception->getMessage() . ' - ' . $exception->getLine()]);
        }

    }

//未提分润金额统计
    public
    function get_user_money(Request $request)
    {
        try {
            $token = $this->parseToken();
            $login_user_id = $token->user_id;
            $user_id = $request->get('user_id', '');
            $is_withdraw = $request->get('is_withdraw'); //服务商id

            $where = [];

            if ($is_withdraw != "") {
                if ($is_withdraw == 0) {
                    $where[] = ['is_withdraw', '=', 0];
                } else if ($is_withdraw == 1) {
                    $where[] = ['is_withdraw', $is_withdraw];
                }
            }
            if ($user_id) {
                $where[] = ['id', $user_id];
            }
            $obj = User::where($where)
                ->where('is_delete', '0')
                ->where('money', '>=', '0.00')
                ->select('id', 'money', 'name', 'phone', 'is_withdraw', 'source', 'is_zero_rate', 'settlement_type', 'reward_money');

            $totalMoney = 0;
            $notMoney = 0;

            $objArr = $obj->get();
            foreach ($objArr as $k => $v) {

                if ($v->is_withdraw == 1) {
                    $totalMoney = $totalMoney + $v->money + $v->reward_money;
                } else {
                    $notMoney = $notMoney + $v->money + $v->reward_money;
                }
            }

            $total = $obj->sum('money') + $obj->sum('reward_money');
            $this->order_data = [
                'total' => number_format($total, 2, '.', ''),
                'totalMoney' => number_format($totalMoney, 2, '.', ''),
                'notMoney' => number_format($notMoney, 2, '.', ''),
            ];

            $this->t = $obj->count();
            $data = $this->page($obj)->orderBy('money', 'desc')->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);


        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }


}
