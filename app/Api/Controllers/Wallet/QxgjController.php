<?php
namespace App\Api\Controllers\Wallet;


use Alipayopen\Sdk\LtLogger;
use App\Api\Controllers\BaseController;
use App\Models\Merchant;
use App\Models\MerchantAccount;
use App\Models\MerchantWithdrawalsRecords;
use App\Models\QxgjUser;
use App\Models\User;
use App\Models\UserAccount;
use App\Models\UserWithdrawalsRecords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use MyBank\Tools;


class QxgjController extends BaseController
{
//    protected $qxgj_url = 'https://openapi.qxjtguanjia.com';    //正式环境
//    protected $appKey = '';
//    protected $appSecret = '';

    protected $qxgj_url = 'https://test.openapi.qxjtguanjia.com';   //测试环境
    protected $appKey = 'test'; // 测试
    protected $appSecret = 'test';


    public function add_qxgj_user(Request $request)
    {
        try {
            $public = $this->parseToken();

            $name = $request->get('name', '');
            $phone = $request->get('phone', '');
            $id_number = $request->get('id_number', '');
            $document_type = $request->get('document_type', '');

            $user_id = $public->user_id;

            $check_data = [
                'name' => '姓名',
                'phone' => '手机号码',
                'id_number' => '证件号码',
                'document_type' => '证件类型',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $account = QxgjUser::where('user_id', $user_id)->where('phone', $phone)->first();
            $staffMessage = "添加成功";
            if ($account) { //修改
                $account->name = $name;
                $account->phone = $phone;
                $account->id_number = $id_number;
                $account->document_type = $document_type;
                $account->save();

            } else { //添加
                QxgjUser::create([
                    'user_id' => $user_id,
                    'name' => $name,
                    'phone' => $phone,
                    'document_type' => $document_type,   //身份证/护照
                    'id_number' => $id_number,
                ]);

                $staffMessage = $this->addWalletStaff($name,$phone,$document_type,$id_number);
            }

            return json_encode([
                'status' => 1,
                'message' => $staffMessage,
                'data' => $request->except(['token'])
            ]);

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage()
            ]);
        }
    }

    public function qxgj_user(Request $request)
    {
        try {
            $public = $this->parseToken();
            $user_id = $public->user_id;

            $account = QxgjUser::where('user_id', $user_id)->first();

            if ($account) {

                $signUrl = $this->walletSignInfo($account->phone);

                $this->status = 1;
                $this->message = $signUrl;
                return $this->format($account);

            }

            return json_encode([
                'status' => 1,
                'message' => '数据返回成功',
                'data' => $request->except(['token'])
            ]);

        } catch (\Exception $exception) {
            return json_encode([
                'status' => 1,
                'message' => $exception->getMessage()
            ]);
        }

    }

    //导入人员接口地址 POST请求
    public function addWalletStaff($name,$phone,$document_type,$id_number){

//        导入人员接口地址: '/staff/import'
        $userImport = '/staff/import';
        $qxgj_url = $this->qxgj_url . $userImport;

        $body = [
            'documentType' => $document_type, //身份证/护照
            'idNumber' => $id_number,
            'name' => $name,
            'phone' => $phone,
            'typesWorkName' => '' //用工类型，不填获取默认用工类型
        ];

        $timestamp = date('Y-m-d H:i:s', time()); //请求交易时间,格式为：格式为：YYYY-MM-DD HH:MM:SS

        $params = [
            'appKey' => $this->appKey,  //'appKey' => 'test',
            'timestamp' => $timestamp,
            'format' => 'json',
            'version' => '1.0',
            'signType' => 'md5',
            'paramJson' => json_encode($body)
        ];

        $signStr = $this->getSignContent($params);
        //签名加密前
        $sign_before = $this->appSecret . $signStr .$this->appSecret;

        $sign = strtoupper(md5($sign_before));

        $paramsStr = [
            'appKey' => $this->appKey,  //'appKey' => 'test',
            'timestamp' => $timestamp,
            'format' => 'json',
            'version' => '1.0',
            'signType' => 'md5',
            'sign' => $sign
        ];

        $paramsStr = $this->getSignContent($paramsStr);
        $qxgj_url = $qxgj_url . '?' . $paramsStr;

        $resData =  $this->posturl($qxgj_url,$body);

        if($resData['code'] == '200'){
            $msg = 'success';
        }else{
            $msg = $resData['msg'];
        }

        return $msg;

    }

    //获取合同签署信息  GET请求
    public function walletSignInfo($phone){

        //获取合同签署信息
        $getSignInfo = '/staff/getSignInfo';
        $qxgj_url = $this->qxgj_url . $getSignInfo;
        $returnUrl = 'pay.jiangsuniuhonghong.cn';

        $timestamp = date('Y-m-d H:i:s', time()); //请求交易时间,格式为：格式为：YYYY-MM-DD HH:MM:SS

        $queryParams = [
            'appKey' => $this->appKey,  //'appKey' => 'test01',
            'timestamp' => $timestamp,
            'format' => 'json',
            'version' => '1.0',
            'signType' => 'md5',
            'terminalType' => 'wx_official_account', //终端类型（微信小程序:wxmini,微信公众号:wx_official_account,APP:app
            'phone' => $phone,
            'returnUrl' => $returnUrl //返回签署完成重定向url
        ];

        $signStr = $this->getSignContent($queryParams);

        //签名加密前
        $sign_before = $this->appSecret . $signStr .$this->appSecret;
        $sign = strtoupper(md5($sign_before));

        $queryParams['sign']  = $sign;

        $paramsStr = $this->getSignContent($queryParams);
        $qxgj_url = $qxgj_url . '?' . $paramsStr;

        $qxgj_url = str_replace(' ', '+', $qxgj_url);  //请求中不能使用空格，使用 + 替换

        $curl = Tools::curl_get($qxgj_url);
        $resData = json_decode($curl, true);
        if($resData['code'] == '200'){
            $status = $resData['data']['status'];  //合同签署状态（1：已签署 0:待签署)
            if($status == '1'){
                $qxgjUser = QxgjUser::where('phone', $phone)->where('is_sign', '0')->first();
                if ($qxgjUser) { //修改
                    $qxgjUser->is_sign = '1';
                    $qxgjUser->save();
                }
                return 'Success';
            }else{
                $signUrlStr = $resData['data']['signUrl'];
                $signUrlStr = base64_decode($signUrlStr);
                $arrSign = explode('?', $signUrlStr);

                $signUrl_ = str_replace(' ', '%', $arrSign[1]);
                $signUrl_ = str_replace(':', '%3A', $signUrl_); //请求中不能使用空格，使用 % 替换 ，: 使用 %3A 替换

                $signUrl = $arrSign[0] . $signUrl_;
            }
            return $signUrl;
        }else{
            return $resData['msg'];
        }

    }

    // GET请求
    public function getBalance(){

        $timestamp = date('Y-m-d H:i:s', time()); //请求交易时间,格式为：格式为：YYYY-MM-DD HH:MM:SS
        $queryParams = [
            'appKey' => $this->appKey,  //'appKey' => 'test01',
            'timestamp' => $timestamp,
            'format' => 'json',
            'version' => '1.0',
            'signType' => 'md5'
        ];
        $signStr = $this->getSignContent($queryParams);

        //签名加密前
        $sign_before = $this->appSecret . $signStr .$this->appSecret;
        $sign = strtoupper(md5($sign_before));
        $queryParams['sign']  = $sign;

        $urla = $this->qxgj_url . '/partner/getBalance';
        $paramsStr = $this->getSignContent($queryParams);

        $urla = $urla . '?' . $paramsStr;

        $urla = str_replace(' ', '+', $urla);  //请求中不能使用空格，使用 + 替换

        $curl = Tools::curl_get($urla);
        $resData = json_decode($curl, true);
        return $resData;

    }

    //下单 POST请求
    public function revenueOrder($userType,$phone,$selectPayType,$bankAccount,$outTradeNo,$money,$sxf_amount){
        try {
            $resMsg = '';
            //检查账户余额
            $accountData =  $this->getBalance();
            if($accountData['code'] == '200'){
                if($accountData['data'] < $money - $sxf_amount){
                    $resMsg = "付款方余额不足";

                    if ($userType == 'user') {
                        //开启事务
                        try {
                            DB::beginTransaction();
                            $UserWithdrawalsRecords = UserWithdrawalsRecords::where('out_trade_no', $outTradeNo)->first();
                            $user = User::where('id', $UserWithdrawalsRecords->user_id)->first();
                            $user->money = $user->money + $money;
                            $user->save();

                            $UserWithdrawalsRecords->status = 3;
                            $UserWithdrawalsRecords->status_desc = '提现失败-' . $resMsg;
                            $UserWithdrawalsRecords->remark = $resMsg;
                            $UserWithdrawalsRecords->save();

                            DB::commit();
                        } catch (\Exception $e) {
                            Log::info($e);
                            DB::rollBack();
                        }
                    }
                    if ($userType == 'merchant') {
                        //开启事务
                        try {
                            DB::beginTransaction();
                            $MerchantWithdrawalsRecords = MerchantWithdrawalsRecords::where('out_trade_no', $outTradeNo)->first();
                            $merchant = Merchant::where('id', $MerchantWithdrawalsRecords->merchant_id)->first();
                            $merchant->money = $merchant->money + $money;
                            $merchant->save();

                            $UserWithdrawalsRecords->status = 3;
                            $UserWithdrawalsRecords->status_desc = '提现失败-' . $resMsg;
                            $UserWithdrawalsRecords->remark = $resMsg;
                            $UserWithdrawalsRecords->save();

                            DB::commit();
                        } catch (\Exception $e) {
                            Log::info($e);
                            DB::rollBack();
                        }
                    }

                    return $resMsg;
                }
            }

            $order = '/revenue/order';
            $qxgj_url = $this->qxgj_url . $order;

            $body = [
                'phone' => $phone,
                'selectPayType' => $selectPayType, //付款方式：bank/alipay
                'bankAccount' => $bankAccount, //支付卡号（银行支付填写银行卡号，支付宝填写支付宝号 不填则取系统默认的支付卡号）
                'subject' => '提现',  //交易标题/订单标题/订单关键字等
                'outTradeNo' => $outTradeNo, //商户订单号
                'revenueMonth' => '', //收入月份：例2020-10，为空则为当月
                'payable' => $money - $sxf_amount, //应付金额：保留两位小数 单位：元
                'remark' => '', //备注
                'callbackUrl' => '', //回调地址

            ];

            $timestamp = date('Y-m-d H:i:s', time()); //请求交易时间,格式为：格式为：YYYY-MM-DD HH:MM:SS

            $params = [
                'appKey' => $this->appKey,  //'appKey' => 'test',
                'timestamp' => $timestamp,
                'format' => 'json',
                'version' => '1.0',
                'signType' => 'md5',
                'paramJson' => json_encode($body)
            ];

            $signStr = $this->getSignContent($params);
            //签名加密前
            $sign_before = $this->appSecret . $signStr .$this->appSecret;
            $sign = strtoupper(md5($sign_before));

            $paramsStr = [
                'appKey' => $this->appKey,  //'appKey' => 'test',
                'timestamp' => $timestamp,
                'format' => 'json',
                'version' => '1.0',
                'signType' => 'md5',
                'sign' => $sign
            ];

            $paramsStr = $this->getSignContent($paramsStr);
            $qxgj_url = $qxgj_url . '?' . $paramsStr;

            $resData =  $this->posturl($qxgj_url,$body);

            if($resData['code'] == '200'){
                $status = $resData['data']['status'];  //订单状态 -1:付款失败 0:待付款 1:支付中 2:已付款
                if($status == '-1'){
                    $resMsg = '付款失败';
                }if($status == '0'){
                    $resMsg = '待付款';
                }if($status == '1'){
                    $resMsg = '支付中';
                }if($status == '2'){
                    $resMsg = '已付款';
                }
            }else{
                $resMsg = $resData['msg'];
            }

            //更改状态
            if ($userType == 'user') {

                if($resData['code'] == '200'){
                    $status = $resData['data']['status'];  //订单状态 -1:付款失败 0:待付款 1:支付中 2:已付款
                    if($status == '1' || $status == '2'){
                        $UserWithdrawalsRecords = UserWithdrawalsRecords::where('out_trade_no', $outTradeNo)->first();
                        $UserWithdrawalsRecords->status = 1;
                        $UserWithdrawalsRecords->status_desc = '成功';
                        $UserWithdrawalsRecords->save();
                    }else if($status == '-1'){
                        //开启事务
                        try {
                            DB::beginTransaction();
                            $UserWithdrawalsRecords = UserWithdrawalsRecords::where('out_trade_no', $outTradeNo)->first();
                            $user = User::where('id', $UserWithdrawalsRecords->user_id)->first();
                            $user->money = $user->money + $money;
                            $user->save();

                            $UserWithdrawalsRecords->status = 3;
                            $UserWithdrawalsRecords->status_desc = '提现失败-' . $resMsg;
                            $UserWithdrawalsRecords->remark = $resMsg;
                            $UserWithdrawalsRecords->save();

                            DB::commit();
                        } catch (\Exception $e) {
                            Log::info($e);
                            DB::rollBack();
                        }
                    }else if($status == '0'){
                        //开启事务
                        try {
                            DB::beginTransaction();
                            $UserWithdrawalsRecords = UserWithdrawalsRecords::where('out_trade_no', $outTradeNo)->first();
                            $user = User::where('id', $UserWithdrawalsRecords->user_id)->first();
                            $user->money = $user->money + $money;
                            $user->save();

                            $UserWithdrawalsRecords->status = 3;
                            $UserWithdrawalsRecords->status_desc = '提现失败-' . $resMsg;
                            $UserWithdrawalsRecords->remark = $resMsg;
                            $UserWithdrawalsRecords->save();

                            DB::commit();
                        } catch (\Exception $e) {
                            Log::info($e);
                            DB::rollBack();
                        }
                    }
                }else{
                    //开启事务
                    try {
                        DB::beginTransaction();
                        $UserWithdrawalsRecords = UserWithdrawalsRecords::where('out_trade_no', $outTradeNo)->first();
                        $user = User::where('id', $UserWithdrawalsRecords->user_id)->first();
                        $user->money = $user->money + $money;
                        $user->save();

                        $UserWithdrawalsRecords->status = 3;
                        $UserWithdrawalsRecords->status_desc = '提现失败-' . $resMsg;
                        $UserWithdrawalsRecords->remark = $resMsg;
                        $UserWithdrawalsRecords->save();

                        DB::commit();
                    } catch (\Exception $e) {
                        Log::info($e);
                        DB::rollBack();
                    }
                }

            }

            if ($userType == 'merchant') {

                if($resData['code'] == '200'){
                    $status = $resData['data']['status'];  //订单状态 -1:付款失败 0:待付款 1:支付中 2:已付款
                    if($status == '1' || $status == '2'){
                        $MerchantWithdrawalsRecords = MerchantWithdrawalsRecords::where('out_trade_no', $outTradeNo)->first();
                        $MerchantWithdrawalsRecords->status = 1;
                        $MerchantWithdrawalsRecords->status_desc = '成功';
                        $MerchantWithdrawalsRecords->save();
                    }else if($status == '-1'){
                        //开启事务
                        try {
                            DB::beginTransaction();
                            $MerchantWithdrawalsRecords = MerchantWithdrawalsRecords::where('out_trade_no', $outTradeNo)->first();
                            $merchant = Merchant::where('id', $MerchantWithdrawalsRecords->merchant_id)->first();
                            $merchant->money = $merchant->money + $money;
                            $merchant->save();

                            $UserWithdrawalsRecords->status = 3;
                            $UserWithdrawalsRecords->status_desc = '提现失败-' . $resMsg;
                            $UserWithdrawalsRecords->remark = $resMsg;
                            $UserWithdrawalsRecords->save();

                            DB::commit();
                        } catch (\Exception $e) {
                            Log::info($e);
                            DB::rollBack();
                        }
                    }else if($status == '0'){
                        //开启事务
                        try {
                            DB::beginTransaction();
                            $MerchantWithdrawalsRecords = MerchantWithdrawalsRecords::where('out_trade_no', $outTradeNo)->first();
                            $merchant = Merchant::where('id', $MerchantWithdrawalsRecords->merchant_id)->first();
                            $merchant->money = $merchant->money + $money;
                            $merchant->save();

                            $UserWithdrawalsRecords->status = 3;
                            $UserWithdrawalsRecords->status_desc = '提现失败-' . $resMsg;
                            $UserWithdrawalsRecords->remark = $resMsg;
                            $UserWithdrawalsRecords->save();

                            DB::commit();
                        } catch (\Exception $e) {
                            Log::info($e);
                            DB::rollBack();
                        }
                    }
                }else{
                    //开启事务
                    try {
                        DB::beginTransaction();
                        $MerchantWithdrawalsRecords = MerchantWithdrawalsRecords::where('out_trade_no', $outTradeNo)->first();
                        $merchant = Merchant::where('id', $MerchantWithdrawalsRecords->merchant_id)->first();
                        $merchant->money = $merchant->money + $money;
                        $merchant->save();

                        $UserWithdrawalsRecords->status = 3;
                        $UserWithdrawalsRecords->status_desc = '提现失败-' . $resMsg;
                        $UserWithdrawalsRecords->remark = $resMsg;
                        $UserWithdrawalsRecords->save();

                        DB::commit();
                    } catch (\Exception $e) {
                        Log::info($e);
                        DB::rollBack();
                    }
                }

            }

            return $resMsg;
        } catch (\Exception $e) {
            $this->writelog(storage_path() . '/logs/QxgjRevenueOrder.log', $e);
        }

    }


    public function writelog($log_file, $exception)
    {
        try {
            $logger = new LtLogger();
            $logger->conf["log_file"] = $log_file;
            $logger->conf["separator"] = "---------------";
            $logData = array(
                date("Y-m-d H:i:s"),
                str_replace("\n", "", $exception),
            );
            $logger->log($logData);
        } catch (\Exception $exception) {
            Log::info($exception);
        }
    }

    function posturl($url,$data){

        $url = str_replace(' ', '+', $url);  //请求中不能使用空格，使用 + 替换

        $data  = json_encode($data);
        $headerArray =array("Content-type:application/json;charset='utf-8'","Accept:application/json");
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,FALSE);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl,CURLOPT_HTTPHEADER,$headerArray);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);
        return json_decode($output,true);
    }

    //参数拼接
    public function getSignContent($params)
    {
        ksort($params);

        $stringToBeSigned = "";
        $i = 0;
        foreach ($params as $k => $v) {
            if (false === $this->checkEmpty($v) && "@" != substr($v, 0, 1)) {
                // 转换成目标字符集
                $v = $this->characet($v, 'UTF-8');

                if ($i == 0) {
                    $stringToBeSigned .= "$k" . "=" . "$v";
                } else {
                    $stringToBeSigned .= "&" . "$k" . "=" . "$v";
                }
                $i++;
            }
        }

        unset ($k, $v);
        return $stringToBeSigned;
    }

    /**
     * 校验$value是否非空
     *  if not set ,return true;
     *    if is null , return true;
     **/
    protected function checkEmpty($value)
    {
        if (!isset($value)) return true;
        if ($value === null) return true;
        if (trim($value) === "") return true;

        return false;
    }

    /**
     * 转换字符集编码
     * @param $data
     * @param $targetCharset
     * @return string
     */
    function characet($data, $targetCharset)
    {
        if (!empty($data)) {
            $fileType = 'UTF-8';
            if (strcasecmp($fileType, $targetCharset) != 0) {
                $data = mb_convert_encoding($data, $targetCharset);
                //$data = iconv($fileType, $targetCharset.'//IGNORE', $data);
            }
        }

        return $data;
    }

}
