<?php

namespace App\Api\Controllers\CustomerApplets;

use Illuminate\Http\Request;
use App\Api\Controllers\CustomerApplets\BaseController;
use App\Models\ProvinceCityArea;

class AreaController extends BaseController
{
    /**
     * 获取省
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProvince(Request $request)
    {
        if ($request->isMethod('POST')) {
            $area = new ProvinceCityArea();
            //获取省
            $result = $area->getProvince();
            return $this->responseDataJson(200, '', $result);
        } else {
            return $this->responseDataJson(203, '请求方式错误!');
        }
    }

    /**
     * 获取市区
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCity(Request $request)
    {
        if ($request->isMethod('POST')) {
            if (empty($request->post('province_id'))) {
                $goBackData = $this->responseDataJson(202, 'province_id传递错误!');
                return $goBackData;
            }
            $area = new ProvinceCityArea();
            //获取市区
            $result = $area->getCity($request->post('province_id'));
            return $this->responseDataJson(200, '', $result);
        } else {
            return $this->responseDataJson(203);
        }
    }

    /**
     * 获取县级
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getArea(Request $request)
    {
        if ($request->isMethod('POST')) {
            if (empty($request->post('city_id'))) {
                $goBackData = $this->responseDataJson(202, 'city_id传递错误!');
                return $goBackData;
            }
            $area = new ProvinceCityArea();
            //获取县级
            $result = $area->getArea($request->post('city_id'));
            return $this->responseDataJson(200, '', $result);
        } else {
            return $this->responseDataJson(203);
        }
    }
}
