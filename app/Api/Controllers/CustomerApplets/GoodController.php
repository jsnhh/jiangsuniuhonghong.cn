<?php

namespace App\Api\Controllers\CustomerApplets;

use Illuminate\Http\Request;
use App\Api\Controllers\CustomerApplets\BaseController;
use Illuminate\Support\Facades\DB;
use App\Models\Goods;

class GoodController extends BaseController
{

    /**
     * 获取该门店下的商品信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGoods(Request $request){
        $requestAllData = $request->all();

        if(!isset($requestAllData['store_id']) || empty($requestAllData['store_id'])){
            return $this->responseDataJson(4002);
        }

        if(!isset($requestAllData['terminal_type']) || empty($requestAllData['terminal_type'])) {
            return $this->responseDataJson(4003);
        }

        $goodsModel = new Goods();

        $user_id = isset($requestAllData['user_id']) ? $requestAllData['user_id'] : "";
        $order_id = isset($requestAllData['order_id']) ? $requestAllData['order_id'] : "";

        $result = $goodsModel->getGoods($user_id, $requestAllData['store_id'], $order_id);

        if($result['status'] == 200){
            return $this->responseDataJson(200,"请求成功",$result['message']);
        }else{
            return $this->responseDataJson($result['status'],$result['message'],"");
        }
    }

    /**
     * 获取商品下的规格数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGoodStandard(Request $request){

        $requestAllData = $request->all();

        if(!isset($requestAllData['terminal_type']) || empty($requestAllData['terminal_type'])) {
            return $this->responseDataJson(4003);
        }

        $goodsModel     = new Goods();

        $result         = $goodsModel->getGoodStandard(isset($requestAllData['user_id']) ? $requestAllData['user_id'] : "",$requestAllData['store_id'],$requestAllData['good_id']);

        if($result['status'] == 200){
            return $this->responseDataJson(200,"请求成功",$result['message']);
        }else{
            return $this->responseDataJson($result['status'],$result['message'],"");
        }

    }

}
