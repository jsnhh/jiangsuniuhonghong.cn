<?php
namespace App\Api\Controllers\CustomerApplets;

use EasyWeChat\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use App\Models\Store;
use App\Models\MemberList;
use App\Models\WeChatMemberList;
use App\Models\WechatMemberMoneyList;
use App\Models\WechatMemberPointsList;
use App\Api\Controllers\Config\WeixinConfigController;

class WechatMemberController extends BaseController
{
    /**
     * 创建微信正式会员
     * @param Request $request
     * @return array
     */
    public function addWechatMember(Request $request)
    {
        $requestData = $request->all();

        // 参数校验
        $check_data = [
            'mb_id'         => '会员id',
            'store_id'      => '门店号',
            // 'applet_openid' => '用户小程序openid',
            // 'out_trade_no'  => '订单号',
            'ways_type'     => '通道',
        ];
        $check = $this->check_required($requestData, $check_data);
        if ($check) {
            return $this->sys_response(400001, $check);
        }

        $model                      = new WeChatMemberList();
        $memberListModel            = new MemberList();
        $wechatMemberMoneyList      = new WechatMemberMoneyList();
        $wechatMemberPointsList     = new WechatMemberPointsList();

        // 获取用户微信公众号openid
        if (isset($requestData['out_trade_no']) && !empty($requestData['out_trade_no'])) {
            $wechat_openid = Cache::get($requestData['mb_id'] . '_wechat_openid');
            if (empty($wechat_openid)) {
                $wechat_openid = $this->getUserWechatOpenid($requestData);
            }
        }

        // 获取此会员临时表数据
        $memberTemporaryInfo = $memberListModel->getMemberInfo($requestData);

        $date_now = date('Y-m-d H:i:s', time());
        $data_update = [
            'store_id'      => $requestData['store_id'],
            'mb_id'         => $requestData['mb_id'],
            'mb_status'     => 1,
            'mb_ver'        => 1,
            'mb_ver_desc'   => '普通会员',
            'mb_logo'       => isset($memberTemporaryInfo['mb_logo']) ? $memberTemporaryInfo['mb_logo'] : '',
            'mb_nickname'   => isset($memberTemporaryInfo['mb_name']) ? $memberTemporaryInfo['mb_name'] : '',
            'mb_phone'      => isset($memberTemporaryInfo['mb_phone']) ? $memberTemporaryInfo['mb_phone'] : '',
            'updated_at'    => $date_now,
        ];

        if (!empty($wechat_openid)) {
            $data_update['wechat_openid'] = $wechat_openid;
        }
        if (isset($requestData['applet_openid']) && !empty($requestData['applet_openid'])) {
            $data_update['applet_openid'] = $requestData['applet_openid'];
        }

        // 查询是否已有微信正式会员信息
        $wechatMemberInfo = $model->getWechatMemberInfo($requestData);

        // 会员信息参数初始化
        $user_money     = 0.00;  // 会员余额
        $user_points    = 0;     // 会员积分
        $money          = 0.00;  // 会员变动后余额

        // 开启事务
        try {
            DB::beginTransaction();

            // 有会员信息更新，没有新增
            if (!empty($wechatMemberInfo)) {
                $user_money         = $wechatMemberInfo['mb_money'];            // 会员余额
                $user_points        = $wechatMemberInfo['user_points'];         // 会员积分
                $mb_virtual_money   = $wechatMemberInfo['mb_virtual_money'];    // 会员虚拟金额

                // 会员余额支付
                if (isset($requestData['user_pay_money']) && !empty($requestData['user_pay_money'])) {
                    // use_pay_money_type == 1 会员充值，use_pay_money_type == 2 会员消费
                    if ($requestData['user_pay_money_type'] == 1) {
                        if (isset($requestData['user_pay_money_give']) && !empty($requestData['user_pay_money_give'])) {
                            $money = $user_money + $requestData['user_pay_money'] + $requestData['user_pay_money_give'];
                            $data_update['mb_virtual_money'] = $mb_virtual_money + $requestData['user_pay_money_give'];
                        } else {
                            $money = $user_money + $requestData['user_pay_money'];
                        }
                    } else if ($requestData['user_pay_money_type'] == 2) {
                        if ($user_money < $requestData['user_pay_money']) {
                            Log::info("微信创建正式会员-更新会员-余额不足");
                            throw new \Exception('会员余额不足');
                        } else {
                            $money = $user_money - $requestData['user_pay_money'];
                        }
                    }

                    $data_update['mb_money'] = $money;
                } else { // 会员微信支付
                    $mb_pay_counts = $wechatMemberInfo['mb_pay_counts'] + 1;
                    $data_update['mb_pay_counts'] = $mb_pay_counts;
                }

                $model->updateData($requestData['mb_id'], $data_update);
            } else {
                // 会员余额支付
                if (isset($requestData['user_pay_money']) && !empty($requestData['user_pay_money'])) {
                    if ($requestData['user_pay_money_type'] == 2) {
                        Log::info("微信创建正式会员-新增会员-余额不足");
                        throw new \Exception('会员余额不足');
                    } else if ($requestData['user_pay_money_type'] == 1) {
                        if (isset($requestData['user_pay_money_give']) && !empty($requestData['user_pay_money_give'])) {
                            $money = $requestData['user_pay_money'] + $requestData['user_pay_money_give'];
                            $data_update['mb_virtual_money'] = $requestData['user_pay_money_give'];
                        } else {
                            $money = $requestData['user_pay_money'];
                        }
                    }

                    $data_update['mb_money'] = $money;
                } else { // 会员微信支付
                    $mb_pay_counts = 1;
                    $data_update['mb_pay_counts'] = $mb_pay_counts;
                }

                $data_update['mb_time']     = $date_now;
                $data_update['created_at']  = $date_now;
                $model->addData($data_update);
            }

            // 会员余额消费、积分消费公共信息
            $data_common = [
                'store_id'      => $requestData['store_id'],
                'mb_id'         => $requestData['mb_id'],
                'created_at'    => $date_now,
                'updated_at'    => $date_now,
            ];

            if (!empty($wechat_openid)) {
                $data_common['wechat_openid'] = $wechat_openid;
            }
            if (isset($requestData['applet_openid']) && !empty($requestData['applet_openid'])) {
                $data_common['applet_openid'] = $requestData['applet_openid'];
            }

            // 修改会员消费记录表（会员余额支付才修改）
            if (isset($requestData['user_pay_money']) && !empty($requestData['user_pay_money'])) {
                $data_member_money                      = $data_common;
                $data_member_money['money']             = $requestData['user_pay_money'];
                $data_member_money['money_type']        = $requestData['user_pay_money_type'];
                $data_member_money['out_trade_no']      = isset($requestData['out_trade_no']) ? $requestData['out_trade_no'] : '';

                if (isset($requestData['user_pay_money_type']) && $requestData['user_pay_money_type'] == 1) {
                    $data_member_money['mb_virtual_money_give'] = isset($requestData['user_pay_money_give']) ? $requestData['user_pay_money_give'] : 0.00;
                } else {
                    $data_member_money['mb_virtual_money_give'] = 0.00;
                }

                $wechatMemberMoneyList->addData($data_member_money);
            }

            // 修改会员积分表
            if (isset($requestData['user_use_points']) && !empty($requestData['user_use_points'])) {
                $data_member_points                 = $data_common;
                $data_member_points['points']       = $requestData['user_use_points'];
                $data_member_points['points_type']  = $requestData['user_use_points_type'];

                $wechatMemberPointsList->addData($data_member_points);
            }

            DB::commit();
            Log::info("创建微信正式会员-操作成功");
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info("创建微信正式会员-操作失败");
            Log::info($e->getMessage());
        }
    }

    // 获取用户openid
    public function getUserWechatOpenid($input)
    {
        $mb_id          = $input['mb_id'];
        $store_id       = $input['store_id'];
        $out_trade_no   = $input['out_trade_no'];
        $ways_type      = $input['ways_type'];

        // 查询用户微信公众号openid（直接引用订单查询里的方法）
        $store = Store::where('store_id', $store_id)
            ->select('config_id', 'merchant_id', 'pid')
            ->first();
        $config_id = $store->config_id;
        $store_pid = $store->pid;

        $config = new WeixinConfigController();

        $options        = [];
        $weixin_store   = [];

        // 微信官方
        if (1999 < $ways_type && $ways_type < 2999) {
            // 区分微信和微信a
            $options = $config->weixin_config($config_id);
            $weixin_store = $config->weixin_merchant($store_id, $store_pid);
        }

        // 微信官方a
        if (3999 < $ways_type && $ways_type < 4999) {
            // 区分微信和微信a
            $options = $config->weixina_config($config_id);
            $weixin_store = $config->weixina_merchant($store_id, $store_pid);
        }

        if (empty($weixin_store)) {
            Log::info("微信创建正式会员-门店商户对应通道号未开通");
        }
        if (empty($options)) {
            Log::info("微信创建正式会员-微信或微信a参数未配置");
        }

        $wechat_openid = '';
        if (!empty($weixin_store)) {
            $wx_sub_merchant_id = $weixin_store->wx_sub_merchant_id;
            $config = [
                'app_id'        => $options['app_id'],
                'mch_id'        => $options['payment']['merchant_id'],
                'key'           => $options['payment']['key'],
                'cert_path'     => $options['payment']['cert_path'], // XXX: 绝对路径！！！！
                'key_path'      => $options['payment']['key_path'], // XXX: 绝对路径！！！！
                'sub_mch_id'    => $wx_sub_merchant_id,
            ];

            $payment = Factory::payment($config);
            $query = $payment->order->queryByOutTradeNumber($out_trade_no);

            $wechat_openid = '';
            if ('SUCCESS' === $query['result_code']) {
                $wechat_openid = $query['openid'];
                Cache::put($mb_id . '_wechat_openid', $wechat_openid, 15);
            } else {
                Log::info("创建微信正式会员-获取openid失败");
                Log::info($query['err_code_des']);
            }
        }

        return $wechat_openid;
    }

    /**
     * 编辑微信正式会员信息
     * @param Request $request
     * @return array
     */
    public function editWechatMember(Request $request)
    {
        $requestData = $request->all();

        // 参数校验
        $check_data = [
            'mb_id' => '会员mb_id',
        ];
        $check = $this->check_required($requestData, $check_data);
        if ($check) {
            return $this->sys_response(400001, $check);
        }

        $model                      = new WeChatMemberList();
        $wechatMemberMoneyList      = new WechatMemberMoneyList();
        $wechatMemberPointsList     = new WechatMemberPointsList();

        // 查询微信正式会员信息
        $wechatMemberInfo = $model->getWechatMemberInfo($requestData);
        $user_money     = $wechatMemberInfo['mb_money'];        // 会员余额
        $user_points    = $wechatMemberInfo['user_points'];     // 会员积分

        $date_now = date('Y-m-d H:i:s', time());
        // 会员正式表更新参数
        $data_update = [
            'mb_nickname'   => $requestData['mb_nickname'],
            'mb_phone'      => $requestData['mb_phone'],
            'mb_ver'        => $requestData['mb_ver'],
            'mb_ver_desc'   => $requestData['mb_ver_desc'],
        ];

        // 开启事务
        try {
            DB::beginTransaction();

            // 更新微信正式会员余额
            if (isset($requestData['user_pay_money']) && !empty($requestData['user_pay_money'])) {
                // use_pay_money_type == 1 会员充值，use_pay_money_type == 2 会员消费
                if ($requestData['user_pay_money_type'] == 1) {
                    if (isset($requestData['user_pay_money_give']) && !empty($requestData['user_pay_money_give'])) {
                        $user_money = $user_money + $requestData['user_pay_money'] + $requestData['user_pay_money_give'];
                        $data_update['mb_virtual_money'] = $requestData['user_pay_money_give'];
                    } else {
                        $user_money = $user_money + $requestData['user_pay_money'];
                    }
                } else if ($requestData['user_pay_money_type'] == 2) {
                    if ($user_money < $requestData['user_pay_money']) {
                        Log::info("微信创建正式会员-更新会员-余额不足");
                    } else {
                        $user_money = $user_money - $requestData['user_pay_money'];
                    }
                }

                $data_update['mb_money'] = $user_money;
            }

            // 更新微信正式会员积分
            if (isset($requestData['user_use_points']) && !empty($requestData['user_use_points'])) {
                // user_use_points_type == 1 会员充值，user_use_points_type == 2 会员消费积分
                if ($requestData['user_use_points_type'] == 1) {
                    $user_points = $user_points + $requestData['user_use_points'];
                } else if ($requestData['user_use_points_type'] == 2) {
                    if ($user_points < $requestData['user_use_points']) {
                        Log::info("微信创建正式会员-更新会员-积分不足");
                    } else {
                        $user_points = $user_points - $requestData['user_use_points'];
                    }
                }

                $data_update['mb_points'] = $user_points;
            }

            $model->updateData($requestData['mb_id'], $data_update);

            // 会员余额消费、积分消费公共信息
            $data_common = [
                'store_id'      => $requestData['store_id'],
                'mb_id'         => $requestData['mb_id'],
                'created_at'    => $date_now,
                'updated_at'    => $date_now,
            ];

            // 修改会员消费记录表（会员余额支付才修改）
            if (isset($requestData['user_pay_money']) && !empty($requestData['user_pay_money'])) {
                $data_member_money                      = $data_common;
                $data_member_money['money']             = $requestData['user_pay_money'];
                $data_member_money['money_type']        = $requestData['user_pay_money_type'];

                $wechatMemberMoneyList->addData($data_member_money);
            }

            // 修改会员积分表
            if (isset($requestData['user_use_points']) && !empty($requestData['user_use_points'])) {
                $data_member_points                 = $data_common;
                $data_member_points['points']       = $requestData['user_use_points'];
                $data_member_points['points_type']  = $requestData['user_use_points_type'];

                $wechatMemberPointsList->addData($data_member_points);
            }

            DB::commit();
            Log::info("编辑微信正式会员-操作成功");
            return $this->sys_response(200);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info("编辑微信正式会员-操作失败");
            return $this->sys_response(40000);
        }

    }

}
