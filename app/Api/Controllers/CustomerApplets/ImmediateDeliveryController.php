<?php
/**
 * Created by PhpStorm.
 * User: wangjie
 * Date: 2021/3/1
 * Time: 14:03
 */

namespace App\Api\Controllers\CustomerApplets;

use App\Api\Controllers\CustomerApplets\BaseController as BBaseController;
use App\Models\CustomerAppletsWeChatOrder;
use App\Models\CustomerAppletsWeChatOrderCancel;
use App\Models\CustomerAppletsWeChatPsConfig;
use App\Models\MerchantStoreAppidSecret;
use App\Models\Store;
use App\Models\CustomerAppletsWeChatCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ImmediateDeliveryController extends BBaseController
{

    /**
     * 开通即时配送(仅第三方调用)
     * 第三方代商户发起开通即时配送权限
     *
     * 使用场景:
     * 1、只能由第三方服务商调用此接口
     * 2、服务商可通过本接口代开发的小程序发起开通即时配送接口权限的操作,当调用成功,小程序管理员将收到模版消息,进行开通操作
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function businessOpen(Request $request)
    {
        $store_id = $request->post('storeId', ''); //系统门店id

        try {
            $except_token_data = $request->except(['token']);
            $validate = Validator::make($except_token_data, [
                'storeId'=>'required|max:50',
            ], [
                'required' => ':attribute参数为必填项',
                'min' => ':attribute参数长度太短',
                'max' => ':attribute参数长度太长',
                'unique' => ':attribute参数已经被人占用',
                'exists' => ':attribute参数不存在',
                'integer' => ':attribute参数必须是数字',
                'required_if' => ':attribute参数满足条件时必传',
            ], [
                'storeId' => '系统门店id'
            ]);
            if ($validate->fails()) {
                return $this->responseDataJson(2, $validate->getMessageBag()->first());
            }

            $access_token = $this->get_access_token($store_id);
            $requestUrl = "https://api.weixin.qq.com/cgi-bin/express/local/business/open?access_token=".$access_token;

            $requestResultObj = $this->curl_post_https($requestUrl);
            $requestResult = json_decode($requestResultObj, true);
            if ($requestResult['errcode'] == 0 || !isset($requestResult['errcode'])) {
                if ($requestResult['resultcode'] == 0) {
                    $status = 1;
                    $status_desc = '开通成功';
//                    return $this->responseDataJson(1, $requestResult['resultmsg'], $requestResult);
                } else {
                    $status = 0;
                    $status_desc = '未开通';
//                    return $this->responseDataJson(2, $requestResult['resultmsg']);
                }
            } else {
                if ($requestResult['errcode'] == 43016) {
                    $status = 2;
                    $status_desc = '小程序未认证';
//                    return $this->responseDataJson(43016);
                } elseif ($requestResult['errcode'] == 86000) {
                    $status = 3;
                    $status_desc = '不是第三方的调用';
//                    return $this->responseDataJson(86000);
                } elseif ($requestResult['errcode'] == 930568) {
                    $status = 4;
                    $status_desc = '不支持个人类型小程序';
//                    return $this->responseDataJson(930568);
                } elseif ($requestResult['errcode'] == 930569) {
                    $status = 5;
                    $status_desc = '已经开通不需要再开通';
//                    return $this->responseDataJson(930569);
                } elseif ($requestResult['errcode'] == 930571) {
                    $status = 6;
                    $status_desc = '该商户没有内测权限';
//                    return $this->responseDataJson(930571);
                } else {
                    $status = 0;
                    $status_desc = '未开通';
//                    return $this->responseDataJson(2, $requestResult['errmsg']);
                }
            }

            //记录入库
            $CustomerAppletsWeChatPsConfigObj = CustomerAppletsWeChatPsConfig::where('store_id', $store_id)->first();
            if ($CustomerAppletsWeChatPsConfigObj) {
                $updateRes = $CustomerAppletsWeChatPsConfigObj->update([
                    'open_status' => $status,
                    'open_status_desc' => $status_desc
                ]);
                if (!$updateRes) {
                    Log::info('开通即时配送(仅第三方调用)-更新入库失败');
                    Log::info($store_id);
                }
            } else {
                Log::info('开通即时配送(仅第三方调用)-未找到配置');
                Log::info($store_id);
            }

            return json_encode([
                'status' => $status,
                'message' => $status_desc,
                'data' => $requestResult
            ]);
        } catch (\Exception $e) {
            Log::info('开通即时配送(仅第三方调用)-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return $this->responseDataJson(-1);
        }
    }


    /**
     * 发起绑定帐号(仅第三方调用)
     * 第三方代商户发起绑定配送公司帐号的请求
     *
     * 使用场景:
     * 1、只能由第三方服务商调用此接口
     * 2、服务商可通过本接口代开发的小程序发起绑定配送公司帐号的操作,当调用成功,小程序管理员将收到模版消息,点击详情进入配送公司网站进行绑定操作
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function businessShopAdd(Request $request)
    {
        $store_id = $request->post('storeId', ''); //系统门店id
        $delivery_id = $request->post('deliveryId', ''); //配送公司ID

        try {
            $except_token_data = $request->except(['token']);
            $validate = Validator::make($except_token_data, [
                'storeId'=>'required|max:50',
                'deliveryId'=>'required'
            ], [
                'required' => ':attribute参数为必填项',
                'min' => ':attribute参数长度太短',
                'max' => ':attribute参数长度太长',
                'unique' => ':attribute参数已经被人占用',
                'exists' => ':attribute参数不存在',
                'integer' => ':attribute参数必须是数字',
                'required_if' => ':attribute参数满足条件时必传',
            ], [
                'storeId' => '系统门店id',
                'deliveryId' => '配送公司ID'
            ]);
            if ($validate->fails()) {
                return $this->responseDataJson(2, $validate->getMessageBag()->first());
            }

            $access_token = $this->get_access_token($store_id);
            $requestUrl = "https://api.weixin.qq.com/cgi-bin/express/local/business/shop/add?access_token=".$access_token;
            $reqData = [
                'delivery_id' => $delivery_id
            ];
            $reqData = json_encode($reqData, JSON_UNESCAPED_UNICODE);
            $requestResultObj = $this->curl_post_https($requestUrl, $reqData);
            $requestResult = json_decode($requestResultObj, true);
            if ($requestResult['errcode'] == 0 || !isset($requestResult['errcode'])) {
                if ($requestResult['resultcode'] == 0) {
                    $status = 1;
                    $status_desc = '绑定成功';
//                    return $this->responseDataJson(1, $requestResult['resultmsg'], $requestResult);
                } else {
                    $status = 0;
                    $status_desc = '未绑定';
//                    return $this->responseDataJson(2, $requestResult['resultmsg']);
                }
            } else {
                if ($requestResult['errcode'] == 930561) {
                    $status = 2;
                    $status_desc = 'delivery_id无效';
//                    return $this->responseDataJson(930561);
                } elseif ($requestResult['errcode'] == 86000) {
                    $status = 3;
                    $status_desc = '不是第三方的调用';
//                    return $this->responseDataJson(86000);
                } else {
                    $status = 0;
                    $status_desc = '未绑定';
//                    return $this->responseDataJson(2, $requestResult['errmsg']);
                }
            }

            //更新状态
            $CustomerAppletsWeChatPsConfigObj = CustomerAppletsWeChatPsConfig::where('store_id', $store_id)->first();
            if ($CustomerAppletsWeChatPsConfigObj) {
                $updateRes = $CustomerAppletsWeChatPsConfigObj->update([
                    'bind_status' => $status,
                    'bind_status_desc' => $status_desc
                ]);
                if (!$updateRes) {
                    Log::info('发起绑定帐号(仅第三方调用)-更新入库失败');
                    Log::info($store_id);
                }
            } else {
                Log::info('发起绑定帐号(仅第三方调用)-未找到配置');
                Log::info($store_id);
            }

            return json_encode([
                'status' => $status,
                'message' => $status_desc,
                'data' => $requestResult
            ]);
        } catch (\Exception $e) {
            Log::info('发起绑定帐号(仅第三方调用)-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return $this->responseDataJson(-1);
        }
    }


    /**
     * 拉取已绑定账号
     *
     * 使用场景:
     * 1、商家可通过本接口查询自己已经在小程序后台绑定的和配送公司签约的账号；
     * 2、服务商可通过本接口查询代开发的小程序在小程序后台绑定的和配送公司签约的账号,为其完成后续的接口代开发业务
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * shop_list[ [audit_result  0-审核通过;1-审核中;2-审核不通过] ]
     */
    public function businessShopGet(Request $request)
    {
        $store_id = $request->post('storeId', ''); //系统门店id

        try {
            $except_token_data = $request->except(['token']);
            $validate = Validator::make($except_token_data, [
                'storeId'=>'required|max:50'
            ], [
                'required' => ':attribute参数为必填项',
                'min' => ':attribute参数长度太短',
                'max' => ':attribute参数长度太长',
                'unique' => ':attribute参数已经被人占用',
                'exists' => ':attribute参数不存在',
                'integer' => ':attribute参数必须是数字',
                'required_if' => ':attribute参数满足条件时必传',
            ], [
                'storeId' => '系统门店id'
            ]);
            if ($validate->fails()) {
                return $this->responseDataJson(2, $validate->getMessageBag()->first());
            }

            $access_token = $this->get_access_token($store_id);
            $requestUrl = "https://api.weixin.qq.com/cgi-bin/express/local/business/shop/get?access_token=".$access_token;
            $requestResultObj = $this->curl_post_https($requestUrl);
            $requestResult = json_decode($requestResultObj, true);
            if ($requestResult['errcode'] == 0 || !isset($requestResult['errcode'])) {
                if ($requestResult['resultcode'] == 0) {
                    $shop_list = $requestResult['shop_list'];

                    return $this->responseDataJson(1, $requestResult['resultmsg'], $shop_list);
                } else {
                    return $this->responseDataJson(2, $requestResult['resultmsg']);
                }
            } else {
                return $this->responseDataJson(2, $requestResult['errmsg']);
            }
        } catch (\Exception $e) {
            Log::info('拉取已绑定账号-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return $this->responseDataJson(-1);
        }
    }


    /**
     * 获取已支持的配送公司列表
     *
     * 使用场景:
     * 查询即时配送接口已支持的配送公司和delivery_id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function businessDeliveryGetAll(Request $request)
    {
        $store_id = $request->post('storeId', ''); //系统门店id

        try {
            $except_token_data = $request->except(['token']);
            $validate = Validator::make($except_token_data, [
                'storeId'=>'required|max:50'
            ], [
                'required' => ':attribute参数为必填项',
                'min' => ':attribute参数长度太短',
                'max' => ':attribute参数长度太长',
                'unique' => ':attribute参数已经被人占用',
                'exists' => ':attribute参数不存在',
                'integer' => ':attribute参数必须是数字',
                'required_if' => ':attribute参数满足条件时必传',
            ], [
                'storeId' => '系统门店id'
            ]);
            if ($validate->fails()) {
                return $this->responseDataJson(2, $validate->getMessageBag()->first());
            }

            $access_token = $this->get_access_token($store_id);
            $requestUrl = "https://api.weixin.qq.com/cgi-bin/express/local/business/delivery/getall?access_token=".$access_token;
            $requestResultObj = $this->curl_post_https($requestUrl);
            $requestResult = json_decode($requestResultObj, true);
            if ($requestResult['errcode'] == 0 || !isset($requestResult['errcode'])) {
                if ($requestResult['resultcode'] == 0) {
                    return $this->responseDataJson(1, $requestResult['resultmsg'], $requestResult['list']);
                } else {
                    return $this->responseDataJson(2, $requestResult['resultmsg']);
                }
            } else {
                return $this->responseDataJson(2, $requestResult['errmsg']);
            }
        } catch (\Exception $e) {
            Log::info('获取已支持的配送公司列表-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return $this->responseDataJson(-1);
        }
    }


    /**
     * 预下配送单
     *
     * 使用场景:
     * 1、在用户提交外卖订单时,商家可调用本接口查询配送公司是否可接单、预计多久接单、运费预估等.预估运费可作为展示给用户的运费参考值.
     * 2、举个例子:商家通过预下配送单接口返回的预估运费是8元,商家可决定前端顾客下外卖单时展示给顾客看的运费是真实的8元,还是其他商家指定的金额.
     * 3、说明:本接口非必须调用接口,若不需要获取配送公司是否可接单、预计多久接单、运费预估等,也可不调用本接口,直接下配送单.
     * 4、顺丰同城可返回配送费用、配送距离、预计骑手接单时间,不支持返回delivery_token.
     * 5、闪送可返回配送费用、配送距离、预计骑手接单时间,不支持返回delivery_token.
     * 6、美团配送返回0时表示校验通过,不支持返回配送费用、配送距离、预计骑手接单时间和delivery_token.
     * 7、达达支持预下单查询配送费用、配送距离、预计骑手接单时间和delivery_token(有效期3分钟)
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function businessOrderPreAdd(Request $request)
    {
        $store_id = $request->post('storeId', ''); //系统门店id
        $shop_no = $request->post('shopNo', ''); //商家门店编号,在配送公司登记,美团、闪送必填
        $delivery_id = $request->post('deliveryId', ''); //配送公司ID
        $openid = $request->post('openId', ''); //下单用户的openid
        $sender_name = $request->post('senderName', ''); //发件人信息姓名,最长不超过256个字符
        $sender_city = $request->post('senderCity', ''); //发件人信息城市名称,如广州市
        $sender_address = $request->post('senderAddress', ''); //发件人信息地址(街道、小区、大厦等,用于定位)
        $sender_address_detail = $request->post('senderAddressDetail', ''); //发件人信息地址详情(楼号、单元号、层号)
        $sender_phone = $request->post('senderPhone', ''); //发件人信息电话/手机号,最长不超过64个字符
        $sender_lng = $request->post('senderLng', ''); //发件人信息经度(火星坐标或百度坐标,和coordinate_type字段配合使用,精确到小数点后6位)
        $sender_lat = $request->post('senderLat', ''); //发件人信息纬度(火星坐标或百度坐标,和coordinate_type字段配合使用,精确到小数点后6位)
        $receiver_name = $request->post('receiverName', ''); //收件人姓名,最长不超过256个字符
        $receiver_city = $request->post('receiverCity', ''); //收件人城市名称,如广州市
        $receiver_address = $request->post('receiverAddress', ''); //收件人地址(街道、小区、大厦等,用于定位)
        $receiver_address_detail = $request->post('receiverAddressDetail', ''); //收件人地址详情(楼号、单元号、层号)
        $receiver_phone = $request->post('receiverPhone', ''); //收件人电话/手机号,最长不超过64个字符
        $receiver_lng = $request->post('receiverLng', ''); //收件人经度(火星坐标或百度坐标,和 coordinate_type 字段配合使用,确到小数点后6位
        $receiver_lat = $request->post('receiverLat', ''); //收件人纬度(火星坐标或百度坐标,和 coordinate_type 字段配合使用,精确到小数点后6位)
        $goods_value = $request->post('goodsValue', ''); //货物价格,单位为元,精确到小数点后两位(如果小数点后位数多于两位,则四舍五入保留两位小数),范围为(0-5000]
        $goods_weight = $request->post('goodsWeight', ''); //货物重量,单位为kg,精确到小数点后两位(如果小数点后位数多于两位,则四舍五入保留两位小数),范围为(0-50]
        $cargo_first_class = $request->post('cargoFirstClass', ''); //品类一级类目
        $cargo_second_class = $request->post('cargoSecondClass', ''); //品类二级类目
        $wxa_path = $request->post('wxaPath', ''); //商家小程序的路径,建议为订单页面
        $img_url = $request->post('imgUrl', ''); //商品缩略图url
        $goods_name = $request->post('goodsName', ''); //商品名称
        $goods_count = $request->post('goodsCount', ''); //商品数量

        $app_key = $request->post('appKey', ''); //商家id,由配送公司分配的appkey
        $app_secret = $request->post('appSecret', '');  //用配送公司提供的appSecret加密的校验串
        $coordinate_type = $request->post('coordinateType', '0'); //0,否,发件人信息坐标类型,0-火星坐标(高德,腾讯地图均采用火星坐标);1-百度坐标
        $goods_height = $request->post('goodsHeight', ''); //否,货物高度,单位为cm,精确到小数点后两位(如果小数点后位数多于两位,则四舍五入保留两位小数),范围为(0-45]
        $goods_length = $request->post('goodsLength', ''); //否,货物长度,单位为cm,精确到小数点后两位(如果小数点后位数多于两位,则四舍五入保留两位小数),范围为(0-65]
        $goods_width = $request->post('goodsWidth', ''); //否,货物宽度,单位为cm,精确到小数点后两位(如果小数点后位数多于两位,则四舍五入保留两位小数),范围为(0-50]
        $goods_detail_arr = $request->post('goodsDetailArr', []); //否,货物详情,最长不超过10240个字符,多个['good_count'=>$good_count,//货物数量
        //                                                                                               'good_name'=>$good_name,//货品名称
        //                                                                                               'good_price'=>$good_price,//否,货品单价,精确到小数点后两位(如果小数点后位数多于两位,则四舍五入保留两位小数)
        //                                                                                               'good_unit'=>$good_unit,//否,货品单位,最长不超过20个字符]   用,连接
        $goods_pickup_info = $request->post('goodsPickupInfo', ''); //否,货物取货信息,用于骑手到店取货,最长不超过100个字符
        $goods_delivery_info = $request->post('goodsDeliveryInfo', ''); //否,货物交付信息,最长不超过100个字符
        $delivery_service_code = $request->post('deliveryServiceCode', ''); //否,配送服务代码,不同配送公司自定义,顺丰和达达不填
        $order_type = $request->post('orderType', '0'); //0,否,订单类型:0-即时单;1-预约单,如预约单,需要设置expected_delivery_time或expected_finish_time或expected_pick_time
        $expected_delivery_time = $request->post('expectedDeliveryTime', '0'); //0,否,期望派单时间(美团、达达支持,美团表示商家发单时间,达达表示系统调度时间,到那个时间才会有状态更新的回调通知),unix-timestamp,比如1586342180
        $expected_finish_time = $request->post('expectedFinishTime', '0'); //0,否,期望送达时间(顺丰同城急送支持),unix-timestamp,比如1586342180
        $expected_pick_time = $request->post('expectedPickTime', '0'); //0,否,期望取件时间(闪送、顺丰同城急送支持,闪送需要设置两个小时后的时间,顺丰同城急送只需传expected_finish_time或expected_pick_time其中之一即可,同时都传则以expected_finish_time为准),unix-timestamp,比如1586342180
        $poi_seq = $request->post('poiSeq', ''); //否,门店订单流水号,建议提供,方便骑手门店取货,最长不超过32个字符
        $note = $request->post('note', ''); //否,备注,最长不超过200个字符
        $order_time = $request->post('orderTime', ''); //否,用户下单付款时间,比如1555220757
        $is_insured = $request->post('isInsured', '0'); //0,否,是否保价:0-非保价;1-保价
        $declared_value = $request->post('declaredValue', ''); //否,保价金额,单位为元,精确到分
        $tips = $request->post('tips', ''); //否,小费,单位为元,下单一般不加小费
        $is_direct_delivery = $request->post('isDirectDelivery', '0'); //否,是否选择直拿直送(0-不需要；1-需要.选择直拿直送后,同一时间骑手只能配送此订单至完成,配送费用也相应高一些,闪送必须选1,达达可选0或1,其余配送公司不支持直拿直送)
        $cash_on_delivery = $request->post('cashOnDelivery', ''); //否,骑手应付金额,单位为元,精确到分
        $cash_on_pickup = $request->post('cashOnPickup', ''); //否,骑手应收金额,单位为元,精确到分
        $rider_pick_method = $request->post('riderPickMethod', ''); //否,物流流向,1:从门店取件送至用户；2:从用户取件送至门店
        $is_finish_code_needed = $request->post('isFinishCodeNeeded', '0'); //否,收货码(0-不需要；1-需要.收货码的作用是:骑手必须输入收货码才能完成订单妥投)
        $is_pickup_code_needed = $request->post('isPickupCodeNeeded', '0'); //否,取货码(0-不需要；1-需要.取货码的作用是:骑手必须输入取货码才能从商家取货)
        $wxa_appid = $request->post('wxaAppid', ''); //否,若结算方式为:第三方向配送公司统一结算,商户后续和第三方结算,则该参数必填；在该结算模式下,第三方用自己的开发小程序替授权商户发起下单,并将授权小程序的appid给平台,后续配送通知中可回流授权商户小程序
        $sub_biz_id = $request->post('subBizId', ''); //否,子商户id,区分小程序内部多个子商户

        try {
            $except_token_data = $request->except(['token']);
            $validate = Validator::make($except_token_data, [
                'storeId'=>'required|max:50',
                'shopNo'=>'required',
                'deliveryId'=>'required',
                'openId'=>'required',
                'senderName'=>'required|max:256',
                'senderCity'=>'required',
                'senderAddress'=>'required',
                'senderAddressDetail'=>'required',
                'senderPhone'=>'required|max:64',
                'senderLng'=>'required',
                'senderLat'=>'required',
                'receiverName'=>'required|max:64',
                'receiverCity'=>'required',
                'receiverAddress'=>'required',
                'receiverAddressDetail'=>'required',
                'receiverPhone'=>'required|max:64',
                'receiverLng'=>'required',
                'receiverLat'=>'required',
                'goodsValue'=>'required',
                'goodsWeight'=>'required',
                'cargoFirstClass'=>'required',
                'cargoSecondClass'=>'required',
                'wxaPath'=>'required',
                'imgUrl'=>'required',
                'goodsName'=>'required',
                'goodsCount'=>'required',
            ], [
                'required' => ':attribute参数为必填项',
                'min' => ':attribute参数长度太短',
                'max' => ':attribute参数长度太长',
                'unique' => ':attribute参数已经被人占用',
                'exists' => ':attribute参数不存在',
                'integer' => ':attribute参数必须是数字',
                'required_if' => ':attribute参数满足条件时必传',
            ], [
                'storeId' => '系统门店id',
                'shopNo' => '商家门店编号,在配送公司登记',
                'deliveryId' => '配送公司ID',
                'openId' => '下单用户的openid',
                'senderName' => '发件人信息姓名',
                'senderCity' => '发件人信息城市名称',
                'senderAddress' => '发件人信息地址',
                'senderAddressDetail' => '发件人信息地址详情',
                'senderPhone' => '发件人信息电话/手机号',
                'senderLng' => '发件人信息经度',
                'senderLat' => '发件人信息经度',
                'receiverName' => '收件人姓名',
                'receiverCity' => '收件人城市名称',
                'receiverAddress' => '收件人地址',
                'receiverAddressDetail' => '收件人地址详情',
                'receiverPhone' => '收件人电话/手机号',
                'receiverLng' => '收件人经度',
                'receiverLat' => '收件人纬度',
                'goodsValue' => '货物价格',
                'goodsWeight' => '货物重量',
                'cargoFirstClass' => '品类一级类目',
                'cargoSecondClass' => '品类二级类目',
                'wxaPath' => '商家小程序的路径',
                'imgUrl' => '商品缩略图地址',
                'goodsName' => '商品缩略图地址',
                'goodsCount' => '商品数量',
            ]);
            if ($validate->fails()) {
                return $this->responseDataJson(2, $validate->getMessageBag()->first());
            }

            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return $this->responseDataJson(2, '门店不存在或状态异常');
            }

            if (!$app_key || !$app_secret) {
                $ps_configs_obj = CustomerAppletsWeChatPsConfig::where('store_id', $store_id)->first();
                if (!$ps_configs_obj || !$ps_configs_obj->app_key || !$ps_configs_obj->app_secret) {
                    return $this->responseDataJson(2, '商户-微信小程序即时配送-未配置');
                }
                $app_key = $ps_configs_obj->app_key;
                $app_secret = $ps_configs_obj->app_secret;
            }

            $shop_order_id = $this->createOrderId();
            if (!$coordinate_type) $coordinate_type = 0;
            if ($expected_delivery_time && ($expected_delivery_time!=='0' || $expected_delivery_time!==0)) $expected_delivery_time = strtotime($expected_delivery_time);
            if ($expected_finish_time && ($expected_finish_time!=='0' || $expected_finish_time!==0)) $expected_finish_time = strtotime($expected_finish_time);
            if ($expected_pick_time && ($expected_pick_time!=='0' || $expected_pick_time!==0)) $expected_pick_time = strtotime($expected_pick_time);

            $access_token = $this->get_access_token($store_id);
            $requestUrl = "https://api.weixin.qq.com/cgi-bin/express/local/business/order/pre_add?access_token=".$access_token;
            $reqData = [
                'shopid' => $app_key,
                'shop_order_id' => $shop_order_id, //唯一标识订单的ID,由商户生成,不超过128字节
                'shop_no' => $shop_no,
                'delivery_sign' => $app_secret,
                'delivery_id' => $delivery_id,
                'openid' => $openid,
                'sender' => [ //发件人信息,闪送、顺丰同城急送必须填写,美团配送、达达,若传了shop_no的值可不填该字段
                    'name' => $sender_name,
                    'city' => $sender_city,
                    'address' => $sender_address,
                    'address_detail' => $sender_address_detail,
                    'phone' => $sender_phone,
                    'lng' => $sender_lng,
                    'lat' => $sender_lat,
                    'coordinate_type' => $coordinate_type
                ],
                'receiver' => [ //收件人信息
                    'name' => $receiver_name,
                    'city' => $receiver_city,
                    'address' => $receiver_address,
                    'address_detail' => $receiver_address_detail,
                    'phone' => $receiver_phone,
                    'lng' => $receiver_lng,
                    'lat' => $receiver_lat,
                    'coordinate_type' => $coordinate_type
                ],
                'cargo' => [ //货物信息
                    'goods_value' => $goods_value,
                    'goods_weight' => $goods_weight,
                    'cargo_first_class' => $cargo_first_class,
                    'cargo_second_class' => $cargo_second_class
                ],
                'shop' => [ //商品信息,会展示到物流通知消息中
                    'wxa_path' => $wxa_path,
                    'img_url' => $img_url,
                    'goods_name' => $goods_name,
                    'goods_count' => $goods_count
                ]
            ];
            if ($goods_height) $reqData['cargo']['goods_height'] = $goods_height;
            if ($goods_length) $reqData['cargo']['goods_length'] = $goods_length;
            if ($goods_width) $reqData['cargo']['goods_width'] = $goods_width;
            if ($goods_pickup_info) $reqData['cargo']['goods_pickup_info'] = $goods_pickup_info;
            if ($goods_delivery_info) $reqData['cargo']['goods_delivery_info'] = $goods_delivery_info;
            if ($delivery_service_code) $reqData['order_info']['delivery_service_code'] = $delivery_service_code;
            if ($order_type || $order_type===0 || $order_type==='0') $reqData['order_info']['order_type'] = $order_type;
            if ($expected_delivery_time || $expected_delivery_time===0 || $expected_delivery_time==='0') $reqData['order_info']['expected_delivery_time'] = $expected_delivery_time;
            if ($expected_finish_time || $expected_finish_time===0 || $expected_finish_time==='0') $reqData['order_info']['expected_finish_time'] = $expected_finish_time;
            if ($expected_pick_time || $expected_pick_time===0 || $expected_pick_time==='0') $reqData['order_info']['expected_pick_time'] = $expected_pick_time;
            if ($poi_seq) $reqData['order_info']['poi_seq'] = $poi_seq;
            if ($note) $reqData['order_info']['note'] = $note;
            if ($order_time) $reqData['order_info']['order_time'] = $order_time;
            if ($is_insured || $is_insured===0 || $is_insured==='0') $reqData['order_info']['is_insured'] = $is_insured;
            if ($declared_value) $reqData['order_info']['declared_value'] = $declared_value;
            if ($tips) $reqData['order_info']['tips'] = $tips;
            if ($is_direct_delivery || $is_direct_delivery===0 || $is_direct_delivery==='0') $reqData['order_info']['is_direct_delivery'] = $is_direct_delivery;
            if ($cash_on_delivery) $reqData['order_info']['cash_on_delivery'] = $cash_on_delivery;
            if ($cash_on_pickup) $reqData['order_info']['cash_on_pickup'] = $cash_on_pickup;
            if ($rider_pick_method) $reqData['order_info']['rider_pick_method'] = $rider_pick_method;
            if ($is_finish_code_needed || $is_finish_code_needed===0 || $is_finish_code_needed==='0') $reqData['order_info']['is_finish_code_needed'] = $is_finish_code_needed;
            if ($is_pickup_code_needed || $is_pickup_code_needed===0 || $is_pickup_code_needed==='0') $reqData['order_info']['is_pickup_code_needed'] = $is_pickup_code_needed;
            if ($goods_detail_arr && $goods_detail_arr != []) $reqData['cargo']['goods_detail'] = [
                'goods' => [$goods_detail_arr]
            ];
            if ($wxa_appid) $reqData['shop']['wxa_appid'] = $wxa_appid;
            if ($sub_biz_id) $reqData['sub_biz_id'] = $sub_biz_id;
            $reqData = json_encode($reqData, JSON_UNESCAPED_UNICODE);
            $requestResultObj = $this->curl_post_https($requestUrl, $reqData);
            $requestResult = json_decode($requestResultObj, true);
            if ($requestResult['errcode'] == 0 || !isset($requestResult['errcode'])) {
                if ($requestResult['resultcode'] == 0) {
                    return $this->responseDataJson(1, $requestResult['resultmsg'], $requestResult);
                } else {
                    return $this->responseDataJson(2, $requestResult['resultmsg']);
                }
            } else {
                return $this->responseDataJson(2, $requestResult['errmsg']);
            }
        } catch (\Exception $e) {
            Log::info('预下配送单-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return $this->responseDataJson(-1);
        }
    }


    /**
     * 下配送单
     *
     * 使用场景:
     * 1、商家可调用本接口向配送公司请求下配送单,配送公司会返回这一单的配送单号、配送费、预计骑手接单时间等信息。
     * 2、如遇下单错误,请先确认一下编码方式,建议json_encode($arr, JSON_UNESCAPED_UNICODE) 可预约时间:达达72小时内,闪送2小时以后,48小时以内
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function businessOrderAdd(Request $request)
    {
        $store_id = $request->post('storeId', ''); //系统门店id
        $shop_no = $request->post('shopNo', ''); //商家门店编号,在配送公司登记,美团、闪送必填
        $delivery_id = $request->post('deliveryId', ''); //配送公司ID
        $openid = $request->post('openId', ''); //下单用户的openid
        $sender_name = $request->post('senderName', ''); //发件人信息姓名,最长不超过256个字符
        $sender_city = $request->post('senderCity', ''); //发件人信息城市名称,如广州市
        $sender_address = $request->post('senderAddress', ''); //发件人信息地址(街道、小区、大厦等,用于定位)
        $sender_address_detail = $request->post('senderAddressDetail', ''); //发件人信息地址详情(楼号、单元号、层号)
        $sender_phone = $request->post('senderPhone', ''); //发件人信息电话/手机号,最长不超过64个字符
        $sender_lng = $request->post('senderLng', ''); //发件人信息经度(火星坐标或百度坐标,和coordinate_type字段配合使用,精确到小数点后6位)
        $sender_lat = $request->post('senderLat', ''); //发件人信息纬度(火星坐标或百度坐标,和coordinate_type字段配合使用,精确到小数点后6位)
        $receiver_name = $request->post('receiverName', ''); //收件人姓名,最长不超过256个字符
        $receiver_city = $request->post('receiverCity', ''); //收件人城市名称,如广州市
        $receiver_address = $request->post('receiverAddress', ''); //收件人地址(街道、小区、大厦等,用于定位)
        $receiver_address_detail = $request->post('receiverAddressDetail', ''); //收件人地址详情(楼号、单元号、层号)
        $receiver_phone = $request->post('receiverPhone', ''); //收件人电话/手机号,最长不超过64个字符
        $receiver_lng = $request->post('receiverLng', ''); //收件人经度(火星坐标或百度坐标,和 coordinate_type 字段配合使用,确到小数点后6位
        $receiver_lat = $request->post('receiverLat', ''); //收件人纬度(火星坐标或百度坐标,和 coordinate_type 字段配合使用,精确到小数点后6位)
        $goods_value = $request->post('goodsValue', ''); //货物价格,单位为元,精确到小数点后两位(如果小数点后位数多于两位,则四舍五入保留两位小数),范围为(0-5000]
        $goods_weight = $request->post('goodsWeight', ''); //货物重量,单位为kg,精确到小数点后两位(如果小数点后位数多于两位,则四舍五入保留两位小数),范围为(0-50]
        $cargo_first_class = $request->post('cargoFirstClass', ''); //品类一级类目
        $cargo_second_class = $request->post('cargoSecondClass', ''); //品类二级类目
        $wxa_path = $request->post('wxaPath', ''); //商家小程序的路径,建议为订单页面
        $img_url = $request->post('imgUrl', ''); //商品缩略图url
        $goods_name = $request->post('goodsName', ''); //商品名称
        $goods_count = $request->post('goodsCount', ''); //商品数量

        $delivery_token = $request->post('deliveryToken', ''); //否	,预下单接口返回的参数,配送公司可保证在一段时间内运费不变
        $user_id = $request->post('userId', ''); //小程序用户id
        $out_trade_no = $request->post('outTradeNo', ''); //支付订单号
        $app_key = $request->post('appKey', ''); //商家id,由配送公司分配的appkey
        $app_secret = $request->post('appSecret', '');  //用配送公司提供的appSecret加密的校验串
        $coordinate_type = $request->post('coordinateType', '0'); //0,否,发件人信息坐标类型,0-火星坐标(高德,腾讯地图均采用火星坐标);1-百度坐标
        $goods_height = $request->post('goodsHeight', ''); //否,货物高度,单位为cm,精确到小数点后两位(如果小数点后位数多于两位,则四舍五入保留两位小数),范围为(0-45]
        $goods_length = $request->post('goodsLength', ''); //否,货物长度,单位为cm,精确到小数点后两位(如果小数点后位数多于两位,则四舍五入保留两位小数),范围为(0-65]
        $goods_width = $request->post('goodsWidth', ''); //否,货物宽度,单位为cm,精确到小数点后两位(如果小数点后位数多于两位,则四舍五入保留两位小数),范围为(0-50]
        $goods_detail_arr = $request->post('goodsDetailArr', []); //否,货物详情,最长不超过10240个字符,多个['good_count'=>$good_count,//货物数量
        //                                                                                               'good_name'=>$good_name,//货品名称
        //                                                                                               'good_price'=>$good_price,//否,货品单价,精确到小数点后两位(如果小数点后位数多于两位,则四舍五入保留两位小数)
        //                                                                                               'good_unit'=>$good_unit,//否,货品单位,最长不超过20个字符]   用,连接
        $goods_pickup_info = $request->post('goodsPickupInfo', ''); //否,货物取货信息,用于骑手到店取货,最长不超过100个字符
        $goods_delivery_info = $request->post('goodsDeliveryInfo', ''); //否,货物交付信息,最长不超过100个字符
        $delivery_service_code = $request->post('deliveryServiceCode', ''); //否,配送服务代码,不同配送公司自定义,顺丰和达达不填
        $order_type = $request->post('orderType', '0'); //0,否,订单类型:0-即时单;1-预约单,如预约单,需要设置expected_delivery_time或expected_finish_time或expected_pick_time
        $expected_delivery_time = $request->post('expectedDeliveryTime', '0'); //0,否,期望派单时间(美团、达达支持,美团表示商家发单时间,达达表示系统调度时间,到那个时间才会有状态更新的回调通知),unix-timestamp,比如1586342180
        $expected_finish_time = $request->post('expectedFinishTime', '0'); //0,否,期望送达时间(顺丰同城急送支持),unix-timestamp,比如1586342180
        $expected_pick_time = $request->post('expectedPickTime', '0'); //0,否,期望取件时间(闪送、顺丰同城急送支持,闪送需要设置两个小时后的时间,顺丰同城急送只需传expected_finish_time或expected_pick_time其中之一即可,同时都传则以expected_finish_time为准),unix-timestamp,比如1586342180
        $poi_seq = $request->post('poiSeq', ''); //否,门店订单流水号,建议提供,方便骑手门店取货,最长不超过32个字符
        $note = $request->post('note', ''); //否,备注,最长不超过200个字符
        $order_time = $request->post('orderTime', ''); //否,用户下单付款时间, 比如1555220757
        $is_insured = $request->post('isInsured', '0'); //0,否,是否保价:0-非保价;1-保价
        $declared_value = $request->post('declaredValue', ''); //否,保价金额,单位为元,精确到分
        $tips = $request->post('tips', ''); //否,小费,单位为元,下单一般不加小费
        $is_direct_delivery = $request->post('isDirectDelivery', '0'); //否,是否选择直拿直送(0-不需要；1-需要.选择直拿直送后,同一时间骑手只能配送此订单至完成,配送费用也相应高一些,闪送必须选1,达达可选0或1,其余配送公司不支持直拿直送)
        $cash_on_delivery = $request->post('cashOnDelivery', ''); //否,骑手应付金额,单位为元,精确到分
        $cash_on_pickup = $request->post('cashOnPickup', ''); //否,骑手应收金额,单位为元,精确到分
        $rider_pick_method = $request->post('riderPickMethod', ''); //否,物流流向,1:从门店取件送至用户；2:从用户取件送至门店
        $is_finish_code_needed = $request->post('isFinishCodeNeeded', '0'); //否,收货码(0-不需要；1-需要.收货码的作用是:骑手必须输入收货码才能完成订单妥投)
        $is_pickup_code_needed = $request->post('isPickupCodeNeeded', '0'); //否,取货码(0-不需要；1-需要.取货码的作用是:骑手必须输入取货码才能从商家取货)
        $wxa_appid = $request->post('wxaAppid', ''); //否,若结算方式为:第三方向配送公司统一结算,商户后续和第三方结算,则该参数必填；在该结算模式下,第三方用自己的开发小程序替授权商户发起下单,并将授权小程序的appid给平台,后续配送通知中可回流授权商户小程序
        $sub_biz_id = $request->post('subBizId', ''); //否,子商户id,区分小程序内部多个子商户

        try {
            $except_token_data = $request->except(['token']);
            $validate = Validator::make($except_token_data, [
                'storeId'=>'required|max:50',
                'shopNo'=>'required',
                'deliveryId'=>'required',
                'openId'=>'required',
                'senderName'=>'required|max:256',
                'senderCity'=>'required',
                'senderAddress'=>'required',
                'senderAddressDetail'=>'required',
                'senderPhone'=>'required|max:64',
                'senderLng'=>'required',
                'senderLat'=>'required',
                'receiverName'=>'required|max:64',
                'receiverCity'=>'required',
                'receiverAddress'=>'required',
                'receiverAddressDetail'=>'required',
                'receiverPhone'=>'required|max:64',
                'receiverLng'=>'required',
                'receiverLat'=>'required',
                'goodsValue'=>'required',
                'goodsWeight'=>'required',
                'cargoFirstClass'=>'required',
                'cargoSecondClass'=>'required',
                'wxaPath'=>'required',
                'imgUrl'=>'required',
                'goodsName'=>'required',
                'goodsCount'=>'required',
            ], [
                'required' => ':attribute参数为必填项',
                'min' => ':attribute参数长度太短',
                'max' => ':attribute参数长度太长',
                'unique' => ':attribute参数已经被人占用',
                'exists' => ':attribute参数不存在',
                'integer' => ':attribute参数必须是数字',
                'required_if' => ':attribute参数满足条件时必传',
            ], [
                'storeId' => '系统门店id',
                'shopNo' => '商家门店编号,在配送公司登记',
                'deliveryId' => '配送公司ID',
                'openId' => '下单用户的openid',
                'senderName' => '发件人信息姓名',
                'senderCity' => '发件人信息城市名称',
                'senderAddress' => '发件人信息地址',
                'senderAddressDetail' => '发件人信息地址详情',
                'senderPhone' => '发件人信息电话/手机号',
                'senderLng' => '发件人信息经度',
                'senderLat' => '发件人信息经度',
                'receiverName' => '收件人姓名',
                'receiverCity' => '收件人城市名称',
                'receiverAddress' => '收件人地址',
                'receiverAddressDetail' => '收件人地址详情',
                'receiverPhone' => '收件人电话/手机号',
                'receiverLng' => '收件人经度',
                'receiverLat' => '收件人纬度',
                'goodsValue' => '货物价格',
                'goodsWeight' => '货物重量',
                'cargoFirstClass' => '品类一级类目',
                'cargoSecondClass' => '品类二级类目',
                'wxaPath' => '商家小程序的路径',
                'imgUrl' => '商品缩略图地址',
                'goodsName' => '商品缩略图地址',
                'goodsCount' => '商品数量',
            ]);
            if ($validate->fails()) {
                return $this->responseDataJson(2, $validate->getMessageBag()->first());
            }

            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return $this->responseDataJson(2, '门店不存在或状态异常');
            }

            if (!$app_key || !$app_secret) {
                $ps_configs_obj = CustomerAppletsWeChatPsConfig::where('store_id', $store_id)->first();
                if (!$ps_configs_obj || !$ps_configs_obj->app_key || !$ps_configs_obj->app_secret) {
                    return $this->responseDataJson(2, '商户-微信小程序即时配送-未配置');
                }
                $app_key = $ps_configs_obj->app_key;
                $app_secret = $ps_configs_obj->app_secret;
            }

            $shop_order_id = $this->createOrderId();
            if (!$coordinate_type) $coordinate_type = 0;
            if ($expected_delivery_time && ($expected_delivery_time!=='0' || $expected_delivery_time!==0)) $expected_delivery_time = strtotime($expected_delivery_time);
            if ($expected_finish_time && ($expected_finish_time!=='0' || $expected_finish_time!==0)) $expected_finish_time = strtotime($expected_finish_time);
            if ($expected_pick_time && ($expected_pick_time!=='0' || $expected_pick_time!==0)) $expected_pick_time = strtotime($expected_pick_time);
            $insertData = [
                'store_id' => $store_id,
                'shop_order_id' => $shop_order_id,
                'shop_no' => $shop_no,
                'delivery_id' => $delivery_id,
                'openid' => $openid,
                'sender_name' => $sender_name,
                'sender_city' => $sender_city,
                'sender_address' => $sender_address,
                'sender_address_detail' => $sender_address_detail,
                'sender_phone' => $sender_phone,
                'sender_lng' => $sender_lng,
                'sender_lat' => $sender_lat,
                'receiver_name' => $receiver_name,
                'receiver_city' => $receiver_city,
                'receiver_address' => $receiver_address,
                'receiver_address_detail' => $receiver_address_detail,
                'receiver_phone' => $receiver_phone,
                'receiver_lng' => $receiver_lng,
                'receiver_lat' => $receiver_lat,
                'goods_value' => $goods_value,
                'goods_weight' => $goods_weight,
                'cargo_first_class' => $cargo_first_class,
                'cargo_second_class' => $cargo_second_class,
                'wxa_path' => $wxa_path,
                'img_url' => $img_url,
                'goods_name' => $goods_name,
                'goods_count' => $goods_count
            ];
            if ($user_id) $insertData['user_id'] = $user_id;
            if ($out_trade_no) $insertData['out_trade_no'] = $out_trade_no;
            if ($coordinate_type || $coordinate_type===0 || $coordinate_type ==='0') $insertData['coordinate_type'] = $coordinate_type;
            if ($goods_height) $insertData['goods_height'] = $goods_height;
            if ($goods_length) $insertData['goods_length'] = $goods_length;
            if ($goods_width) $insertData['goods_width'] = $goods_width;
            if ($goods_detail_arr) $insertData['goods_detail_arr'] = $goods_detail_arr;
            if ($goods_pickup_info) $insertData['goods_pickup_info'] = $goods_pickup_info;
            if ($goods_delivery_info) $insertData['goods_delivery_info'] = $goods_delivery_info;
            if ($delivery_service_code) $insertData['delivery_service_code'] = $delivery_service_code;
            if ($order_type) $insertData['order_type'] = $order_type;
            if ($expected_delivery_time || $expected_delivery_time===0 || $expected_delivery_time==='0') $insertData['expected_delivery_time'] = $expected_delivery_time;
            if ($expected_finish_time || $expected_finish_time===0 || $expected_finish_time==='0') $insertData['expected_finish_time'] = $expected_finish_time;
            if ($expected_pick_time || $expected_pick_time===0 || $expected_pick_time==='0') $insertData['expected_pick_time'] = $expected_pick_time;
            if ($poi_seq) $insertData['poi_seq'] = $poi_seq;
            if ($note) $insertData['note'] = $note;
            if ($order_time) $insertData['order_time'] = strtotime($order_time);
            if ($is_insured || $is_insured===0 || $is_insured==='0') $insertData['is_insured'] = $is_insured;
            if ($declared_value) $insertData['declared_value'] = $declared_value;
            if ($tips) $insertData['tips'] = $tips;
            if ($is_direct_delivery || $is_direct_delivery===0 || $is_direct_delivery==='0') $insertData['is_direct_delivery'] = $is_direct_delivery;
            if ($cash_on_delivery) $insertData['cash_on_delivery'] = $cash_on_delivery;
            if ($cash_on_pickup) $insertData['cash_on_pickup'] = $cash_on_pickup;
            if ($rider_pick_method) $insertData['rider_pick_method'] = $rider_pick_method;
            if ($is_finish_code_needed || $is_finish_code_needed===0 || $is_finish_code_needed==='0') $insertData['is_finish_code_needed'] = $is_finish_code_needed;
            if ($is_pickup_code_needed || $is_pickup_code_needed===0 || $is_pickup_code_needed==='0') $insertData['is_pickup_code_needed'] = $is_pickup_code_needed;
            if ($wxa_appid) $insertData['wxa_appid'] = $wxa_appid;
            if ($sub_biz_id) $insertData['sub_biz_id'] = $sub_biz_id;
            $insertRes = CustomerAppletsWeChatOrder::create($insertData);
            if (!$insertRes) {
                Log::info('微信即时配送-下配送单-入库失败: ');
                Log::info($store_id);
                return $this->responseDataJson(2, '微信即时配送-下配送单-入库失败');
            }

            $access_token = $this->get_access_token($store_id);
            $requestUrl = "https://api.weixin.qq.com/cgi-bin/express/local/business/order/add?access_token=".$access_token;
            $reqData = [
                'shopid' => $app_key,
                'shop_order_id' => $shop_order_id, //唯一标识订单的ID,由商户生成,不超过128字节
                'shop_no' => $shop_no,
                'delivery_sign' => $app_secret,
                'delivery_id' => $delivery_id,
                'openid' => $openid,
                'sender' => [ //发件人信息,闪送、顺丰同城急送必须填写,美团配送、达达,若传了shop_no的值可不填该字段
                    'name' => $sender_name,
                    'city' => $sender_city,
                    'address' => $sender_address,
                    'address_detail' => $sender_address_detail,
                    'phone' => $sender_phone,
                    'lng' => $sender_lng,
                    'lat' => $sender_lat,
                    'coordinate_type' => $coordinate_type
                ],
                'receiver' => [ //收件人信息
                    'name' => $receiver_name,
                    'city' => $receiver_city,
                    'address' => $receiver_address,
                    'address_detail' => $receiver_address_detail,
                    'phone' => $receiver_phone,
                    'lng' => $receiver_lng,
                    'lat' => $receiver_lat,
                    'coordinate_type' => $coordinate_type
                ],
                'cargo' => [ //货物信息
                    'goods_value' => $goods_value,
                    'goods_weight' => $goods_weight,
                    'cargo_first_class' => $cargo_first_class,
                    'cargo_second_class' => $cargo_second_class
                ],
                'shop' => [ //商品信息,会展示到物流通知消息中
                    'wxa_path' => $wxa_path,
                    'img_url' => $img_url,
                    'goods_name' => $goods_name,
                    'goods_count' => $goods_count
                ]
            ];
            if ($delivery_token) $reqData['delivery_token'] = $delivery_token;
            if ($goods_height) $reqData['cargo']['goods_height'] = $goods_height;
            if ($goods_length) $reqData['cargo']['goods_length'] = $goods_length;
            if ($goods_width) $reqData['cargo']['goods_width'] = $goods_width;
            if ($goods_pickup_info) $reqData['cargo']['goods_pickup_info'] = $goods_pickup_info;
            if ($goods_delivery_info) $reqData['cargo']['goods_delivery_info'] = $goods_delivery_info;
            if ($delivery_service_code) $reqData['order_info']['delivery_service_code'] = $delivery_service_code;
            if ($order_type || $order_type===0 || $order_type==='0') $reqData['order_info']['order_type'] = $order_type;
            if ($expected_delivery_time || $expected_delivery_time===0 || $expected_delivery_time==='0') $reqData['order_info']['expected_delivery_time'] = $expected_delivery_time;
            if ($expected_finish_time || $expected_finish_time===0 || $expected_finish_time==='0') $reqData['order_info']['expected_finish_time'] = $expected_finish_time;
            if ($expected_pick_time || $expected_pick_time===0 || $expected_pick_time==='0') $reqData['order_info']['expected_pick_time'] = $expected_pick_time;
            if ($poi_seq) $reqData['order_info']['poi_seq'] = $poi_seq;
            if ($note) $reqData['order_info']['note'] = $note;
            if ($order_time) $reqData['order_info']['order_time'] = strtotime($order_time);
            if ($is_insured || $is_insured===0 || $is_insured==='0') $reqData['order_info']['is_insured'] = $is_insured;
            if ($declared_value) $reqData['order_info']['declared_value'] = $declared_value;
            if ($tips) $reqData['order_info']['tips'] = $tips;
            if ($is_direct_delivery || $is_direct_delivery===0 || $is_direct_delivery==='0') $reqData['order_info']['is_direct_delivery'] = $is_direct_delivery;
            if ($cash_on_delivery) $reqData['order_info']['cash_on_delivery'] = $cash_on_delivery;
            if ($cash_on_pickup) $reqData['order_info']['cash_on_pickup'] = $cash_on_pickup;
            if ($rider_pick_method) $reqData['order_info']['rider_pick_method'] = $rider_pick_method;
            if ($is_finish_code_needed || $is_finish_code_needed===0 || $is_finish_code_needed==='0') $reqData['order_info']['is_finish_code_needed'] = $is_finish_code_needed;
            if ($is_pickup_code_needed || $is_pickup_code_needed===0 || $is_pickup_code_needed==='0') $reqData['order_info']['is_pickup_code_needed'] = $is_pickup_code_needed;
            if ($goods_detail_arr && $goods_detail_arr != []) $reqData['cargo']['goods_detail'] = [
                'goods' => [$goods_detail_arr]
            ];
            if ($wxa_appid) $reqData['shop']['wxa_appid'] = $wxa_appid;
            if ($sub_biz_id) $reqData['sub_biz_id'] = $sub_biz_id;
            $reqData = json_encode($reqData, JSON_UNESCAPED_UNICODE);
//            $reqData = http_build_query($reqData);
            $requestResultObj = $this->curl_post_https($requestUrl, $reqData);
            $requestResult = json_decode($requestResultObj, true);
            if ($requestResult['errcode'] == 0 || !isset($requestResult['errcode'])) {
                if ($requestResult['resultcode'] == 0) {
                    //更新表状态
                    $updateObj = CustomerAppletsWeChatOrder::find($insertRes);
                    $updateData = [
                        'status' => $requestResult['order_status'],
                        'status_desc' => $requestResult['resultmsg']
                    ];
                    if (isset($requestResult['fee'])) $updateData['fee'] = $requestResult['fee'];
                    if (isset($requestResult['deliverfee'])) $updateData['deliverfee'] = $requestResult['deliverfee'];
                    if (isset($requestResult['couponfee'])) $updateData['couponfee'] = $requestResult['couponfee'];
                    if (isset($requestResult['tips'])) $updateData['tips'] = $requestResult['tips'];
                    if (isset($requestResult['insurancefee'])) $updateData['insurancefee'] = $requestResult['insurancefee'];
                    if (isset($requestResult['distance'])) $updateData['distance'] = $requestResult['distance'];
                    if (isset($requestResult['waybill_id'])) $updateData['waybill_id'] = $requestResult['waybill_id']; //配送单号
                    if (isset($requestResult['order_status'])) $updateData['order_status'] = $requestResult['order_status']; //配送状态
                    if (isset($requestResult['finish_code'])) $updateData['finish_code'] = $requestResult['finish_code']; //收货码
                    if (isset($requestResult['pickup_code'])) $updateData['pickup_code'] = $requestResult['pickup_code']; //取货码
                    if (isset($requestResult['dispatch_duration'])) $updateData['dispatch_duration'] = $requestResult['dispatch_duration'];
                    $updateRes = $updateObj->update($updateData);
                    if (!$updateRes) {
                        Log::info('微信即时配送-下配送单-入库更新失败: ');
                        Log::info($requestResult);
                    }

                    return $this->responseDataJson(1, $requestResult['resultmsg'], $requestResult);
                } else {
                    return $this->responseDataJson(2, $requestResult['resultmsg']);
                }
            } else {
                return $this->responseDataJson(2, $requestResult['errmsg']);
            }
        } catch (\Exception $e) {
            Log::info('下配送单-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return $this->responseDataJson(-1);
        }
    }


    /**
     * 重新下单
     *
     * 使用场景:
     * 在调用下配送单接口、订单被取消、过期或者投递异常的情况下,可调用此接口,重新下单,需要保持orderid一致
     * 备注:美团不支持重新下单接口,如果订单被取消商家需要重新下单,请修改orderid之后,调用下配送单接口下单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function businessOrderReAdd(Request $request)
    {
        $store_id = $request->post('storeId', ''); //系统门店id
        $shop_no = $request->post('shopNo', ''); //商家门店编号,在配送公司登记,美团、闪送必填
        $delivery_id = $request->post('deliveryId', ''); //配送公司ID
        $openid = $request->post('openId', ''); //下单用户的openid
        $sender_name = $request->post('senderName', ''); //发件人信息姓名,最长不超过256个字符
        $sender_city = $request->post('senderCity', ''); //发件人信息城市名称,如广州市
        $sender_address = $request->post('senderAddress', ''); //发件人信息地址(街道、小区、大厦等,用于定位)
        $sender_address_detail = $request->post('senderAddressDetail', ''); //发件人信息地址详情(楼号、单元号、层号)
        $sender_phone = $request->post('senderPhone', ''); //发件人信息电话/手机号,最长不超过64个字符
        $sender_lng = $request->post('senderLng', ''); //发件人信息经度(火星坐标或百度坐标,和coordinate_type字段配合使用,精确到小数点后6位)
        $sender_lat = $request->post('senderLat', ''); //发件人信息纬度(火星坐标或百度坐标,和coordinate_type字段配合使用,精确到小数点后6位)
        $receiver_name = $request->post('receiverName', ''); //收件人姓名,最长不超过256个字符
        $receiver_city = $request->post('receiverCity', ''); //收件人城市名称,如广州市
        $receiver_address = $request->post('receiverAddress', ''); //收件人地址(街道、小区、大厦等,用于定位)
        $receiver_address_detail = $request->post('receiverAddressDetail', ''); //收件人地址详情(楼号、单元号、层号)
        $receiver_phone = $request->post('receiverPhone', ''); //收件人电话/手机号,最长不超过64个字符
        $receiver_lng = $request->post('receiverLng', ''); //收件人经度(火星坐标或百度坐标,和 coordinate_type 字段配合使用,确到小数点后6位
        $receiver_lat = $request->post('receiverLat', ''); //收件人纬度(火星坐标或百度坐标,和 coordinate_type 字段配合使用,精确到小数点后6位)
        $goods_value = $request->post('goodsValue', ''); //货物价格,单位为元,精确到小数点后两位(如果小数点后位数多于两位,则四舍五入保留两位小数),范围为(0-5000]
        $goods_weight = $request->post('goodsWeight', ''); //货物重量,单位为kg,精确到小数点后两位(如果小数点后位数多于两位,则四舍五入保留两位小数),范围为(0-50]
        $cargo_first_class = $request->post('cargoFirstClass', ''); //品类一级类目
        $cargo_second_class = $request->post('cargoSecondClass', ''); //品类二级类目
        $wxa_path = $request->post('wxaPath', ''); //商家小程序的路径,建议为订单页面
        $img_url = $request->post('imgUrl', ''); //商品缩略图url
        $goods_name = $request->post('goodsName', ''); //商品名称
        $goods_count = $request->post('goodsCount', ''); //商品数量

        $shop_order_id = $request->post('shopOrderId', ''); //重新下单,需要保持orderid一致,美团不支持重新下单接口,如果订单被取消商家需要重新下单,请修改orderid之后,调用下配送单接口下单
        $delivery_token = $request->post('deliveryToken', ''); //否	,预下单接口返回的参数,配送公司可保证在一段时间内运费不变
        $user_id = $request->post('userId', ''); //小程序用户id
        $out_trade_no = $request->post('outTradeNo', ''); //支付订单号
        $app_key = $request->post('appKey', ''); //商家id,由配送公司分配的appkey
        $app_secret = $request->post('appSecret', '');  //用配送公司提供的appSecret加密的校验串
        $coordinate_type = $request->post('coordinateType', '0'); //0,否,发件人信息坐标类型,0-火星坐标(高德,腾讯地图均采用火星坐标);1-百度坐标
        $goods_height = $request->post('goodsHeight', ''); //否,货物高度,单位为cm,精确到小数点后两位(如果小数点后位数多于两位,则四舍五入保留两位小数),范围为(0-45]
        $goods_length = $request->post('goodsLength', ''); //否,货物长度,单位为cm,精确到小数点后两位(如果小数点后位数多于两位,则四舍五入保留两位小数),范围为(0-65]
        $goods_width = $request->post('goodsWidth', ''); //否,货物宽度,单位为cm,精确到小数点后两位(如果小数点后位数多于两位,则四舍五入保留两位小数),范围为(0-50]
        $goods_detail_arr = $request->post('goodsDetailArr', []); //否,货物详情,最长不超过10240个字符,多个['good_count'=>$good_count,//货物数量
        //                                                                                               'good_name'=>$good_name,//货品名称
        //                                                                                               'good_price'=>$good_price,//否,货品单价,精确到小数点后两位(如果小数点后位数多于两位,则四舍五入保留两位小数)
        //                                                                                               'good_unit'=>$good_unit,//否,货品单位,最长不超过20个字符]   用,连接
        $goods_pickup_info = $request->post('goodsPickupInfo', ''); //否,货物取货信息,用于骑手到店取货,最长不超过100个字符
        $goods_delivery_info = $request->post('goodsDeliveryInfo', ''); //否,货物交付信息,最长不超过100个字符
        $delivery_service_code = $request->post('deliveryServiceCode', ''); //否,配送服务代码,不同配送公司自定义,顺丰和达达不填
        $order_type = $request->post('orderType', '0'); //0,否,订单类型:0-即时单;1-预约单,如预约单,需要设置expected_delivery_time或expected_finish_time或expected_pick_time
        $expected_delivery_time = $request->post('expectedDeliveryTime', '0'); //0,否,期望派单时间(美团、达达支持,美团表示商家发单时间,达达表示系统调度时间,到那个时间才会有状态更新的回调通知),unix-timestamp,比如1586342180
        $expected_finish_time = $request->post('expectedFinishTime', '0'); //0,否,期望送达时间(顺丰同城急送支持),unix-timestamp,比如1586342180
        $expected_pick_time = $request->post('expectedPickTime', '0'); //0,否,期望取件时间(闪送、顺丰同城急送支持,闪送需要设置两个小时后的时间,顺丰同城急送只需传expected_finish_time或expected_pick_time其中之一即可,同时都传则以expected_finish_time为准),unix-timestamp,比如1586342180
        $poi_seq = $request->post('poiSeq', ''); //否,门店订单流水号,建议提供,方便骑手门店取货,最长不超过32个字符
        $note = $request->post('note', ''); //否,备注,最长不超过200个字符
        $order_time = $request->post('orderTime', ''); //否,用户下单付款时间, 比如1555220757
        $is_insured = $request->post('isInsured', '0'); //0,否,是否保价:0-非保价;1-保价
        $declared_value = $request->post('declaredValue', ''); //否,保价金额,单位为元,精确到分
        $tips = $request->post('tips', ''); //否,小费,单位为元,下单一般不加小费
        $is_direct_delivery = $request->post('isDirectDelivery', '0'); //否,是否选择直拿直送(0-不需要；1-需要.选择直拿直送后,同一时间骑手只能配送此订单至完成,配送费用也相应高一些,闪送必须选1,达达可选0或1,其余配送公司不支持直拿直送)
        $cash_on_delivery = $request->post('cashOnDelivery', ''); //否,骑手应付金额,单位为元,精确到分
        $cash_on_pickup = $request->post('cashOnPickup', ''); //否,骑手应收金额,单位为元,精确到分
        $rider_pick_method = $request->post('riderPickMethod', ''); //否,物流流向,1:从门店取件送至用户；2:从用户取件送至门店
        $is_finish_code_needed = $request->post('isFinishCodeNeeded', '0'); //否,收货码(0-不需要；1-需要.收货码的作用是:骑手必须输入收货码才能完成订单妥投)
        $is_pickup_code_needed = $request->post('isPickupCodeNeeded', '0'); //否,取货码(0-不需要；1-需要.取货码的作用是:骑手必须输入取货码才能从商家取货)
        $wxa_appid = $request->post('wxaAppid', ''); //否,若结算方式为:第三方向配送公司统一结算,商户后续和第三方结算,则该参数必填；在该结算模式下,第三方用自己的开发小程序替授权商户发起下单,并将授权小程序的appid给平台,后续配送通知中可回流授权商户小程序
        $sub_biz_id = $request->post('subBizId', ''); //否,子商户id,区分小程序内部多个子商户

        try {
            $except_token_data = $request->except(['token']);
            $validate = Validator::make($except_token_data, [
                'storeId'=>'required|max:50',
                'shopNo'=>'required',
                'deliveryId'=>'required',
                'openId'=>'required',
                'senderName'=>'required|max:256',
                'senderCity'=>'required',
                'senderAddress'=>'required',
                'senderAddressDetail'=>'required',
                'senderPhone'=>'required|max:64',
                'senderLng'=>'required',
                'senderLat'=>'required',
                'receiverName'=>'required|max:64',
                'receiverCity'=>'required',
                'receiverAddress'=>'required',
                'receiverAddressDetail'=>'required',
                'receiverPhone'=>'required|max:64',
                'receiverLng'=>'required',
                'receiverLat'=>'required',
                'goodsValue'=>'required',
                'goodsWeight'=>'required',
                'cargoFirstClass'=>'required',
                'cargoSecondClass'=>'required',
                'wxaPath'=>'required',
                'imgUrl'=>'required',
                'goodsName'=>'required',
                'goodsCount'=>'required',
            ], [
                'required' => ':attribute参数为必填项',
                'min' => ':attribute参数长度太短',
                'max' => ':attribute参数长度太长',
                'unique' => ':attribute参数已经被人占用',
                'exists' => ':attribute参数不存在',
                'integer' => ':attribute参数必须是数字',
                'required_if' => ':attribute参数满足条件时必传',
            ], [
                'storeId' => '系统门店id',
                'shopNo' => '商家门店编号,在配送公司登记',
                'deliveryId' => '配送公司ID',
                'openId' => '下单用户的openid',
                'senderName' => '发件人信息姓名',
                'senderCity' => '发件人信息城市名称',
                'senderAddress' => '发件人信息地址',
                'senderAddressDetail' => '发件人信息地址详情',
                'senderPhone' => '发件人信息电话/手机号',
                'senderLng' => '发件人信息经度',
                'senderLat' => '发件人信息经度',
                'receiverName' => '收件人姓名',
                'receiverCity' => '收件人城市名称',
                'receiverAddress' => '收件人地址',
                'receiverAddressDetail' => '收件人地址详情',
                'receiverPhone' => '收件人电话/手机号',
                'receiverLng' => '收件人经度',
                'receiverLat' => '收件人纬度',
                'goodsValue' => '货物价格',
                'goodsWeight' => '货物重量',
                'cargoFirstClass' => '品类一级类目',
                'cargoSecondClass' => '品类二级类目',
                'wxaPath' => '商家小程序的路径',
                'imgUrl' => '商品缩略图地址',
                'goodsName' => '商品缩略图地址',
                'goodsCount' => '商品数量',
            ]);
            if ($validate->fails()) {
                return $this->responseDataJson(2, $validate->getMessageBag()->first());
            }

            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return $this->responseDataJson(2, '门店不存在或状态异常');
            }

            if (!$app_key || !$app_secret) {
                $ps_configs_obj = CustomerAppletsWeChatPsConfig::where('store_id', $store_id)->first();
                if (!$ps_configs_obj || !$ps_configs_obj->app_key || !$ps_configs_obj->app_secret) {
                    return $this->responseDataJson(2, '商户-微信小程序即时配送-未配置');
                }
                $app_key = $ps_configs_obj->app_key;
                $app_secret = $ps_configs_obj->app_secret;
            }
            if (!$shop_order_id) $shop_order_id = $this->createOrderId();
            if (!$coordinate_type) $coordinate_type = 0;
            if ($expected_delivery_time && ($expected_delivery_time!=='0' || $expected_delivery_time!==0)) $expected_delivery_time = strtotime($expected_delivery_time);
            if ($expected_finish_time && ($expected_finish_time!=='0' || $expected_finish_time!==0)) $expected_finish_time = strtotime($expected_finish_time);
            if ($expected_pick_time && ($expected_pick_time!=='0' || $expected_pick_time!==0)) $expected_pick_time = strtotime($expected_pick_time);
            $insertData = [
                'store_id' => $store_id,
                'shop_order_id' => $shop_order_id,
                'shop_no' => $shop_no,
                'delivery_id' => $delivery_id,
                'openid' => $openid,
                'sender_name' => $sender_name,
                'sender_city' => $sender_city,
                'sender_address' => $sender_address,
                'sender_address_detail' => $sender_address_detail,
                'sender_phone' => $sender_phone,
                'sender_lng' => $sender_lng,
                'sender_lat' => $sender_lat,
                'receiver_name' => $receiver_name,
                'receiver_city' => $receiver_city,
                'receiver_address' => $receiver_address,
                'receiver_address_detail' => $receiver_address_detail,
                'receiver_phone' => $receiver_phone,
                'receiver_lng' => $receiver_lng,
                'receiver_lat' => $receiver_lat,
                'goods_value' => $goods_value,
                'goods_weight' => $goods_weight,
                'cargo_first_class' => $cargo_first_class,
                'cargo_second_class' => $cargo_second_class,
                'wxa_path' => $wxa_path,
                'img_url' => $img_url,
                'goods_name' => $goods_name,
                'goods_count' => $goods_count
            ];
            if ($user_id) $insertData['user_id'] = $user_id;
            if ($out_trade_no) $insertData['out_trade_no'] = $out_trade_no;
            if ($coordinate_type || $coordinate_type===0 || $coordinate_type ==='0') $insertData['coordinate_type'] = $coordinate_type;
            if ($goods_height) $insertData['goods_height'] = $goods_height;
            if ($goods_length) $insertData['goods_length'] = $goods_length;
            if ($goods_width) $insertData['goods_width'] = $goods_width;
            if ($goods_detail_arr) $insertData['goods_detail_arr'] = $goods_detail_arr;
            if ($goods_pickup_info) $insertData['goods_pickup_info'] = $goods_pickup_info;
            if ($goods_delivery_info) $insertData['goods_delivery_info'] = $goods_delivery_info;
            if ($delivery_service_code) $insertData['delivery_service_code'] = $delivery_service_code;
            if ($order_type) $insertData['order_type'] = $order_type;
            if ($expected_delivery_time || $expected_delivery_time===0 || $expected_delivery_time==='0') $insertData['expected_delivery_time'] = $expected_delivery_time;
            if ($expected_finish_time || $expected_finish_time===0 || $expected_finish_time==='0') $insertData['expected_finish_time'] = $expected_finish_time;
            if ($expected_pick_time || $expected_pick_time===0 || $expected_pick_time==='0') $insertData['expected_pick_time'] = $expected_pick_time;
            if ($poi_seq) $insertData['poi_seq'] = $poi_seq;
            if ($note) $insertData['note'] = $note;
            if ($order_time) $insertData['order_time'] = strtotime($order_time);
            if ($is_insured || $is_insured===0 || $is_insured==='0') $insertData['is_insured'] = $is_insured;
            if ($declared_value) $insertData['declared_value'] = $declared_value;
            if ($tips) $insertData['tips'] = $tips;
            if ($is_direct_delivery || $is_direct_delivery===0 || $is_direct_delivery==='0') $insertData['is_direct_delivery'] = $is_direct_delivery;
            if ($cash_on_delivery) $insertData['cash_on_delivery'] = $cash_on_delivery;
            if ($cash_on_pickup) $insertData['cash_on_pickup'] = $cash_on_pickup;
            if ($rider_pick_method) $insertData['rider_pick_method'] = $rider_pick_method;
            if ($is_finish_code_needed || $is_finish_code_needed===0 || $is_finish_code_needed==='0') $insertData['is_finish_code_needed'] = $is_finish_code_needed;
            if ($is_pickup_code_needed || $is_pickup_code_needed===0 || $is_pickup_code_needed==='0') $insertData['is_pickup_code_needed'] = $is_pickup_code_needed;
            if ($wxa_appid) $insertData['wxa_appid'] = $wxa_appid;
            if ($sub_biz_id) $insertData['sub_biz_id'] = $sub_biz_id;
            $is_exist_obj = CustomerAppletsWeChatOrder::where('shop_order_id', $shop_order_id)->first();
            if ($is_exist_obj) {
                $insertRes = $is_exist_obj->update($insertData);
            } else {
                $insertRes = CustomerAppletsWeChatOrder::create($insertData);
            }
            if (!$insertRes) {
                Log::info('微信即时配送-重新下单-入库失败: ');
                Log::info($store_id);
                return $this->responseDataJson(2, '微信即时配送-重新下单-入库失败');
            }

            $access_token = $this->get_access_token($store_id);
            $requestUrl = "https://api.weixin.qq.com/cgi-bin/express/local/business/order/readd?access_token=".$access_token;
            $reqData = [
                'shopid' => $app_key,
                'shop_order_id' => $shop_order_id, //唯一标识订单的ID,由商户生成,不超过128字节
                'shop_no' => $shop_no,
                'delivery_sign' => $app_secret,
                'delivery_id' => $delivery_id,
                'openid' => $openid,
                'sender' => [ //发件人信息,闪送、顺丰同城急送必须填写,美团配送、达达,若传了shop_no的值可不填该字段
                    'name' => $sender_name,
                    'city' => $sender_city,
                    'address' => $sender_address,
                    'address_detail' => $sender_address_detail,
                    'phone' => $sender_phone,
                    'lng' => $sender_lng,
                    'lat' => $sender_lat,
                    'coordinate_type' => $coordinate_type
                ],
                'receiver' => [ //收件人信息
                    'name' => $receiver_name,
                    'city' => $receiver_city,
                    'address' => $receiver_address,
                    'address_detail' => $receiver_address_detail,
                    'phone' => $receiver_phone,
                    'lng' => $receiver_lng,
                    'lat' => $receiver_lat,
                    'coordinate_type' => $coordinate_type
                ],
                'cargo' => [ //货物信息
                    'goods_value' => $goods_value,
                    'goods_weight' => $goods_weight,
                    'cargo_first_class' => $cargo_first_class,
                    'cargo_second_class' => $cargo_second_class
                ],
                'shop' => [ //商品信息,会展示到物流通知消息中
                    'wxa_path' => $wxa_path,
                    'img_url' => $img_url,
                    'goods_name' => $goods_name,
                    'goods_count' => $goods_count
                ]
            ];
            if ($delivery_token) $reqData['delivery_token'] = $delivery_token;
            if ($goods_height) $reqData['cargo']['goods_height'] = $goods_height;
            if ($goods_length) $reqData['cargo']['goods_length'] = $goods_length;
            if ($goods_width) $reqData['cargo']['goods_width'] = $goods_width;
            if ($goods_pickup_info) $reqData['cargo']['goods_pickup_info'] = $goods_pickup_info;
            if ($goods_delivery_info) $reqData['cargo']['goods_delivery_info'] = $goods_delivery_info;
            if ($delivery_service_code) $reqData['order_info']['delivery_service_code'] = $delivery_service_code;
            if ($order_type || $order_type===0 || $order_type==='0') $reqData['order_info']['order_type'] = $order_type;
            if ($expected_delivery_time || $expected_delivery_time===0 || $expected_delivery_time==='0') $reqData['order_info']['expected_delivery_time'] = $expected_delivery_time;
            if ($expected_finish_time || $expected_finish_time===0 || $expected_finish_time==='0') $reqData['order_info']['expected_finish_time'] = $expected_finish_time;
            if ($expected_pick_time || $expected_pick_time===0 || $expected_pick_time==='0') $reqData['order_info']['expected_pick_time'] = $expected_pick_time;
            if ($poi_seq) $reqData['order_info']['poi_seq'] = $poi_seq;
            if ($note) $reqData['order_info']['note'] = $note;
            if ($order_time) $reqData['order_info']['order_time'] = strtotime($order_time);
            if ($is_insured || $is_insured===0 || $is_insured==='0') $reqData['order_info']['is_insured'] = $is_insured;
            if ($declared_value) $reqData['order_info']['declared_value'] = $declared_value;
            if ($tips) $reqData['order_info']['tips'] = $tips;
            if ($is_direct_delivery || $is_direct_delivery===0 || $is_direct_delivery==='0') $reqData['order_info']['is_direct_delivery'] = $is_direct_delivery;
            if ($cash_on_delivery) $reqData['order_info']['cash_on_delivery'] = $cash_on_delivery;
            if ($cash_on_pickup) $reqData['order_info']['cash_on_pickup'] = $cash_on_pickup;
            if ($rider_pick_method) $reqData['order_info']['rider_pick_method'] = $rider_pick_method;
            if ($is_finish_code_needed || $is_finish_code_needed===0 || $is_finish_code_needed==='0') $reqData['order_info']['is_finish_code_needed'] = $is_finish_code_needed;
            if ($is_pickup_code_needed || $is_pickup_code_needed===0 || $is_pickup_code_needed==='0') $reqData['order_info']['is_pickup_code_needed'] = $is_pickup_code_needed;
            if ($goods_detail_arr && $goods_detail_arr != []) $reqData['cargo']['goods_detail'] = [
                'goods' => [$goods_detail_arr]
            ];
            if ($wxa_appid) $reqData['shop']['wxa_appid'] = $wxa_appid;
            if ($sub_biz_id) $reqData['sub_biz_id'] = $sub_biz_id;
            $reqData = json_encode($reqData, JSON_UNESCAPED_UNICODE);
            $requestResultObj = $this->curl_post_https($requestUrl, $reqData);
            $requestResult = json_decode($requestResultObj, true);
            if ($requestResult['errcode'] == 0 || !isset($requestResult['errcode'])) {
                if ($requestResult['resultcode'] == 0) {
                    //更新表状态
                    if ($is_exist_obj) {
                        $updateObj = $is_exist_obj;
                    } else {
                        $updateObj = CustomerAppletsWeChatOrder::find($insertRes);
                    }
                    $updateData = [
                        'status' => $requestResult['order_status'],
                        'status_desc' => $requestResult['resultmsg']
                    ];
                    if (isset($requestResult['fee'])) $updateData['fee'] = $requestResult['fee'];
                    if (isset($requestResult['deliverfee'])) $updateData['deliverfee'] = $requestResult['deliverfee'];
                    if (isset($requestResult['couponfee'])) $updateData['couponfee'] = $requestResult['couponfee'];
                    if (isset($requestResult['tips'])) $updateData['tips'] = $requestResult['tips'];
                    if (isset($requestResult['insurancefee'])) $updateData['insurancefee'] = $requestResult['insurancefee'];
                    if (isset($requestResult['distance'])) $updateData['distance'] = $requestResult['distance'];
                    if (isset($requestResult['waybill_id'])) $updateData['waybill_id'] = $requestResult['waybill_id']; //配送单号
                    if (isset($requestResult['order_status'])) $updateData['order_status'] = $requestResult['order_status']; //配送状态
                    if (isset($requestResult['finish_code'])) $updateData['finish_code'] = $requestResult['finish_code']; //收货码
                    if (isset($requestResult['pickup_code'])) $updateData['pickup_code'] = $requestResult['pickup_code']; //取货码
                    if (isset($requestResult['dispatch_duration'])) $updateData['dispatch_duration'] = $requestResult['dispatch_duration'];
                    $updateRes = $updateObj->update($updateData);
                    if (!$updateRes) {
                        Log::info('微信即时配送-重新下单-入库更新失败: ');
                        Log::info($requestResult);
                    }

                    return $this->responseDataJson(1, $requestResult['resultmsg'], $requestResult);
                } else {
                    return $this->responseDataJson(2, $requestResult['resultmsg']);
                }
            } else {
                return $this->responseDataJson(2, $requestResult['errmsg']);
            }
        } catch (\Exception $e) {
            Log::info('重新下单-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return $this->responseDataJson(-1);
        }
    }


    /**
     * 增加小费
     * 可以对待接单状态的订单增加小费。需要注意:订单的小费,以最新一次加小费动作的金额为准,故下一次增加小费额必须大于上一次小费额
     *
     * 使用场景:
     * 调用本接口,可以给待接单状态的订单增加小费,各家配送公司增加消费的规则如下:
     * 配送公司	           加小费规则
     * 顺丰同城急送	     支持,规则:骑手接单前可加小费,上限10次,200元封顶
     * 闪送	             支持,规则:骑手接单前可加小费,需按固定档位加小费,档位为2、3、5、10、15、20、50、100
     * 美团配送	         不支持
     * 达达配送	         支持,规则:骑手接单前可加小费,小费金额以最新一次为准,同一单新增的小费额须大于上一次的小费额,小费不可以超过货值,上限30元
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function businessOrderAddTips(Request $request)
    {
        $store_id = $request->post('storeId', ''); //系统门店id
        $shop_no = $request->post('shopNo', ''); //商家门店编号,在配送公司登记,如果只有一个门店,闪送shop_no必填,值为店铺id
        $waybill_id = $request->post('waybillId', ''); //商家配送单id
        $openid = $request->post('openId', ''); //下单用户的openid
        $tips = $request->post('tips', ''); //小费金额(单位:元),各家配送公司最大值不同
        $remark = $request->post('remark', ''); //备注

        $app_key = $request->post('appKey', ''); //商家id,由配送公司分配的appkey
        $app_secret = $request->post('appSecret', ''); //用配送公司提供的appSecret加密的校验串

        try {
            $except_token_data = $request->except(['token']);
            $validate = Validator::make($except_token_data, [
                'storeId'=>'required|max:50',
                'shopNo'=>'required',
                'waybillId'=>'required',
                'openId'=>'required',
                'tips'=>'required',
                'remark'=>'required',
            ], [
                'required' => ':attribute参数为必填项',
                'min' => ':attribute参数长度太短',
                'max' => ':attribute参数长度太长',
                'unique' => ':attribute参数已经被人占用',
                'exists' => ':attribute参数不存在',
                'integer' => ':attribute参数必须是数字',
                'required_if' => ':attribute参数满足条件时必传',
            ], [
                'storeId' => '系统门店id',
                'shopNo' => '商家门店编号,在配送公司登记',
                'waybillId' => '商家配送单id',
                'openId' => '下单用户的openid',
                'tips' => '小费金额',
                'remark' => '备注',
            ]);
            if ($validate->fails()) {
                return $this->responseDataJson(2, $validate->getMessageBag()->first());
            }

            if (!$app_key || !$app_secret) {
                $ps_configs_obj = CustomerAppletsWeChatPsConfig::where('store_id', $store_id)->first();
                if (!$ps_configs_obj || !$ps_configs_obj->app_key || !$ps_configs_obj->app_secret) {
                    return $this->responseDataJson(2, '商户-微信小程序即时配送-未配置');
                }
                $app_key = $ps_configs_obj->app_key;
                $app_secret = $ps_configs_obj->app_secret;
            }
            $shop_order_id = $this->createOrderId();

            $access_token = $this->get_access_token($store_id);
            $requestUrl = "https://api.weixin.qq.com/cgi-bin/express/local/business/order/addtips?access_token=".$access_token;
            $reqData = [
                'shopid' => $app_key,
                'shop_order_id' => $shop_order_id,
                'shop_no' => $shop_no,
                'delivery_sign' => $app_secret,
                'waybill_id' => $waybill_id,
                'openid' => $openid,
                'tips' => $tips,
                'remark' => $remark
            ];
            $reqData = json_encode($reqData, JSON_UNESCAPED_UNICODE);
            $requestResultObj = $this->curl_post_https($requestUrl, $reqData);
            $requestResult = json_decode($requestResultObj, true);
            if ($requestResult['errcode'] == 0 || !isset($requestResult['errcode'])) {
                if ($requestResult['resultcode'] == 0) {
                    //TODO:根据waybill_id更新订单信息

                    return $this->responseDataJson(1, $requestResult['resultmsg'], $requestResult);
                } else {
                    return $this->responseDataJson(2, $requestResult['resultmsg']);
                }
            } else {
                return $this->responseDataJson(2, $requestResult['errmsg']);
            }
        } catch (\Exception $e) {
            Log::info('增加小费-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return $this->responseDataJson(-1);
        }
    }


    /**
     * 预取消配送单
     *
     * 使用场景:
     * 在正式取消配送单前,商家可调用本接口查询该订单是否可以取消,取消订单配送公司需要扣除的费用是多少。各家取消规则如下：
     * 配送公司	         取消规则
     * 顺丰同城急送	   配送完成前任意节点可取消配送单
     * 闪送	           配送完成前任意节点可取消配送单
     * 美团配送	       配送完成前任意节点可取消配送单
     * 达达	           骑手取货之前可取消配送单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function businessOrderPreCancel(Request $request)
    {
        $store_id = $request->post('storeId', ''); //系统门店id
        $shop_no = $request->post('shopNo', ''); //商家门店编号,在配送公司登记,闪送shop_no必填,值为店铺id
        $delivery_id = $request->post('deliveryId', ''); //快递公司ID
        $waybill_id = $request->post('waybillId', ''); //配送单id

        $app_key = $request->post('appKey', ''); //商家id,由配送公司分配的appkey
        $app_secret = $request->post('appSecret', ''); //用配送公司提供的appSecret加密的校验串
        $cancel_reason_id = $request->post('cancelReasonId', ''); //否,取消原因Id:1-暂时不需要邮寄 2-价格不合适 3-订单信息有误,重新下单 4-骑手取货不及时 5-骑手配送不及时 6-其他原因(如果选择6,需要填写取消原因,否则不需要填写)
        $cancel_reason = $request->post('cancelReason', ''); //否,取消原因

        try {
            $except_token_data = $request->except(['token']);
            $validate = Validator::make($except_token_data, [
                'storeId'=>'required',
                'shopNo'=>'required',
                'deliveryId'=>'required',
                'waybillId'=>'required',
            ], [
                'required' => ':attribute参数为必填项',
                'min' => ':attribute参数长度太短',
                'max' => ':attribute参数长度太长',
                'unique' => ':attribute参数已经被人占用',
                'exists' => ':attribute参数不存在',
                'integer' => ':attribute参数必须是数字',
                'required_if' => ':attribute参数满足条件时必传',
            ], [
                'storeId' => '系统门店id',
                'shopNo' => '商家门店编号,在配送公司登记',
                'deliveryId' => '快递公司ID',
                'waybillId' => '商家配送单id',
            ]);
            if ($validate->fails()) {
                return $this->responseDataJson(2, $validate->getMessageBag()->first());
            }

            if (!$app_key || !$app_secret) {
                $ps_configs_obj = CustomerAppletsWeChatPsConfig::where('store_id', $store_id)->first();
                if (!$ps_configs_obj || !$ps_configs_obj->app_key || !$ps_configs_obj->app_secret) {
                    return $this->responseDataJson(2, '商户-微信小程序即时配送-未配置');
                }
                $app_key = $ps_configs_obj->app_key;
                $app_secret = $ps_configs_obj->app_secret;
            }
            $shop_order_id = $this->createOrderId();

            $access_token = $this->get_access_token($store_id);
            $requestUrl = "https://api.weixin.qq.com/cgi-bin/express/local/business/order/precancel?access_token=".$access_token;
            $reqData = [
                'shopid' => $app_key,
                'shop_order_id' => $shop_order_id,
                'shop_no' => $shop_no,
                'delivery_sign' => $app_secret,
                'delivery_id' => $delivery_id,
                'waybill_id' => $waybill_id
            ];
            if ($cancel_reason_id) $reqData['cancel_reason_id'] = $cancel_reason_id;
            if ($cancel_reason) $reqData['cancel_reason'] = $cancel_reason;
            $reqData = json_encode($reqData, JSON_UNESCAPED_UNICODE);
            $requestResultObj = $this->curl_post_https($requestUrl, $reqData);
            $requestResult = json_decode($requestResultObj, true);
            if ($requestResult['errcode'] == 0 || !isset($requestResult['errcode'])) {
                if ($requestResult['resultcode'] == 0) {
                    return $this->responseDataJson(1, $requestResult['resultmsg'], $requestResult);
                } else {
                    return $this->responseDataJson(2, $requestResult['resultmsg']);
                }
            } else {
                return $this->responseDataJson(2, $requestResult['errmsg']);
            }
        } catch (\Exception $e) {
            Log::info('预取消配送单-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return $this->responseDataJson(-1);
        }
    }


    /**
     * 取消配送单
     *
     * 使用场景:
     * 调用本接口可向配送公司请求取消配送单,各家取消规则如下：
     * 配送公司	         取消规则
     * 顺丰同城急送	   配送完成前任意节点可取消配送单
     * 闪送	           配送完成前任意节点可取消配送单
     * 美团配送	       配送完成前任意节点可取消配送单
     * 达达	           骑手取货之前可取消配送单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function businessOrderCancel(Request $request)
    {
        $store_id = $request->post('storeId', ''); //系统门店id
        $shop_no = $request->post('shopNo', ''); //商家门店编号,在配送公司登记,闪送shop_no必填,值为店铺id
        $delivery_id = $request->post('deliveryId', ''); //快递公司ID
        $waybill_id = $request->post('waybillId', ''); //配送单id
        $cancel_reason_id = $request->post('cancelReasonId', ''); //取消原因Id:1-暂时不需要邮寄 2-价格不合适 3-订单信息有误,重新下单 4-骑手取货不及时 5-骑手配送不及时 6-其他原因(如果选择6,需要填写取消原因,否则不需要填写)

        $app_key = $request->post('appKey', ''); //商家id, 由配送公司分配的appkey
        $app_secret = $request->post('appSecret', ''); //用配送公司提供的appSecret加密的校验串
        $shop_order_id = $request->post('shopOrderId', ''); //唯一标识订单的ID
        $cancel_reason = $request->post('cancelReason', ''); //否,取消原因

        try {
            $except_token_data = $request->except(['token']);
            $validate = Validator::make($except_token_data, [
                'storeId'=>'required',
                'shopNo'=>'required',
                'deliveryId'=>'required',
                'waybillId'=>'required',
                'cancelReasonId'=>'required',
            ], [
                'required' => ':attribute参数为必填项',
                'min' => ':attribute参数长度太短',
                'max' => ':attribute参数长度太长',
                'unique' => ':attribute参数已经被人占用',
                'exists' => ':attribute参数不存在',
                'integer' => ':attribute参数必须是数字',
                'required_if' => ':attribute参数满足条件时必传',
            ], [
                'storeId' => '系统门店id',
                'shopNo' => '商家门店编号,在配送公司登记',
                'deliveryId' => '快递公司ID',
                'waybillId' => '商家配送单id',
                'cancelReasonId' => '取消原因Id',
            ]);
            if ($validate->fails()) {
                return $this->responseDataJson(2, $validate->getMessageBag()->first());
            }

            if (!$app_key || !$app_secret) {
                $ps_configs_obj = CustomerAppletsWeChatPsConfig::where('store_id', $store_id)->first();
                if (!$ps_configs_obj || !$ps_configs_obj->app_key || !$ps_configs_obj->app_secret) {
                    return $this->responseDataJson(2, '商户-微信小程序即时配送-未配置');
                }
                $app_key = $ps_configs_obj->app_key;
                $app_secret = $ps_configs_obj->app_secret;
            }
            if (!$shop_order_id) $shop_order_id = $this->createOrderId();

            $access_token = $this->get_access_token($store_id);
            $requestUrl = "https://api.weixin.qq.com/cgi-bin/express/local/business/order/cancel?access_token=".$access_token;
            $reqData = [
                'shopid' => $app_key,
                'shop_order_id' => $shop_order_id,
                'shop_no' => $shop_no,
                'delivery_sign' => $app_secret,
                'delivery_id' => $delivery_id,
                'cancel_reason_id' => $cancel_reason_id
            ];
            if ($waybill_id) $reqData['waybill_id'] = $waybill_id;
            if ($cancel_reason) $reqData['cancel_reason'] = $cancel_reason;
            $reqData = json_encode($reqData, JSON_UNESCAPED_UNICODE);
            $requestResultObj = $this->curl_post_https($requestUrl, $reqData);
            $requestResult = json_decode($requestResultObj, true);
            if ($requestResult['errcode'] == 0 || !isset($requestResult['errcode'])) {
                if ($requestResult['resultcode'] == 0) {
                    //取消订单入库
                    $insertData = [
                        'store_id' => $store_id,
                        'app_key' => $app_key,
                        'shop_order_id' => $shop_order_id,
                        'shop_no' => $shop_no,
                        'delivery_id' => $delivery_id,
                        'deduct_fee' => $requestResult['deduct_fee'],
                        'desc' => $requestResult['desc'],
                        'cancel_reason_id' => $cancel_reason_id
                    ];
                    if ($waybill_id) $insertData['waybill_id'] = $waybill_id;
                    if ($cancel_reason) $insertData['cancel_reason'] = $cancel_reason;
                    $insertRes = CustomerAppletsWeChatOrderCancel::create($insertData);
                    if (!$insertRes) {
                        Log::info('微信即时配送-取消配送单-入库失败：');
                        Log::info($store_id);
                    }

                    return $this->responseDataJson(1, $requestResult['resultmsg'], $requestResult);
                } else {
                    return $this->responseDataJson(2, $requestResult['resultmsg']);
                }
            } else {
                return $this->responseDataJson(2, $requestResult['errmsg']);
            }
        } catch (\Exception $e) {
            Log::info('取消配送单-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return $this->responseDataJson(-1);
        }
    }


    /**
     * 异常件退回商家商家确认收货
     *
     * 使用场景:
     * 当订单配送异常,骑手把货物退还给商家,商家收货以后调用本接口返回确认收货
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function businessOrderConfirmReturn(Request $request)
    {
        $store_id = $request->post('storeId', ''); //系统门店id
        $shop_order_id = $request->post('shopOrderId', ''); //唯一标识订单的ID,由商户生成
        $shop_no = $request->post('shopNo', ''); //商家门店编号,在配送公司登记,闪送必填,值为店铺id
        $waybill_id = $request->post('waybillId', ''); //配送单id

        $app_key = $request->post('appKey', ''); //商家id,由配送公司分配的appkey
        $app_secret = $request->post('appSecret', ''); //配送公司提供的appSecret加密的校验串
        $remark = $request->post('remark', ''); //否,备注

        try {
            $except_token_data = $request->except(['token']);
            $validate = Validator::make($except_token_data, [
                'storeId'=>'required',
                'shopOrderId'=>'required',
                'shopNo'=>'required',
                'waybillId'=>'required',
            ], [
                'required' => ':attribute参数为必填项',
                'min' => ':attribute参数长度太短',
                'max' => ':attribute参数长度太长',
                'unique' => ':attribute参数已经被人占用',
                'exists' => ':attribute参数不存在',
                'integer' => ':attribute参数必须是数字',
                'required_if' => ':attribute参数满足条件时必传',
            ], [
                'storeId' => '系统门店id',
                'shopOrderId' => '唯一标识订单的ID',
                'shopNo' => '商家门店编号,在配送公司登记',
                'waybillId' => '商家配送单id',
            ]);
            if ($validate->fails()) {
                return $this->responseDataJson(2, $validate->getMessageBag()->first());
            }

            if (!$app_key || !$app_secret) {
                $ps_configs_obj = CustomerAppletsWeChatPsConfig::where('store_id', $store_id)->first();
                if (!$ps_configs_obj || !$ps_configs_obj->app_key || !$ps_configs_obj->app_secret) {
                    return $this->responseDataJson(2, '商户-微信小程序即时配送-未配置');
                }
                $app_key = $ps_configs_obj->app_key;
                $app_secret = $ps_configs_obj->app_secret;
            }
            if (!$shop_order_id) $shop_order_id = $this->createOrderId();

            $access_token = $this->get_access_token($store_id);
            $requestUrl = "https://api.weixin.qq.com/cgi-bin/express/local/business/order/confirm_return?access_token=".$access_token;
            $reqData = [
                'shopid' => $app_key,
                'shop_order_id' => $shop_order_id,
                'shop_no' => $shop_no,
                'delivery_sign' => $app_secret,
                'waybill_id' => $waybill_id
            ];
            if ($remark) $reqData['remark'] = $remark;
            $reqData = json_encode($reqData, JSON_UNESCAPED_UNICODE);
            $requestResultObj = $this->curl_post_https($requestUrl, $reqData);
            $requestResult = json_decode($requestResultObj, true);
            if ($requestResult['errcode'] == 0 || !isset($requestResult['errcode'])) {
                if ($requestResult['resultcode'] == 0) {
                    //根据waybill_id,shop_order_id更新订单状态信息
                    $customerAppletsWeChatOrderObj = CustomerAppletsWeChatOrder::where('waybill_id', $waybill_id)
                        ->where('shop_order_id', $shop_order_id)
                        ->first();
                    if ($customerAppletsWeChatOrderObj) {
                        $customerAppletsWeChatOrderRes = $customerAppletsWeChatOrderObj->update([
                            'status' => '401',
                            'status_desc' => '骑手返回配送货品阶段,货品返还商户成功'
                        ]);
                        if (!$customerAppletsWeChatOrderRes) {
                            Log::info('异常件退回商家商家确认收货-更新订单状态失败');
                            Log::info('配送单id: '.$waybill_id);
                            Log::info('唯一标识订单的ID: '.$shop_order_id);
                        }
                    } else {
                        Log::info('异常件退回商家商家确认收货-未找到订单信息');
                        Log::info('配送单id: '.$waybill_id);
                        Log::info('唯一标识订单的ID: '.$shop_order_id);
                    }

                    return $this->responseDataJson(1, $requestResult['resultmsg'], $requestResult);
                } else {
                    return $this->responseDataJson(2, $requestResult['resultmsg']);
                }
            } else {
                return $this->responseDataJson(2, $requestResult['errmsg']);
            }
        } catch (\Exception $e) {
            Log::info('异常件退回商家商家确认收货-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return $this->responseDataJson(-1);
        }
    }


    /**
     * 拉取配送单信息
     *
     * 使用场景:
     * 商家可使用本接口查询某一配送单的配送状态,便于商家掌握配送情况
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function businessOrderGet(Request $request)
    {
        $store_id = $request->post('storeId', ''); //系统门店id
        $shop_no = $request->post('shopNo', ''); //商家门店编号,在配送公司登记,闪送必填,值为店铺id

        $app_key = $request->post('appKey', ''); //商家id,由配送公司分配的appkey
        $app_secret = $request->post('appSecret', ''); //配送公司提供的appSecret加密的校验串
        $shop_order_id = $request->post('shopOrderId', ''); //唯一标识订单的ID,由商户生成

        try {
            $except_token_data = $request->except(['token']);
            $validate = Validator::make($except_token_data, [
                'storeId'=>'required',
                'shopNo'=>'required',
            ], [
                'required' => ':attribute参数为必填项',
                'min' => ':attribute参数长度太短',
                'max' => ':attribute参数长度太长',
                'unique' => ':attribute参数已经被人占用',
                'exists' => ':attribute参数不存在',
                'integer' => ':attribute参数必须是数字',
                'required_if' => ':attribute参数满足条件时必传',
            ], [
                'storeId' => '系统门店id',
                'shopNo' => '商家门店编号,在配送公司登记',
            ]);
            if ($validate->fails()) {
                return $this->responseDataJson(2, $validate->getMessageBag()->first());
            }

            if (!$app_key || !$app_secret) {
                $ps_configs_obj = CustomerAppletsWeChatPsConfig::where('store_id', $store_id)->first();
                if (!$ps_configs_obj || !$ps_configs_obj->app_key || !$ps_configs_obj->app_secret) {
                    return $this->responseDataJson(2, '商户-微信小程序即时配送-未配置');
                }
                $app_key = $ps_configs_obj->app_key;
                $app_secret = $ps_configs_obj->app_secret;
            }
            if (!$shop_order_id) $shop_order_id = $this->createOrderId();

            $access_token = $this->get_access_token($store_id);
            $requestUrl = "https://api.weixin.qq.com/cgi-bin/express/local/business/order/get?access_token=".$access_token;
            $reqData = [
                'shopid' => $app_key,
                'shop_order_id' => $shop_order_id,
                'shop_no' => $shop_no,
                'delivery_sign' => $app_secret
            ];
            $reqData = json_encode($reqData, JSON_UNESCAPED_UNICODE);
            $requestResultObj = $this->curl_post_https($requestUrl, $reqData);
            $requestResult = json_decode($requestResultObj, true);
            if ($requestResult['errcode'] == 0 || !isset($requestResult['errcode'])) {
                if ($requestResult['resultcode'] == 0) {
                    return $this->responseDataJson(1, $requestResult['resultmsg'], $requestResult);
                } else {
                    return $this->responseDataJson(2, $requestResult['resultmsg']);
                }
            } else {
                return $this->responseDataJson(2, $requestResult['errmsg']);
            }
        } catch (\Exception $e) {
            Log::info('拉取配送单信息-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return $this->responseDataJson(-1);
        }
    }


    /**
     * 模拟配送公司更新配送单状态,该接口只用于沙盒环境,即订单并没有真实流转到运力方
     *
     * 使用场景:
     * 该接口只能用于测试
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function test(Request $request)
    {
        $store_id = $request->post('storeId', ''); //系统门店id
        $shop_order_id = $request->post('shopOrderId', ''); //唯一标识订单的ID,由商户生成
        $order_status = $request->post('orderStatus', ''); //101:配送公司接单阶段,等待分配骑手,即初始状态; 102:配送公司接单阶段,分配骑手成功; 103:配送公司接单阶段,商家取消订单,订单结束; 201:骑手取货阶段,骑手到店开始取货; 202:骑手取货阶段,取货成功; 203:骑手取货阶段,取货失败,商家取消订单,订单结束; 204:骑手取货阶段,取货失败,骑手因自身原因取消订单,订单结束 205:骑手取货阶段,取货失败,骑手因商家原因取消订单,订单结束; 301:骑手配送阶段,配送中; 302:骑手配送阶段,配送成功,订单结束; 303:骑手配送阶段,商家取消订单,配送物品开始返还商家; 304:骑手配送阶段——无法联系收货人,配送物品开始返还商家; 305:骑手配送阶段,收货人拒收,配送物品开始返还商家; 401:骑手返回配送货品阶段,货品返还商户成功,订单结束;  501:因运力系统原因取消,订单结束; 502:因不可抗拒因素(天气,道路管制等原因)取消,订单结束

        $except_token_data = $request->except(['token']);
        $validate = Validator::make($except_token_data, [
            'storeId'=>'required|max:50',
            'shopOrderId' => 'required',
            'orderStatus' => 'required|in:101,102,103,201,202,203,204,205,301,302,303,304,305,401,501,502'
        ], [
            'required' => ':attribute参数为必填项',
            'min' => ':attribute参数长度太短',
            'max' => ':attribute参数长度太长',
            'unique' => ':attribute参数已经被人占用',
            'exists' => ':attribute参数不存在',
            'integer' => ':attribute参数必须是数字',
            'required_if' => ':attribute参数满足条件时必传',
        ], [
            'storeId' => '系统门店id',
            'shopOrderId' => '唯一标识订单的ID',
            'orderStatus' => '配送状态码'
        ]);
        if ($validate->fails()) {
            return json_encode([
                'status' => 2,
                'message' => $validate->getMessageBag()->first()
            ]);
        }

        try {
            if (!$shop_order_id) $shop_order_id = $this->createOrderId();

            $access_token = $this->get_access_token($store_id);
            $requestUrl = "https://api.weixin.qq.com/cgi-bin/express/local/business/test_update_order?access_token=".$access_token;
            $reqData = [
                'shopid' => 'test_shop_id',
                'shop_order_id' => $shop_order_id,
                'action_time' => time(),
                'order_status' => $order_status
            ];
            $reqData = json_encode($reqData, JSON_UNESCAPED_UNICODE);
            $requestResultObj = $this->curl_post_https($requestUrl, $reqData);
            $requestResult = json_decode($requestResultObj, true);
            if ($requestResult['errcode'] == 0 || !isset($requestResult['errcode'])) {
                if ($requestResult['resultcode'] == 0) {
                    return $this->responseDataJson(1, $requestResult['resultmsg'], $requestResult);
                } else {
                    return $this->responseDataJson(2, $requestResult['resultmsg']);
                }
            } else {
                return $this->responseDataJson($requestResult['errcode'], $requestResult['errmsg']);
            }
        } catch (\Exception $e) {
            Log::info('拉取配送单信息-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
            return $this->responseDataJson(-1);
        }
    }


    /**
     * 配送单配送状态更新通知接口(微信调用商户的接口)
     *
     * 使用场景:
     * 当配送公司更新订单配送状态时,微信会通过本接口同步通知商户
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function onOrderStatus(Request $request)
    {
        $data = $request->all(); //
        Log::info('配送单配送状态更新通知-数据');
        Log::info($data);
//        $data = [
//            "ToUserName" => "toUser", //快递公司小程序UserName
//            "FromUserName" => "fromUser", //微信团队的OpenID(固定值)
//            "CreateTime" => 1546924844, //事件时间,Unix时间戳
//            "MsgType" => "event", //消息类型,固定为event
//            "Event" => "update_waybill_status", //事件类型,固定为update_waybill_status,不区分大小写
//            "shopid" => "123456", //商家id,由配送公司分配的appkey
//            "shop_order_id" => "123456", //
//            "waybill_id" => "123456", //
//            "action_time" => 1546924844, //Unix时间戳
//            "order_status" => 102, //
//            "action_msg" => "", //附加信息
//            "shop_no" => "123456", //商家门店编号,在配送公司侧登记
//            "agent" => [
//                "name" => "xxx", //
//                "phone" => "1234567", //
//                "reach_time" => "" //
//            ]
//        ];

        try {
            $where = [];
            $shop_order_id = '';
            $waybill_id = '';
            $order_status = '';
            $agent_name = '';
            $agent_phone = '';
            $agent_reach_time = '';
            if (isset($data['shop_order_id'])) {
                $shop_order_id = $data['shop_order_id']; //唯一标识订单的ID,由商户生成
                $where[] = ['shop_order_id', '=', $shop_order_id];
            }
            if (isset($data['waybill_id'])) {
                $waybill_id = $data['waybill_id']; //配送单id
                $where[] = ['waybill_id', '=', $waybill_id];
            }
            if (isset($data['order_status'])) $order_status = $data['order_status']; //配送状态,枚举值
            if (isset($data['agent']['name'])) $agent_name = $data['agent']['name']; //骑手姓名
            if (isset($data['agent']['phone'])) $agent_phone = $data['agent']['phone']; //骑手电话
            if (isset($data['agent']['reach_time'])) $agent_reach_time = $data['agent']['reach_time']; //预计送达时间戳,配送中返回

            $obj = CustomerAppletsWeChatOrder::where($where)->first();
            if ($obj) {
                $updateData = [];
                if ($order_status) $updateData['status'] = $order_status;
                if ($agent_name) $updateData['agent_name'] = $agent_name;
                if ($agent_phone) $updateData['agent_phone'] = $agent_phone;
                if ($agent_reach_time) $updateData['agent_reach_time'] = $agent_reach_time;
                if ($updateData && $updateData != []) {
                    $res = $obj->update($updateData);
                    if (!$res) {
                        Log::info('配送单配送状态更新通知接口-更新订单失败: ');
                        Log::info('唯一标识订单的ID: '.$shop_order_id);
                        Log::info('配送单id: '.$waybill_id);
                    }
                }
            } else {
                Log::info('配送单配送状态更新通知接口-订单未找到: ');
                Log::info('唯一标识订单的ID: '.$shop_order_id);
                Log::info('配送单id: '.$waybill_id);
            }

        } catch (\Exception $e) {
            Log::info('配送单配送状态更新通知接口-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
        }
    }


    /**
     * 系统配送订单查询
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderLists(Request $request)
    {
        $token = $this->parseToken();
        $page = $request->get('p', '1');
        $limit = $request->get('l', '15');
        $store_id = $request->get('storeId', ''); //系统门店id
        $user_id = $request->get('userId', ''); //小程序用户id
        $out_trade_no = $request->get('outTradeNo', ''); //系统支付订单号
        $shop_order_id = $request->get('shopOrderId', ''); //唯一标识订单的ID
        $delivery_id = $request->get('deliveryId', ''); //配送公司ID
        $openid = $request->get('openId', ''); //下单用户的openid
        $sender_phone = $request->get('senderPhone', ''); //发件人信息电话/手机号
        $receiver_phone = $request->get('receiverPhone', ''); //收件人电话/手机号
        $order_type = $request->get('orderType', ''); //订单类型:0-即时单;1-预约单
        $poi_seq = $request->get('poiSeq', ''); //门店订单流水号
        $start_time = $request->get('startTime', ''); //用户下单付款时间,开始时间
        $end_time = $request->get('endTime', ''); //用户下单付款时间,结束时间
        $is_insured = $request->get('isInsured', ''); //是否保价:0-非保价;1-保价
        $is_direct_delivery = $request->get('isDirectDelivery', ''); //是否选择直拿直送(0-不需要,1-需要)
        $rider_pick_method = $request->get('riderPickMethod', ''); //物流流向(1-从门店取件送至用户;2-从用户取件送至门店)
        $wxa_appid = $request->get('wxaAppid', ''); //授权小程序的appid
        $status = $request->get('status', ''); //订单状态(101:配送公司接单阶段,等待分配骑手,即初始状态; 102:配送公司接单阶段,分配骑手成功; 103:配送公司接单阶段,商家取消订单,订单结束; 201:骑手取货阶段,骑手到店开始取货; 202:骑手取货阶段,取货成功; 203:骑手取货阶段,取货失败,商家取消订单,订单结束; 204:骑手取货阶段,取货失败,骑手因自身原因取消订单,订单结束 205:骑手取货阶段,取货失败,骑手因商家原因取消订单,订单结束; 301:骑手配送阶段,配送中; 302:骑手配送阶段,配送成功,订单结束; 303:骑手配送阶段,商家取消订单,配送物品开始返还商家; 304:骑手配送阶段——无法联系收货人,配送物品开始返还商家; 305:骑手配送阶段,收货人拒收,配送物品开始返还商家; 401:骑手返回配送货品阶段,货品返还商户成功,订单结束;  501:因运力系统原因取消,订单结束; 502:因不可抗拒因素(天气,道路管制等原因)取消,订单结束)',
        $waybill_id = $request->get('waybillId', ''); //配送单号
        $finish_code = $request->get('finishCode', ''); //收货码
        $pickup_code = $request->get('pickupCode', ''); //取货码

        try {
            $check_data = [
                'storeId' => '系统门店id'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return $this->responseDataJson(2, $check);
            }

            $where = [];
            if ($user_id) $where[] = ['user_id', '=', $user_id];
            if ($out_trade_no) $where[] = ['out_trade_no', '=', $out_trade_no];
            if ($shop_order_id) $where[] = ['shop_order_id', '=', $shop_order_id];
            if ($delivery_id) $where[] = ['delivery_id', '=', $delivery_id];
            if ($openid) $where[] = ['openid', '=', $openid];
            if ($sender_phone) $where[] = ['sender_phone', '=', $sender_phone];
            if ($receiver_phone) $where[] = ['receiver_phone', '=', $receiver_phone];
            if ($order_type || $order_type===0 || $order_type==='0') $where[] = ['order_type', '=', $order_type];
            if ($poi_seq) $where[] = ['poi_seq', '=', $poi_seq];
            if ($start_time) $where[] = ['order_time', '>=', strtotime($start_time)];
            if ($end_time) $where[] = ['order_time', '<=', strtotime($end_time)];
            if ($is_insured || $is_insured===0 || $is_insured==='0') $where[] = ['is_insured', '=', $is_insured];
            if ($is_direct_delivery || $is_direct_delivery===0 || $is_direct_delivery==='0') $where[] = ['is_direct_delivery', '=', $is_direct_delivery];
            if ($rider_pick_method) $where[] = ['rider_pick_method', '=', $rider_pick_method];
            if ($wxa_appid) $where[] = ['wxa_appid', '=', $wxa_appid];
            if ($status) $where[] = ['status', '=', $status];
            if ($waybill_id) $where[] = ['waybill_id', '=', $waybill_id];
            if ($finish_code) $where[] = ['finish_code', '=', $finish_code];
            if ($pickup_code) $where[] = ['pickup_code', '=', $pickup_code];
            $obj = CustomerAppletsWeChatOrder::where('store_id', $store_id)
                ->where($where)
                ->get();
            if ($obj) {
                return $this->responseDataJson(1, '查询成功', $obj);
            } else {
                Log::info('配送订单查询-订单未找到: ');
                Log::info('查询条件: '.$where);
                return $this->responseDataJson(2, '订单不存在');
            }
        } catch (\Exception $e) {
            Log::info('配送订单查询-error');
            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
        }
    }


    //唯一标识订单的ID,由商户生成,不超过128字节
    protected function createOrderId($length = 8)
    {
        list($usec, $sec) = explode(' ', microtime());
        $mi = sprintf("%.0f", ((float)$usec + (float)$sec) * 100000000); //微秒
        $num = mt_rand(1, 9);

        $length = isset($length) ? $length : 8;
        for ($i = 0; $i < $length - 1; $i++) {
            $num .= mt_rand(0, 9);
        }
        $str = $num.$mi;

        $order_id = 'weixin_applet_' . date('YmdHis', time()) . substr(microtime(), 2, 6) . $str . sprintf('%03d', rand(0, 999));
        $result = CustomerappletsWechatOrder::where('origin_id', $order_id)->first();
        if ($result) $this->createOrderId();

        return $order_id;
    }


    /**
     * 获取微信小程序品类
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWeChatAppletCategory(Request $request)
    {
        $pid = $request->get('pid', '0'); //上级id

        $cacheData = Cache::has('weChat_applet_'.$pid);
        if ($cacheData) {
            $data = Cache::get('weChat_applet_'.$pid);
        } else {
            $data = CustomerAppletsWeChatCategory::where('pid', $pid)
                ->get();
            Cache::put('weChat_applet_'.$pid, $data, 10000);
        }

        return $this->responseDataJson(1, $data);
    }


    /**
     * 获取小程序全局唯一后台接口调用凭据(access_token)
     *
     * @param $store_id
     * @return mixed|string
     */
    function get_access_token($store_id)
    {
        $merchant_store_appid_secrets_obj = MerchantStoreAppidSecret::where('store_id', $store_id);
        if (!isset($merchant_store_appid_secrets_obj->wechat_appid) || !isset($merchant_store_appid_secrets_obj->wechat_secret)) {
            return json_encode([
                'status' => '2',
                'message' => '微信小程序参数未配置'
            ]);
        }

        $appid = $merchant_store_appid_secrets_obj->wechat_appid;
        $secret = $merchant_store_appid_secrets_obj->wechat_secret;

        if (Cache::has($appid)) {
            $access_token = Cache::get($appid);
        } else {
            $access_token = '';
            if ($merchant_store_appid_secrets_obj->wechat_access_token_invalid_date && (strtotime($merchant_store_appid_secrets_obj->wechat_access_token_invalid_date) > time())) {
                $access_token = $merchant_store_appid_secrets_obj->wechat_access_token;
            }

            if (!$access_token) {
                $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$appid."&secret=".$secret;
                $res = $this->https_request($url, '', 'get', 'json');
                Log::info('获取微信小程序access_token');
                Log::info($res);
                $access_token = $res['access_token'];
                $merchantStoreAppIdSecretModel = new MerchantStoreAppidSecret();
                $merchantStoreAppIdSecretModel->updateWeChatInfo($appid, $res);
                Cache::put($appid, $access_token, 100);
            }
        }

        return $access_token;
    }


    /**
     * curl请求
     *
     * @param $url
     * @param string $data
     * @param string $type
     * @param string $res
     * @return mixed
     */
    function https_request($url, $data = '', $type = 'get', $res = 'json')
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        if ($type == 'post'){
            curl_setopt($curl, CURLOPT_POST, 1);
//            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data, 320));
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data, JSON_UNESCAPED_UNICODE));
        }
        $output = curl_exec($curl);
        curl_close($curl);
        if ($res == 'json') {
            return json_decode($output, true);
        }
    }


}
