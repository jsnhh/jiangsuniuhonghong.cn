<?php
namespace App\Api\Controllers\CustomerApplets;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Api\Controllers\CustomerApplets\BaseController;

class TestController extends BaseController
{
    // openssl_encrypt
    // openssl_decrypt

    protected $key;
    protected $encodingAesKey;
    protected $encodingAesKey2;

    public function __construct()
    {
        $this->key = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFG';
        $this->encodingAesKey = 'mnbvcxzlkjhgfdsapoiuytrewqjhfdlkjuy573duv6c';
        $this->encodingAesKey2 = '2253313b120045f856d64000ecb8db22a788f4c6';
    }


    // 微信消息解密
    public function decryptMsgCommon(Request $request)
    {
        $encodingAesKey = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFG";
        $token = "pamtest";
        $timeStamp = "1409304348";
        $nonce = "1595825783";
        $appId = "wxb11529c136998cb6";
        $text = "<xml><ToUserName><![CDATA[oia2Tj我是中文jewbmiOUlr6X-1crbLOvLw]]></ToUserName><FromUserName><![CDATA[gh_7f083739789a]]></FromUserName><CreateTime>1407743423</CreateTime><MsgType><![CDATA[video]]></MsgType><Video><MediaId><![CDATA[eYJ1MbwPRJtOvIEabaxHs7TX2D-HV71s79GUxqdUkjm6Gs2Ed1KF3ulAOA9H1xG0]]></MediaId><Title><![CDATA[testCallBackReplyVideo]]></Title><Description><![CDATA[testCallBackReplyVideo]]></Description></Video></xml>";

        // $pc = new WXBizMsgCrypt($token, $encodingAesKey, $appId);

        $encryptMsg = "<xml>
                            <AppId><![CDATA[wx569476fd97973ad2]]></AppId>
                            <Encrypt><![CDATA[ruroaM8MzCxH0yWUBSJYAOOKHr/8E/VuZ3fKzHZVqzhjjZ7eRBxj2Ox5Cq7GzoWB2h2jFDIsUaxY2xsmQJ5wzmMXexAxf7lwXcVmx8RZ4vMFF/DKqj444KtvyZGkzB1iP8+oMgSD/zeBAD8EPoFl2l6l+yXlh/isOF5Ux8bZ1FxUWw0IoLweKGhcXBfXqSevrsFXds6scAo44n98AYwx8vlBMvoGjh8Ue8ct+2u59tngrrffXaTDPynnMPGhuCf5rOW05BkVfhXpA5cNkDmI8W4NMksJLDSMS1u2U4SEAsr/V2A81oN0MvC6lOE2gl7h7YM1V7DXaXVN87PC0iMPAX7E6bhg+EcAwyy/9PpU+HoDd9Jk7faAxgMiVCkqEerWlYJplbzRKA06bJIxsLAYJdyKLFmsCibykpVNIS7OcUHVh8YrJDBLDuMCsk/T13uMVB+1CyOW2rfNmqmZJPBHxw==]]></Encrypt>
                        </xml>";

        // $encryptMsg = '';
        $xml_tree = new \DOMDocument();
        $xml_tree->loadXML($encryptMsg);
        $array_e = $xml_tree->getElementsByTagName('Encrypt');
        $array_s = $xml_tree->getElementsByTagName('MsgSignature');
        var_dump($array_s);die;
        $encrypt = $array_e->item(0)->nodeValue;
        $msg_sign = $array_s->item(0)->nodeValue;

        $format = "<xml><ToUserName><![CDATA[toUser]]></ToUserName><Encrypt><![CDATA[%s]]></Encrypt></xml>";
        $from_xml = sprintf($format, $encrypt);

        // 第三方收到公众号平台发送的消息
        $msg = '';
        $errCode = $this->decryptMsg($msg_sign, $timeStamp, $nonce, $from_xml, $msg);
        if ($errCode == 0) {
            print("解密后: " . $msg . "\n");
        } else {
            print($errCode . "\n");
        }
    }

    public function extract($xmltext)
    {
        libxml_disable_entity_loader(true);
        try {
            $xml = new \DOMDocument();
            $xml->loadXML($xmltext);
            $array_e = $xml->getElementsByTagName('Encrypt');
            $array_a = $xml->getElementsByTagName('ToUserName');
            $encrypt = $array_e->item(0)->nodeValue;
            $tousername = $array_a->item(0)->nodeValue;
            return array(0, $encrypt, $tousername);
        } catch (\Exception $e) {
            //print $e . "\n";
            // return array(ErrorCode::$ParseXmlError, null, null);
            return $this->sys_response(40002, "提取密文出错");
        }
    }

    public function getSHA1($token, $timestamp, $nonce, $encrypt_msg)
    {
        //排序
        try {
            $array = array($encrypt_msg, $token, $timestamp, $nonce);
            sort($array, SORT_STRING);
            $str = implode($array);
            // return array(ErrorCode::$OK, sha1($str));
            return $this->sys_response(0);
        } catch (\Exception $e) {
            //print $e . "\n";
            // return array(ErrorCode::$ComputeSignatureError, null);
            return $this->sys_response(40003, "sha加密生成签名失败");
        }
    }

    public function decryptMsg($msgSignature, $timestamp = null, $nonce, $postData, &$msg)
    {
        if (strlen($this->encodingAesKey) != 43) {
            // return ErrorCode::$IllegalAesKey;
            return $this->sys_response('43', "长度不为43");
        }

        // $pc = new Prpcrypt($this->encodingAesKey);

        //提取密文
        $array = $this->extract($postData);
        $ret = $array[0];

        if ($ret != 0) {
            return $ret;
        }

        if ($timestamp == null) {
            $timestamp = time();
        }

        $encrypt = $array[1];
        $touser_name = $array[2];

        //验证安全签名
        // $sha1 = new SHA1;
        $array = $this->getSHA1($this->token, $timestamp, $nonce, $encrypt);
        $ret = $array[0];

        if ($ret != 0) {
            return $ret;
        }

        $signature = $array[1];
        if ($signature != $msgSignature) {
            return $this->sys_response(40001, "签名验证错误");
        }

        $result = $this->decryptTestA($encrypt, $this->app_id);
        if ($result[0] != 0) {
            return $result[0];
        }
        $msg = $result[1];

        return $this->sys_response(200, "OK");
    }

    public function decrypt($encrypted, $appid)
    {

        try {
            //使用BASE64对需要解密的字符串进行解码
            $ciphertext_dec = base64_decode($encrypted);
            $module = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
            $iv = substr($this->key, 0, 16);
            mcrypt_generic_init($module, $this->key, $iv);

            //解密
            $decrypted = mdecrypt_generic($module, $ciphertext_dec);
            mcrypt_generic_deinit($module);
            mcrypt_module_close($module);
        } catch (\Exception $e) {
            return array(ErrorCode::$DecryptAESError, null);
        }


        try {
            //去除补位字符
            // $pkc_encoder = new PKCS7Encoder;
            $result = $this->decode($decrypted);
            //去除16位随机字符串,网络字节序和AppId
            if (strlen($result) < 16)
                return "";
            $content = substr($result, 16, strlen($result));
            $len_list = unpack("N", substr($content, 0, 4));
            $xml_len = $len_list[1];
            $xml_content = substr($content, 4, $xml_len);
            $from_appid = substr($content, $xml_len + 4);
        } catch (\Exception $e) {
            //print $e;
            // return array(ErrorCode::$IllegalBuffer, null);
            return $this->sys_response(40008, "解密后得到的buffer非法");
        }
        if ($from_appid != $appid)
            return $this->sys_response(40005, "appid 校验错误");
        return array(0, $xml_content);

    }

    public function decode($text)
    {

        $pad = ord(substr($text, -1));
        if ($pad < 1 || $pad > 32) {
            $pad = 0;
        }
        return substr($text, 0, (strlen($text) - $pad));
    }


    public function decryptTestA($encrypted, $appid)
    {

        try {
            $iv = substr($this->key, 0, 16);
            $decrypted = openssl_decrypt($encrypted,'AES-256-CBC',substr($this->key, 0, 32),OPENSSL_ZERO_PADDING,$iv);
        } catch (\Exception $e) {
            return $this->sys_response(40007, "aes 解密失败");
        }


        try {
            //去除补位字符
            // $pkc_encoder = new PKCS7Encoder;
            $result = $this->decode($decrypted);
            //去除16位随机字符串,网络字节序和AppId
            if (strlen($result) < 16)
                return "";
            $content = substr($result, 16, strlen($result));
            $len_list = unpack("N", substr($content, 0, 4));
            $xml_len = $len_list[1];
            $xml_content = substr($content, 4, $xml_len);
            $from_appid = substr($content, $xml_len + 4);
            if (!$appid)
                $appid = $from_appid;
            //如果传入的appid是空的，则认为是订阅号，使用数据中提取出来的appid
        } catch (\Exception $e) {
            //print $e;
            return $this->sys_response(40008, "解密后得到的buffer非法");
        }
        if ($from_appid != $appid)
            return $this->sys_response(40005, "appid 校验错误");
        //不注释上边两行，避免传入appid是错误的情况
        echo 11;
        return array(0, $xml_content, $from_appid);
        //增加appid，为了解决后面加密回复消息的时候没有appid的订阅号会无法回复
    }

}
