<?php

namespace App\Api\Controllers\CustomerApplets;

use App\Models\AliThirdConfig;
use App\Models\CustomerAppletsCouponUsers;
use Illuminate\Http\Request;
use Alipayopen\Sdk\AopClient;
use Alipayopen\Sdk\Request\AlipayPassTemplateAddRequest;
use Alipayopen\Sdk\Request\AlipayPassInstanceAddRequest;
use Alipayopen\Sdk\Request\AlipayPassInstanceUpdateRequest;
use Alipayopen\Sdk\Request\AlipaySystemOauthTokenRequest;
use App\Api\Controllers\CustomerApplets\BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CouponAliPayController extends BaseController
{
    /**
     * 记录用户领支付宝卡券回调数据
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    public function userCallbackAliPayStoreCoupon(Request $request){
        $requestAllRequest = $request->all();
        if(isset($requestAllRequest['memberCardType']) || isset($requestAllRequest['type'])){
            if($requestAllRequest['memberCardType'] == "aliPay"){
                //用户领支付宝会员卡回调数据
                Log::info("用户领支付宝会员卡回调数据1：".json_encode($requestAllRequest));
            }else{
                //用户领支付宝会员卡回调数据
                Log::info("用户领支付宝会员卡回调数据2：".json_encode($requestAllRequest));
            }
        }else{
            //用户领支付宝卡券回调数据
            Log::info("用户领支付宝卡券回调数据3：".json_encode($requestAllRequest));
        }
        //调用卡券实例发放接口
        if(isset($requestAllRequest['templateId'])){
            if(!empty($requestAllRequest['templateId'])){
                $result = $this->aliPayPassInstanceAdd($requestAllRequest['templateId'],2,$requestAllRequest['userId'],$requestAllRequest['token']);
                Log::info("发券数据：".json_encode($result,JSON_UNESCAPED_UNICODE));
                if($result['status'] == 200){
                    return json_encode(["success" => true,"passId" => $result['passId'],"resultDesc" => "发券成功"],JSON_UNESCAPED_UNICODE);
                }else{
                    return json_encode(["success" => false,"passId" => "","resultDesc" => "发券失败"],JSON_UNESCAPED_UNICODE);
                }
            }
        }
    }

    /**
     * 创建支付宝小程序卡券
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createAliPayStoreCoupon(Request $request){
        $model = new AliThirdConfig();
        $data = $model->getInfo();
        if (empty($data)) {
            return $this->sys_response(4000, '请先配置支付宝第三方参数');
        }
        $aliPayCallBackUrl = env('APP_URL').$data->ali_pay_callback_url;

        $requestAllData = $request->all();

        if(!isset($requestAllData['store_id']) || empty($requestAllData['store_id'])){
            return $this->responseDataJson(202,"门店id参数不可为空");
        }

        if(!isset($requestAllData['available_begin_time']) || empty($requestAllData['available_begin_time'])){
            return $this->responseDataJson(202,"开始时间不可为空");
        }

        if(!isset($requestAllData['available_end_time']) || empty($requestAllData['available_end_time'])){
            return $this->responseDataJson(202,"结束时间不可为空");
        }

        if(!isset($requestAllData['discount_amount']) || empty($requestAllData['discount_amount'])){
            return $this->responseDataJson(202,"优惠金额不可为空");
        }

        if(!isset($requestAllData['transaction_minimum']) || empty($requestAllData['transaction_minimum'])){
            return $this->responseDataJson(202,"消费门槛不可为空");
        }

        $storeNameInfo = DB::table("stores")->select("merchant_id","store_name","store_short_name")->where(['store_id' => $requestAllData['store_id']])->first();

        if(empty($storeNameInfo)){
            return $this->responseDataJson(202,"该门店信息不存在");
        }
        if(empty($storeNameInfo->store_name) && empty($storeNameInfo->store_short_name)){
            return $this->responseDataJson(202,"请先完善该门店名称信息");
        }

        $storeInfo = DB::table("merchant_store_appid_secrets")->where(['store_id' => $requestAllData['store_id']])->first();

        if(empty($storeInfo)){
            return $this->responseDataJson(202,"该门店appid信息不存在");
        }
        if(empty($storeInfo->alipay_appid)){
            return $this->responseDataJson(202,"请先完善该门店支付宝小程序信息");
        }

        $authorizeAppId = $this->getAuthorizeAppId($storeInfo->alipay_appid);
        if(empty($authorizeAppId['t_appid'])){
            return $this->responseDataJson(202,"请先完成该小程序在支付宝第三方应用平台上进行授权");
        }

        try {

            $aop = new AopClient();
            $aop->gatewayUrl            = 'https://openapi.alipay.com/gateway.do';
            $aop->appId                 = $authorizeAppId['t_appid'];
            $aop->method                = "alipay.pass.template.add";
            $aop->charset               = "utf-8";
            $aop->timestamp             = date("Y-m-d H:i:s",time());
            $aop->rsaPrivateKey         = $this->rsaPrivateKey;
            $aop->alipayrsaPublicKey    = $this->rsaPublicKey;
            $aop->apiVersion            = '1.0';
            $aop->signType              = 'RSA2';
            $aop->postCharset           = 'utf-8';
            $aop->format                = 'json';

            $logoText  = "满".$requestAllData['transaction_minimum']."减".$requestAllData['discount_amount'];
            $brandName = $storeNameInfo->store_short_name ? $storeNameInfo->store_short_name : $storeNameInfo->store_name;

            $request = new AlipayPassTemplateAddRequest ();

            // $request->setBizContent("{\"unique_id\":\" " . $this->uuid() . "\",\"tpl_content\":{\"logo\":\"https://alipass.alipay.com//temps/free/logo.png\",\"strip\":\"https://alipass.alipay.com//temps/free/strip.png\",\"icon\":\"http://alipassprod.test.alipay.net/temps/free/icon.png\",\"content\":{\"evoucherInfo\":{\"product\":\"free\",\"einfo\":{\"logoText\":\"".$logoText."\",\"secondLogoText\":\"\",\"brandName\":\"".$brandName."\",\"passImgRatio\":\"2.79\",\"customFields\":[{\"more\":{\"url\":\"https://alipass.alipay.com//temps/free/logo.png\"},\"label\":\"详细说明\",\"type\":\"url\",\"value\":\"\"}],\"banner\":{\"bannerImg\":\"\",\"url\":\"\"},\"originPrice\":\"\",\"passImg\":\"https://tfsimg.alipay.com/images/alipassprod/TB17fLOXB8rDuNk6Xejwu2EYXXa\",\"auxiliaryFields\":[],\"useLimitDesc\":\"领券\"},\"title\":\"\$title\$\",\"type\":\"marketVoucher\",\"operation\":[{\"altText\":\"\$altText\$\",\"format\":\"url\",\"messageEncoding\":\"UTF-8\",\"message\":\"\$assd\$\"}],\"endDate\":\"\$endTime\$\",\"startDate\":\"\$startTime\$\"},\"fileInfo\":{\"serialNumber\":\"\$serialNumber\$\",\"canShare\":true,\"formatVersion\":\"4\",\"canPresent\":false,\"canBuy\":false},\"merchant\":{\"mname\":\"优惠券\",\"mcallbackUrl\":\"".config('api.aliPayCallBackUrl')."\"},\"style\":{\"backgroundColor\":\"RGB(255,126,0)\"},\"source\":\"alipassprod\",\"platform\":{\"channelID\":\"".$storeInfo->alipay_appid."\"}}}}");
            $request->setBizContent("{\"unique_id\":\" " . $this->uuid() . "\",\"tpl_content\":{\"logo\":\"https://alipass.alipay.com//temps/free/logo.png\",\"strip\":\"https://alipass.alipay.com//temps/free/strip.png\",\"icon\":\"http://alipassprod.test.alipay.net/temps/free/icon.png\",\"content\":{\"evoucherInfo\":{\"product\":\"free\",\"einfo\":{\"logoText\":\"".$logoText."\",\"secondLogoText\":\"\",\"brandName\":\"".$brandName."\",\"passImgRatio\":\"2.79\",\"customFields\":[{\"more\":{\"url\":\"https://alipass.alipay.com//temps/free/logo.png\"},\"label\":\"详细说明\",\"type\":\"url\",\"value\":\"\"}],\"banner\":{\"bannerImg\":\"\",\"url\":\"\"},\"originPrice\":\"\",\"passImg\":\"https://tfsimg.alipay.com/images/alipassprod/TB17fLOXB8rDuNk6Xejwu2EYXXa\",\"auxiliaryFields\":[],\"useLimitDesc\":\"领券\"},\"title\":\"\$title\$\",\"type\":\"marketVoucher\",\"operation\":[{\"altText\":\"\$altText\$\",\"format\":\"url\",\"messageEncoding\":\"UTF-8\",\"message\":\"\$assd\$\"}],\"endDate\":\"\$endTime\$\",\"startDate\":\"\$startTime\$\"},\"fileInfo\":{\"serialNumber\":\"\$serialNumber\$\",\"canShare\":true,\"formatVersion\":\"4\",\"canPresent\":false,\"canBuy\":false},\"merchant\":{\"mname\":\"优惠券\",\"mcallbackUrl\":\"".$aliPayCallBackUrl."\"},\"style\":{\"backgroundColor\":\"RGB(255,126,0)\"},\"source\":\"alipassprod\",\"platform\":{\"channelID\":\"".$storeInfo->alipay_appid."\"}}}}");

            $result = $aop->execute($request,null,$authorizeAppId['AuthorizationCode']);

            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";

            $resultCode = $result->$responseNode->code;
            if(!empty($resultCode) && $resultCode == 10000){
                //说明创建卡券成功
                $createdResult = $result->$responseNode->result;
                $createdResult = json_decode($createdResult,true);
                $createCouponResult = DB::table("customerapplets_aliPay_coupons")->insert([
                    'store_id'              => $requestAllData['store_id'],
                    'tpl_id'                => $createdResult['tpl_id'],
                    'coupon_stock_name'     => $requestAllData['coupon_stock_name'],
                    'available_begin_time'  => $requestAllData['available_begin_time'],
                    'available_end_time'    => $requestAllData['available_end_time'],
                    'discount_amount'       => $requestAllData['discount_amount'],
                    'transaction_minimum'   => $requestAllData['transaction_minimum'],
                    'use_method'            => 1,
                    'coupon_source'         => 2,
                    //但是等到领取的时候，每个用户的传的serialNumber不是一样的
                    'serialNumber'          => time().rand(1,10000),
                    'coupon_message'        => date("YmdHis").time().rand(1,10000),
                    'channelID'             => $storeInfo->alipay_appid,
                    'created_at'            => date("Y-m-d H:i:s",time()),
                    'updated_at'            => date("Y-m-d H:i:s",time()),
                ]);
                if($createCouponResult){
                    return $this->responseDataJson(200,"创建成功",['tpl_id' => $createdResult['tpl_id']]);
                }else{
                    return $this->responseDataJson(202,"数据存储失败",['tpl_id' => $createdResult['tpl_id']]);
                }
            } else {
                return $this->responseDataJson(202,$result->$responseNode->msg);
            }
        }catch (\Exception $e){
            return $this->responseDataJson(202,$e->getMessage());
        }
    }

    /**
     * 封装 卡券实例发放接口
     * 详细文档介绍请看：https://opendocs.alipay.com/apis/api_24/alipay.pass.instance.add
     * @param $tpl_id
     *          模板id
     * @param $recognition_type
     *          AliPass添加对象识别类型
     * @param $aliPay_user_id
     *          用户在支付宝的id
     * @param $aliPay_user_token
     *          用户领取卡券的token
     * @return array
     * @throws \Exception
     */
    public function aliPayPassInstanceAdd($tpl_id,$recognition_type,$aliPay_user_id,$aliPay_user_token){
        try {

            $couponInfo = DB::table("customerapplets_aliPay_coupons")->where(['tpl_id' => $tpl_id])->first();
            if(empty($couponInfo)){
                return ['status' => 202 , 'message' => "卡券信息不存在"];
            }

            $storeInfo     = DB::table("merchant_store_appid_secrets")->where(['store_id' => $couponInfo->store_id])->first();
            if(empty($storeInfo)){
                return ['status' => 202 , 'message' => "该门店appid信息不存在"];
            }
            if(empty($storeInfo->alipay_appid)){
                return ['status' => 202 , 'message' => "请先完善该门店支付宝小程序信息"];
            }

            $authorizeAppId = $this->getAuthorizeAppId($storeInfo->alipay_appid);
            if(empty($authorizeAppId['t_appid'])){
                return ['status' => 202 , 'message' => "请先完成该小程序在支付宝第三方应用平台上进行授权"];
            }

            $aop = new AopClient();
            $aop->gatewayUrl            = 'https://openapi.alipay.com/gateway.do';
            $aop->appId                 = $authorizeAppId['t_appid'];
            $aop->method                = "alipay.pass.instance.add";
            $aop->charset               = "utf-8";
            $aop->timestamp             = date("Y-m-d H:i:s",time());
            $aop->rsaPrivateKey         = $this->rsaPrivateKey;
            $aop->alipayrsaPublicKey    = $this->rsaPublicKey;
            $aop->apiVersion            = '1.0';
            $aop->signType              = 'RSA2';
            $aop->postCharset           = 'utf-8';
            $aop->format                = 'json';

            $request = new AlipayPassInstanceAddRequest();

            if(!empty($couponInfo)){
                $startTime = $couponInfo->available_begin_time;
                $endTime   = $couponInfo->available_end_time;
                $serialNumber = $couponInfo->serialNumber.$aliPay_user_id;

                $request->setBizContent("{\"tpl_id\":\"".$tpl_id."\",\"tpl_params\":\"{\\\"title\\\":\\\"bbbb\\\",\\\"altText\\\":\\\"领取\\\",\\\"assd\\\":\\\"".$couponInfo->coupon_message."\\\",\\\"startTime\\\":\\\"".$startTime."\\\",\\\"endTime\\\":\\\"".$endTime."\\\",\\\"serialNumber\\\":\\\"".$serialNumber."\\\"}\",\"recognition_type\":\"".$recognition_type."\",\"recognition_info\":\"{\\\"user_id\\\":\\\"".$aliPay_user_id."\\\",\\\"user_token\\\":\\\"".$aliPay_user_token."\\\"}\"}");
                $result = $aop->execute($request,null,$authorizeAppId['AuthorizationCode']);
                $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
                $resultCode = $result->$responseNode->code;
                if(!empty($resultCode) && $resultCode == 10000){
                    $passInstanceResult = $result->$responseNode->result;
                    Log::info($passInstanceResult);
                    $passInstanceResult = json_decode($passInstanceResult,true);
                    //添加到领取记录
                    $aliPayUserInfo = DB::table("customerapplets_user_alipays")->select("user_id","store_id")->where(['alipay_user_id' => $aliPay_user_id])->first();
                    DB::table("customerapplets_coupon_users")->insert([
                        'user_id'       => $aliPayUserInfo->user_id,
                        'store_id'      => $aliPayUserInfo->store_id,
                        'stock_id'      => $tpl_id,
                        'coupon_code'   => $aliPay_user_token,
                        'is_use_coupon' => 2,
                        'created_at'    => date("Y-m-d H:i:s",time()),
                        'updated_at'    => date("Y-m-d H:i:s",time()),
                    ]);
                    return ['status' => 200 , 'message' => '发券成功','passId' => $passInstanceResult['passId'],'serialNumber' => $passInstanceResult['serialNumber']];
                } else {
                    return ['status' => 202 , 'message' => $result->$responseNode->msg.'|'.$result->$responseNode->sub_code.'|'.$result->$responseNode->sub_msg];
                }
            }else{
                return ['status' => 202 , 'message' => "卡券信息不存在"];
            }
        }catch (\Exception $e){
            return ['status' => 202 , 'message' => $e->getMessage()];
        }
    }

    /**
     * 封装 核销卡券
     * @param $receive_coupon_id
     * @param $store_id
     * @param $user_id
     * @return array
     */
    public function userUseAliPayCoupon($receive_coupon_id,$store_id,$user_id){

        $storeInfo = DB::table("merchant_store_appid_secrets")->where(['store_id' => $store_id])->first();

        if(empty($storeInfo)){
            return ['status' => 202 , 'message' => "该门店appid信息不存在"];
        }
        if(empty($storeInfo->alipay_appid)){
            return ['status' => 202 , 'message' => "请先完善该门店支付宝小程序信息"];
        }

        $authorizeAppId = $this->getAuthorizeAppId($storeInfo->alipay_appid);
        if(empty($authorizeAppId['t_appid'])){
            return ['status' => 202 , 'message' => "请先完成该小程序在支付宝第三方应用平台上进行授权"];
        }

        try {
            $aop = new AopClient();
            $aop->gatewayUrl            = 'https://openapi.alipay.com/gateway.do';
            $aop->appId                 = $authorizeAppId['t_appid'];
            $aop->method                = "alipay.pass.instance.update";
            $aop->charset               = "utf-8";
            $aop->timestamp             = date("Y-m-d H:i:s",time());
            $aop->rsaPrivateKey         = $this->rsaPrivateKey;
            $aop->alipayrsaPublicKey    = $this->rsaPublicKey;
            $aop->apiVersion            = '1.0';
            $aop->signType              = 'RSA2';
            $aop->postCharset           = 'utf-8';
            $aop->format                = 'json';

            $request = new AlipayPassInstanceUpdateRequest();

            $aliPayCouponInfo = DB::table("customerapplets_aliPay_coupons")->where(['store_id' => $store_id,'tpl_id' => $receive_coupon_id])->first();

            if(!empty($aliPayCouponInfo)){

                $request->setBizContent("{" .
                    "\"serial_number\":\"".$aliPayCouponInfo->serialNumber."\"," .
                    "\"channel_id\":\"".$aliPayCouponInfo->channelID."\"," .
                    "\"tpl_params\":\"{\\\"title\\\":\\\"bbbb\\\",\\\"altText\\\":\\\"领取\\\",\\\"assd\\\":\\\"".$aliPayCouponInfo->coupon_message."\\\",\\\"startTime\\\":\\\"".$aliPayCouponInfo->available_begin_time."\\\",\\\"endTime\\\":\\\"".$aliPayCouponInfo->available_end_time."\\\",\\\"serialNumber\\\":\\\"".$aliPayCouponInfo->serialNumber."\\\"}\"," .
                    "\"status\":\"USED\"," .
                    "\"verify_code\":\"".$aliPayCouponInfo->coupon_message."\"," .
                    "\"verify_type\":\"url\"" .
                    "  }");
                $result = $aop->execute($request,null,$authorizeAppId['AuthorizationCode']);
                $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
                $resultCode   = $result->$responseNode->code;
                if(!empty($resultCode) && $resultCode == 10000){
                    $passInstanceResult = $result->$responseNode->result;
                    Log::info("支付宝核销卡券数据:".$passInstanceResult);
                    $passInstanceResult = json_decode($passInstanceResult,true);

                    //查询该用户有没有领取过该卡券
                    if(!empty($user_id)){
                        $userAliPayCoupon = DB::table("customerapplets_coupon_users")->where(['store_id' => $store_id,'stock_id' => $receive_coupon_id,'user_id' => $user_id])->first();
                        if(!empty($userAliPayCoupon)){
                            DB::table("customerapplets_coupon_users")->where(['store_id' => $store_id,'stock_id' => $receive_coupon_id,'user_id' => $user_id])->update([
                                'is_use_coupon' => 1,
                                'wechatpay_use_time' => date("Y-m-d H:i:s",time()),
                                'updated_at'         => date("Y-m-d H:i:s",time())
                            ]);
                        }
                    }

                    return ['status' => 200 , 'message' => '核销成功'];
                } else {
                    return ['status' => 202 , 'message' => $result->$responseNode->msg.'|'.$result->$responseNode->sub_code.'|'.$result->$responseNode->sub_msg];
                }
            }else{
                return ['status' => 202 , 'message' => "卡券信息不存在"];
            }
        }catch (\Exception $e){
            return ['status' => 202 , 'message' => $e->getMessage()];
        }
    }

    /**
     * 获取用户已经领取的卡券
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserCoupon(Request $request){
        $requestAllData = $request->all();
        if(!isset($requestAllData['store_id']) || empty($requestAllData['store_id'])){
            return $this->responseDataJson(202,"门店id参数不可为空");
        }
        if(!isset($requestAllData['user_id']) || empty($requestAllData['user_id'])){
            return $this->responseDataJson(202,"用户id参数不可为空");
        }

        if(!isset($requestAllData['status']) || empty($requestAllData['status'])){
            return $this->responseDataJson(202,"状态参数不可为空");
        }

        if(!isset($requestAllData['terminal_type']) || empty($requestAllData['terminal_type'])){
            return $this->responseDataJson(202,"请求类型不可为空");
        }

        if($requestAllData['terminal_type'] != config('api.aliPay')){
            return $this->responseDataJson(202,"请求类型不正确");
        }

        $storeInfo = DB::table("merchant_store_appid_secrets")->where(['store_id' => $requestAllData['store_id']])->first();

        if(empty($storeInfo)){
            return $this->responseDataJson(202,"该门店appid信息不存在");
        }

        $userWeChatInfo = DB::table("customerapplets_user_alipays")->where(['user_id' => $requestAllData['user_id']])->first();

        if(empty($userWeChatInfo)){
            return $this->responseDataJson(202,"该用户信息不存在");
        }

        $result = DB::table("customerapplets_coupon_users")
            ->select("customerapplets_coupon_users.id","customerapplets_coupon_users.stock_id","customerapplets_coupon_users.coupon_code",
                "customerapplets_coupon_users.is_use_coupon","customerapplets_coupon_users.wechatpay_use_time","customerapplets_coupon_users.openid",
                "customerapplets_aliPay_coupons.id as coupon_id","customerapplets_aliPay_coupons.coupon_stock_name","customerapplets_aliPay_coupons.available_begin_time","customerapplets_aliPay_coupons.available_end_time",
                "customerapplets_aliPay_coupons.discount_amount","customerapplets_coupon_users.transaction_minimum")
            ->leftJoin("customerapplets_aliPay_coupons","customerapplets_aliPay_coupons.tpl_id","=","customerapplets_coupon_users.stock_id")
            ->where(["customerapplets_coupon_users.user_id" => $requestAllData['user_id'],"customerapplets_coupon_users.store_id" => $requestAllData['store_id']]);

        $date = date("Y-m-d H:i:s",time());

        if($requestAllData['status'] == 1){
            //已使用
            $result = $result->where(["customerapplets_coupon_users.is_use_coupon" => 1])->where("customerapplets_aliPay_coupons.available_end_time",">",$date);
        }else if($requestAllData['status'] == 2){
            //未使用
            $result = $result->where(["customerapplets_coupon_users.is_use_coupon" => 2])->where("customerapplets_aliPay_coupons.available_end_time",">",$date);
        }else{
            //已过期
            $result = $result->where("customerapplets_aliPay_coupons.available_end_time","<",$date);
        }
        $result = $result->get();
        return $this->responseDataJson(200,"请求成功",$result);
    }

    public function getUserAliPayCoupon(Request $request){
        $requestAllData = $request->all();
        if(!isset($requestAllData['store_id']) || empty($requestAllData['store_id'])){
            return $this->responseDataJson(202,"门店id参数不可为空");
        }
        if(!isset($requestAllData['user_id']) || empty($requestAllData['user_id'])){
            return $this->responseDataJson(202,"用户id参数不可为空");
        }

        if(!isset($requestAllData['status']) || empty($requestAllData['status'])){
            return $this->responseDataJson(202,"状态参数不可为空");
        }

        if(!isset($requestAllData['terminal_type']) || empty($requestAllData['terminal_type'])){
            return $this->responseDataJson(202,"请求类型不可为空");
        }

        if($requestAllData['terminal_type'] != config('api.aliPay')){
            return $this->responseDataJson(202,"请求类型不正确");
        }

        $storeInfo = DB::table("merchant_store_appid_secrets")->where(['store_id' => $requestAllData['store_id']])->first();

        if(empty($storeInfo)){
            return $this->responseDataJson(202,"该门店appid信息不存在");
        }

        $userWeChatInfo = DB::table("customerapplets_user_alipays")->where(['user_id' => $requestAllData['user_id']])->first();

        if(empty($userWeChatInfo)){
            return $this->responseDataJson(202,"该用户信息不存在");
        }



        $customerAppletsCouponUsersModel = new CustomerAppletsCouponUsers();
        $customerAppletsCouponUsersResult = $customerAppletsCouponUsersModel->getUserAliPayCoupon($requestAllData['user_id'],$requestAllData['store_id'],$requestAllData['status'],isset($requestAllData['money']) ? $requestAllData['money']:"");

        $num = 0;
        //查询卡券是否还能继续使用的状态
        if(!empty($requestAllData['type']) && $requestAllData['type'] == "cart"){
            if(!empty($customerAppletsCouponUsersResult)){
                foreach ($customerAppletsCouponUsersResult as $key => $val){
                    $available_end_time = strtotime($val -> available_end_time);
                    $now_time = time();
                    if($available_end_time-$now_time>=0){
                        $num += 1;
                        $val -> coupon_state = true;
                    }else{
                        $val -> coupon_state = false;
                        $val -> coupon_text = "卡券已过期";
                    }
                }
            }
        }

        $customerAppletsCouponUsersResult['can_num'] = $num;

        return $this->responseDataJson(200,"请求成功",$customerAppletsCouponUsersResult);

//        $result = DB::table("customerapplets_coupon_users")
//            ->select("customerapplets_coupon_users.id","customerapplets_coupon_users.stock_id","customerapplets_coupon_users.coupon_code",
//                "customerapplets_coupon_users.is_use_coupon","customerapplets_coupon_users.wechatpay_use_time","customerapplets_coupon_users.openid",
//                "customerapplets_aliPay_coupons.id as coupon_id","customerapplets_aliPay_coupons.coupon_stock_name","customerapplets_aliPay_coupons.available_begin_time","customerapplets_aliPay_coupons.available_end_time",
//                "customerapplets_aliPay_coupons.discount_amount","customerapplets_aliPay_coupons.transaction_minimum")
//            ->leftJoin("customerapplets_aliPay_coupons","customerapplets_aliPay_coupons.tpl_id","=","customerapplets_coupon_users.stock_id")
//            ->where(["customerapplets_coupon_users.user_id" => $requestAllData['user_id'],"customerapplets_coupon_users.store_id" => $requestAllData['store_id']]);
//
//        $date = date("Y-m-d H:i:s",time());
//
//        if($requestAllData['status'] == 1){
//            //已使用
//            $result = $result->where(["customerapplets_coupon_users.is_use_coupon" => 1])->where("customerapplets_aliPay_coupons.available_end_time",">",$date);
//        }else if($requestAllData['status'] == 2){
//            //未使用
//            $result = $result->where(["customerapplets_coupon_users.is_use_coupon" => 2])->where("customerapplets_aliPay_coupons.available_end_time",">",$date);
//        }else{
//            //已过期
//            $result = $result->where("customerapplets_aliPay_coupons.available_end_time","<",$date);
//        }
//        $result = $result->get();
//        return $this->responseDataJson(200,"请求成功",$result);
    }
}

