<?php
namespace App\Api\Controllers\CustomerApplets;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Exception\RequestException;
use App\Models\WeixinaStore;
use App\Models\WeixinStore;
use App\Models\WechatCashCouponConfig;
use App\Models\CustomerAppletsUserOrders;
use App\Models\CustomerAppletsUserOrderGoods;

class WechatThirdController extends WechatTicketController
{
    // 快速创建小程序获取小程序二维码
    public function createAppletQrcode(Request $request)
    {
        $requestAllData = $request->all();
        // 参数校验
        $check_data = [
            'store_id'  => '门店id',
            'path'      => 'path'
        ];
        $check = $this->check_required($requestAllData, $check_data);
        if ($check) {
            return $this->sys_response(40000, $check);
        }

        // 第三方平台appid
        $component_appid = $this->app_id;

        $storeConfigAppletsInfo = DB::table("merchant_store_appid_secrets")->where(['store_id' => $requestAllData['store_id']])->first();
        if(empty($storeConfigAppletsInfo) || !$storeConfigAppletsInfo || empty($storeConfigAppletsInfo->wechat_appid)){
            return $this->responseDataJson(202,"请先完善小程序信息");
        }
        // 授权方小程序appid
        $wechat_appid = $storeConfigAppletsInfo->wechat_appid;

        $customerAppletsAuthorizeAppid = DB::table($this->customerAppletsAuthorizeAppid)->where(['t_appid' => $component_appid,'AuthorizerAppid' => $storeConfigAppletsInfo->wechat_appid])->first();
        if(empty($customerAppletsAuthorizeAppid) || !$customerAppletsAuthorizeAppid){
            return $this->responseDataJson(202,"该小程序未授权成功，不可上传代码");
        }
        // 授权方小程序authorizer_refresh_token
        $authorizer_refresh_token = $customerAppletsAuthorizeAppid->authorizer_refresh_token;

        $authorizerTokenResult = $this->getAuthorizerToken($component_appid, $wechat_appid, $authorizer_refresh_token);
        if ($authorizerTokenResult['status'] == 200) {
            $authorizer_access_token = $authorizerTokenResult['data']['authorizer_access_token'];
            // 再次更新刷新令牌
            if (isset($authorizerTokenResult['data']['authorizer_refresh_token']) && !empty($authorizerTokenResult['data']['authorizer_refresh_token'])) {
                //更新该授权方 appid的authorizer_refresh_token
                $updateRefreshToken = DB::table($this->customerAppletsAuthorizeAppid)->where(['t_appid' => $component_appid,'AuthorizerAppid' => $storeConfigAppletsInfo->wechat_appid])->update([
                    'authorizer_refresh_token' =>$authorizerTokenResult['data']['authorizer_refresh_token'],
                    'updated_at' => date("Y-m-d H:i:s",time())
                ]);
                if (!$updateRefreshToken) {
                    return $this->responseDataJson(202,"更新authorizer_refresh_token刷新令牌失败");
                }
            }

            // 获取小程序二维码
            $getAppletQrcodeData = [
                "path"  => $requestAllData['path'],
                "width" => 300
            ];
            $getAppletQrcodeUrl = 'https://api.weixin.qq.com/cgi-bin/wxaapp/createwxaqrcode?access_token='.$authorizer_access_token;
            $getAppletQrcodeRes = $this->curl_post_https($getAppletQrcodeUrl,json_encode($getAppletQrcodeData,JSON_UNESCAPED_UNICODE));

            $dir = 'appletQrcode/';
            // 检查是否存在
            if (!is_dir(public_path($dir))) {
                mkdir(public_path($dir), 0777);
            }

            // 保存图片名称
            $fileName = $requestAllData['store_id'] . time() . '.jpg';

            $save_dir = public_path($dir);
            $resource = fopen($save_dir . $fileName, 'a');
            fwrite($resource, $getAppletQrcodeRes);
            fclose($resource);

            $resData['qrcode_url'] = env('APP_URL') . '/' . $dir . $fileName;

            // 将图片存库
            DB::table("customerapplets_stores")->where(['store_id' => $requestAllData['store_id']])->update([
                'wechat_applet_qr_code' => isset($resData['qrcode_url']) ? $resData['qrcode_url'] : '',
                'updated_at'            => date("Y-m-d H:i:s",time())
            ]);

            return $this->responseDataJson(200,'请求成功', $resData);
        } else {
            return $this->responseDataJson($authorizerTokenResult['status'],$authorizerTokenResult['message']);
        }
    }

    // 获取快速创建小程序的二维码等信息
    public function getAppletInfo(Request $request)
    {
        $requestData = $request->post();
        // 参数校验
        $check_data = [
            'store_id'  => '门店id',
        ];
        $check = $this->check_required($requestData, $check_data);
        if ($check) {
            return $this->sys_response(40000, $check);
        }

        $info = DB::table("customerapplets_stores")->where(['store_id' => $requestData['store_id']])->first();

        return $this->responseDataJson(200,'请求成功', $info);
    }

    /**
     * 获取门店对应通道商户号
     * @param $store_id
     * @param $channel
     * @return mixed
     */
    public function getAvailableMerchants($store_id, $channel)
    {
        $model = new WeixinStore();

        if ($channel == 'wechat') {
            $model = new WeixinStore();
        } else if ($channel == 'wechat_a') {
            $model = new WeixinaStore();
        }

        return $model->where('store_id', $store_id)->first();
    }

    /**
     * 微信点餐订单信息同步（渠道商），扫码点餐
     * 当点餐订单状态发生变化时，都上传全量的订单明细
     * 1、用户在扫码点餐小程序/h5页面中下单时，上报用户的下单信息，此时 status 为 CREATE_DEAL
     * 2、确定支付成功后上报用户的支付信息，此时 status 为 PAY_SUCCESS
     * @param Request $request
     */
    public function wechatOrder(Request $request)
    {
        $requestData = $request->all();

        // 参数校验
        $check_data = [
            'applet_appid'  => '小程序appid',
            'order_id'      => '小程序订单号',
            'user_openid'   => '用户openid',
            'session_key'   => '用户session_key',
            'status'        => 'status',
        ];
        $check = $this->check_required($requestData, $check_data);
        if ($check) {
            return $this->sys_response(400001, $check);
        }

        $model                  = new WechatCashCouponConfig();
        $appletOrderModel       = new CustomerAppletsUserOrders();
        $appletOrderGoodsModel  = new CustomerAppletsUserOrderGoods();

        // 根据服务商user_id查询服务商微信代金券配置参数
        $wechatThirdInfo = $model->getWechatCashCouponConfig();
        if (empty($wechatThirdInfo)) {
            return $this->sys_response(40025, "服务商微信V3参数未配置");
        }

        // 微信服务商商户号
        $wx_merchant_id = $wechatThirdInfo->wx_merchant_id;
        $clientDataMap['merchantId']                = $wx_merchant_id;
        $clientDataMap['merchantSerialNumber']      = $wechatThirdInfo->api_serial_no;
        $clientDataMap['merchantPrivateKey']        = $wechatThirdInfo->key_path;
        $clientDataMap['wechatPayCertificate']      = $wechatThirdInfo->wechat_pay_certificate_key_path;

        // 获取微信V3加密所需参数,构造client
        $client = $this->get_client_common($clientDataMap);

        // 查询对应通道的商户号
        $channel = $wechatThirdInfo->channel;
        $wxStoreInfo = $this->getAvailableMerchants($requestData['store_id'], $channel);
        if(empty($wxStoreInfo)){
            return $this->sys_response(2002, "微信通道商户号未开通");
        }
        $wx_sub_merchant_id = $wxStoreInfo->wx_sub_merchant_id;

        // 小程序点餐路径
        $order_entry = "pages/ordermenu/ordermenu";
        // 支付订单号（status为PAY_SUCCESS时必填）
        $transaction_id = $request->post('out_trade_no', '');

        // 根据 order_id 查询 小程序订单信息，
        $orderInfo      = $appletOrderModel->getOrderInfo($requestData);
        if (empty($orderInfo)) {
            return $this->sys_response(40002, "暂无订单数据");
        }

        $total_amount       = $orderInfo['order_total_money'] * 100;
        $discount_amount    = $orderInfo['use_coupon_money'] * 100;
        $user_amount        = $orderInfo['order_pay_money'] * 100;

        // 组合用户菜品信息
        $orderGoodsInfo = $appletOrderGoodsModel->getOrderGoodsInfo($requestData);
        if (empty($orderGoodsInfo)) {
            return $this->sys_response(40002, "暂无订单商品数据");
        }

        // 用户点餐菜品信息
        $dish_list = [];
        foreach ($orderGoodsInfo as $key => $value) {
            $dish_list[] = [
                'out_dish_no'   => (string)$orderGoodsInfo[$key]['good_id'],    // 商户菜品ID
                'name'          => $orderGoodsInfo[$key]['name'],               // 菜品名称
                'price'         => $orderGoodsInfo[$key]['good_price'] * 100,   // 菜品单价，单位为分
                'unit'          => 'BY_SHARE',                                  // 菜品单位，BY_SHARE-按份 BY_WEIGHT-按重量
                'count'         => $orderGoodsInfo[$key]['good_num'],           // 菜品数量，保留小数点后2位有效数字
            ];
        }

        $date = date("Y-m-d H:i:s", time());
        $date_now = $this->dateTransformationTimezone($date);

        try {
            // 进行请求
            $resp = $client->request('POST', 'https://api.mch.weixin.qq.com/v3/catering/orders/sync-status', [
                'json' => [
                    // 'channel_id'        => $wx_merchant_id,                     // 微信支付分配的渠道商商户号
                    'sp_mchid'          => $wx_merchant_id,                     // 微信支付分配的渠道商商户号
                    'sub_mchid'         => $wx_sub_merchant_id,                 // 微信支付分配子商户商户号
                    'sub_appid'         => $requestData['applet_appid'],        // 子商户在微信公众平台申请服务号对应的APPID
                    'out_shop_no'       => $requestData['store_id'],            // 商户旗下门店的唯一编号
                    'sub_openid'        => $requestData['user_openid'],         // 用户子标识，用户在子商户appid下的openid
                    'login_token'       => $requestData['session_key'],         // 微信用户登录接口返回的登录票据，小程序，填写session_key，公众号，填写页面授权access_token
                    'order_entry'       => $order_entry,                        // 点餐入口，公众号：点餐页面完整URL，小程序：点餐页面path路径
                    'total_amount'      => $total_amount,                       // 总价，单位为分
                    'discount_amount'   => $discount_amount,                    // 优惠金额，单位为分
                    'user_amount'       => $user_amount,                        // 实际支付金额，单位为分
                    'status'            => $requestData['status'],              // 订单状态，取值如下：CREATE_DEAL—用户下单；PAY_SUCCESS—支付完成，结账成功；
                    'action_time'       => $date_now,                           // 状态发生变化的时间
                    'out_order_no'      => $requestData['out_order_no'],        // 渠道商系统内部订单号
                    'pay_time'          => $date_now,                           // （status为PAY_SUCCESS时必填）
                    'transaction_id'    => $transaction_id,                     // 支付订单号（status为PAY_SUCCESS时必填）
                    'out_trade_no'      => $transaction_id,                     // 渠道商系统内部支付订单号（status为PAY_SUCCESS时必填）
                    'dish_list'         => $dish_list,                          // 菜品信息
                ],
                'headers' => ['Accept' => 'application/json']
            ]);

            $res = json_decode($resp->getBody(),true);

            return $this->sys_response($resp->getStatusCode(), "操作成功", $res);
        } catch (RequestException $e) {
            $errorData = json_decode($e->getResponse()->getBody(),true);
            return $this->sys_response($e->getResponse()->getStatusCode(),$errorData['message'],$errorData);
        }
    }

}
