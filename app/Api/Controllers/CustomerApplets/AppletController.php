<?php
namespace App\Api\Controllers\CustomerApplets;

use Illuminate\Http\Request;
use App\Api\Controllers\BaseController;
use App\Models\MerchantStoreTableManage;

class AppletController extends BaseController
{
    // 用户扫码跳转微信、支付宝小程序
    public function toApplet(Request $request)
    {
		header('Location: https://test.yunsoyi.cn/minipro?id=123');
        // 接收参数
        $input = $request->all();
        if (!empty($input)) {
            $table_id_info = explode('/', $input['table_id_info']);
            $input['store_id'] = $table_id_info[0];
            $input['table_id'] = $table_id_info[1];
        }

        // // 获取桌号信息
        // $model = new MerchantStoreTableManage();
        // $tableInfo = $model->getTableInfo($input);
        // if (empty($tableInfo)) {
            // return $this->sys_response(202, '桌号小程序码暂未配置');
        // }
        // $wechat_applet_qrcode = $tableInfo->wechat_applet_qrcode ?? '';
        // $alipay_applet_qrcode = $tableInfo->alipay_applet_qrcode ?? '';

        // 判断是不是微信
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
            echo '<img src="https://test.yunsoyi.cn/tableAppletQrCode/table_id_20201229140927_20201229140927076576.png"  width="300px" height="300px">';
        }

        // 判断是不是支付宝
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'AlipayClient') !== false) {
            return $alipay_applet_qrcode;
        }
    }

}
