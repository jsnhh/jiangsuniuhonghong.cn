<?php

namespace App\Api\Controllers\CustomerApplets;

use Illuminate\Http\Request;
use App\Api\Controllers\CustomerApplets\BaseController;
use App\Models\CustomerAppletsUserAddress;

class AddressController extends BaseController
{
    /**
     * 获取用户的默认地址
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserDefaultAddress(Request $request)
    {
        $requestAllData = $request->all();

        // if(!isset($requestAllData['mobile']) || empty($requestAllData['mobile'])){
        //     return $this->responseDataJson(4010);
        // }

        $customerAppletsUserAddressModel = new CustomerAppletsUserAddress();

        // $result = $customerAppletsUserAddressModel->getUserDefaultAddress($requestAllData['user_id'],$requestAllData['mobile'],$requestAllData['store_id']);
        $result = $customerAppletsUserAddressModel->getUserDefaultAddress($requestAllData['user_id'],$requestAllData['store_id']);

        return $this->responseDataJson(200,"",$result);
    }

    /**
     * 获取用户的地址
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserAddress(Request $request)
    {
        $requestAllData = $request->all();

        // if(!isset($requestAllData['mobile']) || empty($requestAllData['mobile'])){
        //     return $this->responseDataJson(4010);
        // }

        $customerAppletsUserAddressModel = new CustomerAppletsUserAddress();

        $result = $customerAppletsUserAddressModel->getUserAddress($requestAllData);

        return $this->responseDataJson(200,"",$result);
    }

    /**
     * 根据地址id查询某一条地址
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserFirstAddress(Request $request)
    {
        $requestAllData = $request->all();

        if(!isset($requestAllData['address_id']) || empty($requestAllData['address_id'])){
            return $this->responseDataJson(202,"地址id参数未定义");
        }

        $customerAppletsUserAddressModel = new CustomerAppletsUserAddress();

        $result = $customerAppletsUserAddressModel->getUserAddressFirst($requestAllData['address_id']);

        return $this->responseDataJson(200,"",$result);
    }

    /**
     * 创建用户地址数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addUserAddress(Request $request){
        $requestAllData = $request->all();

        if(!isset($requestAllData['mobile']) || empty($requestAllData['mobile'])){
            return $this->responseDataJson(4010);
        }

        if(!isset($requestAllData['province_id']) || empty($requestAllData['province_id'])){
            return $this->responseDataJson(202,"省份参数不可为空");
        }
        if(!isset($requestAllData['city_id']) || empty($requestAllData['city_id'])){
            return $this->responseDataJson(202,"市级参数不可为空");
        }
        if(!isset($requestAllData['area_id']) || empty($requestAllData['area_id'])){
            return $this->responseDataJson(202,"区县级参数不可为空");
        }
        if(!isset($requestAllData['username']) || empty($requestAllData['username'])){
            return $this->responseDataJson(202,"姓名不可为空");
        }

        $customerAppletsUserAddressModel = new CustomerAppletsUserAddress();

        $result = $customerAppletsUserAddressModel->addUserAddress($requestAllData);

        if(!$result){
            return $this->responseDataJson(202);
        }
        if($result['status'] == 200){
            return $this->responseDataJson(200,$result['message']);
        }else{
            return $this->responseDataJson(202,$result['message']);
        }
    }

    /**
     * 更新用户地址数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUserAddress(Request $request){
        $requestAllData = $request->all();

        if(!isset($requestAllData['address_id']) || empty($requestAllData['address_id'])){
            return $this->responseDataJson(202,"地址id参数不可为空");
        }

        if(!isset($requestAllData['mobile']) || empty($requestAllData['mobile'])){
            return $this->responseDataJson(4010);
        }

        if(!isset($requestAllData['province_id']) || empty($requestAllData['province_id'])){
            return $this->responseDataJson(202,"省份参数不可为空");
        }
        if(!isset($requestAllData['city_id']) || empty($requestAllData['city_id'])){
            return $this->responseDataJson(202,"市级参数不可为空");
        }
        if(!isset($requestAllData['area_id']) || empty($requestAllData['area_id'])){
            return $this->responseDataJson(202,"区县级参数不可为空");
        }

        $customerAppletsUserAddressModel = new CustomerAppletsUserAddress();

        $result = $customerAppletsUserAddressModel->updateUserAddress($requestAllData);

        if(!$result){
            return $this->responseDataJson(202);
        }
        if($result['status'] == 200){
            return $this->responseDataJson(200,$result['message']);
        }else{
            return $this->responseDataJson(202,$result['message']);
        }
    }

    /**
     * 删除地址
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUserAddress(Request $request){
        $requestAllData = $request->all();

        if(!isset($requestAllData['address_id']) || empty($requestAllData['address_id'])){
            return $this->responseDataJson(202,"地址id参数不可为空");
        }

        $customerAppletsUserAddressModel = new CustomerAppletsUserAddress();

        $result = $customerAppletsUserAddressModel->deleteUserAddress($requestAllData);

        if(!$result){
            return $this->responseDataJson(202);
        }
        if($result['status'] == 200){
            return $this->responseDataJson(200,$result['message']);
        }else{
            return $this->responseDataJson(202,$result['message']);
        }
    }

}
