<?php

namespace App\Api\Controllers\CustomerApplets;

use App\Api\Controllers\Config\VbillConfigController;
use App\Api\Controllers\Device\YlianyunAopClient;
use App\Api\Controllers\Qmtt\AliBaseController;
use App\Models\CustomerAppletsUserAddress;
use App\Models\CustomerAppletsUserOrderGoods;
use App\Models\CustomerAppletsUserOrders;
use App\Models\DadaOrder;
use App\Models\Device;
use App\Models\Goods;
use App\Models\MqttConfig;
use App\Models\Order;
use App\Models\Store;
use App\Models\VConfig;
use Illuminate\Http\Request;
use App\Api\Controllers\CustomerApplets\BaseController;
use Illuminate\Support\Facades\DB;
use App\Models\MerchantStore;
use Illuminate\Support\Facades\Log;
use MyBank\Tools;
use TencentCloud\Common\Credential;
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Common\Profile\HttpProfile;
use TencentCloud\Common\Exception\TencentCloudSDKException;
use TencentCloud\Iotcloud\V20180614\IotcloudClient;
use TencentCloud\Iotcloud\V20180614\Models\PublishMessageRequest;

class StoreController extends BaseController
{

    /**
     * 获取商家的详情信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStoreInfo(Request $request){

        $requestAllData = $request->all();

        if(!isset($requestAllData['store_id']) || empty($requestAllData['store_id'])){
            return $this->responseDataJson(4002);
        }

        $result = DB::table("stores")->select("stores.store_id","stores.merchant_id","stores.store_name", "stores.store_short_name","stores.province_name","stores.city_name","stores.area_name","stores.store_address",
            "stores.people", "stores.people_phone","stores.s_time","stores.e_time", "stores.announcement", "stores.bg_image", "stores.logo","store_imgs.store_logo_img","store_imgs.store_license_img","store_imgs.store_logo_img_little", "store_imgs.store_img_a", "store_imgs.store_img_b", "store_imgs.store_img_c",
            "store_imgs.store_other_img","store_imgs.store_other_img_a","store_imgs.store_other_img_b","store_imgs.store_other_img_c","store_imgs.store_logo_img",
            "store_imgs.store_auth_bank_img", "store_imgs.store_qr_code")
            ->leftJoin("store_imgs","store_imgs.store_id","=","stores.store_id")
            ->where("stores.store_id","=",$requestAllData['store_id'])
            ->first();

        if (empty($result)) {
            return $this->responseDataJson(40000,"没有该门店");
        }

        //查看该商家的配送信息设置(是否支持跨天配送，是否支持外卖，打包费)
        $store_deliver_settings = DB::table("store_deliver_settings")->where(['store_id' => $requestAllData['store_id']])->first();
        if(!empty($store_deliver_settings)){
            //说明有设置信息
            if(!empty($store_deliver_settings->packing_fee_method)){
                $result->packing_fee_method = $store_deliver_settings->packing_fee_method;
            }else{
                //默认按订单收费
                $result->packing_fee_method = 1;
            }

            if(!empty($store_deliver_settings->packing_charge)){
                $result->packing_charge = $store_deliver_settings->packing_charge;
            }else{
                $result->packing_charge = 0;
            }

            if(!empty($store_deliver_settings->is_same_day)){
                $result->is_same_day = $store_deliver_settings->is_same_day;
            }else{
                //是否跨天预定
                $result->is_same_day = 0;
            }

            if(!empty($store_deliver_settings->delivery_method)){
                $result->delivery_method = $store_deliver_settings->delivery_method;
            }else{
                //配送方式，默认商家自配送
                $result->delivery_method = 1;
            }

            if(!empty($store_deliver_settings->delivery_range)){
                $result->delivery_range = $store_deliver_settings->delivery_range;
            }else{
                //配送范围
                $result->delivery_range = 0;
            }

            if(!empty($store_deliver_settings->status)){
                $result->status = $store_deliver_settings->status;
            }else{
                //是否开启外卖配送
                $result->status = 0;
            }

            if(!empty($store_deliver_settings->delivery_start_amount)){
                $result->delivery_start_amount = $store_deliver_settings->delivery_start_amount;
            }else{
                //最低配送金额
                $result->delivery_start_amount = 0;
            }

            if(!empty($store_deliver_settings->free_delivery_amount)){
                $result->free_delivery_amount = $store_deliver_settings->free_delivery_amount;
            }else{
                //免配送门槛
                $result->free_delivery_amount = 0;
            }

            if(!empty($store_deliver_settings->delivery_amount)){
                $result->delivery_amount = $store_deliver_settings->delivery_amount;
            }else{
                //配送费
                $result->delivery_amount = 0;
            }

            if(!empty($store_deliver_settings->delivery_start_time)){
                $result->delivery_start_time = $store_deliver_settings->delivery_start_time;
            }else{
                //配送开始时段
                $result->delivery_start_time = "";
            }

            if(!empty($store_deliver_settings->delivery_end_time)){
                $result->delivery_end_time = $store_deliver_settings->delivery_end_time;
            }else{
                //配送结束时段
                $result->delivery_end_time = "";
            }
        }else{
            $result->packing_fee_method = 0;
            $result->packing_charge = 0;
            $result->is_same_day = 0;
            $result->delivery_method = 0;
            $result->delivery_range = 0;
            $result->status = 0;
            $result->delivery_start_amount = 0;
            $result->free_delivery_amount = 0;
            $result->delivery_amount = 0;
        }

        if(!empty($result->s_time)){
            $result->s_time = substr($result->s_time,0,5);
        }
        if(!empty($result->e_time)){
            $result->e_time = substr($result->e_time,0,5);
        }

        return $this->responseDataJson(200,"",$result);

    }

    /**
     * 获取当前经纬度与目标经纬度的距离
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDistance(Request $request){
        $requestAllData = $request->all();

        if(!isset($requestAllData['store_id']) || empty($requestAllData['store_id'])){
            return $this->responseDataJson(4002);
        }

        $result = DB::table("stores")->select("store_id","lat", "lng")
            ->where("store_id","=",$requestAllData['store_id'])
            ->first();

        if(!empty($result)){

            if(empty($result->lat) || empty($result->lng)){
                return $this->responseDataJson(202,"该门店无位置信息");
            }else{

                $distanceNumber = $this->getDistanceNumber($requestAllData['longitude'],$requestAllData['latitude'],$result->lng,$result->lat);

                if($distanceNumber < 1000){
                    $distance = round($distanceNumber);
                    return $this->responseDataJson(200,"",["distance" => $distance."m"]);
                }else{
                    $distance = round($distanceNumber / 1000,1);
                    return $this->responseDataJson(200,"",["distance" => $distance."km"]);
                }

            }

        }else{
            return $this->responseDataJson(202,"该门店未查询到信息");
        }

    }

    /**
     * 求两个已知经纬度之间的距离,单位为米
     * @param lng1 $ ,lng2 经度
     * @param lat1 $ ,lat2 纬度
     * @return float 距离，单位米
     * @author www.Alixixi.com
     */
    public function getDistanceNumber($lng1, $lat1, $lng2, $lat2) {
        // 将角度转为狐度
        $radLat1 = deg2rad($lat1); //deg2rad()函数将角度转换为弧度
        $radLat2 = deg2rad($lat2);
        $radLng1 = deg2rad($lng1);
        $radLng2 = deg2rad($lng2);
        $a = $radLat1 - $radLat2;
        $b = $radLng1 - $radLng2;
        $s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2))) * 6378.137 * 1000;
        return $s;
    }

    /**
     * 扣款顺序列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function pay_ways_sort(Request $request){

        $requestAllData = $request->all();

        if(!isset($requestAllData['store_id']) || empty($requestAllData['store_id'])){
            return $this->responseDataJson(4002);
        }

        if(!isset($requestAllData['terminal_type']) || empty($requestAllData['terminal_type'])){
            return $this->responseDataJson(202,"terminal_type类型不可为空");
        }

        try {
            if ($requestAllData['store_id']) {
                switch ($requestAllData['terminal_type'])
                {
                    case 'alipay':
                        $ways_source = 'alipay';
                        break;
                    case 'weixin':
                        $ways_source = 'weixin';
                        break;
                    case 'jd':
                        $ways_source = 'jd';
                        break;
                    case 'unionpay':
                        $ways_source = 'unionpay';
                        break;
//                    case 'applet':
//                        $ways_source = 'applet';
//                        break;
                    default:
                        $ways_source = 'weixin';
                }
//                $ways_source = "weixin";
//
//                if($requestAllData['terminal_type'] == config('api.aliPay')){
//                    $ways_source = "alipay";
//                }

                $applet = DB::table('store_pay_ways')
                    ->where('store_id', $requestAllData['store_id'])
                    ->where('ways_source', $ways_source)
                    ->select('id as store_pay_ways_id', 'ways_desc', 'ways_type', 'store_id', 'sort', 'ways_source')
                    ->where('status', 1)
                    ->orderBy('store_pay_ways.sort', 'asc')
                    ->first();

                return $this->responseDataJson(200,"数据返回成功",['applet' => $applet]);
            }else{
                return $this->responseDataJson(202,"没有绑定店铺","");
            }
        } catch (\Exception $exception) {
            return $this->responseDataJson(202,$exception->getMessage(),"");
        }
    }

    /**
     * 获取，设置门店小程序信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStoreAppletsInfo(Request $request){
        $requestAllData = $request->all();
        if(!isset($requestAllData['store_id']) || empty($requestAllData['store_id'])){
            return $this->responseDataJson(4002);
        }

        if(!isset($requestAllData['type']) || empty($requestAllData['type'])){
            return $this->responseDataJson(202,"type类型不可为空");
        }

        $result = DB::table("merchant_store_appid_secrets")->select("wechat_appid","wechat_secret", "alipay_appid","alipay_rsaPrivateKey","alipay_alipayrsaPublicKey","store_id","created_at","updated_at")
            ->where("store_id","=",$requestAllData['store_id'])
            ->first();

        if($requestAllData['type'] == "select"){
            //查询状态
            return $this->responseDataJson(200,"查询成功",$result);
        }else{
            //更新状态
            if(!empty($result)){
                //更新这条数据
                try{
                    $updateResult = DB::table("merchant_store_appid_secrets")->where("store_id","=",$requestAllData['store_id'])->update([
                        'wechat_appid' => $requestAllData['wechat_appid'] ? $requestAllData['wechat_appid']:"",
                        'wechat_secret' => $requestAllData['wechat_secret'] ? $requestAllData['wechat_secret']:"",
                        'alipay_appid' => $requestAllData['alipay_appid'] ? $requestAllData['alipay_appid']:"",
                        'alipay_rsaPrivateKey' => $requestAllData['alipay_rsaPrivateKey'] ? $requestAllData['alipay_rsaPrivateKey']:"",
                        'alipay_alipayrsaPublicKey' => $requestAllData['alipay_alipayrsaPublicKey'] ? $requestAllData['alipay_alipayrsaPublicKey']:"",
                        'updated_at' => date("Y-m-d H:i:s",time()),
                    ]);
                    if($updateResult){
                        return $this->responseDataJson(200,"更新成功");
                    }else{
                        return $this->responseDataJson(202,"更新失败");
                    }
                }catch (\Exception $e){
                    return $this->responseDataJson(202,$e->getMessage());
                }
            }else{
                //添加这条数据
                try{
                    $insertResult = DB::table("merchant_store_appid_secrets")->insert([
                        'store_id'     => $requestAllData['store_id'],
                        'wechat_appid' => $requestAllData['wechat_appid'] ? $requestAllData['wechat_appid']:"",
                        'wechat_secret' => $requestAllData['wechat_secret'] ? $requestAllData['wechat_secret']:"",
                        'alipay_appid' => $requestAllData['alipay_appid'] ? $requestAllData['alipay_appid']:"",
                        'alipay_rsaPrivateKey' => $requestAllData['alipay_rsaPrivateKey'] ? $requestAllData['alipay_rsaPrivateKey']:"",
                        'alipay_alipayrsaPublicKey' => $requestAllData['alipay_alipayrsaPublicKey'] ? $requestAllData['alipay_alipayrsaPublicKey']:"",
                        'created_at' => date("Y-m-d H:i:s",time()),
                        'updated_at' => date("Y-m-d H:i:s",time()),
                    ]);
                    if($insertResult){
                        //添加成功，请求随行付“微信子商户支付参数配置”
                        $vbillConfigControllerObj = new VbillConfigController();
                        $vbill_config = $vbillConfigControllerObj->vbill_config('');
                        $vbill_merchant_obj = $vbillConfigControllerObj->vbill_merchant($requestAllData['store_id'], '');
                        if (!$vbill_merchant_obj) {
                            $vbill_merchant_obj = $vbillConfigControllerObj->vbilla_merchant($requestAllData['store_id'], '');
                            if ($vbill_merchant_obj) {
                                $vbill_config = $vbillConfigControllerObj->vbilla_config('');
                            }
                        }
                        if ($vbill_config && $vbill_merchant_obj) {
                            $obj = new \App\Api\Controllers\Vbill\BaseController();
                            $url = $obj->weChatPaySetAddConf;
                            $weChatPaySetAddData = [
                                'orgId' => $vbill_config->orgId,
                                'reqId' => time(),
                                'version' => '1.0',
                                'timestamp' => date('Ymdhis', time()),
                                'signType' => 'RSA',
                                'reqData' => [
                                    'mno' => $vbill_merchant_obj->mno,
                                    'subMchId' => $vbill_merchant_obj->childNo, //微信子商户号;说明：渠道子商户号
                                    'type' => '01', //01 支付Appid;02 关注Appid;03 jsapi授权目录
                                    'subAppid' => $requestAllData['wechat_appid'], //支付Appid配置类型为01，02时必传
                                    'accountType' => '01', //支付Appid类型配置类型为01时必传,00公众号 01小程序 02APP（暂不支持APP支付）
                                    'subscribeAppid' => $requestAllData['wechat_appid'] //推荐关注公众号Appid配置类型为02时与推荐关注小程序Appid二选一
                                ]
                            ];
                            $weChatPaySetAddRes = $obj->execute($weChatPaySetAddData, $url, $vbill_config->privateKey, $vbill_config->sxfpublic);
                            Log::info('随行付-小程序-微信子商户支付参数配置');
                            Log::info($weChatPaySetAddData);
                            Log::info($weChatPaySetAddRes);
                        }

                        return $this->responseDataJson(200,"创建成功");
                    }else{
                        return $this->responseDataJson(202,"创建失败");
                    }
                }catch (\Exception $e){
                    return $this->responseDataJson(202,$e->getMessage());
                }
            }
        }
    }


    /**
     * 打印小票 易联云打印机
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function yly_print(Request $request)
    {
        $action_type = $request->post('actionType', '3'); //1-外卖单;2-自提单;3-扫码点餐
        $out_trade_no = $request->post('outTradeNo', ''); //系统支付订单号
        $pickup_code = $request->post('pickupCode', ''); //取货码,actionType为2时必填
        $is_print = $request->post('is_print', ''); // 商品是否打印
        $pay_success_print = $request->post('pay_success_print', ''); // 支付成功之后打印

        if(!isset($out_trade_no) || empty($out_trade_no)){
            return $this->responseDataJson(202, "系统支付订单号不可为空");
        }

//        $order_obj = Order::where('out_trade_no', $out_trade_no)->first();
//        if (!$order_obj) {
//            return $this->responseDataJson(2, "订单不存在");
//        }
//        $store_id = $order_obj->store_id;
//        $merchant_id = $order_obj->merchant_id;
//        $pay_status = $order_obj->pay_status;

        $user_order_obj = CustomerAppletsUserOrders::where('out_trade_no', $out_trade_no)
            ->first();
        if (!$user_order_obj) {
            return $this->responseDataJson(2, "用户订单不存在");
        }
        $user_id = $user_order_obj->user_id; //小程序用户id
        $created_at = $user_order_obj->created_at;
        $delivery_fee = $user_order_obj->delivery_fee; //配送费
        $packing_fee = $user_order_obj->packing_fee; //打包费
        $use_coupon_money = $user_order_obj->use_coupon_money ?? 0.00; //如果使用优惠券，优惠券优惠额度
        $order_pay_money = $user_order_obj->order_pay_money; //订单实际支付金额
        $order_total_money = $user_order_obj->order_total_money; //订单总金额
        if (!empty($pay_success_print) && $pay_success_print == 1) {
            $order_pay_money = $order_total_money;
        }

        $order_pay_status = $user_order_obj->order_pay_status; //订单状态：1(已支付)2(待支付)3(已取消)4(订单开始配送)5(订单配送结束)6(退款订单)7(骑手正在取货)8(订单已指定配送员)9(妥投异常之物品返回中)10(妥投异常之物品返回完成)11(订单已完成)100( 骑士到店)1000(创建达达运单失败)
        $self_raising_order_time = $user_order_obj->self_raising_order_time; //如果是自提订单，自提的时间
        $self_raising_order_mobile = $user_order_obj->self_raising_order_mobile; //如果是自提订单，预留的手机号
        $address_id = $user_order_obj->address_id; //如果是外卖订单，订单配送的地址
        $table_number = $user_order_obj->table_name; //桌名称
        $store_id = $user_order_obj->store_id;
        $remark = $user_order_obj->order_remarks;
        $order_pay_type = $user_order_obj->order_pay_type; //订单支付方式 1：微信 2：支付宝
        $user_pay_type = $user_order_obj->user_pay_type; //用户支付方式 1：余额支付 2：原生app支付

        $store_obj = Store::where('store_id', $store_id)->first();
        if (!$store_obj) {
            return $this->responseDataJson(2, "门店不存在");
        }

        $store_address = $store_obj->store_address;
        $store_tel = $store_obj->people_phone;
        $store_name = $store_obj->store_name;

        if ($order_pay_status == 1) {
            $pay_status_desc = '已在线支付';
        } elseif ($order_pay_status == 2) {
            $pay_status_desc = '待支付';
        } elseif ($order_pay_status == 3) {
            $pay_status_desc = '已取消';
        } elseif ($order_pay_status == 4) {
            $pay_status_desc = '订单开始配送';
        } elseif ($order_pay_status == 5) {
            $pay_status_desc = '订单配送结束';
        } elseif ($order_pay_status == 6) {
            $pay_status_desc = '退款订单';
        } elseif ($order_pay_status == 7) {
            $pay_status_desc = '骑手正在取货';
        } elseif ($order_pay_status == 8) {
            $pay_status_desc = '订单已指定配送员';
        } elseif ($order_pay_status == 9) {
            $pay_status_desc = '妥投异常之物品返回中';
        } elseif ($order_pay_status == 10) {
            $pay_status_desc = '妥投异常之物品返回完成';
        } elseif ($order_pay_status == 11) {
            $pay_status_desc = '订单已完成';
        } elseif ($order_pay_status == 100) {
            $pay_status_desc = '骑士到店';
        }  elseif ($order_pay_status == 1000) {
            $pay_status_desc = '创建达达运单失败';
        } else {
            $pay_status_desc = '支付状态未知';
        }

        $devices_obj = Device::where('store_id', $store_id)
//            ->where('merchant_id', $merchant_id)
            ->where('type', 'p')
            ->get();

        //收银员未绑定走门店机器
        if ($devices_obj->isEmpty()) {
            $devices_obj = Device::where('store_id', $store_id)
//                ->where('merchant_id', '')
                ->where('type', 'p')
                ->get();
        }

        if (!$devices_obj->isEmpty()) {
            foreach ($devices_obj as $values) {
                if ($values->device_type == "p_yly_k4") {
                    $VConfig = VConfig::where('config_id', $values->config_id)
                        ->select('yly_user_id', 'yly_api_key')->first();
                    if (!$VConfig) {
                        $VConfig = VConfig::where('config_id', '1234')
                            ->select(
                                'yly_user_id',
                                'yly_api_key'
                            )->first();
                    }
                    if (!$VConfig) {
                        Log::info('易联云打印未配置app/Api/Controllers/CustomerApplets/StoreController.php/yly_print');
                        continue;
                    }

                    try {
                        $yLiObj = new YlianyunAopClient();
                        $push_id = $VConfig->yly_user_id; //用户id
                        $push_key = $VConfig->yly_api_key; //api密钥
                        $device_key = $values->device_key;
                        $device_no = $values->device_no;
//                        $type = $order_obj->ways_type_desc;
//                        $store_name = $order_obj->store_name;
//                        $out_trade_no = $order_obj->out_trade_no;
//                        $total_amount = $order_obj->total_amount;
//                        $remark = $order_obj->remark;

                        $order_goods_obj_map = [
                            'out_trade_no' => $out_trade_no,
                            'store_id' => $store_id
                        ];
                        if (!empty($is_print)) {
                            $order_goods_obj_map['is_print'] = $is_print;
                        }

                        // $order_goods_obj = CustomerAppletsUserOrderGoods::where('out_trade_no', $out_trade_no)
                        //     ->where('store_id', $store_id)
                        //     ->get();
                        $order_goods_obj = CustomerAppletsUserOrderGoods::where($order_goods_obj_map)->get();
                        if (!$order_goods_obj) {
                            return $this->responseDataJson(2, "商品订单不存在");
                        }
                        $order_goods_list_arr = $order_goods_obj->toArray();
                        $goods_list = '';
                        if ($order_goods_list_arr && $order_goods_list_arr!=[] && is_array($order_goods_list_arr)) {
                            foreach ($order_goods_list_arr as $value) {
                                $goods_obj = Goods::find($value['good_id']);
                                $length = strlen($goods_obj->name);
                                if ($length <= 9) {
                                    $goods_list .= str_pad($goods_obj->name, $length+10, " ") . '    X'. $value['good_num'] .'     '. $value['good_price'] . "\n\r";
                                } elseif ((9 < $length) && ($length <= 27)) {
                                    $goods_list .= str_pad($goods_obj->name, $length+4, " ") . '    X'. $value['good_num'] .'     '. $value['good_price'] . "\n\r";
                                } else {
                                    $goods_list .= str_pad($goods_obj->name, 45-$length, " ") . '    X'. $value['good_num'] .'     '. $value['good_price'] . "\n\r";
                                }
                            }
                        }

                        if ($action_type == '3') { //扫码点餐
                            if ($order_pay_status == 1) {
                                $pay_status_desc = '已完成线上支付';
                            } else {
                                $pay_status_desc = '未支付,等待前台支付';
                            }

                            $content_str = "<center>" . '***#1 堂食单***' . "</center>
<FB>" . $store_name . "</FB>
[下单时间]" . $created_at . "
[订单号]" . $out_trade_no . "
--------------------------------
<FB>" . "<FS>" . '桌号: ' . $table_number . "
$pay_status_desc
</FS></FB>" . "
--------------------------------
名称               数量    售价
--------------------------------
" . $goods_list . "
--------------------------------";
                            if ($packing_fee && $packing_fee!=='0.00') {
                                $content_str .= '打包费: ' . round($packing_fee, 2);
                            }
                            if ($use_coupon_money && $use_coupon_money!=='0.00') {
                                $content_str .= '优惠金额: ' . round($use_coupon_money, 2);
                            }
                            if (($packing_fee && $packing_fee!=='0.00') || ($use_coupon_money && $use_coupon_money!=='0.00')) {
                                $content_str .= '--------------------------------';
                            }
                            $content_str .= '总计:                ￥' . round($order_pay_money, 2) . "
备注：" . $remark .
"<center>            ***#1 完***    </center>";
                        }
                        elseif ($action_type == '1') { //外卖单
                            $data_order_obj = DadaOrder::where('out_trade_no', $out_trade_no)->first();
                            if (!$data_order_obj) {
                                return $this->responseDataJson(2, "达达支付订单不存在");
                            }
                            $delay_publish_time = $data_order_obj->delay_publish_time ? date('Y-m-d H-i-s', $data_order_obj->delay_publish_time): '立即配送'; //预约发单时间

                            if (isset($address_id)) {
                                $user_address_obj = CustomerAppletsUserAddress::find($address_id);
                            } else {
                                $user_address_obj = CustomerAppletsUserAddress::where('user_id', $user_id)
                                    ->where('store_id', $store_id)
                                    ->first();
                            }
                            if (!$user_address_obj) {
                                return $this->responseDataJson(2, "外卖用户地址信息不存在11");
                            }
                            $customer_tel = $user_address_obj->user_mobile; //用户地址预留的手机号
                            $dada_order_address = $user_address_obj->address_detail; //详细地址

                            $content_str = "<center>" . '***#1 外卖单***' . "</center>
<FB>" . $store_name . "</FB>
[下单时间]" . $created_at . "
[配送时间]" . $delay_publish_time . "
[支付状态]" . $pay_status_desc . "
[订单号]" . $out_trade_no . "
--------------------------------
<FB>" . "<FS>" . '配送地址: ' . $dada_order_address . "
$customer_tel
</FS></FB>" . "
--------------------------------
名称               数量    售价
--------------------------------
" . $goods_list . "
--------------------------------";
                            if ($delivery_fee && $delivery_fee!=='0.00') {
                                $content_str .= '配送费: ' . round($delivery_fee, 2);
                            }
                            if ($packing_fee && $packing_fee!=='0.00') {
                                $content_str .= '打包费: ' . round($packing_fee, 2);
                            }
                            if ($use_coupon_money && $use_coupon_money!=='0.00') {
                                $content_str .= '优惠金额: ' . round($use_coupon_money, 2);
                            }
                            if (($packing_fee && $packing_fee!=='0.00') || ($use_coupon_money && $use_coupon_money!=='0.00') || ($delivery_fee && $delivery_fee!=='0.00') ) {
                                $content_str .= '--------------------------------';
                            }
                            $content_str .= "总计:                ￥". round($order_pay_money, 2) .
$remark.
"<center>            ***#1 完***    </center>";
                        }
                        else { //自提单
                            $content_str = "<center>" . '***#1 自提单***' . "</center>
<FB>" . $store_name . "</FB>
[下单时间]" . $created_at . "
[支付状态]" . $pay_status_desc . "
[订单号]" . $out_trade_no . "
--------------------------------
<FB>" . "<FS>" . '自提时间: ' . $self_raising_order_time . "
$pickup_code
</FS></FB>" . "
--------------------------------
名称               数量    售价
--------------------------------
" . $goods_list . "
--------------------------------";
                            if ($packing_fee && $packing_fee!=='0.00') {
                                $content_str .= '打包费: ' . round($packing_fee, 2);
                            }
                            if ($use_coupon_money && $use_coupon_money!=='0.00') {
                                $content_str .= '优惠金额: ' . round($use_coupon_money, 2);
                            }
                            if (($packing_fee && $packing_fee!=='0.00') || ($use_coupon_money && $use_coupon_money!=='0.00')) {
                                $content_str .= '
--------------------------------';
                            }
                        $content_str .= '总计:                ￥' . round($order_pay_money, 2) .
$remark.
"<center>" . '          ***#1 完***' . "</center>";
                        }

                        $content = urlencode($content_str);

                        $yLiObj->action_print($push_id, $device_no, $content, $push_key, $device_key);
                    } catch (\Exception $exc) {
                    Log::info('易联云打印-error');
                    Log::info($exc->getMessage().' | '.$exc->getFile().' | '.$exc->getLine());
                    continue;
                    }
                }
            }

            //1 支付宝 2 微信支付 3京东支付 4 银联闪付  5 积分抵扣  6.会员卡
//            $type = '0';
//            if ($user_pay_type == 2) { //1：余额支付 2：原生app支付
//                if ($order_pay_type == 2) { //订单支付方式 1：微信 2：支付宝
//                    $type = '1';
//                }
//                if ($order_pay_type == 1) {
//                    $type = '2';
//                }
//            } else {
//                $type = '6';
//            }
//            $this->v_send(round($order_pay_money, 2), $store_id, $type, '', $config_id = "1234", $out_trade_no);

            return json_encode([
                'status' => '1',
                'message' => '打印成功'
            ]);
        } else {
            return json_encode([
                'status' => '2',
                'message' => '没有匹配到打印设备'
            ]);
        }
    }

    /**
     * 获取门店下面的微信小程序，支付宝小程序对应的模板消息列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStoreAppletTemplate(Request $request){
        $requestAllData = $request->all();
        if(!isset($requestAllData['store_id']) || empty($requestAllData['store_id'])){
            return $this->responseDataJson(4002);
        }

        $storeInfo = DB::table("merchant_store_appid_secrets")
            ->select("wechat_appid","alipay_appid")
            ->where(['store_id' => $requestAllData['store_id']])->first();
        if(empty($storeInfo)){
            return $this->responseDataJson(202,"请先完善该门店小程序信息");
        }

        $weChatAppId = $storeInfo->wechat_appid;

        $weChatTemplate = DB::table("customerapplets_template_infos")->where([
            'applet_id' => $weChatAppId,
            'type'      => 1,
            'status'    => 1
        ])->get();

        foreach ($weChatTemplate as $key => $val){
            if(!empty($val->template_keyword)){
                //正则过滤模板
                preg_match_all('/(?:\{\{)(.*?)(?:\}\})/', $val->template_keyword, $keywords);
                $val->keywords = isset($keywords[1]) ? $keywords[1] : $keywords[0];
//                preg_match_all('/.*?(?=:)/', $val->template_keyword, $template_input);
                preg_match_all('/[\x{4e00}-\x{9fff}]+/u', $val->template_keyword, $template_input);
                $val->template_input = $template_input[0];
            }
        }

        $aliPayTemplate = DB::table("customerapplets_template_infos")->where([
            'applet_id' => config("api.aliPay_applet_template_id"),
            'type'      => 2,
            'status'    => 1
        ])->get();

        foreach ($aliPayTemplate as $key => $val){
            if(!empty($val->template_keyword)){
                //正则过滤模板
                preg_match_all('/(?:\{\{)(.*?)(?:\}\})/', $val->template_keyword, $keywords);
                $val->keywords = isset($keywords[1]) ? $keywords[1] : $keywords[0];
                preg_match_all('/[\x{4e00}-\x{9fff}]+/u', $val->template_keyword, $template_input);
                $val->template_input = $template_input[0];
            }
        }

        return $this->responseDataJson("200","请求成功",['weChatTemplate' => $weChatTemplate,'aliPayTemplate' => $aliPayTemplate]);
    }


    //播报设备 金额是分
    public function v_send($price, $store_id, $type, $merchant_id, $config_id = "1234", $out_trade_no, $device_id = "")
    {
        try {
            //设备播报
            if ($merchant_id && (int)$merchant_id != 0) {
                $Device = Device::where('store_id', $store_id)
                    ->where('merchant_id', $merchant_id)
                    ->whereIn("device_type", ["v_bp_1", 'v_jbp_d30', 'v_kd_58', 'v_zw_1', 's_bp_sl51', 's_bp_sl56', 'v_zlbz_1', 's_ky_mp10','v_ps_sp08','phone', 'v_cls_q038'])
                    ->get();
            } else {
                $Device = Device::where('store_id', $store_id)
                    ->whereIn("device_type", ["v_bp_1", 'v_jbp_d30', 'v_kd_58', 'v_zw_1', 's_bp_sl51', 's_bp_sl56', 'v_zlbz_1', 's_ky_mp10','v_ps_sp08', 'phone', 'v_cls_q038'])
                    ->get();
            }

            if ($Device->isEmpty()) {

            } else {
                foreach ($Device as $k => $v) {
                    //智联博众
                    if ($v->device_type == "v_zlbz_1") {
                        $VConfig = VConfig::where('config_id', $config_id)
                            ->select('zlbz_token')->first();
                        if (!$VConfig) {
                            $VConfig = VConfig::where('config_id', '1234')
                                ->select('zlbz_token')->first();
                        }
                    }

                    //智网
                    if ($v->device_type == "v_zw_1") {
                        $VConfig = VConfig::where('config_id', $config_id)
                            ->select('zw_token')->first();
                        if (!$VConfig) {
                            $VConfig = VConfig::where('config_id', '1234')
                                ->select('zw_token')->first();
                        }
                        $url = 'http://cloudspeaker.smartlinkall.com/add.php?token=' . $VConfig->zw_token . '&id=' . $v->device_no . '&uid=' . $store_id . $v->device_no . '&seq=' . time() . '&price=' . $price . '&pt=' . $type . '&vol=100';
                        $data = Tools::curl_get($url);

                        //老的智网喇叭配置
                        $oldVConfig = VConfig::where('config_id', $config_id)
                            ->select('old_zw_token')
                            ->first();
                        if (!$oldVConfig) {
                            $oldVConfig = VConfig::where('config_id', '1234')
                                ->select('old_zw_token')
                                ->first();
                        }
                        if ($oldVConfig) {
                            $url = 'http://cloudspeaker.smartlinkall.com/add.php?token=' . $oldVConfig->old_zw_token . '&id=' . $v->device_no . '&uid=' . $store_id . $v->device_no . '&seq=' . time() . '&price=' . $price . '&pt=' . $type . '&vol=100';
                            $data = Tools::curl_get($url);
                        }
                    }

                    //推送卡台小喇叭+波谱sl51
                    if (in_array($v->device_type, ['v_bp_1', 'v_jbp_d30', 'v_kd_58', 's_bp_sl51', 's_bp_sl56', 's_ky_mp10','v_ps_sp08','phone'])) {
                        $type_desc = "";
                        //1 支付宝 2 微信支付 3京东支付 4 银联闪付  5 积分抵扣  6.会员卡
                        if ($type == '1') {
                            $type_desc = "支付宝";
                        }
                        if ($type == '2') {
                            $type_desc = "微信";
                        }
                        if ($type == '3') {
                            $type_desc = "京东";
                        }
                        if ($type == '4') {
                            $type_desc = "银联云闪付";
                        }
                        if ($type == '5') {
                            $type_desc = "积分抵扣";
                        }
                        if ($type == '6') {
                            $type_desc = "会员卡";
                        }

                        //反扫推送类型的设备不播报
                        if (in_array($v->device_type, ['s_bp_sl51', 's_bp_sl56']) && strpos($out_trade_no, 'scan')) {

                        } else {
                            $message = $type_desc . '支付' . ($price / 100) . '元';
                            $orderNum = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                            $this->mqtt($v->device_no, $v->device_type, $message, $orderNum, $config_id, $type, $price);
                        }
                    }

                    // 畅立收Q038
                    if ($v->device_type == "v_cls_q038") {
                        $VConfig = VConfig::where('config_id', $config_id)
                            ->select('tencent_cloud_secretid', 'tencent_cloud_secretkey', 'cls_product_id', 'cls_prefix')
                            ->first();
                        if (!$VConfig) {
                            $VConfig = VConfig::where('config_id', '1234')
                                ->select('tencent_cloud_secretid', 'tencent_cloud_secretkey', 'cls_product_id', 'cls_prefix')
                                ->first();
                        }

                        $secretId = $VConfig->tencent_cloud_secretid;
                        $secretKey = $VConfig->tencent_cloud_secretkey;
                        $productId = $VConfig->cls_product_id;
                        $deviceName = $v->device_no;
                        $money = ($price / 100);
                        $msg = $VConfig->cls_prefix . $money .'元，' . '赠送您一张优惠券，下次消费自动抵扣';
                        $channel = 0;
                        $msgid = $store_id . date('YmdHis', time());

                        try {
                            $cred = new Credential($secretId, $secretKey);
                            $httpProfile = new HttpProfile();
                            $httpProfile->setEndpoint("iotcloud.tencentcloudapi.com");
                            $clientProfile = new ClientProfile();
                            $clientProfile->setHttpProfile($httpProfile);
                            $client = new IotcloudClient($cred, "", $clientProfile);
                            $req = new PublishMessageRequest();
                            $params = array(
                                "Topic" => $productId . '/' . $deviceName . '/data',
                                "Payload" => "{\"msg\":\"$msg\",\"channel\":$channel,\"money\":\"$money\",\"msgid\":\"$msgid\"}",
                                "ProductId" => $productId,
                                "DeviceName" => $deviceName
                            );
                            $req->fromJsonString(json_encode($params));
                            $client->PublishMessage($req);
                        } catch(TencentCloudSDKException $e) {
                            Log::info($e->getMessage().' | '.$e->getFile().' | '.$e->getLine());
                        }
                    }
                }
            }
        } catch (\Exception $ex) {
            Log::info('播报设备666-error');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
        }
    }


    public function mqtt($device_id, $device_type, $message, $orderNum, $config_id, $type, $price)
    {
        try {
            $MqttConfig = MqttConfig::where('config_id', $config_id)->first();
            if (!$MqttConfig) {
                $MqttConfig = MqttConfig::where('config_id', '1234')->first();
            }

            if (!$MqttConfig) {
                return false;
            }
            $server = $MqttConfig->server;
            $port = $MqttConfig->port;
            $mq_group_id = $MqttConfig->group_id;
            $username = "Signature|" . $MqttConfig->access_key_id . "|" . $MqttConfig->instance_id . "";
            $str = '' . $MqttConfig->group_id . '@@@' . $device_id . '1' . '';
            $key = $MqttConfig->access_key_secret;
            $str = mb_convert_encoding($str, "UTF-8");
            $password = base64_encode(hash_hmac("sha1", $str, $key, true));
            $client_id = $mq_group_id . '@@@' . $device_id;
            $server_client_id = $mq_group_id . '@@@' . $device_id . '1';
            $topic = $MqttConfig->topic . "/p2p/" . $client_id;

            $content = json_encode([
                'message' => $message,
                'orderNum' => $orderNum,
                'type' => $type, //1 支付宝 2 微信支付 3京东支付 4 银联闪付  5 积分抵扣  6.会员卡
                'price' => '' . $price . ''//分
            ]);

            $mqtt = new AliBaseController($server, $port, $server_client_id);
            if ($mqtt->connect(false, NULL, $username, $password)) {
                $mqtt->publish($topic, $content, 1);
                //$mqtt->close();
            } else {

            }
        } catch (\Exception $ex) {
            Log::info('mqtt推送6666-errors');
            Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
        }
    }


}
