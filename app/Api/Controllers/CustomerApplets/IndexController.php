<?php

namespace App\Api\Controllers\CustomerApplets;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{

    public function get(){
        return response()->json([
            'status' => 200,
            'msg'    => "请求成功"
        ]);
    }
}
