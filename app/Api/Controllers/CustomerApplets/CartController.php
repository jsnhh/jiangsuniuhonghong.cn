<?php

namespace App\Api\Controllers\CustomerApplets;

use Illuminate\Http\Request;
use App\Api\Controllers\CustomerApplets\BaseController;
use App\Models\GoodCart;

class CartController extends BaseController
{
    /**
     * 获取购物车信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGoodCartInfo(Request $request){

        $requestAllData = $request->all();

        $goodCartModel = new GoodCart();

        $goodCartModelResult = $goodCartModel -> getGoodCartInfo($requestAllData['user_id'],$requestAllData['store_id']);

        if($goodCartModelResult['status'] == 200){
            return $this->responseDataJson($goodCartModelResult['status'],"请求成功",$goodCartModelResult['message']);
        }else{
            return $this->responseDataJson($goodCartModelResult['status'],$goodCartModelResult['message']);
        }

    }

    /**
     * 添加购物车信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addGoodCart(Request $request){

        $requestAllData = $request->all();

        $goodCartModel = new GoodCart();

        $goodCartModelResult = $goodCartModel -> addGoodCart($requestAllData['user_id'],$requestAllData['store_id'],$requestAllData['good_id'],$requestAllData['num'],isset($requestAllData['standard']) ? $requestAllData['standard'] : "");

        if($goodCartModelResult['status'] == 200){
            return $this->responseDataJson($goodCartModelResult['status'],"请求成功",$goodCartModelResult['message']);
        }else{
            return $this->responseDataJson($goodCartModelResult['status'],$goodCartModelResult['message']);
        }
    }


    /**
     * 清空购物车信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function clearGoodCart(Request $request){

        $requestAllData      = $request->all();

        $goodCartModel       = new GoodCart();

        $goodCartModelResult = $goodCartModel -> clearCart($requestAllData['user_id'],$requestAllData['store_id']);

        if($goodCartModelResult['status'] == 200){
            return $this->responseDataJson($goodCartModelResult['status'],"请求成功","");
        }else{
            return $this->responseDataJson($goodCartModelResult['status'],$goodCartModelResult['message']);
        }
    }

}
