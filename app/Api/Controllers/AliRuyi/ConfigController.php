<?php


namespace App\Api\Controllers\AliRuyi;

use App\Api\Controllers\BaseController;
use App\Models\AlipaySpiConfig;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ConfigController extends BaseController
{
    //支付宝如意配置
    public function alipaySpiConfig(Request $request)
    {
        try {
            $user = $this->parseToken();
            $data = $request->except(['token', 'type']);
            $config_type = $request->get('config_type', '01');
            $alipay_gateway = $request->get('alipay_gateway', 'https://openapi.alipay.com/gateway.do');
            $callback = $request->get('callback', '');
            $type = $request->get('type', '2'); //1：添加修改，2：查询

            $user = User::where('id', $user->user_id)->first();

            $config_id = $user->config_id;//配置的id
            $AlipaySpiConfig = AlipaySpiConfig::where('config_id', $config_id)
                ->where('config_type', $config_type)
                ->first();
            //查询
            if ($type == '2') {
                if ($AlipaySpiConfig) {
                    return json_encode(['status' => 1, 'data' => $AlipaySpiConfig]);
                } else {
                    return json_encode(['status' => 2, 'data' => [], 'message' => '请配置参数']);
                }
            }
            //添加修改
            if ($type == '1') {
                $check_data = [
                    'app_id' => '应用appid',
                    'app_name' => '应用名称',
                    'rsa_private_key' => '应用私钥PrivateKey',
                    'alipay_rsa_public_key' => '支付宝应用公钥',
                ];

                $check = $this->check_required($request->except(['token']), $check_data);
                if ($check) {
                    return json_encode([
                        'status' => 2,
                        'message' => $check
                    ]);
                }

                $data['config_id'] = $config_id;
                $data['config_type'] = $config_type;
                $data['app_id'] = trim($data['app_id']);
                $data['app_name'] = trim($data['app_name']);
                $data['rsa_private_key'] = trim($data['rsa_private_key']);
                $data['alipay_rsa_public_key'] = trim($data['alipay_rsa_public_key']);
                $data['callback'] = $callback;
                $data['alipay_gateway'] = $alipay_gateway;

                if ($AlipaySpiConfig) {
                    $AlipaySpiConfig->update($data);
                    $AlipaySpiConfig->save();
                } else {
                    AlipaySpiConfig::create($data);
                }
            }
            return json_encode(['status' => 1, 'message' => '添加成功', 'data' => $data]);

        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage()]);
        }
    }
}