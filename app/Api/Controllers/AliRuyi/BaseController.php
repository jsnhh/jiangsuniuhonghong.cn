<?php
namespace App\Api\Controllers\AliRuyi;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class BaseController extends Controller
{
    // 表单提交字符集编码
    public $postCharset = "UTF-8";
    public $fileCharset = "UTF-8";
    public $format = "json";

//    public $alipayPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmE9hcPC8ca2sbZpp0TWN4cWCwoZz6P9bicM5UO8/tkEHMGcoI2RtwPV7Js1gu8mofMxSE1R8BSAY9e/LXKD+MjfqrEtpsEs6wD+j9E9CznXvY1x/ya5oOYli8u9axU7+7ntnvF422v+uKFuO/0+A5T+SubMXXIjKPVUF5uVIEqX1DSryY3vsXSQr5U9YdXPuoLsLVg1Q131OHn2uhG+uPv+0K2zt/0bz0SPddooiWSRMkh0fOgQxjmXP2641324nP3BwuXp75HzH2S2R66qhUDHvY1AeBFMEeM2lOR8qssc4lBF7+B7UCoTXD2yZ0k3EmFqhKxQCUv7eo13bWYtpwwIDAQAB";
//    public $rsaPrivateKey = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC32NutMZQJUndrO5qUGo6boHRyrB/S+9xy89vZCuK+O6ttRjH0goiKiFXWb5Wdctbv7y/1vSA69B4VSN+V+1bPkSoQnPGb0Pub+6dxbbyY1xO4THAizc5rcNBrr2ixDZPbLLAN7l39C6yFd+D0Bsid/5pAIzndGOLn6x/2GE+WwY7M2/2wQW2azDadw3tZFZfheM/HwxZcJKS0jAz0+pJeEwLZeH2gIYALJ8upAH6JUVF+s+VSlsznubShj9QfFmkNOk5VUl4fhYS8tNkjtXSw+n+l0aF02iANvw9Qah4cpFYW6ztubTefLeX3fWpBTrYUJs+3YE/2ElpFXkSaCZuzAgMBAAECggEBALQogG0iLm1MMNYWXI5Rz/nVrQdKgQ0h3o+PRJtWPYgptP2qUf3HhMIsg+FVC4p1JQeOnrLzGIhs/KXi+adh21ogaNZK1cDZdBcQ7q8K2Ev2R87MqP6SK3vB25wLEk6efYvEKOod8mCsde184iaAF0wtVB0R5/wwDPszv+hLPoL9lR+RpIP4wuj378GI7qOjYZXDPIDpEW2R082QHNnSfvoc1KFkqCn07Gz/HGvvm8JNM97CmYPm0BSc160uZjIxldYUkYQLtU70daswrOeuvtL2G2lOLMr1TBEqD3H5IdQ0P70HvMmP/+nftTLWK1uIkqfBIZQ3IuM+FopVqKKI/RECgYEA2XM2WbwaAVm2th5ZBrAMbpPDXgzlyXRfXpl4JmnIOPdDUOCxeHXdZIFA6LhY5O9Y7o7FPMK/Usx2ZBS++FLT4jbTo/YO3yXE7gOgAntuCBOnHE5ZhvK5+AECnPVz3EVtFH2NGqsgdev5PS0S+R8MId5sSc2hAlG6MLKkZbDntycCgYEA2HCatcP2y8erdXlcsdqPVsoIwx54/70MvY7QB8PHbczn65IzOP2qFpqnVVCsaqD1SuKDT0kaDdowYRGUOzuDy9LT648+jDkV5fo649/CjBMY9Q35R3GjcOIdji/ICtOAG5b+d/F7AZ1lFknncpHt0v2k3WmEaDx4A0W2X/+FLpUCgYAQlU7BzSyWicb7YJkdf3OTQ0WU+JrVoEBdsIqPM2YHoiPKGTivnR2DOrA+j0CIRRCdMNot5hQ9NkaZH1PpEDgEenXtfShpMDIyjKg5SDx99TG7YOZG+l/gYANZAQcS92B4PZLzQ7W4CIT7e2KFCS8drh7IChyLVrxCVn1fG/yv5QKBgG/Flt+PF4Ac2NqNleTy7Vrwcp3wuqO2yUGK4rtfyzlUu9/XXERuGswZiZhxVNirzoChgRYMtju5FYrLPTVpgqJbeb0H+FEJmdlt48OR5jHYGudSFA1lDfZZjtHCsxeEKYAV9cuTFOa8lVtxRujPm3V+Lwl4WDxMHd3yvj/NAMYRAoGAczn/wpUMsTtQEN6rYgeY82+4f3V6aox/4FFAgA4mmRhdRL5SRbw3QTWPuiKYPjGlMEyQXRpHvIfEJpCqRu5Is20D7Te/k5ZdsmoWmbTw1ZGCpMRnaiWj2GdfaAJTv5NxL40vnMJ7zJYZT59PxOZtvhenlrMe55syuldw4WM5XKc=";

    public $alipayPublicKey = "";
    public $rsaPrivateKey ="";

    public $sign_type = "RSA2";

    public $app_cert_path = "/cert/ruyi/appCertPublicKey_2021002161685372.crt";
    public $alipay_cert_path = "/cert/ruyi/alipayCertPublicKey_RSA2.crt";
    public $alipay_root_cert_path = "/cert/ruyi/alipayRootCert.crt";


    public function spi_config($config_id)
    {
        $spiConfig = new AlipaySpiConfigController();
        $config = $spiConfig->AlipaySpiConfig($config_id);
        return $config;
    }


    /**
     * 通讯数据加密
     */
    public static function encode($data)
    {
        return $data = base64_encode(json_encode((array)$data));
    }


    /**
     * 通讯数据解密
     */
    public static function decode($data)
    {
        return json_decode(base64_decode((string)$data), true);
    }

    /**
     * 校验必填字段
     */
    public function check_required($check, $data)
    {
        $rules = [];
        $attributes = [];
        foreach ($data as $k => $v) {
            $rules[$k] = 'required';
            $attributes[$k] = $v;
        }
        $messages = [
            'required' => ':attribute不能为空',
        ];
        $validator = Validator::make($check, $rules,
            $messages, $attributes);
        $message = $validator->getMessageBag();
        return $message->first();
    }


}
