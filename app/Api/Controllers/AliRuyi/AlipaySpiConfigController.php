<?php


namespace App\Api\Controllers\AliRuyi;


use App\Http\Controllers\Controller;
use App\Models\AlipaySpiConfig;

class AlipaySpiConfigController extends Controller
{
    public function AlipaySpiConfig($config_id)
    {
        $config_type = '01';
        $config = AlipaySpiConfig::where('config_id', $config_id)
            ->where('config_type', $config_type)
            ->first();

        return $config;
    }

}