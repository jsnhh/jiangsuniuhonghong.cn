<?php


namespace App\Api\Controllers\AliRuyi;

use App\Api\Controllers\Merchant\PayBaseController;
use AlipayCommerceIotDeviceBindRequest;
use AlipayMerchantOrderExternalPaychannelSyncRequest;
use AopCertClient;
use App\Models\Merchant;
use App\Models\Order;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class RuyiPayController extends BaseController
{



    /**
     * 如意设备绑定API : alipay.commerce.iot.device.bind
     */
    public function deviceBind($request)
    {
        try {
            Log::info("---调用绑定设备接口----result:".json_encode($request));
            $store_id = $request['storeId']; //系统门店id
            $device_sn = $request['device_sn'];                //可选,设备序列号：SN
            $source = $request['source'];                      //受理商户的ISV在支付宝的pid
            $external_id = $request['external_id'];            //商户编号，由ISV定义，需要保证在ISV下唯一
            $merchant_id_type = $request['merchant_id_type'];  //区分商户ID类型，直连商户填写direct，间连商户填写indirect
            $merchant_id = $request['merchant_id'];            //商户角色id。对于直连开店场景，填写商户pid；对于间连开店场景，填写商户smid。
            $shop_id = $request['shop_id'];                    //店铺ID
            $bind_user_id = $request['bind_user_id'];          //店铺PID
            $terminal_bind_info = [
                'storeId'=>$store_id
            ];  //可选
            $storeObj = Store::where('store_id', $store_id)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }

            // $config = $storeObj->config_id;
            $config = 1234;

            $AlipaySpiConfigObj = $this->spi_config($config);
            if(!$AlipaySpiConfigObj){
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->app_id) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意应用ID,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->rsa_private_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '开发者私钥,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->alipay_rsa_public_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝公钥,未配置'
                ]);
            }
            $app_id = $AlipaySpiConfigObj->app_id;

            $rsa_private_key = $AlipaySpiConfigObj->rsa_private_key;
            $alipay_rsa_public_key = $AlipaySpiConfigObj->alipay_rsa_public_key;
            $aopCert = new AopCertClient();

            $aopCert->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aopCert->appId = $app_id;
            $aopCert->rsaPrivateKey = $rsa_private_key;
            //调用getPublicKey从支付宝公钥证书中提取公钥
            $aopCert->alipayrsaPublicKey = $aopCert->getPublicKey($_SERVER['DOCUMENT_ROOT'].$this->alipay_cert_path);
            $aopCert->format = $this->format;
            $aopCert->postCharset = $this->postCharset;
            $aopCert->signType = $this->sign_type;
            //调用getCertSN获取证书序列号
            $aopCert->appCertSN = $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path);
            //调用getRootCertSN获取支付宝根证书序列号
            $aopCert->alipayRootCertSN = $aopCert->getRootCertSN($_SERVER['DOCUMENT_ROOT'].$this->alipay_root_cert_path);

            if($merchant_id_type == 'direct'){
                $data_re_arr = array(
                    "app_type" => 'MINI_APP',
                    "mini_app_id" => 'RUYI_LITE',
                    "device_id_type" => 'SN',
                    "supplier_id" => '201901111100635561',    //设备供应商ID，请填写“201901111100635561”
                    //"biz_tid" => $biz_tid,                  //设备Id
                    "device_sn" => $device_sn,              //设备的SN
                    "source" => $source,                    //受理商户的ISV在支付宝的pid
                    "external_id" => $external_id,          //商户编号，由ISV定义，需要保证在ISV下唯一
                    "merchant_id_type" => $merchant_id_type,    //直连商户填写direct，间连商户填写indirect
                    "merchant_id" => $merchant_id,          //对于直连开店场景，填写商户pid；对于间连开店场景，填写商户smid
                    "shop_id" => $shop_id,                  //店铺ID
                    "spi_app_id" => $app_id,     //isv应用id，isv的标识；用于标识ISV身份
                    "terminal_bind_info" => json_encode(['storeId'=>$store_id,'external_app_id'=>$app_id]),
                );
            }else{
                $data_re_arr = array(
                    "app_type" => 'MINI_APP',
                    "mini_app_id" => 'RUYI_LITE',
                    "device_id_type" => 'SN',
                    "supplier_id" => '201901111100635561',    //设备供应商ID，请填写“201901111100635561”
                    //"biz_tid" => $biz_tid,                  //设备Id
                    "device_sn" => $device_sn,              //设备的SN
                    "source" => $source,                    //受理商户的ISV在支付宝的pid
                    "external_id" => $external_id,          //商户编号，由ISV定义，需要保证在ISV下唯一
                    "merchant_id_type" => $merchant_id_type,    //直连商户填写direct，间连商户填写indirect
                    "merchant_id" => $merchant_id,          //对于直连开店场景，填写商户pid；对于间连开店场景，填写商户smid
                    "shop_id" => $shop_id,                  //店铺ID
                    "spi_app_id" => $app_id,     //isv应用id，isv的标识；用于标识ISV身份
                    "pid" => $bind_user_id,     //merchant_id_type为间连indirect时，smid关联的pid
                    "terminal_bind_info" => json_encode(['storeId'=>$store_id,'external_app_id'=>$app_id]),
                );
            }

            $data_re = json_encode($data_re_arr);
            $request = new AlipayCommerceIotDeviceBindRequest();
            $request->setBizContent($data_re);
            $result = $aopCert->execute($request);
            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;
            //成功
            if (!empty($resultCode) && $resultCode == 10000) {
                Log::info("---deviceBind----绑定成功。");
                return [
                    'status' => 1,
                    'message' => '绑定成功',
                    'data' => $data_re_arr,
                ];
            } else {
                Log::info("---deviceBind----绑定失败:".$result->$responseNode->sub_msg . $result->$responseNode->sub_code);
                return [
                    'status' => 2,
                    'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                    'result_code' => $resultCode,
                ];

            }

        }catch (\Exception $e){
            return ['status' => 202,'message' => $e->getMessage()];
        }

    }
    /**
     * IoT设备-商户-店铺绑定查询:alipay.commerce.iot.device.bind.query
     */
    public function bindQuery(Request $request)
    {
        try {
            Log::info("---调用店铺绑定查询接口成功----result:".json_encode($request));
            $data = $request->getContent();
            $data = json_decode($data, true); //array
            if (!$data) {
                $data = $request->all(); //获取请求参数
            }
            $device_sn = $request->get('device_sn', '');
            $terminalBindInfo = json_decode($data['terminal_bind_info'],true); //array
            $storeId = $terminalBindInfo['storeId'];
            $storeObj = Store::where('store_id', $storeId)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }

            $config = 1234;

            $AlipaySpiConfigObj = $this->spi_config($config);
            if(!$AlipaySpiConfigObj){
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->app_id) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意应用ID,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->rsa_private_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '开发者私钥,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->alipay_rsa_public_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝公钥,未配置'
                ]);
            }

            $app_id = $AlipaySpiConfigObj->app_id;
            $rsa_private_key = $AlipaySpiConfigObj->rsa_private_key;
            $alipay_rsa_public_key = $AlipaySpiConfigObj->alipay_rsa_public_key;
            $aopCert = new AopCertClient();

            $aopCert->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aopCert->appId = $app_id;
            $aopCert->rsaPrivateKey =  $rsa_private_key;
            //调用getPublicKey从支付宝公钥证书中提取公钥
            $aopCert->alipayrsaPublicKey = $aopCert->getPublicKey($_SERVER['DOCUMENT_ROOT'].$this->alipay_cert_path);
            $aopCert->format = $this->format;
            $aopCert->postCharset = $this->postCharset;
            $aopCert->signType = $this->sign_type;
            //调用getCertSN获取证书序列号
            $aopCert->appCertSN = $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path);
            //调用getRootCertSN获取支付宝根证书序列号
            $aopCert->alipayRootCertSN = $aopCert->getRootCertSN($_SERVER['DOCUMENT_ROOT'].$this->alipay_root_cert_path);

            $data_re_arr = array(
                "app_type" => 'MINI_APP',
                "mini_app_id" => 'RUYI_LITE',
                "device_id_type" => 'SN',
                "supplier_id" => '201901111100635561',    //设备供应商ID，请填写“201901111100635561”
                //"biz_tid" => $biz_tid,                  //设备Id
                "device_sn" => $device_sn,                //设备的SN
//                "application_id" => "2021002161685372",   //isv应用id，isv的标识；用于标识ISV身份
            );

            $data_re = json_encode($data_re_arr);

            $request = new \AlipayCommerceIotDeviceBindQueryRequest();
            $request->setBizContent($data_re);

            $result = $aopCert->execute($request);

            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;

            //成功
            if (!empty($resultCode) && $resultCode == 10000) {
                return json_encode([
                    'status' => 1,
                    'message' => '查询成功',
                    'data' => $result->$responseNode->bind_info_list,
                ]);
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                    'result_code' => $resultCode,
                ]);
            }

        }catch (\Exception $e){
            return ['status' => 202,'message' => $e->getMessage()];
        }

    }
    /**
     * IoT设备-商户-店铺解绑:alipay.commerce.iot.device.unbind
     */
    public function deviceUnbind($request)
    {
        try {
            Log::info("---调用设备解绑接口---:result:".json_encode($request));


            // $device_sn = $request->get('device_sn', '');
            // $source =$request->get('source', '');
            // $external_id =$request->get('external_id', '');
            // $merchant_id_type =$request->get('merchant_id_type', '');
            // $merchant_id =$request->get('merchant_id', '');
            // $shop_id =$request->get('shop_id', '');
            // $storeId=$request->get('storeId', '');

            $device_sn = $request['device_sn'];
            $source = $request['source'];
            $external_id = $request['external_id'];
            $merchant_id_type = $request['merchant_id_type'];
            $merchant_id = $request['merchant_id'];
            $shop_id = $request['shop_id'];
            $storeId=$request['storeId'];

            $storeObj = Store::where('store_id', $storeId)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return [
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ];
            }

            $config = 1234;

            $AlipaySpiConfigObj = $this->spi_config($config);
            if(!$AlipaySpiConfigObj){
                return [
                    'status' => 2,
                    'message' => '支付宝如意未配置'
                ];
            }
            if (!$AlipaySpiConfigObj->app_id) {
                return [
                    'status' => 2,
                    'message' => '支付宝如意应用ID,未配置'
                ];
            }
            if (!$AlipaySpiConfigObj->rsa_private_key) {
                return [
                    'status' => 2,
                    'message' => '开发者私钥,未配置'
                ];
            }
            if (!$AlipaySpiConfigObj->alipay_rsa_public_key) {
                return [
                    'status' => 2,
                    'message' => '支付宝公钥,未配置'
                ];
            }

            $app_id = $AlipaySpiConfigObj->app_id;
            $rsa_private_key = $AlipaySpiConfigObj->rsa_private_key;
            $alipay_rsa_public_key = $AlipaySpiConfigObj->alipay_rsa_public_key;

            $aopCert = new AopCertClient();

            $aopCert->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aopCert->appId = $app_id;
            $aopCert->rsaPrivateKey = $rsa_private_key;
            //调用getPublicKey从支付宝公钥证书中提取公钥
            $aopCert->alipayrsaPublicKey = $aopCert->getPublicKey($_SERVER['DOCUMENT_ROOT'].$this->alipay_cert_path);
            $aopCert->format = $this->format;
            $aopCert->postCharset = $this->postCharset;
            $aopCert->signType = $this->sign_type;

            //调用getCertSN获取证书序列号
            $aopCert->appCertSN = $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path);
            //调用getRootCertSN获取支付宝根证书序列号
            $aopCert->alipayRootCertSN = $aopCert->getRootCertSN($_SERVER['DOCUMENT_ROOT'].$this->alipay_root_cert_path);


            $data_re_arr = array(
                "app_type" => 'MINI_APP',
                "mini_app_id" => 'RUYI_LITE',
                "device_id_type" => 'SN',
                "supplier_id" => '201901111100635561',    //设备供应商ID，请填写“201901111100635561”
                //"biz_tid" => $biz_tid,                  //设备Id
                "device_sn" => $device_sn,              //设备的SN
                "source" => $source,                    //受理商户的ISV在支付宝的pid
                "external_id" => $external_id,          //商户编号，由ISV定义，需要保证在ISV下唯一
                "merchant_id_type" => $merchant_id_type,    //直连商户填写direct，间连商户填写indirect
                "merchant_id" => $merchant_id,          //对于直连开店场景，填写商户pid；对于间连开店场景，填写商户smid
                "shop_id" => $shop_id,                  //店铺ID
                "terminal_bind_info" => json_encode(['storeId'=>$storeId,'external_app_id'=>$app_id]),
            );

            $data_re = json_encode($data_re_arr);
            $request = new \AlipayCommerceIotDeviceUnbindRequest();
            $request->setBizContent($data_re);

            $result = $aopCert->execute($request);
            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            $resultCode = $result->$responseNode->code;

            //成功
            if (!empty($resultCode) && $resultCode == 10000) {
                return [
                    'status' => 1,
                    'message' => '解绑成功',
                    'data' => $data_re_arr,
                ];
            } else {
                return [
                    'status' => 2,
                    'message' => $result->$responseNode->sub_msg . $result->$responseNode->sub_code,
                    'result_code' => $resultCode,
                ];
            }

        }catch (\Exception $e){
            return ['status' => 202,'message' => $e->getMessage()];
        }

    }
    /**
     * 查询收银员列表：spi.alipay.commerce.fmcgsaascashier.batchquery
     */
    public function batchQueryCashier(Request $request)
    {
        try {
            //Log::info("-----调用查询收银员列表接口成功-----result:".json_decode($request));
            //获取请求参数
            $data = $request->getContent();

            $data = json_decode($data, true); //array
            if (!$data) {
                $data = $request->all(); //获取请求参数
            }
            //收银终端设备序列号SN

            $terminalId = $data['terminal_id'];
            $terminalBindInfo = json_decode($data['terminal_bind_info'],true); //array

            $storeId = $terminalBindInfo['storeId'];
            $appId = $terminalBindInfo['external_app_id'];
            $storeObj = Store::where('store_id', $storeId)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }

            $config = 1234;

            $AlipaySpiConfigObj = $this->spi_config($config);
            if(!$AlipaySpiConfigObj){
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->app_id) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意应用ID,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->rsa_private_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '开发者私钥,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->alipay_rsa_public_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝公钥,未配置'
                ]);
            }

            $app_id = $AlipaySpiConfigObj->app_id;
            $rsa_private_key = $AlipaySpiConfigObj->rsa_private_key;
            $alipay_rsa_public_key = $AlipaySpiConfigObj->alipay_rsa_public_key;


            //验签代码
            $aopCert = new AopCertClient();
            $aopCert->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aopCert->appId = $app_id;
            $aopCert->rsaPrivateKey = $rsa_private_key;
            //调用getPublicKey从支付宝公钥证书中提取公钥
            $aopCert->alipayrsaPublicKey = $aopCert->getPublicKey($_SERVER['DOCUMENT_ROOT'].$this->alipay_cert_path);
            $aopCert->format = $this->format;
            $aopCert->postCharset = $this->postCharset;
            $aopCert->signType = $this->sign_type;
            //调用getCertSN获取证书序列号
            $aopCert->appCertSN = $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path);
            //调用getRootCertSN获取支付宝根证书序列号
            $aopCert->alipayRootCertSN = $aopCert->getRootCertSN($_SERVER['DOCUMENT_ROOT'].$this->alipay_root_cert_path);

            $certSN= $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path); //获取证书序列号
            $flag = $aopCert->rsaCheckV1($data, $this->alipayPublicKey, $this->sign_type);

            if (!$flag) {
                $data_response = [
                    "code" => '40004',
                    "msg" => 'Business Failed',
                    "sub_code" => 'ISV-VERIFICATION-FAILED',
                    "sub_msg" => '验签失败',
                ];

                //sdk内封装的签名方法
                $sign = $aopCert->alonersaSign(json_encode($data_response),$rsa_private_key,$this->sign_type,false);

                $result = [
                    "sign" => $sign,
                    "response" => $data_response,
                    "app_cert_sn" => $certSN,
                ];

                return json_encode($result);
            }

            //业务处理

            if ($storeId) {
                $where[] = ['merchant_stores.store_id', '=', $storeId];
            }

            $cashierObj = DB::table('merchant_stores')
                ->join('merchants','merchant_stores.merchant_id', '=', 'merchants.id')
                ->where($where)
                ->select('merchants.id as cashier_id', 'merchants.name as cashier_name')->get();

            $cashier = [];
            if($cashierObj){
                foreach ($cashierObj as $key => $value){
                    $cashier[$key] = $value;
                }
            }

            $data_response = [
                "code" => '10000',
                "msg" => 'Success',
                "cashier_list" => $cashier,
            ];

            //sdk内封装的签名方法
            $sign = $aopCert->alonersaSign(json_encode($data_response),$rsa_private_key,$this->sign_type,false);

            $result = [
                "sign" => $sign,
                "response" => $data_response,
                "app_cert_sn" => $certSN,
            ];

            //Log::info("-----batchQueryCashier----result:".json_encode($result));
            return json_encode($result);

        }catch (\Exception $e){
            return ['status' => 202,'message' => $e->getMessage()];
        }

    }


    /**
     * 查询收银员状态：spi.alipay.commerce.fmcgsaascashier.query
     */
    public function query(Request $request)
    {
        try {

            //Log::info("----调用查询收银员状态接口成功---:".json_encode($request));
            //获取请求参数
            $data = $request->getContent();

            $data = json_decode($data, true); //array
            if (!$data) {
                $data = $request->all(); //获取请求参数
            }

            //收银终端设备序列号SN

            $terminalId = $data['terminal_id'];
            $terminalBindInfo = json_decode($data['terminal_bind_info'],true); //array

            $storeId = $terminalBindInfo['storeId'];
            $appId = $terminalBindInfo['external_app_id'];
            $storeObj = Store::where('store_id', $storeId)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }

            $config = 1234;

            $AlipaySpiConfigObj = $this->spi_config($config);
            if(!$AlipaySpiConfigObj){
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->app_id) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意应用ID,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->rsa_private_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '开发者私钥,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->alipay_rsa_public_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝公钥,未配置'
                ]);
            }

            $app_id = $AlipaySpiConfigObj->app_id;
            $rsa_private_key = $AlipaySpiConfigObj->rsa_private_key;
            $alipay_rsa_public_key = $AlipaySpiConfigObj->alipay_rsa_public_key;


            //验签代码
            $aopCert = new AopCertClient();
            $aopCert->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aopCert->appId = $app_id;
            $aopCert->rsaPrivateKey = $rsa_private_key;
            //调用getPublicKey从支付宝公钥证书中提取公钥
            $aopCert->alipayrsaPublicKey = $aopCert->getPublicKey($_SERVER['DOCUMENT_ROOT'].$this->alipay_cert_path);
            $aopCert->format = $this->format;
            $aopCert->postCharset = $this->postCharset;
            $aopCert->signType = $this->sign_type;
            //调用getCertSN获取证书序列号
            $aopCert->appCertSN = $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path);
            //调用getRootCertSN获取支付宝根证书序列号
            $aopCert->alipayRootCertSN = $aopCert->getRootCertSN($_SERVER['DOCUMENT_ROOT'].$this->alipay_root_cert_path);

            $certSN= $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path); //获取证书序列号

            $flag = $aopCert->rsaCheckV1($data, $this->alipayPublicKey, $this->sign_type);

            if (!$flag) {
                $data_response = [
                    "code" => '40004',
                    "msg" => 'Business Failed',
                    "sub_code" => 'ISV-VERIFICATION-FAILED',
                    "sub_msg" => '验签失败',
                ];

                //sdk内封装的签名方法
                $sign = $aopCert->alonersaSign(json_encode($data_response),$rsa_private_key,$this->sign_type,false);

                $result = [
                    "sign" => $sign,
                    "response" => $data_response,
                    "app_cert_sn" => $certSN,
                ];

                return json_encode($result);
            }

            //业务处理

            $data_response = [
                "code" => '10000',
                "msg" => 'Success',
                "status" => '1',        //签到状态，0失败，1成功
                "cashier_id" => "123",
            ];

            //sdk内封装的签名方法

            $sign = $aopCert->alonersaSign(json_encode($data_response),$rsa_private_key,$this->sign_type,false);

            $result = [
                "sign" => $sign,
                "response" => $data_response,
                "app_cert_sn" => $certSN,
            ];

            return json_encode($result);

        }catch (\Exception $e){
            return ['status' => 202,'message' => $e->getMessage()];
        }

    }

    /**
     * 收银员签到：spi.alipay.commerce.fmcgsaascashier.sign
     */
    public function sign(Request $request)
    {
        try {
            //Log::info("-----调用收银员签到接口成功----result:".json_encode($request));
            //获取请求参数
            $data = $request->getContent();

            $data = json_decode($data, true); //array
            if (!$data) {
                $data = $request->all(); //获取请求参数
            }
            //收银终端设备序列号SN

            $terminalId = $data['terminal_id'];
            $terminalBindInfo = json_decode($data['terminal_bind_info'],true); //array

            $storeId = $terminalBindInfo['storeId'];
            $appId = $terminalBindInfo['external_app_id'];
            $storeObj = Store::where('store_id', $storeId)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }

            $config = 1234;

            $AlipaySpiConfigObj = $this->spi_config($config);
            if(!$AlipaySpiConfigObj){
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->app_id) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意应用ID,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->rsa_private_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '开发者私钥,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->alipay_rsa_public_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝公钥,未配置'
                ]);
            }

            $app_id = $AlipaySpiConfigObj->app_id;
            $rsa_private_key = $AlipaySpiConfigObj->rsa_private_key;
            $alipay_rsa_public_key = $AlipaySpiConfigObj->alipay_rsa_public_key;

            //验签代码
            $aopCert = new AopCertClient();
            $aopCert->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aopCert->appId = $app_id;
            $aopCert->rsaPrivateKey = $rsa_private_key;
            //调用getPublicKey从支付宝公钥证书中提取公钥
            $aopCert->alipayrsaPublicKey = $aopCert->getPublicKey($_SERVER['DOCUMENT_ROOT'].$this->alipay_cert_path);
            $aopCert->format = $this->format;
            $aopCert->postCharset = $this->postCharset;
            $aopCert->signType = $this->sign_type;
            //调用getCertSN获取证书序列号
            $aopCert->appCertSN = $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path);
            //调用getRootCertSN获取支付宝根证书序列号
            $aopCert->alipayRootCertSN = $aopCert->getRootCertSN($_SERVER['DOCUMENT_ROOT'].$this->alipay_root_cert_path);

            $certSN= $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path); //获取证书序列号
            $flag = $aopCert->rsaCheckV1($data, $this->alipayPublicKey, $this->sign_type);

            if (!$flag) {
                $data_response = [
                    "code" => '40004',
                    "msg" => 'Business Failed',
                    "sub_code" => 'ISV-VERIFICATION-FAILED',
                    "sub_msg" => '验签失败',
                ];

                //sdk内封装的签名方法
                $sign = $aopCert->alonersaSign(json_encode($data_response),$rsa_private_key,$this->sign_type,false);

                $result = [
                    "sign" => $sign,
                    "response" => $data_response,
                    "app_cert_sn" => $certSN,
                ];

                return json_encode($result);
            }

            //业务处理
            $data_response = [
                "code" => '10000',
                "msg" => 'Success',
                "status" => '1',
//                "fail_reason" => "已经在其他设备上绑定",
            ];

            //sdk内封装的签名方法
            $sign = $aopCert->alonersaSign(json_encode($data_response),$rsa_private_key,$this->sign_type,false);

            $result = [
                "sign" => $sign,
                "response" => $data_response,
                "app_cert_sn" => $certSN,
            ];

            return json_encode($result);

        }catch (\Exception $e){
            return ['status' => 202,'message' => $e->getMessage()];
        }

    }

    /**
     * 收银员签退：spi.alipay.commerce.fmcgsaascashier.unsign
     */
    public function unsign(Request $request)
    {
        try {
            //Log::info("-----调用收银员签退接口成功----result:".json_decode($request));
            //获取请求参数
            $data = $request->getContent();

            $data = json_decode($data, true); //array
            if (!$data) {
                $data = $request->all(); //获取请求参数
            }
            //收银终端设备序列号SN

            $terminalId = $data['terminal_id'];
            $terminalBindInfo = json_decode($data['terminal_bind_info'],true); //array

            $storeId = $terminalBindInfo['storeId'];
            $appId = $terminalBindInfo['external_app_id'];
            $storeObj = Store::where('store_id', $storeId)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }

            $config = 1234;

            $AlipaySpiConfigObj = $this->spi_config($config);
            if(!$AlipaySpiConfigObj){
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->app_id) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意应用ID,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->rsa_private_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '开发者私钥,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->alipay_rsa_public_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝公钥,未配置'
                ]);
            }

            $app_id = $AlipaySpiConfigObj->app_id;
            $rsa_private_key = $AlipaySpiConfigObj->rsa_private_key;
            $alipay_rsa_public_key = $AlipaySpiConfigObj->alipay_rsa_public_key;

            //验签代码
            $aopCert = new AopCertClient();
            $aopCert->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aopCert->appId = $storeId;
            $aopCert->rsaPrivateKey = $rsa_private_key;
            //调用getPublicKey从支付宝公钥证书中提取公钥
            $aopCert->alipayrsaPublicKey = $aopCert->getPublicKey($_SERVER['DOCUMENT_ROOT'].$this->alipay_cert_path);
            $aopCert->format = $this->format;
            $aopCert->postCharset = $this->postCharset;
            $aopCert->signType = $this->sign_type;
            //调用getCertSN获取证书序列号
            $aopCert->appCertSN = $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path);
            //调用getRootCertSN获取支付宝根证书序列号
            $aopCert->alipayRootCertSN = $aopCert->getRootCertSN($_SERVER['DOCUMENT_ROOT'].$this->alipay_root_cert_path);

            $certSN= $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path); //获取证书序列号
            $flag = $aopCert->rsaCheckV1($data, $this->alipayPublicKey, $this->sign_type);

            if (!$flag) {
                $data_response = [
                    "code" => '40004',
                    "msg" => 'Business Failed',
                    "sub_code" => 'ISV-VERIFICATION-FAILED',
                    "sub_msg" => '验签失败',
                ];

                //sdk内封装的签名方法
                $sign = $aopCert->alonersaSign(json_encode($data_response),$rsa_private_key,$this->sign_type,false);

                $result = [
                    "sign" => $sign,
                    "response" => $data_response,
                    "app_cert_sn" => $certSN,
                ];

                return json_encode($result);
            }

            //业务处理
            $data_response = [
                "code" => '10000',
                "msg" => 'Success',
                "status" => '1',
//                "fail_reason" => "已经在其他设备上绑定",
            ];

            //sdk内封装的签名方法
            $sign = $aopCert->alonersaSign(json_encode($data_response),$rsa_private_key,$this->sign_type,false);

            $result = [
                "sign" => $sign,
                "response" => $data_response,
                "app_cert_sn" => $certSN,
            ];

            return json_encode($result);

        }catch (\Exception $e){
            return ['status' => 202,'message' => $e->getMessage()];
        }

    }

    /**
     * 交易流水信息查询接口：spi.alipay.commerce.billdetail.query
     */
    public function billDetail(Request $request)
    {
        try {

            //获取请求参数
            $data = $request->getContent();

            $data = json_decode($data, true);
            if (!$data) {
                $data = $request->all(); //获取请求参数
            };
            Log::info("-----billDetaildata----result:".json_encode($data));
            $cashier_id = $data['cashier_id'];
            $shopId = $data['shop_id'];			//外部门店id
            $date = $data['date'];				//不传默认当日账单
            $page_num=$data['page_num'];//
            //收银终端设备序列号SN
            $terminalId = $data['terminal_id'];
            $terminalBindInfo = json_decode($data['terminal_bind_info'],true); //array

            $storeId = $terminalBindInfo['storeId'];
            $appId = $terminalBindInfo['external_app_id'];
            $storeObj = Store::where('store_id', $storeId)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }

            $config = 1234;

            $AlipaySpiConfigObj = $this->spi_config($config);
            if(!$AlipaySpiConfigObj){
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->app_id) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意应用ID,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->rsa_private_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '开发者私钥,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->alipay_rsa_public_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝公钥,未配置'
                ]);
            }

            $app_id = $AlipaySpiConfigObj->app_id;
            $rsa_private_key = $AlipaySpiConfigObj->rsa_private_key;
            $alipay_rsa_public_key = $AlipaySpiConfigObj->alipay_rsa_public_key;

            //验签代码
            $aopCert = new AopCertClient();
            $aopCert->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aopCert->appId = $app_id;
            $aopCert->rsaPrivateKey =$rsa_private_key;
            //调用getPublicKey从支付宝公钥证书中提取公钥
            $aopCert->alipayrsaPublicKey = $aopCert->getPublicKey($_SERVER['DOCUMENT_ROOT'].$this->alipay_cert_path);
            $aopCert->format = $this->format;
            $aopCert->postCharset = $this->postCharset;
            $aopCert->signType = $this->sign_type;
            //调用getCertSN获取证书序列号
            $aopCert->appCertSN = $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path);
            //调用getRootCertSN获取支付宝根证书序列号
            $aopCert->alipayRootCertSN = $aopCert->getRootCertSN($_SERVER['DOCUMENT_ROOT'].$this->alipay_root_cert_path);

            $certSN= $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path); //获取证书序列号
            $flag = $aopCert->rsaCheckV1($data, $this->alipayPublicKey, $this->sign_type);

            if (!$flag) {
                $data_response = [
                    "code" => '40004',
                    "msg" => 'Business Failed',
                    "sub_code" => 'ISV-VERIFICATION-FAILED',
                    "sub_msg" => '验签失败',
                ];

                //sdk内封装的签名方法
                $sign = $aopCert->alonersaSign(json_encode($data_response),$rsa_private_key,$this->sign_type,false);

                $result = [
                    "sign" => $sign,
                    "response" => $data_response,
                    "app_cert_sn" => $certSN,
                ];

                return json_encode($result);
            }

            //业务处理
            //获取当天订单总条数
            $total_size = DB::table('orders')
                ->where('device_id',$terminalId)
                ->where('store_id',$storeId)
                ->where('merchant_id',$cashier_id)
                ->where('pay_time','>=',$date)
                ->where(function($query){
                    $query->where('pay_status','1')
                        ->orWhere(function($query){
                            $query->where('pay_status', '6');
                        });
                })
                ->count('id');
            //Log::info("-----total_size----result:". $total_size);
            //获取当天订单详情
            $order = Order::where('device_id',$terminalId)
                ->where('store_id',$storeId)
                ->where('merchant_id',$cashier_id)
                ->where('pay_time','>=',$date)
                ->where(function($query){
                    $query->where('pay_status','1')
                        ->orWhere(function($query){
                            $query->where('pay_status', '6');
                        });
                })->orderByRaw('pay_time')->skip($page_num-1)->take(1)
                ->first();
            //Log::info("-----order----result:".json_encode($order));

            $array=[];
            if(!$order){
                $data_response['code']='40004';
                $data_response['msg']='Business Failed';
                $data_response['sub_code']='ORDER_NOT_EXIST';
                $data_response['sub_msg']='无交易记录';
            }else{
                //判断支付状态
                if($order->pay_status=='1'){
                    $array['order_type']='PAY';
                }else if($order->pay_status=='6'){
                    $array['order_type']='REFUND';
                    $array['refund_amount']=$order->refund_amount;
                    $array['channel_refund_no']=$order->trade_no;
                    $array['isv_refund_no']=$order->out_trade_no;
                    $array['merchant_refund_no']=$order->other_no;
                }
                $array['isv_order_no']=$order->out_trade_no;
                //判断支付通道
                if($order->ways_source=='weixin'){
                    $array['channel_type']='002';
                }
                if($order->ways_source=='alipay'){
                    $array['channel_type']='001';
                }
                $array['total_amount']=$order->total_amount;
                $array['order_time']=$order->pay_time;
                $array['terminal_id']=$order->device_id;
                $array['channel_order_no']=$order->trade_no;
                $array['receipt_amount']=$order->total_amount;
                $array['buyer_pay_amount']=$order->buyer_pay_amount;
                $array['cashier_id']=$order->merchant_id;
                $array['cashier_name']=$order->merchant_name;
                $array['subject']=$order->remark;
            }
            Log::info("-----array----result:". json_encode($array));
            $data_response = [
                "code" => '10000',
                "msg" => 'Success',
                "total_size" => $total_size,    //查询数据获取
                "order_list" => $array,
            ];

            //sdk内封装的签名方法
            $sign = $aopCert->alonersaSign(json_encode($data_response),$rsa_private_key,$this->sign_type,false);

            $result = [
                "sign" => $sign,
                "response" => $data_response,
                "app_cert_sn" => $certSN,
            ];

            return json_encode($result);

        }catch (\Exception $e){
            return ['status' => 202,'message' => $e->getMessage()];
        }

    }

    /**
     * 交易统计查询接口：spi.alipay.commerce.billstatistics.query
     */
    public function billStatistics(Request $request)
    {
        try {
            //Log::info("-----调用交易统计查询接口成功----result:".json_encode($request));
            //获取请求参数
            $data = $request->getContent();
            $data = json_decode($data, true);
            if (!$data) {
                $data = $request->all(); //获取请求参数
            }

            $cashier_id = $data['cashier_id'];
            $shopId = $data['shop_id'];			//外部门店id
            $date = $data['date'];				//不传默认当日账单

            //收银终端设备序列号SN
            $terminalId = $data['terminal_id'];
            $terminalBindInfo = json_decode($data['terminal_bind_info'],true); //array

            $storeId = $terminalBindInfo['storeId'];
            $appId = $terminalBindInfo['external_app_id'];
            $storeObj = Store::where('store_id', $storeId)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }

            $config = 1234;

            $AlipaySpiConfigObj = $this->spi_config($config);
            if(!$AlipaySpiConfigObj){
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->app_id) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意应用ID,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->rsa_private_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '开发者私钥,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->alipay_rsa_public_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝公钥,未配置'
                ]);
            }

            $app_id = $AlipaySpiConfigObj->app_id;
            $rsa_private_key = $AlipaySpiConfigObj->rsa_private_key;
            $alipay_rsa_public_key = $AlipaySpiConfigObj->alipay_rsa_public_key;

            //验签代码
            $aopCert = new AopCertClient();
            $aopCert->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aopCert->appId = $app_id;
            $aopCert->rsaPrivateKey = $rsa_private_key;
            //调用getPublicKey从支付宝公钥证书中提取公钥
            $aopCert->alipayrsaPublicKey = $aopCert->getPublicKey($_SERVER['DOCUMENT_ROOT'].$this->alipay_cert_path);
            $aopCert->format = $this->format;
            $aopCert->postCharset = $this->postCharset;
            $aopCert->signType = $this->sign_type;
            //调用getCertSN获取证书序列号
            $aopCert->appCertSN = $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path);
            //调用getRootCertSN获取支付宝根证书序列号
            $aopCert->alipayRootCertSN = $aopCert->getRootCertSN($_SERVER['DOCUMENT_ROOT'].$this->alipay_root_cert_path);

            $certSN= $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path); //获取证书序列号
            $flag = $aopCert->rsaCheckV1($data, $this->alipayPublicKey, $this->sign_type);

            if (!$flag) {
                $data_response = [
                    "code" => '40004',
                    "msg" => 'Business Failed',
                    "sub_code" => 'ISV-VERIFICATION-FAILED',
                    "sub_msg" => '验签失败',
                ];

                //sdk内封装的签名方法
                $sign = $aopCert->alonersaSign(json_encode($data_response),$rsa_private_key,$this->sign_type,false);

                $result = [
                    "sign" => $sign,
                    "response" => $data_response,
                    "app_cert_sn" => $certSN,
                ];

                return json_encode($result);
            }
            //业务处理
            //获取商家总金额
            $receipt_amount = DB::table('orders')
                ->where('device_id',$terminalId)
                ->where('store_id',$storeId)
                ->where('merchant_id',$cashier_id)
                ->where('pay_time','>=',$date)
                ->where(function($query){
                    $query->where('pay_status','1')
                        ->orWhere(function($query){
                            $query->where('pay_status', '6');
                        });
                })
                ->sum('receipt_amount');
            // Log::info("-----receipt_amount----result:". $receipt_amount);
            //获取商家总笔数
            $receipt_count = DB::table('orders')
                ->where('device_id',$terminalId)
                ->where('store_id',$storeId)
                ->where('merchant_id',$cashier_id)
                ->where('pay_time','>=',$date)
                ->where(function($query){
                    $query->where('pay_status','1')
                        ->orWhere(function($query){
                            $query->where('pay_status', '6');
                        });
                })
                ->count('id');
            //Log::info("-----receipt_count----result:". $receipt_count);
            //获取商家退款总金额
            $refund_amount = DB::table('orders')
                ->where('device_id',$terminalId)
                ->where('store_id',$storeId)
                ->where('merchant_id',$cashier_id)
                ->where('pay_time','>=',$date)
                ->where('pay_status','6')
                ->sum('refund_amount');
            //Log::info("-----refund_amount----result:". $refund_amount);
            //获取商家退款总笔数
            $refund_count = DB::table('orders')
                ->where('device_id',$terminalId)
                ->where('store_id',$storeId)
                ->where('merchant_id',$cashier_id)
                ->where('pay_time','>=',$date)
                ->where('pay_status','6')
                ->count('id');
            //Log::info("-----refund_count----result:". $refund_count);
            //计算商家收入结余
            $total_income=bcsub($receipt_amount,$refund_amount,2);
            $data_response = [
                "code" => '10000',
                "msg" => 'Success',
                "statistic_time" => $date,
                "receipt_amount" => $receipt_amount,
                "receipt_count" =>  $receipt_count,
                "refund_amount" => $refund_amount,
                "refund_count" => $refund_count,
                "total_income" => $total_income
            ];

            //sdk内封装的签名方法
            $sign = $aopCert->alonersaSign(json_encode($data_response),$rsa_private_key,$this->sign_type,false);

            $result = [
                "sign" => $sign,
                "response" => $data_response,
                "app_cert_sn" => $certSN,
            ];

            return json_encode($result);

        }catch (\Exception $e){
            return ['status' => 202,'message' => $e->getMessage()];
        }

    }

    /**
     * IoT接入外部isv标准支付接口：spi.alipay.merchant.order.commonisv.pay
     */
    public function orderPay(Request $request)
    {
        try {
            // Log::info("-----调用IoT接入外部isv标准支付接口成功----result:".json_encode($request));
            //获取请求参数
            $data = $request->getContent();
            $data = json_decode($data, true);
            if (!$data) {
                $data = $request->all(); //获取请求参数
            }
            $merchantOrderNo = $data['merchant_order_no'];//商户订单号
            $authCode = $data['auth_code'];			//付款码
            $shopId = $data['shop_id'];			//外部门店id
            $payTimeout = $data['pay_timeout'];		//订单有效时间，需要控制未支付超时关闭，单位为分钟，默认1
            $totalAmount = $data['total_amount']; 	//订单金额
            $cashier_id=$data['cashier_id'];//收银员id
            //$subject=$data['subject'];
            //收银终端设备序列号SN
            $terminalId = $data['terminal_id'];
            $terminalBindInfo = json_decode($data['terminal_bind_info'],true); //array

            $storeId = $terminalBindInfo['storeId'];
            $appId = $terminalBindInfo['external_app_id'];

            $storeObj = Store::where('store_id', $storeId)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }

            $config = 1234;

            $AlipaySpiConfigObj = $this->spi_config($config);
            if(!$AlipaySpiConfigObj){
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->app_id) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意应用ID,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->rsa_private_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '开发者私钥,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->alipay_rsa_public_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝公钥,未配置'
                ]);
            }

            $app_id = $AlipaySpiConfigObj->app_id;
            $rsa_private_key = $AlipaySpiConfigObj->rsa_private_key;
            $alipay_rsa_public_key = $AlipaySpiConfigObj->alipay_rsa_public_key;
            //验签代码
            $aopCert = new AopCertClient();
            $aopCert->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aopCert->appId = $app_id;
            $aopCert->rsaPrivateKey = $rsa_private_key;
            //调用getPublicKey从支付宝公钥证书中提取公钥
            $aopCert->alipayrsaPublicKey = $aopCert->getPublicKey($_SERVER['DOCUMENT_ROOT'].$this->alipay_cert_path);
            $aopCert->format = $this->format;
            $aopCert->postCharset = $this->postCharset;
            $aopCert->signType = $this->sign_type;
            //调用getCertSN获取证书序列号
            $aopCert->appCertSN = $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path);
            //调用getRootCertSN获取支付宝根证书序列号
            $aopCert->alipayRootCertSN = $aopCert->getRootCertSN($_SERVER['DOCUMENT_ROOT'].$this->alipay_root_cert_path);

            $certSN= $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path); //获取证书序列号
            $flag = $aopCert->rsaCheckV1($data, $this->alipayPublicKey, $this->sign_type);
            if (!$flag) {
                $data_response = [
                    "code" => '40004',
                    "msg" => 'Business Failed',
                    "sub_code" => 'ISV-VERIFICATION-FAILED',
                    "sub_msg" => '验签失败',
                ];

                //sdk内封装的签名方法
                $sign = $aopCert->alonersaSign(json_encode($data_response),$rsa_private_key,$this->sign_type,false);

                $result = [
                    "sign" => $sign,
                    "response" => $data_response,
                    "app_cert_sn" => $certSN,
                ];

                return json_encode($result);
            }


            $attach_params = [
                "storeId" => $storeId,
                "terminal_id" => $terminalId,
                "merchant_order_no" => $merchantOrderNo,
            ];
            //支付处理
            $merchant=Merchant::where('id',$cashier_id)
                ->select('name')
                ->first();
            $merchant_name=$merchant->name;//获取收银员姓名
            $shop = Store::where('store_id', $storeId)
                ->select('store_name')
                ->first();
            // Log::info("-----shop----:". json_encode($shop));
            $shop_name=$shop->store_name;//获取商户名称
            $pay_data=[
                'config_id'=>'',
                'merchant_id'=>$cashier_id,
                'merchant_name'=>$merchant_name,
                'code'=>$authCode,
                'total_amount'=>$totalAmount,
                'shop_price'=>$totalAmount,
                'remark'=>'如意设备支付',
                //'device_id'=>$terminalId,
                'device_id'=>'如意设备',
                'device_type'=>'ruyi',
                'shop_name'=>$shop_name,
                'shop_desc'=>$shop_name,
                'store_id'=>$storeId,
                'other_no'=>$merchantOrderNo,
                'out_trade_no'=>$merchantOrderNo,
                'pay_method'=>''
            ];
            // Log::info("-----pay_data----:". json_encode($pay_data));
            $payBase=new PayBaseController();
            //调用支付接口
            $resp=$payBase->scan_pay_public($pay_data);
            $resp_data = json_decode($resp,true);
            //业务处理
            $data_response = [];
            if( $resp_data['status']=='1'){
                $order=Order::where('out_trade_no',$resp_data['data']['out_trade_no'])
                    ->first();//查询订单详情

                $data_response['code']='10000';
                $data_response['msg']='Success';
                $data_response['merchant_order_no']=$order['other_no'];
                $data_response['isv_order_no']=$order['out_trade_no'];
                $data_response['channel_order_no']=$order['trade_no'];
                //判断支付状态
                if($order['pay_status']=='1'){
                    $data_response['order_state']='ORDER_SUCCESS';
                }
                if($order['pay_status']=='2'){
                    $data_response['order_state']='WAIT_PAY';
                }
                if($order['pay_status']=='3'){
                    $data_response['order_state']='ORDER_CLOSED';
                }
                //判断支付通道
                if($order['ways_type']=='weixin'){
                    $data_response['channel_type']='002';
                }
                if($order['ways_type']=='alipay'){
                    $data_response['channel_type']='001';
                }
                $data_response['pay_time']=$order['pay_time'];
                $data_response['total_amount']=$order['total_amount'];
                $data_response['receipt_amount']=$order['receipt_amount'];
                $data_response['buyer_pay_amount']=$order['buyer_pay_amount'];
                $data_response['buyer_user_id']=$order['buyer_id'];
                $data_response['buyer_account_name']=$order['buyer_logon_id'];
                $data_response['attach_params']=json_encode($attach_params);
            }elseif ($resp_data['result_code']=='40004'){
                $data_response['code']='40004';
                $data_response['msg']='Business Failed';
                $data_response['sub_code']='AUTH_CODE_INVALID';
                $data_response['sub_msg']='付款码错误';
            }

//             //业务处理
            //sdk内封装的签名方法
            $sign = $aopCert->alonersaSign(json_encode($data_response),$rsa_private_key,$this->sign_type,false);

            $result = [
                "sign" => $sign,
                "response" => $data_response,
                "app_cert_sn" => $certSN
            ];
            // Log::info("-----orderPay----result:".json_encode($result));
            return json_encode($result);

        }catch (\Exception $e){
            return ['status' => 202,'message' => $e->getMessage()];
        }

    }

    /**
     * IoT接入外部isv标准查询接口：spi.alipay.merchant.order.commonisv.query
     */
    public function orderQuery(Request $request)
    {
        try {
            //Log::info("-----调用IoT接入外部isv标准查询接口成功----result:".json_encode($request));
            //获取请求参数
            $data = $request->getContent();
            $data = json_decode($data, true);
            if (!$data) {
                $data = $request->all(); //获取请求参数
            }

            $merchantOrderNo = $request->get('merchant_order_no', '');//商户订单号
            //收银终端设备序列号SN
            $terminalBindInfo = json_decode($data['terminal_bind_info'],true); //array

            $storeId = $terminalBindInfo['storeId'];
            $appId = $terminalBindInfo['external_app_id'];
            $storeObj = Store::where('store_id', $storeId)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }

            $config = 1234;

            $AlipaySpiConfigObj = $this->spi_config($config);
            if(!$AlipaySpiConfigObj){
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->app_id) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意应用ID,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->rsa_private_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '开发者私钥,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->alipay_rsa_public_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝公钥,未配置'
                ]);
            }

            $app_id = $AlipaySpiConfigObj->app_id;
            $rsa_private_key = $AlipaySpiConfigObj->rsa_private_key;
            $alipay_rsa_public_key = $AlipaySpiConfigObj->alipay_rsa_public_key;

            //验签代码
            $aopCert = new AopCertClient();
            $aopCert->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aopCert->appId = $app_id;
            $aopCert->rsaPrivateKey = $rsa_private_key;
            //调用getPublicKey从支付宝公钥证书中提取公钥
            $aopCert->alipayrsaPublicKey = $aopCert->getPublicKey($_SERVER['DOCUMENT_ROOT'].$this->alipay_cert_path);
            $aopCert->format = $this->format;
            $aopCert->postCharset = $this->postCharset;
            $aopCert->signType = $this->sign_type;
            //调用getCertSN获取证书序列号
            $aopCert->appCertSN = $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path);
            //调用getRootCertSN获取支付宝根证书序列号
            $aopCert->alipayRootCertSN = $aopCert->getRootCertSN($_SERVER['DOCUMENT_ROOT'].$this->alipay_root_cert_path);

            $certSN= $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path); //获取证书序列号
            $flag = $aopCert->rsaCheckV1($data, $this->alipayPublicKey, $this->sign_type);

            if (!$flag) {
                $data_response = [
                    "code" => '40004',
                    "msg" => 'Business Failed',
                    "sub_code" => 'ISV-VERIFICATION-FAILED',
                    "sub_msg" => '验签失败',
                ];

                //sdk内封装的签名方法
                $sign = $aopCert->alonersaSign(json_encode($data_response),$rsa_private_key,$this->sign_type,false);

                $result = [
                    "sign" => $sign,
                    "response" => $data_response,
                    "app_cert_sn" => $certSN,
                ];

                return json_encode($result);
            }

            $attach_params = [
                "storeId" => $storeId,
                "merchantOrderNo" => $merchantOrderNo,
            ];

            //业务处理

            $orders=Order::where('other_no',$merchantOrderNo)
                ->first();
            if (empty($orders)) {
                $data_response['code']='40004';
                $data_response['msg']='Business Failed';
                $data_response['sub_code']='ORDER_NOT_EXIST';
                $data_response['sub_msg']='订单不存在';
            } else {
                    $data_response['code']='10000';
                    $data_response['msg']='Success';
                    $data_response['merchant_order_no']=$orders->other_no;
                    $data_response['isv_order_no']=$orders->out_trade_no;
                    $data_response['channel_order_no']=$orders->trade_no;
                    $data_response['terminal_id']=$orders->device_id;
                    //判断支付状态
                    if($orders->pay_status=='1'){
                        $data_response['order_state']='ORDER_SUCCESS';
                    }
                    if($orders->pay_status=='2'){
                        $data_response['order_state']='WAIT_PAY';
                    }
                    if($orders->pay_status=='3'){
                        $data_response['order_state']='ORDER_CLOSED';
                    }
                    $data_response['total_amount']=$orders->total_amount;
                    $data_response['receipt_amount']=$orders->total_amount;
                    $data_response['buyer_pay_amount']=$orders->buyer_pay_amount;
                    //判断支付通道
                    if($orders->ways_type=='weixin'){
                        $data_response['channel_type']='002';
                    }
                    if($orders->ways_type=='alipay'){
                        $data_response['channel_type']='001';
                    }
                    $data_response['cashier_id']=$orders->merchant_id;
                    $data_response['cashier_name']=$orders->merchant_name;
                    $data_response['subject']=$orders->remark;
                    $data_response['pay_time']=$orders->pay_time;
                    $data_response['refund_process_amount']=$orders->refund_amount;
                    $data_response['refund_accept_amount']=$orders->refund_amount;
                    $data_response['refund_success_amount']=$orders->refund_amount;
                    $data_response['buyer_user_id']=$orders->buyer_id;
                    $data_response['buyer_account_name']=$orders->buyer_logon_id;
                    $data_response['attach_params']=json_encode($attach_params);

            }

            //sdk内封装的签名方法
            $sign = $aopCert->alonersaSign(json_encode($data_response),$rsa_private_key,$this->sign_type,false);

            $result = [
                "sign" => $sign,
                "response" => $data_response,
                "app_cert_sn" => $certSN
            ];

            return json_encode($result);

        }catch (\Exception $e){
            return ['status' => 202,'message' => $e->getMessage()];
        }

    }
    /**
     * 订单状态查询接口：spi.alipay.merchant.order.commonisv.bill.query
     */
    public function billQuery(Request $request)
    {
        try {
            // Log::info("-----调用订单状态查询接口接口成功----result:". json_encode($request));
            //获取请求参数
            $data = $request->getContent();
            $data = json_decode($data, true);
            if (!$data) {
                $data = $request->all(); //获取请求参数
            }
            Log::info("-----billQuerydata----result:". json_encode($data));
            $cashier_id = $data['cashier_id'];
            $shopId = $data['shop_id'];			//外部门店id
            $isv_order_no=$data['bill_merchant_no'];//isv_order_no订单号
            //收银终端设备序列号SN
            $terminalId = $data['terminal_id'];
            $terminalBindInfo = json_decode($data['terminal_bind_info'],true); //array

            $storeId = $terminalBindInfo['storeId'];
            $appId = $terminalBindInfo['external_app_id'];
            $storeObj = Store::where('store_id', $storeId)
                ->where('is_close', 0)
                ->where('is_delete', 0)
                ->first();
            if (!$storeObj) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在或状态异常,请联系管理员'
                ]);
            }

            $config = 1234;

            $AlipaySpiConfigObj = $this->spi_config($config);
            if(!$AlipaySpiConfigObj){
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->app_id) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝如意应用ID,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->rsa_private_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '开发者私钥,未配置'
                ]);
            }
            if (!$AlipaySpiConfigObj->alipay_rsa_public_key) {
                return json_encode([
                    'status' => 2,
                    'message' => '支付宝公钥,未配置'
                ]);
            }
            $app_id = $AlipaySpiConfigObj->app_id;
            $rsa_private_key = $AlipaySpiConfigObj->rsa_private_key;
            $alipay_rsa_public_key = $AlipaySpiConfigObj->alipay_rsa_public_key;
            $aopCert = new AopCertClient();

            $aopCert->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aopCert->appId =$app_id;
            $aopCert->rsaPrivateKey = $rsa_private_key;
            //调用getPublicKey从支付宝公钥证书中提取公钥
            $aopCert->alipayrsaPublicKey = $aopCert->getPublicKey($_SERVER['DOCUMENT_ROOT'].$this->alipay_cert_path);
            $aopCert->format = $this->format;
            $aopCert->postCharset = $this->postCharset;
            $aopCert->signType = $this->sign_type;
            //调用getCertSN获取证书序列号
            $aopCert->appCertSN = $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path);
            //调用getRootCertSN获取支付宝根证书序列号
            $aopCert->alipayRootCertSN = $aopCert->getRootCertSN($_SERVER['DOCUMENT_ROOT'].$this->alipay_root_cert_path);


            $certSN= $aopCert->getCertSN($_SERVER['DOCUMENT_ROOT'].$this->app_cert_path); //获取证书序列号
            $flag = $aopCert->rsaCheckV1($data, $this->alipayPublicKey, $this->sign_type);

            if (!$flag) {
                $data_response = [
                    "code" => '40004',
                    "msg" => 'Business Failed',
                    "sub_code" => 'ISV-VERIFICATION-FAILED',
                    "sub_msg" => '验签失败',
                ];

                //sdk内封装的签名方法
                $sign = $aopCert->alonersaSign(json_encode($data_response),$rsa_private_key,$this->sign_type,false);

                $result = [
                    "sign" => $sign,
                    "response" => $data_response,
                    "app_cert_sn" => $certSN,
                ];

                return json_encode($result);
            }
//            $attach_params = [
//                "storeId" => $storeId,
//                "merchantOrderNo" => $isv_order_no,
//            ];
            //业务处理
            $order=Order::where('store_id',$storeId)
                ->where('out_trade_no',$isv_order_no)
                ->orWhere('trade_no',$isv_order_no)
                ->first();//通过isv_order_no订单号查询订单信息
            $data_response=[];
            //Log::info("-----order----result:".json_encode($order));
            if(!$order){
                $data_response['code']= "40004";
                $data_response['msg']= "Business Failedd";
                $data_response['sub_code']= "NO_PERMISSION";
                $data_response['sub_msg']= "无权执行当前操作";
            }else{
                if($order->device_id==$terminalId){
                    $data_response['code']='10000';
                    $data_response['msg']='Success';
                    $data_response['merchant_order_no']=$order->other_no;
                    $data_response['isv_order_no']=$order->out_trade_no;
                    $data_response['channel_order_no']=$order->trade_no;
                    $data_response['terminal_id']=$order->device_id;
                    //判断支付状态
                    if($order->pay_status=='1'){
                        $data_response['order_state']='ORDER_SUCCESS';
                    }
                    if($order->pay_status=='2'){
                        $data_response['order_state']='WAIT_PAY';
                    }
                    if($order->pay_status=='3'){
                        $data_response['order_state']='ORDER_CLOSED';
                    }
                    if($order->pay_status=='6'){
                        $data_response['order_state']='REFUND_SUCCESS';
                    }
                    $data_response['total_amount']=$order->total_amount;
                    $data_response['receipt_amount']=$order->total_amount;
                    $data_response['buyer_pay_amount']=$order->buyer_pay_amount;
                    //判断支付通道
                    if($order->ways_type=='weixin'){
                        $data_response['channel_type']='002';
                    }
                    if($order->ways_type=='alipay'){
                        $data_response['channel_type']='001';
                    }
                    $data_response['cashier_id']=$order->merchant_id;
                    $data_response['cashier_name']=$order->merchant_name;
                    $data_response['subject']=$order->remark;
                    $data_response['pay_time']=$order->pay_time;
                    $data_response['refund_process_amount']=$order->refund_amount;
                    $data_response['refund_accept_amount']=$order->refund_amount;
                    $data_response['refund_success_amount']=$order->refund_amount;
                    $data_response['buyer_user_id']=$order->buyer_id;
                    $data_response['buyer_account_name']=$order->buyer_logon_id;
                }else{
                    $data_response['code']= "40004";
                    $data_response['msg']= "Business Failedd";
                    $data_response['sub_code']= "INVALID_QUERY_CHANNEL";
                    $data_response['sub_msg']= "非该设备该渠道发起的订单";
                }

                //$data_response['attach_params']=json_encode($attach_params);
            }
            //sdk内封装的签名方法
            $sign = $aopCert->alonersaSign(json_encode($data_response),$rsa_private_key,$this->sign_type,false);

            $result = [
                "sign" => $sign,
                "response" => $data_response,
                "app_cert_sn" => $certSN,
            ];
            //Log::info("-----response----result:".json_encode($result));
            return json_encode($result);

        }catch (\Exception $e){
            return ['status' => 202,'message' => $e->getMessage()];
        }

    }

}