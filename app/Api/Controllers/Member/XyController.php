<?php
namespace App\Api\Controllers\Member;


use App\Api\Controllers\BaseController;
use App\Models\MemberCzList;
use App\Models\MemberCzSet;
use App\Models\MemberTplWx;
use App\Models\MemberXyList;
use App\Models\MemberYxStoreid;
use App\Models\MerchantStore;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class XyController extends BaseController
{
    //营销活动列表
    public function yx_lists(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');

            $store_ids = [];
            if ($store_id && $store_id != "NULL") {
                $store_ids = [
                    [
                        'store_id' => $store_id,
                    ]
                ];
            } else {
                $MerchantStore = MerchantStore::where('merchant_id', $user->merchant_id)
                    ->select('store_id')
                    ->get();

                if (!$MerchantStore->isEmpty()) {
                    $store_ids = $MerchantStore->toArray();
                }
            }

            $yx_ids = MemberYxStoreid::whereIn('store_id', $store_ids)
                ->select('yx_id')
                ->get();

            if ($yx_ids->isEmpty()) {
                return json_encode(['status' => 2, 'message' => '没有找到相关营销活动']);

            }
            $yx_ids = $yx_ids->toArray();

            $obj = MemberXyList::whereIn('yx_id', $yx_ids);

            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }


    //营销活动删除
    public function yx_del(Request $request)
    {
        try {
            $user = $this->parseToken();//

            $yx_type = $request->get('yx_type');
            $yx_id = $request->get('yx_id');

            $check_data = [
                'yx_type' => '营销类型',
                'yx_id' => '营销id',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            //开启事务
            try {
                DB::beginTransaction();

                //
                MemberXyList::where('yx_id', $yx_id)->where('yx_type', $yx_type)->delete();
                MemberYxStoreid::where('yx_id', $yx_id)->where('yx_type', $yx_type)->delete();

                //充值
                MemberCzSet::where('yx_id', $yx_id)->where('yx_type', $yx_type)->delete();

                //满减

                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                return json_encode(['status' => 2, 'message' => $e->getMessage().' | '.$e->getFile().' | '.$e->getLine()]);
            }

            return json_encode([
                'status' => 1,
                'message' => '删除成功'
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }


    //充值类型列表
    public function cz_type(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');

            $check_data = [
                'store_id' => '门店ID'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $data = [
                [
                    'cz_type' => 1,
                    'cz_type_desc' => '普通充值'
                ],
                [
                    'cz_type' => 2,
                    'cz_type_desc' => '充值满送'
                ]
            ];

            return json_encode([
                'status' => 1,
                'message' => '数据成功',
                'data' => $data
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }


}
