<?php
namespace App\Api\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\WeChatMemberList;
use App\Models\WechatMemberMoneyList;
use App\Models\WechatMemberPointsList;
use App\Api\Controllers\BaseController;

class WechatMemberController extends BaseController
{
    /**
     * 编辑微信正式会员信息
     * @param Request $request
     * @return array
     */
    public function editWechatMember(Request $request)
    {
        $requestData = $request->all();

        // 参数校验
        $check_data = [
            'mb_id' => '会员mb_id',
        ];
        $check = $this->check_required($requestData, $check_data);
        if ($check) {
            return $this->sys_response(400001, $check);
        }

        $model                      = new WeChatMemberList();
        $wechatMemberMoneyList      = new WechatMemberMoneyList();
        $wechatMemberPointsList     = new WechatMemberPointsList();

        // 查询微信正式会员信息
        $wechatMemberInfo   = $model->getWechatMemberInfo($requestData);
        $user_money         = $wechatMemberInfo['mb_money'];            // 会员余额
        $mb_virtual_money   = $wechatMemberInfo['mb_virtual_money'];    // 会员虚拟金额
        $user_points        = $wechatMemberInfo['mb_points'];           // 会员积分
        $applet_openid      = $wechatMemberInfo['applet_openid'];       // 小程序openid
        $wechat_openid      = $wechatMemberInfo['wechat_openid'];       // 微信公众号openid

        $date_now = date('Y-m-d H:i:s', time());
        // 会员正式表更新参数
        $data_update = [
            'mb_nickname'   => $requestData['mb_nickname'],
            'mb_phone'      => $requestData['mb_phone'],
            'mb_ver'        => $requestData['mb_ver'],
            'mb_ver_desc'   => $requestData['mb_ver_desc'],
        ];

        // 开启事务
        try {
            DB::beginTransaction();

            // 更新微信正式会员余额
            if (isset($requestData['user_pay_money']) && !empty($requestData['user_pay_money'])) {
                // use_pay_money_type == 1 会员充值，use_pay_money_type == 2 会员消费
                if ($requestData['user_pay_money_type'] == 1) {
                    if (isset($requestData['user_pay_money_give']) && !empty($requestData['user_pay_money_give'])) {
                        $user_money = $user_money + $requestData['user_pay_money'] + $requestData['user_pay_money_give'];
                        $data_update['mb_virtual_money'] = $mb_virtual_money + $requestData['user_pay_money_give'];
                    } else {
                        $user_money = $user_money + $requestData['user_pay_money'];
                    }
                } else if ($requestData['user_pay_money_type'] == 2) {
                    if ($user_money < $requestData['user_pay_money']) {
                        Log::info("微信创建正式会员-更新会员-余额不足");
                    } else {
                        $user_money = $user_money - $requestData['user_pay_money'];
                    }
                }

                $data_update['mb_money'] = $user_money;
            }

            // 更新微信正式会员积分
            if (isset($requestData['user_use_points']) && !empty($requestData['user_use_points'])) {
                // user_use_points_type == 1 会员充值，user_use_points_type == 2 会员消费积分
                if ($requestData['user_use_points_type'] == 1) {
                    $user_points = $user_points + $requestData['user_use_points'];
                } else if ($requestData['user_use_points_type'] == 2) {
                    if ($user_points < $requestData['user_use_points']) {
                        Log::info("更新微信正式会员-更新会员-积分不足");
                    } else {
                        $user_points = $user_points - $requestData['user_use_points'];
                    }
                }

                $data_update['mb_points'] = $user_points;
            }

            $wechatMemberRes = $model->updateData($requestData['mb_id'], $data_update);

            // 会员余额消费、积分消费公共信息
            $data_common = [
                'store_id'          => $requestData['store_id'],
                'mb_id'             => $requestData['mb_id'],
                'applet_openid'     => $applet_openid,
                'wechat_openid'     => $wechat_openid,
                'created_at'        => $date_now,
                'updated_at'        => $date_now,
            ];

            // 修改会员消费记录表（会员余额支付才修改）
            if (isset($requestData['user_pay_money']) && !empty($requestData['user_pay_money'])) {
                $data_member_money                      = $data_common;
                $data_member_money['money']             = $requestData['user_pay_money'];
                $data_member_money['money_type']        = $requestData['user_pay_money_type'];

                $wechatMemberMoneyList->addData($data_member_money);
            }

            // 修改会员积分表
            if (isset($requestData['user_use_points']) && !empty($requestData['user_use_points'])) {
                $data_member_points                 = $data_common;
                $data_member_points['points']       = $requestData['user_use_points'];
                $data_member_points['points_type']  = $requestData['user_use_points_type'];

                $wechatMemberPointsList->addData($data_member_points);
            }

            DB::commit();
            Log::info("创建微信正式会员-操作成功");
            return $this->sys_response(200);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info("创建微信正式会员-操作失败");
            return $this->sys_response(40000);
        }

    }

}
