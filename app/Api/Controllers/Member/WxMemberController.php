<?php
namespace App\Api\Controllers\Member;


use App\Api\Controllers\BaseController;
use App\Models\MemberTplWx;
use App\Models\Store;
use Illuminate\Http\Request;

class WxMemberController extends BaseController
{

    //微信卡券模板设置
    public function wx_member_card(Request $request)
    {
        try {
            $token = $this->parseToken(); //
            $store_id = $request->get('store_id', '');
            $custom_a = $request->get('custom_a', '');
            $custom_b = $request->get('custom_b', '');
            $custom_c = $request->get('custom_c', '');
            $custom_d = $request->get('custom_d', '');

            $check_data = [
                'store_id' => '门店ID',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => '2',
                    'message' => $check
                ]);
            }

            $Store = Store::where('store_id', $store_id)
                ->select('config_id')
                ->first();
            if (!$Store) {
                return json_encode([
                    'status' => '2',
                    'message' => '门店ID不存在'
                ]);
            }

            $config_id = $Store->config_id;
            $in_data = [
                'store_id' => $store_id,
                'config_id' => $config_id,
                'custom_a' => $custom_a,
                'custom_b' => $custom_b,
                'custom_c' => $custom_c,
                'custom_d' => $custom_d,
                'wx_status' => 2,
                'wx_status_desc' => '审核中'
            ];
            $MemberTplWx = MemberTplWx::where('store_id', $store_id)->first();
            if ($MemberTplWx) {
                $MemberTplWx->update($in_data);
                $updateRes = $MemberTplWx->save();
                if ($updateRes) {
                    return json_encode([
                        'status' => '1',
                        'message' => '模版信息更新成功',
                        'data' => $in_data
                    ]);
                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => '模版信息更新失败',
                        'data' => $in_data
                    ]);
                }
            } else {
                $addRes = MemberTplWx::create($in_data);
                if ($addRes) {
                    return json_encode([
                        'status' => '1',
                        'message' => '模版信息创建成功，等待审核',
                        'data' => $in_data
                    ]);
                } else {
                    return json_encode([
                        'status' => '2',
                        'message' => '模版信息创建失败，等待审核',
                        'data' => $in_data
                    ]);
                }
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


}
