<?php
namespace App\Api\Controllers\Member;


use App\Api\Controllers\BaseController;
use App\Models\MemberJf;
use App\Models\MemberList;
use App\Models\WeChatMemberList;
use App\Models\MemberSetJf;
use App\Models\MemberYxStoreid;
use App\Models\MerchantStore;
use App\Models\Store;
use App\Models\StoreMonthOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SelectController extends BaseController
{

    //会员列表
    public function mb_lists(Request $request)
    {
        try {
            $user = $this->parseToken(); //
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');
            $mb_id = $request->get('mb_id', ''); //会员卡号，姓名，手机号
            $store_id = $request->get('store_id', '');
            $wx_openid = $request->get('wxOpenid', ''); //微信openid

            $check_data = [
                'store_id' => '门店ID'
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $where = [];
            $where[] = ['ali_user_id', '!=', ''];

            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
            }
            if ($time_start) {
                $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                $where[] = ['mb_time', '>=', $time_start];
            }
            if ($time_end) {
                $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                $where[] = ['mb_time', '<=', $time_end];
            }
            if ($mb_id) {
                $mb_id = trim($mb_id);
                if (strlen($mb_id) == 23) {
                    $where[] = ['mb_id', '=', $mb_id];
                } elseif (strlen($mb_id) == 11) {
                    $where[] = ['mb_phone', '=', $mb_id];
                } else {
                    $where[] = ['mb_name', 'like', '%' . $mb_id . '%'];
                }
            }
            if ($wx_openid) {
                $where[] = ['wx_openid', '=', $wx_openid];
            }

            $obj = MemberList::where($where);
            $this->t = $obj->count();
            $data = $this->page($obj)
                ->orderBy('updated_at', 'desc')
                ->get();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }

    // 微信正式会员列表
    public function wechatMemberLists(Request $request)
    {
        $requestData = $request->all();

        try {
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');
            $mb_id = $request->get('mb_id', ''); //会员卡号，姓名，手机号
            $store_id = $request->get('store_id', '');
            $wx_openid = $request->get('wxOpenid', ''); //微信openid

            // 参数校验
            $check_data = [
                'store_id'      => '门店号',
            ];
            $check = $this->check_required($requestData, $check_data);
            if ($check) {
                return $this->sys_response(400001, $check);
            }

            $where = [];

            if ($store_id) {
                $where[] = ['a.store_id', '=', $store_id];
            }
            if ($time_start) {
                $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                $where[] = ['a.mb_time', '>=', $time_start];
            }
            if ($time_end) {
                $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                $where[] = ['a.mb_time', '<=', $time_end];
            }
            if ($mb_id) {
                $mb_id = trim($mb_id);
                if (strlen($mb_id) == 23) {
                    $where[] = ['a.mb_id', '=', $mb_id];
                } elseif (strlen($mb_id) == 11) {
                    $where[] = ['a.mb_phone', '=', $mb_id];
                } else {
                    $where[] = ['a.mb_nickname', 'like', '%' . $mb_id . '%'];
                }
            }
            if ($wx_openid) {
                $where[] = ['a.wechat_openid', '=', $wx_openid];
            }

            $obj = DB::table('wechat_member_lists as a')
                ->leftjoin('wechat_member_money_list as b', function ($join) {
                    $join->on('a.mb_id', '=', 'b.mb_id')
                        ->where('b.money_type', '=', 2);
                })
                ->leftjoin('wechat_member_points_list as c', function ($join) {
                    $join->on('a.mb_id', '=', 'c.mb_id')
                        ->where('c.points_type', '=', 2);
                })
                ->selectRaw("a.*, sum(ifNull(b.money, 0)) as mb_used_money, sum(ifNull(c.points, 0)) as mb_used_points")
                ->groupBy('a.mb_id')
                ->where($where);

            $this->t = $obj->count();
            $data = $this->page($obj)
                ->orderBy('a.updated_at', 'desc')
                ->get();

            foreach ($data as $k => $v) {
                if (empty($v->mb_phone)) {
                    $v->mb_phone = '未绑定';
                }
                if (empty($v->mb_nickname)) {
                    $v->mb_nickname = '未设置';
                }
            }

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }

    // 微信正式会员信息
    public function wechatMemberInfo(Request $request)
    {
        $requestData = $request->all();

        // 会员返回数据
        $memberInfoRes = [];
        if (!isset($requestData['mb_id']) || empty($requestData['mb_id'])) {
            $memberInfoRes = [];
        }

        $model = new WeChatMemberList();

        $memberInfo = $model->getWechatMemberInfo($requestData);
        if (!empty($memberInfo)) {
            $memberInfoRes = [
                'mb_id'     => $memberInfo['mb_id'],
                'mb_money'  => $memberInfo['mb_money'],
                'mb_points' => $memberInfo['mb_points']
            ];
        }

        return $this->sys_response(200, "操作成功", $memberInfoRes);
    }


    //会员信息
    public function mb_infos(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $mb_id = $request->get('mb_id', '');
            $store_id = $request->get('store_id', '');
            $open_id = $request->get('open_id', '');
            $wx_openid = $request->get('wx_openid', '');
            $ali_user_id = $request->get('ali_user_id', '');
            $xcx_openid = $request->get('xcx_openid', '');

            $check_data = [
                'store_id' => '门店ID',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            if ($wx_openid == "" && $ali_user_id == "") {
                return json_encode([
                    'status' => 2,
                    'message' => '缺少用户ID-wx_openid/ali_user_id'
                ]);
            }

            $where = [];
            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
            }
            if ($mb_id) {
                $where[] = ['mb_id', '=', $mb_id];
            }
            if ($wx_openid) {
                $where[] = ['wx_openid', '=', $wx_openid];
            }
            if ($ali_user_id) {
                $where[] = ['ali_user_id', '=', $ali_user_id];
            }
            if ($xcx_openid) {
                $where[] = ['xcx_openid', '=', $xcx_openid];
            }

            $data = MemberList::where($where)->first();
            if (!$data || $data->mb_phone == "未设置") {
                return json_encode([
                    'status' => 1,
                    'message' => '数据返回成功',
                    'is_member' => "0",
                    'data' => []
                ]);
            }

            if ($data->mb_phone == "未绑定") {
                return json_encode([
                    'status' => 1,
                    'message' => '数据返回成功',
                    'is_member' => "0",
                    'data' => []
                ]);
            }

            $MemberSetJf = MemberSetJf::where('store_id', $store_id)
                ->select('dk_jf_m', 'dk_rmb')
                ->first();
            if ($MemberSetJf) {
                $mb_jf = $data->mb_jf; //用户积分
                //用户的积分一共可以抵扣多少钱
                $jf_dk_money = ($mb_jf / $MemberSetJf->dk_jf_m) * $MemberSetJf->dk_rmb;
                $jf_dk_money = number_format($jf_dk_money, 2, '.', '');
            }

            return json_encode(
                [
                    'status' => 1,
                    'message' => '数据返回成功',
                    'is_member' => "1",
                    'data' => $data,
                    'additional' => isset($jf_dk_money) ? $jf_dk_money : ''
                ]
            );


        } catch (\Exception $exception) {
            return json_encode(['status' => 2, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }


    //新增加会员统计
    public function mb_counts(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');//1 交易笔数 由大到小

            $check_data = [
                'store_id' => '门店ID',
            ];


            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }


            $all_count = MemberList::where('store_id', $store_id)
                ->select('id')
                ->get()
                ->count('id');

            $time_start1 = date('Y-m-d H:i:s', time());
            $time_end1 = date('Y-m-d 23:59:59', time());


            $day_count = MemberList::where('store_id', $store_id)
                ->where('created_at', '<=', $time_end1)
                ->where('created_at', '>=', $time_start1)
                ->select('id')
                ->get()
                ->count('id');


            return json_encode([
                    'status' => 1,
                    'message' => '查询成功',
                    'data' => [
                        'day_count' => $day_count,
                        'all_count' => $all_count,
                    ],
                ]
            );


        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }


    //积分信息查询
    public function jf(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');//1 交易笔数 由大到小

            $check_data = [
                'store_id' => '门店ID',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $MemberSetJf = MemberSetJf::where('store_id', $store_id)->first();
            if (!$MemberSetJf) {
                return json_encode([
                    'status' => 2,
                    'message' => '没有设置积分信息'
                ]);
            }

            return json_encode([
                    'status' => 1,
                    'message' => '返回数据成功',
                    'data' => $MemberSetJf,
                ]
            );
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }


    //积分设置
    public function set_jf(Request $request)
    {
        try {
            $user = $this->parseToken(); //
            $store_id = $request->get('store_id', ''); //

            $data = $request->except(['token']);

            $check_data = [
                'store_id' => '门店ID',
                'xf' => '消费送积分-消费',
                'xf_s_jf' => '消费送积分-积分',
                'dk_jf_m' => '积分抵扣-积分',
                'dk_rmb' => '积分抵扣-人民币元',
                'new_mb_s_jf' => '新开会员送积分',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' =>'2',
                    'message' => $check
                ]);
            }

            $MemberSetJf = MemberSetJf::where('store_id', $store_id)->first();
            if ($MemberSetJf) {
                $res = $MemberSetJf->update($data);
            } else {
                $res = MemberSetJf::create($data);
            }

            if ($res) {
                return json_encode([
                    'status' => '1',
                    'message' => '积分规则设置成功',
                    'data' => $request->except(['token'])
                ]);
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '积分规则设置失败',
                    'data' => $request->except(['token'])
                ]);
            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()
            ]);
        }
    }


    //积分领取条件查询-固定
    public function jflqtj(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');//1 交易笔数 由大到小

            $check_data = [
                'store_id' => '门店ID',
//                'xf' => '消费送积分-消费',
//                'xf_s_jf_' => '消费送积分-积分',
            ];


            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }


            return json_encode([
                    'status' => 1,
                    'message' => '查询成功',
                    'data' => [
                        [
                            'xf' => '1',
                            'xf_s_jf' => '1',
                            'xf_desc' => '消费1元送1积分'
                        ],
                        [
                            'xf' => '10',
                            'xf_s_jf' => '1',
                            'xf_desc' => '消费10元送1积分'
                        ],
                        [
                            'xf' => '100',
                            'xf_s_jf' => '1',
                            'xf_desc' => '消费100元送1积分'
                        ],

                    ],
                ]
            );


        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }


    //积分使用条件查询-固定
    public function jfsytj(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');//1 交易笔数 由大到小

            $check_data = [
                'store_id' => '门店ID',

            ];


            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }


            return json_encode([
                    'status' => 1,
                    'message' => '查询成功',
                    'data' => [
                        [
                            'dk_jf_m' => '10',
                            'dk_rmb' => '1',
                            'dk_desc' => '10积分抵扣1元'
                        ],
                        [
                            'dk_jf_m' => '100',
                            'dk_rmb' => '1',
                            'dk_desc' => '100积分抵扣1元'
                        ], [
                            'dk_jf_m' => '1000',
                            'dk_rmb' => '1',
                            'dk_desc' => '1000积分抵扣1元'
                        ],

                    ],
                ]
            );


        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }


    //新会员送积分查询-固定
    public function newmbsjf(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');//1 交易笔数 由大到小

            $check_data = [
                'store_id' => '门店ID',
            ];


            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }


            return json_encode([
                    'status' => 1,
                    'message' => '查询成功',
                    'data' => [
                        [
                            'new_mb_s_jf' => '1',
                            'new_mb_desc' => '新开会员送1积分'
                        ],
                        [
                            'new_mb_s_jf' => '10',
                            'new_mb_desc' => '新开会员送10积分'
                        ],
                        [
                            'new_mb_s_jf' => '100',
                            'new_mb_desc' => '新开会员送100积分'
                        ],

                    ],
                ]
            );


        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }


    //会员积分明细查询
    public function mb_jfinfo(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');//1 交易笔数 由大到小
            $mb_id = $request->get('mb_id', '');//1 交易笔数 由大到小

            $check_data = [
                'store_id' => '门店ID',
                'mb_id' => '会员ID',
            ];


            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $where = [];

            if ($store_id) {
                $where[] = ['store_id', '=', $store_id];
            }

            if ($mb_id) {
                $where[] = ['mb_id', '=', $mb_id];
            }

            $obj = MemberJf::where($where);
            $this->t = $obj->count();
            $data = $this->page($obj)->get();

            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);


        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }


    //会员积分统计
    public function jf_counts(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');//
            $mb_id = $request->get('mb_id', '');//

            $check_data = [
                'store_id' => '门店ID',
            ];


            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $MemberList = MemberList::where('store_id', $store_id)
                ->where('mb_id', $mb_id)
                ->select('mb_jf', 'pay_counts', 'mb_time', 'mb_logo', 'mb_name')
                ->first();

            if (!$MemberList) {
                return json_encode([
                    'status' => 2,
                    'message' => '会员信息不存在'
                ]);
            }
            return json_encode([
                    'status' => 1,
                    'message' => '查询成功',
                    'data' => [
                        'jf_count' => $MemberList->mb_jf,
                        'pay_count' => $MemberList->pay_counts,
                        'mb_time' => $MemberList->mb_time,
                        'mb_logo' => $MemberList->mb_logo,
                        'mb_name' => $MemberList->mb_name,
                    ],
                ]
            );


        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }


    //生成会员二维码
    public function create_qr(Request $request)
    {
        try {
            $user = $this->parseToken(); //
            $store_id = $request->get('store_id', ''); //1 交易笔数 由大到小

            $check_data = [
                'store_id' => '门店ID',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $store = Store::where('store_id', $store_id)
                ->select('store_name')
                ->first();
            $store_name = "";
            if ($store) {
                $store_name = $store->store_name;
            }

            return json_encode([
                    'status' => 1,
                    'message' => '查询成功',
                    'data' => [
                        'qr_url' => url('/qr?store_id=') . $store_id,
                        'qr_desc' => '扫码即成为会员、新会员送积分',
                        'store_name' => $store_name
                    ],
                ]
            );

        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()
            ]);
        }
    }


    //判断有没有设置
    public function is_set_jf(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $jf_status = 1;
            $store_id = $request->get('store_id');
            $MemberSetJf = MemberSetJf::where('store_id', $store_id)
                ->select('id')
                ->first();

            if ($MemberSetJf) {
                $jf_status = 1;
            }

            return json_encode([
                'status' => '1',
                'message' => '查询成功',
                'data' => [
                    'jf_status' => $jf_status
                ]
            ]);
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    public function yx_store_lists(Request $request)
    {
        try {
            $merchant = $this->parseToken();
            $merchant_id = $request->get('merchant_id', '');
            $yx_type = $request->get('yx_type', '');
            //门店创建者id
            if ($merchant_id == "" || $merchant_id == "NULL") {
                $merchant_id = $merchant->merchant_id;
            }
            $MerchantStore = MerchantStore::where('merchant_id', $merchant_id)
                ->select('store_id')
                ->get();

            if ($MerchantStore->isEmpty()) {
                $stores = [];

            } else {
                $stores = $MerchantStore->toArray();
            }
            $is_yx = [];
            $no_yx = [];
            foreach ($stores as $k => $v) {
                $store_id = $v['store_id'];
                $store = Store::where('store_id', $store_id)
                    ->select('store_name')
                    ->first();

                if ($store) {
                    $store_name = $store->store_name;
                    $yx = MemberYxStoreid::where('store_id', $store_id)
                        ->where('yx_type', $yx_type)
                        ->select('id')
                        ->first();

                    if ($yx) {
                        $is_yx[] = [
                            'store_id' => $store_id,
                            'store_name' => $store_name
                        ];
                    } else {
                        $no_yx[] = [
                            'store_id' => $store_id,
                            'store_name' => $store_name
                        ];
                    }

                }
            }


            $datas = [
                'is_yx_stores' => $is_yx,
                'no_yx_stores' => $no_yx
            ];
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($datas);


        } catch (\Exception $exception) {
            $this->status = -1;
            $this->message = $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine();
            return $this->format();
        }
    }


    //积分设置列表
    public function member_set_jf_list(Request $request)
    {
        $token = $this->parseToken(); //
        $store_id = $request->get('store_id', ''); //

        $storeInfo = Store::where('store_id', $store_id)
            ->first();
        if (!$storeInfo) {
            return json_encode([
                'status' => '2',
                'message' => '门店不存在'
            ]);
        }

        try {
            $obj = MemberSetJf::where('store_id', $store_id);
            if ($obj->get()->isEmpty()) {
                $this->status = '2';
                $this->message = '暂无数据';

                return $this->format();
            }

            $this->t = $obj->count();
            $data = $this->page($obj)
                ->orderBy('created_at', 'desc')
                ->get();
            $this->status = 1;
            $this->message = '数据返回成功';

            return $this->format($data);
        } catch (\Exception $ex) {
            return json_encode([
                'status' => '-1',
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


}
