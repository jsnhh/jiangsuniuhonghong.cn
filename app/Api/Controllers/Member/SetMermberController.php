<?php
namespace App\Api\Controllers\Member;


use App\Api\Controllers\BaseController;
use App\Models\MemberList;
use App\Models\MemberTpl;
use App\Models\MemberType;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SetMermberController extends BaseController
{
    //设置模板
    public function set_tpl(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');
            $tpl_name = $request->get('tpl_name', '');
            $tpl_desc = $request->get('tpl_desc', '');
            $tpl_bck = $request->get('tpl_bck', '');
            $tpl_sku = $request->get('tpl_sku', '100000');
            $tpl_content = $request->get('tpl_content', '');
            $tpl_status = $request->get('tpl_status', '');
            $tpl_phone = $request->get('tpl_phone', '');

            $check_data = [
                'store_id' => '门店ID',
                'tpl_name' => '会员卡名称',
                'tpl_desc' => '宣传语',
                'tpl_bck' => '会员卡背景模板',
                'tpl_content' => '使用说明',
                'tpl_status' => '模版状态',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $Store = Store::where('store_id', $store_id)
                ->select('config_id')
                ->first();
            if (!$Store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店ID不存在'
                ]);
            }

            $config_id = $Store->config_id;

            $in_data = [
                'store_id' => $store_id,
                'config_id' => $config_id,
                'tpl_name' => $tpl_name,
                'tpl_desc' => $tpl_desc,
                'tpl_bck' => $tpl_bck,
                'tpl_sku' => $tpl_sku,
                'tpl_content' => $tpl_content,
                'tpl_status' => $tpl_status,
                'tpl_phone' => $tpl_phone,
            ];

            $MemberTpl = MemberTpl::where('store_id', $store_id)->first();
            if ($MemberTpl) {
                $MemberTpl->update($in_data);
                $MemberTpl->save();
            } else {
                MemberTpl::create($in_data);
            }
            $re_data = $in_data;

            return json_encode([
                'status' => 1,
                'message' => '模版提交成功',
                'data' => $re_data
            ]);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()
            ]);
        }
    }


    //查询模板
    public function query_tpl(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');

            $check_data = [
                'store_id' => '门店ID',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => '2',
                    'message' => $check
                ]);
            }

            $Store = Store::where('store_id', $store_id)
                ->select('config_id')
                ->first();
            if (!$Store) {
                return json_encode([
                    'status' => '2',
                    'message' => '门店ID不存在'
                ]);
            }

            $MemberTpl = MemberTpl::where('store_id', $store_id)->first();
            if ($MemberTpl) {
                return json_encode([
                    'status' => 1,
                    'message' => '数据返回',
                    'data' => $MemberTpl
                ]);
            } else {
                return json_encode([
                    'status' => 2,
                    'message' => '会员模板未设置',
                    'data' => []
                ]);
            }
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()
            ]);
        }
    }


    //查询等级
    public function query_type(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');

            $check_data = [
                'store_id' => '门店ID',

            ];

            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $data = [
                [
                    'type' => 1,
                    'type_desc' => '普通会员'
                ],
                [
                    'type' => 2,
                    'type_desc' => '黄金会员'
                ], [
                    'type' => 3,
                    'type_desc' => '白金会员'
                ],
                [
                    'type' => 4,
                    'type_desc' => '钻石会员'
                ],

            ];


            return json_encode([
                'status' => 1,
                'message' => '数据成功',
                'data' => $data
            ]);


        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }


    //添加等级
    public function set_type(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');
            $type = $request->get('type', '');
            $type_desc = $request->get('type_desc', '');
            $pay_amount = $request->get('pay_amount', '');

            $check_data = [
                'store_id' => '门店ID',
                'pay_amount' => '消费金额',
                'type_desc' => '会员等级说明',
                'type' => '会员等级',

            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            if (!in_array($type, [1, 2, 3, 4, 5])) {
                return json_encode(['status' => 2, 'message' => '会员等级id不正确']);

            }


            $in_sert = [
                'store_id' => $store_id,
                'type' => $type,
                'type_desc' => $type_desc,
                'pay_amount' => $pay_amount,
            ];


            $MemberType = MemberType::where('store_id', $store_id)
                ->where('type', $type)
                ->first();
            if ($MemberType) {
                $MemberType->update($in_sert);
                $MemberType->save();
            } else {
                MemberType::create($in_sert);
            }


            return json_encode([
                'status' => 1,
                'message' => '设置成功',
                'data' => $in_sert
            ]);


        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }


    //查看等级列表
    public function query_type_list(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');

            $check_data = [
                'store_id' => '门店ID',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $MemberType = MemberType::where('store_id', $store_id)->get();

            return json_encode([
                'status' => 1,
                'message' => '数据返回',
                'data' => $MemberType
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }


    //查看等级
    public function query_type_info(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');
            $type = $request->get('type', '');


            $check_data = [
                'store_id' => '门店ID',
                'type' => '等级类型',
            ];

            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $MemberType = MemberType::where('store_id', $store_id)
                ->where('type', $type)
                ->first();


            return json_encode([
                'status' => 1,
                'message' => '数据返回',
                'data' => $MemberType
            ]);


        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }


    //查询会员信息-无token
    public function query_tpl_web(Request $request)
    {
        try {
            $store_id = $request->get('store_id', '');


            $check_data = [
                'store_id' => '门店ID',
            ];


            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }


            $Store = Store::where('store_id', $store_id)
                ->select('config_id')
                ->first();
            if (!$Store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店ID不存在'
                ]);
            }

            $MemberTpl = MemberTpl::where('store_id', $store_id)
                ->select(
                    'tpl_name',
                    'tpl_desc',
                    'tpl_bck')
                ->first();

            if ($MemberTpl) {
                $MemberTpl->tpl_bck_url = url('/member/vip-bg-') . $MemberTpl->tpl_bck . '.png';
                $MemberTpl->logo_url = url('/member/logo.png');

            }
            return json_encode([
                'status' => 1,
                'message' => '数据返回',
                'data' => $MemberTpl
            ]);


        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }


    //会员信息补齐-无token
    public function member_info_save(Request $request)
    {
        try {
            $store_id = $request->get('store_id', '');
            $open_id = $request->get('open_id', ''); //微信、支付宝openID
            $mb_name = $request->get('mb_name', '');
            $mb_phone = $request->get('mb_phone', '');
            $sms_code = $request->get('sms_code', '');
            $ways_source = $request->get('ways_source', ''); //alipay-支付宝;weixin-微信;applet-微信小程序
            $merchant_id = $request->get('merchant_id', '');

            $check_data = [
                'store_id' => '门店ID',
                'merchant_id'=>'收银员ID',
                'open_id' => '用户ID',
                'mb_name' => '会员名称',
                'mb_phone' => '会员手机号',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $store = Store::where('store_id', $store_id)
                ->select('config_id')
                ->first();
            if (!$store) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店不存在'
                ]);
            }

            $ali_user_id = "";
            $wx_openid = "";
            $xcx_openid = "";
            $MemberList = "";
            $config_id = $store->config_id;

            if ($ways_source == "alipay") {
                $ali_user_id = $open_id;
                $MemberList = MemberList::where('store_id', $store_id)
                    ->where('ali_user_id', $open_id)
                    ->first();
            }

            if ($ways_source == "weixin") {
                $wx_openid = $open_id;
                $MemberList = MemberList::where('store_id', $store_id)
                    ->where('wx_openid', $open_id)
                    ->first();
            }

            if ($ways_source == "applet") {
                $xcx_openid = $open_id;
                $MemberList = MemberList::where('store_id', $store_id)
                    ->where('xcx_openid', $open_id)
                    ->first();
            }

            if ($MemberList) {
                //返回数据
                $re_data = [
                    'store_id' => $store_id,
                    'config_id' => $config_id,
                    'mb_name' => $mb_name,
                    'mb_phone' => $mb_phone,
                    'mb_jf' => $MemberList->mb_jf,
                    'mb_money' => $MemberList->mb_money,
                    'wx_openid' => $wx_openid,
                    'xcx_openid' => $xcx_openid,
                    'ali_user_id' => $ali_user_id,
                    'mb_ver' => $MemberList->mb_ver,
                    'mb_ver_desc' => $MemberList->mb_ver_desc,
                ];

                $in_sert_data = [
                    'store_id' => $store_id,
                    'config_id' => $config_id,
                    'mb_name' => $mb_name,
                    'mb_phone' => $mb_phone,
                ];
                $MemberList->update($in_sert_data);
                $MemberList->save();
            } else {
                //会员不存在开卡会员
                $mb_id = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                $in_sert_data = [
                    'store_id' => $store_id,
                    'config_id' => $config_id,
                    'merchant_id' => $merchant_id,
                    'mb_id' => $mb_id,
                    'mb_status' => '1',
                    'mb_ver' => '1',
                    'mb_ver_desc' => '普通会员',
                    'mb_name' => $mb_name,
                    'mb_phone' => $mb_phone,
                    'mb_nick_name' => $mb_name,
                    'mb_logo' => url('/member/mrtx.png'),
                    'mb_jf' => 0,
                    'mb_money' => '0.00', //储值金额
                    'mb_time' => date('Y-m-d H:i:s', time()),
                    'pay_counts' => 1,
                    'pay_moneys' => 0,
                    'wx_openid' => $wx_openid,
                    'ali_user_id' => $ali_user_id,
                ];
                MemberList::create($in_sert_data);

                //返回数据
                $re_data = [
                    'mb_id'=>$mb_id,
                    'store_id' => $store_id,
                    'config_id' => $config_id,
                    'mb_name' => $mb_name,
                    'mb_phone' => $mb_phone,
                    'mb_jf' => 0,
                    'mb_money' => '0.00',//储值金额
                    'wx_openid' => $wx_openid,
                    'ali_user_id' => $ali_user_id,
                    'mb_ver' => '1',
                    'mb_ver_desc' => '普通会员',
                ];
            }

            return json_encode([
                'status' => 1,
                'message' => '数据更新成功',
                'data' => $re_data
            ]);
        } catch (\Exception $exception) {
            return json_encode(['status' => -1, 'message' => $exception->getMessage().' | '.$exception->getFile().' | '.$exception->getLine()]);
        }
    }


    //删除会员等级信息
    public function del_member_type(Request $request)
    {
        try {
            $token = $this->parseToken();
            $id = $request->get('id', '');

            $check_data = [
                'id' => '会员等级ID',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => '2',
                    'message' => $check
                ]);
            }

            $memberTypeObj = MemberType::where('id', $id)->first();
            if (!$memberTypeObj) {
                return json_encode([
                    'status' => '2',
                    'message' => '会员等级信息不存在'
                ]);
            }

            $res = $memberTypeObj->delete();
            if ($res) {
                return json_encode([
                    'status' => '1',
                    'message' => '删除成功'
                ]);
            } else {
                return json_encode([
                    'status' => '2',
                    'message' => '删除失败'
                ]);
            }
        } catch (\Exception $ex) {
            return json_encode([
                'status' => -1,
                'message' => $ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine()
            ]);
        }
    }


    //清空会员积分
    public function del_member_points(Request $request)
    {
        $token = $this->parseToken();
        $mb_id = $request->post('mbId', ''); //会员卡号
        $points = $request->post('points', 0); //减少的积分

        $check_data = [
            'mbId' => '会员卡号'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = 2;
            $this->message = $check;
            return $this->format();
        }

        $member_list_obj = MemberList::where('mb_id', $mb_id)
            ->where('mb_status', 1)
            ->first();
        if (!$member_list_obj) {
            $this->status = 2;
            $this->message = '未找到会员卡或状态异常';
            return $this->format();
        }

        $update_point = 0;
        if ($points) {
            $update_point = $member_list_obj->mb_jf - $points;
            if ($update_point < 0) {
                $update_point = 0;
            }
        }

        try {
            $res = $member_list_obj->update([
                'mb_jf' => $update_point
            ]);
            if ($res) {
                $this->status = 1;
                $this->message = '更新成功';
                return $this->format();
            } else {
                $this->status = 2;
                $this->message = '更新失败';
                return $this->format();
            }
        } catch (\Exception $e) {
            $this->status = -1;
            $this->message = $e->getMessage().' | '.$e->getFile().' | '.$e->getLine();
            return $this->format();
        }
    }


    //删除会员卡信息
    public function del_member(Request $request)
    {
        $token = $this->parseToken();
        $mb_id = $request->post('mbId', ''); //会员卡号

        $check_data = [
            'mbId' => '会员卡号'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = 2;
            $this->message = $check;
            return $this->format();
        }

        $member_list_obj = MemberList::where('mb_id', $mb_id)->first();
        if (!$member_list_obj) {
            $this->status = 2;
            $this->message = '未找到会员卡信息';
            return $this->format();
        }

        try {
            $res = $member_list_obj->delete();
            if ($res) {
                $this->status = 1;
                $this->message = '删除成功';
                return $this->format();
            } else {
                $this->status = 2;
                $this->message = '删除失败';
                return $this->format();
            }
        } catch (\Exception $e) {
            $this->status = -1;
            $this->message = $e->getMessage().' | '.$e->getFile().' | '.$e->getLine();
            return $this->format();
        }
    }


    //修改会员卡信息
    public function update_member(Request $request)
    {
        $token = $this->parseToken();
        $mb_id = $request->post('mbId', ''); //会员卡号
        $mb_status = $request->post('mbStatus', ''); //会员卡状态
        $mb_ver = $request->post('mbVer', ''); //会员卡等级
        $mb_ver_desc = $request->post('mbVerDesc', ''); //会员卡等级描述
        $mb_jf = $request->post('mbJf', ''); //会员卡积分
        $mb_money = $request->post('mbMoney', ''); //会员卡余额
        $mb_name = $request->post('mbName', ''); //会员卡名称
        $mb_nick_name = $request->post('mbNickName', ''); //会员卡昵称
        $mb_phone = $request->post('mbPhone', ''); //会员卡手机号

        $check_data = [
            'mbId' => '会员卡号'
        ];
        $check = $this->check_required($request->except(['token']), $check_data);
        if ($check) {
            $this->status = 2;
            $this->message = $check;
            return $this->format();
        }

        if ($mb_jf && (int)$mb_jf < 0) {
            $this->status = 2;
            $this->message = '会员积分不能小于0';
            return $this->format();
        }

        if ($mb_money && ($mb_money < 0)) {
            $this->status = 2;
            $this->message = '会员卡余额不能小于0';
            return $this->format();
        }

        $member_list_obj = MemberList::where('mb_id', $mb_id)->first();
        if (!$member_list_obj) {
            $this->status = 2;
            $this->message = '未找到会员卡信息';
            return $this->format();
        }

        try {
            $update_data = [
                'mb_id' => $mb_id
            ];
            if ($mb_status || ($mb_status === '0') || ($mb_status === 0)) $update_data['mb_status'] = $mb_status;
            if ($mb_ver) $update_data['mb_ver'] = $mb_ver;
            if ($mb_ver_desc) $update_data['mb_ver_desc'] = $mb_ver_desc;
            if ($mb_jf || ($mb_jf === '0') || ($mb_jf === 0)) $update_data['mb_jf'] = $mb_jf;
            if ($mb_money || ($mb_money === '0') || ($mb_money === '0.00') || ($mb_money === 0) || ($mb_money === 0.00)) $update_data['mb_money'] = $mb_money;
            if ($mb_name) $update_data['mb_name'] = $mb_name;
            if ($mb_nick_name) $update_data['mb_nick_name'] = $mb_nick_name;
            if ($mb_phone) $update_data['mb_phone'] = $mb_phone;
            $res = $member_list_obj->update($update_data);
            if ($res) {
                $this->status = 1;
                $this->message = '更新成功';
                return $this->format();
            } else {
                $this->status = 2;
                $this->message = '更新失败';
                return $this->format();
            }
        } catch (\Exception $e) {
            $this->status = -1;
            $this->message = $e->getMessage().' | '.$e->getFile().' | '.$e->getLine();
            return $this->format();
        }
    }

    public function editMember(Request $request)
    {
        $input = $request->all();
        // 参数校验
        $check_data = [
            'id' => 'id',
        ];
        $check = $this->check_required($input, $check_data);
        if ($check) {
            return $this->sys_response(2, $check);
        }

        $model = new MemberList();

        // 编辑
        $data['mb_ver'] = $input['mb_ver'];
        $data['mb_ver_desc'] = $input['mb_ver_desc'];
        $data['mb_virtual_money'] = $input['mb_virtual_money'];
        $data['mb_phone'] = $input['mb_phone'];
        $data['mb_money'] = $input['mb_money'];
        $data['mb_jf'] = $input['mb_jf'];
        $data['updated_at'] = date('Y-m-d H:i:s');

        $editRes = $model->editMember($input['id'], $data);
        if ($editRes) {
            return $this->sys_response_layui(1, '编辑成功');
        } else {
            return $this->sys_response_layui(2, '编辑失败');
        }
    }

}
