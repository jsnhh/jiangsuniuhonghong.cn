<?php
namespace App\Api\Controllers\Member;


use App\Api\Controllers\BaseController;
use App\Models\MemberCzList;
use App\Models\MemberCzSet;
use App\Models\MemberTplWx;
use App\Models\MemberXyList;
use App\Models\MemberYxSet;
use App\Models\MemberYxStoreid;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CzController extends BaseController
{
    //商家充值活动设置
    public function cz_set(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_ids = $request->get('store_ids', '');
            $yx_type = $request->get('yx_type', '1');
            $cz_list = $request->get('cz_list', '');
            $b_time = $request->get('b_time', '');
            $e_time = $request->get('e_time', '');
            $yx_id = time() . $user->phone;
            $check_data = [
                'store_ids' => '门店ID',
                'b_time' => '充值活动开始时间',
                'e_time' => '充值活动结束时间',
                'yx_type' => '营销活动类型',
                'cz_list' => '充值列表'
            ];

            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $is_true = 0;
            $store_ids = explode(',', $store_ids);
            $store_ids = array_unique($store_ids); //去掉重复的字符串
            foreach ($store_ids as $k => $v) {
                $store = Store::where('store_id', $v)
                    ->select('id')
                    ->first();
                if ($store) {

                    $MemberYxSet = MemberYxStoreid::where('yx_type', $yx_type)
                        ->where('store_id', $v)
                        ->select('id')
                        ->first();

                    if ($MemberYxSet) {
                        $is_true = 1;
                    }


                    $insert_member_yx_store_ids[] = [
                        'store_id' => $v,
                        'yx_id' => $yx_id,
                        'yx_type' => $yx_type,
                        'created_at' => '2019-01-01',
                        'updated_at' => '2019-01-01',

                    ];
                }
            }

            if ($is_true) {
                return json_encode([
                    'status' => 2,
                    'message' => '门店已经有类似活动了'
                ]);
            }


            //开启事务
            try {
                DB::beginTransaction();


                $m_list = [
                    'yx_type' => $yx_type,
                    'yx_id' => $yx_id,
                    'yx_name' => '会员充值送',
                    'yx_desc' => '会员充值送大礼',
                    'merchant_id' => isset($user->merchant_id) ? $user->merchant_id : "0",
                ];

                MemberXyList::create($m_list);


                DB::table('member_yx_store_ids')->insert($insert_member_yx_store_ids);


                $in_data = [
                    'yx_type' => $yx_type,
                    'yx_id' => $yx_id,
                    'cz_list' => $cz_list,
                    'b_time' => $b_time,
                    'e_time' => $e_time,
                ];

                MemberCzSet::create($in_data);

                $re_data = $in_data;


                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                return json_encode(['status' => 2, 'message' => $e->getMessage()]);
            }

            return json_encode([
                'status' => 1,
                'message' => '设置成功',
                'data' => $re_data
            ]);


        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage().' | '.$exception->getLine()
            ]);
        }
    }


    //充值活动详细地址
    public function cz_info(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $yx_type = $request->get('yx_type');
            $yx_id = $request->get('yx_id');

            $check_data = [
                'yx_type' => '营销类型',
                'yx_id' => '营销id',
            ];

            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $MemberCzSet = MemberCzSet::where('yx_id', $yx_id)
                ->where('yx_type', $yx_type)
                ->first();


            if (!$MemberCzSet) {
                return json_encode([
                    'status' => 2,
                    'message' => '营销活动不存在',
                ]);
            }

            $MemberYxStoreid = MemberYxStoreid::join('stores', 'member_yx_store_ids.store_id', '=', 'stores.store_id')
                ->where('member_yx_store_ids.yx_id', $yx_id)
                ->where('member_yx_store_ids.yx_type', $yx_type)
                ->select('stores.store_short_name as store_name')
                ->get();
            $MemberCzSet->store_info = $MemberYxStoreid;
            return json_encode([
                'status' => 1,
                'message' => '查询成功',
                'MemberCzSet' => $MemberCzSet,
            ]);


        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage().' | '.$exception->getLine()
            ]);
        }
    }


    //充值列表
    public function cz_lists(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');
            $mb_id = $request->get('mb_id', '');
            $cz_type = $request->get('cz_type', '');
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');
            $return_type = $request->get('return_type', '');
            $pay_status = $request->get('pay_status', '');

            $check_data = [
                'store_id' => '门店ID',

            ];

            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }
            $where = [];
            $where[] = ['store_id', $store_id];


            if ($mb_id) {
                $where[] = ['mb_id', 'like', '%' . $mb_id . '%'];
            }

            if ($cz_type) {
                $where[] = ['cz_type', $cz_type];
            }

            if ($pay_status) {
                $where[] = ['pay_status', $pay_status];
            }

            if ($time_start) {
                $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                $where[] = ['updated_at', '>=', $time_start];
            }
            if ($time_end) {
                $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                $where[] = ['updated_at', '<=', $time_end];
            }


            $obj = MemberCzList::where($where)
                ->orderBy('updated_at', 'desc');

            $all_total_amount = $obj->sum('total_amount');
            $all_cz_money = $obj->sum('cz_money');
            $all_cz_s_money = $obj->sum('cz_s_money');
            $all_fee_amount = $obj->sum('fee_amount');
            $receipt_amount = $obj->sum('receipt_amount');


            $data = [
                'all_total_amount' => $all_total_amount,
                'all_cz_money' => $all_cz_money,
                'all_cz_s_money' => $all_cz_s_money,
                'all_fee_amount' => $all_fee_amount,
                'all_receipt_amount' => $receipt_amount,
            ];

            if ($return_type == 1) {
                $this->status = 1;
                $this->message = '数据返回成功';
                return $this->format($data);
            } elseif ($return_type == 2) {
                $this->t = $obj->count();
                $data = $this->page($obj)->get();
                $this->status = 1;
                $this->message = '数据返回成功';
                return $this->format($data);
            } else {
                $this->t = $obj->count();
                $data['lists'] = $this->page($obj)->get();
                $this->status = 1;
                $this->message = '数据返回成功';
                return $this->format($data);
            }


        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage().' | '.$exception->getLine()
            ]);
        }
    }


    //充值类型
    public function cz_type(Request $request)
    {
        try {
            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');

            $check_data = [
                'store_id' => '门店ID',

            ];

            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $data = [
                [
                    'cz_type' => 1,
                    'cz_type_desc' => '普通充值'
                ],
                [
                    'cz_type' => 2,
                    'cz_type_desc' => '充值满送'
                ]

            ];


            return json_encode([
                'status' => 1,
                'message' => '数据成功',
                'data' => $data
            ]);


        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage().' | '.$exception->getLine()
            ]);
        }
    }


    //根据门店ID获取充值详细
    public function cz_query(Request $request)
    {
        try {

            $user = $this->parseToken();//
            $store_id = $request->get('store_id', '');


            $check_data = [
                'store_id' => '门店ID',

            ];

            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $time = date('Y-m-d H:i:s', time());
            // $time = '2020-01-01 00:00:00';

            $data = MemberCzSet::join('member_yx_store_ids', 'member_cz_sets.yx_id', '=', 'member_yx_store_ids.yx_id')
                ->where('member_yx_store_ids.store_id', $store_id)
                ->where('member_yx_store_ids.yx_type', 1)
                ->where('member_cz_sets.yx_type', 1)
                ->select('member_cz_sets.cz_list')
                ->where('member_cz_sets.b_time', '<=', $time)
                ->where('member_cz_sets.e_time', '>', $time)
                ->first();

            if (!$data) {
                return json_encode([
                    'status' => 2,
                    'message' => '商户未设置充值活动'
                ]);
            }


            return json_encode([
                'status' => 1,
                'message' => '数据返回成功',
                'data' => $data
            ]);


        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage().' | '.$exception->getLine()
            ]);
        }
    }


    //充值记录
    public function cz_list_mb_ids(Request $request)
    {
        try {
            $store_id = $request->get('store_id', '');
            $mb_id = $request->get('mb_id', '');
            $cz_type = $request->get('cz_type', '');
            $time_start = $request->get('time_start', '');
            $time_end = $request->get('time_end', '');
            $pay_status = $request->get('pay_status', '1');

            $check_data = [
                'store_id' => '门店ID',
                'mb_id' => '会员卡ID',
            ];
            $check = $this->check_required($request->except(['token']), $check_data);
            if ($check) {
                return json_encode([
                    'status' => 2,
                    'message' => $check
                ]);
            }

            $where = [];
            $where[] = ['store_id', $store_id];
            if ($mb_id) {
                $where[] = ['mb_id', $mb_id];
            }
            if ($cz_type) {
                $where[] = ['cz_type', $cz_type];
            }
            if ($pay_status) {
                $where[] = ['pay_status', $pay_status];
            }
            if ($time_start) {
                $time_start = date('Y-m-d H:i:s', strtotime($time_start));
                $where[] = ['updated_at', '>=', $time_start];
            }
            if ($time_end) {
                $time_end = date('Y-m-d H:i:s', strtotime($time_end));
                $where[] = ['updated_at', '<=', $time_end];
            }

            $obj = MemberCzList::where($where)
                ->select(
                    'cz_money',//'充值支付金额',
                    'cz_s_money',//'充值送金额',
                    'pay_status',
                    'pay_time',
                    'cz_type',
                    'cz_type_desc')
                ->orderBy('updated_at', 'desc');

            $this->t = $obj->count();
            $data = $this->page($obj)->get();
            $this->status = 1;
            $this->message = '数据返回成功';
            return $this->format($data);
        } catch (\Exception $exception) {
            return json_encode([
                'status' => -1,
                'message' => $exception->getMessage().' | '.$exception->getLine()
            ]);
        }
    }


}
