<?php

namespace App\Jobs;

use Alipayopen\Sdk\LtLogger;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class MerchantFuwu implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $title;//
    protected $ways_type_desc;//
    protected $store_id;//
    protected $merchant_id;//
    protected $total_amount;//
    protected $out_trade_no;//


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($title, $ways_type_desc, $store_id, $merchant_id, $total_amount, $out_trade_no)
    {
        //

        $this->title = $title;
        $this->ways_type_desc = $ways_type_desc;
        $this->store_id = $store_id;
        $this->merchant_id = $merchant_id;
        $this->total_amount = $total_amount;
        $this->out_trade_no = $out_trade_no;


    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        \App\Common\MerchantFuwu::insert($this->title, $this->ways_type_desc, $this->store_id, $this->merchant_id, $this->total_amount, $this->out_trade_no);

    }

    /**
     * 任务失败的处理过程
     *
     * @param  Exception $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        // 给用户发送任务失败的通知，等等……
        $this->writelog(storage_path() . '/logs/job_MerchantFuwu.log', $exception);

    }


    public function writelog($log_file, $exception)
    {
        try {
            $logger = new LtLogger();
            $logger->conf["log_file"] = $log_file;
            $logger->conf["separator"] = "---------------";
            $logData = array(
                date("Y-m-d H:i:s"),
                str_replace("\n", "", $exception),
            );
            $logger->log($logData);
        } catch (\Exception $exception) {
            Log::info($exception);
        }
    }
}