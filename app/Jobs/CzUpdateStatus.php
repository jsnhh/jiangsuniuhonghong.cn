<?php

namespace App\Jobs;

use Alipayopen\Sdk\LtLogger;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class CzUpdateStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $CzUpdateStatus;//


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($CzUpdateStatus)
    {
        //

        $this->CzUpdateStatus = $CzUpdateStatus;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $obj = new  \App\Common\CzUpdateStatus($this->CzUpdateStatus);
        $obj->insert();
    }

    /**
     * 任务失败的处理过程
     *
     * @param  Exception $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        // 给用户发送任务失败的通知，等等……
        $this->writelog(storage_path() . '/logs/job_CzUptateStatus.log', $exception);

    }


    public function writelog($log_file, $exception)
    {
        try {
            $logger = new LtLogger();
            $logger->conf["log_file"] = $log_file;
            $logger->conf["separator"] = "---------------";
            $logData = array(
                date("Y-m-d H:i:s"),
                str_replace("\n", "", $exception),
            );
            $logger->log($logData);
        } catch (\Exception $exception) {
            Log::info($exception);
        }
    }
}
