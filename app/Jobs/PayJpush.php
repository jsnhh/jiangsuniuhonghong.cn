<?php

namespace App\Jobs;

use Alipayopen\Sdk\LtLogger;
use App\Api\Controllers\Push\JpushController;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class PayJpush implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $ways_type_desc;//
    protected $total_amount;//
    protected $out_trade_no;//
    protected $merchant_id;//
    protected $store_id;//
    protected $config_id;//


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($ways_type_desc, $total_amount, $out_trade_no, $merchant_id, $store_id, $config_id)
    {
        //

        $this->ways_type_desc = $ways_type_desc;
        $this->total_amount = $total_amount;
        $this->out_trade_no = $out_trade_no;
        $this->merchant_id = $merchant_id;
        $this->store_id = $store_id;
        $this->config_id = $config_id;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //1.安卓app语音播报
        $jpush = new JpushController();
        $jpush->push($this->ways_type_desc, $this->total_amount, $this->out_trade_no, $this->merchant_id, $this->store_id, $this->config_id);

    }

    /**
     * 任务失败的处理过程
     *
     * @param  Exception $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        // 给用户发送任务失败的通知，等等……
        $this->writelog(storage_path() . '/logs/job_PayJpush.log', $exception);

    }


    public function writelog($log_file, $exception)
    {
        try {
            $logger = new LtLogger();
            $logger->conf["log_file"] = $log_file;
            $logger->conf["separator"] = "---------------";
            $logData = array(
                date("Y-m-d H:i:s"),
                str_replace("\n", "", $exception),
            );
            $logger->log($logData);
        } catch (\Exception $exception) {
            Log::info($exception);
        }
    }
}
