<?php

namespace App\Console;


use App\Console\Commands\ArchitectureRewardTask;
use App\Console\Commands\BoxRebateAudit;
use App\Console\Commands\CashBack;
use App\Console\Commands\HkrtStoreAudit;
use App\Console\Commands\IsDeductTask;
use App\Console\Commands\JdStoreAudit;
use App\Console\Commands\MyBankStoreAudit;
use App\Console\Commands\NewLandStoreAudit;
use App\Console\Commands\OrderSuccess;
use App\Console\Commands\Orders2019Audit;
use App\Console\Commands\RechargeOrderSuccess;
use App\Console\Commands\SchoolOrderSuccess;
use App\Console\Commands\Settlement;
use App\Console\Commands\SettlementDay;
use App\Console\Commands\SettlementMonthly;
use App\Console\Commands\SettlementMonthlySub;
use App\Console\Commands\SettlementMounthTrue;
use App\Console\Commands\SettlementTrue;
use App\Console\Commands\UserMonthLevelTask;
use App\Console\Commands\UserServiceChargeTask;
use App\Console\Commands\UsersIntegralReward;
use App\Console\Commands\UserSubTrainRewardTask;
use App\Console\Commands\UserTrainRewardTask;
use App\Console\Commands\VbillaStoreAudit;
use App\Console\Commands\VbillOrderSuccess;
use App\Console\Commands\VbillStoreAudit;
use App\Console\Commands\VbillSyncRate;
use App\Console\Commands\ZftStoreAudit;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;


class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        MyBankStoreAudit::class,
        JdStoreAudit::class,
        NewLandStoreAudit::class,
        Settlement::class,
        VbillStoreAudit::class,
        ZftStoreAudit::class,
        VbillaStoreAudit::class,
        HkrtStoreAudit::class,
        VbillOrderSuccess::class,
        VbillSyncRate::class,
        OrderSuccess::class,
        CashBack::class,
        SettlementMonthly::class,
        SchoolOrderSuccess::class,
        RechargeOrderSuccess::class,
        UserServiceChargeTask::class,
        UserTrainRewardTask::class,
        UserMonthLevelTask::class,
        UserSubTrainRewardTask::class,
        UsersIntegralReward::class,
        BoxRebateAudit::class,
        ArchitectureRewardTask::class,
        IsDeductTask::class,
        SettlementMonthlySub::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        //终端交易返现定时任务
        $schedule->command('cashBack')->daily()->at('01:00');
        $schedule->command('users_integral_reward')->daily()->at('00:00');
        $schedule->command('userMonthLevelTask')->monthlyOn(1, '02:00');//计算代理上月等级
        //$schedule->command('isDeductTask')->monthlyOn(2, '01:00');//计算扣除交易量
        $schedule->command('settlementMonthly')->monthlyOn(25, '04:00');//月结
        $schedule->command('settlementMonthlySub')->monthlyOn(25, '05:00');//下级月结
        //$schedule->command('user_service_charge_task')->monthlyOn(1, '03:00');//扣除服务费
        $schedule->command('user_train_reward_task')->monthlyOn(1, '06:00');//培养奖励
        //$schedule->command('user_sub_train_reward_task')->monthlyOn(1, '4:00');//下级交易返佣
        //$schedule->command('box_rebate_audit')->monthlyOn(1, '05:30');//音箱返现
        $schedule->command('architectureRewardTask')->monthlyOn(15, '03:00');


    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
