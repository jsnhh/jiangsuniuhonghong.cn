<?php

namespace App\Console\Commands;


use App\Models\SettlementList;
use App\Models\User;
use App\Models\UserWalletDetail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Settlement extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'settlement';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'settlement';

    /**
     * Settlement constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $users = User::where('is_delete', '0')->select('id', 'money', 'name', 'config_id')->get();
            $s_time = date("Y-m-d 00:00:00", strtotime("-1 day"));
            $e_time = date("Y-m-d 23:59:59", strtotime("-1 day"));
            foreach ($users as $user) {
                $table = DB::table('user_wallet_details');
                $wallet_obj = $table->select([
                    'money', 'pay_money', 'out_trade_no',
                    DB::raw("SUM(`money`) AS `day_money`"),
                    DB::raw("SUM(`pay_money`) AS `day_pay_money`"),
                ])
                    ->where('settlement', '02')
                    ->where('user_id', $user->id)
                    ->where('created_at', '>=', $s_time)
                    ->where('created_at', '<=', $e_time)
                    ->distinct('out_trade_no')//去重
                    ->first();
                if (!$wallet_obj->day_pay_money) {
                    continue;
                }
                //开启事务
                try {
                    DB::beginTransaction();
                    //总表结算
                    UserWalletDetail::where('user_id', $user->id)
                        ->where('settlement', '02')
                        ->update(['settlement' => '01', 'settlement_desc' => '已结算']);

                    //用户金额结算
                    $money = $user->money + $wallet_obj->day_money;
                    User::where('id', $user->id)->update(['money' => $money]);
                    $settlementDate = [
                        'user_id' => $user->id,
                        'config_id' => $user->config_id,
                        'user_name' => $user->name,
                        's_time' => $s_time,
                        'e_time' => $e_time,
                        'total_amount' => $wallet_obj->day_pay_money,
                        'commission_amount' => $wallet_obj->day_money,
                        'rate' => 0.001,
                        'commission_type' => 1,
                        'out_trade_no' => $wallet_obj->out_trade_no
                    ];
                    SettlementList::create($settlementDate);
                    DB::commit();
                } catch (\Exception $e) {
                    Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
                    DB::rollBack();
                    continue;
                }
            }
        } catch (\Exception $exception) {
            Log::info('日结结算定时任务error');
            Log::info($exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine());
        }
    }


}
