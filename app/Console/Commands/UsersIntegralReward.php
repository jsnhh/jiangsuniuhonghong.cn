<?php

namespace App\Console\Commands;


use App\Models\QrListInfo;
use App\Models\QrUserRewardInfo;
use App\Models\SettlementList;
use App\Models\Store;
use App\Models\User;
use App\Models\UserWalletDetail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UsersIntegralReward extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users_integral_reward';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'users_integral_reward';

    /**
     * Settlement constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $time = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s', time()) . ' -30 day'));
            $stores = Store::where('created_at', '>=', $time)
                ->select('store_id', 'store_name', 'user_id')
                ->get();
            foreach ($stores as $store) {
                $qrListInfo = QrListInfo::where('store_id', $store->store_id)->where('status', '1')->select('code_num')->first();//查询商户是否绑定码牌
                if (!$qrListInfo) {
                    continue;
                }
                $table = DB::table('orders');
                $order_obj = $table->select([
                    'total_amount',
                    DB::raw("SUM(`total_amount`) AS `total_amount`"),
                ])
                    ->where('pay_status', 1)
                    ->where('store_id', $store->store_id)
                    ->first();
                if (!$order_obj->total_amount) {
                    continue;
                }
                if ($order_obj->total_amount < 100) {//支付100后奖励积分
                    continue;
                }
                $qrUserRewardInfo = QrUserRewardInfo::where('store_id', $store->store_id)
                    ->select('code_num')->first();//查询码牌是否奖励过
                if ($qrUserRewardInfo) {
                    continue;
                }
                //开启事务
                try {
                    DB::beginTransaction();
                    $user = User::where('id', $store->user_id)->select('activation_integral')->first();
                    $activation_integral = $user->activation_integral + 10;
                    User::where('id', $store->user_id)->update(['activation_integral' => $activation_integral]);
                    $reward_info = [
                        'code_num' => $qrListInfo->code_num,
                        'user_id' => $store->user_id,
                        'award_amount' => 0,
                        'store_id' => $store->store_id,
                        'store_name' => $store->store_name,
                        'status' => '1',
                        'status_desc' => '本级奖励',
                        'activation_integral' => 10
                    ];
                    QrUserRewardInfo::create($reward_info);
                    DB::commit();
                } catch (\Exception $e) {
                    Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
                    DB::rollBack();
                    continue;
                }
            }
        } catch (\Exception $exception) {
            Log::info('积分奖励定时任务error');
            Log::info($exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine());
        }
    }


}
