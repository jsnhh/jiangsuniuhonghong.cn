<?php

namespace App\Console\Commands;

use App\Api\Controllers\BaseController;
use App\Models\AgentLevels;
use App\Models\SettlementMonthInfo;
use App\Models\SettlementMonthList;
use App\Models\User;
use App\Models\UserMonthLevel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SettlementMonthlySub extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'settlementMonthlySub';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '下级代理商分润月结';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            Log::info('下级月结---------');
            //获取上一个月时间
            $time_start = date("Y-m-01 00:00:00", strtotime("-1 months")); //获取上个月月初的时间
            //获取上一个月最后一天
            $time_end = date("Y-m-d 23:59:59", strtotime(-date('d') . 'day'));
            $userMonthLevels = UserMonthLevel::where('start_time', $time_start)
                ->where('end_time', $time_end)
                //->where('user_id', 1586)
                ->select('*')->get();
            foreach ($userMonthLevels as $userMonthLevel) {
                $user_id = $userMonthLevel->user_id;//本级ID
                $user = User::where('id', $user_id)->where('is_delete', '0')->select('name')->first();
                if (!$user) {
                    continue;
                }
                $p_user = User::where('id', $user_id)->where('is_delete', '0')->select('id', 'money', 'pid')->first();
                if (!$p_user) {
                    continue;
                }
                $p_user_id = $p_user->pid;//上级ID
                $s_user_level = UserMonthLevel::where('start_time', $time_start)
                    ->where('end_time', $time_end)
                    ->where('user_id', $user_id)
                    ->select('*')->first();//本级等级
                $s_user_level_id = 1;
                if ($s_user_level) {
                    $s_user_level_id = $s_user_level->level_weight;
                }
                $p_user_level = UserMonthLevel::where('start_time', $time_start)
                    ->where('end_time', $time_end)
                    ->where('user_id', $p_user_id)
                    ->select('*')->first();//上级等级
                $p_user_level_id = 1;
                if ($p_user_level) {
                    $p_user_level_id = $p_user_level->level_weight;
                }
                $month_rate = 0.00;
                $transaction_amount = 0.00;
                $month_money = 0.00;

                if ($s_user_level_id < $p_user_level_id) {//上级比本级等级高
                    $user_level = AgentLevels::where('level_weight', $p_user_level->level_weight)->select('*')->first();//查询上级对应的分润点
                    //查询已结算的分润点
                    $table = DB::table('settlement_month_list');
                    $sub_obj = $table->select([
                        'rate',
                        DB::raw("SUM(`rate`) AS `rate`"),
                    ])
                        ->where('user_id', $user_id)
                        ->where('start_time', $time_start)
                        ->where('end_time', $time_end)
                        ->first();
                    $month_rate = $user_level->rate - 10;
                    $month_rate = $month_rate / 10000 - $sub_obj->rate;//减去秒结分润点和已结算的分润点
                    if ($month_rate <= 0) {
                        $month_rate = 0.00;
                    }
                    //查询直属交易量
                    $order_table = DB::table('orders');
                    $wallet_obj = $order_table->select([
                        'total_amount',
                        DB::raw("SUM(`total_amount`) AS `month_pay_money`"),
                    ])
                        ->where('user_id', $user_id)
                        ->where('pay_status', 1)//成功
                        ->where('created_at', '>=', $time_start)
                        ->where('created_at', '<=', $time_end)
                        ->first();
                    if ($wallet_obj->month_pay_money) {
                        $transaction_amount = $wallet_obj->month_pay_money;
                    }
                    $month_money = $month_rate * $transaction_amount;
                }
                try {
                    DB::beginTransaction();
                    $settlementMonth = SettlementMonthList::where('user_id', $user_id)
                        ->where('p_user_id', '=', $p_user_id)
                        ->where('settlement_type', '2')
                        ->where('start_time', '=', $time_start)
                        ->where('end_time', '=', $time_end)
                        ->select('id')->first();

                    if (!$settlementMonth) {

                        //用户金额结算
                        $User=User::where('id', $p_user_id)->select('money')->first();
                        //Log::info("代理金额1===" . $p_user_id . "====" . $User->money);
                        $money = $User->money + $month_money;
                        User::where('id', $p_user_id)->update(['money' => $money]);
                        $settlementDate = [
                            'user_id' => $user_id,
                            'user_name' => $user->name,
                            'start_time' => $time_start,
                            'end_time' => $time_end,
                            'rate' => $month_rate,
                            'transaction_amount' => $transaction_amount,
                            'total_amount' => $month_money,
                            'settlement_type' => '2',
                            'p_user_id' => $p_user_id
                        ];
                        SettlementMonthList::create($settlementDate);
                    }
                    DB::commit();
                } catch (\Exception $e) {
                    Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => '2',
                        'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
                    ]);
                }
                if ($p_user_level_id != 9) {
                    $this->circulate_reward($p_user_id, $s_user_level_id, $time_start, $time_end, $user_id, $user->name);
                }

            }
        } catch (\Exception $exception) {
            Log::info('服务商月结定时任务error');
            Log::info($exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine());
        }
    }

    public function circulate_reward($user_id, $user_level, $time_start, $time_end, $primary_user_id, $primary_user_name)
    {
        $p_user = User::where('id', $user_id)->where('is_delete', '0')->select('id', 'name', 'pid', 'money')->first();

        if ($p_user && $p_user->pid != 1) {
            $p_user_level = UserMonthLevel::where('start_time', $time_start)
                ->where('end_time', $time_end)
                ->where('user_id', $p_user->pid)
                ->select('*')->first();//上级等级
            $month_rate = 0.00;
            $transaction_amount = 0.00;
            $month_money = 0.00;
            $p_user_level_id = 1;
            if ($p_user_level) {
                $p_user_level_id = $p_user_level->level_weight;
            }
            if ($user_level < $p_user_level_id) {//上级比本级等级高
                $agent_level = AgentLevels::where('level_weight', $p_user_level_id)->select('*')->first();//查询上级对应的分润点
                //查询已结算的分润点
                $table = DB::table('settlement_month_list');
                $sub_obj = $table->select([
                    'rate',
                    DB::raw("SUM(`rate`) AS `rate`"),
                ])
                    ->where('user_id', $primary_user_id)
                    ->where('start_time', $time_start)
                    ->where('end_time', $time_end)
                    ->first();

                $month_rate = $agent_level->rate - 10;
                $month_rate = $month_rate / 10000 - $sub_obj->rate;//减去秒结分润点和已结算的分润点
                if ($month_rate <= 0) {
                    $month_rate = 0.00;
                }
                //查询直属交易量
                $order_table = DB::table('orders');
                $wallet_obj = $order_table->select([
                    'total_amount',
                    DB::raw("SUM(`total_amount`) AS `month_pay_money`"),
                ])
                    ->where('user_id', $primary_user_id)
                    ->where('pay_status', 1)//成功
                    ->where('created_at', '>=', $time_start)
                    ->where('created_at', '<=', $time_end)
                    ->first();
                if ($wallet_obj->month_pay_money) {
                    $transaction_amount = $wallet_obj->month_pay_money;
                }
                $month_money = $month_rate * $transaction_amount;

            }
            try {
                DB::beginTransaction();
                $settlementMonth = SettlementMonthList::where('user_id', $primary_user_id)
                    ->where('p_user_id', '=', $p_user->pid)
                    ->where('settlement_type', '2')
                    ->where('start_time', '=', $time_start)
                    ->where('end_time', '=', $time_end)
                    ->select('id')->first();

                if (!$settlementMonth) {
                    //用户金额结算
                    $User = User::where('id', $p_user->pid)->select('money')->first();
                    // Log::info("代理金额2===" . $User->id . "====" . $User->money);
                    $money = $User->money + $month_money;
                    User::where('id', $p_user->pid)->update(['money' => $money]);
                    $settlementDate = [
                        'user_id' => $primary_user_id,
                        'user_name' => $primary_user_name,
                        'start_time' => $time_start,
                        'end_time' => $time_end,
                        'rate' => $month_rate,
                        'transaction_amount' => $transaction_amount,
                        'total_amount' => $month_money,
                        'settlement_type' => '2',
                        'p_user_id' => $p_user->pid
                    ];
                    SettlementMonthList::create($settlementDate);
                }
                DB::commit();
            } catch (\Exception $e) {
                Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
                DB::rollBack();
                return json_encode([
                    'status' => '2',
                    'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
                ]);
            }
            if ($p_user_level_id != 9) {
                $this->circulate_reward($p_user->pid, $user_level, $time_start, $time_end, $primary_user_id, $primary_user_name);
            }
        }

    }


}
