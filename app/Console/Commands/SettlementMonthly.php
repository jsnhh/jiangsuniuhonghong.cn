<?php

namespace App\Console\Commands;

use App\Api\Controllers\BaseController;
use App\Models\AgentLevels;
use App\Models\SettlementMonthInfo;
use App\Models\SettlementMonthList;
use App\Models\User;
use App\Models\UserMonthLevel;
use App\Models\UserWalletDetail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SettlementMonthly extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'settlementMonthly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '代理商分润月结';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            Log::info('月结---------');
            //获取上一个月时间
            $time_start = date("Y-m-01 00:00:00", strtotime("-1 months")); //获取上个月月初的时间
            //获取上一个月最后一天
            $time_end = date("Y-m-d 23:59:59", strtotime(-date('d') . 'day'));
            //$users = User::where('is_delete', '0')->where('id', 3988)->select('id', 'money', 'name', 'config_id')->get();
            $users = User::where('is_delete', '0')->select('id', 'money', 'name', 'config_id')->get();
            foreach ($users as $user) {
                $table = DB::table('orders');
                $wallet_obj = $table->select([
                    'total_amount',
                    DB::raw("SUM(`total_amount`) AS `month_pay_money`"),
                ])
                    ->where('user_id', $user->id)
                    ->where('pay_status', 1)//成功
                    ->where('created_at', '>=', $time_start)
                    ->where('created_at', '<=', $time_end)
                    ->first();
                $month_pay_money = 0.00;
                if ($wallet_obj->month_pay_money) {
                    $month_pay_money = $wallet_obj->month_pay_money;
                }
                $level_weight=1;
                $userMonthLevel = UserMonthLevel::where('user_id', $user->id)
                    ->where('start_time', $time_start)
                    ->where('end_time', $time_end)
                    ->select('*')->first();
                if($userMonthLevel){
                    $level_weight=$userMonthLevel->level_weight;
                };
                $agentLevel = AgentLevels::where('level_weight', $level_weight)
                    ->select('*')
                    ->first();
                $month_rate = $agentLevel->rate - 10;
                $month_rate = $month_rate / 10000;
                $month_money = $month_rate * $month_pay_money;
                //开启事务
                try {
                    DB::beginTransaction();
                    $settlementMonth = SettlementMonthList::where('user_id', $user->id)
                        ->where('settlement_type', '1')
                        ->where('start_time', '=', $time_start)
                        ->where('end_time', '=', $time_end)
                        ->select('id')->first();
                    if (!$settlementMonth) {
                        //用户金额结算
                        $money = $user->money + $month_money;
                        User::where('id', $user->id)->update(['money' => $money]);
                        $settlementDate = [
                            'user_id' => $user->id,
                            'user_name' => $user->name,
                            'start_time' => $time_start,
                            'end_time' => $time_end,
                            'transaction_amount' => $month_pay_money,
                            'total_amount' => $month_money,
                            'rate' => $month_rate,
                            'settlement_type' => '1',
                        ];
                        SettlementMonthList::create($settlementDate);
                    }
                    DB::commit();


                } catch (\Exception $e) {
                    Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
                    DB::rollBack();
                    return json_encode([
                        'status' => '2',
                        'message' => $e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine()
                    ]);
                }
                //$this->sub_settlement($user->id, $time_start, $time_end, $level_weight);
            }


        } catch (\Exception $exception) {
            Log::info('服务商月结定时任务error');
            Log::info($exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine());
        }
    }

    public function sub_settlement($p_user_id, $time_start, $time_end, $p_level_weight)
    {
        try {
            Log::info("下级=============");
            $users = User::where('pid', $p_user_id)->where('is_delete', '0')->where('created_at', '<=', $time_end)->select('id', 'name')->get();
            foreach ($users as $user) {
                $userMonthLevel = UserMonthLevel::where('user_id', $user->id)
                    ->where('start_time', $time_start)
                    ->where('end_time', $time_end)
                    ->select('*')->first();
                $sub_level_weight = 1;
                $transaction_amount = 0.00;
                if ($userMonthLevel) {
                    $sub_level_weight = $userMonthLevel->level_weight;
                    $transaction_amount = $userMonthLevel->transaction_amount;
                }
                $agentLevel = AgentLevels::where('level_weight', $sub_level_weight)
                    ->select('*')
                    ->first();
                $p_agent_level = AgentLevels::where('level_weight', $p_level_weight)
                    ->select('*')
                    ->first();
                if ($sub_level_weight >= $p_agent_level->level_weight) {
                    $settlementMonth = SettlementMonthList::where('user_id', $user->id)
                        ->where('p_user_id', '=', $p_user_id)
                        ->where('settlement_type', '2')
                        ->where('start_time', '=', $time_start)
                        ->where('end_time', '=', $time_end)
                        ->select('id')->first();

                    if (!$settlementMonth) {
                        $settlementDate = [
                            'user_id' => $user->id,
                            'user_name' => $user->name,
                            'start_time' => $time_start,
                            'end_time' => $time_end,
                            'rate' => 0.00,
                            'transaction_amount' => $transaction_amount,
                            'total_amount' => 0.00,
                            'settlement_type' => '2',
                            'p_user_id' => $p_user_id
                        ];
                        SettlementMonthList::create($settlementDate);
                    }
                    continue;
                }
                $baseController = new BaseController();
                $ids = $baseController->getSubIdsAll($user->id);
                unset($ids[0]);
                if($ids){
                    //Log::info("原始ids=====================".json_encode($ids));
                    //查出所有下级需要减掉的交易量
                    $user_level_ids = UserMonthLevel::whereIn('user_id', $ids)->where('start_time', $time_start)
                        ->where('end_time', $time_end)->where('is_deduct', '2')->select('user_id')->get();
                    Log::info(json_encode($user_level_ids));
                    foreach ($user_level_ids as $id) {
                        $sub_ids = $baseController->getSubIdsAll($id->user_id);
                        if(!$sub_ids){
                            continue;
                        }
                        unset($sub_ids[0]);
                        foreach ($sub_ids as $sub_id) {
                            foreach ($ids as $key => $value) {
                                if($sub_id==$value){
                                    //Log::info("减掉=====================".$value."=====".$key);
                                    unset($ids[$key]);
                                }
                            }
                        }
                    }
                    //Log::info("处理后的ids=====================".json_encode($ids));
                    //查询下级是否存在比本级高的等级
                    $table = DB::table('user_month_level');
                    $sub_obj = $table->select([
                        'transaction_amount',
                        DB::raw("SUM(`transaction_amount`) AS `transaction_amount`"),
                    ])
                        ->whereIn('user_id', $ids)
                        ->where('start_time', $time_start)
                        ->where('end_time', $time_end)
                        ->where('is_deduct', '2')
                        ->where('level_weight', '>=', $p_level_weight)
                        ->first();
                    if ($sub_obj->transaction_amount) {//存在比本级高的代理减去交易量
                        $transaction_amount = $transaction_amount - $sub_obj->transaction_amount;
                    }
                }

                $month_rate = $p_agent_level->rate - $agentLevel->rate;
                $month_rate = $month_rate / 10000;
                $month_money = $month_rate * $transaction_amount;
                $settlementMonth = SettlementMonthList::where('user_id', $user->id)
                    ->where('p_user_id', '=', $p_user_id)
                    ->where('settlement_type', '2')
                    ->where('start_time', '=', $time_start)
                    ->where('end_time', '=', $time_end)
                    ->select('id')->first();

                if (!$settlementMonth) {
                    //用户金额结算
                    $p_user = User::where('id', $p_user_id)->select('money')->first();
                    $money = $p_user->money + $month_money;
                    User::where('id', $p_user_id)->update(['money' => $money]);
                    $settlementDate = [
                        'user_id' => $user->id,
                        'user_name' => $user->name,
                        'start_time' => $time_start,
                        'end_time' => $time_end,
                        'rate' => $month_rate,
                        'transaction_amount' => $transaction_amount,
                        'total_amount' => $month_money,
                        'settlement_type' => '2',
                        'p_user_id' => $p_user_id
                    ];
                    SettlementMonthList::create($settlementDate);
                }
            }
        } catch (\Exception $exception) {
            Log::info('服务商月结级差定时任务error');
            Log::info($exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine());
        }

    }

}
