<?php

namespace App\Console\Commands;

use App\Api\Controllers\BaseController;
use App\Models\AgentLevels;
use App\Models\AgentLevelsInfos;
use App\Models\SettlementMonthInfo;
use App\Models\User;
use App\Models\UserMonthLevel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserMonthMoneyTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'userMonthMoneyTask';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '计算代理上月交易量';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            Log::info("计算代理上月交易量=========");
            //获取上一个月时间
            $time_start = date("Y-m-01 00:00:00", strtotime("-1 months")); //获取上个月月初的时间
            //获取上一个月最后一天
            $time_end = date("Y-m-d 23:59:59", strtotime(-date('d') . 'day'));
            //$users = User::where('is_delete', '0')->where('id', '=', 23)->select('id', 'money', 'name', 'config_id', 'pid')->get();
            $users = User::where('is_delete', '0')->where('id', '!=', 1)->select('id', 'money', 'name', 'config_id', 'pid')->get();
            $baseController = new BaseController();
            foreach ($users as $user) {
                $ids = $baseController->getSubIdsAll($user->id);
                $month_pay_money = 0.00;
                $table = DB::table('orders');
                $wallet_obj = $table->select([
                    'total_amount',
                    DB::raw("SUM(`total_amount`) AS `month_pay_money`"),
                ])
                    ->whereIn('user_id', $ids)
                    ->where('pay_status', 1)//成功
                    ->where('created_at', '>=', $time_start)
                    ->where('created_at', '<=', $time_end)
                    ->first();
                if ($wallet_obj->month_pay_money) {
                    $month_pay_money = $wallet_obj->month_pay_money;
                }
                $userMonthLevel = UserMonthLevel::where('user_id', $user->id)
                    ->where('start_time', $time_start)
                    ->where('end_time', $time_end)
                    ->select('id')->first();
                if ($userMonthLevel) {
                    $up = [
                        'transaction_amount' => $month_pay_money,
                        'start_time' => $time_start,
                        'end_time' => $time_end,
                    ];
                    UserMonthLevel::where('user_id', $user->id)->where('start_time', $time_start)
                        ->where('end_time', $time_end)->update($up);
                } else {
                    $data = [
                        'user_id' => $user->id,
                        'transaction_amount' => $month_pay_money,
                        'start_time' => $time_start,
                        'end_time' => $time_end,
                    ];
                    UserMonthLevel::create($data);
                }
            }

        } catch (\Exception $exception) {
            Log::info('计算代理上月交易量error');
            Log::info($exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine());
        }
    }

}
