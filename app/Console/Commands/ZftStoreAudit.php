<?php
namespace App\Console\Commands;


use Alipayopen\Sdk\AopClient;
use Alipayopen\Sdk\Request\AntMerchantExpandOrderQueryRequest;
use Aliyun\AliSms;
use App\Api\Controllers\Config\AlipayIsvConfigController;
use App\Api\Controllers\Config\VbillConfigController;
use App\Models\AlipayZftStore;
use App\Models\SmsConfig;
use App\Models\StorePayWay;
use App\Models\VbillStore;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ZftStoreAudit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zft-audit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'zft-audit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("alipay_zft_stores");
            } else {
                $obj = DB::table('alipay_zft_stores');
            }

            $alipay_zft_stores = $obj->where('status', '031')->first();
            if ($alipay_zft_stores) {
                $store_id = $alipay_zft_stores->store_id;
                $config_id = $alipay_zft_stores->config_id;
                $order_id = $alipay_zft_stores->order_id;

                //配置
                $isvconfig = new AlipayIsvConfigController();
                $config_type = '03';//直付通
                $config = $isvconfig->AlipayIsvConfig($config_id, $config_type);
                if ($config) {
                    $aop = new AopClient();
                    $aop->useHeader = false;
                    $aop->appId = $config->app_id;
                    $aop->rsaPrivateKey = $config->rsa_private_key;
                    $aop->alipayrsaPublicKey = $config->alipay_rsa_public_key;
                    $aop->gatewayUrl = $config->alipay_gateway;
                    $aop->apiVersion = '1.0';
                    $aop->signType = 'RSA2';
                    $aop->format = 'json';
                    $aop->method = "ant.merchant.expand.order.query";
                    $request = new AntMerchantExpandOrderQueryRequest();
                    $request->setBizContent("{" .
                        "\"order_id\":\"" . $order_id . "\"" .
                        "  }");
                    $result = $aop->execute($request);

                    $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
                    $resultCode = $result->$responseNode->code;
                    if (!empty($resultCode) && $resultCode == 10000) {
                        $ext_info = $result->$responseNode->ext_info;
                        $data = json_decode($ext_info, true);
                        //成功
                        if ($result->$responseNode->status == "99") {
                            $smid = $data['smid'];
                            $card_alias_no = isset($data['card_alias_no']) ? $data['card_alias_no'] : "";

                            AlipayZftStore::where('store_id', $store_id)
                                ->update([
                                    'ext_info' => $ext_info,
                                    'status' => '99',
                                    'smid' => $smid,
                                    'card_alias_no' => $card_alias_no,
                                ]);

                            $data_up = [
                                'status' => 1,
                                'status_desc' => '审核成功',
                            ];

                            StorePayWay::where('store_id', $store_id)
                                ->where('company', 'zft')
                                ->update($data_up);
                        }

                        //失败
                        if ($result->$responseNode->status == "-1") {
                            AlipayZftStore::where('store_id', $store_id)
                                ->update([
                                    'ext_info' => $ext_info,
                                    'status' => '-1',
                                ]);

                            $data_up = [
                                'status' => 3,
                                'status_desc' => '审核失败',
                                'pcredit'=>'01',
                                'credit'=>"00",
                            ];
                            StorePayWay::where('store_id', $store_id)
                                ->where('company', 'zft')
                                ->update($data_up);
                        }
                    } else {
                        AlipayZftStore::where('store_id', $store_id)
                            ->update([
                                'ext_info' => '',
                                'status' => '-1',
                            ]);

                        $data_up = [
                            'status' => 3,
                            'status_desc' => '系统报错请检查配置-'.$resultCode,
                            'pcredit'=>'01',
                            'credit'=>"00",
                        ];
                        StorePayWay::where('store_id', $store_id)
                            ->where('company', 'zft')
                            ->update($data_up);

                    }
                }
            }

        } catch (\Exception $exception) {
            Log::info('直付通同步门店状态报错');
            Log::info($exception);
        }
    }


    public function send($name, $status, $phone, $config_id = '1234')
    {
        try {
            $config = SmsConfig::where('type', '7')->where('config_id', $config_id)->first();
            if (!$config) {
                $config = SmsConfig::where('type', '7')->where('config_id', '1234')->first();
            }

            if ($config && $config->app_key && $phone) {
                $data = ["name" => $name, 'status' => $status];
                $this->sendSms($phone, $config->app_key, $config->app_secret, $config->SignName, $config->TemplateCode, $data);
            }
        } catch (\Exception $exception) {
        }
    }


    public function sendSms($phone, $app_key, $app_secret, $SignName, $TemplateCode, $data)
    {
        $demo = new AliSms($app_key, $app_secret);
        $response = $demo->sendSms(
            $SignName, // 短信签名
            $TemplateCode, // 短信模板编号
            $phone, // 短信接收者
            $data
        );
        return $response;

    }


}
