<?php

namespace App\Console\Commands;


use App\Api\Controllers\Config\EasyPayConfigController;
use App\Api\Controllers\Merchant\MerchantRechargeController;
use App\Models\Store;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RechargeOrderSuccess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recharge_order_query';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '更新商户充值等待支付的订单-易生';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        for ($i = 0; $i < 60; $i++) {
            sleep(3);
            $this->handle_query();
        }
    }

    public function handle_query()
    {
        $time_end = date('Y-m-d H:i:s', time());
        $time_start = date('Y-m-d 00:00:00', strtotime($time_end) - 300); //5分钟之前的时间
        try {
            //等待支付订单信息
            $order_obj = DB::table('merchant_recharges')
                ->select('*')
                ->where('status', '2')
                ->where('pay_way', 'easy')
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->get();
            if ($order_obj) {
                foreach ($order_obj as $key => $values) {
                    $store_obj = Store::where('merchant_id', $values->merchant_id)
                        ->select('config_id', 'store_id', 'pid', 'people_phone')
                        ->first();
                    if (!$store_obj) {
                        continue;
                    }

                    $config_id = $store_obj->config_id;
                    $store_pid = $store_obj->pid;
                    $store_id = $store_obj->store_id;
                    $out_trade_no = $values->order_id;
                    $config = new EasyPayConfigController();
                    $easyPayConfig = $config->easypay_config($config_id);
                    if (!$easyPayConfig) {
                        continue;
                    }
                    $easyPayMerchant = $config->easypay_merchant($store_id, $store_pid);
                    if (!$easyPayMerchant) {
                        continue;
                    }
                    //易生支付
                    $obj = new \App\Api\Controllers\EasyPay\PayController();
                    $easyPayData = [
                        'channel_id' => $easyPayConfig->channel_id, //渠道编号
                        'mer_id' => $easyPayMerchant->term_mercode, //终端商戶编号
                        'device_id' => $easyPayMerchant->term_termcode, //终端编号
                        'out_trade_no' => $out_trade_no //原交易流水
                    ];
                    $return = $obj->order_query($easyPayData); //-1 系统错误 0-其他 1-成功 2-下单失败

                    if ($return['status'] == '1') {
                        $MerchantRechargeController = new MerchantRechargeController();
                        $data = [
                            'oriOrgTrace' => $out_trade_no
                        ];
                        $MerchantRechargeController->query_easy($data);
                    }

                }
            }
        } catch (\Exception $ex) {
            Log::info('等待支付状态轮询查询-错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

    }


}
