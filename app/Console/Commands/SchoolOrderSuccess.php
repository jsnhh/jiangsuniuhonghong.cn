<?php

namespace App\Console\Commands;

use App\Api\Controllers\Config\CcBankPayConfigController;
use App\Api\Controllers\Config\EasyPayConfigController;
use App\Api\Controllers\Config\EasySkPayConfigController;
use App\Api\Controllers\Config\HwcPayConfigController;
use App\Api\Controllers\Config\NewLandConfigController;
use App\Api\Controllers\Config\QfPayConfigController;
use App\Api\Controllers\Config\VbillConfigController;
use App\Api\Controllers\Config\WftPayConfigController;
use App\Api\Controllers\Merchant\TransactionDeductionController;
use App\Api\Controllers\Vbill\PayController;
use App\Api\Controllers\QfPay\PayController as QfPayController;
use App\Api\Controllers\HwcPay\PayController as HwcPayPayController;
use App\Api\Controllers\WftPay\PayController as WftPayPayController;
use App\Common\PaySuccessAction;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\RefundOrder;
use App\Models\Store;
use App\Models\StuOrder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use App\Api\Controllers\Config\PostPayConfigController;
use Illuminate\Support\Facades\Cache;

class SchoolOrderSuccess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'school_order_query';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '更新缴费等待支付的订单';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        for ($i = 0; $i < 60; $i++) {
            sleep(3);
            // usleep(500000); //纳秒
            $this->handle_query();
        }

    }
    public function handle_query()
    {
        // $time_start = date('Y-m-d 00:00:00', time());
        $time_end = date('Y-m-d H:i:s', time());
        $time_start = date('Y-m-d H:i:s', strtotime($time_end) - 300); //5分钟之前的时间
        // Log::info('查询订单执行 begTime:' .$time_start);
        // Log::info('查询订单执行 endTime:' .$time_end);

        try {
            //等待支付订单信息
            $order_obj = DB::table('stu_orders')
                ->select('*')
                // ->whereIn('company', ['vbill', 'vbilla', 'newland', 'easypay', 'qfpay', 'wftpay', 'hwcpay', 'postpay', 'ccbankpay'])
                ->whereIn('pay_status', ['2','3'])
                //->where('pay_status', '2')
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->get();
            if ($order_obj) {
                foreach ($order_obj as $key => $values) {
                    $store_obj = Store::where('store_id', $values->store_id)
                        ->select('config_id', 'merchant_id', 'pid', 'people_phone')
                        ->first();
                    if (!$store_obj) {
                        continue;
                    }

                    $config_id = $store_obj->config_id;
                    $store_pid = $store_obj->pid;
                    $store_id = $values->store_id;
                    $ways_type = $values->ways_type;
                    $out_trade_no = $values->out_trade_no;
                    $trade_no = $values->trade_no;
                    $pay_status = $values->pay_status;

                    //易生支付
                   if ('20999' < $ways_type && $ways_type < '21999') {
                        //读取配置
                        $config = new EasyPayConfigController();
                        $easypay_config = $config->easypay_config($config_id);
                        if (!$easypay_config) {
                            continue;
                        }

                        $easypay_merchant = $config->easypay_merchant($store_id, $store_pid);
                        if (!$easypay_merchant) {
                            continue;
                        }

                        $obj = new \App\Api\Controllers\EasyPay\PayController();
                        $easypay_data = [
                            'channel_id' => $easypay_config->channel_id, //渠道编号
                            'mer_id' => $easypay_merchant->term_mercode, //终端商戶编号
                            'device_id' => $easypay_merchant->term_termcode, //终端编号
                            'out_trade_no' => $out_trade_no //原交易流水
                        ];
//
                        $return = $obj->order_query($easypay_data); //-1 系统错误 0-其他 1-成功 2-下单失败
//

                        //支付成功
                        if ($return['status'] == '1') {
                            $pay_time = (isset($return['data']['timeEnd']) && !empty($return['data']['timeEnd'])) ? date('Y-m-d H:i:m', strtotime($return['data']['timeEnd'])) :  date('Y-m-d H:i:m',time()); //支付完成时间，格式为 yyyyMMddhhmmss，如 2009年12月27日9点10分10秒表示为 20091227091010
                            $trade_no = $return['data']['outTrace']; //系统订单号
                            //改变数据库状态
                            if ($pay_status != 1) {
                                $update = [
                                    'pay_status' => 1,
                                    'pay_status_desc' => '缴费成功',
                                    'trade_no' => $trade_no,
                                    'pay_time' => $pay_time,
                                    'gmt_end' => $pay_time,
                                ];

                                StuOrder::where('out_trade_no', $out_trade_no)->update($update);

                            }
                        } //订单失败关闭
                        elseif ($return['status'] == '3') {
                            //改变数据库状态
                            if ($pay_status != '1') {
                                $update = [
                                    'pay_status' => 3,
                                    'pay_status_desc' => '支付失败',
                                    'trade_no' => $trade_no,
                                ];

                                StuOrder::where('out_trade_no', $out_trade_no)->update($update);
                            } else {
                                continue;
                            }
                        } //其他
                        else {
                            continue;
                        }
                    }


                    // 易生数科 交易查询
                    if (32000 < $ways_type && $ways_type < 32010) {
                        //读取配置
                        $config = new EasySkPayConfigController();
                        $easyskpay_config = $config->easyskpay_config($config_id);
                        if (!$easyskpay_config) {
                            continue;
                        }

                        $easyskpay_merchant = $config->easyskpay_merchant($store_id, $store_pid);
                        if (!$easyskpay_merchant) {
                            continue;
                        }
                        $obj = new \App\Api\Controllers\EasySkPay\PayController();
                        $easyskpay_data = [
                            'org_id' => $easyskpay_config->org_id,
                            'mer_id' => $easyskpay_merchant->mer_id,
                            'orig_request_no' => $out_trade_no
                        ];

                        $return = $obj->order_query($easyskpay_data);
//                        Log::info($return);

                        //支付成功
                        if ($return['status'] == '1') {

                            $trade_no = $return['data']['tradeNo']; //系统订单号
                            $pay_time = (isset($return['data']['bizData']['payTime']) && !empty($return['data']['bizData']['payTime'])) ? date('Y-m-d H:i:m', strtotime($return['data']['bizData']['payTime'])) : date('Y-m-d H:i:m',time());
                            $buyer_pay_amount = isset($return['data']['bizData']['amount']) ? ($return['data']['bizData']['amount'] / 100) : ''; //实付金额，单位分
                            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                            //改变数据库状态
                            if ($pay_status != 1) {
                                $update = [
                                    'pay_status' => 1,
                                    'pay_status_desc' => '缴费成功',
                                    'trade_no' => $trade_no,
                                    'pay_time' => $pay_time,
                                    'gmt_end' => $pay_time,
                                ];

                                StuOrder::where('out_trade_no', $out_trade_no)->update($update);

                            }
                        } //订单失败关闭
                        elseif ($return['status'] == '3') {
                            //改变数据库状态
                            if ($pay_status != '1') {
                                $update = [
                                    'pay_status' => 3,
                                    'pay_status_desc' => '支付失败',
                                    'trade_no' => $trade_no,
                                ];

                                StuOrder::where('out_trade_no', $out_trade_no)->update($update);
                            } else {
                                continue;
                            }
                        } //其他
                        else {
                            continue;
                        }
                    }

                }
            }
        } catch (\Exception $ex) {
            Log::info('等待支付状态轮询查询-错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

    }


}
