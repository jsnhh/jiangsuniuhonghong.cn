<?php

namespace App\Console\Commands;


use App\Models\AgentLevels;
use App\Models\AgentLevelsInfos;
use App\Models\SettlementList;
use App\Models\User;
use App\Models\UserTrainReward;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class UserTrainRewardTask extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user_train_reward_task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'user_train_reward_task';

    /**
     * Settlement constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            //获取代理最高等级
            $user_level = AgentLevels::where('start_transaction', '>', 0)
                ->orderBy('code_num', 'desc')
                ->first();
            $user_level_weight = $user_level->level_weight;
            $current_time = date('Y-m-d 00:00:00', time());
            $sub_id = $user_level->sid;//下级等级
            $sid_num = $user_level->sid_num;//下级等级数量
            //查询代理等级表中达到最高等级的代理
            $user_level_infos = AgentLevelsInfos::where('level_weight', $user_level_weight)
                ->where('end_time', '>=', $current_time)
                ->get();
            if ($user_level_infos) {
                foreach ($user_level_infos as $user_level_info) {
                    $type = '1';//代理商等级表
                    $this->user_level_exist($user_level_weight, $current_time, $user_level_info->user_id, $sid_num, $sub_id, $type);
                }
            }

            //查询交易量达到最高等级的代理
            $settlementLists = SettlementList::where('commission_type', '2')
                ->where('created_at', '>=', $current_time)
                ->where('total_amount', '>=', $user_level->start_transaction)
                ->get();

            if ($settlementLists) {
                $qualify_user_arr = [];
                foreach ($settlementLists as $settlementList) {
                    //判断交易量达标要求下级代理等级和数量是否达标
                    $type = '2'; //交易量达标
                    $res = $this->sub_user_level($settlementList->user_id, $sub_id, $sid_num, $type);
                    if ($res) {
                        array_push($qualify_user_arr, $settlementList);
                    }

                }
                if ($qualify_user_arr) {
                    foreach ($qualify_user_arr as $qualify_user) {
                        $this->user_level_exist($user_level_weight, $current_time, $qualify_user->user_id, $sid_num, $sub_id, '2');
                    }
                }
            }

        } catch (\Exception $exception) {
            Log::info('培养奖定时任务error');
            Log::info($exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine());
        }
    }

    public function user_level_exist($user_level_weight, $current_time, $user_id, $sid_num, $sub_id, $type)
    {
        $s_time = date('Y-m-d 00:00:00', time());
        $e_time = date('Y-m-d 23:59:59', time());
        $users = User::where('pid', $user_id)->select('id')->get()
            ->toArray();
        $subIDs = array_column($users, 'id');
        //培养奖-本金代理和下级代理同时达到最高级比，所以level_weight和上级的是一样的
        $user_level_infos = AgentLevelsInfos::where('level_weight', $user_level_weight)
            ->where('end_time', '>=', $current_time)
            ->whereIn('user_id', $subIDs)
            ->get();

        $this->reward($user_level_infos, $s_time, $e_time, $user_id);

        $agentLevelsInfo = AgentLevelsInfos::where('user_id', $user_id)
            ->where('level_weight', $user_level_weight)
            ->where('end_time', '>=', $current_time)
            ->select('*')
            ->first();
        $user_level_infos = [];
        if ($agentLevelsInfo && $type == '2') {
            $res = $this->sub_user_level($user_id, $sub_id, $sid_num, $type);
            if ($res) {
                $settlementList = SettlementList::where('commission_type', '2')
                    ->where('created_at', '>=', $s_time)
                    ->where('created_at', '<=', $e_time)
                    ->first();
                array_push($user_level_infos, $settlementList);
            }
        } else {
            $agentLevel = AgentLevels::where('level_weight', $sub_id)->select('*')->first();
            $settlementLists = SettlementList::where('commission_type', '2')
                ->where('created_at', '>=', $s_time)
                ->where('created_at', '<=', $e_time)
                ->where('total_amount', '>=', $agentLevel->start_transaction)
                ->whereIn('user_id', $subIDs)
                ->get();
            //下级等数量不达标 将不在走下面代码
            if (!$settlementLists || count($settlementLists) < $sid_num) {
                return;
            }

            foreach ($settlementLists as $settlementList) {
                //查询下级等级是否满足要求
                $res = $this->sub_user_level($settlementList->user_id, $sub_id, $sid_num, $type);
                if ($res) {
                    array_push($user_level_infos, $settlementList);
                }
            }
        }
        $this->reward($user_level_infos, $s_time, $e_time, $user_id);

    }

    //判断要求下级代理等级和数量是否达标
    public function sub_user_level($user_id, $s_id, $sid_num, $type): bool
    {
        $current_time = date('Y-m-d H:i:s', time());
        $s_time = date('Y-m-d 00:00:00', time());
        $e_time = date('Y-m-d 23:59:59', time());
        $user_level = AgentLevels::where('start_transaction', '>', 0)
            ->orderBy('code_num', 'desc')
            ->first();
        if ($type == '2') {
            $agentLevelsInfo = AgentLevelsInfos::where('user_id', $user_id)
                ->where('level_weight', $user_level->level_weight)
                ->where('end_time', '>=', $current_time)
                ->select('*')
                ->first();
            if ($agentLevelsInfo) {//等级表中存在不用校验下级
                return true;
            }
        }

        $users = User::where('pid', $user_id)->where('is_delete', '0')->select('id')->get();
        $users = $users->toArray();
        $subIDs = array_column($users, 'id');
        $agentLevelsInfos = AgentLevelsInfos::where('level_weight', $s_id)
            ->where('end_time', '>=', $current_time)
            ->whereIn('user_id', $subIDs)
            ->get();

        $level_num = count($agentLevelsInfos);
        if ($level_num >= $sid_num) {
            return true;
        } else {
            $agentLevel = AgentLevels::where('level_weight', $s_id)->select('*')->first();
            $settlementLists = SettlementList::where('commission_type', '2')
                ->where('created_at', '>=', $s_time)
                ->where('created_at', '<=', $e_time)
                ->where('total_amount', '>=', $agentLevel->start_transaction)
                ->whereIn('user_id', $subIDs)
                ->get();

            if ($settlementLists && count($settlementLists) >= $sid_num) {
                return true;
            } else {
                return false;
            }

        }

    }

    public function reward($user_level_infos, $s_time, $e_time, $user_id)
    {
        //循环奖励
        $user = User::where('is_delete', '0')->where('id', $user_id)->select('id', 'money', 'name', 'created_at')->first();
        if (!$user) {
            return;
        }
        if ($user_level_infos) {
            foreach ($user_level_infos as $user_level_info) {
                $settlement = SettlementList::where('user_id', $user_level_info->user_id)
                    ->where('commission_type', '2')
                    ->where('created_at', '>=', $s_time)
                    ->where('created_at', '<=', $e_time)
                    ->first();
                if (!$settlement) {
                    continue;
                }
                $userTrainReward = UserTrainReward::where('sub_user_id', $user_level_info->user_id)
                    ->where('user_id', $user->id)
                    ->where('created_at', '>=', $s_time)
                    ->where('created_at', '<=', $e_time)
                    ->first();
                if ($userTrainReward) {//本月奖励了不在奖励
                    continue;
                }
                $train_reward = $settlement->total_amount * 0.2 / 10000;
                if ($train_reward > 20000) {//单月最高2万
                    $train_reward = 20000;
                }

                //开启事务
                try {
                    DB::beginTransaction();
                    $money = $user->money + $train_reward;
                    User::where('id', $user->id)->update(['money' => $money]);
                    $userTrainRewardData = [
                        'user_id' => $user->id,
                        'user_name' => $user->name,
                        'sub_user_id' => $user_level_info->user_id,
                        'transaction_amount' => $settlement->total_amount,
                        'train_reward_amount' => $train_reward,
                        'train_reward_rate' => 0.00002
                    ];
                    UserTrainReward::create($userTrainRewardData);
                    DB::commit();
                } catch (\Exception $e) {
                    Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
                    DB::rollBack();
                    continue;
                }
            }
        }
    }

}
