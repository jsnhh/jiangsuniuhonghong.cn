<?php

namespace App\Console\Commands;

use App\Api\Controllers\BaseController;
use App\Api\Controllers\Config\AllinPayConfigController;
use App\Api\Controllers\Config\CcBankPayConfigController;
use App\Api\Controllers\Config\EasyPayConfigController;
use App\Api\Controllers\Config\EasySkPayConfigController;
use App\Api\Controllers\Config\FuiouConfigController;
use App\Api\Controllers\Config\HwcPayConfigController;
use App\Api\Controllers\Config\LklConfigController;
use App\Api\Controllers\Config\NewLandConfigController;
use App\Api\Controllers\Config\QfPayConfigController;
use App\Api\Controllers\Config\VbillConfigController;
use App\Api\Controllers\Config\WftPayConfigController;
use App\Api\Controllers\Config\YinshengConfigController;
use App\Api\Controllers\Merchant\TransactionDeductionController;
use App\Api\Controllers\Vbill\PayController;
use App\Api\Controllers\QfPay\PayController as QfPayController;
use App\Api\Controllers\HwcPay\PayController as HwcPayPayController;
use App\Api\Controllers\WftPay\PayController as WftPayPayController;
use App\Common\PaySuccessAction;
use App\Models\EasypayStoresImages;
use App\Models\FuiouConfig;
use App\Models\FuiouStore;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\RefundOrder;
use App\Models\Store;
use App\Models\StuOrder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use App\Api\Controllers\Config\PostPayConfigController;
use Illuminate\Support\Facades\Cache;

class VbillOrderSuccess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vbill_order_query';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '更新等待支付的订单';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        for ($i = 0; $i < 20; $i++) {
            // for ($i = 0; $i < 120; $i++) {
            sleep(3);
            // usleep(500000); //纳秒
            $this->handle_query();
        }

        //建行通道 3s处理一次
        for ($i = 0; $i < 12; $i++) {
            // for ($i = 0; $i < 20; $i++) {
            sleep(5);
            // sleep(3);
            $this->handle_query_ccbank();
        }

    }


    //当天交易公共更改
    public function update_day_order($update_data, $out_trade_no, $table = "")
    {
        try {
            if ($table == "") {
                $day = date('Ymd', time());
                $table = 'orders_' . $day;
            }

            if (Schema::hasTable($table)) {
                $order = DB::table($table)
                    ->where('out_trade_no', $out_trade_no)
                    ->update($update_data);
            } else {
                $order = Order::where('out_trade_no', $out_trade_no)
                    ->update($update_data);
            }
            if (Schema::hasTable('order_items')) {
                OrderItem::where('out_trade_no', $out_trade_no)->update($update_data);
            }

            return $order;
        } catch (\Exception $exception) {
            Log::info('update_order');
            Log::info($exception);
        }
    }


    public function handle_query()
    {
        // $time_start = date('Y-m-d 00:00:00', time());
        $time_end = date('Y-m-d H:i:s', time());
        $time_start = date('Y-m-d H:i:s', strtotime($time_end) - 300); //5分钟之前的时间
        // Log::info('查询订单执行 begTime:' .$time_start);
        // Log::info('查询订单执行 endTime:' .$time_end);

        try {
            //等待支付订单信息
            $order_obj = DB::table('orders')
                ->select(['out_trade_no', 'trade_no', 'qwx_no', 'other_no', 'store_id', 'ways_type', 'company', 'ways_type_desc', 'total_amount', 'rate', 'fee_amount', 'merchant_id', 'user_id', 'store_name', 'ways_source', 'device_id', 'pay_status', 'created_at'])
                // ->whereIn('company', ['vbill', 'vbilla', 'newland', 'easypay', 'qfpay', 'wftpay', 'hwcpay', 'postpay', 'ccbankpay'])
                ->whereIn('pay_status', [2, 3])
                //->where('pay_status', 2)
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->get();

            if ($order_obj) {
                foreach ($order_obj as $key => $values) {
                    $store_obj = Store::where('store_id', $values->store_id)
                        ->select('config_id', 'merchant_id', 'pid', 'people_phone')
                        ->first();
                    if (!$store_obj) {
                        continue;
                    }

                    $config_id = $store_obj->config_id;
                    $store_pid = $store_obj->pid;
                    $store_id = $values->store_id;
                    $ways_type = $values->ways_type;
                    $out_trade_no = $values->out_trade_no;
                    $trade_no = $values->trade_no;
                    $pay_status = $values->pay_status;
                    $merchant_id = $values->merchant_id;
                    $ways_source = $values->ways_source;
                    $total_amount = $values->total_amount;
                    $company = $values->company;
                    $ways_type_desc = $values->ways_type_desc;
                    $rate = $values->rate;
                    $fee_amount = $values->fee_amount;
                    $user_id = $values->user_id;
                    $store_name = $values->store_name;
                    $shop_date = $values->created_at;

                    //富友
                    if (11000 < $ways_type && $ways_type < 11010) {
                        $config = new FuiouConfigController();
                        $fuiou_config = $config->fuiou_config($config_id);
                        if (!$fuiou_config) {
                            continue;
                        }

                        $fuiou_merchant = $config->fuiou_merchant($store_id, $store_pid);
                        if (!$fuiou_merchant) {
                            continue;
                        }

                        $obj = new \App\Api\Controllers\Fuiou\PayController();
                        $data['ins_cd'] = $fuiou_config->ins_cd;//机构号
                        $data['mchnt_cd'] = $fuiou_merchant->mchnt_cd;
                        $data['term_id'] = '88888888';
                        if ($ways_source == 'alipay') {
                            $data['order_type'] = 'ALIPAY';
                        }
                        if ($ways_source == 'weixin') {
                            $data['order_type'] = 'WECHAT';
                        }
                        if ($ways_source == 'unionpay') {
                            $data['order_type'] = 'UNIONPAY';
                        }
                        $data['mchnt_order_no'] = $out_trade_no;
                        $data['pem'] = $fuiou_config->my_private_key;
                        $return = $obj->order_query($data);

                        //支付成功
                        if ($return['status'] == '1') {
                            $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['reserved_txn_fin_ts']));
                            $trade_no = $return['data']['transaction_id'];
                            $buyer_pay_amount = $total_amount;
                            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                            // 实收金额（暂定）
                            $receipt_amount = $buyer_pay_amount;

                            //改变数据库状态
                            if ($pay_status != '1') {
                                $insert_data = [
                                    'status' => '1',
                                    'pay_status' => '1',
                                    'pay_status_desc' => '支付成功',
                                    'buyer_logon_id' => '',
                                    'trade_no' => $trade_no,
                                    'pay_time' => $pay_time,
                                    'buyer_pay_amount' => $buyer_pay_amount,
                                    'receipt_amount' => $receipt_amount,
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);

                                //支付成功后的动作
                                $paySucData = [
                                    'ways_type' => $ways_type,
                                    'company' => $company,
                                    'ways_type_desc' => $ways_type_desc,
                                    'source_type' => '11000',
                                    'source_desc' => '富友',
                                    'total_amount' => $total_amount,
                                    'out_trade_no' => $out_trade_no,
                                    'rate' => $rate,
                                    'fee_amount' => $fee_amount,
                                    'merchant_id' => $merchant_id,
                                    'store_id' => $store_id,
                                    'user_id' => $user_id,
                                    'config_id' => $config_id,
                                    'store_name' => $store_name,
                                    'ways_source' => $ways_source,
                                    'pay_time' => $pay_time,
                                    'device_id' => isset($values->device_id) ? $values->device_id : '',
                                ];

                                if (Cache::has('console_out_trade_no_' . $out_trade_no)) {
                                    Log::info('--fuiou--检查是否操作---Cache.  ---console_out_trade_no_:' . $out_trade_no);
                                    return;
                                }

                                Cache::add('console_out_trade_no_' . $out_trade_no, '1', 1);

                                PaySuccessAction::action($paySucData);
                            } else {
                                continue;
                            }
                        } //订单失败关闭
                        elseif ($return['status'] == '3') {
                            //改变数据库状态
                            if ($pay_status != '1') {
                                $insert_data = [
                                    'status' => '3',
                                    'pay_status' => '3',
                                    'pay_status_desc' => '支付失败',
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);
                            } else {
                                continue;
                            }
                        } //其他
                        else {
                            continue;
                        }
                    }

                    //随行付
                    if (12999 < $ways_type && $ways_type < 13999) {
                        $config = new VbillConfigController();
                        $vbill_config = $config->vbill_config($config_id);
                        if (!$vbill_config) {
                            continue;
                        }

                        $vbill_merchant = $config->vbill_merchant($store_id, $store_pid);
                        if (!$vbill_merchant) {
                            continue;
                        }

                        $obj = new PayController();
                        $data['request_url'] = $obj->order_query_url;
                        $data['mno'] = $vbill_merchant->mno;
                        $data['privateKey'] = $vbill_config->privateKey;
                        $data['sxfpublic'] = $vbill_config->sxfpublic;
                        $data['orgId'] = $vbill_config->orgId;
                        $data['out_trade_no'] = $out_trade_no;
                        $return = $obj->order_query($data);
                        // Log::info('随行付-定时任务查询等待支付-结果');
                        // Log::info($return);

                        //支付成功
                        if ($return['status'] == '1') {
                            $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['respData']['tranTime']));
                            $trade_no = $return['data']['respData']['sxfUuid'];
                            $buyer_pay_amount = $return['data']['respData']['totalOffstAmt'];
                            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                            // 实收金额（暂定）
                            $receipt_amount = isset($return['data']['respData']['settleAmt']) ? $return['data']['respData']['settleAmt'] : $buyer_pay_amount;

                            //改变数据库状态
                            if ($pay_status != '1') {
                                $insert_data = [
                                    'status' => '1',
                                    'pay_status' => '1',
                                    'pay_status_desc' => '支付成功',
                                    'buyer_logon_id' => '',
                                    'trade_no' => $trade_no,
                                    'pay_time' => $pay_time,
                                    'buyer_pay_amount' => $buyer_pay_amount,
                                    'receipt_amount' => $receipt_amount,
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);

                                //支付成功后的动作
                                $paySucData = [
                                    'ways_type' => $ways_type,
                                    'company' => $company,
                                    'ways_type_desc' => $ways_type_desc,
                                    'source_type' => '13000',
                                    'source_desc' => '随行付',
                                    'total_amount' => $total_amount,
                                    'out_trade_no' => $out_trade_no,
                                    'rate' => $rate,
                                    'fee_amount' => $fee_amount,
                                    'merchant_id' => $merchant_id,
                                    'store_id' => $store_id,
                                    'user_id' => $user_id,
                                    'config_id' => $config_id,
                                    'store_name' => $store_name,
                                    'ways_source' => $ways_source,
                                    'pay_time' => $pay_time,
                                    'device_id' => isset($values->device_id) ? $values->device_id : '',
                                ];

                                if (Cache::has('console_out_trade_no_' . $out_trade_no)) {
                                    Log::info('--Vibill--检查是否操作---Cache.  ---console_out_trade_no_:' . $out_trade_no);
                                    return;
                                }

                                Cache::add('console_out_trade_no_' . $out_trade_no, '1', 1);

                                PaySuccessAction::action($paySucData);
                            } else {
                                continue;
                            }
                        } //订单失败关闭
                        elseif ($return['status'] == '3') {
                            //改变数据库状态
                            if ($pay_status != '1') {
                                $insert_data = [
                                    'status' => '3',
                                    'pay_status' => '3',
                                    'pay_status_desc' => '支付失败',
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);
                            } else {
                                continue;
                            }
                        } //其他
                        else {
                            continue;
                        }
                    }

                    //随行付a
                    if (18999 < $ways_type && $ways_type < 19999) {
                        $config = new VbillConfigController();
                        $vbilla_config = $config->vbilla_config($config_id);
                        if (!$vbilla_config) {
                            continue;
                        }

                        $vbilla_merchant = $config->vbilla_merchant($store_id, $store_pid);
                        if (!$vbilla_merchant) {
                            continue;
                        }

                        $obj = new PayController();
                        $data['request_url'] = $obj->order_query_url;
                        $data['mno'] = $vbilla_merchant->mno;
                        $data['privateKey'] = $vbilla_config->privateKey;
                        $data['sxfpublic'] = $vbilla_config->sxfpublic;
                        $data['orgId'] = $vbilla_config->orgId;
                        $data['out_trade_no'] = $out_trade_no;
                        $return = $obj->order_query($data);
                        // Log::info('随行付A-定时任务查询等待支付-结果');
                        // Log::info($return);

                        //支付成功
                        if ($return['status'] == '1') {
                            $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['respData']['tranTime']));
                            $trade_no = $return['data']['respData']['sxfUuid'];
                            $buyer_pay_amount = $return['data']['respData']['totalOffstAmt'];
                            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                            // 实收金额（暂定）
                            $receipt_amount = isset($return['data']['respData']['settleAmt']) ? $return['data']['respData']['settleAmt'] : $buyer_pay_amount;

                            //改变数据库状态
                            if ($pay_status != '1') {
                                $insert_data = [
                                    'status' => '1',
                                    'pay_status' => '1',
                                    'pay_status_desc' => '支付成功',
                                    'buyer_logon_id' => '',
                                    'trade_no' => $trade_no,
                                    'pay_time' => $pay_time,
                                    'buyer_pay_amount' => $buyer_pay_amount,
                                    'receipt_amount' => $receipt_amount,
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);

                                //支付成功后的动作
                                $paySucData = [
                                    'ways_type' => $ways_type,
                                    'company' => $company,
                                    'ways_type_desc' => $ways_type_desc,
                                    'source_type' => '19000',//返佣来源
                                    'source_desc' => '随行付A',//返佣来源说明
                                    'total_amount' => $total_amount,
                                    'out_trade_no' => $out_trade_no,
                                    'rate' => $rate,
                                    'fee_amount' => $fee_amount,
                                    'merchant_id' => $merchant_id,
                                    'store_id' => $store_id,
                                    'user_id' => $user_id,
                                    'config_id' => $config_id,
                                    'store_name' => $store_name,
                                    'ways_source' => $ways_source,
                                    'pay_time' => $pay_time,
                                    'device_id' => isset($values->device_id) ? $values->device_id : '',
                                ];

                                if (Cache::has('console_out_trade_no_' . $out_trade_no)) {
                                    Log::info('--Vibill--检查是否操作---Cache.  ---console_out_trade_no_:' . $out_trade_no);
                                    return;
                                }

                                Cache::add('console_out_trade_no_' . $out_trade_no, '1', 1);

                                PaySuccessAction::action($paySucData);

                            } else {
                                continue;
                            }
                        } //订单失败关闭
//                        elseif ($return['status'] == '3') {
//                            //改变数据库状态
//                            if ($pay_status != '1') {
//                                $insert_data = [
//                                    'status' => '3',
//                                    'pay_status' => '3',
//                                    'pay_status_desc' => '支付失败',
//                                ];
//                                $this->update_day_order($insert_data, $out_trade_no);
//                            }
//                            else {
//                                continue;
//                            }
//                        }
                        //其他
                        else {
                            continue;
                        }
                    }

                    //新大陆
                    if (7999 < $ways_type && $ways_type < 8999) {
                        //读取配置
                        $config = new NewLandConfigController();
                        $new_land_config = $config->new_land_config($config_id);
                        if (!$new_land_config) {
                            continue;
                        }

                        $mybank_merchant = $config->new_land_merchant($store_id, $store_pid);
                        if (!$mybank_merchant) {
                            continue;
                        }

                        $request_data = [
                            'out_trade_no' => $out_trade_no,
                            'key' => $mybank_merchant->nl_key,
                            'org_no' => $new_land_config->org_no,
                            'merc_id' => $mybank_merchant->nl_mercId,
                            'trm_no' => $mybank_merchant->trmNo,
                            'op_sys' => '3',
                            'opr_id' => $merchant_id,
                            'trm_typ' => 'T',
                        ];
//                        Log::info('新大陆-轮询-交易查询');
//                        Log::info($request_data);
                        $obj = new \App\Api\Controllers\Newland\PayController();
                        $return = $obj->order_query($request_data);
//                        Log::info($return);

                        //支付成功
                        if ($return["status"] == 1) {
                            $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['sysTime']));
                            $trade_no = $return['data']['orderNo'];
                            $buyer_pay_amount = $return['data']['amount'] / 100;
                            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                            //改变数据库状态
                            if ($pay_status != 1) {
                                $insert_data = [
                                    'status' => '1',
                                    'pay_status' => 1,
                                    'pay_status_desc' => '支付成功',
                                    'buyer_logon_id' => '',
                                    'trade_no' => $trade_no,
                                    'pay_time' => $pay_time,
                                    'buyer_pay_amount' => $buyer_pay_amount,
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);

                                if (env('APP_URL') == 'https://admin.erspay.cn') {
                                    //支付成功后的动作
                                    $paySucData = [
                                        'ways_type' => $ways_type,
                                        'company' => $company,
                                        'ways_type_desc' => $ways_type_desc,
                                        'source_type' => '8000',
                                        'source_desc' => '新大陆',
                                        'total_amount' => $total_amount,
                                        'out_trade_no' => $out_trade_no,
                                        'rate' => $rate,
                                        'fee_amount' => $fee_amount,
                                        'merchant_id' => $merchant_id,
                                        'store_id' => $store_id,
                                        'user_id' => $user_id,
                                        'config_id' => $config_id,
                                        'store_name' => $store_name,
                                        'ways_source' => $ways_source,
                                        'pay_time' => $pay_time,
                                        'device_id' => isset($device_id) ? $device_id : ""
                                    ];

                                    if (Cache::has('console_out_trade_no_' . $out_trade_no)) {
                                        Log::info('--newland--检查是否操作---Cache.  ---console_out_trade_no_:' . $out_trade_no);
                                        return;
                                    }

                                    Cache::add('console_out_trade_no_' . $out_trade_no, '1', 1);
                                    PaySuccessAction::action($paySucData);
                                }
                            }
                        } else {
                            continue;
                        }
                    }

                    //易生支付
                    if (20999 < $ways_type && $ways_type < 21999) {
                        //读取配置
                        $easyPayStoresImages = EasypayStoresImages::where('store_id', $store_id)->select('new_config_id')->first();
                        if (!$easyPayStoresImages) {
                            $easyPayStoresImages = EasypayStoresImages::where('store_id', $store_pid)->select('new_config_id')->first();
                        }
                        $config = new EasyPayConfigController();
                        $easypay_config = $config->easypay_config($easyPayStoresImages->new_config_id);
                        if (!$easypay_config) {
                            continue;
                        }

                        $easypay_merchant = $config->easypay_merchant($store_id, $store_pid);
                        if (!$easypay_merchant) {
                            continue;
                        }

                        $obj = new \App\Api\Controllers\EasyPay\PayController();
                        $easypay_data = [
                            'channel_id' => $easypay_config->channel_id, //渠道编号
                            'mer_id' => $easypay_merchant->term_mercode, //终端商戶编号
                            'device_id' => $easypay_merchant->term_termcode, //终端编号
                            'out_trade_no' => $out_trade_no, //原交易流水
                            'key' => $easypay_config->channel_key
                        ];
//                        Log::info('易生支付-轮询-交易查询');
//                        Log::info($easypay_data);
                        $return = $obj->order_query($easypay_data); //-1 系统错误 0-其他 1-成功 2-下单失败
//                        Log::info($return);

                        //支付成功
                        if ($return['status'] == '1') {
                            //1.0
//                            $pay_time = $return['data']['wxtimeend'] ? date('Y-m-d H:i:s', strtotime($return['data']['wxtimeend'])) : ''; //支付完成时间,如2009年12月27日9点10分10秒表示为20091227091010
//                            $trade_no = $return['data']['wtorderid']; //系统订单号
//                            $buyer_pay_amount = $return['data']['payamt'] ? ($return['data']['payamt'] / 100) : ''; //实付金额,单位分
//                            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                            //2.0
                            $pay_time = (isset($return['data']['timeEnd']) && !empty($return['data']['timeEnd'])) ? date('Y-m-d H:i:m', strtotime($return['data']['timeEnd'])) : date('Y-m-d H:i:m', time()); //支付完成时间，格式为 yyyyMMddhhmmss，如 2009年12月27日9点10分10秒表示为 20091227091010
                            $trade_no = $return['data']['outTrace']; //系统订单号
                            $buyer_logon_id = $return['data']['payerId'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
                            $buyer_pay_amount = $return['data']['payerAmt'] ? ($return['data']['payerAmt'] / 100) : ''; //实付金额，单位分
                            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                            //改变数据库状态
                            $stuOrder = StuOrder::where('out_trade_no', $out_trade_no)->select('*')->first();//校园订单表
                            if ($stuOrder) {
                                if ($stuOrder->pay_status != 1) {
                                    $update = [
                                        'pay_status' => 1,
                                        'pay_status_desc' => '缴费成功',
                                        'trade_no' => $trade_no,
                                        'pay_time' => $pay_time,
                                        'gmt_end' => $pay_time,
                                    ];

                                    StuOrder::where('out_trade_no', $out_trade_no)->update($update);
                                }
                            }
                            if ($pay_status != 1) {
                                $insert_data = [
                                    'status' => '1',
                                    'pay_status' => 1,
                                    'pay_status_desc' => '支付成功',
                                    'buyer_logon_id' => '',
                                    'trade_no' => $trade_no,
                                    'pay_time' => $pay_time,
                                    'buyer_pay_amount' => $buyer_pay_amount,
                                    'receipt_amount' => $buyer_pay_amount,
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);

                                //支付成功后的动作
                                $paySucData = [
                                    'ways_type' => $ways_type,
                                    'company' => $company,
                                    'ways_type_desc' => $ways_type_desc,
                                    'source_type' => '21000', //返佣来源
                                    'source_desc' => '易生支付', //返佣来源说明
                                    'total_amount' => $total_amount,
                                    'out_trade_no' => $out_trade_no,
                                    'rate' => $rate,
                                    'fee_amount' => $fee_amount,
                                    'merchant_id' => $merchant_id,
                                    'store_id' => $store_id,
                                    'user_id' => $user_id,
                                    'config_id' => $config_id,
                                    'store_name' => $store_name,
                                    'ways_source' => $ways_source,
                                    'pay_time' => $pay_time,
                                    'device_id' => isset($values->device_id) ? $values->device_id : '',
                                ];

                                if (Cache::has('console_out_trade_no_' . $out_trade_no)) {
                                    Log::info('--VibillOrderSuccess--easypay--检查是否操作---Cache.  ---console_out_trade_no_:' . $out_trade_no);
                                    return;
                                }

                                //Log::info('--VibillOrderSuccess-----Cache.  ---console_out_trade_no_:' . $out_trade_no);

                                Cache::add('console_out_trade_no_' . $out_trade_no, '1', 1);

                                PaySuccessAction::action($paySucData);

                            }
                        } //订单失败关闭
                        elseif ($return['status'] == '3') {
                            //改变数据库状态
                            $stuOrder = StuOrder::where('out_trade_no', $out_trade_no)->select('*')->first();//校园订单表
                            if ($stuOrder) {
                                if ($stuOrder->pay_status != 1) {
                                    $update = [
                                        'pay_status' => 3,
                                        'pay_status_desc' => '支付失败',
                                        'trade_no' => $trade_no,
                                    ];

                                    StuOrder::where('out_trade_no', $out_trade_no)->update($update);
                                }
                            }
                            if ($pay_status != '1') {
                                $insert_data = [
                                    'status' => '3',
                                    'pay_status' => '3',
                                    'pay_status_desc' => '支付失败',
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);
                            } else {
                                continue;
                            }
                        } //其他
                        else {
                            continue;
                        }
                    }

                    //威富通 交易查询
                    if (26999 < $ways_type && $ways_type < 27999) {
                        $config = new WftPayConfigController();
                        $wftpay_config = $config->wftpay_config($config_id);
                        if (!$wftpay_config) {
                            continue;
                        }

                        $wftpay_merchant = $config->wftpay_merchant($store_id, $store_pid);
                        if (!$wftpay_merchant) {
                            continue;
                        }

                        //支付状态为 退款中、已退款、有退款,退款查询接口
                        $obj = new WftPayPayController();
                        $wftpay_data = [
                            'mch_id' => $wftpay_merchant->mch_id,
                            'out_trade_no' => $out_trade_no,
                            'private_rsa_key' => $wftpay_config->private_rsa_key,
                            'public_rsa_key' => $wftpay_config->public_rsa_key
                        ];
                        if ($pay_status == 5 || $pay_status == 6 || $pay_status == 7) {
//                            Log::info('威富通-退款查询-入参');
//                            Log::info($wftpay_data);
                            $return = $obj->refund_query($wftpay_data); //0-系统错误 1-成功 2-退款失败 3-退款处理中 4-退款到银行发现用户的卡作废或者冻结了,导致原路退款银行卡失败
//                            Log::info('威富通-退款查询');
//                            Log::info($return);
                            //支付成功
                            if ($return['status'] == 1) {
                                $refund_count = $return['data']['refund_count'] - 1; //退款记录数
                                $refund_no = $return['data']['out_refund_id_' . $refund_count] ?? $return['data']['out_refund_no_' . $refund_count]; //第三方退款单号or平台退款单号
                                $pay_time = isset($return['data']['refund_time_' . $refund_count]) ? date('Y-m-d H:i:s', strtotime($return['data']['refund_time_' . $refund_count])) : ''; //退款时间,yyyyMMddHHmmss
                                $refund_amount = isset($return['data']['refund_fee_' . $refund_count]) ? ($return['data']['refund_fee_' . $refund_count] / 100) : $total_amount; //退款总金额,单位为分,可以做部分退款
                                $refund_amount = number_format($refund_amount, 2, '.', '');
                                $trade_no = $return['data']['out_transaction_id']; //第三方订单号

                                //改变数据库状态
                                if ($pay_status != 6) {
                                    $insert_data = [
                                        'status' => '6',
                                        'pay_status' => 6,
                                        'pay_status_desc' => '退款成功',
                                        'refund_no' => $refund_no,
                                        'refund_amount' => $refund_amount
                                    ];
                                    if ($pay_time) $insert_data['pay_time'] = $pay_time;
                                    $this->update_day_order($insert_data, $out_trade_no);

                                    //退款成功后的动作
                                    $return_data = [
                                        'out_trade_no' => $out_trade_no,
                                        'trade_no' => $trade_no,
                                        'store_id' => $store_id,
                                        'merchant_id' => $merchant_id,
                                        'type' => $ways_type,
                                        'ways_source' => $ways_source,
                                        'refund_amount' => $refund_amount,
                                        'refund_no' => $refund_no
                                    ];
                                    RefundOrder::created($return_data);
                                }
                            }
                        } else {
//                            Log::info('威富通-交易查询-入参: ');
//                            Log::info($wftpay_data);
                            $return = $obj->order_query($wftpay_data); //0-系统错误 1-成功 2-失败 3-转入退款 4-未支付 5-已关闭 6-已冲正 7-已撤销 8-用户支付中
//                            Log::info('威富通-交易查询-结果: ');
//                            Log::info($return);
                            //支付成功
                            if ($return['status'] == 1) {
                                $pay_time = $return['data']['time_end'] ? date('Y-m-d H:i:s', strtotime($return['data']['time_end'])) : ''; //支付完成时间,如2009年12月27日9点10分10秒表示为20091227091010
                                $trade_no = $return['data']['transaction_id']; //平台交易号
                                $buyer_pay_amount = $return['data']['total_fee'] ? ($return['data']['total_fee'] / 100) : ''; //总金额,以分为单位,不允许包含任何字、符号
                                $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                                $depBankSeq = $return['data']['out_transaction_id'] ?? ''; //第三方交易号

                                //改变数据库状态
                                if ($pay_status != 1) {
                                    $insert_data = [
                                        'status' => '1',
                                        'pay_status' => 1,
                                        'pay_status_desc' => '支付成功',
                                        'buyer_logon_id' => '',
                                        'trade_no' => $trade_no,
                                        'pay_time' => $pay_time,
                                        'buyer_pay_amount' => $buyer_pay_amount,
                                        'other_no' => $depBankSeq
                                    ];
                                    $this->update_day_order($insert_data, $out_trade_no);

                                    //支付成功后的动作
                                    $paySucData = [
                                        'ways_type' => $ways_type,
                                        'company' => $company,
                                        'ways_type_desc' => $ways_type_desc,
                                        'source_type' => '27000', //返佣来源
                                        'source_desc' => 'wftpay', //返佣来源说明
                                        'total_amount' => $total_amount,
                                        'out_trade_no' => $out_trade_no,
                                        'rate' => $rate,
                                        'fee_amount' => $fee_amount,
                                        'merchant_id' => $merchant_id,
                                        'store_id' => $store_id,
                                        'user_id' => $user_id,
                                        'config_id' => $config_id,
                                        'store_name' => $store_name,
                                        'ways_source' => $ways_source,
                                        'pay_time' => $pay_time,
                                        'device_id' => isset($values->device_id) ? $values->device_id : '',
                                    ];

                                    if (Cache::has('console_out_trade_no_' . $out_trade_no)) {
                                        Log::info('--wftpay--检查是否操作---Cache.  ---console_out_trade_no_:' . $out_trade_no);
                                        return;
                                    }

                                    Cache::add('console_out_trade_no_' . $out_trade_no, '1', 1);

                                    PaySuccessAction::action($paySucData);

                                }
                            }
                        }
                    }

                    //汇旺财 交易查询
                    if (27999 < $ways_type && $ways_type < 28999) {
                        $config = new HwcPayConfigController();
                        $hwcpay_config = $config->hwcpay_config($config_id);
                        if (!$hwcpay_config) {
                            continue;
                        }

                        $hwcpay_merchant = $config->hwcpay_merchant($store_id, $store_pid);
                        if (!$hwcpay_merchant) {
                            continue;
                        }

                        //支付状态为 退款中、已退款、有退款,退款查询接口
                        $obj = new HwcPayPayController();
                        $hwcpay_data = [
                            'mch_id' => $hwcpay_merchant->mch_id,
                            'out_trade_no' => $out_trade_no,
                            'private_rsa_key' => $hwcpay_config->private_rsa_key,
                            'public_rsa_key' => $hwcpay_config->public_rsa_key
                        ];
                        if ($pay_status == 5 || $pay_status == 6 || $pay_status == 7) {
//                            Log::info('汇旺财-退款查询-入参');
//                            Log::info($hwcpay_data);
                            $return = $obj->refund_query($hwcpay_data); //0-系统错误 1-成功 2-退款失败 3-退款处理中 4-退款到银行发现用户的卡作废或者冻结了,导致原路退款银行卡失败
//                            Log::info('汇旺财-退款查询-结果');
//                            Log::info($return);
                            //支付成功
                            if ($return['status'] == 1) {
                                $refund_count = $return['data']['refund_count'] - 1; //退款记录数
                                $refund_no = $return['data']['out_refund_id_' . $refund_count] ?? $return['data']['out_refund_no_' . $refund_count]; //第三方退款单号or平台退款单号
                                $pay_time = isset($return['data']['refund_time_' . $refund_count]) ? date('Y-m-d H:i:s', strtotime($return['data']['refund_time_' . $refund_count])) : ''; //退款时间,yyyyMMddHHmmss
                                $refund_amount = $return['data']['refund_fee_' . $refund_count] ? ($return['data']['refund_fee_' . $refund_count] / 100) : ''; //退款总金额,单位为分,可以做部分退款
                                $refund_amount = number_format($refund_amount, 2, '.', '');
                                $trade_no = $return['data']['out_transaction_id'] ?? $return['data']['refund_id_' . $refund_count]; //第三方订单号or平台退款单号

                                //改变数据库状态
                                if ($pay_status != 6) {
                                    $insert_data = [
                                        'status' => '6',
                                        'pay_status' => 6,
                                        'pay_status_desc' => '退款成功',
                                        'refund_no' => $refund_no,
                                        'refund_amount' => $refund_amount
                                    ];
                                    if ($pay_time) $insert_data['pay_time'] = $pay_time;
                                    $this->update_day_order($insert_data, $out_trade_no);

                                    //退款成功后的动作
                                    $return_data = [
                                        'out_trade_no' => $out_trade_no,
                                        'trade_no' => $trade_no,
                                        'store_id' => $store_id,
                                        'merchant_id' => $merchant_id,
                                        'type' => $ways_type,
                                        'ways_source' => $ways_source,
                                        'refund_amount' => $refund_amount,
                                        'refund_no' => $refund_no
                                    ];
                                    RefundOrder::created($return_data);
                                }
                            }
                        } else {
//                            Log::info('汇旺财-交易查询-入参: ');
//                            Log::info($hwcpay_data);
                            $return = $obj->order_query($hwcpay_data); //0-系统错误 1-成功 2-失败 3-转入退款 4-未支付 5-已关闭 6-已冲正 7-已撤销 8-用户支付中
//                            Log::info('汇旺财-交易查询-结果: ');
//                            Log::info($return);
                            //支付成功
                            if ($return['status'] == 1) {
                                $pay_time = $return['data']['time_end'] ? date('Y-m-d H:i:s', strtotime($return['data']['time_end'])) : ''; //支付完成时间,如2009年12月27日9点10分10秒表示为20091227091010
                                $trade_no = isset($return['data']['third_order_no']) ? $return['data']['third_order_no'] : $return['data']['transaction_id']; //平台交易号
                                $buyer_pay_amount = $return['data']['total_fee'] ? ($return['data']['total_fee'] / 100) : ''; //总金额,以分为单位,不允许包含任何字、符号
                                $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                                $depBankSeq = $return['data']['out_transaction_id'] ?? ''; //第三方交易号

                                //改变数据库状态
                                if ($pay_status != 1) {
                                    $insert_data = [
                                        'status' => '1',
                                        'pay_status' => 1,
                                        'pay_status_desc' => '支付成功',
                                        'buyer_logon_id' => '',
                                        'trade_no' => $trade_no,
                                        'pay_time' => $pay_time,
                                        'buyer_pay_amount' => $buyer_pay_amount,
                                        'other_no' => $depBankSeq
                                    ];
                                    $this->update_day_order($insert_data, $out_trade_no);

                                    //支付成功后的动作
                                    $paySucData = [
                                        'ways_type' => $ways_type,
                                        'company' => $company,
                                        'ways_type_desc' => $ways_type_desc,
                                        'source_type' => '28000', //返佣来源
                                        'source_desc' => 'hwcpay', //返佣来源说明
                                        'total_amount' => $total_amount,
                                        'out_trade_no' => $out_trade_no,
                                        'rate' => $rate,
                                        'fee_amount' => $fee_amount,
                                        'merchant_id' => $merchant_id,
                                        'store_id' => $store_id,
                                        'user_id' => $user_id,
                                        'config_id' => $config_id,
                                        'store_name' => $store_name,
                                        'ways_source' => $ways_source,
                                        'pay_time' => $pay_time,
                                        'device_id' => isset($values->device_id) ? $values->device_id : '',
                                    ];

                                    if (Cache::has('console_out_trade_no_' . $out_trade_no)) {
                                        Log::info('--hwcpay--检查是否操作---Cache.  ---console_out_trade_no_:' . $out_trade_no);
                                        return;
                                    }

                                    Cache::add('console_out_trade_no_' . $out_trade_no, '1', 1);

                                    PaySuccessAction::action($paySucData);
                                }
                            }
                        }
                    }

                    //钱方 交易查询
                    if (23999 < $ways_type && $ways_type < 24999) {
                        $config = new QfPayConfigController();
                        $qfpay_config = $config->qfpay_config($config_id);
                        if (!$qfpay_config) {
                            continue;
                        }

                        $qfpay_merchant = $config->qfpay_merchant($store_id, $store_pid);
                        if (!$qfpay_merchant) {
                            continue;
                        }

                        //支付状态为 退款中、已退款、有退款,退款查询接口
                        $obj = new QfPayController();
                        $qfpay_data = [
                            'mchid' => $qfpay_merchant->mchid,  //商户号
                            'key' => $qfpay_config->key,  //加签key
                            'code' => $qfpay_config->code,  //钱方唯一标识
                            'out_trade_no' => $out_trade_no,  //否,外部订单号查询,开发者平台订单号
                            'syssn' => $trade_no  //否,钱方订单号查询,多个以英文逗号区分开
                        ];
                        $return = $obj->query($qfpay_data); //1-成功 2-请求下单成功 3-交易中

                        //支付成功
                        if ($return['status'] == 1) {
                            $syssn = isset($return['data']['syssn']) ? $return['data']['syssn'] : ''; //钱方订单号
                            $other_no = isset($return['data']['out_trade_no']) ? $return['data']['out_trade_no'] : ''; //外部订单号,开发者平台订单号
                            $pay_type = isset($return['data']['pay_type']) ? $return['data']['pay_type'] : ''; //支付类型,多个以英文逗号区分开,支付宝扫码:800101；支付宝反扫:800108；支付宝服务窗：800107；微信扫码:800201；微信刷卡:800208；微信公众号支付:800207
                            $order_type = isset($return['data']['order_type']) ? $return['data']['order_type'] : 'payment'; //订单类型:支付的订单：payment；退款的订单：refund；关闭的订单：close
                            $txdtm = isset($return['data']['txdtm']) ? $return['data']['txdtm'] : ''; //请求交易时间 格式为：YYYY-MM-DD HH:MM:SS
                            $txamt = isset($return['data']['txamt']) ? $return['data']['txamt'] : 0; //订单支付金额,单位分
                            $sysdtm = isset($return['data']['sysdtm']) ? $return['data']['sysdtm'] : $txdtm; //系统交易时间
                            $cancel = isset($return['data']['cancel']) ? $return['data']['cancel'] : ''; //撤销/退款标记 正常交易：0；已撤销：2；已退货：3
                            $respcd = isset($return['data']['respcd']) ? $return['data']['respcd'] : ''; //支付结果返回码 0000表示交易支付成功；1143、1145表示交易中,需继续查询交易结果； 其他返回码表示失败
                            $errmsg = isset($return['data']['errmsg']) ? $return['data']['errmsg'] : ''; //支付结果描述
                            $cardtp = isset($return['data']['cardtp']) ? $return['data']['cardtp'] : ''; //卡类型 未识别卡=0,借记卡=1,信用卡(贷记卡)=2,准贷记卡=3,储值卡= 4,第三方帐号=5

                            $buyer_pay_amount = number_format($txamt / 100, 2, '.', '');

                            if ($respcd == 0000 && $order_type == 'payment') {
                                //改变数据库状态
                                if ($pay_status != 1 && $syssn) {
                                    $insert_data = [
                                        'status' => '1',
                                        'pay_status' => 1,
                                        'pay_status_desc' => '支付成功',
                                        'buyer_logon_id' => '',
                                        'trade_no' => $syssn,
                                        'pay_time' => $sysdtm,
                                        'buyer_pay_amount' => $buyer_pay_amount,
                                        'receipt_amount' => $buyer_pay_amount, //商家实际收款金额
                                        'other_no' => $other_no
                                    ];
                                    $this->update_day_order($insert_data, $out_trade_no);

                                    //支付成功后的动作
                                    $paySucData = [
                                        'ways_type' => $ways_type,
                                        'company' => $company,
                                        'ways_type_desc' => $ways_type_desc,
                                        'source_type' => '24000', //返佣来源
                                        'source_desc' => 'qfpay', //返佣来源说明
                                        'total_amount' => $total_amount,
                                        'out_trade_no' => $out_trade_no,
                                        'rate' => $rate,
                                        'fee_amount' => $fee_amount,
                                        'merchant_id' => $merchant_id,
                                        'store_id' => $store_id,
                                        'user_id' => $user_id,
                                        'config_id' => $config_id,
                                        'store_name' => $store_name,
                                        'ways_source' => $ways_source,
                                        'pay_time' => $pay_time,
                                        'device_id' => isset($values->device_id) ? $values->device_id : '',
                                    ];

                                    if (Cache::has('console_out_trade_no_' . $out_trade_no)) {
                                        Log::info('--qfpay--检查是否操作---Cache.  ---console_out_trade_no_:' . $out_trade_no);
                                        return;
                                    }

                                    Cache::add('console_out_trade_no_' . $out_trade_no, '1', 1);

                                    PaySuccessAction::action($paySucData);
                                }
                            }
                        }

                        continue;
                    }

                    //邮驿付 交易查询
                    if (29000 < $ways_type && $ways_type < 29010) {
                        //读取配置
                        $config = new PostPayConfigController();
                        $post_config = $config->post_pay_config($config_id);
                        if (!$post_config) {
                            continue;
                        }

                        $post_merchant = $config->post_pay_merchant($store_id, $store_pid);
                        if (!$post_merchant) {
                            continue;
                        }

                        $obj = new \App\Api\Controllers\PostPay\PayController();
                        $post_data = [
                            'out_trade_no' => $out_trade_no,//原交易流水 //渠道编号
                            'custLogin' => $store_obj->people_phone, //商户手机号
                            'custId' => $post_merchant->cust_id, //终端编号
                            'agetId' => $post_config->org_id
                        ];
                        $return = $obj->order_query($post_data);
//                        Log::info($return);

                        //支付成功
                        if ($return['status'] == '1') {
                            $pay_time = $return['data']['orderTime'] ? date('Y-m-d H:i:s', strtotime($return['data']['orderTime'])) : ''; //支付完成时间，如2009年12月27日9点10分10秒表示为20091227091010
                            $trade_no = $return['data']['orderNo']; //系统订单号
                            $buyer_logon_id = $return['data']['openId'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
                            $buyer_pay_amount = $return['data']['txamt'] ? ($return['data']['txamt'] / 100) : ''; //实付金额，单位分
                            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                            //改变数据库状态
                            if ($pay_status != 1) {
                                $insert_data = [
                                    'status' => '1',
                                    'pay_status' => 1,
                                    'pay_status_desc' => '支付成功',
                                    'buyer_logon_id' => '',
                                    'trade_no' => $trade_no,
                                    'pay_time' => $pay_time,
                                    'buyer_pay_amount' => $buyer_pay_amount,
                                    'receipt_amount' => $buyer_pay_amount,
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);

                                //支付成功后的动作
                                $paySucData = [
                                    'ways_type' => $ways_type,
                                    'company' => $company,
                                    'ways_type_desc' => $ways_type_desc,
                                    'source_type' => '21000', //返佣来源
                                    'source_desc' => '邮驿付支付', //返佣来源说明
                                    'total_amount' => $total_amount,
                                    'out_trade_no' => $out_trade_no,
                                    'rate' => $rate,
                                    'fee_amount' => $fee_amount,
                                    'merchant_id' => $merchant_id,
                                    'store_id' => $store_id,
                                    'user_id' => $user_id,
                                    'config_id' => $config_id,
                                    'store_name' => $store_name,
                                    'ways_source' => $ways_source,
                                    'pay_time' => $pay_time,
                                    'device_id' => isset($values->device_id) ? $values->device_id : '',
                                ];
                                if (Cache::has('console_out_trade_no_' . $out_trade_no)) {
                                    Log::info('--postpay--检查是否操作---Cache.  ---console_out_trade_no_:' . $out_trade_no);
                                    return;
                                }

                                Cache::add('console_out_trade_no_' . $out_trade_no, '1', 1);

                                PaySuccessAction::action($paySucData);
                            }
                        } //订单失败关闭
                        elseif ($return['status'] == '3') {
                            //改变数据库状态
                            if ($pay_status != '1') {
                                $insert_data = [
                                    'status' => '3',
                                    'pay_status' => '3',
                                    'pay_status_desc' => '支付失败',
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);
                            } else {
                                continue;
                            }
                        } //其他
                        else {
                            continue;
                        }
                    }

                    // 易生数科 交易查询
                    if (32000 < $ways_type && $ways_type < 32010) {
                        //读取配置
                        $config = new EasySkPayConfigController();
                        $easyskpay_config = $config->easyskpay_config($config_id);
                        if (!$easyskpay_config) {
                            continue;
                        }

                        $easyskpay_merchant = $config->easyskpay_merchant($store_id, $store_pid);
                        if (!$easyskpay_merchant) {
                            continue;
                        }
                        $obj = new \App\Api\Controllers\EasySkPay\PayController();
                        $easyskpay_data = [
                            'org_id' => $easyskpay_config->org_id,
                            'mer_id' => $easyskpay_merchant->mer_id,
                            'orig_request_no' => $out_trade_no
                        ];

                        $return = $obj->order_query($easyskpay_data);
//                        Log::info($return);

                        //支付成功
                        if ($return['status'] == '1') {

                            $trade_no = $return['data']['tradeNo']; //系统订单号
                            $pay_time = (isset($return['data']['bizData']['payTime']) && !empty($return['data']['bizData']['payTime'])) ? date('Y-m-d H:i:m', strtotime($return['data']['bizData']['payTime'])) : date('Y-m-d H:i:m', time());
                            $buyer_pay_amount = isset($return['data']['bizData']['amount']) ? ($return['data']['bizData']['amount'] / 100) : ''; //实付金额，单位分
                            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                            //改变数据库状态
                            $stuOrder = StuOrder::where('out_trade_no', $out_trade_no)->select('*')->first();//校园订单表
                            if ($stuOrder) {
                                if ($stuOrder->pay_status != 1) {
                                    $update = [
                                        'pay_status' => 1,
                                        'pay_status_desc' => '缴费成功',
                                        'trade_no' => $trade_no,
                                        'pay_time' => $pay_time,
                                        'gmt_end' => $pay_time,
                                    ];

                                    StuOrder::where('out_trade_no', $out_trade_no)->update($update);
                                }
                            }

                            if ($pay_status != 1) {
                                $insert_data = [
                                    'status' => '1',
                                    'pay_status' => 1,
                                    'pay_status_desc' => '支付成功',
                                    'buyer_logon_id' => '',
                                    'trade_no' => $trade_no,
                                    'pay_time' => $pay_time,
                                    'buyer_pay_amount' => $buyer_pay_amount,
                                    'receipt_amount' => $buyer_pay_amount,
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);
                                $update = [
                                    'pay_status' => 1,
                                    'pay_status_desc' => '缴费成功',
                                    'trade_no' => $trade_no,
                                    'pay_time' => $pay_time,
                                    'gmt_end' => $pay_time,
                                ];

                                StuOrder::where('out_trade_no', $out_trade_no)->update($update);
                                //支付成功后的动作
                                $paySucData = [
                                    'ways_type' => $ways_type,
                                    'company' => $company,
                                    'ways_type_desc' => $ways_type_desc,
                                    'source_type' => '32000', //返佣来源
                                    'source_desc' => '易生数科支付', //返佣来源说明
                                    'total_amount' => $total_amount,
                                    'out_trade_no' => $out_trade_no,
                                    'rate' => $rate,
                                    'fee_amount' => $fee_amount,
                                    'merchant_id' => $merchant_id,
                                    'store_id' => $store_id,
                                    'user_id' => $user_id,
                                    'config_id' => $config_id,
                                    'store_name' => $store_name,
                                    'ways_source' => $ways_source,
                                    'pay_time' => $pay_time,
                                    'device_id' => isset($values->device_id) ? $values->device_id : '',
                                ];

                                if (Cache::has('console_out_trade_no_' . $out_trade_no)) {
                                    Log::info('--easyskpay--检查是否操作---Cache.  ---console_out_trade_no_:' . $out_trade_no);
                                    return;
                                }

                                Cache::add('console_out_trade_no_' . $out_trade_no, '1', 1);

                                PaySuccessAction::action($paySucData);
                            }
                        } //订单失败关闭
                        elseif ($return['status'] == '3') {
                            $stuOrder = StuOrder::where('out_trade_no', $out_trade_no)->select('*')->first();//校园订单表
                            if ($stuOrder) {
                                if ($stuOrder->pay_status != 1) {
                                    $update = [
                                        'pay_status' => 3,
                                        'pay_status_desc' => '支付失败',
                                        'trade_no' => $trade_no,
                                    ];

                                    StuOrder::where('out_trade_no', $out_trade_no)->update($update);
                                }
                            }
                            //改变数据库状态
                            if ($pay_status != '1') {
                                $insert_data = [
                                    'status' => '3',
                                    'pay_status' => '3',
                                    'pay_status_desc' => '支付失败',
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);

                            } else {
                                continue;
                            }
                        } //其他
                        else {
                            continue;
                        }
                    }

                    //通联支付 交易查询
                    if (33000 < $ways_type && $ways_type < 33010) {
                        $config = new AllinPayConfigController();
                        $allin_config = $config->allin_pay_config($config_id);
                        if (!$allin_config) {
                            return json_encode([
                                'status' => '2',
                                'message' => '通联支付配置不存在请检查配置'
                            ]);
                        }

                        $allin_merchant = $config->allin_pay_merchant($store_id, $store_pid);
                        if (!$allin_merchant) {
                            return json_encode([
                                'status' => '2',
                                'message' => '通联支付商户号不存在'
                            ]);
                        }

                        $obj = new \App\Api\Controllers\AllinPay\PayController();
                        $allin_data = [
                            'ageId' => $allin_config->org_id, //渠道编号
                            'cusId' => $allin_merchant->cus_id, //终端商戶编号
                            'out_trade_no' => $out_trade_no, //原交易流水
                            'trxid' => $trade_no,
                            'appid' => $allin_merchant->appid
                        ];

                        $return = $obj->order_query($allin_data); //-1 系统错误 0-其他 1-成功 2-下单失败
                        //支付成功
                        if ($return['status'] == '1') {
                            $pay_time = date('Y-m-d H:i:m', time());
                            $trade_no = $return['data']['trxid']; //系统订单号
                            $buyer_id = $return['data']['acct'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
                            $buyer_pay_amount = isset($return['data']['trxamt']) ? ($return['data']['amount'] / 100) : ''; //实付金额，单位分
                            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                            if ($pay_status != 1) {
                                $insert_data = [
                                    'status' => '1',
                                    'pay_status' => 1,
                                    'pay_status_desc' => '支付成功',
                                    'buyer_logon_id' => '',
                                    'trade_no' => $trade_no,
                                    'pay_time' => $pay_time,
                                    'buyer_pay_amount' => $buyer_pay_amount,
                                    'receipt_amount' => $buyer_pay_amount,
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);

                                //支付成功后的动作
                                $paySucData = [
                                    'ways_type' => $ways_type,
                                    'company' => $company,
                                    'ways_type_desc' => $ways_type_desc,
                                    'source_type' => '33000', //返佣来源
                                    'source_desc' => '通联支付', //返佣来源说明
                                    'total_amount' => $total_amount,
                                    'out_trade_no' => $out_trade_no,
                                    'rate' => $rate,
                                    'fee_amount' => $fee_amount,
                                    'merchant_id' => $merchant_id,
                                    'store_id' => $store_id,
                                    'user_id' => $user_id,
                                    'config_id' => $config_id,
                                    'store_name' => $store_name,
                                    'ways_source' => $ways_source,
                                    'pay_time' => $pay_time,
                                    'device_id' => isset($values->device_id) ? $values->device_id : '',
                                ];

                                if (Cache::has('console_out_trade_no_' . $out_trade_no)) {
                                    Log::info('--VibillOrderSuccess--allinpay--检查是否操作---Cache.  ---console_out_trade_no_:' . $out_trade_no);
                                    return;
                                }
                                Cache::add('console_out_trade_no_' . $out_trade_no, '1', 1);

                                PaySuccessAction::action($paySucData);

                            }
                        } elseif ($return['status'] == '3') {
                            //改变数据库状态
                            if ($pay_status != '1') {
                                $insert_data = [
                                    'status' => '3',
                                    'pay_status' => '3',
                                    'pay_status_desc' => '支付失败',
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);
                            } else {
                                continue;
                            }
                        } //其他
                        else {
                            continue;
                        }

                    }

                    //银盛 交易查询
                    if (14000 < $ways_type && $ways_type < 14010) {
                        //读取配置
                        $config = new YinshengConfigController();
                        $yinsheng_config = $config->yinsheng_config($config_id);
                        if (!$yinsheng_config) {
                            continue;
                        }
                        $yinsheng_merchant = $config->yinsheng_merchant($store_id, $store_pid);
                        if (!$yinsheng_merchant) {
                            continue;
                        }
                        if (!$trade_no) {
                            continue;
                        }
                        $obj = new \App\Api\Controllers\YinSheng\PayController();
                        $ys_data = [
                            'out_trade_no' => $out_trade_no,
                            'partner_id' => $yinsheng_config->partner_id,
                            'trade_no' => $trade_no,
                            'shop_date' => $shop_date,
                            'seller_id' => $yinsheng_merchant->mer_code
                        ];
                        $return = $obj->order_query($ys_data);

                        //支付成功
                        if ($return['status'] == '1') {
                            $pay_time = date('Y-m-d H:i:s', time()); //支付完成时间，如2009年12月27日9点10分10秒表示为20091227091010
                            $trade_no = $return['data']['trade_no']; //系统订单号
                            $buyer_logon_id = $return['data']['openid'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
                            $buyer_pay_amount = $return['data']['total_amount'] ? ($return['data']['total_amount'] / 100) : ''; //实付金额，单位分
                            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                            //改变数据库状态
                            if ($pay_status != 1) {
                                $insert_data = [
                                    'status' => '1',
                                    'pay_status' => 1,
                                    'pay_status_desc' => '支付成功',
                                    'buyer_logon_id' => '',
                                    'trade_no' => $trade_no,
                                    'pay_time' => $pay_time,
                                    'buyer_pay_amount' => $buyer_pay_amount,
                                    'receipt_amount' => $buyer_pay_amount,
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);

                                //支付成功后的动作
                                $paySucData = [
                                    'ways_type' => $ways_type,
                                    'company' => $company,
                                    'ways_type_desc' => $ways_type_desc,
                                    'source_type' => '14000', //返佣来源
                                    'source_desc' => '银盛支付', //返佣来源说明
                                    'total_amount' => $total_amount,
                                    'out_trade_no' => $out_trade_no,
                                    'rate' => $rate,
                                    'fee_amount' => $fee_amount,
                                    'merchant_id' => $merchant_id,
                                    'store_id' => $store_id,
                                    'user_id' => $user_id,
                                    'config_id' => $config_id,
                                    'store_name' => $store_name,
                                    'ways_source' => $ways_source,
                                    'pay_time' => $pay_time,
                                    'device_id' => isset($values->device_id) ? $values->device_id : '',
                                ];
                                if (Cache::has('console_out_trade_no_' . $out_trade_no)) {
                                    Log::info('--ysepay--检查是否操作---Cache.  ---console_out_trade_no_:' . $out_trade_no);
                                    return;
                                }

                                Cache::add('console_out_trade_no_' . $out_trade_no, '1', 1);

                                PaySuccessAction::action($paySucData);
                            }
                        } //订单失败关闭
                        elseif ($return['status'] == '3') {
                            //改变数据库状态
                            if ($pay_status != '1') {
                                $insert_data = [
                                    'status' => '3',
                                    'pay_status' => '3',
                                    'pay_status_desc' => '支付失败',
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);
                            } else {
                                continue;
                            }
                        } //其他
                        else {
                            continue;
                        }
                    }

                    //拉卡拉 交易查询
                    if (34000 < $ways_type && $ways_type < 34010) {
                        //读取配置
                        $config = new LklConfigController();
                        $lkl_config = $config->lkl_config($config_id);
                        if (!$lkl_config) {
                            continue;
                        }
                        $lkl_merchant = $config->lkl_merchant($store_id, $store_pid);
                        if (!$lkl_merchant) {
                            continue;
                        }
                        $obj = new \App\Api\Controllers\LklPay\PayController();
                        $lkl_data = [
                            'out_trade_no' => $out_trade_no,
                            'merchant_no' => $lkl_merchant->customer_no, //商户号
                            'term_no' => $lkl_merchant->term_no,
                        ];
                        $return = $obj->order_query($lkl_data);

                        //支付成功
                        if ($return['status'] == '1') {
                            $pay_time = date('Y-m-d H:i:s', time());
                            $trade_no = $return['data']['trade_no']; //系统订单号
                            $buyer_pay_amount = $return['data']['total_amount'] ? ($return['data']['total_amount']) : ''; //实付金额，单位分
                            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
                            $other_no = $return['data']['acc_trade_no'];
                            //改变数据库状态
                            if ($pay_status != 1) {
                                $insert_data = [
                                    'status' => '1',
                                    'pay_status' => 1,
                                    'pay_status_desc' => '支付成功',
                                    'buyer_logon_id' => '',
                                    'trade_no' => $trade_no,
                                    'other_no' => $other_no,
                                    'pay_time' => $pay_time,
                                    'buyer_pay_amount' => $buyer_pay_amount,
                                    'receipt_amount' => $buyer_pay_amount,
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);

                                //支付成功后的动作
                                $paySucData = [
                                    'ways_type' => $ways_type,
                                    'company' => $company,
                                    'ways_type_desc' => $ways_type_desc,
                                    'source_type' => '34000', //返佣来源
                                    'source_desc' => '拉卡拉支付', //返佣来源说明
                                    'total_amount' => $total_amount,
                                    'out_trade_no' => $out_trade_no,
                                    'rate' => $rate,
                                    'fee_amount' => $fee_amount,
                                    'merchant_id' => $merchant_id,
                                    'store_id' => $store_id,
                                    'user_id' => $user_id,
                                    'config_id' => $config_id,
                                    'store_name' => $store_name,
                                    'ways_source' => $ways_source,
                                    'pay_time' => $pay_time,
                                    'device_id' => isset($values->device_id) ? $values->device_id : '',
                                ];
                                if (Cache::has('console_out_trade_no_' . $out_trade_no)) {
                                    Log::info('--lkl--检查是否操作---Cache.  ---console_out_trade_no_:' . $out_trade_no);
                                    return;
                                }

                                Cache::add('console_out_trade_no_' . $out_trade_no, '1', 1);

                                PaySuccessAction::action($paySucData);
                            }
                        } //订单失败关闭
                        elseif ($return['status'] == '3') {
                            //改变数据库状态
                            if ($pay_status != '1') {
                                $insert_data = [
                                    'status' => '3',
                                    'pay_status' => '3',
                                    'pay_status_desc' => '支付失败',
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);
                            } else {
                                continue;
                            }
                        } //其他
                        else {
                            continue;
                        }
                    }

                }
            }
        } catch (\Exception $ex) {
            Log::info('等待支付状态轮询查询-错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

    }

    //建行通道 5s处理一次
    public function handle_query_ccbank()
    {

        $time_end = date('Y-m-d H:i:s', time());
        $time_start = date('Y-m-d H:i:s', strtotime($time_end) - 300); //5分钟之前的时间

        try {
            //等待支付订单信息
            $order_obj = DB::table('orders')
                ->select(['out_trade_no', 'trade_no', 'qwx_no', 'other_no', 'store_id', 'ways_type', 'company', 'ways_type_desc', 'total_amount', 'rate', 'fee_amount', 'merchant_id', 'user_id', 'store_name', 'ways_source', 'device_id', 'pay_status'])
                ->where('company', 'ccbankpay')
                ->whereIn('pay_status', [2, 3])
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->get();

            if ($order_obj) {
                foreach ($order_obj as $key => $values) {
                    $store_obj = Store::where('store_id', $values->store_id)
                        ->select('config_id', 'merchant_id', 'pid', 'people_phone')
                        ->first();
                    if (!$store_obj) {
                        continue;
                    }

                    $config_id = $store_obj->config_id;
                    $store_pid = $store_obj->pid;
                    $store_id = $values->store_id;
                    $ways_type = $values->ways_type;
                    $out_trade_no = $values->out_trade_no;
                    $trade_no = $values->trade_no;
                    $pay_status = $values->pay_status;
                    $merchant_id = $values->merchant_id;
                    $ways_source = $values->ways_source;
                    $total_amount = $values->total_amount;
                    $company = $values->company;
                    $ways_type_desc = $values->ways_type_desc;
                    $rate = $values->rate;
                    $fee_amount = $values->fee_amount;
                    $user_id = $values->user_id;
                    $store_name = $values->store_name;

                    //建设银行 交易查询
                    if (31000 < $ways_type && $ways_type < 31010) {
                        // sleep(5);//延迟5秒
                        //设置缓存记录订单查询次数
                        $qrytime = Cache::get($out_trade_no);
                        if (!$qrytime) {
                            Cache::add($out_trade_no, 1, 10);
                            $qrytime = 1;
                        }

                        //读取配置
                        $config = new CcBankPayConfigController();
                        $ccBank_merchant = $config->ccBank_pay_merchant($store_id, $store_pid);
                        if (!$ccBank_merchant) {
                            continue;
                        }
                        if ($ways_type == 31001) {
                            $qrCodeType = '3';
                        }
                        if ($ways_type == 31002) {
                            $qrCodeType = '2';
                        }
                        if ($ways_type == 31003) {
                            $qrCodeType = '1';
                        }
                        $obj = new \App\Api\Controllers\CcBankPay\PayController();
                        $ccBank_data = [
                            'cust_id' => $ccBank_merchant->cust_id,
                            'pos_id' => $ccBank_merchant->pos_id,
                            'branch_id' => $ccBank_merchant->branch_id,
                            'out_trade_no' => $out_trade_no,
                            'qr_code_type' => $qrCodeType,
                            'public_key' => $ccBank_merchant->public_key,
                            'termno1' => $ccBank_merchant->termno1,
                            'termno2' => $ccBank_merchant->termno2,
                            'qrytime' => $qrytime
                        ];
                        //  Log::info(json_encode($post_data));
                        $return = $obj->order_query($ccBank_data);
//                        Log::info($return);

                        //支付成功
                        if ($return['status'] == '1') {
                            $pay_time = date('Y-m-d H:i:m', time());
                            $buyer_pay_amount = $return['data']['AMOUNT'] ? ($return['data']['AMOUNT']) : ''; //实付金额


                            //改变数据库状态
                            if ($pay_status != 1) {
                                $insert_data = [
                                    'status' => '1',
                                    'pay_status' => 1,
                                    'pay_status_desc' => '支付成功',
                                    'buyer_logon_id' => '',
                                    'pay_time' => $pay_time,
                                    'receipt_amount' => $buyer_pay_amount,
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);

                                //支付成功后的动作
                                $paySucData = [
                                    'ways_type' => $ways_type,
                                    'company' => $company,
                                    'ways_type_desc' => $ways_type_desc,
                                    'source_type' => '31000', //返佣来源
                                    'source_desc' => '建设银行', //返佣来源说明
                                    'total_amount' => $total_amount,
                                    'out_trade_no' => $out_trade_no,
                                    'rate' => $rate,
                                    'fee_amount' => $fee_amount,
                                    'merchant_id' => $merchant_id,
                                    'store_id' => $store_id,
                                    'user_id' => $user_id,
                                    'config_id' => $config_id,
                                    'store_name' => $store_name,
                                    'ways_source' => $ways_source,
                                    'pay_time' => $pay_time,
                                    'device_id' => isset($values->device_id) ? $values->device_id : '',
                                    'no_user_money' => '1',//不计算返佣
                                ];
                                PaySuccessAction::action($paySucData);


                                $store = Store::where('store_id', $store_id)->select('zero_rate_type')->first();
                                if ($store->zero_rate_type == '0') {//0收费  1不收费
                                    $TransactionDeductionController = new TransactionDeductionController();
                                    $deduction = [
                                        'merchant_id' => $merchant_id,
                                        'total_amount' => $total_amount,
                                        'user_id' => $user_id,
                                        'order_id' => $out_trade_no,
                                        'company' => $company,
                                        'rate' => $rate //结算费率
                                    ];
                                    $TransactionDeductionController->deduction($deduction);//交易扣款
                                }

                                Cache::forget($out_trade_no);//交易成功删除缓存
                            }
                        } //订单失败关闭
                        elseif ($return['status'] == '3') {
                            Cache::forget($out_trade_no);//交易失败删除缓存
                            //改变数据库状态
                            if ($pay_status != '1') {
                                $insert_data = [
                                    'status' => '3',
                                    'pay_status' => '3',
                                    'pay_status_desc' => '支付失败',
                                ];
                                $this->update_day_order($insert_data, $out_trade_no);
                            } else {
                                continue;
                            }
                        } elseif ($return['status'] == '2') {
                            $value = Cache::pull($out_trade_no);//先取出查询次数在删除
                            $num = $value + 1;
                            Cache::add($out_trade_no, $num, 10);//重新存入此订单查询次数
                        } else {
                            continue;
                        }
                    }


                }
            }
        } catch (\Exception $ex) {
            Log::info('等待支付状态轮询查询-错误');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

    }
}
