<?php

namespace App\Console\Commands;

use App\Api\Controllers\BaseController;
use App\Models\AgentLevels;
use App\Models\SettlementMonthInfo;
use App\Models\SettlementMonthList;
use App\Models\User;
use App\Models\UserArchitectureRewards;
use App\Models\UserMonthLevel;
use App\Models\UserWalletDetail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ArchitectureRewardTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'architectureRewardTask';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '代理架构奖';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            Log::info('架构奖---------');
            //获取上一个月时间
            $time_start = date("Y-m-01 00:00:00", strtotime("-1 months")); //获取上个月月初的时间
            //获取上一个月最后一天
            $time_end = date("Y-m-d 23:59:59", strtotime(-date('d') . 'day'));
            //$users = User::where('is_delete', '0')->where('id', 32)->select('id', 'money', 'name', 'config_id')->get();
            $users = User::where('is_delete', '0')->select('id', 'money', 'name', 'config_id', 'reward_money')->get();
            foreach ($users as $user) {
                $baseController = new BaseController();
                $ids = $baseController->getSubIds($user->id);
                unset($ids[0]);
                $userMonthLevels = UserMonthLevel::where('transaction_amount', '>=', 10000000)
                    ->where('start_time', $time_start)
                    ->where('end_time', $time_end)
                    ->whereIn('user_id', $ids)
                    ->get();
                if (count($userMonthLevels) >= 3) {
                    $sub_user_id = '';
                    $sub_user_name = '';
                    foreach ($userMonthLevels as $userMonthLevel) {
                        $sub_user_id = $sub_user_id . $userMonthLevel->user_id . ',';
                        $sub_user = User::where('id', $user->id)->select('name')->first();
                        $sub_user_name = $sub_user_name . $sub_user->name . ',';
                    }
                    $UserArchitectureRewards = UserArchitectureRewards::where('user_id', $user->id)->where('start_time', $time_start)
                        ->where('end_time', $time_end)->select('id')->first();
                    if (!$UserArchitectureRewards) {
                        $amount = 1000 * count($userMonthLevels);
                        $in_data = [
                            'user_id' => $user->id,
                            'reward_amount' => $amount,
                            'sub_user_id' => $sub_user_id,
                            'start_time' => $time_start,
                            'end_time' => $time_end,
                            'sub_user_name' => $sub_user_name,
                            'user_name' => $user->name
                        ];
                        UserArchitectureRewards::create($in_data);
                        $reward_money = $user->reward_money + $amount;
                        User::where('id', $user->id)->update(['reward_money' => $reward_money]);
                    }
                    continue;
                }
                $userMonthLevels = UserMonthLevel::where('transaction_amount', '>=', 2000000)
                    ->where('start_time', $time_start)
                    ->where('end_time', $time_end)
                    ->whereIn('user_id', $ids)
                    ->get();
                if (count($userMonthLevels) >= 3) {
                    $sub_user_id = '';
                    $sub_user_name = '';
                    foreach ($userMonthLevels as $userMonthLevel) {
                        $sub_user_id = $sub_user_id . $userMonthLevel->user_id . ',';
                        $sub_user = User::where('id', $userMonthLevel->id)->select('name')->first();
                        $sub_user_name = $sub_user_name . $sub_user->name . ',';
                    }
                    $UserArchitectureRewards = UserArchitectureRewards::where('user_id', $user->id)->where('start_time', $time_start)
                        ->where('end_time', $time_end)->select('id')->first();
                    if (!$UserArchitectureRewards) {
                        $amount = 200 * count($userMonthLevels);
                        $in_data = [
                            'user_id' => $user->id,
                            'reward_amount' => $amount,
                            'sub_user_id' => $sub_user_id,
                            'start_time' => $time_start,
                            'end_time' => $time_end,
                            'sub_user_name' => $sub_user_name,
                            'user_name' => $user->name
                        ];
                        UserArchitectureRewards::create($in_data);
                        $reward_money = $user->reward_money + $amount;
                        User::where('id', $user->id)->update(['reward_money' => $reward_money]);
                    }
                }
            }


        } catch (\Exception $exception) {
            Log::info('架构奖定时任务error');
            Log::info($exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine());
        }
    }

}
