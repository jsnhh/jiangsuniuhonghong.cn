<?php

namespace App\Console\Commands;

use Aliyun\AliSms;
use App\Api\Controllers\Config\HkrtConfigController;
use App\Models\HkrtStore;
use App\Models\SmsConfig;
use App\Models\StorePayWay;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class HkrtStoreAudit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hkrt-audit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'hkrt-audit';

    /**
     * HkrtStoreAudit constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            if (env('DB_D1_HOST')) {
                $obj = DB::connection("mysql_d1")->table("hkrt_stores");
            } else {
                $obj = DB::table('hkrt_stores');
            }
            $HkrtStoreItem = $obj->where('status', 0)
                ->get()
                ->toArray();

            foreach ($HkrtStoreItem as $k => $value) {
                $config = new HkrtConfigController();
                $hkrt_config = $config->hkrt_config($value->config_id);
                if (!$hkrt_config) {
                    continue;
                }

                $access_id = $hkrt_config->access_id;
                $access_key = $hkrt_config->access_key;
                $agent_no = $hkrt_config->agent_no;
                $agent_apply_no = $value->agent_apply_no;

                if (!$agent_apply_no) {
                    continue;
                }

                $obj = new \App\Api\Controllers\Hkrt\BaseController();
                $data = [
                    'accessid' => $access_id,
                    'agent_no' => $agent_no,
                    'agent_apply_no' => $agent_apply_no,
                    'apply_type' => '1' //1-商户入网申请
                ];
                $data['sign'] = $obj->getSignContent($data, $access_key);
                Log::info('海科融通-商户入网申请-结果');
                Log::info($data);
                $re = $obj->execute($data, $obj->url.$obj->merchant_biz_query);
                Log::info($re);

                if (!isset($re) && empty($re)) {
                   continue;
               }

               $return_data_arr = json_decode($re, true);
                //成功
                if ($return_data_arr['return_code'] == "10000") {
                    //1-提交失败 2-已受理 3-自动审核 4-待审核 5-审核失败 6-审核拒绝 7-审核成功
                    //审核成功
                    if (($return_data_arr['status'] == '7') || ($return_data_arr['status'] == '3')) {
                        $data_up = [
                            'status' => '1',
                            'status_desc' => '审核成功',
                        ];
                        StorePayWay::where('store_id', $value->store_id)
                            ->where('company', 'hkrt')
                            ->update($data_up);

                        $in_data = [
                            'status' => $return_data_arr['status'],
                        ];
                        HkrtStore::where('id', $value->id)
                            ->update($in_data);

                        if ($value->merch_no) {
                            //报备微信授权目录
                            $wx_data = [
                                'agent_no' => $agent_no,
                                'merch_no' => $value->merch_no,
                                'conf_key' => 'auth_path', //授权目录
                                'conf_value' => env('APP_URL') . '/api/hkrt/weixin/',
                                'accessid' => $access_id
                            ];
                            $wx_data['sign'] = $obj->getSignContent($wx_data, $access_key);
                            Log::info('海科融通-报备微信授权目录-结果');
                            Log::info($wx_data);
                            $wx_auth_res = $obj->execute($wx_data, $obj->url.$obj->wx_appid_conf_add);
                            Log::info($wx_auth_res);
                        }
                    }

                    //审核失败
                    if (($return_data_arr['status'] == '1') || ($return_data_arr['status'] == '5') || ($return_data_arr['status'] == '6')) {
                        $data_up = [
                            'status' => '3',
                            'status_desc' => $return_data_arr['msg'],
                        ];
                        StorePayWay::where('store_id', $value->store_id)
                            ->where('company', 'hkrt')
                            ->update($data_up);

                        $in_data = [
                            'status' => $return_data_arr['status'],
                        ];
                        HkrtStore::where('id', $value->id)
                            ->update($in_data);
                    }
                } else {
                    continue;
                }
            }

        } catch (\Exception $ex) {
            Log::info('海科融通-同步门店进件状态-报错');
            Log::info($ex->getMessage());
        }
    }


}
