<?php

namespace App\Console\Commands;

use App\Api\Controllers\BaseController;
use App\Models\AgentLevels;
use App\Models\AgentLevelsInfos;
use App\Models\SettlementMonthInfo;
use App\Models\User;
use App\Models\UserMonthLevel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class IsDeductTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'isDeductTask';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '交易量扣除';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            //获取上一个月时间
            $time_start = date("Y-m-01 00:00:00", strtotime("-1 months")); //获取上个月月初的时间
            //获取上一个月最后一天
            $time_end = date("Y-m-d 23:59:59", strtotime(-date('d') . 'day'));

            $users = User::where('is_delete', '0')->where('id', '!=', 1)->select('id', 'money', 'name', 'config_id', 'pid')->get();
            foreach ($users as $user) {
                $userMonthLevel = UserMonthLevel::where('user_id', $user->id)
                    ->where('start_time', $time_start)
                    ->where('end_time', $time_end)
                    ->select('level_weight')->first();
                $level_weight = 1;
                if ($userMonthLevel) {
                    $level_weight = $userMonthLevel->level_weight;
                }
                $pUserMonthLevel = UserMonthLevel::where('user_id', $user->pid)
                    ->where('start_time', $time_start)
                    ->where('end_time', $time_end)
                    ->select('level_weight')->first();
                $p_level_weight = 1;
                if ($pUserMonthLevel) {
                    $p_level_weight = $pUserMonthLevel->level_weight;
                }
                if ($level_weight >= $p_level_weight) {
                    UserMonthLevel::where('user_id', $user->id)->where('start_time', $time_start)
                        ->where('end_time', $time_end)->update(['is_deduct' => '2']);
                }else{
                    UserMonthLevel::where('user_id', $user->id)->where('start_time', $time_start)
                        ->where('end_time', $time_end)->update(['is_deduct' => '1']);
                }
            }

        } catch (\Exception $exception) {
            Log::info('服务商等级设置定时任务error');
            Log::info($exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine());
        }
    }


}
