<?php

namespace App\Console\Commands;

use App\Api\Controllers\BaseController;
use App\Models\AgentLevels;
use App\Models\AgentLevelsInfos;
use App\Models\SettlementMonthInfo;
use App\Models\User;
use App\Models\UserMonthLevel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserMonthLevelTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'userMonthLevelTask';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '计算代理上月等级';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            Log::info("计算代理上月等级=========");
            //获取上一个月时间
            $time_start = date("Y-m-01 00:00:00", strtotime("-1 months")); //获取上个月月初的时间
            //获取上一个月最后一天
            $time_end = date("Y-m-d 23:59:59", strtotime(-date('d') . 'day'));
            //$users = User::where('is_delete', '0')->where('id', '=', 23)->select('id', 'money', 'name', 'config_id', 'pid')->get();
            $users = User::where('is_delete', '0')->where('id', '!=', 1)->select('id', 'money', 'name', 'config_id', 'pid')->get();
            $baseController = new BaseController();
            foreach ($users as $user) {
                $ids = $baseController->getSubIdsAll($user->id);
                $month_pay_money = 0.00;
                $table = DB::table('orders');
                $wallet_obj = $table->select([
                    'total_amount',
                    DB::raw("SUM(`total_amount`) AS `month_pay_money`"),
                ])
                    ->whereIn('user_id', $ids)
                    ->where('pay_status', 1)//成功
                    ->where('created_at', '>=', $time_start)
                    ->where('created_at', '<=', $time_end)
                    ->first();
                if ($wallet_obj->month_pay_money) {
                    $month_pay_money = $wallet_obj->month_pay_money;
                }
                //Log::info("交易金额====".$user->id."======".$month_pay_money);
                $agentLevel = AgentLevels::where('start_transaction', '<=', $month_pay_money)
                    ->where('end_transaction', '>=', $month_pay_money)
                    ->select('*')
                    ->first();
                //Log::info("交易金额的等级====".$user->id."====".$agentLevel);
                $user_level_info = AgentLevelsInfos::where('user_id', $user->id)
                    ->where('start_time', '<=', $time_start)->select('*')->first();
                //Log::info("购买的等级====".$user_level_info);
                $user_level_weight = 1;
                if ($user_level_info) {
                    if (strtotime($user_level_info->end_time) < strtotime($time_start)) {//降级处理
                        //Log::info("降级处理");
                        AgentLevelsInfos::where('user_id', $user->id)->update(['level_weight' => 1, 'level' => 'V1']);
                    }
                    if ($agentLevel->level_weight <= $user_level_info->level_weight) {//判定等级未购买码牌的等级
                        // Log::info("本月等级1====");
                        $user_level_weight = $user_level_info->level_weight;
                    } else {
                        if ($agentLevel->sid_num) {
                            // Log::info("本月等级2====");
                            $res = $this->lower_level_sub($user->id, $agentLevel->sid, $agentLevel->sid_num, $time_start, $time_end);
                            //Log::info("交易金额条件====".$user->id."res".$res['num']);
                            //满足条件
                            if ($res['status'] == 1) {
                                $user_level_weight = $agentLevel->level_weight;
                            } else {//没有满足条件实际等级
                                $realityAgentLevel = AgentLevels::where('sid_num', '<=', $res['num'])
                                    ->where('level_weight', '<=', $agentLevel->level_weight)
                                    ->select('*')
                                    ->orderBy('level_weight', 'desc')
                                    ->first();
                                $user_level_weight = $realityAgentLevel->level_weight;
                            }
                        }else {
                            $user_level_weight = $agentLevel->level_weight;
                        }
                    }


                } else {
                    $user_level_weight = $agentLevel->level_weight;
                    $user_level_info = AgentLevelsInfos::where('user_id', $user->id)->select('*')->first();
                    $up_user_level_weight = 1;
                    if ($user_level_info && $user_level_info->up_status == '2' && $user_level_weight <= $user_level_info->up_level) {
                        $up_user_level_weight = $user_level_info->up_level;
                        AgentLevelsInfos::where('user_id', $user->id)->update(['up_status' => '1']);
                    }
                    if ($agentLevel->sid_num) {
                        // Log::info("本月等级2====");
                        $res = $this->lower_level_sub($user->id, $agentLevel->sid, $agentLevel->sid_num, $time_start, $time_end);
                        //Log::info("交易金额条件====".$user->id."res".$res['num']);
                        //满足条件
                        if ($res['status'] == 1) {
                            $user_level_weight = $agentLevel->level_weight;
                        } else {//没有满足条件实际等级
                            $realityAgentLevel = AgentLevels::where('sid_num', '<=', $res['num'])
                                ->where('level_weight', '<=', $agentLevel->level_weight)
                                ->select('*')
                                ->orderBy('level_weight', 'desc')
                                ->first();
                            $user_level_weight = $realityAgentLevel->level_weight;
                        }
                    } else {
                        $user_level_weight = $agentLevel->level_weight;
                    }
                    if ($up_user_level_weight > $user_level_weight) {
                        $user_level_weight = $up_user_level_weight;
                    }
                }
                $userMonthLevel = UserMonthLevel::where('user_id', $user->id)
                    ->where('start_time', $time_start)
                    ->where('end_time', $time_end)
                    ->select('id')->first();
                if ($userMonthLevel) {
                    $up = [
                        'transaction_amount' => $month_pay_money,
                        'level_weight' => $user_level_weight,
                        'start_time' => $time_start,
                        'end_time' => $time_end,
                    ];
                    UserMonthLevel::where('user_id', $user->id)->where('start_time', $time_start)
                        ->where('end_time', $time_end)->update($up);
                } else {
                    $data = [
                        'user_id' => $user->id,
                        'transaction_amount' => $month_pay_money,
                        'level_weight' => $user_level_weight,
                        'start_time' => $time_start,
                        'end_time' => $time_end,
                    ];
                    UserMonthLevel::create($data);
                }
            }


        } catch (\Exception $exception) {
            Log::info('服务商等级设置定时任务error');
            Log::info($exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine());
        }
    }

    public function lower_level_sub($user_id, $sid, $sid_num, $time_start, $time_end): array
    {
        $users = User::where('pid', $user_id)->where('is_delete', '0')->select('id')->get();
        $num = 0;
        foreach ($users as $user) {
            $baseController = new BaseController();
            $ids = $baseController->getSubIdsAll($user->id);
            $month_pay_money = 0.00;
            $table = DB::table('orders');
            $wallet_obj = $table->select([
                'total_amount',
                DB::raw("SUM(`total_amount`) AS `month_pay_money`"),
            ])
                ->whereIn('user_id', $ids)
                ->where('pay_status', 1)//成功
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->first();
            if ($wallet_obj->month_pay_money) {
                $month_pay_money = $wallet_obj->month_pay_money;
            }
            $agentLevel = AgentLevels::where('start_transaction', '<=', $month_pay_money)
                ->where('end_transaction', '>=', $month_pay_money)
                ->select('*')
                ->first();
            if ($agentLevel->level_weight >= $sid) {
                $num = $num + 1;
            }
        }
        Log::info("本月等级1====".$num);
        if ($num >= $sid_num) {
            return ['status' => 1, 'num' => $num];
        } else {
            return ['status' => 2, 'num' => $num];

        }
    }

}
