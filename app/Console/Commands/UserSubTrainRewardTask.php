<?php

namespace App\Console\Commands;


use App\Models\AgentLevels;
use App\Models\AgentLevelsInfos;
use App\Models\Order;
use App\Models\SettlementList;
use App\Models\User;
use App\Models\UserSubTrainReward;
use App\Models\UserTrainReward;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class UserSubTrainRewardTask extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user_sub_train_reward_task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'user_sub_train_reward_task';

    /**
     * Settlement constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            //获取上一个月时间
            $time_start = date("Y-m-01 00:00:00", strtotime("-1 months")); //获取上个月月初的时间
            //获取上一个月最后一天
            $time_end = date("Y-m-d 23:59:59", strtotime(-date('d') . 'day'));
            $users = User::where('is_delete', 0)->select('*')->get();
            foreach ($users as $user) {
                $sub_users = User::where('is_delete', 0)->where('pid', $user->id)->select('*')->get();
                if (count($sub_users) >= 3) {//下级满三个才计算奖励
                    foreach ($sub_users as $sub_user) {
                        $this->setSubTrain($sub_user->id, $user->id,$user->name, $time_start, $time_end);
                    }
                    $userSubTrainRewards = UserSubTrainReward::where('p_user_id', $user->id)
                        ->where('time_start', '>=', $time_start)
                        ->where('time_end', '<=', $time_end)
                        ->where('status', '2')
                        ->select('id')->get();
                    if (count($userSubTrainRewards) >= 3) {//查询上月下级代理是否达到标准  大于200万并且数量大于等于3
                        //按照下级最高交易量奖励
                        $userSubTrainReward = UserSubTrainReward::where('p_user_id', $user->id)
                            ->where('time_start', '>=', $time_start)
                            ->where('time_end', '<=', $time_end)
                            ->where('status', '2')
                            ->select('transaction_amount')
                            ->orderBy('transaction_amount', 'desc')
                            ->first();
                        foreach ($userSubTrainRewards as $subTrainReward) {
                            $amount = 200;
                            if ($userSubTrainReward->transaction_amount >= 5000000 && $userSubTrainReward->transaction_amount <= 9999999) {
                                $amount = 500;
                            }
                            if ($userSubTrainReward->transaction_amount >= 10000000) {
                                $amount = 1000;
                            }
                            $up_data = [
                                'amount' => $amount,
                                'status' => '1'
                            ];
                            UserSubTrainReward::where('id', $subTrainReward->id)->update($up_data);
                            $user = User::where('id', $subTrainReward->p_user_id)->select('reward_money')->first();
                            $reward_money = $user->reward_money + $amount;
                            UserSubTrainReward::where('id', $subTrainReward->p_user_id)->update(['reward_money' => $reward_money]);
                        }
                    }
                }
            }


        } catch (\Exception $e) {
            Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
            DB::rollBack();

        }
    }

    public function setSubTrain($user_id, $p_id, $p_user_name, $time_start, $time_end)
    {
        try {
            $orders = DB::table('orders');
            $orders_obj = $orders->select([
                'total_amount',
                DB::raw("SUM(`total_amount`) as `total_amount`"),
            ])->where('user_id', $user_id)
                ->where('created_at', '>=', $time_start)
                ->where('created_at', '<=', $time_end)
                ->where('pay_status', 1)
                ->first();
            if ($orders_obj->total_amount < 2000000) {//必须大于200万
                return;
            }
            $userSubTrainReward = UserSubTrainReward::where('user_id', $p_id)->where('status', '1')->select('id')->first();
            if ($userSubTrainReward) {//只奖励一次
                return;
            }
            $in_data = [
                'user_id' => $user_id,
                'p_user_id' => $p_id,
                'amount' => 0.00,
                'transaction_amount' => $orders_obj->total_amount,
                'time_start' => $time_start,
                'time_end' => $time_end,
                'p_user_name' => $p_user_name
            ];

            UserSubTrainReward::create($in_data);
        } catch (\Exception $e) {
            Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
            DB::rollBack();

        }
    }


}
