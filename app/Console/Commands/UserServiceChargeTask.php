<?php

namespace App\Console\Commands;



use App\Models\SettlementList;
use App\Models\User;
use App\Models\UserServiceCharge;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserServiceChargeTask extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user_service_charge_task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'user_service_charge_task';

    /**
     * Settlement constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $users = User::where('is_delete', '0')->select('id', 'money', 'name', 'created_at')->get();

            foreach ($users as $user) {
                $user_time_th = date("Y-m-d H:i:s", strtotime($user->created_at . ' +3 month'));
                $current_time = date('Y-m-d H:i:s', time());
                if (strtotime($current_time) < strtotime($user_time_th)) {
                    continue;
                }
                $deduct_time = date('Y-m-d', time());
                $settlement = SettlementList::where('user_id', $user->id)
                    ->where('commission_type', '2')
                    ->where('commission_amount', '>=', 500)
                    ->where('created_at', '>=', $deduct_time)
                    ->first();
                if (!$settlement) {
                    continue;
                }

                //开启事务
                try {
                    DB::beginTransaction();
                    $money = $user->money - 30;//代理商分润金额减去30
                    User::where('id', $user->id)->update(['money' => $money]);
                    $serviceChargeData = [
                        'user_id' => $user->id,
                        'user_name' => $user->name,
                        'cost' => 30,
                        'transaction_amount' => $settlement->total_amount,
                        'commission_month' => $settlement->commission_amount,
                    ];
                    UserServiceCharge::create($serviceChargeData);
                    DB::commit();
                } catch (\Exception $e) {
                    Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
                    DB::rollBack();
                    continue;
                }
            }
        } catch (\Exception $exception) {
            Log::info('扣除服务费定时任务error');
            Log::info($exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine());
        }
    }


}
