<?php

namespace App\Console\Commands;


use App\Models\AgentLevels;
use App\Models\AgentLevelsInfos;
use App\Models\CashBackRule;
use App\Models\CashBackRuleUser;
use App\Models\Device;
use App\Models\Merchant;
use App\Models\MerchantWalletDayCount;
use App\Models\MerchantWalletDetail;
use App\Models\Order;
use App\Models\QrListInfo;
use App\Models\QrUserReturnAmountInfo;
use App\Models\Store;
use App\Models\StoreTransactionReward;
use App\Models\TerminalReward;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use DateTime;

class CashBack extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cashBack';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'cashBack';

    /**
     * Settlement constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $time = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s', time()) . ' -30 day'));
            $stores = Store::where('created_at', '>=', $time)
                ->where('store_type', '!=', 3)
                ->select('store_id', 'created_at', 'store_type', 'user_id', 'store_name')
                ->get();
            //Log::info($stores);
            foreach ($stores as $store) {
                $end_time = date('Y-m-d H:i:s', strtotime($store->created_at . ' +30 day'));
                $table = DB::table('orders');
                $order_obj = $table->select([
                    DB::raw("SUM(`total_amount`) as `total_amount_sum`"),
                ])
                    ->where('store_id', $store->store_id)
                    ->where('created_at', '<=', $end_time)
                    ->where('pay_status', 1)
                    ->first();

                $total_amount_sum = $order_obj->total_amount_sum;
                if ($total_amount_sum >= 10000) {
                    $qrListInfo = QrListInfo::where('store_id', $store->store_id)
//                        ->where('user_id', $store->user_id)
                        ->where('status', '1')
                        ->select('code_num', 'user_id')
                        ->first();
                    if (!$qrListInfo) {
                        continue;
                    }

                    $agentLevelsInfos = AgentLevelsInfos::where('user_id', $store->user_id)->select('*')->first();
                    $level_weight = 1;
                    if ($agentLevelsInfos) {
                        $level_weight = $agentLevelsInfos->level_weight;
                    }
                    $agentLevel = AgentLevels::where('level_weight', $level_weight)->select('*')->first();
                    $amount = $agentLevel->activation_amount;
                    $this->user_cash_back($store->user_id, $qrListInfo->code_num, $store->store_id, $amount, $store->store_name, $store->store_type, '1');
                }

            }

        } catch (\Exception $exception) {
            Log::info('交易返现定时任务error');
            return json_encode(['status' => -1, 'message' => $exception->getMessage()]);
        }

    }

    public function user_cash_back($user_id, $code_num, $store_id, $amount, $store_name, $store_type, $return_type)
    {
        try {
            $table = DB::table('qr_user_return_amount_info');
            $code_obj = $table->select([
                DB::raw("SUM(`amount`) as `amount_sum`"),
            ])
                ->where('code_num', $code_num)
                ->first();
            if ($code_obj->amount_sum >= 20) {
                return;
            }
            $qrUserReturnAmountInfo = QrUserReturnAmountInfo::where('user_id', $user_id)
                ->where('store_id', $store_id)
                ->where('code_num', $code_num)
                ->select('id')
                ->first();
            if (!$qrUserReturnAmountInfo) {
                $inData = [
                    'code_num' => $code_num,
                    'user_id' => $user_id,
                    'store_name' => $store_name,
                    'store_type' => $store_type,
                    'amount' => $amount,
                    'return_type' => $return_type,
                    'store_id' => $store_id
                ];
                try {
                    DB::beginTransaction();
                    $user = User::where('id', $user_id)->select('reward_money')->first();
                    $reward_money = $user->reward_money + $amount;
                    User::where('id', $user_id)
                        ->update(['reward_money' => $reward_money]);
                    QrUserReturnAmountInfo::create($inData);
                    DB::commit();
                } catch (\Exception $e) {
                    Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
                    DB::rollBack();
                }
            }
            $p_user = User::where('id', $user_id)->select('pid')->first();
            if ($p_user->pid == 1) {
                return;
            }
            if ($amount == 0) {
                $max_amount = QrUserReturnAmountInfo::where('store_id', $store_id)
                    ->where('code_num', $code_num)
                    ->select('amount')
                    ->orderBy('amount', 'desc')
                    ->first();
                $amount = $max_amount->amount;
            }
            $agentLevelsInfos = AgentLevelsInfos::where('user_id', $p_user->pid)->select('*')->first();
            $level_weight = 1;
            if ($agentLevelsInfos) {
                $level_weight = $agentLevelsInfos->level_weight;
            }
            $agentLevel = AgentLevels::where('level_weight', $level_weight)->select('*')->first();
            if ($agentLevel->activation_amount >= $amount) {
                $amount = $agentLevel->activation_amount - $amount;
            } else {
                $amount = 0;
            }

            $this->user_cash_back($p_user->pid, $code_num, $store_id, $amount, $store_name, $store_type, '2');

        } catch (\Exception $e) {
            Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());

        }

    }


}
