<?php

namespace App\Console\Commands;


use App\Models\Device;
use App\Models\Store;
use App\Models\User;
use App\Models\UserBoxRebateInfo;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BoxRebateAudit extends Command
{

    /**
     * The name and signature of the console command.
     *音箱返现
     * @var string
     */
    protected $signature = 'box_rebate_audit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'box_rebate_audit';

    /**
     * Settlement constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $devices = Device::where('status', 1)->select('store_id', 'created_at')->get();

            foreach ($devices as $device) {
                $userBoxRebateInfo = UserBoxRebateInfo::where('store_id', $device->store_id)->select('id')->first();
                if ($userBoxRebateInfo) {
                    continue;
                }
                $store = Store::where('store_id', $device->store_id)->where('is_close', 0)
                    ->where('is_delete', '0')->select('user_id', 'store_id', 'store_name')->first();
                if (!$store) {
                    continue;
                }
                Log::info($device->created_at);
                $obj = DB::table('orders');
                $order_obj = $obj->select([
                    'total_amount',
                    DB::raw("SUM(`total_amount`) AS `amount`"),
                ])
                    ->where('pay_status', 1)
                    ->where('store_id', $store->store_id)
                    ->where('created_at', '>=', $device->created_at)
                    ->first();
                Log::info($order_obj->amount);
                if ($order_obj->amount < 600000) {
                    continue;
                }
                //开启事务
                try {
                    DB::beginTransaction();
                    $user = User::where('id', $store->user_id)->where('is_delete', '0')->select('id', 'reward_money', 'name')->first();

                    $money = $user->reward_money + $order_obj->amount;
                    User::where('id', $user->id)->update(['reward_money' => $money]);

                    $boxRebateDate = [
                        'user_id' => $user->id,
                        'user_name' => $user->name,
                        'store_id' => $store->store_id,
                        'store_name' => $store->store_name,
                        'amount' => 99
                    ];
                    UserBoxRebateInfo::create($boxRebateDate);
                    DB::commit();
                } catch (\Exception $e) {
                    Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
                    DB::rollBack();
                    continue;
                }
            }

        } catch (\Exception $e) {
            Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
        }
    }
}
