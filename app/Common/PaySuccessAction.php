<?php

namespace App\Common;

use App\Api\Controllers\BaseController;
use App\Api\Controllers\Config\WeixinConfigController;
use App\Api\Controllers\Device\YlianyunAopClient;
use App\Api\Controllers\Device\ZlbzController;
use App\Api\Controllers\Merchant\TemplateMessageController;
use App\Api\Controllers\Push\JpushController;
use App\Api\Controllers\Qmtt\AliBaseController;
use App\Jobs\CzUpdateStatus;
use App\Models\Device;
use App\Models\MqttConfig;
use App\Models\VConfig;
use App\Models\WeixinNotify;
use App\Models\WeixinNotifyTemplate;
use EasyWeChat\Factory;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use karpy47\PhpMqttClient\MQTTClient;
use MyBank\Tools;
use TencentCloud\Common\Credential;
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Common\Profile\HttpProfile;
use TencentCloud\Common\Exception\TencentCloudSDKException;
use TencentCloud\Iotcloud\V20180614\IotcloudClient;
use TencentCloud\Iotcloud\V20180614\Models\PublishMessageRequest;
use Illuminate\Support\Facades\Log;

class PaySuccessAction
{
    //支付成功以后的操作
    public static function action($data)
    {
        try {
            //检查是否操作
            if (Cache::has('out_trade_no_' . $data['out_trade_no'])) {
                // Log::info('--PaySuccessAction  action--检查是否操作---Cache.  ---out_trade_no:' . $data['out_trade_no']);
                return;
            }
            self::action_tb($data);

            Cache::add('out_trade_no_' . $data['out_trade_no'], '1', 5);

        } catch (\Exception $ex) {
            \Illuminate\Support\Facades\Log::info('支付成功后error');
            \Illuminate\Support\Facades\Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }
    }


    //同步操作数据库
    public static function action_tb($data)
    {
        $push_action = 'tb';
        $count_action = "tb";
        $user_money = "tb";//
        $fuwu = "tb";//
        $member = "tb";//
        $member_cz = "tb";//
        $merchant_money = "tb";//

        $QUEUE_DRIVER = Cache::remember('QUEUE_DRIVER', 1000, function () {
            $QUEUE_DRIVER = env('QUEUE_DRIVER');
            return $QUEUE_DRIVER;
        }); //cmq、memcache

        if ($QUEUE_DRIVER == "redis") {
            //2.统计
            $count_action = "yb";
            //3.返佣
            $user_money = "yb";
            //4.服务
            $fuwu = "yb";
            //8会员
            $member = "tb";
            //9.充值
            $member_cz = "tb";
            //10 商户返佣
            $merchant_money = "yb";
        }

        //8.会员
        try {
            if ($member == "tb") {
                $obj = new AddMember($data);
                $obj->insert();
            } else {
                dispatch(new \App\Jobs\AddMember($data));
            }
        } catch (\Exception $ex) {
            \Illuminate\Support\Facades\Log::info('8.会员error');
            \Illuminate\Support\Facades\Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

        //9.充值订单处理
        if (isset($data['device_id']) && $data['device_id'] == 'member_cz') {
            try {
                if ($member_cz == "tb") {
                    $obj = new \App\Common\CzUpdateStatus($data);
                    $obj->insert();
                } else {
                    dispatch(new \App\Jobs\CzUpdateStatus($data));
                }
            } catch (\Exception $ex) {
                \Illuminate\Support\Facades\Log::info('会员充值状态同步error');
                \Illuminate\Support\Facades\Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            }
        }

        //1.安卓app语音播报
        try {
            //主扫不播报
            if (strpos($data['out_trade_no'], 'scan')) {
                $data['no_push'] = 1;
            }

            //不播报
            if (isset($data['no_push'])) {
                //不播报
            } else {
                //1.安卓app语音播报
                if ($push_action == "tb") {
                    $jpush = new JpushController();
                    $jpush->push($data['ways_type_desc'], $data['total_amount'], $data['out_trade_no'], $data['merchant_id'], $data['store_id'], $data['config_id']);
                } else {
                    //  dispatch(new \App\Jobs\PayJpush($data['ways_type_desc'], $data['total_amount'], $data['out_trade_no'], $data['merchant_id'], $data['store_id'], $data['config_id']));
                }
            }
        } catch (\Exception $ex) {
            \Illuminate\Support\Facades\Log::info('安卓app语音播报error');
            \Illuminate\Support\Facades\Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

        //2.统计
        try {
            if (isset($data['no_count'])) {

            } else {
                $fee_amount = isset($data['fee_amount']) && $data['fee_amount'] ? $data['fee_amount'] : $data['rate'] * $data['total_amount'];
                $fee_amount = number_format($fee_amount, 4, '.', '');
                $data['fee_amount'] = $fee_amount;
                if ($count_action == "tb") {
                    StoreDayMonthOrder::insert($data);
                } else {
                    //队列
                    dispatch(new \App\Jobs\StoreDayMonthOrder($data));
                }
            }
        } catch (\Exception $ex) {
            \Illuminate\Support\Facades\Log::info('2.统计error');
            \Illuminate\Support\Facades\Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

        //3.服务商返佣
        try {
            if (isset($data['no_user_money'])) {
                // Log::info('----------不计算返佣---------->'.$data['company']);
            } else {
                if ($user_money == "tb") {
                    $UserGetMoney = [
                        'config_id' => $data['config_id'],//服务商ID
                        'user_id' => $data['user_id'],//直属代理商
                        'store_id' => $data['store_id'],//门店
                        'out_trade_no' => $data['out_trade_no'],//订单号
                        'ways_type' => $data['ways_type'],//通道类型
                        'order_total_amount' => $data['total_amount'],//交易金额
                        'store_ways_type_rate' => $data['rate'],//交易时的费率
                        'source_type' => $data['source_type'],//返佣来源
                        'source_desc' => $data['source_desc'],//返佣来源说明
                    ];
                    $obj = new UserGetMoney($UserGetMoney);
                    $obj->insert();
                } else {
                    //队列
                    $UserGetMoney = [
                        'config_id' => $data['config_id'],//服务商ID
                        'user_id' => $data['user_id'],//直属代理商
                        'store_id' => $data['store_id'],//门店
                        'out_trade_no' => $data['out_trade_no'],//订单号
                        'ways_type' => $data['ways_type'],//通道类型
                        'order_total_amount' => $data['total_amount'],//交易金额
                        'store_ways_type_rate' => $data['rate'],//交易时的费率
                        'source_type' => $data['source_type'],//返佣来源
                        'source_desc' => $data['source_desc'],//返佣来源说明
                    ];
                    dispatch(new \App\Jobs\UserGetMoney($UserGetMoney));
                }
            }
        } catch (\Exception $ex) {
            \Illuminate\Support\Facades\Log::info('3.服务商返佣error');
            \Illuminate\Support\Facades\Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

        //4.服务
        try {
            if (isset($data['no_fuwu'])) {

            } else {
                if ($fuwu == "tb") {
                    //   $title = $data['ways_type_desc'] . '到账' . $data['total_amount'] . '元';
                    //   MerchantFuwu::insert($title, $data['ways_type_desc'], $data['store_id'], $data['merchant_id'], $data['total_amount'], $data['out_trade_no']);
                } else {
                    //  $title = $data['ways_type_desc'] . '到账' . $data['total_amount'] . '元';
                    //  dispatch(new \App\Jobs\MerchantFuwu($title, $data['ways_type_desc'], $data['store_id'], $data['merchant_id'], $data['total_amount'], $data['out_trade_no']));
                }
            }
        } catch (\Exception $ex) {
            \Illuminate\Support\Facades\Log::info('4服务error');
            \Illuminate\Support\Facades\Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

        //5.微信公众号消息提醒
        try {
//            if (isset($data['no_wx'])) {
//
//            } else {
//                self::weixin_notify($data);
//            }
            $templateMessageController = new TemplateMessageController();
            $templateData = [
                'store_id' => $data['store_id'],
                'total_amount' => $data['total_amount'],
                'store_name' => $data['store_name'],
                'ways_type_desc' => $data['ways_type_desc'],
                'out_trade_no' => $data['out_trade_no'],
                'pay_time' => $data['pay_time'],
            ];
            $templateMessageController->template_message($templateData);//消息通知
        } catch (\Exception $ex) {
            \Illuminate\Support\Facades\Log::info('5微信公众号消息提醒error');
            \Illuminate\Support\Facades\Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

        //6.打印
        try {
            if (isset($data['no_print'])) {
                //不打印
            } else {
                if (isset($data['remark'])) {

                } else {
                    $data['remark'] = '备注';
                }
                self::print_data($data);
            }
        } catch (\Exception $ex) {
            \Illuminate\Support\Facades\Log::info('打印error');
            \Illuminate\Support\Facades\Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

        //7.播报
        try {
            if (isset($data['no_v'])) {
                //不播报
            } else {
                //1 支付宝 2 微信支付 3京东支付 4 银联闪付  5 积分抵扣  6.会员卡
                $type = '0';
                if ($data['ways_source'] == "alipay") {
                    $type = '1';
                }

                if ($data['ways_source'] == "weixin") {
                    $type = '2';
                }

                if ($data['ways_source'] == "member") {
                    $type = '6';
                }

                $device_id = isset($data['device_id']) ? $data['device_id'] : "";

                self::v_send($data['total_amount'] * 100, $data['store_id'], $type, $data['merchant_id'], $data['config_id'], $data['out_trade_no'], $device_id);
            }
        } catch (\Exception $ex) {
            \Illuminate\Support\Facades\Log::info('播报error');
            \Illuminate\Support\Facades\Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }

        //3.商户返佣
        try {
            if (isset($data['no_merchant_money'])) {

            } else {
                if ($merchant_money == "tb") {
                    $MerchantGetMoney = [
                        'config_id' => $data['config_id'], //服务商ID
                        'user_id' => $data['user_id'], //直属代理商
                        'store_id' => $data['store_id'], //门店
                        'out_trade_no' => $data['out_trade_no'], //订单号
                        'ways_type' => $data['ways_type'], //通道类型
                        'order_total_amount' => $data['total_amount'], //交易金额
                        'store_ways_type_rate' => $data['rate'], //交易时的费率
                        'source_type' => $data['source_type'], //返佣来源
                        'source_desc' => $data['source_desc'], //返佣来源说明
                        'company' => $data['company'], //
                        'ways_source' => $data['ways_source'], //
                    ];

                    $obj = new  MerchantGetMoney($MerchantGetMoney);
                    $obj->insert();
                } else {
                    //队列
                    $MerchantGetMoney = [
                        'config_id' => $data['config_id'], //服务商ID
                        'user_id' => $data['user_id'], //直属代理商
                        'store_id' => $data['store_id'], //门店
                        'out_trade_no' => $data['out_trade_no'], //订单号
                        'ways_type' => $data['ways_type'], //通道类型
                        'order_total_amount' => $data['total_amount'], //交易金额
                        'store_ways_type_rate' => $data['rate'], //交易时的费率
                        'source_type' => $data['source_type'], //返佣来源
                        'source_desc' => $data['source_desc'], //返佣来源说明
                        'company' => $data['company'], //
                        'ways_source' => $data['ways_source'], //
                    ];
                    dispatch(new \App\Jobs\MerchantGetMoney($MerchantGetMoney));
                }
            }
        } catch (\Exception $ex) {
            \Illuminate\Support\Facades\Log::info('商户返佣error');
            \Illuminate\Support\Facades\Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }
    }


    //微信公众号消息提醒
    public static function weixin_notify($data)
    {
        try {
            //判断服务商是否有设置模版消息
            $config_id = $data['config_id'];
            $config = new WeixinConfigController();
            $config_obj = $config->weixin_config_obj($config_id);

            if ($config_obj) {
                $config = [
                    'app_id' => $config_obj->wx_notify_appid,
                    'secret' => $config_obj->wx_notify_secret,
                ];

                //有收银员，推送给收银员和店长
                if (isset($data['merchant_id']) && !empty($data['merchant_id'])) {
                    $WeixinNotify = WeixinNotify::where('merchant_id', $data['merchant_id'])
                        ->where('store_id', $data['store_id'])
                        ->select('wx_open_id')
                        ->first();
                    if ($WeixinNotify) {
                        $app = Factory::officialAccount($config);
                        $app->template_message->send([
                            'touser' => $WeixinNotify->wx_open_id,
                            'template_id' => $config_obj->template_id,
                            'url' => url('/mb/login'),
                            'data' => [
                                'first' => '门店名称:' . $data['store_name'],
                                'keyword1' => $data['total_amount'] . '元',
                                'keyword2' => $data['ways_type_desc'],
                                'keyword3' => date('Y-m-d H:i:s', time()),
                                'keyword4' => $data['out_trade_no'],
                                'remark' => ''
                            ]
                        ]);
                    }

                    $managerNotify = DB::table('weixin_notifys as wn')
                        ->select(['wn.store_id', 'wn.merchant_id', 'wn.wx_open_id'])
                        ->join('merchant_stores as ms', function ($join) {
                            $join->on('wn.store_id', '=', 'ms.store_id')
                                ->on('wn.merchant_id', '=', 'ms.merchant_id')
                                ->where('ms.type', '=', '1');
                        })
                        ->join('merchants as m', function ($join2) {
                            $join2->on('m.id', '=', 'wn.merchant_id')
                                ->where('m.type', '=', '1');
                        })
                        ->where('wn.merchant_id', '<>', $data['merchant_id'])
                        ->where('wn.store_id', $data['store_id'])
                        ->get();
                    if ($managerNotify) {
                        foreach ($managerNotify as $key => $values) {
                            if ($values->wx_open_id) {
                                $app = Factory::officialAccount($config);
                                $app->template_message->send([
                                    'touser' => $values->wx_open_id,
                                    'template_id' => $config_obj->template_id,
                                    'url' => url('/mb/login'),
                                    'data' => [
                                        'first' => '门店名称:' . $data['store_name'],
                                        'keyword1' => $data['total_amount'] . '元',
                                        'keyword2' => $data['ways_type_desc'],
                                        'keyword3' => date('Y-m-d H:i:s', time()),
                                        'keyword4' => $data['out_trade_no'],
                                        'remark' => ''
                                    ],
                                ]);
                            }
                        }
                    }

                } else {
                    $WeixinNotify = WeixinNotify::where('store_id', $data['store_id'])
                        ->select('wx_open_id')
                        ->get();
                    if ($WeixinNotify) {
                        foreach ($WeixinNotify as $k => $v) {
                            $app = Factory::officialAccount($config);
                            $app->template_message->send([
                                'touser' => $v->wx_open_id,
                                'template_id' => $config_obj->template_id,
                                'url' => url('/mb/login'),
                                'data' => [
                                    'first' => '门店名称:' . $data['store_name'],
                                    'keyword1' => $data['total_amount'] . '元',
                                    'keyword2' => $data['ways_type_desc'],
                                    'keyword3' => date('Y-m-d H:i:s', time()),
                                    'keyword4' => $data['out_trade_no'],
                                    'remark' => ''
                                ],
                            ]);
                        }
                    }
                }
            }

        } catch (\Exception $ex) {
            \Illuminate\Support\Facades\Log::info('微信公众号消息提醒error');
            \Illuminate\Support\Facades\Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }
    }


    //打印
    public static function print_data($data)
    {
        try {
            $p = Device::where('store_id', $data['store_id'])
                ->where('merchant_id', $data['merchant_id'])
                ->where("type", "p")
                ->get();

            //收银员未绑定走门店机器
            if ($p->isEmpty()) {
                $p = Device::where('store_id', $data['store_id'])
                    ->where('merchant_id', '')
                    ->where("type", "p")
                    ->get();
            }

            if (!$p->isEmpty()) {
                foreach ($p as $v) {
                    //智联
                    if ($v->device_type == "p_zlbz_1") {
                        $da = new ZlbzController();
                        $store_name = $data['store_name'];
                        $order_no = '' . $data['out_trade_no'] . '';
                        $price = '' . $data['total_amount'] . '';
                        $remark = '' . $data['remark'] . '';
                        $qr_url = 'https:www';
                        $type = $data['ways_type_desc'];
                        try {
                            $device_id = $v->device_no;
                            $da->print_send($device_id, $type, $store_name, $order_no, $price, $remark, $qr_url);
                        } catch (\Exception $exception) {
                            \Illuminate\Support\Facades\Log::info($exception);
                            continue;
                        }
                    }

                    //K4
                    if ($v->device_type == "p_yly_k4") {
                        $VConfig = VConfig::where('config_id', $v->config_id)
                            ->select(
                                'yly_user_id',
                                'yly_api_key'
                            )->first();
                        if (!$VConfig) {
                            $VConfig = VConfig::where('config_id', '1234')
                                ->select(
                                    'yly_user_id',
                                    'yly_api_key'
                                )->first();
                        }
                        if (!$VConfig) {
                            \Illuminate\Support\Facades\Log::info('易联云打印未配置app/common/PaySuccessAction.php');
                            continue;
                        }

                        try {
                            $da = new YlianyunAopClient();
                            $push_id = $VConfig->yly_user_id; //用户id
                            $push_key = $VConfig->yly_api_key; //api密钥
                            $data['device_key'] = $v->device_key;
                            $data['device_no'] = $v->device_no;
                            $data['push_id'] = $push_id;
                            $data['push_key'] = $push_key;
                            $data['type'] = $data['ways_type_desc'];
                            $da->send_print($data);
                        } catch (\Exception $exception) {
                            \Illuminate\Support\Facades\Log::info('易联云打印');
                            \Illuminate\Support\Facades\Log::info($exception);
                            continue;
                        }
                    }
                }
            }
        } catch (\Exception $ex) {
            \Illuminate\Support\Facades\Log::info('打印error');
            \Illuminate\Support\Facades\Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }
    }


    //播报设备 金额是分
    public static function v_send($price, $store_id, $type, $merchant_id, $config_id = "1234", $out_trade_no, $device_id = "")
    {
        try {
            //设备播报
            if ($merchant_id && (int)$merchant_id != 0) {
                $Device = Device::where('store_id', $store_id)
                    ->where('merchant_id', $merchant_id)
                    ->whereIn("device_type", ["v_bp_1", 'v_jbp_d30', 'v_kd_58', 'v_zw_1', 's_bp_sl51', 's_bp_sl56',
                        'v_zlbz_1', 's_ky_mp10', 'v_ps_sp08', 'phone', 'v_cls_q038', 'v_cls_wifi'])
                    ->get();
            } else {
                Log::info("-----merchant_id is null----:");
                $Device = Device::where('store_id', $store_id)
                    //->where('merchant_id', '')
                    ->whereIn("device_type", ["v_bp_1", 'v_jbp_d30', 'v_kd_58', 'v_zw_1', 's_bp_sl51', 's_bp_sl56',
                        'v_zlbz_1', 's_ky_mp10', 'v_ps_sp08', 'phone', 'v_cls_q038', 'v_cls_wifi'])
                    ->get();
            }
            if ($Device->isEmpty()) {

            } else {
                foreach ($Device as $k => $v) {
//                    \Illuminate\Support\Facades\Log::info('播报设备');
//                    \Illuminate\Support\Facades\Log::info($v->device_no);
                    //智联博众
                    if ($v->device_type == "v_zlbz_1") {
                        $VConfig = VConfig::where('config_id', $config_id)
                            ->select('zlbz_token')->first();

                        if (!$VConfig) {
                            $VConfig = VConfig::where('config_id', '1234')
                                ->select(
                                    'zlbz_token'
                                )->first();
                        }

                        $datap = [
                            'id' => $v->device_no,
                            'price' => $price,
                            'uid' => $store_id,
                            'token' => $VConfig->zlbz_token,
                            'pt' => $type
                        ];

                        //  Tools::curl($datap, 'http://39.106.131.149/add.php');
                    }

                    //智网
                    if ($v->device_type == "v_zw_1") {
                        $VConfig = VConfig::where('config_id', $config_id)
                            ->select(
                                'zw_token'
                            )->first();
                        if (!$VConfig) {
                            $VConfig = VConfig::where('config_id', '1234')
                                ->select(
                                    'zw_token'
                                )->first();
                        }
                        $url = 'http://cloudspeaker.smartlinkall.com/add.php?token=' . $VConfig->zw_token . '&id=' . $v->device_no . '&uid=' . $store_id . $v->device_no . '&seq=' . time() . '&price=' . $price . '&pt=' . $type . '&vol=100';
                        $data = Tools::curl_get($url);

                        //老的智网喇叭配置
                        $oldVConfig = VConfig::where('config_id', $config_id)
                            ->select('old_zw_token')
                            ->first();
                        if (!$oldVConfig) {
                            $oldVConfig = VConfig::where('config_id', '1234')
                                ->select('old_zw_token')
                                ->first();
                        }
                        if ($oldVConfig) {
                            $url = 'http://cloudspeaker.smartlinkall.com/add.php?token=' . $oldVConfig->old_zw_token . '&id=' . $v->device_no . '&uid=' . $store_id . $v->device_no . '&seq=' . time() . '&price=' . $price . '&pt=' . $type . '&vol=100';
                            $data = Tools::curl_get($url);
                        }
                    }

                    //推送卡台小喇叭+波谱sl51
                    if (in_array($v->device_type, ['v_bp_1', 'v_jbp_d30', 'v_kd_58', 's_bp_sl51', 's_bp_sl56', 's_ky_mp10', 'v_ps_sp08', 'phone'])) {
                        $type_desc = "";
                        //1 支付宝 2 微信支付 3京东支付 4 银联闪付  5 积分抵扣  6.会员卡
                        if ($type == '1') {
                            $type_desc = "支付宝";
                        }
                        if ($type == '2') {
                            $type_desc = "微信";
                        }

                        if ($type == '3') {
                            $type_desc = "京东";
                        }
                        if ($type == '4') {
                            $type_desc = "银联云闪付";
                        }

                        if ($type == '5') {
                            $type_desc = "积分抵扣";
                        }

                        if ($type == '6') {
                            $type_desc = "会员卡";
                        }

                        //反扫推送类型的设备不播报
                        if (in_array($v->device_type, ['s_bp_sl51', 's_bp_sl56']) && strpos($out_trade_no, 'scan')) {

                        } else {
                            $message = $type_desc . '支付' . ($price / 100) . '元';
                            $orderNum = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));
                            self::mqtt($v->device_no, $v->device_type, $message, $orderNum, $config_id, $type, $price);
                        }
                    }

                    // 畅立收Q038
                    if ($v->device_type == "v_cls_q038") {
                        $VConfig = VConfig::where('config_id', $config_id)
                            ->select(
                                'tencent_cloud_secretid',
                                'tencent_cloud_secretkey',
                                'cls_product_id',
                                'cls_prefix'
                            )->first();
                        if (!$VConfig) {
                            $VConfig = VConfig::where('config_id', '1234')
                                ->select(
                                    'tencent_cloud_secretid',
                                    'tencent_cloud_secretkey',
                                    'cls_product_id',
                                    'cls_prefix'
                                )->first();
                        }

                        $secretId = $VConfig->tencent_cloud_secretid;
                        $secretKey = $VConfig->tencent_cloud_secretkey;
                        $productId = $VConfig->cls_product_id;
                        $deviceName = $v->device_no;
                        $money = ($price / 100);
                        $msg = $VConfig->cls_prefix . $money . '元';
                        $channel = 0;
                        $msgid = $store_id . date('YmdHis', time());
                        try {
                            $cred = new Credential($secretId, $secretKey);
                            $httpProfile = new HttpProfile();
                            $httpProfile->setEndpoint("iotcloud.tencentcloudapi.com");

                            $clientProfile = new ClientProfile();
                            $clientProfile->setHttpProfile($httpProfile);
                            $client = new IotcloudClient($cred, "", $clientProfile);

                            $req = new PublishMessageRequest();
                            $params = array(
                                "Topic" => $productId . '/' . $deviceName . '/data',
                                "Payload" => "{\"msg\":\"$msg\",\"channel\":$channel,\"money\":\"$money\",\"msgid\":\"$msgid\"}",
                                "ProductId" => $productId,
                                "DeviceName" => $deviceName
                            );
                            $req->fromJsonString(json_encode($params));

                            $client->PublishMessage($req);
                        } catch (TencentCloudSDKException $e) {
                        }
                    }

                    // 畅立收Wfi
                    if ($v->device_type == "v_cls_wifi") {

                        $VConfig = VConfig::where('config_id', $config_id)
                            ->select(
                                'tencent_cloud_secretid',
                                'tencent_cloud_secretkey',
                                'cls_product_id',
                                'cls_prefix'
                            )->first();
                        if (!$VConfig) {
                            $VConfig = VConfig::where('config_id', '1234')
                                ->select(
                                    'tencent_cloud_secretid',
                                    'tencent_cloud_secretkey',
                                    'cls_product_id',
                                    'cls_prefix'
                                )->first();
                        }

                        $secretId = $VConfig->tencent_cloud_secretid;
                        $secretKey = $VConfig->tencent_cloud_secretkey;
                        $productId = $VConfig->cls_product_id;
                        $deviceName = $v->device_no;
                        $msgid = $store_id . date('YmdHis', time());
                        $money = ($price / 100);

                        $amountArr = explode('.', $money);
                        $num_type = 1;// 1 整数， 0 小数
                        if (count($amountArr) > 1) {
                            $num_type = 0;
                        }
                        //$type 1 支付宝 2 微信支付
                        $msg = self::convertAmountToCn($money, $type, $num_type);

                        try {
                            $cred = new Credential($secretId, $secretKey);
                            $httpProfile = new HttpProfile();
                            $httpProfile->setEndpoint("iotcloud.tencentcloudapi.com");

                            $clientProfile = new ClientProfile();
                            $clientProfile->setHttpProfile($httpProfile);
                            $client = new IotcloudClient($cred, "", $clientProfile);

                            $req = new PublishMessageRequest();

                            $params = array(
                                "Topic" => $productId . '/' . $deviceName . '/data',
                                "Payload" => "{\"playAudibleMsg\":\"$msg\",\"orderId\":\"$msgid\"}",
                                "ProductId" => $productId,
                                "DeviceName" => $deviceName
                            );


                            $req->fromJsonString(json_encode($params));

                            $client->PublishMessage($req);
                        } catch (TencentCloudSDKException $e) {
                        }
                    }

                }
            }
        } catch (\Exception $ex) {
            \Illuminate\Support\Facades\Log::info('播报设备error');
            \Illuminate\Support\Facades\Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }
    }


    //mqtt推送
    public static function mqtt($device_id, $device_type, $message, $orderNum, $config_id, $type, $price)
    {
        try {
            $MqttConfig = MqttConfig::where('config_id', $config_id)->first();
            if (!$MqttConfig) {
                $MqttConfig = MqttConfig::where('config_id', '1234')->first();
            }

            if (!$MqttConfig) {
                return false;
            }
            $server = $MqttConfig->server;
            $port = $MqttConfig->port;
            $mq_group_id = $MqttConfig->group_id;
            $username = "Signature|" . $MqttConfig->access_key_id . "|" . $MqttConfig->instance_id . "";
            $str = '' . $MqttConfig->group_id . '@@@' . $device_id . '1' . '';
            $key = $MqttConfig->access_key_secret;
            $str = mb_convert_encoding($str, "UTF-8");
            $password = base64_encode(hash_hmac("sha1", $str, $key, true));

            $client_id = $mq_group_id . '@@@' . $device_id;
            $server_client_id = $mq_group_id . '@@@' . $device_id . '1';
            $topic = $MqttConfig->topic . "/p2p/" . $client_id;

            $content = json_encode([
                'message' => $message,
                'orderNum' => $orderNum,
                'type' => $type,//  1 支付宝 2 微信支付 3京东支付 4 银联闪付  5 积分抵扣  6.会员卡
                'price' => '' . $price . ''//分
            ]);

            $mqtt = new AliBaseController($server, $port, $server_client_id);
            if ($mqtt->connect(false, NULL, $username, $password)) {
                $mqtt->publish($topic, $content, 1);
                //$mqtt->close();
            } else {

            }
        } catch (\Exception $ex) {
            \Illuminate\Support\Facades\Log::info('mqtt推送errors');
            \Illuminate\Support\Facades\Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
        }
    }

    public static function convertAmountToCn($amount, $type, $num_type)
    {

        // 预定义中文转换的数组
        //$digital = array('零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖');
        // 预定义单位转换的数组
        //$position = array('仟', '佰', '拾', '亿', '仟', '佰', '拾', '万', '仟', '佰', '拾', '点');

        $digital = array('000-', '001-', '002-', '003-', '004-', '005-', '006-', '007-', '008-', '009-');
        $position = array('012-', '011-', '010-', '亿', '012-', '011-', '010-', '013-', '012-', '011-', '010-', '014-');

        // 将金额的数值字符串拆分成数组
        $amountArr = explode('.', $amount);

        // 将整数位的数值字符串拆分成数组
        $integerArr = str_split($amountArr[0], 1);

        // 将整数部分替换成大写汉字
        $result = '';
        $integerArrLength = count($integerArr);     // 整数位数组的长度
        $positionLength = count($position);         // 单位数组的长度
        $zeroCount = 0;                             // 连续为0数量
        for ($i = 0; $i < $integerArrLength; $i++) {
            //如果是0.02类型
            if ($integerArrLength == 1 && $integerArr[$i] == 0) {  //个位数处理
                $result = $digital[0] . $position[$positionLength - $integerArrLength + $i];;
                break;
            } else if ($integerArrLength == 2 && $integerArr[$i] == 1 && $i == 0) { //10特殊处理
                $result .= $position[$positionLength - $integerArrLength + $i];;
                continue;
            } else {
                // 如果数值不为0,则正常转换
                if ($integerArr[$i] != 0) {
                    // 如果前面数字为0需要增加一个零
                    if ($zeroCount >= 1) {
                        $result .= $digital[0];
                    }
                    $result .= $digital[$integerArr[$i]] . $position[$positionLength - $integerArrLength + $i];

                    $zeroCount = 0;
                } else {
                    $zeroCount += 1;

                    // 如果数值为0, 且单位是亿,万,元这三个的时候,则直接显示单位
                    if (($positionLength - $integerArrLength + $i + 1) % 4 == 0) {
                        $result = $result . $position[$positionLength - $integerArrLength + $i];
                    }

                }
            }

        }

        if ($num_type == 1) {
            $result = str_replace('014-', '', $result);
        }
        // 如果小数位也要转换
        if ($num_type == 0) {

            // 将小数位的数值字符串拆分成数组
            $decimalArr = str_split($amountArr[1], 1);
            // 将角替换成大写汉字. 如果为0,则不替换
            if ($decimalArr[0] != 0) {
                $result = $result . $digital[$decimalArr[0]];
            } else {
                $result = $result . $digital[0];
            }
            // 将分替换成大写汉字. 如果为0,则不替换
            if (isset($decimalArr[1])) {
                if ($decimalArr[1] != 0) {
                    $result = $result . $digital[$decimalArr[1]];
                }
//            }else{
//                $result = $result . $digital[0];
            }


        }
        if ($type == 1) { //069-支付宝
            return '069-' . '065-' . $result . '015';
        } else if ($type == 2) { //067-微信
            return '067-' . '065-' . $result . '015';
        } else {
            return '065-' . $result . '015';
        }
    }

}

