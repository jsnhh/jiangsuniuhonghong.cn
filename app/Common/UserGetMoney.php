<?php

namespace App\Common;


use App\Models\User;
use App\Models\UserWalletDetail;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Log;

class UserGetMoney
{

    public $store_id;//门店
    public $out_trade_no;//订单号
    public $order_total_amount;//订单金额
    public $user_id;//直属推广员ID
    public $ways_type;//通道类型
    public $store_ways_type_rate;//门店通道类型费率
    public $source_type;//返佣类型
    public $source_desc;//返佣类型说明
    public $config_id;//返佣类型说明
    public $rate;//分润比例


    /**
     * Create a new job instance.
     *
     * UserGetMoney constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->user_id = $data['user_id'];
        $this->store_id = $data['store_id'];
        $this->out_trade_no = $data['out_trade_no'];
        $this->ways_type = $data['ways_type'];
        $this->order_total_amount = $data['order_total_amount'];
        $this->store_ways_type_rate = $data['store_ways_type_rate'];
        $this->source_type = $data['source_type'];
        $this->source_desc = $data['source_desc'];
        $this->config_id = $data['config_id'];
        $this->rate = 0.001;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function insert()
    {
        try {
            $now_user_id = $this->user_id; //商户直接归属的代理商id
            $user = User::where('id', $now_user_id)
                ->select('pid', 'id', 'name', 'phone')
                ->first();

            if ($user) {
                $day = date('Ymd', time());
                $name = $user->name;
                $phone = $user->phone;

                $money = $this->order_total_amount * $this->rate; //直属代理商
                $money = number_format($money, 4, '.', '');
                $store_id = $this->store_id;
                $config_id = $this->config_id;
                $out_trade_no = $this->out_trade_no;
                $source_type = $this->source_type;
                $source_desc = $this->source_desc;

                $user_detail = [
                    'store_id' => $store_id,
                    'config_id' => $config_id,
                    'user_id' => $now_user_id,
                    'user_name' => $name,
                    'phone' => $phone,
                    'money' => $money,
                    'out_trade_no' => $out_trade_no,
                    'trade_no' => $out_trade_no,
                    'source_type' => $source_type,
                    'source_desc' => $source_desc,
                    'pay_money' => $this->order_total_amount,
                    'rate' => $this->rate,
                    'settlement' => '01',
                    'settlement_desc' => '已结算'
                ];


                //检查是否已入库
                if (Cache::has($source_type . $store_id . $out_trade_no . $now_user_id . $money)) {
                    Log::info('--UserWalletDetail--检查是否已入库---Cache.  ---out_trade_no:' . $out_trade_no);
                    return;
                }

                $add_user_wallet_detail_res = self::insert_UserWalletDetail($user_detail, $day);
                if (!$add_user_wallet_detail_res) {
                    Log::info('代理商日结入库-直属入库-失败');
                    Log::info($out_trade_no);
                    Log::info($store_id);
                }

                $userSettlementDay = new UserSettlementDay();
                $settlementDate = [
                    'user_id' => $now_user_id,
                    'day_money' => $money,
                    'total_amount' => $this->order_total_amount,
                    'out_trade_no' => $out_trade_no
                ];
                $userSettlementDay->settlementDay($settlementDate);
                Cache::add($source_type . $store_id . $out_trade_no . $now_user_id . $money, '1', 10);


            } else {
                Log::info('日结入库-代理商-不存在');
                Log::info($now_user_id);
                Log::info($this->ways_type);
            }
        } catch (\Exception $exception) {
            Log::info($exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine());
        }
    }


//公共进库
    public
    static function insert_UserWalletDetail($UserWalletDetail, $day)
    {
        try {
            //如果存在base
            if (Schema::hasTable('user_wallet_details_base')) {
                //后面可以不进这个数据库
                $table = 'user_wallet_details_' . $day;
                //判断是否有表
                if (!Schema::hasTable($table)) {
                    //无表
                    DB::update('create table ' . $table . ' like 	user_wallet_details_base');
                }
                $UserWalletDetail['created_at'] = date('Y-m-d H:i:s', time());
                $UserWalletDetail['updated_at'] = date('Y-m-d H:i:s', time());
                $res = DB::table($table)->insert($UserWalletDetail);
            } else {
                $userWalletDetail = UserWalletDetail::where('out_trade_no', $UserWalletDetail['out_trade_no'])->select('out_trade_no')->first();
                if (!$userWalletDetail) {
                    $res = UserWalletDetail::create($UserWalletDetail);
                } else {
                    $res = 1;
                }

            }

            return $res;
        } catch (\Exception $e) {
            Log::info('代理商日结-公共进库-error');
            Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
        }
    }

}