<?php
namespace App\Common\Enum;

class BaseStatusCodeEnum
{
    /************** layui 返回状态码 *****************/
    const CODE_1 = "操作成功";
    const CODE_2 = "操作失败";

    /************** 200 请求成功 *****************/
    const CODE_200 = "操作成功";

    /************** 4 请求失败 *****************/
    const CODE_40000 = "操作失败";
    const CODE_3 = "测试";
}
