<?php
namespace App\Common;


use App\Models\MemberJf;
use App\Models\MemberList;
use App\Models\MemberSetJf;
use App\Models\MemberTpl;
use App\Models\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Log;

class AddMember
{
    public $store_id;//门店
    public $out_trade_no;//订单号
    public $ways_type;//通道类型
    public $config_id;//
    public $ways_source;//来源


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->store_id = $data['store_id'];
        $this->out_trade_no = $data['out_trade_no'];
        $this->ways_type = $data['ways_type'];
        $this->config_id = $data['config_id'];
        $this->ways_source = $data['ways_source'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function insert()
    {
        try {
            $is_set = 0;
            $order = '';
            $MemberTpl = MemberTpl::where('store_id', $this->store_id)
                ->select('tpl_status')
                ->first();
            if ($MemberTpl && $MemberTpl->tpl_status == 1) {
                $is_set = 1;

                $day = date('Ymd', time());
                $table = 'orders_' . $day;
                if (Schema::hasTable($table)) {
                    $order = DB::table($table)->where('out_trade_no', $this->out_trade_no)
                        ->where('store_id', $this->store_id)
                        ->where('config_id', $this->config_id)
                        ->where('ways_type', $this->ways_type)
                        ->where('ways_source', $this->ways_source)
                        ->select('buyer_id', 'total_amount', 'merchant_id')
                        ->first();
                } else {
                    $order = Order::where('out_trade_no', $this->out_trade_no)
                        ->where('store_id', $this->store_id)
                        ->where('config_id', $this->config_id)
                        ->where('ways_type', $this->ways_type)
                        ->where('ways_source', $this->ways_source)
                        ->select('buyer_id', 'total_amount', 'merchant_id')
                        ->first();
                }
            }

            //商户设置过会员而且有订单数据
            if ($is_set && $order) {
                $buyer_id = $order->buyer_id;
                $merchant_id = $order->merchant_id;
                $total_amount = $order->total_amount;
                $MemberList = "";
                $ali_user_id = "";
                $wx_openid = "";
                $xcx_openid = "";

//                if ($this->ways_source == "alipay") {
//                    $ali_user_id = $buyer_id;
//                    $MemberList = MemberList::where('store_id', $this->store_id)
//                        ->where('ali_user_id', $buyer_id)
//                        ->first();
//                }
//
//                if ($this->ways_source == "weixin") {
//                    $wx_openid = $buyer_id;
//                    $MemberList = MemberList::where('store_id', $this->store_id)
//                        ->where('wx_openid', $buyer_id)
//                        ->first();
//                }

                $MemberList = MemberList::where('store_id', $this->store_id)
                    ->where('ali_user_id', $buyer_id)
                    ->first();
                if ($MemberList) {
                    $ali_user_id = $buyer_id;
                } else {
                    $MemberList = MemberList::where('store_id', $this->store_id)
                        ->where('wx_openid', $buyer_id)
                        ->first();
                    if ($MemberList) {
                        $wx_openid = $buyer_id;
                    } else {
                        $MemberList = MemberList::where('store_id', $this->store_id)
                            ->where('xcx_openid', $buyer_id)
                            ->first();
                        if ($MemberList) {
                            $xcx_openid = $buyer_id;
                        }
                    }
                }

                //积分赠送情况
                $MemberSetJf = MemberSetJf::where('store_id', $this->store_id)
                    ->first();

                $new_mb_jf = 0;
                $up_mb_jf = 0;
                if ($MemberSetJf) {
                    $xf = $MemberSetJf->xf;
                    $xf_s_jf = $MemberSetJf->xf_s_jf;
                    $xf_s_jf = floor(($total_amount / $xf)) * $xf_s_jf;
                    $new_mb_s_jf = $MemberSetJf->new_mb_s_jf;
                    $new_mb_jf = $total_amount >= $xf ? $new_mb_s_jf + $xf_s_jf : $new_mb_s_jf;
                    $up_mb_jf = $total_amount >= $xf ? $xf_s_jf : 0;
                }

                //会员存在 根据规则更新积分 交易数据等
                if ($MemberList) {
                    $updata = [
                        'mb_jf' => $MemberList->mb_jf + $up_mb_jf,
                        'pay_counts' => $MemberList->pay_counts + 1,
                        'pay_moneys' => $MemberList->pay_moneys + $total_amount,
                    ];
                    $MemberList->update($updata);
                    $MemberList->save();

                    //积分记录入库
                    if ($up_mb_jf > 0) {
                        $insert_data = [
                            'store_id' => $this->store_id,
                            'mb_id' => $MemberList->mb_id,
                            'jf' => $up_mb_jf,
                            'jf_desc' => '消费送积分',
                            'type' => '2',
                            'type_desc' => "消费送积分",
                        ];

                        MemberJf::create($insert_data);
                    }
                } else {
                    //会员不存在开卡会员
                    $mb_id = date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));

                    $in_sert_data = [
                        'store_id' => $this->store_id,
                        'config_id' => $this->config_id,
                        'merchant_id' => $merchant_id,
                        'mb_id' => $mb_id,
                        'mb_status' => '1',
                        'mb_ver' => '1',
                        'mb_ver_desc' => '普通会员',
                        'mb_name'=>"未设置",
                        'mb_phone'=>"未绑定",
                        'mb_nick_name'=>"未设置",
                        'mb_logo' => url('/member/mrtx.png'),
                        'mb_jf' => $new_mb_jf,
                        'mb_money' => '0.00',//储值金额
                        'mb_time' => date('Y-m-d H:i:s', time()),
                        'pay_counts' => 1,
                        'pay_moneys' => $total_amount,
                        'wx_openid' => $wx_openid,
                        'ali_user_id' => $ali_user_id,
                        'xcx_openid' => $xcx_openid,
                    ];
                    //MemberList::create($in_sert_data);

                    //积分记录入库
                    if ($new_mb_jf > 0) {
                        $insert_data = [
                            'store_id' => $this->store_id,
                            'mb_id' => $mb_id,
                            'jf' => $new_mb_jf,
                            'jf_desc' => '新会员送积分',
                            'type' => '1',
                            'type_desc' => "新会员送积分",

                        ];

                        MemberJf::create($insert_data);
                    }
                }
            }
        } catch (\Exception $ex) {
            \Illuminate\Support\Facades\Log::info('会员卡入库-error');
            \Illuminate\Support\Facades\Log::info($ex->getMessage().' | '.$ex->getFile().' | '.$ex->getLine());
        }
    }


}
