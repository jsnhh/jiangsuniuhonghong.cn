<?php

namespace App\Common;


use App\Models\MemberCzList;
use App\Models\MemberList;
use App\Models\Order;
use App\Models\User;
use App\Models\UserWalletDetail;

class CzUpdateStatus
{

    public $store_id;//门店
    public $out_trade_no;//订单号


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->store_id = $data['store_id'];
        $this->other_no = isset($data['other_no']) ? $data['other_no'] : "";


    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function insert()
    {
        try {

            $MemberCzList = MemberCzList::where('out_trade_no', $this->other_no)
                ->where('store_id', $this->store_id)
                ->first();


            if ($MemberCzList) {
                $mb_id = $MemberCzList->mb_id;
                $MemberCzList->pay_status = 1;
                $MemberCzList->pay_time = date('Y-m-d H:i:s', time());
                $MemberCzList->save();

                $MemberList = MemberList::where('store_id', $this->store_id)
                    ->where('mb_id', $mb_id)
                    ->first();

                if ($MemberList) {
                    $MemberList->mb_money = ($MemberList->mb_money + $MemberCzList->cz_money + $MemberCzList->cz_s_money);
                    $MemberList->save();
                }


            }


        } catch (\Exception $exception) {
            \Illuminate\Support\Facades\Log::info($exception);
        }
    }


}
