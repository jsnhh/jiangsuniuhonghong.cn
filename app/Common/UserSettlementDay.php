<?php

namespace App\Common;


use App\Models\SettlementList;
use App\Models\User;
use App\Models\UserWalletDetail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserSettlementDay
{

    public function settlementDay($data)
    {
        try {
            //开启事务
            try {
                $settlementList = SettlementList::where('out_trade_no', $data['out_trade_no'])->select('out_trade_no')->first();
                if ($settlementList) {
                    return;
                }
                DB::beginTransaction();
                //总表结算
                $user = User::where('is_delete', '0')
                    ->where('id', $data['user_id'])
                    ->select('id', 'money', 'name', 'config_id')->first();
                if (!$user) {
                    return;
                }
                //用户金额结算
                $money = $user->money + $data['day_money'];
                $current_time = date('Y-m-d H:i:s', time());
                User::where('id', $user->id)->update(['money' => $money]);
                $settlementDate = [
                    'user_id' => $user->id,
                    'config_id' => $user->config_id,
                    'user_name' => $user->name,
                    's_time' => $current_time,
                    'e_time' => $current_time,
                    'total_amount' => $data['total_amount'],
                    'commission_amount' => $data['day_money'],
                    'rate' => 0.001,
                    'out_trade_no' => $data['out_trade_no']
                ];
                SettlementList::create($settlementDate);
//                UserWalletDetail::where('user_id', $data['user_id'])
//                    ->where('settlement', '02')
//                    ->where('out_trade_no', $data['out_trade_no'])
//                    ->update(['settlement' => '01', 'settlement_desc' => '已结算']);
                DB::commit();
            } catch (\Exception $e) {
                Log::info($e->getMessage() . ' | ' . $e->getFile() . ' | ' . $e->getLine());
                DB::rollBack();
            }
        } catch (\Exception $exception) {
            Log::info('日结结算error');
            Log::info($exception->getMessage() . ' | ' . $exception->getFile() . ' | ' . $exception->getLine());
        }
    }


}