<?php
namespace App\Common\XingPOS\Request;
/*
3.1.18获取微信刷脸凭证（sdkGetFaceAuth）
请求参数


请求地址：测试：http://sandbox.starpos.com.cn/adpservice/sdkGetFaceAuth.json
生产：https://gateway.starpos.com.cn/adpservice/sdkGetFaceAuth.json

*/

class XingStoreHuQuWeiXinShuaLianPingZheng
{
	public $pdf_type = 2;
	public $url_sandbox = 'http://sandbox.starpos.com.cn/adpservice/sdkGetFaceAuth.json';
	public $url = 'https://gateway.starpos.com.cn/adpservice/sdkGetFaceAuth.json';
	public $content = [];
	public $request_sign_field = [
	    'wxSubAppid',
	    'deviceid',
	    'rawData',
	    'stoeId',
	    'stoeNm',
	];
	public $back_sign_field = [
		'wxAppid',
		'wxMchid',
		'wxSubAppid',
		'wxSubMchid',
		'authInfo',
		'expiresIn',
	];


	public function setBizContent($data = [])
    {
		$this->content += $data;
	}


	public function getBizContent()
    {
		return $this->content;
	}


}