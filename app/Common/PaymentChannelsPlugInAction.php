<?php


namespace App\Common;


use App\Api\Controllers\BaseController;
use App\Api\Controllers\Config\AllinPayConfigController;
use App\Api\Controllers\Config\CcBankPayConfigController;
use App\Api\Controllers\Config\ConstructPayConfigController;
use App\Api\Controllers\Config\EasyPayConfigController;
use App\Api\Controllers\Config\EasySkPayConfigController;
use App\Api\Controllers\Config\FuiouConfigController;
use App\Api\Controllers\Config\HConfigController;
use App\Api\Controllers\Config\HkrtConfigController;
use App\Api\Controllers\Config\HuiPayConfigController;
use App\Api\Controllers\Config\HwcPayConfigController;
use App\Api\Controllers\Config\JdConfigController;
use App\Api\Controllers\Config\LianfuConfigController;
use App\Api\Controllers\Config\LinkageConfigController;
use App\Api\Controllers\Config\LtfConfigController;
use App\Api\Controllers\Config\MyBankConfigController;
use App\Api\Controllers\Config\PostPayConfigController;
use App\Api\Controllers\Config\QfPayConfigController;
use App\Api\Controllers\Config\TfConfigController;
use App\Api\Controllers\Config\VbillConfigController;
use App\Api\Controllers\Config\WftPayConfigController;
use App\Api\Controllers\Config\YinshengConfigController;
use App\Api\Controllers\DuoLaBao\ManageController;
use App\Api\Controllers\EasyPay\PayController;
use App\Api\Controllers\HwcPay\PayController as HwcpayPayController;
use App\Api\Controllers\Linkage\PayController as LinkagePayController;
use App\Api\Controllers\Merchant\TransactionDeductionController;
use App\Api\Controllers\MyBank\TradePayController;
use App\Api\Controllers\QfPay\PayController as QfPayController;
use App\Api\Controllers\WftPay\PayController as WftpayPayController;
use App\Api\Controllers\YinSheng\PayController as YinShengPayController;
use App\Models\EasypayStoresImages;
use App\Models\MerchantConsumerDetails;
use App\Models\MerchantRecharge;
use App\Models\MerchantStore;
use App\Models\Store;
use Illuminate\Support\Facades\Log;

class PaymentChannelsPlugInAction extends BaseController
{

    //京东收银支付宝微信等扫一扫公共部分
    public function jd_pay_public($data_insert, $data)
    {
        try {
            //读取配置
            $store_id = $data['store_id'];
            $store_pid = $data['store_pid'];
            $merchant_id = $data['merchant_id'];
            $total_amount = $data['total_amount'];
            $ways_type = $data['ways_type'];
            $store_name = $data['store_name'];
            $config_id = $data['config_id'];
            $desc = $data['ways_type_desc'];
            $title = $data['title'];
            $tg_user_id = $data['tg_user_id'];//推广员ID;
            $config = new JdConfigController();
            $jd_config = $config->jd_config($data['config_id']);
            if (!$jd_config) {
                return json_encode([
                    'status' => 2,
                    'message' => '京东配置不存在请检查配置'
                ]);
            }

            $jd_merchant = $config->jd_merchant($store_id, $store_pid);
            if (!$jd_merchant) {
                return json_encode([
                    'status' => 2,
                    'message' => '京东商户号不存在'
                ]);
            }

            $out_trade_no = $data['out_trade_no'];

            //入库参数
            $data_insert['out_trade_no'] = $out_trade_no;
            $data_insert['ways_type'] = $data['ways_type'];
            $data_insert['company'] = $data['company'];
            $data_insert['ways_type_desc'] = $data['ways_type_desc'];
            $data_insert['ways_source'] = $data['ways_source'];
            $data_insert['ways_source_desc'] = $data['ways_source_desc'];

            $insert_re = $this->insert_day_order($data_insert);
            if (!$insert_re) {
                return json_encode([
                    'status' => 2,
                    'message' => '订单未入库'
                ]);
            }

            $obj = new \App\Api\Controllers\Jd\PayController();
            $data['notify_url'] = url('/api/jd/pay_notify_url');//回调地址
            $data['request_url'] = $obj->scan_url;//请求地址;
            $data['merchant_no'] = $jd_merchant->merchant_no;
            $data['md_key'] = $jd_merchant->md_key;//
            $data['des_key'] = $jd_merchant->des_key;//
            $data['systemId'] = $jd_config->systemId;//
            $return = $obj->scan_pay($data);

            if ($return['status'] == 0) {
                return json_encode([
                    'status' => 2,
                    'message' => $return['message']
                ]);
            }

            //返回支付成功
            if ($return['status'] == 1) {
                $trade_no = $return['data']['tradeNo'];
                $channelNoSeq = isset($return['data']['channelNoSeq']) ? $return['data']['channelNoSeq'] : $out_trade_no;//条码
                $buyer_pay_amount = $return['data']['piAmount'] / 100;
                $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['payFinishTime']));
                $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

                $data_update = [
                    'trade_no' => $channelNoSeq,
                    'buyer_id' => '',
                    'buyer_logon_id' => '',
                    'status' => '1',
                    'pay_status_desc' => '支付成功',
                    'pay_status' => 1,
                    'payment_method' => '',
                    'pay_time' => $pay_time,
                    'buyer_pay_amount' => $buyer_pay_amount,//用户实际支付
                ];
                $this->update_day_order($data_update, $out_trade_no);

                //支付成功后的动作
                $data = [
                    'ways_type' => $data['ways_type'],
                    'company' => $data_insert['company'],
                    'ways_type_desc' => $data['ways_type_desc'],
                    'source_type' => '6000',//返佣来源
                    'source_desc' => '京东金融',//返佣来源说明
                    'total_amount' => $total_amount,
                    'out_trade_no' => $out_trade_no,
                    'rate' => $data_insert['rate'],
                    'fee_amount' => $data_insert['fee_amount'],
                    'merchant_id' => $merchant_id,
                    'store_id' => $store_id,
                    'user_id' => $tg_user_id,
                    'config_id' => $config_id,
                    'store_name' => $store_name,
                    'ways_source' => $data['ways_source'],
                    'pay_time' => $pay_time,
                    'no_push' => '1',//不推送
                    'no_fuwu' => '1',//不服务消息
                    'no_print' => '1',//不打印
                    //'no_v' => '1',//不小盒子播报

                ];
                PaySuccessAction::action($data);

                return json_encode([
                    'status' => 1,
                    'pay_status' => '1',
                    'message' => '支付成功',
                    'data' => [
                        'out_trade_no' => $out_trade_no,
                        'ways_type' => $ways_type,
                        'total_amount' => $total_amount,
                        'store_id' => $store_id,
                        'store_name' => $store_name,
                        'config_id' => $config_id,
                        'pay_time' => $pay_time,
                        'trade_no' => $trade_no,
                        'out_transaction_id' => $channelNoSeq,
                    ]
                ]);
            }

            //正在支付
            if ($return['status'] == 2) {
                return json_encode([
                    'status' => 9,//正在支付 app 和收钱啦 状态不一样
                    'pay_status' => '2',
                    'message' => '正在支付',
                    'data' => [
                        'out_trade_no' => $out_trade_no,
                        'ways_type' => $ways_type,
                        'total_amount' => $total_amount,
                        'store_id' => $store_id,
                        'store_name' => $store_name,
                        'config_id' => $config_id,
                    ]
                ]);
            }

            //支付失败
            if ($return['status'] == 3) {
                return json_encode([
                    'status' => 2,
                    'pay_status' => '3',
                    'message' => $return['message'],
                    'data' => [
                        'out_trade_no' => $out_trade_no,
                        'ways_type' => $ways_type,
                        'total_amount' => $total_amount,
                        'store_id' => $store_id,
                        'store_name' => $store_name,
                        'config_id' => $config_id,
                    ]
                ]);
            }

        } catch (\Exception $ex) {
            Log::info('收钱啦-京东收银-error');
            Log::info($ex->getMessage() . ' | ' . $ex->getFile() . ' | ' . $ex->getLine());
            return json_encode([
                'status' => 2,
                'message' => $ex->getMessage() . ' | ' . $ex->getLine()
            ]);
        }
    }


    //快钱收银支付宝微信等扫一扫公共部分
    public function mybank_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $remark = $data['remark'];
        $code = $data['code'];
        $type_source = $data['pay_type'];
        $out_trade_no = $data['out_trade_no'];

        $config = new MyBankConfigController();
        $mybank_merchant = $config->mybank_merchant($store_id, $store_pid);
        if (!$mybank_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '快钱不存在'
            ]);
        }
        $wx_AppId = $mybank_merchant->wx_AppId;
        $MyBankConfig = $config->MyBankConfig($data['config_id'], $wx_AppId);
        if (!$MyBankConfig) {
            return json_encode([
                'status' => 2,
                'message' => '快钱配置不存在请检查配置'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];
        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $data['type_source'] = $type_source; //alipay,weixin
        $data['out_trade_no'] = $out_trade_no;
        $data['mybank_merchant_id'] = $mybank_merchant->MerchantId;
        $data['merchant_id'] = $merchant_id;
        $data['code'] = $code;
        $data['TotalAmount'] = $total_amount * 100;//单位分
        $data['is_fq'] = 0;//0
        $data['fq_num'] = 3;//3
        $data['hb_fq_seller_percent'] = 0;//0
        $data['buydata'] = [];//数组
        $data['SettleType'] = 'T1';//T1
        $data['remark'] = $remark;
        $data['body'] = $title;
        $data['store_id'] = $store_id;
        $data['attach'] = $remark;//附加信息，原样返回。
        $data['PayLimit'] = "";//禁用方式
        $data['config_id'] = $config_id;

        $obj = new TradePayController();
        $return = $obj->TradePay($data);

        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = $return['data']['MerchantOrderNo'];//条码订单
            $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['GmtPayment']));
            $payment_method = strtolower($return['data']['Credit']);
            $buyer_id = '';
            //微信付款的id
            if ($data['type_source'] == 'weixin') {
                $buyer_id = $return['data']['SubOpenId'];
            }
            if ($data['type_source'] == 'alipay') {
                $buyer_id = $return['data']['BuyerUserId'];
            }

            $data_update = [
                'trade_no' => $trade_no,
                'buyer_id' => $buyer_id,
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => $payment_method,
                'pay_time' => $pay_time,
            ];
            $this->update_day_order($data_update, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'company' => $data_insert['company'],
                'ways_type_desc' => $data['ways_type_desc'],
                'source_type' => '3000',//返佣来源
                'source_desc' => '快钱支付',//返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'no_push' => '1',//不推送
                'no_fuwu' => '1',//不服务消息
                'no_print' => '1',//不打印
                //'no_v' => '1',//不小盒子播报


            ];
            PaySuccessAction::action($data);

            $out_transaction_id = $out_trade_no;
            if ($type_source == "weixin") {
                $out_transaction_id = $trade_no;
            }

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'out_transaction_id' => $out_transaction_id,

                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 9,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

    }


    //和融通
    public function h_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $out_trade_no = $data['out_trade_no'];

        $config = new HConfigController();
        $h_config = $config->h_config($data['config_id']);
        if (!$h_config) {
            return json_encode([
                'status' => 2,
                'message' => '和融通配置不存在请检查配置'
            ]);
        }

        $h_merchant = $config->h_merchant($store_id, $store_pid);
        if (!$h_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '和融通商户号不存在'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\Huiyuanbao\PayController();
        $data['notify_url'] = url('/api/huiyuanbao/pay_notify'); //回调地址
        $data['request_url'] = $obj->scan_url; //请求地址;
        $data['mid'] = $h_merchant->h_mid;
        $data['md_key'] = $h_config->md_key; //
        $data['orgNo'] = $h_config->orgNo; //
        $return = $obj->scan_pay($data);
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = '112121' . $return['data']['transactionId'];
            $pay_time = date('Y-m-d H:i:s', time());
            $buyer_pay_amount = $total_amount;
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $data_update = [
                'trade_no' => $trade_no,
                'buyer_id' => '',
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount,//用户实际支付
            ];
            $this->update_day_order($data_update, $out_trade_no);


            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'company' => $data_insert['company'],
                'ways_type_desc' => $data['ways_type_desc'],
                'source_type' => '9000',//返佣来源
                'source_desc' => '和融通',//返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'no_push' => '1',//不推送
                'no_fuwu' => '1',//不服务消息
                'no_print' => '1',//不打印
                //'no_v' => '1',//不小盒子播报

            ];


            PaySuccessAction::action($data);


            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'out_transaction_id' => $trade_no,
                ]
            ]);

        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 9,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

    }


    //新大陆
    public function newland_pay_public($data_insert, $data_request)
    {
        try {
            $insert_re = $this->insert_day_order($data_insert);
            if (!$insert_re) {
                return json_encode([
                    'status' => 2,
                    'message' => '订单未入库'
                ]);
            }

            $obj = new \App\Api\Controllers\Newland\PayController();
            $return = $obj->scan_pay($data_request);

            if ($return['status'] == 0) {
                return json_encode([
                    'status' => 2,
                    'message' => $return['message']
                ]);
            }

            //返回支付成功
            if ($return['status'] == 1) {
                $trade_no = $return['data']['orderNo'];
                $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['sysTime']));
                $payment_method = '';
                $buyer_id = '';

                $data_update = [
                    'trade_no' => $trade_no,
                    'buyer_id' => $buyer_id,
                    'status' => '1',
                    'pay_status_desc' => '支付成功',
                    'pay_status' => 1,
                    'payment_method' => $payment_method,
                    'pay_time' => $pay_time,
                ];
                $this->update_day_order($data_update, $data_insert['out_trade_no']);

                //支付成功后的动作
                $data = [
                    'ways_type' => $data_insert['ways_type'],
                    'company' => $data_insert['company'],
                    'ways_type_desc' => $data_insert['ways_type_desc'],
                    'source_type' => '8000',//返佣来源
                    'source_desc' => '新大陆',//返佣来源说明
                    'total_amount' => $data_insert['total_amount'],
                    'out_trade_no' => $data_insert['out_trade_no'],
                    'rate' => $data_insert['rate'],
                    'fee_amount' => $data_insert['fee_amount'],
                    'merchant_id' => $data_insert['merchant_id'],
                    'store_id' => $data_insert['store_id'],
                    'user_id' => $data_insert['user_id'],
                    'config_id' => $data_insert['config_id'],
                    'store_name' => $data_insert['store_name'],
                    'ways_source' => $data_insert['ways_source'],
                    'pay_time' => $pay_time,
                    'no_push' => '1',//不推送
                    'no_fuwu' => '1',//不服务消息
                    'no_print' => '1',//不打印
                    //'no_v' => '1',//不小盒子播报

                ];


                PaySuccessAction::action($data);


                return json_encode([
                    'status' => 1,
                    'pay_status' => '1',
                    'message' => '支付成功',
                    'data' => [
                        'out_trade_no' => $data_insert['out_trade_no'],
                        'ways_type' => $data_insert['ways_type'],
                        'total_amount' => $data_insert['total_amount'],
                        'store_id' => $data_insert['store_id'],
                        'store_name' => $data_insert['store_name'],
                        'config_id' => $data_insert['config_id'],
                        'pay_time' => $pay_time,
                        'trade_no' => $trade_no,
                        'out_transaction_id' => $trade_no,
                    ]
                ]);

            }

            //正在支付
            if ($return['status'] == 2) {
                return json_encode([
                    'status' => 9,
                    'pay_status' => '2',
                    'message' => '正在支付',
                    'data' => [
                        'out_trade_no' => $data_insert['out_trade_no'],
                        'ways_type' => $data_insert['ways_type'],
                        'total_amount' => $data_insert['total_amount'],
                        'store_id' => $data_insert['store_id'],
                        'store_name' => $data_insert['store_name'],
                        'config_id' => $data_insert['config_id'],
                        'ways_source' => $data_insert['ways_source'],
                    ]
                ]);
            }

            //支付失败
            if ($return['status'] == 3) {
                return json_encode([
                    'status' => 2,
                    'pay_status' => '3',
                    'message' => '支付失败',
                    'data' => [
                        'out_trade_no' => $data_insert['out_trade_no'],
                        'ways_type' => $data_insert['ways_type'],
                        'total_amount' => $data_insert['total_amount'],
                        'store_id' => $data_insert['store_id'],
                        'store_name' => $data_insert['store_name'],
                        'config_id' => $data_insert['config_id'],
                    ]
                ]);
            }

        } catch (\Exception $exception) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => $exception->getMessage() . ' | ' . $exception->getLine()
            ]);
        }
    }


    //联拓富收银支付宝微信等扫一扫公共部分
    public function ltf_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $out_trade_no = $data['out_trade_no'];

        $config = new LtfConfigController();
        $ltf_merchant = $config->ltf_merchant($store_id, $store_pid);
        if (!$ltf_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '商户号不存在'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\Ltf\PayController();
        $data['notify_url'] = url('/api/ltf/pay_notify_url');//回调地址
        $data['request_url'] = $obj->scan_url;//请求地址;
        $data['merchant_no'] = $ltf_merchant->merchantCode;
        $data['appId'] = $ltf_merchant->appId;//
        $data['key'] = $ltf_merchant->md_key;//
        $return = $obj->scan_pay($data);
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = $return['data']['outTransactionId'];
            $buyer_id = $return['data']['buyerId'];
            $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['payTime']));
            $buyer_pay_amount = $return['data']['receiptAmount'];
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $data_update = [
                'trade_no' => $trade_no,
                'buyer_id' => $buyer_id,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount,//用户实际支付
            ];

            $this->update_day_order($data_update, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '10000',//返佣来源
                'source_desc' => '联拓富',//返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,

            ];


            PaySuccessAction::action($data);


            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                    'out_transaction_id' => $trade_no,


                ]
            ]);

        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 9,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,

                ]
            ]);
        }

    }


    //随行付收银支付宝微信等扫一扫公共部分
    public function vbill_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $out_trade_no = $data['out_trade_no'];
        $rate = $data_insert['rate'] ?? 0;

        $config = new VbillConfigController();
        $vbill_config = $config->vbill_config($data['config_id']);
        if (!$vbill_config) {
            return json_encode([
                'status' => 2,
                'message' => '随行付配置不存在请检查配置'
            ]);
        }

        $vbill_merchant = $config->vbill_merchant($store_id, $store_pid);
        if (!$vbill_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '随行付商户号不存在'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];
        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\Vbill\PayController();
        $data['notify_url'] = url('/api/vbill/pay_notify_url'); //回调地址
        $data['request_url'] = $obj->scan_url; //请求地址;
        $data['mno'] = $vbill_merchant->mno;
        $data['privateKey'] = $vbill_config->privateKey; //
        $data['sxfpublic'] = $vbill_config->sxfpublic; //
        $data['orgId'] = $vbill_config->orgId; //
        $return = $obj->scan_pay($data);
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = $return['data']['respData']['sxfUuid'];
            $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['respData']['payTime']));
            $buyer_pay_amount = $return['data']['respData']['totalOffstAmt']; //消费者付款金额
            $discount_amount = isset($return['data']['respData']['pointAmount']) ? $return['data']['respData']['pointAmount'] : 0; //代金券金额,积分支付金额，优惠金额或折扣券的金额
            $receipt_amount = isset($return['data']['respData']['settleAmt']) ? $return['data']['respData']['settleAmt'] : $total_amount - $discount_amount; //商家入账金额,说明：包含手续费、预充值、平台补贴（优惠），不含免充值代金券（商家补贴）
            $recFeeAmt = isset($return['data']['respData']['recFeeAmt']) ? $return['data']['respData']['recFeeAmt'] : ''; //交易手续费；单位元
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
            $buyer_id = isset($return['data']['respData']['buyerId']) ? $return['data']['respData']['buyerId'] : "";
            $promotionDetail = isset($return['data']['respData']['promotionDetail']) ? $return['data']['respData']['promotionDetail'] : ''; //优惠信息（jsonArray格式字符串）
            $promotion_name = '';
            $promotion_amount = 0.00;
            if ($promotionDetail) {
                $promotion_detail_arr = json_decode($promotionDetail, true);
                if ($promotion_detail_arr && is_array($promotion_detail_arr)) {
//                    $promotion_name = isset($promotion_detail_arr[0]['name']) ? $promotion_detail_arr[0]['name'] : ''; //优惠名称
//                    $promotion_amount = isset($promotion_detail_arr[0]['amount']) ? $promotion_detail_arr[0]['amount']: 0.00; //优惠总额；单位元，保留两位小数
                    $promotion_name = '刷脸支付天天有优惠';
                    $promotion_amount = array_sum(array_column($promotion_detail_arr, 'amount'));
                }
            }

            $fee_amount = $recFeeAmt ? $recFeeAmt : round(($rate * $receipt_amount) / 100, 2);

            $data_update = [
                'trade_no' => $trade_no,
                'buyer_id' => $buyer_id,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount,
                'receipt_amount' => $receipt_amount,
                'fee_amount' => $fee_amount,
                'discount_amount' => $promotion_amount
            ];
            $this->update_day_order($data_update, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '13000',//返佣来源
                'source_desc' => '随行付',//返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'rate' => $rate,
                'fee_amount' => $fee_amount,
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : ""
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'out_transaction_id' => $trade_no,
                    'ways_source' => $data['ways_source']
                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 9,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source']
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }

    }


    //随行付a收银支付宝微信等扫一扫公共部分
    public function vbilla_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $out_trade_no = $data['out_trade_no'];
        $rate = $data_insert['rate'] ?? 0;

        $config = new VbillConfigController();
        $vbill_config = $config->vbilla_config($data['config_id']);
        if (!$vbill_config) {
            return json_encode([
                'status' => 2,
                'message' => '随行付配置不存在请检查配置'
            ]);
        }

        $vbill_merchant = $config->vbilla_merchant($store_id, $store_pid);
        if (!$vbill_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '随行付商户号不存在'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\Vbill\PayController();
        $data['notify_url'] = url('/api/vbill/pay_notify_a_url');//回调地址
        $data['request_url'] = $obj->scan_url;//请求地址;
        $data['mno'] = $vbill_merchant->mno;
        $data['privateKey'] = $vbill_config->privateKey;//
        $data['sxfpublic'] = $vbill_config->sxfpublic;//
        $data['orgId'] = $vbill_config->orgId;//

//        Log::info('收钱啦-随行付a-被扫');
//        Log::info($data);
        $return = $obj->scan_pay($data);
//        Log::info('收钱啦-随行付a-被扫-结果');
//        Log::info($return);

        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = $return['data']['respData']['sxfUuid'];
            $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['respData']['payTime']));
            $buyer_pay_amount = $return['data']['respData']['totalOffstAmt']; //消费者付款金额
            $discount_amount = isset($return['data']['respData']['pointAmount']) ? $return['data']['respData']['pointAmount'] : 0; //代金券金额,积分支付金额，优惠金额或折扣券的金额
            $receipt_amount = isset($return['data']['respData']['settleAmt']) ? $return['data']['respData']['settleAmt'] : $total_amount - $discount_amount; //商家入账金额,说明：包含手续费、预充值、平台补贴（优惠），不含免充值代金券（商家补贴）
            $recFeeAmt = isset($return['data']['respData']['recFeeAmt']) ? $return['data']['respData']['recFeeAmt'] : ''; //交易手续费；单位元
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
            $buyer_id = isset($return['data']['respData']['buyerId']) ? $return['data']['respData']['buyerId'] : "";
            $promotionDetail = isset($return['data']['respData']['promotionDetail']) ? $return['data']['respData']['promotionDetail'] : ''; //优惠信息（jsonArray格式字符串）
            $promotion_name = '';
            $promotion_amount = 0.00;
            if ($promotionDetail) {
                $promotion_detail_arr = json_decode($promotionDetail, true);
                if ($promotion_detail_arr && is_array($promotion_detail_arr)) {
//                    $promotion_name = isset($promotion_detail_arr[0]['name']) ? $promotion_detail_arr[0]['name'] : ''; //优惠名称
//                    $promotion_amount = isset($promotion_detail_arr[0]['amount']) ? $promotion_detail_arr[0]['amount']: 0.00; //优惠总额；单位元，保留两位小数
                    $promotion_name = '刷脸支付天天有优惠';
                    $promotion_amount = array_sum(array_column($promotion_detail_arr, 'amount'));
                }
            }

            $fee_amount = $recFeeAmt ? $recFeeAmt : round(($rate * $receipt_amount) / 100, 2);

            $data_update = [
                'trade_no' => $trade_no,
                'buyer_id' => $buyer_id,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount,
                'receipt_amount' => $receipt_amount,
                'fee_amount' => $fee_amount,
                'discount_amount' => $promotion_amount
            ];
            $this->update_day_order($data_update, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '19000',//返佣来源
                'source_desc' => '随行付A',//返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'rate' => $rate,
                'fee_amount' => $fee_amount,
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : ""
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'out_transaction_id' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 9,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],

                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],

                ]
            ]);
        }

    }


    //哆啦宝
    public function dlb_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $out_trade_no = $data['out_trade_no'];

        $manager = new ManageController();
        $dlb_config = $manager->pay_config($data['config_id']);
        if (!$dlb_config) {
            return json_encode([
                'status' => 2,
                'message' => '哆啦宝配置配置不存在请检查配置'
            ]);
        }

        $dlb_merchant = $manager->dlb_merchant($store_id, $store_pid);
        if (!$dlb_merchant && !empty($dlb_merchant->mch_num) && !empty($dlb_merchant->shop_num) && !empty($dlb_merchant->machine_num)) {
            return json_encode([
                'status' => 2,
                'message' => '哆啦宝配置商户未补充商户编号等信息!'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];
        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }
        $request = [
            "accessKey" => $dlb_config->access_key,
            "secretKey" => $dlb_config->secret_key,
            "agentNum" => $dlb_config->agent_num,
            "customerNum" => $dlb_merchant->mch_num,
            "authCode" => $data['code'],
            "machineNum" => $dlb_merchant->machine_num,
            "shopNum" => $dlb_merchant->shop_num,
            "requestNum" => $out_trade_no,
            "amount" => $total_amount,
            "source" => 'API',
            //"payType" => "NATIVE",
            'callbackUrl' => url('api/dlb/pay_notify')

        ];
        $return = $manager->pay_scan($request);

        if ($return['status'] == 1) {
            $scanpay_data = $return['data'];
            //下单成功
            $timewait = !empty($dlb_config) && $dlb_config->qwx_timewait > 0 && $dlb_config->qwx_timewait < 10 ? $dlb_config->qwx_timewait : 3;
            sleep($timewait);
            $query_data = [
                "accessKey" => $dlb_config->access_key,
                "secretKey" => $dlb_config->secret_key,
                "agentNum" => $dlb_config->agent_num,
                "customerNum" => $dlb_merchant->mch_num,
                "shopNum" => $dlb_merchant->shop_num,
                "requestNum" => $out_trade_no
            ];
            $return = $manager->query_bill($query_data);

            if ($return['status'] == 1) {
                $query_result = $return['data'];
                if ($query_result['status'] == "SUCCESS") {
                    $pay_time = $query_result['completeTime'];
                    $trade_no = $query_result['orderNum'];
                    $buyer_pay_amount = $query_result['orderAmount'];

                    $data_update = [
                        'trade_no' => $trade_no,
                        'buyer_id' => '',
                        'buyer_logon_id' => '',
                        'status' => '1',
                        'pay_status_desc' => '支付成功',
                        'pay_status' => 1,
                        'payment_method' => '',
                        'pay_time' => $pay_time,
                        'buyer_pay_amount' => $buyer_pay_amount,//用户实际支付
                    ];
                    $this->update_day_order($data_update, $out_trade_no);

                    //支付成功后的动作
                    $data = [
                        'ways_type' => $data['ways_type'],
                        'ways_type_desc' => $data['ways_type_desc'],
                        'source_type' => '15000',//返佣来源
                        'source_desc' => '哆啦宝',//返佣来源说明
                        'company' => 'dlb',
                        'fee_amount' => $data_insert['fee_amount'],
                        'total_amount' => $total_amount,
                        'out_trade_no' => $out_trade_no,
                        'rate' => $data_insert['rate'],
                        'merchant_id' => $merchant_id,
                        'store_id' => $store_id,
                        'user_id' => $tg_user_id,
                        'config_id' => $config_id,
                        'store_name' => $store_name,
                        'ways_source' => $data['ways_source'],
                        'pay_time' => $pay_time,
                        'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
                    ];
                    PaySuccessAction::action($data);

                    $return = [
                        'status' => 1,
                        'pay_status' => '1',
                        'message' => '支付成功',
                        'data' => [
                            'out_trade_no' => $out_trade_no,
                            'ways_type' => $ways_type,
                            'total_amount' => $total_amount,
                            'store_id' => $store_id,
                            'store_name' => $store_name,
                            'config_id' => $config_id,
                            'pay_time' => $pay_time,
                            'trade_no' => $trade_no,
                            'out_transaction_id' => $trade_no,
                            'ways_source' => $data['ways_source']
                        ]
                    ];
                } elseif ($query_result['status'] == "INIT") {
                    $return = [
                        'status' => 9,
                        'pay_status' => '2',
                        'message' => '正在支付',
                        'data' => [
                            'out_trade_no' => $out_trade_no,
                            'ways_type' => $ways_type,
                            'total_amount' => $total_amount,
                            'store_id' => $store_id,
                            'store_name' => $store_name,
                            'config_id' => $config_id
                        ]
                    ];
                } elseif ($query_result['status'] == "CANCEL") {
                    $return = [
                        'status' => 2,
                        'pay_status' => '3',
                        'message' => '支付失败',
                        'data' => [
                            'out_trade_no' => $out_trade_no,
                            'ways_type' => $ways_type,
                            'total_amount' => $total_amount,
                            'store_id' => $store_id,
                            'store_name' => $store_name,
                            'config_id' => $config_id
                        ]
                    ];
                }
            }
        }
        return json_encode($return);
    }


    //传化
    public function tf_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $channel = $data['channel'];
        $out_trade_no = $data['out_trade_no'];

        $config = new TfConfigController();
        $h_merchant = $config->tf_merchant($store_id, $store_pid);
        if (!$h_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '传化商户号不存在'
            ]);
        }

        $h_config = $config->tf_config($data['config_id'], $h_merchant->qd);
        if (!$h_config) {
            return json_encode([
                'status' => 2,
                'message' => '传化配置不存在请检查配置'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];
        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\Tfpay\PayController();
        $data['mch_id'] = $h_config->mch_id;//
        $data['pub_key'] = $h_config->pub_key;//
        $data['pri_key'] = $h_config->pri_key;//
        $data['sub_mch_id'] = $h_merchant->sub_mch_id;//
        $data['notify_url'] = url('/api/tfpay/notify_url');
        $return = $obj->scan_pay($data);
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = $return['data']['channel_no'];
            $user_info = $return['data']['user_info'];
            $pay_time = date('Y-m-d H:i:s', time());
            $buyer_pay_amount = $total_amount;
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $data_update = [
                'trade_no' => $trade_no,
                'buyer_id' => $user_info,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount,//用户实际支付
            ];

            $this->update_day_order($data_update, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '12000',//返佣来源
                'source_desc' => 'TF',//返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",


            ];


            PaySuccessAction::action($data);


            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                    'out_transaction_id' => $trade_no,
                ]
            ]);


        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 9,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

    }

    //银盛 被扫 公共
    public function yinsheng_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $out_trade_no = $data['out_trade_no'];
        $code = $data['code'];
        $ways_source = $data['ways_source'];
        $device_id = $data['device_id'] ?? ''; //可空,终端设备号.注:中国银联时,为必填

        $config = new YinshengConfigController();
        $yinsheng_config = $config->yinsheng_config($data['config_id']);
        if (!$yinsheng_config) {
            return json_encode([
                'status' => 2,
                'message' => '银盛支付配置不存在,请检查配置'
            ]);
        }

        $yinsheng_merchant = $config->yinsheng_merchant($store_id, $store_pid);
        if (!$yinsheng_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '银盛支付,商户号不存在'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '银盛支付,订单未入库'
            ]);
        }
        $bus_open_type = $yinsheng_merchant->bus_open_type;
        $openTypeArr = explode('|', $bus_open_type);
        $business_code = '';
        foreach ($openTypeArr as $openType) {
            if ($openType == '00') { //00-扫码工作日到账
                $business_code = '00510030';
            }
            if ($openType == '01') { //01-扫码实时到账 00510080
                $business_code = '00510030';
            }
            if ($openType == '20') { //20-扫码次日到账
                $business_code = '00510030';
            }
        }
        $obj = new \App\Api\Controllers\BaseController();
        $data['partner_id'] = $yinsheng_config->partner_id; //商户号
        $data['out_trade_no'] = $out_trade_no; //商户生成的订单号,生成规则前8位必须为交易日期,如20180525,范围跨度支持包含当天在内的前后一天,且只能由大小写英文字母,数字,下划线及横杠组成
        $data['ways_source'] = $ways_source;
        $data['body'] = $store_name; //商品的标题,交易标题,订单标题,订单关键字等,最长为250个汉字
        $data['total_amount'] = $total_amount; //该笔订单的资金总额,单位为RMB-元.取值范围为[0.01,100000000.00],精确到小数点后两位
        //$data['store_name'] = $yinsheng_merchant->seller_name; //收款方银盛支付客户名(注册时公司名称)
        $data['business_code'] = $business_code; //业务代码
        $data['seller_id'] = $yinsheng_merchant->mer_code; //商户号
        $data['code'] = $code; //扫码支付授权码,设备读取用户展示的条码或者二维码信息
        $data['device_id'] = $device_id; //可空,终端设备号.注:当bank_type域为中国银联-9001002时,为必填
        Log::info('银盛支付-微收银-被扫-入参');
        Log::info($data);
        $return = $obj->scan_pay($data); //-1系统错误 1-交易成功 2-失败 3-等待买家付款 4-客户主动关闭订单 5-交易正在处理中 6-部分退款成功 7-全部退款成功
        Log::info('银盛支付-微收银-被扫-结果');
        Log::info($return);

        $trade_no = isset($return['data']['trade_no']) ? $return['data']['trade_no'] : $return['data']['trade_no']; //第三方商户单号，可在支持的商户扫码退款
        $other_no = isset($return['data']['channel_send_sn']) ? $return['data']['channel_send_sn'] : ''; //第三方订单号
        $pay_time = isset($return['data']['pay_success_time']) ? $return['data']['pay_success_time'] : date('Y-m-d H:i:s', time());
        $buyer_pay_amount = isset($return['data']['total_amount']) ? $return['data']['total_amount'] : $return['data']['total_amount']; //实收金额，单位为元，两位小数。该金额为本笔交易，商户账户能够实际收到的金额or总金额，以分为单位
        $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
        //支付成功
        if ($return['status'] == 1) {
            $data_update = [
                'trade_no' => $trade_no,
                'other_no' => $other_no,
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '', //credit--信用卡 pcredit--花呗（仅支付宝） debit--借记卡 balance--余额 unknown--未知
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount, //用户实际支付
                'receipt_amount' => $buyer_pay_amount
            ];
            $this->update_day_order($data_update, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '14000',//返佣来源
                'source_desc' => '银盛支付',//返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => '1',
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $buyer_pay_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'out_transaction_id' => $out_trade_no,
                    'ways_source' => $ways_source,
                ]
            ]);
        } //支付失败
        elseif ($return['status'] == 2) {
            $update_data = [
                'trade_no' => $trade_no,
                'other_no' => $other_no,
            ];
            $this->update_day_order($update_data, $out_trade_no);
            return json_encode([
                'status' => '9',
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $buyer_pay_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $ways_source,
                ]
            ]);
        } else {
            return json_encode([
                'status' => '2',
                'pay_status' => 3,
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $buyer_pay_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $ways_source,
                ]
            ]);
        }

    }

    //钱方 被扫 公共
    public function qf_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $out_trade_no = $data['out_trade_no'];
        $code = $data['code'];
        $ways_source = $data['ways_source'];
        $device_id = $data['device_id'] ?? ''; //终端设备号

        $QfPayConfigControllerObj = new QfPayConfigController();
        $qfPayConfig = $QfPayConfigControllerObj->qfpay_config($data['config_id']);
        if (!$qfPayConfig) {
            return json_encode([
                'status' => 2,
                'message' => '钱方支付配置不存在,请检查配置'
            ]);
        }

        $qfPayMerchant = $QfPayConfigControllerObj->qfpay_merchant($store_id, $store_pid);
        if (!$qfPayMerchant) {
            return json_encode([
                'status' => 2,
                'message' => '钱方支付,商户号不存在'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '钱方支付,订单未入库'
            ]);
        }

        $obj = new QfPayController();
        $reqData = [
            'ways_source' => $ways_source,  //支付类型,alipay-支付宝;weixin-微信;jd-京东;unionpay-银联
            'key' => $qfPayConfig->key,  //加签key
            'code' => $qfPayConfig->code,  //开发唯一标识
            'total_amount' => $total_amount,  //订单支付金额
            'mchid' => $qfPayMerchant->mchid,  //子商户号,标识子商户身份,由钱方分配(渠道系统后台查看对应商户(非业务员)子商户号,被视为对应商户的交易)
            'out_trade_no' => $out_trade_no,  //外部订单号,开发者平台订单号,同子商户(mchid)下,每次成功调用支付(含退款)接口下单,该参数值均不能重复使用,保证单号唯一,长度不超过128字符
            'auth_code' => $code,  //微信或者支付宝的授权码
            'pay_type' => 800008  //支付类型: 支付宝扫码-800101; 支付宝反扫-800108; 支付宝服务窗支付-800107;;微信扫码-800201; 微信反扫-800208; 微信公众号支付-800207; 微信小程序支付-800213;QQ扫码-800601; QQ反扫-800608(暂不可用);;云闪付扫码-800701; 云闪付反扫-800708;;统一聚合反扫-800008
        ];
        Log::info('钱方支付-微收银-被扫-入参');
        Log::info($reqData);
        $return = $obj->payment($reqData); //1-成功 3-交易中
        Log::info('钱方支付-微收银-被扫-结果');
        Log::info($return);

        //支付成功
        if ($return['status'] == 1) {
            $trade_no = isset($return['data']['syssn']) ? $return['data']['syssn'] : ''; //钱方订单号
            $txamt = isset($return['data']['txamt']) ? $return['data']['txamt'] : 0; //订单支付金额，单位分
            $txamt = number_format($txamt / 100, 2, '.', '');
            $pay_time = isset($return['data']['txdtm']) ? $return['data']['txdtm'] : ''; //请求交易时间,格式为：YYYY-MM-DD HH:MM:SS
            $sysdtm = isset($return['data']['sysdtm']) ? $return['data']['sysdtm'] : $pay_time; //系统时间
            $other_no = isset($return['data']['out_trade_no']) ? $return['data']['out_trade_no'] : ''; //外部订单号，开发者平台订单号
            $pay_type = isset($return['data']['pay_type']) ? $return['data']['pay_type'] : ''; //支付类型:微信反扫:800208；支付宝反扫:800108;云闪付反扫:800708;QQ反扫:800608（暂不可用）;统一聚合反扫-800008
            $resperr = isset($return['data']['resperr']) ? $return['data']['resperr'] : ''; //信息描述
            $txcurrcd = isset($return['data']['txcurrcd']) ? $return['data']['txcurrcd'] : ''; //币种:港币-HKD；人民币-CNY
            $respmsg = isset($return['data']['respmsg']) ? $return['data']['respmsg'] : ''; //调试信息
            $respcd = isset($return['data']['respcd']) ? $return['data']['respcd'] : ''; //交易返回码0000表示交易支付成功；1143、1145表示交易中，需继续查询交易结果； 其他返回码表示交易失败

            $data_update = [
                'trade_no' => $trade_no,
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $sysdtm,
                'buyer_pay_amount' => $txamt,
                'receipt_amount' => $txamt
            ];
            $this->update_day_order($data_update, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '24000', //返佣来源
                'source_desc' => '钱方支付', //返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $sysdtm,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $sysdtm,
                    'trade_no' => $trade_no,
                    'out_transaction_id' => $trade_no,
                    'ways_source' => $data['ways_source']
                ]
            ]);
        } elseif ($return['status'] == 3) { //交易中,建议调用查询接口
            return json_encode([
                'status' => 9,
                'pay_status' => 2,
                'message' => '交易中,请查询交易结果确认',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source']
                ]
            ]);
        } else {
            return json_encode([
                'status' => $return['status'],
                'message' => $return['message']
            ]);
        }

    }

    //汇付收银,支付宝微信 b_扫_c 公共部分
    public function huipay_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $config = new HuiPayConfigController();
        $hui_pay_config = $config->hui_pay_config($data['config_id']);
        if (!$hui_pay_config) {
            return json_encode([
                'status' => 2,
                'message' => '汇付配置不存在请检查配置'
            ]);
        }

        $hui_pay_merchant = $config->hui_pay_merchant($store_id, $store_pid);
        if (!$hui_pay_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '汇付商户不存在'
            ]);
        }
        if (!$hui_pay_merchant->mer_id) {
            return json_encode([
                'status' => 2,
                'message' => '汇付商户号不存在'
            ]);
        }
        if (!$hui_pay_merchant->user_cust_id) {
            return json_encode([
                'status' => 2,
                'message' => '汇付用户客户号不存在'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
//        $insert_re = self::insert_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\HuiPay\PayController();
        $data['notify_url'] = url('/api/huipay/pay_notify_url'); //回调地址
        $data['request_url'] = $obj->back_scan; //反扫请求地址
        $data['mer_cust_id'] = $hui_pay_config->mer_cust_id; //商户客户号
        $data['user_cust_id'] = $hui_pay_merchant->user_cust_id; //用户客户号
        $data['private_key'] = $hui_pay_config->private_key;
        $data['public_key'] = $hui_pay_config->public_key;
        $data['org_id'] = $hui_pay_config->org_id; //机构号,渠道来源
        $data['device_id'] = $hui_pay_merchant->device_id ?? $data['device_id']; //机具id
//        Log::info('汇付b_扫_c: '.str_replace("\\/", "/", json_encode($data, JSON_UNESCAPED_UNICODE)));
        $return = $obj->scan_pay($data);
//        Log::info('汇付b_扫_c接口返回: '.str_replace("\\/", "/", json_encode($return, JSON_UNESCAPED_UNICODE)));
        //{"status":1,"message":"成功","data":{"bank_code":"SUCCESS","bank_message":"交易成功","cust_id":"6666000001036124","device_id":"SMSM34358719666880610","extension":"","fee_amt":"0.00","mer_priv":"","order_id":"wxscan20200109153908824837706","out_trans_id":"4200000488202001097182035737","party_order_id":"MCS00000012001091539097mg9ygiwVx","pay_type":"W0","platform_seq_id":"84086417201823744","service_amt":"0.00","trans_amt":"0.01","trans_date":"20200109","trans_time":"153909"}}
        //验签失败
        if ($return['status'] === '0') {
            return json_encode([
                'status' => '0',
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trans_date = isset($return['data']['trans_date']) ? date('Y-m-d', strtotime($return['data']['trans_date'])) : '';
            $trans_time = isset($return['data']['trans_time']) ? date('H:i:s', strtotime($return['data']['trans_time'])) : '';
            $trade_no = $return['data']['party_order_id'] ?? ''; //渠道支付凭证
            $pay_time = $trans_date . ' ' . $trans_time;
            $buyer_pay_amount = $return['data']['trans_amt'] ?? ''; //交易金额
//            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
//            $buyer_id = $return['cust_id']; //支付用户id
            $data_update = [
                'trade_no' => $trade_no,
//                'buyer_id' => $buyer_id,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount,//用户实际支付
            ];

            $this->update_day_order($data_update, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '18000', //返佣来源
                'source_desc' => '汇付', //返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
//                'other_no' => $data_insert['other_no'],
                'rate' => $data_insert['rate'],
                'fee_amount' => $return['data']['fee_amt'] ?? $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];

            PaySuccessAction::action($data);

            $test345 = [
                'out_trade_no' => $out_trade_no,
                'ways_type' => $ways_type,
                'total_amount' => $total_amount,
                'store_id' => $store_id,
                'store_name' => $store_name,
                'config_id' => $config_id,
                'pay_time' => $pay_time,
                'trade_no' => $trade_no,
                'ways_source' => $data['ways_source']
            ];
//            Log::info('汇付反扫返回值：'.str_replace("\\/", "/", json_encode($test345, JSON_UNESCAPED_UNICODE)));
            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                    'out_transaction_id' => $trade_no,
                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 1,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }
    }


    //工行
    public function lianfu_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $tg_user_id = $data['tg_user_id'];
        $out_trade_no = $data['out_trade_no'];

        $config = new LianfuConfigController();
        $h_merchant = $config->lianfu_merchant($store_id, $store_pid);
        if (!$h_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '工行商户号不存在'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\lianfu\PayController();
        $data['apikey'] = $h_merchant->apikey;//
        $data['signkey'] = $h_merchant->signkey;//
        $return = $obj->scan_pay($data);
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = $out_trade_no;
            $user_info = '';
            $pay_time = date('Y-m-d H:i:s', time());
            $buyer_pay_amount = $total_amount;
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $data_update = [
                'trade_no' => $trade_no,
                'buyer_id' => $user_info,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount,//用户实际支付
            ];
            $this->update_day_order($data_update, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '20000', //返佣来源
                'source_desc' => 'lianfu', //返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                    'out_transaction_id' => $trade_no
                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 9,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

    }


    //海科融通 被扫 公共
    public function hkrt_pay_public($data_insert, $data)
    {
        $ways_type_desc = $data['ways_type_desc'] ?? '';
        $ways_source_desc = $data['ways_source_desc'] ?? '';
        $code = $data['code'] ?? '';
        $out_trade_no = $data['out_trade_no'] ?? '';
        $config_id = $data['config_id'] ?? '';
        $store_id = $data['store_id'] ?? '';
        $store_pid = $data['store_pid'] ?? '';
        $ways_type = $data['ways_type'] ?? '';
        $ways_source = $data['ways_source'] ?? '';
        $company = $data['company'] ?? '';
        $total_amount = $data['total_amount'] ?? '';
        $remark = $data['remark'] ?? '';
        $device_id = $data['device_id'] ?? '';
        $shop_name = $data['shop_name'] ?? '';
        $merchant_id = $data['merchant_id'] ?? '';
        $store_name = $data['store_name'] ?? '';
        $tg_user_id = $data['tg_user_id'] ?? '';

        $config = new HkrtConfigController();
        $hkrt_config = $config->hkrt_config($config_id);
        if (!$hkrt_config) {
            return json_encode([
                'status' => 2,
                'message' => '海科融通配置不存在请检查配置'
            ]);
        }

        $hkrt_merchant = $config->hkrt_merchant($store_id, $store_pid);
        if (!$hkrt_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '海科融通商户号不存在'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $ways_type;
        $data_insert['ways_type_desc'] = $ways_type_desc;
        $data_insert['company'] = $company;
        $data_insert['ways_source'] = $ways_source;
        $data_insert['ways_source_desc'] = $ways_source_desc;
        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\Hkrt\PayController();
        $hkrt_data = [
            'access_id' => $hkrt_config->access_id,
            'merch_no' => $hkrt_merchant->merch_no,
            'out_trade_no' => $out_trade_no,
            'total_amount' => $total_amount,
            'code' => $code,
            'notify_url' => url('/api/hkrt/pay_notify_url'), //回调地址
//            'sn' => '', //厂商终端号
//            'pn' => '', //SAAS终端号,标准服务商必填
            'remark' => $remark,
//            'limit_pay' => '0', //0-不限制贷记卡支付;1-禁止使用贷记卡支付。limit_pay默认为0
//            'receive_no' => '', //收账方(海科商户号)ledger_relation组内参数
//            'amt' => '', //分账金额（单位：元）
//            'goods_name' => '' //商品名称(仅限支付宝微信被扫，银联二维码上送字段也不会被使用)
            'access_key' => $hkrt_config->access_key
        ];
        Log::info('收钱啦-海科融通-被扫');
        Log::info($hkrt_data);
//        $hkrt_data['sign'] = $obj->getSignContent($hkrt_data, $hkrt_config->access_key);
//        Log::info($hkrt_data['sign']);
        $return = $obj->scan_pay($hkrt_data); //0-系统错误 1-成功 2-失败 3-交易进行中
        if ($return['status'] == '0') {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == '1') {
            //            $return['data'] = '{
            //                "out_trade_no":"aliscan20200528103800950648811",
            //                "channel_trade_no":"011220052810381308990MC",
            //                "trade_no":"AL200528103801605753925627",
            //                "trade_status":1,
            //                "trade_type":"ALI",
            //                "trade_end_time":"2020-05-28 10:38:03",
            //                "userid":"2088012904744331",
            //                "trade_channel_end_time":"2020-05-28 10:38:02",
            //                "return_code":10000,
            //                "sign":"31E7A6AD0908910C0067F5402A4BFBE1"
            //            }';
            $pay_time = $return['data']['trade_end_time'] ?? '';
            $trade_no = $return['data']['trade_no'] ?? '';
            $other_no = $return['data']['channel_trade_no'] ?? '';
            $payment_method = $return['data']['trade_type'] ?? '';
            $data_update = [
                'trade_no' => $trade_no,
                'other_no' => $other_no,
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => $payment_method,
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $total_amount //用户实际支付
            ];
            $this->update_day_order($data_update, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $ways_type,
                'ways_type_desc' => $ways_type_desc,
                'company' => $company,
                'source_type' => '22000', //返佣来源
                'source_desc' => '海科融通', //返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $ways_source,
                'pay_time' => $pay_time,
                'device_id' => $device_id,
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'out_transaction_id' => $out_trade_no,
                    'ways_source' => $ways_source,
                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 9,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $ways_source,
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $ways_source,
                ]
            ]);
        }

    }


    //易生支付 被扫 公共
    public function easypay_pay_public($data_insert, $data)
    {
        $ways_type_desc = $data['ways_type_desc'] ?? '';
        $ways_source_desc = $data['ways_source_desc'] ?? '';
        $code = $data['code'] ?? '';
        $config_id = $data['config_id'] ?? '';
        $store_id = $data['store_id'] ?? '';
        $store_pid = $data['store_pid'] ?? '';
        $ways_type = $data['ways_type'] ?? '';
        $ways_source = $data['ways_source'] ?? '';
        $company = $data['company'] ?? '';
        $total_amount = $data['total_amount'] ?? '';
        $remark = $data['remark'] ?? '';
        $device_id = $data['device_id'] ?? '';
        $shop_name = $data['shop_name'] ?? '';
        $merchant_id = $data['merchant_id'] ?? '';
        $store_name = $data['store_name'] ?? '';
        $tg_user_id = $data['tg_user_id'] ?? '';

        $config = new EasyPayConfigController();
        $easypay_config = $config->easypay_config($config_id);
        if (!$easypay_config) {
            return json_encode([
                'status' => '2',
                'message' => '易生支付配置不存在请检查配置'
            ]);
        }

        $easypay_merchant = $config->easypay_merchant($store_id, $store_pid);
        if (!$easypay_merchant) {
            return json_encode([
                'status' => '2',
                'message' => '易生支付商户号不存在'
            ]);
        }
        $out_trade_no = substr($easypay_config->channel_id, 0, 4) . substr($easypay_config->channel_id, -4) . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%02d', rand(0, 99));
        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $ways_type;
        $data_insert['ways_type_desc'] = $ways_type_desc;
        $data_insert['company'] = $company;
        $data_insert['ways_source'] = $ways_source;
        $data_insert['ways_source_desc'] = $ways_source_desc;
        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => '2',
                'message' => '订单未入库'
            ]);
        }

        $store = Store::where('store_id', $store_id)->first();
        $location = '';
        if (!$store->lat || !$store->lng) {
            $storeController = new \App\Api\Controllers\User\StoreController();
            $address = $store->province_name . $store->city_name . $store->area_name . $store->store_address;//获取经纬度的地址
            $api_res = $storeController->query_address($address, env('LBS_KEY'));
            if ($api_res['status'] == '1') {
                $store_lng = $api_res['result']['lng'];
                $store_lat = $api_res['result']['lat'];
                $store->update([
                    'lng' => $store_lng,
                    'lat' => $store_lat,
                ]);
                $location = "+" . $store_lat . "/-" . $store_lng;
            }

        } else {
            $location = "+" . $store->lat . "/-" . $store->lng;
        }
        //获取到账标识
        $EasypayStoresImages = EasypayStoresImages::where('store_id', $store_id)
            ->select('real_time_entry')
            ->first();

        $patnerSettleFlag = '0'; //秒到
        if ($EasypayStoresImages) {
            if ($EasypayStoresImages->real_time_entry == 1) {
                $patnerSettleFlag = '0';
            } else {
                $patnerSettleFlag = '1';
            }
        }

        $obj = new PayController();
        $easypay_data = [
            'channel_id' => $easypay_config->channel_id, //渠道编号
            'mno' => $easypay_merchant->term_mercode, //终端商户编号
            'device_id' => $easypay_merchant->term_termcode, //终端编号
            'out_trade_no' => $out_trade_no, //商户订单号
            'total_amount' => $total_amount, //交易金额
            'code' => $code,
            'shop_name' => $shop_name, //商品或支付单简要描述（给用户看）
            'location' => $location,
            'patnerSettleFlag' => $patnerSettleFlag
        ];
        Log::info('插件-易生支付-被扫');
        Log::info($easypay_data);
        $return = $obj->scan_pay($easypay_data); //-1 系统错误 0-其他 1-成功 2-待支付
        if ($return['status'] == '-1') {
            return json_encode([
                'status' => '2',
                'message' => $return['message']
            ]);
        }

//-------1.0-----
//        $pay_time = isset($return['data']['wxtimeend']) ? date('Y-m-d H:i:s', strtotime($return['data']['wxtimeend'])): ''; //支付完成时间，格式为yyyyMMddhhmmss，如2009年12月27日9点10分10秒表示为 20091227091010
//        $trade_no = $return['data']['wtorderid'] ?? ''; //系统订单号
//        $other_no = $return['data']['tradetrace'] ?? ''; //商户订单号（请求原值返回）
//        $clearamt = isset($return['data']['clearamt']) ? ($return['data']['clearamt'] / 100): ''; //结算金额，单位分
//        $payamt = isset($return['data']['payamt']) ? ($return['data']['payamt'] / 100): ''; //实付金额，单位分
//------2.0------
        $pay_time = isset($return['data']['timeEnd']) ? date('Y-m-d H:i:s', strtotime($return['data']['timeEnd'])) : date('Y-m-d H:i:m',time());
        $trade_no = $return['data']['outTrace'] ?? ''; //系统订单号
        $other_no = $return['data']['outTrace'] ?? ''; //商户订单号（请求原值返回）
        $clearamt = isset($return['data']['settleAmt']) ? ($return['data']['settleAmt'] / 100) : ''; //结算金额，单位分
        $payamt = isset($return['data']['payerAmt']) ? ($return['data']['payerAmt'] / 100) : ''; //实付金额，单位分

        //支付成功
        if ($return['status'] == '1') {
            $data_update = [
                'trade_no' => $trade_no,
                'other_no' => $other_no,
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '', //credit--信用卡 pcredit--花呗（仅支付宝） debit--借记卡 balance--余额 unknown--未知
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $payamt, //用户实际支付
                'receipt_amount' => $payamt
            ];
            $this->update_day_order($data_update, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $ways_type,
                'ways_type_desc' => $ways_type_desc,
                'company' => $company,
                'source_type' => '21000', //返佣来源
                'source_desc' => '易生支付', //返佣来源说明
                'total_amount' => $payamt,
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $ways_source,
                'pay_time' => $pay_time,
                'device_id' => $device_id,
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => '1',
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $payamt,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'out_transaction_id' => $out_trade_no,
                    'ways_source' => $ways_source,
                ]
            ]);
        }

        //待支付
        if ($return['status'] == '2') {
            return json_encode([
                'status' => '9',
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $payamt,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $ways_source,
                ]
            ]);
        }

        //其他
        if ($return['status'] == '0') {
            return json_encode([
                'status' => '2',
                'pay_status' => 3,
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $payamt,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $ways_source,
                ]
            ]);
        }

    }


    //联动优势 被扫 公共
    public function linkage_pay_public($data_insert, $data)
    {
        $orderType = $data['orderType'];
        $goodsId = $data['goodsId'];
        $title = $data['title'];
        $ways_source_desc = $data['ways_source_desc'];
        $ways_type_desc = $data['ways_type_desc'];
        $code = $data['code'];
        $out_trade_no = $data['out_trade_no'];
        $config_id = $data['config_id'];
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $ways_type = $data['ways_type'];
        $ways_source = $data['ways_source'];
        $company = $data['company'];
        $total_amount = $data['total_amount'];
        $remark = $data['remark'];
        $device_id = $data['device_id'];
        $shop_name = $data['shop_name'];
        $merchant_id = $data['merchant_id'];
        $store_name = $data['store_name'];
        $tg_user_id = $data['tg_user_id'];

        $config = new LinkageConfigController();
        $linkage_config = $config->linkage_config($data['config_id']);
        if (!$linkage_config) {
            return json_encode([
                'status' => '2',
                'message' => '联动配置不存在请检查配置'
            ]);
        }

        $linkage_merchant = $config->linkage_merchant($store_id, $store_pid);
        if (!$linkage_merchant) {
            return json_encode([
                'status' => '2',
                'message' => '联动商户号不存在'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $ways_type;
        $data_insert['ways_type_desc'] = $ways_type_desc;
        $data_insert['ways_source'] = $ways_source;
        $data_insert['company'] = $company;
        $data_insert['ways_source_desc'] = $ways_source_desc;

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => '2',
                'message' => '订单未入库'
            ]);
        }

        $obj = new LinkAgePayController();
        $linkageData['acqSpId'] = $linkage_config->mch_id; //代理商编号
        $linkageData['acqMerId'] = $linkage_merchant->acqMerId; //商户号
        $linkageData['out_trade_no'] = $out_trade_no; //商户订单号
        $linkageData['total_amount'] = $total_amount; //订单金额
        $linkageData['ways_source'] = $ways_source; //订单类型
        $linkageData['authCode'] = $code; //付款码
        $linkageData['backUrl'] = url('/api/linkage/pay_notify_url'); //O,通知地址
        $linkageData['privateKey'] = $linkage_config->privateKey; //私钥
        $linkageData['publicKey'] = $linkage_config->publicKey; //公钥

        Log::info('收钱啦-联动优势-被扫');
        Log::info($linkageData);
        $return = $obj->scan_pay($linkageData); //-1 系统错误 0-其他 1-成功 2-验签失败 3-支付中 4-交易失败
        Log::info($return);

        //支付成功
        if ($return['status'] == '1') {
//            $out_trade_no = $return['data']['orderNo'] ?? ''; //O,商户订单号
            $paySeq = $return['data']['paySeq'] ?? ''; //O,支付流水号（条形码），成功返回
            $trade_no = $return['data']['transactionId']; //联动优势的订单号
            $total_amount = isset($return['data']['txnAmt']) ? ($return['data']['txnAmt'] / 100) : 0; //订单金额
            $user_info = '';
            $pay_time = date('Y-m-d H:i:s', time());
            $buyer_pay_amount = $total_amount;
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $user_info,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status' => 1,
                'pay_status_desc' => '支付成功',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount //用户实际支付
            ];
            if ($paySeq) $update_data['auth_code'] = $paySeq;
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $ways_type,
                'ways_type_desc' => $ways_type_desc,
                'company' => $data_insert['company'],
                'source_type' => '5000', //返佣来源
                'source_desc' => 'linkage', //返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $ways_source,
                'pay_time' => $pay_time,
                'device_id' => $device_id ?? ''
            ];
            if (isset($data_insert['other_no'])) $data['other_no'] = $data_insert['other_no'];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => '1',
                'pay_status' => 1,
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $ways_source
                ]
            ]);
        } //正在支付
        elseif ($return['status'] == '3' || ($return['message'] == '用户密码输入中')) {
            sleep(15); //15秒后调用查询接口
            $linkageReturnData = [
                'acqSpId' => $linkage_config->mch_id, //代理商编号
                'privateKey' => $linkage_config->privateKey, //
                'publicKey' => $linkage_config->publicKey, //
                'acqMerId' => $linkage_merchant->acqMerId, //商户号
                'trade_no' => $trade_no ?? '', //联动优势的订单号，建议优先使用
                'out_trade_no' => $out_trade_no //商户订单号
            ];
            Log::info('联动优势-收钱啦-交易查询222');
            Log::info($linkageReturnData);
            $returnResult = $obj->order_query($linkageReturnData); //-1系统错误；0-其他；1-交易成功；2-验签失败；3-转入退款；4-交易结果未明；5-已关闭；6-已撤销(付款码支付)；7-明确支付失败
            Log::info($returnResult);
            if ($returnResult['status'] == '1') {
                $trade_no = $returnResult['data']['transactionId'] ?? '';
                $paySeq = $returnResult['data']['paySeq'] ?? '';
                $pay_time = date('Y-m-d H:i:s', strtotime(substr($returnResult['data']['platDate'], 0, 4) . $returnResult['data']['payTime']));
                $buyer_pay_amount = isset($returnResult['data']['txnAmt']) ? ($returnResult['data']['txnAmt'] / 100) : 0;
                $update_data = [
                    'status' => '1',
                    'pay_status' => 1,
                    'pay_status_desc' => '支付成功',
                    'pay_time' => $pay_time
                ];
                if ($trade_no) $update_data['trade_no'] = $trade_no;
                if ($paySeq) $update_data['auth_code'] = $paySeq;
                if ($buyer_pay_amount) $update_data['buyer_pay_amount'] = $buyer_pay_amount;
                $this->update_day_order($update_data, $out_trade_no);

                //支付成功后的动作
                $data = [
                    'ways_type' => $ways_type,
                    'ways_type_desc' => $ways_type_desc,
                    'company' => $data_insert['company'],
                    'source_type' => '5000', //返佣来源
                    'source_desc' => 'linkage', //返佣来源说明
                    'total_amount' => $total_amount,
                    'out_trade_no' => $out_trade_no,
                    'rate' => $data_insert['rate'],
                    'fee_amount' => $data_insert['fee_amount'],
                    'merchant_id' => $merchant_id,
                    'store_id' => $store_id,
                    'user_id' => $tg_user_id,
                    'config_id' => $config_id,
                    'store_name' => $store_name,
                    'ways_source' => $ways_source,
                    'pay_time' => $pay_time,
                    'device_id' => $device_id ?? ''
                ];
                if (isset($data_insert['other_no'])) $data['other_no'] = $data_insert['other_no'];
                PaySuccessAction::action($data);

                return json_encode([
                    'status' => '1',
                    'pay_status' => 1,
                    'message' => '支付成功',
                    'data' => [
                        'out_trade_no' => $out_trade_no,
                        'ways_type' => $ways_type,
                        'total_amount' => $total_amount,
                        'store_id' => $store_id,
                        'store_name' => $store_name,
                        'config_id' => $config_id,
                        'pay_time' => $pay_time,
                        'trade_no' => $trade_no,
                        'ways_source' => $ways_source
                    ]
                ]);
            } else {
                return json_encode([
                    'status' => '9',
                    'pay_status' => 2,
                    'message' => $returnResult['data']['respMsg'] ?? '等待支付',
                    'data' => [
                        'out_trade_no' => $out_trade_no,
                        'ways_type' => $ways_type,
                        'ways_source' => $ways_source,
                        'total_amount' => $total_amount,
                        'store_id' => $store_id,
                        'store_name' => $store_name,
                        'config_id' => $config_id
                    ]
                ]);
            }
        } //支付失败
        elseif ($return['status'] == '4') {
            return json_encode([
                'status' => '2',
                'pay_status' => 3,
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $ways_source,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id
                ]
            ]);
        } //验签失败
        elseif ($return['status'] == '2') {
            return json_encode([
                'status' => '2',
                'message' => $return['message'] ?? '验签失败'
            ]);
        } //系统错误
        elseif ($return['status'] == '-1') {
            return json_encode([
                'status' => '2',
                'message' => $return['message'] ?? '系统错误'
            ]);
        } //其他
        else {
            return json_encode([
                'status' => '2',
                'message' => $return['message']
            ]);
        }

    }


    //威富通 被扫 公共
    public function wft_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $out_trade_no = $data['out_trade_no'];
        $code = $data['code'];

        $config = new WftPayConfigController();
        $wftpay_config = $config->wftpay_config($data['config_id']);
        if (!$wftpay_config) {
            return json_encode([
                'status' => 2,
                'message' => '威富通配置不存在请检查配置'
            ]);
        }

        $wftpay_merchant = $config->wftpay_merchant($store_id, $store_pid);
        if (!$wftpay_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '威富通商户号不存在'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new WftpayPayController();
        $data['mch_id'] = $wftpay_merchant->mch_id;
        $data['out_trade_no'] = $out_trade_no;
        $data['body'] = $title;
        $data['total_amount'] = $total_amount;
        $data['code'] = $code;
        $data['private_rsa_key'] = $wftpay_config->private_rsa_key;
        $data['public_rsa_key'] = $wftpay_config->public_rsa_key;
//        Log::info('威富通-收钱啦-被扫-入参');
//        Log::info($data);
        $return = $obj->scan_pay($data); //0-系统错误 1-成功 2-失败
//        Log::info('威富通-收钱啦-被扫');
//        Log::info($return);
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //支付成功
        if ($return['status'] == 1) {
            $trade_no = $return['data']['transaction_id']; //平台交易号
            $other_no = $return['data']['out_transaction_id']; //第三方订单号
            $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['time_end'])); //支付完成时间，格式为yyyyMMddHHmmss，如2009年12月27日9点10分10秒表示为20091227091010
            $buyer_pay_amount = isset($return['data']['receipt_amount']) ? $return['data']['receipt_amount'] : $return['data']['total_fee']; //实收金额，单位为元，两位小数。该金额为本笔交易，商户账户能够实际收到的金额
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
            $buyer_id = $return['data']['buyer_user_id'] ?? ''; //买家在支付宝的用户id
            $buyer_logon_id = $return['data']['buyer_logon_id'] ?? ''; //买家支付宝账号

            $data_update = [
                'trade_no' => $trade_no,
                'other_no' => $other_no,
                'buyer_id' => $buyer_id,
                'buyer_logon_id' => $buyer_logon_id,
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount
            ];
            $this->update_day_order($data_update, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '27000',//返佣来源
                'source_desc' => '威富通',//返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'out_transaction_id' => $trade_no,
                    'ways_source' => $data['ways_source']
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source']
                ]
            ]);
        }

    }


    //汇旺财 被扫 公共
    public function hwc_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $out_trade_no = $data['out_trade_no'];
        $code = $data['code'];

        $config = new HwcPayConfigController();
        $hwcpay_config = $config->hwcpay_config($data['config_id']);
        if (!$hwcpay_config) {
            return json_encode([
                'status' => 2,
                'message' => '汇旺财配置不存在请检查配置'
            ]);
        }

        $hwcpay_merchant = $config->hwcpay_merchant($store_id, $store_pid);
        if (!$hwcpay_merchant) {
            return json_encode([
                'status' => 2,
                'message' => '汇旺财商户号不存在'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new HwcpayPayController();
        $data['mch_id'] = $hwcpay_merchant->mch_id;
        $data['out_trade_no'] = $out_trade_no;
        $data['body'] = $title;
        $data['total_amount'] = $total_amount;
        $data['code'] = $code;
        $data['private_rsa_key'] = $hwcpay_config->private_rsa_key;
        $data['public_rsa_key'] = $hwcpay_config->public_rsa_key;
//        Log::info('汇旺财-收钱啦-被扫-入参');
//        Log::info($data);
        $return = $obj->scan_pay($data); //0-系统错误 1-成功 2-失败
//        Log::info('汇旺财-收钱啦-被扫');
//        Log::info($return);
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //支付成功
        if ($return['status'] == 1) {
            $trade_no = isset($return['data']['third_order_no']) ? $return['data']['third_order_no'] : $return['data']['transaction_id']; //第三方商户单号，可在支持的商户扫码退款
            $other_no = $return['data']['out_transaction_id']; //第三方订单号
            $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['time_end'])); //支付完成时间，格式为yyyyMMddHHmmss，如2009年12月27日9点10分10秒表示为20091227091010
            $buyer_pay_amount = isset($return['data']['receipt_amount']) ? $return['data']['receipt_amount'] : $return['data']['total_fee']; //实收金额，单位为元，两位小数。该金额为本笔交易，商户账户能够实际收到的金额or总金额，以分为单位
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');
            $buyer_id = $return['data']['buyer_user_id'] ?? ''; //买家在支付宝的用户id
            $buyer_logon_id = $return['data']['buyer_logon_id'] ?? ''; //买家支付宝账号

            $data_update = [
                'trade_no' => $trade_no,
                'other_no' => $other_no,
                'buyer_id' => $buyer_id,
                'buyer_logon_id' => $buyer_logon_id,
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount
            ];
            $this->update_day_order($data_update, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '28000',//返佣来源
                'source_desc' => '汇旺财',//返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'out_transaction_id' => $trade_no,
                    'ways_source' => $data['ways_source']
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source']
                ]
            ]);
        }

    }

    //邮驿付 被扫  公共
    public function post_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $tg_user_id = $data['tg_user_id'];
        $phone = $data['phone'];
        $payChannel = $data['pay_type'];

        $config = new PostPayConfigController();
        $post_config = $config->post_pay_config($data['config_id']);
        if (!$post_config) {
            return json_encode([
                'status' => '2',
                'message' => '邮驿付支付支付配置不存在，请检查'
            ]);
        }

        $post_merchant = $config->post_pay_merchant($store_id, $store_pid);
        if (!$post_merchant) {
            return json_encode([
                'status' => '2',
                'message' => '邮驿付支付通道未开通成功'
            ]);
        }
        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => '2',
                'message' => '订单未入库'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];
        $total_amount = $data['total_amount'];
        $total_amount = number_format($total_amount, 2, '.', '');

        $obj = new \App\Api\Controllers\PostPay\PayController();
        $post_data = [
            'out_trade_no' => $data['out_trade_no'],
            'code' => $data['code'],
            'device_id' => $post_merchant->drive_no,
            'orgId' => $post_config->org_id,
            'phone' => $phone,
            'custId' => $post_merchant->cust_id,
            'payChannel' => $payChannel,
            'total_amount' => $total_amount,
        ];

        $return = $obj->scan_pay($post_data); //-1 系统错误 0-其他 1-成功 2-待支付

        //支付成功
        if ($return['status'] == '1') {
            $trade_no = $return['data']['orderNo']; //系统订单号
            $pay_time = (isset($return['data']['orderTime']) && !empty($return['data']['orderTime'])) ? date('Y-m-d H:i:m', strtotime($return['data']['orderTime'])) : ''; //支付完成时间，格式为 yyyyMMddhhmmss，如 2009年12月27日9点10分10秒表示为 20091227091010
            $acctype = $return['data']['cardType']; //交易账户类型
            $wxopenid = $return['data']['openId'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
            $clearamt = isset($return['data']['txamt']) ? ($return['data']['txamt'] / 100) : $total_amount; //结算金额，单位分
            $payamt = isset($return['data']['netrAmt']) ? ($return['data']['netrAmt'] / 100) : $total_amount; //实付金额，单位分

            $payment_method = '';
            if ($acctype) {
                switch ($acctype) {
                    case '02': //信用卡
                        $payment_method = 'credit';
                        break;
                    case '01': //借记卡
                        $payment_method = 'debit';
                        break;
                    default: //U其他
                        $payment_method = 'unknown';
                }
            }

            $fee_amount = ($data_insert['rate'] * $payamt) / 100;
            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $wxopenid,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status' => 1,
                'pay_status_desc' => '支付成功',
                'payment_method' => $payment_method, //credit:信用卡 pcredit:花呗（仅支付宝） debit:借记卡 balance:余额 unknown:未知
                'buyer_pay_amount' => $clearamt, //买家付款的金额
                'receipt_amount' => $payamt, //实付
                'pay_time' => $pay_time,
                'fee_amount' => $fee_amount
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '29000', //返佣来源
                'source_desc' => '邮驿付支付', //返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $fee_amount,
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => $post_merchant->drive_no ?? '',
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => 1,
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } //待支付
        elseif ($return['status'] == '2') {
            return json_encode([
                'status' => 9,
                'pay_status' => 2,
                'message' => '待支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } else {
            return json_encode([
                'status' => '2',
                'pay_status' => 3,
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }


    }

    //建设银行 被扫  公共
    public function ccBank_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $tg_user_id = $data['tg_user_id'];


        $config = new CcBankPayConfigController();

        $ccBank_merchant = $config->ccBank_pay_merchant($store_id, $store_pid);
        if (!$ccBank_merchant) {
            return json_encode([
                'status' => '2',
                'message' => '建设银行支付通道未开通成功'
            ]);
        }

        $store = Store::where('store_id', $store_id)->select('zero_rate_type')->first();
        if ($store->zero_rate_type == '0') {//0收费  1不收费
            //查询商户是否充值
            $MerchantStore = MerchantStore::where('store_id', $store_id)->select('merchant_id')->first();
            if (!empty($MerchantStore)) {
                $merchant_id = $MerchantStore->merchant_id;
            }
            $MerchantConsumerDetails = MerchantConsumerDetails::where('merchant_id', $merchant_id)
                ->where('type', '1')
                ->first();
            if (!$MerchantConsumerDetails) {
                return json_encode([
                    'status' => '2',
                    'message' => '该商户没有充值，请商户及时充值'
                ]);
            }
            $MerchantConsumerDetails = MerchantConsumerDetails::where('merchant_id', $merchant_id)
                ->whereIn('type', [1, 2])
                ->orderBy('created_at', 'desc')
                ->first();
            if ($MerchantConsumerDetails->avail_amount < 0) {
                return json_encode([
                    'status' => '2',
                    'message' => '该商户可用余额已为负值，请商户及时充值'
                ]);
            }
        }
        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => '2',
                'message' => '订单未入库'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];
        $total_amount = $data['total_amount'];
        $total_amount = number_format($total_amount, 2, '.', '');

        $obj = new \App\Api\Controllers\CcBankPay\PayController();
        $ccBank_data = [
            'cust_id' => $ccBank_merchant->cust_id,
            'pos_id' => $ccBank_merchant->pos_id,
            'branch_id' => $ccBank_merchant->branch_id,
            'out_trade_no' => $out_trade_no,
            'code' => $data['code'],
            'total_amount' => $data['total_amount'],
            'public_key' => $ccBank_merchant->public_key,
            'termno1' => $ccBank_merchant->termno1,
            'termno2' => $ccBank_merchant->termno2,
        ];

        $return = $obj->scan_pay($ccBank_data); //-1 系统错误 0-其他 1-成功 2-待支付

        //支付成功
        if ($return['status'] == '1') {
            $trade_no = $return['data']['TRACEID']; //系统订单号
            $pay_time = date('Y-m-d H:i:m', time());
            $acctype = $return['data']['PAYMENT_DETAILS']['DEBIT_CREDIT_TYPE'] ?? ''; //交易账户类型
            $wxopenid = $return['data']['OPENID'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
            $clearamt = isset($return['data']['AMOUNT']) ? ($return['data']['AMOUNT']) : $total_amount; //结算金额
            $payamt = isset($return['data']['AMOUNT']) ? ($return['data']['AMOUNT']) : $total_amount; //实付金额

            $payment_method = '';
            if ($acctype) {
                switch ($acctype) {
                    case '02': //信用卡
                        $payment_method = 'CREDIT';
                        break;
                    case '01': //借记卡
                        $payment_method = 'DEBIT';
                        break;
                    default: //U其他
                        $payment_method = 'unknown';
                }
            }

            $fee_amount = ($data_insert['rate'] * $payamt) / 100;
            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $wxopenid,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status' => 1,
                'pay_status_desc' => '支付成功',
                'payment_method' => $payment_method, //credit:信用卡 pcredit:花呗（仅支付宝） debit:借记卡 balance:余额 unknown:未知
                'buyer_pay_amount' => $clearamt, //买家付款的金额
                'receipt_amount' => $payamt, //实付
                'pay_time' => $pay_time,
                'fee_amount' => $fee_amount
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '31000', //返佣来源
                'source_desc' => '建设银行', //返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $fee_amount,
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => $post_merchant->drive_no ?? '',
                'no_user_money' => '1',//不计算返佣
            ];
            PaySuccessAction::action($data);

            if ($store->zero_rate_type == '0') {//0收费  1不收费
                $TransactionDeductionController = new TransactionDeductionController();
                $deduction = [
                    'merchant_id' => $merchant_id,
                    'total_amount' => $total_amount,
                    'user_id' => $tg_user_id,
                    'order_id' => $out_trade_no,
                    'company' => $data_insert['company'],
                    'rate' => $data_insert['rate'] //结算费率
                ];
                $TransactionDeductionController->deduction($deduction);//交易扣款
            }

            return json_encode([
                'status' => 1,
                'pay_status' => 1,
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } //待支付
        elseif ($return['status'] == '2') {
            $trade_no = $return['data']['TRACEID']; //系统订单号
            $pay_time = date('Y-m-d H:i:m', time());
            $wxopenid = $return['data']['OPENID'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
            $clearamt = isset($return['data']['AMOUNT']) ? ($return['data']['AMOUNT']) : $total_amount; //结算金额
            $payamt = isset($return['data']['AMOUNT']) ? ($return['data']['AMOUNT']) : $total_amount; //实付金额
            $fee_amount = ($data_insert['rate'] * $payamt) / 100;
            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $wxopenid,
                //'payment_method' => $payment_method, //credit:信用卡 pcredit:花呗（仅支付宝） debit:借记卡 balance:余额 unknown:未知
                'buyer_pay_amount' => $clearamt, //买家付款的金额
                'receipt_amount' => $payamt, //实付
                'pay_time' => $pay_time,
                'fee_amount' => $fee_amount
            ];
            $this->update_day_order($update_data, $out_trade_no);
            return json_encode([
                'status' => 9,
                'pay_status' => 2,
                'message' => '待支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } else {
            return json_encode([
                'status' => '2',
                'pay_status' => 3,
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }
    }

    //易生数科支付 被扫 公共
    public function easyskpay_pay_public($data_insert, $data)
    {
        $ways_type_desc = $data['ways_type_desc'] ?? '';
        $ways_source_desc = $data['ways_source_desc'] ?? '';
        $code = $data['code'] ?? '';
        $out_trade_no = $data['out_trade_no'] ?? '';
        $config_id = $data['config_id'] ?? '';
        $store_id = $data['store_id'] ?? '';
        $store_pid = $data['store_pid'] ?? '';
        $ways_type = $data['ways_type'] ?? '';
        $ways_source = $data['ways_source'] ?? '';
        $company = $data['company'] ?? '';
        $total_amount = $data['total_amount'] ?? '';
        $remark = $data['remark'] ?? '';
        $device_id = $data['device_id'] ?? '';
        $shop_name = $data['shop_name'] ?? '';
        $merchant_id = $data['merchant_id'] ?? '';
        $store_name = $data['store_name'] ?? '';
        $tg_user_id = $data['tg_user_id'] ?? '';

        $config = new EasySkPayConfigController();
        $easyskpay_config = $config->easyskpay_config($config_id);
        if (!$easyskpay_config) {
            return json_encode([
                'status' => '2',
                'message' => '易生数科支付配置不存在请检查配置'
            ]);
        }

        $easyskpay_merchant = $config->easyskpay_merchant($store_id, $store_pid);
        if (!$easyskpay_merchant) {
            return json_encode([
                'status' => '2',
                'message' => '易生数科支付商户号不存在'
            ]);
        }

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $ways_type;
        $data_insert['ways_type_desc'] = $ways_type_desc;
        $data_insert['company'] = $company;
        $data_insert['ways_source'] = $ways_source;
        $data_insert['ways_source_desc'] = $ways_source_desc;
        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => '2',
                'message' => '订单未入库'
            ]);
        }

        $store = Store::where('store_id', $store_id)->first();
        $location = '';
        if (!$store->lat || !$store->lng) {
            $storeController = new \App\Api\Controllers\User\StoreController();
            $address = $store->province_name . $store->city_name . $store->area_name . $store->store_address;//获取经纬度的地址
            $api_res = $storeController->query_address($address, env('LBS_KEY'));
            if ($api_res['status'] == '1') {
                $store_lng = $api_res['result']['lng'];
                $store_lat = $api_res['result']['lat'];
                $store->update([
                    'lng' => $store_lng,
                    'lat' => $store_lat,
                ]);
                $location = "+" . $store_lat . "/-" . $store_lng;
            }

        } else {
            $location = "+" . $store->lat . "/-" . $store->lng;
        }

        $obj = new \App\Api\Controllers\EasySkPay\PayController();
        $easypay_data = [
            'org_id' => $easyskpay_config->org_id,
            'mer_id' => $easyskpay_merchant->mer_id,
            'request_no' => $data['out_trade_no'],
            'amount' => $data['total_amount'],
            'authCode' => $data['code'],
            'pay_type' => $data['pay_type'],
            'location' => $location
        ];
        Log::info('插件-易生数科支付-被扫');
        Log::info($easypay_data);
        $return = $obj->codePayByBtoC($easypay_data); //-1 系统错误 0-其他 1-成功 2-待支付
        if ($return['status'] == '-1') {
            return json_encode([
                'status' => '2',
                'message' => $return['message']
            ]);
        }

        $other_no = $return['data']['outTrace'] ?? ''; //商户订单号（请求原值返回）
        $trade_no = $return['data']['tradeNo']; //系统订单号
        $pay_time = (isset($return['data']['bizData']['payTime']) && !empty($return['data']['bizData']['payTime'])) ? date('Y-m-d H:i:m', strtotime($return['data']['bizData']['payTime'])) : '';
        $acctype = $return['data']['bizData']['payAccType']; //交易账户类型
        $wxopenid = $return['data']['bizData']['channelOpenId'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
        $clearamt = isset($return['data']['bizData']['stlmAmt']) ? ($return['data']['bizData']['stlmAmt'] / 100) : $total_amount; //结算金额，单位分
        $payamt = isset($return['data']['bizData']['amount']) ? ($return['data']['bizData']['amount'] / 100) : $total_amount; //实付金额，单位分
        //支付成功
        if ($return['status'] == '1') {
            $data_update = [
                'trade_no' => $trade_no,
                'other_no' => $other_no,
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '', //credit--信用卡 pcredit--花呗（仅支付宝） debit--借记卡 balance--余额 unknown--未知
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $payamt //用户实际支付
            ];
            $this->update_day_order($data_update, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $ways_type,
                'ways_type_desc' => $ways_type_desc,
                'company' => $company,
                'source_type' => '32000', //返佣来源
                'source_desc' => '易生数科支付', //返佣来源说明
                'total_amount' => $payamt,
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $ways_source,
                'pay_time' => $pay_time,
                'device_id' => $device_id,
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => '1',
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $payamt,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'out_transaction_id' => $out_trade_no,
                    'ways_source' => $ways_source,
                ]
            ]);
        }

        //待支付
        if ($return['status'] == '2') {
            return json_encode([
                'status' => '9',
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $payamt,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $ways_source,
                ]
            ]);
        }

        //其他
        if ($return['status'] == '0') {
            return json_encode([
                'status' => '2',
                'pay_status' => 3,
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $payamt,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $ways_source,
                ]
            ]);
        }

    }
    //通联 被扫  公共
    public function allin_pay_public($data_insert, $data)
    {
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $tg_user_id = $data['tg_user_id'];
        $config = new AllinPayConfigController();
        $allin_config = $config->allin_pay_config($data['config_id']);
        if (!$allin_config) {
            return json_encode([
                'status' => '2',
                'message' => '通联支付支付配置不存在，请检查'
            ]);
        }

        $allin_merchant = $config->allin_pay_merchant($store_id, $store_pid);
        if (!$allin_merchant) {
            return json_encode([
                'status' => '2',
                'message' => '通联支付通道未开通成功'
            ]);
        }
        $out_trade_no = $data['out_trade_no'];

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => '2',
                'message' => '订单未入库'
            ]);
        }

        $out_trade_no = $data['out_trade_no'];
        $total_amount = $data['total_amount'];
        $total_amount = number_format($total_amount, 2, '.', '');

        $obj = new \App\Api\Controllers\AllinPay\PayController();
        $allin_data = [
            'out_trade_no' => $data['out_trade_no'],
            'total_amount' => $data['total_amount'],
            'code' => $data['code'],
            'orgId' => $allin_config->org_id,
            'cusId' => $allin_merchant->cus_id,
            'boby'=>$data['store_name'],
            'appid' => $allin_merchant->appid,
            'sub_appid' => $allin_config->wx_appid,
            'termno' =>$allin_merchant->termno,
            //'notify_url' =>$allin_merchant->termno,

        ];

        $return = $obj->scan_pay($allin_data); //-1 系统错误 0-其他 1-成功 2-待支付

        //支付成功
        if ($return['status'] == '1') {
            $trade_no = $return['data']['trxid']; //系统订单号
            $pay_time = date('Y-m-d H:i:m', tiem()); //支付完成时间，格式为 yyyyMMddhhmmss
            $openid = $return['data']['acct'] ?? ''; //用户在商户 appid 下的唯一标识(微信/支付宝渠道返回)
            $clearamt = isset($return['data']['trxamt']) ? ($return['data']['trxamt'] / 100) : $total_amount; //结算金额，单位分
            $payamt = $total_amount; //实付金额，单位分
            $payment_method = '';
            $fee_amount = ($data_insert['rate'] * $payamt) / 100;
            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => $openid,
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status' => 1,
                'pay_status_desc' => '支付成功',
                'payment_method' => $payment_method, //credit:信用卡 pcredit:花呗（仅支付宝） debit:借记卡 balance:余额 unknown:未知
                'buyer_pay_amount' => $clearamt, //买家付款的金额
                'receipt_amount' => $payamt, //实付
                'pay_time' => $pay_time,
                'fee_amount' => $fee_amount
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '33000', //返佣来源
                'source_desc' => '通联支付', //返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'other_no' => $data_insert['other_no'],
                'rate' => $data_insert['rate'],
                'fee_amount' => $fee_amount,
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => $post_merchant->drive_no ?? '',
            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => 1,
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } //待支付
        elseif ($return['status'] == '2') {
            return json_encode([
                'status' => 1,
                'pay_status' => 2,
                'message' => '待支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        } else {
            return json_encode([
                'status' => '3',
                'message' => $return['message']
            ]);
        }


    }

    //富友
    public function fuiou_pay_public($data_insert, $data)
    {
        //读取配置
        $store_id = $data['store_id'];
        $store_pid = $data['store_pid'];
        $merchant_id = $data['merchant_id'];
        $total_amount = $data['total_amount'];
        $ways_type = $data['ways_type'];
        $store_name = $data['store_name'];
        $config_id = $data['config_id'];
        $desc = $data['ways_type_desc'];
        $title = $data['title'];
        $tg_user_id = $data['tg_user_id'];
        $code = $data['code'];

        $config = new FuiouConfigController();
        $fuiou_config = $config->fuiou_config($data['config_id']);
        if (!$fuiou_config) {
            return json_encode([
                'status' => 2,
                'message' => '富友配置不存在请检查配置'
            ]);
        }

        $fuiou__merchant = $config->fuiou_merchant($store_id, $store_pid);
        if (!$fuiou__merchant) {
            return json_encode([
                'status' => 2,
                'message' => '富友商户号不存在'
            ]);
        }

        $out_trade_no = '1300' . date('YmdHis', time()) . substr(microtime(), 2, 6) . sprintf('%03d', rand(0, 999));

        //入库参数
        $data_insert['out_trade_no'] = $out_trade_no;
        $data_insert['ways_type'] = $data['ways_type'];
        $data_insert['ways_type_desc'] = $data['ways_type_desc'];
        $data_insert['ways_source'] = $data['ways_source'];
        $data_insert['company'] = $data['company'];
        $data_insert['ways_source_desc'] = $data['ways_source_desc'];

        $insert_re = $this->insert_day_order($data_insert);
        if (!$insert_re) {
            return json_encode([
                'status' => 2,
                'message' => '订单未入库'
            ]);
        }

        $obj = new \App\Api\Controllers\Fuiou\PayController();

        $order_type = '';
        if ($data['ways_source'] == "alipay") {
            $order_type = 'ALIPAY';
        }
        if ($data['ways_source'] == "weixin") {
            $order_type = 'WECHAT';
        }
        if ($data['ways_source'] == "unionpay") {
            $order_type = 'UNIONPAY';
        }

        $request = [
            'ins_cd' => $fuiou_config->ins_cd, //机构号
            'mchnt_cd' => $fuiou__merchant->mchnt_cd, //商户号
            'order_type' => $order_type, //订单类型订单类型:ALIPAY，WECHAT，UNIONPAY(银联二维码），BESTPAY(翼支付)
            'goods_des' => $store_id, //商品描述
            'mchnt_order_no' => $out_trade_no, //商户订单号
            'order_amt' => $total_amount * 100,//总金额 分
            'auth_code' => $code,//付款码,
            'pem' => $fuiou_config->my_private_key,
            'url' => $obj->scpay_url,
        ];
        $return = $obj->scan_pay($request);
        if ($return['status'] == 0) {
            return json_encode([
                'status' => 2,
                'message' => $return['message']
            ]);
        }

        //返回支付成功
        if ($return['status'] == 1) {
            $trade_no = $return['data']['transaction_id'];
            $pay_time = date('Y-m-d H:i:s', strtotime($return['data']['reserved_txn_fin_ts']));
            $buyer_pay_amount = $total_amount;
            $buyer_pay_amount = number_format($buyer_pay_amount, 2, '.', '');

            $update_data = [
                'trade_no' => $trade_no,
                'buyer_id' => '',
                'buyer_logon_id' => '',
                'status' => '1',
                'pay_status_desc' => '支付成功',
                'pay_status' => 1,
                'payment_method' => '',
                'pay_time' => $pay_time,
                'buyer_pay_amount' => $buyer_pay_amount,//用户实际支付
                'receipt_amount' => $buyer_pay_amount //用户实际支付
            ];
            $this->update_day_order($update_data, $out_trade_no);

            //支付成功后的动作
            $data = [
                'ways_type' => $data['ways_type'],
                'ways_type_desc' => $data['ways_type_desc'],
                'company' => $data_insert['company'],
                'source_type' => '11000',//返佣来源
                'source_desc' => '富友',//返佣来源说明
                'total_amount' => $total_amount,
                'out_trade_no' => $out_trade_no,
                'rate' => $data_insert['rate'],
                'other_no' => $data_insert['other_no'],
                'fee_amount' => $data_insert['fee_amount'],
                'merchant_id' => $merchant_id,
                'store_id' => $store_id,
                'user_id' => $tg_user_id,
                'config_id' => $config_id,
                'store_name' => $store_name,
                'ways_source' => $data['ways_source'],
                'pay_time' => $pay_time,
                'device_id' => isset($data['device_id']) ? $data['device_id'] : "",


            ];
            PaySuccessAction::action($data);

            return json_encode([
                'status' => 1,
                'pay_status' => '1',
                'message' => '支付成功',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                    'pay_time' => $pay_time,
                    'trade_no' => $trade_no,
                    'ways_source' => $data['ways_source'],
                ]
            ]);
        }

        //正在支付
        if ($return['status'] == 2) {
            return json_encode([
                'status' => 1,
                'pay_status' => '2',
                'message' => '正在支付',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }

        //支付失败
        if ($return['status'] == 3) {
            return json_encode([
                'status' => 2,
                'pay_status' => '3',
                'message' => '支付失败',
                'data' => [
                    'out_trade_no' => $out_trade_no,
                    'ways_type' => $ways_type,
                    'ways_source' => $data['ways_source'],
                    'total_amount' => $total_amount,
                    'store_id' => $store_id,
                    'store_name' => $store_name,
                    'config_id' => $config_id,
                ]
            ]);
        }
    }


}