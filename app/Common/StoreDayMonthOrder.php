<?php
namespace App\Common;


class StoreDayMonthOrder
{

    //订单入库
    public static function insert($data)
    {
        try {
            $type = $data['ways_type'];
            $source_type = $data['ways_source'];
            $store_id = $data['store_id'];
            $merchant_id = $data['merchant_id'];
            $total_amount = $data['total_amount'];
            $fee_amount = $data['fee_amount'];
            $company = $data['company'];
            $user_id = $data['user_id'];
            //这个代表店铺收银员每天的金额相加
            $day = date('Ymd', time());

            //这个代表店铺每天的所有类型金额相加
            $day = date('Ymd', time());
            $StoreDayAllOrder = \App\Models\StoreDayAllOrder::where('day', $day)
                ->where('store_id', $store_id)
                ->where('user_id', $user_id)
                ->first();
            if ($StoreDayAllOrder && $total_amount) {
                $StoreDayAllOrder->total_amount = ($total_amount + $StoreDayAllOrder->total_amount);
                $StoreDayAllOrder->fee_amount = ($fee_amount + $StoreDayAllOrder->fee_amount);
                $StoreDayAllOrder->order_sum = ($StoreDayAllOrder->order_sum + 1); //笔数
                $StoreDayAllOrder->save();
            } else {
                \App\Models\StoreDayAllOrder::create([
                    'store_id' => $store_id,
                    'user_id' => $user_id,
                    'day' => $day,
                    'total_amount' => $total_amount,
                    'order_sum' => 1,
                    'fee_amount' => $fee_amount,
                    'refund_amount' => '0.00',
                    'refund_count' => 0,
                ]);
            }
        } catch (\Exception $exception) {
            \Illuminate\Support\Facades\Log::info('对账相加');
            \Illuminate\Support\Facades\Log::info($exception->getMessage().' | '.$exception->getFile().' | '.$exception->getFile());
        }
    }
    

}
