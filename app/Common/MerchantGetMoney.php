<?php

namespace App\Common;


use App\Models\ActivityStoreRate;
use App\Models\MerchantWalletDetail;
use App\Models\Store;
use App\Models\UserWalletDetail;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class MerchantGetMoney
{

    public $store_id;//门店
    public $out_trade_no;//订单号
    public $order_total_amount;//订单金额
    public $user_id;//直属推广员ID
    public $ways_type;//通道类型
    public $store_ways_type_rate;//门店通道类型费率
    public $source_type;//返佣类型
    public $source_desc;//返佣类型说明
    public $config_id;//返佣类型说明
    public $company;//通道
    public $ways_source;//类型


    /**
     * MerchantGetMoney constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->user_id = $data['user_id'];
        $this->store_id = $data['store_id'];
        $this->out_trade_no = $data['out_trade_no'];
        $this->ways_type = $data['ways_type'];
        $this->order_total_amount = $data['order_total_amount'];
        $this->store_ways_type_rate = $data['store_ways_type_rate'];
        $this->source_type = $data['source_type'];
        $this->source_desc = $data['source_desc'];
        $this->config_id = $data['config_id'];
        $this->company = $data['company'];
        $this->ways_source = $data['ways_source'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function insert()
    {
        try {
            $now_time = date('Y-m-d H:i:s', time());
            $day = date('Ymd', time());

            //传化的活动
            if (0&&in_array($this->ways_type, ['12001', '12002']) && $this->order_total_amount < 300) {

                //其他正常的活动添加
                $store = Store::where('store_id', $this->store_id)
                    ->select('merchant_id', 'store_name', 'people_phone', 'user_id')
                    ->first();

                if (!$store) {
                    return '';
                }

                //手续费
                $money = $this->order_total_amount * $this->store_ways_type_rate;//直属代理商
                $money = $money / 100;
                $money = number_format($money, 4, '.', '');


                //看下传化是否有添加手续费返回活动
                $ActivityStoreRate = ActivityStoreRate::where('store_id', $this->store_id)
                    ->where('company', $this->company)
                    ->where('time_start', '<', $now_time)
                    ->where('time_end', '>', $now_time)
                    ->where('total_amount_s', '<', $this->order_total_amount)
                    ->where('total_amount_e', '>', $this->order_total_amount)
                    ->select('alipay', 'weixin')
                    ->first();

                //即是传化 又设置了活动
                if ($ActivityStoreRate) {

                    $ways_source = $this->ways_source;

                    //手续费
                    $money = $this->order_total_amount * $this->store_ways_type_rate;//直属代理商
                    $money = $money / 100;
                    $f = 0;
                    $bl = 0;
                    //返还
                    if ($ways_source == "alipay") {
                        $f = $money * $ActivityStoreRate->alipay;
                        $bl = $ActivityStoreRate->alipay;
                    }
                    if ($ways_source == "weixin") {
                        $f = $money * $ActivityStoreRate->weixin;
                        $bl = $ActivityStoreRate->weixin;
                    }


                    //返还
                    $f = $f / 100;
                    $f = number_format($f, 4, '.', '');

                    if ($f > 0.0000) {

                        //流水明细
                        $MerchantWalletDetail = [
                            'config_id' => $this->config_id,
                            'store_id' => $this->store_id,
                            'merchant_id' => $store->merchant_id,
                            'merchant_name' => '',
                            'store_name' => $store->store_name,
                            'phone' => $store->people_phone,
                            'money' => $f,
                            'out_trade_no' => $this->out_trade_no,
                            'trade_no' => $this->out_trade_no,
                            'source_type' => $this->source_type,
                            'source_desc' => $this->source_desc,
                        ];

                        //检查是否已入库
                        if (Cache::has($this->source_type . $this->store_id . $this->out_trade_no . $store->merchant_id . $f)) {
                            return;
                        }

                        //数据入库
                        self::insert_MerchantWalletDetail($MerchantWalletDetail, $day);

                        Cache::add($this->source_type . $this->store_id . $this->out_trade_no . $store->merchant_id . $f,'1', 10);

                        //统计入库
                        $StoreDayOrder = \App\Models\MerchantWalletDayCount::where('day', $day)
                            ->where('source_type', $this->source_type)
                            ->where('store_id', $this->store_id)
                            ->first();


                        if ($StoreDayOrder) {
                            $StoreDayOrder->money = ($f + $StoreDayOrder->money);
                            $StoreDayOrder->save();
                        } else {
                            \App\Models\MerchantWalletDayCount::create([
                                'store_id' => $this->store_id,
                                'config_id' => $this->config_id,
                                'source_type' => $this->source_type,
                                'source_desc' => $this->source_desc,
                                'money' => $f,
                                'merchant_id' => $store->merchant_id,
                                'day' => $day,
                            ]);
                        }


                    }


                    //剩下的返给服务商
                    $fu_f = $money - $f;


                    //3.服务商返佣
                    try {
                        $user_detail = [
                            'config_id' => $this->config_id,
                            'store_id' => $this->store_id,
                            'user_id' => $store->user_id,
                            'user_name' => '未知',
                            'phone' => '未知',
                            'money' => $fu_f,
                            'out_trade_no' => $this->out_trade_no,
                            'trade_no' => $this->out_trade_no,
                            'source_type' => $this->source_type,
                            'source_desc' => $this->source_desc . '-' . $bl,
                            'pay_money' => $this->order_total_amount
                        ];

                        //直属入库
                        if ($fu_f > 0) {
                            //检查是否已入库
                            if (Cache::has($this->source_type . $this->store_id . $this->out_trade_no . $store->user_id . $fu_f)) {
                                return;
                            }

                            self::insert_UserWalletDetail($user_detail,$day);
                            Cache::add($this->source_type . $this->store_id . $this->out_trade_no . $store->user_id . $fu_f,'1', 10);
                        }
                    } catch (\Exception $exception) {
                        \Illuminate\Support\Facades\Log::info('返佣');
                        \Illuminate\Support\Facades\Log::info($exception);
                    }
                } else {
                    $day = date('Ymd', time());

                    //流水明细
                    $MerchantWalletDetail = [
                        'config_id' => $this->config_id,
                        'store_id' => $this->store_id,
                        'merchant_id' => $store->merchant_id,
                        'merchant_name' => '',
                        'store_name' => $store->store_name,
                        'phone' => $store->people_phone,
                        'money' => $money,
                        'out_trade_no' => $this->out_trade_no,
                        'trade_no' => $this->out_trade_no,
                        'source_type' => $this->source_type,
                        'source_desc' => $this->source_desc,
                    ];

                    //检查是否已入库
                    if (Cache::has($this->source_type . $this->store_id . $this->out_trade_no . $store->merchant_id . $money)) {
                        return;
                    }

                    self::insert_MerchantWalletDetail($MerchantWalletDetail, $day);

                    Cache::add($this->source_type . $this->store_id . $this->out_trade_no . $store->merchant_id . $money,'1', 10);

                    //统计入库
                    $StoreDayOrder = \App\Models\MerchantWalletDayCount::where('day', $day)
                        ->where('source_type', $this->source_type)
                        ->where('store_id', $this->store_id)
                        ->first();
                    if ($StoreDayOrder) {
                        $StoreDayOrder->money = ($money + $StoreDayOrder->money);
                        $StoreDayOrder->save();
                    } else {
                        \App\Models\MerchantWalletDayCount::create([
                            'store_id' => $this->store_id,
                            'config_id' => $this->config_id,
                            'source_type' => $this->source_type,
                            'source_desc' => $this->source_desc,
                            'money' => $money,
                            'merchant_id' => $store->merchant_id,
                            'day' => $day,
                        ]);
                    }
                }
            } else {
                //其他正常的活动添加
                $store = Store::where('store_id', $this->store_id)
                    ->select('merchant_id', 'store_name', 'people_phone')
                    ->first();
                if (!$store) {
                    return '';
                }

                //判断商户有没有设置这个活动
                $ActivityStoreRate = ActivityStoreRate::where('store_id', $this->store_id)
                    ->where('company', $this->company)
                    ->where('time_start', '<', $now_time)
                    ->where('time_end', '>', $now_time)
                    ->where('total_amount_s', '<', $this->order_total_amount)
                    ->where('total_amount_e', '>', $this->order_total_amount)
                    ->select('alipay', 'weixin')
                    ->first();
                if ($ActivityStoreRate) {
                    $ways_source = $this->ways_source;

                    //手续费
                    $money = $this->order_total_amount * $this->store_ways_type_rate;//直属代理商
                    $money = $money / 100;

                    //返还
                    if ($ways_source == "alipay") {
                        $f = $money * $ActivityStoreRate->alipay;
                    }
                    if ($ways_source == "weixin") {
                        $f = $money * $ActivityStoreRate->weixin;
                    }

                    //返还
                    $f = $f / 100;
                    $f = number_format($f, 4, '.', '');

                    if ($f > 0.0000) {
                        //流水明细
                        $day = date('Ymd', time());

                        $MerchantWalletDetail = [
                            'config_id' => $this->config_id,
                            'store_id' => $this->store_id,
                            'merchant_id' => $store->merchant_id,
                            'merchant_name' => '',
                            'store_name' => $store->store_name,
                            'phone' => $store->people_phone,
                            'money' => $f,
                            'out_trade_no' => $this->out_trade_no,
                            'trade_no' => $this->out_trade_no,
                            'source_type' => $this->source_type,
                            'source_desc' => $this->source_desc,
                        ];
                        //检查是否已入库
                        if (Cache::has($this->source_type . $this->store_id . $this->out_trade_no . $store->merchant_id . $f)) {
                            return;
                        }

                        self::insert_MerchantWalletDetail($MerchantWalletDetail, $day);

                        Cache::add($this->source_type . $this->store_id . $this->out_trade_no . $store->merchant_id . $f,'1', 10);

                        //统计入库
                        $StoreDayOrder = \App\Models\MerchantWalletDayCount::where('day', $day)
                            ->where('source_type', $this->source_type)
                            ->where('store_id', $this->store_id)
                            ->first();
                        if ($StoreDayOrder) {
                            $StoreDayOrder->money = ($f + $StoreDayOrder->money);
                            $StoreDayOrder->save();
                        } else {
                            \App\Models\MerchantWalletDayCount::create([
                                'store_id' => $this->store_id,
                                'config_id' => $this->config_id,
                                'source_type' => $this->source_type,
                                'source_desc' => $this->source_desc,
                                'money' => $f,
                                'merchant_id' => $store->merchant_id,
                                'day' => $day,
                            ]);
                        }
                    }
                }
            }
        } catch (\Exception $exception) {
            \Illuminate\Support\Facades\Log::info($exception);
        }
    }

    //公共进库
    public static function insert_MerchantWalletDetail($MerchantWalletDetail, $day)
    {
        try {

            //如果存在base
            if (Schema::hasTable('merchant_wallet_details_base')) {
                //后面可以不进这个数据库
                $table = 'merchant_wallet_details_' . $day;
                //判断是否有表
                if (!Schema::hasTable($table)) {
                    //无表
                    DB::update('create table ' . $table . ' like merchant_wallet_details_base');
                }
                $MerchantWalletDetail['created_at'] = date('Y-m-d H:i:s', time());
                $MerchantWalletDetail['updated_at'] = date('Y-m-d H:i:s', time());
                DB::table($table)->insert($MerchantWalletDetail);

            } else {

                MerchantWalletDetail::create($MerchantWalletDetail);

            }


        } catch (\Exception $exception) {
            \Illuminate\Support\Facades\Log::info('merchant_wallet_details_base');
            \Illuminate\Support\Facades\Log::info($exception);
        }
    }


    //公共进库
    public static function insert_UserWalletDetail($UserWalletDetail, $day)
    {
        try {

            //如果存在base
            if (Schema::hasTable('user_wallet_details_base')) {
                //后面可以不进这个数据库
                $table = 'user_wallet_details_' . $day;
                //判断是否有表
                if (!Schema::hasTable($table)) {
                    //无表
                    DB::update('create table ' . $table . ' like 	user_wallet_details_base');
                }
                $UserWalletDetail['created_at'] = date('Y-m-d H:i:s', time());
                $UserWalletDetail['updated_at'] = date('Y-m-d H:i:s', time());
                DB::table($table)->insert($UserWalletDetail);

            } else {

                UserWalletDetail::create($UserWalletDetail);

            }


        } catch (\Exception $exception) {
            \Illuminate\Support\Facades\Log::info('merchant_wallet_user_wallet_details_base');
            \Illuminate\Support\Facades\Log::info($exception);
        }
    }
}
