<?php

namespace App\Http\Controllers\AuthorizeApplets;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StoreInputController extends Controller
{
    /**
    * 输出商户进件页面
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function storeNetwork()
    {
        return view("storeInputPage.storeNetwork");
    }
    /**
     * 输出商户进件页面
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function storeInput()
    {
        return view("storeInputPage.storeInput");
    }
    /**
    * 输出进件步骤页面
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function parts()
    {
        return view("storeInputPage.parts");
    }
    /**
     * 输出进件步骤页面
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function partsTow()
    {
        return view("storeInputPage.partsTow");
    }
    /**
     * 输出店铺信息
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function shop()
    {
        return view("storeInputPage.shop");
    }
    /**
     * 输出门店地址
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addaddress()
    {
        return view("storeInputPage.addaddress");
    }
    /**
     * 输出法人认证
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addidcardphoto()
    {
        return view("storeInputPage.addidcardphoto");
    }
    /**
     * 输出结算信息
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addbindcard()
    {
        return view("storeInputPage.addbindcard");
    }
    /**
     * 输出开户许可证
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addpermit()
    {
        return view("storeInputPage.addpermit");
    }
    /**
     * 输出银行卡类型
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function bankcardtype()
    {
        return view("storeInputPage.bankcardtype");
    }
    /**
     * 输出开户支行
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function branchbank()
    {
        return view("storeInputPage.branchbank");
    }
    /**
     * 输出上传手持身份证正面照
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function Cardphoto()
    {
        return view("storeInputPage.Cardphoto");
    }
    /**
     * 输出银行卡照片
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addbankphoto()
    {
        return view("storeInputPage.addbankphoto");
    }
    /**
     * 输出营业执照
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addLicense()
    {
        return view("storeInputPage.addLicense");
    }
    /**
     * 输出进件layui
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function storeInputs()
    {
        return view("storeInputPage.storeInputs");
    }
}
