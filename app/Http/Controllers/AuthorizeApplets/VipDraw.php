<?php

namespace App\Http\Controllers\AuthorizeApplets;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VipDraw extends Controller
{
    /**
    * 输出vip抽奖
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function VipDraw()
    {
        return view("VipDraw.VipDraw");
    }
}
