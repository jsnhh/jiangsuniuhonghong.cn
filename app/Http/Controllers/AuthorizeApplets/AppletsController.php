<?php

namespace App\Http\Controllers\AuthorizeApplets;

use App\Http\Controllers\Controller;
use App\Api\Controllers\CustomerApplets\WechatTicketController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\WechatThirdConfig;

class AppletsController extends Controller
{
    protected $customerAppletsAuthorizeAppid = "customerapplets_authorize_appids";

    //第三方开放平台appid
    protected $app_id;
    //第三方开放平台secret
    protected $secret;
    //第三方开放平台token
    protected $token;
    //第三方开放平台aes_key
    protected $aes_key;
    //域名
    protected $domain;
    //微信公众号的appid
    protected $weChatServerAppId;
    //微信公众号的秘钥
    protected $weChatServerSecret;

    public function __construct()
    {
        $model = new WechatThirdConfig();
        $data = $model->getInfo();
        $this->app_id = isset($data->app_id) ? $data->app_id : '';
        $this->secret  = isset($data->app_secret) ? $data->app_secret : '';
        $this->token   = isset($data->token) ? $data->token : '';
        $this->aes_key = isset($data->aes_key) ? $data->aes_key : '';
        $this->weChatServerAppId   = isset($data->weChatServerAppId) ? $data->weChatServerAppId : '';
        $this->weChatServerSecret  = isset($data->weChatServerSecret) ? $data->weChatServerSecret : '';
        $this->domain  = env('APP_URL');
    }

    public function getAuthorizerAccessToken($component_appid,$authorization_code){

        $component_access_token = Cache::get("component_access_token"); //第三方平台接口的调用凭据。令牌的获取是有限制的，每个令牌的有效期为 2 小时

        if(empty($component_access_token)){
            return ['status' => 202,'message' => 'component_access_token数据为空,请稍等10分钟再进行尝试'];
        }

        try {
            $componentTokenData = [
                'component_appid'    => $component_appid,
                'authorization_code' => $authorization_code
            ];

            $url    = 'https://api.weixin.qq.com/cgi-bin/component/api_query_auth?component_access_token='.$component_access_token;

            $result = $this->curl_post_https($url,json_encode($componentTokenData));

            $result = json_decode($result,true);
            Log::info("获取小程序信息-B");
            Log::info($result);

            if(!empty($result['authorization_info']) && isset($result['authorization_info'])){
                return ['status' => 200,'message' => '获取成功','data' => $result];
            }else{
                $errmsg = $this->getCodeMessage($result['errcode']);
                return ['status' => $result['errcode'],'message' => $errmsg ? $errmsg : $result['errcode'].":".$result['errmsg']];
            }
        } catch (\Exception $e) {
            return ['status' => 202,'message' => $e->getMessage().' | '.$e->getFile().' | '.$e->getLine()];
        }
    }

    public function curl_post_https($url, $data = "", $header=""){
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
        if(!empty($header)){
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_HEADER, 0);//返回response头部信息
        }
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); // 从证书中检查SSL加密算法是否存在
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        if(!empty($data)){
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        }
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $tmpInfo = curl_exec($curl); // 执行操作
        if (curl_errno($curl)) {
            return 'Errno'.curl_error($curl);//捕抓异常
        }
        curl_close($curl); // 关闭CURL会话
        return $tmpInfo; // 返回数据，json格式
    }

    /**
     * 授权小程序回调通知
     * @param $store_id
     *        门店id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function refundAuthUrl($store_id, Request $request)
    {
        $requestAllData    = $request->all();
        if (!empty($requestAllData)) {
            DB::table("customerapplets_stores")
                ->where('store_id', $store_id)
                ->where('type', 1)
                ->update(['created_step'   => 4]);
        }

        $getAuthorizerAccessTokenRes = $this->getAuthorizerAccessToken($this->app_id, $requestAllData['auth_code']);
        if ($getAuthorizerAccessTokenRes['status'] == 200) {
            $AuthorAppId = $getAuthorizerAccessTokenRes['data']['authorization_info']['authorizer_appid'];
            $storeAppletInfo = DB::table("merchant_store_appid_secrets")->where(['store_id' => $store_id])->first();
            Log::info("ly-授权小程序回调-入参");
            Log::info($store_id);
            Log::info($AuthorAppId);

            if (!empty($storeAppletInfo)) {
                DB::table("merchant_store_appid_secrets")->where(['store_id' => $store_id])->update([
                    'wechat_appid' => $AuthorAppId,
                    'updated_at'   => date("Y-m-d H:i:s", time())
                ]);
            } else {
                DB::table("merchant_store_appid_secrets")->insert([
                    'store_id'     => $store_id,
                    'wechat_appid' => $AuthorAppId,
                    'created_at'   => date("Y-m-d H:i:s", time()),
                    'updated_at'   => date("Y-m-d H:i:s", time()),
                ]);
            }
        }

        // $appletInfo = DB::table($this->customerAppletsAuthorizeAppid)->where(['t_appid' => $this->app_id,'applet_type' => 1,'AuthorizationCode' => $requestAllData['auth_code']])->first();
        // if (!empty($appletInfo)) {
        //     $AuthorAppId = $appletInfo->AuthorizerAppid;
        //     $storeAppletInfo = DB::table("merchant_store_appid_secrets")->where(['store_id' => $store_id])->first();
        //     if (!empty($storeAppletInfo)) {
        //         DB::table("merchant_store_appid_secrets")->where(['store_id' => $store_id])->update([
        //             'wechat_appid' => $AuthorAppId,
        //             'updated_at'   => date("Y-m-d H:i:s", time())
        //         ]);
        //     } else {
        //         DB::table("merchant_store_appid_secrets")->insert([
        //             'store_id'     => $store_id,
        //             'wechat_appid' => $AuthorAppId,
        //             'created_at'   => date("Y-m-d H:i:s", time()),
        //             'updated_at'   => date("Y-m-d H:i:s", time()),
        //         ]);
        //     }
        // }
        return view("webH5Mobile.authorizeSuccess");
    }

    /**
     * 输出授权小程序页面
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view("webH5Mobile.authorize");
    }

    /**
     * 输出创建小程序页面
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createApplets()
    {
        return view("webH5Mobile.createApplets");
    }

    /**
     * 输出小程序管理列表
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function appletsList()
    {
        return view("webH5Mobile.appletList");
    }
}
