<?php

namespace App\Http\Controllers\AuthorizeApplets;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FaceSearch extends Controller
{
    /**
    * 输出刷脸余额查询页面
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function FaceSearch()
    {
        return view("FaceSearchPage.FaceSearch");
    }
}
