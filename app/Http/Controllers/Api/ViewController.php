<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

/**
 * Created by PhpStorm.
 * User: daimingkang
 * Date: 2018/7/4
 * Time: 上午11:33
 */
class ViewController extends Controller
{
    //微信
    //易生数科
    public function easySkPayeducation()
    {

        return view('easyskpay.school.weixin.payeducation');
    }
    public function easySkPayrecord()
    {

        return view('easyskpay.school.weixin.payrecord');
    }
    public function easySkIndex()
    {

        return view('easyskpay.school.weixin.index');
    }
    public function easySkStudentManage()
    {

        return view('easyskpay.school.weixin.studentManage');
    }
    public function easySkAddStudent()
    {

        return view('easyskpay.school.weixin.addStudent');
    }
    public function easySkEditStudent()
    {

        return view('easyskpay.school.weixin.editStudent');
    }
    //易生
    public function easyPayeducation()
    {

        return view('easypay.school.weixin.payeducation');
    }
    public function easyPayrecord()
    {

        return view('easypay.school.weixin.payrecord');
    }
    public function easyIndex()
    {

        return view('easypay.school.weixin.index');
    }
    public function easyStudentManage()
    {

        return view('easypay.school.weixin.studentManage');
    }
    public function easyAddStudent()
    {

        return view('easypay.school.weixin.addStudent');
    }
    public function easyEditStudent()
    {

        return view('easypay.school.weixin.editStudent');
    }
    //支付宝

    //易生数科
    public function easySkAliPayeducation()
    {

        return view('easyskpay.school.alipay.payeducation');
    }
    public function easySkAliPayrecord()
    {

        return view('easyskpay.school.alipay.payrecord');
    }

    public function easySkAliIndex()
    {

        return view('easyskpay.school.alipay.index');
    }
    public function easySkAliStudentManage()
    {

        return view('easyskpay.school.alipay.studentManage');
    }
    public function easySkAliAddStudent()
    {

        return view('easyskpay.school.alipay.addStudent');
    }
    public function easySkAliEditStudent()
    {

        return view('easypay.school.alipay.editStudent');
    }
    //易生
    public function easyAliPayeducation()
    {

        return view('easypay.school.alipay.payeducation');
    }
    public function easyAliPayrecord()
    {

        return view('easypay.school.alipay.payrecord');
    }

    public function easyAliIndex()
    {

        return view('easypay.school.alipay.index');
    }
    public function easyAliStudentManage()
    {

        return view('easypay.school.alipay.studentManage');
    }
    public function easyAliAddStudent()
    {

        return view('easypay.school.alipay.addStudent');
    }
    public function easyAliEditStudent()
    {

        return view('easypay.school.alipay.editStudent');
    }


}