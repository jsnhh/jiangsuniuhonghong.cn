<?php

namespace App\Http\Controllers\Qr;

use App\Api\Controllers\Config\AlipayIsvConfigController;
use App\Api\Controllers\Config\AllinPayConfigController;
use App\Api\Controllers\Config\EasyPayConfigController;
use App\Api\Controllers\Config\EasySkPayConfigController;
use App\Api\Controllers\Config\PayWaysController;
use App\Api\Controllers\Config\PostPayConfigController;
use App\Api\Controllers\Config\VbillConfigController;
use App\Api\Controllers\Config\WeixinConfigController;
use App\Api\Controllers\Config\YinshengConfigController;
use App\Api\Controllers\Vbill\PayController;
use App\Http\Controllers\Controller;
use App\Models\EasypayStoresImages;
use App\Models\QrCodeHb;
use App\Models\QrListInfo;
use App\Models\QrPayInfo;
use App\Models\Store;
use App\Models\TfStore;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;


class QrController extends Controller
{
    //多码合一
    public function qr(Request $request)
    {
        //可以传门店、收银员、支付渠道、
        $no = $request->get('no');//码编号
        $store_id = $request->get('store_id', '');//门店ID
        $merchant_id = $request->get('merchant_id', '');//收银员
        $company = $request->get('company', '');//支付渠道公司
        $other_no = $request->get('other_no', '');//收银员
        $notify_url = $request->get('notify_url', '');//收银员
        $voice_type = $request->get('voiceType', ''); //

        $url = $request->url();
        $is_https = substr($url, 0, 5);
        if ($is_https != "https") {
            $url = $request->server();
            return redirect('https://' . $url['SERVER_NAME'] . $url['REQUEST_URI']);
        }

        //门店ID 为空 编号不能为空
        if ($store_id == "") {
            //编号不能为空
            if ($no == "") {
                $message = "空码编号或者门店ID不能为空";
                return view('errors.page_errors', compact('message'));
            }

            $qr = $no;
            if (!$qr) {
                $qr = QrListInfo::where('code_num', $no)
                    ->select('store_id', 'merchant_id')
                    ->first();
            }

            if (!$qr) {
                $message = "码没有绑定激活";
                return view('errors.page_errors', compact('message'));
            }

            $store_id = $qr->store_id;
            $merchant_id = $qr->merchant_id;
        }

        if ($store_id) {
            $pay_type = "unionpay";
            //判断是不是微信
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
                $pay_type = 'weixin';
            }

            //判断是不是支付宝
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'AlipayClient') !== false) {
                $pay_type = 'alipay';
            }

            //判断是不是翼支付
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'Bestpay') !== false) {
                $pay_type = 'Bestpay';
            }

            //判断是不是京东
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'WalletClient') !== false || strpos($_SERVER['HTTP_USER_AGENT'], 'JDJR-App') !== false) {
                $pay_type = 'jd';
            }

            if (strpos($_SERVER['HTTP_USER_AGENT'], 'WalletClient') !== false || strpos($_SERVER['HTTP_USER_AGENT'], 'jdapp') !== false) {
                $pay_type = 'jd';
            }

            $where = [];
            $where[] = ['ways_source', '=', $pay_type];
            $where[] = ['store_id', '=', $store_id];

            //缓存
            $store = Cache::get($store_id . 'qr-pay');
            if (!$store) {
                $store = Store::where('store_id', $store_id)
                    ->select('config_id', 'user_id', 'pid', 'merchant_id', 'store_short_name', 'store_address')
                    ->first();

                Cache::put($store_id . 'qr-pay', $store, 1000);
            }

            if (!$store) {
                $message = "门店不存在";

                return view('errors.page_errors', compact('message'));
            }

            $store_pid = $store->pid;
            $user_id = $store->user_id;
            $obj_ways = new PayWaysController();
            $StorePayWay = $obj_ways->ways_source($pay_type, $store_id, $store_pid);

            $user = User::where('id', $user_id)->select('id', 'is_delete')->first();
            if (!$user) {
                $message = "业务员被删除，请联系归属业务员";

                return view('errors.page_errors', compact('message'));
            }

            if ($user->is_delete) {
                $message = "业务员被删除，请联系归属业务员";

                return view('errors.page_errors', compact('message'));
            }

            if ($company) {
                $StorePayWay = $obj_ways->company_ways_source($company, $pay_type, $store_id, $store_pid);
            }

            if (!$StorePayWay) {
                $message = "没有找到合适的支付方式";

                return view('errors.page_errors', compact('message'));
            }

            $ways_type = $StorePayWay->ways_type;

            //商户id为空
            if ($merchant_id == "") {
                $merchant_id = $store->merchant_id;
            }

            //配置
            $state = [
                'store_id' => $store_id,
                'store_name' => $store->store_short_name,
                'store_address' => $store->store_address,
                'config_id' => $store->config_id,
                'merchant_id' => $merchant_id,
                'auth_type' => '02',
                'other_no' => $other_no,
                'notify_url' => $notify_url,
                'store_pid' => $store_pid
            ];
            if ($voice_type) $state['voice_type'] = $voice_type;

            //支付宝
            if ($pay_type == 'alipay') {
                $isvconfig = new AlipayIsvConfigController();
                $config = $isvconfig->AlipayIsvConfig($store->config_id);

                //官方支付宝
                if ($ways_type == 1000) {
                    $state['bank_type'] = 'alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //网商支付宝
                if ($ways_type == 3001) {
                    $state['bank_type'] = 'mbalipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //京东支付支付宝
                if ($ways_type == 6001) {
                    $state['bank_type'] = 'jdalipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //新大陆支付宝
                if ($ways_type == 8001) {
                    $state['bank_type'] = 'nlalipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //会员宝支付宝
                if ($ways_type == 9001) {
                    $state['bank_type'] = 'halipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //联拓富支付支付宝
                if ($ways_type == 10001) {
                    $state['bank_type'] = 'lftalipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //富有支付宝
                if ($ways_type == 11001) {
                    $state['bank_type'] = 'fuioualipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //哆啦宝支付宝
                if ($ways_type == 15001) {
                    $state['bank_type'] = 'dlb_alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //传化支付宝
                if ($ways_type == 12001) {
                    $state['bank_type'] = 'tfpay_alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //随行付支付宝
                if ($ways_type == 13001) {
                    $state['bank_type'] = 'vbill_alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //汇付-支付宝
                if ($ways_type == 18001) {
                    $state['bank_type'] = 'huipay_alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;
//                    Log::info('alipay_url: '.$code_url);

                    return redirect($code_url);
                }

                //随行付a支付宝
                if ($ways_type == 19001) {
                    $state['bank_type'] = 'vbilla_alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //海科融通 支付宝
                if ($ways_type == 22001) {
                    $state['bank_type'] = 'hkrt_alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //易生支付 支付宝
                if ($ways_type == 21001) {
                    $state['bank_type'] = 'easypay_alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //联动优势支付宝
                if ($ways_type == 5001) {
                    $state['bank_type'] = 'linkage_alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //直付通支付宝
                if ($ways_type == 16002) {
                    $state['bank_type'] = 'zft_alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //葫芦天下支付宝
                if ($ways_type == 23001) {
                    $state['bank_type'] = 'hltx_alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //威富通 支付宝
                if ($ways_type == 27001) {
                    $state['bank_type'] = 'wftpay_alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //汇旺财 支付宝
                if ($ways_type == 28001) {
                    $state['bank_type'] = 'hwcpay_alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //银盛 支付宝
                if ($ways_type == 14001) {
                    $state['bank_type'] = 'yinsheng_alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;

                    return redirect($code_url);
                }

                //钱方 支付宝
                if ($ways_type == 24001) {
                    $state['bank_type'] = 'qfpay_alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;
                    return redirect($code_url);
                }

                //邮驿付 支付宝
                if ($ways_type == 29001) {
                    $state['bank_type'] = 'postpay_alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;
                    return redirect($code_url);
                }

                //易生数科 支付宝
                if ($ways_type == 32001) {
                    $state['bank_type'] = 'easyskpay_alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;
                    return redirect($code_url);
                }

                //通联支付 支付宝
                if ($ways_type == 33001) {
                    $state['bank_type'] = 'allinPay_alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;
                    return redirect($code_url);
                }

                //拉卡拉 支付宝
                if ($ways_type == 34001) {
                    $state['bank_type'] = 'lklpay_alipay';
                    $state = \App\Common\TransFormat::encode($state);
                    $app_auth_url = $config->alipay_app_authorize;
                    $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;
                    return redirect($code_url);
                }


            }

            //微信
            if ($pay_type == 'weixin') {
                $config = new WeixinConfigController();
                $WeixinConfig = $config->weixin_config_obj($store->config_id);
                if (!$WeixinConfig) {
                    $message = "微信配置不存在~";
                    return view('errors.page_errors', compact('message'));
                }
                $config_type = $WeixinConfig->config_type;

                $weixin_merchant_obj = $config->weixin_merchant($store->store_id);

                $code_url = '';
                //官方微信支付
                if ($ways_type == 2000) {
                    $state['bank_type'] = 'weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;
                    if ($weixin_merchant_obj && $weixin_merchant_obj->is_profit_sharing) {
                        $state['is_profit_sharing'] = true;
                    }

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //官方微信支付a
                if ($ways_type == 4000) {
                    $state['bank_type'] = 'weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    $weixin_merchant_obj = $config->weixina_merchant($store->store_id);
                    if ($weixin_merchant_obj && $weixin_merchant_obj->is_profit_sharing) {
                        $state['is_profit_sharing'] = true;
                    }

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/weixinopen/oautha?state=' . $state);

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/weixin/oautha?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //快钱微信支付
                if ($ways_type == 3002) {
                    //走想用渠道
                    $array = [
                        'https://dl.shouqianloupay.cn',
                        'https://x.xdspay.com',
                        'https://pay.ycfgd.com',
                        'https://pay.umxnt.com',
                        'https://pay.shouqupay.com',
                        'https://ss.tonlot.com',
                        'https://yb.xiangyongpay.com',
                        'https://pay.fuya18.com',
                        'https://pay.mynkj.cn',
                        'https://yh.yihoupay.com',
                        'https://pay.hsfpay.com',
                        'https://yf.hzbinfan.com',
                        'https://fyb.umxnt.com',
                        'https://pay.saomafupay.com',
                        'https://pay.zhekoulou.com',
                        'https://fu.shualianfu.wang',
                        'https://x.woyoupos.com',
                        'https://hsf.umxnt.com',
                        'https://msf.umxnt.com',
                        'https://pay.shualianpay.com.cn',
                        'https://www.s1spay.com',
                        'https://pay.shanghaiyif.com',
                        'https://pay.dgjgd168.com',
                        'https://pay.judianbang1688.com',
                        'https://www.yunsoyi.cn',
                        'https://yunfu.yuzewangluokeji.com',
                        'https://pay.wzhy815.com',
                        'https://p.unistoneits.com',
                        'https://pay.xiangyongkeji.cn',
                        'https://pay.baidexun.com',
                        'https://x.thewind.com.cn',
                        'https://qht.qdshsh.com',
                        'https://smile.hongzhoukeji.com',
                        'https://ririfu.shishifuba.com',
                        'https://payment.yinboxx.com',
                        'https://pay.ruoxinpay.com',
                        'https://pay.mefancy.cn',
                        'https://pay.yunmuzf.com',
                        'https://pay.kanyiyan.net',
                        'https://kzf.xiangyongpay.com',
                        'https://b.lefu.run',
                        'https://b.ymgyee.cn',
                        'https://pay.zcxxkj.top',
                        'https://pay.qingsongfu.net',
                        'https://pay.slzfsh.com',
                        'https://pay.caochaochao.com',
                        'https://sm.pinyougaoxiao.com',
                        'https://pay.mfbisv.com',
                        'https://pays.hnsxpay.com',
                        'https://sly.umxnt.com',
                        'https://pay.qiaobei.co',
                        'https://sm.pinyougaoxiao.com',
                        'https://pay.huachengxulong.com',
                        'https://cps.afpay.cn',
                        'https://515151ufo.cn'
                    ];

                    if (in_array(url(''), $array)) {
                        //看下配置是不是想用网商的是的话就去想用那边拿openID
                        $config_type = '3';//需要使用想用网商微信的其他独立平台
                    }

                    $state['bank_type'] = 'mybank_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } elseif ($config_type == "3") {
                        //跳转到想用去获取openid
                        $state['callback_url'] = url('/api/mybank/weixin/pay_view');
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = 'https://x.umxnt.com/api/mybank/weixin/oauth_openid?state=' . $state;

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/mybank/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //京东微信支付
                if ($ways_type == 6002) {
                    $state['bank_type'] = 'jd_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/jd/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //新大陆微信支付
                if ($ways_type == 8002) {
                    $state['bank_type'] = 'nl_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/newland/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //和融通微信支付
                if ($ways_type == 9002) {
                    $array = [
                        'https://pay.ycfgd.com',
                        'https://pay.umxnt.com',
                        'https://pay.shouqupay.com',
                        'https://ss.tonlot.com',
                        'https://yb.xiangyongpay.com',
                        'https://pay.fuya18.com',
                        'https://pay.mynkj.cn',
                    ];
                    //走想用渠道的商户
                    if (in_array(url(''), $array)) {
                        //看下配置是不是想用和融通的是的话就去想用那边拿openID
                        //$StorePayWay

                        //以前用的还是想用的渠道
                        $created_at = $StorePayWay->created_at;
                        if ($created_at < '2019-06-21 12:00:00') {
                            $config_type = '3';//需要使用想用和融通微信的其他独立平台
                        } else {
                            //用和融通的渠道
                        }
                    }

                    $state['bank_type'] = 'h_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } elseif ($config_type == "3") {
                        //跳转到想用去获取openid
                        $state['callback_url'] = url('/api/huiyuanbao/weixin/pay_view');
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = 'https://pay.umxnt.com/api/huiyuanbao/weixin/oauth_openid?state=' . $state;

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/huiyuanbao/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //微信支付
                if ($ways_type == 10002) {
                    $state['bank_type'] = 'ltf_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/ltf/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //富友微信支付
                if ($ways_type == 11002) {
                    $state['bank_type'] = 'fuioy_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/fuiou/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //哆啦宝微信支付
                if ($ways_type == 15002) {
                    $state['bank_type'] = 'dlb_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;
                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/dlb/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/dlb/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //传化微信支付
                if ($ways_type == 12002) {
                    $state['bank_type'] = 'tfpay_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //走想用渠道
                    $array = [
                        'https://dl.shouqianloupay.cn',
                        'https://x.xdspay.com',
                        'https://pay.ycfgd.com',
                        'https://pay.umxnt.com',
                        'https://pay.shouqupay.com',
                        'https://ss.tonlot.com',
                        'https://yb.xiangyongpay.com',
                        'https://pay.fuya18.com',
                        'https://pay.mynkj.cn',
                        'https://yh.yihoupay.com',
                        'https://pay.hsfpay.com',
                        'https://yf.hzbinfan.com',
                        'https://fyb.umxnt.com',
                        'https://pay.saomafupay.com',
                        'https://pay.zhekoulou.com',
                        'https://fu.shualianfu.wang',
                        'https://x.woyoupos.com',
                        'https://hsf.umxnt.com',
                        'https://msf.umxnt.com',
                        'https://pay.shualianpay.com.cn',
                        'https://www.s1spay.com',
                        'https://pay.shanghaiyif.com',
                        'https://pay.dgjgd168.com',
                        'https://pay.judianbang1688.com',
                        'https://www.yunsoyi.cn',
                        'https://yunfu.yuzewangluokeji.com',
                        'https://pay.wzhy815.com',
                        'https://p.unistoneits.com',
                        'https://pay.xiangyongkeji.cn',
                        'https://pay.baidexun.com',
                        'https://x.thewind.com.cn',
                        'https://qht.qdshsh.com',
                        'https://smile.hongzhoukeji.com',
                        'https://ririfu.shishifuba.com',
                        'https://payment.yinboxx.com',
                        'https://pay.ruoxinpay.com',
                        'https://pay.mefancy.cn',
                        'https://pay.yunmuzf.com',
                        'https://pay.kanyiyan.net',
                        'https://kzf.xiangyongpay.com',
                        'https://b.lefu.run',
                        'https://b.ymgyee.cn',
                        'https://pay.zcxxkj.top',
                        'https://pay.qingsongfu.net',
                        'https://pay.slzfsh.com',
                        'https://pay.caochaochao.com',
                        'https://sm.pinyougaoxiao.com',
                        'https://pay.mfbisv.com',
                        'https://pays.hnsxpay.com',
                        'https://sly.umxnt.com',
                        'https://pay.qiaobei.co',
                        'https://sm.pinyougaoxiao.com',
                        'https://pay.huachengxulong.com',
                        'https://cps.afpay.cn',
                        'https://515151ufo.cn'
                    ];

                    $qd = 1;
                    $TfStore = TfStore::where('store_id', $store_id)
                        ->select('qd')
                        ->first();
                    if ($TfStore) {
                        $qd = $TfStore->qd;
                    }
                    $state['qd'] = $qd;

                    if (in_array(url(''), $array) && $qd == 1) {
                        //看下配置是不是想用网商的是的话就去想用那边拿openID
                        $config_type = '3';//需要使用想用网商微信的其他独立平台
                    }

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/tfpay/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } elseif ($config_type == "3") {
                        //跳转到想用去获取openid
                        $state['callback_url'] = url('/api/tfpay/weixin/pay_view');
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = 'https://x.umxnt.com/api/mybank/weixin/oauth_openid?state=' . $state;

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/tfpay/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //随行付微信支付
                if ($ways_type == 13002) {
                    $state['bank_type'] = 'vbill_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/vbill/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } elseif ($config_type == "3") {
                        //跳转到想用去获取openid
                        $state['callback_url'] = url('/api/vbill/weixin/pay_view');
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = 'https://x.umxnt.com/api/vbill/weixin/oauth_openid?state=' . $state;

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/vbill/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //汇付-微信支付
                if ($ways_type == 18002) {
                    $state['bank_type'] = 'huipay_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/huipay/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } elseif ($config_type == "3") {
                        //跳转到想用去获取openid
                        $state['callback_url'] = url('/api/huipay/weixin/pay_view');
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = 'https://x.umxnt.com/api/huipay/weixin/oauth_openid?state=' . $state;

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/huipay/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //随行付a微信支付
                if ($ways_type == 19002) {
                    $state['bank_type'] = 'vbilla_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/vbill/weixinopen/oautha?state=' . $state);

                        return redirect($code_url);
                    } elseif ($config_type == "3") {
                        //跳转到想用去获取openid
                        $state['callback_url'] = url('/api/vbill/weixin/pay_viewa');
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = 'https://x.umxnt.com/api/vbill/weixin/oauth_openida?state=' . $state;

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/vbill/weixin/oautha?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //海科融通 微信支付
                if ($ways_type == 22002) {
                    $state['bank_type'] = 'hkrt_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/hkrt/weixinopen/oautha?state=' . $state);

                        return redirect($code_url);
                    } elseif ($config_type == "3") {
                        //跳转到想用去获取openid
                        $state['callback_url'] = url('/api/hkrt/weixin/pay_viewa');
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = 'https://x.umxnt.com/api/hkrt/weixin/oauth_openida?state=' . $state;

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/hkrt/weixin/oautha?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //易生支付 微信支付
                if ($ways_type == 21002) {
                    $state['bank_type'] = 'easypay_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;
                    $easyPayStoresImages = EasypayStoresImages::where('store_id', $store_id)->select('new_config_id')->first();
                    if ($easyPayStoresImages) {
                        $state['config_id'] = $easyPayStoresImages->new_config_id;
                    }

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/easypay/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } elseif ($config_type == "3") {
                        //跳转到想用去获取openid
                        $state['callback_url'] = url('/api/easypay/weixin/pay_viewa');
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = 'https://x.umxnt.com/api/easypay/weixin/oauth_openida?state=' . $state;

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/easypay/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //葫芦天下
                if ($ways_type == 23002) {
                    $manager = new \App\Api\Controllers\Hltx\ManageController();
                    $hltx_merchant = $manager->pay_merchant($store_id, $store_pid);

                    $qd = $hltx_merchant->qd;
                    $state['bank_type'] = 'hltx_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;
                    $state['qd'] = $qd;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/hltx/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //联动优势 微信支付
                if ($ways_type == 5002) {
                    $state['bank_type'] = 'linkage_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/linkage/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } elseif ($config_type == "3") {
                        //跳转到想用去获取openid
                        $state['callback_url'] = url('/api/linkage/weixin/pay_viewa');
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = 'https://x.umxnt.com/api/linkage/weixin/oauth_openida?state=' . $state;

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/linkage/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //威富通 微信支付
                if ($ways_type == 27002) {
                    $state['bank_type'] = 'wftpay_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/wftpay/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } elseif ($config_type == "3") {
                        //跳转到想用去获取openid
                        $state['callback_url'] = url('/api/wftpay/weixin/pay_viewa');
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = 'https://x.umxnt.com/api/wftpay/weixin/oauth_openida?state=' . $state;

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/wftpay/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //汇旺财 微信支付
                if ($ways_type == 28002) {
                    $state['bank_type'] = 'hwcpay_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/hwcpay/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } elseif ($config_type == "3") {
                        //跳转到想用去获取openid
                        $state['callback_url'] = url('/api/hwcpay/weixin/pay_viewa');
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = 'https://x.umxnt.com/api/hwcpay/weixin/oauth_openida?state=' . $state;

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/hwcpay/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //银盛 微信支付
                if ($ways_type == 14002) {
                    $state['bank_type'] = 'yinsheng_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/ysepay/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } elseif ($config_type == "3") {
                        //跳转到想用去获取openid
                        $state['callback_url'] = url('/api/ysepay/weixin/pay_viewa');
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = 'https://x.umxnt.com/api/ysepay/weixin/oauth_openida?state=' . $state;

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/ysepay/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //钱方 微信支付
                if ($ways_type == 24002) {
                    $state['bank_type'] = 'qfpay_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/qfpay/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } elseif ($config_type == "3") {
                        //跳转到想用去获取openid
                        $state['callback_url'] = url('/api/qfpay/weixin/pay_viewa');
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = 'https://x.umxnt.com/api/qfpay/weixin/oauth_openida?state=' . $state;

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/qfpay/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //邮驿付支付 微信支付
                if ($ways_type == 29002) {
                    $state['bank_type'] = 'postpay_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/postpay/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } elseif ($config_type == "3") {
                        //跳转到想用去获取openid
                        $state['callback_url'] = url('/api/postpay/weixin/pay_viewa');
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = 'https://x.umxnt.com/api/postpay/weixin/oauth_openida?state=' . $state;

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/postpay/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //易生数科 微信支付
                if ($ways_type == 32002) {
                    $state['bank_type'] = 'easyskpay_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/easyskpay/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } elseif ($config_type == "3") {
                        //跳转到想用去获取openid
                        $state['callback_url'] = url('/api/easyskpay/weixin/pay_viewa');
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = 'https://x.umxnt.com/api/easyskpay/weixin/oauth_openida?state=' . $state;

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/easyskpay/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //通联支付 微信支付
                if ($ways_type == 33002) {
                    $state['bank_type'] = 'allinPay_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/allinPay/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } elseif ($config_type == "3") {
                        //跳转到想用去获取openid
                        $state['callback_url'] = url('/api/allinPay/weixin/pay_viewa');
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = 'https://x.umxnt.com/api/allinPay/weixin/oauth_openida?state=' . $state;

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/allinPay/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

                //拉卡拉 微信支付
                if ($ways_type == 34002) {
                    $state['bank_type'] = 'lklpay_weixin';
                    $state['scope_type'] = 'snsapi_base';
                    $state['config_id'] = $store->config_id;

                    //开放平台代替授权
                    if ($config_type == "2") {
                        $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
                        $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/lklpay/weixinopen/oauth?state=' . $state);

                        return redirect($code_url);
                    } elseif ($config_type == "3") {
                        //跳转到想用去获取openid
                        $state['callback_url'] = url('/api/lklpay/weixin/pay_viewa');
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = 'https://x.umxnt.com/api/lklpay/weixin/oauth_openida?state=' . $state;

                        return redirect($code_url);
                    } //服务商特约
                    else {
                        $state = \App\Common\TransFormat::encode($state);
                        $code_url = url('api/lklpay/weixin/oauth?state=' . $state);

                        return redirect($code_url);
                    }
                }

            }


            //京东
            if ($pay_type == 'jd') {
                //京东金融京东支付
                if ($ways_type == 6003) {
                    $data = $state;
                    return view('jd.jd', compact('data'));
                }

                //京东金融京东支付
                if ($ways_type == 15003) {
                    $data = $state;
                    return view('dlb.jd', compact('data'));
                }

                //威富通 京东支付
                if ($ways_type == 27003) {
                    $data = $state;
                    return view('wftpay.jd', compact('data'));
                }

                //汇旺财 京东支付
                if ($ways_type == 28003) {
                    $data = $state;
                    return view('hwcpay.jd', compact('data'));
                }

            }

            //银联
            if ($pay_type == "unionpay") {
                //随行付支付 银联
                if ($ways_type == 13004) {
                    $userAuthCode = $request->get('userAuthCode');
                    if ($userAuthCode) {
                        $config = new VbillConfigController();
                        $vbill_config = $config->vbill_config($store->config_id);
                        if (!$vbill_config) {
                            $message = "随行付配置不存在请检查配置~";

                            return view('errors.page_errors', compact('message'));
                        }
                        $userAgent = $_SERVER['HTTP_USER_AGENT'];
                        $matches = array();
                        preg_match_all('/UnionPay\/[^ ]+\s[a-zA-Z0-9]+/', $userAgent, $matches);
                        $paymentApp = $matches[0][0];
                        $un = [
                            'userAuthCode' => $userAuthCode,
                            'paymentApp' => $paymentApp,
                            'orgId' => $vbill_config->orgId,
                            'privateKey' => $vbill_config->privateKey,
                            'sxfpublic' => $vbill_config->sxfpublic
                        ];
                        $obj = new PayController();
                        $re = $obj->get_un_openid($un);

                        $open_id = isset($re['data']['respData']['data']) ? $re['data']['respData']['data'] : "";
                        if ($open_id == "") {
                            $message = "获取银联用户ID失败请稍后再试~";

                            return view('errors.page_errors', compact('message'));
                        }

                        $state['open_id'] = $open_id;
                    } else {
                        $url = $request->url() . '?store_id=' . $store_id;
                        $url = urlencode($url);
                        $url = "https://qr.95516.com/qrcGtwWeb-web/api/userAuth?version=1.0.0&redirectUrl=" . $url;

                        return redirect($url);
                    }
                    $data = $state;

                    return view('vbill.un', compact('data'));
                }

                //易生云闪付
                if ($ways_type == 21004) {
                    $userAuthCode = $request->get('userAuthCode');
                    if ($userAuthCode) {
                        $config = new EasyPayConfigController();
                        $easy_config = $config->easypay_config($store->config_id);
                        if (!$easy_config) {
                            $message = "易生配置不存在请检查配置~";

                            return view('errors.page_errors', compact('message'));
                        }
                        $easyskpay_merchant = $config->easypay_merchant($store_id, $store_pid);
                        if (!$easyskpay_merchant) {
                            $message = "易生商户不存在请检查配置~";

                            return view('errors.page_errors', compact('message'));
                        }
                        $userAgent = $_SERVER['HTTP_USER_AGENT'];
                        $matches = array();
                        preg_match_all('/UnionPay\/[^ ]+\s[a-zA-Z0-9]+/', $userAgent, $matches);
                        $qrAppUpIdentifier = $matches[0][0];
                        $data = [
                            'qrAppUpIdentifier' => $qrAppUpIdentifier,
                            'qrUserAuthCode' => $userAuthCode,
                            'orgId' => $easy_config->channel_id,
                            'orgMercode' => $easyskpay_merchant->term_mercode,
                            'orgTermno' => $easyskpay_merchant->term_termcode,
                        ];
                        $PayController = new \App\Api\Controllers\EasyPay\PayController();
                        $res = $PayController->qrGetUserId($data);
                        if ($res['status'] == '1') {
                            $state['open_id'] = $res['data']['payerId'];
                            $data = $state;

                            return view('easypay.un', compact('data'));
                        } else {
                            $message = "获取银联用户ID失败请稍后再试~";

                            return view('errors.page_errors', compact('message'));
                        }
                    } else {
                        $url = $request->url() . '?store_id=' . $store_id;
                        $url = urlencode($url);
                        $url = "https://qr.95516.com/qrcGtwWeb-web/api/userAuth?version=1.0.0&redirectUrl=" . $url;
                        return redirect($url);
                    }

                }

                //邮驿付云闪付
                if ($ways_type == 29003) {
                    $userAuthCode = $request->get('userAuthCode');
                    if ($userAuthCode) {
                        $config = new PostPayConfigController();
                        $post_config = $config->post_pay_config($store->config_id);
                        if (!$post_config) {
                            $message = "邮驿付配置不存在请检查配置~";

                            return view('errors.page_errors', compact('message'));
                        }
                        $post_merchant = $config->post_pay_merchant($store_id, $store_pid);
                        if (!$post_merchant) {
                            return json_encode([
                                'status' => '2',
                                'message' => '邮驿付支付商户号不存在'
                            ]);
                        }
                        $userAgent = $_SERVER['HTTP_USER_AGENT'];
                        $matches = array();
                        preg_match_all('/UnionPay\/[^ ]+\s[a-zA-Z0-9]+/', $userAgent, $matches);
                        $payCode = $matches[0][0];
                        $data = [
                            'agetId' => $post_config->org_id,
                            'custId' => $post_merchant->cust_id,
                            'payCode' => $payCode,
                            'authCode' => $userAuthCode,
                            'trmNo' => $post_merchant->drive_no
                        ];
                        $PayController = new \App\Api\Controllers\PostPay\PayController();
                        $res = $PayController->get_un_openid($data);
                        if ($res['status'] == '1') {
                            $state['open_id'] = $res['data']['userId'];
                            $state['atqTag'] = $res['data']['atqTag'];
                            $data = $state;

                            return view('postpay.un', compact('data'));
                        } else {
                            $message = "获取银联用户ID失败请稍后再试~";

                            return view('errors.page_errors', compact('message'));
                        }
                    } else {
                        $url = $request->url() . '?store_id=' . $store_id;
                        $url = urlencode($url);
                        $url = "https://qr.95516.com/qrcGtwWeb-web/api/userAuth?version=1.0.0&redirectUrl=" . $url;
                        return redirect($url);
                    }

                }

                //随行付a支付 银联
                if ($ways_type == 19004) {
                    $userAuthCode = $request->get('userAuthCode');
                    Log::info('随行付a支付-银联-userAuthCode');
                    Log::info($userAuthCode);
                    if ($userAuthCode) {
                        $config = new VbillConfigController();
                        $vbill_config = $config->vbilla_config($store->config_id);
                        if (!$vbill_config) {
                            $message = "随行付a配置不存在请检查配置~";

                            return view('errors.page_errors', compact('message'));
                        }
                        $userAgent = $_SERVER['HTTP_USER_AGENT'];
                        $matches = array();
                        preg_match_all('/UnionPay\/[^ ]+\s[a-zA-Z0-9]+/', $userAgent, $matches);
                        $paymentApp = $matches[0][0];
                        $un = [
                            'userAuthCode' => $userAuthCode,
                            'paymentApp' => $paymentApp,
                            'orgId' => $vbill_config->orgId,
                            'privateKey' => $vbill_config->privateKey,
                            'sxfpublic' => $vbill_config->sxfpublic
                        ];
                        $obj = new PayController();
                        $re = $obj->get_un_openid($un);
                        Log::info('随行付a支付-银联-获取用户id');
                        Log::info($re);
                        $open_id = isset($re['data']['respData']['data']) ? $re['data']['respData']['data'] : "";
                        if ($open_id == "") {
                            $message = "获取银联用户ID失败请稍后再试~";

                            return view('errors.page_errors', compact('message'));
                        }

                        $state['open_id'] = $open_id;
                    } else {
                        $url = $request->url() . '?store_id=' . $store_id;
                        $url = urlencode($url);
                        $url = "https://qr.95516.com/qrcGtwWeb-web/api/userAuth?version=1.0.0&redirectUrl=" . $url;
                        Log::info('随行付a支付-银联主扫');
                        Log::info($url);
                        return redirect($url);
                    }
                    $data = $state;
                    Log::info($data);
                    return view('vbilla.un', compact('data'));
                }

                //葫芦银联
                if ($ways_type == 23004) {
                    $userAuthCode = $request->get('userAuthCode');
                    if ($userAuthCode) {
                        $manager = new \App\Api\Controllers\Hltx\ManageController();
                        $hltx_merchant = $manager->pay_merchant($store_id, $store_pid);
                        $qd = $hltx_merchant->qd;
                        $hltx_config = $manager->pay_config($store->config_id, $qd);
                        if (!$hltx_config) {
                            $message = "hl支付配置不存在请检查配置~";

                            return view('errors.page_errors', compact('message'));
                        }

                        $manager->init($hltx_config);
                        if (!$hltx_merchant || empty($hltx_merchant->mer_no)) {
                            return json_encode([
                                'status' => 2,
                                'message' => 'hl商户未成功开通通道!'
                            ]);
                        }
                        $merNo = $hltx_merchant->mer_no;
                        $re = $manager->get_openid($merNo, $userAuthCode);
                        if ($re['status'] == 0) {
                            $message = $re['message'];

                            return view('errors.page_errors', compact('message'));
                        }

                        $open_id = isset($re['data']['userId']) ? $re['data']['userId'] : "";
                        if ($open_id == "") {
                            $message = "获取银联用户ID失败请稍后再试~";

                            return view('errors.page_errors', compact('message'));
                        }

                        $state['open_id'] = $open_id;
                    } else {
                        $url = $request->url() . '?store_id=' . $store_id;
                        $url = urlencode($url);
                        $url = "https://qr.95516.com/qrcGtwWeb-web/api/userAuth?version=1.0.0&redirectUrl=" . $url;

                        return redirect($url);
                    }
                    $data = $state;

                    return view('hltx.un', compact('data'));
                }

                //联动优势 银联
                if ($ways_type == 5004) {
                    $respCode = $request->get('respCode'); //银联标识
                    if ($respCode == '00') { //获取用户信息成功
                        $userAuthCode = $request->get('userAuthCode'); //用户标识,银联二维码上传userAuthCode
                        if ($userAuthCode) {
                            $state['open_id'] = $userAuthCode;
                        } else {
                            $url = $request->url() . '?store_id=' . $store_id;
                            $url = urlencode($url);
                            $url = "https://qr.95516.com/qrcGtwWeb-web/api/userAuth?version=1.0.0&redirectUrl=" . $url;
                            return redirect($url);
                        }
                        $data = $state;

                        return view('linkage.un', compact('data'));
                    }

                    if ($respCode != '00' && $respCode != '34') { //获取用户信息失败
                        $message = "联动优势-云闪付-用户授权失败";
                        return view('errors.page_errors', compact('message'));
                    }
                }

                //威富通 银联
                if ($ways_type == 27004) {
                    $respCode = $request->get('respCode'); //银联标识
                    if ($respCode == '00') { //获取用户信息成功
                        $userAuthCode = $request->get('userAuthCode'); //用户标识,银联二维码上传userAuthCode
                        if ($userAuthCode) {
                            $state['open_id'] = $userAuthCode;
                        } else {
                            $url = $request->url() . '?store_id=' . $store_id;
                            $url = urlencode($url);
                            $url = "https://qr.95516.com/qrcGtwWeb-web/api/userAuth?version=1.0.0&redirectUrl=" . $url;
                            return redirect($url);
                        }
                        $data = $state;

                        return view('wftpay.un', compact('data'));
                    }

                    if ($respCode != '00' && $respCode != '34') { //获取用户信息失败
                        $message = "威富通-云闪付-用户授权失败";
                        return view('errors.page_errors', compact('message'));
                    }
                }

                //汇旺财 银联
                if ($ways_type == 28004) {
                    $respCode = $request->get('respCode'); //银联标识
                    if ($respCode == '00') { //获取用户信息成功
                        $userAuthCode = $request->get('userAuthCode'); //用户标识,银联二维码上传userAuthCode
                        if ($userAuthCode) {
                            $state['open_id'] = $userAuthCode;
                        } else {
                            $url = $request->url() . '?store_id=' . $store_id;
                            $url = urlencode($url);
                            $url = "https://qr.95516.com/qrcGtwWeb-web/api/userAuth?version=1.0.0&redirectUrl=" . $url;
                            return redirect($url);
                        }
                        $data = $state;

                        return view('hwcpay.un', compact('data'));
                    }

                    if ($respCode != '00' && $respCode != '34') { //获取用户信息失败
                        $message = "汇旺财-云闪付-用户授权失败";
                        return view('errors.page_errors', compact('message'));
                    }
                }

                //银盛 银联
                if ($ways_type == 14004) {
                    $userAuthCode = $request->get('userAuthCode');
                    if ($userAuthCode) {
                        $config = new YinshengConfigController();
                        $yinsheng_config = $config->yinsheng_config($store->config_id);
                        if (!$yinsheng_config) {
                            $message = "银盛配置不存在请检查配置~";

                            return view('errors.page_errors', compact('message'));
                        }
                        $yinsheng_merchant = $config->yinsheng_merchant($store_id, $store_pid);
                        if (!$yinsheng_merchant) {
                            return json_encode([
                                'status' => '2',
                                'message' => '银盛商户号不存在'
                            ]);
                        }
                        $userAgent = $_SERVER['HTTP_USER_AGENT'];
                        $matches = array();
                        preg_match_all('/UnionPay\/[^ ]+\s[a-zA-Z0-9]+/', $userAgent, $matches);
                        $appUpIdentifier = $matches[0][0];
                        $data = [
                            'partner_id' => $yinsheng_config->partner_id,
                            'userAuthCode' => $userAuthCode,
                            'appUpIdentifier' => $appUpIdentifier,
                        ];
                        $payController = new \App\Api\Controllers\YinSheng\PayController();
                        $res = $payController->get_user_id($data);
                        if ($res['status'] == '1') {
                            $state['open_id'] = $res['data']['userId'];
                            $data = $state;

                            return view('yinsheng.un', compact('data'));
                        } else {
                            $message = "获取银联用户ID失败请稍后再试~";

                            return view('errors.page_errors', compact('message'));
                        }
                    } else {
                        $url = $request->url() . '?store_id=' . $store_id;
                        $url = urlencode($url);
                        $url = "https://qr.95516.com/qrcGtwWeb-web/api/userAuth?version=1.0.0&redirectUrl=" . $url;
                        return redirect($url);
                    }
                }

                //易生数科云闪付
                if ($ways_type == 32003) {
                    $userAuthCode = $request->get('userAuthCode');
                    if ($userAuthCode) {
                        $config = new EasySkPayConfigController();
                        $easyskpay_config = $config->easyskpay_config($store->config_id);
                        if (!$easyskpay_config) {
                            $message = "易生数科配置不存在请检查配置~";

                            return view('errors.page_errors', compact('message'));
                        }
                        $easyskpay_merchant = $config->easyskpay_merchant($store_id, $store_pid);
                        if (!$easyskpay_merchant) {
                            return json_encode([
                                'status' => '2',
                                'message' => '易生数科支付商户号不存在'
                            ]);
                        }
                        $userAgent = $_SERVER['HTTP_USER_AGENT'];
                        $matches = array();
                        preg_match_all('/UnionPay\/[^ ]+\s[a-zA-Z0-9]+/', $userAgent, $matches);
                        $unionPayAppID = $matches[0][0];
                        //Log::info('支付标识--'.$matches[0][0]);
                        $data = [
                            'unionPayAppID' => $unionPayAppID,
                            'unionPayAuthCode' => $userAuthCode,
                            'org_id' => $easyskpay_config->org_id,
                            'mer_id' => $easyskpay_merchant->mer_id
                        ];
                        $PayController = new \App\Api\Controllers\EasySkPay\PayController();
                        $res = $PayController->getUnOpenid($data);
                        if ($res['status'] == '1') {
                            $state['open_id'] = $res['data']['bizData']['openId'];
                            $data = $state;

                            return view('easyskpay.un', compact('data'));
                        } else {
                            $message = "获取银联用户ID失败请稍后再试~";

                            return view('errors.page_errors', compact('message'));
                        }
                    } else {
                        $url = $request->url() . '?store_id=' . $store_id;
                        $url = urlencode($url);
                        $url = "https://qr.95516.com/qrcGtwWeb-web/api/userAuth?version=1.0.0&redirectUrl=" . $url;
                        return redirect($url);
                    }

                }

                //通联云闪付
                if ($ways_type == 33003) {
                    $userAuthCode = $request->get('userAuthCode');
                    if ($userAuthCode) {
                        $config = new AllinPayConfigController();
                        $allin_config = $config->allin_pay_config($store->config_id);
                        if (!$allin_config) {
                            $message = "通联配置不存在请检查配置~";

                            return view('errors.page_errors', compact('message'));
                        }
                        $allin_merchant = $config->allin_pay_merchant($store_id, $store_pid);
                        if (!$allin_merchant) {
                            $message = "易生商户不存在请检查配置~";

                            return view('errors.page_errors', compact('message'));
                        }
                        $userAgent = $_SERVER['HTTP_USER_AGENT'];
                        $matches = array();
                        preg_match_all('/UnionPay\/[^ ]+\s[a-zA-Z0-9]+/', $userAgent, $matches);
                        $qrAppUpIdentifier = $matches[0][0];
                        $data = [
                            'identify' => $qrAppUpIdentifier,
                            'authcode' => $userAuthCode,
                            'orgId' => $allin_config->org_id,
                            'appid' => $allin_merchant->appid,
                            'cusId' => $allin_config->cust_id,
                            'authtype' => '02'
                        ];
                        $PayController = new \App\Api\Controllers\AllinPay\PayController();
                        $res = $PayController->qrGetUserId($data);
                        if ($res['status'] == '1') {
                            $state['open_id'] = $res['data']['acct'];
                            $data = $state;
                            return view('allinpay.un', compact('data'));
                        } else {
                            $message = "获取银联用户ID失败请稍后再试~";

                            return view('errors.page_errors', compact('message'));
                        }
                    } else {
                        $url = $request->url() . '?store_id=' . $store_id;
                        $url = urlencode($url);
                        $url = "https://qr.95516.com/qrcGtwWeb-web/api/userAuth?version=1.0.0&redirectUrl=" . $url;
                        return redirect($url);
                    }

                }
            }

            $message = "您使用的客户端与要求不符~";
        } else {
            $message = "  二维码没有绑定激活";
        }
        return view('errors.page_errors', compact('message'));
    }


    public function qr_code_hb(Request $request)
    {
        $pay_type = "";
        $id = $request->get('id', '');//门店ID

        //判断是不是微信
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
            $pay_type = 'weixin';
        }

        //判断是不是支付宝
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'AlipayClient') !== false) {
            $pay_type = 'alipay';
        }

        //判断是不是翼支付
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'Bestpay') !== false) {
            $pay_type = 'Bestpay';
        }

        //判断是不是京东
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'WalletClient') !== false || strpos($_SERVER['HTTP_USER_AGENT'], 'JDJR-App') !== false) {
            $pay_type = 'jd';
        }

        if (strpos($_SERVER['HTTP_USER_AGENT'], 'WalletClient') !== false || strpos($_SERVER['HTTP_USER_AGENT'], 'jdapp') !== false) {
            $pay_type = 'jd';
        }

        $QrCodeHb = QrCodeHb::where('id', $id)->first();
        if (!$QrCodeHb) {
            $message = "  二维码ID不正确";
            return view('errors.page_errors', compact('message'));
        }

        if ($pay_type == "alipay") {
            return redirect($QrCodeHb->ali_url);
        } elseif ($pay_type == "weixin") {
            $url = $url = $QrCodeHb->wx_url;
            $url = "http://qr.topscan.com/api.php?text=" . $url;
            return redirect($url);
        } else {
            $url = url('/qr_code_hb?id=') . $id;

            //方式2
            $url = url('/qr_code_hb?id=') . $id;
            $url = "http://qr.topscan.com/api.php?text=" . $url;
            return redirect($url);
        }
    }


    public function cr(Request $request)
    {
        //可以传门店、收银员、支付渠道、
        $store_id = $request->get('store_id', '');//门店ID
        $template_id = $request->get('template_id', '');//模板id
        $store = Store::where('store_id', $store_id)
            ->select('config_id', 'user_id', 'pid', 'merchant_id', 'store_short_name', 'store_address')
            ->first();
        $state = [
            'store_id' => $store_id,
            'config_id' => $store->config_id,
            'store_name' => $store->store_short_name,
            'auth_type' => '04',
            'template_id' => $template_id,

        ];
        $isvconfig = new AlipayIsvConfigController();
        $config = $isvconfig->AlipayIsvConfig($store->config_id);
        $state = \App\Common\TransFormat::encode($state);
        $app_auth_url = $config->alipay_app_authorize;
        $code_url = $app_auth_url . '?app_id=' . $config->app_id . "&redirect_uri=" . $config->callback . '&scope=auth_base&state=' . $state;
        return redirect($code_url);
    }

    public function openidQr(Request $request)
    {

        $store_id = $request->get('store_id', '');//门店ID
        $url = $request->url();
        $is_https = substr($url, 0, 5);
        if ($store_id == "") {
            $message = "门店id不能为空";
            return view('errors.page_errors', compact('message'));
        }
        if ($is_https != "https") {
            $url = $request->server();
            return redirect('https://' . $url['SERVER_NAME'] . $url['REQUEST_URI']);
        }
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') == false) {
            $message = "请使用微信扫码";
            return view('errors.page_errors', compact('message'));
        }
        //缓存
        $store = Cache::get($store_id . 'qr-pay');
        if (!$store) {
            $store = Store::where('store_id', $store_id)
                ->select('config_id', 'user_id', 'pid', 'merchant_id', 'store_short_name', 'store_address')
                ->first();

            Cache::put($store_id . 'qr-pay', $store, 1000);
        }
        $state = [
            'store_id' => $store_id,
            'config_id' => $store->config_id,
            'bank_type' => 'getOpenid',
            'auth_type' => '02',
            'scope_type' => 'snsapi_base',

        ];

        $config = new WeixinConfigController();
        $WeixinConfig = $config->weixin_config_obj($store->config_id);

        if (!$WeixinConfig) {
            $message = "微信配置不存在";
            return view('errors.page_errors', compact('message'));

        }

        //开放平台代替授权
        if ($WeixinConfig->config_type == '2') {
            $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
            $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
            $state = \App\Common\TransFormat::encode($state);
            $code_url = url('api/weixinopen/oauth?state=' . $state);
        } //服务商特约
        else {
            $state = \App\Common\TransFormat::encode($state);
            $code_url = url('api/weixin/oauth?state=' . $state);
        }

        return redirect($code_url);
    }

    public function shoppingOpenidQr(Request $request)
    {


        $url = $request->url();
        $is_https = substr($url, 0, 5);
        if ($is_https != "https") {
            $url = $request->server();
            return redirect('https://' . $url['SERVER_NAME'] . $url['REQUEST_URI']);
        }
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') == false) {
            $message = "请使用微信扫码";
            return view('errors.page_errors', compact('message'));
        }
        $state = [
            'config_id' => '1234',
            'bank_type' => 'getShoppingOpenid',
            'auth_type' => '02',
            'scope_type' => 'snsapi_base',

        ];

        $config = new WeixinConfigController();
        $WeixinConfig = $config->weixin_config_obj('1234');

        if (!$WeixinConfig) {
            $message = "微信配置不存在";
            return view('errors.page_errors', compact('message'));

        }

        //开放平台代替授权
        if ($WeixinConfig->config_type == '2') {
            $state['authorizer_appid'] = $WeixinConfig->authorizer_appid;
            $state['authorizer_refresh_token'] = $WeixinConfig->authorizer_refresh_token;
            $state = \App\Common\TransFormat::encode($state);
            $code_url = url('api/weixinopen/oauth?state=' . $state);
        } //服务商特约
        else {
            $state = \App\Common\TransFormat::encode($state);
            $code_url = url('api/weixin/oauth?state=' . $state);
        }

        return redirect($code_url);
    }

}
