<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;

class ViewController extends Controller
{
    public function login()
    {
        return view('agent.login');
    }

    public function index()
    {
        return view('agent.index');
    }

    public function alipayconfirm()//支付宝配置
    {
        return view('agent.alipayconfirm');
    }

    public function wechatconfirm()//微信配置
    {
        return view('agent.wechatconfirm');
    }

    public function schoollist()//学校列表
    {
        return view('agent.schoollist');
    }

    public function agentlist()//代理商列表
    {
        return view('agent.agentlist');
    }
    public function home()//代理商列表
    {
        return view('agent.home');
    }

    public function waterlist()
    {
        return view('agent.waterlist');
    }

    public function qrcode()
    {
        return view('agent.qrcode');
    }

    public function seewater()
    {
        return view('agent.seewater');
    }

    public function payitem()
    {
        return view('agent.payitem');
    }

    public function addagent()
    {
        return view('agent.addagent');
    }

    public function examineschool()
    {
        return view('agent.examineschool');
    }

    public function editschool()
    {
        return view('agent.editschool');
    }

    public function forget()
    {
        return view('agent.forget');
    }

    public function storelist()
    {
        return view('agent.storelist');
    }

    public function seestore()
    {
        return view('agent.seestore');
    }
    public function editstore()
    {
        return view('agent.editstore');
    }

    //设备管理
    public function devicelist()
    {
        return view('agent.devicelist');
    }

    public function adddevice()
    {
        return view('agent.adddevice');
    }

    public function editdevice()
    {
        return view('agent.editdevice');
    }

    // 消息
    public function appmsg()
    {
        return view('agent.appmsg');
    }

    public function addappmsg()
    {
        return view('agent.addappmsg');
    }

    public function bannerlist()
    {
        return view('agent.bannerlist');
    }

    public function addbanner()
    {
        return view('agent.addbanner');
    }

    // 角色
    public function role()
    {
        return view('agent.role');
    }

    public function addrole()
    {
        return view('agent.addrole');
    }

    public function permissions()
    {
        return view('agent.permissions');
    }

    public function rolelist()
    {
        return view('agent.rolelist');
    }

    // 权限
    public function power()
    {
        return view('agent.power');
    }

    public function addpower()
    {
        return view('agent.addpower');
    }

    // 交易流水
    public function tradelist()
    {
        return view('agent.tradelist');
    }
    // 通道管理
    public function passway()
    {
        return view('agent.passway');
    }

    // 通道费率列表
    public function ratelist()
    {
        return view('agent.ratelist');
    }

    // 花呗分期流水列表
    public function flowerlist()
    {
        return view('agent.flowerlist');
    }

    // 分店管理
    public function branchshop()
    {
        return view('agent.branchshop');
    }

    public function addbranchdevice()
    {
        return view('agent.addbranchdevice');
    }

    //二维码统一管理
    public function qrcodemanage()
    {
        return view('agent.qrcodemanage');
    }

    //广告
    public function ad()
    {
        return view('agent.ad');
    }

    public function addad()
    {
        return view('agent.addad');
    }

    public function adsee()
    {
        return view('agent.adsee');
    }

    public function editad()
    {
        return view('agent.editad');
    }

    //赏金
    public function reward()
    {
        return view('agent.reward');
    }

    //提现
    public function putforward()
    {
        return view('agent.putforward');
    }

    //支付宝红包
    public function alipayred()
    {
        return view('agent.alipayred');
    }

    public function addalipayred()
    {
        return view('agent.addalipayred');
    }

    //收银插件
    public function shouyin()
    {
        return view('agent.shouyin');
    }

    public function addshouyin()
    {
        return view('agent.addshouyin');
    }

    //京东配置
    public function jdconfigure()
    {
        return view('agent.jdconfigure');
    }

    public function openpassway()
    {
        return view('agent.openpassway');
    }

    public function newworld()
    {
        return view('agent.newworld');
    }

    public function settlement()
    {
        return view('agent.settlement');
    }

    public function jdwhitebar()
    {
        return view('agent.jdwhitebar');
    }

    public function storeratelist()
    {
        return view('agent.storeratelist');
    }

    public function updata()
    {
        return view('agent.updata');
    }

    public function appconfig()
    {
        return view('agent.appconfig');
    }

    public function pushconfig()
    {
        return view('agent.pushconfig');
    }

    public function msgconfig()
    {
        return view('agent.msgconfig');
    }

    public function hrtconfig()
    {
        return view('agent.hrtconfig');
    }

    public function passwaysort()
    {
        return view('agent.passwaysort');
    }

    public function storeconfig()
    {
        return view('agent.storeconfig');
    }

    public function reconciliation()
    {
        return view('agent.reconciliation');
    }

    public function devicemanage()
    {
        return view('agent.devicemanage');
    }

    public function merchantnumber()
    {
        return view('agent.merchantnumber');
    }

    public function withdrawrecord()
    {
        return view('agent.withdrawrecord');
    }

    public function deviceconfig()
    {
        return view('agent.deviceconfig');
    }

    public function bound()
    {
        return view('agent.bound');
    }

    public function unbound()
    {
        return view('agent.unbound');
    }

    public function cashset()
    {
        return view('agent.cashset');
    }

    public function settlerecord()
    {
        return view('agent.settlerecord');
    }

    public function settledetail()
    {
        return view('agent.settledetail');
    }

    public function mqtt()
    {
        return view('agent.mqtt');
    }

    // 银盈通
    public function yytong()
    {
        return view('agent.yytong');
    }

    public function merchantmanage()
    {
        return view('agent.merchantmanage');
    }

    public function addstoretransfer()
    {
        return view('agent.addstoretransfer');
    }

    public function transactionlist()
    {
        return view('agent.transactionlist');
    }

    public function percode()
    {
        return view('agent.percode');
    }

    public function addpercode()
    {
        return view('agent.addpercode');
    }

    public function makemoney()
    {
        return view('agent.makemoney');
    }

    // 押金流水deposit
    public function depositwater()
    {
        return view('agent.depositwater');
    }

    public function depositacount()
    {
        return view('agent.depositacount');
    }

    public function cashwithdrawal()
    {
        return view('agent.cashwithdrawal');
    }

    public function fuyoumanage()
    {
        return view('agent.fuyoumanage');
    }

    public function logoconfig()
    {
        return view('agent.logoconfig');
    }

    public function fee()
    {
        return view('agent.fee');
    }

    public function addfee()
    {
        return view('agent.addfee');
    }

    public function agentrecovery()
    {
        return view('agent.agentrecovery');
    }

    public function dlbconfig()
    {
        return view('agent.dlbconfig');
    }

    public function appindexfunction()
    {
        return view('agent.appindexfunction');
    }

    public function appmyfunction()
    {
        return view('agent.appmyfunction');
    }

    public function apphead()
    {
        return view('agent.apphead');
    }

    public function addappindexfunction()
    {
        return view('agent.addappindexfunction');
    }

    public function addappmyfunction()
    {
        return view('agent.addappmyfunction');
    }

    public function zfthbfq()
    {
        return view('agent.zfthbfq');
    }

    public function flowerfq()
    {
        return view('agent.flowerfq');
    }

    public function merchantlist()
    {
        return view('agent.merchantlist');
    }

    public function bindcashier()
    {
        return view('agent.bindcashier');
    }

    public function editcashier()
    {
        return view('agent.editcashier');
    }

    public function tfconfig()
    {
        return view('agent.tfconfig');
    }

    //收款批量置顶
    public function editpaywaysort()
    {
        return view('agent.editpaywaysort');
    }

    //分润账单
    public function commissionlist()
    {
        return view('agent.commissionlist');
    }
    //桌号管理
    public function tableNumber()
    {
        return view('agent.tableNumber');
    }
    //商品分类
    public function commodityClass()
    {
        return view('agent.commodityClass');
    }
    //商品列表
    public function commodityList()
    {
        return view('agent.commodityList');
    }
    //商品订单
    public function commodityOeder()
    {
        return view('agent.commodityOeder');
    }
    //退款订单
    public function refundOrder()
    {
        return view('agent.refundOrder');
    }
    //微信代金券
    public function cashCoupon()
    {
        return view('agent.cashCoupon');
    }
    //微信代金券核销
    public function cashCoupondetailed()
    {
        return view('agent.cashCoupondetailed');
    }
    //支付宝代金券
    public function alipayCoupon()
    {
        return view('agent.alipayCoupon');
    }
    //支付宝代金券核销
    public function alipayCoupondetailed()
    {
        return view('agent.alipayCoupondetailed');
    }
}
