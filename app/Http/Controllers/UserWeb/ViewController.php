<?php

namespace App\Http\Controllers\UserWeb;

use App\Http\Controllers\Controller;

/**
 * Class ViewController
 * @package App\Http\Controllers\User
 */
class ViewController extends Controller
{
    public function index()
    {
        return view('userweb.index');
    }
    public function setupshop()
    {
        return view('userweb.setupshop');
    }
    public function applettemplate()
    {
        return view('userweb.applettemplate');
    }
    public function home()
    {
        return view('userweb.home');
    }
    public function interestedbuyers()
    {
        return view('userweb.interestedbuyers');
    }
    public function Exhibition()
    {
        return view('userweb.Exhibition');
    }
    public function smallprogram()
    {
        return view('userweb.smallprogram');
    }
    public function auditSuccess()
    {
        return view('userweb.auditSuccess');
    }
    public function WeChatshenheing()
    {
        return view('userweb.WeChatshenheing');
    }
    public function WeChatshenhefail()
    {
        return view('userweb.WeChatshenhefail');
    }
    public function WeChatshenhesuccess()
    {
        return view('userweb.WeChatshenhesuccess');
    }
    public function createApplet()
    {
        return view('userweb.createApplet');
    }
    public function contactus()
    {
        return view('userweb.contactus');
    }
    public function pagedetails()
    {
        return view('userweb.pagedetails');
    }
    public function wechatfabu()
    {
        return view('userweb.wechatfabu');
    }

    public function smallprogramfabu()
    {
        return view('userweb.smallprogramfabu');
    }
    public function Exhibitionfabu()
    {
        return view('userweb.Exhibitionfabu');
    }
    public function contactusfabu()
    {
        return view('userweb.contactusfabu');
    }
    public function indexpagetwo()
    {
        return view('userweb.indexpagetwo');
    }
}
