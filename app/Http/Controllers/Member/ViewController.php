<?php
/**
 * Created by PhpStorm.
 * User: daimingkang
 * Date: 2018/6/9
 * Time: 下午4:03
 */

namespace App\Http\Controllers\Member;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ViewController extends Controller
{
    public function cz_view(Request $request)
    {
        $path = $request->server();
        $ways_type = $request->get('ways_type');
        $url = "";
        //微信

        if ($ways_type == "2000") {
            $url = url('/api/weixin/member_cz_pay_view');
            return redirect($url . '?' . $path['QUERY_STRING']);

        }

        if ($ways_type == "3002") {
            $url = url('/api/mybank/weixin/member_cz_pay_view');
            return redirect($url . '?' . $path['QUERY_STRING']);

        }

        if ($ways_type == "6002") {
            $url = url('/api/jd/weixin/member_cz_pay_view');
            return redirect($url . '?' . $path['QUERY_STRING']);

        }

        if ($ways_type == "8002") {
            $url = url('/api/newland/weixin/member_cz_pay_view');
            return redirect($url . '?' . $path['QUERY_STRING']);

        }

        if ($ways_type == "9002") {
            $url = url('/api/huiyuanbao/weixin/member_cz_pay_view');
            return redirect($url . '?' . $path['QUERY_STRING']);

        }

        if ($ways_type == "15002") {
            $url = url('/api/dlb/weixin/member_cz_pay_view');
            return redirect($url . '?' . $path['QUERY_STRING']);

        }

        return view('member.cz');

    }


    public function member_info(Request $request)
    {

        $data = $request->all();
        return view('member.member_info', compact('data'));

    }

}