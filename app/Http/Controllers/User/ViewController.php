<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;

/**
 * Class ViewController
 * @package App\Http\Controllers\User
 */
class ViewController extends Controller
{
    public function login()
    {
        return view('user.login');
    }

    public function index()
    {
        return view('user.index');
    }

    public function szbl()
    {
        return view('user.szbl');
    }

    public function setProfitRatio()
    {
        return view('user.setProfitRatio');
    }

    public function ysjj()
    {
        return view('user.ysjj');
    }

    public function VipDraw()
    {
        return view('user.VipDraw');
    }

    public function alipayconfirm() //支付宝配置
    {
        return view('user.alipayconfirm');
    }

    public function wechatconfirm() //微信配置
    {
        return view('user.wechatconfirm');
    }

    public function wechatcashcouponconfirm() //微信代金券配置
    {
        return view('user.wechatcashcouponconfirm');
    }

    public function wechatmerchantcashcouponconfirm() //微信商家券配置
    {
        return view('user.wechatmerchantcashcouponconfirm');
    }

    public function wechatthirdconfig() //微信第三方平台配置
    {
        return view('user.wechatthirdconfig');
    }

    public function alithirdconfig() //支付宝第三方平台配置
    {
        return view('user.alithirdconfig');
    }

    public function schoollist() //学校列表
    {
        return view('user.schoollist');
    }

    public function agentlist() //代理商列表
    {
        return view('user.agentlist');
    }

    public function home() //代理商列表
    {
        return view('user.home');
    }

    public function waterlist()
    {
        return view('user.waterlist');
    }

    public function qrcode()
    {
        return view('user.qrcode');
    }

    public function seewater()
    {
        return view('user.seewater');
    }

    public function payitem()
    {
        return view('user.payitem');
    }

    public function addagent()
    {
        return view('user.addagent');
    }

    public function examineschool()
    {
        return view('user.examineschool');
    }

    public function editschool()
    {
        return view('user.editschool');
    }

    public function forget()
    {
        return view('user.forget');
    }

    public function storelist()
    {
        return view('user.storelist');
    }

    public function wechatCouponList()
    {
        return view('user.wechatCouponList');
    }

    public function seestore()
    {
        return view('user.seestore');
    }

    public function editstore()
    {
        return view('user.editstore');
    }

    //设备管理
    public function devicelist()
    {
        return view('user.devicelist');
    }

    public function adddevice()
    {
        return view('user.adddevice');
    }

    public function editdevice()
    {
        return view('user.editdevice');
    }

    // 消息
    public function appmsg()
    {
        return view('user.appmsg');
    }

    public function addappmsg()
    {
        return view('user.addappmsg');
    }

    public function bannerlist()
    {
        return view('user.bannerlist');
    }

    public function addbanner()
    {
        return view('user.addbanner');
    }

    // 角色
    public function role()
    {
        return view('user.role');
    }

    public function addrole()
    {
        return view('user.addrole');
    }

    public function permissions()
    {
        return view('user.permissions');
    }

    public function rolelist()
    {
        return view('user.rolelist');
    }

    // 权限
    public function power()
    {
        return view('user.power');
    }

    public function addpower()
    {
        return view('user.addpower');
    }

    // 交易流水
    public function tradelist()
    {
        return view('user.tradelist');
    }

    // 通道管理
    public function passway()
    {
        return view('user.passway');
    }

    // 通道管理(河南畅立收)
    public function passwayHn()
    {
        return view('user.passwayHn');
    }

    // 通道费率列表
    public function ratelist()
    {
        return view('user.ratelist');
    }

    // 花呗分期流水列表
    public function flowerlist()
    {
        return view('user.flowerlist');
    }

    // 分店管理
    public function branchshop()
    {
        return view('user.branchshop');
    }

    public function addbranchdevice()
    {
        return view('user.addbranchdevice');
    }

    //二维码统一管理
    public function qrcodemanage()
    {
        return view('user.qrcodemanage');
    }

    //广告
    public function ad()
    {
        return view('user.ad');
    }

    public function addad()
    {
        return view('user.addad');
    }

    public function adsee()
    {
        return view('user.adsee');
    }

    public function editad()
    {
        return view('user.editad');
    }

    //赏金
    public function reward()
    {
        return view('user.reward');
    }

    //提现
    public function putforward()
    {
        return view('user.putforward');
    }

    //支付宝红包
    public function alipayred()
    {
        return view('user.alipayred');
    }

    public function addalipayred()
    {
        return view('user.addalipayred');
    }

    //收银插件
    public function shouyin()
    {
        return view('user.shouyin');
    }

    public function addshouyin()
    {
        return view('user.addshouyin');
    }

    //京东配置
    public function jdconfigure()
    {
        return view('user.jdconfigure');
    }

    public function openpassway()
    {
        return view('user.openpassway');
    }

    public function newworld()
    {
        return view('user.newworld');
    }

    public function settlement()
    {
        return view('user.settlement');
    }

    public function jdwhitebar()
    {
        return view('user.jdwhitebar');
    }

    public function storeratelist()
    {
        return view('user.storeratelist');
    }

    public function updata()
    {
        return view('user.updata');
    }

    public function appconfig()
    {
        return view('user.appconfig');
    }

    public function pushconfig()
    {
        return view('user.pushconfig');
    }

    public function msgconfig()
    {
        return view('user.msgconfig');
    }

    public function hrtconfig()
    {
        return view('user.hrtconfig');
    }

    public function passwaysort()
    {
        return view('user.passwaysort');
    }

    public function storeconfig()
    {
        return view('user.storeconfig');
    }

    public function reconciliation()
    {
        return view('user.reconciliation');
    }

    public function devicemanage()
    {
        return view('user.devicemanage');
    }

    public function merchantnumber()
    {
        return view('user.merchantnumber');
    }

    public function withdrawrecord()
    {
        return view('user.withdrawrecord');
    }

    public function deviceconfig()
    {
        return view('user.deviceconfig');
    }

    public function bound()
    {
        return view('user.bound');
    }

    public function unbound()
    {
        return view('user.unbound');
    }

    public function cashset()
    {
        return view('user.cashset');
    }

    public function settlerecord()
    {
        return view('user.settlerecord');
    }

    public function settledetail()
    {
        return view('user.settledetail');
    }

    public function mqtt()
    {
        return view('user.mqtt');
    }

    // 银盈通
    public function yytong()
    {
        return view('user.yytong');
    }

    public function merchantmanage()
    {
        return view('user.merchantmanage');
    }

    public function addstoretransfer()
    {
        return view('user.addstoretransfer');
    }

    public function transactionlist()
    {
        return view('user.transactionlist');
    }

    public function percode()
    {
        return view('user.percode');
    }

    public function addpercode()
    {
        return view('user.addpercode');
    }

    public function makemoney()
    {
        return view('user.makemoney');
    }

    // 押金流水deposit
    public function depositwater()
    {
        return view('user.depositwater');
    }

    public function depositacount()
    {
        return view('user.depositacount');
    }

    public function cashwithdrawal()
    {
        return view('user.cashwithdrawal');
    }

    public function fuyoumanage()
    {
        return view('user.fuyoumanage');
    }

    public function logoconfig()
    {
        return view('user.logoconfig');
    }

    public function fee()
    {
        return view('user.fee');
    }

    public function addfee()
    {
        return view('user.addfee');
    }

    public function agentrecovery()
    {
        return view('user.agentrecovery');
    }

    public function dlbconfig()
    {
        return view('user.dlbconfig');
    }

    public function appindexfunction()
    {
        return view('user.appindexfunction');
    }

    public function appmyfunction()
    {
        return view('user.appmyfunction');
    }

    public function apphead()
    {
        return view('user.apphead');
    }

    public function addappindexfunction()
    {
        return view('user.addappindexfunction');
    }

    public function addappmyfunction()
    {
        return view('user.addappmyfunction');
    }

    public function zfthbfq()
    {
        return view('user.zfthbfq');
    }

    public function flowerfq()
    {
        return view('user.flowerfq');
    }

    public function merchantlist()
    {
        return view('user.merchantlist');
    }

    public function storeseting()
    {
        return view('user.storeseting');
    }

    public function addcashier()
    {
        return view('user.addcashier');
    }

    public function tableNumber()
    {
        return view('user.tableNumber');
    }

    public function bindcashier()
    {
        return view('user.bindcashier');
    }

    public function editcashier()
    {
        return view('user.editcashier');
    }

    public function tfconfig()
    {
        return view('user.tfconfig');
    }

    public function sxfconfig()
    {
        return view('user.sxfconfig');
    }

    public function sxfaconfig()
    {
        return view('user.sxfaconfig');
    }

    public function editmsg()
    {
        return view('user.editmsg');
    }

    //月对账
    public function monthrecord()
    {
        return view('user.monthrecord');
    }

    public function agentrecord()
    {
        return view('user.agentrecord');
    }

    public function returnexport()
    {
        return view('user.returnexport');
    }

    //收款批量置顶
    public function editpaywaysort()
    {
        return view('user.editpaywaysort');
    }

    //分润账单
    public function commissionlist()
    {
        return view('user.commissionlist');
    }

    //首付科技-分润账单
    public function sfkj()
    {
        return view('user.sfkj');
    }

    //汇付支付配置
    public function huipayconfig()
    {
        return view('user.huipayconfig');
    }

    //修改密码
    public function password()
    {
        return view('user.password');
    }

    //公告
    public function notice()
    {
        return view('user.notice');
    }

    //门店修改
    public function modifystore()
    {
        return view('user.modifystore');
    }

    //添加哆啦宝门店信息
    public function editdlbstore()
    {
        return view('user.editdlbstore');
    }

    //添加哆啦宝门店信息
    public function edithkrtstoreinfo()
    {
        return view('user.edithkrtstoreinfo');
    }

    //云收易系统更新包
    public function ysyupdata()
    {
        return view('user.ysyupdata');
    }

    //海科融通支付配置
    public function hkrtconfig()
    {
        return view('user.hkrtconfig');
    }

    //易生支付
    public function easypay()
    {
        return view('user.easypay');
    }

    //分店收银员
    public function cashier()
    {
        return view('user.cashier');
    }

    // 转移代理商
    public function transferstore()
    {
        return view('user.transferstore');
    }

    public function subledgermanagement()
    {
        return view('user.subledgermanagement');
    }

    public function returnSettings()
    {
        return view('user.returnSettings');
    }

    public function Settingreturn()
    {
        return view('user.Settingreturn');
    }

    public function hltxconfig()
    {
        return view('user.hltxconfig');
    }

    public function linkageconfig()
    {
        return view('user.linkageconfig');
    }

    public function facepaymentstatistics()
    {
        return view('user.facepaymentstatistics');
    }

    public function profitsharinglists()
    {
        return view('user.profitsharinglists');
    }

    public function applets()
    {
        return view('user.applets');
    }

    public function appletsAliPay()
    {
        return view('user.appletsAliPay');
    }

    public function appletInformation()
    {
        return view('user.appletInformation');
    }

    public function appletCode()
    {
        return view('user.appletCode');
    }

    public function appletWeChatTemplate()
    {
        return view('user.appletsWeChatTemInfo');
    }

    public function appletWeChatTemplateIndex()
    {
        return view('user.appletsWeChatTemInfoIndex');
    }

    public function appletWeChatCateGory()
    {
        return view('user.appletWeChatCateGory');
    }

    public function appletAliPayTemplate()
    {
        return view('user.appletsAliPayTemInfo');
    }

    public function wftpayconfig()
    {
        return view('user.wftpayconfig');
    }

    public function hwcpayconfig()
    {
        return view('user.hwcpayconfig');
    }

    public function dadaconfig()
    {
        return view('user.dadaconfig');
    }

    public function dadaconfigset()
    {
        return view('user.dadaconfigset');
    }

    public function facepaymentdis()
    {
        return view('user.facepaymentdis');
    }

    //收钱啦 收银插件
    public function shouqianla()
    {
        return view('user.shouqianla');
    }

    public function addshouqianla()
    {
        return view('user.addshouqianla');
    }

    //微信a配置
    public function wechataconfirm()
    {
        return view('user.wechataconfirm');
    }

    //apk配置
    public function apkconfig()
    {
        return view('user.apkconfig');
    }

    //apk修改
    public function editapk()
    {
        return view('user.editapk');
    }

    //apk添加
    public function addapk()
    {
        return view('user.addapk');
    }

    //apk添加
    public function transactionvolume()
    {
        return view('user.transactionvolume');
    }

    public function xunfeilist()
    {
        return view('user.xunfeilist');
    }

    public function settlementday()
    {
        return view('user.settlementday');
    }

    public function addsettlementday()
    {
        return view('user.addsettlementday');
    }

    public function unbalanced()
    {
        return view('user.unbalanced');
    }

    public function editsetday()
    {
        return view('user.editsetday');
    }

    //广告链接
    public function adlinks()
    {
        return view('user.adlinks');
    }

    //添加广告链接
    public function addadlinks()
    {
        return view('user.addadlinks');
    }

    //修改广告链接
    public function editadlinks()
    {
        return view('user.editadlinks');
    }

    public function addoperation() //代运营授权
    {
        return view('user.addoperation');
    }

    public function operationlist() //代运营授权列表
    {
        return view('user.operationlist');
    }

    public function antstorelist() //蚂蚁门店
    {
        return view('user.antstorelist');
    }

    public function iotbind() //设备绑定
    {
        return view('user.iotbind');
    }

    public function bluesea() //新蓝海报名
    {
        return view('user.bluesea');
    }

    public function storesealist() //新蓝海报名
    {
        return view('user.storesealist');
    }

    public function bluesealist() //新蓝海报名列表
    {
        return view('user.bluesealist');
    }

    public function editbluesea() //新蓝海报名修改
    {
        return view('user.editbluesea');
    }

    public function cashlessvoucher() //支付宝无资金券
    {
        return view('user.cashlessvoucher');
    }

    public function cashlesssee()
    {
        return view('user.cashlesssee');
    }

    //银盛
    public function yinshengconfig()
    {
        return view('user.yinshengconfig');
    }

    //钱方
    public function qfpayconfig()
    {
        return view('user.qfpayconfig');
    }

    public function editeasypaystore()
    {
        return view('user.editeasypaystore');
    }

    //如意Lite
    public function alispiconfig()
    {
        return view('user.alispiconfig');
    }

    public function cashBackRule()
    {
        return view('user.cashBackRule');
    }

    public function addCashBackRule()
    {
        return view('user.addCashBackRule');
    }

    public function detailsCashBackRule()
    {
        return view('user.detailsCashBackRule');
    }

    public function editCashBackRule()
    {
        return view('user.editCashBackRule');
    }

    public function editUserRule()
    {
        return view('user.editUserRule');
    }

    public function storeTransactionRewardList()
    {
        return view('user.storeTransactionRewardList');
    }

    public function detailsStoreTransactionReward()
    {
        return view('user.detailsStoreTransactionReward');
    }

    public function editUserRuleReward()
    {
        return view('user.editUserRuleReward');
    }

    public function userRules()
    {
        return view('user.userRules');
    }

    public function setProfit()
    {
        return view('user.setProfit');
    }

    public function editUserSettlementType()
    {
        return view('user.editUserSettlementType');
    }

    public function rechargeList()
    {
        return view('user.rechargeList');
    }

    public function passagewayList()
    {
        return view('user.passagewayList');
    }

    public function zeroRateSettlementDay()
    {
        return view('user.zeroRateSettlementDay');
    }

    public function zeroRateSettlementMonth()
    {
        return view('user.zeroRateSettlementMonth');
    }
    public function recharges()
    {
        return view('user.recharges');
    }
    public function electronic()
    {
        return view('user.electronic');
    }
    public function getUserMoney()
    {
        return view('user.getUserMoney');
    }

}
