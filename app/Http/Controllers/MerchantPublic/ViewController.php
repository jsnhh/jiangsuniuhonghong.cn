<?php
/**
 * Created by PhpStorm.
 * User: daimingkang
 * Date: 2018/6/9
 * Time: 下午4:03
 */

namespace App\Http\Controllers\MerchantPublic;

use App\Http\Controllers\Controller;

class ViewController extends Controller
{
    public function login()
    {
        return view('merchantpublic.login');
    }
    public function forget()
    {
        return view('merchantpublic.forget');
    }
    public function cashCouponTwo()
    {
        return view('merchantpublic.cashCouponTwo');
    }
    public function cashCoupondetailedTwo()
    {
        return view('merchantpublic.cashCoupondetailedTwo');
    }
    public function index()
    {
        return view('merchantpublic.index');
    }
    public function store()
    {
        return view('merchantpublic.store');
    }
    public function seestore()
    {
        return view('merchantpublic.seestore');
    }
    public function cashier()
    {
        return view('merchantpublic.cashier');
    }
    public function auditIng()
    {
        return view('merchantpublic.auditIng');
    }
    public function auditFailed()
    {
        return view('merchantpublic.auditFailed');
    }
    public function auditSuccess()
    {
        return view('merchantpublic.auditSuccess');
    }
    public function addcash()
    {
        return view('merchantpublic.addcash');
    }
    public function tableNumber()
    {
        return view('merchantpublic.tableNumber');
    }
    public function commodityClass()
    {
        return view('merchantpublic.commodityClass');
    }
    public function commodityList()
    {
        return view('merchantpublic.commodityList');
    }
    public function commodityOeder()
    {
        return view('merchantpublic.commodityOeder');
    }
    public function refundOrder()
    {
        return view('merchantpublic.refundOrder');
    }
    public function editcashier()
    {
        return view('merchantpublic.editcashier');
    }
    public function waterlist()
    {
        return view('merchantpublic.waterlist');
    }
    public function flowerlist()
    {
        return view('merchantpublic.flowerlist');
    }
    public function appletmoneyqrcodelist()
    {
        return view('merchantpublic.appletmoneyqrcodelist');
    }
    public function home()
    {
        return view('merchantpublic.home');
    }
    public function merchantnumber()
    {
        return view('merchantpublic.merchantnumber');
    }
    public function withdrawrecord()
    {
        return view('merchantpublic.withdrawrecord');
    }
    public function memberlist()
    {
        return view('merchantpublic.memberlist');
    }
    public function wxmemberlist()
    {
        return view('merchantpublic.wxmemberlist');
    }
    public function setmember()
    {
        return view('merchantpublic.setmember');
    }
    public function templatemember()
    {
        return view('merchantpublic.templatemember');
    }

    public function setpoints()
    {
        return view('merchantpublic.setpoints');
    }
    public function rechargerecord()
    {
        return view('merchantpublic.rechargerecord');
    }
    public function wechatsynchro()
    {
        return view('merchantpublic.wechatsynchro');
    }
    public function bindcashier()
    {
        return view('merchantpublic.bindcashier');
    }
    public function marketlist()
    {
        return view('merchantpublic.marketlist');
    }
    public function addmarket()
    {
        return view('merchantpublic.addmarket');
    }
    public function reward()
    {
        return view('merchantpublic.reward');
    }
    public function putforward()
    {
        return view('merchantpublic.putforward');
    }
    public function setpaypassward()
    {
        return view('merchantpublic.setpaypassward');
    }
    public function editpaypassward()
    {
        return view('merchantpublic.editpaypassward');
    }
    public function monthrecord()
    {
        return view('merchantpublic.monthrecord');
    }
    public function alipaysynchro()
    {
        return view('merchantpublic.alipaysynchro');
    }
    public function pointslist()
    {
        return view('merchantpublic.pointslist');
    }
    public function dadapsseting()
    {
        return view('merchantpublic.dadapsseting');
    }

    public function createApplet()
    {
        return view('merchantpublic.createApplet');
    }
    public function alipycreatApplet()
    {
        return view('merchantpublic.alipycreatApplet');
    }
    public function cashCoupon()
    {
        return view('merchantpublic.cashCoupon');
    }
    public function merchantCashCoupon()
    {
        return view('merchantpublic.merchantCashCoupon');
    }
    public function cashCoupondetailed()
    {
        return view('merchantpublic.cashCoupondetailed');
    }
    //微信代金券核销详情
    public function writeoffdetails()
    {
        return view('merchantpublic.writeoffdetails');
    }
    public function writeoffdetailstwo()
    {
        return view('merchantpublic.writeoffdetailstwo');
    }
    //支付宝代金券
    public function alipayCoupon()
    {
        return view('merchantpublic.alipayCoupon');
    }
    //支付宝代金券核销
    public function alipayCoupondetailed()
    {
        return view('merchantpublic.alipayCoupondetailed');
    }
    //支付宝代金券核销详情
    public function alipaydetails()
    {
        return view('merchantpublic.alipaydetails');
    }
    //支付宝无金额代金券
    public function cashlessvoucher()
    {
        return view('merchantpublic.cashlessvoucher');
    }
    //微信审核页
    public function WeChatshenheing()
    {
        return view('merchantpublic.WeChatshenheing');
    }
    //微信审核失败页
    public function WeChatshenhefail()
    {
        return view('merchantpublic.WeChatshenhefail');
    }
    public function WeChatshenhesuccess()
    {
        return view('merchantpublic.WeChatshenhesuccess');
    }
    public function addcashier()
    {
        return view('merchantpublic.addcashier');
    }
    public function deviceBind()
    {
        return view('merchantpublic.deviceBind');
    }
    public function deviceList()
    {
        return view('merchantpublic.deviceList');
    }
}
