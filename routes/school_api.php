<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
//merchant表邓旒   需要token
    $api->group(['namespace' => 'App\Api\Controllers\School', 'prefix' => 'school', 'middleware' => 'merchant.api'], function ($api) {

      // 公告
        $api->any('teacher/announce/add','AnnounceController@add');
        $api->any('teacher/announce/save','AnnounceController@save');
        $api->any('teacher/announce/del','AnnounceController@del');
        $api->any('teacher/announce/lst','AnnounceController@lst');
        $api->any('teacher/announce/cate/lst','AnnounceController@cateLst');
        $api->any('teacher/announce/show','AnnounceController@show');


    	//创建学校
        $api->any('teacher/typelst','IndexController@typeLst');
        $api->any('teacher/add','IndexController@add');
        $api->any('teacher/save','IndexController@save');
        $api->any('teacher/lst','IndexController@lst');
        $api->any('teacher/show','IndexController@show');
        $api->any('teacher/check','IndexController@check');
        $api->any('teacher/sync','IndexController@sync');
        $api->any('teacher/ali/auth/url','IndexController@aliAuth');
        //首页数据
        $api->any('get_data', 'IndexController@get_data');

        //年级
        $api->any('teacher/grade/add','GradeController@add');
        $api->any('teacher/grade/lst','GradeController@lst');
        $api->any('teacher/grade/show','GradeController@show');
        $api->any('teacher/grade/save','GradeController@save');
        $api->any('teacher/grade/del','GradeController@del');



        //班级
        $api->any('teacher/class/add','ClassController@add');
        $api->any('teacher/class/lst','ClassController@lst');
        $api->any('teacher/class/show','ClassController@show');
        $api->any('teacher/class/save','ClassController@save');
        $api->any('teacher/class/del','ClassController@del');



        //学生
        $api->any('teacher/stu/import','StudentController@importExcel');
        $api->any('teacher/stu/add','StudentController@add');
        $api->any('teacher/stu/lst','StudentController@lst');
        $api->any('teacher/stu/show','StudentController@show');
        $api->any('teacher/stu/save','StudentController@save');
        $api->any('teacher/stu/del','StudentController@del');

        //缴费模板
        $api->any('teacher/template/add','PayTemplateController@add');
        $api->any('teacher/template/lst','PayTemplateController@lst');
        $api->any('teacher/template/show','PayTemplateController@show');
        $api->any('teacher/template/save','PayTemplateController@save');
        $api->any('teacher/template/check','PayTemplateController@check');
        $api->any('teacher/template/del','PayTemplateController@del');


        //缴费项目
        // $api->any('teacher/payitem/add','PayItemController@add');//newAdd
        $api->any('teacher/payitem/add','PayItemController@newAdd');//newAdd
        $api->any('teacher/payitem/lst','PayItemController@lst');
        $api->any('teacher/payitem/show','PayItemController@show');
        $api->any('teacher/payitem/save','PayItemController@save');
        $api->any('teacher/payitem/check','PayItemController@check');
        $api->any('teacher/payitem/del','PayItemController@del');



        $api->any('teacher/payitem/remind','PayItemController@remind');
        $api->any('teacher/payitem/remind/one','PayItemController@remindOne');

        //订单
        $api->any('teacher/order/lst','OrderController@lst');
        $api->any('teacher/order/lstnotpay','OrderController@lstnotpay');
        $api->any('teacher/order/show','OrderController@show');
        $api->any('teacher/order/send','OrderController@sendOrder');//按照项目发送支付宝账单
        $api->any('teacher/order/send/one','OrderController@sendOneOrder');//根据单号发送支付宝账单


        // 新流程：直接excel导入订单和项目
        $api->any('teacher/excel/order/import','ExcelOrderController@import');



        // 教师管理
        $api->any('teacher/ter/add','TeacherController@add');
        $api->any('teacher/ter/show','TeacherController@show');
        $api->any('teacher/ter/save','TeacherController@save');
        $api->any('teacher/ter/del','TeacherController@del');
        $api->any('teacher/ter/lst','TeacherController@lst');
        $api->any('teacher/ter/typelst','TeacherController@typelst');
        $api->any('teacher/ter/relate','TeacherController@relate');


        // 
        $api->any('teacher/login/info','TeacherController@loginerInfo');
        $api->any('teacher/login/class/lst','TeacherController@loginerClassLst');

        $api->any('teacher/ter/class/unbind','TeacherController@unbind');




        //教育缴费情况统计
        $api->any('teacher/stat/pay','StatPayController@pay');




    	$api->any('teacher/make/order','OrderController@make');
    		$api->any('test',function(){echo 8888;});
    });

    $api->group(['namespace' => 'App\Api\Controllers\School', 'prefix' => 'school'], function ($api) {


        //学校添加异步通知
        $api->any('pay/notify','NotifyController@index');
        //获取学生信息
        $api->any('stu/getStudentInfo','StudentController@getStudentInfo');
        //学生缴费易生支付
        $api->any('school_pay','SchoolPayController@school_pay');
        //学生缴费易生数科支付回调
        $api->any('pay_notify_easySk','SchoolPayController@pay_notify_easySk');
        //学生缴费易生支付回调
        $api->any('pay_notify_easy','SchoolPayController@pay_notify_easy');
        //获取缴费记录
        $api->any('stu/getPayList','StudentController@getPayList');
        //获取缴费项目
        $api->any('stu/getOrderBatchList','StudentController@getOrderBatchList');
        //获取缴费项目明细
        $api->any('stu/getOrderBatchInfo','StudentController@getOrderBatchInfo');
        //获取学生信息
        $api->any('stu/getStudentList','StudentController@getStudentList');
        $api->any('teacher/grade/lst','GradeController@lst');//获取年级列表  无需token
        $api->any('teacher/class/lst','ClassController@lst');//获取班级列表  无需token
        $api->any('teacher/stu/add','StudentController@add');//获取学生列表  无需token
        $api->any('teacher/stu/show','StudentController@show');//获取学生信息  无需token
        $api->any('teacher/stu/save','StudentController@save');//学生修改  无需token
        $api->any('stu/getStudentNoAndStuClassNo','StudentController@getStudentNoAndStuClassNo');//根据学生学号和班级编号获取学生信息
    });









//users表   需要token
    $api->group(['namespace' => 'App\Api\Controllers\School\Agent', 'prefix' => 'school/agent', 'middleware' => 'user.api'], function ($api) {

        $api->any('typelst','SchoolController@typeLst');

        $api->any('save','AgentSchoolController@save');
        $api->any('show','AgentSchoolController@show');
        $api->any('sync','AgentSchoolController@sync');



        $api->any('check','AgentSchoolController@check');
        $api->any('lst','SchoolController@lst');
        $api->any('grade/lst','GradeController@lst');
        $api->any('class/lst','ClassController@lst');
        $api->any('batch/lst','PayItemController@lst');
        $api->any('order/lst','OrderController@lst');
        $api->any('order/show','OrderController@show');

        // $api->any('order/show','OrderController@show');

        // $api->any('make/order','OrderController@make');


            // $api->any('test',function(){echo 8888;});
    });









});