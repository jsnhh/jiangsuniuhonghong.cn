<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace' => 'Member','prefix' => 'web/member'], function () {
    Route::get('cz_view', 'ViewController@cz_view');
    Route::get('member_info', 'ViewController@member_info');

});
