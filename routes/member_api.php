<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    //公共端 token
    $api->group(['namespace' => 'App\Api\Controllers\Member', 'prefix' => 'member', 'middleware' => 'public.api'], function ($api) {

        $api->any('mb_lists', 'SelectController@mb_lists');

        $api->any('mb_infos', 'SelectController@mb_infos');

        $api->any('jf', 'SelectController@jf');
        $api->any('set_jf', 'SelectController@set_jf');
        $api->any('member_set_jf_list', 'SelectController@member_set_jf_list'); //积分设置列表
        $api->any('jflqtj', 'SelectController@jflqtj');
        $api->any('jfsytj', 'SelectController@jfsytj');
        $api->any('newmbsjf', 'SelectController@newmbsjf');
        $api->any('mb_counts', 'SelectController@mb_counts');
        $api->any('mb_jfinfo', 'SelectController@mb_jfinfo');
        $api->any('jf_counts', 'SelectController@jf_counts');
        $api->any('create_qr', 'SelectController@create_qr');
        $api->any('is_set_jf', 'SelectController@is_set_jf');
        $api->any('yx_store_lists', 'SelectController@yx_store_lists');

        $api->any('set_tpl', 'SetMermberController@set_tpl');
        $api->any('query_tpl', 'SetMermberController@query_tpl');
        $api->any('wx_member_card', 'WxMemberController@wx_member_card');

        $api->any('query_type', 'SetMermberController@query_type');
        $api->any('set_type', 'SetMermberController@set_type');
        $api->any('query_type_list', 'SetMermberController@query_type_list');
        $api->any('query_type_info', 'SetMermberController@query_type_info');
        $api->post('del_member_type', 'SetMermberController@del_member_type');
        $api->post('del_member_points', 'SetMermberController@del_member_points'); //清空or更新会员积分
        $api->post('del_member', 'SetMermberController@del_member'); //注销会员
        $api->post('update_member', 'SetMermberController@update_member'); //更新会员信息
        $api->post('edit_member', 'SetMermberController@editMember'); // 编辑会员信息

        $api->any('cz_type', 'CzController@cz_type');
        $api->any('cz_lists', 'CzController@cz_lists');
        $api->any('yx_lists', 'XyController@yx_lists');
        $api->any('yx_del', 'XyController@yx_del');

        $api->any('cz_set', 'CzController@cz_set');
        $api->any('cz_info', 'CzController@cz_info');

    });

    //公共端
    $api->group(['namespace' => 'App\Api\Controllers\Member', 'prefix' => 'member'], function ($api) {
        $api->any('cz_query', 'CzController@cz_query');
        $api->any('cz_list_mb_ids', 'CzController@cz_list_mb_ids');
        $api->any('cz', 'PayController@cz');
        $api->any('qr_pay_submit', 'PayController@qr_pay_submit');
        $api->any('order_foreach', 'PayController@order_foreach');

        $api->any('query_tpl_web', 'SetMermberController@query_tpl_web');
        $api->any('member_info_save', 'SetMermberController@member_info_save');

        //
        $api->any('member_pay_submit', 'PayController@member_pay_submit');
        $api->any('cz_b', 'PayController@cz_b');

    });

    // 微信正式会员
    $api->group(['namespace' => 'App\Api\Controllers\Member', 'prefix' => 'member', 'middleware' => 'public.api'], function ($api) {
        $api->post('wechatMemberLists', 'SelectController@wechatMemberLists');  // 微信正式会员列表
        $api->post('wechatMemberInfo', 'SelectController@wechatMemberInfo');  // 微信正式会员信息
        $api->post('editWechatMember', 'WechatMemberController@editWechatMember');  // 编辑微信正式会员
    });

});
