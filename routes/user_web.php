<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace' => 'User', 'prefix' => 'user'], function () {
    Route::get('login', 'ViewController@login');
    Route::get('notice', 'ViewController@notice');
    Route::get('index', 'ViewController@index');
    Route::get('ysjj', 'ViewController@ysjj');
    Route::get('szbl', 'ViewController@szbl');
    Route::get('setProfitRatio', 'ViewController@setProfitRatio');
    Route::get('VipDraw', 'ViewController@VipDraw');
    Route::get('alipayconfirm', 'ViewController@alipayconfirm');
    Route::get('wechatconfirm', 'ViewController@wechatconfirm');
    Route::get('wechatcashcouponconfirm', 'ViewController@wechatcashcouponconfirm');
    Route::get('wechatmerchantcashcouponconfirm', 'ViewController@wechatmerchantcashcouponconfirm');
    Route::get('wechatthirdconfig', 'ViewController@wechatthirdconfig');
    Route::get('alithirdconfig', 'ViewController@alithirdconfig');
    Route::get('schoollist', 'ViewController@schoollist');
    Route::get('agentlist', 'ViewController@agentlist');
    Route::get('home', 'ViewController@home');
    Route::get('waterlist', 'ViewController@waterlist');
    Route::get('qrcode', 'ViewController@qrcode');
    Route::get('seewater', 'ViewController@seewater');
    Route::get('payitem', 'ViewController@payitem');
    Route::get('addagent', 'ViewController@addagent');
    Route::get('examineschool', 'ViewController@examineschool');
    Route::get('editschool', 'ViewController@editschool');
    Route::get('forget', 'ViewController@forget');
    Route::get('storelist', 'ViewController@storelist');
    Route::get('wechat_coupon_list', 'ViewController@wechatCouponList');
    Route::get('seestore', 'ViewController@seestore');
    Route::get('editstore', 'ViewController@editstore');
    Route::get('devicelist', 'ViewController@devicelist');
    Route::get('adddevice', 'ViewController@adddevice');
    Route::get('editdevice', 'ViewController@editdevice');
    Route::get('appmsg', 'ViewController@appmsg');
    Route::get('bannerlist', 'ViewController@bannerlist');
    Route::get('addappmsg', 'ViewController@addappmsg');
    Route::get('addbanner', 'ViewController@addbanner');
    Route::get('role', 'ViewController@role');
    Route::get('addrole', 'ViewController@addrole');
    Route::get('permissions', 'ViewController@permissions');
    Route::get('power', 'ViewController@power');
    Route::get('addpower', 'ViewController@addpower');
    Route::get('rolelist', 'ViewController@rolelist');
    Route::get('tradelist', 'ViewController@tradelist');
    Route::get('newBluesea', 'ViewController@newBluesea');
    Route::get('passway', 'ViewController@passway');
    Route::get('passwayHn', 'ViewController@passwayHn');
    Route::get('ratelist', 'ViewController@ratelist');
    Route::get('flowerlist', 'ViewController@flowerlist');
    Route::get('branchshop', 'ViewController@branchshop');
    Route::get('addbranchdevice', 'ViewController@addbranchdevice');
    Route::get('qrcodemanage', 'ViewController@qrcodemanage');
    Route::get('ad', 'ViewController@ad');
    Route::get('addad', 'ViewController@addad');
    Route::get('adsee', 'ViewController@adsee');
    Route::get('editad', 'ViewController@editad');
    Route::get('reward', 'ViewController@reward');
    Route::get('putforward', 'ViewController@putforward');
    Route::get('alipayred', 'ViewController@alipayred');
    Route::get('addalipayred', 'ViewController@addalipayred');
    Route::get('shouyin', 'ViewController@shouyin');
    Route::get('addshouyin', 'ViewController@addshouyin');
    Route::get('jdconfigure', 'ViewController@jdconfigure');
    Route::get('openpassway', 'ViewController@openpassway');
    Route::get('newworld', 'ViewController@newworld');
    Route::get('settlement', 'ViewController@settlement');
    Route::get('jdwhitebar', 'ViewController@jdwhitebar');
    Route::get('storeratelist', 'ViewController@storeratelist');
    Route::get('updata', 'ViewController@updata');
    Route::get('appconfig', 'ViewController@appconfig');
    Route::get('pushconfig', 'ViewController@pushconfig');
    Route::get('msgconfig', 'ViewController@msgconfig');
    Route::get('hrtconfig', 'ViewController@hrtconfig');
    Route::get('passwaysort', 'ViewController@passwaysort');
    Route::get('storeconfig', 'ViewController@storeconfig');
    Route::get('reconciliation', 'ViewController@reconciliation');
    Route::get('devicemanage', 'ViewController@devicemanage');
    Route::get('merchantnumber', 'ViewController@merchantnumber');
    Route::get('withdrawrecord', 'ViewController@withdrawrecord');
    Route::get('deviceconfig', 'ViewController@deviceconfig');
    Route::get('bound', 'ViewController@bound');
    Route::get('unbound', 'ViewController@unbound');
    Route::get('cashset', 'ViewController@cashset');
    Route::get('settlerecord', 'ViewController@settlerecord');
    Route::get('settledetail', 'ViewController@settledetail');
    Route::get('mqtt', 'ViewController@mqtt');
    Route::get('yytong', 'ViewController@yytong');
    Route::get('merchantmanage', 'ViewController@merchantmanage');
    Route::get('addstoretransfer', 'ViewController@addstoretransfer');
    Route::get('transactionlist', 'ViewController@transactionlist');
    Route::get('percode', 'ViewController@percode');
    Route::get('addpercode', 'ViewController@addpercode');
    Route::get('makemoney', 'ViewController@makemoney');
    Route::get('depositwater', 'ViewController@depositwater');
    Route::get('depositacount', 'ViewController@depositacount');
    Route::get('cashwithdrawal', 'ViewController@cashwithdrawal');
    Route::get('fuyoumanage', 'ViewController@fuyoumanage');
    Route::get('logoconfig', 'ViewController@logoconfig');
    Route::get('fee', 'ViewController@fee');
    Route::get('addfee', 'ViewController@addfee');
    Route::get('agentrecovery', 'ViewController@agentrecovery');
    Route::get('dlbconfig', 'ViewController@dlbconfig');
    Route::get('appindexfunction', 'ViewController@appindexfunction');
    Route::get('appmyfunction', 'ViewController@appmyfunction');
    Route::get('apphead', 'ViewController@apphead');
    Route::get('addappindexfunction', 'ViewController@addappindexfunction');
    Route::get('addappmyfunction', 'ViewController@addappmyfunction');
    Route::get('zfthbfq', 'ViewController@zfthbfq');
    Route::get('flowerfq', 'ViewController@flowerfq');
    Route::get('merchantlist', 'ViewController@merchantlist');
    Route::get('storeseting', 'ViewController@storeseting');
    Route::get('addcashier', 'ViewController@addcashier');
    Route::get('tableNumber', 'ViewController@tableNumber');
    Route::get('commodityClass', 'ViewController@commodityClass');
    Route::get('commodityList', 'ViewController@commodityList');
    Route::get('commodityOeder', 'ViewController@commodityOeder');
    Route::get('refundOrder', 'ViewController@refundOrder');
    Route::get('bindcashier', 'ViewController@bindcashier');
    Route::get('editcashier', 'ViewController@editcashier');
    Route::get('tfconfig', 'ViewController@tfconfig');
    Route::get('sxfconfig', 'ViewController@sxfconfig');
    Route::get('sxfaconfig', 'ViewController@sxfaconfig');
    Route::get('editmsg', 'ViewController@editmsg');
    Route::get('monthrecord', 'ViewController@monthrecord');
    Route::get('agentrecord', 'ViewController@agentrecord');
    Route::get('returnexport', 'ViewController@returnexport');
    Route::get('editpaywaysort', 'ViewController@editpaywaysort');
    Route::get('commissionlist', 'ViewController@commissionlist');
    Route::get('sfkj', 'ViewController@sfkj');
    Route::get('huipayconfig', 'ViewController@huipayconfig');
    Route::get('password', 'ViewController@password');
    Route::get('modifystore', 'ViewController@modifystore');
    Route::get('editdlbstore', 'ViewController@editdlbstore'); //添加哆啦宝门店信息
    Route::get('edithkrtstoreinfo', 'ViewController@edithkrtstoreinfo'); //添加海科融通门店信息
    Route::get('ysyupdata', 'ViewController@ysyupdata'); //云收易更新
    Route::get('hkrtconfig', 'ViewController@hkrtconfig'); //海科融通支付配置
    Route::get('easypay', 'ViewController@easypay');
    Route::get('cashier', 'ViewController@cashier');
    Route::get('transferstore', 'ViewController@transferstore');
    Route::get('subledgermanagement', 'ViewController@subledgermanagement');
    Route::get('returnSettings', 'ViewController@returnSettings');
    Route::get('Settingreturn', 'ViewController@Settingreturn');
    Route::get('hltxconfig', 'ViewController@hltxconfig');
    Route::get('linkageconfig', 'ViewController@linkageconfig'); //联动优势
    Route::get('facepaymentstatistics', 'ViewController@facepaymentstatistics'); //刷脸支付去重统计
    Route::get('profitsharinglists', 'ViewController@profitsharinglists'); //微信分账流水
    Route::get('applets', 'ViewController@applets'); //授权小程序管理页面
    Route::get('appletsAliPay', 'ViewController@appletsAliPay'); //授权小程序管理页面
    Route::get('appletInformation', 'ViewController@appletInformation'); //完善小程序信息页面
    Route::get('appletCode', 'ViewController@appletCode'); //上传小程序代码页面
    Route::get('appletWeChatTemplate', 'ViewController@appletWeChatTemplate'); //微信小程序模板消息
    Route::get('appletWeChatCateGory', 'ViewController@appletWeChatCateGory'); //微信小程序类目
    Route::get('appletAliPayTemplate', 'ViewController@appletAliPayTemplate'); //支付宝小程序模板消息
    Route::get('appletWeChatTemplateIndex', 'ViewController@appletWeChatTemplateIndex'); //微信小程序模板消息位置
    Route::get('wftpayconfig', 'ViewController@wftpayconfig'); //威富通支付
    Route::get('hwcpayconfig', 'ViewController@hwcpayconfig'); //汇旺财支付
    Route::get('dadaconfig', 'ViewController@dadaconfig'); //达达配置
    Route::get('dadaconfigset', 'ViewController@dadaconfigset'); //达达配置
    Route::get('facepaymentdis', 'ViewController@facepaymentdis'); //刷脸设备去重统计
    Route::get('shouqianla', 'ViewController@shouqianla'); //收钱啦插件
    Route::get('addshouqianla', 'ViewController@addshouqianla'); //收钱啦插件
    Route::get('wechataconfirm', 'ViewController@wechataconfirm'); //官方微信a
    Route::get('dongguanconfig', 'ViewController@dongguanconfig'); //东莞银行
    Route::get('apkconfig', 'ViewController@apkconfig'); //apk配置
    Route::get('editapk', 'ViewController@editapk'); //apk修改
    Route::get('addapk', 'ViewController@addapk'); //apk新增
    Route::get('transactionvolume', 'ViewController@transactionvolume');
    Route::get('xunfeilist', 'ViewController@xunfeilist');
    Route::get('settlementday', 'ViewController@settlementday');//赏金日结算列表
    Route::get('addsettlementday', 'ViewController@addsettlementday');//赏金日结算新增
    Route::get('editsetday', 'ViewController@editsetday');//赏金日结算新增
    Route::get('adlinks', 'ViewController@adlinks');//广告链接
    Route::get('addadlinks', 'ViewController@addadlinks'); //新增广告链接
    Route::get('editadlinks', 'ViewController@editadlinks'); //修改广告链接
    Route::get('addaligenie', 'ViewController@addaligenie'); //
    Route::get('aligenielist', 'ViewController@aligenielist'); //
    Route::get('yinshengconfig', 'ViewController@yinshengconfig'); //银盛
    Route::get('qfpayconfig', 'ViewController@qfpayconfig'); //钱方
    Route::get('unbalanced', 'ViewController@unbalanced');//未结算佣金

    Route::get('storesealist', 'ViewController@storesealist'); //新蓝海报名
    Route::get('addoperation', 'ViewController@addoperation'); //代运营授权
    Route::get('operationlist', 'ViewController@operationlist'); //代运营授权
    Route::get('antstorelist', 'ViewController@antstorelist'); //代运营授权
    Route::get('iotbind', 'ViewController@iotbind'); //设备绑定
    Route::get('bluesea', 'ViewController@bluesea'); //新蓝海报名
    Route::get('bluesealist', 'ViewController@bluesealist'); //新蓝海列表
    Route::get('editbluesea', 'ViewController@editbluesea'); //新蓝海修改编辑

    Route::get('cashlessvoucher', 'ViewController@cashlessvoucher'); //支付宝无资金券
    Route::get('cashlesssee', 'ViewController@cashlesssee');

    Route::get('editeasypaystore', 'ViewController@editeasypaystore'); //易生商户入网
    
    Route::get('alispiconfig', 'ViewController@alispiconfig'); //如意lite配置


    Route::get('cashBackRule', 'ViewController@cashBackRule');//交易返现规则主页面
    Route::get('addCashBackRule', 'ViewController@addCashBackRule');//跳转交易返现规则添加页面
    Route::get('detailsCashBackRule', 'ViewController@detailsCashBackRule');//跳转交易返现规则详情页面
    Route::get('editCashBackRule', 'ViewController@editCashBackRule');//跳转交易返现规则修改页面
    Route::get('editUserRule', 'ViewController@editUserRule');//跳转代理商添加规则页面
    Route::get('storeTransactionRewardList', 'ViewController@storeTransactionRewardList');//跳转商户交易达标返现页面
    Route::get('detailsStoreTransactionReward', 'ViewController@detailsStoreTransactionReward');//跳转商户交易达标返现详情页面
    Route::get('editUserRuleReward', 'ViewController@editUserRuleReward');//跳转调整下级奖励金额
    Route::get('userRules', 'ViewController@userRules');//跳转调整下级奖励金额

    Route::get('addTradeLimit', 'ViewController@addTradeLimit');//跳转添加限额页面
    Route::get('seeTradeLimit', 'ViewController@seeTradeLimit');//跳转查看限额页面
    Route::get('editTradeLimit', 'ViewController@editTradeLimit');//跳转修改限额页面

    Route::get('setProfit', 'ViewController@setProfit');//设置充值比例
    Route::get('editUserSettlementType', 'ViewController@editUserSettlementType');//设置零费率结算方式

    Route::get('rechargeList', 'ViewController@rechargeList');//商户充值记录
    Route::get('passagewayList', 'ViewController@passagewayList');//通道使用记录
    Route::get('zeroRateSettlementDay', 'ViewController@zeroRateSettlementDay');//零费率日结算记录
    Route::get('zeroRateSettlementMonth', 'ViewController@zeroRateSettlementMonth');//零费率月结算记录

    Route::get('recharges', 'ViewController@recharges');//商户充值设置

    Route::get('electronic', 'ViewController@electronic');//易生电子协议

    Route::get('getUserMoney', 'ViewController@getUserMoney');//未提分润统计


});
