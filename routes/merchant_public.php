<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace' => 'MerchantPublic','prefix' => 'mb'], function () {
    Route::get('login', 'ViewController@login');
    Route::get('forget', 'ViewController@forget');
    Route::get('index', 'ViewController@index');
    Route::get('store', 'ViewController@store');
    Route::get('seestore', 'ViewController@seestore');
    Route::get('cashier', 'ViewController@cashier');
    Route::get('auditIng', 'ViewController@auditIng');
    Route::get('auditFailed', 'ViewController@auditFailed');
    Route::get('auditSuccess', 'ViewController@auditSuccess');
    Route::get('addcash', 'ViewController@addcash');
    Route::get('tableNumber', 'ViewController@tableNumber');
    Route::get('commodityClass', 'ViewController@commodityClass');
    Route::get('commodityList', 'ViewController@commodityList');
    Route::get('commodityOeder', 'ViewController@commodityOeder');
    Route::get('refundOrder', 'ViewController@refundOrder');
    Route::get('editcashier', 'ViewController@editcashier');
    Route::get('waterlist', 'ViewController@waterlist');
    Route::get('flowerlist', 'ViewController@flowerlist');
    Route::get('appletmoneyqrcodelist', 'ViewController@appletmoneyqrcodelist');
    Route::get('home', 'ViewController@home');
    Route::get('merchantnumber', 'ViewController@merchantnumber');
    Route::get('withdrawrecord', 'ViewController@withdrawrecord');
    Route::get('memberlist', 'ViewController@memberlist');
    Route::get('wxmemberlist', 'ViewController@wxmemberlist');
    Route::get('setmember', 'ViewController@setmember');
    Route::get('templatemember', 'ViewController@templatemember');
    Route::get('setpoints', 'ViewController@setpoints');
    Route::get('rechargerecord', 'ViewController@rechargerecord');
    Route::get('wechatsynchro', 'ViewController@wechatsynchro');
    Route::get('bindcashier', 'ViewController@bindcashier');
    Route::get('marketlist', 'ViewController@marketlist');
    Route::get('addmarket', 'ViewController@addmarket');
    Route::get('reward', 'ViewController@reward');
    Route::get('putforward', 'ViewController@putforward');
    Route::get('setpaypassward', 'ViewController@setpaypassward');
    Route::get('editpaypassward', 'ViewController@editpaypassward');
    Route::get('monthrecord', 'ViewController@monthrecord');
    Route::get('alipaysynchro', 'ViewController@alipaysynchro');
    Route::get('pointslist', 'ViewController@pointslist');
    Route::get('dadapsseting', 'ViewController@dadapsseting');
    Route::get('createApplet', 'ViewController@createApplet');
    Route::get('alipycreatApplet', 'ViewController@alipycreatApplet');
    //输出创建微信代金券页面
    Route::get('cashCoupon', 'ViewController@cashCoupon');
    Route::get('cashCouponTwo', 'ViewController@cashCouponTwo');
    Route::get('cashCoupondetailedTwo', 'ViewController@cashCoupondetailedTwo');
    Route::get('merchantCashCoupon', 'ViewController@merchantCashCoupon');
    Route::get('cashCoupondetailed', 'ViewController@cashCoupondetailed');
    Route::get('writeoffdetails', 'ViewController@writeoffdetails');
    Route::get('writeoffdetailstwo', 'ViewController@writeoffdetailstwo');
    //输出支付宝发券页面
    Route::get('alipayCoupon', 'ViewController@alipayCoupon');
    Route::get('cashlessvoucher', 'ViewController@cashlessvoucher');
    Route::get('alipayCoupondetailed', 'ViewController@alipayCoupondetailed');
    Route::get('alipaydetails', 'ViewController@alipaydetails');
    Route::get('WeChatshenheing', 'ViewController@WeChatshenheing');
    Route::get('WeChatshenhefail', 'ViewController@WeChatshenhefail');
    Route::get('WeChatshenhesuccess', 'ViewController@WeChatshenhesuccess');
    Route::get('addcashier', 'ViewController@addcashier');
    
    Route::get('deviceList', 'ViewController@deviceList');
    Route::get('deviceBind', 'ViewController@deviceBind');
});
