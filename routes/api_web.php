<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace' => 'Api', 'prefix' => 'api'], function () {
    //微信
    //易生数科
    Route::get('easyskpay/weixin/payeducation', 'ViewController@easySkPayeducation');
    Route::get('easyskpay/weixin/payrecord', 'ViewController@easySkPayrecord');
    Route::get('easyskpay/weixin/index', 'ViewController@easySkIndex');
    Route::get('easyskpay/weixin/studentManage', 'ViewController@easySkStudentManage');
    Route::get('easyskpay/weixin/addStudent', 'ViewController@easySkAddStudent');
    Route::get('easyskpay/weixin/editStudent', 'ViewController@easySkEditStudent');
    //易生
    Route::get('easypay/weixin/payeducation', 'ViewController@easyPayeducation');
    Route::get('easypay/weixin/payrecord', 'ViewController@easyPayrecord');
    Route::get('easypay/weixin/index', 'ViewController@easyIndex');
    Route::get('easypay/weixin/studentManage', 'ViewController@easyStudentManage');
    Route::get('easypay/weixin/addStudent', 'ViewController@easyAddStudent');
    Route::get('easypay/weixin/editStudent', 'ViewController@easyEditStudent');

    //支付宝
    //易生数科
    Route::get('easyskpay/alipay/payeducation', 'ViewController@easySkAliPayeducation');
    Route::get('easyskpay/alipay/payrecord', 'ViewController@easySkAliPayrecord');
    Route::get('easyskpay/alipay/index', 'ViewController@easySkAliIndex');
    Route::get('easyskpay/alipay/studentManage', 'ViewController@easySkAliStudentManage');
    Route::get('easyskpay/alipay/addStudent', 'ViewController@easySkAliAddStudent');
    Route::get('easyskpay/alipay/editStudent', 'ViewController@easySkAliEditStudent');
    //易生
    Route::get('easypay/alipay/payeducation', 'ViewController@easyAliPayeducation');
    Route::get('easypay/alipay/payrecord', 'ViewController@easyAliPayrecord');
    Route::get('easypay/alipay/index', 'ViewController@easyAliIndex');
    Route::get('easypay/alipay/studentManage', 'ViewController@easyAliStudentManage');
    Route::get('easypay/alipay/addStudent', 'ViewController@easyAliAddStudent');
    Route::get('easypay/alipay/editStudent', 'ViewController@easyAliEditStudent');
});
