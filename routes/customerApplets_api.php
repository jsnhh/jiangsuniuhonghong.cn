<?php
/**
 * Created by PhpStorm.
 * User: shenshulong
 * Date: 2020/8/22
 * Time: 10:10
 */

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {

    $api->group(['namespace' => 'App\Api\Controllers\CustomerApplets', 'prefix' => 'customer'], function ($api) {

        //需要token验证的接口---------------------------------------------------------------------------------------------

        $api->group(['middleware' => ['customerApplets.api']], function ($api) {

            $api->group(['prefix' => "user"], function($api){
                //获取用户的基本信息接口
                $api->post("getUserWeChatOrAliPayInfo","UserController@getUserWeChatOrAliPayInfo");
                //获取用户所有的地址
                $api->post("getUserAddress","AddressController@getUserAddress");
                //获取用户默认的地址
                $api->post("getUserDefaultAddress","AddressController@getUserDefaultAddress");
                //获取用户某一条的地址
                $api->post("getUserFirstAddress","AddressController@getUserFirstAddress");
                //新增用户地址
                $api->post("addUserAddress","AddressController@addUserAddress");
                //修改用户地址
                $api->post("updateUserAddress","AddressController@updateUserAddress");
                //删除地址
                $api->post("deleteUserAddress","AddressController@deleteUserAddress");

                //加入购物车
                $api->post("addGoodCart","CartController@addGoodCart");
                //查询购物车
                $api->post("getGoodCartInfo","CartController@getGoodCartInfo");
                //清空购物车
                $api->post("clearGoodCart","CartController@clearGoodCart");
            });

            $api->group(['prefix' => "order"],function($api){
                //支付成功，生成订单
                $api->post("generateOrderData","OrderController@generateOrderData");
                //查询订单列表
                $api->post("getOrderData","OrderController@getOrderData");

                //查询订单详情
                $api->post("getOrderDetailData","OrderController@getOrderDetailData");
                //用户退款
                $api->post("userRefundOrder","OrderController@userRefundOrder");
                // 完成待支付订单支付
                $api->post("done_pay_order","OrderController@donePayOrder");

                $api->post("businessOrderPreAdd", "ImmediateDeliveryController@businessOrderPreAdd"); //预下配送单
                $api->post("businessOrderAdd", "ImmediateDeliveryController@businessOrderAdd"); //下配送单
                $api->post("businessOrderReAdd", "ImmediateDeliveryController@businessOrderReAdd"); //重新下单
                $api->post("businessOrderAddTips", "ImmediateDeliveryController@businessOrderAddTips"); //增加小费
                $api->post("businessOrderPreCancel", "ImmediateDeliveryController@businessOrderPreCancel"); //预取消配送单
                $api->post("businessOrderCancel", "ImmediateDeliveryController@businessOrderCancel"); //取消配送单
                $api->post("businessOrderGet", "ImmediateDeliveryController@businessOrderGet"); //拉取配送单信息
                $api->post("getOrderLists", "ImmediateDeliveryController@getOrderLists"); //配送订单查询
            });

            $api->group(['prefix' => "alipay"],function($api){
                $api->any("getTicket", "WechatTicketController@getTicket");
            });

            //系统会员卡
            $api->group(['prefix' => "member"], function($api){
                $api->get("mb_lists", "UserMembershipCardController@mb_lists");
//                $api->post("member_update", "UserMembershipCardController@member_update");
//                $api->post("member_cz", "UserMembershipCardController@member_cz");
            });
        });

        //不需要token验证的接口-------------------------------------------------------------------------------------------

        $api->group(['prefix' => "user"],function($api){
            //用户更新订单信息
            $api->post("updateUserOrderData","OrderController@updateUserOrderData");
            //查询订单详情
            $api->post("getOrderDetailData","OrderController@getOrderDetailData");

            //用户的授权登录接口
            $api->post("userLogin","UserController@userLogin");
            //用户授权后，将用户信息传给后端，进行更新
            $api->post("updateUserInfo","UserController@updateUserInfo");
            //用户授权手机号
            $api->post("getUserMobile","UserController@getUserMobile");
            // 根据用户小程序openid查询信息
            $api->post("getMemberInfoByAppletOpenid", "UserController@getMemberInfoByAppletOpenid");

            //获取省份
            $api->post("getProvince","AreaController@getProvince");
            //获取市
            $api->post("getCity","AreaController@getCity");
            //获取区
            $api->post("getArea","AreaController@getArea");

            //获取门店下的商品信息
            $api->any("getGoods","GoodController@getGoods");
            //获取商品的规格信息
            $api->any("getGoodStandard","GoodController@getGoodStandard");

            //获取微信支付券批次详情
            $api->any("getMchCouponDetail","CouponController@getMchCouponDetail");
            //条件查询批次列表API
            $api->any("getMchCoupon","CouponController@getMchCoupon");
            //激活代金券批次API
            $api->any("userStartCoupons","CouponController@userStartCoupons");
            //发放代金券
            $api->any("userReceiveCoupons","CouponController@userReceiveCoupons");
            //获取平台证书列表
            $api->any("certificates","CouponController@certificates");
            //获取V2签名规则的签名
            $api->any("getWeChatPaySignMd5ANDHMACSHA256","CouponController@getWeChatPaySignMd5ANDHMACSHA256");

            //创建微信支付商家券
            $api->any("createWeChatPayStoreCoupon","CouponController@createWeChatPayStoreCoupon");
            //保存用户领取卡券数据记录
            $api->any("userReceiveCoupon","CouponController@userReceiveCoupon");
            //获取用户已经领取的卡券
            $api->any("getUserCoupon","CouponController@getUserCoupon");
            //查询门店下面的卡券数据
            $api->any("getUserStoreCoupon","CouponController@getUserStoreCoupon");
            // 删除商家券
            $api->any("delUserStoreCoupon","CouponController@delUserStoreCoupon");
            //查询商家券详情API
            $api->any("getCouponDetail","CouponController@getCouponDetail");
            //查询用户单张券详情API
            $api->any("selectUserCouponStatusDetail","CouponController@selectUserCouponStatusDetail");
            // 核销商家券
            $api->any("userUseCouponNew","CouponController@userUseCouponNew");

            //创建微信支付代金券
            $api->any("createWeChatPayCashCoupon","CouponController@createWeChatPayCashCoupon");
            // 测试
            $api->any("test_a/{id}","CouponController@test_a");
            // 设置代金券核销回调地址
            $api->any("setNotifyUrl","CouponController@setNotifyUrl");
            // 微信代金券发券测试
            $api->any("oauth_callback_test","CouponController@oauth_callback_test");
            // 用户支付后获取可用的微信商家券
            $api->any("getStoreUserCanUseCoupon","CouponController@getStoreUserCanUseCoupon");
            // 支付后推送代金券
            $api->any("usePaySendCashCoupon","CouponController@usePaySendCashCoupon");
            // 代金券核销回调
            $api->any("djqNotify/{merchant_id}","CouponController@djqNotify");
            // 微信代金券扫二维码领券
            $api->any('oauth', 'CouponController@oauth');
            // 扫码领券回调
            $api->any('oauth_callback', 'CouponController@oauth_callback');
            // 获取微信代金券二维码
            $api->any('coupon_qr', 'CouponController@couponQr');

            //创建支付宝卡券
            $api->any("createAliPayStoreCoupon","CouponAliPayController@createAliPayStoreCoupon");
            //获取用户已经领取的支付宝卡券
            $api->any("getUserAliPayCoupon","CouponAliPayController@getUserAliPayCoupon");
            //支付宝卡券，用户领取时的回调记录
            $api->any("userCallbackAliPayStoreCoupon","CouponAliPayController@userCallbackAliPayStoreCoupon");

            //创建支付宝会员卡
            $api->any("userAliPayCreateMemberCard","UserAliPayMembershipCardController@userAliPayCreateMemberCard");
            //测试 商户对用户进行 会员卡开卡
//            $api->any("aliPayMarketingCardOpenTest","UserAliPayMembershipCardController@aliPayMarketingCardOpenTest");
            //上传图片到支付宝平台
            $api->any("aliPayOfflineMaterialImageUpload","UserAliPayMembershipCardController@aliPayOfflineMaterialImageUpload");

            //获取商家下面的订单信息
            $api->post("getStoreOrderData","OrderController@getStoreOrderData");
            //更新商家订单信息
            $api->post("updateStoreOrderData","OrderController@updateStoreOrderData");
            //查询商家订单的统计量
            $api->post("getStoreOrder","OrderController@getStoreOrder");

            //创建会员卡
            $api->post("userCreateMemberCard", "UserMembershipCardController@userCreateMemberCard");
            //获取会员卡信息
            $api->get("getMemberCardInfo", "UserMembershipCardController@getMemberCardInfo");
            //获取获取门店下的会员卡信息
            $api->post("getStoreMemberCardInfo", "UserMembershipCardController@getStoreMemberCardInfo");
            //获取当前用户领取的会员卡
            $api->post("getUserMemberCardInfo", "UserMembershipCardController@getUserMemberCardInfo");
            //获取小程序领取会员卡的签名
            $api->post("getMemberCardSignature", "UserMembershipCardController@getMemberCardSignature");
            //记录用户领取会员卡记录
            $api->post("createUserMemberCardInfo", "UserMembershipCardController@createUserMemberCardInfo");

        });

        $api->group(['prefix' => "store"],function($api){
            //获取门店信息接口
            $api->any("getStoreInfo","StoreController@getStoreInfo");
            //获取当前经纬度与目标经纬度的距离
            $api->post("getDistance","StoreController@getDistance");
            //扣款顺序列表
            $api->post("pay_ways_sort","StoreController@pay_ways_sort");

            //获取，设置门店小程序信息
            $api->post("getStoreAppletsInfo","StoreController@getStoreAppletsInfo");
            //打印小票
            $api->post("yly_print","StoreController@yly_print");
            //获取门店下面的微信小程序，支付宝小程序对应的模板消息列表
            $api->post("getStoreAppletTemplate","StoreController@getStoreAppletTemplate");
        });

        $api->group(['prefix' => "weixin"],function($api){
            //上传图片
            $api->post("uploadFile","WechatTicketController@uploadFile");
            // 小程序上传图片
            $api->post("appletUploadFile","WechatTicketController@appletUploadFile");
            //接收微信的授权通知
            $api->any("getTicket", "WechatTicketController@getTicket");
            //消息与事件接收URL
            $api->any("message/{appid}", "WechatTicketController@message");
            //快速创建小程序
            $api->any("fastRegisterWeApp", "WechatTicketController@fastRegisterWeApp");
            //查询创建任务状态
            $api->any("fastRegisterWeAppStatus", "WechatTicketController@fastRegisterWeAppStatus");
            //获取预授权码
            $api->any("getPreAuthCode", "WechatTicketController@getPreAuthCode");
            //拉取所有已授权的帐号信息
            $api->any("getAuthorizerList", "WechatTicketController@getAuthorizerList");
            // 测试
            $api->any("decryptTestA", "TestController@decryptTestA");
            $api->any("decryptMsgCommon", "TestController@decryptMsgCommon");

            //上传代码
            $api->any("uploadCode", "WechatTicketController@uploadCode");
            $api->any("getAuthorizerAccessToken2", "WechatTicketController@getAuthorizerAccessToken2");

            //发布审核
            $api->any("submit_audit", "WechatTicketController@submit_audit");
            //微信认证名称检测,并且修改名称
            $api->any("checkWxVerifyNickname", "WechatTicketController@checkWxVerifyNickname");
            //修改头像
            $api->any("modifyHeadImage", "WechatTicketController@modifyHeadImage");
            //修改服务器域名
            $api->any("updateServerDomain", "WechatTicketController@updateServerDomain");
            //发布已通过审核的小程序
            $api->any("releaseApplet", "WechatTicketController@releaseApplet");
            //查询服务商的当月提审限额（quota）和加急次数
            $api->any("queryQuota", "WechatTicketController@queryQuota");
            //获取当前帐号所设置的类目信息
            $api->any("getWeChatCateGory", "WechatTicketController@getWeChatCateGory");

            //获取可以设置的所有类目
            $api->any("getAllCategories", "WechatTicketController@getAllCategories");
            //获取已设置的所有类目
            $api->any("getSetCateGory", "WechatTicketController@getSetCateGory");
            //添加类目
            $api->any("addCateGory", "WechatTicketController@addCateGory");
            //删除类目
            $api->any("deleteCateGory", "WechatTicketController@deleteCateGory");

            //查询模板应用的位置
            $api->any("getAllUseIndex", "WechatTicketController@getAllUseIndex");
            //查询模板应用的位置列表，主要是提供给表格使用
            $api->any("getTemplateIndexListData", "WechatTicketController@getTemplateIndexListData");
            //添加模板应用的位置
            $api->any("addUserIndex", "WechatTicketController@addUserIndex");
            //修改模板应用的位置
            $api->any("updateUserIndex", "WechatTicketController@updateUserIndex");
            //修改模板已经使用的位置
            $api->any("updateTemplateUseIndex", "WechatTicketController@updateTemplateUseIndex");

            //获取某一个位置的小程序模板消息id
            $api->post("getAppletTemplateId", "WechatTicketController@getAppletTemplateId");

            //发送模板消息
            $api->post("sendTemplateInfo", "WechatTicketController@sendTemplateInfo");
            // 支付成功后发送模板消息
            $api->post("wechatPaySendTemplateInfo", "WechatTicketController@wechatPaySendTemplateInfo");

            $api->post('getAppletsInfoListByStore', 'WechatTicketController@getAppletsInfoListByStore'); //获取微信小程序创建信息
            $api->post('getStoreAppletInfo', 'WechatTicketController@getStoreAppletInfo'); //获取门店对应的小程序信息
            $api->post('getSessionKeyOpenid', 'WechatTicketController@getSessionKeyOpenid'); //code换取openid
            $api->post('getLatestAuditStatus', 'WechatTicketController@getLatestAuditStatus'); //查询最新一次提交的审核状态
            $api->post('speedUpAudit', 'WechatTicketController@speedUpAudit'); //加急审核
            $api->post('queryQuotaNew', 'WechatTicketController@queryQuotaNew'); //查询服务商的当月提审限额（quota）和加急次数
            $api->post('getWxaQrcode', 'WechatTicketController@getWxaQrcode'); //获取体验版二维码

            $api->post('getAppletsStatusByStore', 'WechatTicketController@getAppletsStatusByStore'); // 获取微信小程序创建步骤
            $api->post('updateAppletsStatus', 'WechatTicketController@updateAppletsStatus'); // 更新微信小程序创建步骤
            $api->post('checkAuthorizationAndUpdateStep', 'WechatTicketController@checkAuthorizationAndUpdateStep'); // 查询授权是否正确，授权后进入下一步
            $api->any("getTemplateDraftList", "WechatTicketController@getTemplateDraftList"); // 获取代码草稿列表

            // 微信快速创建小程序-创建小程序码
            $api->any("createAppletQrcode", "WechatThirdController@createAppletQrcode");
            // 微信快速创建小程序-获取小程序码等信息
            $api->any("getAppletInfo", "WechatThirdController@getAppletInfo");
            // 微信点餐订单信息同步（渠道商），扫码点餐
            $api->post("wechatOrder", "WechatThirdController@wechatOrder");
        });

        $api->group(['prefix' => "aliPay"],function($api){
            //接收支付宝的授权通知
            $api->any("getMessage", "AliPayTicketController@message");
            //根据小程序名称搜索小程序的appid
            $api->any("getAppletAppId", "AliPayTicketController@getAppletAppId");

            //构建小程序版本
            $api->any("aliPayOpenMiniVersionUpload", "AliPayTicketController@aliPayOpenMiniVersionUpload");
            //查询使用模板的小程序列表
            $api->any("getAliPayOpenMiniTemplateUsageQuery", "AliPayTicketController@getAliPayOpenMiniTemplateUsageQuery");
            //小程序提交审核
            $api->any("getAliPayOpenMiniVersionAuditApply", "AliPayTicketController@getAliPayOpenMiniVersionAuditApply");
            //小程序进行发布上架或者下架
            $api->any("getAliPayOpenMiniVersionOnline", "AliPayTicketController@getAliPayOpenMiniVersionOnline");
            //小程序查询信息或者修改小程序基础信息
            $api->any("getAliPayOpenMiniBaseInfoQuery", "AliPayTicketController@getAliPayOpenMiniBaseInfoQuery");
            //小程序退回开发 | 小程序结束灰度 | 小程序删除版本 | 小程序撤销审核
            $api->any("getAliPayOpenMiniVersionAuditedCancel", "AliPayTicketController@getAliPayOpenMiniVersionAuditedCancel");
            //保存FormId
            $api->any("saveFormId", "AliPayTicketController@saveFormId");
            //获取支付宝小程序模板列表
            $api->any("getTemplateListData", "AliPayTicketController@getTemplateListData");
            //保存支付宝小程序模板
            $api->any("addTemplateListData", "AliPayTicketController@addTemplateListData");
            //修改支付宝小程序模板
            $api->any("updateTemplateListData", "AliPayTicketController@updateTemplateListData");
            //获取该商家小程序的aesKey密钥
            $api->any("getAliPayOpenAuthAppAesGet", "AliPayTicketController@getAliPayOpenAuthAppAesGet");
            //获取支付宝第三方应用的appid
            $api->any("getAliPayOpenAppletId", "AliPayTicketController@getAliPayOpenAppletId");
            $api->post('updateAliAppletsStep', 'AliPayTicketController@updateAliAppletsStep');   //更新下一步接口
            $api->post('getAliAppletsStep', 'AliPayTicketController@getAliAppletsStep');   //查询步骤状态
            $api->post('getTemplate', 'AliPayTicketController@getTemplate');   //获取模板
            $api->post('aliPayOpenMiniStatus', 'AliPayTicketController@aliPayOpenMiniStatus');   //获取模板
            $api->post('aliPayOpenBusinessCertifyy', 'AliPayTicketController@aliPayOpenBusinessCertifyy');   //代商户升级
            $api->post('updateAliVersion', 'AliPayTicketController@updateAliVersion');   //传入版本号

        });

        //系统会员卡
        $api->group(['prefix' => "member"], function($api){
            $api->post("member_update", "UserMembershipCardController@member_update");
            $api->post("member_cz", "UserMembershipCardController@member_cz"); //商家充值-c扫b
        });

        // 微信正式会员
        $api->group(['prefix' => "member"], function($api){
            $api->post("addWechatMember", "WechatMemberController@addWechatMember"); // 新增微信正式会员
        });

        //微信小程序
        $api->group(['prefix' => "order"], function($api){
            $api->post("test", "ImmediateDeliveryController@test"); //沙盒测试
            //查询此桌号订单信息
            $api->post("getTableOrderInfo","OrderController@getTableOrderInfo");
            // 更新小程序用户商品是否打印状态
            $api->post("updateUserGoodIsPrint","OrderController@updateUserGoodIsPrint");
        });

    });
});
