<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace' => 'UserWeb','prefix' => 'userweb'], function () {
    Route::get('index', 'ViewController@index');
    Route::get('setupshop', 'ViewController@setupshop');
    Route::get('applettemplate', 'ViewController@applettemplate');
    Route::get('home', 'ViewController@home');
    Route::get('interestedbuyers', 'ViewController@interestedbuyers');
    Route::get('smallprogram', 'ViewController@smallprogram');
    Route::get('Exhibition', 'ViewController@Exhibition');
    Route::get('createApplet', 'ViewController@createApplet');
    Route::get('WeChatshenheing', 'ViewController@WeChatshenheing');
    Route::get('WeChatshenhefail', 'ViewController@WeChatshenhefail');
    Route::get('WeChatshenhesuccess', 'ViewController@WeChatshenhesuccess');
    Route::get('auditSuccess', 'ViewController@auditSuccess');
    Route::get('contactus', 'ViewController@contactus');
    Route::get('pagedetails', 'ViewController@pagedetails');
    Route::get('wechatfabu', 'ViewController@wechatfabu');
    Route::get('smallprogramfabu', 'ViewController@smallprogramfabu');
    Route::get('Exhibitionfabu', 'ViewController@Exhibitionfabu');
    Route::get('contactusfabu', 'ViewController@contactusfabu');
    Route::get('indexpagetwo', 'ViewController@indexpagetwo');
});
