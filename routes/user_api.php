<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {

    //user 端无需token
    $api->group(['namespace' => 'App\Api\Controllers\User', 'prefix' => 'user'], function ($api) {
        $api->any('login', 'LoginController@login');
//        $api->any('register_user', 'LoginController@register_user');//用户注册
        $api->any('edit_password', 'LoginController@edit_password');

        $api->any('sub_code_url', 'SelectController@sub_code_url');
        $api->any('s_code_url', 'SelectController@s_code_url');

        $api->any('banners', 'BannerController@banners'); //公共banner
        $api->any('couponQr', 'StoreController@couponQr'); //发券二维码

        $api->any('getAreaNo', 'EasyPayStoreController@getAreaNo'); //获取易生地区
        $api->any('getBanks', 'EasyPayStoreController@getBanks'); //获取易生银行
        $api->any('getMcc', 'EasyPayStoreController@getMcc'); //获取易生mcc
        $api->any('getProjectId', 'EasyPayStoreController@getProjectId'); //获取易生项目ID
        $api->any('getTermModel', 'EasyPayStoreController@getTermModel'); //参数查询 设备型号(termModel)

        $api->any('getDevicePermissions', 'DevicePermissionsController@getDevicePermissions'); // 获取设备权限
        $api->post('mapEditName', 'InfoController@mapEditName');
        $api->post('pay_test', 'TestController@pay_test');
        $api->post('order_query_test', 'TestController@order_query_test');
        $api->post('refund_query', 'TestController@refund_query');
        $api->any('getFirstPayWay', 'StoreController@getFirstPayWay');
        $api->any('register_user', 'UserController@register_user');
        $api->any('easy_pay_notify', 'ShoppingController@easy_pay_notify');//支付回调
        $api->any('forget_password', 'UserController@forget_password');
        $api->post('testShopping', 'TestController@testShopping');
        $api->any('getUserId', 'TestController@getUserId');
    });

    //需要token
    $api->group(['namespace' => 'App\Api\Controllers\User', 'prefix' => 'user', 'middleware' => 'user.api'], function ($api) {
        $api->any('index', 'LoginController@index');
        $api->any('ranking', 'SelectController@ranking');
        $api->any('dk_select', 'SelectController@dk_select');
        $api->any('select_pid_user', 'SelectController@select_pid_user');
        $api->any('clear_ways_type', 'SelectController@clear_ways_type');
        $api->post('volume', 'SelectController@volume'); //门店交易量

        //实名认证
        $api->any('getUserAuthInfo', 'UserAuthController@getUserAuthInfo');
        $api->any('getUserAuth', 'UserAuthController@getUserAuth');
        $api->any('userAuth', 'UserAuthController@userAuth');
        $api->any('getFaceId', 'UserAuthController@getFaceId');
        $api->any('queryfacerecord', 'UserAuthController@queryfacerecord');

        //月统计
        $api->any('Order_mouth_count', 'OrderCountController@Order_mouth_count');
        $api->any('order_month_count', 'OrderCountController@order_month_count');

        $api->any('merchant_lists', 'SelectController@merchant_lists');
        $api->any('add_merchant', 'SelectController@add_merchant');
        $api->any('bind_merchant', 'SelectController@bind_merchant');
        $api->any('up_merchant', 'SelectController@up_merchant');
        $api->any('del_merchant', 'SelectController@del_merchant');
        $api->any('merchant_info', 'SelectController@merchant_info');

        $api->any('info', 'UserController@userInfo');
        $api->any('users_lists_all', 'UserController@users_lists_all');

        $api->any('get_sub_users', 'UserController@get_sub_users');
        $api->any('app_get_sub_users', 'UserController@app_get_sub_users');
        $api->any('app_user_info', 'UserController@app_user_info');

        $api->any('getUserPhone', 'UserController@getUserPhone');
        $api->any('get_sf_sub_users', 'UserController@get_sf_sub_users');

        $api->any('set_withdraw', 'UserController@set_withdraw');
        $api->any('set_zeroRate', 'UserController@set_zeroRate');
        $api->any('set_incoming', 'UserController@set_incoming');
        $api->any('set_bind_qr', 'UserController@set_bind_qr');

        $api->any('get_del_sub_users', 'UserController@get_del_sub_users');
        $api->any('add_sub_user', 'UserController@add_sub_user');
        $api->any('add_sub_user_new', 'UserController@add_sub_user_new');
        $api->any('del_sub_user', 'UserController@del_sub_user');
        $api->any('fog_sub_user', 'UserController@fog_sub_user');
        $api->any('cdel_sub_user', 'UserController@cdel_sub_user');

        $api->any('user_info', 'UserController@user_info');
        $api->any('my_info', 'UserController@my_info');
        $api->any('up_user', 'UserController@up_user');
        $api->any('set_password', 'UserController@set_password');
        $api->any('edit_login_phone', 'UserController@edit_login_phone');

        $api->any('add_pay_password', 'UserController@add_pay_password');
        $api->any('edit_pay_password', 'UserController@edit_pay_password');
        $api->any('forget_pay_password', 'UserController@forget_pay_password');
        $api->any('check_pay_password', 'UserController@check_pay_password');
        $api->post('check_platform_pay_password', 'UserController@check_platform_pay_password'); //验证平台支付密码
        $api->any('is_pay_password', 'UserController@is_pay_password');

        $api->any('get_my_data', 'UserController@get_my_data');

        $api->any('user_ways_all', 'UserController@user_ways_all');

        $api->any('user_ways_default', 'UserController@user_ways_default');

        $api->any('user_ways_info', 'UserController@user_ways_info');
        $api->any('edit_user_rate', 'UserController@edit_user_rate');
        $api->any('del_user_rate', 'UserController@del_user_rate');

        $api->any('edit_user_un_rate', 'UserController@edit_user_un_rate');
        $api->any('edit_user_unqr_rate', 'UserController@edit_user_unqr_rate');

        $api->any('edit_user_store_all_rate', 'UserController@edit_user_store_all_rate');

        $api->any('edit_user_un_store_all_rate', 'UserController@edit_user_un_store_all_rate');
        $api->any('edit_user_unqr_store_all_rate', 'UserController@edit_user_unqr_store_all_rate');

        $api->any('getRateProfit', 'UserController@getRateProfit');//查询代理分润比例（建行通道）
        $api->any('editRateProfit', 'UserController@editRateProfit');//编辑代理分润比例（建行通道）


        $api->any('alipay_isv_config', 'ConfigController@alipay_isv_config');
        $api->any('alipay_zft_config', 'ConfigController@alipay_zft_config');

        $api->any('weixin_config', 'ConfigController@weixin_config');
        $api->any('wechat_cash_coupon_config', 'ConfigController@wechatCashCouponConfig');
        $api->any('save_wechat_cash_coupon_config', 'ConfigController@saveWechatCashCouponConfig');
        $api->any('weixina_config', 'ConfigController@weixina_config'); //官方微信a
        $api->any('jd_config', 'ConfigController@jd_config');
        $api->any('new_land_config', 'ConfigController@new_land_config');
        $api->any('h_config', 'ConfigController@h_config');
        $api->any('mqtt_config', 'ConfigController@mqtt_config');

        // 微信商家券配置
        $api->any('wechat_merchant_cash_coupon_config', 'ConfigController@wechatMerchantCashCouponConfig');
        $api->any('save_wechat_merchant_cash_coupon_config', 'ConfigController@saveWechatMerchantCashCouponConfig');

        $api->any('pay_ways_sort', 'SetController@pay_ways_sort');
        $api->any('pay_ways_sort_edit', 'SetController@pay_ways_sort_edit');
        $api->post('pay_ways_sort_top', 'SetController@pay_ways_sort_top');
        $api->any('user_store_set_status', 'SetController@user_store_set_status');
        $api->any('pay_ways_sort_start', 'SetController@pay_ways_sort_start');
        $api->any('store_ways_set', 'SetController@store_ways_set');
        $api->any('store_ways_select', 'SetController@store_ways_select');
        $api->post('pay_ways_open', 'SetController@pay_ways_open');

        $api->post('edit_pwd', 'UserController@edit_pwd');  //更改密码
        $api->post('reset_pwd', 'UserController@reset_pwd');  //重置密码
        $api->post('update_affiliation', 'UserController@update_affiliation');  //代理商转移

        //门店列表
        $api->any('store_lists', 'StoreController@store_lists');
        $api->any('store_lists_new', 'StoreController@store_lists_new');
        $api->any('store_pc_lists', 'StoreController@store_pc_lists');
        $api->any('store_all_lists', 'StoreController@store_all_lists');

        $api->any('storeListByStatus', 'StoreController@storeListByStatus'); //根据审核状态获取门店列表
        $api->any('createMerchantNo', 'StoreController@createMerchantNo'); //创建商户号

        $api->any('add_easypay_store', 'StoreController@add_easypay_store');
        $api->any('update_easypay_store', 'StoreController@update_easypay_store');
        $api->any('get_easypay_store', 'StoreController@get_easypay_store');
        $api->any('checkEasypayStore', 'StoreController@checkEasypayStore'); //审核易生进件资料

        $api->any('add_vbill_store', 'StoreController@add_vbill_store');
        $api->any('update_vbill_store', 'StoreController@update_vbill_store');
        $api->any('add_ysepay_store', 'StoreController@add_ysepay_store');
        $api->any('update_ysepay_store', 'StoreController@update_ysepay_store');
        $api->any('add_lkl_store', 'StoreController@add_lkl_store');
        $api->any('update_lkl_store', 'StoreController@update_lkl_store');
        $api->any('add_easePay_store', 'StoreController@add_easePay_store');
        $api->any('update_easePay_store', 'StoreController@update_easePay_store');

        $api->any('store', 'StoreController@store');
        $api->any('up_store', 'StoreController@up_store');
        $api->any('get_store_base_info', 'StoreController@getStoreBaseInfo');
        $api->any('pay_ways_all', 'StoreController@pay_ways_all');
        $api->post('pay_ways_all_new', 'StoreController@pay_ways_all_new');
        $api->any('store_all_pay_way_lists', 'StoreController@store_all_pay_way_lists');
        $api->any('store_open_pay_way_lists', 'StoreController@store_open_pay_way_lists');
        $api->any('dlb_store_info', 'StoreController@dlb_store_info'); //获取哆啦宝-进件所需相应信息
        $api->post('checkAddStoreInfo', 'StoreController@checkAddStoreInfo'); //进件参数检查

        $api->any('company_pay_ways_info', 'StoreController@company_pay_ways_info');
        $api->any('open_company_pay_ways', 'StoreController@open_company_pay_ways');

        $api->any('pay_ways_info', 'StoreController@pay_ways_info');

        $api->any('edit_store_rate', 'StoreController@edit_store_rate');
        $api->any('edit_store_un_rate', 'StoreController@edit_store_un_rate');
        $api->any('edit_store_unqr_rate', 'StoreController@edit_store_unqr_rate');
        $api->any('edit_store_zero_rate', 'StoreController@edit_store_zero_rate'); //设置门店零费率开关

        $api->any('del_store', 'StoreController@del_store');
        $api->any('col_store', 'StoreController@col_store');
        $api->any('ope_store', 'StoreController@ope_store');
        $api->any('clear_store', 'StoreController@clear_store');
        $api->any('rec_store', 'StoreController@rec_store');
        $api->any('update_user', 'StoreController@update_user');

        $api->any('open_ways_type', 'StoreController@open_ways_type');
        $api->any('add_sub_store', 'StoreController@add_sub_store');
        $api->any('check_store', 'StoreController@check_store');
        $api->any('alipay_auth', 'StoreController@alipay_auth');

        $api->any('store_pay_qr', 'StoreController@store_pay_qr');
        $api->any('store_openid_qr', 'StoreController@store_openid_qr');//获取商户openid
        $api->any('get_mer_id', 'StoreController@get_mer_id'); //汇付-获取商户号
        $api->any('hui_pay_bind', 'StoreController@hui_pay_bind'); //汇付-绑定机具
//        $api->any('couponQr', 'StoreController@couponQr'); //发券二维码

        $api->any('storeListToInstall', 'StoreController@storeListToInstall');
        $api->any('updateStoreInstallStatus', 'StoreController@updateStoreInstallStatus');

        $api->post('updateDaDaConfig', 'StoreController@updateDaDaConfig');  //达达配送设置
        $api->post('applyWeChatRealName', 'StoreController@applyWeChatRealName');  //微信实名认证申请
        $api->post('queryWeChatRealName', 'StoreController@queryWeChatRealName');  //微信实名认证结果查询
        $api->post('backWeChatRealName', 'StoreController@backWeChatRealName');  //微信实名认证撤销
        $api->post('queryGrantStatusWeChatRealName', 'StoreController@queryGrantStatusWeChatRealName');  //微信子商户授权状态查询

        $api->any('notice_news', 'InfoController@notice_news');
        $api->any('add_notice_news', 'InfoController@add_notice_news');
        $api->any('del_notice_news', 'InfoController@del_notice_news');
        $api->any('toutiao', 'InfoController@toutiao');
        $api->any('toutiao_top', 'InfoController@toutiao_top');
        $api->any('notice_news_type', 'InfoController@notice_news_type');

        $api->any('merchant_index', 'InfoController@merchant_index');
        $api->any('add_merchant_index', 'InfoController@add_merchant_index');
        $api->any('del_merchant_index', 'InfoController@del_merchant_index');

        $api->any('my_index', 'InfoController@my_index');
        $api->any('add_my_index', 'InfoController@add_my_index');
        $api->any('del_my_index', 'InfoController@del_my_index');
        $api->any('user_tt_set', 'InfoController@user_tt_set');

//        $api->any('banners', 'BannerController@banners');
        $api->any('add_banners', 'BannerController@add_banners');
        $api->any('del_banners', 'BannerController@del_banners');
        $api->any('banner_type', 'BannerController@banner_type');

        $api->any('order', 'OrderController@order');
        $api->any('order_app', 'OrderController@order_app');
        $api->any('order_count_app', 'OrderController@order_count_app');
        $api->any('sub_order_count_app', 'OrderController@sub_order_count_app');
        $api->any('order_info', 'OrderController@order_info');

        $api->any('VipDraw', 'OrderController@VipDraw');

        $api->any('order_count', 'OrderController@order_count');
        $api->any('order_data', 'OrderController@order_data');
        $api->any('store_order_data', 'OrderController@store_order_data');
        $api->any('settle_order', 'OrderController@settle_order');

        //花呗分期
        $api->any('fq/order', 'AlipayFqOrderController@order');
        $api->any('fq/order_info', 'AlipayFqOrderController@order_info');

        $api->any('fq/set_fq_get_rate', 'AlipayFqOrderController@set_fq_get_rate');
        $api->any('fq/set_fq_get_list', 'AlipayFqOrderController@set_fq_get_list');
        $api->any('fq/del_fq_get_rate', 'AlipayFqOrderController@del_fq_get_rate');

        $api->post('profit_sharing_lists', 'OrderController@profit_sharing_lists'); //微信分账流水

        $api->get('indexDataOverview', 'OrderController@indexDataOverview'); //服务商首页 数据概览
        $api->post('indexTransactionData', 'OrderController@indexTransactionData'); //服务商首页 交易数据
        $api->post('passagewayStatistics', 'OrderController@passagewayStatistics'); //服务商首页 支付通道金额/笔数

        //空码管理
        $api->any('QrLists', 'QrController@QrLists');
        $api->any('QrListinfos', 'QrController@QrListinfos');
        $api->any('DownloadQr', 'QrController@DownloadQr');
        $api->any('createQr', 'QrController@createQr');
        $api->any('bindQr', 'QrController@bindQr');
        $api->any('unbindQr', 'QrController@unbindQr');
        $api->any('transferCode', 'QrController@transferCode');
        $api->any('backCode', 'QrController@backCode');

        $api->any('qr_code_hb_list', 'QrController@qr_code_hb_list');
        $api->any('qr_code_hb_add', 'QrController@qr_code_hb_add');
        $api->any('qr_code_hb_del', 'QrController@qr_code_hb_del');
        $api->any('getActivationQr', 'QrController@getActivationQr');

        $api->any('activity_store_rate_list', 'ActivityController@activity_store_rate_list');
        $api->any('add_activity_store_rate', 'ActivityController@add_activity_store_rate');
        $api->any('del_activity_store_rate', 'ActivityController@del_activity_store_rate');

        $api->any('dlb_config', 'ConfigController@dlb_config');  //哆啦宝
        $api->any('hltx_config', 'ConfigController@hltx_config'); //葫芦天下
        $api->any('tf_config', 'ConfigController@tf_config');  //传化
        $api->any('vbill_config', 'ConfigController@vbill_config'); //随行付支付配置
        $api->any('vbilla_config', 'ConfigController@vbilla_config'); //随行付a支付配置
        $api->any('easypay_config', 'ConfigController@easypay_config'); //易生支付支付配置
        $api->any('update_config', 'ConfigController@update_config'); //代理升级独立配置
        $api->any('hui_pay_config', 'ConfigController@hui_pay_config'); //汇付
        $api->any('hkrt_config', 'ConfigController@hkrt_config'); //海科融通
        $api->any('linkage_config', 'ConfigController@linkage_config'); //联动优势
        $api->post('business_scope', 'StoreController@business_scope'); //营业范围
        $api->post('putVoice', 'StoreController@putVoice'); //营业范围
        $api->post('voicePayQr', 'StoreController@voicePayQr'); //营业范围
        $api->post('wftpay_config', 'ConfigController@wftpay_config'); //威富通
        $api->post('hwcpay_config', 'ConfigController@hwcpay_config'); //威富通
        $api->post('dada_config', 'ConfigController@dada_config'); //达达配置
        $api->post('dongguan_config', 'ConfigController@dongguan_config'); //东莞银行
        $api->post('yinsheng_config', 'ConfigController@yinsheng_config'); //银盛
        $api->post('qfpay_config', 'ConfigController@qfpay_config'); //钱方

        // 微信代金券管理
        $api->post('wx_cash_coupon_list', 'WxCashCouponController@wxCashCouponList');
        $api->post('check_wechat_coupon', 'WxCashCouponController@checkWechatCoupon');
        $api->post('delete_wechat_coupon', 'WxCashCouponController@deleteWechatCoupon');

        // 微信第三方平台管理
        $api->any('wechat_third_config_info', 'WechatThirdConfigController@wechatThirdConfigInfo');
        $api->any('save_wechat_third_config', 'WechatThirdConfigController@saveWechatThirdConfig');

        // 支付宝第三方平台管理
        $api->any('ali_third_config_info', 'AliThirdConfigController@aliThirdConfigInfo');
        $api->any('save_ali_third_config', 'AliThirdConfigController@saveAliThirdConfig');

        $api->post('easyPayMerchantAccess', 'EasyPayStoreController@easyPayMerchantAccess'); //易生商户入网 资料补充
        $api->post('alterMerchant', 'EasyPayStoreController@alterMerchant'); //商户信息变更
        $api->post('alterPayAcc', 'EasyPayStoreController@alterPayAcc'); //结算账户变更
        $api->post('alterFunc', 'EasyPayStoreController@alterFunc'); //商户功能变更
        $api->post('alterPayAccApp', 'EasyPayStoreController@alterPayAccApp'); //结算账户变更App
        $api->post('alterFuncApp', 'EasyPayStoreController@alterFuncApp'); //商户功能变更App

        $api->post('addPercentage', 'UserController@percentage');

        $api->any('add_cash_back_rule', 'CashBackRuleController@addCashBackRule');//添加终端交易达标返现规则
        $api->any('ruleList', 'CashBackRuleController@ruleList');//查询终端交易达标返现规则
        $api->any('selRule', 'CashBackRuleController@selRule');//构造下拉列表
        $api->any('getCashBackRuleId', 'CashBackRuleController@getCashBackRuleId');//根据id查询终端交易达标返现规则
        $api->any('testing_standard', 'CashBackRuleController@testing_standard');//检测终端交易达标返现规则
        $api->any('del_cash_back_rule', 'CashBackRuleController@delCashBackRule');//删除交易达标返现规则
        $api->any('up_cash_back_rule', 'CashBackRuleController@upCashBackRule');//修改终端交易达标返现规则
        $api->any('add_user_rule', 'CashBackRuleController@addUserRule');//修改终端交易达标返现规则
        $api->any('get_user_rule', 'CashBackRuleController@getUserRule');//修改终端交易达标返现规则
        $api->any('store_transaction_reward_list', 'StoreTransactionRewardController@storeTransactionRewardList');//查询商户返现列表
        $api->any('get_store_transaction_reward', 'StoreTransactionRewardController@getStoreTransactionReward');//查看商户交易返现详情
        $api->any('get_store_transaction', 'CashBackRuleController@getStoreTransaction');//查看商户交易
        $api->any('close_user_rule', 'CashBackRuleController@closeUserRule');//关闭商户交易
        $api->any('openUserRule', 'CashBackRuleController@openUserRule');//开启商户交易
        $api->any('editUserRule', 'CashBackRuleController@editUserRule');//修改下级返现金额
        $api->any('userRules', 'CashBackRuleController@userRules');//查询代理返现
        $api->any('editSettlementType', 'UserController@editSettlementType');//设置零费率结算方式
        $api->any('getSettlementType', 'UserController@getSettlementType');//获取零费率结算方式
        $api->any('easypayElectronic', 'EasyPayElectronicController@easypayElectronic');//易生电子协议
        $api->any('sendSms', 'EasyPayElectronicController@sendSms');//易生电子协议 发送验证码
        $api->any('createContract', 'EasyPayElectronicController@createContract');//易生电子协议 创建合同
        $api->any('signContract', 'EasyPayElectronicController@signContract');//易生电子协议 签署合同
        $api->any('queryContract', 'EasyPayElectronicController@queryContract');//易生电子协议 查询合同
        $api->any('getContract', 'EasyPayElectronicController@getContract');//
        $api->any('addContractInfo', 'EasyPayElectronicController@addContractInfo');//添加合同信息
        $api->any('downloadContract', 'EasyPayElectronicController@downloadContract');//易生电子协议 下载合同
        $api->any('deleteContract', 'EasyPayElectronicController@deleteContract');

        $api->any('getAddressList', 'ShoppingController@address_list');//查询代理商收货地址
        $api->any('addAddress', 'ShoppingController@add_address');//新增收货地址
        $api->any('delAddress', 'ShoppingController@del_address');//删除收货地址
        $api->any('getShoppingCategoryList', 'ShoppingController@shopping_category_list');//商品类别列表
        $api->any('shoppingCategorySelect', 'ShoppingController@shopping_category_select');//商品类别列表 下拉选使用
        $api->any('addShoppingCategory', 'ShoppingController@add_shopping_category');//新增商品类别
        $api->any('delShoppingCategory', 'ShoppingController@del_shopping_category');//删除商品类别
        $api->any('getShoppingGoodsList', 'ShoppingController@shopping_goods_list');//查询商品列表
        $api->any('addShoppingGoods', 'ShoppingController@add_goods');//查询商品列表
        $api->any('delShoppingGoods', 'ShoppingController@del_goods');//删除商品
        $api->any('getGoodsInfo', 'ShoppingController@get_goods_info');//获取商品详情
        $api->any('getShoppingOrderList', 'ShoppingController@shopping_order_list');//获取订单列表
        $api->any('shoppingPay', 'ShoppingController@shopping_pay');//订单支付

        $api->any('editOrderStatus', 'ShoppingController@edit_order_status');//更新订单状态
        $api->any('deliverGoods', 'ShoppingController@deliver_goods');//发货
        $api->any('editOrder', 'ShoppingController@edit_order');//修改订单
        $api->any('delOrder', 'ShoppingController@del_order');//删除订单
        $api->any('getAgentLevelList', 'AgentLevelsController@agent_level_lists');//查询等级
        $api->any('addAgentLevel', 'AgentLevelsController@add_agent_level');//新增等级
        $api->any('updateAgentLevel', 'AgentLevelsController@update_agent_level');//新增等级
        $api->any('getIndexDayData', 'SelectController@getIndexDayData');//app获取首页当日数据
        $api->any('getIndexMonthData', 'SelectController@getIndexMonthData');//app获取首页当日数据
        $api->post('getIncome', 'SelectController@getIncome');
        $api->post('getUserWalletDetails', 'SelectController@getUserWalletDetails');
        $api->post('getUserRewardList', 'SelectController@getUserRewardList');
        $api->post('getUserWithdrawalList', 'SelectController@getUserWithdrawalList');
        $api->post('getPerformance', 'SelectController@getPerformance');
        $api->post('getPerformanceActivation', 'SelectController@getPerformanceActivation');
        $api->post('getSettlementDayList', 'SelectController@getSettlementDayList');
        $api->post('getSettlementMonthList', 'SelectController@getSettlementMonthList');
        $api->post('getActivationList', 'SelectController@getActivationList');
        $api->post('getUserReturnAmountList', 'SelectController@getUserReturnAmountList');
        $api->post('getUserReturnAmountListApp', 'SelectController@getUserReturnAmountListApp');
        $api->post('getUserServiceChargeListApp', 'SelectController@getUserServiceChargeListApp');
        $api->post('getUserServiceChargeList', 'SelectController@getUserServiceChargeList');
        $api->post('getUserTrainRewardList', 'SelectController@getUserTrainRewardList');
        $api->post('getUserTrainRewardListApp', 'SelectController@getUserTrainRewardListApp');
        $api->post('getUserLevel', 'StoreController@getUserLevel');
        $api->post('getUserLevelIs', 'StoreController@getUserLevelIs');
        $api->post('up_likeness', 'UserController@up_likeness');
        $api->post('get_user_qr_num', 'StoreController@get_user_qr_num');
        $api->post('getUserShoppingRewardList', 'SelectController@getUserShoppingRewardList');
        $api->post('getSubUserTrainRewardList', 'SelectController@getSubUserTrainRewardList');
        $api->post('getUserShoppingRewardListPc', 'SelectController@getUserShoppingRewardListPc');
        $api->post('getSubUserTrainRewardListPc', 'SelectController@getSubUserTrainRewardListPc');
        $api->post('getUserQrList', 'QrController@getUserQrList');
        $api->post('upShoppingRewardNum', 'QrController@upShoppingRewardNum');
        $api->post('getUserBoxRebateList', 'SelectController@getUserBoxRebateList');
        $api->post('reset_store_pwd', 'StoreController@reset_store_pwd');
        $api->post('getYsStore', 'UserController@getYsStore');
        $api->post('getEsStore', 'UserController@getEsStore');
        $api->post('getUserArchitectureRewardList', 'SelectController@getUserArchitectureRewardList');
        $api->post('emptyUserAuth', 'UserController@emptyUserAuth');
        $api->post('emptyEsign', 'UserController@emptyEsign');

        $api->post('getUserTransferList', 'SelectController@getUserTransferList');

    });
    
    //需要token
    $api->group(['namespace' => 'App\Api\Controllers\AliRuyi', 'prefix' => 'aliruyi', 'middleware' => 'user.api'], function ($api) {
        $api->post('alipay_spi_config', 'ConfigController@alipaySpiConfig');
    });

    $api->group(['namespace' => 'App\Api\Controllers\User', 'prefix' => 'user' , 'middleware' => 'user.api'], function ($api) {
        $api->any('getMerchantBalance', 'MerchantRechargeController@getMerchantBalance'); //查询商户余额
        $api->any('paymentLog', 'MerchantRechargeController@paymentLog'); //充值记录
        $api->any('consumer_details', 'MerchantRechargeController@consumer_details'); //查询消费明细
        $api->any('payment_type', 'MerchantRechargeController@payment_type');
        $api->any('merchant_payments_list', 'MerchantRechargeController@merchant_payments_list'); //添加支付方式

    });

    $api->group(['namespace' => 'App\Api\Controllers\User', 'prefix' => 'user' , 'middleware' => 'user.api'], function ($api) {
        $api->any('realName', 'UserInfoController@realName'); //实名认证
        $api->any('getUserInfo', 'UserInfoController@getUserInfo'); //获取实名认证
    });

    //无需token
    $api->group(['namespace' => 'App\Api\Controllers\Esign', 'prefix' => 'esign'], function ($api) {
        $api->any('getUserEsign', 'EsignPersonController@getUserEsign'); //查询签署信息

        $api->any('personAuth', 'EsignPersonController@personAuth'); //个人认证
        $api->any('createByFile', 'SignFlowController@createByFile'); //文件发起签署
        $api->any('templatesTComponents', 'SignFlowController@templatesTComponents'); //查询合同模板中控件详情
        $api->any('downloadFile', 'SignFlowController@downloadFile');

        $api->any('notify_url', 'NotifyController@notify_url'); //异步通知
    });


});
