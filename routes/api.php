<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    $api->group(['namespace' => 'App\Api\Controllers', 'prefix' => 'testluyu'], function ($api) {
        $api->any('get_sign_key', 'TestController@get_sign_key');
        $api->any('wechat_sandbox_pay', 'TestController@wechat_sandbox_pay');
        $api->any('wechat_sandbox_order_query', 'TestController@wechat_sandbox_order_query');
        $api->any('wechat_sandbox_refund', 'TestController@wechat_sandbox_refund');
        $api->any('wechat_sandbox_query_refund', 'TestController@wechat_sandbox_query_refund');
        $api->any('wechat_sandbox_bill', 'TestController@wechat_sandbox_bill');
        $api->any('test_ly', 'TestController@test_ly');
    });

    //基本查询 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Basequery', 'prefix' => 'basequery'], function ($api) {
        $api->any('city', 'CityController@city');
        $api->any('test', 'TestController@test');
        $api->any('test1', 'TestController@test1');
        $api->any('appUpdate', 'SelectController@appUpdate');
        $api->any('settle_mode_type', 'SelectController@settle_mode_type');
        $api->any('alipay_isv_info', 'SelectController@alipay_isv_info');
        $api->any('beian', 'SelectController@beian');
        $api->any('get_logo', 'SelectController@get_logo');
        $api->any('vbill_store_category', 'SelectController@vbill_store_category'); //随行付门店经营类型

        $api->any('tf_store_bb', 'SelectController@tf_store_bb');

        $api->any('xcx_logos_info', 'SelectController@xcx_logos_info'); //小程序二维码

        $api->any('selectDictionaries', 'SelectController@selectDictionaries');//数据字典查询

        $api->any('getAreaCodeList', 'CommonController@getAreaCodeList');//获取商户地区码-通用
        $api->any('getStoreMccPList', 'CommonController@getStoreMccPList');//获取商户MCC-通用
        $api->any('getStoreMccList', 'CommonController@getStoreMccList');//获取商户MCC-通用
        $api->any('getFhcMccPList', 'CommonController@getFhcMccPList');//获取金控商户MCC-通用
        $api->any('getFhcMccList', 'CommonController@getFhcMccList');//获取金控商户MCC-通用
        $api->any('getBankTypeList', 'CommonController@getBankTypeList');//获取银行大类-通用
        $api->any('getBankCodeList', 'CommonController@getBankCodeList');//获取银行代码-通用
        $api->any('getLklStoreMccPList', 'CommonController@getLklStoreMccPList');//获取商户MCC-通用-lkl
        $api->any('getLklStoreMccList', 'CommonController@getLklStoreMccList');//获取商户MCC-通用-lkl
        $api->any('getEaseStoreMccPList', 'CommonController@getEaseStoreMccPList');//获取商户MCC-通用-首易信
        $api->any('getEaseStoreMccList', 'CommonController@getEaseStoreMccList');//获取商户MCC-通用-首易信

    });


    //识别查询 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Basequery', 'prefix' => 'basequery'], function ($api) {
        $api->any('businessLicense', 'AiController@businessLicense');
        $api->any('idcardFront', 'AiController@idcardFront');
        $api->any('idcardBack', 'AiController@idcardBack');
        $api->any('getBankCat', 'AiController@getBankCat');
        $api->any('getSubBank', 'AiController@getSubBank');
        $api->any('getMccId', 'AiController@getMccId');
        $api->any('getMccName', 'AiController@getMccName');
        $api->any('getAreaId', 'AiController@getAreaId');
        $api->any('openways', 'StorePayWaysController@openways');
    });

    //基本查询 无需token
//    $api->group(['namespace' => 'App\Api\Controllers\Basequery', 'prefix' => 'basequery'], function ($api) {
//        $api->any('tf_store_bb', 'SelectController@tf_store_bb');
//        $api->any('xcx_logos_info', 'SelectController@xcx_logos_info'); //小程序二维码
//    });

    //基本查询 需token
    $api->group(['namespace' => 'App\Api\Controllers\Basequery', 'prefix' => 'basequery', 'middleware' => 'public.api'], function ($api) {
        $api->any('upload', 'UploadController@upload');
        $api->any('webupload', 'UploadController@webupload');
        $api->any('contact', 'AppIndexController@contact');
        $api->any('file_content', 'UploadController@file_content');

        $api->any('merchant_lists', 'SelectController@merchant_lists');
        $api->any('store_type', 'SelectController@store_type');
        $api->any('store_category', 'SelectController@store_category');
        $api->any('bank', 'SelectController@bank');
        $api->any('sub_bank', 'SelectController@sub_bank');
        $api->any('order_query_b', 'SelectController@order_query_b');
        $api->any('app_oem_info', 'SelectController@app_oem_info');
        $api->any('j_push_info', 'SelectController@j_push_info');
        $api->any('sms_info', 'SelectController@sms_info');
        $api->any('sms_type', 'SelectController@sms_type');
        $api->any('update_order', 'SelectController@update_order');
        $api->any('app_logos_info', 'SelectController@app_logos_info');

        $api->any('/updateInfo', 'AppController@updateInfo')->name("updateInfo");
        $api->any('/appUpdateFile', 'AppController@appUpdateFile')->name("appUpdateFile");
        $api->any('/ysyUpdateInfo', 'AppController@ysyUpdateInfo')->name("ysyUpdateInfo"); //云收易系统更新信息
        $api->any('/ysyAppUpdateFile', 'AppController@ysyAppUpdateFile')->name("ysyAppUpdateFile"); //云收易系统更新
        $api->any('/setApp', 'AppController@setApp')->name("setApp");
        $api->any('/setAppPost', 'AppController@setAppPost')->name("setAppPost");

        $api->any('count_pt_data', 'SelectController@count_pt_data');
        $api->any('count_pt_data_excel', 'SelectController@count_pt_data_excel');

        $api->any('tf_bb', 'SelectController@tf_bb');

    });


    //支付宝 无需token
    $api->group(['namespace' => 'App\Api\Controllers\AlipayOpen', 'prefix' => 'alipayopen'], function ($api) {
        $api->any('callback', 'OauthController@callback');
        $api->any('qr_pay_notify', 'NotifyController@qr_pay_notify');
        $api->any('fq_pay_notify', 'NotifyController@fq_pay_notify');

        $api->any('zft_qr_pay_notify', 'NotifyController@zft_qr_pay_notify');
        $api->any('zft_fq_pay_notify', 'NotifyController@zft_fq_pay_notify');

        $api->any('gateway', 'NotifyController@gateway');

        $api->any('get_user_infos', 'UserController@get_user_infos');
        $api->any('get_invoice_title', 'InvoiceController@get_invoice_title');

        $api->post('voucherTemplateDetailQuery', 'CashlessVoucherController@voucherTemplateDetailQuery'); //查询模板详情
        $api->post('voucherTemplateListQuery', 'AlipayOpen@voucherTemplateListQuery'); //查询券模板列表
        $api->post('voucherQuery', 'AlipayOpen@voucherQuery'); //券查询
        $api->post('useRulePidQuery', 'AlipayOpen@useRulePidQuery'); //商户使用场景规则PID查询

    });

    //支付宝 需token
    $api->group(['namespace' => 'App\Api\Controllers\AlipayOpen', 'prefix' => 'alipayopen', 'middleware' => 'public.api'], function ($api) {
        $api->any('query_store_status', 'ZftController@query_store_status');
        $api->any('user_rate', 'ZftController@user_rate');
        $api->any('set_user_rate', 'ZftController@set_user_rate');

        $api->any('store_fq_rate', 'ZftController@store_fq_rate');
        $api->any('set_store_fq_rate', 'ZftController@set_store_fq_rate');

        $api->any('store_all_fq_rate', 'ZftController@store_all_fq_rate');
        $api->any('set_store_all_fq_rate', 'ZftController@set_store_all_fq_rate');

        $api->any('iot_device_bind', 'IotController@iot_device_bind'); //支付宝lot设备绑定
        $api->any('iot_device_unbind', 'IotController@iot_device_unbind'); //支付宝lot设备解绑
        $api->any('iot_device_bind_query', 'IotController@iot_device_bind_query'); //支付宝lot设备绑定查询

        $api->post('iotOperationBind', 'IotController@iotOperationBind');   //支付宝绑定
        $api->post('operationList', 'IotController@operationList');   //绑定列表
        $api->post('iotOperationBindQuery', 'IotController@iotOperationBindQuery');   //查询单条状态
        $api->post('iotOperationStoreQuery', 'IotController@iotOperationStoreQuery');   //查询蚂蚁门店接口
        $api->post('iotOperationStorelist', 'IotController@iotOperationStorelist');   //查询蚂蚁门店信息
        $api->post('iotAntStorelist', 'IotController@iotAntStorelist');   //返回蚂蚁门店绑定设备信息
        $api->post('iotBindDevice', 'IotController@iotBindDevice');   //绑定设备
        $api->post('iotBindDeviceQuery', 'IotController@iotBindDeviceQuery');   //绑定设备查询
        $api->post('iotBindDeviceVerify', 'IotController@iotBindDeviceVerify');   //绑定设备校验
        $api->post('iotBindDeviceShopQuery', 'IotController@iotBindDeviceShopQuery');   //绑定设备校验
        $api->post('iotBlueseaCreate', 'IotController@iotBlueseaCreate');   //新蓝海报名
        $api->post('iotBlueseaUp', 'IotController@iotBlueseaUp');   //新蓝海活动单修改
        $api->post('iotBlueseaList', 'IotController@iotBlueseaList');   //新蓝海活动列表
        $api->post('iotBluesQuery', 'IotController@iotBluesQuery');   //查询新蓝海状态
        $api->get('shopType', 'IotController@shopType');   //查询新蓝海店铺信息
        $api->post('zfbAccount', 'IotController@zfbAccount');   //查询新蓝海店铺信息
        $api->post('blueseaInfo', 'IotController@blueseaInfo');   //查询新蓝海报名单条信息
        $api->post('aliPayMerchantOrderSync', 'IotController@aliPayMerchantOrderSync'); //订单数据同步接口

        $api->post('createCoupon', 'CashCouponController@createCoupon');   //新建无金额模板
        $api->post('couponUp', 'CashCouponController@couponUp');   //新建无金额模板
        $api->post('sendCoupon', 'CashCouponController@sendCoupon');   //发送卡券
        $api->post('useCoupon', 'CashCouponController@useCoupon');   //使用卡券
        $api->post('couponList', 'CashCouponController@couponList');   //使用卡券
        $api->post('couponDetailQuery', 'CashCouponController@couponDetailQuery');   //使用模板详情
        $api->post('voucherQuery', 'CashCouponController@voucherQuery');   //券查询
        $api->post('voucherList', 'CashCouponController@voucherList');   //券列表
        $api->post('voucherTemList', 'CashCouponController@voucherTemList');   //券详细
        $api->post('createCashlessCoupon', 'CashCouponController@createCashlessCoupon');   //无资金营销活动
        $api->post('CashlessList', 'CashCouponController@CashlessList');   //无资金营销列表
        $api->post('CashlessUserList', 'CashCouponController@CashlessUserList');   //无资金服务商营销列表
        $api->post('checkStatus', 'CashCouponController@checkStatus');   //无资金服务商审核状态
        $api->post('CashlessDel', 'CashCouponController@CashlessDel');   //无资金优惠券商户删除券
        $api->post('CashlessInfo', 'CashCouponController@CashlessInfo');   //无资金单条信息查看
        $api->post('CashlessUserDel', 'CashCouponController@CashlessUserDel');   //无资金服务商删除券

        $api->post('agentCreate', 'AppletsController@agentCreate');   //获得batch_no
        $api->post('aliPayOpenMiniCategoryQuery', 'AppletsController@aliPayOpenMiniCategoryQuery');   //支付宝小程序类目
        $api->post('agentMiniCreate', 'AppletsController@agentMiniCreate');   //支付宝创建小程序
        $api->post('aliPayApplet', 'AppletsController@aliPayApplet');   //授权后下一步接口





    });


    //网商 无需token
    $api->group(['namespace' => 'App\Api\Controllers\MyBank', 'prefix' => 'mybank'], function ($api) {
        $api->any('notify', 'NotifyController@notify');
        $api->any('notifyPayResult', 'NotifyController@notifyPayResult');

        //静态码提交接口
        $api->any('qr_pay_submit', 'QrpayController@qr_pay_submit');
        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');


        //获取到open_id
        $api->any('weixin/oauth_openid', 'OauthController@oauth_openid');
        $api->any('weixin/oauth_callback_openid', 'OauthController@oauth_callback_openid');

        $api->any('test', 'TestController@test');
    });

    //网商 需token
    $api->group(['namespace' => 'App\Api\Controllers\MyBank', 'prefix' => 'mybank', 'middleware' => 'public.api'], function ($api) {
        $api->any('payResultQuery', 'SelectController@payResultQuery');
        $api->any('storeinfo', 'SelectController@storeinfo');
        $api->any('set_weixin_path', 'SelectController@set_weixin_path');

        $api->any('import_mercId', 'SelectController@import_mercId');
        $api->any('store_list', 'SelectController@store_list');
        $api->any('del_store', 'SelectController@del_store');


    });


    //富友 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Fuiou', 'prefix' => 'fuiou'], function ($api) {
        $api->any('pay_notify', 'NotifyController@pay_notify');
        $api->any('notifyPayResult', 'NotifyController@notifyPayResult');

        //静态码提交接口
        $api->any('qr_pay_submit', 'QrpayController@qr_pay_submit');
        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');


        //获取到open_id
        $api->any('weixin/oauth_openid', 'OauthController@oauth_openid');
        $api->any('weixin/oauth_callback_openid', 'OauthController@oauth_callback_openid');

        $api->any('test', 'TestController@test');
    });

    //富友 需token
    $api->group(['namespace' => 'App\Api\Controllers\Fuiou', 'prefix' => 'fuiou', 'middleware' => 'public.api'], function ($api) {
        $api->any('import_mercId', 'SelectController@import_mercId');
        $api->any('store_list', 'SelectController@store_list');
        $api->any('del_store', 'SelectController@del_store');
        $api->any('open_da', 'SelectController@open_da');
        $api->any('select_money', 'SelectController@select_money');
        $api->any('out_money', 'SelectController@out_money');


    });


    //联拓富 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Ltf', 'prefix' => 'ltf'], function ($api) {
        $api->any('pay_notify', 'NotifyController@pay_notify');
        //静态码提交接口
        $api->any('qr_pay_submit', 'QrpayController@qr_pay_submit');
        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');


        //获取到open_id
        $api->any('weixin/oauth_openid', 'OauthController@oauth_openid');
        $api->any('weixin/oauth_callback_openid', 'OauthController@oauth_callback_openid');


    });


    //传化 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Tfpay', 'prefix' => 'tfpay'], function ($api) {
        $api->any('store_notify', 'NotifyController@store_notify');
        $api->any('notify_url', 'NotifyController@notify_url');


        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');


        //获取到open_id
        $api->any('weixin/oauth_openid', 'OauthController@oauth_openid');
        $api->any('weixin/oauth_callback_openid', 'OauthController@oauth_callback_openid');


        //sign
        $api->any('weixin/oauth_sign_openid', 'OauthOpenidController@oauth_sign_openid');
        $api->any('weixin/oauth_sign_callback_openid', 'OauthOpenidController@oauth_sign_callback_openid');


    });

    //传化 需token
    $api->group(['namespace' => 'App\Api\Controllers\Tfpay', 'prefix' => 'tfpay', 'middleware' => 'public.api'], function ($api) {
        $api->any('select_bank', 'SelectController@select_bank');
    });


    //随行付 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Vbill', 'prefix' => 'vbill'], function ($api) {
        $api->any('pay_notify_url', 'NotifyController@pay_notify_url');
        $api->any('pay_notify_a_url', 'NotifyController@pay_notify_a_url'); //随行付a 支付回调
        $api->any('refund_notify_url', 'NotifyController@refund_notify_url');
        $api->any('refund_notify_a_url', 'NotifyController@refund_notify_a_url');
        $api->any('weChatApplyNotify', 'NotifyController@weChatApplyNotify'); //微信实名认证回调
        $api->any('weChatApplyNotifyA', 'NotifyController@weChatApplyNotifyA'); //随行付a 微信实名认证回调

        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');

        $api->any('weixin/oautha', 'OauthController@oautha');
        $api->any('weixin/oauth_callbacka', 'OauthController@oauth_callbacka');
        $api->any('weixin/pay_view_a', 'OauthController@pay_view_a');
        $api->any('weixin/member_cz_pay_viewa', 'OauthController@member_cz_pay_viewa');

        //获取到open_id
        $api->any('weixin/oauth_openid', 'OauthController@oauth_openid');
        $api->any('weixin/oauth_callback_openid', 'OauthController@oauth_callback_openid');

        //获取到open_id
        $api->any('weixin/oauth_openida', 'OauthController@oauth_openida');
        $api->any('weixin/oauth_callback_openida', 'OauthController@oauth_callback_openida');

        //sign
        $api->any('weixin/oauth_sign_openid', 'OauthOpenidController@oauth_sign_openid');
        $api->any('weixin/oauth_sign_callback_openid', 'OauthOpenidController@oauth_sign_callback_openid');

        // 微信扫码点餐-上送餐品信息接口（随行付）
        $api->any('weixin/wechatOrder', 'PayController@wechatOrder');

    });


    //会员宝 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Huiyuanbao', 'prefix' => 'huiyuanbao'], function ($api) {
        $api->any('pay_notify', 'NotifyController@pay_notify');
        $api->any('store_notify', 'NotifyController@store_notify');
        $api->any('pay_action', 'QrPayController@pay_action');
        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');

        //获取到open_id
        $api->any('weixin/oauth_openid', 'OauthController@oauth_openid');
        $api->any('weixin/oauth_callback_openid', 'OauthController@oauth_callback_openid');

    });


    //新大陆 无需token
//    $api->group(['namespace' => 'App\Api\Controllers\Newland', 'prefix' => 'newland'], function ($api) {
//        $api->any('pay_notify', 'NotifyController@pay_notify');
//        $api->any('store_query', 'SelectController@store_query');
//        $api->any('mcc_query', 'SelectController@mcc_query');
//    });

    //新大陆 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Newland', 'prefix' => 'newland'], function ($api) {
        $api->any('refund_url', 'NotifyController@refund_url');
        $api->any('pay_notify', 'NotifyController@pay_notify');
        $api->any('store_query', 'SelectController@store_query');
        $api->any('mcc_query', 'SelectController@mcc_query');
        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');
        $api->any('pay_action', 'QrPayController@pay_action');
    });

    //新大陆支付 token
    $api->group(['namespace' => 'App\Api\Controllers\Newland', 'prefix' => 'newland', 'middleware' => 'public.api'], function ($api) {
        $api->any('open_da', 'SelectController@open_da');
        $api->any('get_da_info', 'SelectController@get_da_info');
        $api->any('da_out_select', 'SelectController@da_out_select');
        $api->any('get_da', 'SelectController@get_da');
        $api->any('set_da_rate', 'SelectController@set_da_rate');
        $api->any('import_mercId', 'SelectController@import_mercId');
        $api->any('store_list', 'SelectController@store_list');
        $api->any('del_store', 'SelectController@del_store');

        $api->any('PayInOrder', 'QrPayController@PayInOrder');
    });
    //邮驿付 无需token
    $api->group(['namespace' => 'App\Api\Controllers\PostPay', 'prefix' => 'postpay'], function ($api) {
//        $api->any('pay_notify', 'NotifyController@pay_notify'); //(主扫支付)支付回调
        $api->any('pay_notify_url', 'NotifyController@pay_notify_url'); //(公众号/服务窗支付)支付回调
//        $api->any('refund_notify_url', 'NotifyController@refund_notify_url'); //
        $api->any('merchant_apply_notify_url', 'NotifyController@merchant_apply_notify_url'); //商户审核状态变更后的通知地址
        $api->any('merchant_bank_info_modify_notify_url', 'NotifyController@merchant_bank_info_modify_notify_url'); //商户结算卡变更状态的通知地址

        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');
        $api->any('getCustResult', 'PayController@getCustResult');
        $api->any('scan_pay', 'PayController@scan_pay');

        //获取到open_id
        $api->any('weixin/oauth_openid', 'OauthController@oauth_openid');
        $api->any('weixin/oauth_callback_openid', 'OauthController@oauth_callback_openid');

        //sign
        $api->any('weixin/oauth_sign_openid', 'OauthOpenidController@oauth_sign_openid');
        $api->any('weixin/oauth_sign_callback_openid', 'OauthOpenidController@oauth_sign_callback_openid');
    });
    $api->group(['namespace' => 'App\Api\Controllers\AllinPay', 'prefix' => 'allinPay'], function ($api) {
        $api->any('pay_notify_url', 'NotifyController@pay_notify_url'); //(公众号/服务窗支付)支付回调
        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');
        $api->any('scan_pay', 'PayController@scan_pay');
    });

    $api->group(['namespace' => 'App\Api\Controllers\EasePay', 'prefix' => 'easePay'], function ($api) {
        $api->any('pay_notify_url', 'NotifyController@pay_notify_url'); //(公众号/服务窗支付)支付回调
        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');
        $api->any('scan_pay', 'PayController@scan_pay');
        $api->any('getOccupation', 'StoreController@getOccupation');
        $api->any('getEasePayStore', 'StoreController@getEasePayStore');
    });

    //新大陆支付 token
//    $api->group(['namespace' => 'App\Api\Controllers\Newland', 'prefix' => 'newland', 'middleware' => 'public.api'], function ($api) {
//        $api->any('PayInOrder', 'QrPayController@PayInOrder');
//    });


    //微信 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Weixin', 'prefix' => 'weixin'], function ($api) {
        $api->any('oauth', 'OauthController@oauth');
        $api->any('callback', 'OauthController@callback');
        $api->any('qr_pay_notify', 'NotifyController@qr_pay_notify');
        $api->any('school_pay_notify', 'NotifyController@school_pay_notify');

        $api->any('oautha', 'OauthController@oautha'); //微信支付a 授权
        $api->any('callbacka', 'OauthController@callbacka'); //微信支付a 授权回调
        $api->any('qr_pay_notify_a', 'NotifyController@qr_pay_notify_a'); //微信支付a 支付回调
        $api->any('school_pay_notify_a', 'NotifyController@school_pay_notify_a'); //微信支付a

        //支付静态页面
        $api->any('qr_pay_view', 'OauthController@qr_pay_view');
        $api->any('qr_pay_view_a', 'OauthController@qr_pay_view_a'); //微信支付a
        $api->any('member_cz_pay_view', 'OauthController@member_cz_pay_view');
        $api->any('paydetails', 'OauthController@paydetails');
        $api->any('server', 'ServerController@server');

        $api->any('getWXCardBgColor', 'WxCardController@getWXCardBgColor');
        $api->any('getWXCardType', 'WxCardController@getWXCardType');
        $api->any('getWXCardCodeType', 'WxCardController@getWXCardCodeType');
        $api->any('createCardLandingpage', 'WxCardController@createCardLandingpage');
        $api->any('getCardCode', 'WxCardController@getCardCode');
        $api->any('consumeCardCode', 'WxCardController@consumeCardCode');
        $api->any('getCardList', 'WxCardController@getCardList');
        $api->any('getCardInfo', 'WxCardController@getCardInfo');
        $api->any('getCardBatch', 'WxCardController@getCardBatch');
        $api->any('getCardBizuinInfo', 'WxCardController@getCardBizuinInfo');
        $api->any('getCardCardInfo', 'WxCardController@getCardCardInfo');

        // 微信特约商户进件
        $api->any('wechat_merchant_store', 'StoreController@wechat_merchant_store');
        // 特约商户进件上传图片
        $api->any('wechat_store', 'StoreController@wechat_store');
        $api->any('getOpenid', 'MiniController@getOpenid');
    });


    //微信开放平台 无需token
    $api->group(['namespace' => 'App\Api\Controllers\WeixinOpen', 'prefix' => 'weixinopen'], function ($api) {
        //公众号授权平台
        $api->any('openoauth', 'OpenOauthController@openoauth');
        $api->any('opencallback', 'OpenOauthController@opencallback');
        $api->any('auth_notify', 'NotifyController@auth_notify');
        $api->any('oauth', 'OauthController@oauth');
        $api->any('callback', 'OauthController@callback');
        $api->any('remind_notify/{appid}', 'NotifyController@remind_notify');

    });


    //角色权限 需token
    $api->group(['namespace' => 'App\Api\Controllers\RolePermission', 'prefix' => 'role_permission', 'middleware' => 'public.api'], function ($api) {
        $api->any('role_list', 'RoleController@role_list');
        $api->any('permission_list', 'RoleController@permission_list');
        $api->any('assign_role', 'RoleController@assign_role');
        $api->any('assign_permission', 'RoleController@assign_permission');
        $api->any('add_role', 'RoleController@add_role');
        $api->any('update_role', 'RoleController@update_role');
        $api->any('add_permission', 'RoleController@add_permission');
        $api->any('update_permission', 'RoleController@update_permission');
        $api->any('user_role_list', 'RoleController@user_role_list');
        $api->any('role_permission_list', 'RoleController@role_permission_list');
        $api->any('del_role', 'RoleController@del_role');
        $api->any('del_permission', 'RoleController@del_permission');
        $api->any('user_permission_list', 'RoleController@user_permission_list');
        $api->any('user_assign_permission', 'RoleController@user_assign_permission');
        $api->get('roleMenuTreeselect', 'RoleController@roleMenuTreeselect');
        $api->get('getRouters', 'RoleController@getRouters');

    });

    //角色权限 无需token
    $api->group(['namespace' => 'App\Api\Controllers\RolePermission', 'prefix' => 'role_permission'], function ($api) {
        $api->any('treeselect', 'RoleController@treeselect');
    });


    //短信接口 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Sms', 'prefix' => 'Sms'], function ($api) {
        $api->any('send', 'SmsController@send');
    });


    //设备管理 需token
    $api->group(['namespace' => 'App\Api\Controllers\Device', 'prefix' => 'device', 'middleware' => 'public.api'], function ($api) {
        $api->any('device_type', 'DeviceController@device_type');
        $api->any('add', 'DeviceController@add');
        $api->any('lists', 'DeviceController@lists');
        $api->any('up', 'DeviceController@up');
        $api->any('del', 'DeviceController@del');
        $api->any('select', 'DeviceController@select');
        $api->any('get_device', 'DeviceController@get_device');
        $api->any('v_config', 'DeviceController@v_config');
        $api->post('aliGenieList', 'DeviceController@aliGenieList'); //天猫精灵 绑定列表
        $api->post('addAliGenie', 'DeviceController@addAliGenie'); //天猫精灵 绑定门店
        $api->post('delAliGenie', 'DeviceController@delAliGenie'); //天猫精灵 删除记录

        $api->any('print_tpl', 'PrintController@print_tpl');
        $api->any('order_tpl', 'PrintController@order_tpl');

        $api->any('device_oem_lists', 'SelectController@device_oem_lists');
        $api->any('device_oem_del', 'SelectController@device_oem_del');
        $api->any('device_oem_add', 'SelectController@device_oem_add');
        $api->any('device_oem_up', 'SelectController@device_oem_up');
        $api->any('device_oem_info', 'SelectController@device_oem_info');
        $api->any('device_oem_import', 'SelectController@device_oem_import');

        $api->any('test_a', 'DeviceController@test_a');

    });

    //设备管理 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Device', 'prefix' => 'device'], function ($api) {
        $api->any('test', 'TestController@test');

    });


    //导出管理 需token
    $api->group(['namespace' => 'App\Api\Controllers\Export', 'prefix' => 'export', 'middleware' => 'public.api'], function ($api) {
        $api->any('MerchantOrderExcelDown', 'OrderExportController@MerchantOrderExcelDown'); //商户端 流水查询导出
        $api->any('UserOrderExcelDown', 'OrderExportController@UserOrderExcelDown');
        $api->any('MerchantOrderCountExcelDown', 'OrderExportController@MerchantOrderCountExcelDown');
        $api->any('OrderMounthCountExportDown', 'OrderExportController@OrderMounthCountExportDown'); //代理月对账查询  OK
        $api->any('OrderMonthCountExportDown', 'OrderExportController@OrderMonthCountExportDown');  //门店月对账导出  ok
        $api->any('OrdermouthcountdataDown', 'OrderExportController@OrdermouthcountdataDown');  //商户管理系统 月对账导出
        $api->any('CommissionExcelDown', 'OrderExportController@CommissionExcelDown'); //分润账单导出
        $api->any('SFKJCommissionExcelDown', 'OrderExportController@SFKJCommissionExcelDown'); //首付科技-分润账单导出
        $api->any('facePaymentExcelDown', 'OrderExportController@facePaymentExcelDown'); //刷脸支付(去重)统计导出
        $api->any('facePaymentDisExcelDown', 'OrderExportController@facePaymentDisExcelDown'); //刷脸设备(去重)统计导出
        $api->any('settlerecordExcelDown', 'OrderExportController@settlerecordExcelDown'); //结算列表导出
        $api->any('SettlementCommissionExcelDown', 'SettlementCommissionExportController@SettlementCommissionExcelDown'); //未结算佣金导出
        $api->any('store_transaction_reward_export_down', 'StoreTransactionRewardExportController@StoreTransactionRewardExportDown'); //终端交易奖励导出
        $api->any('userRuleExportDown', 'UserRuleExportController@userRuleExportDown'); //代理返现详情导出
        $api->any('rechargeRecordExportDown', 'PassagewayUseExportController@rechargeRecordExportDown'); //商户交易记录导出
        $api->any('passagewayRecordExportDown', 'PassagewayUseExportController@passagewayRecordExportDown'); //商户交易记录导出
        $api->any('ZeroRateSettlementDayExcelDown', 'ZeroRateSettlementExportController@ZeroRateSettlementDayExcelDown'); //零费率日结算记录导出
        $api->any('ZeroRateSettlementMonthExcelDown', 'ZeroRateSettlementExportController@ZeroRateSettlementMonthExcelDown'); //零费率月结算记录导出
        $api->any('putForwardExcelDown', 'PutForwardExportController@putForwardExcelDown'); //提现记录导出
        $api->any('shoppingOrderExcelDown', 'ShoppingOrderExportController@shoppingOrderExcelDown'); //商城订单导出
	});


    //赏金管理 需token
    $api->group(['namespace' => 'App\Api\Controllers\Wallet', 'prefix' => 'wallet', 'middleware' => 'public.api'], function ($api) {
        $api->any('source_type', 'SelectController@source_type');
        $api->any('source_query', 'SelectController@source_query');
        $api->any('source_query_info', 'SelectController@source_query_info');
        $api->any('records_query_info', 'SelectController@records_query_info');

        $api->post('sf_commission_lists', 'SelectController@sf_commission_lists');  //首付科技-分润账单
        $api->post('settlementDayQuery', 'SelectController@settlementDayQuery'); //日结算列表
        $api->post('dayInfo', 'SelectController@dayInfo'); //日结算单条信息
        $api->post('upSettlement', 'SelectController@upSettlement'); //更改税率
        $api->post('changeDaily', 'SelectController@changeDaily'); //更改日常开关


        $api->any('select_account', 'SelectController@select_account');
        $api->any('account', 'SelectController@account');
        $api->any('add_account', 'SelectController@add_account');
        $api->any('out_wallet', 'SelectController@out_wallet');
        $api->any('out_wallet_list', 'SelectController@out_wallet_list');
        $api->any('getUserOutWallerList', 'SelectController@getUserOutWallerList');
        $api->any('settlement', 'SelectController@settlement');
        $api->any('addSettlement', 'SelectController@addSettlement');//日结算新增
        $api->any('edit_wallet_status', 'SelectController@edit_wallet_status');//更新提现状态

        $api->any('get_qxgj_user', 'QxgjController@qxgj_user');
        $api->any('add_qxgj_user', 'QxgjController@add_qxgj_user');
        $api->any('addWalletStaff', 'QxgjController@addWalletStaff');
        $api->any('walletSignInfo', 'QxgjController@walletSignInfo');
        $api->any('revenueOrder', 'QxgjController@revenueOrder');
        $api->any('getBalance', 'QxgjController@getBalance');


        $api->any('settlement_configs', 'setController@settlement_configs');
        $api->any('settlement_lists', 'SelectController@settlement_lists');
        $api->any('settlement_list_infos', 'SelectController@settlement_list_infos');
        $api->any('settlement_list_del', 'SelectController@settlement_list_del');
        $api->any('settlement_list_true', 'SelectController@settlement_list_true');
        $api->any('tf_settlemment', 'SelectController@tf_settlemment');
        $api->any('tf_settlemment_m', 'SelectController@tf_settlemment_m');
        $api->any('tf_user_settlemment', 'SelectController@tf_user_settlemment');
        $api->any('tf_settlemment_all', 'SelectController@tf_settlemment_all');
        $api->any('get_user_money', 'SelectController@get_user_money'); //未提分润金额统计

    });


    //广告管理 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Ad', 'prefix' => 'ad'], function ($api) {
        $api->any('ad_lists', 'AdController@ad_lists');
        $api->any('ad_create', 'AdController@ad_create');
        $api->any('ad_up', 'AdController@ad_up');
        $api->any('ad_del', 'AdController@ad_del');
        $api->any('ad_p_id', 'AdController@ad_p_id');
        $api->any('ad_info', 'AdController@ad_info');

        $api->any('ad_lists_new', 'AdController@ad_lists_new');
    });


    //活动、公告栏 需token
    $api->group(['namespace' => 'App\Api\Controllers\Huodong', 'prefix' => 'huodong', 'middleware' => 'public.api'], function ($api) {
        $api->any('get_list', 'AlipayHongbao@get_list');
        $api->any('add', 'AlipayHongbao@add');
        $api->any('del', 'AlipayHongbao@del');


        //拉新
        $api->any('hd_list', 'SelectController@hd_list');
        $api->any('old_hd_list', 'SelectController@old_hd_list');
        $api->any('get_info', 'SelectController@get_info');


        $api->any('jdbt', 'JdbtController@jdbt');


    });


    //微收银 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Qwx', 'prefix' => 'qwx'], function ($api) {
        $api->any('scan_pay', 'IndexController@scan_pay');
        $api->any('order_query', 'IndexController@order_query');
        $api->any('refund', 'IndexController@refund');
        $api->any('refund_query', 'IndexController@refund_query');
    });

    //微收银 需token
    $api->group(['namespace' => 'App\Api\Controllers\Qwx', 'prefix' => 'qwx', 'middleware' => 'public.api'], function ($api) {
        $api->any('add_store', 'StoreController@add_store');
        $api->any('add_code', 'StoreController@add_code');
        $api->any('del_code', 'StoreController@del_code');
        $api->any('code_list', 'StoreController@code_list');

        $api->post('get_order_by_authcode', 'SelectController@get_order_by_authcode');
    });


    //意锐 无需token
    $api->group(['namespace' => 'App\Api\Controllers\In', 'prefix' => 'in'], function ($api) {
        $api->any('scan_pay', 'IndexController@scan_pay');
        $api->any('order_query', 'IndexController@order_query');
        $api->any('refund', 'IndexController@refund');
        $api->any('cancel', 'IndexController@cancel');
        $api->any('consume', 'IndexController@consume');
        $api->any('device_order', 'IndexController@device_order');
        $api->any('order_query_count', 'IndexController@order_query_count');
        $api->any('query_store', 'IndexController@query_store');
        $api->any('login', 'IndexController@login');
        $api->any('login_out', 'IndexController@login_out');

    });


    //收钱啦 需token
    $api->group(['namespace' => 'App\Api\Controllers\WinCode', 'prefix' => 'wincode', 'middleware' => 'public.api'], function ($api) {
        $api->any('add_store', 'StoreController@add_store');
        $api->any('add_code', 'StoreController@add_code');
        $api->any('del_code', 'StoreController@del_code');
        $api->any('code_list', 'StoreController@code_list');

        $api->post('get_order_by_authcode', 'SelectController@get_order_by_authcode');
    });

    //收钱啦 无需token
    $api->group(['namespace' => 'App\Api\Controllers\WinCode', 'prefix' => 'wincode'], function ($api) {
        $api->post('interaction', 'IndexController@interaction');
        $api->post('scan_pay', 'IndexController@scan_pay');
    });


    //京东支付 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Jd', 'prefix' => 'jd'], function ($api) {
        $api->any('notify_url', 'NotifyController@notify_url');
        $api->any('refund_url', 'NotifyController@refund_url');
        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');

    });


    //系统报错 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Errors', 'prefix' => 'errors'], function ($api) {
        $api->any('self_errors', 'SelfErrorsController@self_errors');
    });


    //设备接口 无需token
    $api->group(['namespace' => 'App\Api\Controllers\DevicePay', 'prefix' => 'devicepay'], function ($api) {
        $api->any('scan_pay', 'IndexController@scan_pay');
        $api->any('qr_pay', 'IndexController@qr_pay');
        $api->any('qr_auth_pay', 'IndexController@qr_auth_pay');
        $api->any('store_pay_ways', 'IndexController@store_pay_ways');
        $api->any('order', 'IndexController@order');
        $api->any('order_list', 'IndexController@order_list');
        $api->any('mini_pay', 'IndexController@mini_pay');
        $api->any('order_query', 'IndexController@order_query');
        $api->any('refund', 'IndexController@refund');
        $api->any('refund_query', 'IndexController@refund_query');

        $api->any('get_mq_info', 'SelectController@get_mq_info');
        $api->any('GetRequestApiUrl', 'SelectController@GetRequestApiUrl');
        $api->any('update', 'SelectController@update');


        $api->any('face_device_start', 'DeviceFaceController@face_device_start');
        $api->any('face_pay_start', 'DeviceFaceController@face_pay_start');
        $api->any('wxfacepay_initialize', 'DeviceFaceController@wxfacepay_initialize');
        $api->any('all_pay', 'DeviceFaceController@all_pay');
        $api->any('all_pay_query', 'DeviceFaceController@all_pay_query');
        $api->any('order_pay_cancel', 'DeviceFaceController@order_pay_cancel');

        $api->any('fq_scan_pay', 'FqIndexController@fq_scan_pay');
        $api->any('fq_order_query', 'FqIndexController@fq_order_query');
        $api->any('fq_query_rate', 'FqIndexController@fq_query_rate');
        $api->any('fq_refund', 'FqIndexController@fq_refund');
        $api->any('scan_pay_box', 'IndexController@scan_pay_box');
    });

    //设备接口 -分期
//    $api->group(['namespace' => 'App\Api\Controllers\DevicePay', 'prefix' => 'devicepay'], function ($api) {
//        $api->any('fq_scan_pay', 'FqIndexController@fq_scan_pay');
//        $api->any('fq_order_query', 'FqIndexController@fq_order_query');
//        $api->any('fq_query_rate', 'FqIndexController@fq_query_rate');
//        $api->any('fq_refund', 'FqIndexController@fq_refund');
//    });


    //代付 需token
    $api->group(['namespace' => 'App\Api\Controllers\DfPay', 'prefix' => 'dfpay', 'middleware' => 'public.api'], function ($api) {
        $api->any('info_import', 'IndexController@info_import');
        $api->any('order_list', 'IndexController@order_list');
    });

    //代付 无需token
    $api->group(['namespace' => 'App\Api\Controllers\DfPay', 'prefix' => 'dfpay'], function ($api) {
        $api->any('pay_notify', 'NotifyController@pay_notify');
    });


    //哆啦宝 无需token
    $api->group(['namespace' => 'App\Api\Controllers\DuoLaBao', 'prefix' => 'dlb'], function ($api) {
        $api->any('test', 'ManageController@test');
        $api->any('pay_notify', 'ManageController@pay_notify');
        $api->any('audit_notify', 'ManageController@audit_notify'); //审核结果回调
        $api->any('store_notify', 'ManageController@store_notify');
        $api->any('weixin/oauth', 'WxOauthController@oauth');
        $api->any('weixin/callback', 'WxOauthController@callback');
        $api->any('weixinopen/oauth', 'WxOpenOauthController@oauth');
        $api->any('weixinopen/callback', 'WxOpenOauthController@callback');
        $api->any('weixin/qr_pay_view', 'WxOauthController@qr_pay_view');
        $api->any('weixin/member_cz_pay_view', 'WxOpenOauthController@member_cz_pay_view');
    });


    //汇付 无需token
    $api->group(['namespace' => 'App\Api\Controllers\HuiPay', 'prefix' => 'huipay'], function ($api) {
        $api->any('pay_notify_url', 'NotifyController@pay_notify_url'); //交易回调
        $api->any('return_pay_notify_url', 'NotifyController@return_pay_notify_url'); //退款回调

        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');

        //获取到open_id
        $api->any('weixin/oauth_openid', 'OauthController@oauth_openid');
        $api->any('weixin/oauth_callback_openid', 'OauthController@oauth_callback_openid');

        //sign
        $api->any('weixin/oauth_sign_openid', 'OauthOpenidController@oauth_sign_openid');
        $api->any('weixin/oauth_sign_callback_openid', 'OauthOpenidController@oauth_sign_callback_openid');

    });


    //易生 无需token
    $api->group(['namespace' => 'App\Api\Controllers\EasyPay', 'prefix' => 'easypay'], function ($api) {
//        $api->any('pay_notify', 'NotifyController@pay_notify'); //(主扫支付)支付回调
        $api->any('pay_notify_url', 'NotifyController@pay_notify_url'); //(公众号/服务窗支付)支付回调
        $api->any('pay_notify_url_u', 'NotifyController@pay_notify_url_u'); //云闪付前台支付通知
//        $api->any('refund_notify_url', 'NotifyController@refund_notify_url'); //
        $api->any('merchant_apply_notify_url', 'NotifyController@merchant_apply_notify_url'); //商户审核状态变更后的通知地址
        $api->any('merchant_bank_info_modify_notify_url', 'NotifyController@merchant_bank_info_modify_notify_url'); //商户结算卡变更状态的通知地址

        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');

        //获取到open_id
        $api->any('weixin/oauth_openid', 'OauthController@oauth_openid');
        $api->any('weixin/oauth_callback_openid', 'OauthController@oauth_callback_openid');

        //sign
        $api->any('weixin/oauth_sign_openid', 'OauthOpenidController@oauth_sign_openid');
        $api->any('weixin/oauth_sign_callback_openid', 'OauthOpenidController@oauth_sign_callback_openid');

        $api->any('test', 'TestController@test');
        $api->any('testEasyPayMPS', 'TestController@testEasyPayMPS'); //电子协议

        $api->any('getSignKey', 'OauthController@getSignKey');
    });


    //海科融通 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Hkrt', 'prefix' => 'hkrt'], function ($api) {
        $api->any('pay_notify_url', 'NotifyController@pay_notify_url');
        $api->any('refund_notify_url', 'NotifyController@refund_notify_url');
        $api->any('merchant_apply_notify_url', 'NotifyController@merchant_apply_notify_url'); //商户审核状态变更后的通知地址
        $api->any('merchant_bank_info_modify_notify_url', 'NotifyController@merchant_bank_info_modify_notify_url'); //商户结算卡变更状态的通知地址

        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');

        //获取到open_id
        $api->any('weixin/oauth_openid', 'OauthController@oauth_openid');
        $api->any('weixin/oauth_callback_openid', 'OauthController@oauth_callback_openid');

        //sign
        $api->any('weixin/oauth_sign_openid', 'OauthOpenidController@oauth_sign_openid');
        $api->any('weixin/oauth_sign_callback_openid', 'OauthOpenidController@oauth_sign_callback_openid');

        //test
        $api->any('test', 'TestController@test');
    });


    //葫芦天下 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Hltx', 'prefix' => 'hltx'], function ($api) {
        $api->any('test', 'TestController@test');
        $api->any('alipay_view', 'TestController@alipay_view');
        $api->any('wx_callback', 'TestController@wx_callback');
        $api->any('wx_oauth', 'TestController@wx_oauth');
        $api->any('wxpay_view', 'TestController@wxpay_view');

        $api->any('merchant_notify', 'ManageController@merchant_notify');
        $api->any('pay_notify', 'ManageController@pay_notify');
        $api->any('weixin/oauth', 'WxOauthController@oauth');
        $api->any('weixin/callback', 'WxOauthController@callback');
        $api->any('weixinopen/oauth', 'WxOpenOauthController@oauth');
        $api->any('weixinopen/callback', 'WxOpenOauthController@callback');
        $api->any('weixin/qr_pay_view', 'WxOauthController@qr_pay_view');

    });

    //葫芦天下 需token
    $api->group(['namespace' => 'App\Api\Controllers\Hltx', 'prefix' => 'hltx', 'middleware' => 'public.api'], function ($api) {
        $api->any('edit_store_rate', 'ManageController@edit_store_rate');
        $api->any('weixin_config_set', 'ManageController@weixin_config_set');
    });


    //联动优势 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Linkage', 'prefix' => 'linkage'], function ($api) {
        $api->any('pay_notify_url', 'NotifyController@pay_notify_url'); //支付回调
        $api->any('merchant_apply_notify_url', 'NotifyController@merchant_apply_notify_url'); //商户入网审核结果通知
//        $api->any('refund_notify_url', 'NotifyController@refund_notify_url'); //退款回调

        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');

        //获取到open_id
        $api->any('weixin/oauth_openid', 'OauthController@oauth_openid');
        $api->any('weixin/oauth_callback_openid', 'OauthController@oauth_callback_openid');

        //sign
        $api->any('weixin/oauth_sign_openid', 'OauthOpenidController@oauth_sign_openid');
        $api->any('weixin/oauth_sign_callback_openid', 'OauthOpenidController@oauth_sign_callback_openid');

        $api->any('test', 'TestController@test');
    });


    //美团配送 无需token
    $api->group(['namespace' => 'App\Api\Controllers\MeiTuanPeiSong', 'prefix' => 'mtps'], function ($api) {
        $api->post('callBackStoreStatus', 'CallBackController@callBackStoreStatus'); //门店状态回调
        $api->post('callBackOrderStatus', 'CallBackController@callBackOrderStatus'); //订单状态回调
        $api->post('callBackOrderErrStatus', 'CallBackController@callBackOrderErrStatus'); //订单异常回调

        $api->get('getMeiTuanCityList', 'SelectController@getMeiTuanCityList'); //城市id列表
        $api->get('getMeiTuanCategoryList', 'SelectController@getMeiTuanCategoryList'); //品类列表

    });


    //达达配送回调 无需token
    $api->group(['namespace' => 'App\Api\Controllers\DaDa', 'prefix' => 'dada'], function ($api) {
        $api->post('callBackOrder', 'CallBackController@callBackOrder'); //订单回调
        $api->post('callBackReturnStatus', 'CallBackController@callBackReturnStatus'); //骑士取消订单回调

        $api->post('cityCodeList', 'SelectController@cityCodeList'); //城市信息列表
        $api->post('orderCancelReasons', 'SelectController@orderCancelReasons'); //取消原因列表

    });

    //达达配送 需token
    $api->group(['namespace' => 'App\Api\Controllers\DaDa', 'prefix' => 'dada',  'middleware' => 'public.api'], function ($api) {
        $api->post('merchantAdd', 'AddController@merchantAdd'); //注册商户
        $api->post('orderAddOrder', 'AddController@orderAddOrder'); //新增配送单
        $api->post('reAddOrder', 'AddController@reAddOrder'); //重新发布订单

        $api->post('orderFormalCancel', 'CancelController@orderFormalCancel'); //取消订单

        $api->post('orderStatusQuery', 'SelectController@orderStatusQuery'); //达达订单详情查询

        $api->post('dada_order_info', 'SelectController@dada_order_info'); //系统订单详情
    });


    //威富通支付 无需token
    $api->group(['namespace' => 'App\Api\Controllers\WftPay', 'prefix' => 'wftpay'], function ($api) {
        $api->any('pay_notify_url', 'NotifyController@pay_notify_url'); //支付回调
//        $api->any('merchant_apply_notify_url', 'NotifyController@merchant_apply_notify_url'); //商户入网审核结果通知
//        $api->any('refund_notify_url', 'NotifyController@refund_notify_url'); //退款回调

        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');

        //获取到open_id
        $api->any('weixin/oauth_openid', 'OauthController@oauth_openid');
        $api->any('weixin/oauth_callback_openid', 'OauthController@oauth_callback_openid');

        //sign
        $api->any('weixin/oauth_sign_openid', 'OauthOpenidController@oauth_sign_openid');
        $api->any('weixin/oauth_sign_callback_openid', 'OauthOpenidController@oauth_sign_callback_openid');

        $api->any('test', 'PayController@test');
    });


    //汇旺财支付 无需token
    $api->group(['namespace' => 'App\Api\Controllers\HwcPay', 'prefix' => 'hwcpay'], function ($api) {
        $api->any('pay_notify_url', 'NotifyController@pay_notify_url'); //支付回调
//        $api->any('merchant_apply_notify_url', 'NotifyController@merchant_apply_notify_url'); //商户入网审核结果通知
//        $api->any('refund_notify_url', 'NotifyController@refund_notify_url'); //退款回调

        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');

        //获取到open_id
        $api->any('weixin/oauth_openid', 'OauthController@oauth_openid');
        $api->any('weixin/oauth_callback_openid', 'OauthController@oauth_callback_openid');

        //sign
        $api->any('weixin/oauth_sign_openid', 'OauthOpenidController@oauth_sign_openid');
        $api->any('weixin/oauth_sign_callback_openid', 'OauthOpenidController@oauth_sign_callback_openid');

    });


    //银盛 无需token
    $api->group(['namespace' => 'App\Api\Controllers\YinSheng', 'prefix' => 'ysepay'], function ($api) {
        $api->any('pay_notify_url', 'NotifyController@pay_notify_url'); //支付回调
//        $api->any('refund_notify_url', 'NotifyController@refund_notify_url'); //退款回调

        $api->any('merchant_notify_url', 'NotifyController@merchant_notify_url'); //商户入网审核结果通知

        $api->any('ysepayMerchantAccess', 'YsepayStoreController@ysepayMerchantAccess'); //商户入网补充资料
        $api->any('queryMccList', 'YsepayStoreController@queryMccList'); //mcc查询

        $api->any('queryCustApply', 'YsepayStoreController@queryCustApply'); //查询商户状态

        $api->any('addCustInfoApply', 'YsepayStoreController@addCustInfoApply'); //入网申请
        $api->any('changeMercBaseInfo', 'YsepayStoreController@changeMercBaseInfo'); //商户基本信息变更申请
        $api->any('auditCustInfoApply', 'YsepayStoreController@auditCustInfoApply'); //商户入网审核
        $api->any('changeBaseAudit', 'YsepayStoreController@changeBaseAudit'); //基本信息变更审核
        $api->any('changeMercStlAccInfo', 'YsepayStoreController@changeMercStlAccInfo'); //结算信息变更申请
        $api->any('changeRate', 'YsepayStoreController@changeRate'); //费率信息变更申请
        $api->any('onlineOpen', 'YsepayStoreController@onlineOpen'); //开通/关闭线上D0权限


        $api->any('smscSign', 'YsepayStoreController@smscSign'); //合同签约
        $api->any('sendSmsOrEmailMsg', 'YsepayStoreController@sendSmsOrEmailMsg'); //重发签约短信或邮件
        $api->any('queryContract', 'YsepayStoreController@queryContract'); //电子合同查询签约状态
        $api->any('downloadContract', 'YsepayStoreController@downloadContract'); //电子合同下载


        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');

        //获取到open_id
        $api->any('weixin/oauth_openid', 'OauthController@oauth_openid');
        $api->any('weixin/oauth_callback_openid', 'OauthController@oauth_callback_openid');

        //sign
        $api->any('weixin/oauth_sign_openid', 'OauthOpenidController@oauth_sign_openid');
        $api->any('weixin/oauth_sign_callback_openid', 'OauthOpenidController@oauth_sign_callback_openid');

        $api->any('test', 'TestController@test');
    });

    //拉卡拉 无需token
    $api->group(['namespace' => 'App\Api\Controllers\LklPay', 'prefix' => 'lklpay'], function ($api) {
        $api->any('pay_notify_url', 'NotifyController@pay_notify_url'); //支付回调
        $api->any('merchant_examine_push', 'NotifyController@merchant_examine_push'); //商户入网审核结果通知
        $api->any('update_merchant_push', 'NotifyController@update_merchant_push'); //商户改件审核结果通知


        $api->any('queryMccList', 'StoreController@queryMccList'); //mcc查询
        $api->any('getLklPayMccApp', 'StoreController@getLklPayMccApp'); //mcc查询 app
        $api->any('queryAreaList', 'StoreController@queryAreaList'); //地区查询
        $api->any('queryBankAreaList', 'StoreController@queryBankAreaList'); //银行地区查询
        $api->any('queryBankList', 'StoreController@queryBankList'); //银行查询

        $api->any('lklPayMerchantReplenish', 'StoreController@lklPayMerchantReplenish'); //商户入网补充资料
        $api->any('updateMerchant', 'StoreController@updateMerchant'); //商户基本信息变更申请
        $api->any('updateMerchantSettle', 'StoreController@updateMerchantSettle'); //结算信息变更申请
        $api->any('updateMerchantFee', 'StoreController@updateMerchantFee'); //费率信息变更申请

        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');

        //获取到open_id
        $api->any('weixin/oauth_openid', 'OauthController@oauth_openid');
        $api->any('weixin/oauth_callback_openid', 'OauthController@oauth_callback_openid');

        //sign
        $api->any('weixin/oauth_sign_openid', 'OauthOpenidController@oauth_sign_openid');
        $api->any('weixin/oauth_sign_callback_openid', 'OauthOpenidController@oauth_sign_callback_openid');
        $api->any('getSubMerchant', 'StoreController@getSubMerchant'); //查询子商户号

    });


    //东莞银行 无需token
    $api->group(['namespace' => 'App\Api\Controllers\DongGuan', 'prefix' => 'dongguan'], function ($api) {
        $api->any('pay_notify_url', 'NotifyController@pay_notify_url'); //支付回调
        $api->any('merchant_apply_notify_url', 'NotifyController@merchant_apply_notify_url'); //商户入网审核结果通知
//        $api->any('refund_notify_url', 'NotifyController@refund_notify_url'); //退款回调

        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');

        //获取到open_id
        $api->any('weixin/oauth_openid', 'OauthController@oauth_openid');
        $api->any('weixin/oauth_callback_openid', 'OauthController@oauth_callback_openid');

        //sign
        $api->any('weixin/oauth_sign_openid', 'OauthOpenidController@oauth_sign_openid');
        $api->any('weixin/oauth_sign_callback_openid', 'OauthOpenidController@oauth_sign_callback_openid');

        $api->any('test', 'TestController@test');
    });


    //apk 无需token
    $api->group(['namespace' => 'App\Api\Controllers\Apk', 'prefix' => 'apk'], function ($api) {
        $api->post('apkLists', 'ApkController@apkLists');
        $api->post('apkCreate', 'ApkController@apkCreate');
        $api->post('apkUpload', 'ApkController@apkUpload');
        $api->post('apkUp', 'ApkController@apkUp');
        $api->post('apkInfo', 'ApkController@apkInfo');
        $api->post('apkDel', 'ApkController@apkDel'); //删除apk
    });


    //广告链接管理 无需token
    $api->group(['namespace' => 'App\Api\Controllers\AdLinks', 'prefix' => 'adlinks'], function ($api) {
        $api->post('adlinkslists', 'AdLinksController@adlinkslists'); //广告链接列表
        $api->post('adlinksCreate', 'AdLinksController@adlinksCreate'); //新增广告链接
        $api->post('adlinksDel', 'AdLinksController@adlinksDel'); //删除广告链接
        $api->post('adlinksUp', 'AdLinksController@adlinksUp');
        $api->post('adlinksInfo', 'AdLinksController@adlinksInfo');
        $api->post('adlinksTotal', 'AdLinksController@adlinksTotal'); //查看门店生成总链接

    });


    //钱方 无需token
    $api->group(['namespace' => 'App\Api\Controllers\QfPay', 'prefix' => 'qfpay'], function ($api) {
        $api->any('pay_notify_url', 'NotifyController@pay_notify_url'); //支付回调
//        $api->any('refund_notify_url', 'NotifyController@refund_notify_url'); //退款回调

        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');

        //获取到open_id
        $api->any('weixin/oauth_openid', 'OauthController@oauth_openid');
        $api->any('weixin/oauth_callback_openid', 'OauthController@oauth_callback_openid');

        //sign
        $api->any('weixin/oauth_sign_openid', 'OauthOpenidController@oauth_sign_openid');
        $api->any('weixin/oauth_sign_callback_openid', 'OauthOpenidController@oauth_sign_callback_openid');

        $api->any('test', 'TestController@test');
    });


    //阿尔丰盒子
    $api->group(['namespace' => 'App\Api\Controllers\Merchant', 'prefix' => 'merchant'], function ($api) {
        $api->any('c01Refund', 'OrderController@c01Refund');
    });

    $api->group(['namespace' => 'App\Api\Controllers\AliRuyi', 'prefix' => 'aliruyi'], function ($api) {

        $api->post('device_bind', 'RuyiPayController@deviceBind'); //如意设备绑定
        $api->post('device_bind_query', 'RuyiPayController@bindQuery'); //如意设备绑定查询
        $api->post('device_unbind', 'RuyiPayController@deviceUnbind'); //如意设备解绑

        $api->post('cashier/batchquery', 'RuyiPayController@batchQueryCashier'); //查询收银员列表
        $api->post('cashier/query', 'RuyiPayController@query'); //查询收银员状态
        $api->post('cashier/sign', 'RuyiPayController@sign'); //收银员签到
        $api->post('cashier/unsign', 'RuyiPayController@unsign'); //收银员签退

        $api->post('billdetail/query', 'RuyiPayController@billDetail');      //交易流水信息查询
        $api->post('billstatistics/query', 'RuyiPayController@billStatistics');  //交易统计查询

        $api->post('order/pay', 'RuyiPayController@orderPay');       //isv标准支付接口
        $api->post('order/query', 'RuyiPayController@orderQuery');   //isv标准查询接口
        $api->post('bill/query', 'RuyiPayController@billQuery');   //isv订单状态接口



    });

    //易生数科 无需token
    $api->group(['namespace' => 'App\Api\Controllers\EasySkPay', 'prefix' => 'easyskpay'], function ($api) {
//        $api->any('pay_notify', 'NotifyController@pay_notify'); //(主扫支付)支付回调
        $api->any('pay_notify_url', 'NotifyController@pay_notify_url'); //(公众号/服务窗支付)支付回调
        $api->any('pay_notify_url_u', 'NotifyController@pay_notify_url_u'); //银联前台通知地址
//        $api->any('refund_notify_url', 'NotifyController@refund_notify_url'); //
        $api->any('merchant_apply_notify_url', 'NotifyController@merchant_apply_notify_url'); //商户审核状态变更后的通知地址
        $api->any('merchant_bank_info_modify_notify_url', 'NotifyController@merchant_bank_info_modify_notify_url'); //商户结算卡变更状态的通知地址

        $api->any('weixin/oauth', 'OauthController@oauth');
        $api->any('weixin/oauth_callback', 'OauthController@oauth_callback');
        $api->any('weixin/pay_view', 'OauthController@pay_view');
        $api->any('weixin/member_cz_pay_view', 'OauthController@member_cz_pay_view');

        //获取到open_id
        $api->any('weixin/oauth_openid', 'OauthController@oauth_openid');
        $api->any('weixin/oauth_callback_openid', 'OauthController@oauth_callback_openid');

        //sign
        $api->any('weixin/oauth_sign_openid', 'OauthOpenidController@oauth_sign_openid');
        $api->any('weixin/oauth_sign_callback_openid', 'OauthOpenidController@oauth_sign_callback_openid');

        $api->any('test', 'TestController@test');
        $api->any('getSignKey', 'OauthController@getSignKey');
    });

});
