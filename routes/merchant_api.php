<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    //无需token
    $api->group(['namespace' => 'App\Api\Controllers\Merchant', 'prefix' => 'merchant'], function ($api) {
        $api->any('login', 'LoginController@login');
        $api->any('weixinapp_login', 'LoginController@weixinapp_login');
        $api->any('app_login', 'LoginController@app_login');
        $api->any('register', 'LoginController@register');
        $api->any('edit_password', 'LoginController@edit_password');
        $api->any('checkLoginInfo', 'LoginController@checkLoginInfo'); //检查是否手机号登录

        //静态二维码请求
        $api->any('qr_auth_pay', 'PayBaseController@qr_auth_pay');

        $api->any('fq_qr_auth_pay', 'PayBaseController@fq_qr_auth_pay');

        $api->any('school_pay', 'PayBaseController@school_pay');

        $api->any('fq/hb_query_rate', 'AlipayFqOrderController@hb_query_rate');


        $api->post('updateGoodsCategory', 'GoodsController@updateGoodsCategory');
        $api->post('delGoodsCategory', 'GoodsController@delGoodsCategory');
        $api->get('goodsCategoryList', 'GoodsController@goodsCategoryList');

        $api->post('updateGoodsAttribute', 'GoodsController@updateGoodsAttribute');
        $api->post('delGoodsAttribute', 'GoodsController@delGoodsAttribute');
        $api->get('goodsAttributeList', 'GoodsController@goodsAttributeList');

        $api->post('updateGoodsStandard', 'GoodsController@updateGoodsStandard');
        $api->post('delGoodsStandard', 'GoodsController@delGoodsStandard');
        $api->get('goodsStandardList', 'GoodsController@goodsStandardList');

        $api->post('updateGoodsTag', 'GoodsController@updateGoodsTag');
        $api->post('delGoodsTag', 'GoodsController@delGoodsTag');
        $api->get('goodsTagList', 'GoodsController@goodsTagList');

        $api->any('qr_pay', 'PayBaseController@qr_pay'); //生成二维码支付
        $api->any('store_pay_qr', 'StoreController@store_pay_qr'); //商户收款码

        $api->get('newland_pay_test', 'PayBaseController@newland_pay_test'); //生成二维码支付
        $api->post('pay_jsapi', 'PayBaseController@pay_jsapi');
        $api->post('pay_scanPay', 'PayBaseController@pay_scanPay');
        $api->post('refund_org', 'OrderController@refund_org');//客户退款接口
        $api->post('order_query_org', 'OrderController@order_query_org');//客户订单查询
        $api->any('win_pay_notify', 'MerchantRechargeController@win_pay_notify');//商户充值-支付回调
        $api->any('easy_pay_notify', 'MerchantRechargeController@easy_pay_notify');//商户充值-支付回调

        $api->any('orderDetails', 'OrderController@orderDetails');
    });


    //无需token
    $api->group(['namespace' => 'App\Api\Controllers\AlipayOpen', 'prefix' => 'alipayopen'], function ($api) {
        $api->any('callback', 'OauthController@callback');
    });


    //需要token
    $api->group(['namespace' => 'App\Api\Controllers\Merchant', 'prefix' => 'merchant', 'middleware' => 'merchant.api'], function ($api) {
        $api->any('login_list', 'LoginController@login_list');
        $api->any('login_del', 'LoginController@login_del');
        $api->any('wx_get_openid', 'LoginController@wx_get_openid');
        $api->any('get_mq_info', 'LoginController@get_mq_info');
        $api->any('select_bb_status', 'LoginController@select_bb_status');

        $api->any('index', 'IndexController@index');

        $api->any('get_data', 'IndexController@get_data');

        $api->any('my', 'MyController@my');
        $api->any('add_email', 'MyController@add_email');
        $api->any('add_weixin', 'MyController@add_weixin');

        $api->any('scan', 'IndexController@scan');
        $api->any('discount', 'IndexController@discount');
        $api->any('notice_news', 'InfoController@notice_news');
        $api->any('info_list', 'InfoController@info_list');
        $api->any('fuwu', 'InfoController@fuwu');
        $api->any('order_count_print', 'InfoController@order_count_print');

        $api->post('updateGoods', 'GoodsController@updateGoods');
        $api->post('updateGoodsStatus', 'GoodsController@updateGoodsStatus');
        $api->post('delGoods', 'GoodsController@delGoods');
        $api->get('goodsList', 'GoodsController@goodsList'); //小程序商品列表
        $api->post('batchUpdateGoods', 'GoodsController@batchUpdateGoods');
        $api->post('goodsDetails', 'GoodsController@goodsDetails');
        $api->post('goodsListNew', 'GoodsController@goodsListNew'); //PC端商品列表

        // 微信商家券列表
        $api->any('getWechatMerchantCouponList', 'WxMerchantCouponController@getWechatMerchantCouponList');
        // 微信商家券核销明细
        $api->any('wxMerchantCouponStocksDetail', 'WxMerchantCouponController@wxMerchantCouponStocksDetail');
        // 微信商家券核销列表页
        $api->any('getWechatMerchantCouponUsedList', 'WxMerchantCouponController@getWechatMerchantCouponUsedList');
        // 微信商家券批次使用、核销数量
        $api->any('wxMerchantCouponStocksCount', 'WxMerchantCouponController@wxMerchantCouponStocksCount');
        // 删除商家券
        $api->any('delUserStoreCoupon', 'WxMerchantCouponController@delUserStoreCoupon');
        
        $api->any('bind_device', 'StoreController@bind_device'); //绑定设备
        $api->any('device_list', 'StoreController@device_list');
        $api->any('unbind_device', 'StoreController@unbind_device');
        $api->any('device_type', 'StoreController@device_type');

    });

    //需要token 门店相关
    $api->group(['namespace' => 'App\Api\Controllers\Merchant', 'prefix' => 'merchant', 'middleware' => 'merchant.api'], function ($api) {
        $api->any('add_store', 'StoreController@add_store');
        $api->any('store', 'StoreController@store');
        $api->any('store_type', 'StoreController@store_type');
        $api->any('store_category', 'StoreController@store_category');
        $api->any('alipay_auth', 'StoreController@alipay_auth');
        $api->any('store_lists', 'StoreController@store_lists');
        $api->any('merchant_lists', 'StoreController@merchant_lists');
        $api->any('sub_merchant_lists', 'StoreController@sub_merchant_lists');
        $api->any('add_merchant', 'StoreController@add_merchant');
        $api->any('bind_merchant', 'StoreController@bind_merchant');
        $api->any('add_merchant_qr', 'StoreController@add_merchant_qr');
        $api->any('add_wx_merchant_qr', 'StoreController@add_wx_merchant_qr');

        $api->any('add_shops', 'StoreController@add_shops');

        $api->any('add_sub_store', 'StoreController@add_sub_store');
        $api->any('del_merchant', 'StoreController@del_merchant');
        $api->any('up_merchant', 'StoreController@up_merchant');
        $api->any('up_sub_store', 'StoreController@up_sub_store');
        $api->any('merchant_info', 'StoreController@merchant_info');
        $api->any('del_store', 'StoreController@del_store');
        $api->any('add_store_short_name', 'StoreController@add_store_short_name');

        $api->any('check_store', 'StoreController@check_store');

        $api->any('order', 'OrderController@order');
        $api->any('order_info', 'OrderController@order_info');
        $api->any('refund', 'OrderController@refund');
        $api->any('order_count', 'OrderController@order_count');
        $api->any('data_count', 'OrderController@data_count');
        $api->any('order_data', 'OrderController@order_data');
        $api->any('order_foreach', 'OrderController@order_foreach');
        $api->any('hb_order_foreach', 'OrderController@hb_order_foreach');
        $api->any('weixinapp_index_count', 'OrderController@weixinapp_index_count');

        $api->any('pay_ways_all', 'StoreController@pay_ways_all');
        $api->any('store_all_pay_way_lists', 'StoreController@store_all_pay_way_lists');
        $api->any('company_pay_ways_info', 'StoreController@company_pay_ways_info');
        $api->any('open_company_pay_ways', 'StoreController@open_company_pay_ways');

        $api->any('settle_mode_type', 'StoreController@settle_mode_type');
        $api->any('open_pay_ways', 'StoreController@open_pay_ways');
        $api->any('store_pay_ways', 'StoreController@store_pay_ways');
        $api->any('get_wx_notify', 'StoreController@get_wx_notify');
        $api->any('check_wx_notify', 'StoreController@check_wx_notify');
        $api->any('get_wx_notify_del', 'StoreController@get_wx_notify_del');

        $api->post('createXcxQr', 'StoreController@createXcxQr'); //邀请业务员进件


        //支付收款
        $api->any('scan_pay', 'PayBaseController@scan_pay');
//        $api->any('qr_pay', 'PayBaseController@qr_pay');

        //分期
        $api->any('fq/fq_pay', 'AliFqPayController@fq_pay');
        $api->any('fq/ways_source', 'AliFqPayController@ways_source');
        $api->any('fq/hb_fq_num', 'AliFqPayController@hb_fq_num');
        $api->any('fq/order', 'AlipayFqOrderController@order');
        $api->any('fq/order_info', 'AlipayFqOrderController@order_info');
        $api->any('fq/hbrate', 'AliFqPayController@hbrate');
        $api->any('fq/refund', 'AlipayFqOrderController@refund');
        $api->any('fq/hb_query_rate', 'AlipayFqOrderController@hb_query_rate');
        $api->any('fq/hb_order_cancel', 'AlipayFqOrderController@hb_order_cancel');

        //设置
        $api->any('set_password', 'SetController@set_password');
        $api->any('edit_login_phone', 'SetController@edit_login_phone');
        $api->any('pay_ways_sort', 'SetController@pay_ways_sort');
        $api->any('pay_ways_sort_edit', 'SetController@pay_ways_sort_edit');
        $api->any('pay_ways_open', 'SetController@pay_ways_open');
        $api->any('bind_store_qr', 'SetController@bind_store_qr');
        // 空二维码绑定小程序
        $api->any('bind_applet_store_qr', 'SetController@bind_applet_store_qr');
        $api->any('test_luyu', 'SetController@test_luyu');

        $api->any('add_pay_password', 'SetController@add_pay_password');
        $api->any('edit_pay_password', 'SetController@edit_pay_password');
        $api->any('forget_pay_password', 'SetController@forget_pay_password');
        $api->any('check_pay_password', 'SetController@check_pay_password');
        $api->any('is_pay_password', 'SetController@is_pay_password');
        $api->any('is_password', 'SetController@is_password');
        $api->any('add_password', 'SetController@add_password');

        $api->post('dada_delivery_setting', 'SetController@dada_delivery_setting'); //达达配送设置

        $api->any('me', 'MyController@me');
        $api->any('edit_merchant', 'MyController@edit_merchant');

        //按月统计
        $api->any('Order_mouth_count','OrderCountController@Order_mouth_count');
        $api->any('get_merchant','OrderCountController@get_merchant');      //3-2

        $api->post('yly_print', 'StoreController@yly_print'); //补打小票
        $api->post('update_store', 'StoreController@update_store'); //修改门店信息
        $api->post('store_deliver_settings', 'StoreController@store_deliver_settings'); //门店配送设置
        $api->post('confirmReceiptPrint', 'StoreController@confirmReceiptPrint'); //堂食确认收款 打印单

        // 门店桌号管理
        $api->any('table_lists', 'StoreTableController@getTableLists');
        $api->post('create_table', 'StoreTableController@createTable');
        $api->post('delete_table', 'StoreTableController@deleteTable');
        $api->post('edit_table', 'StoreTableController@editTable');
        $api->any('upload_img', 'StoreTableController@uploadImg');
        $api->any('close_table', 'StoreTableController@closeTable');

        // 小程序收款码
        $api->any('money_qrcode_lists', 'StoreMoneyQrcodeController@getMoneyQrcodeLists');
        $api->post('create_applet_money_qrcode', 'StoreMoneyQrcodeController@createAppletMoneyQrcode');

        // 小程序三方微信接口调用
        $api->post('create_qrcode_rule', 'StoreWechatTicketController@createQrcodeRule');
        $api->any('deduction', 'TransactionDeductionController@deduction');
        $api->any('onlinePayment', 'MerchantRechargeController@onlinePayment');//商户充值
        $api->any('onlinePaymentEasy', 'MerchantRechargeController@onlinePaymentEasy');//商户充值-易生
        $api->any('getTransaction', 'TransactionDeductionController@getTransaction');//商户充值-扣款-记录
        $api->any('getStoreBalance', 'TransactionDeductionController@getStoreBalance');//商户可用余额
        $api->any('getMerchantMonthlyPayments', 'TransactionDeductionController@getMerchantMonthlyPayments');//商户可用余额
        $api->any('getMonthAndSeasonPackage', 'TransactionDeductionController@getMonthAndSeasonPackage');//商户月包季包使用期限
    });


    //需要token 银行卡相关
    $api->group(['namespace' => 'App\Api\Controllers\Merchant', 'prefix' => 'merchant', 'middleware' => 'merchant.api'], function ($api) {
        $api->any('sub_bank', 'BankController@sub_bank');

    });

    //需要token 微信卡券
    $api->group(['namespace' => 'App\Api\Controllers\Weixin', 'prefix' => 'wechat', 'middleware' => 'merchant.api'], function ($api) {
        $api->any('mediaUploadImg', 'WxCardController@mediaUploadImg');
        $api->any('createCard', 'WxCardController@createCard');
        $api->any('getQRTicket', 'WxCardController@getQRTicket');
        $api->any('wxCardWhiteList', 'WxCardController@wxCardWhiteList');

        // 微信代金券
        $api->any('wx_cash_coupon_list', 'WxCashCouponController@wxCashCouponList');
        // 微信代金券批次明细使用、核销数量
        $api->any('wx_cash_coupon_stocks_count', 'WxCashCouponController@wxCashCouponStocksCount');
        // 微信代金券批次明细
        $api->any('wx_cash_coupon_stocks_detail', 'WxCashCouponController@wxCashCouponStocksDetail');
        // 微信代金券核销明细列表
        $api->any('wx_cash_coupon_used_page_list', 'WxCashCouponController@wxCashCouponUsedPageList');
        // 创建免充值微信代金券
        $api->any('create_merchant_coupon', 'WxCashCouponController@createMerchantCoupon');

        // 微信商家券核销明细
        $api->any('wechatMerchantCouponUsedList', 'WxCashCouponController@getWechatMerchantCouponUsedList');
        // 更新微信卡券强制发送状态
        $api->any('updateCouponSendStatus', 'WxCashCouponController@updateCouponSendStatus');
    });

    $api->group(['namespace' => 'App\Api\Controllers\Weixin', 'prefix' => 'wechat'], function ($api) {
        // 微信代金券扫二维码领券
        $api->any('oauth', 'WxCashCouponController@oauth');
        // 扫码领券回调
        $api->any('oauth_callback', 'WxCashCouponController@oauth_callback');
    });

    //需要token
    $api->group(['namespace' => 'App\Api\Controllers\MeiTuanPeiSong', 'prefix' => 'mtps', 'middleware' => 'merchant.api'], function ($api) {
        $api->any('orderCreateByShop', 'AddController@orderCreateByShop');
        $api->any('orderCreateByCoordinates', 'AddController@orderCreateByCoordinates');
        $api->any('shopCreate', 'AddController@shopCreate');

        $api->any('orderDelete', 'CancelController@orderDelete');

        $api->any('orderStatusQuery', 'SelectController@orderStatusQuery');
        $api->post('orderRiderLocation', 'SelectController@orderRiderLocation');
        $api->post('shopAreaQuery', 'SelectController@shopAreaQuery');
        $api->post('shopQuery', 'SelectController@shopQuery');

    });

    //需要token 达达配送
    $api->group(['namespace' => 'App\Api\Controllers\DaDa', 'prefix' => 'dada', 'middleware' => 'merchant.api'], function ($api) {
        $api->post('orderConfirmGoods', 'AddController@orderConfirmGoods'); //妥投异常之物品返回完成
        $api->post('messageConfirm', 'AddController@messageConfirm'); //消息确认
        $api->post('shopAdd', 'AddController@shopAdd'); //批量新增门店

    });


    //微官网 无需token
    $api->group(['namespace' => 'App\Api\Controllers\MiNiWebsite', 'prefix' => 'miniweb'], function ($api) {
        $api->post('getMiniWebIndex', 'IndexController@getMiniWebIndex'); //获取 首页模块
        $api->post('getMiniWebTag', 'IndexController@getMiniWebTag'); //获取 标签
        $api->post('getMiniWebShowList', 'IndexController@getMiniWebShowList'); //获取 展示列表
        $api->post('getMiniWebContact', 'IndexController@getMiniWebContact'); //获取 联系我们

    });

    //微官网 需token
    $api->group(['namespace' => 'App\Api\Controllers\MiNiWebsite', 'prefix' => 'miniweb', 'middleware' => 'merchant.api'], function ($api) {
        $api->post('updateMiniWebIndex', 'IndexController@updateMiniWebIndex'); //编辑首页模块
        $api->post('delMiniWebIndex', 'IndexController@delMiniWebIndex'); //删除 首页模块

        $api->post('updateMiniWebTag', 'IndexController@updateMiniWebTag'); //编辑 标签
        $api->post('delMiniWebTag', 'IndexController@delMiniWebTag'); //删除 标签

        $api->post('updateMiniWebShowList', 'IndexController@updateMiniWebShowList'); //编辑 展示列表
        $api->post('delMiniWebShowList', 'IndexController@delMiniWebShowList'); //删除 展示列表

        $api->post('updateMiniWebContact', 'IndexController@updateMiniWebContact'); //编辑 联系我们
        $api->post('delMiniWebContact', 'IndexController@delMiniWebContact'); //删除 联系我们


    });
    //无需token
    $api->group(['namespace' => 'App\Api\Controllers\MiNiWebsite', 'prefix' => 'miniwebsite'], function ($api) {
        $api->post('updateBodyHits', 'AppletsController@updateBodyHits'); //单个用户点击数
        $api->post('momentUp', 'AppletsController@momentUp'); //更新秒数
        $api->post('updateDailyHits', 'AppletsController@updateDailyHits'); //每次每天点击量
        $api->post('getOpenPhone', 'AppletsController@getOpenPhone'); //获取手机号

    });


    //微信即时配送 无需token
    $api->group(['namespace' => 'App\Api\Controllers\CustomerApplets', 'prefix' => 'wechatapplet'], function ($api) {
        $api->post('businessShopGet', 'ImmediateDeliveryController@businessShopGet'); //拉取已绑定账号
        $api->post('businessDeliveryGetAll', 'ImmediateDeliveryController@businessDeliveryGetAll'); //获取已支持的配送公司列表
        $api->post('businessOrderGet', 'ImmediateDeliveryController@businessOrderGet'); //拉取配送单信息
        $api->any('onOrderStatus', 'ImmediateDeliveryController@onOrderStatus'); //配送单配送状态更新通知接口(微信调用商户的接口)

    });

    //微信即时配送 需token
    $api->group(['namespace' => 'App\Api\Controllers\CustomerApplets', 'prefix' => 'wechatapplet', 'middleware' => 'merchant.api'], function ($api) {
        $api->post('businessOpen', 'ImmediateDeliveryController@businessOpen'); //开通即时配送
        $api->post('businessShopAdd', 'ImmediateDeliveryController@businessShopAdd'); //发起绑定帐号
        $api->post('businessOrderPreCancel', 'ImmediateDeliveryController@businessOrderPreCancel'); //预取消配送单
        $api->post('businessOrderCancel', 'ImmediateDeliveryController@businessOrderCancel'); //取消配送单
        $api->post('businessOrderConfirmReturn', 'ImmediateDeliveryController@businessOrderConfirmReturn'); //异常件退回商家商家确认收货
        $api->post('getOrderLists', 'ImmediateDeliveryController@getOrderLists'); //配送订单查询

    });


    $api->group(['namespace' => 'App\Api\Controllers\MiNiWebsite', 'prefix' => 'miniweb','middleware' => 'merchant.api'], function ($api) {
        $api->post('getIntentionality', 'AppletsController@getIntentionality'); //意向客户列表
        $api->post('memberCounts', 'AppletsController@memberCounts'); //统计数
        $api->post('remarksUp', 'AppletsController@remarksUp'); //添加评论
        $api->post('sevenCounts', 'AppletsController@sevenCounts'); //七天统计
    });

});
