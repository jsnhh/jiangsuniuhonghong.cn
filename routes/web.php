<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return view('merchantpc.login');
    return view('user.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['namespace' => 'AlipayOpen'], function () {
    Route::get('/alipay_oqr_create', 'AlipayCreateOrderController@alipay_oqr_create')->name('alipay_oqr_create');
});

//授权后跳转成功
Route::group(['namespace' => 'Merchant', 'prefix' => 'merchant'], function () {
    //跳转支付宝app
    Route::get('/appAlipay', 'MerchantController@appAlipay')->name('appAlipay');
});

//单页面
Route::group(['namespace' => 'Page', 'prefix' => 'page'], function () {
    Route::get('/success', 'PageController@success');
    Route::get('/pay_errors', 'PageController@pay_errors');
    Route::get('/pay_success', 'PageController@pay_success');
    Route::get('/cz_success', 'PageController@cz_success');

    //小程序添加员工
    Route::get('/add_merchant', 'PageController@add_merchant');

    //微信点金计划
    Route::get('/gold_plan', 'PageController@gold_plan');
});

//二维
Route::group(['namespace' => 'Qr'], function () {
    //空码生成
    Route::get('/qr', 'QrController@qr')->name('qr');
    Route::get('/qr_code_hb', 'QrController@qr_code_hb')->name('qr_code_hb');
    Route::get('/cr', 'QrController@cr')->name('cr');
    Route::get('/openidQr', 'QrController@openidQr')->name('openidQr');
    Route::get('/shoppingOpenidQr', 'QrController@shoppingOpenidQr')->name('shoppingOpenidQr');
});

//授权小程序页面
Route::group(['namespace' => 'AuthorizeApplets','prefix' => 'authorize'], function () {
    //输出授权小程序
    Route::get('/applets', 'AppletsController@index');
    //输出创建小程序
    Route::get('/createApplets', 'AppletsController@createApplets');
    //输出创建小程序
    Route::get('/appletsList', 'AppletsController@appletsList');
    //授权回调 URI
    Route::any("refundAuthUrl/{store_id}", "AppletsController@refundAuthUrl");
});

//商户进件页面
Route::group(['namespace' => 'AuthorizeApplets','prefix' => 'authorize'], function () {
    //商户入网
    Route::get('/storeNetwork', 'StoreInputController@storeNetwork');
    //商户进件
    Route::get('/storeInput', 'StoreInputController@storeInput');
    //商户进件
    Route::get('/parts', 'StoreInputController@parts');
    //商户进件
    Route::get('/partsTow', 'StoreInputController@partsTow');
    //店铺信息
    Route::get('/shop', 'StoreInputController@shop');
    //门店地址
    Route::get('/addaddress', 'StoreInputController@addaddress');
    //法人认证
    Route::get('/addidcardphoto', 'StoreInputController@addidcardphoto');
    //结算信息
    Route::get('/addbindcard', 'StoreInputController@addbindcard');
    //开户许可证
    Route::get('/addpermit', 'StoreInputController@addpermit');
    //银行卡类型
    Route::get('/bankcardtype', 'StoreInputController@bankcardtype');
    //开户支行
    Route::get('/branchbank', 'StoreInputController@branchbank');
    //上传手持身份证正面照
    Route::get('/Cardphoto', 'StoreInputController@Cardphoto');
    //银行卡照片
    Route::get('/addbankphoto', 'StoreInputController@addbankphoto');
    //营业执照
    Route::get('/addLicense', 'StoreInputController@addLicense');
    //商户进件layui
    Route::get('/storeInputs', 'StoreInputController@storeInputs');
});

//刷脸支付查询页面
Route::group(['namespace' => 'AuthorizeApplets','prefix' => 'authorize'], function () {
    Route::get('/FaceSearch', 'FaceSearch@FaceSearch');
});

//vip抽奖
Route::group(['namespace' => 'AuthorizeApplets','prefix' => 'authorize'], function () {
    Route::get('/VipDraw', 'VipDraw@VipDraw');
});
