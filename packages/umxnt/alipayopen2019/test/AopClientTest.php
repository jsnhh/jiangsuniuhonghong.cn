<?php

require_once '../AopClient.php';
require_once '../AopCertification.php';
require_once '../request/AlipayTradeQueryRequest.php';
require_once '../request/AlipayTradeWapPayRequest.php';
require_once '../request/AlipayTradeAppPayRequest.php';
require_once '../request/AlipaySystemOauthTokenRequest.php';

require_once '../request/AlipayOfflineMaterialImageUploadRequest.php';

/**
 * 证书类型AopClient功能方法使用测试
 * 1、execute 调用示例
 * 2、sdkExecute 调用示例
 * 3、pageExecute 调用示例
 */
 
$aop = new AopClient ();
$aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
$aop->appId = '2021001168634163';
$aop->method = "alipay.offline.material.image.upload";
$aop->charset = "utf-8";
$aop->timestamp = "2020-11-16 16:11:56";
$aop->rsaPrivateKey = 'MIIEpAIBAAKCAQEAsDA3gWgOM+6yiq4xZnjzCVVYjnfLmbJ/ofiPHQd4Vvt2U1eHndwEElnBrQ4HBn31AqclKZLNIPFPuA73chk/FAE5cLxUy1EE8jHx6FacRwDIy+oeGjaffdlCx2NMc5O79zYd32GKulRt9s/p6Lj0YhCgqUdYATeC8/YPGw8Jp8hUULvbHQ/Pf2ZK10BHlsj9TFvUd4oxj9G1mWWN8XQ31me7X/IDkh6C5wLQLUZT00RqtycfVN8F7KMZWyEwlHNjyoijCjoX/XBS5UlcP6XQ61tb3W0pcG+W5Ddi5G3183rnJCOZLUEXwG3joiCuBHSpVorQdSkQ9gg7i8g3U8j6RQIDAQABAoIBAAyaZl4LY3izinIBI+eAn2H7iOooB5hm/FpJvXMGJcWTF1O+3ySbqG7nnMMUTcxZFj2qVy4yIhpJKLqx12GCDNERhVpbHnWsxf3NNNC54VPy6morxRt67M3DzNMWd1GNL9TvOhPQlmvdvj/5f3H2SQT6y4NQZysvHV72Kv50hOxLeXqWXfj4iRTD8ZJPDtIDIwkqxgEpLYs5vLL/W5i0Le1zZGTTYmip4ge63l9Kc6zSgk0sz2kh+8SZ9or/wq8NPY60R0dhRe3h+EuUBugBfP7z6t/LulcshwNOss64RVO3/YwgSV5l6e0u4YIaJX63SUkYTmSdD2KHExGJCy6krAECgYEA+GUDVlEE5XtpKoPob1v1KoK1SBj2Kx/Q+XQ2sHFFzXaEitAPC+7KGs17gGVP3QYo9VJHbFEsxOs60+YzBDs6f32aDcHR1YIvfwR46X3YY5/ooUjVcV/iVqaNEo4NBOP620hIW4erAIKpJoRett0tMayqXgap9NqrHrWu3H/aITECgYEAtZU7G/ydlkks1bfJXdm81LyqFqOlkf4Fi2UuEXvTL78NBwxnYLNCWSiNGC6UcgkOZo7cZA4a1S2ncb7aNEbg1MuFaPXPgW92qTMlED5JONjw4bHwSPGYtVQR890zSYZ4/MfhDomqmwEAOSF9ZoFyt2yhIVgHMRlDeBtV0huWBVUCgYEAvgubGAl485MOjYbK6Ziz5UiIJpdikPAQ1SsDRNAFlgPduM8jFAApBYS9vyjEh9jUsY24M7mPxUIsij+QPOaMBUs1+NyYDQFEEax8TjkIvIB2Xh/5nnq8E4vuV4nzkBBVuDx/j/gm74cIn/2a8dIbZ+wxlSsIEdzUG/Tpyq7PV6ECgYAsXI/7T2UEkU4FzIfCiO5sHseDDmnl1+Rs4MQ3e0Pl90vse+hJpKHB/Hkz/jVAAtxky+QmAZgmmqe8zz6p5OMKucoXEW6nlZ6syZT1efITlFSNHxawC24eZrKYHGNqRBMM1pwyHAn1InJ2gqsLPvbSvxRCfCoNobls/2ihEyI8nQKBgQCGCf4C6tQEVJoASIcCIkRe/BWumlhN1WMPJ7UV1KN8ad9itE4Tv2/g5wOFq10ZdyG6btPOicgPQa3PRZxeBoCTgwJLtUPcCtE256tKymR4qn3iQtuDthAzZEA0Jty6cvAbozr8xU+6/YAF7SHo4UYCGJriOF/3264ExoeKvDpIXA==';
$aop->alipayrsaPublicKey = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAku9/AwrKQ6/L/BxNgq41m366iP4MThotpDU/g3rvnPETLAHpp+sFjEwF/bXEBpSd0FCSBF6Gd1UxSeV3ayaDCNOemaRU7rfvelddt16rcAnB3fTvPU8Ksssve48EkvgSAJxst5pEM8eUv0Dbe3IPX0h33NgOUYBWmBLVWX8ljj8AiHm92moVTT/eBuSx7HBmsETd+lb3Lq5mnWo+jz/VTjsirtLYRY+O7BtufJcw7iQa06EV6tZLFq+wXdHHFNakXJrxSbRcnPAZ1X3hBqhop0gxWSKJP0AuFWosnAqXFw9uSAWIP2nF4N7gCgNvtWh8VvMJtCTx0dZ1e1aMZKQh5wIDAQAB';
$aop->apiVersion = '1.0';
$aop->signType = 'RSA2';
$aop->postCharset = 'utf-8';
$aop->format='json';

$request = new AlipayOfflineMaterialImageUploadRequest ();
$request->setImageType("jpg");
$request->setImageName("avatar");
$request->setImageContent("@"."C:\phpstudy_pro\WWW\wechat_proejct\wechat_basketball\static\image\avatar.jpg");

$result = $aop->execute ( $request,null,"202011BB9b99d46c6fd0442bb850e7913949dX73");

$responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
$resultCode = $result->$responseNode->code;
if(!empty($resultCode)&&$resultCode == 10000){
	$image_id  = $result->$responseNode->image_id;
    $image_url = $result->$responseNode->image_url;
	echo $image_id."<br/>".$image_url;
} else {
	echo "失败";
}
			

/**
 *
//1、execute 使用
$aop = new AopClient ();

$aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
$aop->appId = '2021001189605846';
$aop->method = "alipay.system.oauth.token";
$aop->charset = "utf-8";
$aop->timestamp = "2020-08-24 12:56:56";
$aop->rsaPrivateKey = 'MIIEpAIBAAKCAQEAsRwKFxj3lpYAFnW3Emnn8QVK2gfJAIeXxUQCmG1Ict3KQDU+dynmXYktAzPz5GljJ5BldfKoQ+dACNXDPiF9s+2foMcbkM5jSdAya2kz7hTAz3/PHN1rTdOY4H/ck76dWh+uSQFIqzkhNQTZGWwDc1uqKDaxzYEhEfjKmEd4axjFdm+K1WpPnLeRt4YS/N2cpgBlosPaGKPUk1ABh+czFFaQ5KQTKx6VV4jgAGgLn+bFrpNiz62Kb0hUpjiPoYeFOSwwwAUy9Ac4jnx38QtNL7tmy/09rbLFC9yzj+8X4SS47tuRsIUvJcYSUhClLeWSbtEkffdBPpXS3aT9m2M+JwIDAQABAoIBAQCuq1KgOPVlRAZOIEYvGNew8mFB6k+oABIEcPCGV7EN6ALhlCFI8L/LG9CSyhgM+orZqCUIUAr2/QhALmHh0rHgU8gMjpF3HLT7RSOUnyqh8UG+4vPmKdD77mVlc3SdGW6iRYBja6q5gxxm56Mc2VQF3y9PfbGZVXLCALX9vO7b3utSzgFnAZvnYIVBV8OcAqw29XkOG8E6c63UNJFtkgMj6XEUNwMjKgWFYqb3AnXksI/27Jrb9j/kqhwZS9tokQylibRE9aRVwW6HHC7F6pFJ++iE3S4dl8Tzs1sUaOcAENocKxS5IoTDiQxgzc/jtW9xLXg8dlH7J6MQuTXni39RAoGBAOQG2UvaRxLK+8+mfxFwhsnA0OlbuJdLXMT5jGqwddY0YZDGeEqs2xEi7fkFSSyhIiizzoyKO0PPuCy5BdijLRnT9Q7WM1d8Eev2kxQXDiaByGCcnZyw/a+vzaDrbFcCHzCx9kopQXLypbYxtkWjk2T1INTsa6USz3LJlWTP8gLLAoGBAMbWJEMo2UlY44ufVWTKPKJkqPI5CRfTppv6fgILBaitHUcKaA1/N7fzhWjnO16A4p4LB4+V0l1UqDn5Ya/KjXH3yTbQzc3kusggomnjKBzYwKQpOD8/jr23yfhiOTZX9cyg2EC9IEVYS4I+z3+HgvQvNv60suo3SVE5XMAC3RqVAoGAB+3xCoE5rX6fiRYKHyohlcx9t0+4OJKnVDY1WtRuFJfXuLgeIMKoD41+ZwV3CPVFgdLW4O5Bp1cG2Y9Jk8IA5IAJao2qVWV8Hg+CB9XRaSdkN2/az0McX0qmxOzK5K+vM/tMTEHpA71KY+6QnxVUH9OiY6UhDSl9S0mVR18v1EcCgYB/joMG3p6+chrPyHCkzySk3USQ18wgj/PzXngV4XJI6n28xE1KU1GwXbY5PRYWI0K+Nr8r+uZQX23oezaS6rNg6KAZ3UwaCOFWX7Exkrt5/gdfiN3/nUt6I/yarpCbb0RNeOYiafHY6rLOJHNDEfENq3hxYATjQoJzbAQ3Xd7wHQKBgQDW6VZz76zzJFOZtwmGMqVyUNQAmkgimwPWTjRB509kLB5QR/pYMlZGEnEI6F/I+U6YcY4eDOVqEaUZ99DbBLNAFQGnaPip8q9e9bpFWwT8o3+F5jelXDgsmGYCZqVQfI8B4Cx3f0szyFubOQsxZ/beQiMIjGhKdsu8pSF94ZScVg==';
$aop->alipayrsaPublicKey = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5NhTUb47muscqu9CrB4Yg1odTHpKPaB33t2RcYsvG8+eFg7SW9PRnM7QMHgZWq7R+atpISslFlUt9p0SL9r1GaPt1IFnlT+DpLrudlzoT7vMeatH/anQRTiK6CWVJcCauINImDVqVda3NL6n2YoYS3a5XMJFxQMMbRL/ZAdB6cQQKRXpwRuF+4UplxbGd0QV1RRBSZOorNwLrUBvuNYGH00XtU152pQ1Mb4eZNezmm/zVkNXsz0KXNlQXYju9eT8lYx+/fBVGQ5isCy7Zqls3KTtZB1o62epqOB/7UUsCHRKd/+fx9bTBWedKIqkw92mxAhuoVgOYx+sUGIjQiISEwIDAQAB';
$aop->apiVersion = '1.0';
$aop->signType = 'RSA2';
$aop->postCharset = 'utf-8';
$aop->format = 'json';
$aop->grant_type = "authorization_code";

$request = new AlipayTradeQueryRequest ();
$request->setBizContent("{" .
    "\"out_trade_no\":\"20150320010101001\"," .
    "\"trade_no\":\"2014112611001004680 073956707\"," .
    "\"org_pid\":\"2088101117952222\"," .
    "      \"query_options\":[" .
    "        \"TRADE_SETTE_INFO\"" .
    "      ]" .
    "  }");
var_dump($request);

//$result = $aop->execute($request);
//var_dump($result);

//2、sdkExecute 测试

$aop = new AopClient ();

$aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
$aop->appId = '你的appid';
$aop->rsaPrivateKey = '你的应用私钥';
$aop->alipayrsaPublicKey = '你的支付宝公钥';
$aop->apiVersion = '1.0';
$aop->signType = 'RSA2';
$aop->postCharset = 'utf-8';
$aop->format = 'json';

$request = new AlipayTradeAppPayRequest ();
$request->setBizContent("{" .
    "\"timeout_express\":\"90m\"," .
    "\"total_amount\":\"9.00\"," .
    "\"product_code\":\"QUICK_MSECURITY_PAY\"," .
    "\"body\":\"Iphone6 16G\"," .
    "\"subject\":\"大乐透\"," .
    "\"out_trade_no\":\"70501111111S001111119\"," .
    "\"time_expire\":\"2016-12-31 10:05\"," .
    "\"goods_type\":\"0\"," .
    "\"promo_params\":\"{\\\"storeIdType\\\":\\\"1\\\"}\"," .
    "\"passback_params\":\"merchantBizType%3d3C%26merchantBizNo%3d2016010101111\"," .
    "\"extend_params\":{" .
    "\"sys_service_provider_id\":\"2088511833207846\"," .
    "\"hb_fq_num\":\"3\"," .
    "\"hb_fq_seller_percent\":\"100\"," .
    "\"industry_reflux_info\":\"{\\\\\\\"scene_code\\\\\\\":\\\\\\\"metro_tradeorder\\\\\\\",\\\\\\\"channel\\\\\\\":\\\\\\\"xxxx\\\\\\\",\\\\\\\"scene_data\\\\\\\":{\\\\\\\"asset_name\\\\\\\":\\\\\\\"ALIPAY\\\\\\\"}}\"," .
    "\"card_type\":\"S0JP0000\"" .
    "    }," .
    "\"merchant_order_no\":\"20161008001\"," .
    "\"enable_pay_channels\":\"pcredit,moneyFund,debitCardExpress\"," .
    "\"store_id\":\"NJ_001\"," .
    "\"specified_channel\":\"pcredit\"," .
    "\"disable_pay_channels\":\"pcredit,moneyFund,debitCardExpress\"," .
    "      \"goods_detail\":[{" .
    "        \"goods_id\":\"apple-01\"," .
    "\"alipay_goods_id\":\"20010001\"," .
    "\"goods_name\":\"ipad\"," .
    "\"quantity\":1," .
    "\"price\":2000," .
    "\"goods_category\":\"34543238\"," .
    "\"categories_tree\":\"124868003|126232002|126252004\"," .
    "\"body\":\"特价手机\"," .
    "\"show_url\":\"http://www.alipay.com/xxx.jpg\"" .
    "        }]," .
    "\"ext_user_info\":{" .
    "\"name\":\"李明\"," .
    "\"mobile\":\"16587658765\"," .
    "\"cert_type\":\"IDENTITY_CARD\"," .
    "\"cert_no\":\"362334768769238881\"," .
    "\"min_age\":\"18\"," .
    "\"fix_buyer\":\"F\"," .
    "\"need_check_info\":\"F\"" .
    "    }," .
    "\"business_params\":\"{\\\"data\\\":\\\"123\\\"}\"," .
    "\"agreement_sign_params\":{" .
    "\"personal_product_code\":\"CYCLE_PAY_AUTH_P\"," .
    "\"sign_scene\":\"INDUSTRY|DIGITAL_MEDIA\"," .
    "\"external_agreement_no\":\"test20190701\"," .
    "\"external_logon_id\":\"13852852877\"," .
    "\"access_params\":{" .
    "\"channel\":\"ALIPAYAPP\"" .
    "      }," .
    "\"sub_merchant\":{" .
    "\"sub_merchant_id\":\"2088123412341234\"," .
    "\"sub_merchant_name\":\"滴滴出行\"," .
    "\"sub_merchant_service_name\":\"滴滴出行免密支付\"," .
    "\"sub_merchant_service_description\":\"免密付车费，单次最高500\"" .
    "      }," .
    "\"period_rule_params\":{" .
    "\"period_type\":\"DAY\"," .
    "\"period\":3," .
    "\"execute_time\":\"2019-01-23\"," .
    "\"single_amount\":10.99," .
    "\"total_amount\":600," .
    "\"total_payments\":12" .
    "      }" .
    "    }" .
    "  }");
$result = $aop->sdkExecute($request);

$responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
echo $responseNode;
$resultCode = $result->$responseNode->code;
if (!empty($resultCode) && $resultCode == 10000) {
    echo "成功";
} else {
    echo "失败";
}


//3、pageExecute 测试
$aop = new AopClient ();

$aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
$aop->appId = '你的appid';
$aop->rsaPrivateKey = '你的应用私钥';
$aop->alipayrsaPublicKey = '你的支付宝公钥';
$aop->apiVersion = '1.0';
$aop->signType = 'RSA2';
$aop->postCharset = 'utf-8';
$aop->format = 'json';

$request = new AlipayTradeWapPayRequest ();
$request->setBizContent("{" .
    "    \"body\":\"对一笔交易的具体描述信息。如果是多种商品，请将商品描述字符串累加传给body。\"," .
    "    \"subject\":\"测试\"," .
    "    \"out_trade_no\":\"70501111111S001111119\"," .
    "    \"timeout_express\":\"90m\"," .
    "    \"total_amount\":9.00," .
    "    \"product_code\":\"QUICK_WAP_WAY\"" .
    "  }");
$result = $aop->pageExecute($request);
echo $result;
 */
