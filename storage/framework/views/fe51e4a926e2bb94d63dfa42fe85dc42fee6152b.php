<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>代运营授权列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style>
        .edit{background-color: #ed9c3a;}
        .shenhe{background-color: #429488;}
        .see{background-color: #7cb717;}
        .cur{color:#009688;}
        .del {background-color: #e85052;}    /*.laytable-cell-1-school_icon{height:100%;}*/
        .yname{
            font-size: 13px;
            color: #444;
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">

            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12" style="margin-top:0px">
                        <div class="layui-card">
                            <div class="layui-card-header">代运营授权列表</div>

                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;display:flex">

                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    

                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    

                                    
                                    
                                    
                                    
                                    
                                    
                                    



                                </div>

                                <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>


                                
                                
                                

                                <script type="text/html" id="table-content-list">
                                    <a class="layui-btn layui-btn-normal layui-btn-xs edit" lay-event="edit">查询授权状态</a>
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<input type="hidden" class="user_id">
<input type="hidden" class="status">
<!-- 操作按钮 -->
<!--

<a class="layui-btn layui-btn-normal layui-btn-xs see" lay-event="detail">查看</a> -->
<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var id="<?php echo e($_GET['id']); ?>";
    var store_id="<?php echo e($_GET['store_id']); ?>";
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form', 'table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,table = layui.table
            ,form = layui.form
            ,laydate = layui.laydate;


        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="<?php echo e(url('/user/login')); ?>";
            }
        })

        table.render({
            elem: '#test-table-page'
            ,url: "<?php echo e(url('/api/alipayopen/operationList')); ?>"
            ,method: 'post'
            ,where:{
                token:token,
                id:id,
                store_id:store_id,
            }
            ,request:{
                pageName: 'p',
                limitName: 'l'
            }
            ,page: true
            ,cellMinWidth: 150
            ,cols: [[
                {field:'store_type', title: '类型'}
                ,{field:'merchant_no', title: '2088商户号'}
                ,{field:'alipay_account', title: '支付宝账号'}
                ,{field:'bind_user_id', title: 'bind_user_id'}
                ,{field:'handle_status', title: '授权状态'}
                ,{width:200,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
            ]]
            ,response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                ,statusCode: 1 //成功的状态码，默认：0
                ,msgName: 'message' //状态信息的字段名称，默认：msg
                ,countName: 't' //数据总数的字段名称，默认：count
                ,dataName: 'data' //数据列表的字段名称，默认：data
            }
            ,done: function(res, curr, count){
                console.log(res);
                $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});	//进行表头样式设置
            }

        });



        table.on('tool(test-table-page)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var e = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            console.log(e);
            // sessionStorage.setItem('s_store_id', e.store_id);

            if(layEvent === 'del'){
                layer.confirm('确认删除此消息?',{icon: 2}, function(index){
                    $.post("<?php echo e(url('/api/adlinks/adlinksDel')); ?>",
                        {
                            token:token
                            ,id:e.id
                        },function(res){
                            console.log(res);
                            if(res.status==170005){
                                obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                                layer.close(index);
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 1
                                    ,time: 2000
                                });
                            }else{
                                layer.msg(res.message, {
                                    offset: '50px'
                                    ,icon: 2
                                    ,time: 3000
                                });
                            }
                        },"json");

                });
            }else if(layEvent === 'edit'){
                
                $.post("<?php echo e(url('/api/alipayopen/iotOperationBindQuery')); ?>",
                    {
                        token:token
                        ,id:e.id
                    },function(res){
                        console.log(res);
                        if(res.status==1){
                            layer.msg(res.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 2000
                            });
                        }else{
                            layer.msg(res.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 3000
                            });
                        }
                    },"json");
            }else if(layEvent === 'zfb'){
                

                $.post("<?php echo e(url('/api/alipayopen/iotOperationStoreQuery')); ?>",
                    {
                        token:token
                        ,id:e.id
                    },function(res){
                        console.log(res);
                        if(res.status==1){
                            layer.msg(res.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 2000
                            });
                        }else{
                            layer.msg(res.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 3000
                            });
                        }
                    },"json");
            }else if(layEvent === 'stores'){
                $('.stores').attr('lay-href',"<?php echo e(url('/user/antstorelist?')); ?>"+e.id);
            }

        });


        // 选择学校
        form.on('select(schooltype)', function(data){
            var type = data.value;

            //执行重载
            table.reload('test-table-page', {
                where: {
                    type: type
                }
                ,page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });


        //监听搜索
        form.on('submit(LAY-app-contlist-search)', function(data){
            var value = data.field.id;
            //执行重载
            table.reload('test-table-page', {
                where: {
                    title: value
                }
                ,page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });



    });


</script>

</body>
</html>