<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>转移代理商</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
<link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
<style type="text/css">
    .userbox{
        height:200px;
        border:1px solid #eee;
        overflow-y: auto;
    }
    .userbox .list{
        height:38px;line-height: 38px;cursor:pointer;
        padding-left:10px;
    }
    .userbox .list:hover{
        background-color:#eeeeee;
    }
    .userboxs{
        height:200px;
        border:1px solid #eee;
        overflow-y: auto;
    }
    .userboxs .list{
        height:38px;line-height: 38px;cursor:pointer;
        padding-left:10px;
    }
    .userboxs .list:hover{
        background-color:#eeeeee;
    }
</style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header">转移代理商</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
            <!-- <div class="layui-form-item">
                    <label class="layui-form-label">上级代理商</label>
                    <div class="layui-input-block">
                        <div class="layui-form-mid agentname"></div>
                    </div>
                </div>  -->
                <div class="layui-form-item">
                    <label class="layui-form-label">转移代理商</label>
                    <div class="layui-input-block"style="width:600px">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off"  class="layui-input">

                        <!-- <div class="userbox" style='display: none'></div> -->
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">转&emsp;移&emsp;到</label>
                    <div class="layui-input-block"style="width:600px">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" class="layui-input transfers">

                        <div class="userboxs" style='display: none'></div>
                    </div>
                </div>
                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit site-demo-active"style="border-radius:5px" data-type="tabChange">保存</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="js_user_id">
<input type="hidden" class="js_user_ids">

<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script> 
<script>
    var token = sessionStorage.getItem("Usertoken");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','element'], function(){
        var $ = layui.$ 
            ,admin = layui.admin
            ,element = layui.element
            ,form = layui.form

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="<?php echo e(url('/user/login')); ?>";
            }
        });


    //     // 需转代理商
    //     $(".transfer").bind("input propertychange",function(event){
    // //       console.log($(this).val());
    //        $.post("<?php echo e(url('/api/user/get_sub_users')); ?>",
    //         {
    //             token:token
    //             ,user_name:$(this).val()
    //             ,self:'1'
    //         },function(res){
    //            console.log(res);
              
    //             var html="";
    // //            console.log(res.t);
    //             if(res.t==0){
    //                 $('.userbox').html('')
    //             }else{
    //                 for(var i=0;i<res.data.length;i++){
    //                     html+='<div class="list" data='+res.data[i].id+'>'+res.data[i].name+'</div>'
    //                 }
    //                 $(".userbox").show();
    //                 $('.userbox').html('');
    //                 $('.userbox').append(html);
    //             }
    //         },"json");
    //     });

    //     $(".userbox").on("click",".list",function(){
    //         $('.transfer').val($(this).html());
    //         $('.js_user_id').val($(this).attr('data'));
    //         $('.userbox').hide();
    //         console.log($('.js_user_id').val())
    //     });

        $('.layui-form .layui-form-item').eq(0).find('input').val("<?php echo e($_GET['user_name']); ?>");
        // console.log($('.layui-form .layui-form-item').eq(0).find('input').val("<?php echo e($_GET['user_name']); ?>"))

       
        
        // 目标代理商
        $(".transfers").bind("input propertychange",function(event){
    //       console.log($(this).val());
           $.post("<?php echo e(url('/api/user/get_sub_users')); ?>",
            {
                token:token
                ,user_name:$(this).val()
                ,self:'1'
            },function(res){
    //            console.log(res);
                var html="";
    //            console.log(res.t);
                if(res.t==0){
                    $('.userboxs').html('')
                }else{
                    for(var i=0;i<res.data.length;i++){
                        html+='<div class="list" data='+res.data[i].id+'>'+res.data[i].name+'</div>'
                    }
                    $(".userboxs").show();
                    $('.userboxs').html('');
                    $('.userboxs').append(html);
                    
                }
            },"json");
        });

        $(".userboxs").on("click",".list",function(){
            $('.transfers').val($(this).html());
            $('.js_user_ids').val($(this).attr('data'));
            $('.userboxs').hide();
            console.log($('.js_user_ids').val())
        });
         


        $('.submit').on('click', function(){
            $.post("<?php echo e(url('/api/user/update_affiliation')); ?>",
            {
                token:token,
                userId:"<?php echo e($_GET['user_id']); ?>",
                userPid:$('.js_user_ids').val()
            },function(res){
            //    console.log(token,$('.js_user_id').val(),$('.js_user_ids').val())
            console.log(res)
                if(res.status==1){
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 3000
                    });
                }else{
                    layer.alert(res.message, {icon: 2});
                }
            },"json");
        });

    });

</script>

</body>
</html>
