<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>设备列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style>
        .edit{background-color: #ed9c3a;}
        .shenhe{background-color: #429488;}
        .see{background-color: #7cb717;}
        .tongbu{background-color: #4c9ef8;color:#fff;}
        .cur{color:#009688;}
        .yname{
          font-size: 13px;
          color: #444;
          margin-bottom:8px;
        }
        .xgrate{
        color: #fff;
        font-size: 15px;
        padding: 7px;
        height: 30px; 
        line-height: 30px;
        /* border: 1px solid #666; */
        background-color: #3475c3;
      }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12">
                        <div class="layui-card" style="margin-top:0px">
                            <div class="layui-card-header">设备列表</div>
                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">

                                    <!-- 搜索 -->
                                    <div class="layui-form" lay-filter="component-form-group" style="width:400px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <div class="layui-inline">
                                                <div class="layui-input-inline">
                                                    <text class="yname">设备ID</text>
                                                    <input type="text" name="deviceid" placeholder="请输入设备ID" autocomplete="off" class="layui-input">
                                                </div>
                                            </div>
                                            <div class="layui-inline">
                                                <button class="layui-btn layuiadmin-btn-list" lay-submit="" lay-filter="LAY-app-contlist-search" style="border-radius:5px;margin-top:23px;margin-bottom: 0;height:36px;line-height: 36px;">
                                                    <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>


                                    <a href="<?php echo e(url('/qr68-tpl.xls')); ?>">
                                        <button class="layui-btn layui-btn-danger" style="border-radius:5px;margin-top:23px;margin-bottom: 4px;height:36px;line-height: 36px;">百富qr68设备导入模版下载</button>
                                    </a>
                                    <a href="<?php echo e(url('/cm07-tpl.xls')); ?>">
                                        <button class="layui-btn layui-btn-normal" style="border-radius:5px;margin-top:23px;margin-bottom: 4px;height:36px;line-height: 36px;">青蛙++织点CM07设备导入模版下载</button>
                                    </a>
                                    <a href="<?php echo e(url('/qingting-tpl.xls')); ?>">
                                        <button class="layui-btn layui-btn-warm" style="border-radius:5px;margin-top:23px;margin-bottom: 4px;height:36px;line-height: 36px;">蜻蜓商米导入模板下载</button>
                                    </a>
                                    <button class="layui-btn import" style="border-radius:5px;margin-top:23px;margin-bottom: 4px;height:36px;line-height: 36px;">导入设备</button>
                                    <button class="layui-btn adddevice" style="border-radius:5px;margin-top:23px;margin-bottom: 4px;height:36px;line-height: 36px;">添加设备</button>
                                </div>

                                <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>

                                <!-- 判断状态 -->
                                <script type="text/html" id="statusTap">
                                {{#  if(d.pay_status == 1){ }}
                                <span class="cur">{{ d.pay_status_desc }}</span>
                                {{#  } else { }}
                                {{ d.pay_status_desc }}
                                {{#  } }}
                                </script>
                                <!-- 判断状态 -->

                                <script type="text/html" id="table-content-list" class="layui-btn-small">
                                    <a class="layui-btn layui-btn-xs" lay-event="edit" style="margin-right:20px;border-radius:3.5px;background-color: #FFB800;">设备修改</a>
                                    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del" style="margin-right:3.5px;border-radius:3.5px;">设备删除</a>
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="open_import" class="hide" style="display: none;background-color: #fff;">
    <div class="xgrate">上传文件</div>
    <div class="layui-card-body" style="padding: 15px;">
        <div class="layui-form">
            <!-- <a href="<?php echo e(url('/student-add.xlsx')); ?>">模板下载</a> -->
            <div class="layui-form-item" style="display: inline-block;">
                <form id= "uploadForm" style="display: inline-block;padding-left:50px;">
                    <div class="layui-btn name" style="margin-right:20px;margin-top:20px;border-radius:5px">选择所需上传文件</div>
                    <input type="file" name="file" id="file" style="display: none;"/>
                    <input type="button" style="border-radius:5px;margin-top:20px;" value="确定上传" id="Upload" class="layui-btn"/>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="open_button" class="hide" style="display: none;background-color: #fff;">
    <div class="xgrate">设备修改</div>
    <div class="layui-card-body" style="height:250px;">
        <div class="layui-form" style="margin-top: 20px;">
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">设备号:</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="请输入设备号" class="layui-input item1">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">设备类型:</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="请输入设备类型" class="layui-input item2">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">请求地址:</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="请输入请求地址" class="layui-input item3">
                </div>
            </div>
            
                
                
                    
                
            
            
                
                
                    
                
            
            
                
                
                    
                
            
            
                
                
                    
                
            
            
                
                
                    
                
            
            
                
                
                    
                
            
            
                
                
                    
                
            
            
                
                
                    
                
            

            <button class="layui-btn devicesave" style="margin-left: 200px;margin-bottom: 4px;height:36px;line-height: 36px;">保存</button>
        </div>
    </div>
</div>

<div id="add_device" class="hide" style="display: none;background-color: #fff;">
    <div class="xgrate">添加设备</div>
    <div class="layui-card-body" style="padding: 15px;overflow-y: auto;">
        <div class="layui-form">
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">设备号:</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="请输入设备号" class="layui-input items1">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="text-align:center">设备类型:</label>
                <div class="layui-input-block">
                    <select name="microBizType" id="microBizType" lay-filter="">
                        <option value="MICRO_TYPE_STORE">请选择设备类型</option>
                        <option value="face_TZH-L1">face_TZH-L1</option>
                        <option value="s_bf_qr68">s_bf_qr68</option>
                        <option value="face_f4">face_f4</option>
                        <option value="face_pro">face_pro</option>
                        <option value="AECR F8">AECR F8</option>
                        <option value="c_01">c_01</option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item"> 
                <label class="layui-form-label" style="text-align:center">请求地址:</label>
                <div class="layui-input-block">
                    <input type="text" placeholder="请输入请求地址" class="layui-input items3">
                </div>
            </div>

            <button class="layui-btn adddevicesave" style="margin-left: 200px;margin-bottom: 4px;height:36px;line-height: 36px;">保存</button>
        </div>
    </div>
</div>

<input type="hidden" class="store_id">
<input type="hidden" class="sort">
<input type="hidden" class="stu_class_no">

<input type="hidden" class="stu_order_batch_no">
<input type="hidden" class="user_id">

<input type="hidden" class="pay_status">
<input type="hidden" class="pay_type">

<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var str=location.search;
    var user_id=str.split('?')[1];

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate'], function(){
        var $ = layui.$
        ,admin = layui.admin
        ,form = layui.form
        ,table = layui.table
        ,laydate = layui.laydate;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="<?php echo e(url('/user/login')); ?>";
            }
        });

        // 渲染表格
        table.render({
            elem: '#test-table-page'
            ,url: "<?php echo e(url('/api/device/device_oem_lists')); ?>"
            ,method: 'post'
            ,where:{
                token:token
            }
            ,request:{
                pageName: 'p',
                limitName: 'l'
            }
            ,page: true
            ,cellMinWidth: 150
            ,cols: [[
                //{field:'id', title: '序号'}
                //,{field:'user_id', title: 'user_id'}
                {field:'device_id', title: '设备号'}
                ,{field:'device_type', title: '设备类型'}
                ,{field:'store_name',  title: '绑定门店'}
                ,{field:'Request', title: '请求地址'}
                //,{field:'created_at', title: '创建时间'}
                ,{field:'updated_at', title: '导入时间',width:250}
                ,{width:200,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
            ]]
            ,response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                ,statusCode: 1 //成功的状态码，默认：0
                ,msgName: 'message' //状态信息的字段名称，默认：msg
                ,countName: 't' //数据总数的字段名称，默认：count
                ,dataName: 'data' //数据列表的字段名称，默认：data
            }
            ,done: function(res, curr, count){
            console.log(res);
            $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});	//进行表头样式设置
            }
        });

        table.on('tool(test-table-page)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var e = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
//            console.log(e);
            // sessionStorage.setItem('s_store_id', e.store_id);

            if(layEvent === 'del'){ //审核
                layer.confirm('确认删除此设备?',{icon: 2}, function(index){
                $.post("<?php echo e(url('/api/device/device_oem_del')); ?>",
                {
                    token:token
                    ,device_id:e.device_id
                    ,device_type:e.device_type
                },function(res){
                //              console.log(res);
                    if(res.status==1){
                        obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                        layer.close(index);
                        layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 2000
                        });
                    }else{
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 3000
                        });
                    }
                },"json");

                });
            }else if(layEvent === 'edit'){
                $.post("<?php echo e(url('/api/device/device_oem_info')); ?>",
                {
                token:token,
                device_id:e.device_id,
                device_type:e.device_type
                },function(res){
                    //          console.log(res);
                    if(res.status==1){
                        $('.item1').val(res.data.device_id);
                        $('.item2').val(res.data.device_type);
                        $('.item3').val(res.data.Request);
                        $('.item4').val(res.data.ScanPay);
                        $('.item5').val(res.data.QrPay);
                        $('.item6').val(res.data.QrAuthPay);
                        $('.item7').val(res.data.PayWays);
                        $('.item8').val(res.data.OrderQuery);
                        $('.item9').val(res.data.Order);
                        $('.item10').val(res.data.OrderList);
                        $('.item11').val(res.data.Refund);
                    }else{
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 3000
                        });
                    }
                },"json");

                layer.open({
                    type: 1,
                    title: false,
                    closeBtn: 0,
                    area: '516px',
                    skin: 'layui-layer-nobg', //没有背景色
                    shadeClose: true,
                    content: $('#open_button')
                });
            }
        });

        $('.devicesave').click(function(){
            $.post("<?php echo e(url('/api/device/device_oem_up')); ?>",
            {
                token:token
                ,device_id:$('.item1').val()
                ,device_type:$('.item2').val()
                ,Request:$('.item3').val()
                ,ScanPay:$('.item4').val()
                ,QrPay:$('.item5').val()
                ,QrAuthPay:$('.item6').val()
                ,PayWays:$('.item7').val()
                ,OrderQuery:$('.item8').val()
                ,Order:$('.item9').val()
                ,OrderList:$('.item10').val()
                ,Refund:$('.item11').val()
            },function(res){
    //            console.log(res);
                if(res.status==1){
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 2000
                    },function(){
                        window.location.reload();
                    });
                }else{
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 3000
                    });
                }
            },"json");
        });

        $('.adddevice').click(function(){
            layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area: '516px',
                skin: 'layui-layer-nobg', //没有背景色
                shadeClose: true,
                content: $('#add_device')
            });
        });

        $('.adddevicesave').click(function(){
            $.post("<?php echo e(url('/api/device/device_oem_add')); ?>",
            {
                token:token
                ,device_id:$('.items1').val()
                ,device_type:$('#microBizType').val()
                ,Request:$('.items3').val()
            },function(res){
//            console.log(res);
                if(res.status==1){
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 2000
                    },function(){
                        window.location.reload();
                    });
                }else{
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 3000
                    });
                }
            },"json");
        });

        //监听搜索
        form.on('submit(LAY-app-contlist-search)', function(data){
            var obj = data.field;
            // console.log(obj)
            var device_id = data.field.deviceid;
//            console.log(data);
            //执行重载
            table.reload('test-table-page', {
                where: {
                    device_id: device_id
                }
                ,page: {
                    l: 1 //重新从第 1 页开始
                }
            });
        });

        $('.import').click(function(){
            layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area: '516px',
                skin: 'layui-layer-nobg', //没有背景色
                shadeClose: true,
                content: $('#open_import')
            });
        });

        // 导入商户
        $('.name').click(function(){
            console.log('00');
            $("#file").click();
        });

        // 获取文件名
        var file = $('#file');
        file.on('change', function( e ){
        //e.currentTarget.files 是一个数组，如果支持多个文件，则需要遍历
            var name = e.currentTarget.files[0].name;
//            console.log( name );
            $('.name').html(name);
        });

        // excel文件导入
        $('#Upload').click(function(){
            var formData = new FormData($( "#uploadForm" )[0]);
//            console.log(formData);
            $.ajax({
                url: "<?php echo e(url('/api/device/device_oem_import?token=')); ?>"+token,
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (res) {
//                    console.log(res);
                    if(res.status==1){
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 3000
                        });
                    }else{
                        layer.alert(res.message, {icon: 2});
                    }
                },
                error: function (res) {
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 3000
                    });
                }
            });
        });

    });

  </script>

</body>
</html>
