<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>添加返手续费</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
<link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
<style>
    .layui-card-header{width:80px;text-align: right;float:left;}
    .layui-card-body{margin-left:28px;}
    .layui-upload-img{width: 92px; height: 92px; margin: 0 10px 10px 0;}
    .up{position: relative;display: inline-block;cursor: pointer;border-color: #1ab394; color: #FFF;width: 92px !important;font-size: 10px !important;text-align: center !important;}
    .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
    .layui-upload-list{width: 100px;height:96px;overflow: hidden;}
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {-webkit-appearance: none !important;margin: 0;}
    .width{
      width:60%;float:left;
    }
    p{
      float:left;line-height: 36px;margin-left:10px;
    }
    .userbox,.storebox{
      height:200px;
      overflow-y: auto;
      z-index: 999;
      position: absolute;
      left: 0px;
      top: 38px;
      width:298px;
      background-color:#ffffff;
      border: 1px solid #ddd;
    }
    .userbox .list,.storebox .list{
      height:38px;line-height: 38px;cursor:pointer;
      padding-left:10px;
    }
    .userbox .list:hover,.storebox .list:hover{
      background-color:#eeeeee;
    }
    .s_id{
      line-height: 36px;
    }
</style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card" style="margin-top:0px">
        <div class="layui-card-header" style="width:auto !important">添加返手续费&nbsp;&nbsp;&nbsp;<span class="zong_school_name"></span></div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item" style="width:500px;">        
                  <label class="layui-form-label">门店名称</label>                  
                  <div class="layui-input-block">
                    <input type="text" placeholder="请输入门店名称" class="layui-input inputstore">
                    <div class="storebox" style='display: none'></div>
                  </div>
                </div>
                <div class="layui-form-item" style="width:500px;">        
                  <label class="layui-form-label">门&nbsp;&nbsp;店&nbsp;&nbsp;ID</label>                  
                  <div class="layui-input-block">
                  <input type="text" placeholder="请输入门店ID" autocomplete="off" class="layui-input item1">
                      <div class="s_id"></div>
                  </div>
                </div>
                <div class="layui-form-item school" style="width:500px;">
                  <label class="layui-form-label">通道类型</label>
                  <div class="layui-input-block">
                      <select name="store" id="store" lay-filter="store">
                      </select>
                  </div>
                </div>  
            </div>
        </div>
        <div class="layui-card-header" style="width:auto !important;">活动条件&nbsp;&nbsp;&nbsp;<span class="zong_school_name"></span></div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item">
                  <label class="layui-form-label">活动时间</label>
                  <div class="layui-input-block">
                      <div class="layui-form" style="display: inline-block;">                      
                        <div class="layui-form-item">                          
                          <div class="layui-inline">                            
                            <div class="layui-input-inline">
                              <input type="text" class="layui-input start-item test-item" placeholder="活动开始时间" lay-key="23">
                            </div>
                          </div>
                          -
                          <div class="layui-inline" style='margin-left:10px;'>
                            <div class="layui-input-inline">
                              <input type="text" class="layui-input end-item test-item" placeholder="活动结束时间" lay-key="24">
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
                <div class="layui-form-item">
                  <label class="layui-form-label">金额条件</label>
                  <div class="layui-input-block" style="float:left;margin-left:0px;">
                      <div class="layui-form-item">                          
                        <div class="layui-inline">                            
                          <div class="layui-input-inline">
                            <input type="text" class="layui-input tjs" placeholder="">
                          </div>
                        </div>
                        -
                        <div class="layui-inline" style='margin-left:10px;'>
                          <div class="layui-input-inline">
                            <input type="text" class="layui-input tje" placeholder="">
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
            </div>
        </div>
        <div class="layui-card-header" style="width:auto !important;">返还比例&nbsp;&nbsp;&nbsp;<span class="zong_school_name"></span></div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item" style="width:430px;">
                  <label class="layui-form-label">支&ensp;付&ensp;宝</label>
                  <div class="layui-input-block">
                      <input type="number" placeholder="" class="layui-input width alipayf"><p>%<p>
                  </div>
                </div>
                <div class="layui-form-item" style="width:430px;">
                  <label class="layui-form-label">微信支付</label>
                  <div class="layui-input-block">
                      <input type="number" placeholder="" class="layui-input width weixinf"><p>%<p>
                  </div>
                </div>
                <div class="layui-form-item">
                  <div class="layui-input-block" style="color:#a2a2a2;">
                    备注:100%代表全返
                  </div>
                </div>
                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit site-demo-active" style="border-radius:5px;" data-type="tabChange">确定提交</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="store_id">
<input type="hidden" class="company">

<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script> 
<script>
    var token = sessionStorage.getItem("Usertoken");
    // var str=location.search;
    // var school_name=str.split('?')[1];

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','element','laydate'], function(){
        var $ = layui.$ 
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;

        var src=$('#demo1').attr('src');
        element.render();
        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="<?php echo e(url('/user/login')); ?>";
            }
        });
        // 门店
        $(".inputstore").bind("input propertychange",function(event){
//         console.log($(this).val());
        store_name = $(this).val()
            if (store_name.length == 0) {
                $('.storebox').html('')
                $('.storebox').hide()
            } else {
                $.post("<?php echo e(url('/api/user/store_lists')); ?>",
                {
                    token:token
                    ,store_name:$(this).val()
                    ,l:100
                },function(res){
                    // console.log(res);
                    var html="";
                    // console.log(res.t);
                    if(res.t==0){
                        $('.storebox').html('')
                    }else{
                        for(var i=0;i<res.data.length;i++){
                            html+='<div class="list" data='+res.data[i].store_id+'>'+res.data[i].store_name+'</div>'
                        }
                        $(".storebox").show();
                        $('.storebox').html('');
                        $('.storebox').append(html);
                    }
                },"json");
            }
        });

        $(".storebox").on("click",".list",function(){
  
          $('.inputstore').val($(this).html())
          $('.store_id').val($(this).attr('data'))
          $('.storebox').hide()
          $('.s_id').html($(this).attr('data'))


          // 选择门店
          $.ajax({
              url : "<?php echo e(url('/api/user/store_open_pay_way_lists')); ?>",
              data : {token:token,l:100,store_id:$('.store_id').val()},
              type : 'post',
              dataType:'json',
              success : function(data) {
//                  console.log(data);
                  var optionStr = "";
                  for(var i=0;i<data.data.length;i++){
                      optionStr += "<option value='" + data.data[i].company + "'>"
                        + data.data[i].company_desc + "</option>";
                  }
                  $("#store").html('');
                  $("#store").append('<option value="">选择通道</option>'+optionStr);
                  layui.form.render('select');
              },
              error : function(data) {
                  alert('查找板块报错');
              }
          });

        });

        form.on('select(store)', function(data){
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.company').val(category);
            // $('.storename').val(categoryName);
        });

        // 时间++++++++++++++++++++++++++++++++++++++++++++++++
        laydate.render({
            elem: '.start-item'
            ,type: 'datetime'
            ,trigger: 'click'
            ,done: function(value){
            }
        });

        laydate.render({
            elem: '.end-item'
            ,type: 'datetime'
            ,trigger: 'click'
            ,done: function(value){
            }
        });

        $(".alipayf").bind("input propertychange",function(event){
//          console.log($(this).val());
            if($(".alipayf").val()>100){
                layer.msg("返还比例不得超过100%", {
                    offset: '50px'
                    ,icon: 8
                    ,time: 3000
                });
            }
        });

        $(".weixinf").bind("input propertychange",function(event){
//          console.log($(this).val());
            if($(".weixinf").val()>100){
                layer.msg("返还比例不得超过100%", {
                    offset: '50px'
                    ,icon: 8
                    ,time: 3000
                });
            }
        });

        $('.submit').on('click', function(){
            $.post("<?php echo e(url('/api/user/add_activity_store_rate')); ?>",
            {
                token:token,
                store_id:$('.store_id').val(),
                time_start:$('.start-item').val(),
                time_end:$('.end-item').val(),
                total_amount_s:$('.tjs').val(),
                total_amount_e:$('.tje').val(),
                alipay:$('.alipayf').val(),
                weixin:$('.weixinf').val(),
                company:$('.company').val()
            },function(res){
//                console.log(res);
                if(res.status==1){
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 3000
                    });
                }else{
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 3000
                    });
                }
            },"json");
        });

    });

</script>

</body>
</html>
