<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>交易流水</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .tongbu {
            background-color: #4c9ef8;
            color: #fff;
            border-radius: 3.5px
        }

        .cur {
            color: #21c4f5;
        }

        .userbox, .storebox {
            height: 190px;
            margin-right: 0;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 63px;
            width: 298px;
            background-color: #ffffff;
            border: 1px solid #ddd;
        }

        .userbox .list, .storebox .list {
            height: 38px;
            line-height: 38px;
            cursor: pointer;
            padding-left: 10px;
        }

        .userbox .list:hover, .storebox .list:hover {
            background-color: #eeeeee;
        }

        .yname {
            font-size: 13px;
            color: #444;
            margin-bottom: 8px;
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12" style="margin-top:0px">
            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12">
                        <div class="layui-card">
                            <div class="layui-card-header">交易流水列表</div>
                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">
                                    <!-- 选择业务员 -->
                                    <!-- <div class="layui-form" lay-filter="component-form-group" style="width:190px;margin-right:0;display: block;">
                                      <div class="layui-form-item">
                                        <div class="layui-input-block" style="margin-left:5px;border-radius:5px">
                                            <select name="agent" id="agent" lay-filter="agent" lay-search>
                                            </select>
                                        </div>
                                      </div>
                                    </div> -->
                                    <div style="font-size:14px">
                                        <div class="layui-form" lay-filter="component-form-group"
                                             style="width:190px;margin-right:0;display: inline-block;">
                                            <div class="layui-form-item">
                                                <div class="layui-input-block"
                                                     style="margin-left:5px;border-radius:5px">
                                                    <text class="yname">代理商名称</text>
                                                    <input type="text" style="border-radius:5px"
                                                           tyle="border-radius:5px" name="schoolname"
                                                           lay-verify="schoolname" autocomplete="off"
                                                           placeholder="请输入代理商名称" class="layui-input transfer">
                                                    <div class="userbox" style='display: none'></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 学校 -->
                                        <!-- <div class="layui-form" lay-filter="component-form-group" style="width:190px;margin-right:0;display: inline-block;">
                                          <div class="layui-form-item">
                                            <div class="layui-input-block" style="margin-left:5px;border-radius:5px">
                                                <select name="schooltype" id="schooltype" lay-filter="schooltype" lay-search>
                                                </select>
                                            </div>
                                          </div>
                                        </div> -->
                                        <div class="layui-form" lay-filter="component-form-group"
                                             style="width:190px;margin-right:0;display: inline-block;">
                                            <div class="layui-form-item">
                                                <div class="layui-input-block"
                                                     style="margin-left:5px;border-radius:5px">
                                                    <text class="yname">门店名称</text>
                                                    <input type="text" style="border-radius:5px" name="schoolname"
                                                           lay-verify="schoolname" autocomplete="off"
                                                           placeholder="请输入门店名称" class="layui-input inputstore">
                                                    <div class="storebox" style='display: none'></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 支付状态 -->
                                        <div class="layui-form" lay-filter="component-form-group"
                                             style="width:190px;margin-right:0;display: inline-block;">
                                            <div class="layui-form-item">
                                                <div class="layui-input-block"
                                                     style="margin-left:5px;border-radius:5px">
                                                    <text class="yname">支付状态</text>
                                                    <select name="status" id="status" lay-filter="status">
                                                        <option value="">全部</option>
                                                        <option value="1">成功</option>
                                                        <option value="2">等待支付</option>
                                                        <option value="3">失败</option>
                                                        <option value="4">关闭</option>
                                                        <option value="5">退款中</option>
                                                        <option value="6">已退款</option>
                                                        <option value="7">有退款</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 支付类型 -->
                                        <div class="layui-form" lay-filter="component-form-group"
                                             style="width:190px;margin-right:0;display: inline-block;">
                                            <div class="layui-form-item">
                                                <div class="layui-input-block"
                                                     style="margin-left:5px;border-radius:5px">
                                                    <text class="yname">支付类型</text>
                                                    <select name="type" id="type" lay-filter="type">
                                                        <option value="">全部</option>
                                                        <option value="alipay">支付宝</option>
                                                        <option value="weixin">微信</option>
                                                        <option value="alipay_face">支付宝刷脸</option>
                                                        <option value="weixin_face">微信刷脸</option>
                                                        <option value="jd">京东</option>
                                                        <option value="unionpay">银联刷卡</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 排序 -->
                                        <div class="layui-form" lay-filter="component-form-group"
                                             style="width:190px;margin-right:0;display: inline-block;">
                                            <div class="layui-form-item">
                                                <div class="layui-input-block"
                                                     style="margin-left:5px;border-radius:5px">
                                                    <text class="yname">选择排序</text>
                                                    <select name="grade" id="grade" lay-filter="grade">
                                                        <option value="">选择排序</option>
                                                        <option value="desc">金额从大到小</option>
                                                        <option value="asc">金额从小到大</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 缴费时间 -->
                                        <div class="layui-form" style="display: flex;">
                                            <!-- 通道类型 -->
                                            <div class="layui-form" lay-filter="component-form-group"
                                                 style="width:190px;margin-right:0;display: inline-block;">
                                                <div class="layui-form-item">
                                                    <div class="layui-input-block"
                                                         style="margin-left:5px;border-radius:5px">
                                                        <text class="yname">选择通道</text>
                                                        <select name="passway" id="passway" lay-filter="passway">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="layui-form-item" style="margin-left:10px;">
                                                <div class="layui-inline">
                                                    <div class="layui-input-inline" style="width:175px;margin-right:0;">
                                                        <text class="yname">订单开始时间</text>
                                                        <input type="text" style="border-radius:5px"
                                                               class="layui-input start-item test-item"
                                                               placeholder="订单开始时间" lay-key="23">
                                                    </div>
                                                </div>
                                                <div class="layui-inline">
                                                    <div class="layui-input-inline" style="width:175px;margin-right:0;">
                                                        <text class="yname">订单结束时间</text>
                                                        <input type="text" style="border-radius:5px"
                                                               class="layui-input end-item test-item"
                                                               placeholder="订单结束时间" lay-key="24">
                                                    </div>
                                                </div>
                                                <div class="layui-inline" style='margin-right:0'>
                                                    <div class="layui-input-inline" style="width:190px;margin-right:0;">
                                                        <text class="yname">订单金额</text>
                                                        <input type="text" style="border-radius:5px"
                                                               class="layui-input amount_start" placeholder="订单金额">
                                                    </div>
                                                </div>
                                                -
                                                <div class="layui-inline">
                                                    <div class="layui-input-inline" style="width:190px;margin-right:0">
                                                        <text class="yname">订单金额</text>
                                                        <input type="text" style="border-radius:5px"
                                                               class="layui-input amount_end" placeholder="订单金额">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- 搜索 -->
                                    <!--<div class="layui-form" lay-filter="component-form-group" style="width:800px;display: inline-block;">-->
                                    <!--    <div class="layui-form-item" style="margin-left:5px;">-->
                                    <!--        <div class="layui-inline" style='margin-right:0'>-->
                                    <!--            <div class="layui-input-inline" style="width:185px">-->
                                    <!--                <text class="yname">设备ID</text>-->
                                    <!--                <input type="text" style="border-radius:5px" name="deviceid" placeholder="请输入设备ID号" autocomplete="off" class="layui-input">-->
                                    <!--            </div>-->
                                    <!--        </div>-->
                                    <!--        <div class="layui-inline" style='margin-right:0'>-->
                                    <!--            <div class="layui-input-inline" style="width:185px">-->
                                    <!--                <text class="yname">订单号</text>-->
                                    <!--                <input type="text" style="border-radius:5px" name="tradeno" placeholder="请输入订单号" autocomplete="off" class="layui-input">-->
                                    <!--            </div>-->
                                    <!--        </div>-->
                                    <!--        <div class="layui-inline" style='margin-right:0'>-->
                                    <!--            <div class="layui-input-inline" style="width:185px">-->
                                    <!--                <text class="yname">条码单号</text>-->
                                    <!--                <input type="text" style="border-radius:5px" name="paytradeno" placeholder="支付条码单号" autocomplete="off" class="layui-input">-->
                                    <!--            </div>-->
                                    <!--        </div>-->
                                    <!--        <div class="layui-inline">-->
                                    <!--            <button class="layui-btn layuiadmin-btn-list" lay-submit="" lay-filter="LAY-app-contlist-search" style="border-radius:5px;margin-top:20px;margin-bottom: 0;height:36px;line-height: 36px;">-->
                                    <!--                <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>-->
                                    <!--            </button>-->
                                    <!--          </div>-->
                                    <!--        <button class="layui-btn export" style="border-radius:5px;margin-top:20px;margin-bottom: 4px;height:36px;line-height: 36px;">导出</button>-->
                                    <!--    </div>-->
                                    <!--</div>-->
                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:100%;display: flex;">
                                        <div class="layui-form" lay-filter="component-form-group"
                                             style="width:190px;margin-right:10px;display: inline-block;">
                                            <div class="layui-form-item">
                                                <div class="layui-input-block" style="margin-left:0;border-radius:5px">
                                                    <text class="yname">设备类型</text>
                                                    <select name="device_type" id="device_type"
                                                            lay-filter="device_type">
                                                        <option value="">全部</option>
                                                        <option value="收银系统">收银系统</option>
                                                        <option value="码牌">码牌</option>
                                                        <option value="收钱啦内插">收钱啦内插</option>
                                                        <option value="微收银外插">微收银外插</option>
                                                        <option value="微信小程序">微信小程序</option>
                                                        <option value="如意设备">如意设备</option>
                                                        <option value="刷脸设备">刷脸设备</option>
                                                        <option value="支付宝蜻蜓">支付宝蜻蜓</option>
                                                        <option value="播报设备(畅立收Q038)">播报设备(畅立收Q038)</option>
                                                        <option value="百富扫码王(qr68)">百富扫码王(qr68)</option>
                                                        <option value="学生缴费">学生缴费</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layui-form-item" style="margin-left:5px;">
                                            <div class="layui-inline" style='margin-right:0'>
                                                <div class="layui-input-inline" style="width:185px">
                                                    <text class="yname">设备SN</text>

                                                    <input type="text" style="border-radius:5px" name="deviceid"
                                                           placeholder="请输入设备SN" autocomplete="off"
                                                           class="layui-input">
                                                </div>
                                            </div>
                                            <div class="layui-inline" style='margin-right:0'>
                                                <div class="layui-input-inline" style="width:185px">
                                                    <text class="yname">订单号</text>
                                                    <input type="text" style="border-radius:5px" name="tradeno"
                                                           placeholder="请输入订单号" autocomplete="off" class="layui-input">
                                                </div>
                                            </div>
                                            <div class="layui-inline" style='margin-right:0'>
                                                <div class="layui-input-inline" style="width:185px">
                                                    <text class="yname">条码单号</text>
                                                    <input type="text" style="border-radius:5px" name="paytradeno"
                                                           placeholder="支付条码单号" autocomplete="off" class="layui-input">
                                                </div>
                                            </div>
                                            <div class="layui-inline">
                                                <button class="layui-btn layuiadmin-btn-list" lay-submit=""
                                                        lay-filter="LAY-app-contlist-search"
                                                        style="border-radius:5px;margin-top:20px;margin-bottom: 0;height:36px;line-height: 36px;">
                                                    <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                                </button>
                                            </div>
                                            <button class="layui-btn export"
                                                    style="border-radius:5px;margin-top:20px;margin-bottom: 4px;height:36px;line-height: 36px;">
                                                导出
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>

                                <!-- 判断状态 -->
                                <script type="text/html" id="statusTap">
                                    {{#  if(d.pay_status == 1){ }}
                                    <span class="cur">{{ d.pay_status_desc }}</span>
                                    {{#  } else { }}
                                    {{ d.pay_status_desc }}
                                    {{#  } }}
                                </script>

                                <!-- 判断实收金额 -->
                                <script type="text/html" id="moneyTap">
                                    {{#  if(d.receipt_amount != ""){ }}
                                    {{  d.receipt_amount  }}
                                    {{#  } else { }}
                                    {{  d.total_amount - d.mdiscount_amount  }}
                                    {{#  } }}
                                </script>

                                <script type="text/html" id="paymoney">
                                    {{ d.rate }}%
                                </script>

                                <!-- 通道类型 -->
                                <script type="text/html" id="company_type">
                                    {{#  if(d.company=='member'){ }}
                                    会员卡
                                    {{#  } else if(d.company=='vbilla') { }}
                                    随行付A
                                    {{#  } else if(d.company=='vbill') { }}
                                    随行付
                                    {{#  } else if(d.company=='alipay') { }}
                                    支付宝
                                    {{#  } else if(d.company=='weixin') { }}
                                    微信
                                    {{#  } else if(d.company=='mybank') { }}
                                    快钱支付
                                    {{#  } else if(d.company=='herongtong') { }}
                                    和融通
                                    {{#  } else if(d.company=='newland') { }}
                                    新大陆
                                    {{#  } else if(d.company=='fuiou') { }}
                                    富友
                                    {{#  } else if(d.company=='jdjr') { }}
                                    京东聚合
                                    {{#  } else if(d.company=='dlb') { }}
                                    哆啦宝
                                    {{#  } else if(d.company=='zft') { }}
                                    花呗分期
                                    {{#  } else if(d.company=='tfpay') { }}
                                    TF通道
                                    {{#  } else if(d.company=='hkrt') { }}
                                    海科融通
                                    {{#  } else if(d.company=='easypay') { }}
                                    易生
                                    {{#  } else if(d.company=='hltx') { }}
                                    葫芦天下
                                    {{#  } else if(d.company=='linkage') { }}
                                    联动优势
                                    {{#  } else if(d.company=='lianfu') { }}
                                    工行
                                    {{#  } else if(d.company=='changsha') { }}
                                    长沙银行
                                    {{#  } else if(d.company=='lianfuyouzheng') { }}
                                    邮政
                                    {{#  } else if(d.company=='wftpay') { }}
                                    威富通
                                    {{#  } else if(d.company=='hwcpay') { }}
                                    汇旺财
                                    {{#  } else if(d.company=='weixina') { }}
                                    微信a
                                    {{#  } else if(d.company=='yinsheng') { }}
                                    银盛
                                    {{#  } else if(d.company=='qfpay') { }}
                                    钱方
                                    {{#  } else if(d.company=='postpay') { }}
                                    邮驿付
                                    {{#  } else if(d.company=='ccbankpay') { }}
                                    建设银行
                                    {{#  } else if(d.company=='easyskpay') { }}
                                    易生数科
                                    {{#  } else { }}
                                    {{ d.company }}
                                    {{#  } }}
                                </script>

                                <script type="text/html" id="table-content-list" class="layui-btn-small">
                                    <a class="layui-btn layui-btn-normal layui-btn-xs tongbu"
                                       lay-event="tongbu">同步状态</a>
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="store_id">
<input type="hidden" class="sort">
<input type="hidden" class="stu_class_no">

<input type="hidden" class="stu_order_batch_no">
<input type="hidden" class="user_id">

<input type="hidden" class="pay_status">
<input type="hidden" class="pay_type">

<input type="hidden" class="paycode">
<input type="hidden" class="company_id">

<input type="hidden" class="sbid">
<input type="hidden" class="sbname">
<input type="hidden" class="danhao">
<input type="hidden" class="tiaoma">

<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var str = location.search;
    var store_id = str.split('?')[1];


    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'table', 'laydate'], function () {
        var $ = layui.$
            , admin = layui.admin
            , form = layui.form
            , table = layui.table
            , laydate = layui.laydate;

        $('.store_id').val(store_id);

        // 未登录,跳转登录页面
        $(document).ready(function () {
            if (token == null) {
                window.location.href = "<?php echo e(url('/user/login')); ?>";
            }
        });

        var s_storename = sessionStorage.getItem('s_storename');

        if (store_id == undefined) {

        } else {
            $('.inputstore').val(s_storename);
        }

        // 选择门店
        $.ajax({
            url: "<?php echo e(url('/api/user/store_lists')); ?>",
            data: {token: token, l: 100},
            type: 'post',
            success: function (data) {
                //console.log(data);
                var optionStr = "";
                for (var i = 0; i < data.data.length; i++) {
                    optionStr += "<option value='" + data.data[i].store_id + "' " + ((store_id == data.data[i].store_id) ? "selected" : "") + ">" + data.data[i].store_name + "</option>";
                }
                $("#schooltype").append('<option value="">选择门店</option>' + optionStr);
                layui.form.render('select');
            },
            error: function (data) {
                alert('查找板块报错');
            }
        });

        // 选择通道
        $.ajax({
            url: "<?php echo e(url('/api/user/store_open_pay_way_lists')); ?>",
            data: {token: token, l: 100},
            type: 'post',
            dataType: 'json',
            success: function (data) {
//            console.log(data);
                var optionStr = "";
                for (var i = 0; i < data.data.length; i++) {
                    optionStr += "<option value='" + data.data[i].company + "'>"
                        + data.data[i].company_desc + "</option>";
                }
                $("#passway").append('<option value="">选择通道类型</option>' + optionStr);
                layui.form.render('select');
            },
            error: function (data) {
                alert('查找板块报错');
            }
        });

        $(".transfer").bind("input propertychange", function (event) {
            //         console.log($(this).val());
            user_name = $(this).val();
            if (user_name.length == 0) {
                $('.userbox').html('');
                $('.userbox').hide();
            } else {
                $.post("<?php echo e(url('/api/user/get_sub_users')); ?>",
                    {
                        token: token
                        , user_name: $(this).val()
                        , self: '1'
                    }, function (res) {
                        var html = "";

                        if (res.t == 0) {
                            $('.userbox').html('');
                        } else {
                            for (var i = 0; i < res.data.length; i++) {
                                html += '<div class="list" data=' + res.data[i].id + '>' + res.data[i].name + '-' + res.data[i].level_name + '</div>'
                            }
                            $(".userbox").show();
                            $('.userbox').html('');
                            $('.userbox').append(html);
                        }
                    }, "json");
            }
        });

        $(".userbox").on("click", ".list", function () {
            $('.transfer').val($(this).html());
            $('.user_id').val($(this).attr('data'));
            $('.userbox').hide();

            table.reload('test-table-page', {
                where: {
                    user_id: $(this).attr('data'),
                    store_id: $('store_id').val()
                }
                , page: {
                    curr: 1
                }
            });
        });

        $(".inputstore").bind("input propertychange", function (event) {
            store_name = $(this).val();
            if (store_name.length == 0) {
                $('.storebox').html('');
                $('.storebox').hide();
            } else {
                $.post("<?php echo e(url('/api/user/store_lists')); ?>",
                    {
                        token: token
                        , store_name: $(this).val()
                        , l: 100
                    }, function (res) {
                        var html = "";
                        if (res.t == 0) {
                            $('.storebox').html('');
                        } else {
                            for (var i = 0; i < res.data.length; i++) {
                                html += '<div class="list" data=' + res.data[i].store_id + '>' + res.data[i].store_name + '</div>'
                            }
                            $(".storebox").show();
                            $('.storebox').html('');
                            $('.storebox').append(html);
                        }
                    }, "json");
            }
        });

        $(".storebox").on("click", ".list", function () {
            $('.inputstore').val($(this).html());
            $('.store_id').val($(this).attr('data'));
            $('.storebox').hide();

            table.reload('test-table-page', {
                where: {
                    user_id: $('user_id').val(),
                    store_id: $(this).attr('data')
                }
                , page: {
                    curr: 1
                }
            });

            $("#passway").html('');

            // 选择通道
            $.ajax({
                url: "<?php echo e(url('/api/user/store_open_pay_way_lists')); ?>",
                data: {
                    token: token
                    , l: 100
                    , store_id: $('.store_id').val()
                },
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    var optionStr = "";
                    for (var i = 0; i < data.data.length; i++) {
                        optionStr += "<option value='" + data.data[i].company + "'>" + data.data[i].company_desc + "</option>";
                    }
                    $("#passway").append('<option value="">选择通道类型</option>' + optionStr);
                    layui.form.render('select');
                },
                error: function (data) {
                    alert('查找板块报错');

                }
            });
        });

        // 渲染表格
        table.render({
            elem: '#test-table-page'
            , url: "<?php echo e(url('/api/user/order')); ?>"
            , method: 'post'
            , where: {
                token: token
                , store_id: store_id
            }
            , request: {
                pageName: 'p'
                , limitName: 'l'
            }
            , page: true
            , cellMinWidth: 100
            , cols: [[
                {field: 'created_at', align: 'center', title: '交易时间', width: 160}
                // ,{field:'pay_time',  title: '支付时间',width:160}
                , {field: 'device_name', align: 'center', title: '设备类型'}
                , {field: 'device_id', align: 'center', title: '设备SN'}
                , {field: 'store_name', align: 'center', title: '门店', width: 160}
                , {field: 'company', align: 'center', title: '通道类型', toolbar: '#company_type'}
                , {field: 'ways_source_desc', align: 'center', title: '支付方式'}
                // ,{field:'shop_price', title: '订单金额'}
                , {field: 'total_amount', align: 'center', title: '交易金额'}
                , {field: 'receipt_amount', align: 'center', title: '实收金额', templet: '#moneyTap'}
                , {field: 'refund_amount', align: 'center', title: '退款金额'}
                , {field: 'rate', align: 'center', title: '费率', templet: '#paymoney'}
                , {field: 'pay_status_desc', align: 'center', title: '状态', templet: '#statusTap'}
                , {field: 'mdiscount_amount', align: 'center', title: '优惠金额', templet: '#jfdkTap'}
                , {field: 'out_trade_no', align: 'center', title: '外部订单号', width: 260}
                , {field: 'trade_no', align: 'center', title: '原订单号', width: 150}
                // ,{field:'remark',  title: '备注'}
                , {width: 100, align: 'center', fixed: 'right', toolbar: '#table-content-list', title: '操作'}
            ]]
            , response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                , statusCode: 1 //成功的状态码，默认：0
                , msgName: 'message' //状态信息的字段名称，默认：msg
                , countName: 't' //数据总数的字段名称，默认：count
                , dataName: 'data' //数据列表的字段名称，默认：data
            }
            , done: function (res, curr, count) {
                console.log(res);
                $('th').css({
                    'font-weight': 'bold',
                    'font-size': '15',
                    'color': 'black',
                    'background': 'linear-gradient(#f2f2f2,#cfcfcf)'
                });	//进行表头样式设置
            }
        });

        table.on('tool(test-table-page)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var e = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            //          console.log(e);
            // sessionStorage.setItem('s_store_id', e.store_id);

            if (layEvent === 'tongbu') { //同步
                $.post("<?php echo e(url('/api/basequery/update_order')); ?>",
                    {
                        token: token
                        , store_id: e.store_id
                        , out_trade_no: e.out_trade_no
                    }, function (res) {
                        console.log(res)
                        if (res.status == 1) {
                            layer.msg(res.message, {offset: '50px', icon: 1, time: 2000});
                            table.reload('test-table-page');
                        } else {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 2
                                , time: 2000
                            });
                        }
                    }, "json");
            }

            var data = obj.data;
            if (obj.event === 'setSign') {
                layer.open({
                    type: 2,
                    title: '模板详细',
                    shade: false,
                    maxmin: true,
                    area: ['60%', '70%'],
                    content: "<?php echo e(url('/merchantpc/paydetail?')); ?>" + e.stu_order_type_no
                });
            }
        });

        // 选择门店
        form.on('select(schooltype)', function (data) {
            var store_id = data.value;
            $('.store_id').val(store_id);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    store_id: store_id
                    , user_id: $('.user_id').val()
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });

        // 选择排序
        form.on('select(grade)', function (data) {
            var sort = data.value;
            $('.sort').val(sort);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    sort: sort
                }
                , page: {
                    curr: 1//重新从第 1 页开始
                }
            });
        });

        // 选择业务员
        form.on('select(agent)', function (data) {
            var user_id = data.value;
            $('.user_id').val(user_id);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    user_id: $(".user_id").val()
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });

            // 选择门店
            $.ajax({
                url: "<?php echo e(url('/api/user/store_lists')); ?>",
                data: {
                    token: token
                    , user_id: user_id
                    , l: 100
                },
                type: 'post',
                success: function (data) {
                    var optionStr = "";
                    for (var i = 0; i < data.data.length; i++) {
                        optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
                    }
                    $("#schooltype").html('');
                    $("#schooltype").append('<option value="">选择门店</option>' + optionStr);
                    layui.form.render('select');
                },
                error: function (data) {
                    alert('查找板块报错');
                }
            });
        });

        // 选择状态
        form.on('select(status)', function (data) {
            var pay_status = data.value;
            $('.pay_status').val(pay_status);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    pay_status: pay_status
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });

        // 选择支付类型
        form.on('select(type)', function (data) {
            var pay_type = data.value;
            $('.pay_type').val(pay_type);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    ways_source: pay_type
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });

        // 选择设备类型
        form.on('select(device_type)', function (data) {
            var device_type = data.value;
            $('.sbname').val(device_type);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    device_name: device_type
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });

        // 选择通道类型
        form.on('select(passway)', function (data) {
            var company_id = data.value;
            $('.company_id').val(company_id);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    company: company_id
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });

        laydate.render({
            elem: '.start-item'
            , type: 'datetime'
            , trigger: 'click'
            , done: function (value) {
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: value
                        , time_end: $('.end-item').val()
                    }
                    , page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        laydate.render({
            elem: '.end-item'
            , type: 'datetime'
            , trigger: 'click'
            , done: function (value) {
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: $('.start-item').val()
                        , time_end: value
                    }
                    , page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        //开始金额
        $('.amount_start').bind("input propertychange", function (event) {
            var amount_start = parseInt($(this).val());
            var amount_end = parseInt($('.amount_end').val());

            if (amount_start < amount_end) { //开始金额
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        amount_start: $(this).val()
                        , amount_end: $('.amount_end').val()
                    }
                    , page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            } else if ($('.amount_end').val() == '') { //结束金额

            } else {
                // layer.msg('开始金额不能大于结束金额')
            }
        });

        //结束金额
        $('.amount_end').bind("input propertychange", function (event) {
            var amount_start = parseInt($('.amount_start').val());
            var amount_end = parseInt($(this).val());
            if (amount_start < amount_end) { //开始金额
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        amount_start: $('.amount_start').val()
                        , amount_end: $(this).val()
                    }
                    , page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            } else if ($('.amount_start').val() == '') {//结束金额

            } else {//结束金额
                // layer.msg('开始金额不能大于结束金额')
            }
        });

        form.on('submit(LAY-app-contlist-search)', function (data) {
            var paytradeno = data.field.paytradeno;  //条码单号
            var out_trade_no = data.field.tradeno; //订单号
            var device_id = data.field.deviceid;

            $('.danhao').val(out_trade_no);
            $('.tiaoma').val(paytradeno);
            $('.sbid').val(device_id);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    out_trade_no: out_trade_no
                    , trade_no: paytradeno
                    , device_id: device_id
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });

        $('.export').click(function () {
            var store_id = $('.store_id').val();
            var user_id = $('.user_id').val();
            var sort = $('.sort').val();
            var pay_status = $('.pay_status').val();
            var ways_source = $('.pay_type').val();
            var company = $('.company_id').val();
            var time_start = $('.start-item').val();
            var time_end = $('.end-item').val();
            var device_id = $('.sbid').val();
            var out_trade_no = $('.danhao').val();
            var trade_no = $('.tiaoma').val();
            var amount_start = $('.amount_start').val();
            var amount_end = $('.amount_end').val();
            var device_name = $('.sbname').val();

            window.location.href = "<?php echo e(url('/api/export/UserOrderExcelDown')); ?>" + "?token=" + token + "&store_id=" + store_id + "&user_id=" + user_id + "&sort=" + sort + "&pay_status=" + pay_status + "&ways_source=" + ways_source + "&company=" + company + "&time_start=" + time_start + "&time_end=" + time_end + "&out_trade_no=" + out_trade_no + "&trade_no=" + trade_no + "&amount_start=" + amount_start + "&amount_end=" + amount_end + "&device_id=" + device_id + "&device_name=" + device_name;
        })

    });

</script>

</body>
</html>
