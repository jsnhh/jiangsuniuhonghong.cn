<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>修改apk</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/formSelects-v4.css')); ?>" media="all">
    <style>
        .layui-card-header{width:80px;text-align: right;float:left;}
        .layui-card-body{margin-left:28px;}
        /*.layui-upload-img{width: 92px; height: 92px; margin: 0 10px 10px 0;}*/
        .up{position: relative;display: inline-block;cursor: pointer;border-color: #1ab394; color: #FFF;width: 92px !important;font-size: 10px !important;text-align: center !important;}
        .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
        .layui-upload-list{overflow: hidden;}
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {-webkit-appearance: none !important;margin: 0;}
        .layui-upload{width:100%;}
        #demo1{width:100%;}
        .layui-card{box-shadow:none;}
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card" style="margin-top:0px">
        <div class="layui-card-header">修改apk</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">







                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center">类型</label>
                    <div class="layui-input-block type">
                        <select name="type" id="type" lay-filter="type">
                            <option value="">选择设备类型</option>
                            <option value="face_TZH-L1">青蛙L1</option>
                            <option value="s_bf_qr68">百富qr68</option>
                            <option value="face_f4">蜻蜓F4</option>
                            <option value="face_pro">青蛙Pro</option>
                            <option value="face_sdk">青蛙Sdk</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-card-header" style="text-align:center">apk</div>
                    <div class="layui-card">
                        <div class="layui-card-body" style="display: inline-block;padding:0;margin-left:0;width:391px;">
                            <input type="text" placeholder="上传apk" class="layui-input layui-upload-img" id="demo1">
                        </div>
                        <button class="layui-btn up" style="border-radius:5px;margin-left:10px;margin-top:-5px"><input type="file" name="apk_upload" class="test1">上传apk</button>
                    </div>
                </div>
                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center">version</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入版本" class="layui-input version">
                    </div>
                </div>
                <div class="layui-form-item" style="width:500px">
                    <label class="layui-form-label" style="text-align:center">描&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;述</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolcode" lay-verify="schoolcode" autocomplete="off" placeholder="请输入描述" class="layui-input msg">
                    </div>
                </div>
                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit site-demo-active"style="border-radius:5px" data-type="tabChange">修改</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="user_id" value="">
<input type="hidden" class="device">


<div id="BOX" style="display:none">
</div>

<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var str=location.search;
    var id=str.split('?')[1];

    var type="<?php echo e($_GET['type']); ?>";

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'table','form','upload'], function(){
        var $ = layui.$;
        admin = layui.admin
            ,table = layui.table
            ,element = layui.element
            ,upload = layui.upload
            ,form = layui.form;

        element.render();

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="<?php echo e(url('/user/login')); ?>";
            }
        });


        $("#type").val(type);
        form.render('select', 'type');
        if(type=='face_TZH-L1'){
            $('.type .layui-select-title input').val('青蛙L1')
        }else if(type=='s_bf_qr68'){
            $('.type .layui-select-title input').val('百富qr68')
        } else if(type=='face_f4'){
            $('.type .layui-select-title input').val('蜻蜓F4')
        }else if(type=='face_pro'){
            $('.type .layui-select-title input').val('青蛙Pro')
        }else if(type=='face_sdk'){
            $('.type .layui-select-title input').val('青蛙Sdk')
        }else {
            $('.type .layui-select-title input').val('未知类型')
        }
        $('.device').val(type);

        form.on('select(type)', function(data){
            device = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            // console.log(category);
            $('.device').val(device);
            // $('.agentname').val(categoryName);
        });

        //加载页面参数
        $.post("<?php echo e(url('/api/apk/apkInfo')); ?>",
            {
                token:token,
                id:id
            },function(res){
//            console.log(res);
                $('.device').val(res.data.type);
                $('.user_id').val(res.data.id);
                $('.version').val(res.data.version);
                $('.msg').val(res.data.msg);
                $('#demo1').val(res.data.UpdateUrl);
            },"json");

        //提交
        $('.submit').on('click', function(){
            $.post("<?php echo e(url('/api/apk/apkUp')); ?>",
                {
                    token:token,
                    id:id,
                    type:$('.device').val(),
                    update_url:$('#demo1').val(),
                    version:$('.version').val(),
                    msg:$('.msg').val()
                },function(res){
//                console.log(res);
                    if(res.status==1){
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 3000
                        });
                    }else{
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 3000
                        });
                    }
                },"json");
        });

        var uploadInst = upload.render({
            url : "<?php echo e(url('/api/apk/apkUpload')); ?>",  //提交到的地址 可以自定义其他参数
            elem : '.test1',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            accept : 'file',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            exts : 'zip|rar|7z|txt|apk',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            // size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
//                console.log(res);
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    // layui.jquery('#demo1').attr("src", res.data.img_url);
                    $('#demo1').val(res.data.apk_url);
                }
                //console.log(res); //上传成功返回值，必须为json格式
            }
        });

    });
</script>

</body>
</html>
