<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>子商户会员卡列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .cur {
            color: #009688;
        }

        .del {
            background-color: #e85052;
        }

        /*.laytable-cell-1-school_icon{height:100%;}*/
        .yname {
            font-size: 13px;
            color: #444;
        }

        #code {
            width: 190px;
            margin-right: 10px;
            height: 190px;
            margin-right: 10px;
            margin: 20px auto;
        }

        #code canvas {
            width: 100%;
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">

            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12" style="margin-top:0px">
                        <div class="layui-card">
                            <div class="layui-card-header">会员卡列表</div>

                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">
                                    <div style="font-size:14px">

                                        <div class="layui-form" lay-filter="component-form-group"
                                             style="width:190px;margin-right:0;display: inline-block;">
                                            <div class="layui-form-item">

                                                <div class="layui-input-block"
                                                     style="margin-left:5px;border-radius:5px">
                                                    <text class="yname">门店名称</text>
                                                    <input type="text" style="border-radius:5px" name="schoolname"
                                                           lay-verify="schoolname" autocomplete="off"
                                                           placeholder="请输入门店名称" class="layui-input inputstore">
                                                    <div class="storebox" style='display: none'></div>
                                                </div>
                                                <div class="layui-form-item">
                                                    <div class="layui-input-block"
                                                         style="margin-left:5px;border-radius:5px">
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                <script type="text/html" id="table-content-list">
                                    <a class="layui-btn layui-btn-normal layui-btn-xs memberCard"
                                       style="background-color:#ed9c3a" lay-event="memberCard">会员卡设置</a>
                                    {{#  if(d.verify_status =='1'){ }}
                                    <a class="layui-btn layui-btn-normal layui-btn-xs memberCard"
                                       style="background-color:darkgreen" lay-event="card_qr">二维码</a>
                                    {{#  }}}
                                    <a class="layui-btn layui-btn-normal layui-btn-xs del"
                                       style="background-color:#cf3e36" lay-event="del">删除</a>
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div id="card_qr" class="hide" style="display: none;background-color: #fff;">
    <div class="layui-tab layui-tab-card" style="margin:0px 0px;border-style:none">
        <p style="font-size: 20px;text-align: center;padding-top: 20px">门店会员卡二维码</p>
        <div class="layui-tab-content" style="height: 290px;">
            <div class="layui-tab-item layui-show">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <div style="display:flex;color: black;font-size: 15px;">
                            <div>门店名称：</div>
                            <div style="text-align: center;" class="store_name"></div>
                        </div>
                        <div id="code"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" class="user_id">
<input type="hidden" class="status">
<!-- 操作按钮 -->
<!--

<a class="layui-btn layui-btn-normal layui-btn-xs see" lay-event="detail">查看</a> -->
<script type="text/html" id="status">
    {{#  if(d.status =='1'){ }}
    启用
    {{#  } else { }}
    禁用
    {{#  } }}
</script>
<script type="text/html" id="verify_status">
    {{#  if(d.verify_status =='1'){ }}
    审核通过
    {{#  } else if(d.verify_status =='2'){ }}
    审核中
    {{#  } else { }}
    审核失败
    {{#  } }}
</script>
<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script src="<?php echo e(asset('/layuiadmin/layui/jquery-2.1.4.js')); ?>"></script>
<script src="<?php echo e(asset('/layuiadmin/layui/jquery.qrcode.min.js')); ?>"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'table', 'laydate'], function () {
        var $ = layui.$
            , admin = layui.admin
            , table = layui.table
            , form = layui.form
            , laydate = layui.laydate;
        // 未登录,跳转登录页面
        $(document).ready(function () {
            if (token == null) {
                window.location.href = "<?php echo e(url('/user/login')); ?>";
            }
        });
        $('.addSonStoreWeiXin').click(function () {
            sessionStorage.setItem('store_id', '0');
            $(this).attr('lay-href', "<?php echo e(url('/user/addSonStoreWeiXin')); ?>");
        });

        table.render({
            elem: '#test-table-page'
            , url: "<?php echo e(url('/api/member/sonStoreCardList')); ?>"
            , method: 'post'
            , where: {
                token: token,
            }
            , request: {
                pageName: 'p',
                limitName: 'l'
            }
            , page: true
            , cellMinWidth: 150
            , cols: [[
                {field: 'store_name', title: '门店名称'}
                , {field: 'merchant_id', title: '子商户id'}
                , {field: 'card_id', title: '会员卡id'}
                , {field: 'title', title: '会员卡标题'}
                , {field: 'status', title: '启用状态', toolbar: '#status'}
                , {field: 'verify_status', title: '审核状态', toolbar: '#verify_status'}
                , {field: 'quantity', title: '会员卡数量'}
                , {field: 'created_at', title: '创建时间'}
                , {width: 300, align: 'center', fixed: 'right', toolbar: '#table-content-list', title: '操作'}
            ]]
            , response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                , statusCode: 1 //成功的状态码，默认：0
                , msgName: 'message' //状态信息的字段名称，默认：msg
                , countName: 't' //数据总数的字段名称，默认：count
                , dataName: 'data' //数据列表的字段名称，默认：data
            }
            , done: function (res, curr, count) {
                $('th').css({
                    'font-weight': 'bold',
                    'font-size': '15',
                    'color': 'black',
                    'background': 'linear-gradient(#f2f2f2,#cfcfcf)'
                });	//进行表头样式设置
            }

        });


        table.on('tool(test-table-page)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var e = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            if (layEvent === 'memberCard') {
                sessionStorage.setItem('card_id', e.card_id);
                $('.memberCard').attr('lay-href', "<?php echo e(url('/user/memberCard')); ?>");
            } else if (layEvent === 'card_qr') {
                $('.store_name').html(e.store_name);
                $.post("<?php echo e(url('/api/member/getCardQr')); ?>",
                    {
                        token: token
                        , card_id: e.card_id
                    },
                    function (res) {
                        console.log(res);
                        if (res.status == 1) {
                            $('#code').html('');
                            $('#code').qrcode(res.url);
                            layer.open({
                                type: 1,
                                title: false,
                                closeBtn: 0,
                                area: '400px',
                                skin: 'layui-layer-nobg', //没有背景色
                                shadeClose: true,
                                content: $('#card_qr')
                            });
                        } else if (res.status == 2) {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 2
                                , time: 3000
                            });
                        }
                    }, "json");
            } else if (layEvent === 'del') {
                layer.confirm('确定删除吗？', function (index) {
                    $.post("<?php echo e(url('/api/member/delStoreCard')); ?>",
                        {
                            token: token
                            , store_id: e.store_id
                            , card_id: e.card_id
                        }, function (res) {
                            if (res.status == 1) {
                                layer.msg(res.message, {
                                    offset: '50px'
                                    , icon: 1
                                    , time: 1000
                                }, function () {
                                    window.location.reload();
                                });
                            } else {
                                layer.msg(res.message, {
                                    offset: '50px'
                                    , icon: 2
                                    , time: 2000
                                });
                            }
                        }, "json");
                });
            }

        });


        // 选择学校
        form.on('select(schooltype)', function (data) {
            var type = data.value;

            //执行重载
            table.reload('test-table-page', {
                where: {
                    type: type
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });


        //监听搜索
        form.on('submit(LAY-app-contlist-search)', function (data) {
            var value = data.field.id;
            //执行重载
            table.reload('test-table-page', {
                where: {
                    title: value
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });


    });


</script>

</body>
</html>