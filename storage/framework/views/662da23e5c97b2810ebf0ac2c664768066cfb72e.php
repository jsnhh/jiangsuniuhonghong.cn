<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>logo配置</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style>
        .img{width:130px;height:90px;overflow: hidden;}
        .img img{width:100%;height:100%;}
        .layui-layer-nobg{width: none !important;}
        /*.layui-layer-content{width:600px;height:550px;}*/
        .layui-card-header{width:80px;text-align: right;float:left;}
        .layui-card-body{margin-left:28px;}
        .layui-upload-img{width: 150px; height: 150px; /*margin: 0 10px 10px 0;*/}

        .up{position: relative;display: inline-block;cursor: pointer;border-color: #1ab394; color: #FFF;width: auto !important;font-size: 10px !important;text-align: center !important;}
        .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
        .layui-upload-list{width: 150px;height:150px;overflow: hidden;margin: 10px auto;}
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {-webkit-appearance: none !important;margin: 0;}
        .logosize{font-size: 10px;text-align: center;margin-top: 10px;}
    </style>
</head>
<body>

<div class="layui-fluid" style="margin-top: 0px;">
    <div class="layui-card" style="margin-top:26px;">
        <div class="layui-card-header">logo配置</div>
        <div class="layui-card-body layui-row layui-col-space10">
            <div class="layui-row layui-form">
                <div class="layui-card">
                    <div class="layui-card-body" style="margin-left:28px;padding:0 15px;float:left;">
                        <div class="layui-upload" style="display: inline-block;">
                            <button class="layui-btn up" style="border-radius:5px;margin-left:16px"><input type="file" name="img_upload" class="test1">刷脸小设备logo</button>
                            <div class="logosize">建义尺寸 64px * 64px</div>
                            <div class="layui-upload-list">
                                <img class="layui-upload-img" id="demo1">
                                <p id="demoText"></p>
                            </div>
                        </div>
                        <div class="layui-upload" style="display: inline-block;margin-left:30px;">
                            <button class="layui-btn up" style="border-radius:5px;margin-left:16px"><input type="file" name="img_upload" class="test2">刷脸登录页logo</button>
                            <div class="logosize">建义尺寸 1144px * 160px</div>
                            <div class="layui-upload-list">
                                <img class="layui-upload-img" id="demo2">
                                <p id="demoText"></p>
                            </div>
                        </div>
                        <div class="layui-upload" style="display: inline-block;margin-left:30px;">
                            <button class="layui-btn up" style="border-radius:5px;margin-left:16px"><input type="file" name="img_upload" class="test3">启动页背景图</button>
                            <div class="logosize">建义尺寸 800px * 1280px</div>
                            <div class="layui-upload-list">
                                <img class="layui-upload-img" id="demo3">
                                <p id="demoText"></p>
                            </div>
                        </div>
                        <div class="layui-upload" style="display: inline-block;margin-left:30px;">
                            <button class="layui-btn up" style="border-radius:5px;margin-left:16px"><input type="file" name="img_upload" class="test4">二维码图片</button>
                            <div class="logosize">建义尺寸 870px * 900px</div>
                            <div class="layui-upload-list">
                                <img class="layui-upload-img" id="demo4">
                                <p id="demoText"></p>
                            </div>
                        </div>
                        <div class="layui-upload" style="display: inline-block;margin-left:30px;">
                            <button class="layui-btn up" style="border-radius:5px;margin-left:30px"><input type="file" name="img_upload" class="test5">平台logo</button>
                            <div class="logosize">建义尺寸 200px * 60px</div>
                            <div class="layui-upload-list">
                                <img class="layui-upload-img" id="demo5">
                                <p id="demoText"></p>
                            </div>
                        </div>
                        <div class="layui-upload" style="display: inline-block;margin-left:30px;">
                            <button class="layui-btn up" style="border-radius:5px;margin-left:12px"><input type="file" name="img_upload" class="test6">支付宝默认广告</button>
                            <div class="logosize">建义尺寸 800px * 1280px</div>
                            <div class="layui-upload-list">
                                <img class="layui-upload-img" id="demo6">
                                <p id="demoText"></p>
                            </div>
                        </div>
                        <div class="layui-upload" style="display: inline-block;margin-left:30px;">
                            <button class="layui-btn up" style="border-radius:5px;margin-left:18px"><input type="file" name="img_upload" class="test7">微信默认广告</button>
                            <div class="logosize">建义尺寸 800px * 1280px</div>
                            <div class="layui-upload-list">
                                <img class="layui-upload-img" id="demo7">
                                <p id="demoText"></p>
                            </div>
                        </div>
                        <div class="layui-upload" style="display: inline-block;margin-left:30px;">
                            <button class="layui-btn up" style="border-radius:5px;margin-left:18px"><input type="file" name="img_upload" class="test8">网站图标</button>
                            <div class="logosize">建义尺寸 36px * 36px</div>
                            <div class="layui-upload-list">
                                <img class="layui-upload-img" id="demo8">
                                <p id="demoText"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item layui-layout-admin">
                <div class="layui-input-block">
                    <div class="layui-footer" style="left: 0;">
                        <button class="layui-btn submit site-demo-active" style="border-radius:5px" data-type="tabChange">保存</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var store_id = sessionStorage.getItem("store_store_id");

    var province_code = sessionStorage.getItem("store_province_code");
    var city_code = sessionStorage.getItem("store_city_code");
    var area_code = sessionStorage.getItem("store_area_code");

    var str=location.search;
    var store_id_add=str.split('?')[1];
    // console.log(store_id_add)

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form
            ,upload = layui.upload
            ,formSelects = layui.formSelects;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="<?php echo e(url('/user/login')); ?>";
            }
        });

        //加载logo
        $.post("<?php echo e(url('/api/basequery/app_logos_info')); ?>",
            {
                token:token
                ,type:'2'
            },function(res){
                if(res.status == 1){
                    $('#demo1').attr('src',res.data.face_logo);
                    $('#demo2').attr('src',res.data.login_logo);
                    $('#demo3').attr('src',res.data.start_bk_img);
                    $('#demo4').attr('src',res.data.qr_img);
                    $('#demo5').attr('src',res.data.ht_img);
                    $('#demo6').attr('src',res.data.ali_ad_default);
                    $('#demo7').attr('src',res.data.wechat_ad_default);
                    $('#demo8').attr('src',res.data.favicon_url);
                }
//            else {
//                layer.msg(res.message, {
//                    offset: '50px'
//                    ,icon: 2
//                    ,time: 3000
//                });
//            }
            }, "json");

        //上传图片 刷脸设备logo
        var uploadInst = upload.render({
            url : "<?php echo e(url('/api/basequery/webupload?act=images')); ?>"+'&token='+token+"&type="+"img", //提交到的地址 可以自定义其他参数
            elem : '.test1',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            ext : 'jpg|png|gif',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
//                console.log(res);
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    layui.jquery('#demo1').attr("src", res.data.img_url);
                }
                //console.log(res); //上传成功返回值，必须为json格式
            }
        });

        //上传图片 刷脸登录页logo
        var uploadInst = upload.render({
            url : "<?php echo e(url('/api/basequery/webupload?act=images')); ?>"+'&token='+token+"&type="+"img", //提交到的地址 可以自定义其他参数
            elem : '.test2',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            ext : 'jpg|png|gif',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
//                console.log(res);
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    layui.jquery('#demo2').attr("src", res.data.img_url);
                }
                //console.log(res); //上传成功返回值，必须为json格式
            }
        });

        //上传图片 启动页背景图
        var uploadInst = upload.render({
            url : "<?php echo e(url('/api/basequery/webupload?act=images')); ?>"+'&token='+token+"&type="+"img", //提交到的地址 可以自定义其他参数
            elem : '.test3',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            ext : 'jpg|png|gif',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
//                console.log(res);
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    layui.jquery('#demo3').attr("src", res.data.img_url);
                }
                //console.log(res); //上传成功返回值，必须为json格式
            }
        });

        //上传图片 二维码
        var uploadInst = upload.render({
            url : "<?php echo e(url('/api/basequery/webupload?act=images')); ?>"+'&token='+token+"&type="+"img", //提交到的地址 可以自定义其他参数
            elem : '.test4',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            ext : 'jpg|png|gif',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
//                console.log(res);
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    layui.jquery('#demo4').attr("src", res.data.img_url);

                    //新增或修改二维码
                    // 
                    // 
                    // 
                    // 
                    // 
                    // 
                    // 
                    // 
                    // 
                    // 
                    // 
                    // 
                    // 
                    // 
                    // 
                    // 
                    // 
                    // 
                    // 
                    // 
                }
            }
        });

        //上传图片 平台logo
        var uploadInst = upload.render({
            url : "<?php echo e(url('/api/basequery/webupload?act=images')); ?>"+'&token='+token+"&type="+"img", //提交到的地址 可以自定义其他参数
            elem : '.test5',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            ext : 'jpg|png|gif',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    layui.jquery('#demo5').attr("src", res.data.img_url);
                }
                //console.log(res); //上传成功返回值，必须为json格式
            }
        });

        //上传图片 支付宝默认广告
        var uploadInst = upload.render({
            url : "<?php echo e(url('/api/basequery/webupload?act=images')); ?>"+'&token='+token+"&type="+"img", //提交到的地址 可以自定义其他参数
            elem : '.test6',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            ext : 'jpg|png|gif',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    layui.jquery('#demo6').attr("src", res.data.img_url);
                }
                //console.log(res); //上传成功返回值，必须为json格式
            }
        });

        //上传图片 微信默认广告
        var uploadInst = upload.render({
            url : "<?php echo e(url('/api/basequery/webupload?act=images')); ?>"+'&token='+token+"&type="+"img", //提交到的地址 可以自定义其他参数
            elem : '.test7',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            ext : 'jpg|png|gif',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    layui.jquery('#demo7').attr("src", res.data.img_url);
                }
                //console.log(res); //上传成功返回值，必须为json格式
            }
        });

        //上传图片 网站图标
        var uploadInst = upload.render({
            url : "<?php echo e(url('/api/basequery/webupload?act=images')); ?>"+'&token='+token+"&type="+"img", //提交到的地址 可以自定义其他参数
            elem : '.test8',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            ext : 'jpg|png|gif',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    layui.jquery('#demo8').attr("src", res.data.img_url);
                }
                //console.log(res); //上传成功返回值，必须为json格式
            }
        });

        //提交事件
        $('.submit').click(function(){
            $.post("<?php echo e(url('/api/basequery/app_logos_info')); ?>",
                {
                    token:token
                    ,type:'1'
                    ,face_logo:$('#demo1').attr('src')
                    ,login_logo:$('#demo2').attr('src')
                    ,start_bk_img:$('#demo3').attr('src')
                    ,qr_img:$('#demo4').attr('src')
                    ,ht_img:$('#demo5').attr('src')
                    ,aliAdDefault:$('#demo6').attr('src')
                    ,weChatAdDefault:$('#demo7').attr('src')
                    ,faviconUrl:$('#demo8').attr('src')
                },function(res){
                    //console.log(res);
                    if(res.status==1){
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 3000
                        });
                    }else{
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 3000
                        });
                    }
                },"json");
        });

    });

</script>

</body>
</html>
