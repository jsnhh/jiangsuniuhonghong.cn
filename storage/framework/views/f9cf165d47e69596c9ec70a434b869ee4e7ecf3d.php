<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>微信代金券配置</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style>
        .up{position: relative;display: inline-block;cursor: pointer;background-color: #1ab394;border-color: #1ab394; color: #FFF;width: 100px !important;text-align: center !important; padding: 0px 8px !important;}
        .up input {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            opacity: .01;
            width: 100px;
            height: 32px;
        }
        .modal{top:25% !important;}
        .gohome{display: none;}
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card" style="margin-top:0px">
        <div class="layui-card-header">微信代金券配置</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                
                <div class="layui-form-item">
                    <label class="layui-form-label">微信服务商商户号</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">API-V3秘钥</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">商户API证书序列号</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">商户私钥文件</label>
                    <div class="layui-input-block" style="width: 70%;float: left;display: inline-block;margin-left: 0px;">
                        <input type="text" name="schoolshortname" lay-verify="schoolshortname" autocomplete="off" placeholder="" class="layui-input zs">
                    </div>
                    <div class="col-sm-1" style="float:right;">
                        <div class="btn-img" style="width:115px;">
                            <button class="layui-btn up"><input type="file" name="img_upload" class="test2">上传密钥文件</button>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">微信支付平台证书</label>
                    <div class="layui-input-block" style="width: 70%;float: left;display: inline-block;margin-left: 0px;">
                        <input type="text" id="certificate" name="schoolshortname" lay-verify="schoolshortname" autocomplete="off" placeholder="" class="layui-input my">
                    </div>
                    <div class="col-sm-1" style="float:right;">
                        <div class="btn-img" style="width:115px;">
                            <button class="layui-btn up"><input type="button" id="getCertificate"  name="img_upload2" class="test3">获取平台证书</button>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">设置核销回调地址</label>
                    <div class="layui-input-block" style="width: 70%;float: left;display: inline-block;margin-left: 0px;">
                        <input type="text" id="backUrl" name="schoolshortname" lay-verify="schoolshortname" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                    <div class="col-sm-1" style="float:right;">
                        <div class="btn-img" style="width:115px;">
                            <button class="layui-btn up"><input type="button" id="setUrl" class="test4">设置回调地址</button>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">appid</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">secret</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">可用通道</label>
                    <div class="layui-input-block">
                        <select name="channel" >
                            <option value="">请选择</option>
                            <option value="wechat">微信官方</option>
                            <option value="wechat_a">微信官方a</option>
                        </select>
                    </div>
                </div>

                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit site-demo-active" data-type="tabChange">保存</button>
                        </div>
                    </div>
                </div>
                <div id="edit_rate" class="hide" style="display: none;background-color:#fff;border-radius:10px;">
                    <div class="layui-card-body tankuang">
                        <div class="layui-form">
                            <div class="layui-form-item">
                                <label class="layui-form-label">是否保存</label>
                            </div>
                            <div class="layui-form-item" style="display: flex;">
                                <div class="layui-input-block">
                                    <div class="layui-footer">
                                        <button class="layui-btn submits yes" style="background-color: #1E9FFF;border-radius:5px;margin-left:-55px;">是</button>
                                    </div>
                                </div>
                                <div class="layui-input-block">
                                    <div class="layui-footer">
                                        <button class="layui-btn submits no" style="background-color: #FF5722;border-radius:5px;">否</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>

<script>
    var token = sessionStorage.getItem("Usertoken");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','element'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,form = layui.form
            ,upload = layui.upload;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="<?php echo e(url('/user/login')); ?>";
            }
        });
        $.post("<?php echo e(url('/api/user/wechat_cash_coupon_config')); ?>",
        {
            token:token
            ,type:'2'
        },function(res){
            $('.layui-form .layui-form-item').eq(0).find('input').val(res.data.wx_merchant_id);
            $('.layui-form .layui-form-item').eq(1).find('input').val(res.data.api_v3_secret);
            $('.layui-form .layui-form-item').eq(2).find('input').val(res.data.api_serial_no);
            $('.layui-form .layui-form-item').eq(3).find('input.zs').val(res.data.key_path);
            $('.layui-form .layui-form-item').eq(4).find('input.my').val(res.data.wechat_pay_certificate_key_path);
            $('.layui-form .layui-form-item').eq(5).find('input').val(res.data.notify_url);
            $('.layui-form .layui-form-item').eq(6).find('input').val(res.data.appid);
            $('.layui-form .layui-form-item').eq(7).find('input').val(res.data.secret);
            console.log(res.data.channel)
            if(res.data.channel == "wechat"){
                $('.layui-form .layui-form-item').eq(8).find('input').val("微信官方");
            }else if(res.data.channel == "wechat_a"){
                $('.layui-form .layui-form-item').eq(8).find('input').val("微信官方a");
            }else{
                $('.layui-form .layui-form-item').eq(8).find('input').val("");
            }
            
        },"json");

        // demo2
        upload.render({
            url : "<?php echo e(url('/api/basequery/webupload?act=file')); ?>"+'&token='+token+'&type='+'wxfile',  //提交到的地址 可以自定义其他参数
            elem : '.test2',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'file',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            accept : 'file',
            /*ext : 'jpg|png|gif',*/    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            before : function(obj){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                console.log(res);
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    layui.jquery('.zs').val(res.data.img_url);
                }
            }
        });

        $("#getCertificate").click(function () {
            $.post("<?php echo e(url('/api/customer/user/certificates')); ?>",
                {
                    token:token
                    ,wx_merchant_id:$('.layui-form .layui-form-item').eq(0).find('input').val()
                    ,api_v3_secret:$('.layui-form .layui-form-item').eq(1).find('input').val()
                    ,api_serial_no:$('.layui-form .layui-form-item').eq(2).find('input').val()
                    ,key_path:$('.layui-form .layui-form-item').eq(3).find('input').val()
                },function(res){
                    $('#certificate').val(res.data);
                },"json");
        })

        $("#setUrl").click(function () {
            $.post("<?php echo e(url('/api/customer/user/setNotifyUrl')); ?>",
                {
                    token:token
                    ,wx_merchant_id:$('.layui-form .layui-form-item').eq(0).find('input').val()
                    ,merchantSerialNumber:$('.layui-form .layui-form-item').eq(2).find('input').val()
                    ,merchantPrivateKey:$('.layui-form .layui-form-item').eq(3).find('input').val()
                    ,wechatpayCertificate:$('.layui-form .layui-form-item').eq(4).find('input').val()
                },function(res){
                    console.log(res)
                    $('#backUrl').val(res.data.notify_url);
                },"json");
        })

        $('.submit').on('click', function(){
            let datalistform = $('.layui-form .layui-form-item').eq(8).find('input').val();
            console.log(datalistform)
            var datalistformnew = "";
            if(datalistform == ""){
                layer.msg("请选择可用通道", {icon:2, shade:0.5, time:2000});
                return false;
            }else if(datalistform == "微信官方"){
                datalistformnew = "wechat"
            }else if(datalistform == "微信官方a"){
                datalistformnew = "wechat_a"
            }else{
                layer.msg("请选择可用通道", {icon:2, shade:0.5, time:2000});
                return false;
            }
            console.log(datalistformnew)
            layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
                area: '350px',
                skin: 'layui-layer-nobg',
                shadeClose: true,
                content: $('#edit_rate')
            });
            $('.yes').on('click',function(){
                $.post("<?php echo e(url('/api/user/save_wechat_cash_coupon_config')); ?>",
                    {
                        token:token
                        ,type:1
                        ,wx_merchant_id:$('.layui-form .layui-form-item').eq(0).find('input').val()
                        ,api_v3_secret:$('.layui-form .layui-form-item').eq(1).find('input').val()
                        ,api_serial_no:$('.layui-form .layui-form-item').eq(2).find('input').val()
                        ,key_path:$('.layui-form .layui-form-item').eq(3).find('input').val()
                        ,wechat_pay_certificate_key_path:$('.layui-form .layui-form-item').eq(4).find('input').val()
                        ,notify_url:$('.layui-form .layui-form-item').eq(5).find('input').val()
                        ,appid:$('.layui-form .layui-form-item').eq(6).find('input').val()
                        ,secret:$('.layui-form .layui-form-item').eq(7).find('input').val()
                        ,channel:datalistformnew
                    },function(res){
                        if(res.status==1){
                            layer.msg(res.msg, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 2000
                            },function(){
                                window.location.reload();
                            });
                        }else{
                            layer.alert(res.msg, {icon: 2});
                        }
                    },'json')
            });

            $('.no').on('click',function(){
                $('#layui-layer-shade1').css('opacity','0');
                $('#edit_rate').css('display', 'none');
            });
        });

        $('.submits').click(function(){
            $('#edit_rate').css('display','none');
        });

    });
</script>
</body>
</html>
