 <!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>赏金结算</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
  <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
  <style>
    .edit{background-color: #ed9c3a;}
    .shenhe{background-color: #429488;}    
    .see{background-color: #7cb717;} 
    .tongbu{background-color: #4c9ef8;color:#fff;}
    .cur{color:#009688;}

    .userbox,.storebox{
      height:200px;
      overflow-y: auto;
      z-index: 999;
      position: absolute;
      left: 0px;
      top: 38px;
      width:498px;
      background-color:#ffffff;
      border: 1px solid #ddd;
    }
    .userbox .list,.storebox .list{
      height:38px;line-height: 38px;cursor:pointer;
      padding-left:10px;
    }
    .userbox .list:hover,.storebox .list:hover{
      background-color:#eeeeee;
    }
    .s_id{
      line-height: 36px;
    }
  </style>
</head>
<body>

  <div class="layui-fluid" style="margin-top: 0px;">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">

        <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
              <div class="layui-card"> 
                <div class="layui-card-header">返佣导出</div>

                <div class="layui-card-body">
                  <div class="layui-btn-container" style="font-size:14px;">

                    <!-- 缴费时间 -->
                    <div class="layui-form" style="display: inline-block;">                      
                      <div class="layui-form-item">              
                        <label class="layui-form-label">选择时间</label>           
                        <div class="layui-inline">
                          
                          <div class="layui-input-inline">
                            <input type="text" class="layui-input start-item test-item" placeholder="开始时间" lay-key="23">
                          </div>
                        </div>
                        <div class="layui-inline">
                          <div class="layui-input-inline">
                            <input type="text" class="layui-input end-item test-item" placeholder="结束时间" lay-key="24">
                          </div>
                        </div>
                        
                      </div>
                    </div>
                    <div class="layui-form" lay-filter="component-form-group">
                      <div class="layui-form-item">   
                        <label class="layui-form-label" style="float:left;">服务商名称</label>                       
                        <div class="layui-input-block" style="width:500px;margin-left:0;float:left;">
                          <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入服务商名称" class="layui-input transfer">

                          <div class="userbox" style='display: none'></div>
                        </div>
                      </div>
                    </div>
                    <!-- 选择通道 -->
                    <div class="layui-form" lay-filter="component-form-group" style="">
                      <div class="layui-form-item">                       
                        <label class="layui-form-label" style="float:left;">通道</label>     
                        <div class="layui-input-block" style="margin-left:0;width:500px;float:left;">
                          <select name="schooltype" id="schooltype" lay-filter="schooltype" lay-search>
                              
                          </select>
                            
                        </div>
                      </div>
                    </div>
                    <div class="layui-form" lay-filter="component-form-group" style="">
                      <div class="layui-form-item">                       
                        <label class="layui-form-label" style="float:left;">支付类型</label>     
                        <div class="layui-input-block" style="margin-left:0;width:500px;float:left;">
                          <select name="type" id="type" lay-filter="type">
                            <option value="">选择支付类型</option>
                            <option value="alipay">支付宝</option>
                            <option value="weixin">微信</option>
                            <option value="alipay_face">支付宝刷脸</option>
                            <option value="weixin_face">微信刷脸</option>
                            <option value="member">会员支付</option>
                            <option value="jd">京东</option>
                            <option value="unionpay">银联刷卡</option>
                            <option value="unionpayqr">银联扫码</option>
                            <option value="unionpaysf">银联闪付</option>
                          </select>
                            
                        </div>
                      </div>
                    </div>
                    

                    
                    

                    <div class="layui-form-item">
                      <label class="layui-form-label">服务商成本</label>
                      <div class="layui-input-block"  style="margin-left:0;width:500px;float: left;line-height: 38px;">
                        <input type="text" placeholder="请输入成本" class="layui-input rate" style="width: 70%;float: left;margin-right: 10px;">例如：0.0021                        
                      </div>
                    </div>
                    <div class="layui-form-item">
                      <label class="layui-form-label">开票点</label>
                      <div class="layui-input-block"  style="margin-left:0;width:500px;float: left;line-height: 38px;">
                        <input type="text" placeholder="请输入开票点" class="layui-input piao" style="width: 70%;float: left;margin-right: 10px;">例如：0.06                       
                      </div>
                    </div>
                    <div class="layui-form-item">
                      <label class="layui-form-label">指定费率</label>
                      <div class="layui-input-block"  style="margin-left:0;width:500px;float: left;line-height: 38px;">
                        <input type="text" placeholder="默认不填" class="layui-input zdrate" style="width: 70%;float: left;margin-right: 10px;">例如：蓝海绿洲:0.200                     
                      </div>
                    </div>

                    

                    <div class="layui-form-item layui-layout-admin">
                      <div class="layui-input-block">
                          <div class="layui-footer" style="left: 0;">
                              <button class="layui-btn submit site-demo-active" data-type="tabChange">确认导出</button>
                          </div>
                      </div>
                    </div>

                  </div>
                  
                 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>


  
  <input type="hidden" class="daili_id">
  <input type="hidden" class="company">
  <input type="hidden" class="company_name">
  <input type="hidden" class="pay_type">



  <script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script> 
    <script>
    var token = sessionStorage.getItem("Usertoken");
    var str=location.search;
    var user_id=str.split('?')[1];

    
    layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate;
        // 未登录,跳转登录页面
        $(document).ready(function(){        
          if(token==null){
              window.location.href="<?php echo e(url('/user/login')); ?>"; 
          }
      
        })


        // 时间***********************************
       
        // 获取时间
        var nowdate = new Date();
        var year=nowdate.getFullYear();
        var mounth=nowdate.getMonth();
        var day=nowdate.getDate();
        var hour = nowdate.getHours();       
        var min = nowdate.getMinutes();     
        var sec = nowdate.getSeconds();
        

       
        function getDaysInOneMonth(year, month){
          month = parseInt(month, 10);
          var d= new Date(year, month, 0);
          return d.getDate();
        }
        var dayN=getDaysInOneMonth(year,mounth)
        console.log(dayN)

        var startdata = year+'-'+mounth+'-1 '+'00'+':'+'00'+':'+'00';
        var enddata = year+'-'+mounth+'-'+dayN+' '+'23'+':'+'59'+':'+'59';
        console.log(startdata,enddata)
        $('.start-item').val(startdata);
        $('.end-item').val(enddata);
        


       
        // 选择通道
        $.ajax({
          url : "<?php echo e(url('/api/user/store_open_pay_way_lists')); ?>",
          data : {token:token,l:100},
          type : 'post',
          dataType:'json',
          success : function(data) {
              console.log(data);
              var optionStr = "";
              for(var i=0;i<data.data.length;i++){
                  optionStr += "<option value='" + data.data[i].company + "'>"
                      + data.data[i].company_desc + "</option>";
              }    
              $("#schooltype").append('<option value="">选择通道类型</option>'+optionStr);
              layui.form.render('select');
          },
          error : function(data) {
              alert('查找板块报错');
          }
        });
 
        
    

        
        // 选择赏金来源
        form.on('select(schooltype)', function(data){
          var index = data.elem.selectedIndex;           
          var company = data.value;
          $('.company').val(company);
          $('.company_name').val(data.elem.options[index].text);
          
        });
        form.on('select(type)', function(data){
          var pay_type = data.value;
          $('.pay_type').val(pay_type);
          
        });
        
        
        
        
      
    // 时间++++++++++++++++++++++++++++++++++++++++++++++++
        laydate.render({
          elem: '.start-item'
          ,type: 'datetime'
          ,trigger: 'click'
          ,done: function(value){
           
          }
        });

        laydate.render({
          elem: '.end-item'
          ,type: 'datetime'
          ,trigger: 'click'
          ,done: function(value){
           
          }
        });



        $(".transfer").bind("input propertychange",function(event){
        //   console.log($(this).val())
        user_name = $(this).val()
            if (user_name.length == 0) {
                $('.userbox').html('')
                $('.userbox').hide()
            } else {
                $.post("<?php echo e(url('/api/user/get_sub_users')); ?>",
                {
                    token:token,
                    user_name:$(this).val(),
                    self:'1',
                    level:'1'         

                },function(res){
                    var html="";
                    if(res.t==0){
                        $('.userbox').html('')
                    }else{
                        for(var i=0;i<res.data.length;i++){
                            html+='<div class="list" data='+res.data[i].id+'>'+res.data[i].name+'-'+res.data[i].level_name+'</div>'
                        }
                        $(".userbox").show()
                        $('.userbox').html('')
                        $('.userbox').append(html)
                    }
                    
                },"json");
            }
          
        });

        $(".userbox").on("click",".list",function(){
  
          $('.transfer').val($(this).html())          
          $('.userbox').hide()
          $('.daili_id').val($(this).attr('data'))

        });

        //  $(".transfer").blur(function(){
//           $('.userbox').hide()
        // });



        $('.submit').click(function(){
          var time_start=$('.start-item').val();
          var time_end=$('.end-item').val();
          
          var user_id=$('.daili_id').val();
          var company=$('.company').val();
          if($('.daili_id').val()==''){
            layer.msg('请填写服务商名称', {
              offset: '50px'
              ,icon: 7
              ,time: 3000
            });
          }else if($('.company').val()==''){
            layer.msg('请选择通道', {
              offset: '50px'
              ,icon: 7
              ,time: 3000
            });
          }else if($('.rate').val()==''){
            layer.msg('请填写服务商成本', {
              offset: '50px'
              ,icon: 7
              ,time: 3000
            });
          }else if($('.piao').val()==''){
            layer.msg('请填写开票点', {
              offset: '50px'
              ,icon: 7
              ,time: 3000
            });
          }else{
            window.location.href="<?php echo e(url('/api/basequery/count_pt_data_excel')); ?>"+"?token="+token+"&time_start="+time_start+"&time_end="+time_end+"&user_id="+user_id+"&company="+company+"&company_name="+$('.company_name').val()+"&kp="+$('.piao').val()+"&rate="+$('.rate').val()+"&zd="+$('.zdrate').val()+"&ways_source="+$('.pay_type').val(); 
          }

          
          

        })

        
    });

  </script>

</body>
</html>





