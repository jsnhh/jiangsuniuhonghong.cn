<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>通道管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .floweredit {
            background-color: #ed9c3a;
        }

        .shua {
            background-color: #ed9c3a;
        }

        .sao {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .tongbu {
            background-color: #4c9ef8;
            color: #fff;
        }

        .cur {
            color: #009688;
        }

        .way {
            height: 38px;
            line-height: 38px;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
        }

        input[type="number"] {
            -moz-appearance: textfield;
        }

        .f_inputs {
            width: 80%;
            display: inline-block;
            margin-right: 10px;
        }

        .f_block {
            margin-left: 0;
            width: 300px;
            float: left;
        }

        .delpassway {
            background-color: #e85052;
        }

        .realName {
            background-color: #98E165;
        }

        .wx_list {
            display: none;
        }

        .lianfushow {
            display: none;
        }

        .returnSettings {
            display: block;
            width: 80px;
            height: 40px;
            line-height: 40px;
            margin-left: 35px;
        }

        .checkbox {
            width: 10px;
            height: 10px;
        }

        .xgrate {
            color: #fff;
            font-size: 15px;
            padding: 7px;
            height: 30px;
            line-height: 30px;
            background-color: #3475c3;
        }

        .buttons {
            display: flex;
            justify-content: space-around;
        }

        .sumbut_rz,
        .sumbut_cx,
        .sumbut_chexiao,
        .sumbut_result {
            height: 36px;
            line-height: 36px;
            border-radius: 5px;
            background-color: #3475c3;
            color: #fff;
            text-align: center;
            font-size: 14px;
            width: 100px;
        }

        .realName_way {
            line-height: 37px;
        }
        a{float: left;}
    </style>
</head>

<body>

    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-fluid">
                    <div class="layui-row layui-col-space15">
                        <div class="layui-col-md12">
                            <div class="layui-card" style="margin-top:0px">
                                <div class="layui-card-header">通道管理列表</div>
                                <div class="layui-card-body">
                                    <div class="layui-btn-container" style="font-size:14px;">
                                        <table class="layui-hide" id="test-table-page" lay-filter="test-table-page">
                                        
                                        </table>

                                        <!-- 判断状态 -->
                                        <script type="text/html" id="rate">
                                            {{ d.rate }}%
                                        </script>
                                        <!-- 判断状态 -->

                                        <script type="text/html" id="table-content-list" class="layui-btn-small" id="passway">
                                        	
                                        	
                                        	
                                            {{# if(d.ways_type == 8005 || d.ways_type == 6005){ }}
                                                <a class="layui-btn layui-btn-danger layui-btn-xs shua" lay-event="shua">修改费率</a>
                                            {{#  } else if(d.ways_type == 8004 || d.ways_type == 6004){ }}
                                                <a class="layui-btn layui-btn-danger layui-btn-xs sao" lay-event="sao">修改费率</a>
                                            {{#  } else if(d.ways_type == 16001) { }}
                                                <a class="layui-btn layui-btn-danger layui-btn-xs floweredit" lay-event="floweredit">修改费率</a>
                                            {{#  } else { }}
                                                <a class="layui-btn layui-btn-danger layui-btn-xs edit" lay-event="edit">修改费率</a>
                                            {{#  } }}

                                            {{#  if(d.company == "jdjr"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs pay" lay-event="pay">商户号</a>
                                            {{#  } else if(d.company == "alipay"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs alipay" lay-event="alipay">商户号</a>
                                            {{#  } else if(d.company == "newland"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs newland" lay-event="newland">商户号</a>
                                            {{#  } else if(d.company == "herongtong"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs hrt" lay-event="hrt">商户号</a>
                                            {{#  } else if(d.company == "ltf"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs ltf" lay-event="ltf">商户号</a>
                                            {{#  } else if(d.company == "dlb"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs dlb" lay-event="dlb">商户号</a>
                                            {{#  } else if(d.company == "tfpay"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs tf" lay-event="tf">商户号</a>
                                            {{#  } else if(d.company == "vbill"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs sxf" lay-event="sxf">商户号</a>
                                            {{#  } else if(d.company == "weixin"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs openweixin" lay-event="openweixin">商户号</a>
                                            {{#  } else if(d.company == "lianfu"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs lianfu" lay-event="lianfu">商户号</a>
                                            {{#  } else if(d.company == "hltx"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs hltx" lay-event="hltx">商户号</a>
                                            {{#  } else if(d.company == "changsha"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs changsha" lay-event="changsha">商户号</a>
                                            {{#  } else if(d.company == "lianfuyoupay"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs lianfuyoupay" lay-event="lianfuyoupay">商户号</a>
					                        {{#  } else if(d.company == "huipay"){ }}
                    				            <a class="layui-btn layui-btn-normal layui-btn-xs huipay" lay-event="huipay">商户号</a>
                                            {{#  } else if(d.company == "easypay"){ }}
                    				            <a class="layui-btn layui-btn-normal layui-btn-xs easypay" lay-event="easypay">商户号</a>
                                            {{#  } else if(d.company == "wftpay"){ }}
                    				            <a class="layui-btn layui-btn-normal layui-btn-xs wftpay" lay-event="wftpay">商户号</a>
                                            {{#  } else if(d.company == "hwcpay"){ }}
                    				            <a class="layui-btn layui-btn-normal layui-btn-xs hwcpay" lay-event="hwcpay">商户号</a>
                                            {{#  } else if(d.company == "weixina"){ }}
                                            <a class="layui-btn layui-btn-normal layui-btn-xs openweixina" lay-event="openweixina">商户号</a>
                                            {{#  } else if(d.company == "hkrt"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs hkrt" lay-event="hkrt">商户号</a>
                                            {{#  } else if(d.company == "yinsheng"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs yinsheng" lay-event="yinsheng">商户号</a>
                                            {{#  } else if(d.company == "qfpay"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs qfpay" lay-event="qfpay">商户号</a>
                                            {{#  } else { }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs open" lay-event="open">商户号</a>
                                            {{#  } }}
                                            
											
											{{#  if(d.company == "hkrt"){ }}
                                                <a class="layui-btn layui-btn-danger layui-btn-xs" id="adduserdatahkrt" lay-event="adduserdatahkrt">补充资料</a>
                                            {{#  } else if(d.company == "easypay"){ }}
												<a class="layui-btn layui-btn-danger layui-btn-xs" id="adduserdataeasypay" lay-event="adduserdataeasypay">补充资料</a>
                                             {{#  } else if(d.company == "dlb"){ }}
												<a class="layui-btn layui-btn-danger layui-btn-xs" id="adduserdatadlb" lay-event="adduserdatadlb">补充资料</a>
                                            {{#  } }}



                                            {{#  if(d.company == "easypay"){ }}
												<a class="layui-btn layui-btn-danger layui-btn-xs" id="easypaydataupdata" lay-event="easypaydataupdata">同步资料</a>
                                            {{#  } }}
                                            <!--<a class="layui-btn layui-btn-danger layui-btn-xs" id="adduserdata" lay-event="adduserdata">补充资料</a>--> 
											


                                            {{#  if(d.company == "mybank" || d.company == "jdjr" || d.company == "newland" || d.company == "herongtong" || d.company == "fuiou"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs js" lay-event="js">开通通道</a>
                                            {{#  } else if(d.company == "alipay"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs alipaycode" lay-event="alipaycode">开通通道</a>
                                            {{#  } else if(d.company == "tfpay"){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs js_tf" lay-event="js_tf">开通通道</a>
                                            {{#  } else if(d.company == "hltx"){ }}
                                            <a class="layui-btn layui-btn-normal layui-btn-xs js_hltx" lay-event="js_hltx">开通通道</a>
                                            {{#  } else { }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs js" lay-event="js">开通通道</a>
                                            {{#  } }}
                                            
                                            
                                            
                                            <a class="layui-btn layui-btn-normal layui-btn-xs delpassway" lay-event="delpassway">清除通道</a>
                                            <a class="layui-btn layui-btn-normal layui-btn-xs passwaydisable" lay-event="passwaydisable">通道禁用</a>

                                            <!-- {{#  if(d.ways_source == "weixin" && (d.company == "vbill" || d.company == "vbilla") ){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs realName" lay-event="realName">微信实名认证</a>
                                            {{#  } }} -->
                                            {{#  if(d.ways_source == "weixin" && (d.company == "vbill" || d.company == "vbilla") ){ }}
                                                <a class="layui-btn layui-btn-normal layui-btn-xs realName" lay-event="realName">微信实名认证</a>
                                            {{#  } }}

                                        </script>
                                    </div>
                                </div>
                                <a class="layui-btn layui-btn-normal layui-btn-xs sort" lay-event="sort" lay-href='' style="margin:20px;">修改收款顺序</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="edit_rate" class="hide" style="display: none;background-color: #fff;">
            <div class="xgrate">修改费率</div>
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block">
                            <div class="way"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">费率:</label>
                        <div class="layui-input-block">
                            <input type="number" placeholder="请输入修改费率" class="layui-input rate">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn submit">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="edit_shua" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">门店名称:</label>
                        <div class="layui-input-block" style="line-height: 36px;">
                            <div class="agent"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block" style="line-height: 36px;">
                            <div class="ways"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">贷记卡费率:</label>
                        <div class="layui-input-block">
                            <input type="number" placeholder="请输入贷记卡费率" class="layui-input rate1">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">借记卡费率:</label>
                        <div class="layui-input-block">
                            <input type="number" placeholder="请输入借记卡费率" class="layui-input rate2">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">借记卡封顶:</label>
                        <div class="layui-input-block">
                            <input type="number" placeholder="请输入借记卡封顶" class="layui-input rate3">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn submits">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" class="types">
        </div>

        <div id="open_passway" class="hide" style="display: none;background-color: #fff;">
            <div class="xgrate">输入商户号</div>
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block">
                            <div class="way"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">商户号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入商户号" class="layui-input wx_sub_merchant_id">
                        </div>
                    </div>
                    <div class="layui-form-item wx_list">
                        <label class="layui-form-label">微信支付目录:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入微信支付目录" class="layui-input wx_path">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn open_way">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="open_passway_lianfu" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block">
                            <div class="way"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">商户号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入商户号" class="layui-input lianfu_sub_merchant_id">
                        </div>
                    </div>
                    <div class="layui-form-item lianfushow">
                        <label class="layui-form-label">signkey:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入signkey" class="layui-input lianfukey">
                        </div>
                    </div>
                    <div class="layui-form-item wx_list">
                        <label class="layui-form-label">微信支付目录:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入微信支付目录" class="layui-input lianfu_wx_path">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn lianfuButton">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="open_passway_weixin" class="hide" style="display: none;background-color: #fff;">
            <form class="layui-form">
                <div class="layui-card-body">
                    <div class="layui-form" style="padding: 15px;height:750px">
                        <div class="layui-form-item">
                            <label class="layui-form-label">通道:</label>
                            <div class="layui-input-block">
                                <div class="way"></div>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">商户号:</label>
                            <div class="layui-input-block">
                                <input type="text" placeholder="请输入商户号" name="wx_sub_merchant_id_shop" class="layui-input wx_sub_merchant_id_shop">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">微信支付目录:</label>
                            <div class="layui-input-block">
                                <input type="text" placeholder="请输入微信支付目录" class="layui-input wx_path_shop">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">优惠商品ID:</label>
                            <div class="layui-input-block">
                                <input type="text" placeholder="请输入优惠商品ID" class="layui-input wx_shop">
                            </div>
                        </div>
                        <div class="layui-form-item" id="IsPurchased" style="display:none">
                            <label class="layui-form-label">开启分账:</label>
                            <div class="layui-input-block">
                                <input type="radio" name="wx_sharing_open" value="1" title="开启" lay-skin="sharing" lay-filter="sharing_open">
                                <input type="radio" name="wx_sharing_open" value="0" title="关闭" lay-skin="sharing" lay-filter="sharing_open" checked="">
                            </div>
                        </div>
                        <div class="layui-form-item weixin_share_list">
                            <label class="layui-form-label">分账接收方类型:</label>
                            <div class="layui-input-block" class="layui-input">
                                <select name="wx_sharing_type" class="wx_sharing_type" lay-filter="wx_sharing_type">
                                    <option value="MERCHANT_ID">商户ID</option>
                                    <option value="PERSONAL_OPENID">个人openid（由父商户APPID转换得到）</option>
                                    <option value="PERSONAL_SUB_OPENID">个人sub_openid（由子商户APPID转换得到）</option>
                                </select>
                            </div>
                        </div>
                        <div class="layui-form-item weixin_share_list">
                            <label class="layui-form-label">分账接收方帐号:</label>
                            <div class="layui-input-block">
                                <input type="text" placeholder="分账接收方帐号" class="layui-input wx_sharing_account">
                            </div>
                        </div>
                        <div class="layui-form-item weixin_share_list">
                            <label class="layui-form-label">分账接收方全称:</label>
                            <div class="layui-input-block">
                                <input type="text" placeholder="分账接收方全称" class="layui-input wx_sharing_name">
                            </div>
                        </div>
                        <div class="layui-form-item weixin_share_list">
                            <label class="layui-form-label">设置比例:</label>
                            <div class="layui-input-block">
                                <input type="text" placeholder="设置比例" class="layui-input wx_sharing_proportion">
                            </div>
                        </div>
                        <div class="layui-form-item weixin_share_list">
                            <label class="layui-form-label">与分账方的关系类型:</label>
                            <div class="layui-input-block">
                                <input type="checkbox" name="wx_sharing_relation_type" class="wx_sharing_relation_type wx_sharing_relation_type_STORE" as="STORE" value="STORE" title="门店">
                                <input type="checkbox" name="wx_sharing_relation_type" class="wx_sharing_relation_type wx_sharing_relation_type_STAFF" as="STAFF" value="STAFF" title="员工">
                                <input type="checkbox" name="wx_sharing_relation_type" class="wx_sharing_relation_type wx_sharing_relation_type_STORE_OWNER" as="STORE_OWNER" value="STORE_OWNER" title="店主">
                                <input type="checkbox" name="wx_sharing_relation_type" class="wx_sharing_relation_type wx_sharing_relation_type_USER" as="USER" value="USER" title="用户">
                                <input type="checkbox" name="wx_sharing_relation_type" class="wx_sharing_relation_type wx_sharing_relation_type_HEADQUARTER" as="HEADQUARTER" value="HEADQUARTER" title="总部">
                                <input type="checkbox" name="wx_sharing_relation_type" class="wx_sharing_relation_type wx_sharing_relation_type_SERVICE_PROVIDER" as="SERVICE_PROVIDER" value="SERVICE_PROVIDER" title="服务商">
                                <input type="checkbox" name="wx_sharing_relation_type" class="wx_sharing_relation_type wx_sharing_relation_type_BRAND" as="BRAND" value="BRAND" title="品牌方">
                                <input type="checkbox" name="wx_sharing_relation_type" class="wx_sharing_relation_type wx_sharing_relation_type_DISTRIBUTOR" as="DISTRIBUTOR" value="DISTRIBUTOR" title="分销商">
                                <input type="checkbox" name="wx_sharing_relation_type" class="wx_sharing_relation_type wx_sharing_relation_type_PARTNER" as="PARTNER" value="PARTNER" title="合作伙伴">
                                <input type="checkbox" name="wx_sharing_relation_type" class="wx_sharing_relation_type wx_sharing_relation_type_SUPPLIER" as="SUPPLIER" value="SUPPLIER" title="供应商">
                                
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <div class="layui-input-block">
                                <div class="layui-footer" style="left: 0;">
                                    <button type="submit" lay-submit="" lay-filter="open_way_weixin" class="layui-btn open_way_weixin">确定</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div id="open_passway_weixina" class="hide" style="display: none;background-color: #fff;">
            <form class="layui-form">
                <div class="layui-card-body">
                    <div class="layui-form" style="padding: 15px;height:750px">
                        <div class="layui-form-item">
                            <label class="layui-form-label">通道:</label>
                            <div class="layui-input-block">
                                <div class="way"></div>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">商户号:</label>
                            <div class="layui-input-block">
                                <input type="text" placeholder="请输入商户号" name="wxa_sub_merchant_id_shop" class="layui-input wxa_sub_merchant_id_shop">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">微信支付目录:</label>
                            <div class="layui-input-block">
                                <input type="text" placeholder="请输入微信支付目录" class="layui-input wxa_path_shop">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">优惠商品ID:</label>
                            <div class="layui-input-block">
                                <input type="text" placeholder="请输入优惠商品ID" class="layui-input wxa_shop">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <div class="layui-input-block">
                                <div class="layui-footer" style="left: 0;">
                                    <button type="submit" lay-submit="" lay-filter="open_way_weixina" class="layui-btn open_way_weixina">确定</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div id="open_tf_passway" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block">
                            <div class="way"></div>
                        </div>
                    </div>
                    <div class="layui-form-item tf_show">
                        <span class="layui-form-label">渠道子商户号:</span>
                        <span class="qd_item2" style='line-height: 36px;'>渠道子商户号:</span>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">商户号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入商户号" class="layui-input tf_merchant_id">
                        </div>
                    </div>
                    <div class="layui-form-item qdopen" style="margin:20px 0 30px 0;">
                        <label class="layui-form-label">渠道</label>
                        <div class="layui-input-block">
                            <select name="tf_qdopen" id="tf_qdopen" lay-filter="tf_qdopen">
                                <option value="">选择渠道</option>
                                <option value="1">渠道1</option>
                                <option value="2">渠道2</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item wx_list">
                        <label class="layui-form-label">微信支付目录:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入微信支付目录" class="layui-input wx_path_tf">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn save_tf">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="newland" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block">
                            <div class="way"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">商户号:</label>
                        <div class="layui-input-block">
                            <input type="number" placeholder="请输入商户号" class="layui-input wxmerchantda">
                        </div>
                    </div>
                    <div class="layui-form-item wx_list">
                        <label class="layui-form-label">微信子公众号id:</label>
                        <div class="layui-input-block">
                            <input type="number" placeholder="请输入微信子公众号id" class="layui-input wxsubappid">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">商户密钥:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入商户密钥" class="layui-input wxmerchantdb">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">设备号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入设备号" class="layui-input wxmerchantdc">
                        </div>
                    </div>
                    <div class="layui-form-item wx_list">
                        <label class="layui-form-label">微信支付目录:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入微信支付目录" class="layui-input wx_path_newland">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn save_newland">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="open_alipay" class="hide" style="display: none;background-color: #fff;">
            <div class="xgrate">开通通道</div>
            <div class="layui-card-body" style="padding: 15px;text-align: center;">
                <div style="padding-bottom:10px;">支付宝授权</div>
                <div class="img" id="code"><img src=""></div>
            </div>
        </div>

        <div id="open_js" class="hide" style="display: none;background-color: #fff;">
            <div class="xgrate">开通通道</div>
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item xingzhi" style="margin:20px 0 30px 0">
                        <label class="layui-form-label">结算类型</label>
                        <div class="layui-input-block">
                            <select name="open" id="open" lay-filter="open"></select>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn open_jiesuan">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="open_tf_js" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item js_openTf" style="margin:20px 0 30px 0">
                        <label class="layui-form-label">结算类型</label>
                        <div class="layui-input-block">
                            <select name="openTf" id="openTf" lay-filter="openTf">
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item qdopen" style="margin:20px 0 30px 0;">
                        <label class="layui-form-label">渠道</label>
                        <div class="layui-input-block">
                            <select name="qdopen" id="qdopen" lay-filter="qdopen">
                                <option value="">选择渠道</option>
                                <option value="1">渠道1</option>
                                <option value="2" selected>渠道2</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn open_tf_jiesuan">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="open_hltx_js" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;height: 260px;">
                <div class="layui-form">

                    <div class="layui-form-item js_openTf" style="margin:20px 0 30px 0">
                        <label class="layui-form-label">结算类型</label>
                        <div class="layui-input-block">
                            <select name="openhltx" id="openhltx" lay-filter="openhltx">

                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item qdopen" style="margin:20px 0 30px 0;">
                        <label class="layui-form-label">渠道</label>
                        <div class="layui-input-block">
                            <select name="hltx_qdopen" id="hltx_qdopen" lay-filter="hltx_qdopen">
                                <option value="">选择渠道</option>
                                <option value="1">渠道1</option>
                                <option value="2">渠道2</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn open_hltx_jiesuan">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="edit_sao" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">门店名称:</label>
                        <div class="layui-input-block" style="line-height: 36px;">
                            <div class="agent"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block" style="line-height: 36px;">
                            <div class="ways"></div>
                        </div>
                    </div>
                    <div style="padding: 20px;">金额 0-1000</div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">贷记卡费率:</label>
                        <div class="layui-input-block">
                            <input type="number" placeholder="请输入贷记卡费率" class="layui-input rates1">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">借记卡费率:</label>
                        <div class="layui-input-block">
                            <input type="number" placeholder="请输入借记卡费率" class="layui-input rates2">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">借记卡封顶:</label>
                        <div class="layui-input-block">
                            <input type="number" placeholder="请输入借记卡封顶" class="layui-input rates3">
                        </div>
                    </div>

                    <div style="padding: 20px;">金额 大于1000</div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">贷记卡费率:</label>
                        <div class="layui-input-block">
                            <input type="number" placeholder="请输入贷记卡费率" class="layui-input rates4">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">借记卡费率:</label>
                        <div class="layui-input-block">
                            <input type="number" placeholder="请输入借记卡费率" class="layui-input rates5">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">借记卡封顶:</label>
                        <div class="layui-input-block">
                            <input type="number" placeholder="请输入借记卡封顶" class="layui-input rates6">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn submitsao">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="open_passways" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block">
                            <div class="way"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">商户号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入商户号" class="layui-input wx_sub_hrt">
                        </div>
                    </div>
                    <div class="layui-form-item wx_list">
                        <label class="layui-form-label">微信支付目录:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入微信支付目录" class="layui-input wx_path_hrt">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn open_ways">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="open_ltf" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block">
                            <div class="way"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">merchantCode:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入merchantCode" class="layui-input ltf1">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">md_key:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入md_key" class="layui-input ltf2">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">appId:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入appId" class="layui-input ltf3">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn open_ltf_save">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" class="ltf_type">
        </div>

        <div id="alipay_store_id" class="hide" style="display: none;background-color: #fff;">
            <div class="xgrate">商户号</div>
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block" style="line-height: 36px;">
                            <div class="alipayway"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">口碑外部ID:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入口碑外部ID" class="layui-input wx_sub_alipay_wai">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">口碑门店ID:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入口碑门店ID" class="layui-input wx_sub_alipay">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn open_alipay_store_id">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" class="alipaytype">
        </div>

        <div id="dlb" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block">
                            <div class="way"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">商户编号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入商户编号" class="layui-input dlb_inputs">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">门店编号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入门店编号" class="layui-input dlb_inputs">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">机具编号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入机具编号" class="layui-input dlb_inputs">
                        </div>
                    </div>
                    <div class="layui-form-item wx_list">
                        <label class="layui-form-label">微信支付目录:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入微信支付目录" class="layui-input wx_path_dlb">
                        </div>
                    </div>
                    <input type="hidden" class="dlb_type">
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn open_dlb_save">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="vbill" class="hide" style="display: none;background-color: #fff;">
            <div class="xgrate">商户号</div>
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block">
                            <div class="way"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">商户号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入商户号" class="layui-input sxf_input1">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">微信渠道号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入商户号" class="layui-input sxf_input2">
                        </div>
                    </div>
                    <div class="layui-form-item wx_list">
                        <label class="layui-form-label">微信支付目录:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入微信支付目录" class="layui-input wx_path_sxf">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn sxf_save">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="edit_flower" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form f_box" lay-filter="component-form-group">

                    <!--   <div class="layui-form-item">
            <label class="layui-form-label" >3期:</label>
            <div class="layui-input-block f_block">
              <input type="text" placeholder="请输入商户编号" class="layui-input f_inputs">
            </div>

            <button class="layui-btn open_f_save">确定</button>
          </div>  -->

                    <div class="layui-unselect layui-form-checkbox" lay-skin="primary">
                        <span>阅读</span>
                        <i class="layui-icon layui-icon-ok"></i>
                    </div>
                </div>
                <!-- <button class="layui-btn returnSettings">返还设置</button> -->
                <a class="layui-btn layui-btn-normal layui-btn-xs returnSettings" lay-event="returnSettings" lay-href="">返还设置</a>
            </div>
        </div>

        <div id="open_disable" class="hide" style="display: none;background-color: #fff;">
            <div class="xgrate">通道禁用</div>
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item js_openTf" style="margin:20px 0 0px 0">
                        <label class="layui-form-label" style="width: 100px;">通道开启关闭</label>
                        <div class="layui-input-block" style="margin-left: 130px;">
                            <select name="passway1" id="passway1" lay-filter="passway1">
                                <option value="0">开启</option>
                                <option value="1">关闭</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item qdopen" style="margin:20px 0 0px 0;">
                        <label class="layui-form-label" style="width: 100px;">B扫C信用通道</label>
                        <div class="layui-input-block" style="margin-left: 130px;">
                            <select name="passway2" id="passway2" lay-filter="passway2">
                                <option value="01">开启</option>
                                <option value="00">关闭</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item qdopen" style="margin:20px 0 30px 0;">
                        <label class="layui-form-label" style="width: 100px;">C扫B信用通道</label>
                        <div class="layui-input-block" style="margin-left: 130px;">
                            <select name="passway3" id="passway3" lay-filter="passway3">
                                <option value="01">开启</option>
                                <option value="00">关闭</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label" style="width: 100px;">单笔限额:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入单笔限额" class="layui-input dbxe" style="width: 94%;">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn save_display">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="sumbit_realName" class="hide" style="display: none;background-color: #fff;">
            <div class="xgrate">实名认证</div>
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form-item">
                    <label class="layui-form-label">认证状态:</label>
                    <div class="layui-input-block">
                        <div class="realName_way"></div>
                    </div>
                </div>
                <div class="layui-form-item realName_empower">
                    <label class="layui-form-label">授权结果:</label>
                    <div class="layui-input-block">
                        <div class="authStatus"></div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label" style="width:230px;">请联系商户使用微信扫码完成操作</label>
                    <div class="layui-input-block">
                        <div class="realName_code"></div>
                    </div>
                </div>
                <div class="buttons">
                    <div class="sumbut_rz">提交认证</div>
                    <div class="sumbut_cx">更新状态</div>
                    <div class="sumbut_chexiao">撤销申请</div>
                    <div class="sumbut_result">授权结果</div>
                </div>
            </div>
        </div>

        <div id="hltx_passway" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block">
                            <div class="way"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">渠道商户号:</label>
                        <div class="layui-input-block">
                            <div class="waynum"></div>
                        </div>
                    </div>
                    <div class="layui-form-item qdopen" style="margin:20px 0 30px 0;">
                        <label class="layui-form-label">渠道</label>
                        <div class="layui-input-block">
                            <select name="hltx_qdopen_td" id="hltx_qdopen_td" lay-filter="hltx_qdopen_td">
                                <option value=''>选择渠道</option>
                                <option value="1">渠道1</option>
                                <option value="2">渠道2</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">商户号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入商户号" class="layui-input hltx_wx_sub_merchant_id">
                        </div>
                    </div>
                    <div class="layui-form-item wx_list">
                        <label class="layui-form-label">微信支付目录:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入微信支付目录" class="layui-input hltx_wx_path">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn hltx_way">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="changsha_passway" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block">
                            <div class="way"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">商户号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入商户号" class="layui-input changsha_wx_sub_merchant_id">
                        </div>
                    </div>
                    <div class="layui-form-item changsha_list">
                        <label class="layui-form-label">微信支付目录:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入微信支付目录" class="layui-input changsha_wx_path">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">门店编号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入门店编号" class="layui-input store_num">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">收银员编号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入收银员编号" class="layui-input merchant_num">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn changsha_save">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="lianfuyoupay_passway" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block">
                            <div class="way"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">商户号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入商户号" class="layui-input lianfuyoupay_merchant_id">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">POS_SN:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入pos_sn" class="layui-input pos_sn">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">signkey:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入signkey" class="layui-input lianfuyoupaykey">
                        </div>
                    </div>
                    <div class="layui-form-item lianfuyoupay_list">
                        <label class="layui-form-label">微信支付目录:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入微信支付目录" class="layui-input lianfuyoupay_wx_path">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn lianfuyoupay_save">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="open_easypay_passway" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block">
                            <div class="way"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">原操作流水号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入原操作流水号" class="layui-input easypay_operatrace">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">终端商户号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入终端商户号" class="layui-input easypay_mercode">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">终端终端号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入终端终端号" class="layui-input easypay_termcode">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn easypay_save">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="open_wftpay_passway" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block">
                            <div class="way"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">商户号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入商户号" class="layui-input wftpay_mch_id">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn wftpay_save">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="open_hwcpay_passway" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block">
                            <div class="way"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">平台门店编号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入门店编号" class="layui-input hwcpay_mch_id">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">商户编号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入商户编号" class="layui-input hwcpay_merchant_num">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn hwcpay_save">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="open_yinsheng_passway" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block">
                            <div class="way"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">商户号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入商户号" class="layui-input yinsheng_partner_id">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">银盛平台注册时公司名称:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入银盛支付客户名" class="layui-input yinsheng_seller_name">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">业务代码:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入业务代码" class="layui-input yinsheng_business_code">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn yinsheng_save">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="open_qfpay_passway" class="hide" style="display: none;background-color: #fff;">
            <div class="layui-card-body" style="padding: 15px;">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">通道:</label>
                        <div class="layui-input-block">
                            <div class="way"></div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">商户号:</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入商户号" class="layui-input qfpay_mchid">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0;">
                                <button class="layui-btn qfpay_save">确定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <input type="hidden" class="passway1" value="0">
    <input type="hidden" class="passway2" value="01">
    <input type="hidden" class="passway3" value="01">
    <input type="hidden" class="passway_company">
    <input type="hidden" class="passway_ways_type">

    <input type="hidden" class="type">
    <input type="hidden" class="types">
    <input type="hidden" class="js">
    <input type="hidden" class="js_open_tf">
    <input type="hidden" class="qd" value="2">
    <input type="hidden" class="hltxqd" value="">
    <input type="hidden" class="tfqd" value="">
    <input type="hidden" class="ways_source" value="">

    <script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
    <script src="<?php echo e(asset('/layuiadmin/layui/jquery-2.1.4.js')); ?>"></script>
    <script src="<?php echo e(asset('/layuiadmin/layui/jquery.qrcode.min.js')); ?>"></script>
    <script>
        var token = sessionStorage.getItem("Usertoken");
        var store_name = sessionStorage.getItem("store_store_name");
        var agentName = sessionStorage.getItem("agentName");
        // var str = location.search;
        var store_id = sessionStorage.getItem("store_store_id");

        layui.config({
            base: '../../layuiadmin/' //静态资源所在路径
        }).extend({
            index: 'lib/index' //主入口模块
        }).use(['index', 'form', 'table', 'laydate'], function() {
            var $ = layui.$,
                admin = layui.admin,
                form = layui.form,
                table = layui.table,
                laydate = layui.laydate;

            // 未登录,跳转登录页面
            $(document).ready(function() {
                if (token == null) {
                    window.location.href = "<?php echo e(url('/user/login')); ?>";
                }
                if (store_id != undefined || store_id != '') {
                    $('.returnSettings').show()
                }
            });

            $('.agent').html(store_name);
            $('.dbxe').val('10000');
            // $('.layui-card-header').html(agentName);
            $('.layui-card-header').html(store_name);
            console.log(store_id);

            // 渲染表格
            table.render({
                elem: '#test-table-page',
                url: "<?php echo e(url('/api/user/pay_ways_all')); ?>",
                method: 'post',
                where: {
                    token: token,
                    store_id: store_id
                },
                request: {
                    pageName: 'p',
                    limitName: 'l'
                }
                // ,page: true
                ,
                cellMinWidth: 150,
                cols: [
                    [{
                        field: 'ways_desc',
                        title: '通道名称'
                    }, {
                        field: 'rate',
                        title: '结算费率',
                        templet: '#rate'
                    }, {
                        field: 'settlement_type',
                        title: '到账时间'
                    }, {
                        field: 'status_desc',
                        title: '是否开通'
                    }, {
                        field: 'status',
                        width: 522,
                        align: 'center',
                        fixed: 'right',
                        toolbar: '#table-content-list',
                        title: '操作',
                        templet: '#passway'
                    }]
                ],
                response: {
                    statusName: 'status' //数据状态的字段名称，默认：code
                        ,
                    statusCode: 1 //成功的状态码，默认：0
                        ,
                    msgName: 'message' //状态信息的字段名称，默认：msg
                        ,
                    countName: 't' //数据总数的字段名称，默认：count
                        ,
                    dataName: 'data' //数据列表的字段名称，默认：data
                },
                done: function(res, curr, count) {
                    $('th').css({
                        'font-weight': 'bold',
                        'font-size': '15',
                        'color': 'black',
                        'background': 'linear-gradient(#f2f2f2,#cfcfcf)'
                    }); //进行表头样式设置
                }
            });
            
            var this2 = $(this);
            table.on('tool(test-table-page)', function(obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
                var e = obj.data; //获得当前行数据
                var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
                var tr = obj.tr; //获得当前行 tr 的DOM对象
                //            console.log(e);
                //            console.log(layEvent);
                // sessionStorage.setItem('s_store_id', e.store_id);

                if (layEvent === 'edit') { //修改费率
                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);
                    $('.rate').val(e.rate);
                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#edit_rate')
                    });
                }  else if (layEvent === "adduserdatadlb"){
                    
                    $(this).attr("lay-href","/user/editdlbstore")
                    
                } else if (layEvent === "easypaydataupdata"){

                    $.post("/api/user/alterMerchant",{
                        token:token,
                        storeId:store_id,
                        type:1,
                    },function(data){
                        console.log(data)
                        if(data.status == 1){
                            layer.msg("商户信息变更成功", {icon:1, shade:0.5, time:1000});
                        }else{
                            layer.msg(data.message, {icon:2, shade:0.5, time:2000});
                        }
                    })
                    $.post("/api/user/alterPayAcc",{
                        token:token,
                        storeId:store_id,
                        type:1,
                    },function(data){
                        if(data.status == 1){
                            layer.msg("结算账户变更成功", {icon:1, shade:0.5, time:1000});
                        }else{
                            layer.msg(data.message, {icon:2, shade:0.5, time:2000});
                        }
                    })

                }else if( layEvent === "adduserdatahkrt" ){

                    $(this).attr("lay-href","/user/edithkrtstoreinfo")
                	
                } else if( layEvent === "adduserdataeasypay" ){

                    $(this).attr("lay-href","/user/ysjj?"+store_id+"")
                    
                }else if (layEvent === 'shua') {
                    $('.ways').html(e.ways_desc);
                    $('.types').val(e.ways_type);
                    $('.rate1').val(e.rate_e);
                    $('.rate2').val(e.rate_f);
                    $('.rate3').val(e.rate_f_top);
                    var openshua = layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#edit_shua')
                    });
                } else if (layEvent === 'newland') {
                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);
                    $('.ways_source').val(e.ways_source);
                    if (e.ways_source == "weixin") {
                        $('.wx_list').show();
                    } else {
                        $('.wx_list').hide();
                    }

                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: e.ways_type,
                        nl_mercId: ''
                    }, function(res) {
                        //                    console.log(res);
                        //                    console.log(res.data.nl_key);
                        if (res.status == 1) {
                            $('.wxmerchantda').val(res.data.nl_mercId);
                            $('.wxmerchantdb').val(res.data.nl_key);
                            $('.wxmerchantdc').val(res.data.trmNo);
                            $('.wxsubappid').val(res.data.wx_sub_appid);
                        }
                    }, "json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#newland')
                    });
                } else if (layEvent === 'open') {
                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);
                    $('.ways_source').val(e.ways_source);
                    $('.lianfushow').hide();

                    if (e.ways_source == "weixin") {
                        $('.wx_list').show();
                    } else {
                        $('.wx_list').hide();
                    }

                    if (e.ways_type == 2000) {
                        $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                            token: token,
                            store_id: store_id,
                            ways_type: e.ways_type,
                            wx_sub_merchant_id: ''
                        }, function(res) {
                            //                        console.log(res);
                            if (res.status == 1) {
                                $('.wx_sub_merchant_id').val(res.data.wx_sub_merchant_id);
                            }
                        }, "json");

                        layer.open({
                            type: 1,
                            title: false,
                            closeBtn: 0,
                            area: '516px',
                            skin: 'layui-layer-nobg', //没有背景色
                            shadeClose: true,
                            content: $('#open_passway')
                        });
                    } else {
                        $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                            token: token,
                            store_id: store_id,
                            ways_type: e.ways_type,
                            MerchantId: ''
                        }, function(res) {
                            //                        console.log(res);
                            if (res.status == 1) {
                                $('.wx_sub_merchant_id').val(res.data.MerchantId);
                            }
                        }, "json");

                        layer.open({
                            type: 1,
                            title: false,
                            closeBtn: 0,
                            area: '516px',
                            skin: 'layui-layer-nobg', //没有背景色
                            shadeClose: true,
                            content: $('#open_passway')
                        });
                    }
                } else if (layEvent === 'openweixin') {
                    /**
                     * 点击门店管理列表，通道管理页面中的 商户号 按钮
                     */

                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);
                    $('.ways_source').val(e.ways_source);

                    //发送请求
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: e.ways_type,
                        wx_sub_merchant_id: ''
                    }, function(res) {
                        if (res.status == 1) {
                            layui.use('form', function() {
                                var form = layui.form;
                                //商户号输入框赋值
                                $('.wx_sub_merchant_id_shop').val(res.data.wx_sub_merchant_id);
                                //优惠商品ID输入框赋值
                                $('.wx_shop').val(res.data.wx_shop_id);
                                //页面上没有查找到对应的html dom元素，暂时应该用不到
                                $('.is_profit_sharing').val(res.data.is_profit_sharing);

                                //分账接收方类型选择框赋值
                                if (res.data.wx_sharing_type != "") {
                                    $('.wx_sharing_type').val(res.data.wx_sharing_type);
                                    var wx_sharing_type = $('.wx_sharing_type').children();
                                    $('.wx_sharing_type option').each(function(i, e) {
                                        // console.log($(this).attr("value"),res.data.wx_sharing_type);
                                        if ($(this).attr("value") == res.data.wx_sharing_type) {
                                            $('.wx_sharing_type').children().eq(i).attr("selected", true);
                                        }
                                    });
                                }

                                //分账接收方帐号输入框赋值
                                $('.wx_sharing_account').val(res.data.wx_sharing_account);
                                //分账接收方全称输入框赋值
                                $('.wx_sharing_name').val(res.data.wx_sharing_name);
                                //设置比例
                                $('.wx_sharing_proportion').val(res.data.wx_sharing_rate);
                                //开启分账
                                if (res.data.is_profit_sharing == 1) {
                                    //开启
                                    $(".weixin_share_list").show();
                                    $("input[name=wx_sharing_open][value=0]").attr("checked", false);
                                    $("input[name=wx_sharing_open][value=1]").attr("checked", true);
                                } else {
                                    ///关闭
                                    $(".weixin_share_list").hide();
                                    $("input[name=wx_sharing_open][value=0]").attr("checked", true);
                                    $("input[name=wx_sharing_open][value=1]").attr("checked", false);
                                }

                                //与分账方的关系类型
                                var wx_sharing_relation_type = res.data.wx_sharing_relation_type.split(",");
                                if (wx_sharing_relation_type.length > 0) {
                                    for (var i = 0; i < wx_sharing_relation_type.length; i++) {
                                        $('.wx_sharing_relation_type_' + wx_sharing_relation_type[i]).attr("checked", true);
                                    }
                                }
                                form.render(); //更新全部
                            });
                        }
                    }, "json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_passway_weixin')
                    });

                } else if (layEvent === 'lianfu') {
                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);
                    $('.ways_source').val(e.ways_source);
                    $('.lianfushow').show();

                    if (e.ways_source == "weixin") {
                        $('.wx_list').show();
                    } else {
                        $('.wx_list').hide();
                    }

                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: e.ways_type,
                        wx_sub_merchant_id: '',
                        signkey: ''
                    }, function(res) {
                        //                        console.log(res);
                        if (res.status == 1) {
                            $('.wx_sub_merchant_id').val(res.data.MerchantId);
                            $('.lianfukey').val(res.data.signkey);
                        }
                    }, "json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_passway_lianfu')
                    });
                } else if (layEvent === 'tf') {
                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);
                    $('.ways_source').val(e.ways_source);

                    if (e.ways_source == "weixin") {
                        $('.wx_list').show();
                    } else {
                        $('.wx_list').hide();
                    }

                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: e.ways_type,
                        MerchantId: ''
                    }, function(res) {
                        //                    console.log(res);
                        if (res.status == 1) {
                            $('.tf_merchant_id').val(res.data.MerchantId);
                            $('.tfqd').val(res.data.qd);

                            if (res.data.length == 0) {
                                $('.tf_show').hide();
                            } else {
                                $('.tf_show').show();
                            }

                            $('.qd_item2').html(res.data.channel_mch_id);
                            var optionStr = "";
                            $("#tf_qdopen").html('');

                            if (res.data.qd == 1) {
                                optionStr += "<option value=''>选择渠道</option>";
                                optionStr += "<option value='1' selected>渠道1</option>";
                                optionStr += "<option value='2'>渠道2</option>";
                            } else if (res.data.qd == 2) {
                                //                            console.log('00000');
                                optionStr += "<option value=''>选择渠道</option>";
                                optionStr += "<option value='1'>渠道1</option>";
                                optionStr += "<option value='2' selected>渠道2</option>";
                            } else if (res.data.qd == null) {
                                //                            console.log('11111');
                                optionStr += "<option value=''>选择渠道</option>";
                                optionStr += "<option value='1'>渠道1</option>";
                                optionStr += "<option value='2'>渠道2</option>";
                            }

                            $("#tf_qdopen").append(optionStr);
                            layui.form.render('select');
                        }
                    }, "json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_tf_passway')
                    });
                } else if (layEvent === 'hrt') {
                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);
                    $('.ways_source').val(e.ways_source);

                    if (e.ways_source == "weixin") {
                        $('.wx_list').show();
                    } else {
                        $('.wx_list').hide();
                    }

                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: e.ways_type,
                        h_mid: ''
                    }, function(res) {
                        //                    console.log(res);
                        if (res.status == 1) {
                            $('.wx_sub_hrt').val(res.data.h_mid);
                        }
                    }, "json");
                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_passways')
                    });
                } else if (layEvent === "ltf") {
                    $('.way').html(e.ways_desc);
                    $('.ltf_type').val(e.ways_type);
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: e.ways_type,
                        merchantCode: ''
                    }, function(res) {
                        //                    console.log(res);
                        if (res.status == 1) {
                            $('.ltf1').val(res.data.merchantCode);
                            $('.ltf2').val(res.data.md_key);
                            $('.ltf3').val(res.data.appId);
                        }
                    }, "json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_ltf')
                    });
                } else if (layEvent === 'dlb') {
                    $('.way').html(e.ways_desc);
                    $('.dlb_type').val(e.ways_type);
                    $('.ways_source').val(e.ways_source);

                    if (e.ways_source == "weixin") {
                        $('.wx_list').show();
                    } else {
                        $('.wx_list').hide();
                    }

                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: e.ways_type,
                        getdlbdata: 1
                    }, function(res) {
                        //                    console.log(res);
                        if (res.status == 1) {
                            $('.dlb_inputs').eq(0).val(res.data.mch_num);
                            $('.dlb_inputs').eq(1).val(res.data.shop_num);
                            $('.dlb_inputs').eq(2).val(res.data.machine_num);
                        }
                    }, "json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#dlb')
                    });
                } else if (layEvent === 'sxf') {
                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);
                    $('.ways_source').val(e.ways_source);

                    if (e.ways_source == "weixin") {
                        $('.wx_list').show();
                    } else {
                        $('.wx_list').hide();
                    }

                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: e.ways_type,
                        MerchantId: ''
                    }, function(res) {
                        //                    console.log(res);
                        if (res.status == 1) {
                            $('.sxf_input1').val(res.data.MerchantId);
                            $('.sxf_input2').val(res.data.wx_channel_appid);
                        }
                    }, "json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#vbill')
                    });
                } else if (layEvent === 'alipay') {
                    $('.alipayway').html(e.ways_desc);
                    $('.alipaytype').val(e.ways_type);

                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: e.ways_type,
                        alipay_store_id: ''
                    }, function(res) {
                        //                    console.log(res);
                        if (res.status == 1) {
                            $('.wx_sub_alipay').val(res.data.alipay_store_id);
                            $('.wx_sub_alipay_wai').val(res.data.out_store_id)
                        }
                    }, "json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#alipay_store_id')
                    });
                } else if (layEvent === 'pay') {
                    $('.pay').attr('lay-href', "<?php echo e(url('/user/openpassway?store_id=')); ?>" + store_id + "&ways_type=" + e.ways_type);
                } else if (layEvent === 'alipaycode') {
                    $.post("<?php echo e(url('/api/user/alipay_auth')); ?>", {
                        token: token,
                        store_id: store_id
                    }, function(res) {
                        //                    console.log(res);
                        $('#code').html('');
                        $('#code').qrcode(res.data.qr_url);
                    }, "json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_alipay')
                    });
                } else if (layEvent === 'js_tf') {
                    $('.type').val(e.ways_type);

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_tf_js')
                    });

                    $("#openTf").html('');

                    // 结算类型查询
                    $.ajax({
                        url: "<?php echo e(url('/api/basequery/settle_mode_type')); ?>",
                        data: {
                            token: token,
                            ways_type: e.ways_type
                        },
                        type: 'get',
                        success: function(data) {
                            //                        console.log(data);
                            var optionStr = "";
                            for (var i = 0; i < data.data.length; i++) {
                                optionStr += "<option value='" + data.data[i].settle_mode_type + "' >" +
                                    data.data[i].settle_mode_type_desc + "</option>";
                            }
                            $("#openTf").append('<option value="">请选择结算方式</option>' + optionStr);
                            layui.form.render('select');
                        },
                        error: function(data) {
                            alert('查找板块报错');
                        }
                    });
                    // var str = "";
                    // str += "<option value='2'>"渠道2"</option>";
                    // $("#qdopen").append('<option value="2">渠道2</option>');
                    // layui.form.render('select');
                } else if (layEvent === 'js_hltx') {
                    $('.type').val(e.ways_type);
                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_hltx_js')
                    });
                    $("#openhltx").html('');

                    // 结算类型查询
                    $.ajax({
                        url: "<?php echo e(url('/api/basequery/settle_mode_type')); ?>",
                        data: {
                            token: token,
                            ways_type: e.ways_type
                        },
                        type: 'get',
                        success: function(data) {
                            //                        console.log(data);
                            var optionStr = "";
                            for (var i = 0; i < data.data.length; i++) {
                                optionStr += "<option value='" + data.data[i].settle_mode_type + "' >" +
                                    data.data[i].settle_mode_type_desc + "</option>";
                            }
                            $("#openhltx").append('<option value="">请选择结算方式</option>' + optionStr);
                            layui.form.render('select');
                        },
                        error: function(data) {
                            alert('查找板块报错');
                        }
                    });

                    // var str = "";
                    // str += "<option value='2'>"渠道2"</option>";
                    // $("#qdopen").append('<option value="2">渠道2</option>');
                    // layui.form.render('select');
                } else if (layEvent === 'js') {
                    $('.type').val(e.ways_type);
                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_js')
                    });
                    $("#open").html('');
                    // 结算类型查询
                    $.ajax({
                        url: "<?php echo e(url('/api/basequery/settle_mode_type')); ?>",
                        data: {
                            token: token,
                            ways_type: e.ways_type
                        },
                        type: 'get',
                        success: function(data) {
                            //                        console.log(data);
                            var optionStr = "";
                            for (var i = 0; i < data.data.length; i++) {
                                optionStr += "<option value='" + data.data[i].settle_mode_type + "' >" +
                                    data.data[i].settle_mode_type_desc + "</option>";
                            }
                            $("#open").append('<option value="">请选择结算方式</option>' + optionStr);
                            layui.form.render('select');
                        },
                        error: function(data) {
                            alert('查找板块报错');
                        }
                    });
                } else if (layEvent === 'sao') {
                    $('.agent').html();
                    $('.ways').html(e.ways_desc);
                    $('.types').val(e.ways_type);
                    $('.rates1').val(e.rate_a);
                    $('.rates2').val(e.rate_b);
                    $('.rates3').val(e.rate_b_top);
                    $('.rates4').val(e.rate_c);
                    $('.rates5').val(e.rate_d);
                    $('.rates6').val(e.rate_d_top);
                    var openshua = layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#edit_sao')
                    });
                } else if (layEvent === 'floweredit') {
                    var openshua = layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#edit_flower')
                    });

                    $('.f_box').html('');

                    $.post("<?php echo e(url('/api/alipayopen/store_fq_rate')); ?>", {
                        token: token,
                        store_id: store_id
                    }, function(res) {
                        //                    console.log(res);
                        var html = '';
                        if (res.status == 1) {
                            for (var i = 0; i < res.data.length; i++) {
                                html += '<div class="layui-form-item">';

                                if (res.data[i].status == '01') { //开启
                                    html += '<div class="f_status" style="float:left;margin-left:10px;"  data="' + res.data[i].status + '">';
                                    html += '<div class="checkbox layui-unselect layui-form-checkbox layui-form-checked" lay-skin="primary" data="' + res.data[i].status + '">';
                                    html += '<i class="layui-icon layui-icon-ok"></i>';
                                    html += '</div>';
                                    html += '</div>';
                                } else { //关闭
                                    html += '<div class="f_status" style="float:left;margin-left:10px;"  data="' + res.data[i].status + '">';
                                    html += '<div class="checkbox layui-unselect layui-form-checkbox" lay-skin="primary" data="' + res.data[i].status + '">';
                                    html += '<i class="layui-icon layui-icon-ok"></i>';
                                    html += '</div>';
                                    html += '</div>';
                                }

                                html += '<label class="layui-form-label">' + res.data[i].num + '期费率:</label>';
                                html += '<div class="layui-input-block f_block" data="' + res.data[i].num + '">';
                                html += '<input type="text" placeholder="请输入' + res.data[i].num + '期费率" class="layui-input f_inputs" value="' + res.data[i].rate + '">%';
                                html += '</div>'; 
                                html += '<button class="layui-btn open_f_save">确定</button>';
                                html += '</div>';
                            }

                            $('.f_box').append(html);
                        }
                    }, "json");
                } else if (layEvent === 'delpassway') {
                    layer.confirm('确认清除此通道?', {
                        icon: 2
                    }, function(index) {
                        $.post("<?php echo e(url('/api/user/clear_ways_type')); ?>", {
                            token: token,
                            company: e.company,
                            store_id: store_id
                        }, function(res) {
                            //                        console.log(res);
                            if (res.status == 1) {
                                obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                                layer.close(index);
                                layer.msg(res.message, {
                                    offset: '50px',
                                    icon: 1,
                                    time: 2000
                                });
                            } else {
                                layer.msg(res.message, {
                                    offset: '50px',
                                    icon: 2,
                                    time: 3000
                                });
                            }
                        }, "json");
                    });
                } else if (layEvent === 'passwaydisable') {
                    $('.passway_ways_type').val(e.ways_type);
                    $('.passway_company').val(e.company);
                    $.post("<?php echo e(url('/api/user/store_ways_select')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: e.ways_type,
                        company: e.company
                    }, function(res) {
                        //                        console.log(res);
                        if (res.status == 1) {
                            if (res.data.length != 0) {
                                $('.passway_ways_type').val(res.data.ways_type);
                                $('.passway_company').val(res.data.company);
                                $('.passway1').val(res.data.is_close);
                                $('.passway2').val(res.data.pcredit);
                                $('.passway3').val(res.data.credit);
                                $('.dbxe').val(res.data.pay_amount_e);

                                // 第一个
                                $("#passway1").html('');
                                var optionStr1 = "";
                                if (res.data.is_close == 0) {
                                    optionStr1 += "<option value='0' selected>开启</option>";
                                    optionStr1 += "<option value='1'>关闭</option>";
                                } else {
                                    optionStr1 += "<option value='0'>开启</option>";
                                    optionStr1 += "<option value='1' selected>关闭</option>";
                                }

                                $("#passway1").append(optionStr1);
                                layui.form.render('select');

                                // 第二个
                                $("#passway2").html('');
                                var optionStr2 = "";
                                if (res.data.pcredit == "01") {
                                    optionStr2 += "<option value='01' selected>开启</option>";
                                    optionStr2 += "<option value='00'>关闭</option>";
                                } else {
                                    optionStr2 += "<option value='01'>开启</option>";
                                    optionStr2 += "<option value='00' selected>关闭</option>";
                                }

                                $("#passway2").append(optionStr2);
                                layui.form.render('select');

                                // 第三个
                                $("#passway3").html('');
                                var optionStr3 = "";
                                if (res.data.credit == "01") {
                                    optionStr3 += "<option value='01' selected>开启</option>";
                                    optionStr3 += "<option value='00'>关闭</option>";
                                } else {
                                    optionStr3 += "<option value='01'>开启</option>";
                                    optionStr3 += "<option value='00' selected>关闭</option>";
                                }

                                $("#passway3").append(optionStr3);
                                layui.form.render('select');
                            }
                        }
                    }, "json");

                    var openshua = layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_disable')
                    });
                } else if (layEvent === 'realName') {
                    $('.type').val(e.ways_type);
                    if (e.ways_type === 19002 || e.ways_type === 13002) {
                        // $.post("<?php echo e(url('/api/user/applyWeChatRealName')); ?>",
                        // {
                        //     token:token
                        //     ,storeId:store_id
                        //     ,waysType:e.ways_type
                        // },function(res){
                        //     console.log(res);
                        // if(res.data.idenStatus == 0){
                        // $('.realName_way').html("未认证");
                        //提交按钮
                        $(".sumbut_rz").show();
                        //刷新按钮
                        $(".sumbut_cx").hide();
                        //撤销按钮
                        $(".sumbut_chexiao").hide();
                        //授权结果按钮
                        $(".sumbut_result").hide();
                        //授权结果展示div
                        $(".realName_empower").hide();
                        //提交认证
                        $(".sumbut_rz").on("click", function() {
                            $.post("<?php echo e(url('/api/user/applyWeChatRealName')); ?>", {
                                token: token,
                                storeId: store_id,
                                waysType: e.ways_type
                            }, function(res) {
                                console.log(res);
                                // if(res.status == 1){
                                $(".sumbut_cx").show();
                                //刷新状态
                                $(".sumbut_cx").on("click", function() {
                                    $.post("<?php echo e(url('/api/user/queryWeChatRealName')); ?>", {
                                        token: token,
                                        storeId: store_id,
                                        waysType: e.ways_type
                                    }, function(res) {
                                        console.log(res);
                                        if (res.data.idenStatus == 0) {
                                            $('.realName_way').html("未认证");
                                        } else if (res.data.idenStatus == 1) {
                                            $('.realName_way').html("审核通过");
                                            $(".sumbut_rz").hide();
                                            $(".sumbut_cx").hide();
                                            $(".sumbut_chexiao").hide();
                                            $(".sumbut_result").show();
                                            //授权结果
                                            $(".sumbut_result").on("click", function() {
                                                $.post("<?php echo e(url('/api/user/queryGrantStatusWeChatRealName')); ?>", {
                                                    token: token,
                                                    storeId: store_id,
                                                    waysType: e.ways_type
                                                }, function(res) {
                                                    if (res.status == 1) {
                                                        $(".realName_empower").show();
                                                        $(".realName_code").html(res.data.infoQrcode);
                                                        if (res.data.authStatus == 0) {
                                                            $(".authStatus").html("未知")
                                                        } else if (res.data.authStatus == 1) {
                                                            $(".authStatus").html("已授权")
                                                        } else if (res.data.authStatus == 2) {
                                                            $(".authStatus").html("未授权")
                                                        }
                                                        layer.msg(res.message, {
                                                            offset: '15px',
                                                            icon: 1,
                                                            time: 2000
                                                        });
                                                    } else {
                                                        layer.msg(res.message, {
                                                            offset: '15px',
                                                            icon: 2,
                                                            time: 2000
                                                        });
                                                    }
                                                }, "json");
                                            });
                                        } else if (res.data.idenStatus == 2) {
                                            $('.realName_way').html("待确认联系信息");
                                            $(".sumbut_rz").hide();
                                            $(".sumbut_cx").hide();
                                            $(".sumbut_chexiao").hide();
                                            $(".realName_code").html(res.data.infoQrcode)
                                        } else if (res.data.idenStatus == 3) {
                                            $('.realName_way').html("待账户验证");
                                            $(".sumbut_rz").hide();
                                            $(".sumbut_cx").hide();
                                            $(".sumbut_chexiao").hide();
                                        } else if (res.data.idenStatus == 4) {
                                            $('.realName_way').html("审核中");
                                            $(".sumbut_rz").hide();
                                            $(".sumbut_cx").hide();
                                            //撤销申请
                                            $(".sumbut_chexiao").on("click", function() {
                                                $.post("<?php echo e(url('/api/user/backWeChatRealName')); ?>", {
                                                    token: token,
                                                    storeId: store_id,
                                                    waysType: e.ways_type
                                                }, function(res) {
                                                    if (res.status == 1) {
                                                        layer.msg(res.message, {
                                                            offset: '15px',
                                                            time: 2000
                                                        });
                                                    } else {
                                                        layer.msg(res.message, {
                                                            offset: '15px',
                                                            icon: 2,
                                                            time: 2000
                                                        });
                                                    }
                                                }, "json");
                                            });
                                        } else if (res.data.idenStatus == 5) {
                                            $('.realName_way').html("审核驳回");
                                            $(".sumbut_rz").hide();
                                            $(".sumbut_cx").hide();
                                            //撤销申请
                                            $(".sumbut_chexiao").on("click", function() {
                                                $.post("<?php echo e(url('/api/user/backWeChatRealName')); ?>", {
                                                    token: token,
                                                    storeId: store_id,
                                                    waysType: e.ways_type
                                                }, function(res) {
                                                    if (res.status == 1) {
                                                        layer.msg(res.message, {
                                                            offset: '15px',
                                                            time: 2000
                                                        });
                                                    } else {
                                                        layer.msg(res.message, {
                                                            offset: '15px',
                                                            icon: 2,
                                                            time: 2000
                                                        });
                                                    }
                                                }, "json");
                                            });
                                        } else if (res.data.idenStatus == 6) {
                                            $('.realName_way').html("已冻结");
                                            $(".sumbut_rz").hide();
                                            $(".sumbut_cx").hide();
                                            $(".sumbut_chexiao").hide();
                                        } else if (res.data.idenStatus == 7) {
                                            $('.realName_way').html("已作废");
                                            $(".sumbut_rz").hide();
                                            $(".sumbut_cx").hide();
                                            $(".sumbut_chexiao").hide();
                                        } else if (res.data.idenStatus == 8) {
                                            $('.realName_way').html("重复认证");
                                            $(".sumbut_rz").hide();
                                            $(".sumbut_cx").hide();
                                            $(".sumbut_chexiao").hide();
                                        }
                                        $(".realName_code").html(res.data.infoQrcode)
                                        layer.msg(res.message, {
                                            offset: '15px',
                                            time: 2000
                                        });
                                    }, "json");
                                });
                                // });
                                // }
                                layer.msg(res.message, {
                                    offset: '15px',
                                    time: 2000
                                });
                            }, "json");
                        });
                        // }else if(res.data.idenStatus == 1){
                        //     $('.realName_way').html("审核通过");
                        //     $(".sumbut_rz").hide();
                        //     $(".sumbut_cx").show();

                        // }else if(res.data.idenStatus == 2){
                        //     $('.realName_way').html("待确认联系信息");
                        //     $(".sumbut_rz").hide();
                        // }else if(res.data.idenStatus == 3){
                        //     $('.realName_way').html("待账户验证");
                        //     $(".sumbut_rz").hide();
                        // }else if(res.data.idenStatus == 4){
                        //     $('.realName_way').html("审核中");
                        //     $(".sumbut_rz").hide();
                        // }else if(res.data.idenStatus == 5){
                        //     $('.realName_way').html("审核驳回");
                        //     $(".sumbut_rz").hide();
                        // }else if(res.data.idenStatus == 6){
                        //     $('.realName_way').html("已冻结");
                        //     $(".sumbut_rz").hide();
                        // }else if(res.data.idenStatus == 7){
                        //     $('.realName_way').html("已作废");
                        //     $(".sumbut_rz").hide();
                        // }else if(res.data.idenStatus == 8){
                        //     $('.realName_way').html("重复认证");
                        //     $(".sumbut_rz").hide();
                        // }
                        // },"json");
                        var openshua = layer.open({
                            type: 1,
                            title: false,
                            closeBtn: 0,
                            area: '516px',
                            skin: 'layui-layer-nobg', //没有背景色
                            shadeClose: true,
                            content: $('#sumbit_realName')
                        });
                    } else {
                        layer.msg("暂不支持", {
                            icon: 1,
                            shade: 0.5,
                            time: 1000
                        });
                    }

                } else if (layEvent === 'hltx') {
                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);
                    $('.ways_source').val(e.ways_source);
                    // $('.lianfushow').hide()

                    if (e.ways_source == "weixin") {
                        $('.wx_list').show();
                    } else {
                        $('.wx_list').hide();
                    }

                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: e.ways_type,
                        MerchantId: ''
                    }, function(res) {
                        //                        console.log(res);
                        if (res.status == 1) {
                            $('.hltx_wx_sub_merchant_id').val(res.data.MerchantId);
                            //                            console.log(e.ways_type);
                            //                            console.log(res.data.ali_merchant_no);
                            if (e.ways_type === 23001) {
                                $('.waynum').html(res.data.ali_merchant_no);
                            } else if (e.ways_type === 23002) {
                                $('.waynum').html(res.data.wx_merchant_no);
                            } else if (e.ways_type === 23003) {
                                $('.waynum').html(res.data.union_merchant_no);
                            }

                            $('.hltxqd').val(res.data.qd);
                            $("#hltx_qdopen_td").html('');

                            var optionStr = "";
                            if (res.data.qd == 1) {
                                optionStr += "<option value=''>选择渠道</option>";
                                optionStr += "<option value='1' selected>渠道1</option>";
                                optionStr += "<option value='2'>渠道2</option>";
                            } else if (res.data.qd == 2) {
                                //                                console.log('00000');
                                optionStr += "<option value=''>选择渠道</option>";
                                optionStr += "<option value='1'>渠道1</option>";
                                optionStr += "<option value='2' selected>渠道2</option>";
                            } else if (res.data.qd == null) {
                                //                                console.log('11111');
                                optionStr += "<option value=''>选择渠道</option>";
                                optionStr += "<option value='1'>渠道1</option>";
                                optionStr += "<option value='2'>渠道2</option>";
                            }

                            $("#hltx_qdopen_td").append(optionStr);
                            layui.form.render('select');
                        }
                    }, "json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#hltx_passway')
                    });
                } else if (layEvent === 'changsha') {
                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);
                    $('.ways_source').val(e.ways_source);
                    // $('.lianfushow').hide()

                    if (e.ways_source == "weixin") {
                        $('.changsha_list').show();
                    } else {
                        $('.changsha_list').hide();
                    }

                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: e.ways_type,
                        MerchantId: ''
                    }, function(res) {
                        //                    console.log(res);
                        if (res.status == 1) {
                            $('.changsha_wx_sub_merchant_id').val(res.data.MerchantId);
                            $('.store_num').val(res.data.DeptId);
                            $('.merchant_num').val(res.data.StaffId);
                        }
                    }, "json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#changsha_passway')
                    });
                } else if (layEvent === 'lianfuyoupay') {
                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);
                    $('.ways_source').val(e.ways_source);
                    // $('.lianfushow').hide()

                    if (e.ways_source == "weixin") {
                        $('.lianfuyoupay_list').show();
                    } else {
                        $('.lianfuyoupay_list').hide();
                    }

                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: e.ways_type,
                        MerchantId: ''
                    }, function(res) {
                        //                        console.log(res);
                        if (res.status == 1) {
                            $('.lianfuyoupay_merchant_id').val(res.data.MerchantId);
                            $('.lianfuyoupaykey').val(res.data.signkey);
                            $('.pos_sn').val(res.data.pos_sn);
                        }
                    }, "json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#lianfuyoupay_passway')
                    });
                } else if (layEvent === 'huipay') { //获取商户号
                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);

                    $.post("<?php echo e(url('/api/user/get_mer_id')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: e.ways_type
                    }, function(res) {
                        //                      console.log(res);
                        if (res.status == 1) {
                            $('.huipay_merchant_id').val(res.data.mer_id);
                            $('.huipay_inputs').val(res.data.device_id);
                            //$('.huipay_merchant_id').html(res.data.mer_id);
                            //$('.huipay_inputs').html(res.data.device_id);
                            //                          console.log(typeof(res.data.length));
                            if (res.data.length == 0) {
                                $('.huipay_show').hide();
                            } else {
                                $('.huipay_show').show();
                            }
                        }
                    }, "json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_huipay_passway')
                    });
                } else if (layEvent === 'easypay') { //获取商户号
                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);

                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: e.ways_type
                    }, function(res) {
                        //                      console.log(res);
                        if (res.status == 1) {
                            $('.easypay_mercode').val(res.data.easyPayMerCode);
                            $('.easypay_termcode').val(res.data.easyPayTermCode);
                            $('.easypay_operatrace').val(res.data.operaTrace);
                        }
                    }, "json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_easypay_passway')
                    });
                } else if (layEvent === 'wftpay') { //威富通-获取商户号
                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);

                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: e.ways_type
                    }, function(res) {
                        if (res.status == 1) {
                            $('.wftpay_mch_id').val(res.data.wftpayMchId);
                        }
                    }, "json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_wftpay_passway')
                    });
                } else if (layEvent === 'hwcpay') { //汇旺财-获取商户号
                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);

                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: e.ways_type
                    }, function(res) {
                        if (res.status == 1) {
                            $('.hwcpay_mch_id').val(res.data.hwcpayMchId);
                            $('.hwcpay_merchant_num').val(res.data.hwcpayMerchantNum);
                        }
                    }, "json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_hwcpay_passway')
                    });
                } else if (layEvent === 'openweixina') { //官方微信a
                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);
                    $('.ways_source').val(e.ways_source);

                    //发送请求
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: e.ways_type,
                        wx_sub_merchant_id: ''
                    }, function(res) {
                        console.log
                        if (res.status == 1) {
                            layui.use('form', function() {
                                var form = layui.form;

                                //商户号输入框赋值
                                $('.wxa_sub_merchant_id_shop').val(res.data.wx_sub_merchant_id);

                                //优惠商品ID输入框赋值
                                $('.wxa_shop').val(res.data.wx_shop_id);

                                form.render(); //更新全部
                            });
                        }
                    }, "json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_passway_weixina')
                    });
                } else if(layEvent === 'hkrt'){ //海科融通
                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);
                    $('.ways_source').val(e.ways_source);

                    //发送请求
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>",{
                        token:token
                        ,store_id:store_id
                        ,ways_type:e.ways_type
                    },function(res){
                        if(res.status==1){
                            layui.use('form', function(){
                                var form = layui.form;

                                $('.hkrt_merch_no').val(res.data.hkrtMerchNo);
                                $('.hkrt_agent_apply_no').val(res.data.hkrtAgentApplyNo);

                                form.render(); //更新全部
                            });
                        }
                    },"json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_hkrt_passway')
                    });
                } else if(layEvent === 'yinsheng'){ //银盛
                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);
                    $('.ways_source').val(e.ways_source);

                    //发送请求
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>",{
                        token:token
                        ,store_id:store_id
                        ,ways_type:e.ways_type
                    },function(res){
                        if(res.status==1){
                            layui.use('form', function(){
                                var form = layui.form;

                                $('.yinsheng_partner_id').val(res.data.yinshengPartnerId);
                                $('.yinsheng_seller_name').val(res.data.yinshengSellerName);
                                $('.yinsheng_business_code').val(res.data.yinshengBusinessCode);

                                form.render(); //更新全部
                            });
                        }
                    },"json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_yinsheng_passway')
                    });
                } else if(layEvent === 'qfpay'){ //钱方
                    $('.way').html(e.ways_desc);
                    $('.type').val(e.ways_type);
                    $('.ways_source').val(e.ways_source);

                    //发送请求
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>",{
                        token:token
                        ,store_id:store_id
                        ,ways_type:e.ways_type
                    },function(res){
                        if(res.status==1){
                            layui.use('form', function(){
                                var form = layui.form;
                                $('.qfpay_mchid').val(res.data.qfPayMchid);
                                form.render(); //更新全部
                            });
                        }
                    },"json");

                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        area: '516px',
                        skin: 'layui-layer-nobg', //没有背景色
                        shadeClose: true,
                        content: $('#open_qfpay_passway')
                    });
                }

            });

            $('.f_box').on('click', '.checkbox', function() {
                //            console.log($(this).attr('data'));
                //            console.log($(this).hasClass('layui-form-checked'));
                if ($(this).hasClass('layui-form-checked') == false) {
                    $(this).addClass("layui-form-checked");
                    $(this).attr('data', "01");
                    $(this).parent().attr('data', "01");
                } else {
                    $(this).removeClass("layui-form-checked");
                    $(this).attr('data', "00");
                    $(this).parent().attr('data', "00");
                }
            });

            form.on('select(open)', function(data) {
                category = data.value;
                categoryName = data.elem[data.elem.selectedIndex].text;
                $('.js').val(category);
            });

            form.on('select(openTf)', function(data) {
                category = data.value;
                categoryName = data.elem[data.elem.selectedIndex].text;
                $('.js_open_tf').val(category);
            });

            form.on('select(tf_qdopen)', function(data) {
                category = data.value;
                // categoryName = data.elem[data.elem.selectedIndex].text;
                $('.tfqd').val(category);
            });

            form.on('select(openhltx)', function(data) {
                category = data.value;
                categoryName = data.elem[data.elem.selectedIndex].text;
                $('.js_open_tf').val(category);
            });

            form.on('select(qdopen)', function(data) {
                category = data.value;
                categoryName = data.elem[data.elem.selectedIndex].text;
                $('.qd').val(category);
            });

            // HL通道  2020.7.1**********
            form.on('select(hltx_qdopen)', function(data) {
                category = data.value;
                // categoryName = data.elem[data.elem.selectedIndex].text;
                $('.hltxqd').val(category);
            });

            form.on('select(hltx_qdopen_td)', function(data) {
                category = data.value;
                // categoryName = data.elem[data.elem.selectedIndex].text;
                $('.hltxqd').val(category);
            });
            // HL通道**********

            // 2020.06.15
            form.on('select(passway1)', function(data) {
                val = data.value;
                valname = data.elem[data.elem.selectedIndex].text;
                $('.passway1').val(val);
            });

            form.on('select(passway2)', function(data) {
                val = data.value;
                valname = data.elem[data.elem.selectedIndex].text;
                $('.passway2').val(val);
            });

            form.on('select(passway3)', function(data) {
                val = data.value;
                valname = data.elem[data.elem.selectedIndex].text;
                $('.passway3').val(val);
            });

            $('.save_display').click(function() {
                $.post("<?php echo e(url('/api/user/store_ways_set')); ?>", {
                    token: token,
                    store_id: store_id,
                    ways_type: $('.passway_ways_type').val(),
                    company: $('.passway_company').val(),
                    is_close: $('.passway1').val(),
                    pcredit: $('.passway2').val(),
                    credit: $('.passway3').val(),
                    pay_amount_e: $('.dbxe').val()
                }, function(res) {
                    console.log(res);
                    if (res.status == 1) {
                        layer.msg(res.message, {
                            offset: '15px',
                            icon: 1,
                            time: 2000
                        }, function() {
                            window.location.reload();
                        });
                    } else {
                        layer.msg(res.message, {
                            offset: '15px',
                            icon: 2,
                            time: 3000
                        });
                    }
                }, "json");

            });

            // 修改费率
            $('.submit').click(function() {
                $.post("<?php echo e(url('/api/user/edit_store_rate')); ?>", {
                    token: token,
                    store_id: store_id,
                    ways_type: $('.type').val(),
                    rate: $('.rate').val()
                }, function(res) {

                    if (res.status == 1) {

                        $.post("/api/user/alterFunc",{
                            token:token,
                            storeId:store_id,
                            type:1,
                        },function(data){
                            console.log(data)
                            if(data.status == 1){
                                console.log("商户功能变更成功")
                            }else{
                                layer.msg(data.message, {icon:2, shade:0.5, time:2000});
                            }
                        })

                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 1,
                            time: 2000
                        }, function() {
                            window.location.reload();
                        });

                    } else {

                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 2,
                            time: 3000
                        });
                        
                    }
                }, "json");
                $('#edit_rate').css('display', 'none')
            });

            $('.submits').click(function() {
                $.post("<?php echo e(url('/api/user/edit_store_un_rate')); ?>", {
                    token: token,
                    store_id: store_id,
                    ways_type: $('.types').val(),
                    rate_e: $('.rate1').val(),
                    rate_f: $('.rate2').val(),
                    rate_f_top: $('.rate3').val()
                }, function(res) {
                    //                console.log(res);
                    if (res.status == 1) {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 1,
                            time: 1000
                        }, function() {
                            window.location.reload();
                        });
                    } else {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 2,
                            time: 1000
                        });
                    }
                }, "json");
            });

            $('.submitsao').click(function() {
                $.post("<?php echo e(url('/api/user/edit_store_unqr_rate')); ?>", {
                    token: token,
                    store_id: store_id,
                    ways_type: $('.types').val(),
                    rate_a: $('.rates1').val(),
                    rate_b: $('.rates2').val(),
                    rate_b_top: $('.rates3').val(),
                    rate_c: $('.rates4').val(),
                    rate_d: $('.rates5').val(),
                    rate_d_top: $('.rates6').val()
                }, function(res) {
                    //                console.log(res);
                    if (res.status == 1) {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 1,
                            time: 1000
                        }, function() {
                            window.location.reload();
                        });
                    } else {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 2,
                            time: 1000
                        });
                    }
                }, "json");
            });

            $('.save_newland').click(function() {
                if ($('.ways_source').val() == 'weixin') {
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: $('.type').val(),
                        nl_mercId: $('.wxmerchantda').val(),
                        nl_key: $('.wxmerchantdb').val(),
                        trmNo: $('.wxmerchantdc').val(),
                        weixin_path: $('.wx_path_newland').val(),
                        wx_sub_appid: $('.wxsubappid').val()
                    }, function(res) {
                        //                    console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                    offset: '50px',
                                    icon: 1,
                                    time: 2000
                                }
                                /*,function(){
                                                        window.location.reload();
                                                        }*/
                            );
                        } else {
                            layer.msg(res.message, {
                                offset: '50px',
                                icon: 2,
                                time: 3000
                            });
                        }
                    }, "json");
                } else {
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: $('.type').val(),
                        nl_mercId: $('.wxmerchantda').val(),
                        nl_key: $('.wxmerchantdb').val(),
                        trmNo: $('.wxmerchantdc').val()
                    }, function(res) {
                        //                    console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                    offset: '50px',
                                    icon: 1,
                                    time: 2000
                                }
                                /*,function(){
                                                        window.location.reload();
                                                        }*/
                            );
                        } else {
                            layer.msg(res.message, {
                                offset: '50px',
                                icon: 2,
                                time: 3000
                            });
                        }
                    }, "json");
                }
            });

            // 开通通道
            $('.open_way').click(function() {
                //            console.log($('.ways_source').val());
                if ($('.ways_source').val() == 'weixin') {
                    if ($('.type').val() == 2000) {
                        $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                            token: token,
                            store_id: store_id,
                            ways_type: $('.type').val(),
                            wx_sub_merchant_id: $('.wx_sub_merchant_id').val(),
                            weixin_path: $('.wx_path').val()
                        }, function(res) {
                            //                        console.log(res);
                            if (res.status == 1) {
                                layer.msg(res.message, {
                                    offset: '50px',
                                    icon: 1,
                                    time: 1000
                                }, function() {
                                    window.location.reload();
                                });
                            } else {
                                layer.msg(res.message, {
                                    offset: '50px',
                                    icon: 2,
                                    time: 3000
                                });
                            }
                        }, "json");
                    } else {
                        $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                            token: token,
                            store_id: store_id,
                            ways_type: $('.type').val(),
                            MerchantId: $('.wx_sub_merchant_id').val(),
                            weixin_path: $('.wx_path').val()
                        }, function(res) {
                            //                        console.log(res);
                            if (res.status == 1) {
                                layer.msg(res.message, {
                                    offset: '50px',
                                    icon: 1,
                                    time: 1000
                                }, function() {
                                    window.location.reload();
                                });
                            } else {
                                layer.msg(res.message, {
                                    offset: '50px',
                                    icon: 2,
                                    time: 3000
                                });
                            }
                        }, "json");
                    }
                } else {
                    if ($('.type').val() == 2000) {
                        $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                            token: token,
                            store_id: store_id,
                            ways_type: $('.type').val(),
                            wx_sub_merchant_id: $('.wx_sub_merchant_id').val()
                        }, function(res) {
                            //                        console.log(res);
                            if (res.status == 1) {
                                layer.msg(res.message, {
                                    offset: '50px',
                                    icon: 1,
                                    time: 1000
                                }, function() {
                                    window.location.reload();
                                });
                            } else {
                                layer.msg(res.message, {
                                    offset: '50px',
                                    icon: 2,
                                    time: 3000
                                });
                            }
                        }, "json");
                    } else {
                        $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                            token: token,
                            store_id: store_id,
                            ways_type: $('.type').val(),
                            MerchantId: $('.wx_sub_merchant_id').val()
                        }, function(res) {
                            //                        console.log(res);
                            if (res.status == 1) {
                                layer.msg(res.message, {
                                    offset: '50px',
                                    icon: 1,
                                    time: 1000
                                }, function() {
                                    window.location.reload();
                                });
                            } else {
                                layer.msg(res.message, {
                                    offset: '50px',
                                    icon: 2,
                                    time: 3000
                                });
                            }
                        }, "json");
                    }
                }
            });

            // 开通葫芦天下通道
            $('.hltx_way').click(function() {
                console.log($('.ways_source').val(), $('.type').val())
                if ($('.ways_source').val() == 'weixin') {
                    if ($('.type').val() == 2000) {
                        $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                            token: token,
                            store_id: store_id,
                            ways_type: $('.type').val(),
                            wx_sub_merchant_id: $('.hltx_wx_sub_merchant_id').val(),
                            weixin_path: $('.hltx_wx_path').val(),
                            qd: $('.qd').val()
                        }, function(res) {
                            console.log(res);
                            if (res.status == 1) {
                                layer.msg(res.message, {
                                    offset: '15px',
                                    icon: 1,
                                    time: 1000
                                }, function() {
                                    window.location.reload();
                                });
                            } else {
                                layer.msg(res.message, {
                                    offset: '15px',
                                    icon: 2,
                                    time: 3000
                                });
                            }
                        }, "json");

                    } else {
                        $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                            token: token,
                            store_id: store_id,
                            ways_type: $('.type').val(),
                            MerchantId: $('.hltx_wx_sub_merchant_id').val(),
                            weixin_path: $('.hltx_wx_path').val(),
                            qd: $('.qd').val()
                        }, function(res) {
                            console.log(res);
                            if (res.status == 1) {
                                layer.msg(res.message, {
                                    offset: '15px',
                                    icon: 1,
                                    time: 1000
                                }, function() {
                                    window.location.reload();
                                });
                            } else {
                                layer.msg(res.message, {
                                    offset: '15px',
                                    icon: 2,
                                    time: 3000
                                });
                            }
                        }, "json");
                    }
                } else {
                    if ($('.type').val() == 2000) {
                        $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                            token: token,
                            store_id: store_id,
                            ways_type: $('.type').val(),
                            wx_sub_merchant_id: $('.hltx_wx_sub_merchant_id').val(),
                            qd: $('.hltxqd').val()
                        }, function(res) {
                            console.log(res);
                            if (res.status == 1) {
                                layer.msg(res.message, {
                                    offset: '15px',
                                    icon: 1,
                                    time: 1000
                                }, function() {
                                    window.location.reload();
                                });
                            } else {
                                layer.msg(res.message, {
                                    offset: '15px',
                                    icon: 2,
                                    time: 3000
                                });
                            }
                        }, "json");

                    } else {
                        $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                            token: token,
                            store_id: store_id,
                            ways_type: $('.type').val(),
                            MerchantId: $('.hltx_wx_sub_merchant_id').val(),
                            qd: $('.hltxqd').val()
                        }, function(res) {
                            console.log(res);
                            if (res.status == 1) {
                                layer.msg(res.message, {
                                    offset: '15px',
                                    icon: 1,
                                    time: 1000
                                }, function() {
                                    window.location.reload();
                                });
                            } else {
                                layer.msg(res.message, {
                                    offset: '15px',
                                    icon: 2,
                                    time: 3000
                                });
                            }
                        }, "json");
                    }
                }

            });

            // 工行商户信息
            $('.lianfuButton').click(function() {
                if ($('.ways_source').val() == 'weixin') {
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: $('.type').val(),
                        MerchantId: $('.lianfu_sub_merchant_id').val(),
                        weixin_path: $('.lianfu_wx_path').val(),
                        signkey: $('.lianfukey').val()
                    }, function(res) {
                        console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '15px',
                                icon: 1,
                                time: 1000
                            }, function() {
                                window.location.reload();
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '15px',
                                icon: 2,
                                time: 3000
                            });
                        }
                    }, "json");
                } else {
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: $('.type').val(),
                        MerchantId: $('.lianfu_sub_merchant_id').val(),
                        signkey: $('.lianfukey').val()
                    }, function(res) {
                        console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '15px',
                                icon: 1,
                                time: 1000
                            }, function() {
                                window.location.reload();
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '15px',
                                icon: 2,
                                time: 3000
                            });
                        }
                    }, "json");
                }

            });

            //
            $('.save_tf').click(function() {
                if ($('.ways_source').val() == "weixin") {
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: $('.type').val(),
                        MerchantId: $('.tf_merchant_id').val(),
                        weixin_path: $('.wx_path_tf').val(),
                        qd: $('.tfqd').val()
                    }, function(res) {
                        //                    console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '50px',
                                icon: 1,
                                time: 1000
                            }, function() {
                                window.location.reload();
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '50px',
                                icon: 2,
                                time: 3000
                            });
                        }
                    }, "json");
                } else {
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: $('.type').val(),
                        MerchantId: $('.tf_merchant_id').val(),
                        qd: $('.tfqd').val()
                    }, function(res) {
                        //                    console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '50px',
                                icon: 1,
                                time: 1000
                            }, function() {
                                window.location.reload();
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '50px',
                                icon: 2,
                                time: 3000
                            });
                        }
                    }, "json");
                }
            });

            $('.open_jiesuan').click(function() {
                //loading层
                var index = layer.load(1, {
                    shade: [0.1, '#fff'] //0.1透明度的白色背景
                });

                $.post("<?php echo e(url('/api/basequery/openways')); ?>", {
                    token: token,
                    store_id: store_id,
                    ways_type: $('.type').val(),
                    settle_mode_type: $('.js').val()
                }, function(res) {
                    //                console.log(res);
                    layer.close(index);

                    if (res.status == 1) {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 1,
                            time: 1000
                        }, function() {
                            window.location.reload();
                        });
                    } else {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 2,
                            time: 3000
                        });
                    }
                }, "json");
            });

            $('.open_tf_jiesuan').click(function() {
                //loading层
                var index = layer.load(1, {
                    shade: [0.1, '#fff'] //0.1透明度的白色背景
                });

                $.post("<?php echo e(url('/api/basequery/openways')); ?>", {
                    token: token,
                    store_id: store_id,
                    ways_type: $('.type').val(),
                    settle_mode_type: $('.js_open_tf').val(),
                    qd: $('.qd').val()
                }, function(res) {
                    //                console.log(res);
                    layer.close(index);

                    if (res.status == 1) {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 1,
                            time: 1000
                        }, function() {
                            window.location.reload();
                        });
                    } else {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 2,
                            time: 3000
                        });
                    }
                }, "json");
            });

            $('.open_hltx_jiesuan').click(function() {
                //loading层
                var index = layer.load(1, {
                    shade: [0.1, '#fff'] //0.1透明度的白色背景
                });

                $.post("<?php echo e(url('/api/basequery/openways')); ?>", {
                    token: token,
                    store_id: store_id,
                    ways_type: $('.type').val(),
                    settle_mode_type: $('.js_open_tf').val(),
                    qd: $('.hltxqd').val()
                }, function(res) {
                    console.log(res);
                    layer.close(index)

                    if (res.status == 1) {
                        layer.msg(res.message, {
                            offset: '15px',
                            icon: 1,
                            time: 1000
                        }, function() {
                            window.location.reload();
                        });
                    } else {
                        layer.msg(res.message, {
                            offset: '15px',
                            icon: 2,
                            time: 3000
                        });
                    }
                }, "json");
            });

            /**
             * 点击 确定 按钮，提交商户号内容信息
             * 官方微信添加字段 wx_shop_id
             */
            $('.open_way_weixin').click(function(e) {
                //console.log($('.ways_source').val());
                e.preventDefault();

                var wx_sharing_open = $("#open_passway_weixin input[name=wx_sharing_open]:checked").val();

                var wx_sharing_relation_type = $("#open_passway_weixin input[name=wx_sharing_relation_type]:checked").val();

                var arr = [];
                $.each($('.wx_sharing_relation_type:checkbox:checked'), function() {
                    arr.push($(this).attr("as"));
                });
                if (arr.length > 0) {
                    var wx_sharing_relation_type = arr.join(",");
                } else {
                    var wx_sharing_relation_type = "";
                }

                var request_data = {
                    token: token,
                    store_id: store_id,
                    ways_type: $('.type').val(),
                    //商户号输入框赋值
                    wx_sub_merchant_id: $('.wx_sub_merchant_id_shop').val(),
                    //微信支付目录
                    weixin_path: $('.wx_path_shop').val(),
                    //优惠商品ID
                    wx_shop_id: $('.wx_shop').val(),
                    //开启分账
                    wxSharingOpen: wx_sharing_open,
                    //分账接收方类型
                    wxSharingType: $('.wx_sharing_type').val(),
                    //分账接收方帐号
                    wxSharingTccount: $('.wx_sharing_account').val(),
                    //分账接收方全称
                    wxSharingName: $('.wx_sharing_name').val(),
                    //与分账方的关系类型
                    wxSharingRelationType: wx_sharing_relation_type,
                    //设置比例
                    wxSharingRate: $('.wx_sharing_proportion').val(),
                };

                if ($('.ways_source').val() == 'weixin') {
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", request_data, function(res) {
                        console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '15px',
                                icon: 1,
                                time: 1000
                            }, function() {
                                window.location.reload();
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '15px',
                                icon: 2,
                                time: 3000
                            });
                        }
                    }, "json");
                }
            });

            //官方微信a 提交分账
            $('.open_way_weixina').click(function(e) {
                //console.log($('.ways_source').val());
                e.preventDefault();

                var wxa_sharing_open = $("#open_passway_weixina input[name=wxa_sharing_open]:checked").val();

                var wxa_sharing_relation_type = $("#open_passway_weixina input[name=wxa_sharing_relation_type]:checked").val();

                var arr = [];
                $.each($('.wxa_sharing_relation_type:checkbox:checked'), function() {
                    arr.push($(this).attr("as"));
                });
                if (arr.length > 0) {
                    var wxa_sharing_relation_type = arr.join(",");
                } else {
                    var wxa_sharing_relation_type = "";
                }

                var request_data = {
                    token: token,
                    store_id: store_id,
                    ways_type: $('.type').val(),
                    //商户号输入框赋值
                    wx_sub_merchant_id: $('.wxa_sub_merchant_id_shop').val(),
                    //微信支付目录
                    weixin_path: $('.wxa_path_shop').val(),
                    //优惠商品ID
                    wx_shop_id: $('.wxa_shop').val(),
                    //开启分账
                    wxSharingOpen: wxa_sharing_open,
                    //分账接收方类型
                    wxSharingType: $('.wxa_sharing_type').val(),
                    //分账接收方帐号
                    wxSharingTccount: $('.wxa_sharing_account').val(),
                    //分账接收方全称
                    wxSharingName: $('.wxa_sharing_name').val(),
                    //与分账方的关系类型
                    wxSharingRelationType: wxa_sharing_relation_type,
                    //设置比例
                    wxSharingRate: $('.wxa_sharing_proportion').val(),
                };

                if ($('.ways_source').val() == 'weixin') {
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", request_data, function(res) {
                        //                    console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '15px',
                                icon: 1,
                                time: 1000
                            }, function() {
                                window.location.reload();
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '15px',
                                icon: 2,
                                time: 3000
                            });
                        }
                    }, "json");
                }
            });

            //
            // $('.save_tf').click(function() {
            //     if ($('.ways_source').val() == "weixin") {
            //         $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
            //             token: token,
            //             store_id: store_id,
            //             ways_type: $('.type').val(),
            //             MerchantId: $('.tf_merchant_id').val(),
            //             weixin_path: $('.wx_path_tf').val(),
            //             qd: $('.tfqd').val()
            //         }, function(res) {
            //             //                    console.log(res);
            //             if (res.status == 1) {
            //                 layer.msg(res.message, {
            //                     offset: '50px',
            //                     icon: 1,
            //                     time: 1000
            //                 }, function() {
            //                     window.location.reload();
            //                 });
            //             } else {
            //                 layer.msg(res.message, {
            //                     offset: '50px',
            //                     icon: 2,
            //                     time: 3000
            //                 });
            //             }
            //         }, "json");
            //     } else {
            //         $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
            //             token: token,
            //             store_id: store_id,
            //             ways_type: $('.type').val(),
            //             MerchantId: $('.tf_merchant_id').val(),
            //             qd: $('.tfqd').val()
            //         }, function(res) {
            //             //                    console.log(res);
            //             if (res.status == 1) {
            //                 layer.msg(res.message, {
            //                     offset: '50px',
            //                     icon: 1,
            //                     time: 1000
            //                 }, function() {
            //                     window.location.reload();
            //                 });
            //             } else {
            //                 layer.msg(res.message, {
            //                     offset: '50px',
            //                     icon: 2,
            //                     time: 3000
            //                 });
            //             }
            //         }, "json");
            //     }
            // });

            // $('.open_jiesuan').click(function() {
            //     //loading层
            //     var index = layer.load(1, {
            //         shade: [0.1, '#fff'] //0.1透明度的白色背景
            //     });

            //     $.post("<?php echo e(url('/api/basequery/openways')); ?>", {
            //         token: token,
            //         store_id: store_id,
            //         ways_type: $('.type').val(),
            //         settle_mode_type: $('.js').val()
            //     }, function(res) {
            //         //                console.log(res);
            //         layer.close(index);

            //         if (res.status == 1) {
            //             layer.msg(res.message, {
            //                 offset: '50px',
            //                 icon: 1,
            //                 time: 1000
            //             }, function() {
            //                 window.location.reload();
            //             });
            //         } else {
            //             layer.msg(res.message, {
            //                 offset: '50px',
            //                 icon: 2,
            //                 time: 3000
            //             });
            //         }
            //     }, "json");
            // });

            // $('.open_tf_jiesuan').click(function() {
            //     //loading层
            //     var index = layer.load(1, {
            //         shade: [0.1, '#fff'] //0.1透明度的白色背景
            //     });

            //     $.post("<?php echo e(url('/api/basequery/openways')); ?>", {
            //         token: token,
            //         store_id: store_id,
            //         ways_type: $('.type').val(),
            //         settle_mode_type: $('.js_open_tf').val(),
            //         qd: $('.qd').val()
            //     }, function(res) {
            //         //                console.log(res);
            //         layer.close(index);

            //         if (res.status == 1) {
            //             layer.msg(res.message, {
            //                 offset: '50px',
            //                 icon: 1,
            //                 time: 1000
            //             }, function() {
            //                 window.location.reload();
            //             });
            //         } else {
            //             layer.msg(res.message, {
            //                 offset: '50px',
            //                 icon: 2,
            //                 time: 3000
            //             });
            //         }
            //     }, "json");
            // });

            // $('.open_hltx_jiesuan').click(function() {
            //     //loading层
            //     var index = layer.load(1, {
            //         shade: [0.1, '#fff'] //0.1透明度的白色背景
            //     });

            //     $.post("<?php echo e(url('/api/basequery/openways')); ?>", {
            //         token: token,
            //         store_id: store_id,
            //         ways_type: $('.type').val(),
            //         settle_mode_type: $('.js_open_tf').val(),
            //         qd: $('.hltxqd').val()
            //     }, function(res) {
            //         console.log(res);
            //         layer.close(index)

            //         if (res.status == 1) {
            //             layer.msg(res.message, {
            //                 offset: '15px',
            //                 icon: 1,
            //                 time: 1000
            //             }, function() {
            //                 window.location.reload();
            //             });
            //         } else {
            //             layer.msg(res.message, {
            //                 offset: '15px',
            //                 icon: 2,
            //                 time: 3000
            //             });
            //         }
            //     }, "json");
            // });

            $('.open_ways').click(function() {
                if ($('.ways_source').val() == 'weixin') {
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: $('.type').val(),
                        h_mid: $('.wx_sub_hrt').val(),
                        weixin_path: $('.wx_path_hrt').val()
                    }, function(res) {
                        //                    console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '50px',
                                icon: 1,
                                time: 1000
                            }, function() {
                                window.location.reload();
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '50px',
                                icon: 2,
                                time: 3000
                            });
                        }
                    }, "json");
                } else {
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: $('.type').val(),
                        h_mid: $('.wx_sub_hrt').val()
                    }, function(res) {
                        //                    console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '50px',
                                icon: 1,
                                time: 1000
                            }, function() {
                                window.location.reload();
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '50px',
                                icon: 2,
                                time: 3000
                            });
                        }
                    }, "json");
                }
            });

            $('.open_alipay_store_id').click(function() {
                $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                    token: token,
                    store_id: store_id,
                    ways_type: $('.alipaytype').val(),
                    alipay_store_id: $('.wx_sub_alipay').val(),
                    out_store_id: $('.wx_sub_alipay_wai').val()
                }, function(res) {
                    //                console.log(res);
                    if (res.status == 1) {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 1,
                            time: 1000
                        }, function() {
                            window.location.reload();
                        });
                    } else {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 2,
                            time: 3000
                        });
                    }
                }, "json");
            });

            $('.open_ltf_save').click(function() {
                $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                    token: token,
                    store_id: store_id,
                    ways_type: $('.ltf_type').val(),
                    merchantCode: $('.ltf1').val(),
                    md_key: $('.ltf2').val(),
                    appId: $('.ltf3').val()
                }, function(res) {
                    //                console.log(res);
                    if (res.status == 1) {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 1,
                            time: 1000
                        }, function() {
                            window.location.reload();
                        });
                    } else {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 2,
                            time: 3000
                        });
                    }
                }, "json");
            });

            $('.sxf_save').click(function() {
                if ($('.ways_source').val() == "weixin") {
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: $('.type').val(),
                        MerchantId: $('.sxf_input1').val(),
                        wx_channel_appid: $('.sxf_input2').val(),
                        weixin_path: $('.wx_path_sxf').val()
                    }, function(res) {
                        //                    console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '50px',
                                icon: 1,
                                time: 1000
                            }, function() {
                                window.location.reload();
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '50px',
                                icon: 2,
                                time: 3000
                            });
                        }
                    }, "json");
                } else {
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: $('.type').val(),
                        MerchantId: $('.sxf_input1').val(),
                        wx_channel_appid: $('.sxf_input2').val()
                    }, function(res) {
                        //                    console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '50px',
                                icon: 1,
                                time: 1000
                            }, function() {
                                window.location.reload();
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '50px',
                                icon: 2,
                                time: 3000
                            });
                        }
                    }, "json");
                }
            });

            $('.changsha_save').click(function() {
                console.log($('.ways_source').val())
                if ($('.ways_source').val() == 'weixin') {
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: $('.type').val(),
                        MerchantId: $('.changsha_wx_sub_merchant_id').val(),
                        weixin_path: $('.changsha_wx_path').val(),
                        DeptId: $('.store_num').val(),
                        StaffId: $('.merchant_num').val()
                    }, function(res) {
                        console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '15px',
                                icon: 1,
                                time: 1000
                            }, function() {
                                window.location.reload();
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '15px',
                                icon: 2,
                                time: 3000
                            });
                        }
                    }, "json");

                } else {
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: $('.type').val(),
                        MerchantId: $('.changsha_wx_sub_merchant_id').val(),
                        // weixin_path:$('.changsha_wx_path').val(),
                        DeptId: $('.store_num').val(),
                        StaffId: $('.merchant_num').val()
                    }, function(res) {
                        console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '15px',
                                icon: 1,
                                time: 1000
                            }, function() {
                                window.location.reload();
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '15px',
                                icon: 2,
                                time: 3000
                            });
                        }
                    }, "json");
                }

            });

            $('.lianfuyoupay_save').click(function() {
                //            console.log($('.ways_source').val());
                if ($('.ways_source').val() == 'weixin') {
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: $('.type').val(),
                        MerchantId: $('.lianfuyoupay_merchant_id').val(),
                        weixin_path: $('.lianfuyoupay_wx_path').val(),
                        pos_sn: $('.pos_sn').val(),
                        signkey: $('.lianfuyoupaykey').val()
                    }, function(res) {
                        //                        console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '15px',
                                icon: 1,
                                time: 1000
                            }, function() {
                                window.location.reload();
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '15px',
                                icon: 2,
                                time: 3000
                            });
                        }
                    }, "json");

                } else {
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: $('.type').val(),
                        MerchantId: $('.lianfuyoupay_merchant_id').val(),
                        // weixin_path:$('.lianfuyoupay_wx_path').val(),
                        pos_sn: $('.pos_sn').val(),
                        signkey: $('.lianfuyoupaykey').val()
                    }, function(res) {
                        //                        console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '15px',
                                icon: 1,
                                time: 1000
                            }, function() {
                                window.location.reload();
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '15px',
                                icon: 2,
                                time: 3000
                            });
                        }
                    }, "json");
                }

            });

            $('.sort').click(function() {
                $(this).attr('lay-href', "<?php echo e(url('/user/passwaysort?')); ?>" + store_id);
            });

            $('.open_dlb_save').click(function() {
                if ($('ways_source').val() == "weixin") {
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: $('.dlb_type').val(),
                        mch_num: $('.dlb_inputs').eq(0).val(),
                        shop_num: $('.dlb_inputs').eq(1).val(),
                        machine_num: $('.dlb_inputs').eq(2).val(),
                        weixin_path: $('.wx_path_dlb').val()
                    }, function(res) {
                        //                    console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '50px',
                                icon: 1,
                                time: 1000
                            }, function() {
                                window.location.reload();
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '50px',
                                icon: 2,
                                time: 3000
                            });
                        }
                    }, "json");
                } else {
                    $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                        token: token,
                        store_id: store_id,
                        ways_type: $('.dlb_type').val(),
                        mch_num: $('.dlb_inputs').eq(0).val(),
                        shop_num: $('.dlb_inputs').eq(1).val(),
                        machine_num: $('.dlb_inputs').eq(2).val()
                    }, function(res) {
                        //                    console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '50px',
                                icon: 1,
                                time: 1000
                            }, function() {
                                window.location.reload();
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '50px',
                                icon: 2,
                                time: 3000
                            });
                        }
                    }, "json");
                }
            });

            //汇付-绑定机具
            $('.save_huipay').click(function() {
                $.post("<?php echo e(url('/api/user/hui_pay_bind')); ?>", {
                    token: token,
                    store_id: store_id,
                    ways_type: $('.type').val(),
                    device_id: $('.huipay_inputs').val()
                }, function(res) {
                    console.log(res);
                    if (res.status == 1) {
                        layer.msg(res.message, {
                            offset: '15px',
                            icon: 1,
                            time: 1000
                        }, function() {
                            window.location.reload();
                        });
                    } else {
                        layer.msg(res.message, {
                            offset: '15px',
                            icon: 2,
                            time: 3000
                        });
                    }
                }, "json");
            });

            $('.easypay_save').click(function() {
                $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                    token: token,
                    store_id: store_id,
                    ways_type: $('.type').val(),
                    easyPayMerCode: $('.easypay_mercode').val(),
                    easyPayTermCode: $('.easypay_termcode').val(),
                    easyPayOperaTrace: $('.easypay_operatrace').val()
                }, function(res) {
                    if (res.status == 1) {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 1,
                            time: 1000
                        }, function() {
                            window.location.reload();
                        });
                    } else {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 2,
                            time: 3000
                        });
                    }
                }, "json");
            });

            $('.wftpay_save').click(function() {
                $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                    token: token,
                    store_id: store_id,
                    ways_type: $('.type').val(),
                    wftpayMchId: $('.wftpay_mch_id').val()
                }, function(res) {
                    if (res.status == 1) {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 1,
                            time: 1000
                        }, function() {
                            window.location.reload();
                        });
                    } else {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 2,
                            time: 3000
                        });
                    }
                }, "json");
            });

            $('.hwcpay_save').click(function() {
                $.post("<?php echo e(url('/api/user/open_ways_type')); ?>", {
                    token: token,
                    store_id: store_id,
                    ways_type: $('.type').val(),
                    hwcpayMchId: $('.hwcpay_mch_id').val(),
                    hwcpayMerchantNum: $('.hwcpay_merchant_num').val()
                }, function(res) {
                    if (res.status == 1) {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 1,
                            time: 1000
                        }, function() {
                            window.location.reload();
                        });
                    } else {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 2,
                            time: 3000
                        });
                    }
                }, "json");
            });

            $('.hkrt_save').click(function(){
                $.post("<?php echo e(url('/api/user/open_ways_type')); ?>",
                    {
                        token:token
                        ,store_id:store_id
                        ,ways_type:$('.type').val()
                        ,hkrtMerchNo:$('.hkrt_merch_no').val()
                        ,hkrtAgentApplyNo:$('.hkrt_agent_apply_no').val()
                    },function(res){
                        if(res.status == 1){
                            layer.msg(res.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 1000
                            },function(){
                                window.location.reload();
                            });
                        }else{
                            layer.msg(res.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 3000
                            });
                        }
                    },"json");
            });

            $('.yinsheng_save').click(function(){
                $.post("<?php echo e(url('/api/user/open_ways_type')); ?>",
                    {
                        token:token
                        ,store_id:store_id
                        ,ways_type:$('.type').val()
                        ,yinshengPartnerId:$('.yinsheng_partner_id').val()
                        ,yinshengSellerName:$('.yinsheng_seller_name').val()
                        ,yinshengBusinessCode:$('.yinsheng_business_code').val()
                    },function(res){
                        if(res.status == 1){
                            layer.msg(res.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 1000
                            },function(){
                                window.location.reload();
                            });
                        }else{
                            layer.msg(res.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 3000
                            });
                        }
                    },"json");
            });

            $('.qfpay_save').click(function(){
                $.post("<?php echo e(url('/api/user/open_ways_type')); ?>",
                    {
                        token:token
                        ,store_id:store_id
                        ,ways_type:$('.type').val()
                        ,qfPayMchid:$('.qfpay_mchid').val()
                    },function(res){
                        if(res.status == 1){
                            layer.msg(res.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 1000
                            },function(){
                                window.location.reload();
                            });
                        }else{
                            layer.msg(res.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 3000
                            });
                        }
                    },"json");
            });

            $('.f_box').on('click', '.open_f_save', function() {
                //            console.log($(this).siblings('.f_block').attr('data'));
                //	      console.log($(this).siblings('.f_status').attr('data'));
                //            console.log($(this).siblings().find('.f_inputs').val());
                $.post("<?php echo e(url('/api/alipayopen/set_store_fq_rate')); ?>", {
                    token: token,
                    store_id: store_id,
                    num: $(this).siblings('.f_block').attr('data'),
                    rate: $(this).siblings().find('.f_inputs').val(),
                    status: $(this).siblings('.f_status').attr('data')
                }, function(res) {
                    //                console.log(res);
                    if (res.status == 1) {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 1,
                            time: 1000
                        }, function() {
                            window.location.reload();
                        });
                    } else {
                        layer.msg(res.message, {
                            offset: '50px',
                            icon: 2,
                            time: 3000
                        });
                    }
                }, "json");
            });

            //返还设置
            $('#edit_flower').on('click', '.returnSettings', function() {
                $(this).attr("lay-href", "<?php echo e(url('/user/returnSettings?')); ?>" + store_id);
                //      console.log('0000');
            });
			
        });
    </script>

</body>

</html>