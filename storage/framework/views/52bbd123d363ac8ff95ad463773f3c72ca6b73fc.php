<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>子商户列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .cur {
            color: #009688;
        }

        .del {
            background-color: #e85052;
        }

        /*.laytable-cell-1-school_icon{height:100%;}*/
        .yname {
            font-size: 13px;
            color: #444;
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">

            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12" style="margin-top:0px">
                        <div class="layui-card">
                            <div class="layui-card-header">子商户列表</div>

                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">
                                    <div style="font-size:14px">

                                        <div class="layui-form" lay-filter="component-form-group"
                                             style="width:190px;margin-right:0;display: inline-block;">
                                            <div class="layui-form-item">

                                                <div class="layui-input-block"
                                                     style="margin-left:5px;border-radius:5px">
                                                    <text class="yname">门店名称</text>
                                                    <input type="text" style="border-radius:5px" name="schoolname"
                                                           lay-verify="schoolname" autocomplete="off"
                                                           placeholder="请输入门店名称" class="layui-input inputstore">
                                                    <div class="storebox" style='display: none'></div>
                                                </div>
                                                <div class="layui-form-item">
                                                    <div class="layui-input-block"
                                                         style="margin-left:5px;border-radius:5px">
                                                        <button class="layui-btn addSonStoreWeiXin"
                                                                style="border-radius:5px;margin-top:20px;margin-bottom: 4px;height:36px;line-height: 36px;">
                                                            新增
                                                        </button>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                <script type="text/html" id="table-content-list">
                                    <a class="layui-btn layui-btn-normal layui-btn-xs edit" lay-event="query">查询创建结果</a>
                                    <a class="layui-btn layui-btn-normal layui-btn-xs update" lay-event="update">修改</a>
                                    <a class="layui-btn layui-btn-normal layui-btn-xs memberCard" style="background-color:darkgreen" lay-event="memberCard">新增会员卡</a>
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<input type="hidden" class="user_id">
<input type="hidden" class="status">
<!-- 操作按钮 -->
<!--

<a class="layui-btn layui-btn-normal layui-btn-xs see" lay-event="detail">查看</a> -->
<script type="text/html" id="company_status">
    {{#  if(d.status =='CHECKING'){ }}
    审核中
    {{#  } else if(d.status =='APPROVED'){ }}
    已成功
    {{#  } else if(d.status =='REJECTED'){ }}
    被驳回
    {{#  } else { }}
    协议已过期
    {{#  } }}
</script>
<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'table', 'laydate'], function () {
        var $ = layui.$
            , admin = layui.admin
            , table = layui.table
            , form = layui.form
            , laydate = layui.laydate;


        // 未登录,跳转登录页面
        $(document).ready(function () {
            if (token == null) {
                window.location.href = "<?php echo e(url('/user/login')); ?>";
            }
        });
        $('.addSonStoreWeiXin').click(function () {
            sessionStorage.setItem('store_id', '0');
            $(this).attr('lay-href', "<?php echo e(url('/user/addSonStoreWeiXin')); ?>");
        });

        table.render({
            elem: '#test-table-page'
            , url: "<?php echo e(url('/api/member/sonStoreWeiXinList')); ?>"
            , method: 'post'
            , where: {
                token: token,
            }
            , request: {
                pageName: 'p',
                limitName: 'l'
            }
            , page: true
            , cellMinWidth: 150
            , cols: [[
                {field: 'store_id', title: '门店id'}
                , {field: 'store_name', title: '门店名称'}
                , {field: 'merchant_id', title: '子商户id'}
                , {align: 'center', field: 'status', title: '审核状态', toolbar: '#company_status'}
                , {field: 'created_at', title: '创建时间'}
                , {field: 'updated_at', title: '修改时间'}
                , {width: 300, align: 'center', fixed: 'right', toolbar: '#table-content-list', title: '操作'}
            ]]
            , response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                , statusCode: 1 //成功的状态码，默认：0
                , msgName: 'message' //状态信息的字段名称，默认：msg
                , countName: 't' //数据总数的字段名称，默认：count
                , dataName: 'data' //数据列表的字段名称，默认：data
            }
            , done: function (res, curr, count) {
                $('th').css({
                    'font-weight': 'bold',
                    'font-size': '15',
                    'color': 'black',
                    'background': 'linear-gradient(#f2f2f2,#cfcfcf)'
                });	//进行表头样式设置
            }

        });


        table.on('tool(test-table-page)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var e = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象


            if (layEvent === 'query') {
                $.post("<?php echo e(url('/api/member/getSonStore')); ?>",
                    {
                        token: token
                        , store_id: e.store_id
                        , select_type: '2'
                    }, function (res) {
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 1
                                , time: 2000
                            });
                            table.reload('test-table-page', {
                                where: {
                                    type: type
                                }
                                , page: {
                                    curr: 1 //重新从第 1 页开始
                                }
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 2
                                , time: 3000
                            });
                        }
                    }, "json");
            }  else if (layEvent === 'update') {
                sessionStorage.setItem('store_id', e.store_id);
                $('.update').attr('lay-href', "<?php echo e(url('/user/editSonStoreWeiXin')); ?>");
            }else if (layEvent === 'memberCard') {
                sessionStorage.setItem('store_id', e.store_id);
                sessionStorage.setItem('store_name', e.store_name);
                sessionStorage.setItem('logo_url', e.logo_url);
                sessionStorage.setItem('card_id', '');
                $('.memberCard').attr('lay-href', "<?php echo e(url('/user/memberCard')); ?>");
            }

        });


        // 选择学校
        form.on('select(schooltype)', function (data) {
            var type = data.value;

            //执行重载
            table.reload('test-table-page', {
                where: {
                    type: type
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });


        //监听搜索
        form.on('submit(LAY-app-contlist-search)', function (data) {
            var value = data.field.id;
            //执行重载
            table.reload('test-table-page', {
                where: {
                    title: value
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });


    });


</script>

</body>
</html>