<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>会员卡设置</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style>
        .bj {
            background: url('<?php echo e(asset('/user/img/phone_bgpng.png')); ?>') no-repeat;
            width: 400px;
            height: 800px;
            background-size: contain;
            margin: 0 auto;
        }

        .bj_top {
            height: 30px;
            padding-top: 60px;
            width: 350px;
            margin: 0 auto
        }

        .card_top {
            width: 350px;
            height: 140px;
            background-color: #63b359;
            border-radius: 18px;
            color: #fff;
            margin: 0 auto
        }

        .card_bu {
            width: 350px;
            margin: 0 auto;
            text-align: center;
            padding-top: 20px;
            border-bottom: 1px #ccc solid;
            height: 50px;
            padding-bottom: 5px
        }

        .card_title {
            width: 350px;
            margin-top: 150px;
            margin-left: 30px;
            height: 50px;
            padding-bottom: 5px
        }

        .logo {
            height: 50px;
            width: 50px;
            border: 1px #fff solid;
            background: #34b9ed;
            border-radius: 50%;
            position: absolute;
            top: 120px;
            left: 60px;
        }

        .logo_img {
            width: 60px;
            height: 60px;
            border-radius: 50%;
            max-width: 100%;
            max-height: 100%;
        }

        .bj .name p:last-child {
            font-size: 14px;
            margin-top: 2px;
        }

        .min_img {
            width: 60px;
            height: 70px;
            margin-right: 10px;
            float: left;
            overflow: hidden;
        }

        .min_img img {
            width: 30px;
            height: 30px;
            margin-right: 5px;
        }

        .layui-form-label span {
            color: red;
            padding-right: 5px;
        }

        .layui-form-label {
            width: 100px;
        }

        .layui-input-block .layui-input {
            width: 90%;
        }

        .layui-input-block .layui-textarea {
            width: 90%;
        }

        .fist-msg {
            position: absolute;
            margin-top: 22px;
            margin-left: 85px;
            text-align: left;
        }

        .layui-form-mid {
            margin-left: 30px;
        }
        .up {
            position: relative;
            display: inline-block;
            cursor: pointer;
            border-color: #1ab394;
            color: #FFF;
            width: auto !important;
            font-size: 10px !important;
            text-align: center !important;
        }

        .up input {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            opacity: .01;
            width: 100px;
            height: 30px;
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">

            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12">
                        <div class="layui-card">
                            <div class="layui-card-header">模板设置</div>

                            <div class="layui-card-body">

                                <div class="layui-row">
                                    <div style="float: left ;width: 400px">
                                        <div class="bj">
                                            <div class="bj_top">
                                                <image style="max-height: 100%;max-width: 100%"
                                                       src="<?php echo e(asset('/user/img/time.png')); ?>"></image>
                                            </div>
                                            <div class="card_top">
                                                <div class="logo">
                                                    <image class="logo_img"></image>
                                                </div>
                                                <p style="margin-top:100px;margin-left:20px;position: absolute; font-size: 15px">
                                                    0000-0000-0000</p>
                                                <div class="fist-msg">
                                                    <h3 class="brand_name"></h3>
                                                    <h4 id="title"></h4>
                                                </div>
                                            </div>
                                            <div class="card_bu">
                                                <p>积分</p>
                                                <span style="text-align: center;font-size: 15px;color: #0bb20c">0</span>
                                            </div>
                                            <div class="card_title">
                                                <ul>
                                                    <li style="border-bottom: 1px #ccc solid;padding-bottom: 5px">
                                                        <span style="padding-right: 250px;">会员卡详情</span>
                                                        <i class="layui-icon layui-icon-right"></i>
                                                    </li>
                                                    <li style="border-bottom: 1px #ccc solid ;padding-bottom: 5px;padding-top: 5px">
                                                        <span style="padding-right: 250px">进入公众号</span>
                                                        <i class="layui-icon layui-icon-right"></i>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                    <div style="float: left">
                                        <!-- 选择业务员 -->
                                        <div class="layui-form">
                                            <div class="layui-form-item"
                                                 style="width:200px;display: inline-block;float:left;font-size:26px;">
                                                <label class="layui-form-label" style="width:200px;text-align: left;">会员卡功能</label>
                                            </div>
                                            <div class="layui-form-item">
                                                <label class="layui-form-label">使用状态</label>
                                                <div class="layui-input-block">
                                                    <input class="kaiguan" type="checkbox" checked lay-skin="switch"
                                                           id="status"
                                                           lay-filter="kaiguan" value='1'>
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <label class="layui-form-label"><span>*</span>商户名称:</label>
                                                <div class="layui-input-block">
                                                    <input type="text" class="layui-input store_name">
                                                    <span style="color: red">商户名称不能超过12个汉字</span>
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <label class="layui-form-label"><span>*</span>会员卡logo:</label>
                                                <div class="layui-input-block"
                                                     style="width: 75%;float: left;display: inline-block;margin-left: 0px;">
                                                    <input type="text" class="layui-input logo_url" disabled
                                                           style="width: 100%">
                                                    <span style="color: red">建议上传像素300*300</span>
                                                </div>
                                                <div class="layui-upload" style="float:right;width: 5%;">
                                                    <button class="layui-btn up" style="border-radius:5px;left:6px">
                                                        <input type="file" name="img_upload" class="upload_logo">上传
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <label class="layui-form-label"><span>*</span>会员卡封面:</label>
                                                <div class="layui-input-block" id="temple"
                                                     style="height: 60px;width: 100%">
                                                    <div>
                                                        <div class="min_img">
                                                            <img style="background-color: #63b359">
                                                            <input class="test1" type="radio" name="color"
                                                                   value="Color010"
                                                                   checked lay-filter="color">
                                                        </div>
                                                        <div class="min_img">
                                                            <img style="background-color: #2c9f67">
                                                            <input class="test1" type="radio" name="color"
                                                                   value="Color020"
                                                                   lay-filter="color">
                                                        </div>
                                                        <div class="min_img">
                                                            <img style="background-color: #509fc9">
                                                            <input class="test1" type="radio" name="color"
                                                                   value="Color030"
                                                                   lay-filter="color">
                                                        </div>
                                                        <div class="min_img">
                                                            <img style="background-color: #5885cf">
                                                            <input class="test1" type="radio" name="color"
                                                                   value="Color040"
                                                                   lay-filter="color">
                                                        </div>
                                                        <div class="min_img">
                                                            <img style="background-color: #9062c0">
                                                            <input class="test1" type="radio" name="color"
                                                                   value="Color050"
                                                                   lay-filter="color">
                                                        </div>
                                                        <div class="min_img">
                                                            <img style="background-color: #9062c0">
                                                            <input class="test1" type="radio" name="color"
                                                                   value="Color050"
                                                                   lay-filter="color">
                                                        </div>
                                                        <div class="min_img">
                                                            <img style="background-color: #d09a45">
                                                            <input class="test1" type="radio" name="color"
                                                                   value="Color060"
                                                                   lay-filter="color">
                                                        </div>


                                                    </div>
                                                    <div style="padding-left: 20px">
                                                        <div class="min_img">
                                                            <img style="background-color: #e4b138">
                                                            <input class="test1" type="radio" name="color"
                                                                   value="Color070"
                                                                   lay-filter="color">
                                                        </div>
                                                        <div class="min_img">
                                                            <img style="background-color: #ee903c">
                                                            <input class="test1" type="radio" name="color"
                                                                   value="Color080"
                                                                   lay-filter="color">
                                                        </div>
                                                        <div class="min_img">
                                                            <img style="background-color: #f08500">
                                                            <input class="test1" type="radio" name="color"
                                                                   value="Color081"
                                                                   lay-filter="color">
                                                        </div>
                                                        <div class="min_img">
                                                            <img style="background-color: #a9d92d">
                                                            <input class="test1" type="radio" name="color"
                                                                   value="Color082"
                                                                   lay-filter="color">
                                                        </div>
                                                        <div class="min_img">
                                                            <img style="background-color: #dd6549">
                                                            <input class="test1" type="radio" name="color"
                                                                   value="Color090"
                                                                   lay-filter="color">
                                                        </div>
                                                        <div class="min_img">
                                                            <img style="background-color: #cc463d">
                                                            <input class="test1" type="radio" name="color"
                                                                   value="Color100"
                                                                   lay-filter="color">
                                                        </div>
                                                        <div class="min_img">
                                                            <img style="background-color: #cf3e36">
                                                            <input class="test1" type="radio" name="color"
                                                                   value="Color101"
                                                                   lay-filter="color">
                                                        </div>
                                                        <div class="min_img">
                                                            <img style="background-color: #5E6671">
                                                            <input class="test1" type="radio" name="color"
                                                                   value="Color102"
                                                                   lay-filter="color">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <label class="layui-form-label"><span>*</span>会员卡标题:</label>
                                                <div class="layui-input-block">
                                                    <input type="text" name="title" lay-verify="title"
                                                           autocomplete="off" placeholder="会员卡标题"
                                                           class="layui-input title">
                                                    <span style="color: red">会员卡标题不能超过9个汉字</span>
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <label class="layui-form-label"><span>*</span>操作提示:</label>
                                                <div class="layui-input-block">
                                                    <input type="text" name="notice" lay-verify="notice"
                                                           autocomplete="off" placeholder="操作提示"
                                                           class="layui-input notice">
                                                </div>
                                            </div>
                                            <div class="layui-form-item layui-form-text">
                                                <label class="layui-form-label"><span>*</span>特权说明:</label>
                                                <div class="layui-input-block">
                                                    <textarea name="prerogative" placeholder="特权说明"
                                                              class="layui-textarea prerogative"></textarea>
                                                    <div class="layui-form-mid layui-word-aux ">字数上限为1024个汉字</div>
                                                </div>
                                            </div>
                                            <div class="layui-form-item layui-form-text">
                                                <label class="layui-form-label"><span>*</span>使用须知:</label>
                                                <div class="layui-input-block">
                                                    <textarea name="description" placeholder="使用须知"
                                                              class="layui-textarea description"></textarea>
                                                    <div class="layui-form-mid layui-word-aux ">字数上限为1024个汉字</div>
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <label class="layui-form-label"><span>*</span>核心买单:</label>
                                                <div class="layui-input-block" style="height: 60px;width: 100%">
                                                    <input type="radio" lay-filter="code_type" name="code_type"
                                                           value="CODE_TYPE_ONLY_QRCODE" title="仅显示二维码">
                                                    <input type="radio" lay-filter="code_type" name="code_type"
                                                           value="CODE_TYPE_QRCODE" title="二维码加卡号">
                                                    <input type="radio" lay-filter="code_type" name="code_type"
                                                           value="CODE_TYPE_TEXT" title="仅显示卡号">
                                                    <input type="radio" lay-filter="code_type" name="code_type"
                                                           value="CODE_TYPE_NONE" title="不显示" checked>
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <div class="layui-col-md6">
                                                    <label class="layui-form-label"><span>*</span>会员卡库存:</label>
                                                    <div class="layui-input-block">
                                                        <input type="number" name="quantity"
                                                               autocomplete="off" placeholder="会员卡库存"
                                                               class="layui-input quantity">
                                                    </div>
                                                </div>
                                                <div class="layui-col-md6">
                                                    <label class="layui-form-label">客服电话:</label>
                                                    <div class="layui-input-block">
                                                        <input type="text" name="service_phone"
                                                               autocomplete="off" placeholder="客服电话"
                                                               class="layui-input service_phone">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <div class="layui-col-md6">
                                                    <label style="border-left:3px solid #ff5e5f;padding-left:10px;margin-left:20px;">积分规则</label>
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <label class="layui-form-label">是否积分</label>
                                                <div class="layui-input-block">
                                                    <input class="supplyBonus" type="checkbox" checked lay-skin="switch"
                                                           id=""
                                                           lay-filter="supplyBonus" value='1'>
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <div class="layui-col-md6">
                                                    <label class="layui-form-label">初始积分值:</label>
                                                    <div class="layui-input-block">
                                                        <input type="number" name="init_increase_bonus"
                                                               autocomplete="off" placeholder="初始积分值"
                                                               class="layui-input init_increase_bonus">
                                                    </div>
                                                </div>
                                                <div class="layui-col-md6">
                                                    <label class="layui-form-label">单次积分上限:</label>
                                                    <div class="layui-input-block">
                                                        <input type="number" name="max_increase_bonus"
                                                               autocomplete="off" placeholder="单次积分上限"
                                                               class="layui-input max_increase_bonus">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="layui-form-item">
                                                <div class="layui-col-md6">
                                                    <label class="layui-form-label">消费多少元:</label>
                                                    <div class="layui-input-block">
                                                        <input type="number" name="cost_money_unit"
                                                               autocomplete="off" placeholder="消费多少元"
                                                               class="layui-input cost_money_unit">
                                                    </div>
                                                </div>
                                                <div class="layui-col-md6">
                                                    <label class="layui-form-label">赠送多少积分</label>
                                                    <div class="layui-input-block">
                                                        <input type="number" class="layui-input increase_bonus"
                                                               name="foundationDate" placeholder="赠送多少积分">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="layui-form-item">
                                                <div class="layui-col-md6">
                                                    <label class="layui-form-label">每次使用多少积分:</label>
                                                    <div class="layui-input-block">
                                                        <input type="number" name="cost_bonus_unit"
                                                               autocomplete="off" placeholder="每次使用多少积分"
                                                               class="layui-input cost_bonus_unit">
                                                    </div>
                                                </div>
                                                <div class="layui-col-md6">
                                                    <label class="layui-form-label">抵扣多少钱</label>
                                                    <div class="layui-input-block">
                                                        <input type="number" class="layui-input reduce_money"
                                                               name="foundationDate" placeholder="抵扣多少钱">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="layui-form-item">
                                                <div class="layui-col-md6">
                                                    <label class="layui-form-label">满多少钱可用:</label>
                                                    <div class="layui-input-block">
                                                        <input type="number" name="least_money_to_use_bonus"
                                                               autocomplete="off" placeholder="满多少钱可用"
                                                               class="layui-input least_money_to_use_bonus">
                                                    </div>
                                                </div>
                                                <div class="layui-col-md6">
                                                    <label class="layui-form-label">单笔最多使用多少积分</label>
                                                    <div class="layui-input-block">
                                                        <input type="number" class="layui-input max_reduce_bonus"
                                                               name="max_reduce_bonus" placeholder="单笔最多使用多少积分">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="layui-form-item">
                                                <div class="layui-col-md6">
                                                    <label class="layui-form-label">该会员卡享受的折扣优惠</label>
                                                    <div class="layui-input-block">
                                                        <input type="number" class="layui-input discount"
                                                               name="discount" placeholder="填写赠送多少积分">
                                                    </div>

                                                </div>
                                                <div class="layui-col-md6">
                                                    <label class="layui-form-label"
                                                           style="width: 150px">备注：填10就是九折</label>
                                                </div>

                                            </div>
                                            <div class="layui-form-item">
                                                <div class="layui-input-block">
                                                    <button class="layui-btn" lay-submit="" lay-filter="formDemo"
                                                            id='submit'>提交
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<input type="hidden" class="store_id">
<input type="hidden" class="color" value="Color010">
<input type="hidden" class="code_type" value="CODE_TYPE_NONE">
<input type="hidden" class="status" value="1">
<input type="hidden" class="supply_bonus" value="1">
<input type="hidden" class="card_id" value="">
<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script src="<?php echo e(asset('/layuiadmin/layui/jquery-2.1.4.js')); ?>"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var store_id = sessionStorage.getItem("store_id");
    var store_name = sessionStorage.getItem("store_name");
    var card_id = sessionStorage.getItem("card_id");
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'table', 'laydate','upload'], function () {
        var $ = layui.$
            , admin = layui.admin
            , form = layui.form
            , table = layui.table
            , laydate = layui.laydate
            , upload = layui.upload;
        // 未登录,跳转登录页面
        $(document).ready(function () {
            if (token == null) {
                window.location.href = "<?php echo e(url('/user/login')); ?>";
            }
        });
        $('.store_name').val(store_name);
        $('.store_id').val(store_id);
        $('.brand_name').html(store_name)
        $('.card_id').val(card_id)
        if (card_id != '') {
            getStoreInfo();
        }

        function getStoreInfo() {

            $.post("<?php echo e(url('/api/member/getSonStoreCard')); ?>",
                {
                    token: token
                    , card_id: card_id
                }, function (res) {
                    if (res.status == 1) {
                        $('.store_id').val(res.data.store_id)
                        $('.store_name').val(res.data.store_name)
                        $('.brand_name').html(res.data.store_name)
                        $('.logo_url').val(res.data.logo_url);
                        $('.logo_img').attr("src", res.data.logo_url);
                        $('.code_type').val(res.data.code_type)
                        $('.title').val(res.data.title)
                        $('#title').html(res.data.title)
                        $('.color').val(res.data.color)
                        $('.notice').val(res.data.notice)
                        $('.service_phone').val(res.data.service_phone)
                        $('.description').val(res.data.description)
                        $('.quantity').val(res.data.quantity)
                        $('.prerogative').val(res.data.prerogative)
                        $('.status').val(res.data.status)
                        $('.supply_bonus').val(res.data.supply_bonus)
                        $('.cost_money_unit').val(res.data.cost_money_unit)
                        $('.init_increase_bonus').val(res.data.init_increase_bonus)
                        $('.max_increase_bonus').val(res.data.max_increase_bonus)
                        $('.increase_bonus').val(res.data.increase_bonus)
                        $('.cost_bonus_unit').val(res.data.cost_bonus_unit)
                        $('.reduce_money').val(res.data.reduce_money)
                        $('.least_money_to_use_bonus').val(res.data.least_money_to_use_bonus)
                        $('.max_reduce_bonus').val(res.data.max_reduce_bonus)
                        $('.discount').val(res.data.discount)

                        $('input[name="code_type"][value="' + res.data.code_type + '"]').prop("checked", true)
                        $('input[name="color"][value="' + res.data.color + '"]').prop("checked", true)
                        if (res.data.status == '1') {
                            $("#status").prop("checked", true);//false 为取消选中
                        } else {
                            $("#status").prop("checked", false);//false 为取消选中
                        }
                        if (res.data.supply_bonus == '1') {
                            $(".supplyBonus").prop("checked", true);//false 为取消选中
                        } else {
                            $(".supplyBonus").prop("checked", false);//false 为取消选中
                        }
                        colorRadio(res.data.color)
                        // 重新渲染单选框
                        layui.form.render();
                    } else {
                        layer.msg(res.message, {
                            offset: '50px'
                            , icon: 2
                            , time: 3000
                        });
                    }
                }, "json");
        }

        form.on('switch(kaiguan)', function (data) {
            if (data.value == 1) {
                $('.kaiguan').val('2')
                $('.status').val('2')
            } else {
                $('.kaiguan').val('1')
                $('.status').val('1')
            }
        });
        //门店LOGO
        var uploadInst = upload.render({
            url : "<?php echo e(url('/api/basequery/webupload?act=images')); ?>"+'&token='+token,  //提交到的地址 可以自定义其他参数
            elem : '.upload_logo',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            ext : 'jpg|png|gif',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                if(res.status != 1){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    $('.logo_url').val(res.data.img_url);
                    $('.logo_img').attr("src", res.data.img_url);
                }
            }
        });
        form.on('switch(supplyBonus)', function (data) {
            if (data.value == 1) {
                $('.supplyBonus').val('2')
                $('.supply_bonus').val('2')
            } else {
                $('.supplyBonus').val('1')
                $('.supply_bonus').val('1')
            }
        });

        function colorRadio(data) {
            if (data == 'Color010') {
                $('.card_top').css('background', '#63b359');
            }
            if (data == 'Color020') {
                $('.card_top').css('background', '#2c9f67');
            }
            if (data == 'Color030') {
                $('.card_top').css('background', '#509fc9');
            }
            if (data == 'Color040') {
                $('.card_top').css('background', '#5885cf');
            }
            if (data == 'Color050') {
                $('.card_top').css('background', '#9062c0');
            }
            if (data == 'Color060') {
                $('.card_top').css('background', '#d09a45');
            }
            if (data == 'Color070') {
                $('.card_top').css('background', '#e4b138');
            }
            if (data == 'Color080') {
                $('.card_top').css('background', '#ee903c');
            }
            if (data == 'Color081') {
                $('.card_top').css('background', '#f08500');
            }
            if (data == 'Color082') {
                $('.card_top').css('background', '#a9d92d');
            }
            if (data == 'Color090') {
                $('.card_top').css('background', '#dd6549');
            }
            if (data == 'Color100') {
                $('.card_top').css('background', '#cc463d');
            }
            if (data == 'Color101') {
                $('.card_top').css('background', '#cf3e36');
            }
            if (data == 'Color102') {
                $('.card_top').css('background', '#5E6671');
            }
            $('.color').val(data)
        }

        form.on('radio(color)', function (data) {
            colorRadio(data.value)
        });

        $('.title').bind("input propertychange", function (event) {
            $('#title').html($('.title').val())
        });
        $('.store_name').bind("input propertychange", function (event) {
            $(".brand_name").html($('.store_name').val());
        });
        form.on('radio(code_type)', function (data) {
            $('.code_type').val(data.value)
        })

        // 提交
        $('#submit').click(function () {
            if ($('.supply_bonus').val() == '1') {
                if ($('.cost_money_unit').val() == '') {
                    alert("积分规则-消费多少元不能为空");
                    return;
                }
                if ($('.init_increase_bonus').val() == '') {
                    alert("积分规则-初始积分值不能为空");
                    return;
                }
                if ($('.单次积分上限').val() == '') {
                    alert("积分规则-单次积分上限不能为空");
                    return;
                }
                if ($('.increase_bonus').val() == '') {
                    alert("积分规则-赠送多少积分不能为空");
                    return;
                }
                if ($('.cost_bonus_unit').val() == '') {
                    alert("积分规则-每次使用多少积分不能为空");
                    return;
                }
                if ($('.reduce_money').val() == '') {
                    alert("积分规则-抵扣多少钱不能为空");
                    return;
                }
                if ($('.least_money_to_use_bonus').val() == '') {
                    alert("积分规则-满多少钱可用不能为空");
                    return;
                }
                if ($('.discount').val() == '') {
                    alert("积分规则-该会员卡享受的折扣优惠");
                    return;
                }
                if ($('.title').val().length>9) {
                    alert("会员卡标题不能超过9个汉字");
                    return;
                }
            }
            if($('.card_id').val==""){
                $.post("<?php echo e(url('/api/member/createSonStoreCard')); ?>",
                    {
                        token: token
                        , store_id: $('.store_id').val()
                        , store_name: $('.store_name').val()
                        , code_type: $('.code_type').val()
                        , title: $('.title').val()
                        , color: $('.color').val()
                        , notice: $('.notice').val()
                        , service_phone: $('.service_phone').val()
                        , description: $('.description').val()
                        , quantity: $('.quantity').val()
                        , prerogative: $('.prerogative').val()
                        , status: $('.status').val()
                        , supply_bonus: $('.supply_bonus').val()
                        , cost_money_unit: $('.cost_money_unit').val()
                        , init_increase_bonus: $('.init_increase_bonus').val()
                        , max_increase_bonus: $('.max_increase_bonus').val()
                        , increase_bonus: $('.increase_bonus').val()
                        , cost_bonus_unit: $('.cost_bonus_unit').val()
                        , reduce_money: $('.reduce_money').val()
                        , least_money_to_use_bonus: $('.least_money_to_use_bonus').val()
                        , max_reduce_bonus: $('.max_reduce_bonus').val()
                        , discount: $('.discount').val()
                        ,logo_url:$('.logo_url').val()
                    }, function (res) {
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 1
                                , time: 3000
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 2
                                , time: 3000
                            });
                        }
                    }, "json");
            }else {
                $.post("<?php echo e(url('/api/member/upSonStoreCard')); ?>",
                    {
                        token: token
                        , store_id: $('.store_id').val()
                        , store_name: $('.store_name').val()
                        , code_type: $('.code_type').val()
                        , title: $('.title').val()
                        , color: $('.color').val()
                        , notice: $('.notice').val()
                        , service_phone: $('.service_phone').val()
                        , description: $('.description').val()
                        , prerogative: $('.prerogative').val()
                        , status: $('.status').val()
                        , supply_bonus: $('.supply_bonus').val()
                        , cost_money_unit: $('.cost_money_unit').val()
                        , init_increase_bonus: $('.init_increase_bonus').val()
                        , max_increase_bonus: $('.max_increase_bonus').val()
                        , increase_bonus: $('.increase_bonus').val()
                        , cost_bonus_unit: $('.cost_bonus_unit').val()
                        , reduce_money: $('.reduce_money').val()
                        , least_money_to_use_bonus: $('.least_money_to_use_bonus').val()
                        , max_reduce_bonus: $('.max_reduce_bonus').val()
                        , discount: $('.discount').val()
                        ,logo_url:$('.logo_url').val()
                        ,card_id:$('.card_id').val()
                    }, function (res) {
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 1
                                , time: 3000
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 2
                                , time: 3000
                            });
                        }
                    }, "json");
            }

        })

    });

</script>

</body>
</html>
