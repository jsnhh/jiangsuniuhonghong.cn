<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>设备配置</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style>
        .img{width:130px;height:90px;overflow: hidden;}
        .img img{width:100%;height:100%;}
        .layui-layer-nobg{width: none !important;}
        /*.layui-layer-content{width:600px;height:550px;}*/
        .layui-card-header{width:200px;text-align: left;float:left;}
        .layui-card-body{margin-left:28px;}
        .layui-upload-img{width: 120px; height: 120px; /*margin: 0 10px 10px 0;*/}
        .up{position: relative;display: inline-block;cursor: pointer;border-color: #1ab394; color: #FFF;width: auto !important;font-size: 10px !important;text-align: center !important;}
        .up input{position: absolute;top:0;left: 0;display: block;opacity: .01;width: 100px;height:30px;}
        .layui-upload-list{width: 120px;height:120px;overflow: hidden;margin: 10px auto;}
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {-webkit-appearance: none !important;margin: 0;}
    </style>
</head>
<body>

<div class="layui-fluid">
    
    <div class="layui-card" style="margin-top:0px;">
        <div class="layui-card-header">设备配置</div>
        <div class="layui-card-body layui-row layui-col-space10">
            <div class="layui-form">
                <div class="layui-form-item">
                    <label class="layui-form-label">智网1喇叭token</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入智联喇叭token" class="layui-input templatename">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">智网2喇叭token</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入智网喇叭token" class="layui-input templatedesc">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">易联云用户名</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入易联云用户名" class="layui-input yly_u_name">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">易联云用户id</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入易联云用户id" class="layui-input yly_u_id">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">易联云api密钥</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入易联云api密钥" class="layui-input yly_key">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">腾讯云SecretId</label>
                    <div class="layui-input-block">
                        <input type="text" name="tencent_cloud_secretid" id="tencent_cloud_secretid" lay-verify="schoolname" autocomplete="off" placeholder="请输入腾讯云SecretId" class="layui-input yly_key">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">腾讯云SecretKey</label>
                    <div class="layui-input-block">
                        <input type="text" name="tencent_cloud_secretkey" id="tencent_cloud_secretkey" lay-verify="schoolname" autocomplete="off" placeholder="请输入腾讯云SecretKey" class="layui-input yly_key">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">畅立收播报前缀</label>
                    <div class="layui-input-block">
                        <input type="text" name="cls_prefix" id="cls_prefix" lay-verify="schoolname" autocomplete="off" placeholder="请输入畅立收播报前缀" class="layui-input yly_key">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">畅立收设备ID</label>
                    <div class="layui-input-block">
                        <input type="text" name="cls_product_id" id="cls_product_id" lay-verify="schoolname" autocomplete="off" placeholder="请输入畅立收设备ID" class="layui-input yly_key">
                    </div>
                </div>

                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit">确定提交</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
</div>

<input type="hidden" class="schooltypeid" value="">
<input type="hidden" class="schooltypename" value="">


<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script> 
<script>
    var token = sessionStorage.getItem("Usertoken");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'table','form','upload'], function(){
        var $ = layui.$;
            admin = layui.admin         
            ,table = layui.table
            ,element = layui.element
            ,upload = layui.upload
            ,form = layui.form;

            element.render();

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="<?php echo e(url('/user/login')); ?>"; 
            }
        });

        //加载页面参数
        $.post("<?php echo e(url('/api/device/v_config')); ?>",
        {
            token:token
        },function(res){
            $('.layui-form .layui-form-item').eq(0).find('input').val(res.data.old_zw_token);
            $('.layui-form .layui-form-item').eq(1).find('input').val(res.data.zw_token);
            $('.layui-form .layui-form-item').eq(2).find('input').val(res.data.yly_user_name);
            $('.layui-form .layui-form-item').eq(3).find('input').val(res.data.yly_user_id);
            $('.layui-form .layui-form-item').eq(4).find('input').val(res.data.yly_api_key);
            $('.layui-form .layui-form-item').eq(5).find('input').val(res.data.tencent_cloud_secretid);
            $('.layui-form .layui-form-item').eq(6).find('input').val(res.data.tencent_cloud_secretkey);
            $('.layui-form .layui-form-item').eq(7).find('input').val(res.data.cls_prefix);
            $('.layui-form .layui-form-item').eq(8).find('input').val(res.data.cls_product_id);
        },"json");

        //提交
        $('.submit').on('click', function(){
            $.post("<?php echo e(url('/api/device/v_config')); ?>",
            {
                token:token
                ,type:1
                ,old_zw_token:$('.layui-form .layui-form-item').eq(0).find('input').val()
                ,zw_token:$('.layui-form .layui-form-item').eq(1).find('input').val()
                ,yly_user_name:$('.layui-form .layui-form-item').eq(2).find('input').val()
                ,yly_user_id:$('.layui-form .layui-form-item').eq(3).find('input').val()
                ,yly_api_key:$('.layui-form .layui-form-item').eq(4).find('input').val()
                ,tencent_cloud_secretid:$('#tencent_cloud_secretid').val()
                ,tencent_cloud_secretkey:$('#tencent_cloud_secretkey').val()
                ,cls_prefix:$('#cls_prefix').val()
                ,cls_product_id:$('#cls_product_id').val()
            },function(res){
                if(res.status==1){
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 1
                        ,time: 3000
                    });
                }else{
                    layer.msg(res.message, {
                        offset: '50px'
                        ,icon: 2
                        ,time: 3000
                    });
                }
            },"json");
        });

    });
</script>
</body>
</html>
