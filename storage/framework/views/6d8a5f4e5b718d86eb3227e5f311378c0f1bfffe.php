<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <title>设置比例</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    
</head>
<body>

<div class="layui-fluid" style="margin-top: 0px;">
    <div class="layui-card">
        <div class="layui-card-header">设置比例</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">
                <div class="layui-form-item">
                    <label class="layui-form-label">公司名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">比例</label>
                    <div class="layui-input-block">
                        <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit site-demo-active" data-type="tabChange">保存</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script> 
<script>
    var token = sessionStorage.getItem("Usertoken");
    

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects','element'], function(){
        var $ = layui.$ 
            ,admin = layui.admin
            ,element = layui.element
            ,form = layui.form
        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="<?php echo e(url('/user/login')); ?>"; 
            }
        });

        /* 获取 url */
        $('.layui-form .layui-form-item').eq(0).find('input').val("<?php echo e($_GET['user_name']); ?>");
        $('.layui-form .layui-form-item').eq(1).find('input').val("<?php echo e($_GET['profit_ratio']); ?>");
        
        $('.submit').on('click', function(){

            let dataobj = $('.layui-form .layui-form-item').eq(1).find('input').val();
            if(dataobj > 100 || dataobj < 1){
                layer.msg("比例要大于1，不超过100", {icon:2, shade:0.5, time:2000});
                return false;
            }else{

                $.post("<?php echo e(url('/api/user/addPercentage')); ?>",
                {
                    token:token,
                    user_id:"<?php echo e($_GET['user_id']); ?>",
                    percentage:dataobj,
                    
                },function(res){
                    console.log(res);
                    if(res.status==1){
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 3000
                        });
                    }else{
                        layer.alert(res.message, {icon: 2});
                    }
                },"json");

            }

           
        });


    })

</script>

</body>
</html>
