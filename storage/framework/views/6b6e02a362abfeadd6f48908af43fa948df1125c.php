<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>结算记录</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .cur {
            color: #009688;
        }

        .details {
            background-color: #7cb717;
        }

        .del {
            background-color: #e85052;
        }

        /*.laytable-cell-1-school_icon{height:100%;}*/
        .yname {
            font-size: 13px;
            color: #444;
        }

        .userbox {
            height: 190px;
            margin-right: 10px;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 60px;
            width: 298px;
            background-color: #ffffff;
            border: 1px solid #ddd;
        }

        .userbox .list {
            height: 38px;
            line-height: 38px;
            cursor: pointer;
            padding-left: 10px;
        }

        .userbox .list:hover {
            background-color: #eeeeee;
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">

            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12" style="margin-top:0px">
                        <div class="layui-card">
                            <div class="layui-card-header">结算记录</div>

                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">

                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:250px;margin-right:10px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <div class="layui-input-block" style="margin-left:0">
                                                <text class="yname">选择提现对象</text>
                                                <select name="tixian" id="tixian" lay-filter="tixian">
                                                    <option value="">选择提现对象</option>
                                                    <option value="1">服务商</option>
                                                    <option value="2">商户</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:250px;margin-right:10px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <div class="layui-input-block" style="margin-left:0">
                                                <text class="yname">选择返佣来源</text>
                                                <select name="msg" id="msg" lay-filter="msg">

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:190px;margin-right:10px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <div class="layui-input-block" style="margin-left:0;border-radius:5px">
                                                <text class="yname">服务商名称</text>
                                                <input type="text" name="schoolname" lay-verify="schoolname"
                                                       autocomplete="off" placeholder="请输入服务商名称"
                                                       class="layui-input transfer">
                                                <div class="userbox" style='display: none'></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="layui-form" style="display: inline-block;">
                                    <div class="layui-form-item" style='margin-left:10px;'>
                                        <div class="layui-inline">

                                            <div class="layui-input-inline" style='width:180px;'>
                                                <text class="yname">订单开始时间</text>
                                                <input type="text" class="layui-input start-item test-item"
                                                       placeholder="订单开始时间" lay-key="23">
                                            </div>
                                        </div>
                                        <div class="layui-inline">
                                            <div class="layui-input-inline" style='width:180px;'>
                                                <text class="yname">订单结束时间</text>
                                                <input type="text" class="layui-input end-item test-item"
                                                       placeholder="订单结束时间" lay-key="24">
                                            </div>
                                        </div>

                                        <div class="layui-inline" style='margin-right:0'>
                                            <div class="layui-input-inline" style='width:180px;'>
                                                <text class="yname">订单金额</text>
                                                <input type="text" class="layui-input amount_start" placeholder="订单金额">
                                            </div>
                                        </div>
                                        -
                                        <div class="layui-inline" style='margin-left:10px;'>
                                            <div class="layui-input-inline" style='width:180px;'>
                                                <text class="yname">订单金额</text>
                                                <input type="text" class="layui-input amount_end" placeholder="订单金额">
                                            </div>
                                        </div>
                                        <div class="layui-inline" style="margin-left:10px;margin-top: 36px;">
                                            <div class="layui-form-item">
                                                <button class="layui-btn export"
                                                        style="border-radius:5px;height:36px;line-height: 36px;">导出
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>

                                <script type="text/html" id="company_type">
                                    {{#  if(d.source_type=='12000'){ }}
                                    TF传化
                                    {{#  } else if(d.source_type=='hb') { }}
                                    红包码
                                    {{#  } else if(d.source_type=='21000') { }}
                                    易生支付
                                    {{#  } else if(d.source_type=='3000') { }}
                                    快钱支付
                                    {{#  } else if(d.source_type=='1000') { }}
                                    支付宝
                                    {{#  } else if(d.source_type=='2000') { }}
                                    微信支付
                                    {{#  } else if(d.source_type=='6000') { }}
                                    京东金融
                                    {{#  } else if(d.source_type=='8000') { }}
                                    新大陆
                                    {{#  } else if(d.source_type=='9000') { }}
                                    和融通
                                    {{#  } else if(d.source_type=='6000') { }}
                                    京东金融
                                    {{#  } else if(d.source_type=='15000') { }}
                                    哆啦宝
                                    {{#  } else if(d.source_type=='13000') { }}
                                    随行付
                                    {{#  } else if(d.source_type=='19000') { }}
                                    随行付A
                                    {{#  } else if(d.source_type=='16000') { }}
                                    支付宝-ZFT
                                    {{#  } else if(d.source_type=='16001') { }}
                                    花呗分期-ZFT
                                    {{#  } else if(d.source_type=='1001') { }}
                                    花呗分期
                                    {{#  } else if(d.source_type=='23000') { }}
                                    HL葫芦
                                    {{#  } else { }}
                                    未知
                                    {{#  } }}
                                </script>

                                <script type="text/html" id="istrue">
                                    {{#  if(d.is_true == 0){ }}
                                    <span style="color:#e85052">未确认</span>
                                    {{#  } else { }}
                                    <span style="color:#00963a">确认</span>
                                    {{#  } }}
                                </script>
                                <script type="text/html" id="jiesuan">
                                    {{#  if(d.dx == 1){ }}
                                    服务商
                                    {{#  } else { }}
                                    商户
                                    {{#  } }}
                                </script>
                                <script type="text/html" id="rate">
                                    {{ d.rate }}%
                                </script>
                                <script type="text/html" id="jindu">
                                    {{#  if(d.is_settlement == 0){ }}
                                    <span style="color:#e85052">结算中</span>
                                    {{#  } else { }}
                                    <span style="color:#7cb717">结算完成</span>
                                    {{#  } }}
                                </script>

                                <script type="text/html" id="table-content-list">
                                    {{#  if(d.is_true == 0){ }}
                                    <a class="layui-btn layui-btn-normal layui-btn-xs settlement"
                                       lay-event="settlement">确认结算</a>
                                    {{#  } else { }}

                                    {{#  } }}

                                    <a class="layui-btn layui-btn-normal layui-btn-xs details" lay-event="details"
                                       lay-href="">结算明细</a>
                                    <a class="layui-btn layui-btn-normal layui-btn-xs del" lay-event="del">删除</a>
                                </script>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<input type="hidden" class="user_id">
<input type="hidden" class="status">
<!-- 操作按钮 -->
<!-- 
                    
<a class="layui-btn layui-btn-normal layui-btn-xs see" lay-event="detail">查看</a> -->
<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var source = sessionStorage.getItem("source");
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'table', 'laydate'], function () {
        var $ = layui.$
            , admin = layui.admin
            , table = layui.table
            , form = layui.form
            , laydate = layui.laydate;

        // 获取时间
        var nowdate = new Date();
        // 本月
        var year = nowdate.getFullYear();
        var mounth = nowdate.getMonth() + 1;
        var day = nowdate.getDate();
        var hour = nowdate.getHours();
        var min = nowdate.getMinutes();
        var sec = nowdate.getSeconds();
        if (mounth.toString().length < 2 && day.toString().length < 2) {
            var nwedata = year + '-0' + mounth + '-0' + day + ' ' + hour + ':' + min + ':' + sec;
        } else if (mounth.toString().length < 2) {
            var nwedata = year + '-0' + mounth + '-' + day + ' ' + hour + ':' + min + ':' + sec;
        } else if (day.toString().length < 2) {
            var nwedata = year + '-' + mounth + '-0' + day + ' ' + hour + ':' + min + ':' + sec;
        } else {
            var nwedata = year + '-' + mounth + '-' + day + ' ' + hour + ':' + min + ':' + sec;
        }
        $('.end-item').val(nwedata);//今天的时间
        $('.endtime').val(nwedata)

        //今天的开始时间
        if (mounth.toString().length < 2 && day.toString().length < 2) {
            var nwedatastart = year + '-0' + mounth + '-0' + day + ' ' + '00' + ':' + '00' + ':' + '00';
        } else if (mounth.toString().length < 2) {
            var nwedatastart = year + '-0' + mounth + '-' + day + ' ' + '00' + ':' + '00' + ':' + '00';
        } else if (day.toString().length < 2) {
            var nwedatastart = year + '-' + mounth + '-0' + day + ' ' + '00' + ':' + '00' + ':' + '00';
        } else {
            var nwedatastart = year + '-' + mounth + '-' + day + ' ' + '00' + ':' + '00' + ':' + '00';
        }
        $('.starttime').val(nwedatastart);

        // 上个月
        var y = nowdate.getFullYear();
        var mon = nowdate.getMonth() + 1;
        var d = nowdate.getDate();
        var h = '00';
        var m = '00';
        var s = '00';
        if (mon.toString().length < 2 && d.toString().length < 2) {
            var formatwdate = y + '-0' + mon + '-0' + d + ' ' + h + ':' + m + ':' + s;
        } else if (mon.toString().length < 2) {
            var formatwdate = y + '-0' + mon + '-' + d + ' ' + h + ':' + m + ':' + s;
        } else if (d.toString().length < 2) {
            var formatwdate = y + '-' + mon + '-0' + d + ' ' + h + ':' + m + ':' + s;
        } else {
            var formatwdate = y + '-' + mon + '-' + d + ' ' + h + ':' + m + ':' + s;
        }
        $('.start-item').val(formatwdate);

        laydate.render({
            elem: '.start-item'
            , type: 'datetime'
            , trigger: 'click'
            , done: function (value) {
                $('.start-item').val(value)
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: $('.start-item').val(),
                        time_end: $('.end-item').val(),
                    }
                    , page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        laydate.render({
            elem: '.end-item'
            , trigger: 'click'
            , type: 'datetime'
            , done: function (value) {
                $('.end-item').val(value)
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start: $('.start-item').val(),
                        time_end: $('.end-item').val(),
                    }
                    , page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        // 开始金额-结束金额****
        $('.amount_start').bind("input propertychange", function (event) {
            var amount_start = parseInt($(this).val())
            var amount_end = parseInt($('.amount_end').val())

            if (amount_start < amount_end) {//开始金额
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        amount_start: $('.amount_start').val(),
                        amount_end: $('.amount_end').val(),
                    }
                    , page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            } else if ($('.amount_end').val() == '') {//结束金额
            } else {
            }

        });
        $('.amount_end').bind("input propertychange", function (event) {
            var amount_start = parseInt($('.amount_start').val())
            var amount_end = parseInt($(this).val())
            if (amount_start < amount_end) {//开始金额
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        amount_start: $('.amount_start').val(),
                        amount_end: $('.amount_end').val(),
                    }
                    , page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            } else if ($('.amount_start').val() == '') {//结束金额
            } else {//结束金额
            }

        });


        // 未登录,跳转登录页面
        $(document).ready(function () {
            if (token == null) {
                window.location.href = "<?php echo e(url('/user/login')); ?>";
            }
        })

        $.ajax({
            url: "<?php echo e(url('/api/wallet/source_type')); ?>",
            data: {token: token,source:source},
            type: 'post',
            success: function (data) {
                console.log(data);
                var optionStr = "";
                for (var i = 0; i < data.data.length; i++) {
                    optionStr += "<option value='" + data.data[i].source_type + "'>"
                        + data.data[i].source_desc + "</option>";
                }
                $("#msg").append('<option value="">选择返佣来源</option>' + optionStr);
                layui.form.render('select');
            },
            error: function (data) {
                alert('查找板块报错');
            }
        });

        $(".transfer").bind("input propertychange", function (event) {
            //  console.log($(this).val())
            user_name = $(this).val();
            if (user_name.length == 0) {
                $('.userbox').html('');
                $('.userbox').hide();
            } else {
                $.post("<?php echo e(url('/api/user/get_sub_users')); ?>",
                    {
                        token: token,
                        user_name: user_name,
                        self: '1'
                    }, function (res) {
                        //   console.log(res);
                        var html = "";
//                console.log(res.t);
                        if (res.t == 0) {
                            $('.userbox').html('');
                            $('.userbox').hide();
                        } else {
                            for (var i = 0; i < res.data.length; i++) {
                                html += '<div class="list" data=' + res.data[i].id + '>' + res.data[i].name + '-' + res.data[i].level_name + '</div>'
                            }
                            $(".userbox").show();
                            $('.userbox').html('');
                            $('.userbox').append(html);
                        }
                    }, "json");
            }
        });

        table.render({
            elem: '#test-table-page'
            , url: "<?php echo e(url('/api/wallet/settlement_lists')); ?>"
            , method: 'post'
            , where: {
                token: token,
                time_start: $('.start-item').val(),
                time_end: $('.end-item').val(),
                amount_start: $('.amount_start').val(),
                amount_end: $('.amount_end').val(),
            }
            , request: {
                pageName: 'p',
                limitName: 'l'
            }
            , page: true
            , cellMinWidth: 150
            , cols: [[
                {field: 'id', title: 'id', width: 50}
                , {field: 'is_settlement', title: '进度', templet: '#jindu', width: 95}
                , {field: 's_time', title: '开始时间', width: 120}
                , {field: 'e_time', title: '结束时间', width: 120}
                , {
                    title: '返佣来源',
                    width: 120,
                    toolbar: '#company_type'
                }
                , {field: 'dx_desc', title: '结算对象', width: 230}
                , {field: 'total_amount', title: '需结算金额'}
                , {field: 'rate', title: '税点', templet: '#rate', width: 100}
                , {field: 'get_amount', title: '扣税结算金额'}
                , {field: 'is_true', title: '是否确认', templet: '#istrue', width: 100}
                , {field: 'created_at', title: '创建时间', width: 120}
                , {field: 'updated_at', title: '确认时间', width: 120}
                , {width: 200, align: 'center', fixed: 'right', toolbar: '#table-content-list', title: '操作'}
            ]]
            , response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                , statusCode: 1 //成功的状态码，默认：0
                , msgName: 'message' //状态信息的字段名称，默认：msg
                , countName: 't' //数据总数的字段名称，默认：count
                , dataName: 'data' //数据列表的字段名称，默认：data
            }
            , done: function (res, curr, count) {
                console.log(res);
                $('th').css({
                    'font-weight': 'bold',
                    'font-size': '15',
                    'color': 'black',
                    'background': 'linear-gradient(#f2f2f2,#cfcfcf)'
                });	//进行表头样式设置
            }

        });


        table.on('tool(test-table-page)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var e = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            console.log(e);
            // sessionStorage.setItem('s_store_id', e.store_id);

            if (layEvent === 'del') {
                layer.confirm('确认删除此消息?', {icon: 2}, function (index) {
                    $.post("<?php echo e(url('/api/wallet/settlement_list_del')); ?>",
                        {
                            token: token, settlement_list_id: e.id
                        }, function (data) {
                            console.log(data);
                            if (data.status == 1) {
                                obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                                layer.close(index);
                                layer.msg(data.message, {
                                    offset: '50px'
                                    , icon: 1
                                    , time: 1000
                                });
                            } else {
                                layer.msg(data.message, {
                                    offset: '50px'
                                    , icon: 2
                                    , time: 3000
                                });
                            }
                        }, "json");


                    // $.ajax({
                    //   url : "<?php echo e(url('/api/wallet/settlement_list_del')); ?>",
                    //   data : {token:token,settlement_list_id:e.id},
                    //   type : 'post',
                    //   success : function(data) {
                    //     console.log(data);
                    //     if(data.status==1){
                    //       obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                    //       layer.close(index);
                    //       layer.msg(data.message, {
                    //         offset: '15px'
                    //         ,icon: 1
                    //         ,time: 1000
                    //       });
                    //     }else{
                    //       layer.msg(data.message, {
                    //         offset: '15px'
                    //         ,icon: 2
                    //         ,time: 3000
                    //       });
                    //     }

                    //   },
                    // },"json");

                });
            } else if (layEvent === 'settlement') {
                layer.confirm('确认结算后无法回退请知晓', {icon: 1}, function (index) {
                    layer.close(index);
                    layer.load(1);

                    $.post("<?php echo e(url('/api/wallet/settlement_list_true')); ?>",
                        {
                            token: token, settlement_list_id: e.id
                        }, function (data) {
                            console.log(data);
                            layer.closeAll('loading');
                            if (data.status == 1) {
                                layer.msg(data.message, {
                                    offset: '50px'
                                    , icon: 1
                                    , time: 1000
                                });
                            } else {
                                layer.msg(data.message, {
                                    offset: '50px'
                                    , icon: 2
                                    , time: 3000
                                });
                            }
                        }, "json");

                    // $.ajax({
                    //   url : "<?php echo e(url('/api/wallet/settlement_list_true')); ?>",
                    //   data : {token:token,settlement_list_id:e.id},
                    //   type : 'post',
                    //   success : function(data) {
                    //     console.log(data);
                    //     if(data.status==1){
                    //       // layer.close(load);
                    //       layer.msg(data.message, {
                    //         offset: '15px'
                    //         ,icon: 1
                    //         ,time: 1000
                    //       });
                    //     }else{
                    //       layer.msg(data.message, {
                    //         offset: '15px'
                    //         ,icon: 2
                    //         ,time: 3000
                    //       });
                    //     }

                    //   },
                    // },"json");
                });
            } else if (layEvent === 'details') {
                $('.details').attr('lay-href', "<?php echo e(url('/user/settledetail?id=')); ?>" + e.id);
            }


        });


        // 选择学校
        form.on('select(tixian)', function (data) {
            var dx = data.value;
            //执行重载
            table.reload('test-table-page', {
                where: {
                    dx: dx
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });
        form.on('select(msg)', function (data) {
            var source_type = data.value;
            //执行重载
            table.reload('test-table-page', {
                where: {
                    source_type: source_type
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });
        $(".userbox").on("click",".list",function(){
            $('.transfer').val($(this).html());
            $('.js_user_id').val($(this).attr('data'));
            $('.userbox').hide();

            table.reload('test-table-page', {
                where: {
                    user_id:$(this).attr('data')
                }
                ,page: {
                    curr: 1
                }
            });
        });

        // 导出分润账单
        $('.export').click(function () {
            var time_start = $('.start-item').val();
            var time_end = $('.endt-item').val();
            var amount_start = $('.amount_start').val();
            var amount_end = $('.amount_end').val();

            window.location.href = "<?php echo e(url('/api/export/settlerecordExcelDown')); ?>" + "?token=" + token + "&time_start=" + time_start + "&time_end=" + time_end + "&amount_start=" + amount_start + "&amount_end=" + amount_end;

        })


    });


</script>

</body>
</html>