<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>门店交易统计</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
  <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
  <style>
    .edit{background-color: #ed9c3a;}
    .shenhe{background-color: #429488;}
    .see{background-color: #7cb717;}
    .tongbu{background-color: #4c9ef8;color:#fff;}
    .cur{color:#009688;}
    .yname{
          font-size: 13px;
          color: #444;
        }
  </style>
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12" style="margin-top:0px">

        <div class="layui-fluid">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
              <div class="layui-card">
                <div class="layui-card-header">门店交易统计</div>
                <div class="layui-card-body">
                  <div class="layui-btn-container" style="font-size:14px;">
                    <div class="layui-form" style="width:820px;display: inline-block;">
                        <div class="layui-form-item">
                          <!-- 缴费时间 -->
                          <div class="layui-inline">
                            <div class="layui-input-inline">
                            <text class="yname">交易开始时间</text>
                              <input type="text" class="layui-input start-item test-item" placeholder="交易开始时间" lay-key="23">
                            </div>
                          </div>
                          <div class="layui-inline">
                            <div class="layui-input-inline">
                            <text class="yname">交易结束时间</text>
                              <input type="text" class="layui-input end-item test-item" placeholder="交易结束时间" lay-key="24">
                            </div>
                          </div>
                          <!-- 缴费时间end -->

                            <div class="layui-inline">
                                <button class="layui-btn" style="border-radius:5px;margin-top:31px;" id="today">今日</button>
                                <button class="layui-btn" style="border-radius:5px;margin-top:31px;" id="yesterday">昨日</button>
                            </div>
                            <div class="layui-inline">
                            <text class="yname">选择排序</text>
                                <select name="agent" id="agent" lay-filter="agent" lay-search>
                                    <option value="">默认</option>
                                    <option value="3" selected="selected">交易金额由大到小</option>
                                    <option value="4">交易金额由小到大</option>
                                    <option value="1">交易笔数由大到小</option>
                                    <option value="2">交易笔数由小到大</option>
                                </select>
                            </div>
                    </div>
                  </div>

                  <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                  <!-- 判断状态 -->
                  <script type="text/html" id="statusTap">
                    {{#  if(d.pay_status == 1){ }}
                      <span class="cur">{{ d.pay_status_desc }}</span>
                    {{#  } else { }}
                      {{ d.pay_status_desc }}
                    {{#  } }}
                  </script>
                  <!-- 判断状态 -->

                  <script type="text/html" id="paymoney">
                    {{ d.rate }}%
                  </script>
                  <script type="text/html" id="table-content-list" class="layui-btn-small">
                    <!-- <a class="layui-btn layui-btn-normal layui-btn-xs tongbu" lay-event="tongbu">查看</a> -->
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

    <input type="hidden" class="starttime"><!-- 今天的开始时间 -->
    

    <input type="hidden" class="starttimeY"><!-- 昨天的开始时间 -->
    <input type="hidden" class="endtimeY"><!-- 昨天的结束时间 -->

    <script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
    <script>
    var token = sessionStorage.getItem("Usertoken");
    var str=location.search;
    var store_id = sessionStorage.getItem("store_store_id");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate;

        $('.user_id').val(store_id);
        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="<?php echo e(url('/user/login')); ?>";
            }
        });

        //时间组件
        // var myDate = new Date();
        // var year=myDate.getFullYear();
        // var month = ("0" + (myDate.getMonth() + 1)).slice(-2);
        // var months = year+''+month;
        // $('.start-item').val(year+'-'+month+'-'+day);  //开始时间
        // 获取时间**************
        var nowdate = new Date();
        // 本月
        var year=nowdate.getFullYear();
        var mounth=nowdate.getMonth()+1;
        var day=nowdate.getDate();
        if(mounth.toString().length<2 && day.toString().length<2){
            var nwedata = year+'-0'+mounth+'-0'+day;
        } else if(mounth.toString().length<2){
            var nwedata = year+'-0'+mounth+'-'+day;
        } else if(day.toString().length<2){
            var nwedata = year+'-'+mounth+'-0'+day;
        } else{
            var nwedata = year+'-'+mounth+'-'+day;
        }
        // $('.end-item').val(nwedata);//今天的时间
        $('.endtime').val(nwedata);
        //今天的开始时间
        if(mounth.toString().length<2 && day.toString().length<2){
            var nwedatastart = year+'-0'+mounth+'-0'+day;
        }
        else if(mounth.toString().length<2){
            var nwedatastart = year+'-0'+mounth+'-'+day;
        }
        else if(day.toString().length<2){
            var nwedatastart = year+'-'+mounth+'-0'+day;
        } else{
            var nwedatastart = year+'-'+mounth+'-'+day;
        }
        $('.starttime').val(nwedatastart);

        //**********************
        var years=nowdate.getFullYear();
        var mounths=nowdate.getMonth()+1;
        var days=nowdate.getDate()-1;
        //昨天的开始时间
        if(mounth.toString().length<2 && day.toString().length<2){
            var yesterdaystart = years+'-0'+mounths+'-0'+days;
        } else if(mounth.toString().length<2){
            var yesterdaystart = year+'-0'+mounths+'-'+days;
        } else if(day.toString().length<2){
            var yesterdaystart = years+'-'+mounths+'-0'+days;
        } else{
            var yesterdaystart = years+'-'+mounths+'-'+days;
        }
        //昨天的结束时间
        if (mounth.toString().length<2 && day.toString().length<2) {
            var yesterdayend = years+'-0'+mounths+'-0'+days;
        } else if(mounth.toString().length<2) {
            var yesterdayend = years+'-0'+mounths+'-'+days;
        } else if(day.toString().length<2) {
            var yesterdayend = years+'-'+mounths+'-0'+days;
        } else {
            var yesterdayend = years+'-'+mounths+'-'+days;
        }
        $('.starttimeY').val(yesterdaystart);
        $('.endtimeY').val(yesterdayend);

        // 上个月*******
        var y = nowdate.getFullYear();
        var mon = nowdate.getMonth()+1;
        var d = nowdate.getDate();
        if (mon.toString().length<2 && d.toString().length<2) {
            var formatwdate = y+'-0'+mon+'-0'+d;
        } else if(mon.toString().length<2) {
            var formatwdate = y+'-0'+mon+'-'+d;
        } else if(d.toString().length<2) {
            var formatwdate = y+'-'+mon+'-0'+d;
        } else {
            var formatwdate = y+'-'+mon+'-'+d;
        }
        // console.log(formatwdate);
        $('.start-item').val(formatwdate);

        // 渲染表格*********
        table.render({
            elem: '#test-table-page'
            ,url: "<?php echo e(url('/api/user/ranking')); ?>"
            ,method: 'post'
            ,where:{
                token:token,
                // month:months,
                type:'3',
                time_start:$('.start-item').val(),
                time_end:$('.end-item').val(),
            }
            ,request:{
                pageName: 'p',
                limitName: 'l'
            }
            ,page: true
            ,cellMinWidth: 100
            ,cols: [[
                {align:'center',field:'store_name', title: '门店'}
                ,{align:'center',field:'total_amount', title: '总交易金额'}
                ,{align:'center',field:'total_count', title: '总交易笔数'}
                ,{align:'center',field:'mdiscount_amount',  title: '优惠金额'}
              // ,{align:'center',field:'alipay_amount',  title: '支付宝交易金额'}
              // ,{align:'center',field:'weixin_count', title: '微信交易笔数'}
            //   ,{align:'center',field:'mdiscount_amount',  title: '优惠总金额'}
                ,{align:'center',field:'alipay_face_count',  title: '支付宝刷脸笔数'}
                ,{align:'center',field:'weixin_face_count',  title: '微信刷脸笔数'}
                ,{align:'center',field:'created_at',  title: '交易时间'}
                ,{align:'center',field:'store_created_at',  title: '商户注册时间'}
              // ,{width:100,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
            ]]
            ,response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                ,statusCode: 1 //成功的状态码，默认：0
                ,msgName: 'message' //状态信息的字段名称，默认：msg
                ,countName: 't' //数据总数的字段名称，默认：count
                ,dataName: 'data' //数据列表的字段名称，默认：data
            }
            ,done: function(res, curr, count){
              console.log(res);
              $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});	//进行表头样式设置
            }
        });


        table.on('tool(test-table-page)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var e = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            // console.log(e);
            // sessionStorage.setItem('s_store_id', e.store_id);

          if(layEvent === 'tongbu'){ //审核
              $.post("<?php echo e(url('/api/basequery/update_order')); ?>",
              {
                  token:token,
                  store_id:e.store_id,
                  out_trade_no:e.out_trade_no
              },function(res){
                  // console.log(res);
                  if(res.status==1){
                      layer.msg(res.message, {
                          offset: '50px'
                          ,icon: 1
                          ,time: 2000
                      });
                  }else{
                      layer.msg(res.message, {
                          offset: '50px'
                          ,icon: 2
                          ,time: 2000
                      });
                  }
              },"json");
          }

          var data = obj.data;
          if(obj.event === 'setSign'){
              layer.open({
                type: 2,
                title: '模板详细',
                shade: false,
                maxmin: true,
                area: ['60%', '70%'],
                content: "<?php echo e(url('/merchantpc/paydetail?')); ?>"+e.stu_order_type_no
              });
          }
        });

        // 选择排序
        form.on('select(agent)', function(data){
          var type = data.value;

          //执行重载
          table.reload('test-table-page', {
            where: {
              type: type
            }
            ,page: {
              curr: 1 //重新从第 1 页开始
            }
          });
        });

        //开始时间触发
        laydate.render({
            elem: '.start-item'
            ,format: 'yyyy-MM-dd'
            ,trigger: 'click'
            ,done: function(value){
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start:value,
                        time_end:$('.end-item').val()
                    }
                    ,page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        //结束时间触发
        laydate.render({
          elem: '.end-item'
            ,format: 'yyyy-MM-dd'
            ,trigger: 'click'
            ,done: function(value){
                //执行重载
                table.reload('test-table-page', {
                    where: {
                        time_start:$('.start-item').val(),
                        time_end:value
                    }
                    ,page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            }
        });

        // $('.export').click(function(){
        //   var store_id=$('.store_id').val();
        //   var user_id=$('.user_id').val();
        //   var sort=$('.sort').val();
        //   var pay_status=$('.pay_status').val();
        //   var ways_source=$('.pay_type').val();
        //   var company=$('.company_id').val();

        //   var time_start=$('.start-item').val();
        //   var time_end=$('.end-item').val();

        //   var out_trade_no=$('.danhao').val();
        //   var trade_no=$('.tiaoma').val();

        //   window.location.href="<?php echo e(url('/api/export/UserOrderExcelDown')); ?>"+"?token="+token+"&store_id="+store_id+"&user_id="+user_id+"&sort="+sort+"&pay_status="+pay_status+"&ways_source="+ways_source+"&company="+company+"&time_start="+time_start+"&time_end="+time_end+"&out_trade_no="+out_trade_no+"&trade_no="+trade_no;
        // })

        //今天
        $('#today').click(function(){
            $('.start-item').val($('.starttime').val());
            $('.end-item').val($('.endtime').val());
            //执行重载
          table.reload('test-table-page', {
            where: {
                time_start:nwedatastart
                ,time_end:nwedatastart
                // ,type: type
            }
            ,page: {
              curr: 1 //重新从第 1 页开始
            }
          });
        });

        //昨天
        $('#yesterday').click(function(){
            $('.start-item').val($('.starttimeY').val());
            $('.end-item').val($('.endtimeY').val());
            //执行重载
          table.reload('test-table-page', {
            where: {
                 time_start:yesterdaystart
                ,time_end:yesterdayend
                  // ,type: type
              }
            ,page: {
              curr: 1 //重新从第 1 页开始
            }
          });
        });
    });

  </script>

</body>
</html>





