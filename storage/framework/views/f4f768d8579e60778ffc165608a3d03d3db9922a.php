<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>子商户会员信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .cur {
            color: #009688;
        }

        .del {
            background-color: #e85052;
        }

        /*.laytable-cell-1-school_icon{height:100%;}*/
        .yname {
            font-size: 13px;
            color: #444;
        }

        #code {
            width: 190px;
            margin-right: 10px;
            height: 190px;
            margin-right: 10px;
            margin: 20px auto;
        }

        #code canvas {
            width: 100%;
        }

        .userbox, .storebox {
            height: 190px;
            margin-right: 0;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 63px;
            width: 298px;
            background-color: #ffffff;
            border: 1px solid #ddd;
        }

        .userbox .list, .storebox .list {
            height: 38px;
            line-height: 38px;
            cursor: pointer;
            padding-left: 10px;
        }

        .userbox .list:hover, .storebox .list:hover {
            background-color: #eeeeee;
        }

        .yname {
            font-size: 13px;
            color: #444;
            margin-bottom: 8px;
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">

            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12" style="margin-top:0px">
                        <div class="layui-card">
                            <div class="layui-card-header">会员信息列表</div>

                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">
                                    <div style="font-size:14px">
                                        <div class="layui-form" lay-filter="component-form-group"
                                             style="width:100%;display: flex;">
                                            <div class="layui-form" lay-filter="component-form-group"
                                                 style="width:190px;margin-right:10px;display: inline-block;">
                                                <div class="layui-form-item">
                                                    <div class="layui-input-block"
                                                         style="margin-left:0;border-radius:5px">
                                                        <text class="yname">门店名称</text>
                                                        <input type="text" style="border-radius:5px" name="schoolname"
                                                               lay-verify="schoolname" autocomplete="off"
                                                               placeholder="请输入门店名称" class="layui-input inputstore">
                                                        <div class="storebox" style='display: none'></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="layui-form" lay-filter="component-form-group"
                                                 style="width:190px;margin-right:10px;display: inline-block;">
                                                <div class="layui-form-item">
                                                    <div class="layui-input-block"
                                                         style="margin-left:0;border-radius:5px">
                                                        <text class="yname">是否激活</text>
                                                        <select name="status" lay-filter="status">
                                                            <option value="">全部</option>
                                                            <option value="1">已激活</option>
                                                            <option value="2">未激活</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="layui-form-item" style="display: inline-block;">
                                                <div class="layui-inline">
                                                    <div class="layui-input-inline">
                                                        <text class="yname">会员姓名</text>
                                                        <input type="text" name="user_name" placeholder="请输入会员姓名"
                                                               autocomplete="off" class="layui-input user_name">
                                                    </div>
                                                </div>
                                                <div class="layui-inline">
                                                    <div class="layui-input-inline">
                                                        <text class="yname">会员手机号</text>
                                                        <input type="text" name="user_phone" placeholder="请输入会员手机号"
                                                               autocomplete="off" class="layui-input user_phone">
                                                    </div>
                                                </div>
                                                <div class="layui-inline">
                                                    <div class="layui-input-inline">
                                                        <text class="yname">所属会员卡ID</text>
                                                        <input type="text" name="card_id" placeholder="请输入会员卡ID"
                                                               autocomplete="off" class="layui-input card_id">
                                                    </div>
                                                </div>
                                                <div class="layui-inline">
                                                    <button class="layui-btn layuiadmin-btn-list" lay-submit=""
                                                            lay-filter="LAY-app-contlist-search"
                                                            style="margin-top: 1.3rem;border-radius:5px;margin-bottom: 0;height:36px;line-height: 36px;">
                                                        <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                                    </button>
                                                </div>
                                                
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<input type="hidden" class="store_id">
<input type="hidden" class="status">
<script type="text/html" id="status">
    {{#  if(d.status =='1'){ }}
    已激活
    {{#  } else { }}
    未激活
    {{#  } }}
</script>
<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script src="<?php echo e(asset('/layuiadmin/layui/jquery-2.1.4.js')); ?>"></script>
<script src="<?php echo e(asset('/layuiadmin/layui/jquery.qrcode.min.js')); ?>"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'table', 'laydate'], function () {
        var $ = layui.$
            , admin = layui.admin
            , table = layui.table
            , form = layui.form
            , laydate = layui.laydate;
        // 未登录,跳转登录页面
        $(document).ready(function () {
            if (token == null) {
                window.location.href = "<?php echo e(url('/user/login')); ?>";
            }
        });
        $('.addSonStoreWeiXin').click(function () {
            sessionStorage.setItem('store_id', '0');
            $(this).attr('lay-href', "<?php echo e(url('/user/addSonStoreWeiXin')); ?>");
        });

        table.render({
            elem: '#test-table-page'
            , url: "<?php echo e(url('/api/member/storeMemberList')); ?>"
            , method: 'post'
            , where: {
                token: token,
            }
            , request: {
                pageName: 'p',
                limitName: 'l'
            }
            , page: true
            , cellMinWidth: 150
            , cols: [[
                {field: 'store_name', title: '所属门店'}
                , {field: 'user_name', title: '会员姓名', width: 150}
                , {field: 'user_phone', title: '会员手机号', width: 120}
                , {field: 'code_id', title: '会员卡号', width: 150}
                , {field: 'card_id', title: '所属会员卡ID'}
                , {field: 'status', title: '会员卡状态', toolbar: '#status', width: 100}
                , {field: 'user_bonus', title: '积分', width: 120}
                , {field: 'created_at', title: '创建时间', width: 170}
            ]]
            , response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                , statusCode: 1 //成功的状态码，默认：0
                , msgName: 'message' //状态信息的字段名称，默认：msg
                , countName: 't' //数据总数的字段名称，默认：count
                , dataName: 'data' //数据列表的字段名称，默认：data
            }
            , done: function (res, curr, count) {
                $('th').css({
                    'font-weight': 'bold',
                    'font-size': '15',
                    'color': 'black',
                    'background': 'linear-gradient(#f2f2f2,#cfcfcf)'
                });	//进行表头样式设置
            }

        });
        form.on('select(status)', function (data) {
            var status = data.value;
            $('.status').val(status);

            //执行重载
            table.reload('test-table-page', {
                where: {
                    status: status
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });
        $(".inputstore").bind("input propertychange", function (event) {
            store_name = $(this).val();
            if (store_name.length == 0) {
                $('.storebox').html('');
                $('.storebox').hide();
            } else {
                $.post("<?php echo e(url('/api/user/store_lists')); ?>",
                    {
                        token: token
                        , store_name: $(this).val()
                        , l: 100
                    }, function (res) {
                        var html = "";
                        if (res.t == 0) {
                            $('.storebox').html('');
                        } else {
                            for (var i = 0; i < res.data.length; i++) {
                                html += '<div class="list" data=' + res.data[i].store_id + '>' + res.data[i].store_name + '</div>'
                            }
                            $(".storebox").show();
                            $('.storebox').html('');
                            $('.storebox').append(html);
                        }
                    }, "json");
            }
        });
        $(".storebox").on("click", ".list", function () {
            $('.inputstore').val($(this).html());
            $('.store_id').val($(this).attr('data'));
            $('.storebox').hide();

            table.reload('test-table-page', {
                where: {
                    store_id: $(this).attr('data')
                }
                , page: {
                    curr: 1
                }
            });
        });
        form.on('submit(LAY-app-contlist-search)', function (data) {
            var user_name = data.field.user_name;
            var user_phone = data.field.user_phone;
            var card_id = data.field.card_id;
            var store_id = $('.store_id').val();
            var status = $('.status').val();
            //执行重载
            table.reload('test-table-page', {
                where: {
                    user_name: user_name,
                    user_phone: user_phone,
                    card_id: card_id,
                    store_id: store_id,
                    status: status
                }
                , page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });
    });


</script>

</body>
</html>