<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>APP我的功能</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
<link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
<style>
    .edit{background-color: #ed9c3a;}
    .shenhe{background-color: #429488;}
    .see{background-color: #7cb717;}
    .cur{color:#009688;}
    .manage{background-color:##6c8ff5;}
    .water{background-color:#5fb878;}
    .del{background-color: #e85052;}
    /*.laytable-cell-1-school_icon{height:100%;}*/
</style>
</head>
<body>

    <div class="layui-fluid">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md12">
                <div class="layui-fluid">
                    <div class="layui-row layui-col-space15">
                        <div class="layui-col-md12" style="margin-top:0px">
                            <div class="layui-card">
                                <div class="layui-card-header">APP我的功能</div>
                                <div class="layui-card-body">
                                    <div class="layui-btn-container">
                                        <a class="layui-btn layui-btn-primary addschool" lay-href="<?php echo e(url('/user/addappmyfunction')); ?>" style="background-color:#3475c3;border-radius: 5px;border:none;color:#fff;">添加APP我的功能</a>
                                    </div>
                                    <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                    <script type="text/html" id="imgTpl">
                                        <img style="display: inline-block;height: 100%;" src= {{d.icon }}>
                                    </script>
                                    <script type="text/html" id="table-content-list">
                                        <a class="layui-btn  layui-btn-xs del" lay-event="del">删除</a>
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" class="user_id">
    <input type="hidden" class="status">

<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script>
var token = sessionStorage.getItem("Usertoken");

layui.config({
    base: '../../layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index','form', 'table','laydate'], function(){
    var $ = layui.$
    ,admin = layui.admin
    ,table = layui.table
    ,form = layui.form
    ,laydate = layui.laydate;

    // 未登录,跳转登录页面
    $(document).ready(function(){
        if(token==null){
            window.location.href="<?php echo e(url('/user/login')); ?>";
        }
    });

    table.render({
        elem: '#test-table-page'
        ,url: "<?php echo e(url('/api/user/my_index')); ?>"
        ,method: 'post'
        ,where:{
            token:token
        }
        ,request:{
            pageName: 'p'
            ,limitName: 'l'
        }
        ,page: true
        ,cellMinWidth: 150
        ,cols: [[
            {field:'icon', title: '图标',templet: '#imgTpl'}
            ,{field:'title', title: '功能名称'}
            ,{field:'url',  title: '跳转链接'}
            ,{field:'sort',  title: '排序'}
            ,{width:80,align:'center', fixed: 'right', toolbar: '#table-content-list',title: '操作'}
        ]]
        ,response: {
            statusName: 'status' //数据状态的字段名称，默认：code
            ,statusCode: 1 //成功的状态码，默认：0
            ,msgName: 'message' //状态信息的字段名称，默认：msg
            ,countName: 't' //数据总数的字段名称，默认：count
            ,dataName: 'data' //数据列表的字段名称，默认：data
        }
        ,done: function(res, curr, count){
//        console.log(res);
            $('th').css({'font-weight': 'bold', 'font-size': '15','color': 'black','background':'linear-gradient(#f2f2f2,#cfcfcf)'});	//进行表头样式设置
        }
    });

    table.on('tool(test-table-page)', function(obj){
        var e = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        var tr = obj.tr; //获得当前行 tr 的DOM对象
        sessionStorage.setItem('stu_order_type_no', e.stu_order_type_no);

        if(layEvent === 'del'){
            layer.confirm('确认删除此消息?',{icon: 2}, function(index){
                $.ajax({
                    url : "<?php echo e(url('/api/user/del_my_index')); ?>",
                    data : {
                        token:token
                        ,id:e.id
                    },
                    type : 'post',
                    dataType:'json',
                    success : function(data) {
                        if(data.status==1){
                            obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                            layer.close(index);
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 1
                                ,time: 1000
                            });
                        }else{
                            layer.msg(data.message, {
                                offset: '50px'
                                ,icon: 2
                                ,time: 3000
                            });
                        }
                    }
                });
            });
        }
    });

});

</script>

</body>
</html>
