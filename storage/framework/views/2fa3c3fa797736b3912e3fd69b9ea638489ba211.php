<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>新增子商户</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style>
        .img {
            width: 130px;
            height: 90px;
            overflow: hidden;
        }

        .img img {
            width: 100%;
            height: 100%;
        }

        .layui-layer-nobg {
            width: none !important;
        }

        /*.layui-layer-content{width:600px;height:550px;}*/
        .layui-card-header {
            width: 80px;
            text-align: right;
            float: left;
        }

        .layui-card-body {
            margin-left: 28px;
        }

        .layui-upload-img {
            width: 100px;
            height: 92px; /*margin: 0 10px 10px 0;*/
        }

        .up {
            position: relative;
            display: inline-block;
            cursor: pointer;
            border-color: #1ab394;
            color: #FFF;
            width: auto !important;
            font-size: 10px !important;
            text-align: center !important;
        }

        .up input {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            opacity: .01;
            width: 100px;
            height: 30px;
        }

        .layui-upload-list {
            width: 100px;
            height: 96px;
            overflow: hidden;
            margin: 10px auto;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none !important;
            margin: 0;
        }

        .userbox, .storeBox {
            height: 200px;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 85px;
            width: 400px;
            background-color: #ffffff;
            border: 1px solid #ddd;
        }

        .userbox .list, .storeBox .list {
            height: 38px;
            line-height: 38px;
            cursor: pointer;
            padding-left: 10px;
        }

        .userbox .list:hover, .storeBox .list:hover {
            background-color: #eeeeee;
        }

        .scsfzzmtpnone {
            display: none;
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card" style="margin-top: 0px">
        <div class="layui-card-body layui-row layui-col-space10">
            <div class="layui-row layui-form">
                <div class="layui-col-md6">
                    <div class="layui-form-item storeitem" style="width:500px">
                        <label class="layui-form-label" style="text-align:center;">选择门店</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入门店名称" class="layui-input inputstore">
                            <div class="storeBox" style='display: none'></div>
                        </div>
                    </div>
                    <div class="layui-form-item" style="width:500px">
                        <label class="layui-form-label" style="text-align:center;">子商户APPID</label>
                        <div class="layui-input-block">
                            <input type="text" placeholder="请输入子商户APPID" class="layui-input son_appid">
                        </div>
                    </div>
                    <div class="layui-form-item" style="width:500px;">
                        <label class="layui-form-label" style="text-align:center;">一级卡券目录</label>
                        <div class="layui-input-block addressall">
                            <div class="layui-inline" style="margin-right:0;width:390px;">
                                <select name="primary_category_id" lay-filter="primary_category_id" id="primary_category_id">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item" style="width:500px;">
                        <label class="layui-form-label" style="text-align:center;">二级卡券目录</label>
                        <div class="layui-input-block addressall">
                            <div class="layui-inline" style="margin-right:0;width:390px;">
                                <select name="secondary_category_id" lay-filter="secondary_category_id" id="secondary_category_id">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label" style="text-align:center;">授权函开始日期</label>
                        <div class="layui-inline">
                            <div class="layui-input-inline" style="width:390px">
                                <input type="text" class="layui-input test-item bengin_time" placeholder="授权函开始日期" lay-key="25">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label" style="text-align:center;">授权函截止日期</label>
                        <div class="layui-inline">
                            <div class="layui-input-inline" style="width:390px">
                                <input type="text" class="layui-input test-item end_time" placeholder="授权函截止日期" lay-key="26">
                            </div>

                        </div>
                        <p style="padding-top:10px;font-size: 12px;color: red ">注意：授权函截止日期要与提交授权函的扫描件截止日期一致</p>
                    </div>
                </div>
                <div class="layui-col-md6" style="width:44%;margin-top:50px;">
                    <div class="layui-card">
                        <div class="layui-card-body" style="margin-left:28px;padding:0 50px;float:left;">
                            <div class="layui-upload">
                                <button class="layui-btn up" style="border-radius:5px;left:6px">
                                    <input type="file" name="img_upload" class="test1">门店LOGO
                                </button>
                                <div class="layui-upload-list">
                                    <img class="layui-upload-img" id="demo1">
                                    <p id="demoText"></p>
                                </div>
                            </div>
                        </div>
                        <div class="layui-card-body" style="margin-left:28px;padding:0 50px;float:left;">
                            <div class="layui-upload">
                                <button class="layui-btn up" style="border-radius:5px;left:6px">
                                    <input type="file" name="img_upload" class="test2">上传授权函
                                </button>
                                <div class="layui-upload-list">
                                    <img class="layui-upload-img" id="demo2">
                                    <p id="demoText"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block" style="margin-left:440px">
                        <div class="layui-footer">
                            <button class="layui-btn submit" style="border-radius:5px">保存</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" class="store_id">
<input type="hidden" class="primary_category_id">
<input type="hidden" class="secondary_category_id">
<input type="hidden" class="oauth_url">
<input type="hidden" class="logo_url">
<input type="hidden" class="protocol">
<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var store_name='';
    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form', 'upload', 'formSelects', 'laydate', 'jquery'], function () {
        var $ = layui.$
            , admin = layui.admin
            , element = layui.element
            , layer = layui.layer
            , laydate = layui.laydate
            , form = layui.form
            , upload = layui.upload
            , formSelects = layui.formSelects;
        var $ = jquery = layui.jquery;
        // 未登录,跳转登录页面
        $(document).ready(function () {
            if (token == null) {
                window.location.href = "<?php echo e(url('/user/login')); ?>";
            }
        });

        // 选择门店
        $(".inputstore").bind("input propertychange",function(event){
            store_name = $(this).val()
            if (store_name.length == 0) {
                $('.storeBox').html('')
                $('.storeBox').hide()
            } else {
                $.post("<?php echo e(url('/api/user/store_lists')); ?>",
                    {
                        token:token
                        ,store_name:$(this).val()
                        ,l:100
                    },function(res){
                        var html="";
                        if(res.t==0){
                            $('.storeBox').html('')
                        }else{
                            for(var i=0;i<res.data.length;i++){
                                html+='<div class="list" data='+res.data[i].store_id+'>'+res.data[i].store_name+'</div>'
                            }
                            $(".storeBox").show();
                            $('.storeBox').html('');
                            $('.storeBox').append(html);
                        }
                    },"json");
            }
        });

        $(".storeBox").on("click",".list",function(){

            $('.inputstore').val($(this).html())
            $('.store_id').val($(this).attr('data'))
            $('.storeBox').hide()
            $.ajax({
                url : "<?php echo e(url('/api/member/getCategoryList')); ?>",
                data : {token:token,store_id:$(".store_id").val()},
                type : 'get',
                dataType: 'json',
                success : function(data) {
                    var optionStr = "";
                    for(var i=0;i<data.data.length;i++){
                        optionStr += "<option value='" + data.data[i].primary_category_id + "'>"
                            + data.data[i].category_name + "</option>";
                    }
                    $("#primary_category_id").append('<option value="">请选择一级目录</option>'+optionStr);
                    layui.form.render('select');
                },
                error : function(data) {
                    // alert('查找板块报错');
                }
            });


        });

        form.on('select(primary_category_id)', function (data) {
            category = data.value;
            categoryName = data.elem[data.elem.selectedIndex].text;
            $('.primary_category_id').val(category);
            $.ajax({
                url : "<?php echo e(url('/api/member/getCategoryList')); ?>",
                data : {token:token,primary_category_id:category},
                type : 'get',
                dataType: 'json',
                success : function(data) {
                    var optionStr = "";

                    for(var i=0;i<data.data.length;i++){
                        optionStr += "<option value='" + data.data[i].secondary_category_id + "'>"
                            + data.data[i].category_name + "</option>";
                    }
                    $("#secondary_category_id").append('<option value="">请选择二级目录</option>'+optionStr);
                    layui.form.render('select');
                },
                error : function(data) {
                    // alert('查找板块报错');
                }
            },"json");
        });

        form.on('select(secondary_category_id)', function (data) {
            category = data.value;
            $('.secondary_category_id').val(category);
        });

        laydate.render({
            elem: '.bengin_time'
            ,done: function(value){

            }
        });
        laydate.render({
            elem: '.end_time'
            ,done: function(value){

            }
        });
        //门店LOGO
        var uploadInst = upload.render({
            url : "<?php echo e(url('/api/basequery/webupload?act=images')); ?>"+'&token='+token,  //提交到的地址 可以自定义其他参数
            elem : '.test1',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            ext : 'jpg|png|gif',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                    layui.jquery('#demo1').attr("src", res.data.img_url);
                    $(".logo_url").val(res.data.img_url)
                }
            }
        });
        //授权函
        var uploadInst = upload.render({
            url : "<?php echo e(url('/api/basequery/webupload?act=images')); ?>"+'&token='+token,  //提交到的地址 可以自定义其他参数
            elem : '.test2',  //指定元素的选择器，默认直接查找class为layui-upload-file的元素
            method : 'POST',    //设置http类型，如：post、get。默认post。也可以直接在input设置lay-method="get"来取代。
            type : 'images',    //[images 图片类型，默认][file普通文件类型][video视频文件类型][audio音频文件类型]
            ext : 'jpg|png|gif',    //自定义支持的文件格式
            unwrap : true, //是否不改变input的样式风格。默认false
            size : 5120,
            before : function(input){
                //执行上传前的回调  可以判断文件后缀等等
                layer.msg('上传中，请稍后......', {icon:16, shade:0.5, time:0});
            },
            done: function(res){
                if(res.status == 0){
                    layer.msg(res.msg, {icon:2, shade:0.5, time:res.time});
                }else{
                    $(".oauth_url").val(res.data.img_url)
                    $.post("<?php echo e(url('/api/member/getMediaId')); ?>",
                        {
                            token: token,
                            store_id: $(".store_id").val(),
                            oauth_url:$(".oauth_url").val()

                        }, function (res) {
                            if (res.status == 1) {
                                layer.msg(res.message, {
                                    offset: '50px'
                                    , icon: 1
                                    , time: 3000
                                });
                                layer.msg("文件上传成功", {icon:1, shade:0.5, time:res.time});
                                layui.jquery('#demo2').attr("src",$(".oauth_url").val() );
                                $(".protocol").val(res.data.media_id)
                            } else {
                                layer.msg(res.message, {
                                    offset: '50px'
                                    , icon: 2
                                    , time: 3000
                                });
                            }
                        }, "json");
                }
            }
        });


        // 营业执照时间选择
        laydate.render({
            elem: '.start-item'
            , done: function (value) {

            }
        });
        laydate.render({
            elem: '.end-item'
            , done: function (value) {

            }
        });
        // 提交创建子商户信息
        $('.submit').click(function () {
            $.post("<?php echo e(url('/api/member/addSonStoreWeiXin')); ?>",
                {
                    token: token,
                    store_id: $('.store_id').val(),
                    son_appid: $('.son_appid').val(),
                    logo_url: $('.logo_url').val(),
                    oauth_url: $('.oauth_url').val(),
                    protocol: $('.protocol').val(),
                    bengin_time: $('.bengin_time').val(),
                    end_time: $('.end_time').val(),
                    primary_category_id: $('.primary_category_id').val(),
                    secondary_category_id: $('.secondary_category_id').val()
                }, function (res) {
                    if (res.status == 1) {
                        layer.msg(res.message, {
                            offset: '50px'
                            , icon: 1
                            , time: 3000
                        });
                    } else {
                        layer.msg(res.message, {
                            offset: '50px'
                            , icon: 2
                            , time: 3000
                        });
                    }
                }, "json");
        });

    });

</script>
</body>
</html>
