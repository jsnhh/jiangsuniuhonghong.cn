<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>商户交易达标返现</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .tongbu {
            background-color: #4c9ef8;
            color: #fff;
        }

        .cur {
            color: #009688;
        }

        .storebox {
            height: 200px;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 42px;
            width: 298px;
            background-color: #ffffff;
            border: 1px solid #ddd;
        }

        .storebox .list {
            height: 38px;
            line-height: 38px;
            cursor: pointer;
            padding-left: 10px;
        }

        .storebox .list:hover {
            background-color: #eeeeee;
        }

        .userbox {
            height: 190px;
            margin-right: 10px;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 60px;
            width: 298px;
            background-color: #ffffff;
            border: 1px solid #ddd;
        }

        .userbox .list {
            height: 38px;
            line-height: 38px;
            cursor: pointer;
            padding-left: 10px;
        }

        .userbox .list:hover {
            background-color: #eeeeee;
        }

        .yname {
            font-size: 13px;
            color: #444;
        }

    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12" style="margin-top:0px">

            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12">
                        <div class="layui-card">

                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">
                                
                                
                                <!-- 选择设备 -->
                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:190px;margin-right:10px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <div class="layui-input-block" style="margin-left:0">
                                                <text class="yname">选择规则</text>
                                                <select name="rules" id="rules" lay-filter="rules" lay-search>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:190px;margin-right:10px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <div class="layui-input-block" style="margin-left:0;border-radius:5px">
                                                <text class="yname">代理商名称</text>
                                                <input type="text" name="agent_name" lay-verify="agent_name"
                                                       autocomplete="off" placeholder="请输入代理商名称"
                                                       class="layui-input transfer">
                                                <div class="userbox" style='display: none'></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:190px;margin-right:10px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <div class="layui-input-block" style="margin-left:0;border-radius:5px">
                                                <text class="yname">激活奖励状态</text>
                                                <select name="standard_status" id="standard_status"
                                                        lay-filter="standard_status">
                                                    <option value="">全部</option>
                                                    <option value="1">达标</option>
                                                    <option value="0">未达标</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:190px;margin-right:10px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <div class="layui-input-block" style="margin-left:0;border-radius:5px">
                                                <text class="yname">T+1月奖励状态</text>
                                                <select name="first_status" id="first_status" lay-filter="first_status">
                                                    <option value="">全部</option>
                                                    <option value="1">达标</option>
                                                    <option value="0">未达标</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:190px;margin-right:10px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <div class="layui-input-block" style="margin-left:0;border-radius:5px">
                                                <text class="yname">T+2月奖励状态</text>
                                                <select name="again_status" id="again_status" lay-filter="again_status">
                                                    <option value="">全部</option>
                                                    <option value="1">达标</option>
                                                    <option value="0">未达标</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:100%;display: flex;">
                                        <div class="layui-form-item" style="display: inline-block;">
                                            <div class="layui-inline">
                                                <div class="layui-input-inline">
                                                    <text class="yname">商户名称</text>
                                                    <input type="text" name="store_name" id="store_name" placeholder="请输入商户名称"
                                                           autocomplete="off" class="layui-input">
                                                </div>
                                            </div>
                                            <div class="layui-inline">
                                                <button class="layui-btn layuiadmin-btn-list" lay-submit=""
                                                        lay-filter="LAY-app-contlist-search"
                                                        style="margin-top: 1.3rem;border-radius:5px;margin-bottom: 0;height:36px;line-height: 36px;">
                                                    <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                                </button>
                                            </div>
                                            <div class="layui-inline">
                                                <button class="layui-btn layuiadmin-btn-list export"
                                                        style="border-radius:5px;margin-top:20px;margin-bottom: 4px;height:36px;line-height: 36px;margin-top: 1.5rem">
                                                    导出
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                            <!-- 判断状态 -->
                            <script type="text/html" id="table-content-list" class="layui-btn-small">
                                <a class="layui-btn layui-btn-normal layui-btn-xs details"
                                   lay-event="details">详情</a>
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<input type="hidden" class="rule_id" value="">
<input type="hidden" class="agent_id" value="">
<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'table', 'laydate'], function () {
        var $ = layui.$
            , admin = layui.admin
            , form = layui.form
            , table = layui.table
            , laydate = layui.laydate;
        // 未登录,跳转登录页面
        $(document).ready(function () {
            if (token == null) {
                window.location.href = "<?php echo e(url('/user/login')); ?>";
            }
        })

        // 渲染表格
        table.render({
            elem: '#test-table-page'
            , url: "<?php echo e(url('/api/user/store_transaction_reward_list')); ?>"
            , method: 'post'
            , where: {
                token: token
            }
            , request: {
                pageName: 'p',
                limitName: 'l'
            }
            , page: true
            , cellMinWidth: 150
            , cols: [[
                {field: 'rule_name', align: 'center', title: '活动名称'}
                , {field: 'agent_name', align: 'center', title: '所属代理商'}
                , {field: 'store_name', align: 'center', title: '商户名称'}
                , {
                    field: 'standard_status', align: 'center', title: '激活奖励状态',
                    templet: function (d) {
                        if (d.standard_status == 1) {
                            res = "<p style='color: #0bb20c'>达标</p>"
                        } else {
                            res = "<p style='color: #bd2130'>未达标</p>"
                        }
                        return res;
                    }
                }
                , {field: 'standard_done_total', align: 'center', title: '激活期间交易笔数'}
                , {field: 'standard_time', align: 'center', title: '激活奖励时间'}
                , {field: 'standard_done_amt', align: 'center', title: '激活奖励金额'}
                , {
                    field: 'first_status', align: 'center', title: 'T+1月奖励状态',
                    templet: function (d) {
                        if (d.first_status == 1) {
                            res = "<p style='color: #0bb20c'>达标</p>"
                        } else {
                            res = "<p style='color: #bd2130'>未达标</p>"
                        }
                        return res;
                    }
                }
                , {field: 'first_done_total', align: 'center', title: 'T+1月交易笔数'}
                , {field: 'first_total_amt', align: 'center', title: 'T+1月交易总额'}
                , {field: 'first_done_amt', align: 'center', title: 'T+1月奖励金额'}
                , {
                    field: 'again_status', align: 'center', title: 'T+2月奖励状态',
                    templet: function (d) {
                        if (d.again_status == 1) {
                            res = "<p style='color: #0bb20c'>达标</p>"
                        } else {
                            res = "<p style='color: #bd2130'>未达标</p>"
                        }
                        return res;
                    }
                }
                , {field: 'again_done_total', align: 'center', title: 'T+2月交易笔数'}
                , {field: 'again_total_amt', align: 'center', title: 'T+2月交易总额'}
                , {field: 'again_done_amt', align: 'center', title: 'T+2月奖励金额'}
                , {field: 'total_amt', align: 'center', title: '奖励总金额'}
                , {width: 100, align: 'center', fixed: 'right', toolbar: '#table-content-list', title: '操作'}
            ]]
            , response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                , statusCode: 1 //成功的状态码，默认：0
                , msgName: 'message' //状态信息的字段名称，默认：msg
                , countName: 't' //数据总数的字段名称，默认：count
                , dataName: 'data' //数据列表的字段名称，默认：data
            }
            , done: function (res, curr, count) {
                $('th').css({
                    'font-weight': 'bold',
                    'font-size': '15',
                    'color': 'black',
                    'background': 'linear-gradient(#f2f2f2,#cfcfcf)'
                });	//进行表头样式设置
            }

        });
        getSelRule();

        function getSelRule() {
            $.ajax({
                url: "<?php echo e(url('/api/user/selRule')); ?>",
                type: 'get',
                data: {token: token},
                success: function (data) {
                    //console.log(data);
                    var optionStr = "";
                    for (var i = 0; i < data.data.length; i++) {
                        optionStr += "<option value='" + data.data[i].id + "'>" + data.data[i].rule_name + "</option>";
                    }
                    $("#rules").append('<option value="">选择规则</option>' + optionStr);
                    layui.form.render('select');
                },
                error: function (data) {
                    alert('查找板块报错');
                }
            });
        }

        $(".transfer").bind("input propertychange", function (event) {
            //  console.log($(this).val())
            var user_name = $(this).val();
            if (user_name.length == 0) {
                $('.userbox').html('');
                $('.userbox').hide();
            } else {
                $.post("<?php echo e(url('/api/user/get_sub_users')); ?>",
                    {
                        token: token,
                        user_name: user_name,
                        self: '1'
                    }, function (res) {
                        //   console.log(res);
                        var html = "";
//                console.log(res.t);
                        if (res.t == 0) {
                            $('.userbox').html('');
                            $('.userbox').hide();
                        } else {
                            for (var i = 0; i < res.data.length; i++) {
                                html += '<div class="list" data=' + res.data[i].id + '>' + res.data[i].name + '-' + res.data[i].level_name + '</div>'
                            }
                            $(".userbox").show();
                            $('.userbox').html('');
                            $('.userbox').append(html);
                        }
                    }, "json");
            }
        });
        $(".userbox").on("click", ".list", function () {
            $('.transfer').val($(this).html());
            $('.js_user_id').val($(this).attr('data'));
            $('.userbox').hide();
            $('.agent_id').val($(this).attr('data'))
        });

        //选择活动名称
        form.on('select(rules)', function (data) {
            category = data.value;
            $('.rule_id').val(category)
        });
        //监听搜索
        form.on('submit(LAY-app-contlist-search)', function () {
            var agent_id = $('.agent_id').val()
            var rule_id = $('.rule_id').val()
            var store_name = $('#store_name').val()
            var standard_status = $('#standard_status').val()
            var first_status = $('#first_status').val()
            var again_status = $('#again_status').val()
            //执行重载
            table.reload('test-table-page', {
                page: {
                    curr: 1
                },
                where: {
                    agent_id: agent_id,
                    rule_id:rule_id,
                    store_name:store_name,
                    standard_status:standard_status,
                    first_status:first_status,
                    again_status:again_status
                }
            });
        });
        table.on('tool(test-table-page)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var e = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            //console.log(e);
            // sessionStorage.setItem('s_store_id', e.store_id);

            
            
            
            
            

            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            

            
            
            if (layEvent === 'details') {
                sessionStorage.setItem('store_id', e.store_id);
                $('.details').attr('lay-href', "<?php echo e(url('/user/detailsStoreTransactionReward')); ?>");
            }

        });
        $('.export').click(function(){
            var agent_id = $('.agent_id').val()
            var rule_id = $('.rule_id').val()
            var store_name = $('#store_name').val()
            var standard_status = $('#standard_status').val()
            var first_status = $('#first_status').val()
            var again_status = $('#again_status').val()
            window.location.href="<?php echo e(url('/api/export/store_transaction_reward_export_down')); ?>"+"?token="+token+"&agent_id="+agent_id+"&rule_id="+rule_id
            +"&store_name="+store_name+"&standard_status="+standard_status+"&first_status="+first_status+"&again_status="+again_status;
        })

    });

</script>

</body>
</html>





